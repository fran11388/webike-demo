
<html>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>系統維護中</title>
</head>

<body>
<div class="container maintain">
    <div class="row">
        <div class="span2 content-lr">
        </div>
        <div class="span8-cart content-lr">
            <div>
                <div class="wb-logo">
                    <img src="//img.webike.tw/shopping/image/logo.png" alt="">
                </div>
                <div>
                    <p class="er-fix">
                        各位「Webike台灣」會員好<br/>
                        為了提供更好的服務品質<br/>
                        2018/04/25 17:50至2018/04/25 20:00將進行伺服器升級工程<br/>
                        並進行系統效能調教，將暫時停機進行作業<br/>
                        服務信箱：
                        <a href="mailto:service@webike.tw">service@webike.tw</a>
                        <br>感謝您的配合，請多多包涵。
                    </p>
                </div>
                <div>
                    <img  src="//img.webike.tw/shopping/image/404.jpg" alt="">
                </div>
                <div class="fix-time">
                    系統維護中，請稍後……………<br>
                    網站將在<span style="color:red;">04/25 20:00</span>維護完畢
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    .container {
        margin-right: auto;
        margin-left: auto;
        display: table;
        line-height: 0;
        content: "";
        clear: both;
        display: table;
        line-height: 0;
        content: "";
        text-shadow: none;
        line-height: 1.4;
        font-family: Helvetica,"Microsoft JhengHei","新細明體", Verdana, "LiHei Pro", sans-serif;
        font-size: 20px;
        color: #333333;

    }
    .maintain .row {
        margin-left: 100px;
        display: table;
        content: "";
    }
    .more-7 a{
        display: block;
        padding: 5px;
        width: 200px;
        text-align: center;
        margin: 0 auto;
        background: #ccc;
        color: #000;
        text-decoration: none;
    }
    p.er-fix{
        font-size: 16px;
        width: 500px;
        line-height: 26px;
    }
    .fix-time{
        margin-left:40px;
        width: 80%;
        height: 130px;
        border-radius: 5px;
        border: 1px solid;
        border-color: #8e8e8e;
        background: #e3e3e3;
        text-align: center;
        font-size: 26px;
        line-height: 50px;
        padding-top: 20px;
    }
</style>
