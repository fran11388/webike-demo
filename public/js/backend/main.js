$(document).on('click', '.pre-hide-ui .pre-hide-release', function(){
    if($(this).prop('checked') || $(this).attr('class').indexOf('active') >= 0){
        $(this).closest('.pre-hide-ui').find('.pre-hide-box').removeClass('hide');
    }else{
        $(this).closest('.pre-hide-ui').find('.pre-hide-box').addClass('hide');
        clearInputsInBlock('.pre-hide-box');
    }
});

$(document).on('click', '.panel .panel-heading', function(){
    var body = $(this).closest('.panel').find('.panel-body');
    if(body.prop('class').indexOf('hide') > 0){
        body.removeClass('hide');
    }else{
        body.addClass('hide');
    }
});

function clearInputsInBlock(class_name) {
    $(class_name).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'text':
            case 'textarea':
            case 'file':
            case 'select-one':
            case 'select-multiple':
            case 'date':
            case 'number':
            case 'tel':
            case 'email':
                jQuery(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
                break;
        }
    });

    $(class_name).find('select').each(function() {
        $(this).find('option:selected').removeAttr('selected');
    });
    $(class_name).find('select[multiple]').each(function() {
        $(this).find('option').remove();
    });
}

function runMotorAjaxSelect2(){
    $('.select-motors').select2({
        width:"100%",
        minimumInputLength: 1,
        language: {
            inputTooShort: function(args) {
                // args.minimum is the minimum required length
                // args.input is the user-typed text
                return "請輸入至少"+ args.minimum + "個字元";
            },
//                errorLoading: function() {
//                    return "發生錯誤";
//                },
            noResults: function() {
                return "找不到結果";
            },
            searching: function() {
                return "搜尋中...";
            }
        },
        ajax: {
            url: "/api/motor/manufacturer/model",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    motor_name: params.term, // search term
                    key: 'id',
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.key, text: obj.name };
                    })
                };
            }
        }
    });
}

function runManufacturerAjaxSelect2(){
    $('.select-manufacturers').select2({
        width:"100%",
        minimumInputLength: 1,
        language: {
            inputTooShort: function(args) {
                // args.minimum is the minimum required length
                // args.input is the user-typed text
                return "請輸入至少"+ args.minimum + "個字元";
            },
//                errorLoading: function() {
//                    return "發生錯誤";
//                },
            noResults: function() {
                return "找不到結果";
            },
            searching: function() {
                return "搜尋中...";
            }
        },
        ajax: {
            url: "/api/product/manufacturer/model",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    manufacturer_name: params.term, // search term
                    key: 'id',
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.key, text: obj.name };
                    })
                };
            }
        }
    });
}
