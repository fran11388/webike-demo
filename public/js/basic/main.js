// JavaScript Document
$( document ).ready(function() {
	
	checkWindowSize();

	//Check window size
	function checkWindowSize(){
		if($(window).width() < 767){
	     	mobile();
	      //mobile
	      	
	      //End Mobile
	    }else{
	     	
	      //Desktop
			desktop();
	     //End Desktop
	    }
	}
	$(window).resize(function() {
     	//console.log($(window).width());   
     	checkWindowSize();
 	});

	//Function mobile
	function mobile(){
		// console.log('mobile-d');
		var breadcrumb_mobile = '.breadcrumb-mobile';
		$(breadcrumb_mobile).animate( { scrollLeft: $(breadcrumb_mobile).width() }, 1000 );

		var $b = $('body');
		if($b.hasClass('desktop')){
        	$b.removeClass('desktop');
        }
        $b.addClass('mobile-d');

		$(".menu-shopping").hide();
		showsecondMenu = 0;
      	var contador = 1;
		$('.icon-search').click(function(){
			if (contador == 1) {
				$(".container-header").show();
				$(".content-top-page").css({"margin-top": "115px"});
				contador = 0;
			} else {
				$(".container-header").hide();
				$(".content-top-page").css({"margin-top": "55px"});
				contador = 1;
			}
		});
		$(".content-top-page").css({"margin-top": "55px"},3000);

		/*
		$(document).ready(function() {
			$('.ct-left-search-list > ul > li > .slide-down-btn').click();
			var boxB = $('.box-page-group');
			boxB.find('.title-box-page-group').removeClass('clicked');
			boxB.find('.title-box-page-group').addClass('clicked');
			boxB.find('.ct-box-page-group').hide();
		});
		*/
	}

	//Function desktop
	function desktop(){

		// console.log('pc');
		var $b = $('body'),
        $fixed = $('.col3-ct .static-banners-fix');
        if($b.hasClass('mobile-d')){
        	$b.removeClass('mobile-d');
        }
        $b.addClass('desktop');
        if ($fixed.length) {
            jQuery.getScript('https://bbarakaci.github.io/fixto/dist/fixto.min.js', function() {
                $fixed.fixTo('#contents', {
                    top: $('nav#mainNav').height(),
                    useNativeSticky: false
                });
            });
        }	

			// Show and hide also like
		var contador = 1;
		function main () {
		$('.title-content-below-post, .thumbnail-below-post').click(function(){
			if (contador == 1) {
				$(".also-like").show(200);
				contador = 0;
			} else {
				$(".also-like").hide(200);
				contador = 1;
			}
		});
		$('.btn-close-also-like').click(function(){
				$(".also-like").hide(200);
				contador = 1;
		});

		}
		var showsecondMenu = 0;
		if($b.hasClass('desktop')){
			$('.btn-show-second-menu').click(function(){
				if (showsecondMenu == 0) {
					$(".menu-shopping").show();
					showsecondMenu = 1;
				} else {
					$(".menu-shopping").hide();
					showsecondMenu = 0;
				}
			});
		}

		// Hiden Top Menu
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $('.header').outerHeight();

		$(window).scroll(function(event){
			didScroll = true;
		});

		setInterval(function() {
			if (didScroll && $b.hasClass('desktop')) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);
		function hasScrolled() {
			var st = $(this).scrollTop();
			// Make sure they scroll more than delta
			if(Math.abs(lastScrollTop - st) <= delta)
				return;
			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (st > lastScrollTop && st > navbarHeight){
				// Scroll Down
				$('.header').removeClass('nav-down').addClass('nav-up');
				$(".breadcrumb-mobile").css({"margin-top": "175px"},3000);
				if (showsecondMenu == 0) {
					$(".menu-shopping").css({"display": "none"},3000);
				}
				$('.col1-logo').addClass('active-second-menu');

			} else {
				// Scroll Up
				if(st + $(window).height() < $(document).height()) {
					$('.header').removeClass('nav-up').addClass('nav-down');
					$(".breadcrumb-mobile").css({"margin-top": "150px"},3000);
					if (showsecondMenu == 0) {
					  $(".menu-shopping").css({"display": "block"},3000);
					}
					$('.col1-logo').removeClass('active-second-menu');
				}
			}

			lastScrollTop = st;
		}

		var boxB = $('.box-page-group');
		boxB.find('.title-box-page-group').removeClass('clicked');
		boxB.find('.ct-box-page-group').show();
	}
});
// All size
$(".my-bike").click(function(){
    $(".my-bike").addClass("open");
});
// carousel for 4 items
$('.owl-carousel-4').addClass('owl-carousel').owlCarousel({
	loop:false,
	nav:true,
	margin:5,
	slideBy : 4,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:2
		},
		1000:{
			items:4
		}
	}
});
// carousel for 5 items
$('.owl-carousel-5').addClass('owl-carousel').owlCarousel({
	loop:false,
	nav:true,
	margin:5,
    slideBy : 5,
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:5
		}
	}
})
// carousel for 6 items
$('.owl-carousel-6').addClass('owl-carousel').owlCarousel({
	loop:false,
	nav:true,
	margin:5,
    slideBy : 6,
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:6
		}
	}
})
// carousel for 6 items
$('.owl-carousel-7').addClass('owl-carousel').owlCarousel({
	loop:false,
	nav:true,
	margin:5,
	slideBy : 7,
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:7
		}
	}
})
$('.owl-carousel').addClass('owl-carousel').owlCarousel({
	loop:false,
	nav:true,
	margin:5,
    slideBy : 5,
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:5
		}
	}
})

function loadOwlCarousel(){
	$('.owl-carousel2').addClass('owl-carousel').owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		loop:false,
		margin:5,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	})
}

loadOwlCarousel();

var carousel3 = {
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	loop:false,
	margin:5,
	nav:true,
	slideBy : 7,
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:7
		}
	}
};

$('.owl-carousel3').addClass('owl-carousel').owlCarousel(carousel3);
function loadOwlCarousel3(){
	$('.owl-carousel3').addClass('owl-carousel').owlCarousel(carousel3);
}



//with nav image
$('.owl-carousel-history').addClass('owl-carousel').owlCarousel({
	loop:false,
	nav:true,
	margin:5,
	slideBy : 6,
	URLhashListener:true,
	startPosition: 'URLHash',
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:6
		}
	}
})


$('.owl-carouse-advertisement').addClass('owl-carousel').owlCarousel({
	autoplay:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	loop:false,
	margin:10,
	nav:true,
    slideBy : 3,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});

$('.owl-carousel:not(.custom)').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
$('.owl-carousel:not(.custom)').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
$('.owl-carousel2:not(.custom)').find('.owl-prev').addClass('btn-owl-prev');
$('.owl-carousel2:not(.custom)').find('.owl-next').addClass('btn-owl-next');
function reloadOwlControl()
{
	$('.owl-carousel:not(.custom)').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
	$('.owl-carousel:not(.custom)').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
}

$( document ).ready(function() {
	
	// resize windown to use OWL on tab
	$('.item-product-grid a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var target = $($(this).attr('href'));
		target.find('.product-grid').data('owlCarousel').onResize();
	});
	 
	 $('.slideshow-top').slick({
	  centerMode: true,
	  centerPadding: '20%',
	  slidesToShow:1,
		 autoplay: true,
		 autoplaySpeed: 6000,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			arrows: true,
			centerMode: true,
			centerPadding: '0',
			slidesToShow: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			arrows: false,
			centerMode: true,
			centerPadding: '0',
			slidesToShow: 1
		  }
		}
		]
	   });

		$('.img-slider-for2').slick({
			slidesToShow: 1,
			arrows: false,
			fade: true,
			adaptiveHeight: true,
			mobileFirst: true,
            centerMode: true,
			asNavFor: '.img-slider-nav2'
		});
		$('.img-slider-nav2').slick({
			slidesToShow: 7,
			centerMode: true,
			asNavFor: '.img-slider-for2',
			focusOnSelect: true,
            arrows: true,
            infinite: true,
            autoplaySpeed: 5000,
			responsive: [
				{
					breakpoint: 925,
					settings: {
						slidesToShow: 3,
						asNavFor: '.img-slider-for2',
						centerMode: true,
						focusOnSelect: true
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow:3,
						asNavFor: '.img-slider-for2',
						centerMode: true,
						focusOnSelect: true
					}
				}
			]
		});
	   
		$('.img-slider-for').slick({
		  slidesToShow: 1,
		  arrows: false,
		  fade: true,
			adaptiveHeight: true,
			mobileFirst: true,
		  asNavFor: '.img-slider-nav'
		});
		$('.img-slider-nav').slick({
			lazyLoad: 'ondemand',
		  slidesToShow: 5,
			slidesToScroll: 1,
		  asNavFor: '.img-slider-for',
		  centerMode: false,
		  focusOnSelect: true,
			responsive: [
				{
					breakpoint: 925,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						asNavFor: '.img-slider-for',
						centerMode: false,
						focusOnSelect: true
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow:3,
						slidesToScroll: 1,
						asNavFor: '.img-slider-for',
						centerMode: false,
						focusOnSelect: true
					}
				}
			]
		});

	// Tap country
	$(".tap-box-country").click(function(){
		$(this).addClass("active").siblings('li').removeClass('active');
		$(".container-show-info-brand").hide();
		$('.logo-brand').removeClass('active');
	});

	$(".tap-box-japan").click(function(){
		$("#japan").addClass("active-ct-tap-country").siblings('ul').removeClass('active-ct-tap-country');
	});
	$(".tap-box-eu").click(function(){
		$("#eu").addClass("active-ct-tap-country").siblings('ul').removeClass('active-ct-tap-country');
	});

	$(".tap-box-thai").click(function(){
		$("#thai").addClass("active-ct-tap-country").siblings('ul').removeClass('active-ct-tap-country');
	});

	$(".logo-brand").click(function(){
		$('.logo-brand').removeClass('active');
		$(this).addClass("active");
	});

	$(".logo-brand-honda").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-honda").show();
	});
	$(".logo-brand-yamaha").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-yamaha").show();
	});
	$(".logo-brand-suzuki").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-suzuki").show();
	});
	$(".logo-brand-kawasaki").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-kawasaki").show();
	});

	$(".logo-brand-harley").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-harley").show();
	});
	$(".logo-brand-hondathai").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-hondathai").show();
	});

	$(".logo-brand-kymcotw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-kymcotw").show();
	});
	$(".logo-brand-symtw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-symtw").show();
	});
	$(".logo-brand-yamahatw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-yamahatw").show();
	});
	$(".logo-brand-suzukitw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-suzukitw").show();
	});
	$(".logo-brand-aeontw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-aeontw").show();
	});
	$(".logo-brand-pgotw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-pgotw").show();
	});
	$(".logo-brand-harttw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-harttw").show();
	});
	$(".logo-brand-cpitw").click(function(){
		$(".container-show-info-brand").hide();
		$(".show-info-brand-cpitw").show();
	});
	// customer Q & A show

	//Special select box
	$(".select2").select2( { width:"100%"});

	var contador = 1;
	$(".btn-see-all-pagination-fix-model").click(function(){
		var target = $(this).parent().find(".show-more-fix-model");
		if (!target.is(':visible')) {
			target.show();
		} else {
			target.hide();
		}
	});

	$(".btn-tab").click(function(){
		$(".container-tap-inspired-by-your").css({"width": "10px"});
		$(".container-tap-inspired-by-your").css({"width": "100%"},100);
	});


	// sroll menu product detail
	$(".nav-tabs a").click(function(){
		$(this).tab('show');
	});
	$('body').scrollspy({target: "#myNavbar", offset: 200});
	$("#myNavbar a").on('click', function(event) {
		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
			// Prevent default anchor click behavior
			// event.preventDefault();
			// Store hash
			var hash = this.hash;
			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top - 110
			}, 800, function(){
				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});
		} // End if
	});
});

// google map
function myMap() {
	var mapOptions = {
		center: new google.maps.LatLng(10.762204, 106.676103),
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.HYBRID
	}
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}

$(".close").click(function(){
    $("#answers").hide();
});
$(".popup").click(function(){
    $("#answers").show();
});

$(window).scroll(function() {
	$('.bookmark-ui').hide();
	clearTimeout($.data(this, 'scrollTimer'));
	$.data(this, 'scrollTimer', setTimeout(function() {
		$('.bookmark-ui').fadeIn();
	}, 100));
});


function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    // If Internet Explorer, return version number
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('.item-product-grid a figure').each(function(){
            var display = $(this).css('display');
            if(display == 'table-cell'){
                $(this).css('display', 'inline-block');
            }
        });
    }
}

function validEmail(v) {
	var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
	return (v.match(r) == null) ? false : true;
}

$(document).ready(function(){
    // $("#m-menu").mmenu();
    runMMenu();
    $(window).resize(function(){
        if($(window).width() < 767){
            $('.top-shopping').css('cssText', 'margin-top:0 !important');
        }else{
            $('.top-shopping').removeAttr('style');
        }
        1        });

    function runMMenu(){
        var $originalNavigation = $("#m-menu");
        var $mobileNavigation = $();
        $mobileNavigation = $originalNavigation.mmenu({
            // option
            "offCanvas": {
                "zposition": "front"
            },
            initMenu:function(menu){
                menu.removeClass('hidden-xs');
                menu.find('.select2-container').remove();
                menu.find('.select2').select2();
                menu.find('.li-left-title > .slide-down-btn.clicked').removeClass('clicked');
                menu.find('.li-left-title > .slide-down-btn').addClass('clicked');
                menu.find('.li-left-title .slide-down-btn i.fa-chevron-up').attr('class', '').attr('class', 'fa fa-chevron-down');
                menu.find('.li-left-title .slide-down-btn i.fa-minus').attr('class', '').attr('class', 'fa fa-plus');
                menu.find('.li-left-title ul, .li-left-title ul li').hide();
                menu.find('.li-left-title .ul-sub2 .ul-sub2').css('display', 'block');
                menu.find('.li-left-title .ul-sub2 .ul-sub2 .li-sub2').css('display', 'list-item');

            }
        }, {
            // configuration
            clone: true,
        });
    }
    // iphone select2 bug
    $('#mm-m-menu .ul-cell-dropdown-list').click(function(){
        $('html').removeClass('mm-opened');
    });

    $('form[method="POST"]').append($('<input type="hidden" name="_token" value="'+ $('meta[name="csrf-token"]').attr('content') +'">'));
    jQuery(function(){
        var topBtn = jQuery('#pagetop');
        topBtn.hide();
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 100) {
                topBtn.fadeIn();
            } else {
                topBtn.fadeOut();
            }
        });
        //スクロールしてトップ
        topBtn.click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });

        var agent = navigator.userAgent;
        if (agent.indexOf('iPhone') > 0 ||
            (agent.indexOf('Android') > 0 && agent.indexOf('Mobile') > 0)) {
            jQuery("a").attr("target","_top");
        }

    });

    $('.box-page-group .title-box-page-group').on('click', function(){
        var area = $(this).closest('.box-page-group');
        area.find('.ct-box-page-group').slideToggle(100);
        if ($(this).hasClass('clicked')) {
            var icon = $(this).find('i')
            if (icon.hasClass('fa-chevron-down')) {
                icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else if (icon.hasClass('fa-plus')) {
                icon.removeClass('fa-plus').addClass('fa-minus');
            }
        }else{
            var icon = $(this).find('i')
            if (icon.hasClass('fa-chevron-up')) {
                icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            } else if (icon.hasClass('fa-minus')) {
                icon.removeClass('fa-minus').addClass('fa-plus');
            }
        }
    });

    msieversion();


});