var path = '';
var string = window.location.href;
if(string.indexOf("dev/") >= 0){
    path = '/dev/index.php';
}


function addParameter(url, parameterName, parameterValue, atStart/*Add param before others*/){
    replaceDuplicates = true;
    if(url.indexOf('#') > 0){
        var cl = url.indexOf('#');
        urlhash = url.substring(url.indexOf('#'),url.length);
    } else {
        urlhash = '';
        cl = url.length;
    }
    sourceUrl = url.substring(0,cl);

    var urlParts = sourceUrl.split("?");
    var newQueryString = "";

    if (urlParts.length > 1)
    {
        var parameters = urlParts[1].split("&");
        for (var i=0; (i < parameters.length); i++)
        {
            var parameterParts = parameters[i].split("=");
            if (!(replaceDuplicates && parameterParts[0] == parameterName))
            {
                if (newQueryString == "")
                    newQueryString = "?";
                else
                    newQueryString += "&";
                newQueryString += parameterParts[0] + "=" + (parameterParts[1]?parameterParts[1]:'');
            }
        }
    }
    if (newQueryString == "")
        newQueryString = "?";

    if(atStart){
        newQueryString = '?'+ parameterName + "=" + parameterValue + (newQueryString.length>1?'&'+newQueryString.substring(1):'');
    } else {
        if (newQueryString !== "" && newQueryString != '?')
            newQueryString += "&";
        newQueryString += parameterName + "=" + (parameterValue?parameterValue:'');
    }
    return urlParts[0] + newQueryString + urlhash;
};

function isiPhone(){
    return (
        (navigator.platform.indexOf("iPhone") != -1) ||
        (navigator.platform.indexOf("iPod") != -1)
    );
}

function slipTo(element){
    var navbar = '#mainNav';

    if(!(element instanceof jQuery)){
        element = $(element);
    }
    if ($(navbar).length) {
        var y = parseInt(element.offset().top) - parseInt($(navbar).height());
        if($(navbar).prop('class').indexOf('nav-up') >= 0 && $('.menu-shopping').height() != null){
            y -= parseInt($('.menu-shopping').height());
        }
        $('html,body').animate({scrollTop: y}, 400);
    }else{
        y = parseInt(element.offset().top);
        $('html,body').animate({scrollTop: y}, 400);
    }
}

function goNextPage(){
    var max_number = 1;
    var split_text = '-';
    var now_id = $('.pages-action.active').prop('id');
    $('.pages-action').each(function(key, element){
        var element_id = $(element).prop('id');
        var element_number = parseInt(element_id.substr(element_id.indexOf(split_text) + split_text.length, element_id.length))
        if( element_number > max_number){
            max_number = element_number;
        }
    });
    var next_number = parseInt(now_id.substr(now_id.indexOf(split_text) + split_text.length, now_id.length)) + 1;
    if(max_number >= next_number){
        var next_id = now_id.substr(0, now_id.indexOf(split_text) +  split_text.length) + next_number;
        $('.pages-action').removeClass('active');
        $('#' + next_id).addClass('active');
        slipTo('#' + next_id);
    }else{
        return false;
    }
}

function goPrevPage(){
    var min_number = 1;
    var split_text = '-';
    var now_id = $('.pages-action.active').prop('id');
    $('.pages-action').each(function(key, element){
        var element_id = $(element).prop('id');
        var element_number = parseInt(element_id.substr(element_id.indexOf(split_text) + split_text.length, element_id.length))
        if( element_number < min_number){
            min_number = element_number;
        }
    });
    var prev_number = parseInt(now_id.substr(now_id.indexOf(split_text) + split_text.length, now_id.length)) - 1;
    if(min_number <= prev_number){
        var prev_id = now_id.substr(0, now_id.indexOf(split_text) +  split_text.length) + prev_number;
        $('.pages-action').removeClass('active');
        $('#' + prev_id).addClass('active');
        slipTo('#' + prev_id);
    }else{
        return false;
    }
}

function simpleTesting(config){
    var result = {};
    var has_slip = (config != null && typeof config.has_slip != "undefined") ? config.has_slip : true;
    var red_border = (config != null && typeof config.red_border != "undefined") ? config.red_border : true;
    var number_require = (config != null && typeof config.number_require != "undefined") ? config.number_require : 1;
    var testing_result = true;
    var testing_area = '.simple-testing';
    var testing_row = testing_area + ' .st-row';
    var require_text = ' .st-require-text';
    var require_number = ' .st-require-number';
    $(testing_row).find(require_text).removeClass('st-attention');
    $(testing_row).find(require_number).removeClass('st-attention');

    var has_require = false;
    var require_cols = [require_text, require_number];
    $.each(require_cols, function(col_key, col_value){
        $.each($(col_value), function(key, target){
            if($(target).val().length > 0) {
                has_require = true;
                return false;
            }
        });
    });

    if(!has_require){
        testing_result = false;
        if(has_slip){
            result.error_row = $(testing_row + ':first-child');
        }
        if(red_border){
            $(testing_row + ':first-child').find(require_text).addClass('st-attention');
            $(testing_row + ':first-child').find(require_number).addClass('st-attention');
        }
    }

    $.each($(testing_row), function(key, element){
        var check_text_count = 0;
        var check_number_count = 0;
        var row_require_text = $(element).find(require_text).length;
        var row_require_number = $(element).find(require_number).length;

        $.each($(element).find(require_text), function(key, target){
            if($(target).val().length > 0) {
                check_text_count++;
            }
        });
        $.each($(element).find(require_number), function(key, target){
            var value = $(target).val();
            if($.isNumeric(value) && value.length > 0 && value >= number_require) {
                check_number_count++;
            }
        });
        if(check_text_count && check_number_count && check_text_count == row_require_text && check_number_count == row_require_number){
            return true;
        }else if(!check_text_count && !check_number_count){
            return true;
        }else{
            testing_result = false;
            if(has_slip){
                result.error_row = element;
            }
            if(red_border){
                $(element).find(require_text).addClass('st-attention');
                $(element).find(require_number).addClass('st-attention');
            }
            return false;
        }
    });
    result.success = testing_result;
    return result;
}

$('.one-for-all .switch').on('click', function(){
    var followers = $(this).closest('.one-for-all').find('.follower:not(:disabled)');
    var checked = $(this).prop('checked');
    if(checked){
        followers.prop('checked', true);
    }else{
        followers.removeAttr('checked');
    }
});

// header search bar script
$('#form_search').submit(function(){
    query = $.trim($(this).find('input#search_bar').val());
    site = $(this).find('.selection-search > option:selected').val();
    console.log(query);
    console.log(site);
    if(query){
        switch(site) {
            case 'Parts':
                window.location = "https://"+ window.location.hostname + "/parts" + "?q=" + query;
                break;
            case 'Reviews':
                window.location = "https://"+ window.location.hostname + "/review/list" + "?q=" + query;
                break;
            case 'Google':
                window.location = "https://"+ window.location.hostname + "/search" + "?q=" + query;
                break;
            case 'Moto Market':
                window.location = "http://www.webike.tw/motomarket/search/fulltext" + "?q=" + query;
                break;
            case 'Moto News':
                window.location = "https://www.webike.tw/bikenews/" + "?s=" + query;
                break;
            case 'Moto Channel':
                window.location = "https://www.youtube.com/channel/UCt-BGPMSWp5jW8Ki3tJ_4JA/search" + "?query=" + query;
                break;
            default:
        }
    }else{
        alert('未輸入');
    }
    return false;
});

/**
 * Created by rcvn_2109 on 11/30/2016.
 */
$(function () {
    $('.ul-menu-ct-box-page-group-dropdown li a').click(function () {
        var self = $(this);
        self.parent().find('ul.ul-sub-menu li').slideToggle(100);

        if(self.hasClass('clicked')) {
            self.find('i.fa-plus').removeClass('fa-plus').addClass('fa-minus');
        } else {
            self.find('i.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        }
        self.toggleClass('clicked');
    });
});


function submitForm(element,url, confirm) {
    var form = $(element).closest('form');
    $(form).attr('action',url);
    // confirm = (typeof confirm === "undefined") ? true : confirm;

    // if(confirm){
    //     swal({
    //         title: '確定要執行嗎?',
    //         text: "",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: "確認"
    //     }).then(function () {
            $(form).submit();
    //     });
    // }
    // return false;
}

function addToCart(items, button){
    var sku = [];
    var qty = [];
    var cache = [];

    if(items.length){
        $.each(items, function(){
            if ($(this).prop("checked") || $(this).prop("selected")) {
                var start = $(this).prop('name').indexOf('[');
                var end = $(this).prop('name').indexOf(']');
                var url_rewrite = $(this).prop('name').substring(start + 1, end);
                sku.push(url_rewrite);
                qty.push($(this).val());
                cache.push(cache = $(this).next('input[name^="cache"]').val());
            }
        });
    }

    if(sku.length){
        button.addClass('disabled');
        $.ajax({
            url: '//' + document.domain + path + '/cart/updateItem',
            data: {sku:sku, qty:qty,cache:cache},
            type:"POST",
            dataType:'text',

            success: function(msg){
                // return false;
                window.location.href = '//' + document.domain + path + '/cart';
            },

            error:function(xhr, ajaxOptions, thrownError){
                button.removeClass('disabled');
                swal(
                    'Oops...',
                    '發生例外錯誤!',
                    'error'
                )
            }
        });
    }
}

function confirmActiveUseName(comfirmItem, parentItem, compareText, addClassName) {
    addClassName = addClassName || "active";
    $(comfirmItem).closest(parentItem).removeClass(addClassName);
    $(comfirmItem).each(function(){
        if(compareText.indexOf($(this).prop('name')) >= 0){
            $(this).closest(parentItem).addClass(addClassName);
            return false;
        }
    });
}

function controlQuantity(){
    // using quantity-count to set rule
    $('.quantity-add,.quantity-remove').click(function(){
        var parent = $(this).closest('.quantity-wrapper');
        var input = parent.find('.quantity-count');
        if(!input.prop('disabled')){
            var count = parseInt(input.val());
            if($(this).hasClass('quantity-add')){
                count++;
            }else{
                count--;
            }
            input.val(count).change();
        }
    });
}

function goBack() {
    window.history.back();
}

function autoGoShoppingHome(){
    setTimeout(function(){ location.href = '//' + document.domain + path + '/shopping'}, 10000);
}

var togglePartnerCount = 0;
function togglePartner(_this, start, count){
    start = typeof start !== 'undefined' ? start : 0;
    count = typeof count !== 'undefined' ? count : 10;
    if(togglePartnerCount === 0){
        togglePartnerCount = start;
    }
    var self = $(_this);
    self.closest('ul').find(' > li:nth-child(n+' + togglePartnerCount + ')').slice(0, count).not('.toggle-partner-switch').removeClass('hide');
    togglePartnerCount += count;
}

function getDispCount(){
    $.ajax({
        url: '//' + document.domain + path + '/customer/disp/count',
        data: {},
        type:"POST",
        dataType:'json',

        success: function(result){
            if(result.success){
                // console.log(result.datas);
                $('.tips').text('').closest('li').removeClass('active');
                $.each(result.datas, function(key, value){
                    var tip = $('.tips.' + key);
                    if(value){
                        tip.text('(' + value + ')');
                        tip.closest('li').addClass('active');
                    }
                });
            }
        },

        error:function(xhr, ajaxOptions, thrownError){
            console.log('!');
        }
    });
}

function readDisp(queue){
    $.ajax({
        url: '//' + document.domain + path + '/customer/disp/read',
        data: {queue: queue},
        type:"POST",
        dataType:'json',

        success: function(result){
            // console.log(result);
            if(result.success){
                getDispCount();
            }
        },

        error:function(xhr, ajaxOptions, thrownError){
            console.log('!');
        }
    });
}

function doReadDisp(aTag){
    var folder = $(aTag).closest('.history-box-outside-content');
    $(aTag).fadeOut();
    var queue = $(aTag).prop('name');
    // console.log(queue);
    readDisp(queue);
    $(aTag).remove();
    if(folder.find('a.disp').length <= 0){
        folder.fadeOut();
    }
}

$(function () {
    // Action with button on Outside Content Popup
    $('.history-box-outside-content > div.title-box-page-group').click(function () {
        $(this).closest('div.history-box-outside-content').find('div.ct-box-page-group > ul.ul-menu-ct-box-page-group').slideToggle();
    });
    $('a.history-btn-close').click(function () {
        $(this).closest('div.history-box-outside-content').fadeOut();
        var queue = $('.history-box-outside-content a.disp').map(function() {
            return $(this).prop('name');
        }).get().join('|');
        // console.log(queue);
        readDisp(queue);
    });

    $('a.disp').click(function () {
        doReadDisp($(this));
    });
});

function submitKeyword(_this, target_url){
    target_url = typeof target_url !== 'undefined' ? target_url : baseUrl;

    if(target_url){
        target_url = decodeURIComponent(target_url);

        search_value = $(_this).closest('div').find('input[type=text]').val();

        if(search_value){
            // console.log(addParameter(target_url,'q',search_value));
            window.location.href = addParameter(target_url,'q',search_value);
        }

    }
}

$(window).one('scroll', function() {
    getRecentView();
});

function getRecentView() {
    if($('#recent_view').length){
        $.ajax({
            url: '/customer/recentView',
            type:"POST",
            success: function(data){
                data = JSON.parse(data);
                if(data.success){
                    $('#recent_view').replaceWith(data.html);

                    $('.owl-carousel-history').addClass('owl-carousel').owlCarousel({
                        loop:false,
                        nav:true,
                        margin:5,
                        slideBy : 6,
                        URLhashListener:true,
                        startPosition: 'URLHash',
                        responsive:{
                            0:{
                                items:2
                            },
                            600:{
                                items:3
                            },
                            1000:{
                                items:6
                            }
                        }
                    }).find("img").unveil().trigger("unveil");

                    $('.owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
                    $('.owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
                    $('.owl-carousel2').find('.owl-prev').addClass('btn-owl-prev');
                    $('.owl-carousel2').find('.owl-next').addClass('btn-owl-next');

                }else{

                }
                
            },

            error:function(xhr, ajaxOptions, thrownError){
                console.log('!');
                // swal(
                //     'Oops...',
                //     '發生例外錯誤!',
                //     'error'
                // )
            }
        });
    }    
}




function runOverflowMenu() {
    $('.overflow-menu-ui').each(function(){
        var overflowMenu = {area:null, menu:null, sub_menu:null, collect:null, children:null, ctr:0, varWidth:0, elemWidth:0, fitCount:0, collectedSet:null};
        overflowMenu.area = '.overflow-menu-ui';
        overflowMenu.menu = '.overflow-menu';
        overflowMenu.sub_menu = '.overflow-sub-menu';
        overflowMenu.collect = '.overflow-menu-collect';
        overflowMenu.elemWidth = $(this).find(overflowMenu.menu).width();
        overflowMenu.children = $(this).find(overflowMenu.menu).children();

        overflowMenu.ctr = overflowMenu.children.length;
        overflowMenu.children.removeAttr('style');

        if($(this).width() > 767) {
            overflowMenu.children.each(function () {
                overflowMenu.varWidth += $(this).outerWidth();
                if (overflowMenu.varWidth < overflowMenu.elemWidth) {
                    overflowMenu.fitCount++;
                } else {
                    return false;
                }
            });

            // overflowMenu.fitCount = Math.floor((overflowMenu.elemWidth / overflowMenu.varWidth) * overflowMenu.ctr) - 2;
            overflowMenu.collectedSet = $(this).find(overflowMenu.menu).children(":gt(" + (overflowMenu.fitCount - 2) + ")");
            // console.log(overflowMenu.collectedSet);
            $(this).find(overflowMenu.sub_menu).empty().append(overflowMenu.collectedSet.clone().removeClass('hidden-lg hidden-md hidden-sm hidden-xs'));

            if (overflowMenu.collectedSet.length <= 1 || $(window).width() >= 1200 || $(window).width() < 767) {
                $(this).find(overflowMenu.collect).hide();
                $(this).find(overflowMenu.menu).css({'max-width': 'none'});
            } else {
                overflowMenu.collectedSet.css({"display": "none", "width": "0"});
                var now_width = 0;
                overflowMenu.children.each(function () {
                    now_width += $(this).width();
                });
                $(this).find(overflowMenu.collect).css({left: now_width + 20}).show();
            }
        }


    });
}

runOverflowMenu();
$(window).resize(function(){
    runOverflowMenu();
});



function headerCount() {
    if($('.icon-count').length){
        $.ajax({
            url: '//' + document.domain + path + '/customer/header-count',
            data: {},
            type:"POST",
            dataType:'json',

            success: function(result){
                if(result.success){
                    $.each(result.datas, function(key, value){
                        var tip = $('.icon-count.' + key);
                        if(value){
                            tip.text('(' + value + ')');
                        }
                    });
                }
            },

            error:function(xhr, ajaxOptions, thrownError){
                console.log('!');
            }
        });
    }
}
headerCount();

function toggleCartInfo(_this){
    var target = $(_this);
    target.closest('.toggle-box').find('.toggle-content').toggleClass('hidden-xs');
}