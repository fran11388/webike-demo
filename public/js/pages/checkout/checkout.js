/**
 * Created by rcvn_2109 on 11/16/2016.
 */
$(function(){

    autoFixWidthOfItem();
    $(window).resize(function() {
        autoFixWidthOfItem();
    });

    $('.checkout-page-state a').click(function() {
        $(this).addClass('active-state');
        $(this).siblings().removeClass('active-state');
    });

});

function autoFixWidthOfItem(){
    var $widthOfStatePanel = $('.checkout-page-state').width();
    var $itemCount = $('.checkout-page-state a').size();

    var $percentWidthOfEachItem = (( ($widthOfStatePanel/$itemCount) - 35)*100/$widthOfStatePanel) + '%';
    var $widthOfLastItem = (100 - (($widthOfStatePanel/$itemCount)*100/$widthOfStatePanel)*3) + '%';

    $('.checkout-page-state a').css({'width': $percentWidthOfEachItem});
    $('.checkout-page-state a:last-of-type').css({'width': $widthOfLastItem});
}
