var mitumori_row = '.mitumori-form-block-item';
var mitumori_form = '.mitumori-form';
var mitumori_preview_content = '#mitumori-preview-content';
var mitumori_list = mitumori_form + ' .form-block';
var mitumori_number = '.mitumori-number h1';
function addRow(){
    if($(mitumori_number).length >= 15){
        swal(
            '錯誤',
            '最多新增至15項。',
            'error'
        );
        return false;
    }
    var list = $(mitumori_list);
    var last_row = $(mitumori_row).last().clone();
    last_row.find('input').val('');
    last_row.find('textarea').val('');
    for (var i = 1;i<=3;i++){
        var new_row = last_row.clone();
        var now_number = parseInt(new_row.find(mitumori_number).text()) + i;
        if(now_number > 15){
            return false;
        }
        new_row.find(mitumori_number).text(now_number);
        list.append(new_row);
    }
}
