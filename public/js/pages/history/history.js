/**
 * Created by rcvn_2109 on 11/21/2016.
 */

// page state change
$(function(){
    autoFixWidthOfItem();
    if ($(window).width() < 840) {
        autoAdjustStyle();
        $('.checkout-page-state a').css({'width': '100%'});
    }
    $(window).resize(function() {
        if ($(window).width() > 840) {
            autoFixWidthOfItem();
        } else {
            autoAdjustStyle();
            $('.checkout-page-state a').css({'width': '100%'});
        }
        console.log($(window).width());
    });

    // we do not need this function
    //$('.checkout-page-state a').click(function() {
    //    $(this).addClass('active-state');
    //    $(this).siblings().removeClass('active-state');
    //});

    function autoFixWidthOfItem() {
        var $widthOfPanel = $('.checkout-page-state').width();
        var $itemCount = $('.checkout-page-state a').size();

        var $percentWidthOfEachItem = (( ($widthOfPanel/$itemCount) - 35)*100/$widthOfPanel) + '%';
        var $widthOfLastItem = (100 - ( ($widthOfPanel/$itemCount) * 100 / $widthOfPanel ) * ($itemCount - 1)) + '%';

        $('.checkout-page-state a').css({'width': $percentWidthOfEachItem});
        $('.checkout-page-state a:last-of-type').css({'width': $widthOfLastItem});
    }

    function autoAdjustStyle() {
        var timeout;
        $(window).on("load resize", function() {
            console.log("event debounced");
            if (timeout) {
                window.clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
                console.log("resizing");
                $(".checkout-page-state > a").removeClass("first-type last-type").each(function() {
                    if ($(this).next().length && $(this).offset().top < $(this).next().offset().top) {
                        $(this).addClass("last-type");
                    }
                    if ($(this).prev().length && $(this).offset().top > $(this).prev().offset().top) {
                        $(this).addClass("first-type");
                    }
                });
            }, 100);
        });
    }

});
