var table_reset = 0;
function reSizeTableResponsive(){
    table_reset = 0;
    tableResponsive();
}
function tableResponsive(){
    if($(window).width() < 992 && table_reset == 0){
        table_reset = 1;
        $('[class*=-table-title] ul').each(function(){
            $(this).find('li').each(function(key, element){
                var col_title = $(element).text();

                var target = $(element).closest('ul[class*=-info-table]').find('li[class*=-table-content]').find('ul li:nth-child(' + (key + 1) +')').find('span');
                // console.log(col_title);
                // console.log(key + 1);
    //                    var html = target.html();
    //                    target.html('<div class="bg-info box-padding-2">' + html + '</div>');
                if(col_title){
                    target.each(function(){
                        $(this).prepend('<div class="visible-sm visible-xs font-bold">' + col_title + '：' + '</div>');
                    });
                }
            });
        });
        $('.table-main-title ul').each(function(){
            $(this).find('li').each(function(key, element){
                var col_title = $(element).text();

                var target = $(element).closest('ul.table-main-info').find('li.table-main-content').find('ul li:nth-child(' + (key + 1) +')').find('span');
                //                    var html = target.html();
                //                    target.html('<div class="bg-info box-padding-2">' + html + '</div>');
                if(col_title){
                    target.each(function(){
                        $(this).prepend(col_title + '：');
                    });
                }
            });
        });
    }
}
$(document).ready(function(){
    tableResponsive();
});
$(window).resize(function(){
    tableResponsive();
});