/**
 * Created by rcvn_2109 on 11/30/2016.
 */

$(function () {
    $('.ul-menu-ct-box-page-group-dropdown li a').click(function () {
        var self = $(this);
        self.parent().find('ul.ul-sub-menu li').slideToggle(100);

        if(self.hasClass('clicked')) {
            self.find('i.fa-plus').removeClass('fa-plus').addClass('fa-minus');
        } else {
            self.find('i.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        }
        self.toggleClass('clicked');
    });
});
