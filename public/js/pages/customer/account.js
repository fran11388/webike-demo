var path = '';
var string = window.location.href;
if(string.includes("dev/")){
    path = '/dev/index.php';
}

$(document).on("click", ".btn-remove-vehicle", function() {
    $(this).closest('ul').remove();
    if($('.btn-remove-vehicle').length == 0){
        $('#bike-first-row').show();
    }
});

mybike_ul = $('#bike-first-row').clone();

$(".btn-add-new-vehicle").click(function(){
//            $('#bike-first-row').hide();

    if(checkMybikeFillFinish()){
        return false;
    }

    new_ul = mybike_ul.clone().removeAttr('id');
    // new_ul.find('.btn-add-new-vehicle').replaceWith('<input class=" btn btn-danger border-radius-2 btn-remove-vehicle" type="button" value="移除">')
    new_ul.find('.select2').select2();
    new_ul.find('.select2-container').css('width','100%');
    $('#bike-last-row').before( new_ul );

});

var model_trigger = true;

function select_displacement(select_element , displacement , manufacturer){

    if (typeof manufacturer !== "undefined") {
        var value = manufacturer;
    }else{
        var value = select_element.find('option:selected').val();
    }

    ul = select_element.closest('ul');

    var target = ul.find(".select-motor-displacement");
    var next_target = ul.find(".select-motor-model");
    //manufacturer change , so clear displacement and model
    target.prop("disabled", true).find("option:not(:first)").remove().end();
    next_target.prop("disabled", true).find("option:not(:first)").remove().end();

    if (value){
        var url = path + "/api/motor/displacements?manufacturer=" + value;
        $.get( url , function( data ) {
            for (var i = 0 ; i < data.length ; i++){
                target.append($("<option></option>").attr("value",data[i]).text(data[i]))
            }
            //render finish , enable
            target.prop("disabled", false);

        }).done(function() {

            //default displacement , set the value
            if (typeof displacement !== "undefined") {
//                        target.select2('val',[displacement]);
                target.val(displacement).trigger('change');

            }
        });
    }
}

function select_model(select_element , model ,displacement){
    if (typeof displacement !== "undefined") {
        var value = displacement;
    }else{
        var value = select_element.find('option:selected').val();
    }

    var ul = select_element.closest('ul');
    var target = ul.find(".select-motor-model");
    //displacement change , so clear model
    target.prop("disabled", true).find("option:not(:first)").remove().end();
    // console.log(model);
    if (value){

        var url = path + "/api/motor/model?manufacturer=" +
            ul.find(".select-motor-manufacturer").find('option:selected').val() +
            "&displacement=" +
            value;
        $.get( url , function( data ) {
            for (var i = 0 ; i < data.length ; i++){
                target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
            }
            target.prop("disabled", false);


        }).done(function() {
            if (typeof model !== "undefined") {
                console.log(model);

                target.unbind( "change");
                target.val(model).trigger('change');
                target.bind( "change");


//                        target.select2('val',[model]);
//                        target.closest('ul').addClass('ready-detect');
            }

        });

    }
}

if($('#field_mybike > ul:not(.ready-detect) ').length){
    $('#bike-first-row').hide();
}

$(document).on("change", ".select-motor-manufacturer", function() {
    select_displacement($(this));
});

$(document).on("change", ".select-motor-displacement", function() {
    console.log('trigger change');
    select_model($(this));
});

function checkMybikeFillFinish() {
    disable = false;
    $('select[name^="my-bike"]:visible').each(function(key, element){
        if(!$(element).val()){
            swal(
                '無法新增',
                '請先將填完所有欄位',
                'error'
            );
            disable = true;
            return false;

        }
    });
    return disable;
}

$('#field_mybike').closest('form').submit(function(){
    if(checkMybikeFillFinish()){
        return false;
    }
});