var last_preview = null;
$('.product-item:not(.product-preview-box)').mouseover(function(){
    if($(window).width() > 767 && $(this).find('.product-img-gallery img').length > 1) {
        if (last_preview !== $(this).attr('data-sku')) {
            last_preview = $(this).attr('data-sku');
            getPreviewBox(this);
        } else {
            $('.product-preview-box').show();
        }
    }
});

$('.product-preview-box').mouseleave(function(){
    $('.product-preview-box').hide();
});

$(document).on('mouseover', ".product-preview-box .preview-left img", function(){
    $(this).closest('.product-preview-box').find('.preview-right img.preview-img').prop('src', $(this).attr('data-src'));
});

function getPreviewBox(_this){
    var product = $(_this).clone();
    var preview = $('.product-preview-box');
    var css_data = {
        "min-height": $(_this).height() + 5,
        width: $(_this).innerWidth() * 1.6,
        top: $(_this).position().top
    };
    preview.css(css_data);
    var product_image_area = product.find('.product-img');
    var product_image_gallery_area = product.find('.product-img-gallery');
    var product_description_area = product.find('.product-info-search-list').find('.product-description');
    var product_info = product.find('.product-info-search-list');
    $(product_info).find('.product-description').remove();

    var product_state_area = product.find('.product-state-search-list');
    var preview_right = preview.find('.preview-right');
    var preview_description = preview.find('.preview-description');
    $(product_description_area).find('.dotted-text1').removeClass('dotted-text1');
    $(product_description_area).find('.dotted-text').removeClass('dotted-text');
    var preview_images_area = preview.find('.preview-left .clearfix');
    $(preview_images_area).html('');
    $(product_image_gallery_area).find('.thumbnail').each(function(key, thumbnail){
        $(preview_images_area).append('<div class="img-box"><a class="thumbnail search-list-img zoom-image" href="javascript:void(0)" target="_blank">' + $(thumbnail).html() + '</a></div>');
    });
    if($(product_image_gallery_area).find('img').length > 10){
        // $(preview_images_area).find('.img-box').addClass('thumbnail-col-3');
        $(preview_images_area).find('.img-box').addClass('thumbnail-col-2');
    }else{
        $(preview_images_area).find('.img-box').addClass('thumbnail-col-2');
    }
    preview_right.html('');
    preview_right.append('<div class="product-img">' + $(product_image_area).html() + '</div>');
    preview_right.append($(product_info).html());
    preview_right.css('width', $(_this).innerWidth() - 5 - 1);
    preview.find('.preview-left').css('width', ($(_this).innerWidth() * 0.6) - 5 - 1);

    preview_description.find('.product-info-search-list .product-description').html('');
    preview_description.find('.product-info-search-list .product-description').append($(product_description_area).html());
    preview_description.find('.product-state-search-list').html('');
    preview_description.find('.product-state-search-list').append($(product_state_area).html());
    preview.show();
    $(preview_images_area).find('img').unveil();
    preview.find('.preview-left').css('height', $(preview_right).height());
    preview.css('left', $(_this).position().left - preview.width() + $(_this).width());
}
