$(function(){
    // Toggle left sidebar
    $('.slide-down-btn').click(function() {
        var self = $(this);
        self.closest('li').find('ul>li').slideToggle(100);
        self.closest('li').find('ul').slideToggle(100);
        if (self.hasClass('clicked')) {
            var icon = self.find('i')
            if (icon.hasClass('fa-chevron-down')) {
                icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else if (icon.hasClass('fa-plus')) {
                icon.removeClass('fa-plus').addClass('fa-minus');
            }
        } else {
            var icon = self.find('i')
            if (icon.hasClass('fa-chevron-up')) {
                icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            } else if (icon.hasClass('fa-minus')) {
                icon.removeClass('fa-minus').addClass('fa-plus');
            }
        }
        self.toggleClass('clicked');
    });

    // Page state button
    $('ul.page-change > li.prev-page > a').click(function() {
        console.log('prev');
        $('li.prev-page').addClass('active');
        $('li.next-page').removeClass('active');
    });
    $('ul.page-change > li.next-page > a').click(function() {
        console.log('next');
        $('li.next-page').addClass('active');
        $('li.prev-page').removeClass('active');
    });

    // Change state of list product style
    $('i.menu-icon.list').click(function() {
        $(this).addClass('menu-icon-active');
        $('i.menu-icon.large').removeClass('menu-icon-active');
        $('.product-item').addClass('product-item-style-2');
    });
    $('i.menu-icon.large').click(function() {
        $(this).addClass('menu-icon-active');
        $('i.menu-icon.list').removeClass('menu-icon-active');
        $('.product-item').removeClass('product-item-style-2');
    });

    // Tick color box
    $('.color-box > div > div').on('click', function() {
        var self = $(this);

        if(self.hasClass('checked')) {
            self.find('.fa-check').remove();
        } else {
            self.append('<i class="fa fa-check" style="color: #fff" aria-hidden="true"></i>');
        }
        self.toggleClass('checked');
    });

});
