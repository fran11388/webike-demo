
slider_ajax = $('.owl-carousel-ajax').addClass('owl-carousel'); // Add owl slider class (3*)
slider_ajax.each(function(key,_slider) {

    qty = $(_slider).data('owl-ajax').limit;
    total = $(_slider).data('owl-ajax').total;
    if(total > qty){
        console.log('create loading');

        if(!qty){
            qty = 1;
        }
        for (i = 0; i < qty; i++) {
            $(_slider).append('<li class="owl-item-loading" data-order="'+i+'"><img src="//img-webike-tw-370429.c.cdn77.org/shopping/image/loading.gif"></li>');
        }
    }else{
        console.log('not loading');
    }


    var expects = $(_slider).find('li').map(function() {
        return $(this).data("sku");
    }).get();
    
    
    
    
    $(_slider).owlCarousel({
        loop:false,
        nav:true,
        margin:5,
        lazyContent:true,
        slideBy : qty,
        URLhashListener:true,
        startPosition: 'URLHash',
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:qty
            }
        }

    }).one('translate.owl.carousel', function(event) {
        _this = $(this);
        
        if(ajax_data = _this.data('owl-ajax')){
            if( ajax_data.total > ajax_data.limit ){
                if(!_this.data('owl-ajax-load')){
                    _this.data('owl-ajax-load','true');
                    $.ajax({
                        url: '//' + document.domain + path + '/carousel',
                        type:"GET",
                        data: {'ajax_data': ajax_data ,expects : expects},
                        success:function(result){
                            result = JSON.parse(result);
                            if(result.success){
                                $.each(result.data, function(index, value) {
                                    _this.trigger('add.owl.carousel', [jQuery(value)]);
                                });
                                for (i = 0; i < ajax_data.limit; i++) {
                                    _this.trigger('remove.owl.carousel',parseInt(ajax_data.limit));
                                }
                                _this.find("img").unveil().trigger("unveil");
                                _this.trigger('refresh.owl.carousel');
                            }

                        },error:function(){
                            console.log("ajax fail");
                        },
                    });

                }
            }
        }

    }).find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>')
    $(_slider).find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');

    // for (i = 0; i < qty; i++) {
    //     $(_slider).trigger('add.owl.carousel', [jQuery('<div class="owl-item"><li><img src="//img-webike-tw-370429.c.cdn77.org/shopping/image/loading.gif"></li></div>')]);
    // }


});
