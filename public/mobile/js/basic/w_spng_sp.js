// JavaScript Document
jQuery(function($){

	// �y�[�WTOP�֖߂�
    var topBtn = $("#pagetop");
    topBtn.hide();
    $(window).scroll(function(){
		if($("#pagetop").is(".disnon")){
		}else{
			if ($(window).scrollTop() > 100) {
				topBtn.fadeIn();
			} else {
				topBtn.fadeOut();
			}

		}
    });
	
    //�X�N���[�����ăg�b�v
    topBtn.click(function () {
        $("body,html").animate({
            scrollTop: 0
        }, 500);
        return false;
    });

	// �g�O��
	$(".toggle:not(.open)").next().hide();
	$(".toggle").on('click', function(){
		if($(this).hasClass("open")){
			$(this).removeClass("open");
			$(this).next().hide();
		} else {
			$(this).addClass("open");
			$(this).next().show();
		}
	});

	// ���̏ꏊ�Ŏ󂯎��
	$(".register_form .new_address").hide();
	$(".register_form input[name='receiver']").on('click', function(){
		if($(".register_form input[id='other']").is(':checked')){
			$(".register_form .new_address").show();
		} else {
			$(".register_form .new_address").hide();
		}
	});

	// �Z���N�g�t�H�[���̕����F
	function selectColor(i){
		if($(i).children("option:selected").hasClass("default")){
			$(i).css("color","#aaa");
		}else{
			$(i).css("color","#333");
		}
	}
	$("select").not("#search select").each(function(){
		selectColor(this);
	});
	$("select").not("#search select").on('change', function(){
		selectColor(this);
	});

	// �^�u
	$(".tab .btn > li").on('click', function(){
		var num = $(this).index();
		// advanced_search
		if($(this).parent().parent().hasClass("advanced_search")){
			if($(this).hasClass("selected")){
				$(this).parent().children().removeClass("selected");
				$(this).parent().next(".box").children().hide();
			} else {
				$(this).parent().children().removeClass("selected");
				$(this).addClass("selected");
				$(this).parent().next(".box").children().hide();
				$(this).parent().next(".box").children().eq(num).show();
			}
		} else if (!$(this).hasClass('no_data')) {
			$(this).parent().children().removeClass("selected");
			$(this).addClass("selected");
			$(this).parent().next(".box").children().hide();
			$(this).parent().next(".box").children().eq(num).show();
		}
	});
	$(".tab .box > li").not(".selected").hide();


	// �g�O���^�u
	$(".tbtgl").on('click', function(){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected");
			$(this).next().hide();
		} else {
			$(this).addClass("selected");
			$(this).next().show();
		}
	});

	// �^�u2
	$(".choose_menu > *").on('click', function(){
		var num = $(this).index();

		$(this).parent().children().removeClass("selected");
		$(this).addClass("selected");
		$(this).parent().nextAll(".choose_contents").children().hide();
		$(this).parent().nextAll(".choose_contents").children().eq(num).show();
	});
	if($(".choose_menu > *").hasClass("selected")){
		var num = $(".choose_menu > li.selected").index();
		$(".choose_contents").children().hide();
		$(".choose_contents").children().eq(num).show();
	}else{
		$(".choose_contents").children().hide();
	}
	

	// �w�b�_�[���j���[�J��
	$(".menu_btn, header .overlay, .close_btn").on('click', function(){
		if($(".menu_items").css("display") != "block"){
			// �J��
			$("body, html").css("overflow","hidden");
			$("#w_spng_sp").css("position","relative");
			var winHeight = $(window).height();
			$(".menu_items").height(winHeight).scrollTop(0);
			$(".menu_items .inner").css("min-height",winHeight);
			$(".menu_items").removeClass("hide").addClass("show");
			$("header .overlay, .close_btn").show();
		}else{
			// ����			
			$("header .overlay, .close_btn").hide();
			$(".menu_items dl dd ul").hide();
			$(".menu_items").removeClass("show").addClass("hide");
			setTimeout(function(){$(".menu_items").removeClass("hide");},500);
			$(".menu_items a").removeClass("open");
			$("#w_spng_sp").css("position","absolute");
			$("body, html").css("overflow","");
		}
	});

	// �����T�W�F�X�g
	$("#search #textform").on("click",function(){
		$("html,body").animate({scrollTop:75});
	});
	$("#search #textform").on('blur keydown keyup keypress change text',function(){
		var textWrite = $("#textform").val();
		if(textWrite){
			$("#search_suggest").css("display","block");
		}else{
			$("#search_suggest").css("display","none");
		}
	});
	// �����{�b�N�X�ȊO���N���b�N�����ꍇ�T�W�F�X�g�����
	$(document).on("#w_spng_sp","click",function(event) {
		if (!$.contains($("#search_suggest")[0], event.target)) {
			$("#search_suggest").hide();
		}
	});

	// �Ԏ킩�珤�i��T���J��
	$(".modal_btn,.modal .close,.modal .overlay").on('click', function(){
		
		if($(this).hasClass("modal_btn")){
			var type = $(this)[0].className.split(" ")[1];
		}
		
		if($(".modal_btn").is(".active")){
			$(".modal_btn").removeClass("active");
			$(".scroll_ui > div").css("overflow-x","");
			$("#w_spng_sp").css("position","relative");
			$(".head-motor-search").hide();
			$(".head-motor-search .overlay").height("");
			$("html").css("-webkit-tap-highlight-color","");
		}else{
			$(".modal_btn").addClass("active");
			$(".scroll_ui > div").css("overflow-x","hidden");
			$("#w_spng_sp").css("position","absolute");
			var htmlHeight = $("#w_spng_sp").height();
			$(".head-motor-search" + "." + type).show();
			var winPos = $(window).scrollTop();
			$(".head-motor-search" + "." + type + " .inner").css("top",winPos+10);
			$(".head-motor-search .overlay").height(htmlHeight);
			$("html").css("-webkit-tap-highlight-color","rgba(0, 0, 0, 0)");
		}
	});

	// �v�f�̍����𑵂���
	function uniformHeight(i){
		var hList = new Array();
		$(i).children().each(function(j){
			hList[j]=$(this).height();
		});
		var maxH=Math.max.apply(null,hList);
		$(i).children().css({"height":maxH+"px"});
	}
	$(window).on('load resize', function(){
		$(".height").children().css("height","auto");
		$(".height").each(function(){
			uniformHeight(this);
		});
	});
	$(".tab .btn > li").on('click', function(){
		$(".height").children().css("height","auto");
		$(".height").each(function(){
			uniformHeight(this);
		});
	});

	// ����������
	$(window).on('load', function(){
		$(".text_ellipsis").each(function(){
			if($(this).height() > 120){
				$(this).addClass('continue');
				$(this).after('<a href="javascript:void(0)" class="read_more">������ǂ�</a>');
			}
		});
		$(".read_more").on('click', function(){
			if($(this).hasClass("open")){
				$(this).removeClass("open");
				$(this).text("������ǂ�");		
				$(this).prev().css("max-height","110px");
				$(this).prev().addClass('continue');					
		
			} else {
				$(this).addClass("open");
				$(this).text("����");
				$(this).prev().css("max-height","inherit");
				$(this).prev().removeClass('continue');
			}
		});	
	});		

	// �����������V���v��ver.
	function textEllipsisS(i,j){
		var textLength = i.length;
		if(j < textLength) {
			var textBefore = i.substr(0,j);
			var textAfter = i.substr(j);
			var insertText = "<span>�c</span><a href='javascript:void(0)' class='read_more_s'>������ǂ�</a>";
			var changeHtml = textBefore + "<span style='display:none;'>" + textAfter + "</span>" + insertText;
			return changeHtml;
		}
	}
	$(window).on('load', function(){
		$(".text_ellipsis_s").each(function(){
			$(this).html(textEllipsisS($(this).text(),$(this).attr("data-num")));
		});
		$(".read_more_s").on('click', function(){
			if($(this).hasClass("open")){
				$(this).removeClass("open");
				$(this).text("������ǂ�");
				$(this).prev().prev().hide();
				$(this).prev().show();
			} else {
				$(this).addClass("open");
				$(this).text("����");
				$(this).prev().prev().show();
				$(this).prev().hide();
			}
		});
	});

	// �u�����h�ꗗ�{�^��
	$(".brand_searcher .jp_sub li a,.brand_searcher .en li a").on('click', function(){
		$(".brand_searcher .jp_sub li,.brand_searcher .en li").removeClass("selected");
		$(this).parent().addClass("selected");
	});

	// �������� �u�����h�����ƌ���
	$(".advanced_search .more > a").on('click', function(){
		if($(this).hasClass("open")){
			$(this).text("����");
		} else {
			$(this).text("�����ƕ\������");
		}
	});

	// �C���v���b�V�����]��
	$("#main .impre_list .detail .rating .toggle,#main .product_imps .rating .toggle").on('click', function(){
		if($(this).hasClass("open")){
			$(this).text("����");
		} else {
			$(this).text("�]�����ڂ�������");
		}
	});

	// bxslider�⏕
	function squareFrame(i){
    	var w = $(i).width();
		$(i).css({"height":w+"px"});
	}
	var timer = false;
	$(window).on('load resize', function(){
		if (timer !== false){
			clearTimeout(timer);
		}
		timer = setTimeout(function(){
			$(".product_detail .photo a,.impre_list .detail .photo a").each(function(){
				squareFrame(this);
			});
		},10);
	});

	// ���i�ڍ�
	var ua = navigator.userAgent;
	if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPad') > 0){
		var selects = document.querySelectorAll(".product_detail select");
		for(var i = 0; i < selects.length; i++ ){
			selects[i].appendChild(document.createElement("optgroup"));
		}
	}

	// ���i��� width��height�l ����
	$(".product_info div img, .product_info div table, .product_info div iframe").attr({width:"",height:""});

	// �폜�A���[�g
	$(".button_ui .delete").on('click', function(){
		if(!confirm('�{���ɍ폜���Ă���낵���ł����H')){
			return false;
		}
	});

	// ����o�^ My�o�C�N�ǉ�
	var clickCount = 0;
	$(".mybike_add").on('click', function(){
		$(this).prev(".mybike").clone(true).insertBefore(this);
		$(this).data("click", ++clickCount);
		 var click = $(this).data("click");
		if(click == 1){
			$(this).text("3��ڂ�My�o�C�N��ǉ�����");
		}else if(click == 2){
			$(this).hide();
		}
		return false;
	});

	// ���摊����
	$(function(){
		$(window).scroll(function(){
			$(".model_price .chart_wrap").each(function(){
				var imgPos = $(this).offset().top;
				var scroll = $(window).scrollTop();
				var windowHeight = $(window).height();
				if($(this).scrollLeft() == 0){
					if (scroll > imgPos - windowHeight + windowHeight/5){
						$(this).addClass("show");
					} else {
						$(this).removeClass("show");
					}
				} else {
					$(this).removeClass("show");
				}
			});
		});
		$(".model_price .chart_wrap").on('touchmove',function(){
			if($(this).scrollLeft() != 0){
				$(this).addClass("del");
			}
		});
	});

	// �������i���ς���|�b�v�A�b�v
	setTimeout(function(){
		$('#pop').animate({'bottom' :8 }, 500);
		$('#pop .close').click(function(){
			$('#pop').animate({'bottom' :-200 }, 500);
		});
	},500);

	// �������i���ς���t�H�[���ǉ�
	var defaultShow = 10; // �����\����
	for(i=0;i<=defaultShow;i++){
		$(".genuine_form tr:eq("+i+")").show();
	}
	$(".genuine_form .add a").on('click', function(){
		$(".genuine_form tr").show();
		$(this).parent().hide();
	});

	// �������i�_�C���N�g�����؂�ւ�
	$(".genuine_option").parent(".section").hide();
	$(".genuine_direct .button_ui a").on('click', function(){
		$(".genuine_direct .button_ui a").removeClass("bc02");
		if($(this).hasClass("bc02")){
			$(this).removeClass("bc02");
			$(this).next().hide();
		} else {
			$(this).addClass("bc02");
			$(this).next().show();
		}
		if($(this).hasClass("direct")){
			$(".genuine_option").parent(".section").show();
		} else {
			$(".genuine_option").parent(".section").hide();
		}
	});

	// BIZ�V���b�s���OTOP
	$(window).on('load resize', function(){
		var bizBannerHeight = $(".campaign_scroll #output li").height();
		$(".campaign_scroll #output").css({"height":bizBannerHeight+"px"});
	});

	//-- Suggest --
    // Scroll to search area
    // $("#search #cmTableInput").on("click",function(){
    //     $("html,body").animate({scrollTop:75});
    // });

    // Event when change text
    $("#search #cmTableInput").on('blur keydown keyup keypress change text',function(){
        var textWrite = $("#cmTableInput").val();
        if(textWrite){
            $("#search_suggest").css("display","block");
        }else{
            $("#search_suggest").html("");
            $("#search_suggest").css("display","none");
        }
    });

    // Hide search suggestion if click out of search suggestion area
    // if($("#cmTableInput").size()){
		// 	$("#w_spng_sp").on("click",function(event) {
		// 			if (!$.contains($("#search_suggest")[0], event.target)) {
		// 					// Check event of textbox
		// 					if (event.target.id != null && (event.target.id == "cmTableInput" || event.target.id == "cmTableSubmit") ) {
		// 							return;
		// 					}
		// 					$("#search_suggest").html("");
		// 					$("#search_suggest").hide();
		// 			}
		// 	});
		// }

    // Event for remove text button on IE
    $("#cmTableInput").bind('input propertychange', function() {
        if (this.value == ""){
            $("#search_suggest").html("");
            $("#search_suggest").css("display","none");
        }
    });

    // Request suggestion
	if($("#cmTableInput").size()){
		jQuery("#cmTableInput").autocomplete({
				delay:100,
				source: function(request, response){
						var keyword = $("#cmTableInput").val();

						// Search request
						$.ajax({
								url: "/wbs/suggest.html",
								type: "GET",
								cache: false,
								dataType: "html",
								data: {q:keyword, ua: "sp"},
								success: function(html){
										$("#search_suggest").html(html);
										var result = jQuery("#search_suggest").find(".words ul").size();
										if (result == 0) {
												$("#search_suggest").html("");
												$("#search_suggest").css("display","none");
										}
								},
								error: function(xhr, ts, err){
								}
						});
				},open: function() {

				}
		}).keyup(function(event){
				if (event.keyCode == 13) {

				}
				if (event.which == 13 && event.which == keyDownCode) {

				}
		}).keydown(function(event) {
				keyDownCode = event.which;
		});
	}

});