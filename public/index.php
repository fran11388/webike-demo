<?php

if (isset($_SERVER['HTTP_HOST'] )) {


    if ( $_SERVER['HTTP_HOST'] ==='webike.tw' ){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: http://www.webike.tw");
        exit();
    }

    date_default_timezone_set("Asia/Taipei");
    $maintain = FALSE;//404 維修完畢請改成FALSE並儲存即可
    if ($maintain == TRUE){
        if ( ($_SERVER['REMOTE_ADDR'] != '1.34.149.90') && ($_SERVER['REMOTE_ADDR'] != '122.116.103.32') && ($_SERVER['REMOTE_ADDR'] != '49.158.20.135') && ($_SERVER['REMOTE_ADDR'] != '1.34.199.93')  && ($_SERVER['REMOTE_ADDR'] != '203.67.45.142') && ($_SERVER['REMOTE_ADDR'] != '61.220.45.142') ){
            include 'maintain.php';
            exit();
        }
    }

}


/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */ 

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/../bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

require base_path() . '/constants/define.php';
require base_path() . '/constants/domain.php';

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
