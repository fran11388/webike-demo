<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;

use Ecommerce\Support\DisplacementConverter;
use Everglory\Constants\SEO;

class SummaryComponent extends BaseComponent
{

    public $new_products;
    public $on_sales_products;
    public $current_category;
    public $current_manufacturer;
    public $tree;
    public $reviews;

    public $motor;
    public $category;
    public $manufacturer;
    public $fit_motors = [];
    public $fit_motor_products = [];
    public $my_bikes = [];
    public $ranking_avg;

    public $meta_mt;
    public $meta_ca;
    public $meta_br;
    public $has_child;
    public $top_cate;


    public function __construct()
    {
        parent::__construct();
    }


    public function setBreadcrumbs($params = NULL,$url = NULL)
    {
        $summary_path = '';
        foreach($params as $type => $param){
            if($param){
                if($type == 'mt'){
                    $this->motor = $param;
                    $this->breadcrumbs[] =  ['name'=>'車型索引','url'=>route('motor')];

                    $disp = DisplacementConverter::find($this->motor->displacement);

                    $this->breadcrumbs[] =  ['name' => $this->motor->manufacturer->name . ' ' . $disp->name ,'url' => getMfUrl(['motor_manufacturer' => $this->motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key])];
                    $this->breadcrumbs[] =  ['name' => $this->motor->name ,'url' => getMtUrl(['motor_url_rewrite' => $this->motor->url_rewrite])];

                    $this->motor = $param;

                    $this->meta_mt = $this->motor->manufacturer->name . ' ' . $this->motor->name . ($this->motor->synonym ? '('.$this->motor->synonym.')' : '');
                }elseif($type == 'ca'){
                    $this->category = $param;
                    $mptt = $this->category->mptt;
                    $url_paths = explode('-',$mptt->url_path);
                    $this->top_cate = $url_paths[0];
                    $name_paths = explode('|',$mptt->name_path);
                    $summary_path .= '/ca/';
                    foreach ( $url_paths as $key => $url_path ) {
                        if($key == 0 ){
                            $summary_path .= $url_path;
                        }else{
                            $summary_path .= '-'.$url_path;
                        }
                        $this->breadcrumbs[] =  ['name'=> $name_paths[$key] ,'url'=>route('summary',$summary_path)];
                    }
                    $this->meta_ca = $this->category->name . ( $this->category->synonym ? '('.$this->category->synonym.')' : '' );

                }elseif($type == 'br'){
                    if(!$summary_path){
                        $this->breadcrumbs[] =  ['name'=> '精選品牌' ,'url'=> route('brand')];
                    }

                    if($param != 'top'){
                        $summary_path .= '/br/';
                        $this->manufacturer = $param;
                        $this->breadcrumbs[] =  ['name'=> $this->manufacturer->name. ($this->manufacturer->synonym ? '('.$this->manufacturer->synonym.')' : '')  ,'url'=>route('summary',$this->manufacturer->url_rewrite)];
                        $this->meta_br = $this->manufacturer->name . ($this->manufacturer->synonym ? '('.$this->manufacturer->synonym.')' : '');
                    }
                }
            }
        }

        $this->seo();
    }


    public function seo($param = NULL, $text = NULL)
    {
        $mt_blocks = ['商品分類' ,'新商品' ,'推薦商品' ,'促銷商品' ,'熱門品牌' ,'商品評論' ,'正廠零件' ];
        $ca_blocks = ['商品分類' ,'新商品' ,'促銷商品' ,'新品牌' ,'熱門品牌' ,'商品評論' ];
        $br_blocks = ['品牌介紹' ,'商品分類' ,'新商品' ,'促銷商品' ,'新品牌' ,'熱門品牌' ,'商品評論' ];
        $meta_mt = $this->meta_mt;
        $meta_ca = $this->meta_ca;
        $meta_br = $this->meta_br;
        if($meta_mt){
            $this->seo_image = $this->current_motor->image_path . $this->current_motor->image;
            if($this->current_motor->meta_image){
                $this->seo_image = $this->current_motor->meta_image;
            }
            if($meta_ca and $meta_br){ //mt + ca + br
                $blocks = $this->getBlocksString(array_merge($mt_blocks,$ca_blocks,$br_blocks));
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '【' . $meta_mt . ' ' . $meta_br . ' ' . $meta_ca . '】改裝零件、騎士用品最新商品一覽';
                $this->seo_description = '【' . $meta_mt . ' ' . $meta_br . ' ' . $meta_ca . '】'. $blocks .'，全球知名品牌應有盡有-' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }else if($meta_ca){ //mt + ca
                $blocks = $this->getBlocksString(array_merge($mt_blocks,$ca_blocks));
                $this->seo_image = getCategoryImage($this->current_category);
                $this->seo_title = '【' . $meta_mt . ' ' . $meta_ca . '】改裝零件、騎士用品最新商品一覽';
                $this->seo_description = '【' . $meta_mt . ' ' . $meta_ca . '】'. $blocks .'，全球知名品牌應有盡有-' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }else if($meta_br){ //mt + br
                $blocks = $this->getBlocksString(array_merge($mt_blocks,$br_blocks));
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '【' . $meta_mt . ' ' . $meta_br . '】改裝零件、騎士用品最新商品一覽';
                $this->seo_description = '【' . $meta_mt . ' ' . $meta_br . '】'. $blocks .'，全球知名品牌應有盡有-' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }else{ // mt
                $blocks = $this->getBlocksString($mt_blocks);
                $this->seo_title = '【' . $meta_mt . '】改裝零件、騎士用品最新商品一覽';
                $this->seo_description = '【' . $meta_mt . ' 】最新對應商品一覽、' . $blocks . '，全球知名品牌應有盡有-' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }
        }else if($meta_ca){
            $this->seo_image = getCategoryImage($this->current_category);
            if($meta_br){ //ca + br
                $blocks = $this->getBlocksString(array_merge($ca_blocks, $br_blocks));
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '進口重機、機車【' . $meta_br . ' ' . $meta_ca . '】商品搜尋';
                $this->seo_description = '最新、最多【'. $meta_br . ' ' . $meta_ca .'】'. $blocks .'，相關機車零件商品盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
            }else{  //ca
                $blocks = $this->getBlocksString($ca_blocks);
                if( $this->top_cate == 3000 ){
                    $this->seo_title = '重機、機車【' . $meta_ca . '】全球知名品牌應有盡有';
                    $this->seo_description = '最新、最多【'. $meta_ca . '】' . $blocks . '、人生部品、騎士精品，盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;

                    if( $this->has_child ){
                        $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    }
                }elseif( $this->top_cate == 1000 ){
                    $this->seo_title = '進口重機、機車【' . $meta_ca . '】商品搜尋';
                    $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，相關機車零件商品盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    if( $this->has_child ){
                        $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，相關機車零件、耗材、工具盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    }
                }elseif( $this->top_cate == 4000 ){
                    $this->seo_title = '進口重機、機車【' . $meta_ca . '】商品搜尋';
                    $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，相關機車保養耗材盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    if( $this->has_child ){
                        $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，相關機車保養耗材盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    }
                }elseif( $this->top_cate == 8000 ){
                    $this->seo_title = '進口重機、機車【' . $meta_ca . '】商品搜尋';
                    $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，相關機車維修工具盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    if( $this->has_child ){
                        $this->seo_description = '最新、最多【'. $meta_ca .'】' . $blocks . '，相關機車維修工具盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                    }
                }

            }
        }else{ //br
            if($this->manufacturer){
                $blocks = $this->getBlocksString($br_blocks);
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '【' . $meta_br . '】改裝零件、騎士用品搜尋';
                $this->seo_description = '【' . $meta_br . '】' . $blocks . '，'. SEO::SHIPPING_FREE[activeShippingFree()] .'。';
            }else{
                $this->seo_title = '' . SEO::WEBIKE_SHOPPING_LOGO_TEXT . ' - 進口重機, 機車, 改裝零件, 騎士用品, 最新摩托情報!!';
                $this->seo_description = '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城-' . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '!';
            }
        }
    }

    private function getBlocksString($array)
    {
        return implode('、', array_unique($array));
    }
}