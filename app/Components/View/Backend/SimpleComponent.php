<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View\Backend;

use League\Flysystem\Exception;
use App\Components\View\SimpleComponent as BasicSimpleComponent;
use Everglory\Models\Customer;

class SimpleComponent extends BasicSimpleComponent
{
    public $system_name = 'Webike-摩托百貨後台';

    public function __construct()
    {
        $this->home_url = route('backend');
        $this->tail = '';
        $this->breadcrumbs = [['url' => $this->home_url , 'name' => $this->system_name]];
    }

}