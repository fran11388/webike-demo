<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;


use Ecommerce\Support\LengthAwarePaginator;
use Ecommerce\Repository\CategoryRepository;

class ReviewComponent extends SimpleComponent
{

    //index
    public $customReviewObject;
    public $riderReviewObject;
    public $newReviews=[];
    public $newComments=[];
    public $mostReviewProducts=[];
    public $notWriteReviewCount = 0;

    //show
    public $review;
    public $product;
    //public $reviews = [];

    // search
    public $keyword = '';
    public $current_motor;
    public $reviews = [];
    public $category = null;

    public $pager;

    public function __construct()
    {
        parent::__construct();
        $this->breadcrumbs[] = setBreadcrumbs( route('review'),'商品評論');
    }


    public function setBreadcrumbs($params = NULL,$url = NULL)
    {
        $routeName = \Route::currentRouteName();
        $functionName = '_breadcrumbs_' . str_replace("-", "_" , strtolower($routeName));

        //$this->$functionName();
        if (method_exists($this,$functionName)) {
            $this->$functionName();
        }

        $this->render();
    }

    public function output(){
        $this->initPager();
        $this->setBreadcrumbs();

        return (array) $this;

    }

    private function initPager()
    {
        if ( !$this->reviews or get_class($this->reviews) !== 'Illuminate\Pagination\LengthAwarePaginator')
            return;

        $options = [];
        $options['path'] = '';
        if ($query = TransferToUtf8(request()->get('q')))
            $options['query']['q'] = $query;
        if (request()->get('mt'))
            $options['query']['mt'] = request()->get('mt');
        if (request()->get('sku'))
            $options['query']['sku'] = request()->get('sku');
        if (request()->get('br'))
            $options['query']['br'] = request()->get('br');

        $this->pager = new LengthAwarePaginator($this->reviews, $this->reviews->total(), $this->reviews->perPage() , $this->reviews->currentPage(), $options);
    }


    private function _breadcrumbs_review_show(){

        $review = $this->review;
        $categories = CategoryRepository::getCategory(explode('-', $review->product_category_search));
        foreach ($categories as $category){
            $this->breadcrumbs[] = setBreadcrumbs( route('review-search', $category->url_rewrite), $category->name);
        }

//        $categories = $review->product->categories->where('id' , '!=' , 1)->sortBy('depth');
//        foreach ($categories as $category){
//            $this->breadcrumbs[] = setBreadcrumbs( route('review-search', $category->url_rewrite),$category->name);
//        }
        $this->breadcrumbs[] = setBreadcrumbs( '' , $review->title . '(' . $review->product_manufacturer . '：' . $review->product_name . ')' );
    }

    private function _breadcrumbs_review_search(){
        $this->breadcrumbs[] = setBreadcrumbs(route('review-search',null) , '評論搜尋' );
        if (!$this->category)
            return;

        $categoriesPath = explode("-" , $this->category->mptt->url_path);
        $categoriesName = explode("|" , $this->category->mptt->name_path);
        foreach ( $categoriesPath as $index => $path ){
            $name = $categoriesName[$index];
            $this->breadcrumbs[] = setBreadcrumbs( route('review-search',$path) , $name);
        }

        $this->breadcrumbs[ count($this->breadcrumbs) - 1]['url'] = '';
    }

    private function _breadcrumbs_review_step(){
        $this->breadcrumbs[] = setBreadcrumbs(route('review-step',null) , '商品評論使用說明' );
        if (!$this->category)
            return;

        $categoriesPath = explode("-" , $this->category->mptt->url_path);
        $categoriesName = explode("|" , $this->category->mptt->name_path);
        foreach ( $categoriesPath as $index => $path ){
            $name = $categoriesName[$index];
            $this->breadcrumbs[] = setBreadcrumbs( route('review-search',$path) , $name);
        }

        $this->breadcrumbs[ count($this->breadcrumbs) - 1]['url'] = '';
    }

}