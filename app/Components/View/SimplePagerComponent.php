<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;

use Ecommerce\Support\LengthAwarePaginator;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;

class SimplePagerComponent extends SimpleComponent
{
    public $collection;
    public $count;
    public $options;
    public $pager;
    public $page = 1;
    public $sort = 'created_at';
    public $row = 9999;

    public function __construct()
    {
        parent::__construct();
        if(request()->input('page')){
            $this->page = request()->input('page');
        }
        $this->options['path'] = '';
        foreach (request()->except('page') as $key => $value){
            $this->options['query'][$key] = $value;
        }
    }

    public function setCollection($collection, $paginate = false)
    {
        if($collection instanceof \Illuminate\Pagination\LengthAwarePaginator){
            $this->count = $collection->total();
            $this->row = $collection->perPage();
            $listIntegration = new ListIntegration($collection->getCollection());
            $this->collection = $listIntegration->collection;
            $this->page = $collection->currentPage();
        }else{
            $this->count = $collection->count();
            $this->collection = $collection->forPage($this->page, $this->row);
        }

        if($paginate){
            $this->initPager($collection);
        }
    }

    private function initPager()
    {
        $this->pager = new LengthAwarePaginator($this->collection, $this->count, $this->row, $this->page, $this->options);
    }
}