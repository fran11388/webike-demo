<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;

use Ecommerce\Service\GenuinepartsService;

class GenuineComponent extends SimpleComponent
{
    public $sect_manufacturers;
    public $import_manufacturers;
    public $country_manufacturers;

    public function __construct()
    {
        parent::__construct();
        $service = new GenuinepartsService();
        $this->sect_manufacturers = $service->divideSectManufacturers();
        $this->import_manufacturers = $service->divideImportManufacturers();
        $this->country_manufacturers = $service->divideCountryManufacturers();
    }
}