<?php

namespace App\Components\View;

use Everglory\Constants\SEO;

abstract class BaseComponent
{
    public $tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
    public $breadcrumbs = [];
    public $breadcrumbs_html;
    public $seo_title = SEO::TITLE;
    public $seo_description = SEO::DESCRIPTION;
    public $seo_keywords = SEO::KEYWORDS;
    public $home_url;
    public $current_customer;
    public $breadcrumb_json_list;
    public $mobile;
    public $current_route;

    public function __construct()
    {
        //DO NOT use $current_customer in here
        $this->home_url = route('shopping');
        $this->breadcrumbs[] = ['url' => $this->home_url , 'name' => '摩托百貨'];
        $this->current_route = request()->route()->getName();
    }

    /**
     * @param [] $params
     * @return void
     */
    abstract public function setBreadcrumbs($params = NULL,$url = NULL);

    /**
     * @param array $params
     * @return mixed
     */
    abstract public function seo($param = NULL, $text = NULL);


    public function render()
    {
        //render breadcrumb html
        $this->breadcrumbs_html = '';
        $breadcrumb_json_list = [];
        $schema_json = '
            <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "BreadcrumbList",
                    "itemListElement": [
            ';
        $levels = count($this->breadcrumbs) ;

        foreach ($this->breadcrumbs as $level => $breadcrumb){
            //  (last level or url not setting) and  NOT only one level  =>   will blank style
            if(($levels == $level + 1 or !$breadcrumb['url']) and $levels - 1 != 0 ){
                $this->breadcrumbs_html .= '<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . $breadcrumb['name'] . '</span><meta itemprop="position" content="' .  ($level + 1) . '" /></li>';

                $breadcrumb_json_part = '
                    {
                        "@type": "ListItem",
                        "position": '. ($level + 1) .',
                        "item": {
                          "@id": "",
                          "name": "' . $breadcrumb['name'] . '",
                          "url": ""
                        }
                    }
                ';
            }else{
                $this->breadcrumbs_html .= '<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $breadcrumb['url'] . '" title="' . $breadcrumb['name'] . $this->tail . '" itemprop="item"><span itemprop="name">' . $breadcrumb['name'] . '</span></a><meta itemprop="position" content="' .  ($level + 1) . '" /></li>';

                $breadcrumb_json_part = '
                    {
                        "@type": "ListItem",
                        "position": '. ($level + 1) .',
                        "item": {
                          "@id": "' . (isset($breadcrumb['url']) ? $breadcrumb['url'] : '') . '",
                          "name": "' . $breadcrumb['name'] . '",
                          "url": "' . (isset($breadcrumb['url']) ? $breadcrumb['url'] : '') . '"
                        }
                    }
                ';
            }

            $this->breadcrumb_json_list[] = $breadcrumb_json_part;
        }
        $schema_json .= implode(',', $this->breadcrumb_json_list) . ']}</script>';
        $this->breadcrumbs_html .= $schema_json;

        //seo check
        $this->seo_description = str_limit($this->seo_description, 200, '...');
        $this->seo_title .= $this->tail;
    }

}