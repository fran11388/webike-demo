<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;

use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Support\DisplacementConverter;
use Everglory\Constants\SEO;

class ProductComponent extends BaseComponent
{
    public $product;
    public $reviews_info;
    public $populars;
    public $relation_categories;
    public $other_customer_view_products;
    public $stock_info = null;
    public $recent_views;
    public $h1;
    public $is_sell;
    public $information;
    public $warning_text;
    public $category_names = [];
    public $last_category = null;
    public $second_category = null;
    public $ranking_sales_products = 0;
    public $ranking_popularity_products = 0;


    /**
     * @param [] $params
     * @return void
     */
    public function setBreadcrumbs($params = NULL,$url = NULL)
    {
        $this->h1 =
            '【' .
            $this->product->manufacturer->name .
            ($this->product->manufacturer->synonym ? '(' . $this->product->manufacturer->synonym . ')' : '') .
            '】' .
            $this->product->name;

        // motors
        if (count($this->product->motors) == 1) {
            $motor = $this->product->motors->first();
            $this->breadcrumbs[] = [
                'url' => route('motor'),
                'name' => '車型索引',
            ];

            $disp = DisplacementConverter::find($motor->displacement);
            $this->breadcrumbs[] = [
                'url' => getMfUrl(['motor_manufacturer' => $motor->manufacturer->name, 'motor_disp' => $disp->key]),
                'name' => $motor->manufacturer->name . ' ' . $disp->name,
            ];

            $this->breadcrumbs[] = [
                'url' => getMtUrl(['motor_url_rewrite' => $motor->url_rewrite]),
                'name' => $motor->name,
            ];

        }


        // categories
        $url_path = '';
        foreach ($this->product->categories->sortBy('depth') as $current_depth => $category) {
            if ($category->id > 1) {
                $this->category_names[] = $category->name;
                $category = CategoryRepository::find($category->id, 'id');
                if($category){
                    $this->breadcrumbs[] = [
                        'url' => getCaUrl(['category' => $category]),
                        'name' => $category->name,
                    ];
                    $this->last_category = $category;
                    if($category->depth == 2){
                        $this->second_category = $category;
                    }
                }
            }
        }

        // manufacturer
        $this->breadcrumbs[] = [
            'url' => getBrUrl(['manufacturer_url_rewrite' => $this->product->manufacturer->url_rewrite]),
            'name' => $this->product->manufacturer->name,
        ];

        // $second category + manufacturer
        if($this->second_category and $this->last_category->id != $this->second_category->id){
            $combine_types = [];
            $combine_types['category'] = $this->second_category;
            $combine_types['manufacturer'] = $this->product->manufacturer;
            $this->breadcrumbs[] = [
                'url' => forceGetSummeryUrl($combine_types),
                'name' => $this->product->manufacturer->name . ' ' . $this->second_category->name,
            ];
        }

        if($this->last_category){
            // category + manufacturer
            $combine_types = [];
            $combine_types['category'] = $this->last_category;
            $combine_types['manufacturer'] = $this->product->manufacturer;
            $this->breadcrumbs[] = [
                'url' => forceGetSummeryUrl($combine_types),
                'name' => $this->product->manufacturer->name . ' ' . $this->last_category->name,
            ];

            // motor + category + manufacturer
            if(count($this->product->motors) == 1){
                $motor = $this->product->motors->first();
                $combine_types = [];
                $combine_types['motor'] = $motor;
                $combine_types['category'] = $this->last_category;
                $combine_types['manufacturer'] = $this->product->manufacturer;
                $this->breadcrumbs[] = [
                    'url' => forceGetSummeryUrl($combine_types),
                    'name' => $motor->name . ' ' . $this->product->manufacturer->name . ' ' . $this->last_category->name,
                ];
            }

            /* Recover */
            // parts
//            $this->breadcrumbs[] = [
//                'url' => getPartsUrl(['category' => $this->last_category]),
//                'name' => $this->last_category->name . ' ' . '商品搜尋',
//            ];
//
//            $this->breadcrumbs[] = [
//                'url' => getPartsUrl(['category' => $this->last_category, 'manufacturer' => $this->product->manufacturer]),
//                'name' => $this->product->manufacturer->name . ' ' . $this->last_category->name . '商品搜尋',
//            ];

            $this->warning_text = $this->last_category->warning_text;

        }
        //special product does not show category(1,2,3)

        $this->breadcrumbs[] = [
            'url' => '',
            'name' => $this->product->manufacturer->name . '' . $this->product->name,
        ];

        $this->seo();
    }


    public function seo($param = NULL,$text = NULL)
    {
        $this->seo_title = sprintf("%s：%s", $this->product->manufacturer->name, $this->product->name);
        if($this->product->model_number and strpos($this->product->name, $this->product->model_number) === false){
            $this->seo_title .= ' | 料號：' . $this->product->model_number;
        }

        $descriptions = [];

        //$this->seo_description = sprintf( "【購物滿3千免運費】商品編號：%s" , $this->product->model_number);
        $descriptions[] = sprintf("商品名稱：%s：%s，商品編號：%s",$this->product->manufacturer->name,$this->product->name,$this->product->model_number);

        if ($this->product->motors->count() == 1 ) {
            $descriptions[] = '對應：' . $this->product->motors->implode('name',',');
        }

        if ($this->product->productDescription and $this->product->productDescription->summary) {
            $seo_description = strip_tags(str_replace (array("\r\n", "\n", "\r"), ' ',$this->product->productDescription->summary));
            if(strpos($this->product->sku, 't') === false){
                $descriptions[] = '要點：' .$seo_description;
            }
            $descriptions[] = $seo_description;
        }

        if ($this->product->motors->count() > 1 ) {
            $descriptions[] = '對應：' . $this->product->motors->implode('name',',');
        }


        $descriptions[] = SEO::WEBIKE_SHOPPING_LOGO_TEXT;

        $this->seo_description = implode("，" , $descriptions);

    }
}