<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;


class CartComponent extends SimpleComponent
{

    public $carts;
    public $protect_code;
    public $service;
    public $coupons;

    public function __construct()
    {
        parent::__construct();
    }

}