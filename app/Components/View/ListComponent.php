<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;

use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Support\DisplacementConverter;
use Ecommerce\Support\LengthAwarePaginator;
use Everglory\Constants\SEO;

class ListComponent extends BaseComponent
{
    public $searchResponse;

    public $category;
    public $manufacturer;
    public $motor;
    public $keyword;
    public $price;
    public $country;
    public $merge_names = '';
    public $products = [];

    public $manufacturers = [];

    public $count = 0;
    public $import_count = 0;
    public $domestic_count = 0;

    public $sort;
    public $rows;
    public $direction;
    public $page;

    public $price_range;
    public $tree;
    public $pager;
    public $in_stock;
    public $new_product_flg;

    public $current_manufacturer;
    public $current_category;
    public $current_motor;

    public $meta_mt;
    public $meta_ca;
    public $meta_br;

    public $list_name = '商品一覽';

    public $filter;


    /**
     * For ranking
     * @var
     */
    public $type;

    public function __construct()
    {
        parent::__construct();
        $range = explode('-', request('price'));
        $price_validate = true;
        if(count($range) == 2) {
            if (!isset($range[0]) or !is_numeric($range[0])) {
                $price_validate = false;
            }
            if (!isset($range[1]) or !is_numeric($range[1])) {
                $price_validate = false;
            }
        }else{
            $price_validate = false;
        }
        if($price_validate === false){
            request()->merge(['price' => null]);
        }
        $this->in_stock = request('in_stock');
        $this->price = request('price');
        $this->country = request('country');
        if($this->country){
            if($this->country == 'taiwan'){
                $this->country = '國產商品';
            }else{
                $this->country = '進口商品';
            }
        }
        $this->sort = request('sort');
        $this->type = request('type');
        $this->rows = request('limit', 40);
        $this->direction = request('order', '');
        $this->page = request('page', '1');
        $this->keyword = TransferToUtf8(request('q', ''));
        $this->new_product_flg = request('fn', '');
        $this->filter = request('fl', '');
        $this->in_stock = request('in_stock', '');

        $current_route = \Route::currentRouteName();
        if (strpos($current_route, 'outlet') !== false) {
            $this->list_name = 'Outlet' . $this->list_name;
        }

    }

    private function initPager()
    {
        $options = [];
        $options['path'] = '';
        if ($this->keyword)
            $options['query']['q'] = $this->keyword;
        if ($this->sort)
            $options['query']['sort'] = $this->sort;
        if ($this->direction)
            $options['query']['order'] = $this->direction;
        if ($this->rows != 40)
            $options['query']['limit'] = $this->rows;
        if($this->country){
            $options['query']['country'] = request('country');
        }
        if($this->price){
            $options['query']['price'] = $this->price;
        }
        if($this->filter){
            $options['query']['fl'] = $this->filter;
        }
        if($this->new_product_flg){
            $options['query']['fn'] = $this->new_product_flg;
        }
        if($this->in_stock){
            $options['query']['in_stock'] = $this->in_stock;
        }



        $this->pager = new LengthAwarePaginator($this->products, $this->count, $this->rows, $this->page, $options);
    }

    public function setBreadcrumbs($params = NULL,$url = NULL)
    {
        $this->initPager();
        $addition_text = '';
        $last_category = null;
        if($this->new_product_flg == 1){
            $addition_text = '(新商品)';
        }
        if ( $this->current_motor ) {
            $this->breadcrumbs[] = [
                'url' => route('motor'),
                'name' => '車型索引',
            ];

            $disp = DisplacementConverter::find($this->current_motor->displacement);
            $this->breadcrumbs[] = [
                'url' => getMfUrl(['motor_manufacturer' => $this->current_motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key]),
                'name' => $this->current_motor->manufacturer->name . ' ' . $disp->name,
            ];

            $this->breadcrumbs[] = [
                'url' => getMtUrl(['motor_url_rewrite' => $this->current_motor->url_rewrite]),
                'name' => $this->current_motor->name .  ($this->current_motor->synonym ? '(' . $this->current_motor->synonym . ')' : ''),
            ];

            $this->breadcrumbs[] = [
                'url' => (\Route::currentRouteName() == 'outlet' ? '/outlet' : '/parts' ) . end($this->breadcrumbs)['url'],
                'name' => $this->list_name . $addition_text,
            ];
            $this->meta_mt = $this->current_motor->name . ($this->current_motor->synonym ? '(' . $this->current_motor->synonym . ')' : '');
        }

        // categories
        if ( $this->current_category ) {
            $url_paths = explode('-', $this->current_category->mptt->url_path);
            $this->top_cate = $url_paths[0];
            $categories = CategoryRepository::getParents( $this->current_category  , true );
            foreach ($categories as $category) {
                if ($category->id > 1) {
                    $this->breadcrumbs[] = [
                        'url' => getCaUrl(['category' => $category]),
                        'name' => $category->name . ($category->synonym ? '(' . $category->synonym . ')' : ''),
                    ];
                    $last_category = $category;
                }
            }
            $this->meta_ca = $this->current_category->name . ($this->current_category->synonym ? '(' . $this->current_category->synonym . ')' : '');
        }

        if(!$this->current_motor){
            if(!$this->current_category){
                $this->breadcrumbs[] = [
                    'url' => (\Route::currentRouteName() == 'outlet' ? route('outlet') : route('parts') ),
                    'name' => $this->list_name . $addition_text,
                ];
            }else{
                $this->breadcrumbs[] = [
                    'url' => (\Route::currentRouteName() == 'outlet' ? '/outlet' : '/parts' ) . end($this->breadcrumbs)['url'],
                    'name' => $this->list_name . $addition_text,
                ];
            }
        }

        if ( $this->current_manufacturer){
            if($last_category){
                $this->breadcrumbs[] = [
                    'url' => forceGetSummeryUrl(['category' => $last_category, 'manufacturer' => $this->current_manufacturer]),
                    'name' => $this->current_manufacturer->name . ($this->current_manufacturer->synonym ? '(' . $this->current_manufacturer->synonym . ')' : '') . $last_category->name . ($last_category->synonym ? '(' . $last_category->synonym . ')' : ''),
                ];
            }

            $this->breadcrumbs[] = [
                'url' => '',
                'name' => $this->current_manufacturer->name . ($this->current_manufacturer->synonym ? '(' . $this->current_manufacturer->synonym . ')' : ''),
            ];
            $this->meta_br = $this->current_manufacturer->name . ($this->current_manufacturer->synonym ? '(' . $this->current_manufacturer->synonym . ')' : '');
        }

    }

    public function setRankingBreadcrumbs($params = NULL,$url = NULL)
    {
        $this->initPager();
        $addition_text = '';
        $last_category = null;
        if($this->type == 'popularity'){
            $this->list_name = '商品注目度排行榜';
        }elseif($this->type == 'brands'){
            $this->list_name = '銷售品牌排行榜';
        }else{
            $this->list_name = '銷售商品排行榜';
        }

        if ( $this->current_motor ) {
            $this->breadcrumbs[] = [
                'url' => route('motor'),
                'name' => '車型索引',
            ];

            $disp = DisplacementConverter::find($this->current_motor->displacement);
            $this->breadcrumbs[] = [
                'url' => getMfUrl(['motor_manufacturer' => $this->current_motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key]),
                'name' => $this->current_motor->manufacturer->name . ' ' . $disp->name,
            ];

            $this->breadcrumbs[] = [
                'url' => getMtUrl(['motor_url_rewrite' => $this->current_motor->url_rewrite]),
                'name' => $this->current_motor->name .  ($this->current_motor->synonym ? '(' . $this->current_motor->synonym . ')' : ''),
            ];

            $this->meta_mt = $this->current_motor->name . ($this->current_motor->synonym ? '(' . $this->current_motor->synonym . ')' : '');
        }

        // categories
        if ( $this->current_category ) {
            $url_paths = explode('-', $this->current_category->mptt->url_path);
            $this->top_cate = $url_paths[0];
            $categories = CategoryRepository::getParents( $this->current_category  , true );
            foreach ($categories as $category) {
                if ($category->id > 1) {
                    $this->breadcrumbs[] = [
                        'url' => forceGetSummeryUrl(['motor' => $this->current_motor,'category' => $category]),
                        'name' => $category->name . ($category->synonym ? '(' . $category->synonym . ')' : ''),
                    ];
                    $last_category = $category;
                }
            }
            $this->meta_ca = $this->current_category->name . ($this->current_category->synonym ? '(' . $this->current_category->synonym . ')' : '');
        }

//        if(!$this->current_motor){
//            if(!$this->current_category){
//                $this->breadcrumbs[] = [
//                    'url' => route('ranking'),
//                    'name' => $this->list_name . $addition_text,
//                ];
//            }else{
//                $this->breadcrumbs[] = [
//                    'url' => '/ranking' . end($this->breadcrumbs)['url'],
//                    'name' => $this->list_name . $addition_text,
//                ];
//            }
//        }

        if ( $this->current_manufacturer){
            if($last_category){
                $this->breadcrumbs[] = [
                    'url' =>  forceGetSummeryUrl(['motor' => $this->current_motor,'category' => $last_category, 'manufacturer' => $this->current_manufacturer]) ,
                    'name' => $this->current_manufacturer->name . ($this->current_manufacturer->synonym ? '(' . $this->current_manufacturer->synonym . ')' : '') . $last_category->name . ($last_category->synonym ? '(' . $last_category->synonym . ')' : ''),
                ];
            }else{
                $this->breadcrumbs[] = [
                    'url' => forceGetSummeryUrl(['motor' => $this->current_motor, 'manufacturer' => $this->current_manufacturer]),
                    'name' => $this->current_manufacturer->name . ($this->current_manufacturer->synonym ? '(' . $this->current_manufacturer->synonym . ')' : ''),
                ];
                $this->meta_br = $this->current_manufacturer->name . ($this->current_manufacturer->synonym ? '(' . $this->current_manufacturer->synonym . ')' : '');
            }
        }


        $this->breadcrumbs[] = [
            'url' => '',
            'name' => $this->list_name . $addition_text,
        ];
    }


    public function seo($param = NULL, $text = NULL)
    {
        $seo_list_name = $this->list_name;
        if(strpos($seo_list_name, '排行') !== false){
            $seo_list_name = '推薦' . $seo_list_name;
        }
        $meta_mt = $this->meta_mt;
        $meta_ca = $this->meta_ca;
        $meta_br = $this->meta_br;
        $keyword = TransferToUtf8(request()->input('q'));
        if($keyword and ($meta_mt or $meta_ca or $meta_br)){
            $keyword = $keyword . ' ';
        }
        if($meta_mt){
            $this->seo_image = $this->current_motor->image_path . $this->current_motor->image;
            if($this->current_motor->meta_image){
                $this->seo_image = $this->current_motor->meta_image;
            }
            if($meta_ca and $meta_br){ //mt + ca + br
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '【' . $keyword . $meta_mt . ' ' . $meta_br . ' ' . $meta_ca . '】' . $seo_list_name;
                $this->seo_description = '【' . $keyword . $meta_mt . ' ' . $meta_br . ' ' . $meta_ca . '】全球知名品牌' . $seo_list_name . ' - ' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }else if($meta_ca){ //mt + ca
                $this->seo_image = getCategoryImage($this->current_category);
                $this->seo_title = '【' . $keyword . $meta_mt . ' ' . $meta_ca . '】' . $seo_list_name;
                $this->seo_description = '【' . $keyword . $meta_mt . ' ' . $meta_ca . '】全球知名品牌' . $seo_list_name . ' - ' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }else if($meta_br){ //mt + br
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '【' . $keyword . $meta_mt . ' ' . $meta_br . '】' . $seo_list_name;
                $this->seo_description = '【' . $keyword . $meta_mt . ' ' . $meta_br . '】全球知名品牌' . $seo_list_name . ' - ' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }else{ // mt
                $this->seo_title = '【' . $keyword . $meta_mt . '】' . $seo_list_name;
                $this->seo_description = '【' . $keyword . $meta_mt . '】全球知名品牌' . $seo_list_name . ' - ' . SEO::SHIPPING_FREE[activeShippingFree()] . '!';
            }
        }else if($meta_ca){
            $this->seo_image = getCategoryImage($this->current_category);
            if($meta_br){ //ca + br
                $this->seo_title = '【' . $keyword . $meta_br . ' ' . $meta_ca . '】' . $seo_list_name;
                $this->seo_description = $keyword . $meta_br . ' ' . $meta_ca .'，全球知名品牌' . $seo_list_name . ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '!';
                $this->seo_image = $this->current_manufacturer->image;
            }else{  //ca
                if( $this->top_cate == 3000 ){
                    $this->seo_title = '【' . $keyword . $meta_ca . '】' . $seo_list_name;
                    $this->seo_description = $keyword . $meta_ca . '，全球知名品牌' . $seo_list_name . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '!';

                }elseif( $this->top_cate == 1000 ){
                    $this->seo_title = '進口重機、機車【' . $keyword . $meta_ca . '】' . $seo_list_name;
                    $this->seo_description = $keyword . $meta_ca .'，全球知名品牌 - ' . $seo_list_name . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '!';
                }
            }
        }else{ //br
            if($this->current_manufacturer){
                $this->seo_image = $this->current_manufacturer->image;
                $this->seo_title = '【' . $keyword . $meta_br . '】' . $seo_list_name;
                $this->seo_description = $keyword . $meta_br . '，全球知名品牌 - ' . $seo_list_name . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '!';
            }else{
                $this->seo_title = ($keyword ? '【' . $keyword . '】 - ' : '') . '改裝零件、維護耗材、工具、騎士用品一覽';
                $this->seo_description = ($keyword ? '【' . $keyword . '】 - ' : '') . '改裝零件、維護耗材、工具、騎士用品一覽，全球知名品牌 - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '!';
            }
        }
    }

}