<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 下午 09:11
 */

namespace App\Components\View;

use League\Flysystem\Exception;
use Everglory\Models\Customer;

class SimpleComponent extends BaseComponent
{
    public $tail = ' - 「Webike-摩托百貨」';
    public $current_customer;
    public $base_url; // controller url link

    public function setBreadcrumbs($params = NULL, $url = NULL)
    {
        if(is_array($params)){
            foreach($params as $param) {
                if(isset($param['url']) and isset($param['name'])){
                    $this->breadcrumbs[] =  $param;
                }else{
                    throw new Exception('$params is array but key is error!');
                }
            }
        }else{
            $this->breadcrumbs[] =  ['url' => $url , 'name' => $params];
        }
    }

    public function seo($param = Null, $text = NULL)
    {
        if(is_array($param)) {
            foreach ($param as $name => $content) {
                if ($name == 'title') {
                    $this->{ 'seo_' . $name } = $content;
                }else{
                    $this->{ 'seo_' . $name } = $content;
                }
            }
        }else if($param){
            if ($param == 'title') {
                $this->{ 'seo_' . $param } = $text;
            }else{
                $this->{ 'seo_' . $param } = $text;
            }
            if(!$text){
                \Log::warning('Seo setting warning with seo_' . $param . ' at ' . request()->url() . '');
            }
        }else{
            throw new Exception('if param1 not array, must has param2 to set meta[$param1] = $param2');
        }
    }
}