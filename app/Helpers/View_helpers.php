<?php
function formatPrice( $tag , $value ){
    return sprintf('NT$ <span id="%s"> %s</span>' , $tag , number_format($value));
}

function lazyImage($src , $alt = '' , $id = '' ,$class = '',$style = ''){
    if($alt){
        $alt = 'alt="'.$alt.'"';
    }
    if($id){
        $id = 'id="'.$id.'"';
    }
    if($class){
        $class = 'class="'.$class.'"';
    }
    if($style){
        $style = 'style="'.$style.'"';
    }
    return '<img src="https://img.webike.net/sys_images/bg.png" data-src="'.str_replace('http:','',$src).'" '.$alt.' '.$id.' '.$class.' '.$style.'>';
}

function assetRemote($image_path)
{
    if(\Config::get('app.production')){
        if(substr($image_path,0,1) == '/'){
            $url = IMAGE_URL . '/shopping' . $image_path;
        }else{
            $url = IMAGE_URL . '/shopping' . '/' . $image_path;
        }
    }else{
        $url = asset($image_path);
    }

    $useGetParameter = false;
//    $extensions = ['.css', '.js'];
    $extensions = [];
    foreach ($extensions as $extension){
        if(strpos($url, $extension)){
            $useGetParameter = true;
            break;
        }
    }


    if(!$useGetParameter && \Config::get('app.production')){
        $url = cdnTransform($url);
    }


    return ($useGetParameter ? $url . '?time=' . \Cache::get(STATIC_FILE_CACHE_KEY) : $url);
}

function assetPathTranslator($relative_path, $config_app_env)
{
    if($config_app_env == 'www.webike.tw/dev'){
        if(substr($relative_path,0,1) == '/'){
            $url = '/dev' . $relative_path;
        }else{
            $url = '/dev' . '/' . $relative_path;
        }
    }else{
        $url = asset($relative_path);
    }

    return $url;
}

function paymentFormat($order,$cc = null)
{
    $result = array();
    $result['name'] = $order->payment->name;
    $result['company'] = '';
    $result['ono_info'] = '';
    $result['install'] = '';
    $result['first'] = '';
    $result['each'] = '';
    $result['install_info'] = '';
    $result['risk'] = '';


    if( $order->payment_method == 1 or $order->payment_method == 4 or $order->payment_method == 5 ){
        if(!$cc){
            $cc = \Everglory\Models\Sales\Creditcard::where('order_id',$order->id)->first();
        }
        if($cc){
            $result['ono_info'] = $cc->ONO;
            if($order->payment_method != 1){
                //installment
                $detail = unserialize($cc->remark);
                if($detail and false !== strpos($cc->ONO,"NCCC") ){
                    $result['install'] = $detail['Install'];
                    $result['first'] = $detail['FirstAmt'];
                    $result['each'] = $detail['EachAmt'];
                    $result['risk'] = $detail['RiskMark'];
                    $result['company'] = '聯信';
                }else{
                    $result['install']  = $detail['IP'];
                    $result['first'] = $detail['IFPA'];
                    $result['each'] = $detail['IPA'];
                    $result['company'] = '玉山';
                }
                $result['install_info'] = $result['install']. '期信用卡分期(首期'. $result['first'] .'元/其他期'. $result['each'] .'元)';
            }else{
                $result['company'] = '玉山';
            }

        }
    }

    return $result;
}

function getDealerPositionsLink()
{
    $results = \Cache::remember('footer_dealer_links', 24 * 60, function(){
        $testConnection = testServerConnection('wordpress');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $results = \DB::connection('wordpress')->table('webikewp_2_terms')
            ->select('slug', 'name', 'count')
            ->join('webikewp_2_term_taxonomy' , 'webikewp_2_terms.term_id', '=', 'webikewp_2_term_taxonomy.term_id')
            ->get();
        return $results;
    });

    return $results;
}

function getAssortmentTypesLink()
{
    $results = \Cache::remember('footer_assortment_type_links', 24 * 60, function(){
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $types = $assortmentService->getCollectionTypesByUrlRewrite();
        $types = $assortmentService->filterActiveTypes($types);
        return $types;
    });
    return $results;
}

function hasHtml($string)
{
    $tags = '<b><font><sup><img><iframe><hr><table><tbody><tr><td><strong><p><div><script><style>';
    return (preg_match("/<[^<]+>/", $string, $tags) != 0) ? true : false;
}

function echoWithLinkTag($string)
{
    return preg_replace("~[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]~","<a target=\"_blank\" href=\"\\0\">\\0</a>", $string);
}