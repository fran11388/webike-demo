<?php

/**
 * Validate whether to clear cache.And return false or true.
 *
 * @return bool
 */
function useCache()
{
    return (!config('app.cache') or request()->get('noCache')) ? false : true;
}

/**
 * Validate the useCache() return is true or false.If is false and clear this tag's cache.
 *
 * @param string $tags Cache tags
 */
function cacheControlProcess($tags)
{
    if (!useCache()) {
        \Cache::tags($tags)->flush();
    }
}

function get_http_response_code($url)
{
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

function cdnTransform($url)
{

    $origs = [
        '//img.webike.tw/thumb',
        '//img.webike.tw',
        '//img.webike.net',
        '//thimg.webike.net'
    ];

    $cdn = [
        '//img-webike-tw-370429.c.cdn77.org/thumb',
        '//img-webike-tw-370429.c.cdn77.org',
        '//img-webike-370429.c.cdn77.org',
        '//thimg-afcc.kxcdn.com'
    ];

    foreach ($origs as $key => $orig) {
        if (strpos($url, $orig) !== false) {
//            dd($orig . "\r\n" . $cdn[$key] . "\r\n" . (String)$url );
            $url = str_replace($orig, $cdn[$key], (String)$url);
            break;
        }
    }
    return $url;
}

function strReplaceFirst($from, $to, $content)
{
    $from = '/' . preg_quote($from, '/') . '/';

    return preg_replace($from, $to, $content, 1);
}

function fixHttpText($text, $https = true)
{
    if (strpos($text, 'http') === false) {
        if ($https) {
            $text = str_replace('//', 'https://', $text);
        } else {
            $text = str_replace('https', 'http', $text);
        }
    } else {
        if ($https and strpos($text, 'https') === false) {
            $text = str_replace('http', 'https', $text);
        } else if ($https and strpos($text, 'https') !== false) {
            // do nothing
        } else {
            $text = str_replace('https://', 'http://', $text);
        }
    }
    return $text;
}

function ecNotice($data)
{
    \Log::warning($data->log);
    $ecStaffs = Everglory\Models\Staff::where('department_id', 3)->where('email', '<>', 'bright_peng@everglory.asia')->get();
    $sender = new StdClass();
    $sender->address = 'system@webike.tw';
    $sender->name = 'Webike系統';
    foreach ($ecStaffs as $key => $ecStaff) {
        $recipient = new StdClass();
        $recipient->address = $ecStaff->email;
        $recipient->name = $ecStaff->name;
        $wmail = new \Ecommerce\Core\Wmail();
        $wmail->blank($sender, $recipient, $data->mail['title'], $data->mail['content']);
    }
}

/*
* compile laravel code to echo
* $string - what string need to compile
* $args - what variable inside this string
*/
function parseBladeCode($string, array $args = array())
{

    $generated = \Blade::compileString($string);

    ob_start();
    extract($args, EXTR_SKIP);

    try {
        eval('?>' . $generated);
    } catch (\Exception $e) {
        ob_get_clean();
        throw $e;
    }

    $content = ob_get_clean();

    return $content;
}


function withVersion()
{
//    if(!\Config::get('app.cache')){
//        \Config::set('app.cache_timestamp', true);
//    }
}

function activeValidate($from = null, $to = null, $mode = 'publish')
{
    $promo_stat = strtotime('2016-12-04 22:00:00');//當天有
    $promo_end = strtotime('2016-12-17');//當天無
    $now = strtotime(date('Y-m-d H:i:s'));

    //shopping日期少8小時
    // if(request()->route()->getName() == 'shopping'){
    // }
//        $now = strtotime(date('Y-m-d H:i:s')."+8hours");

    /*
     * Example
     * $form = 2016-12-04 22:00:00
     * $to = 2016-12-17
     * $now = '2016-12-04 22:00:01' => true
     * $now = '2016-12-17 00:00:00' => false
    */
    if ($mode === 'private-test') {
        if (isset($_SERVER['REMOTE_ADDR']) and ($_SERVER['REMOTE_ADDR'] === '49.158.20.135' or $_SERVER['REMOTE_ADDR'] === '122.116.103.32')) {
            return true;
        } else {
            return false;
        }
    }

    if (is_null($from) and is_null($to)) {
        return false;
    }
    if ($from and $now < strtotime($from)) {
        return false;
    }
    if ($to and $now >= strtotime($to)) {
        return false;
    }

    return true;
}

function getCategoryImage($category)
{
    $include_img = array(1638, 1639);
    if (in_array($category->id, $include_img)) {
        return '//img.webike.tw/shopping/image/category/index/' . $category->id . '.jpg';
    } else {
        return '//img.webike.net/sys_images/category/bm_' . $category->url_rewrite . '.gif';
    }
}


function getFileKey($ext = null)
{
    $filename = time() . floor(microtime('get_as_float') * 1000);

    return $filename;
}

function ImageResize($from_filename, $save_filename, $in_width = 500, $in_height = 500, $quality = 90)

{

    $allow_format = array('jpeg', 'png', 'gif');

    $sub_name = $t = '';


    // Get new dimensions

    $img_info = getimagesize($from_filename);

    $width = $img_info['0'];

    $height = $img_info['1'];

//    $imgtype = $img_info['2'];
//
//    $imgtag = $img_info['3'];
//
//    $bits = $img_info['bits'];
//
//    $channels = $img_info['channels'];

    $mime = $img_info['mime'];


    list($t, $sub_name) = explode('/', $mime);

    if ($sub_name == 'jpg') {

        $sub_name = 'jpeg';

    }


    if (!in_array($sub_name, $allow_format)) {

        return false;

    }


    $percent = getResizePercent($width, $height, $in_width, $in_height);

    $new_width = $width * $percent;

    $new_height = $height * $percent;

    $image_new = imagecreatetruecolor($new_width, $new_height);


    switch ($sub_name) {
        case 'jpeg':
            $image = imagecreatefromjpeg($from_filename);
            imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            return imagejpeg($image_new, $save_filename, $quality);
            break;
        case 'png':
            $image = imagecreatefrompng($from_filename);
            imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            $quality = round(abs(($quality - 100) / 11.111111));
            return imagepng($image_new, $save_filename, $quality);
            break;
        case 'gif':
            $image = imagecreatefromgif($from_filename);
            imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            return imagegif($image_new, $save_filename);
            break;
    }
}


function getResizePercent($source_w, $source_h, $inside_w, $inside_h)

{

    if ($source_w < $inside_w && $source_h < $inside_h) {

        return 1; // Percent = 1, 如果都比預計縮圖的小就不用縮

    }


    $w_percent = $inside_w / $source_w;

    $h_percent = $inside_h / $source_h;


    return ($w_percent > $h_percent) ? $h_percent : $w_percent;
}


function convertCollectionToProductAccess($collection)
{

    app()->singleton('EmptyProduct', function () {
        return new \Everglory\Models\Product();
    });

    $result = [];
    foreach ($collection as $object) {
        $result[] = new \Ecommerce\Accessor\ProductAccessor($object);
    }

    return collect($result);
}

function setBreadcrumbs($url, $name)
{
    return [
        'url' => $url,
        'name' => $name,
    ];
}

function formatProductFullName($product_name, $manufacturer_name = NULL)
{
    $fullName = $product_name;
    if ($manufacturer_name) {
        $fullName = '【' . $manufacturer_name . '】' . $fullName;
    }
    return $fullName;
}


function substring_index($subject, $delimiter, $count)
{
    if ($count < 0) {
        return implode($delimiter, array_slice(explode($delimiter, $subject), $count));
    } else {
        return implode($delimiter, array_slice(explode($delimiter, $subject), 0, $count));
    }
}

function testServerConnection($connection_name, $error_callback = null)
{
    $result = new \StdClass;
    $result->success = true;
    try {
        DB::connection($connection_name)->getPdo();
    } catch (\Exception $e) {
        Log::critical('Could not connect to the ' . $connection_name . '.  Please check server status.');
        $result->success = false;
        if ($error_callback) {
            $result->callback = $error_callback;
        } else {
            $result->callback = collect([]);
        }
    }
    return $result;
}

function TransferToUtf8($string)
{
    // IE browser will encode without UTF-8 , use this function to detect and convert
    $encode = mb_detect_encoding($string, array('UTF-8', 'BIG5', 'GB2312'));
    if ($encode && $encode != 'UTF-8') {
        $string = iconv($encode, 'UTF-8', $string);
    }
    return $string;
}

function getBasicAjaxObject()
{
    $basicObject = new \StdClass;
    $basicObject->success = false;
    $basicObject->errors = 0;
    $basicObject->messages = [];
    $basicObject->datas = [];
    $basicObject->html = null;

    return $basicObject;
}

function hasChangedLogistic()
{
    $target_date = '2017-10-01';
    if (strtotime('now') >= strtotime($target_date)) {
        return true;
    }
    return false;
}

function hsaNewOffice()
{
    $target_date = '2018-05-14';
    if (strtotime('now') >= strtotime($target_date)) {
        return true;
    }
    return false;
}

function hasChangeDealerShippingFee()
{

    $target_date = '2017-11-09';
    if (strtotime('now') >= strtotime($target_date)) {
        return true;
    }
    return false;
}

function pointUsageCalculate(&$currents, &$spend)
{
    $usage_date = $spend->created_at->toDateString();
    $current = $currents->shift();
    //dd($current);
    $current_points_spend = $spend->points_spend;

    //echo 'new spend:'.$spend->id."|".$spend->points_spend."<br/>";
    while ($current_points_spend and $current) {
        if ($current->order_id and $current->order_id == $spend->order_id) {
            //prevent use points from self, skip it
            $current = $currents->shift();
            //echo 'current:'.$current->id . '|'.$current->points_current.'spend:'.$current_points_spend."<br/>";
        } elseif (true or $usage_date < $current->end_date) { // temp pass
            // calculate point that can use in current
            $can_use_points = $current->points_current - $current->points_usage;
            // record last usage end date
            $spend->usage_end_date = $current->end_date;
            if ($can_use_points <= $current_points_spend) { // $can_use_points is less then spend >> use all and get next current
                $current_points_spend -= $can_use_points; // remove part of spend points
                $current->points_usage = $current->points_current; // use all points in current
                $current->check = 1;
//                $current->setUpdatedAt($current->updated_at);
                $current->save();
                $current = $currents->shift(); // get new current
            } else { // $can_use_points is more then spend
                $current->points_usage += $current_points_spend; //add spend to usage
                $current_points_spend = 0; // no spend need to do
//                $current->setUpdatedAt($current->updated_at);
                $current->save();
            }
        } else {
            //expired
            $current->check = 1;
//            $current->setUpdatedAt($current->updated_at);
            $current->save();
            $current = $currents->shift();
        }
    }

    $spend->check = 1;
//    $spend->setUpdatedAt($spend->updated_at);
    $spend->save();
}

function customerNotHasGift($date_range)
{
    $customer_id = \Auth::user()->id;
    $customer_gift = Everglory\Models\Customer\Gift::where('customer_id', '=', $customer_id)->whereBetween('date', $date_range)->first();
    if ($customer_gift) {
        return false;
    } else {
        return true;
    }

}

function hasChangedInvoice()
{
    $target_date = '2018-07-16 10:30:00';
    if (strtotime('now') >= strtotime($target_date)) {
        return true;
    }
    return false;
}

function shippingFreeCoupon($coupon_code)
{
    $customer_coupon = true;
    if (\Auth::check()) {
        $customer_id = \Auth::user()->id;
        $customer_coupon = Everglory\Models\Coupon::where('customer_id', '=', $customer_id)->
        where('code', '=', $coupon_code)->
        where('expired_at', '>', date('Y-m-d'))->
        whereNull('order_id')->
        first();
    }

    if ($customer_coupon) {
        return true;
    } else {
        return false;
    }

}

function shippfeeupdate()
{
    $target_date = '2018-09-01 00:00:00';
    if (strtotime('now') >= strtotime($target_date)) {
        return true;
    }
    return false;
}

function activeShippingFree()
{
    $role_id = 7;

    if (\Auth::check()) {
        $role_id = \Auth::user()->role_id;
    }

    if (activeValidate('2019-05-01 00:00:00', '2019-06-01') and in_array($role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::GENERAL, \Everglory\Constants\CustomerRole::NOT_LOGGED_IN, \Everglory\Constants\CustomerRole::REGISTERED])) {
        !defined('FREE_SHIP') ? define('FREE_SHIP', 1000) : '';
        return true;
    } else {
        !defined('FREE_SHIP') ? define('FREE_SHIP', 99999999) : '';
        return false;

    }
}

function active2019MayEvent($role_id, $amount)
{
    $start = '2019-05-01 00:00:00';
    $end = '2019-06-01';
    if (config('app.production') === false) {
        $start = '2019-04-29 00:00:00';
    }
    if (activeValidate($start, $end)
        and in_array($role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF])
        and $amount >= 1000
    ) {
        return true;
    } else {
        return false;
    }
}

function ThumborBuildWithCdn($image, $width, $height)
{
    if (strpos($image, 'thimg.webike.net') !== false) {
        return cdnTransform($image);
    }
    return cdnTransform(\Thumbor\Url\Builder::construct(config('phumbor.server'), config('phumbor.secret'), unicodeConvertForSpace(fixHttpText($image)))->fitIn($width, $height)->build());

}

function ip_is_private($ip)
{
    $pri_addrs = array(
        '10.0.0.0|10.255.255.255', // single class A network
        '172.16.0.0|172.31.255.255', // 16 contiguous class B network
        '192.168.0.0|192.168.255.255', // 256 contiguous class C network
        '169.254.0.0|169.254.255.255', // Link-local address also refered to as Automatic Private IP Addressing
        '127.0.0.0|127.255.255.255' // localhost
    );

    $long_ip = ip2long($ip);
    if ($long_ip != -1) {

        foreach ($pri_addrs AS $pri_addr) {
            list ($start, $end) = explode('|', $pri_addr);

            // IF IS PRIVATE
            if ($long_ip >= ip2long($start) && $long_ip <= ip2long($end)) {
                return true;
            }
        }
    }

    return false;

}

function activeEmailValidate($date)
{
    if(strtotime($date) < strtotime('2019-05-24 11:00:00')){
        return true;
    }
    return false;
}