<?php
use Illuminate\Support\Str;

/**
 * @param $sections
 * [
 *  'motor_manufacturer' => MotorManufacturer::class ,
 *  'disp' => int ,
 * ]
 * @return string
 */
function getMfUrl($sections)
{
    if(isset($sections['motor_manufacturer']) && isset($sections['motor_disp'])) {
        return env('APP_URLMODIFY_PATH', '').'/mf/'. $sections['motor_manufacturer'] . '/' . $sections['motor_disp'];
    }
    return '';
}

function getMtUrl($sections)
{
    if(isset($sections['motor_url_rewrite'])) {
        return env('APP_URLMODIFY_PATH', '').'/mt/'. $sections['motor_url_rewrite'];
    }
    return '';
}

function getCaUrl($sections)
{

    if ( isset($sections['category'])){
        return  env('APP_URLMODIFY_PATH', '').'/ca/' . $sections['category']->mptt->url_path;
    }
    return'';
}

function getBrUrl($sections)
{

    if ( isset($sections['manufacturer_url_rewrite'])){
        return  env('APP_URLMODIFY_PATH', '').'/br/' . $sections['manufacturer_url_rewrite'];
    }
    return'';
}


function getPartsUrl( $types ){
    $path = '/parts';
    if ( isset($types['motor']) and $types['motor']){
        $path .=  '/mt/' . $types['motor']->url_rewrite;
    }
    if ( isset($types['category']) and $types['category']){
        $path .=  '/ca/' . $types['category']->mptt->url_path;
    }
    if ( isset($types['manufacturer']) and $types['manufacturer']){
        $path .=  '/br/' . $types['manufacturer']->url_rewrite;
    }

   return env('APP_URLMODIFY_PATH', '').$path;
}

function forceGetSummeryUrl( $types ){
    $path = '';
    if ( isset($types['motor']) and $types['motor']){
        $path .=  '/mt/' . $types['motor']->url_rewrite;
    }
    if ( isset($types['category']) and $types['category']){
        $path .=  '/ca/' . $types['category']->mptt->url_path;
    }
    if ( isset($types['manufacturer']) and $types['manufacturer']){
        $path .=  '/br/' . $types['manufacturer']->url_rewrite;
    }

    return env('APP_URLMODIFY_PATH', '').$path;
}

function getCurrentExploreUrl($arguments = null, $segments = null, $extra_data = null)
{
    $current_route = \Route::currentRouteName();
    if (strpos($current_route, 'parts') !== false) {
        return modifyPartsUrl($arguments, $segments);
    }else if(strpos($current_route, 'outlet') !== false){
        return modifyPartsUrl($arguments, $segments, 'outlet');
    }else if(strpos($current_route, 'ranking') !== false){
        return modifyPartsUrl($arguments, $segments, 'ranking');
    }else if(strpos($current_route, 'summary') !== false) {
        return modifySummaryUrl($arguments, (isset($extra_data['parts']) ? $extra_data['parts'] : false));
    }else if(strpos($current_route, 'collection-type-detail') !== false) {
        return modifyCollectionGetParameters($arguments);
    }else{
        return modifyGetParameters($arguments, (isset($extra_data['url']) ? $extra_data['url'] : null));
    }
}

function modifyCollectionGetParameters($arguments, $url = null)
{
    $link = Request::url();
    if($url){
        $link = $url;
    }
    $parameters = request()->all();
    $parameters['q'] = request()->get('q');
    $parameters['sort'] = request()->get('sort');
    $parameters['order'] = request()->get('order');
    if (request()->get('limit') != 40)
        $parameters['limit'] = request()->get('limit');
    if (count($arguments) > 0) {
        $parameters = array_merge($parameters, $arguments);
    }
    if(isset($parameters['page'])){
        unset($parameters['page']);
    }

    $parameters = array_filter($parameters, function($value) { return $value; });
    if(count($parameters) > 0){
        $link = $link . '?' . http_build_query($parameters);
    }

    return $link;

}


function modifyPartsUrl($arguments, $segments = null, $path_key = 'parts' ,$url = '')
{

    $parameters['q'] = request()->get('q');
    $parameters['sort'] = request()->get('sort');
    $parameters['fn'] = request()->get('fn');
    $parameters['fl'] = request()->get('fl');
    $parameters['in_stock'] = request()->get('in_stock');
    $parameters['order'] = request()->get('order');
    $parameters['country'] = request()->get('country');
    $parameters['type'] = request()->get('type');
    if (request()->get('limit') != 40)
        $parameters['limit'] = request()->get('limit');
    if (count($arguments) > 0) {
        $parameters = array_merge($parameters, $arguments);
    }

    $parameters =array_filter($parameters, function($value) { return $value; });
    $path = env('APP_URLMODIFY_PATH', ''). '/' . $path_key;
    
    if($url){
        $current_segments = array_values(array_filter(explode('/',$url)));
    }else{
        $current_segments = request()->segments();
    }
    

    $paths = ['mt'=>null,'ca'=>null,'br'=>null];
    for ($i = 1; $i < count($current_segments) ; $i += 2) {
            $key = $current_segments[$i];
            $value = $current_segments[$i + 1];
            $paths[$key] = $value;
    }

    if ($segments){
        $paths = array_merge($paths, $segments);
    }
    $paths =array_filter($paths, function($value) { return $value; });
    foreach ($paths as $key => $value ){
        if ($value)
            $path .= '/' . $key . '/' . $value;
    }

    if (!count($parameters)){
        return $path;
    }
    return $path
        .(Str::contains($path, '?') ? '&' : '?')
        .http_build_query($parameters, '', '&');

}

function modifySummaryUrl($segments = null,$parts = false , $sharePaths = null)
{

    $current_segments = request()->segments();
    $paths = ['mt'=>null,'ca'=>null,'br'=>null];
    for ($i = 0; $i < count($current_segments) ; $i += 2) {
        $key = $current_segments[$i];
        if(array_key_exists($key,$paths) and $current_segments[$i + 1] !== '*'){
            $value = $current_segments[$i + 1];
            $paths[$key] = $value;
        }
    }

    if ($segments){
        $paths = array_merge($paths, $segments);
    }

    // three dna will be parts
    /*
     * remove "$parts" for crawler decide what page more important
     * */
//    if($paths['mt'] and $paths['ca'] and $paths['br']){
//        $parts = false;
//    }
    // last category will be parts
    if($paths['ca']){
        $last_ca = explode('-',$paths['ca']);
        $category = Ecommerce\Core\GlobalVariable::get('modifySummaryUrl:CA' , end($last_ca) );
        if ($category === false){
            $category = \Ecommerce\Repository\CategoryRepository::find(end($last_ca));
            Ecommerce\Core\GlobalVariable::add('modifySummaryUrl:CA' , end($last_ca) , $category );
        }

        if($category){
            /*
     * remove "$parts" for crawler decide what page more important
     * */
//            if($last_ca and $last_ca->n_left + 1 == $last_ca->n_right ){
//                $parts = false;
//            }
            //modify if only put the last path

            $paths['ca'] = $category->mptt->url_path;
        }
    }

    $path = env('APP_URLMODIFY_PATH', '').'';

    /*
     * remove "$parts" for crawler decide what page more important
     * */
    if($parts){
        $path = env('APP_URLMODIFY_PATH', '').'/parts';
    }


    foreach ($paths as $key => $value ){
        if ($value)
            $path .= '/' . $key . '/' . $value;
    }

    return $path;

}

function modifyReviewSearchUrlSummary($segments = null){

    $current_segments = request()->segments();
    $paths = ['ca'=>null,'mt'=>null,'br'=>null];
    for ($i = 0; $i < count($current_segments) ; $i += 2) {
        $key = $current_segments[$i];
        if(array_key_exists($key,$paths)) {
            $value = $current_segments[$i + 1];
            $paths[$key] = $value;
        }
    }

    if ($segments){
        $paths = array_merge($paths, $segments);
    }
    $paths =array_filter($paths, function($value) { return $value; });

    $path = '/review/list';


    foreach ($paths as $key => $value ){
        if ($value) {
            if ($key == 'ca') {
                $ca = explode('-',$value);
                $path .= '/' . end($ca);
            } else {
                $path .= (Str::contains($path, '?') ? '&' : '?') . $key . '=' . $value;
            }
        }
    }

    return env('APP_URLMODIFY_PATH', '').$path;
}

function modifyReviewSearchUrl($parameters){
    $path = request()->url();

    if(isset($parameters['ca']) and $parameters['ca']){
        $paths = explode('/',$path);
        $path = '';
        foreach ($paths as $_path){
            if($_path == 'list'){
                $ca = explode('-',$parameters['ca']);
                $path .= $_path .'/' .end($ca);
                unset($parameters['ca']);
                break;
            }else{
                $path .= $_path .'/';
            }
        }
    }

    $parameters =array_filter($parameters, function($value) { return $value; });
    if (!count($parameters)){
        return $path;
    }

    return $path
        .(Str::contains($path, '?') ? '&' : '?')
        .http_build_query($parameters, '', '&');
}

function modifyManufacturerSearchUrl($parameters){
    $path = request()->url();
    

    $parameters =array_filter($parameters, function($value) { return $value; });
    if (!count($parameters)){
        return $path;
    }

    return $path
    .(Str::contains($path, '?') ? '&' : '?')
    .http_build_query($parameters, '', '&');
}

function modifyGetParameters($arguments, $url = null)
{
    $link = Request::url();
    if($url){
        $link = $url;
    }
    $parameters = request()->all();
    $parameters['q'] = request()->get('q');
    $parameters['sort'] = request()->get('sort');
    $parameters['order'] = request()->get('order');
    if (request()->get('limit') != 40)
        $parameters['limit'] = request()->get('limit');
    if (count($arguments) > 0) {
        $parameters = array_merge($parameters, $arguments);
    }
    $parameters = array_filter($parameters, function($value) { return $value; });
    if(count($parameters) > 0){
        $link = $link . '?' . http_build_query($parameters);
    }

    return $link;

}

function modifyCategoryListUrl($route_name , $parameter)
{
    switch ($route_name) {
        case 'parts':
            return modifyPartsUrl(['page' => ''],['ca'=> $parameter ]);
        case 'brand':
            return modifyGetParameters(['page' => '', 'category'=> $parameter ]);
    }
}

function getBikeNewsUrl($url)
{
    //$url example : 2016/07/27/%E3%80%90%E6%9C%8D%E5%8B%99%E6%9B%B4%E6%96%B0%E3%80%91facebook%E6%9C%83%E5%93%A1%E5%8A%9F%E8%83%BD%E6%9B%B4%E6%96%B0/
    return BIKENEWS . $url;
}

function urlsafe_b64encode($string) {
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_',''),$data);
    return $data;
}

function urlsafe_b64decode($string) {
    $data = str_replace(array('-','_'),array('+','/'),$string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}


function hmac_sha1($key, $data)
{
    // Adjust key to exactly 64 bytes
    if (strlen($key) > 64) {
        $key = str_pad(sha1($key, true), 64, chr(0));
    }
    if (strlen($key) < 64) {
        $key = str_pad($key, 64, chr(0));
    }

    // Outter and Inner pad
    $opad = str_repeat(chr(0x5C), 64);
    $ipad = str_repeat(chr(0x36), 64);

    // Xor key with opad & ipad
    for ($i = 0; $i < strlen($key); $i++) {
        $opad[$i] = $opad[$i] ^ $key[$i];
        $ipad[$i] = $ipad[$i] ^ $key[$i];
    }

    return sha1($opad.sha1($ipad.$data, true));
}


function forceHttps($url)
{
    if(substr( $url, 0, 7 ) === "http://"){
        return preg_replace('/^(http?:\/\/)?/', 'https://', $url, 1);
    }elseif(substr( $url, 0, 2 ) === "//"){
        return preg_replace('/\/\//', 'https://', $url, 1);
//        return $url;
    }else{
        //....other case
    }

    return $url;
}

function unicodeConvertForSpace($string)
{
    return str_replace(' ','%20',$string);
}