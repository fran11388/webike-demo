<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Feed::class,
        \App\Console\Commands\CalculateCustomerLevel::class,
        \App\Console\Commands\Test::class,
        \App\Console\Commands\RemindCustomerCoupons::class,
        \App\Console\Commands\CronMidnight::class,
        \App\Console\Commands\CreateSitemap::class,
        \App\Console\Commands\CouponBirthday::class,
        \App\Console\Commands\Monitor::class,
        \App\Console\Commands\SolrUpdate::class,
        \App\Console\Commands\StaffEstimatePassage::class,
        \App\Console\Commands\SendNewsLetter::class,
        \App\Console\Commands\PriceCheck::class,
        \App\Console\Commands\OutletCheck::class,
        \App\Console\Commands\CouponPromotion::class,
        \App\Console\Commands\EmailCheck::class,
        \App\Console\Commands\SendSystemClearLog::class,
        \App\Console\Commands\WeeklyCacheClear::class,
        \App\Console\Commands\CouponBirthdayCheck::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

//        $schedule->command('feed')->cron('*/15 8-24 * * *');
//        $schedule->command('Remind:CustomerCoupons')->cron('0 * * * *');
        $schedule->command('Remind:CustomerCoupons')->cron('0 0 * * *');
//        $schedule->command('Send:NewsLetter')->cron('*/5 * * * *');
        $schedule->command('Calculate:CustomerLevel true')->dailyAt('22:00');
        $schedule->command('Cron:Midnight')->dailyAt('01:00');
        $schedule->command('Price:Check')->dailyAt('09:00');
        $schedule->command('Price:Check')->dailyAt('16:00');
        $schedule->command('Price:Check')->dailyAt('22:00');
        $schedule->command('Outlet:Check')->cron('18 */2 * * *');
        $schedule->command('Coupon:Promotion')->cron('0 0 24 12 *');
        $schedule->command('Email:Check')->cron(' */30 * * * *');
        $schedule->command('delete:newsletter406')->cron('*/2 * * * *');
        $schedule->command('Send:SystemClearLog')->dailyAt('09:00');
        $schedule->command('Weekly:CacheClear')->cron('0 0 * * 5');
//        $schedule->command('StaffEstimatePassage:genuineparts')->cron('6 * * * *')->withoutOverlapping();
        $schedule->command('coupon:birthday')->cron('00 12 01 * *');
        $schedule->command('coupon:birthday-check')->cron('00 09 02 * *');


        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
