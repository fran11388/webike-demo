<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:newsletter406';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete:newsletter406';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        \DB::table('ec_dbs.email_newsletter_queue_items')->where('queue_id','=',406)->limit(20000)->delete();

    }
}
