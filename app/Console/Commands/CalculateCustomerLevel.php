<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Illuminate\Console\Command;

class CalculateCustomerLevel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Calculate:CustomerLevel {update? : true or false } {year_month? : yyyyMM}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate & update customer level';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $update = strtolower($this->argument('update')) == 'true';
        $year_month = $this->argument('year_month');
        $repository = new CustomerLevelRepository();
        $repository->calculate($update , $year_month);
    }
}
