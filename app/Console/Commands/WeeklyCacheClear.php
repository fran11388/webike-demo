<?php

namespace App\Console\Commands;

use Ecommerce\Core\Wmail;
use Everglory\Models\Campaign;
use Illuminate\Console\Command;
use Uuid;


class WeeklyCacheClear extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $name = 'Weekly:CacheClear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly Cache Clear';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $week =  date('W')+1;
        $Campaign = Campaign::select('url_rewrite')
        ->where('week',$week)
        ->where('active',1)
        ->first();

        if($Campaign){
            $url = "https://www.webike.tw/campaigns/" . $Campaign->url_rewrite . "?noCache=true";        
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_HEADER, true);
            $result = curl_exec($ch);
            curl_close($ch);
        }

    }

}