<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use Everglory\Models\Staff;
use Illuminate\Console\Command;
use Ecommerce\Repository\CouponRepository;
use Everglory\Models\Disp;

class SendSystemClearLog extends Command
{
    public $staffs = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Send:SystemClearLog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send system clear log in every 30 minutes';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $tomorrow = date('Y-m-d',strtotime(date('Y-m-d').'+ 1 days'));
        $system_logs = \DB::select('
            select * from ec_dbs.system_clean_log order by created_at desc
        ');
        $system_logs = collect($system_logs);
        $not_send_mails = $system_logs->where('send_mail','=',0);
        $already_send_mails = $system_logs->where('send_mail','=',1);

        if(count($not_send_mails)){
            $data = [];
            foreach($not_send_mails as $key => &$not_send_mail){
                $last_record = $already_send_mails->where('server','=',$not_send_mail->server)->where('type','=',$not_send_mail->type)->where('service','=',$not_send_mail->service)->first();

                $data[$key]['distance'] = '無先前紀錄';
                if($last_record){
                    $data[$key]['distance'] = (int)((strtotime($not_send_mail->created_at) - strtotime($last_record->created_at))/86400).'天';
                }
                $data[$key]['server'] = $not_send_mail->server;
                $data[$key]['type'] = $not_send_mail->type;
                $data[$key]['service'] = $not_send_mail->service;
                $data[$key]['begin'] = $not_send_mail->begin_usaged_amount.'%';
                $data[$key]['final'] = $not_send_mail->final_usaged_amount.'%';
                $data[$key]['clean_time'] = $not_send_mail->created_at;

                $not_send_mail->send_mail = 1;
                $html = $this->getTableHtml($data);

                \DB::table('ec_dbs.system_clean_log')->where('id','=',$not_send_mail->id)->update(['send_mail' => 1]);
            }

            $title = '伺服器例行清理紀錄 - '.date('Y-m-d h:i:s');
            $subject = '<img src="https://img.webike.tw/shopping/image/webike_tw_logo_new.png"><br><strong>'.$title.'<br></strong><br>'.$html;
            $this->sendMail(null,$title,$subject);
        }
    }

    private function getTableHtml($system_data)
    {
         $html = '
            <table border="1px" style="border-color:#ccc;text-align:center">
                <tr>
                    <th>伺服器</th>
                    <th>清理類型</th>
                    <th>服務</th>
                    <th>清理前使用量</th>
                    <th>清理後使用量</th>
                    <th>清理時間</th>
                    <th>距上次清理時間</th>
                </tr>
        ';

         foreach($system_data as $data){
             $html .= '<tr>
                    <td>'.$data['server'].'</td>
                    <td>'.$data['type'].'</td>
                    <td>'.$data['service'].'</td>
                    <td>'.$data['begin'].'</td>
                    <td>'.$data['final'].'</td>
                    <td>'.$data['clean_time'].'</td>
                    <td>'.$data['distance'].'</td>
                </tr>';
         }

        return $html.'</table>';
    }

    private function sendMail($sender,$title,$subject)
    {
        $wmail = new \Ecommerce\Core\Wmail();

        $recipient = new \StdClass;
        $recipient->address = 'sgml@everglory.asia';
        $recipient->name = 'SG全體同仁';
        $wmail->blank($sender, $recipient, $title, $subject);

    }
}