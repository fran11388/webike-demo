<?php

namespace App\Console\Commands;

use Ecommerce\Core\Wmail;
use Everglory\Models\Coupon;
use Everglory\Models\Coupon\Base;
use Everglory\Models\Coupon\History;
use Everglory\Models\Customer;
use Illuminate\Console\Command;
use Uuid;


class EmailCheck extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $name = 'Email:Check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Email Queue Status';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $queue = null;

        if(date('Y-m-d H:i:s') >= "2018-11-10 12:00:00"){
            $queue = \Everglory\Models\Email\Newsletter\Queue::where('id',423)->where('status_id','<>',3)->first(); 
        }

        if(date('Y-m-d H:i:s') >= "2018-11-11 21:00:00"){
            $queue = \Everglory\Models\Email\Newsletter\Queue::where('id',425)->where('status_id','<>',3)->first(); 
        }

        if($queue){
            $queue->status_id = 3;
            $queue->send_end_time = date('Y-m-d H:i:s');
            $queue->save();
        }

        
    }

}