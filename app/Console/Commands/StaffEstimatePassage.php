<?php

namespace App\Console\Commands;

use Ecommerce\Service\EstimateService;
use Everglory\Models\Customer\Estimate as CustomerEstimate;
use Illuminate\Console\Command;

class StaffEstimatePassage extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $name = 'StaffEstimatePassage:genuineparts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Let staff to get genuineparts price to compare with normal product price';

    protected $max_count = 50;
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $divide = 'import';
        $estimates = CustomerEstimate::where('active', 0)->take(50)->get();
        $had_estimate_items = [];
        $curl_para = [];
        $count = 0;
        $result = null;
        foreach ($estimates->groupBy('customer_id') as $customer_id => $items){
            $curl_para['customer_id'] = $customer_id;
            foreach ($items as $item){
                $curl_para['item'][$count]['model_number'] = preg_replace('/\s(?=)/', '', trim($item->model_number));
                $curl_para['item'][$count]['quantity'] = $item->quantity;
                $curl_para['item'][$count]['manufacturer_id'] = $item->manufacturer_id;
                $curl_para['item'][$count]['product_type'] = 1;
                $curl_para['item'][$count]['note'] = $item->note;
                $had_estimate_items[] = $item;
                $count++;
                if($count == $this->max_count){
                    $result = $this->postEstimate($divide, $curl_para);
                    if(!is_null($result) and $result->error_message == ''){
                        $this->setActiveCustomerEstimate(collect($had_estimate_items), $result->estimate_id);
                    }
                    $had_estimate_items = [];
                    $curl_para = [];
                    $curl_para['customer_id'] = $customer_id;
                    $count = 0;
                }
            }
            if(count($had_estimate_items)){
                $result = $this->postEstimate($divide, $curl_para);
                if(!is_null($result) and $result->error_message == ''){
                    $this->setActiveCustomerEstimate(collect($had_estimate_items), $result->estimate_id);
                }
            }
        }

    }

    private function postEstimate($divide, $curl_para)
    {
        $estimateService = new EstimateService(true);
        $result = $estimateService->estimateProductUseApi($divide, $curl_para);
        return $result;
    }

    private function setActiveCustomerEstimate($items, $estimate_increment_id)
    {
        $customer_estimate_ids = $items->pluck('id');
        CustomerEstimate::whereIn('id', $customer_estimate_ids)->update(['active' => 1, 'updated_at' => date('Y-m-d H:i:s'), 'estimate_increment_id' => $estimate_increment_id]);
    }

}