<?php

namespace App\Console\Commands;

use Everglory\Models\Assortment;
use Everglory\Models\Product;
use Illuminate\Console\Command;

class CreateSitemap extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $signature = 'sitemap {--function=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Sitemap';

    /**
     * Url name
     *
     * @var string
     */
    protected $website;

    /**
     * File location
     *
     * @var string
     */
    protected $sitemap_path = 'sitemap';

    /**
     * Sitemap url counter, reset if value equal 50000 (sitemap limit).
     *
     * @var int
     */
    protected $counter = 0;


    /**
     * Sitemap files counter.
     *
     * @var int
     */
    protected $sitemapCounter = 0;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // create new sitemap object
        $sitemap = \App::make("sitemap");
        $this->website = config('app.url') .'/';
        $destinationPath = public_path() . '/sitemap/';
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777);
        }else{
            $files = glob($destinationPath . '*'); // get all file names
            foreach($files as $file){ // iterate files
                if(is_file($file))
                    unlink($file); // delete file
            }
        }

        $date=date('Y-m-d');

        ini_set('memory_limit','2048M');
        set_time_limit(28800);
        ini_set('max_execution_time', 28800);
        echo date('Y-m-d H:i:s'). "\r\n";;
        $main_pages = [
            'shopping',
            'motor',
            'genuineparts',
            'genuineparts/import',
//             'genuineparts/domestic',
//             'outlet',
            'br',
            'collection',
            'review',
            'benefit',
//             'information/dealer',
//             'groupbuy',
//             'mitumori',
            'benefit/event/motogp/2017/'.urlencode('卡達站'),
            'benefit/event/motogp/2017/'.urlencode('阿根廷站'),
            'benefit/event/motogp/2017/'.urlencode('美國站'),
            'benefit/event/motogp/2017/'.urlencode('西班牙站'),
            'benefit/event/motogp/2017/'.urlencode('法國站'),
            'benefit/event/motogp/2017/'.urlencode('義大利站'),
            'benefit/event/motogp/2017/'.urlencode('加泰隆尼亞站'),
            'benefit/event/motogp/2017/'.urlencode('荷蘭站'),
            'benefit/event/motogp/2017/'.urlencode('德國站'),
            'benefit/event/motogp/2017/'.urlencode('奧地利站'),
            'benefit/event/motogp/2017/'.urlencode('捷克站'),
            'benefit/event/motogp/2017/'.urlencode('英國站'),
            'benefit/event/motogp/2017/'.urlencode('聖馬利諾站'),
            'benefit/event/motogp/2017/'.urlencode('亞拉岡站'),
            'benefit/event/motogp/2017/'.urlencode('日本站'),
            'benefit/event/motogp/2017/'.urlencode('澳洲站'),
            'benefit/event/motogp/2017/'.urlencode('馬來西亞站'),
            'benefit/event/motogp/2017/'.urlencode('瓦倫西亞站'),
            'benefit/event/motogp/2017/'
        ];

        $this->counterCheck($sitemap);
        $sitemap->add($this->website , $date, 1.0, 'weekly');
        foreach($main_pages as $main_page){
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . $main_page , $date, 1.0, 'weekly');
        }

        //Genuinepart
        $genuinepart_service = new \Ecommerce\Service\GenuinepartsService;
        $import_manufacturers = $genuinepart_service->divideImportManufacturers();
        foreach($import_manufacturers as $country => $genuine_manufacturers){
            if(isset($genuine_manufacturers['group'])){
                foreach($genuine_manufacturers['group'] as $genuine_manufacturer){
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'genuineparts/'.$genuine_manufacturers['url_code'] . '/' . $genuine_manufacturer->api_usage_name, $date, 1.0, 'weekly');
                }
            }
        }

        $reviews = \Everglory\Models\Review::select('id')->whereNull('deleted_at')->where('view_status',1)->get();
        foreach ($reviews as $review){
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'review/article/'.$review->id, $date, 0.7, 'weekly');
        }


        $assortments = Assortment::select(\DB::Raw('SUBSTRING(link, 2) as link'),'banner','meta_title','meta_description')->where('link','like','/%')->where('active',1)->get();
        foreach ( $assortments as $assortment ) {
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . $assortment->link, $date, 0.7, 'weekly',
                [
                    ['url' => $assortment->banner , 'title' => $assortment->meta_title, 'caption' => $assortment->meta_description]
                ]
            );
        }

        // Bikenews
        $dealers = \DB::connection('wordpress')->table('webikewp_2_posts')
            ->where('post_type', 'post')->where('post_parent', 0)->where('post_status', 'publish')->where('post_password', '')
            ->select('post_name')
            ->get();
        foreach ( $dealers as $dealer ) {
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'information/dealer/'.$dealer->post_name, $date, 0.6, 'weekly');
        }


        // Product
        $products = Product::select('url_rewrite')->where('type', 0)->where('is_main', 1)->where('active', 1)->get();
        foreach ($products->chunk(2000) as $chunk){
            foreach ($chunk as $product){
                $this->counterCheck($sitemap);
                $sitemap->add($this->website . 'sd/'.$product->url_rewrite, $date, 1.0, 'weekly');
            }
        }


        // Summary ( mt / ca / br / parts )
        $motors = \Everglory\Models\Motor::select('id','url_rewrite')->get();
        foreach ($motors as $motor){
            //mt
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'mt/' . $motor->url_rewrite, $date, 1.0, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'parts/mt/' . $motor->url_rewrite, $date, 0.8, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'mt/' . $motor->url_rewrite . '/tab/top', $date, 1.0, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'mt/' . $motor->url_rewrite . '/tab/service', $date, 0.8, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'mt/' . $motor->url_rewrite . '/tab/review', $date, 0.8, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'mt/' . $motor->url_rewrite . '/tab/video', $date, 0.8, 'weekly');

            $collections = $this->getMotorRelation($motor->id);
            if(count($collections)){
                //mt + ca
                $collection_ca = $collections->groupBy('url_path');
                foreach ( $collection_ca as $url_path => $collection ) {
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'mt/' . $motor->url_rewrite . '/ca/' . $url_path, $date, 1.0, 'weekly');
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'parts/mt/' . $motor->url_rewrite . '/ca/' . $url_path, $date, 0.8, 'weekly');

                    //mt + ca + br
                    foreach ($collection as $entity){
                        $this->counterCheck($sitemap);
                        $sitemap->add($this->website . 'parts/mt/' . $motor->url_rewrite . '/ca/' . $url_path . '/br/' . $entity->br_url_rewrite, $date, 0.8, 'weekly');
                    }
                }
                //mt + br
                $collection_br = $collections->keyBy('br_url_rewrite');
                foreach ( $collection_br as $br_url_rewrite => $collection ) {
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'mt/' . $motor->url_rewrite . '/br/' . $br_url_rewrite, $date, 1.0, 'weekly');
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'parts/mt/' . $motor->url_rewrite . '/br/' . $br_url_rewrite, $date, 0.8, 'weekly');
                }

            }
        }


        $categories = \Everglory\Models\Mptt::select('id','url_path',\DB::Raw('(n_right - n_left) as bottom_category'))
            ->where(function ($query) {
                $query->where('url_path', 'like', '1000%')
                    ->orWhere('url_path', 'like', '3000%')
                    ->orWhere('url_path', 'like', '4000%')
                    ->orWhere('url_path', 'like', '8000%');
            })
            ->where('_active', 1)->get();

        //ca
        foreach($categories as $category){

            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'ca/' . $category->url_path, $date, 1.0, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'parts/ca/' . $category->url_path, $date, 0.8, 'weekly');


            //ca + br
            $collection = $this->getCategoryRelation($category->id);
            if(count($collection)){
                foreach ( $collection as $entity ) {
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'ca/' . $category->url_path.'/br/'.$entity->br_url_rewrite, $date, 0.8, 'weekly');
                    $this->counterCheck($sitemap);
                    $sitemap->add($this->website . 'parts/ca/' . $category->url_path.'/br/'.$entity->br_url_rewrite, $date, 0.8, 'weekly');
                }
            }
        }

        //br
        $manufacturers = \Everglory\Models\Manufacturer::select('id','url_rewrite')->where('active',1)->get();
        foreach ( $manufacturers as $manufacturer ) {
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'br/'.$manufacturer->url_rewrite, $date, 0.8, 'weekly');
            $this->counterCheck($sitemap);
            $sitemap->add($this->website . 'parts/br/'.$manufacturer->url_rewrite, $date, 0.8, 'weekly');
        }
        echo 'All finish' . date('Y-m-d H:i:s') . "\r\n";

        // you need to check for unused items
        if (!empty($sitemap->model->getItems()))
        {
            $this->insertSitemap($sitemap);
        }
        echo 'Check' . date('Y-m-d H:i:s') . "\r\n";
        // generate new sitemapindex that will contain all generated sitemaps above

        $sitemap->store('sitemapindex','sitemap',public_path($this->sitemap_path));
        echo 'Sitemap index created' . date('Y-m-d H:i:s') . "\r\n";
        exit();
    }

    private function counterCheck(&$sitemap){
        if ($this->counter == 50000){
            $this->insertSitemap($sitemap);
            // reset the counter
            $this->counter = 0;
            // count generated sitemap
            $this->sitemapCounter++;
        }
        $this->counter++;
    }

    private function insertSitemap(&$sitemap){
        // generate new sitemap file
        $sitemap->store('xml','sitemap'.$this->sitemapCounter,public_path($this->sitemap_path));
        // add the file to the sitemaps array
        $sitemap->addSitemap($this->website . $this->sitemap_path . '/sitemap'.$this->sitemapCounter.'.xml');
        // reset items array (clear memory)
        $sitemap->model->resetItems();
    }

    private function getMotorRelation($motor_id)
    {
        return \DB::connection('eg_product')->table('product_motor as pm')
            ->join('motors as mt','pm.motor_id','=','mt.id')
            ->join('category_product as cp','cp.product_id','=','pm.product_id')
            ->join('mptt_categories_flat as mptt','mptt.id','=','cp.category_id')
            ->join('products as p','pm.product_id','=','p.id')
            ->join('manufacturers as br','p.manufacturer_id','=','br.id')
            ->where('p.active',1)->where('mptt._active',1)->where('br.active',1)->where(function ($query) {
                $query->where('url_path', 'like', '1000%')
                    ->orWhere('url_path', 'like', '3000%')
                    ->orWhere('url_path', 'like', '4000%')
                    ->orWhere('url_path', 'like', '8000%');
            })
            ->where('motor_id',$motor_id)
            ->groupBy('pm.motor_id','cp.category_id','p.manufacturer_id')
            ->select('mt.url_rewrite as mt_url_rewrite','mptt.url_path','br.url_rewrite as br_url_rewrite')
            ->get();
    }

    private function getCategoryRelation($category_id)
    {
        return \DB::connection('eg_product')->table('category_product as cp')
            ->join('products as p','cp.product_id','=','p.id')
            ->join('manufacturers as br','p.manufacturer_id','=','br.id')
            ->where('p.active',1)->where('br.active',1)
            ->where('cp.category_id',$category_id)
            ->groupBy('cp.category_id','p.manufacturer_id')
            ->select('br.url_rewrite as br_url_rewrite')
            ->get();
    }

}
