<?php

namespace App\Console\Commands;

require base_path() . '/constants/domain.php';

use Ecommerce\Repository\CustomerLevelRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\SolrService;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use Everglory\Models\Staff;
use Everglory\Models\WarehouseProduct;
use Illuminate\Console\Command;
use Ecommerce\Repository\CouponRepository;
use Everglory\Models\Disp;

class OutletCheck extends Command
{
    public $staffs = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Outlet:Check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check outlet sold out or not';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        $url = "http://153.120.83.216:9000/solr/webike/dataimport/?command=status&wt=json";
        if(get_http_response_code($url) == "200"){
            $status = file_get_contents($url);
            $status = json_decode($status);
            if(is_object ($status) ){
                $solrService = new SolrService;
//                $solrService->setCore('webike');

                // stock available
                if(property_exists($status,'status') and $status->status == 'idle') {

                    $product_ids = WarehouseProduct::select('id', 'sku')->where('type_id', 5)->where('status_id', '=', 6)->get()->pluck('id', 'sku');
                    foreach ($product_ids->chunk(200) as $product_id_group) {
//                        $product_id_group = ['t00051659' => 2940710, 't00050152' => 2905537];
//                        $skus = array_keys($product_id_group);
                        $skus = array_keys($product_id_group->toArray());
                        $stocks = ProductService::getEgZeroStockInfoByApi(['barcode' => implode("_", $skus)]);
                        $active_barcode = [];
                        $active_ids = [];
                        foreach ($stocks as $stock) {
                            if (isset($stock->quantity) && $stock->quantity) {
                                $active_barcode[] = $stock->barcode;
                                $active_ids[] = $product_id_group[$stock->barcode];
                            }
                        }
                        if (count($active_barcode) and count($active_ids)) {
                            ProductRepository::active($active_barcode, 'sku');
                            WarehouseProduct::whereIn('id', $active_ids)->update(['status_id' => 9]);
                            $products = ProductRepository::getProductWithRelations($active_barcode, ['motors', 'categories', 'categories.mptt', 'manufacturer', 'productDescription', 'points', 'prices', 'images']);
                            foreach ($products as $product) {
                                $solrService->updateDocumentsByProduct($product);
                            }
                        }
//                        break;
                    }


                    // outlet unavailable
                    $product_ids = WarehouseProduct::select('id', 'sku')->where('type_id', 5)->whereIn('status_id',[8,9])->get()->pluck('id', 'sku');
                    foreach ($product_ids->chunk(200) as $product_id_group) {
//                        $product_id_group = ['t00051659' => 2940710, 't00050152' => 2905537];
//                        $skus = array_keys($product_id_group);
                        $skus = array_keys($product_id_group->toArray());
                        $stocks = ProductService::getEgZeroStockInfoByApi(['barcode' => implode("_", $skus)]);
                        $non_active_barcode = [];
                        $non_active_ids = [];
                        foreach ($stocks as $stock) {
                            if (!$stock->quantity) {
                                $non_active_barcode[] = $stock->barcode;
                                $non_active_ids[] = $product_id_group[$stock->barcode];
                            }
                        }
                        if (count($non_active_barcode)) {
                            ProductRepository::nonActive($non_active_barcode, 'sku');
                            WarehouseProduct::whereIn('id', $non_active_ids)->update(['status_id' => 6]);
                            foreach ($non_active_ids as $non_active_id) {
                                $solrService->removeDocumentsByProduct($non_active_id);
                            }
                        }
//                        break;
                    }



//                    $available_product_ids = \Zero\Entities\Stock::whereIn('product_id',$product_ids)->where('sale_id',2)->whereIn('operation_id',[1,2,6])->lists('product_id');
//                    $remove_array = array_diff($product_ids,$available_product_ids);
//                    if(count($remove_array)){
//                        $products = \Product::with(['images','descriptionDetail','manufacturer','reviews','motors'])->whereIn('id',$remove_array)->get();
//                        foreach($products as $product) {
//                            $product->active = 0;
//                            $product->save();
//                            echo \Solr::updateDocumentsByProduct($product, 'remove') . '<br>';
//                        }
//                        WarehouseProduct::whereIn('id',$remove_array)->update(['status_id'=>6]);
//                    }

                    //outlet 相同料號數量大於2個自動下架
//                    $sale_barcode = [];
//                    $products = WarehouseProduct::select('id', 'sku')->where('type_id', 5)->where('status_id', '=', 9)->get()->pluck('id', 'sku');
//                    foreach ($products->chunk(200) as $product_id_group) {
//                        $skus = array_keys($product_id_group->toArray());
////                        $product_id_group = ['t00123753' => 4133673,'t00123754' => 4133674,'t00123775' => 4133695,'t00123776' => 4133696,'t00123777' => 4133697,'t00123778' => 4133698,'t00123779' => 4133699,'t00123780' => 4133700,'t00123781' => 4133701,'t00123782' => 4133702,'t00123785' => 4133705,'t00123786' => 4133706,'t00123745' => 4133665,'t00123746' => 4133666,'t00123747' => 4133667,'t00123748' => 4133668,'t00123749' => 4133669,'t00123750' => 4133670,'t00123751' => 4133671,'t00123752' => 4133672,'t00123774' => 4133694];
////                        $skus = array_keys($product_id_group);
//                        $stocks = ProductService::getEgZeroStockInfoByApi(['barcode' => implode("_", $skus)]);
//
//                        foreach($stocks as $stock){
//                            $sale_barcode[] = $stock->barcode;
//                        }
//                    }
//
//                    if(count($sale_barcode)){
//                        $sale_products = WarehouseProduct::select('id', 'sku', 'code')->where('type_id', 5)->where('status_id', '=', 9)->whereIn('sku',$sale_barcode)->orderBy('code')->get();
//                        $number = 1;
//                        $code = null;
//                        $non_active_barcode = [];
//                        $non_active_ids = [];
//                        foreach($sale_products as $key => $product){
//                            if($product->code == $code && $number > 2){
//                                $non_active_barcode[] = $product->sku;
//                                $non_active_ids[] = $product->id;
//                            }elseif($product->code != $code){
//                                $number = 1;
//                            }
//                            $number++;
//                            $code = $product->code;
//                        }
//                        if (count($non_active_barcode)) {
//                            ProductRepository::nonActive($non_active_barcode, 'sku');
//                            WarehouseProduct::whereIn('id', $non_active_ids)->update(['status_id' => 6]);
//                            foreach ($non_active_ids as $non_active_id) {
//                                $solrService->removeDocumentsByProduct($non_active_id);
//                            }
//                        }
//                    }
//
//                    //outlet 相同料號數量小於2個自動上架
//                    $sale_barcode = [];
//                    $products = WarehouseProduct::select('id', 'sku')->where('type_id', 5)->whereIn('status_id',[6,9])->whereNotIn('brand_id',[592])->get()->pluck('id', 'sku');
//                    foreach ($products->chunk(200) as $product_id_group) {
//                        $skus = array_keys($product_id_group->toArray());
////                        $product_id_group = ['t00123753' => 4133673,'t00123754' => 4133674,'t00123775' => 4133695,'t00123776' => 4133696,'t00123777' => 4133697,'t00123778' => 4133698,'t00123779' => 4133699,'t00123780' => 4133700,'t00123781' => 4133701,'t00123782' => 4133702,'t00123785' => 4133705,'t00123786' => 4133706,'t00123745' => 4133665,'t00123746' => 4133666,'t00123747' => 4133667,'t00123748' => 4133668,'t00123749' => 4133669,'t00123750' => 4133670,'t00123751' => 4133671,'t00123752' => 4133672,'t00123774' => 4133694];
////                        $skus = array_keys($product_id_group);
//                        $stocks = ProductService::getEgZeroStockInfoByApi(['barcode' => implode("_", $skus)]);
//
//                        foreach ($stocks as $stock) {
//                            if($stock->quantity && !$stock->sold_out){
//                                $sale_barcode[] = $stock->barcode;
//                            }
//                        }
//                    }
//
//                    if(count($sale_barcode)) {
//                        $sale_products = WarehouseProduct::select('id', 'sku', 'code', 'status_id')->where('type_id', 5)->whereIn('status_id', [6, 9])->whereIn('sku', $sale_barcode)->orderBy('code')->get();
//
//                        $sale_products_group_code = $sale_products->groupBy('code');
//                        $non_active_product = null;
//                        $non_active_products = null;
//                        $active_products = null;
//                        $active_barcode = [];
//                        $active_ids = [];
//                        foreach ($sale_products_group_code as $code => $products) {
//                            if (count($products) >= 2) {
//                                foreach ($products->groupBy('status_id') as $status => $products_group_status) {
//                                    if ($status == 6) {
//                                        $non_active_product = $products_group_status->first();
//                                        $non_active_products = $products_group_status;
//                                    } else {
//                                        $active_products = $products_group_status;
//                                    }
//                                }
//
//                                if (count($active_products) == 1) {
//                                    $active_barcode[] = $non_active_product->sku;
//                                    $active_ids[] = $non_active_product->id;
//                                } elseif (!$active_products and count($non_active_products) >= 2) {
//                                    foreach ($non_active_products as $key => $non_product) {
//                                        if ($key <= 1) {
//                                            $active_barcode[] = $non_product->sku;
//                                            $active_ids[] = $non_product->id;
//                                        }
//                                    }
//                                }
//                            }else{
//                                foreach($products as $product){
//                                    if($product->status_id == 6){
//                                        $active_barcode[] = $non_product->sku;
//                                        $active_ids[] = $non_product->id;
//                                    }
//                                }
//                            }
//                        }
//
//                        if (count($active_barcode) and count($active_ids)) {
//                            ProductRepository::active($active_barcode, 'sku');
//                            WarehouseProduct::whereIn('id', $active_ids)->update(['status_id' => 9]);
//                            $products = ProductRepository::getProductWithRelations($active_barcode, ['motors', 'categories', 'categories.mptt', 'manufacturer', 'productDescription', 'points', 'prices', 'images']);
//                            foreach ($products as $product) {
//                                $solrService->updateDocumentsByProduct($product);
//                            }
//                        }
//                    }
                }
            }
        }
    }
}