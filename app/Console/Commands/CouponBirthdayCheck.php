<?php

namespace App\Console\Commands;

use Everglory\Models\Coupon;
use Everglory\Models\Customer;
use Illuminate\Console\Command;

class CouponBirthdayCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupon:birthday-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check birthday coupon';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '2048M');

        echo 'START:' . date('Y-m-d H:i:s') . "\r\n";

        $this_year = date('Y');
        $this_month = (int)date('m');
        $last_year = date('Y', strtotime('last month'));
        $last_month = (int)date('m', strtotime('last month'));
        //dd($this_year.'/'.$this_month.' ; '.$last_year.'/'.$last_month);
//        dd(get_defined_vars());

        //已發送coupon
        $c = Coupon::where('code', 'BIRTHDAY-COUPON')
            ->whereIn(\DB::raw("DATE_FORMAT(created_at,'%Y%c')"), [$last_year . $last_month, $this_year . $this_month])
            ->get();
//        dd($c->count());

        //本月壽星
        $customers = Customer::with('address')
            ->where(\DB::raw('MONTH(birthday)'), $this_month)
            ->whereNotIn('id', $c->pluck('customer_id'))
            ->get();
//        dd($customers);

        //上月壽星
        $last_customers = Customer::with('address')
            ->where(\DB::raw('MONTH(birthday)'), $last_month)
            ->whereNotIn('id', $c->pluck('customer_id'))
            ->get();

        \Log::info($this_month."月壽星未發送數: ".$customers->count().', customer_id: '.implode(',',$customers->pluck('id')->all()));
        \Log::info($last_month."月壽星未發送數: ".$last_customers->count().', customer_id: '.implode(',',$last_customers->pluck('id')->all()));

        echo 'Finish:' . date('Y-m-d H:i:s') . "\r\n";
    }
}
