<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\NewsletterController;
use Ecommerce\Core\Wmail;
use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Everglory\Models\Email\Newsletter\Item;
use Everglory\Models\Email\Newsletter\Queue;
use Illuminate\Console\Command;
use Ecommerce\Repository\CouponRepository;
use Everglory\Models\Disp;

class SendNewsLetter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Send:NewsLetter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Newsletter';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        ini_set('memory_limit','2048M');
        set_time_limit(28800);
        $sending_queue = Queue::where('status_id',2)->whereNull('send_end_time')->take(5)->get();
        foreach ($sending_queue as &$queue) {
            $wmail = new Wmail;
            $wmail->newsletter($queue);
            $count_check = $queue->items()->whereNull('sent_at')->count();
            if($count_check == 0){ //send finish
                $queue->status_id = 3;
                $queue->send_end_time = date('Y-m-d H:i:s');
                $queue->save();
            }
        }

        $send_queue = Queue::where('send_start_time','<',\DB::Raw('now()'))->where('status_id',1)->get();
        foreach($send_queue as &$queue){
            if($queue->send_method == 1) {
                $this->setQueueItem($queue->id);
            }

            $campaign = date("Ymd").'_mailmag';
            $para = '?utm_source=webikeMailmagazine&utm_medium=email&utm_content=mailmag&utm_campaign=' . $campaign;
            $dom = new \DOMDocument();
            $dom->loadHTML(mb_convert_encoding($queue->content, 'HTML-ENTITIES', 'UTF-8'));
            $anchors = $dom->getElementsByTagName('a');
            foreach ($anchors as $a) {
                if ($a->hasAttribute('href')) {
                    $href = $a->getAttribute('href');
                    if(false !== strpos($href, 'webike.tw')){
                        if(false !== strpos($href, '#')) {
                            $hrefs = explode('#',$href);
                            $a->setAttribute('href',$hrefs[0] . $para . '#' .  $hrefs[1] );
                        }else if(false !== strpos($href, '?')) {
                            $hrefs = explode('?',$href);
                            $a->setAttribute('href',$hrefs[0] . $para . '&' .  $hrefs[1] );
                        }else{
                            $a->setAttribute('href',$href . $para);
                        }
                    }

                }
            }
            $queue->content_tracking = $dom->saveHTML();

            $queue->status_id = 2;
            $queue->save();
        }
    }

    private function setQueueItem($queue_id, $customer_ids = [])
    {
        if(!is_array($customer_ids)){
            throw new \Exception('This function need to use array in customer_ids case.');
        }
        if(!count($customer_ids)){
            $customer_ids = Customer::select('id')->where('newsletter', 1)->get()->pluck('id')->toArray();
        }
        foreach ($customer_ids as $customer_id) {
            $item = new Item;
            $item->customer_id = $customer_id;
            $item->queue_id = $queue_id;
            $item->save();
        }
    }
}
