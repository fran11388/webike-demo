<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Everglory\Models\Motogp\Table2018\Choice;
use Illuminate\Console\Command;
use Ecommerce\Repository\CouponRepository;
use Everglory\Models\Disp;

class RemindCustomerCoupons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Remind:CustomerCoupons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind customer have coupons';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        \URL::forceRootUrl('https://www.webike.tw');

        if(activeValidate('2018-03-25 23:59:59','2018-04-01')){
//            $customersCoupons = \Ecommerce\Repository\CouponRepository::getTypeCoupons('2017ChristmasFreeShipping' , 'shippingfee')->groupby('customer_id');
//            $customer_all = Customer::get()->pluck('id')->toArray();
            $customer_not_buy_products = Customer\Gift::select('customer_id')->where('status','>',0)->where('status','<',3)->whereNotNull('product_id')->where('date','>','2018-01-01')->groupBy('customer_id')->pluck('customer_id')->toArray();
            $customer_not_use_coupons = Customer\Gift::select('cn.customer_id')->join('coupons as cn','customer_gifts.customer_id','=','cn.customer_id')->whereNull('cn.order_id')->whereNotIn('cn.customer_id',$customer_not_buy_products)->where('code','like','2018ChineseNewYear%')->groupBy('cn.customer_id')->pluck('cn.customer_id')->toArray();
            $customers = array_merge($customer_not_buy_products,$customer_not_use_coupons);
//            $customer_disps = Choice::groupBy('customer_id')->get()->pluck('customer_id')->toArray();
//            $customers = array_diff($customer_all,$customer_disps);
//            $customers = [17987];
//            $customer_disps = Disp::where('info', '您的紅包還未領取')->get()->pluck('customer_id')->toArray();
//            $customers_not_disps = array_diff($customer_all,$customer_disps);
//            $customers_already = Customer\Gift::where('date','=',date('Y-m-d'))->where('status','>',0)->groupBy('customer_id')->get()->pluck('customer_id')->toArray();
//            $customers = array_diff($customers_not_disps,$customers_already);

//            if(strtotime(date('Y-m-d H:i:s')) > strtotime(date('2017-06-02 19:00:00')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-02 19:59:59'))){
//                $info = '全館免運費活動 倒數5小時';
//                $date = '2017-06-02 19:59:59';
//            }elseif(strtotime(date('Y-m-d H:i:s')) >= strtotime(date('2017-06-02 20:00:00')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-02 20:59:59'))){
//                $info = '全館免運費活動 倒數4小時';
//                $date = '2017-06-02 20:59:59';
//            }elseif(strtotime(date('Y-m-d H:i:s')) >= strtotime(date('2017-06-02 21:00:00')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-02 21:59:59'))){
//                $info = '全館免運費活動 倒數3小時';
//                $date = '2017-06-02 21:59:59';
//            }elseif(strtotime(date('Y-m-d H:i:s')) >= strtotime(date('2017-06-02 22:00:00')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-02 22:59:59'))){
//                $info = '全館免運費活動 倒數2小時';
//                $date = '2017-06-02 22:59:59';
//            }elseif(strtotime(date('Y-m-d H:i:s')) >= strtotime(date('2017-06-02 23:00:00')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-02 23:29:59'))){
//                $info = '全館免運費活動 倒數1小時';
//                $date = '2017-06-02 23:29:59';
//            }elseif(strtotime(date('Y-m-d H:i:s')) >= strtotime(date('2017-06-02 23:30:00')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-02 23:59:59'))){
//                $info = '全館免運費活動 倒數30分鐘';
//                $date = '2017-06-02 23:59:59';
//            }

//            foreach($customersCoupons as $customer_id => $coupons){

            $date = date('Y-m-d 23:59:59');
            $route = route('cart');
            if(count($customers)){
                foreach($customers as $customer_id){
//                $info = '88節免運費禮券剩餘'.count($coupons).'張';
                    $info = '您的獎品將於3/31到期';
                    $disps = new Disp;
                    $disps->customer_id = $customer_id;
                    $disps->object_id = 0;
                    $disps->object_type = 'MorphLink';
                    $disps->increment_id = $route;
                    $disps->info = $info;
                    $disps->sort = '99';
                    $disps->expire_at = $date;
                    $disps->save();
                }
            }
        }
    }
}
