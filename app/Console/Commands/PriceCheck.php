<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use Everglory\Models\Staff;
use Illuminate\Console\Command;
use Ecommerce\Repository\CouponRepository;
use Everglory\Models\Disp;

class PriceCheck extends Command
{
    public $staffs = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Price:Check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check everyday price';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $days = [date('Y-m-d'), date('Y-m-d',strtotime(date('Y-m-d').'+ 1 days'))];
        foreach ($days as $date){
            $products = Product::select(\DB::raw('count(products.id) as products_count'))->where('type','=',0)->where('active','=',1)->first();
            $products_price_count = Product::select(\DB::raw('count(products.id) as products_price_count'))
                ->join('product_prices as ppc',function($query) use($date){
                    $query->on('products.id','=','ppc.product_id')
                        ->where('ppc.rule_date','=',$date)
                        ->where('ppc.role_id','=',2);
                })
                ->join('product_prices as ppd',function($query) use($date){
                    $query->on('products.id','=','ppd.product_id')
                        ->where('ppd.rule_date','=',$date)
                        ->where('ppd.role_id','=',3);
                })->where('type','=',0)
                ->where('products.active','=',1)
                ->first();
            $staffs = Staff::where('department_id', 3)->where('id','<>',9)->get();
//        $staffs = Staff::whereIn('id',[4,15])->get();
            if($products_price_count->products_price_count < ($products->products_count*0.8)){
                $title = '遺失'.$date.'前台商品價格資料，請聯絡SG相關人員';
                $subject = '<img src="https://img.webike.tw/shopping/image/webike_tw_logo_new.png"><br><strong>'.$title.'</strong><br>已偵測到前台商品總數'.number_format($products->products_count).'個<br>已設定'.$date.'商品價格'.number_format($products_price_count->products_price_count).'個<br><br>已設定的商品價格數量低於商品總數80%，故寄信通知，請聯絡SG部門。<br><br><div style="text-align:right"><a href="'.\URL::route('parts').'" target="_blank">「Webike-摩托百貨」查看全部商品</a></div>';
                $recipients = $this->initialMembers($staffs);
                $this->sendMail(null,$recipients,$title,$subject);
            }
        }
    }

    private function initialMembers($staffs)
    {
        foreach($staffs as $key => $staff){
            $this->staffs[$key]['address'] = $staff->email;
            $this->staffs[$key]['name'] = $staff->name;
        }

        return $this->staffs;
    }

    private function sendMail($sender,$staffs,$title,$subject)
    {
        $wmail = new \Ecommerce\Core\Wmail();
        foreach($staffs as $staff){
            $recipient = new \StdClass;
            $recipient->address = $staff['address'];
            $recipient->name = $staff['name'];
            $wmail->blank($sender, $recipient, $title, $subject);
        }
    }
}