<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SolrUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solr {update? : true or false }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Solr update';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
//        $url = "http://153.120.83.216:8080/solr/webike/update?wt=json&commitWithin=1000";
        $url = "http://1.34.196.123:8080/solr/test/update?wt=json&commitWithin=300000";
        $points = \DB::connection('eg_product')->select("
        SELECT today.product_id,tomorrow.point,today.role_id FROM `product_points` today
            join `product_points` tomorrow
            on today.rule_date = CURDATE() and tomorrow.rule_date = CURDATE() + INTERVAL 1 DAY and today.role_id = tomorrow.role_id and today.product_id = tomorrow.product_id
            where today.point <> tomorrow.point ;
        ");
        echo 'number:' . count($points);
        if(count($points)){
            $json_string_array = [];
            $start_time = 1000;
            $time = 1;
            foreach ($points as $key => $point){
                $doc = [];
                $doc['id'] = $point->product_id;
                if($point->role_id == 3){
                    $doc['point_3'] = ['set'=>$point->point];
                    $doc['point_4'] = ['set'=>$point->point];
                }else{
                    $doc['point_1'] = ['set'=>$point->point];
                    $doc['point_2'] = ['set'=>$point->point];
                }
                if($key % 1000 == 0){
                    $time++;
                }

                //$json_string_array[] = '"add":' . json_encode(['doc'=>$doc,'commitWithin'=> $start_time + $time * 5000 ] );
                $json_string_array[] = '"add":' . json_encode(['doc'=>$doc ] );
            }
            if(count($json_string_array)){
                $data = "{".implode(',',$json_string_array)."}";

                $ch     = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data))
                );
                $result = curl_exec($ch);
                curl_close($ch);
                $result = json_decode($result);
                dd($result);
            }
        }
    }


}
