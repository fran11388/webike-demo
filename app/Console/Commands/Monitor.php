<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Illuminate\Console\Command;

class Monitor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor {update? : true or false }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitor product';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $update = strtolower($this->argument('update')) == 'true';

        $this->checkMotorProductQuantity($update);
        $this->checkCategoryProductQuantity($update);
        $this->checkManufacturerProductQuantity($update);
        $this->checkProductRelationTableQuantity($update);
        $this->report();

    }

    private function report()
    {
        $content = '';
        $mail_header = '<table border="5"><thead><tr><th style="width:50px;">ID</th><th style="width:250px;">url_rewrite</th><th style="width:300px;">名稱</th><th style="width:60px;">差異</th><th style="width:60px;">目前數量</th><th style="width:60px;">成長率</th></tr></thead><tbody>';
        $mail_end = '</tbody></table>';

        $motors = \DB::connection('eg_product')->select('SELECT mc.id,url_rewrite,`name`,count - prev_count as diff ,count ,CONCAT(round((count - prev_count) / prev_count * 100 ,2) ,\'%\') as rate FROM `monitor_motors` mc
        join motors mptt
        on mc.id = mptt.id
        where (count - prev_count)  / prev_count  < -0.10;
        ');
        if(count($motors)){
            $content .= '<h3>車型</h3>';
            $content .= $mail_header;
            foreach ( $motors as $motor ) {
                $content .= '<tr>';
                $content .= '<td>'.$motor->id.'</td><td>'.$motor->url_rewrite.'</td><td>'.$motor->name.'</td><td>'.$motor->diff.'</td><td>'.$motor->count.'</td><td>'.$motor->rate.'</td>';
                $content .= '</tr>';
            }
            $content .= $mail_end;
        }

        $categories = \DB::connection('eg_product')->select('SELECT mc.id,url_path,`name`,count - prev_count as diff ,count ,CONCAT(round((count - prev_count) / prev_count * 100 ,2) ,\'%\') as rate FROM `monitor_categories` mc
        join mptt_categories_flat mptt
        on mc.id = mptt.id
        where n_right - n_left = 1 and (count - prev_count)  / prev_count  < -0.10;
        ');
        if(count($categories)){
            $content .= '<h3>分類</h3>';
            $content .= $mail_header;
            foreach ( $categories as $category ) {
                $content .= '<tr>';
                $content .= '<td>'.$category->id.'</td><td>'.$category->url_path.'</td><td>'.$category->name.'</td><td>'.$category->diff.'</td><td>'.$category->count.'</td><td>'.$category->rate.'</td>';
                $content .= '</tr>';
            }
            $content .= $mail_end;
        }


       $manufacturers = \DB::connection('eg_product')->select('SELECT mc.id,url_rewrite,`name`,count - prev_count as diff ,count ,CONCAT(round((count - prev_count) / prev_count * 100 ,2) ,\'%\') as rate FROM `monitor_manufacturers` mc
        join manufacturers mptt
        on mc.id = mptt.id
        where (count - prev_count)  / prev_count  < -0.10;
        ');
        if(count($manufacturers)){
            $content .= '<h3>品牌</h3>';
            $content .= $mail_header;
            foreach ( $manufacturers as $manufacturer ) {
                $content .= '<tr>';
                $content .= '<td>'.$manufacturer->id.'</td><td>'.$manufacturer->url_rewrite.'</td><td>'.$manufacturer->name.'</td><td>'.$manufacturer->diff.'</td><td>'.$manufacturer->count.'</td><td>'.$manufacturer->rate.'</td>';
                $content .= '</tr>';
            }
            $content .= $mail_end;
        }

        $tables = \DB::connection('eg_product')->select('SELECT id,`table`,`name`,count - prev_count as diff ,count ,CONCAT(round((count - prev_count) / prev_count * 100 ,2) ,\'%\') as rate FROM `monitor_product_relations`
         where (count - prev_count)  / prev_count  < -0.05;
        ');

        if(count($tables)){
            $content .= '<h3>商品關聯表</h3>';
            $content .= $mail_header;
            foreach ( $tables as $table ) {
                $content .= '<tr>';
                $content .= '<td>'.$table->id.'</td><td>'.$table->table.'</td><td>'.$table->name.'</td><td>'.$table->diff.'</td><td>'.$table->count.'</td><td>'.$table->rate.'</td>';
                $content .= '</tr>';
            }
            $content .= $mail_end;
        }

        if($content) {
            $recipient = new \StdClass();
            $recipient->address = 'timo_chiang@everglory.asia';
            $recipient->name = 'EG-MD';
            $wmail = new \Ecommerce\Core\Wmail();
            $wmail->blank(null, $recipient, '[EG-MD]商品異常檢查報告: '.date('Y-m-d'), $content);
        }
    }

    private function checkMotorProductQuantity($full_update = true)
    {
        echo "Start checkMotorProductQuantity \r\n";
        if($full_update){
            \DB::connection('eg_product')->statement('UPDATE monitor_motors set prev_count = count,count = NULL;');

        }else{
            \DB::connection('eg_product')->statement('UPDATE monitor_motors set count = NULL;');
        }
        \DB::connection('eg_product')->statement('
            INSERT INTO `monitor_motors` (id,count)
            SELECT * FROM (SELECT tb.motor_id,count(*) as ct from product_motor tb
            join products p 
            on tb.product_id = p.id
            WHERE p.is_main = 1 and p.type = 0 and p.active = 1
            GROUP BY tb.motor_id
            ) as g
            ON DUPLICATE KEY UPDATE count = g.ct;');
        \DB::connection('eg_product')->statement('UPDATE monitor_motors set count = 0 where count is NULL;');
    }

    private function checkCategoryProductQuantity($full_update = true)
    {
        echo "Start checkCategoryProductQuantity \r\n";
        if($full_update){
            \DB::connection('eg_product')->statement('UPDATE monitor_categories set prev_count = count,count = NULL;');

        }else{
            \DB::connection('eg_product')->statement('UPDATE monitor_categories set count = NULL;');
        }
        \DB::connection('eg_product')->statement('
                INSERT INTO `monitor_categories` (id,count)
                SELECT * FROM (SELECT tb.category_id,count(*) as ct from category_product tb
            join products p 
            on tb.product_id = p.id
						join mptt_categories_flat mptt
						on tb.category_id = mptt.id
            WHERE p.is_main = 1 and p.type = 0 and p.active = 1 and mptt._active = 1
            GROUP BY tb.category_id) as g
                ON DUPLICATE KEY UPDATE count = g.ct;');
        \DB::connection('eg_product')->statement('UPDATE monitor_categories set count = 0 where count is NULL;');
    }

    private function checkManufacturerProductQuantity($full_update = true)
    {
        echo "Start checkManufacturerProductQuantity \r\n";
        if($full_update){
            \DB::connection('eg_product')->statement('UPDATE monitor_manufacturers set prev_count = count,count = NULL;');

        }else{
            \DB::connection('eg_product')->statement('UPDATE monitor_manufacturers set count = NULL;');
        }
        \DB::connection('eg_product')->statement('
                INSERT INTO `monitor_manufacturers` (id,count)
                SELECT * FROM (SELECT tb.id,count(*) as ct from manufacturers tb
            join products p 
            on tb.id = p.manufacturer_id
            WHERE p.is_main = 1 and p.type = 0 and p.active = 1 and tb.active = 1
            GROUP BY tb.id) as g
                ON DUPLICATE KEY UPDATE count = g.ct;');
        \DB::connection('eg_product')->statement('UPDATE monitor_manufacturers set count = 0 where count is NULL;');
    }

    private function checkProductRelationTableQuantity($full_update = true)
    {
        if($full_update) {
            \DB::connection('eg_product')->statement('UPDATE monitor_product_relations set prev_count = count,count = NULL;');
        }else{
            \DB::connection('eg_product')->statement('UPDATE monitor_product_relations set count = NULL;');
        }

        $tables = \DB::connection('eg_product')->select('SELECT `table`,`count_column` from monitor_product_relations;');
        foreach ( $tables as $table ) {
            if($table->table and $table->count_column){
                echo "Start check $table->table \r\n";
                $relations = \DB::connection('eg_product')->select('SELECT count('.$table->count_column.') as count from '.$table->table.';');
                foreach ( $relations as $relation ) {
                    \DB::connection('eg_product')->statement('UPDATE monitor_product_relations set count = '.$relation->count.' where `table` = "'.$table->table.'";');
                }
            }
        }
        \DB::connection('eg_product')->statement('UPDATE monitor_product_relations set count = 0 where count is NULL;');
    }
}
