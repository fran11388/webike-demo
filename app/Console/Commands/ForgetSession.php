<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ForgetSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Forget:Session';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Forget session every hour';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        \Session::forget('bannerbar-read');
        echo 'forget bannerbar-read Session' .date('Y-m-d H:m:s');
    }
}
