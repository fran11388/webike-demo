<?php

namespace App\Console\Commands;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Illuminate\Console\Command;

class CronMidnight extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Cron:Midnight';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run script in Midnight (01:00)';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        // canendar for holiday
        $dt = \Carbon\Carbon::now();
        if($dt->dayOfWeek == 6){
            $day = 7 * 41;
            $holiday = \Everglory\Models\Calendar::find(1);
            $canendar = new \Everglory\Models\Calendar;
            $canendar->title = $holiday->title;
            $canendar->description = $holiday->description;
            $canendar->className = $holiday->className;
            $canendar->allday = $holiday->allday;
            $canendar->event_id = $holiday->event_id;
            $canendar->start = date('Y-m-d',strtotime($dt->toDateTimeString() . "+".$day." days"));
            $dt->addDays(2);
            $canendar->end = date('Y-m-d',strtotime($dt->toDateTimeString() . "+".$day." days"));
            $canendar->save();
        }

        // check Genuinepart active = 0
        \Everglory\Models\Product::whereRaw('updated_at <  subdate(current_date, 15)')->where('type','1')->where('active','1')->update(['active'=>'0']);
    }
}
