<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ecommerce\Service\FeedService;
use Carbon\Carbon;

class Feed extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $signature = 'feed {--function=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update feed data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 28800);
        
        /*
         * Weekly Reindex New Product for feed source(running after new products finish
         */
        if ($this->option('function') == 'weeklyReindexNewProduct'){
            //php artisan feed --function=weeklyReindexNewProduct
            $feeder = new \Ecommerce\Core\Feed\Shopping\NewProduct();
            $feeder->import();
        }


        /*
         * Daily Reindex Popular Motor for feed source(running at 2 pm )
         */
        if ($this->option('function') == 'dailyReindexPopularMotor'){
            // php artisan feed --function=dailyReindexPopularMotor
            $feeder = new \Ecommerce\Core\Feed\Motomarket\Popular();
            $feeder->import();
        }

        /*
         * Recover News data by script
         */
        if ($this->option('function') == 'RecoverNews'){
            // php artisan feed --function=RecoverNews
            $feeder = new \Ecommerce\Core\Feed\Bikenews\News();
            $feeder->import();
        }


        if ($this->option('function') == 'UpdateScheduleNews') {
            $service = new FeedService;
            $service->UpdateScheduleNews();

        }

        if ($this->option('function') == 'RecoverReview') {
            $feeder = new \Ecommerce\Core\Feed\Shopping\NewReview();
            $feeder->import();
        }

        exit();
    }

    private function isExecute($now ,$base = 4 ){
        if(rand(1,( $base - floor( $now /  15 ))) == 1){
            return true;
        }else{
            return false;
        }
    }

}
