<?php

namespace App\Console\Commands;

use Ecommerce\Core\Wmail;
use Everglory\Models\Coupon;
use Everglory\Models\Coupon\Base;
use Everglory\Models\Coupon\History;
use Everglory\Models\Customer;
use Illuminate\Console\Command;
use Uuid;


class CouponPromotion extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $name = 'Coupon:Promotion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Coupon Promotion send';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        if(date('Y-m-d') == "2018-12-24"){
            $customers = Customer::select('id')->get();
            $CouponBase = Base::where('code','=','2018ChristmasFreeShipping')->first();
            $CouponBase_id = $CouponBase->id;
            $expired_at = "2018-12-26 00:00:00";
            foreach ($customers as $customer) {
                $this->assign($CouponBase_id,$customer,$expired_at,1);
            }
        }
    }

    private function assign($coupon_base_id,$customer = null ,$time = null,$quantity = 1)
    {
        if(!$customer){
            $customer = $this->customer;
        }
        if(!$time){
            $time = $this->unlimit_time;
        }
        $base = Base::findOrFail($coupon_base_id);
        for ($i = 0; $i < $quantity; $i++) {
            $history = new History;
            $history->base_id = $base->id;
            $history->tag = date('Y') . '-' . (int)date('m');
            $history->name = $base->name;
            $history->customer_ids = $customer->id;
            $history->discount = $base->discount;
            $history->time_limit = $base->time_limit;
            $history->time_limit_unit = $base->time_limit_unit;
            $history->save();

            $coupon = new Coupon();
            $coupon->customer_id = $customer->id;
//            $coupon->base_id = $base->id;
            $coupon->uuid = (string)Uuid::generate(1);
            $coupon->name = $base->name;
            $coupon->code = $base->code;
            $coupon->discount = $base->discount;
            $coupon->discount_type = $base->discount_type;
            $coupon->history_id = $history->id;
            $coupon->expired_at = $time;
            $coupon->save();
        }
    }
}