<?php

namespace App\Console\Commands;

use Ecommerce\Core\Wmail;
use Everglory\Models\Coupon;
use Everglory\Models\Coupon\Base;
use Everglory\Models\Coupon\History;
use Everglory\Models\Customer;
use Everglory\Models\Disp;
use Illuminate\Console\Command;


class CouponBirthday extends Command
{
    /**
     * The name and signature of the consolecommand.
     *
     * @var string
     */
    protected $name = 'coupon:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Coupon Birthday send';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '2048M');

        echo 'START:' . date('Y-m-d H:i:s') . "\r\n";

        $this_year = date('Y');
        $this_month = (int)date('m');
        $last_year = date('Y', strtotime('last month'));
        $last_month = (int)date('m', strtotime('last month'));
        //dd($this_year.'/'.$this_month.' ; '.$last_year.'/'.$last_month);
//        dd(get_defined_vars());

        //已發送coupon
        $c = Coupon::where('code', 'BIRTHDAY-COUPON')
            ->whereIn(\DB::raw("DATE_FORMAT(created_at,'%Y%c')"), [$last_year . $last_month, $this_year . $this_month])
            ->get();
//        dd($c->count());

        //本月壽星
        $customers = Customer::with('address')
            ->where(\DB::raw('MONTH(birthday)'), $this_month)
            ->whereNotIn('id', $c->pluck('customer_id'))
            ->get();
//        dd($customers);

        //上月壽星
        $last_customers = Customer::with('address')
            ->where(\DB::raw('MONTH(birthday)'), $last_month)
            ->whereNotIn('id', $c->pluck('customer_id'))
            ->get();
//        dd($last_customers->count());


        //本月壽星
        if (count($customers)) {
            $tag = $this_year . '-' . $this_month;
            $thisMonth_history = $this->saveCouponHistory($customers->pluck('id'), $tag);

            $this->giveCouponProgram($customers, $thisMonth_history, $this_month);
            $this->setDisp($customers->pluck('id'));
        }

        //上月壽星
        if (count($last_customers)) {
            $tag = $last_year . '-' . $last_month;
            $lastMonth_history = $this->saveCouponHistory($last_customers->pluck('id'), $tag);

            $this->giveCouponProgram($last_customers, $lastMonth_history, $last_month);
            $this->setDisp($last_customers->pluck('id'));
        }

        echo 'Finish:' . date('Y-m-d H:i:s') . "\r\n";
    }

    private function saveCouponHistory($customer_ids, $tag)
    {

        $couponBase = Base::where('id', 1)->first();
        $couponHistory = new History();
        $couponHistory->base_id = $couponBase->id;
        $couponHistory->tag = $tag;
        $couponHistory->name = $couponBase->name;
        if (count($customer_ids) == 1) {
            $couponHistory->customer_ids = $customer_ids->first();
        } else {
            $couponHistory->customer_ids = implode(',', $customer_ids->all());
        }
        $couponHistory->discount = $couponBase->discount;
        $couponHistory->time_limit = $couponBase->time_limit;
        $couponHistory->time_limit_unit = $couponBase->time_limit_unit;
        $couponHistory->save();

        return $couponHistory;
    }

    private function giveCouponProgram($customers, $couponHistory, $month)
    {
        $couponBase = Base::where('id', 1)->first();

        \DB::transaction(function()use($customers,$couponBase,$couponHistory){
            foreach ($customers as $customer) {
                $coupon = new Coupon();
                $coupon->customer_id = $customer->id;
                $coupon->uuid = (string)\Uuid::generate(1);
                $coupon->name = $couponBase->name;
                $coupon->code = $couponBase->code;
                $coupon->discount = $couponBase->discount;
                $coupon->history_id = $couponHistory->id;
                $coupon->discount_type = $couponBase->discount_type;

                $dt = new \DateTime();
                // $dt->add(\DateInterval::createFromDateString('+100 years'));
                $dt->add(\DateInterval::createFromDateString('+' . $couponBase->time_limit . ' ' . $couponBase->time_limit_unit));
                $coupon->expired_at = $dt->format('Y-m-d');
                $coupon->save();

            }
        });

        if (!empty($couponBase->mail_template_code)) {
            foreach($customers as $customer){
                $wmail = new Wmail();
                $wmail->coupon($couponBase, $customer);
            }
        }


        $customer_mobiles = '';
        foreach ($customers as $customer) {
            $addresses = $customer->address->where('is_default', 1);
            if (count($addresses) > 0) $customer_mobiles .= $addresses->first()->mobile . ',';

        }

//        \Log::debug('send mobiles: '.$customer_mobiles.' '.$month.' month');
        $message = '親愛的「Webike-摩托百貨」會員您好，恭喜您為' . $month . '月壽星，您的200元生日禮券已經匯入，請至購物車頁面確認 webike.tw/cart';
        $result = $this->callAPI('POST', config('api.sms'), [
            'mobiles' => $customer_mobiles,
            'message' => $message
        ]);
        if (json_decode($result)->status !== 'success') {
            \Log::critical('SMS send fail, mobile:' . $customer_mobiles . ' message: ' . $message);
        }

    }

    private function setDisp($customer_ids)
    {
        \DB::transaction(function()use($customer_ids){
            foreach ($customer_ids as $customer_id) {
                $coupon = Coupon::where('code', '=', 'BIRTHDAY-COUPON')
                    ->where('discount_type', '=', 'product')
                    ->whereNull('order_id')
                    ->where('expired_at', '>', date("Y-m-d"))
                    ->first();
                $info = 'You have birthday coupon.';
                $disp = new Disp;
                $disp->customer_id = $customer_id;
                $disp->object_id = 0;
                $disp->object_type = 'MorphLink';
                $disp->increment_id = \URL::route('cart');
                $disp->info = $info;
                $disp->sort = '99';
                $disp->expire_at = $coupon->expired_at;
                $disp->save();
            }
        });

    }

    private function callAPI($method, $url, $data)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
//            'APIKEY: 111111111111111111111',
//            'Content-Type: application/json',
//        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {
            die("Connection Failure");
        }
        curl_close($curl);
        return $result;
    }


}
