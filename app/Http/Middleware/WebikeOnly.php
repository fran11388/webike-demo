<?php

namespace App\Http\Middleware;

use Closure;

class WebikeOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $ip = $request->server->get('REMOTE_ADDR');
        $_SERVER['REMOTE_ADDR'];
//        $x_ip = $request->server->get('HTTP_X_FORWARDED_FOR');

        $white_list = [
            '49.158.20.135', // TFC office
            '122.116.103.32', // TFC office
            '1.34.196.123', // office
            '1.34.149.90',  // Mr.Fu
            '10.0.2.2', //Wayne virtual
            '183.6.117.139',// China office
            '133.242.50.148', //webike3.0
            '133.242.19.37',// eagle
            '153.120.83.199',// eg zero
            '153.120.25.92',
            '59.148.244.220',// hong kong
            '223.197.183.237',// hong kong
            '10.0.0.27', //Frank virtual
        ];

        if (!\Config::get('app.production')) {
            $white_list[] = '127.0.0.1';
        }

        if (!in_array($ip, $white_list)) {
            if (!ip_is_private($request->ip())) {
                app()->abort(404);
            }
        }


        return $next($request);
    }
}
