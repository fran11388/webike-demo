<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            $request->session()->flash('pending_data',$request->all());
            $request->session()->flash('pending_route',$request->route()->getName());
//            $session_key = 'virtual_cart';
//            if(!session()->has($session_key)){
//                session([$session_key => [request()->get('sku') => request()->get('qty')]]);
//            }else{
//                $virtual_cart = session()->get($session_key);
//                if(isset($virtual_cart[request()->get('sku')])){
//                    $virtual_cart[request()->get('sku')] += request()->get('qty');
//                }else{
//                    $virtual_cart[request()->get('sku')] = request()->get('qty');
//                }
//            }
//
//            $session_redirect_key = 'register-complete-redirect-route';
//            session([$session_redirect_key => 'cart']);
            return redirect()->route('login');
        }
        return $next($request);
    }
}
