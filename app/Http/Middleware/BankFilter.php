<?php

namespace App\Http\Middleware;

use Closure;

class BankFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = $request->server->get('REMOTE_ADDR');

        $https = $request->server->get('HTTPS');

        if(($ip == '203.67.45.142')
            or ($ip == '61.220.45.142')
            or ($ip == '153.120.25.92')){
            if ( strtolower( $https ) == "on" ){
        
            }else{
                app()->abort( 404 );
            }
        }else{
            app()->abort( 404 );
        }

        
        return $next($request);
    }
}
