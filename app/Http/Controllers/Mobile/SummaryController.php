<?php
namespace App\Http\Controllers\Mobile;


use App\Components\View\Mobile\SummaryComponent;
use App\Http\Controllers\Controller;
use Barryvdh\Reflection\DocBlock\Type\Collection;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Repository\ReviewRepository;
use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Service\NavigationService;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\SummaryService;
use Everglory\Constants\CustomerRole;
use Everglory\Models\Motor;
use Everglory\Models\Product;
use Illuminate\Http\Request;
use Ecommerce\Service\MotorService;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\ProductService;
use Illuminate\View\View;
use Jenssegers\Agent\Agent;

class SummaryController extends Controller
{
    protected $service;
    private $top;
    private $cache_key;

    public function __construct(SummaryComponent $component)
    {
        parent::__construct($component);

        app()->singleton('EmptyProduct', function () {
            return new Product();
        });

        $segments = request()->segments();
        
        try {
            for ($i = 0; $i < count($segments); $i += 2) {
                $key = $segments[$i];
                $value = $segments[$i + 1];
                switch ($key) {
                    case 'ca':
                        request()->request->add(['category' => $value]);
                        break;
                    case 'br':
                        request()->request->add(['manufacturer' => $value]);
                        break;
                    case 'mt':
                        request()->request->add(['motor' => $value]);
                        break;
                }
            }
        } catch (\Exception $e) {
        }


        $this->cache_key = http_build_query(request()->only(['motor', 'category', 'manufacturer']));

    }
    public function index(Request $request)
    {
        cacheControlProcess(['summary']);
        \Debugbar::startMeasure('render','urlValidate');
        if(!str_contains(request()->getHost(),"amp")){
            $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);

        }

        $service = app()->make(SummaryService::class);
        if($redirect_url = $service->urlValidate($request)){
            return redirect()->to(str_replace('/dev/index.php','',$redirect_url),301);
        }
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','prepare DNA');
        $convertService = app()->make(ConvertService::class);
        /** @var SummaryService $service */

        $this->view_component->current_category = $service->getCategory();
        $this->view_component->current_manufacturer = $service->getManufacturer();
        $this->view_component->current_motor = $service->getMotor();
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','new_product_search_response');

        //新商品(時間)
        $carousel_configs = [
            'new_products'=> (object) ['collection'=>null,'params'=>(object)['key'=>'new_products','call'=>'new_products','blade'=>'d','limit'=>'12','parameter'=>$request->all(),'shuffle'=>'off','page'=> 1 ]],
        ];

        $storage_data = [];
        $carouselService = new \Ecommerce\Service\CarouselService($carousel_configs,$storage_data);
        foreach ($carouselService->call() as $key => $carouselData){
            $this->view_component->$key = $carouselData;
        }
        $new_product_count = isset($storage_data['new_product_count']) ? $storage_data['new_product_count'] : 0;

        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','top_sales_products');

        $top_sales_search_response =  \Cache::tags(['summary'])->remember('summary-top-sales-search-response-'.$this->cache_key, 86400 * 7, function () use($request,$service){
            return $service->selectTopSales($request);
        });
//        $this->view_component->top_sales_products = $convertService->convertQueryResponseToProducts($top_sales_search_response);
        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','hot_manufacturers');
        $this->view_component->manufacturer_info = null;

        if(!$this->view_component->current_manufacturer  ) {
            //熱門品牌
            $this->view_component->hot_manufacturers = $convertService->convertFacetFieldToManufacturers($top_sales_search_response);
        }else{
            $this->view_component->manufacturer_info = $this->view_component->current_manufacturer->info;
        }


        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','search_response');
        $search_response =  \Cache::tags(['summary'])->remember('summary-search-response-'.$this->cache_key, 86400 * 7, function () use($request,$service){
            return $service->selectSummary($request);
        });
        \Debugbar::stopMeasure('render');

//        \Debugbar::startMeasure('render','popular_products');
//        //推薦商品(瀏覽量)
//        $this->view_component->popular_products = $convertService->convertQueryResponseToProducts($search_response);
//        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','current_motor');
        if($this->view_component->current_motor){
            $motorService = new MotorService($this->view_component->current_motor->url_rewrite, ['manufacturer', 'specifications']);
            $this->view_component->genuine_link = $motorService->getGenuineSalesLink();
            $this->view_component->modelInfo = $motorService->getMotorFivePower();

            /***disabled in 2019/04/29 by Jason***/
//            $this->view_component->info = $motorService->getMotorInformation($search_response,$new_product_count);

            $this->view_component->addMyBikesDisabled = false;
            if($this->view_component->current_customer){
                $myBikeIds = $this->view_component->current_customer->motors->pluck('id')->toArray();
                $this->view_component->addMyBikesDisabled = in_array($this->view_component->current_motor->id, $myBikeIds);
            }
        }
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','getSummaryTitle and tree');

        $this->view_component->summary_title = $service->getSummaryTitle();

        $this->view_component->tree =  \Cache::tags(['summary'])->remember('summary-tree-'.$this->cache_key, 86400 * 7, function () use($search_response){
            return NavigationService::getNavigation($search_response);
        });
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','manufacturer');
        $this->view_component->manufacturer = $convertService->convertFacetFieldToManufacturers($search_response);
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','ranking_avg');
        $this->view_component->ranking_avg =  \Cache::tags(['summary','review'])->remember('summary-ranking_avg-'.$this->cache_key, 86400 * 7, function () {
            return ReviewRepository::getAvgRanking(
                $this->view_component->current_category,
                $this->view_component->current_manufacturer,
                $this->view_component->current_motor
            );
        });
        \Debugbar::stopMeasure('render');
//        \Debugbar::startMeasure('render','reviews');
//        $this->view_component->reviews =  \Cache::tags(['summary','review'])->remember('summary-reviews-'.$this->cache_key, 86400 * 7, function () {
//            return ReviewRepository::get($this->view_component->current_category, $this->view_component->current_manufacturer, $this->view_component->current_motor , 8);
//        });
//        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','subcategory_view');

        $this->view_component->subcategory_view =  \Cache::tags(['mobile-summary'])->remember('mobile-summary-subcategory_view-'.$this->cache_key, 86400 * 7, function (){
            return view('mobile.pages.summary.partials.subcategory',(array)$this->view_component)->render();
        });

        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','ranking_avg');
        $this->view_component->ranking_avg =  \Cache::tags(['summary','review'])->remember('summary-ranking_avg-'.$this->cache_key, 86400 * 7, function () {
            return ReviewRepository::getAvgRanking(
                $this->view_component->current_category,
                $this->view_component->current_manufacturer,
                $this->view_component->current_motor
            );
        });
        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','rankingPrepare');

        $search_service = app()->make(SearchService::class);

        $this->view_component->ranking_sales_products =  \Cache::tags(['ranking'])->remember('ranking-sales-'.$this->cache_key, 86400 * 7, function () use($request,$search_service,$convertService){

            $ranking_response_sales = $search_service->selectRanking($request);
            return $convertService->convertQueryResponseToProducts($ranking_response_sales);
        });

        request()->request->add(['type' => 'popularity' ]);

        $this->view_component->ranking_popularity_products =  \Cache::tags(['ranking'])->remember('ranking-popularity-'.$this->cache_key, 86400 * 7, function () use($request,$search_service,$convertService){
            $ranking_response_popularity = $search_service->selectRanking($request);
            return $convertService->convertQueryResponseToProducts($ranking_response_popularity);
        });
        
        $this->view_component->url_path = $request->path();

        request()->request->remove('popularity');
        \Debugbar::stopMeasure('render');

        $this->view_component->setBreadcrumbs([
            'mt' => $this->view_component->current_motor,
            'ca' => $this->view_component->current_category,
            'br' => $this->view_component->current_manufacturer,
        ]);
//        $this->view_component->interface = ['rules', 'motors', 'brands', 'sort', 'limit', 'products'];
        $this->view_component->render();
        
        if(str_contains(request()->getHost(),"amp")){
            return view('mobile.pages.amp.summary.index-amp',  (array)$this->view_component);
        }else{
            return view('mobile.pages.summary.index',  (array)$this->view_component);
        }
    }

}