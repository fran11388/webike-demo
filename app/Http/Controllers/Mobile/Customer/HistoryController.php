<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/20
 * Time: 下午 03:49
 */

namespace App\Http\Controllers\Mobile\Customer;


use App\Http\Controllers\Controller;
use Ecommerce\Service\GenuinepartsService;
use Ecommerce\Service\HistoryService;
class HistoryController extends Controller
{
    public function purchasedHistory(HistoryService $historyService)
    {
        ini_set("memory_limit", "2048M");

        $collection = $historyService->getOrdersWithPaginate();
        $this->view_component->setCollection($collection, true);
        $this->view_component->title_text = '購入商品履歷';
        $this->view_component->setBreadcrumbs('購入商品履歷');
        $this->view_component->seo('title', '購入商品履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        $manufactures = GenuinepartsService::getManufacturers();
        $this->view_component->manufacture_suppliers = $manufactures->pluck('supplier_id', 'manufacturer_id')->all();

        $customer_id = \Auth::user()->id;
        $customer_id = 48013;

        $items=HistoryService::getMystyleOrderItems($customer_id);

        $this->view_component->collection = $items;
        $this->view_component->sku_grouped_order_items = HistoryService::getMystyleGroupedOrderItems($items);
        $this->view_component->manufacturers = HistoryService::itemsExtractManufactures($customer_id);
        $this->view_component->first_layer_categories = HistoryService::itemsExtractFirstLayerCategories($customer_id);
        $this->view_component->sortingWays = HistoryService::getMyStyleSortingWay();

//        // 桌機板購買履歷
//        return view('response.pages.customer.history.product-history', (array)$this->view_component);

dd(696969);
//        手機板購買履歷
        return view('mobile.pages.customer.history.product-history', (array)$this->view_component);


    }
}