<?php
namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Ecommerce\Service\EstimateService;
use App\Components\View\SimpleComponent;
use Ecommerce\Core\Auth\Hasher;
use Ecommerce\Service\GroupBuyService;
use Auth;

class GroupBuyController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('groupbuy');
        $component->setBreadcrumbs('團購報價系統',$component->base_url);
    }

    public function index()
    {
        $this->view_component->products = GroupBuyService::getRequestProduct();
        $this->view_component->seo('title','團購報價系統');
        $this->view_component->seo('description','團購報價系統');
        $this->view_component->seo('keyword','團購報價系統');
        $this->view_component->render();
        return view('mobile.pages.groupbuy.index', (array)$this->view_component);
    }

}

?>