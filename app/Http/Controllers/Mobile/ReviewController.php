<?php 

namespace App\Http\Controllers\Mobile;

use App\Components\View\ReviewComponent;
use App\Http\Controllers\Controller;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ReviewRepository;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\MotorRepository;
use Everglory\Constants\SEO;

class ReviewController extends Controller
{
	public function __construct(ReviewComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('review');
        app()->singleton('EmptyProduct', function () {
            return new Product();
        });
    }

    public function index()
    {
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);

        $this->view_component->mostReviewProducts = convertCollectionToProductAccess(ProductRepository::getMostReviewProduct(180,3));
        $this->view_component->mostCommentReview = ReviewRepository::getMostCommentReviews(180,3);
        $this->view_component->customReviewObject = ReviewRepository::getCategoryReviewCount('1000');
        $this->view_component->riderReviewObject = ReviewRepository::getCategoryReviewCount('3000');
        $this->view_component->genuineReviewObject = ReviewRepository::getGenuinePartsReviewCount();
        
        $this->view_component->newReviews = ReviewRepository::getNewReviews(8);
        $this->view_component->newComments = ReviewRepository::getNewReviewComment(5);

        $this->view_component->seo('title', '進口重機、機車商品評論、車友討論區');
        $this->view_component->seo('description', '進口重機、機車改裝零件、正廠零件、人身部品，車友使用心得，商品討論區');
        $this->view_component->render();
        return view('mobile.pages.review.index', $this->view_component->output());

    }

    public function create($url_rewrite)
    {
        if($this->view_component->current_customer->role_id == \Everglory\Constants\CustomerRole::REGISTERED){
            return redirect()->route('customer-account-complete');
        }
        $product = ProductRepository::getMainProduct($url_rewrite,['manufacturer', 'images']);
        if($product){
            $this->view_component->product = new ProductAccessor($product, false);
            return view('mobile.pages.review.create', (array)$this->view_component);
        }else{
            app()->abort(404);
        }

    }

    public function show($id)
    {
        $review = ReviewRepository::find($id);
        if(!$review){
            return abort(404);
        }

        $this->view_component->reviews = ReviewRepository::searchByProductId($review->product_id)->where('id', '!=', $review->id);
        $review->product = ($review->product ? new ProductAccessor($review->product, false) : null);

        $this->view_component->review = $review;
        $this->view_component->motor = null;
        $reviewMotor = $review->motors->first(function($motor){
            return $motor->customer_selected_flg;
        });
        if($reviewMotor){
            $this->view_component->motor = $reviewMotor->motorModel;
        }
        
        if($review->product_category == 420){
            $this->view_component->seo('title', $review->product_manufacturer . '正廠零件：' . $review->product_name . '商品評論、車友使用心得');
        }else{
            $this->view_component->seo('title', $review->product_manufacturer . '：' . $review->product_name . '商品評論、車友使用心得');
        }
        $this->view_component->seo('description', $review->title . '：' . $review->content);
        return view('mobile.pages.review.article', $this->view_component->output());

    }

    public function writingDone()
    {   
        if(!session()->has('code')){
            return abort(404);
        }
        return view('response.pages.review.done', (array)$this->view_component);
    }

    public function upload()
    {

        $result = new \stdClass();
        $result->reload = false;
        if (!\Auth::check()) {
            $result->success = false;
            $result->message = '請重新登入';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        } else {
            $customer = \Auth::user();
        }


        $img_file = request()->file('photo');
        
        if(!$img_file){
            $result->success = false;
            $result->message = '檔案上傳失敗！請檢查格式是否正確';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }

        if ($img_file->getSize() > 5 * 1024 * 1024) {
            $result->success = false;
            $result->message = '上傳檔案需小於 5 MB';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }
        if (!in_array($img_file->getMimeType(),['image/jpeg','image/png','image/gif'])) {
            $result->success = false;
            $result->message = '只允許上傳 JPEG / PNG / GIF 格式檔案';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }
        $destinationPath = public_path() . '/assets/photos/review/';
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777);
        }
        $size = $img_file->getSize();
        $mimetype = $img_file->getMimeType();
        $resolution = getimagesize($img_file->getRealPath());
        $image_key = getFileKey();
        while (file_exists($destinationPath . $image_key . '.' . $img_file->getClientOriginalExtension())) {
            $image_key = getFileKey();
        }
        $filename_tmp = $image_key . '_tmp.' . $img_file->getClientOriginalExtension();
        $location = $destinationPath . $filename_tmp;
        $img_file->move($destinationPath, $filename_tmp);
        $filename = str_replace('_tmp.' . $img_file->getClientOriginalExtension(), '.' . $img_file->getClientOriginalExtension(), $filename_tmp);
        ImageResize($location, $destinationPath . $filename);

        $result->success = true;
        $result->key = $filename;
        $result = json_encode($result);
        echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
        //unlink($location);
        die();

    }

    public function search($url_rewrite = null)
    {
        $this->view_component->current_motor = MotorRepository::find(request()->input('mt'));
        $search_result = ReviewRepository::search($url_rewrite,$this->view_component->current_motor);
        $this->view_component->reviews = $search_result->reviews;
        $this->view_component->keyword = TransferToUtf8(request()->input('q'));
        $this->view_component->current_product = null;
        if(request()->input('sku')){
            $this->view_component->current_product = ProductRepository::getMainProduct(request()->input('sku'),['manufacturer']);
        }

        $this->view_component->current_brand = request()->input('br');
        $this->view_component->ranking = request()->input('ranking');
        $this->view_component->filters = [
            'mt' => ['label'=>'車型','name'=>$this->view_component->current_motor ? $this->view_component->current_motor->name . ($this->view_component->current_motor->synonym ? '(' . $this->view_component->current_motor->synonym . ')' : '') : ''],
            'br' => ['label'=>'品牌','name'=>$this->view_component->current_brand ? $this->view_component->current_brand : '' ],
            'ca' => ['label'=>'分類','name'=>$this->view_component->category ? $this->view_component->category->name . ($this->view_component->category->synonym ? '(' . $this->view_component->category->synonym . ')' : '') : ''],
            'sku' => ['label'=>'商品','name'=>$this->view_component->current_product ? $this->view_component->current_product->name : ''],
            'q' => ['label'=>'關鍵字','name'=>$this->view_component->keyword],
            'ranking' => ['label'=>'評價','name'=>$this->view_component->ranking],
        ];
        $this->view_component->filter_display = [
        ];

        $meta_filters = [];
        foreach ($this->view_component->filters as $key => $filter){
            if(!in_array($key, ['ranking']) and $filter['name']){
                $meta_filters[] = $filter['name'];
            }
        }
        $this->view_component->meta_filters = implode(' & ', $meta_filters);
                
        if($url_rewrite){
            $this->view_component->seo('title', $this->view_component->meta_filters . '商品評論、車友討論區');
        }else{
            $this->view_component->seo('title', '全部商品評論、車友討論區');
        }
        if($this->view_component->meta_filters){
            $this->view_component->seo('description', '進口重機、機車【' . $this->view_component->meta_filters . '】車友使用心得，商品討論區' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        }else{
            $this->view_component->seo('description', '進口重機、機車改裝零件、正廠零件、人身部品，車友使用心得，商品討論區。' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        }

        $this->view_component->render();

        return view('mobile.pages.review.search', $this->view_component->output());
    }
}


























?>