<?php

namespace App\Http\Controllers\Mobile;


use App\Http\Controllers\Controller;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use URL;
use Illuminate\Support\Collection;
use App\Components\View\SimpleComponent;
use Everglory\Constants\EstimateType;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;
use Ecommerce\Support\LengthAwarePaginator;
use Ecommerce\Service\AccountService;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Service\HistoryService;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\CartService;
use Ecommerce\Service\WishlistService;
use Ecommerce\Service\ContactService;
use Ecommerce\Service\GiftService;

class CustomerController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心', $component->base_url);
    }

    public function getPointinfoAmp()
    {
        $breadcrumbs = '現金點數說明';
        $page = 'mobile.pages.customer.rule.amppointinfo';
        $title = '現金點數說明';
        $description = 'Webike會員專屬現金集點活動，一點可扣抵一元，集越多省越多';
        $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
        \Debugbar::disable();

        $this->view_component->customer = \Auth::user();
        $this->view_component->setBreadcrumbs($breadcrumbs);
        $this->view_component->seo('title', $title);
        $this->view_component->seo('description',$description);
        $this->view_component->seo('keyword', $keywords);
        $this->view_component->render();

        return view($page, (array)$this->view_component);
     }               
    
}