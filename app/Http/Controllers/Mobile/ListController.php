<?php
namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Components\View\ListComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Ecommerce\Service\NavigationService;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\SupplierService;
use Ecommerce\Support\PriceConverter;
use Everglory\Models\Product;
use Illuminate\Http\Request;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\Search\ConvertService;

class ListController extends Controller
{
    protected $service;
    protected $skus = [];

    public function __construct(ListComponent $component)
    {
        parent::__construct($component);

        app()->singleton('EmptyProduct', function () {
            return new Product();
        });

        $segments = request()->segments();
        for ($i = 1; $i < count($segments) ; $i += 2) {
            $key = $segments[$i];

            if(!isset($segments[$i + 1])){
                app()->abort(404);
            }
            $value = $segments[$i + 1];
            switch ($key) {
                case 'ca':
                    request()->request->add(['category' => $value ]);
                    break;
                case 'br':
                    request()->request->add(['manufacturer' => $value ]);
                    break;
                case 'mt':
                    request()->request->add(['motor' => $value ]);
                    break;
            }

        }
    }

    public function index(Request $request)
    {
        
        $service = app()->make(SearchService::class);

        // recent view


        if(!$service->urlValidate($request)){
            return view('mobile.pages.list.no-result', (array)$this->view_component);
        }

        $search_response = $service->selectList($request);
        $this->view_component->no_result = false;

        $this->view_component->manufacturer = $this->convertFacetFieldToManufacturers($search_response);
        $this->view_component->count = $search_response->getQueryResponse()->getCount();

        $this->view_component->tree = NavigationService::getNavigation($search_response);


        $this->view_component->current_category = $service->getCategory();
        $this->view_component->current_manufacturer = $service->getManufacturer();
        $this->view_component->current_motor = $service->getMotor();


        $this->view_component->products = $this->convertQueryResponseToProducts($search_response);
        $skus = [];
        $this->view_component->products->map(function($productAccessor) use(&$skus){
            $skus[] = $productAccessor->sku;
        });
//        $this->view_component->stock_informations = ProductService::getStockFlgForList($skus);
//        $domesticResult = SupplierService::isDomesticSupplier($this->skus, 'sku');
//        $rcjSkus = [];
//        foreach ($domesticResult as $sku => $isDomesticSupplier){
//            if(!$isDomesticSupplier){
//                $rcjSkus[] = $sku;
//            }
//        }
//        $this->view_component->tagData['jp_stocks'] = ProductService::getJpStockStatus($rcjSkus);
        $this->view_component->price_range = $this->convertFacetRangeResponseToPrices($search_response);
        $this->view_component->countries = $this->convertFacetRangeResponseToCountry($search_response);


        $this->view_component->category = $service->getCategory();
        $this->view_component->genuine_link = null;

        $this->view_component->setBreadcrumbs();
        /*
        $this->view_component->setBreadcrumbs([
            'category' => $service->getCategory(),
            'manufacturer' => $service->getManufacturer(),
            'motor' => $service->getMotor(),
            'keyword' => $service->getKeyword(),
        ]);
        */

//        $this->view_component->setBreadcrumbs();
        $this->view_component->products_collection = null;
        $this->view_component->seo();
        $this->view_component->render();
        if (!$this->view_component->count){
            $this->view_component->products_collection = $this->search_split($request, $request->get('q'));
            $this->view_component->no_result = true;
            // return view('mobile.pages.list.no-result', (array)$this->view_component);
        }
        return view('mobile.pages.list.page', (array)$this->view_component);
    }



    public function ranking(Request $request)
    {
        $service = app()->make(SearchService::class);
        $convertService = app()->make(ConvertService::class);
        $search_response = $service->selectRanking($request);


        if($request->get('type') == 'brands') {
            $manufacturers = $convertService->convertQueryResponseToManufacturesRanking($search_response);
            
            if(count($manufacturers)){
                $custom_response = $service->selectFacetByManufacturers($manufacturers->keys()->toArray(),$request,1000);
                $convertService->convertQueryResponseToManufacturesRankingProductCount($manufacturers,$custom_response,'custom');
                $riding_response = $service->selectFacetByManufacturers($manufacturers->keys()->toArray(),$request,3000);
                $convertService->convertQueryResponseToManufacturesRankingProductCount($manufacturers,$riding_response,'riding');
                $convertService->convertQueryResponseToManufacturesRankingResult($manufacturers);
                $this->view_component->manufacturers = $manufacturers;
                $this->view_component->count = count($manufacturers);
            }else{
                $this->view_component->count = 0;
            }
        }else{
            $this->view_component->products = $this->convertQueryResponseToProducts($search_response);
            if($docs = $search_response->getGroupResponse()){
                $this->view_component->count = $docs->getCount();
            }else{
                $this->view_component->count = $search_response->getQueryResponse()->getCount();
            }
            $this->view_component->manufacturer = $this->convertFacetFieldToManufacturers($search_response);
        }




        $this->view_component->tree = NavigationService::getNavigation($search_response);


        $this->view_component->current_category = $service->getCategory();
        $this->view_component->current_manufacturer = $service->getManufacturer();
        $this->view_component->current_motor = $service->getMotor();
        $this->view_component->title = $service->getTitle();



        $this->view_component->tagData['jp_stocks'] = ProductService::getJpStockStatus($this->skus);
        $this->view_component->price_range = $this->convertFacetRangeResponseToPrices($search_response);
        $this->view_component->countries = $this->convertFacetRangeResponseToCountry($search_response);


        $this->view_component->category = $service->getCategory();
        $this->view_component->genuine_link = null;

        $this->view_component->setRankingBreadcrumbs();
        $this->view_component->seo();
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $this->view_component->recent_views = ProductService::getRecentViews($this->view_component->current_customer,5);
        $this->view_component->render();
        $this->view_component->interface = ['rules', 'motors', 'brands', 'sort', 'limit', 'products'];
        return view('response.pages.list.ranking.index',  (array)$this->view_component);
    }


    /**
     * @param SearchResponse $search_response
     * @return \Illuminate\Support\Collection
     */
    private function convertFacetFieldToManufacturers($search_response)
    {

        $manufacturer_name_url = $search_response->getFacetFieldResponses()['manufacturer_name_url'];
        $manufacturer_name_url = $manufacturer_name_url->getDocs();

        $result = [];
        foreach ($manufacturer_name_url as $key => $count) {
            $object = new \stdClass();
            list ($object->name, $object->url_rewrite, $object->synonym, $object->image) = explode("@", $key);
            $object->count = $count;
            $result[] = $object;
        }

        return collect($result);
    }

    /**
     * @param SearchResponse $search_response
     * @return \Illuminate\Support\Collection
     */
    private function convertQueryResponseToProducts($search_response , $series = false)
    {
        if($docs =$search_response->getGroupResponse()){
            $docs = $docs->getDocs();
        }else{
            $docs = $search_response->getQueryResponse()->getDocs();
        }

        $result = [];
        foreach ($docs as $doc) {
            $this->skus[] = $doc->sku;
            $result[] = new ProductAccessor($doc);

        }

        if($series){
            return collect($result)->keyBy(function ($item, $key) {

                return $item->relation_product_id;
            });
        }

        return collect($result);
    }


    /**
     * @param SearchResponse $search_response
     * @return \Illuminate\Support\Collection
     */
    private function convertFacetRangeResponseToPrices($search_response)
    {

        $collection = PriceConverter::all();

        foreach ($collection as &$price) {

            foreach ($search_response->getFacetRangeResponses()['price_range']->getCount() as $key => $counts) {

                if ($price->min <= $key and $price->max >= $key) {
                    $price->count += $counts;
                }
            }
        }
        return $collection;

    }

    private function convertFacetRangeResponseToCountry($search_response)
    {
        $result = [];
        $object = new \stdClass();
        $object->name = '進口商品';
        $object->count = $search_response->getFacetQueryResponses()['import']->getCount();
        $object->key = 'import';
        $result[$object->key] = $object;

        $object = new \stdClass();
        $object->name = '國產商品';
        $object->count = $search_response->getFacetQueryResponses()['taiwan']->getCount();
        $object->key = 'taiwan';
        $result[$object->key] = $object;

        return collect($result);
    }

        /**
     * @param $request
     * @param $keyword
     * @return array
     */
    private function search_split( Request $request , $keyword){
        $service = app()->make(SearchService::class);
        $keywords = explode(" ", $keyword);
        $result = [];
        $queries = $request->query->all();
        $url = $request->url();

        foreach ($keywords as $_keyword){
            if ($_keyword){
                $search_response = $service->selectList($request, $_keyword);
                $entity = new \stdClass();
                $entity->products = $this->convertQueryResponseToProducts($search_response);
                $entity->count = $search_response->getQueryResponse()->getCount();
                $queries['q'] = $_keyword;
                $entity->url = $url . '?'. http_build_query($queries);
                $result[$_keyword] = $entity;
            }

        }
        return $result;
    }

}