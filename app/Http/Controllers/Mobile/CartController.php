<?php
namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Components\View\CartComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Service\CartService;
use Ecommerce\Service\AccountService;
use Ecommerce\Service\GiftService;
use Ecommerce\Service\ProductService;
use Ecommerce\Core\TableFactory\src\App\Excel\Service as TableFactory;
use Ecommerce\Shopping\Payment\CoreInstallment;
use Everglory\Models\Coupon;
use Everglory\Constants\SEO;

class CartController extends Controller
{
    /**
     * @var CartService $cartService
     */
    protected $cartService;
    /**
     * @var CartComponent $view_component
     */
    protected $view_component;

    public function __construct(CartComponent $component)
    {
        parent::__construct($component);
        $this->middleware(function ($request, $next) {
            $this->cartService = app()->make(CartService::class);
            return $next($request);
        });

    }

    public function index()
    {
        $giftService = new GiftService();
        $customerGifts = GiftService::getCustomerGiftsByStep('cart');
        $this->view_component->hasGiftInCart = $giftService->hasGiftInCart($customerGifts);
        if(is_null($this->view_component->hasGiftInCart)){
            $customerGifts = GiftService::getCustomerGiftsByStep('checkout');
        }
        $this->view_component->gifts = $giftService->getCustomerGiftsProduct($customerGifts);
        $this->view_component->carts = $this->cartService->getItems();
        $this->view_component->service = $this->cartService;
        $this->view_component->carts_stock_info = $this->cartService->getItemDeliveryInfo($this->view_component->carts);
        $this->view_component->additional_collection = $this->cartService->getAdditionalProducts();
        $this->view_component->coupons = Coupon::whereNull('order_id')
            ->where('customer_id', $this->view_component->current_customer->id)
            ->where('expired_at', '>', date('Y-m-d'))
            ->get();
        if(!$this->cartService->getUsageCoupon()){
            $promotionCoupon = $this->view_component->coupons->first(function($coupon){
                return $coupon->code == '2017ChristmasFreeShipping';
            });
            if ($promotionCoupon){
                $this->cartService->setUsageCoupon($promotionCoupon->uuid);
            }
        }

        $this->view_component->celebration = new \Ecommerce\Core\Celebration();


        $this->view_component->recent_views = ProductService::getRecentViews($this->view_component->current_customer,5);

        $this->view_component->setBreadcrumbs('購物車');
        $this->view_component->seo('title','我的購物車');
        $this->view_component->seo('description','改裝零件、正廠零件、騎士用品、未登錄或團購商品，均可一次加入購物車中，方便、又可省運費。' . SEO::SHIPPING_FREE[activeShippingFree()]);

        $this->view_component->render();

        // return view('mobile.pages.cart.index', (array)$this->view_component);
        return view('mobile.pages.cart.index-new', (array)$this->view_component);
    }

    public function update()
    {
        //Middleware:AuthPost
        if(session()->has('pending_data')){
            $request = collect(session()->get('pending_data'));
        }else{
            $request = request();
        }

        $this->cartService->updateItem($request->get('sku'), $request->get('qty'),$request->get('cache'));

        if(!request()->ajax()){
            return redirect()->route('cart');
        }
    }

    public function remove()
    {
        $this->cartService->removeItem(request()->get('code'));
//        return redirect()->back();
    }

    public function coupon(){
        $this->cartService->setUsageCoupon(request()->get('code'));
//        return redirect()->back();
    }

    public function points(){
        $this->cartService->setUsagePoints(request()->get('points'));
//        return redirect()->back();
    }

    public function quotes()
    {
        $this->cartService->getQuotes();
    }

    public function installment()
    {
        $price = $this->cartService->getSubtotal();
        $coreInstallment = new CoreInstallment;
        $customer_role_id = $this->view_component->current_customer->role_id;
        return response()->json($coreInstallment->getPublishInstallments($customer_role_id, $price, $this->cartService->getFee()));
    }

    public function getTotalPrice()
    {   $result = new \StdClass;

        $result->final_price = number_format($this->cartService->getSubtotal() + $this->cartService->getFee());
        $result->max_points = $this->cartService->getMaxAllowPoints() >= $this->view_component->current_customer->getCurrentPoints() ? $this->view_component->current_customer->getCurrentPoints() : $this->cartService->getMaxAllowPoints();
        $result->now_use_points = request()->session()->get('usage-points' , '0') >= $this->cartService->getMaxAllowPoints() ? $this->cartService->getMaxAllowPoints() : request()->session()->get('usage-points' , '0');
        $result->receive_points = $this->cartService->getReceivedPoints();
        $result->shipping_fee = $this->cartService->getFee();

        return json_encode($result);
    }

    public function checkProduct()
    {
        $product_ids = $this->cartService->getItems()->pluck('product_id');
        return json_encode($product_ids);
    }

    public function reloadAddition()
    {
        $this->view_component->additional_collection = $this->cartService->getAdditionalProducts();
//        $render = view('mobile.pages.cart.partials.promotions',(array)$this->view_component)->render();
        $render = view('mobile.pages.cart.partials.additional',(array)$this->view_component)->render();
        return $render;
    }

    public function joinAdditionProduct()
    {
        $request = request();
        if($request->get('sku')){
            $this->view_component->carts = $this->cartService->getItems();
            foreach($this->view_component->carts as $key => $cart){
                if($cart->product && $cart->product->sku != $request->get('sku')){
                    $this->view_component->carts = $this->view_component->carts->forget($key);
                }
            }
            $this->view_component->service = $this->cartService;
            $this->view_component->carts_stock_info = $this->cartService->getItemDeliveryInfo($this->view_component->carts);

            foreach ($this->view_component->carts as $cartKey => $cart){
                $this->view_component->product = $cart->product;
                $this->view_component->cart = $cart;
//                $render = view('mobile.pages.cart.partials.item',(array)$this->view_component)->render();
                $render = view('mobile.pages.cart.partials.item-v2',(array)$this->view_component)->render();
                return $render;
            }
        }
    }
}