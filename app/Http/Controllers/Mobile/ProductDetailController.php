<?php
namespace App\Http\Controllers\Mobile;


use Auth;
use App\Http\Controllers\Controller;
use App\Components\View\ProductComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\GiftService;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\QaService;
use Ecommerce\Service\RecommendToBuyService;
use Everglory\Constants\CustomerRole;
use Everglory\Constants\ProductType;
use Everglory\Models\Mitumori;
use Everglory\Models\Mitumori\Item AS MitumoriItem;
use Everglory\Models\Product;
use Illuminate\Http\Request;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\WishlistService;
use Everglory\Models\ShippingCountry;
use Everglory\Models\ShippingArea;

class ProductDetailController extends Controller
{
    public function __construct(ProductComponent $component)
    {
        parent::__construct($component);

        app()->singleton('EmptyProduct', function () {
            return new Product();
        });
        $this->view_component->review_per_page = 5;
    }
    public function index(Request $request, $url_rewrite){
        
        start_measure('render','get product detail');
        $product = ProductService::getProductDetail($url_rewrite);
        stop_measure('render');
        
        start_measure('render','check product');
        if($url_redirect = ProductService::pageValidate($product)){
            return redirect()->to($url_redirect,301);
        }
        stop_measure('render');

        start_measure('render','information');
//        $this->view_component->information = $product->getInformation(false);

//        $this->view_component->stock_info = $this->view_component->information->stock;
        stop_measure('render');
        start_measure('render','other_view_category');
        $other_view_category = $product->categories->take( count($product->categories) -1 )->last();
        stop_measure('render');
//        $this->view_component->other_customer_view_products = ProductService::getOtherCustomerViews($product, $other_view_category->id);

//        $reviews_info = $product->getReviewInfo();
        start_measure('render','get relation review');
        $reviews_info = $product->getReviews();
        stop_measure('render');

        start_measure('render','get recommend');
        $this->view_component->recommends = ProductService::getProductRelationRanking($product,'visits',5);
        stop_measure('render');

        start_measure('render','get qa');
        $this->view_component->answers = $product->getQa();
        stop_measure('render');

        start_measure('render','getGroupSelectsAndOptions');
        $product->getGroupSelectsAndOptions();
        stop_measure('render');

        $this->view_component->product = $product;
        start_measure('render','get jp stock');

        stop_measure('render');
        $this->view_component->reviews_info = $reviews_info;
        $this->view_component->reviews_page = 1;

        start_measure('render','get seo');
        $this->view_component->setBreadcrumbs();

        $this->view_component->WishlistItem = false;
        if(\Auth::check()){
            $WishlistService = new WishlistService;
            $WishlistItem = $WishlistService->getItemByProductId($product->id);
            $this->view_component->WishlistItem = $WishlistItem;
        }

        $shippingCountry = \Cache::remember('shippingCountry' , 1440,function(){
           return  ShippingCountry::select()->get();
        });

        $this->view_component->shippingCountry = $shippingCountry;

        $this->view_component->customer_address = null;

        if($this->view_component->current_customer){
            $customer_address = $this->view_component->current_customer->address()->first();
            $this->view_component->customer_address = $customer_address;
            if($customer_address){
                $country = ShippingCountry::where('name',$customer_address->county)->first();
                if($country){
                    $this->view_component->country_area = ShippingArea::where('country_id',$country->id)->get();
                }
            }
        }

        if($this->view_component->last_category){
            \Debugbar::startMeasure('render','rankingPrepare');
            request()->request->add(['category' => $this->view_component->last_category->mptt->url_path ]);
            $cache_key = http_build_query(request()->only(['motor','category','manufacturer']));

            $convertService = app()->make(ConvertService::class);
            $search_service = app()->make(SearchService::class);
            $this->view_component->ranking_sales_products =  \Cache::tags(['ranking'])->remember('ranking-sales-'.$cache_key, 86400 * 7, function () use($request,$search_service,$convertService){

                $ranking_response_sales = $search_service->selectRanking($request);
                return $convertService->convertQueryResponseToProducts($ranking_response_sales);
            });
            request()->request->add(['type' => 'popularity' ]);

            $this->view_component->ranking_popularity_products =  \Cache::tags(['ranking'])->remember('ranking-popularity-'.$cache_key, 86400 * 7, function () use($request,$search_service,$convertService){
                $ranking_response_popularity = $search_service->selectRanking($request);
                return $convertService->convertQueryResponseToProducts($ranking_response_popularity);
            });
            request()->request->remove('popularity');
            request()->request->remove('category');
            \Debugbar::stopMeasure('render');
        }
        
        
        $this->view_component->render();

        stop_measure('render');
        return view('mobile.pages.product-detail.index', (array)$this->view_component);
    }

    public function info(Request $request)
    {
        $current_customer = $this->view_component->current_customer;
        $group_code = $request->get('gc');
        $name = $request->get('na');
        $manufacturer_id = $request->get('mi');
        if($group_code and $name and $manufacturer_id){
            $product = ProductService::selectProductFromOptionFlat( $name , $group_code ,$manufacturer_id);
            $product = new ProductAccessor( $product , false);
        }else{
            $product = ProductService::getProductDetail($request->get('sku'));
        }

        $result = new \stdClass();
        if ($product and $product->id){
            $result->product_id = $product->id;
            $result->sku = $product->sku;
            $result->full_name = $product->full_name;
            $result->url = route('product-detail', ['url_rewrite' => $product->url_rewrite]);
            $result->information = $product->getInformation();
            $result->stock = $result->information->stock;
            $result->tags = $product->getTags($current_customer);
            $result->base_price = number_format($product->price);
            $result->final_price = number_format($product->getFinalPrice( $current_customer ));
            $result->installments = (Array)$product->getInstallments($current_customer);
            if ($current_customer and in_array($current_customer->role_id,[CustomerRole::WHOLESALE,CustomerRole::STAFF])){
                $result->normal_price = number_format($product->getNormalPrice());
            }
            $result->final_point = round($product->getFinalPoint(  $current_customer  ));
            $result->model_number = $product->model_number;
            $package = $product->getAllImages();
            $result->images = $package->images;
            $result->thumbnails = $package->thumbnails;
        }else{
            if($group_code and $manufacturer_id){
                ProductService::reindexProductFromOptionFlat($group_code ,$manufacturer_id);
            }else{
                app()->abort('404');
            }
        }
        return response()->json($result);
    }

    public function review($url_rewrite)
    {
        $result = new \stdClass();
        $result->success = false;
        $product = ProductService::getProductDetail($url_rewrite);
        $reviews_info = $product->getReviews()->sortByDesc(request()->input('sort'));
        $reviews_page = request()->input('page');
        $review_per_page = $this->view_component->review_per_page;
        $result->html = view('response.pages.product-detail.partials.review',compact('reviews_info','reviews_page','review_per_page'))->render();
        $result->success = true;
        return json_encode($result);

    }

    public function qaPraise($product_url_rewrite)
    {
        dd(123);
        $answer_id = request()->get('answer_id');
        $result = new \stdClass();
        $result->success = false;
        $praise = QaService::praise($answer_id);
        $result->count = 0;
        $count = QaService::getPraiseCount($answer_id)->first();
        if($praise and $count){
            $result->success = true;
            $result->count = $count->count;
        }

        return json_encode($result);
    }

    public function qaPraiseCount($product_url_rewrite)
    {
        $answer_ids = request()->get('answer_ids');
        $result = new \stdClass();
        $result->success = false;
        $result->counts = 0;
        $counts = QaService::getPraiseCount($answer_ids);
        if(count($counts)){
            $result->success = true;
            $result->counts = $counts->toArray();
        }

        if(\Auth::check()){
            $locks = QaService::getPraiseCount($answer_ids, \Auth::user()->id);
        }else{
            $locks = collect([]);
        }
        $result->locks = $locks->toArray();
        return json_encode($result);
    }

    public function getAlsoBuy()
    {
        $result = getBasicAjaxObject();
        $product_url_rewrite = request()->get('product_url_rewrite');
        if($product_url_rewrite){
            $productModel = ProductService::getProductDetail($product_url_rewrite);
            $products = [];
            $alsoBuyProductsIds = RecommendToBuyService::getRecommend($productModel);
            $alsoBuyProducts = ProductRepository::findDetail($alsoBuyProductsIds, true, 'id');
            foreach($alsoBuyProducts as $alsoBuyProduct){
                $products[] = new ProductAccessor($alsoBuyProduct);
            }
            $this->view_component->alsoBuyProducts = collect($products);

            if(count($this->view_component->alsoBuyProducts) > 3){
                $result->success = true;
                $result->html = view('response.pages.product-detail.partials.also-buy', (array)$this->view_component)->render();
            }
        }
        return json_encode($result);
    }

    public function deleteWishlist(Request $request){
        $protect_code = request()->get('protect_code');
        $WishlistService = new WishlistService;
        $WishlistService->removeItem($protect_code);
        $result = "已從待購清單移除";
        return  json_encode($result);
    }

    public function insertWishlist(Request $request){
        $sku = request()->get('sku');
        $qut = 1;
        $WishlistService = new WishlistService;
        $item = $WishlistService->updateItem($sku,$qut);
        $result = ['text' => "已加入待購清單",'item' => $item];
        return  json_encode($result);
    }

    public function CheckWishlistItem(Request $request){
        $product_id = request()->get('product_id');
        $result = false;
        if(\Auth::check()){
            $WishlistService = new WishlistService;
            $result = $WishlistService->getItemByProductId($product_id);
        }

        return  json_encode($result);

    }

    public function getAreaByCount(){
        $country_id = request()->get('country_id');
        $area = ShippingArea::Where('country_id',$country_id)->get();
        return json_encode($area);   
    }

    public function getShippingInfo(){
        $shipping_day = request()->get('shipping_day');
        $ship_day = request()->get('ship_day');
        $workDate = new \Ecommerce\Utils\WorkDate;
        $taiwanShippingDate = $workDate->getTaiwanShippingDate($ship_day,$shipping_day);

        $day = \Carbon\Carbon::parse($taiwanShippingDate, 'Asia/Taipei')->format('m月d日');

        $week = ["日","一","二","三","四","五","六"];

        $ShippingInfo = "您預計可於" . $day . " 星期" .$week[date('w',strtotime($taiwanShippingDate))] . "收到商品包裹。";

        return json_encode($ShippingInfo);
    }

}


