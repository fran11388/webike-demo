<?php

namespace App\Http\Controllers\Mobile;

use App\Components\View\Mobile\SimpleComponent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request as Requests;
use Ecommerce\Service\CampaignService;

class CampaignController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('benefit');
        $component->setBreadcrumbs('會員好康', $component->base_url);

        $component->is_phone = false;
        if (!\Ecommerce\Core\Agent::isNotPhone()) {
            $component->is_phone = true;
        }
    }

    public function getCampaignPromotion(Requests $request,$url_rewrite)
    {
        $service = new CampaignService;

        return $service->getRenderCampaign($url_rewrite,$this->view_component);
    }
}