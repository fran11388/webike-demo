<?php

namespace App\Http\Controllers\Mobile;

use App\Components\View\SimpleComponent;
use App\Http\Controllers\Controller;
use Ecommerce\Accessor\CustomerAccessor;
use Ecommerce\Core\Agent;
use Ecommerce\Service\AssortmentService;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Service\GenuinepartsService;
use Ecommerce\Service\ProductService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $conditions = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SimpleComponent $component)
    {
        //Component and User( User must use after construct!!)
        parent::__construct($component);
    }

    public function device()
    {
        //do something when mobile.
    }

    public function index()
    {
        $limit = Agent::limitByDevice([
            'reviews' => [20,5],
            'new_motors' => [10,5],
            'new_product_brands' => [null,10],
            'assortments' => [9,5],
        ]);

        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $display_categories = ['1000', '3000', '4000', '1210', '9000'];
        $this->view_component->main_categories = \Ecommerce\Repository\CategoryRepository::find($display_categories)
            ->sortBy(function($item) use($display_categories){
                return current(array_keys($display_categories, $item->url_rewrite));
            });
        $this->view_component->new_motors = \Ecommerce\Service\MotorProductService::getNewestMotorProductOfCustomer($limit->new_motors);
        $this->view_component->main_manufacturers = \Ecommerce\Repository\MotorRepository::selectManufacturersByUrlRewrite(\Everglory\Constants\ViewDefine::MAIN_MOTOR_MANUFACTURERS);
        $this->view_component->posts = \Ecommerce\Service\BikeNewsService::getPickupCategoryNews(5, [70, 75, 76, 77, 78, 73], 'exclude');
        // $this->view_component->new_product_brands = \Ecommerce\Repository\ManufacturerRepository::getNewProductsBrands($limit->new_product_brands);


        // collections
        // $assortService = new AssortmentService;
        // $this->view_component->assortments = $assortService->getAssortmentByPublishAt($limit->assortments);
        $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();
        $this->view_component->setBreadcrumbs([['name' => 'Webike-台灣', 'url' => route('home')]]);
        $this->view_component->seo('title','「Webike-台灣」進口重機、機車改裝用品情報，新車中古車資訊，全球摩托車新聞滿載!!');
        $this->view_component->seo('description','即時更新摩托百貨、摩托車市、摩托新聞最新情報~Webike滿足您的摩托人生!');
        $this->view_component->render();
        return view('mobile.pages.index',  (array)$this->view_component);
    }

        public function shopping()
    {
        cacheControlProcess(['shopping']);


        //ATS initial
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);

        $ca_random = collect(["4000","8000"])->random();
        $this->view_component->categories = [
            3000 => CategoryRepository::selectChildren('3000','url_rewrite')->filter(function ($category, $key) {
                return $category->mptt->depth == 2 and !in_array($category->url_rewrite,['1212','t005']);
            })->random(1),
            1000 => CategoryRepository::selectChildren('1000','url_rewrite')->filter(function ($category, $key) {
                return $category->mptt->depth == 2 and substr($category->mptt->url_path,0,9) != '1000-4000' ;
            })->random(1)
            ,
            // $ca_random => CategoryRepository::selectChildren($ca_random,'url_rewrite')->filter(function ($category, $key) {
            //     return $category->mptt->depth == 2 and !in_array($category->url_rewrite,['1212','t005']);
            // })->random(1),
            $ca_random => CategoryRepository::find($ca_random),
        ];

        foreach($this->view_component->categories as $root_url_rewrite => $category){
            $carousel_configs['category_'. $root_url_rewrite ] = (object) ['collection'=>null,'params'=>(object)['key'=>'category_'. $root_url_rewrite,'call'=>'category','blade'=>'b','limit'=>'6','parameter'=>['url_path'=>$category->mptt->url_path],'shuffle'=>'on']];
        }
        $carouselService = new \Ecommerce\Service\CarouselService($carousel_configs);

        foreach ($carouselService->call() as $key => $carouselData){
            $this->view_component->$key = $carouselData;
        }

        $genuineparts_service = app()->make(GenuinepartsService::class);

        //modify name
        // $_category = $this->view_component->categories['4000'];
        // $_category->name = '維護耗材及工具';
        // $this->view_component->categories['4000'] = $_category;
        $this->view_component->customer = \Auth::user();

        // recent view
        $this->view_component->render();


        // 2019.1.18 網頁前端重新設計測試 William Tai
        return view('mobile.pages.shopping.shopping', (array)$this->view_component);    
        
    }

}