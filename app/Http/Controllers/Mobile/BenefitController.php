<?php
namespace App\Http\Controllers\Mobile;

use App\Components\View\Mobile\SimpleComponent;
use App\Http\Controllers\Controller;
use Ecommerce\Service\Promotion\PromoService;
use Illuminate\Http\Request as Requests;

class BenefitController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('benefit');
        $component->setBreadcrumbs('會員好康', $component->base_url);
    }

    public function getNewYear($year)
    {
        return view('mobile.pages.benefit.sale.new-year.2018',  (array)$this->view_component);
    }

    public function get2019BigPromotion(Requests $request){
       
        $promo_name = 'Spring2019';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);

        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        
        $this->view_component = $promotion->promotion();
        return view('mobile.pages.benefit.sale.2019promotion.promotion', (array)$this->view_component);
    }

    public function get2019PromotionPreview(Requests $request){
        $promo_name = 'Spring2019';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);

        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $this->view_component = $promotion->promotionPreview();
        return view('mobile.pages.benefit.sale.2019promotion.promotion-preview', (array)$this->view_component);
    }
    public function get2019PromotionBrand(Requests $request){
        $promo_name = 'Spring2019';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);

        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $this->view_component = $promotion->promotionBrand();
        return view('mobile.pages.benefit.sale.2019promotion.promotion-brand', (array)$this->view_component);   
    }

    public function getWeeklyPromotion(){
        $rel_parameter = 'weekly';
        $this->view_component->rel_parameter = $rel_parameter;
        return view('mobile.pages.benefit.sale.weekly.promotion', (array)$this->view_component);
    }
}