<?php
namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Ecommerce\Service\EstimateService;
use App\Components\View\SimpleComponent;
use Auth;

class MitumoriController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('mitumori');
        $component->setBreadcrumbs('未登錄商品查詢購買系統',$component->base_url);
    }

    public function index()
    {
        $this->view_component->seo('title','未登錄商品查詢購買系統');
        $this->view_component->seo('description','未登錄商品查詢購買系統');
        $this->view_component->seo('keyword','未登錄商品查詢購買系統');
        $this->view_component->render();
        return view('mobile.pages.mitumori.index', (array)$this->view_component);
    }

}


?>