<?php
namespace App\Http\Controllers\Mobile;

use App\Components\View\CartComponent;
use Ecommerce\Service\CartService;
use App\Http\Controllers\Controller;
use Ecommerce\Service\CheckoutService;
use Ecommerce\Service\GiftService;
use Ecommerce\Service\PaymentService;
use Ecommerce\Service\Quote\Exception AS QuoteException;
use Ecommerce\Service\QuoteService;
use Ecommerce\Shopping\Payment\PaymentManager;
use Everglory\Constants\Payment;
use Everglory\Models\Customer;
use Everglory\Models\Order;
use Everglory\Models\Sales\Cart;
use Everglory\Models\Sales\Creditcard;
use Everglory\Models\Sales\Quote;
use Illuminate\Http\Request;
use Validator;


class CheckoutController extends Controller
{
    /**
     * @property CartComponent $view_component
     * @property CheckoutService $checkoutService
     * @property CartService $cartService
     */
    protected $view_component;
    protected $checkoutService;
    protected $cartService;

    /**
     * CheckoutController constructor.
     */
    public function __construct()
    {
        parent::__construct(new CartComponent());
        request()->request->add([ 'shipping-method' => 'hct']);
        $this->middleware(function ($request, $next) {
            if(!\Auth::check() and \Route::currentRouteName() == 'checkout-payment-verify'){
                \Log::warning('Customer was logout : ' . $_SERVER['REMOTE_ADDR']);
                $orderID = \request()->get('ONO', \request()->get('OrderID'));
                $creditcard = Creditcard::onWriteConnection()->where('ONO', $orderID)->first();
                if($creditcard and $creditcard->remote_addr == $_SERVER['REMOTE_ADDR']){
                    //Help customer login
                    $customer = Customer::find($creditcard->customer_id);
                    \Auth::login($customer,true);
                    \Log::info('Help Customer Login: ' . $customer->id);
                    $this->view_component->current_customer = $customer;
                }else{
                    \Log::critical('Validate fail!');
                    return redirect()->to('login');
                }
            }
            $this->cartService = app()->make(CartService::class);
            $this->view_component->current_customer->address = $this->view_component->current_customer->address()->first();
            return $next($request);
        });

    }

    public function index()
    {
        if($this->view_component->current_customer->role_id == \Everglory\Constants\CustomerRole::REGISTERED){
            return redirect()->route('customer-account-complete');
        }
        if($this->cartService->getCount() == 0){
            return redirect()->route('cart');
        }

        $giftService = new GiftService();
        $customerGifts = GiftService::getCustomerGiftsByStep('checkout');
        $this->view_component->hasGiftInCart = $giftService->hasGiftInCart($customerGifts);
        if(is_null($this->view_component->hasGiftInCart)){
            $customerGifts = GiftService::getCustomerGiftsByStep('checkout');
        }
        $this->view_component->gifts = $giftService->getCustomerGiftsProduct($customerGifts);

        $quote = QuoteService::createQuote($this->cartService);
        $quote->save();
        $this->view_component->service = $this->cartService;
        $carts = $this->cartService->getItems();
        $this->view_component->protect_code = $quote->code;
        $this->view_component->carts = $carts;
        return view('mobile.pages.checkout.index', (array)$this->view_component);
    }

    public function save()
    {
        if($this->cartService->getCount() == 0){
            return redirect()->route('cart');
        }
        $checkoutService = new CheckoutService();
        $result = $checkoutService->process();
        if ($result instanceof Quote) {
            //Credit card method
            request()->session()->put('quote' , $result);
        }else if($result instanceof QuoteException){
            return redirect()->to($result->redirect)->withErrors($result->message);
        }else{
            //normal checkout method
        }
        $redirect = $checkoutService->getRedirectUrl();

        return redirect()->to($redirect)->with('order_increment_id', $result->increment_id);
    }

    public function payment()
    {
        if($this->cartService->getCount() == 0){
            return redirect()->route('cart');
        }
        $quote = request()->session()->get('quote', null);
        if (!$quote) {
            $current_error = '找不到quote，payment 異常。';
            \Log::error($current_error, session()->all());
            abort(500);
        }
        request()->session()->forget('quote');

        if ($quote->payment_method == Payment::CREDIT_CARD ||
            $quote->payment_method == Payment::CREDIT_CARD_LAYAWAY_ESUN
        ) {
            return view('response.pages.checkout.payment.esun.index', ['quote' => $quote]);
        } elseif ($quote->payment_method == Payment::CREDIT_CARD_LAYAWAY_NCCC) {
            return view('response.pages.checkout.payment.nccc.index', ['quote' => $quote]);
        }
        $current_error = '找不到 payment 異常。';
        \Log::error($current_error, session()->all());
        abort(500);
    }

    public function verify()
    {
        if($this->cartService->getCount() == 0){
            return redirect()->route('cart');
        }
        $orderID = \request()->get('ONO', \request()->get('OrderID'));
        $creditcard = Creditcard::onWriteConnection()->where('ONO', $orderID)->first();
        $service = new PaymentService($creditcard);
        $service->store();
        if ($service->verify()) {
            $order = $service->process();
            $wmail = new \Ecommerce\Core\Wmail();
            $wmail->creditcard($creditcard,$order);
            return redirect()->route('checkout-done')->with('order_increment_id',$order->increment_id);
        } else {
            return redirect()->route('checkout')->withErrors([$service->getErrorMessage()]);

        }
    }

//
//    public function finish(){
//        $order_increment_id = request()->session()->pull('order_increment_id', null);
//        if(!$order_increment_id){
//            \Log::error('找不到訂單編號。', session()->all());
//            return abort(404);
//        }
//        $this->view_component->order_increment_id = $order_increment_id;
//        $this->view_component->setBreadcrumbs('訂單完成(' . $order_increment_id . ')');
//        $this->view_component->seo('title', '訂購查詢完成');
//        $this->view_component->render();
//        return view('response.pages.checkout.done', (array)$this->view_component);
//    }
}