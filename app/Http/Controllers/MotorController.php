<?php
namespace App\Http\Controllers;

use Everglory\Constants\SEO;
use URL;
use App\Components\View\SimpleComponent;
use Ecommerce\Service\MotorService;
use Ecommerce\Service\GenuinepartsService;
use Ecommerce\Service\SummaryService;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Repository\ReviewRepository;
use Ecommerce\Support\DisplacementConverter;
use Ecommerce\Service\NavigationService;

/**
 * Class MotorController
 * @package App\Http\Controllers
 */
class MotorController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('motor');
        $component->setBreadcrumbs('車型索引', $component->base_url);
        $component->addMyBikesDisabled = false;

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $countries_array = array('台灣','日本','美國','德國','義大利','奧地利','英國','瑞典','西班牙','印度','中國');
        $motor_countries = \Ecommerce\Repository\MotorRepository::selectAllManufacturer()->groupBy('country')->filter(function($value,$country) use($countries_array){
            if(in_array($country,$countries_array)){
                return $value;
            }
        });
        $this->view_component->motors = $motor_countries->sortBy(function($value,$country) use($countries_array){
            return array_search($country,$countries_array);
        });

        $motor_manufacturers_array = array('HONDA','YAMAHA','SUZUKI','KAWASAKI','HARLEY-DAVIDSON','BMW','DUCATI','APRILIA','KTM','TRIUMPH','VESPA','PIAGGIO','MOTOGUZZI','MV AGUSTA','HUSQVARNA','BIMOTA','HUSABERG','GASGAS','ROYAL ENFIELD','NORTON','VICTORY','光陽','三陽','台灣山葉','宏佳騰','台鈴','PGO摩特動力','哈特佛','CPI','ADIVA','Gogoro','CAGIVA');
        $motor_manufacturers = \Ecommerce\Repository\MotorRepository::MotorCount()->filter(function($value) use($motor_manufacturers_array){
            if(in_array($value->name,$motor_manufacturers_array)){
                return $value;
            }
        });
        $this->view_component->manufacturers = $motor_manufacturers->sortBy(function($value) use($motor_manufacturers_array){
            return array_search($value->name,$motor_manufacturers_array);
        })->take(12);

        $this->view_component->seo('title', '進口重機、機車全球知名品牌車款搜尋');
        $this->view_component->seo('description', '日本、義大利、美國、德國、台灣各式進口重機、機車型錄，只要選擇廠牌與排氣量即可找到對應的車款，甚至其他冷門車款也找的到!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, GASGAS, ROYAL ENFIELD 車輛規格SPEC');
        $this->view_component->render();

        return view('response.pages.motor.index', (array)$this->view_component);
    }

    /**
     * @param string $url_rewrite Motor Manufacturer's url_rewrite
     * @param int $displacement Motor's displacement
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function manufacturer($url_rewrite, $displacement)
    {
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $manufacturers = \Ecommerce\Repository\MotorRepository::selectAllManufacturer();
        $manufacturer_name = '';
        foreach($manufacturers as $manufacturer){
            if($manufacturer->url_rewrite == $url_rewrite){
                $manufacturer_name = $manufacturer->name;
            }elseif(strtoupper($manufacturer->url_rewrite) == strtoupper($url_rewrite) or $manufacturer->name == $url_rewrite) {
                if(session('_previous')) {
                    \Log::alert('/ _previous:' . implode(',', session('_previous')) );
                }
                return redirect()->route('motor-manufacturer', [$manufacturer->url_rewrite, $displacement], 301);
            }
        }

        if(!$manufacturer_name){
            if(session('_previous')){
                \Log::alert('/ _previous:' . implode(',',session('_previous')) );
            }
            app()->abort('404');
        }

        /*****Motormarket****/
        $this->view_component->newmotors = \Ecommerce\Service\MotorProductService::getNewMotorsByManufacturer($url_rewrite);

        $this->view_component->motors = \Ecommerce\Repository\MotorRepository::getMotorsByDisplacementAndManufacturer($url_rewrite,$displacement);

        if(!count($this->view_component->motors)){
            app()->abort('404');
        }

        switch ($displacement){
            case 50:
                $title = '【'.$manufacturer_name.' - 50cc'.'】';
                $description = $manufacturer_name.' - 50cc';
                $breadcrumbs = ' - 50cc';
                break;
            case 125:
                $title = '【'.$manufacturer_name.' 51cc-125cc'.'】';
                $description = $manufacturer_name.' 51cc-125cc';
                $breadcrumbs = ' 51cc-125cc';
                break;
            case 250:
                $title = '【'.$manufacturer_name.' 126cc-250cc'.'】';
                $description = $manufacturer_name.' 126cc-250cc';
                $breadcrumbs = ' 126cc-250cc';
                break;
            case 400:
                $title = '【'.$manufacturer_name.' 251cc-400cc'.'】';
                $description = $manufacturer_name.' 251cc-400cc';
                $breadcrumbs = ' 251cc-400cc';
                break;
            case 750:
                $title = '【'.$manufacturer_name.' 401cc-750cc'.'】';
                $description = $manufacturer_name.' 401cc-750cc';
                $breadcrumbs = ' 401cc-750cc';
                break;
            case 1000:
                $title = '【'.$manufacturer_name.' 751cc-1000cc'.'】';
                $description = $manufacturer_name.' 751cc-1000cc';
                $breadcrumbs = ' 751cc-1000cc';
                break;
            case 1001:
                $title = '【'.$manufacturer_name.' 1001cc 以上'.'】';
                $description = $manufacturer_name.' 1001cc 以上';
                $breadcrumbs = ' 1001cc 以上';
                break;
            case '00':
                $title = '【'.$manufacturer_name.'】';
                $description = $manufacturer_name;
                $breadcrumbs = '';
                break;
            default:
                app()->abort('404');
                $breadcrumbs = '';
                $title = '';
                $description = '';
                break;
        }

        $this->view_component->setBreadcrumbs($manufacturer_name.$breadcrumbs);
        $this->view_component->seo('title', $title.'重機、機車車型規格搜尋');
        $this->view_component->seo('description', $description.'各排氣量車輛型錄、規格SPEC車款搜尋');
        $this->view_component->seo('keywords', $title.'相關零件、套件查詢,摩托車規格,出廠年份,排氣量,車輛簡介,公司概要,車輛資訊,車輛性能,車輛評價');
        $this->view_component->render();
        return view('response.pages.motor.displacement', (array)$this->view_component);
    }

    public function top($url_rewrite)
    {
        $service = new MotorService($url_rewrite, ['manufacturer', 'specifications']);
        $result = $this->initialMotorInformation($service);
        if($this->view_component->current_customer){
            $myBikeIds = $this->view_component->current_customer->motors->pluck('id')->toArray();
            $this->view_component->addMyBikesDisabled = in_array($this->view_component->current_motor->id, $myBikeIds);
        }
        //Motor Header info

        //Special block for this page
        $convertService = $convertService = app()->make(ConvertService::class);
        if($this->view_component->info->new < 28){
            $this->view_component->new_products = $convertService->convertQueryResponseToProducts($result->summaryService->selectNewProduct(request(),false));
        }else{
            $this->view_component->new_products = $convertService->convertQueryResponseToProducts($result->new_product_search_response);
        }
        $this->view_component->specification = $service->getActiveSpecification();
        $this->view_component->manufacturer = $convertService->convertFacetFieldToManufacturers($result->search_response);

        $this->view_component->news = $service->getMotorTagNews();
        $this->view_component->reviews = ReviewRepository::get(null,null, $this->view_component->current_motor , 8);

        //銷售排行商品

        $top_sales_search_response = \Cache::tags(['summary'])->remember('summary-top-sales-search-response-'.$result->cache_key, 86400 * 7, function () use($result){
            return $result->summaryService->selectTopSales(request());
        });

        $this->view_component->hot_manufacturers = $convertService->convertFacetFieldToManufacturers($top_sales_search_response);

        $this->view_component->newmotors = \Ecommerce\Service\MotorProductService::getNewestMotorProduct(30, $this->view_component->current_motor->id);

        //同級距熱門車款
        $this->view_component->popular_motors = $service->getPopularMotors();
        $this->view_component->url_path = request()->path();

        //Motor Meta
        $disp = DisplacementConverter::find($this->view_component->current_motor->displacement);

        $this->view_component->setBreadcrumbs($this->view_component->current_motor->manufacturer->name . ' ' . $disp->name , getMfUrl(['motor_manufacturer' => $this->view_component->current_motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key]));
        $this->view_component->setBreadcrumbs($this->view_component->current_motor->name . ($this->view_component->current_motor->synonym ? '(' . $this->view_component->current_motor->synonym . ')' : ''), getMtUrl(['motor_url_rewrite' => $this->view_component->current_motor->url_rewrite]));
        $this->view_component->setBreadcrumbs('車型首頁');

        $this->view_component->seo('title', '【'.$this->view_component->current_name . ' 車型首頁】 - 最新消息、相關評論、適用配備及改裝一覽');
        $this->view_component->seo('description', '【'.$this->view_component->current_name . '】基本規格、最新消息、新車・中古車價格及情報、零件用品搜尋、新商品、相關商品評論、熱門品牌，盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);

        $this->view_component->render();

        // if(str_contains(request()->getHost(),"amp")){
        //     return view('mobile.pages.amp.motor.top',  (array)$this->view_component);
        // }else{
            // return view('response.pages.motor.top', (array)$this->view_component);
        // }
            return view('response.pages.motor.top', (array)$this->view_component);
        
    }

    public function service($url_rewrite,$catalogue_id = null)
    {
        $this->view_component->catalogue_id = $catalogue_id;
        $service = new MotorService($url_rewrite, ['manufacturer', 'specifications']);
        $result = $this->initialMotorInformation($service,$catalogue_id);
        if($this->view_component->current_customer){
            $myBikeIds = $this->view_component->current_customer->motors->pluck('id')->toArray();
            $this->view_component->addMyBikesDisabled = in_array($this->view_component->current_motor->id, $myBikeIds);
        }

        //Special block for this page
        //Catalogue / series
        $this->view_component->series = $service->getSeries();
        $this->view_component->current_series_images = $service->getSeriesByCatalogueImage($this->view_component->catalogue,$this->view_component->series);

        $this->view_component->current_images  = $service->getCurrentCatalogueImages($this->view_component->catalogue, $this->view_component->series);

        $this->view_component->tree = \Cache::tags(['summary'])->remember('summary-tree-'.$result->cache_key, 86400 * 7, function () use($result){
            return NavigationService::getNavigation($result->search_response);
        });
        $this->view_component->url_path = request()->path();
        $this->view_component->url_rewrite = $url_rewrite;
        //Motor Meta
        $disp = DisplacementConverter::find($this->view_component->current_motor->displacement);
        $this->view_component->setBreadcrumbs($this->view_component->current_motor->manufacturer->name . ' ' . $disp->name , getMfUrl(['motor_manufacturer' => $this->view_component->current_motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key]));
        $this->view_component->setBreadcrumbs($this->view_component->current_motor->name . ($this->view_component->current_motor->synonym ? '(' . $this->view_component->current_motor->synonym . ')' : ''), getMtUrl(['motor_url_rewrite' => $this->view_component->current_motor->url_rewrite]));
        $this->view_component->setBreadcrumbs($this->view_component->current_motor->name . '規格總覽');

        $this->view_component->seo('title', '【' . $this->view_component->current_name . '規格總覽】 - 各年分詳細車輛規格、配備及圖鑑');
        $this->view_component->seo('description', $this->view_component->current_name . '車輛圖鑑、基本規格、維修資訊、車體、懸吊、配備、引擎、其他詳細車輛規格、商品分類、對應零件快速搜尋。盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);

        $this->view_component->render();
    
        // if(str_contains(request()->getHost(),"amp")){
        //     return view('mobile.pages.amp.motor.service', (array)$this->view_component);   
        // }else{
        //     return view('response.pages.motor.service', (array)$this->view_component);
        // }
        return view('response.pages.motor.service', (array)$this->view_component);
    }

    public function review($url_rewrite)
    {
        $service = new MotorService($url_rewrite, ['manufacturer', 'specifications']);
        $result = $this->initialMotorInformation($service);
        if($this->view_component->current_customer){
            $myBikeIds = $this->view_component->current_customer->motors->pluck('id')->toArray();
            $this->view_component->addMyBikesDisabled = in_array($this->view_component->current_motor->id, $myBikeIds);
        }
        //Special block for this page
        $current_motor = $this->view_component->current_motor;
        
        $this->view_component->reviews = ReviewRepository::get(null, null, $current_motor, 15);
        $this->view_component->url_path = request()->path();
        //Motor Meta
        $disp = DisplacementConverter::find($current_motor->displacement);
        $this->view_component->setBreadcrumbs($current_motor->manufacturer->name . ' ' . $disp->name , getMfUrl(['motor_manufacturer' => $current_motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key]));
        $this->view_component->setBreadcrumbs($current_motor->name , getMtUrl(['motor_url_rewrite' => $current_motor->url_rewrite]));
        $this->view_component->setBreadcrumbs('商品評論');

        $this->view_component->seo('title', '【'.$this->view_component->current_name  . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') . ' 商品評論】- 車友改裝、使用心得、商品討論區。');
        $this->view_component->seo('description', '【'.$this->view_component->current_name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') . '】車友改裝、使用心得、相關商品評論，盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        
        $this->view_component->render();

        // if(str_contains(request()->getHost(),"amp")){
        //     return view('mobile.pages.amp.motor.review',  (array)$this->view_component);
        // }else{
        //     return view('response.pages.motor.review', (array)$this->view_component);;
        // }
        return view('response.pages.motor.review', (array)$this->view_component);;        
    }

    public function video($url_rewrite)
    {
        $service = new MotorService($url_rewrite, ['manufacturer', 'specifications']);
        $result = $this->initialMotorInformation($service);
        if($this->view_component->current_customer){
            $myBikeIds = $this->view_component->current_customer->motors->pluck('id')->toArray();
            $this->view_component->addMyBikesDisabled = in_array($this->view_component->current_motor->id, $myBikeIds);
        }

        //Special block for this page
        $current_motor = $this->view_component->current_motor;
        $this->view_component->manufacturer = \Ecommerce\Repository\MotorRepository::getMotorManufacturer($url_rewrite);
        $this->view_component->motoVideoResult = $service->getMotorVideo($this->view_component->current_motor,$this->view_component->manufacturer);
        $convertService = $convertService = app()->make(ConvertService::class);
        $top_sales_search_response = $result->summaryService->selectTopSales(request());
        $this->view_component->top_sales_products = $convertService->convertQueryResponseToProducts($top_sales_search_response);


        //Motor Meta
        $disp = DisplacementConverter::find($current_motor->displacement);
        $this->view_component->setBreadcrumbs($current_motor->manufacturer->name . ' ' . $disp->name , getMfUrl(['motor_manufacturer' => $current_motor->manufacturer->url_rewrite, 'motor_disp' => $disp->key]));
        $this->view_component->setBreadcrumbs($current_motor->name . ($this->view_component->current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') , getMtUrl(['motor_url_rewrite' => $current_motor->url_rewrite]));
        $this->view_component->setBreadcrumbs('車友影片');

        $this->view_component->seo('title', '【'.$current_motor->manufacturer->name.' '.$current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') . ' 車友影片】- 台灣、日本車友影片分享。');
        $this->view_component->seo('description', '【'.$current_motor->manufacturer->name.' '.$current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') . '】使用狀況、騎乘及試乘影片觀看、相關影片盡在' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        
        $this->view_component->render();
        return view('response.pages.motor.video', (array)$this->view_component);
    }

    public function licensesInfo()
    {
        $this->view_component->seo('title', '什麼是車輛機號?');
        $this->view_component->seo('description', '※部分技術性改裝品需要確認車輛的形式，以確保可以安裝。※車輛機號在每輛車的”行照”上都會註明，請看以下範例。' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        $this->view_component->seo('keyword', '車輛, 機車, 車輛機號, 車輛的形式, 行照, 行照範例, 技術性改裝品');
        $this->view_component->render();

        return view('response.common.moto-licenses-info', (array)$this->view_component);
    }


    private function initialMotorInformation(MotorService $service,$catalogue_id = null){

        //Motor display name
        $result = new \stdClass();
        $this->view_component->current_motor = $service->getMotor();
        $current_motor = $this->view_component->current_motor;
        $this->view_component->catalogue = $service->getServiceData($catalogue_id);
        $this->view_component->current_name = $current_motor->manufacturer->name . ' ' . $current_motor->name;
        if($this->view_component->catalogue) {
            $this->view_component->current_name .= '(' . ($this->view_component->catalogue->model_release_year) . '年)';
        }
        if($current_motor->synonym){
            $this->view_component->current_name .= '(' . ($current_motor->synonym) . ')';
        }
        $this->view_component->summary_title = $this->view_component->current_name;


        $this->view_component->genuine_link = $service->getGenuineSalesLink();

        $summaryService = app()->make(SummaryService::class);

        //Motor Header info

        $summaryService->importSegmentsToRequest();
        $result->cache_key = http_build_query(request()->only(['motor']));

        $new_product_search_response =  \Cache::tags(['summary'])->remember('summary-new-products-search-response-'.$result->cache_key, 86400 * 7, function () use($summaryService) {
            return $summaryService->selectNewProduct(request());
        });

        $new_product_count = $new_product_search_response->getGroupResponse()->getCount();

        $search_response =  \Cache::tags(['summary'])->remember('summary-search-response-'.$result->cache_key, 86400 * 7, function () use($summaryService) {
            return $summaryService->selectSummary(request());
        });

        $this->view_component->info = $service->getMotorInformation($search_response,$new_product_count);
        $this->view_component->modelInfo = $service->getMotorFivePower();


        $this->view_component->ranking_avg = \Cache::tags(['summary','review'])->remember('summary-ranking_avg-'.$result->cache_key, 86400 * 7, function () use($current_motor){
            return ReviewRepository::getAvgRanking(null,null,$current_motor);
        });


        // for other request
        $result->summaryService = $summaryService;
        $result->search_response = $search_response;
        $result->new_product_search_response = $new_product_search_response;

        return $result;
    }

}
