<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Promotion;
use Ecommerce\Service\promoCms\PromoService;
use Ecommerce\Core\Image;
use Ecommerce\Service\Tool\ToolService;
use Everglory\Models\cms_file;

class ToolController extends BackendController
{

   public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend');
        $component->function_name = '小工具';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function imageUploadIndex()
    {   
        
        $this->view_component->events = \Everglory\Models\Calendar\Event::all();
        $this->view_component->seo_title = '圖片上傳';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
      return view('backend.pages.tool.imageUpload', (array)$this->view_component);
    }


    public function upload()
    {
        $result = new \stdClass();
        $result->reload = false;
        var_dump($result);
        if (!\Auth::check()) {
            $result->success = false;
            $result->message = '請重新登入';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        } else {
            $customer = \Auth::user();
        }

        $img_files = request()->file('file');
        
        foreach($img_files as $key => $img_file)
        {
          
          if(!$img_file){
              $result->success = false;
              $result->message = '檔案上傳失敗！請檢查格式是否正確';
              $result = json_encode($result);
              echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
              die();
          }

          if ($img_file->getSize() > 5 * 1024 * 1024) {
              $result->success = false;
              $result->message = '上傳檔案需小於 5 MB';
              $result = json_encode($result);
              echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
              die();
          }
          if (!in_array($img_file->getMimeType(),['image/jpeg','image/png','image/gif'])) {
              $result->success = false;
              $result->message = '只允許上傳 JPEG / PNG / GIF 格式檔案';
              $result = json_encode($result);
              echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
              die();
          }
          $destinationPath = public_path() . '/assets/photos/imageUpload/';
          if (!file_exists($destinationPath)) {
              mkdir($destinationPath, 0777);
          }
          $size = $img_file->getSize();
          $mimetype = $img_file->getMimeType();
          $resolution = getimagesize($img_file->getRealPath());
          $image_key = getFileKey();
          while (file_exists($destinationPath . $image_key . '.' . $img_file->getClientOriginalExtension())) {
              $image_key = getFileKey();
          }
          $filename_tmp = $image_key . '_tmp.' . $img_file->getClientOriginalExtension();
          $location = $destinationPath . $filename_tmp;
          // $img_file->move($destinationPath, $filename_tmp);

          $filename = str_replace('_tmp.' . $img_file->getClientOriginalExtension(), '.' . $img_file->getClientOriginalExtension(), $filename_tmp);
          $img_file->move($destinationPath, $filename);

          $result->success = true;
          $result->key[] = $filename;
          $result->name[] = $img_file->getClientOriginalName();
          
        }
        
        $result = json_encode($result);
        
          echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
        //unlink($location);
        die();
      
    }

    public function store()
    {
      
      $file_name = request()->input('file_name');
      $photo_key = request()->input('photo_key');
      $photos = explode(",",$photo_key);
      $names = explode(",",$file_name);

      foreach($photos as  $photo)
      {

        $image = new Image();
          
          if($photo_key){
            $image->imageUpload( public_path().'/assets/photos/imageUpload/'.$photo,null,$photo);
          }
      }
      $imageUpload  = new ToolService();
      $fileIds[] = $imageUpload->ImageUpload($photos,$names);

      $all_photo = ['photos' => $photos, 'names' =>$names];

          
       return redirect()->route('backend-tool-imageUpload')->with($all_photo);
 
    }

    public function searchPhoto()
    {
        $start = request()->input('start');
        $end = request()->input('end');
        $name = request()->input('search_name');
        // dd($name);
        $images = cms_file::select('name','url');
        if($start and $end and $name) {
            $images = $images->where('name','like','%'.$name.'%')->where('created_at', '>=', $start)->where('created_at', '<', date('Y-m-d', strtotime($end . ' + 1day')));
        }else if($start and !$end and $name){
            $images = $images->where('name','like','%'.$name.'%')->where('created_at', '>=', $start);
        }else if(!$start and $end and $name){
            $images = $images->where('name','like','%'.$name.'%')->where('created_at', '<', date('Y-m-d', strtotime($end . ' + 1day')));
        }else if($start and !$end and !$name){
          $images = $images->where('created_at', '>=', $start);
        }else if(!$start and $end and !$name){
            $images = $images->where('created_at', '<', date('Y-m-d', strtotime($end . ' + 1day')));
        }else if($start and $end and !$name){
            $images = $images->where('created_at', '>=', $start)->where('created_at', '<', date('Y-m-d', strtotime($end . ' + 1day')));
        }else if(!$start and !$end and $name){
            $images = $images->where('name','like','%'.$name.'%');
        }else{
            $images = $images->where('created_at', '>=', date('Y-m-d'))->where('created_at', '<', date('Y-m-d', strtotime(date('Y-m-d') . ' + 1day')));
        }
        $images = $images->orderBy('created_at','desc')->get();
        $all_images['images'] = $images->toArray();
        return redirect()->route('backend-tool-imageUpload')->with($all_images);
    }
    


}