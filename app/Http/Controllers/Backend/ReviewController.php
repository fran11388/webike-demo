<?php
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Feed;
use Everglory\Models\Review;

class ReviewController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-review');
        $component->function_name = '評論';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function index($type = null)
    {
        $this->view_component->title = '已審核';
        $this->view_component->seo('title', '已審核');
        switch ($type){
            case 'validate':
                $this->view_component->title = '待審核';
                $this->view_component->seo('title', '待審核');
                $status = 0;
                break;
            case 'violation':
                $this->view_component->title = '不合格評論';
                $this->view_component->seo('title', '不合格評論');
                $status = 9;
                break;
            default :
                $status = 1;
                break;
        }
//        dd($type);

        $reviews = Review::with('customer')->where('view_status',$status)->orderBy('id','desc')->paginate(20);

        $this->view_component->reviews = $reviews;
        $this->view_component->active = [0 => '關閉',1 => '開啟'];
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();

        return view('backend.pages.review.index', (array)$this->view_component);
    }

    public function postReview()
    {
        $active_type = \Request::get('active_type');
        $review_ids = \Request::get('check');
        $choose_reviews = Review::whereIn('id',$review_ids)->get();

        foreach($choose_reviews as $review){

            switch($active_type){
                case 'validate':
                    $review->view_status = 1;
                    $review->deleted_at = null;
                    $review->save();
                    break;
                case 'disable':
                    $review->view_status = 9;
                    $review->deleted_at = date('Y-m-d h:i:s');
                    $review->save();
                    break;
                case 'delete':
                    $this->delete($review_ids);
                    break;
                default:
                    # code...
                    break;
            }
        }
        return \Redirect::back();
    }

    public function delete($ids)
    {
        Review::whereIn('id',$ids)->forceDelete();
        return \Redirect::back();
    }
}
