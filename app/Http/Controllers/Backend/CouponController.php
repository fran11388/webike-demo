<?php 
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Coupon;
use Everglory\Models\Coupon\Base as CouponBase;
use Everglory\Models\Coupon\Base;
use Everglory\Models\Coupon\History;
use Everglory\Models\Customer;
use Faker\Provider\zh_TW\DateTime;

class CouponController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-coupon');
        $component->function_name = 'COUPON';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function getIndex()
    {
        $this->view_component->coupons = CouponBase::orderBy('id','desc')->paginate(10);

        $this->view_component->seo_title = 'List';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
    	return view('backend.pages.coupon.index', (array)$this->view_component);
    }

    public function getCreate(){

        $this->view_component->coupon = new CouponBase();
        $this->view_component->seo_title = 'Edit';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
        return view('backend.pages.coupon.edit', (array)$this->view_component);
    }

    public function postCreate(){
        $coupon = new CouponBase();
        $rules =  array(
            'code' => 'max:50',
            'name' => 'required|max:10',
            'time_limit' => 'required|numeric',
            'time_limit_unit' => 'required',
            'discount' => 'required|numeric|min:0.01',
        );

        $messages = array();

        $validator = \Validator::make( request()->all() ,$rules , $messages );
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $coupon->code = request()->input('code');
        $coupon->name = request()->input('name');
        $coupon->time_limit = request()->input('time_limit');
        $coupon->time_limit_unit = request()->input('time_limit_unit');
        $coupon->discount_type = request()->input('discount_type');

        if (request()->input('period')){
            $coupon->period = request()->input('period');
        }

        $coupon->discount = request()->input('discount');
        $coupon->save();

        return redirect()->route('backend-coupon');
    }


    public function getEdit($id){

        $this->view_component->coupon = CouponBase::findOrFail($id);
        $this->view_component->seo_title = 'Edit';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
        return view('backend.pages.coupon.edit', (array)$this->view_component);
    }


    public function postEdit($id){
        $coupon = CouponBase::findOrFail($id);

        $rules =  array(
            'code' => 'unique:eg_zero.coupon_base,code,' . $id . '|max:50',
            'name' => 'required|max:10',
            'time_limit' => 'required|numeric',
            'time_limit_unit' => 'required',
            'discount' => 'required|numeric|min:0.01',
        );

        $messages = array();
        $validator = \Validator::make( request()->all() ,$rules , $messages );
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        $coupon->code = request()->input('code');
        $coupon->name = request()->input('name');
        $coupon->time_limit = request()->input('time_limit');
        $coupon->time_limit_unit = request()->input('time_limit_unit');
        $coupon->period = request()->input('period');
        $coupon->discount = request()->input('discount');
        $coupon->save();

        return redirect()->route('backend-coupon');
    }

    public function getFilter()
    {
        $customers = Customer::with('role');

        if ( request()->get('realname')){
            $customers = $customers->where( 'realname',  'like' , '%' . request()->get('realname') . '%' );
        }

        if ( request()->get('id_begin') ){
            $customers = $customers->where( 'customers.id',  '>=' ,request()->get('id_begin') );
        }

        if ( request()->get('id_end') ){
            $customers = $customers->where( 'customers.id',  '<=' , request()->get('id_end') );
        }

        if ( request()->get('email')){
            $customers = $customers->where( 'email',  'like' , '%' . request()->get('email') . '%' );
        }

        if ( request()->get('role_id')){
            $customers = $customers->where( 'role_id',  request()->get('role_id') );
        }

        if ( request()->get('birthday-begin') ){
            $customers = $customers->whereRaw( "DATE_FORMAT(birthday,'%m-%d')  >=  '".request()->get('birthday-begin')."'");
        }

        if ( request()->get('birthday.to') ){
            $customers = $customers->whereRaw( "DATE_FORMAT(birthday,'%m-%d')  <=  '".request()->get('birthday-end')."'");
        }

        if ( request()->get('coupon_get') ){
            if(request()->get('coupon_get') == 'yes'){
                $customers = $customers
                    ->leftJoin('coupons', function($leftJoin)
                    {
                        $leftJoin->on('customers.id', '=', 'coupons.customer_id')
                            ->where('coupons.code','=','BIRTHDAY-COUPON')
                            ->where('coupons.created_at','>',\DB::Raw("DATE_ADD(now(),INTERVAL -8 MONTH)"));
                    })
                    ->whereNotNull('coupons.id')
                    ->groupBy('customers.id');
            }else if(request()->get('coupon_get') == 'no'){
                $customers = $customers
                    ->leftJoin('coupons', function($leftJoin)
                    {
                        $leftJoin->on('customers.id', '=', 'coupons.customer_id')
                            ->where('coupons.code','=','BIRTHDAY-COUPON')
                            ->where('coupons.created_at','>',\DB::Raw("DATE_ADD(now(),INTERVAL -8 MONTH)"));
                    })
                    ->whereNull('coupons.id')
                    ->groupBy('customers.id');
            }
        }

        $customer_ids = implode(',',$customers->select('customers.*')->pluck('customers.id')->toArray());

        $customers = $customers->select('customers.*')->orderBy('id','desc')->paginate(20);

        $this->view_component->customer_ids = $customer_ids;
        $this->view_component->customers = $customers;

        $this->view_component->seo_title = 'Filter';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
        return view('backend.pages.coupon.filter', (array)$this->view_component);
    }

    public function postHistory()
    {
        $this->insertHistoryAndCoupon(request()->all());

        return redirect()->route('backend-coupon-filter');

    }

    private function insertHistoryAndCoupon($request)
    {
        $customer_ids = $request['customer_ids'];
        $customer_ids = str_replace( " " , "" , $customer_ids);
        $couponBase = Base::findOrFail( $request['couponbase_id'] );
        $couponHistory = new History();
        $couponHistory->base_id = $couponBase->id;
        $couponHistory->tag = date('Y').'-'.(int)date('m');
        $couponHistory->name = $couponBase->name;
        $couponHistory->customer_ids = $customer_ids;
        $couponHistory->discount = $couponBase->discount;
        $couponHistory->time_limit = $couponBase->time_limit;
        $couponHistory->time_limit_unit = $couponBase->time_limit_unit;
        $couponHistory->save();

        $this->inserCoupon($customer_ids,$couponBase,$couponHistory);
    }

    private function inserCoupon($customer_ids,$couponBase,$couponHistory)
    {
        $customers = \Everglory\Models\Customer::whereIn( 'id' , explode( ',' , $customer_ids)  )->get();

        foreach ($customers as $customer) {
            $coupon = new Coupon();
            $coupon->customer_id = $customer->id;
            $coupon->uuid = (string)\Uuid::generate(1);
            $coupon->name = $couponBase->name;
            $coupon->code = $couponBase->code;
            $coupon->discount = $couponBase->discount;
            $coupon->history_id = $couponHistory->id;
            $coupon->discount_type = $couponBase->discount_type;

            $dt = new \DateTime();
            $dt->add(\DateInterval::createFromDateString('+'.$couponBase->time_limit.' '.$couponBase->time_limit_unit));
            $coupon->expired_at = $dt->format('Y-m-d');
            $coupon->save();

        }
    }
}