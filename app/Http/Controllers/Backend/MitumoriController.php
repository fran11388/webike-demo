<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/9/11
 * Time: 下午 03:09
 */

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Ecommerce\Service\Backend\PriceCalculation;
use Illuminate\Http\Request;
use Everglory\Models\Mitumori;
use Everglory\Models\Mitumori\Nouki;
use Everglory\Models\Mitumori\Item;
use Ecommerce\Service\MitumoriService;
use Ecommerce\Core\Disp\Program as dispService;

class MitumoriController extends BackendController
{
    protected $mitumoriService;
    protected $dispService;

    public function __construct(SimpleComponent $component, MitumoriService $mitumoriService, dispService $dispService)
    {
        parent::__construct($component);
        $component->base_url = route('backend-mitumori');
        $component->function_name = '未登錄查詢回覆';
        $component->setBreadcrumbs($component->function_name, $component->base_url);

        $this->mitumoriService = $mitumoriService;
        $this->dispService = $dispService;
    }

    public function getIndex()
    {
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();
        $this->view_component->collection = Mitumori::with('customer')->orderBy('status', 'asc')->orderBy('request_date', 'desc')->paginate(20);
//        return \View::make('backend.page.mitumori.index',compact('collection'));
        return view('backend.pages.mitumori.index', (array)$this->view_component);
    }

    public function postDelete(Request $request)
    {
        $ids = $request->get('ids');
        $ids = explode('_', $ids);
        $collection = Mitumori::whereIn('id', $ids)->delete();
        return \Redirect::back();
    }

    public function postIndex(Request $request)
    {
        if ($request->ajax()) {
            $mitu = Mitumori::findOrFail($request->get('id'));
            $mitu->jp_order_code = $request->get('jp_order_code');
            $mitu->timestamps = false;
            $mitu->save();
            return $mitu->jp_order_code;
        }
    }

    public function getEdit($id)
    {
        $this->view_component->function_name = '未登錄商品建立';
        $this->view_component->setBreadcrumbs($this->view_component->function_name, $this->view_component->base_url);
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();
        $this->view_component->entity = Mitumori::findOrFail($id);
        $this->view_component->manufacturers = \Everglory\Models\Manufacturer::all();
//        $nouki = array(''=>'請選擇交期') + Nouki::where('nouki_code','not like','%A')->pluck('nouki_text','nouki_code') + Nouki::where('nouki_code','like','%A')->pluck('nouki_text','nouki_code');

//        $nouki=(array(''=>'請選擇交期'));
        $this->view_component->nouki = Nouki::pluck('nouki_text', 'nouki_code')->all();


//        return \View::make('backend.page.mitumori.edit',compact('entity','nouki','manufacturers'));
        return view('backend.pages.mitumori.edit', (array)$this->view_component);
    }

    public function postItem(Request $request, $id)
    {
        $item = Item::findOrFail($id);

        /*except _token, because laravel 5's CSRF policy*/
        foreach ($request->except('custom_manufacturer', 'rate', '_token') as $key => $value) {
            $item->$key = $value;
        }
        if ($custom_manufacturer = $request->has('custom_manufacturer')) {
            $item->custom_manufacturer = 1;
            $item->product_manufacturer_id = 0;
        } else {
            $item->custom_manufacturer = 0;
            $item->product_manufacturer_name = null;
        }
        if ($item->save()) {
            echo $item->product_check;
            exit();
        } else {
            return false;
        }
    }

    public function postEdit($id)
    {
        $entity = Mitumori::findOrFail($id);

        $count = $entity->items()->where('product_check', '=', 0)->count();
        if ($count) {
            return \Redirect::back()->with('danger', '還有項目還沒確認！');
        }

        $items = $entity->items()->where('product_nouki', 'not like', 'A%')->get();
        foreach ($items as $key => $item) {
            $object = new \stdClass();
            $object->name = $item->product_name;
            $object->model_number = $item->product_code;
            $object->manufacturer_id = $item->product_manufacturer_id;
            $object->manufacturer_name = $item->product_manufacturer_name;
            $object->custom_manufacturer = $item->custom_manufacturer;
            $object->category_id = 1627;
            $object->price = $item->product_price;
            $object->cost = $item->product_cost;
            $object->customer_id = $entity->customer_id;
            $object->item_id = $item->id;
            $this->mitumoriService->add($object); //新增商品佇列
        }

        $warehouseQueues = $this->mitumoriService->get();  //產出商品API
        if (count($items) > 0) {
            if(count($warehouseQueues) == 0){
                return \Redirect::back()->with('danger', '失敗！!');
            }
            //product_id 對應回填
            foreach ($warehouseQueues as $warehouseQueue) {
                if ($warehouseQueue->category_id == 1627) {
                    $mitumoriItem = Mitumori\Item::where('id', $warehouseQueue->item_id)->first();
                    if ($mitumoriItem) {
                        $mitumoriItem->product_id = $warehouseQueue->product_id;
                        $mitumoriItem->save();
                    }
                }
            }
        } else {
            // nothing to do.
        }

        //站內訊息通知
        $this->dispService->addDisp($entity);
        //email通知
        $entity->status = 1;
        $entity->response_date = date('Y-m-d H:i:s');
        $entity->timestamps = false;
        $entity->read = 0;
        $entity->save();
        $Wmail=new \Ecommerce\Core\Wmail;
        $Wmail->mitumori($entity);
        return \Redirect::back()->with('success', '已成功回覆!');

    }

    public function changeRate(){
        if(\Request::ajax()){
            $priceCalculation = new PriceCalculation;
            $result = $priceCalculation->setRate(\Request::get('rate'))
                ->setCustomer(\Request::get('role_id'));
            $discount = \Request::get('discount');
            if(!empty($discount)){
                $result = $result->setDiscount($discount);
            }
            $result = $result->getGroupBuy(\Request::get('price'));

            return json_encode($result);
        }
    }
}