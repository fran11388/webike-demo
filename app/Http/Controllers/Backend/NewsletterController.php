<?php
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Ecommerce\Core\Wmail;
use Ecommerce\Service\Newsletter\InstallerService;
use Ecommerce\Service\CustomerService;
use Everglory\Models\Customer;
use Everglory\Models\Email\Newsletter\Item;
use Everglory\Models\Email\Newsletter\Queue;
use Everglory\Models\Newsletter;
use Illuminate\Http\Request;
use Everglory\Models\base;
use Excel;
use DB;

class NewsletterController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-newsletter');
        $component->function_name = '電子報';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function index()
    {
        $newsletters = Newsletter::orderBy('id','desc')->paginate(20);

        $this->view_component->newsletters = $newsletters;
        $this->view_component->active = [0 => '關閉',1 => '開啟'];
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();

        return view('backend.pages.newsletter.index', (array)$this->view_component);
    }

    public function insertNewsletter($newsletter_id = null)
    {
        if($newsletter_id){
            $newsletter = Newsletter::where('id','=',$newsletter_id)->first();
            $this->view_component->newsletter = $newsletter;
            $this->view_component->seo('title', '編輯特輯');
        }else{
            $this->view_component->newsletter = '';
            $this->view_component->seo('title', '新增特輯');
        }

        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        return view('backend.pages.newsletter.newsletter.create', (array)$this->view_component);
    }

    public function postInsertNewsletter(Request $request,$newsletter_id = null)
    {
        $newsletter = new Newsletter;
        if($newsletter_id){
            $newsletter = Newsletter::where('id','=',$newsletter_id)->first();
        }
        $newsletter->name = $request->input('name');
        $newsletter->title = $request->input('title');
        $content = $request->input('content');
        $content = str_replace('{{%24', '{{$', $content);
        $newsletter->content = $content;
        $newsletter->save();

        return redirect()->route('backend-newsletter');
    }

    public function getQueue()
    {

        $newsletter_queues = Queue::Orderby('id','desc')->paginate(20);


        $this->view_component->status = ['尚未開始','準備發送中','正在發送中','發送完成'];
        $this->view_component->newsletter_queues = $newsletter_queues;
        $this->view_component->seo('title', '電子報眝列');
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        return view('backend.pages.newsletter.queue.queue', (array)$this->view_component);
    }

    public function getQueueInsert($newsletter_queue_id = null)
    {
        $this->view_component->newsletter_queue = null;
        if($newsletter_queue_id){
            $newsletter_queue = Queue::with(['items', 'items.customer'])->where('id','=',$newsletter_queue_id)->first();
            $this->view_component->newsletter_queue = $newsletter_queue;
        }
        $newsletters = Newsletter::orderBy('id','desc')->get();
        $this->view_component->newsletters = $newsletters;

        $this->view_component->status = ['尚未啟動','準備發送中'];
        $this->view_component->methods = ['自訂收件者','寄給電子報訂閱者'];
        $this->view_component->seo('title', '新增眝列');
        $this->view_component->setBreadcrumbs('電子報眝列', route('backend-newsletter-queue'));
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        return view('backend.pages.newsletter.queue.insert', (array)$this->view_component);
    }

    public function postQueueInsert(Request $request,$queue_id = null)
    {
        $customer_ids = $request->get('customer_ids');
        $receiveGroup = [];
        if($customer_ids){
            $receiveGroup = Customer::select('id')->whereIn('id', $customer_ids)->where('newsletter', 1)->get();
            $receiveGroup = $receiveGroup->pluck('id')->toArray();
        }
        $newsletter = Newsletter::where('id','=',$request->input('select_newsletter'))->first();
        if($queue_id){
            $queue = Queue::where('id','=',$queue_id)->first();
            $queue->status_id = $request->input('select_status');
        }else{
            $queue = new Queue;
            $queue->status_id = 0;
        }
        $queue->title = $newsletter->title;
        $queue->content = $newsletter->content;
        $queue->newsletter_id = $request->input('select_newsletter');
        $queue->send_start_time = $request->input('begin_date');
        $queue->send_method = $request->input('select_method');
        $queue->save();

        if(count($receiveGroup)){
            if($queue->send_method == 0){
                $queue->items()->delete();
            }
            $this->setQueueItem($queue->id, $receiveGroup);
        }

        return redirect()->route('backend-newsletter-queue');
    }

    public function delete(Request $request)
    {
        if($request->input('type') == 'queue'){
            Queue::whereIn('id',$request->input('check'))->forceDelete();
        }elseif($request->input('type') == 'newsletter'){
            Newsletter::whereIn('id',$request->input('check'))->forceDelete();
        }
        return \Redirect::back();
    }

    public function setNewsletterCustomer()
    {
        ini_set('memory_limit','2048M');
        set_time_limit(28800);
        $sending_queue = Queue::where('status_id',2)->whereNull('send_end_time')->take(5)->get();
        foreach ($sending_queue as &$queue) {
            $wmail = new Wmail;
            $wmail->newsletter($queue);
            $count_check = $queue->items()->whereNull('sent_at')->count();
            if($count_check == 0){ //send finish
                $queue->status_id = 3;
                $queue->send_end_time = date('Y-m-d H:i:s');
                $queue->save();
            }
        }

        $send_queue = Queue::where('send_start_time','<',\DB::Raw('now()'))->where('status_id',1)->get();
        foreach($send_queue as &$queue){
            if($queue->send_method == 1) {
                $this->setQueueItem($queue->id);
            }

            $campaign = date("Ymd").'_mailmag';
            $para = '?utm_source=webikeMailmagazine&utm_medium=email&utm_content=mailmag&utm_campaign=' . $campaign;
            $dom = new \DomDocument();
            $dom->loadHTML(mb_convert_encoding($queue->content, 'HTML-ENTITIES', 'UTF-8'));
            $anchors = $dom->getElementsByTagName('a');
            foreach ($anchors as $a) {
                if ($a->hasAttribute('href')) {
                    $href = $a->getAttribute('href');
                    if(false !== strpos($href, 'webike.tw')){
                        if(false !== strpos($href, '#')) {
                            $hrefs = explode('#',$href);
                            $a->setAttribute('href',$hrefs[0] . $para . '#' .  $hrefs[1] );
                        }else if(false !== strpos($href, '?')) {
                            $hrefs = explode('?',$href);
                            $a->setAttribute('href',$hrefs[0] . $para . '&' .  $hrefs[1] );
                        }else{
                            $a->setAttribute('href',$href . $para);
                        }
                    }

                }
            }
            $queue->content_tracking = $dom->saveHTML();

            $queue->status_id = 2;
            $queue->save();
        }

    }

    private function setQueueItem($queue_id, $customer_ids = [])
    {
        if(!is_array($customer_ids)){
            throw new \Exception('This function need to use array in customer_ids case.');
        }
        if(!count($customer_ids)){
            $customer_ids = Customer::select('customers.id')
                ->join('ec_dbs.email_newsletter_queue_items as enq','enq.customer_id','=','customers.id')
                ->where('customers.newsletter','=',1)
                ->whereNotNull('enq.get_open')
                ->groupBy('customers.id')
                ->get()->pluck('id')->toArray();
//            $customer_ids = Customer::select('id')->where('newsletter', 1)->get()->pluck('id')->toArray();
        }else{
            $customer_ids = Customer::select('id')->whereIn('id', $customer_ids)->where('newsletter', 1)->get()->pluck('id')->toArray();
        }
        foreach ($customer_ids as $customer_id) {
            $item = new Item;
            $item->customer_id = $customer_id;
            $item->queue_id = $queue_id;
            $item->save();
        }
    }


    public function getNewsEdit()
    {
        // $customerGifts =  CustomerGifts::with('gift.origProduct')->select('customer_gifts.id as id','customer_gifts.gift_id','customer_gifts.status')->join('gifts as gf','customer_gifts.gift_id','=','gf.id')->where('customer_id','=',$customer_id)->where('type','=',GiftService::PRODUCT)

        $base =  DB::connection('ec_dbs')->table('email_newsletter_base')->first();
        $base_blade = $base->blade;
                // dd($base_blade); 

        $this->view_component->give_point_url = 'test';
        $this->view_component->open_image = 'img'; 
        $html = view( $base_blade, (array)$this->view_component)->render();
        $this->view_component->html = $html;

        return view( 'backend.pages.newsletter.newEdit.edit', (array)$this->view_component);
    }

    public function postNewsEdit(Request $request)
    {   
    
        if($request)
        {
            $file = $request->file('upfile');
            // $request->file('upfile')->getoriginalName();
            $filename = $request->file('upfile')->getClientOriginalName();
            // dd($filename);
            $file->move(storage_path('backend/newsletter/upload'),$filename);
            
            EXCEL::load(storage_path('backend/newsletter/upload/'.$filename), function($excel)
            { 
                $sheet_names = $excel->getSheetNames();
                $sheet_name= array_first($sheet_names);
                $excel->sheet($sheet_name, function($sheet) use($sheet_name){
                    $total_row = $sheet->getHighestRow();
                    $columns = array("A","B","C","D","E","F");
                    foreach($columns as $column){
                        for($row = 1;$row <= $total_row;$row++){
                               $value = $sheet->getCell($column.$row)->getValue();
                               $name[$column.$row] = $value;
                        }
                    }

                    $this->view_component->base = $name;
                    view( 'backend.pages.newsletter.newEdit.edit',(array)$this->view_component)->render();
                    

                });


            });

    

        }else{

            return 'not file';
        }
       
    }

    public function getInstall()
    {
        

        $this->view_component->function_name = '匯入電子報';
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        
        
        $this->view_component->html = "</br>";
        return view( 'backend.pages.newsletter.install.index', (array)$this->view_component);
        // dd($this->view_component);
    }

    public function install()
    {

        $contents = [];
        $file = request()->file('file');
        $installerService = new InstallerService();
        $datas = $installerService->getColsByExcelFile($file);
        
        foreach ($datas as $key => $data) {
            $installer = $installerService->getInstaller($data['0']);
            $contents[] = $installer->getHtml($data);
            
        }
        
   
        $html = implode('', $contents);
 
        $this->view_component->html = $html;

        return view('backend.pages.newsletter.install.preview',(array)$this->view_component);
    }

    public function intoNewsletter()
    {
        $content = request()->get('content');
        if($content){
            $installerService = new InstallerService();
            $newsletter = $installerService->addNewsletterByContent($content);
            if($newsletter){
              $newsletter_id = $newsletter->id;
               return redirect()->route("backend-newsletter-insert",['id' => $newsletter_id]);
            }
        }
    }


}
