<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Promotion;
use Ecommerce\Service\promoCms\PromoService;
use Ecommerce\Core\Image;
use Ecommerce\Service\Tool\ToolService;
use Everglory\Models\cms_file;
use Illuminate\Http\Request;
use Ecommerce\Service\Backend\GuessService;

class GuessController extends BackendController
{
    private $errors = array();
    protected $guessService;

    public function __construct(SimpleComponent $component, GuessService $guessService)
    {
        parent::__construct($component);
        $component->base_url = route('backend-guess');
        $component->function_name = 'Guess';
        $component->setBreadcrumbs($component->function_name, $component->base_url);

        $this->guessService = $guessService;
    }

    public function getIndex()
    {
//        $this->view_component->seo('66666', $this->view_component->function_name);
//        $this->view_component->render();
//        return view('backend.pages.guess.index', (array)$this->view_component);
        return redirect()->route('backend-guess-list');
    }

    public function postIndex(Request $request)
    {
        if ($request->hasFile('mainbanner')) {
            $mainbanner = $request->file('mainbanner');
            $url = $this->guessService->fileUpload($mainbanner, 'webikeguess750');
            //dd($url);
        }
        if ($request->hasFile('tit1')) {
            $tit1 = $request->file('tit1');
            $url = $this->guessService->fileUpload($tit1, 'webikeguess_tit01');
        }
        if ($request->hasFile('tit2')) {
            $tit2 = $request->file('tit2');
            $url = $this->guessService->fileUpload($tit2, 'webikeguess_tit02');
        }
        return view('backend.pages.guess.index', (array)$this->view_component);
    }

    public function getList(Request $request)
    {
        $this->view_component->seo('66666', $this->view_component->function_name);
        $this->view_component->render();

        $this->view_component->request = $request;

        $fillters = $request->all();
        $guesses = $this->guessService->getList($fillters);
        $this->view_component->guesses = $guesses;

        return view('backend.pages.guess.list', (array)$this->view_component);

    }

    public function postDelete(Request $request)
    {
        $id_group = $request->get('delete_id');
        $this->guessService->delete($id_group);

        return \Redirect::route('backend-guess-list');
    }

    public function getEdit($guess_id = NULL)
    {
        $guess = $this->guessService->getGuessById($guess_id);
        $this->view_component->guess = $guess;

        return view('backend.pages.guess.edit', (array)$this->view_component);
    }

    public function postEdit(Request $request)
    {
        $result = $this->guessService->createOrUpdateGuess($request);

        if (count($result['errors']) > 0) {
            return \Redirect::back()->with('errors', $this->errors);
        }

        if ($result['update']) {
            return \Redirect::route('backend-guess-edit', array('guess_id' => $result['guess']->id));
        }

        return \Redirect::route('backend-guess-list');
    }

    public function getLog(Request $request)
    {
        $this->view_component->request=$request;

        $fillters = $request->all();
        $result = $this->guessService->getLogData($fillters);

        if (count($result['errors']) > 0) {
            return \Redirect::back()->with('errors', $result['errors']);
        }

        if ($request->has('csv_output') and $request->get('csv_output') == 'true') {
            $data = $result['attends']->get();
            $title = array('客戶編號', '姓名', 'E-mail', '答題時間', '會員答案');
            $this->view_component->main_str = $this->csvOutput($data, $title);
            $this->view_component->filename = 'WebikeGuess答題會員列表';

            return view('backend.pages.csv_output', (array)$this->view_component);
        }

        $this->view_component->attend_counts = $result['attends']->count();
        $this->view_component->complete_counts = $result['completes']->get()->count();
        $this->view_component->attends = $result['attends']->orderBy('created_at', 'DESC')->paginate($result['limit']);

        return view('backend.pages.guess.log', (array)$this->view_component);
    }

    private function csvOutput($data, $title)
    {
        $main = "\"";
        $main = $main . implode("\",\"", $title);
        $main .= "\"\n";
        foreach ($data as $num => $item) {
            $customer = $item->customer;
            $main = $main . "\"{$customer->id}\",\"{$customer->realname}\",\"{$customer->email}\",\"{$item->created_at}\",\"{$item->answer}\"\n";
        }

        //dd($main);
        return $main;
    }

    public function test() /*test jqpuzzle work*/
    {
        return view('backend.pages.guess.test');
    }


}