<?php 
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;

class SettingController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend');
        $component->function_name = 'Setting';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function calendar()
    {
        $this->view_component->events = \Everglory\Models\Calendar\Event::all();
        $this->view_component->seo_title = 'Calendar';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
    	return view('backend.pages.setting.calendar', (array)$this->view_component);
    }

    public function postCalendar()
    {
        $data = request()->input('data');
        switch (request()->input('action')) {
            case 'create_event':
                \Everglory\Models\Calendar\Event::create($data);

                break;
            case 'add_event':
                $event = new \Everglory\Models\Calendar;
                $event->title = $data['title'];
                $event->description = $data['description'];
                $event->icon = $data['icon'];
                $event->className = $data['className'];
                $event->allDay = 1;
                $event->start = $data['start'];
                $event->end = date('Y-m-d',strtotime($data['end'] . "+1 days"));;
                $event->event_id = $data['eventid'];
                if($data['eventdel']){
                    \Everglory\Models\Calendar\Event::find($data['eventid'])->delete();
                }
                $event->save();
                return $event->id;
                break;
            case 'edit':
                $event = \Everglory\Models\Calendar::find($data['id']);
                $event->title = $data['title'];
                $event->start = $data['start'];
                $event->end = $data['end'];
                $event->save();
                break;
            case 'delete':
                $event = \Everglory\Models\Calendar::find($data['id']);
                $event->delete();
                break;
            default:
                # code...
                break;
        }
    }

    public function postEvent()
    {
        if(request()->ajax()){
            // $start = date('Y-m-d',request()->input('start'));
            // $end = date('Y-m-d',request()->input('end'));
            $start = request()->input('start');
            $end = request()->input('end');
            $events = \Everglory\Models\Calendar::where('start', '<', $end)->where('end', '>', $start)->get();
            foreach ($events as $event) {
                $data[] = array(
                    'id' => $event->id,
                    'title' => $event->title,
                    'description' => $event->description,
                    'start' => $event->start,
                    'end' => $event->end,
                    'allDay' => $event->allday,
                    'className' => $event->className,
                    'icon' => $event->icon,
                );
            }
            echo json_encode($data);
        }
    }
}