<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/3
 * Time: 下午 02:00
 */

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Propaganda;
use Illuminate\Http\Request;

class PropagandaController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-propagandas');
        $component->function_name = '行銷CMS';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function getIndex()
    {

        $propagandas = Propaganda::with('type', 'position', 'audience', 'bannedPaths')
            ->orderBy('updated_at','DESC')
            ->get();

        $this->view_component->propagandas = $propagandas;
        $this->view_component->render();

        return view('backend.pages.propaganda.index', (array)$this->view_component);
    }

    public function deleteIndex(){

        $ids=explode(',',\request()->get('ids'));
        Propaganda::whereIn('id',$ids)->delete();
        return 'ok';
    }


    public function getEdit($id)
    {


        $this->view_component->propaganda = Propaganda::findOrFail($id);

        $this->view_component->types = Propaganda\Type::get();
        $this->view_component->positions = Propaganda\Position::get();
        $this->view_component->audiences = Propaganda\Audience::get();

        $this->view_component->seo_title = 'Edit';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
        return view('backend.pages.propaganda.edit', (array)$this->view_component);
    }


    public function postEdit($id)
    {
        \DB::transaction(function () use ($id) {
            $propaganda = Propaganda::findOrFail($id);
            $propaganda->type_id = \request()->get('type_id');
            $propaganda->start_time = \request()->get('start_time');
            $propaganda->end_time = \request()->get('end_time');
            $propaganda->pic_path = \request()->get('pic_path');
            $propaganda->text = \request()->get('text');
            $propaganda->link = \request()->get('link');
            $propaganda->position_id = \request()->get('position_id');
            $propaganda->audience_id = \request()->get('audience_id');
            $propaganda->save();

            Propaganda\Banned\Path::where('propaganda_id', $id)->delete();
            $banned_paths = explode("\r\n", \request()->get('banned_paths'));

            foreach ($banned_paths as $banned_path) {
                if($banned_path=='') continue;
                $path = new Propaganda\Banned\Path();
                $path->original_url = $banned_path;
                $path->path = parse_url($banned_path, PHP_URL_PATH);
                $path->propaganda_id = $propaganda->id;
                $path->save();
            }
        });

        return redirect()->route('backend-propagandas');
    }

    public function getCreate()
    {

        $this->view_component->seo_title = 'Edit';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();

        $this->view_component->types = Propaganda\Type::get();
        $this->view_component->positions = Propaganda\Position::get();
        $this->view_component->audiences = Propaganda\Audience::get();


        return view('backend.pages.propaganda.create', (array)$this->view_component);
    }

    public function postCreate()
    {
        \DB::transaction(function () {
            $propaganda = new Propaganda;
            $propaganda->type_id = \request()->get('type_id');
            $propaganda->start_time = \request()->get('start_time');
            $propaganda->end_time = \request()->get('end_time');
            $propaganda->pic_path = \request()->get('pic_path');
            $propaganda->text = \request()->get('text');
            $propaganda->link = \request()->get('link');
            $propaganda->position_id = \request()->get('position_id');
            $propaganda->audience_id = \request()->get('audience_id');
            $propaganda->save();

            $banned_paths = explode("\r\n", \request()->get('banned_paths'));
            foreach ($banned_paths as $banned_path) {
                if($banned_path=='') continue;
                $path = new Propaganda\Banned\Path();
                $path->original_url = $banned_path;
                $path->path = parse_url($banned_path, PHP_URL_PATH);
                $path->propaganda_id = $propaganda->id;
                $path->save();
            }
        });
        return redirect()->route('backend-propagandas');

    }
}