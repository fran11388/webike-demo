<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Ecommerce\Service\CustomerService;
use Everglory\Models\Disp;
use Ecommerce\Service\Disp\DispService;
use Everglory\Models\Customer;

class DispController extends BackendController
{

   public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-disp');
        $component->function_name = '通知小視窗訊息發送';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function index()
    {
        $disp = Disp::orderBy('id','desc')->paginate(20);
        $this->view_component->disp = $disp;
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();

        return view('backend.pages.disp.index', (array)$this->view_component);
    }

    public function postDispA()
    {   


        $user_id = \Auth::check() ? \Auth::user()->id : null;
        $reqcustomer_id = request()->get('customer_ids');
        $customer_ids = isset($reqcustomer_id) ? request()->get('customer_ids') : [$user_id];

            
            $select_method = request()->get('select_method');
            $info = request()->get('info');
            $link = request()->get('link');


            $disp = [ 'info' => $info, 'link' => $link, 'customer_ids' => $customer_ids,'select_method' => $select_method];

            $DispService = new DispService;
            $DispService->TestDisp($disp);



 
        return json_encode($disp);
        
    }

    public function postDisp()
    {   
        $select_method = request()->get('select_method');
        if($select_method == 1){
            ini_set("memory_limit","3072M");
            set_time_limit(0);
            $customer_ids = Customer::orderBy('id')->pluck('id');
        }else{
            $user_id = \Auth::check() ? \Auth::user()->id : null;
            $customer_ids = request()->get('customer_ids') == null ? [$user_id] : request()->get('customer_ids');
        }
            $start = request()->get('start');
            $maturity = request()->get('maturity');
            $info = request()->get('info');
            $link = request()->get('link');
            $submitbtn = request()->get('submitbtn');

            $data = ['start' => $start, 'maturity' => $maturity, 'info' => $info, 'link' => $link, 'submitbtn' => $submitbtn, 'select_method' => $select_method];

            $DispService = new DispService;
            $DispService->IntoDisp($data,$customer_ids);

            return redirect()->route("backend-disp-massage");

   

    }

    


}