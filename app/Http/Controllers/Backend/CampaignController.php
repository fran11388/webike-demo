<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Illuminate\Http\Request as Requests;
use Ecommerce\Service\CampaignService;
use Everglory\Models\Campaign;
use Ecommerce\Service\AssortmentService;

class CampaignController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-campaigns');
        $component->function_name = 'COUPON';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function getCampaignPromotion(Requests $request)
    {

//        $promotion = Promotion::orderBy('id','desc')->paginate(20);
//        $this->view_component->promotion = $promotion;
//        $this->view_component->seo('title', $this->view_component->function_name);
//        $this->view_component->render();
        

        return view('backend.pages.weekly-promotion.setting', (array)$this->view_component);

    }

    public function postImportData()
    {
        $file = request()->file('file');

        $campaign_service = new CampaignService();
        $campaign_excel_data = $campaign_service->getValueByExcelFile($file);
        $campaign_service->importCampaignData($campaign_excel_data);

        return redirect()->route('backend-campaigns-list');

    }

    public function getCampaignPromotionList(Requests $request){

        $this->view_component->campaigns = Campaign::orderBy('id','desc')->paginate(20);
        $this->view_component->render();

        return view('backend.pages.weekly-promotion.list', (array)$this->view_component);

    }

    public function postCampaign()
    {
        $active_type = \Request::get('active_type');
        $campaign_ids = \Request::get('check');
        $choose_campaign = Campaign::whereIn('id',$campaign_ids)->get();

        $AssortmentService = new AssortmentService;

        foreach($choose_campaign as $campaign){

            switch($active_type){
                case 'open':
                    $assortment_id = $AssortmentService->insertWeeklyAssortment($campaign->data);
                    $campaign->active = 1;
                    $campaign->assortment_id = $assortment_id;
                    $campaign->save();
                    break;
                case 'close':
                    $assortment = $AssortmentService->colseAssortment($campaign->assortment_id);
                    $campaign->active = 0;
                    $campaign->save();
                    break;
                case 'delete':
                    $assortment = $AssortmentService->deleteAssortment($campaign->assortment_id);
                    $campaign->delete();
                    break;
                default:
                    # code...
                    break;
            }
        }
        return \Redirect::back();
    }

    public function deleteCampaign($campaign)
    {
        $campaign = Campaign::whereIn('id',$campaign->id)->forceDelete();
        
    }
}