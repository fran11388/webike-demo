<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Customer\ContactController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Components\View\BaseComponent;
use Ecommerce\Core\Disp\Program as dispProgram;
use Ecommerce\Service\HistoryService;
use Auth;
use Route;
use Cache;
use Ecommerce\Accessor\CustomerAccessor;

class BackendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $view_component;

    public function __construct(BaseComponent $component)
    {
        $this->view_component = $component;
        $this->middleware(function( $request , $next) use(&$component){
            $component->route_name = Route::currentRouteName();
            return $next($request);
        });
    }

}
