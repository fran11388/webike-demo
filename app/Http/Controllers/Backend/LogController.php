<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Promotion;
use Ecommerce\Service\promoCms\PromoService;
use Ecommerce\Core\Image;
use Ecommerce\Service\Tool\ToolService;
use Everglory\Models\cms_file;
use Illuminate\Http\Request;
use Ecommerce\Service\Backend\LogService;

class LogController extends BackendController
{
    private $errors = array();
    protected $logService;

    public function __construct(SimpleComponent $component, LogService $logService)
    {
        parent::__construct($component);
        $component->base_url = route('backend-guess');
        $component->function_name = 'Guess';
        $component->setBreadcrumbs($component->function_name, $component->base_url);

        $this->logService = $logService;
    }

    public function getSalesLog(Request $request)
    {
        $this->view_component->request = $request;

        $filter = $request->all();
        $result = $this->logService->getSalesLog($filter);

        if ($request->get('csv_output') and $request->get('csv_output') == 'true') {
            $data = $result['order_items']->orderby('order_items.created_at', 'DESC')->get();
            $title = array('No.', '訂單編號', '查詢時間', '姓名', 'email', '帳號屬性', '品牌', '商品編號', '商品名稱', 'sku', '商品金額', '數量', '商品來源', '對應車型', '型式/分類', '標籤', '分類(第 2 層)', '分類(第 3 層)', '產品屬性', '訂單狀態');
            $this->view_component->main_str = $this->csvOutput($data, $title, 'sales');
            $this->view_component->type = '銷售log';
            return view('backend.pages.log.csv_output', (array)$this->view_component);
        }

        $this->view_component->count = $result['order_items']->count();
        $this->view_component->order_items = $result['order_items']->orderby('order_items.created_at', 'DESC')->paginate($result['limit']);

        return view('backend.pages.log.sales_log', (array)$this->view_component);

    }

    public function getCreditCard(Request $request)
    {
        $this->view_component->request = $request;

        $args = $request->all();

        $result = $this->logService->getCreditCard($args);
        $this->view_component->logs = $result['logs']->orderby('created_at', 'desc')->paginate($result['limit']);
        return view('backend.pages.log.creditcard', (array)$this->view_component);
    }

    public function csvOutput($data, $title, $type)
    {
        $main = "\"";
        $main = $main . implode("\",\"", $title);
        $main .= "\"\n";
        switch ($type) {
            case 'genuineparts':
                foreach ($data as $num => $item) {
                    $num = $num + 1;
                    $status = ($item->status_id >= 2) ? '已回覆' : '查詢中';
                    $main = $main . "\"{$num}\",\"{$item->created_at}\",\"{$item->realname}\",\"{$item->email}\",\"{$item->role}\",\"{$item->manufacturer}\",\"{$item->model_number}\",\"{$item->product_price}\",\"{$item->price}\",\"{$status}\",\"{$item->text}\"\n";
                }
                break;

            case 'sales':
                foreach ($data as $num => $item) {
                    $details = $item->itemDetail;
                    $categories = explode('|', ($details->category_name_path ? $details->category_name_path : ''));
                    $categories_code = explode('-', ($details->category_url_path ? $details->category_url_path : ''));

                    $motor_group = '';
                    if (isset($details->motor_names)) {
                        $motor_group = str_replace('|', ", ", $details->motor_names);
                    }

                    $models = '';
                    if (isset($details->models)) {
                        $models = str_replace('|', ", ", $details->models);
                        $models = str_replace('$', ", ", $models);
                    }

                    $categories[1] = isset($categories[1]) ? $categories[1] : '';
                    $categories[2] = isset($categories[2]) ? $categories[2] : '';
                    $price = number_format($item->price, 0);

                    $suppliers = array();
                    foreach ($item->purchases as $purchase) {
                        if ($purchase->supplier) {
                            $suppliers[] = $purchase->supplier->name;
                        }
                    }
                    $suppliers_str = implode(', ', $suppliers);
                    $customer = $item->customer;
                    $product = $item->product;

                    $num = $num + 1;
                    if ($customer) {
                        $main = $main . "\"{$num}\",\"{$item->order->increment_id}\",\"{$item->created_at}\",\"{$item->customer->realname}\",\"{$item->customer->email}\",\"{$item->customer->role->name}\",\"{$details->manufacturer_name}\",\"{$item->model_number}\",\"{$details->product_name}\",\"{$item->sku}\",\"{$price}\",\"{$item->quantity}\",\"{$suppliers_str}\",\"{$motor_group}\",\"{$models}\",\"" . ($product ? $product->attribute_tag : '') . "\",\"{$categories[1]}\",\"{$categories[2]}\",\"{$categories[0]}\",\"{$item->order->status->name}\"\n";
                    } else {
                        $main = $main . "\"{$num}\",\"{$item->order->increment_id}\",\"{$item->created_at}\",\"{''}\",\"{''}\",\"{''}\",\"{$details->manufacturer_name}\",\"{$item->model_number}\",\"{$details->product_name}\",\"{$item->sku}\",\"{$price}\",\"{$item->quantity}\",\"{$suppliers_str}\",\"{$motor_group}\",\"{$models}\",\"" . ($product ? $product->attribute_tag : '') . "\",\"{$categories[1]}\",\"{$categories[2]}\",\"{$categories[0]}\",\"{$item->order->status->name}\"\n";

                    }
                }
                break;
            case 'mybike':
                foreach ($data as $num => $item) {
                    $num = $num + 1;
                    $main = $main . "\"{$num}\",\"{$item->id}\",\"{$item->realname}\",\"{$item->email}\",\"{$item->role}\",\"{$item->updated_at}\",\"{$item->manufacturer}\",\"{$item->motor_name}\",\"{$item->displacement}\"\n";
                }
                break;
            case 'points':
                foreach ($data as $num => $item) {
                    $num = $num + 1;
                    $status = 1;
                    if ($item->points_current) {
                        $status_text = '給予';
                    } else {
                        $status = 2;
                        $status_text = '使用';
                    }
                    $main = $main . "\"{$num}\",\"{$item->start_date}\",\"" . ($item->description ? $item->description : "訂單(" . $item->increment_id . ")") . "\",\"" . ($status == 1 ? $item->points_current : $item->points_spend) . "\",\"{$status_text}\",\"{$item->realname}\",\"{$item->role}\",\"{$item->email}\"\n";
                }
                break;
            case 'coupons':
                foreach ($data as $num => $item) {
                    $num = $num + 1;
                    $status = 1;
                    if ($item->increment_id) {
                        $status_text = 'O 訂單(' . $item->increment_id . ')';
                    } else {
                        $status = 2;
                        $status_text = 'X';
                    }
                    $main = $main . "\"{$num}\",\"{$item->created_at}\",\"{$item->transcode}\",\"{$item->name}\",\"{$item->expired_at}\",\"{$item->discount}\",\"{$status_text}\",\"{$item->realname}\",\"{$item->role}\",\"{$item->email}\"\n";
                }
                break;
            case 'virtualbank':
                foreach ($data as $num => $item) {
                    $num = $num + 1;
                    $main = $main . "\"{ $num++ }\",\"{ $item->bank_deal_date }\",\"{ $order ? $order->increment_id : '-' }\",\"{ $item->virtual_account }\",\"{ $item->credit_amount }\",\"{ $customer ? $customer->realname : '-' }\",\"{ $customer ? $customer->rmail : '-' }\",\"{ $item->created_at }\"\n";
                }
                break;
            default:
                # code...
                break;
        }
        //dd($main);
        return $main;
    }

    public function getVirtualBankLog(Request $request)
    {
        $filter = $request->all();
        $result = $this->logService->getVirtualBankLog($filter);

        if ($request->get('csv_output') and $request->get('csv_output') == 'true') {
            $data = $result['collection']->get()->sortby('bank_deal_date', 'DESC');
            $title = array('No.', '匯款日期', '訂單編號', '虛擬帳號', '金額', 'Email', '真實姓名', '建立時間');
            $this->view_component->main_str = $this->csvOutput($data, $title, 'coupons');
            $this->view_component->type = 'virtualbank';
            return view('backend.pages.log.csv_output', (array)$this->view_component);
        }

        $this->view_component->request = $request;
        $this->view_component->count = $result['collection']->count();
        $this->view_component->collection = $result['collection']->orderby("bank_deal_date", 'DESC')->paginate($result['limit']);

        return view('backend.pages.log.virtualbank', (array)$this->view_component);
    }

    public function getGenuinePartsLog(Request $request)
    {
        $filter = $request->all();
        $result = $this->logService->getGenuinePartsLog($filter);

        if ($request->get('csv_output') and $request->get('csv_output') == 'true') {
            $data = $result['genuineparts']->orderby('items.created_at', 'DESC')->get();
            $title = array('No.', '查詢時間', '姓名', 'email', '帳號屬性', '品牌', '料號', '廠商定價', '售價', '狀態', '回覆交期');
            $this->view_component->main_str = $this->csvOutput($data, $title, 'genuineparts');
            $this->view_component->type = '正廠零件log';
            return view('backend.pages.log.csv_output', (array)$this->view_component);
        }

        $this->view_component->request = $request;
        $this->view_component->count = $result['genuineparts']->count();
        $this->view_component->genuineparts = $result['genuineparts']->orderby('items.created_at', 'DESC')->paginate($result['limit']);

        return view('backend.pages.log.genuine_log', (array)$this->view_component);
    }

    public function getCouponsLog(Request $request)
    {
        $filter = $request->all();
        $result = $this->logService->getCouponsLog($filter);

        if ($request->get('csv_output') and $request->get('csv_output') == 'true') {
            $data = $result['collection']->orderby('coupons.created_at', 'DESC')->get();
            $title = array('No.', '名稱', '發放日', '過期日', '金額', '已使用', '姓名', '帳號屬性', 'email');
            $this->view_component->main_str = $this->csvOutput($data, $title, 'coupons');
            $this->view_component->type = 'coupons_Log';
            return view('backend.pages.log.csv_output', (array)$this->view_component);
        }

        $this->view_component->request = $request;
        $this->view_component->count = $result['collection']->count();
        $this->view_component->collection = $result['collection']->orderby('coupons.created_at', 'DESC')->paginate($result['limit']);

        if (!empty($result['errors'])) {
            return view('backend.pages.log.coupons', (array)$this->view_component)->with('errors', $result['errors']);
        }

        return view('backend.pages.log.coupons', (array)$this->view_component);
    }

    public function getPointsLog(Request $request)
    {
        $filter = $request->all();
        $result = $this->logService->getPointsLog($filter);

        if ($request->get('csv_output') and $request->get('csv_output') == 'true') {
            $data = $result['collection']->orderby('order_rewards.start_date', 'DESC')->get();
            $title = array('No.', '名稱', '發放日', '過期日', '金額', '狀態', '姓名', '帳號屬性', 'email');
            $this->view_component->main_str = $this->csvOutput($data, $title, 'points');
            $this->view_component->type = 'points_Log';
            return view('backend.pages.log.csv_output', (array)$this->view_component);
        }

        $this->view_component->request = $request;
        $this->view_component->count = $result['collection']->count();
        $this->view_component->collection = $result['collection']->orderby('order_rewards.start_date', 'DESC')->paginate($result['limit']);
        //->toSql();dd($collection);

        if (!empty($result['errors'])) {
            return view('backend.pages.log.points', (array)$this->view_component)->with('errors', $result['errors']);
        }

        return view('backend.pages.log.points', (array)$this->view_component);
    }


}