<?php 
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;

class IndexController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
    }

    public function getIndex()
    {
        $this->view_component->seo_title = '首頁';
        $this->view_component->setBreadcrumbs($this->view_component->seo_title, $this->view_component->home_url);
        $this->view_component->render();
    	return view('backend.pages.index', (array)$this->view_component);
    }
}