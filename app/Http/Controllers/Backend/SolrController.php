<?php 
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\SolrService;
use Everglory\Constants\CustomerRole;
use Everglory\Constants\ProductType;
use Everglory\Models\Coupon;
use Everglory\Models\Coupon\Base as CouponBase;
use Everglory\Models\Coupon\Base;
use Everglory\Models\Coupon\History;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use Faker\Provider\zh_TW\DateTime;

class SolrController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $this->view_component->base_url = route('backend-solr');
        $this->view_component->function_name = 'Solr更新作業';
        $this->view_component->setBreadcrumbs($this->view_component->function_name, $this->view_component->base_url);
    }

    public function index()
    {
        $this->view_component->seo_title = $this->view_component->function_name;
        $this->view_component->render();
        return view('backend.pages.solr.index', (array)$this->view_component);
    }

    public function commandStatus()
    {
        \Debugbar::disable();
        $result = getBasicAjaxObject();
        $solrService = new SolrService();
        $result->success = true;
        $result->datas = $solrService->getCommandStatus();
        $result->datas->timestamp = date('Y-m-d H:i:s');
        if(request()->get('check')){
            dd($result);
        }
        return json_encode($result);
    }

    public function product()
    {
        $this->view_component->base_url = route('backend-solr-product');
        $this->view_component->function_name = '商品資訊即時更新';
        $this->view_component->setBreadcrumbs($this->view_component->function_name, $this->view_component->base_url);
        $this->view_component->seo_title = $this->view_component->function_name;
        $this->view_component->render();
    	return view('backend.pages.solr.product.index', (array)$this->view_component);
    }

    public function productRemove()
    {
        \Debugbar::disable();
        $this->toDieForSolrBusy();
        if(strpos(request()->get('skus'), '_') !== false){
            $skus = explode("_", trim(request()->get('skus')));
        }else{
            $skus = explode("\r\n", trim(request()->get('skus')));
        }
        $this->view_component->alert = '';
        $this->view_component->products = [];
        $this->view_component->base_url = route('backend-solr-product');
        $this->view_component->setBreadcrumbs('商品資訊即時更新', $this->view_component->base_url);
        $this->view_component->function_name = '商品移除失敗';
        if(count($skus)){
            $this->view_component->dealer_account = Customer::where('role_id', CustomerRole::WHOLESALE)->first();
            $this->view_component->products = ProductRepository::getProductWithRelations($skus, ['motors', 'categories', 'categories.mptt', 'manufacturer', 'productDescription', 'points', 'prices', 'images']);
            $solrService = new SolrService();
            foreach ($this->view_component->products as $product){
                $solrService->removeDocumentsByProduct($product->id);
            }
            $this->view_component->function_name = '商品移除成功';
        }else{
            $this->view_component->alert = '未輸入Sku' . '<br>' . var_dump(request()->get('skus'));
        }

        $this->view_component->seo_title = $this->view_component->function_name;
        $this->view_component->setBreadcrumbs($this->view_component->function_name);
        $this->view_component->render();
        return view('backend.pages.solr.product.remove-finish', (array)$this->view_component);
    }

    public function resetStock()
    {
        \Debugbar::disable();
        $this->toDieForSolrBusy();
        $this->view_component->result = SolrService::resetInStockFlg();
        $this->view_component->function_name = '即時庫存資訊重置';
        $this->view_component->seo_title = $this->view_component->function_name;
        $this->view_component->setBreadcrumbs($this->view_component->function_name);
        $this->view_component->render();
        return view('backend.pages.solr.product.stock.reset', (array)$this->view_component);
    }

    public function productUpdate()
    {
        \Debugbar::disable();
        $this->toDieForSolrBusy();
        set_time_limit(60 * 30);
        $illegal_pass = request()->get('illegal_pass');
        if(strpos(request()->get('skus'), '_') !== false){
            $skus = explode("_", trim(request()->get('skus')));
        }else{
            $skus = explode("\r\n", trim(request()->get('skus')));
        }
        $product_limit = 1000;
        $this->view_component->base_url = route('backend-solr-product');
        $this->view_component->setBreadcrumbs('商品資訊即時更新', $this->view_component->base_url);
        $this->view_component->products = [];
        $this->view_component->function_name = '商品更新失敗';
        $this->view_component->alert = '';
        if(count($skus) and count($skus) <= $product_limit){
            $this->view_component->dealer_account = Customer::where('role_id', CustomerRole::WHOLESALE)->first();
            $validate_skus = Product::select('sku')
                ->whereIn('sku', $skus)
                ->where('is_main', 1)
                ->where('active', 1)
                ->whereIn('type', [ProductType::NORMAL, ProductType::OUTLET])
                ->get()
                ->pluck('sku')
                ->toArray();
            $illegals = array_diff($skus, $validate_skus);
            if(($illegal_pass or !count($illegals)) and count($validate_skus)){
                $this->view_component->products = ProductRepository::getProductWithRelations($validate_skus, ['motors', 'categories', 'categories.mptt', 'manufacturer', 'productDescription', 'points', 'prices', 'images','options']);
                $solrService = new SolrService();
                foreach ($this->view_component->products as $product){
                    $solrService->updateDocumentsByProduct($product);
                }
                $this->view_component->function_name = '商品更新成功';
            }else if(count($illegals)){
                $this->view_component->alert = '只能更新上架中商品且商品種類須為一般商品(' . ProductType::NORMAL . ')及Outlet(' . ProductType::OUTLET . ')，下列為更新失敗sku。<br>' . implode('<br>', $illegals);
            }
        }elseif(count($skus) >= $product_limit){
            $this->view_component->alert = '每次更新的商品數不可超過' . $product_limit;
        }

        $this->view_component->seo_title = $this->view_component->function_name;
        $this->view_component->setBreadcrumbs($this->view_component->function_name);
        $this->view_component->render();
        return view('backend.pages.solr.product.update-finish', (array)$this->view_component);
    }

    public function updateStock()
    {
        \Debugbar::disable();
        $this->toDieForSolrBusy();
        set_time_limit(60 * 30);
        $result = getBasicAjaxObject();
        $solrService = new SolrService();
        $barcodeCollection = request()->get('barcode');
        $sold_out = request()->has('sold_out') ? request()->get('sold_out') : false;
        if(!count($barcodeCollection)){
            $result->errors++;
            $result->messages[] = 'No barcode.';
            return json_encode($result);
        }
        if($sold_out){
            $stockRows = $solrService->formatUpdateInStockFlgRows($barcodeCollection, true);
        }else{
            $stockRows = $solrService->formatUpdateInStockFlgRows($barcodeCollection);
        }
        $solrResult = $solrService->updateRows($stockRows);
        if($solrResult){
            $result->datas = $solrResult->responseHeader;
            if(!isset($solrResult->error)){
                $result->success = true;
                $result->messages[] = (is_array($barcodeCollection) ? implode(', ', $barcodeCollection) : $barcodeCollection) . '項目已更新solr資訊完成。';
            }else{
                $result->errors++;
                $result->messages[] = $solrResult->error->msg;
            }
        }else{
            $result->errors++;
            $result->messages[] = 'Solr api not response, please check port at webike frontend.';
        }
        return json_encode($result);
    }

    private function toDieForSolrBusy()
    {
        $result = json_decode($this->commandStatus());
        if(!$result or !$result->datas or $result->datas->status === 'busy'){
            $this->view_component->function_name = 'Solr(Core:webike)忙碌中';
            $this->view_component->seo_title = $this->view_component->function_name;
            $this->view_component->setBreadcrumbs($this->view_component->function_name);
            $this->view_component->render();
            echo view('backend.pages.solr.product.die-for-busy', (array)$this->view_component)->render();
            exit();
        }
    }

}