<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/9/17
 * Time: 上午 10:39
 */

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Illuminate\Http\Request;

use Everglory\Models\GroupBuy;
use Everglory\Models\GroupBuy\Item;
use Ecommerce\Service\GroupBuyService;
use Ecommerce\Core\Disp\Program as dispService;
class GroupBuyController extends BackendController
{

    protected $groupBuyService;
    protected $dispService;

    public function __construct(SimpleComponent $component, GroupBuyService $groupBuyService,dispService $dispService)
    {
        parent::__construct($component);
        $component->base_url = route('backend-groupbuy');
        $component->function_name = '團購查詢回復';
        $component->setBreadcrumbs($component->function_name, $component->base_url);

        $this->groupBuyService = $groupBuyService;
        $this->dispService = $dispService;
    }

    public function getIndex()
    {
        $this->view_component->collection = GroupBuy::with('customer')->orderBy('status', 'asc')->orderBy('request_date', 'desc')->paginate(20);

        return view('backend.pages.groupbuy.index', (array)$this->view_component);
    }

    public function postDelete(Request $request)
    {

        $ids = $request->get('ids');
        $ids = explode('_', $ids);
        $collection = GroupBuy::whereIn('id', $ids)->delete();
        return \Redirect::back();
    }

    public function postIndex(Request $request)
    {
        if ($request->ajax()) {
            $mitu = GroupBuy::findOrFail($request->get('id'));
            $mitu->jp_order_code = $request->get('jp_order_code');
            $mitu->timestamps = false;
            $mitu->save();
            return $mitu->jp_order_code;
        }
    }

    public function getEdit($id)
    {
        $this->view_component->entity = GroupBuy::findOrFail($id);
        $this->view_component->manufacturers = \Everglory\Models\Manufacturer::all();
//        $this->view_component->nouki = array(''=>'請選擇交期') + \Everglory\Models\GroupBuy\Nouki::where('nouki_code','not like','%A')->pluck('nouki_text','nouki_code') + \Everglory\Models\GroupBuy\Nouki::where('nouki_code','like','%A')->pluck('nouki_text','nouki_code');


        $this->view_component->nouki = \Everglory\Models\GroupBuy\Nouki::pluck('nouki_text', 'nouki_code');

        return view('backend.pages.groupbuy.edit', (array)$this->view_component);
    }

    public function postEdit($id)
    {
        $entity = GroupBuy::findOrFail($id);
        // $category = \Category::where('url_rewrite','1627')->first();
        $count = $entity->items()->where('product_check', '=', 0)->count();
        if ($count) {
            return \Redirect::back()->with('danger', '還有項目還沒確認！');
        }

        $items = $entity->items()->where('product_nouki', 'not like', 'A%')->get();
        foreach ($items as $key => $item) {
            $object = new \stdClass();
            $object->name = $item->product_name . '(共' . $item->syouhin_kosuu . '件)';
            $object->model_number = $item->product_code;
            $object->manufacturer_id = $item->product_manufacturer_id;
            $object->manufacturer_name = $item->product_manufacturer_name;
            $object->custom_manufacturer = $item->custom_manufacturer;
            $object->category_id = 1628;
            $object->price = $item->product_price;
            $object->cost = $item->product_cost * $item->syouhin_kosuu;
            $object->customer_id = $entity->customer_id;
            $object->item_id = $item->id;
            $this->groupBuyService->add($object); //新增商品佇列
        }

        $warehouseQueues = $this->groupBuyService->get();  //產出商品API
        if (count($items) > 0) {
            if(count($warehouseQueues) == 0){
                return \Redirect::back()->with('danger', '失敗！!');
            }
            //product_id 對應回填
            foreach ($warehouseQueues as $warehouseQueue) {
                if ($warehouseQueue->category_id == 1628) {
                    $groupBuyItem = GroupBuy\Item::where('id', $warehouseQueue->item_id)->first();
                    if ($groupBuyItem) {
                        $groupBuyItem->product_id = $warehouseQueue->product_id;
                        $groupBuyItem->save();
                    }
                }
            }
        } else {
            // nothing to do.
        }

        //站內訊息通知
        $this->dispService->addDisp($entity);
        //email通知
        $entity->status = 1;
        $entity->response_date = date('Y-m-d H:i:s');
        $entity->timestamps = false;
        $entity->read = 0;
        $entity->save();
        $Wmail=new \Ecommerce\Core\Wmail;
        $Wmail->groupbuy($entity); //
        return \Redirect::back()->with('success', '已成功回覆!');
    }

    public function postItem($id, Request $request)
    {

        $item = Item::findOrFail($id);

        /*except _token, because laravel 5's CSRF policy*/
        foreach ($request->except('custom_manufacturer', 'rate', '_token') as $key => $value) {
            $item->$key = $value;
        }
        if ($custom_manufacturer = $request->has('custom_manufacturer')) {
            $item->custom_manufacturer = 1;
            $item->product_manufacturer_id = 0;
        } else {
            $item->custom_manufacturer = 0;
            $item->product_manufacturer_name = null;
        }
        if ($item->save()) {
            return $item->product_check;
        } else {
            return false;
        }

    }
}