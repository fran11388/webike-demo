<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Ecommerce\Service\CustomerService;

class ApiController extends BackendController
{

    protected $basic;

    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $this->basic = new \StdClass;
        $this->basic->success = false;
        $this->basic->errors = 0;
        $this->basic->messages = [];
        $this->basic->datas = [];
        \Debugbar::disable();
    }

    public function getCustomerNamesByText()
    {
        $customers = [];
        $search_text = request()->get('search_text');
        if($search_text){
            $this->basic->success = true;
            $customers = CustomerService::getCustomerNamesByPointSearch($search_text);
        }
        $this->basic->datas = $customers;
        return json_encode($this->basic);
    }


}