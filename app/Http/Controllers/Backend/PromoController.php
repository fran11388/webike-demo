<?php

namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Everglory\Models\Promotion;
use Ecommerce\Service\PromoCms\PromoService;

class PromoController extends BackendController
{

   public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-promo');
        $component->function_name = '促銷活動設定';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function index()
    {
        $promotion = Promotion::orderBy('id','desc')->paginate(20);
        $this->view_component->promotion = $promotion;
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();

        return view('backend.pages.promo.index', (array)$this->view_component);
    }
    public function promoPreview()
    {
        $file = request()->file('file');
       
       $PromoService = new PromoService();
       $promoData = $PromoService->getColsByExcelFile($file);
       $promotion = $PromoService->getPromotionData($promoData['1']);
       $info = $PromoService->getpromoPreview($promoData);
       $this->view_component->promotion = $promotion;
       $this->view_component->info = $info;
       
      dd($promoItem);


    }

    public function promoData()
    {   
        
       $file = request()->file('file');
       
       $PromoService = new PromoService();
       $promoData = $PromoService->getColsByExcelFile($file);
       
       $promoUrl = $PromoService->intoPromotionsItem($promoData);
       
       return redirect()->route('benefit-sale-month-promotion',['code' => $promoUrl]);     
        
    }


    


}