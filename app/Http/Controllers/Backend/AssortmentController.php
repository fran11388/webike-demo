<?php 
namespace App\Http\Controllers\Backend;

use App\Components\View\Backend\SimpleComponent;
use Ecommerce\Service\AssortmentService;
use Ecommerce\Repository\AssortmentRepository;
use Everglory\Models\Assortment\Item;
use Everglory\Models\Assortment;
use Ecommerce\Service\ImageService;

class AssortmentController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-assortment');
        $component->function_name = '流行特輯';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function getIndex()
    {
    	$service = new AssortmentService;
    	$this->view_component->assortments = $service->getAssortmentsByLayout(3,['type','items']);
//        dd($this->view_component->assortments);
        $this->view_component->active = [0 => '關閉',1 => '開啟'];
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();

    	return view('backend.pages.assortment.index', (array)$this->view_component);
    }

    public function postIndex()
    {
        foreach(\Request::get('active') as $id => $active){
            $assortment = Assortment::where('id',$id)->first();
            $assortment->active = $active;
            $assortment->save();
        };
        if(is_array(\Request::get('delete'))){
            $assortment = Assortment::whereIn('id',\Request::get('delete'))->delete();
        }

        return redirect()->route('backend-assortment');
    }

    public function getAssortmentInsert($assortment_id = NULL)
    {
        $this->view_component->assortment = null;
        $repository = new AssortmentRepository;
        $this->view_component->layouts = $repository->getLayouts();
        $this->view_component->types = $repository->getTypes();
        if($assortment_id){
            $repository = new AssortmentRepository;
            $this->view_component->assortment = $repository->getAssortmentById($assortment_id,true);
            $this->view_component->seo('title', '編輯特輯');
        }else{
            $this->view_component->seo('title', '新增特輯');
        }

        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();

        return view('backend.pages.assortment.edit.edit', (array)$this->view_component);
    }

    public function postAssortmentInsert($assortment_id = NULL)
    {
        $service = new AssortmentService;
        $datas = \Request::all();
        if ($assortment_id) {
            $repository = new AssortmentRepository;
            $assortment = $repository->getAssortmentById($assortment_id);
        } else {
            $assortment = new Assortment;
        }

        $image = new ImageService;
        $image->store(\Request::get('photo_key'));
        $service->insertAssortment($datas, $assortment);
        $assortment_id = $assortment->id;

        return redirect()->route('backend-assortment-insert',['assortment_id' => $assortment_id]);
    }


    public function getAssortmentInsertBlock($assortment_id,$assortmentItem_id = null)
    {

        $repository = new AssortmentRepository;
        $this->view_component->pluginTemplates = $repository->getPluginTemplates();
        $assortment = $repository->getAssortmentById($assortment_id,true);

        $this->view_component->assortmentItem = null;
        if($assortmentItem_id){
            $assortmentItem = $assortment->items->first(function($item) use($assortmentItem_id){
                if($item->id == $assortmentItem_id){
                    return $item;
                }
            });

            $template_id = $assortmentItem->template_id;
            $plugin_id = $assortmentItem->plugin_id;
            $service = new AssortmentService;
            $columns = $service->getPluginTemplateColumns($plugin_id,$template_id);
            $this->view_component->column_info = $columns->quantity;
            $this->view_component->template = $assortmentItem->plugin->templates->filter(function($template) use($template_id){
                return $template->id == $template_id;
            })->last();

            $this->view_component->assortmentItem = $assortmentItem;

            $data = json_decode($assortmentItem->data,true);
            $this->view_component->data = $data;
        }
        $this->view_component->layoutSection = json_decode($assortment->layout->section);
        $this->view_component->assortment_id = $assortment_id;

        $this->view_component->seo('title', '特輯區塊新增');
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();

        return view('backend.pages.assortment.edit.block', (array)$this->view_component);
    }

    public function postAssortmentInsertBlock($assortment_id,$assortmentItem_id = NULL)
    {
        $repository = new AssortmentRepository;
        $assortment = $repository->getAssortmentById($assortment_id);
        $plugin_template = \Request::get('plugin_template')[0];
        $section = \Request::get('section')[0];
        if($assortmentItem_id) {
            $assortmentItem = $repository->getAssortmentItemById($assortmentItem_id);
            $sort = $assortmentItem->sort;
        }else{
            $sort = $assortment->items->max('sort') + 1;
            $assortmentItem = new Item;
        }

        $image = new ImageService;
        $image->store(\Request::get('photo_key'));

        $service = new AssortmentService;
        $service->insertAssortmentItems(\Request::all(),$plugin_template,$assortment_id,$section,$sort,$assortmentItem);

        return redirect()->route('backend-assortment-insert',['assortment_id' => $assortment_id]);
    }

    public function postOtherAssortmentItemChange($assortment_id)
    {
        foreach(\Request::get('sort') as $item_id => $sort){
            $item = Item::where('id',$item_id)->where('assortment_id',$assortment_id)->first();
            $item->sort = $sort;
            $item->save();
        };
        if(is_array(\Request::get('delete'))){
            $ids = array_keys(\Request::get('delete'));
            $item = Item::whereIn('id',$ids)->delete();
        }


        return redirect()->route('backend-assortment-insert',['assortment_id' => $assortment_id]);
    }

    public function getAssortmentEditBlock()
    {
        $repository = new AssortmentRepository;
        $this->view_component->layouts = $repository->getLayouts();
        $this->view_component->types = $repository->getTypes();
        $this->view_component->pluginTemplates = $repository->getPluginTemplates();
//    	$plugin = new \Ecommerce\Service\Assortment\Analytic\CategoryList();
        $this->view_component->seo('title', $this->view_component->function_name.'新增');
        $this->view_component->setBreadcrumbs('特輯新增',\URL::route('backend-assortment-edit'));
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();

        return view('backend.pages.assortment.edit.block', (array)$this->view_component);
    }

    public function postColumns()
    {
        $plugin_id = \Request::get('plugin');
        $template_id = \Request::get('template');
        $service = new AssortmentService;
        $result = new \stdClass();
        $pluginTemplates = $service->getPluginTemplateColumns($plugin_id,$template_id);
        $result->columns = $pluginTemplates->columns;
        $result->other_columns = $pluginTemplates->other_columns;
        $result->quantity = $pluginTemplates->quantity;
        $result->example = $pluginTemplates->example;
        return json_encode($result);
    }

    public function upload($path = null)
    {
        $service = new ImageService;
        $img_file = request()->file('photo');
        $img_path = request()->get('path');
        $service->upload($img_path,$img_file);
        die();
    }
}