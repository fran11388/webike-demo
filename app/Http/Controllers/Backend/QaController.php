<?php
namespace App\Http\Controllers\Backend;

use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\ContactService;
use Ecommerce\Service\QaService;
use \Everglory\Models\Qa\Relation as QaRelation;
use App\Components\View\Backend\SimpleComponent;

class QaController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-qa');
        $component->function_name = 'Q&A';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function index(){
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();
        return view('backend.pages.qa.index', (array)$this->view_component);
    }

    public function search(){
        $qaService = new QaService;
        $this->view_component->questions = $qaService->search(request()->all(), 20);
        $this->view_component->seo('title', $this->view_component->function_name . '列表');
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        return view('backend.pages.qa.search', (array)$this->view_component);
    }

    public function edit(){
//        $this->view_component->cateogries = CategoryRepository::selectChildren(1);
//        $this->view_component->manufacturers = ManufacturerRepository::all();
        $this->view_component->answer = null;
        $this->view_component->question = null;
        $this->view_component->skus = null;
        $this->view_component->manufacturer_ids = null;
        $this->view_component->category_ids = null;
        $this->view_component->motor_ids = null;
        if(request()->get('answer_id')){
            $this->view_component->answer = QaService::findQaInfoByAnswer(request()->get('answer_id'));
            $this->view_component->question = $this->view_component->answer->question;
            $relation = $this->view_component->answer->relation;
            if($relation->product_id){
                $product = ProductRepository::findDetail($relation->product_id, false, 'id', []);
                if($product){
                    $this->view_component->skus = $product->sku;
                }
            }
            if($relation->manufacturer_id){
                $this->view_component->manufacturer_ids = ManufacturerRepository::find($relation->manufacturer_id, 'id');
            }
            if($relation->category_id){

            }
            if($relation->motor_ids){
                $this->view_component->motor_ids = MotorRepository::find($relation->motor_id, 'id');
            }
        }
        $this->view_component->seo('title', $this->view_component->function_name.'編輯');
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        return view('backend.pages.qa.edit', (array)$this->view_component);
    }

    public function write()
    {
        $requires = ['skus', 'series_ids', 'motor_ids', 'category_ids', 'manufacturer_ids'];
        $validator = QaService::getValidator($requires);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $qaService = new QaService;
        $question = $qaService->addQuestionAnswer(request()->all());
        if($question){
            return redirect()->route('backend-qa-search', ['question_id[]' => $question->id]);
        }else{
            return redirect()->route('backend-qa-edit')->withErrors($validator)->withInput();
        }
    }

    public function import()
    {
        set_time_limit(60 * 60 * 3);
        ini_set('memory_limit', '2048M');

        $qaService = new QaService;
        $qaService->autoTransformToQa('ticket');
        return redirect()->route('backend-qa-weekly');
    }

    public function weekly()
    {
        $this->view_component->answers = QaService::findThisWeekQa();
        $this->view_component->collection = QaService::getRelationProducts($this->view_component->answers);
        $this->view_component->seo('title', $this->view_component->function_name . '本周上架');
        $this->view_component->setBreadcrumbs($this->view_component->seo_title);
        $this->view_component->render();
        return view('backend.pages.qa.weekly', (array)$this->view_component);
    }

}