<?php
namespace App\Http\Controllers\Backend\setting;

use App\Components\View\Backend\SimpleComponent;
use App\Http\Controllers\Backend\BackendController;
use Ecommerce\Core\Wmail;
use Everglory\Models\Customer;
use Everglory\Models\Email\Newsletter\Item;
use Everglory\Models\Email\Newsletter\Queue;
use Everglory\Models\Newsletter;
use Illuminate\Http\Request;

class CacheController extends BackendController
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('backend-newsletter');
        $component->function_name = 'Cache manage';
        $component->setBreadcrumbs($component->function_name, $component->base_url);
    }

    public function getIndex()
    {
        $this->view_component->seo('title', $this->view_component->function_name);
        $this->view_component->render();

        return view('backend.pages.setting.cache', (array)$this->view_component);
    }
}