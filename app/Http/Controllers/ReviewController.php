<?php
namespace App\Http\Controllers;

use App\Components\View\ReviewComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Wmail;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Repository\ReviewRepository;
use Everglory\Constants\SEO;
use Everglory\Models\Product;
use Everglory\Models\Review;
use Ecommerce\Core\Image;
use Ecommerce\Repository\MotorRepository;
use Everglory\Models\ReviewComment;
use Ecommerce\Service\ProductService;

class ReviewController extends Controller
{
    public function __construct(ReviewComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('review');
        app()->singleton('EmptyProduct', function () {
            return new Product();
        });
    }

    public function index()
    {
//        評論搜尋
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);

        $this->view_component->mostReviewProducts = convertCollectionToProductAccess(ProductRepository::getMostReviewProduct(180,6));
        $this->view_component->mostCommentReview = ReviewRepository::getMostCommentReviews();
        $this->view_component->customReviewObject = ReviewRepository::getCategoryReviewCount('1000');
        $this->view_component->riderReviewObject = ReviewRepository::getCategoryReviewCount('3000');
        $this->view_component->genuineReviewObject = ReviewRepository::getGenuinePartsReviewCount();
        $this->view_component->notWriteReview = ReviewRepository::getCustomerOrderItemWriteReview( $this->view_component->current_customer ,true);

        $this->view_component->newReviews = ReviewRepository::getNewReviews(8);
        $this->view_component->newComments = ReviewRepository::getNewReviewComment(5);
        $this->view_component->my_bikes = [];
        if($this->view_component->current_customer){
            $this->view_component->my_bikes = $this->view_component->current_customer->motors;
        }
        
        

        $this->view_component->seo('title', '進口重機、機車商品評論、車友討論區');
        $this->view_component->seo('description', '進口重機、機車改裝零件、正廠零件、人身部品，車友使用心得，商品討論區');
        $this->view_component->render();
        return view('response.pages.review.index', $this->view_component->output());

    }

    public function search($url_rewrite = null)
    {
        $this->view_component->category = CategoryRepository::find($url_rewrite);
        $this->view_component->current_motor = MotorRepository::find(request()->input('mt'));
        $search_result = ReviewRepository::search($url_rewrite,$this->view_component->current_motor);
        $this->view_component->reviews = $search_result->reviews;
        $this->view_component->tree = $search_result->tree;
        $this->view_component->manufacturers = $search_result->manufacturers;
        $this->view_component->rankings = $search_result->rankings;
        $this->view_component->keyword = TransferToUtf8(request()->input('q'));
        $this->view_component->current_product = null;
        if(request()->input('sku')){
            $this->view_component->current_product = ProductRepository::getMainProduct(request()->input('sku'),['manufacturer']);
        }

        $this->view_component->current_manufacturer = request()->input('br');
        $this->view_component->ranking = request()->input('ranking');
        $this->view_component->my_bikes = [];
        $this->view_component->filters = [
            'mt' => ['label'=>'車型','name'=>$this->view_component->current_motor ? $this->view_component->current_motor->name . ($this->view_component->current_motor->synonym ? '(' . $this->view_component->current_motor->synonym . ')' : '') : ''],
            'br' => ['label'=>'品牌','name'=>$this->view_component->current_manufacturer ? $this->view_component->current_manufacturer : '' ],
            'ca' => ['label'=>'分類','name'=>$this->view_component->category ? $this->view_component->category->name . ($this->view_component->category->synonym ? '(' . $this->view_component->category->synonym . ')' : '') : ''],
            'sku' => ['label'=>'商品','name'=>$this->view_component->current_product ? $this->view_component->current_product->name : ''],
            'q' => ['label'=>'關鍵字','name'=>$this->view_component->keyword],
            'ranking' => ['label'=>'評價','name'=>$this->view_component->ranking],
        ];
        $this->view_component->filter_display = [
        ];
        if($this->view_component->current_customer){
            $this->view_component->my_bikes = $this->view_component->current_customer->motors;
        }
        $meta_filters = [];
        foreach ($this->view_component->filters as $key => $filter){
            if(!in_array($key, ['ranking']) and $filter['name']){
                $meta_filters[] = $filter['name'];
            }
        }
        $this->view_component->meta_filters = implode(' & ', $meta_filters);
        // dd($this->view_component->output());
        
        


        if($url_rewrite){
            $this->view_component->seo('title', $this->view_component->meta_filters . '商品評論、車友討論區');
        }else{
            $this->view_component->seo('title', '全部商品評論、車友討論區');
        }
        if($this->view_component->meta_filters){
            $this->view_component->seo('description', '進口重機、機車【' . $this->view_component->meta_filters . '】車友使用心得，商品討論區' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        }else{
            $this->view_component->seo('description', '進口重機、機車改裝零件、正廠零件、人身部品，車友使用心得，商品討論區。' . SEO::WEBIKE_SHOPPING_LOGO_TEXT);
        }

        $this->view_component->render();

        return view('response.pages.review.search', $this->view_component->output());
    }

    public function create($url_rewrite)
    {

        if($this->view_component->current_customer->role_id == \Everglory\Constants\CustomerRole::REGISTERED){
            return redirect()->route('customer-account-complete');
        }
        $product = ProductRepository::getMainProduct($url_rewrite,['manufacturer', 'images']);
        if($product){
            $this->view_component->product = new ProductAccessor($product, false);
            return view('response.pages.review.create', (array)$this->view_component);
        }else{
            app()->abort(404);
        }

    }

    public function show($id)
    {

        $review = ReviewRepository::find($id);
        if(!$review){
            return abort(404);
        }

        $this->view_component->reviews = ReviewRepository::searchByProductId($review->product_id)->where('id', '!=', $review->id);

        $review->product = ($review->product ? new ProductAccessor($review->product, false) : null);

        $this->view_component->review = $review;
        $this->view_component->motor = null;
        $reviewMotor = $review->motors->first(function($motor){
            return $motor->customer_selected_flg;
        });
        if($reviewMotor){
            $this->view_component->motor = $reviewMotor->motorModel;
        }

        $this->view_component->mostReviewProducts = convertCollectionToProductAccess(ProductRepository::getMostReviewProduct());
        $this->view_component->notWriteReview = ReviewRepository::getCustomerOrderItemWriteReview( $this->view_component->current_customer ,true);

        

        if($review->product_category == 420){
            $this->view_component->seo('title', $review->product_manufacturer . '正廠零件：' . $review->product_name . '商品評論、車友使用心得');
        }else{
            $this->view_component->seo('title', $review->product_manufacturer . '：' . $review->product_name . '商品評論、車友使用心得');
        }
        $this->view_component->seo('description', $review->title . '：' . $review->content);
        return view('response.pages.review.show', $this->view_component->output());

    }

    public function upload()
    {

        $result = new \stdClass();
        $result->reload = false;
        if (!\Auth::check()) {
            $result->success = false;
            $result->message = '請重新登入';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        } else {
            $customer = \Auth::user();
        }


        $img_file = request()->file('photo');
        
        if(!$img_file){
            $result->success = false;
            $result->message = '檔案上傳失敗！請檢查格式是否正確';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }

        if ($img_file->getSize() > 5 * 1024 * 1024) {
            $result->success = false;
            $result->message = '上傳檔案需小於 5 MB';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }
        if (!in_array($img_file->getMimeType(),['image/jpeg','image/png','image/gif'])) {
            $result->success = false;
            $result->message = '只允許上傳 JPEG / PNG / GIF 格式檔案';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }
        $destinationPath = public_path() . '/assets/photos/review/';
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777);
        }
        $size = $img_file->getSize();
        $mimetype = $img_file->getMimeType();
        $resolution = getimagesize($img_file->getRealPath());
        $image_key = getFileKey();
        while (file_exists($destinationPath . $image_key . '.' . $img_file->getClientOriginalExtension())) {
            $image_key = getFileKey();
        }
        $filename_tmp = $image_key . '_tmp.' . $img_file->getClientOriginalExtension();
        $location = $destinationPath . $filename_tmp;
        $img_file->move($destinationPath, $filename_tmp);
        $filename = str_replace('_tmp.' . $img_file->getClientOriginalExtension(), '.' . $img_file->getClientOriginalExtension(), $filename_tmp);
        ImageResize($location, $destinationPath . $filename);

        $result->success = true;
        $result->key = $filename;
        $result = json_encode($result);
        echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
        //unlink($location);
        die();

    }

    public function store()
    {

        $customer = \Auth::user();
        $result = new \stdClass();

        //photo preview

        $product = Product::with(['motors'])->findOrFail(request()->input('product_id'));
        $images = $product->images;
        $thumbnail = 'http://img.webike.net/catalogue/noimage.gif';
        if (count($images)) {
            $thumbnail = $images[0]->thumbnail;
        }
        $category = $product->categories()->join('mptt_categories_flat as c', 'category_product.category_id', '=', 'c.id')->orderBy('c.depth', 'desc')->select('c.id', 'url_path')->first();
        $motor_name = '';
        $motor_manufacturer = '';
        $motor = '';
        if(request()->input('motor_id')){
            $motor = \Everglory\Models\Motor::where('url_rewrite',request()->input('motor_id'))->first();
            if($motor){
                $motor_name = $motor->name;
                $motor_manufacturer = $motor->manufacturer->name;
            }
        }
        $view_status = 0;

        $review = Review::create(array(
            'product_id' => $product->id,
            'customer_id' => $customer->id,
            'nick_name' => $customer->nickname,
            'ranking' => request()->input('ranking'),
            'rating' => serialize(request()->input('rating')),
            'title' => request()->input('title'),
            'content' => request()->input('content'),
            'photo_key' => request()->input('photo_key'),
            'motor_id' => request()->input('motor_id'),
            'customer_ip' => \Request::server('REMOTE_ADDR'),
            'view_status' => $view_status,
            'product_sku' => $product->sku,
            'product_manufacturer' => $product->manufacturer->name,
            'product_manufacturer_id' => $product->manufacturer->id,
            'product_manufacturer_code' => $product->manufacturer->url_rewrite,
            'product_name' => $product->name,
            'product_thumbnail' => $thumbnail,
            'product_category' => ($category ? $category->id : 276),
            'product_category_search' => ($category ? $category->url_path : 3000),
            'motor_name' => $motor_name,
            'motor_manufacturer' => $motor_manufacturer,
            'deleted_at' => date('Y-m-d H:i:s'),
        ));

        $customer_motor_id = '';
        if($motor){
            $review->motors()->create([
                'motor_id' => $motor->id,
                'customer_selected_flg' => 1,
            ]);
            $customer_motor_id = $motor->id;
        }

        if($motors = $product->motors){
            foreach($motors as $_motor){
                if($_motor->id != $customer_motor_id){
                    $review->motors()->create([
                        'motor_id' => $_motor->id,
                        'customer_selected_flg' => 0,
                    ]);
                }
            }
        }

        $image = new Image();

        if($review->photo_key){
            $image->review( public_path().'/assets/photos/review/'.$review->photo_key,null,$review->photo_key);
        }
        
        $feed = new \Ecommerce\Core\Feed\Shopping\NewReview($review);
        $feed->importByEntity($review);
        
        $wmail = new Wmail();
        $wmail->review($review);
        
        session()->flash('code', 'review'.$review->id);

       
        return redirect()->route('review-writing-done');
    }

    public function writingDone()
    {   
        if(!session()->has('code')){
            return abort(404);
        }
        return view('response.pages.review.done', (array)$this->view_component);
    }

    public function comment($id)
    {
        $review = Review::findOrFail($id);

        $customer = $this->view_component->current_customer;
        $comment = new ReviewComment();
        $comment->review_id = $review->id;
        $comment->customer_id =  $customer->id;
        $comment->nick_name = $customer->nickname;
        $comment->content = strip_tags(trim(request()->get('content')));
        $comment->verify = 0;
        $comment->customer_ip = request()->server('REMOTE_ADDR');
        $comment->save();

//        $wmail = new Wmail();
//        $wmail->review($review, $comment);
        return \Redirect::back();
    }

    public function getStep()
    {
        $this->view_component->seo('title', '「Webike-摩托百貨」 - 進口重機, 機車, 改裝零件, 騎士用品, 最新摩托情報!!');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        
        return view('response.pages.review.step', $this->view_component->output());

    }

}