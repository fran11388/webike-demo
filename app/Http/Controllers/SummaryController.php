<?php
namespace App\Http\Controllers;


use App\Components\View\SummaryComponent;

use Barryvdh\Reflection\DocBlock\Type\Collection;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Repository\ReviewRepository;
use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Service\NavigationService;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\SummaryService;
use Everglory\Models\Motor;
use Everglory\Models\Product;
use Illuminate\Http\Request;
use Ecommerce\Service\MotorService;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\ProductService;
use Illuminate\View\View;
use Jenssegers\Agent\Agent;

class SummaryController extends Controller
{
    protected $service;
    private $top;
    private $cache_key;

    public function __construct(SummaryComponent $component)
    {

        parent::__construct($component);

        app()->singleton('EmptyProduct', function () {
            return new Product();
        });

        $segments = request()->segments();

        try {
            for ($i = 0; $i < count($segments) ; $i += 2) {
                $key = $segments[$i];
                $value = $segments[$i + 1];
                switch ($key) {
                    case 'ca':
                        request()->request->add(['category' => $value ]);
                        break;
                    case 'br':
                        request()->request->add(['manufacturer' => $value ]);
                        break;
                    case 'mt':
                        request()->request->add(['motor' => $value ]);
                        break;
                }
            }
        }catch (\Exception $e) {
        }

        $this->is_not_phone = false;
        if(\Ecommerce\Core\Agent::isNotPhone()){
            $this->is_not_phone = true;
        }

        $this->cache_key = http_build_query(request()->only(['motor','category','manufacturer']));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {


        cacheControlProcess(['summary']);
        \Debugbar::startMeasure('render','urlValidate');
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $this->view_component->is_not_phone = $this->is_not_phone;

        $service = app()->make(SummaryService::class);
        if($redirect_url = $service->urlValidate($request)){
            return redirect()->to(str_replace('/dev/index.php','',$redirect_url),301);
        }
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','prepare DNA');
        $convertService = app()->make(ConvertService::class);
        /** @var SummaryService $service */

        $this->view_component->current_category = $service->getCategory();
        $this->view_component->current_manufacturer = $service->getManufacturer();
        $this->view_component->current_motor = $service->getMotor();
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','new_product_search_response');

        //新商品(時間)
        $carousel_configs = [
            'new_products'=> (object) ['collection'=>null,'params'=>(object)['key'=>'new_products','call'=>'new_products','blade'=>'d','limit'=>'6','parameter'=>$request->all(),'shuffle'=>'off','page'=> 1 ]],
            'popular_products'=> (object) ['collection'=>null,'params'=>(object)['key'=>'popular_products','call'=>'popular_products','blade'=>'d','limit'=>'6','parameter'=>$request->all(),'shuffle'=>'off','page'=> 2 ]],
        ];

        $storage_data = [];
        $carouselService = new \Ecommerce\Service\CarouselService($carousel_configs,$storage_data);

        foreach ($carouselService->call() as $key => $carouselData){
            $this->view_component->$key = $carouselData;
        }
        
        $new_product_count = isset($storage_data['new_product_count']) ? $storage_data['new_product_count'] : 0;

        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','on_sales_products');

        //促銷商品(折扣)
        $this->view_component->on_sales_products =  \Cache::tags(['summary'])->remember('summary-on-sales-products-'.$this->cache_key, 86400 * 7, function () use($request,$service,$convertService){
            return $convertService->convertQueryResponseToProducts($service->selectOnSales($request));
        });
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','top_sales_products');

        $top_sales_search_response =  \Cache::tags(['summary'])->remember('summary-top-sales-search-response-'.$this->cache_key, 86400 * 7, function () use($request,$service){
            return $service->selectTopSales($request);
        });
//        $this->view_component->top_sales_products = $convertService->convertQueryResponseToProducts($top_sales_search_response);
        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','hot_manufacturers');
        $this->view_component->manufacturer_info = null;

        if(!$this->view_component->current_manufacturer  ) {
            //熱門品牌
            $this->view_component->hot_manufacturers = $convertService->convertFacetFieldToManufacturers($top_sales_search_response);
        }else{
            $this->view_component->manufacturer_info = $this->view_component->current_manufacturer->info;
        }


        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','search_response');
        $search_response =  \Cache::tags(['summary'])->remember('summary-search-response-'.$this->cache_key, 86400 * 7, function () use($request,$service){
            return $service->selectSummary($request);
        });
        \Debugbar::stopMeasure('render');

//        \Debugbar::startMeasure('render','popular_products');
//        //推薦商品(瀏覽量)
//        $this->view_component->popular_products = $convertService->convertQueryResponseToProducts($search_response);
//        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','current_motor');
        if($this->view_component->current_motor){

            $motorService = new MotorService($this->view_component->current_motor->url_rewrite, ['manufacturer', 'specifications']);
            $this->view_component->genuine_link = $motorService->getGenuineSalesLink();
            $this->view_component->modelInfo = $motorService->getMotorFivePower();
            $this->view_component->info = $motorService->getMotorInformation($search_response,$new_product_count);

            $this->view_component->addMyBikesDisabled = false;
            if($this->view_component->current_customer){
                $myBikeIds = $this->view_component->current_customer->motors->pluck('id')->toArray();
                $this->view_component->addMyBikesDisabled = in_array($this->view_component->current_motor->id, $myBikeIds);
            }
        }
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','getSummaryTitle and tree');

        $this->view_component->summary_title = $service->getSummaryTitle();

        $this->view_component->tree =  \Cache::tags(['summary'])->remember('summary-tree-'.$this->cache_key, 86400 * 7, function () use($search_response){
            return NavigationService::getNavigation($search_response);
        });
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','manufacturer');
        $this->view_component->manufacturer = $convertService->convertFacetFieldToManufacturers($search_response);
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','ranking_avg');
        $this->view_component->ranking_avg =  \Cache::tags(['summary','review'])->remember('summary-ranking_avg-'.$this->cache_key, 86400 * 7, function () {
            return ReviewRepository::getAvgRanking(
                $this->view_component->current_category,
                $this->view_component->current_manufacturer,
                $this->view_component->current_motor
            );
        });
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','reviews');
        $this->view_component->reviews =  \Cache::tags(['summary','review'])->remember('summary-reviews-'.$this->cache_key, 86400 * 7, function () {
            return ReviewRepository::get($this->view_component->current_category, $this->view_component->current_manufacturer, $this->view_component->current_motor , 8);
        });
        \Debugbar::stopMeasure('render');



        if($this->view_component->is_not_phone){
            \Debugbar::startMeasure('render','category_list_view');
            $this->view_component->category_list_view =  \Cache::tags(['summary'])->remember('summary-category-list-view-'.$this->cache_key, 86400 * 7, function (){
                return view('response.pages.summary.partials.category-list',(array)$this->view_component)->render();
            });
        }


        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','subcategory_view');
        $this->view_component->subcategory_view =  \Cache::tags(['summary'])->remember('summary-subcategory_view-'.$this->cache_key, 86400 * 7, function (){
            return view('response.pages.summary.partials.subcategory',(array)$this->view_component)->render();
        });

        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','ranking_avg');
        $this->view_component->ranking_avg =  \Cache::tags(['summary','review'])->remember('summary-ranking_avg-'.$this->cache_key, 86400 * 7, function () {
            return ReviewRepository::getAvgRanking(
                $this->view_component->current_category,
                $this->view_component->current_manufacturer,
                $this->view_component->current_motor
            );
        });
        \Debugbar::stopMeasure('render');
        \Debugbar::startMeasure('render','setFitMotorResult');

        if($this->view_component->is_not_phone) {
            $this->setFitMotorResult();
        }

        \Debugbar::stopMeasure('render');

        \Debugbar::startMeasure('render','rankingPrepare');

        $search_service = app()->make(SearchService::class);

        $this->view_component->ranking_sales_products =  \Cache::tags(['ranking'])->remember('ranking-sales-'.$this->cache_key, 86400 * 7, function () use($request,$search_service,$convertService){

            $ranking_response_sales = $search_service->selectRanking($request);
            return $convertService->convertQueryResponseToProducts($ranking_response_sales);
        });

        request()->request->add(['type' => 'popularity' ]);

        $this->view_component->ranking_popularity_products =  \Cache::tags(['ranking'])->remember('ranking-popularity-'.$this->cache_key, 86400 * 7, function () use($request,$search_service,$convertService){
            $ranking_response_popularity = $search_service->selectRanking($request);
            return $convertService->convertQueryResponseToProducts($ranking_response_popularity);
        });

        $this->view_component->url_path = $request->path();
        
        request()->request->remove('popularity');
        \Debugbar::stopMeasure('render');

        $this->view_component->setBreadcrumbs([
            'mt' => $this->view_component->current_motor,
            'ca' => $this->view_component->current_category,
            'br' => $this->view_component->current_manufacturer,
        ]);
        $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();
//        $this->view_component->interface = ['rules', 'motors', 'brands', 'sort', 'limit', 'products'];

        $this->view_component->render();

        // if(str_contains(request()->getHost(),"amp")){
        //     return view('mobile.pages.amp.summary.index-amp',  (array)$this->view_component);
        // }else{
        //     return view('response.pages.summary.index',  (array)$this->view_component);
        // }
        return view('response.pages.summary.index',  (array)$this->view_component);
    }

    public function lazy(Request $request)
    {
        \Debugbar::disable();
        $this->view_component->is_not_phone = $this->is_not_phone;

        $service = app()->make(SummaryService::class);
        if($service->urlValidate($request)){
            app()->abort('405');
        }
        $convertService = app()->make(ConvertService::class);
        if($request->has('forceTitle')){
            $this->view_component->summary_title = $request->get('forceTitle');
        }else{
            $this->view_component->summary_title = $service->getSummaryTitle();
        }

        $result = new \stdClass();
        $result->html = [];
        $result->html['motor-btn'] = '';
        $this->view_component->current_motor = $service->getMotor();
        $this->view_component->current_manufacturer = $service->getManufacturer();
        $this->view_component->current_category = $service->getCategory();

        $this->setFitMotorResult();

        if(!$this->view_component->is_not_phone and $this->view_component->hasFitMotors) {
            $result->html['motor-btn'] = view('response.pages.summary.partials.custom-parts', (array)$this->view_component)->render();
        }
//        $result->html['custom-parts'] = view('response.pages.summary.partials.custom-parts',(array)$this->view_component)->render();

        return json_encode($result);
    }

    private function setFitMotorResult($search_response = null)
    {
        
        $request = request();
        $service = app()->make(SummaryService::class);
        $service->urlValidate($request);
        $convertService = app()->make(ConvertService::class);

        $fitMotorPackage = new \StdClass;
        $fitMotorPackage->my_bikes = null;
        $fitMotorPackage->fit_mybikes = null;
        $fitMotorPackage->fit_motors = null;
        $fitMotorPackage->fit_motor_products = null;
        $fitMotorPackage->hasFitMotors = true;
        $fitMotorPackage->fit_motor_title = [];
        if($this->view_component->current_category){
            $fitMotorPackage->fit_motor_title[] = $this->view_component->current_category->name;
        }
        if($this->view_component->current_manufacturer){
            $fitMotorPackage->fit_motor_title[] = $this->view_component->current_manufacturer->name;
        }
        $fitMotorPackage->fit_motor_title = implode('&', $fitMotorPackage->fit_motor_title);
        if($this->view_component->current_motor and !$this->view_component->current_manufacturer and !$this->view_component->current_category){
            $fitMotorPackage->hasFitMotors = false;
        }else{
            if ($this->view_component->current_customer and count($this->view_component->current_customer->motors)){
                if(!$search_response){
                    $fitMyBikeResult = $service->selectFitMotorIds($request , $this->view_component->current_customer->motors->pluck('id')->all());
                }else{
                    $fitMyBikeResult = $search_response;
                }
                $fitMotorPackage->my_bikes = $this->view_component->current_customer->motors;
                $fit_mybikes = $convertService->convertFacetFieldToMotors($fitMyBikeResult , $this->view_component->current_customer->motors);
                $fitMotorPackage->fit_mybikes = $fit_mybikes->groupBy('manufacturer');
                $fitMotorPackage->fit_motor_products = $convertService->convertQueryResponseToProducts($fitMyBikeResult,true);
            }
            $cachePackage = \Cache::tags(['summary'])->remember('summary-fitMotor-'.$this->cache_key, 86400 * 7, function () use($search_response, $request, $fitMotorPackage, $convertService, $service){
                $cachePackage = new \StdClass;
                if(!$search_response){
                    $fitMotorResult = $service->selectFitMotorIds($request);
                }else{
                    $fitMotorResult = $search_response;
                }
                $fit_motors = $convertService->convertFacetFieldToMotors($fitMotorResult);
                if($this->view_component->current_customer and count($this->view_component->current_customer->motors)){
                    $my_motor_ids = $this->view_component->current_customer->motors->pluck('id')->toArray();
                    $fit_motors = $fit_motors->filter(function($fit_motor) use($my_motor_ids){
                        return !in_array($fit_motor->id, $my_motor_ids);
                    });
                }
                if($this->view_component->current_motor){
                    $current_motor = $this->view_component->current_motor;
                    $fit_motors = $fit_motors->filter(function($fit_motor) use($current_motor){
                        return $fit_motor->id != $current_motor->id;
                    });
                }
                $fit_motors = $fit_motors->sortByDesc('count'); // method keyBy will take last element.

                $motor_manufacturers = MotorRepository::getMotorManufacturers()->pluck('sort', 'url_rewrite');
                $fit_motors = $fit_motors->groupBy('manufacturer')->take(6);
                $fit_motors = collect($fit_motors);
                $cachePackage->fit_motors = $fit_motors->sortBy(function($manufacturer_group) use($motor_manufacturers){
                    return (int)$motor_manufacturers[$manufacturer_group->first()->manufacturer_url_rewrite];
                });
                if(!(count($cachePackage->fit_motors) + count($fitMotorPackage->fit_mybikes))){
                    $cachePackage->hasFitMotors = false;
                }
                return $cachePackage;
            });

            foreach ($cachePackage as $attr => $value){
                $fitMotorPackage->$attr = $value;
            }
        }

        foreach ($fitMotorPackage as $attr => $value){
            $this->view_component->$attr = $value;
        }
    }


    /**
     * All brands page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brand(Request $request){

        $service = app()->make(SummaryService::class);
        $convertService = app()->make(ConvertService::class);

        //REDIRECT
        foreach(\Everglory\Constants\Category::REDIRECTS as $redirect_from => $redirect_to){
            if(false !== strpos($request->input('category'),$redirect_from)){
                request()->request->replace(['category' => $redirect_to ]);
            }
        }
        /** @var SummaryService $service */


        $search_response = $service->selectManufacturerSummary($request);
        if(!$search_response){
            app()->abort(404);
        }

        /**
         * @var SummaryComponent $this->view_component
         */
        $this->view_component->current_category = $service->getCategory();
        $this->view_component->current_manufacturer = $service->getManufacturer();
        $this->view_component->current_motor = $service->getMotor();

        $this->view_component->tree = NavigationService::getNavigation($search_response);

        $manufacturer = $convertService->convertFacetFieldToManufacturers($search_response);
        $this->view_component->manufacturer_countries = $convertService->convertFacetFieldToManufacturersCountry($search_response);

        $this->view_component->manufacturers = $manufacturer->groupBy(function ($item, $key) {
            $first_char = substr($item->name, 0,1);
            if (preg_match('/[^A-Za-z0-9]/', $first_char)) // '/[^a-z\d]/i' should also work.
            {
                return '其他';
            }elseif(is_numeric($first_char)){
                return '數字';
            }else {
                return strtoupper($first_char);
            }
        })->sortBy(function ($product, $key) {
            if($key == '其他'){
                return 9999;
            }elseif($key == '數字'){
                return 999;
            }else{
                return $key;
            }

        });

        $this->view_component->filters = [
            'country' => ['label'=>'品牌國籍','name'=>request()->input('country')],
            'char' => ['label'=>'字首','name'=>request()->input('char')],
            'category' => ['label'=>'分類','name'=>$this->view_component->current_category ? $this->view_component->current_category->name : ''],
            'motor' => ['label'=>'車型','name'=>$this->view_component->current_motor ? $this->view_component->current_motor->name : ''],
        ];

        $this->view_component->manufacturers_keys = $this->view_component->manufacturers->keys();


        if ($this->view_component->current_customer and count($this->view_component->current_customer->motors)){

            $result = $service->selectFitMotorIds($request , $this->view_component->current_customer->motors->pluck('id')->all());
            $this->view_component->fit_motors = $convertService->convertFacetFieldToMotors($result , $this->view_component->current_customer->motors)->groupBy('manufacturer');
            $this->view_component->fit_motor_products = $convertService->convertQueryResponseToProducts($result,true);
            $this->view_component->my_bikes = $this->view_component->current_customer->motors;
        }


        $top_sales_search_response = $service->selectTopSales($request);
        $this->view_component->hot_manufacturers = $convertService->convertFacetFieldToManufacturers($top_sales_search_response);


        $this->view_component->setBreadcrumbs([
            'br' => 'top',
        ]);

        $this->view_component->render();

        return view('response.pages.summary.brand-index' ,  (array)$this->view_component);
    }


}