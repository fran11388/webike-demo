<?php
namespace App\Http\Controllers;

use Everglory\Models\MotoMarket\Customer;
use Illuminate\Routing\Controller as BaseController;

class MagicController extends BaseController
{
    public function loginWithEmail()
    {
        if(request()->has('email')){
            $customer = \Everglory\Models\Customer::where('email',request()->get('email'))->first();
            if($customer){
                \Auth::logout();
                \Auth::login($customer);
                return redirect()->route('customer');
            }
        }
        return 'Email address not found!';
    }
}