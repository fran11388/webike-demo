<?php
namespace App\Http\Controllers\Benefit;

use App\Http\Controllers\Controller;
use App\Components\View\SimpleComponent;
use Ecommerce\Repository\MotoGp\Gp2017\MotogpRepository;
use Ecommerce\Repository\MotoGp\Gp2018\MotogpRepository as MotogpRepository2018;
use Ecommerce\Repository\MotoGp\Gp2019\MotogpRepository as MotogpRepository2019;
use Ecommerce\Service\AccountService;
use Ecommerce\Service\BikeNewsService;
use Ecommerce\Service\MotoGp\Gp2018\MotoGpService;
use Ecommerce\Service\MotoGp\Gp2019\MotoGpService as MotoGpService2019;
use Illuminate\Http\Request;
use Everglory\Constants\CustomerRole;
use Ecommerce\Repository\MotorRepository;
use Everglory\Models\Motogp\Choice;
use Everglory\Models\Motogp\Racer;
use Everglory\Models\Motogp\Speedway;
use Everglory\Models\Customer;
use Everglory\Models\Mission;
use Everglory\Models\Order\Reward;


class EventController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('benefit');
        $component->setBreadcrumbs('會員好康', $component->base_url);

        if(\Ecommerce\Core\Agent::isNotPhone()){
            $component->is_phone = false;
        }else{
            $component->is_phone = true;
        }
    }

    public function motoGp2017()
    {
        if (!useCache()) {
            \Cache::forget('racerinfo');
            \Cache::forget('racerinfo-blade');
            \Cache::forget('speedwayinfo');
            \Cache::forget('stationinfo-blade');
            \Cache::forget('nowspeedwayinfo');
        }

        $route_name = \Route::currentRouteName();
        $session_redirect_key = 'register-complete-redirect-route';
        if(!$this->view_component->current_customer or ($this->view_component->current_customer and $this->view_component->current_customer->role_id == CustomerRole::REGISTERED)){
            session([$session_redirect_key => $route_name]);
        }else{
            session()->forget($session_redirect_key);
        }

        $photo_home = 'https://img.webike.tw/assets/images/motogp/2017/';
        $this->view_component->photo_home = $photo_home;
        $this->view_component->choices = [];
        if(\Auth::check()){
            $this->view_component->choice_result = MotogpRepository::choice_result();
            $this->view_component->choices = MotogpRepository::getChoices();

            $customer_choices = MotogpRepository::getCustomerChoices();
            $real_choices = MotogpRepository::getreal_choices();
            if( $customer_choices and count($customer_choices) ){
                $this->view_component->speedway_choices = $customer_choices;
                $this->view_component->speedway_real_choices = $real_choices;
            }
            $this->view_component->times = array(25 => 0, 20 => 0, 16 => 0);
            $this->view_component->point = MotogpRepository::getmotogppoint();
            $this->view_component->my_champigns = MotogpRepository::getmychampigns();
            $this->view_component->customer = \Auth::user();
        }
        
        $this->view_component->total_join = MotogpRepository::getParticipantsCustomerCount();
        $this->view_component->stations = MotogpRepository::getstations();
        $this->view_component->other_champigns = MotogpRepository::getotherchampigns();

        $racers = \Cache::rememberForever('racerinfo',function(){
            return MotogpRepository::getRacers();
        });
        $this->view_component->racers = $racers;
        $racer_block = \Cache::rememberForever('racerinfo-blade',function() use($racers,$photo_home){
            $racer_block = view('response.pages.benefit.event.motogp.2017.version2.partials.racer_block')->with(['racers'=>$racers,'photo_home'=>$photo_home])->render();
            return $racer_block;
        });
        $this->view_component->racer_block = $racer_block;

        $speedwaies = \Cache::rememberForever('speedwayinfo',function(){
            return MotogpRepository::getspeedwaies();
        });
        $this->view_component->speedwaies = $speedwaies;
        $tail = '- 「Webike-摩托百貨」';
        $color = array('box-light-blue','box-green','box-light-red2','box-light-red2','box-light-blue','box-green','box-green','box-light-red2','box-light-blue','box-light-blue','box-green','box-light-red2','box-light-red2','box-light-blue','box-green','box-green','box-light-red2','box-light-blue');
        $station_block = view('response.pages.benefit.event.motogp.2017.version2.partials.station_block')->with(['speedwaies'=>$speedwaies,'color'=>$color,'tail'=>$tail])->render();
        $this->view_component->station_block = $station_block;

        $this->view_component->top10 = $this->view_component->racers->sortByDesc('total_scores')->take(10)->all();
        $this->view_component->top10_ids = $this->view_component->racers->sortByDesc('total_scores')->take(10)->map(function($racer, $key){
                return $racer->id;
            })->toArray();
        $this->view_component->top10_score = MotogpRepository::gettop10_score();
        $this->view_component->teamData = MotogpRepository::getteamData();
        $this->view_component->makerData = MotogpRepository::getmakerData();

        $this->view_component->now_speedway = \Cache::rememberForever('nowspeedwayinfo',function() use($racers) {
            return MotogpRepository::getNowSpeedway();
        });

        $this->view_component->nowstation_block = $nowstation_block = view('response.pages.benefit.event.motogp.2017.version2.partials.nowstation_block')->with(['now_speedway'=>$this->view_component->now_speedway,'tail'=>'- 「Webike-摩托百貨」'])->render();

    	$this->view_component->setBreadcrumbs('2017MotoGP冠軍大預測');

        $this->view_component->seo('title','2017MotoGP冠軍大預測');
        $this->view_component->seo('description','最詳細2017年正賽賽程、車手資訊、歷年資訊、賽道簡介，更有賽事活動的好康三重送，「好康1」2017年卡達站至瓦倫西亞站共18站，每站預測出當站冠軍，18站全部預測答對者將可獲得SHOEI X14安全帽一頂。「好康2」預測每站前三名，每站最多$61點，年度最多$1098現金點數回饋。「好康3」敬請把握每站答題機會，最終預測出年度冠軍者，即可獲得AGV K3 SV Bollo 46 賽車安全帽。最新MotoGP排位成績、積分排名、賽事討論，請至「Webike-摩托百貨」。');
        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/banner_facebook.jpg'));
    	$this->view_component->seo('keywords','進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
    	$this->view_component->render();
        return view('response.pages.benefit.event.motogp.2017.version2.index-response', (array)$this->view_component);
    }

    public function postMotoGp2017()
    {
        $scroe_map = [
            1 => 25,
            2 => 20,
            3 => 16,
            4 => 13,
            5 => 11,
            6 => 10,
            7 => 9,
            8 => 8,
            9 => 7,
            10 =>6,
            11 =>5,
            12 =>4,
            13 =>3,
            14 =>2,
            15 =>1,
        ];
        
        $racers = MotogpRepository::getRacers();
        $racers_compare = $racers->filter(function($racer){
            return in_array($racer->id, \Request::get('my_choice'));
        });
        if(count($racers_compare) !== 3){
            return \Redirect::route('benefit-event-motogp-2017');
        }

        if(!\Auth::check()){
            return \Redirect::back()->with('danger','沒登入!');
        }else{
            $now_speedway = MotogpRepository::postspeedway();
            if(!$now_speedway){
                return \Redirect::back()->with('danger','目前非賽事中!');
            }else{
                $customer = \Auth::user();
                if( $customer->role_id == 1 ){
                    return \Redirect::back()->with('danger','須完成"完整註冊"。');
                }

                $choice_check = MotogpRepository::postchoices();
                if(count($choice_check) == 3){
                    \Redirect::back()->with('danger','你已經選完車手!');
                }
                $num = 1;
                foreach (\Request::get('my_choice') as $value) {
                    $choice = new Choice;
                    $choice->speedway_id = $now_speedway->id;
                    $choice->racer_id = $value;
                    if(array_key_exists($num, $scroe_map)){
                        $choice->score = $scroe_map[$num];
                    }else{
                        $choice->score = 0;
                    }
                    $choice->customer_id = $customer->id;
                    $choice->save();
                    $num++;
                }
                return \Redirect::route('benefit-event-motogp-2017', ['finish'=>'true']);
            }
        }
    }

    public function motoGpInfo2017()
    {   
        $this->view_component->setBreadcrumbs('2017MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2017'));
        $this->view_component->setBreadcrumbs('活動說明');
        $this->view_component->seo('title','2017MotoGP冠軍大預測 - 活動說明');
        $this->view_component->seo('description','最詳細2017年正賽賽程、車手資訊、歷年資訊、賽道簡介，更有賽事活動的好康三重送，「好康1」2017年卡達站至瓦倫西亞站共18站，每站預測出當站冠軍，18站全部預測答對者將可獲得AGV K3 SV Bollo 46 賽車安全帽一頂。「好康2」預測每站前三名，每站最多$61點，年度最多$1098現金點數回饋。「好康3」敬請把握每站答題機會，最終預測出年度冠軍者，將可獲得$300元，並有機會贏得AGV K3 SV Bollo 46 賽車安全帽、Webike套裝紀念貼紙及300元禮券。最新MotoGP排位成績、積分排名、賽事討論，請至「Webike-摩托百貨」。');
        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/banner1.png'));
        $this->view_component->render();
        return view('response.pages.benefit.event.motogp.2017.version2.info-response', (array)$this->view_component);
    }
    public function motoGpDetail2017($station)
    {   
        $thisSpeedway = MotogpRepository::thisSpeedway($station);
        $stationNew = MotogpRepository::getMotogpTagNews('2017',$station)->first();
        $this->view_component->stationNew = $stationNew;
        $this->view_component->stationNewsContent = null;
        if($stationNew){
            $stationNewsContent = strip_tags($stationNew->post_content);//过滤换行
            $stationNewsContent = str_replace("\r\n","",$stationNewsContent);//过滤换行
            $stationNewsContent = str_replace("&nbsp;","<br>",$stationNewsContent);//过滤换行
            $this->view_component->stationNewsContent = str_limit( $stationNewsContent,500);
            $this->view_component->stationNew = $stationNew;

            $dom = new \DOMDocument();
            $dom->loadHTML($stationNew->post_content);
            $images = $dom->getElementsByTagName('img');
            $this->view_component->stationNewsimg = [];
            foreach ($images as $key => $value) {
                if(strpos($value->getAttribute('src'), 'promobanner') === false){
                    $this->view_component->stationNewsimg[] = $value->getAttribute('src');
                }
            }
        }
        $this->view_component->thisSpeedway = $thisSpeedway;
        if(!$thisSpeedway){
            return abort('404');
        }
        $this->view_component->racers = MotogpRepository::getRacers();
        $this->view_component->top10 = $this->view_component->racers->sortByDesc('total_scores')->take(10)->all();
        $this->view_component->top10_ids = $this->view_component->racers->sortByDesc('total_scores')->take(10)->map(function($racer, $key){
                return $racer->id;
            })->toArray();
        $this->view_component->top10_score = MotogpRepository::gettop10_score();
        $this->view_component->teamData = MotogpRepository::getteamData();
        $this->view_component->makerData = MotogpRepository::getmakerData();
        $this->view_component->speedwaies = MotogpRepository::getspeedwaies();
        $this->view_component->now_speedway = MotogpRepository::getNowSpeedway();
        $this->view_component->other_champigns = MotogpRepository::getotherchampigns();
        $now_speedway = $this->view_component->now_speedway;

        $this->view_component->setBreadcrumbs('2017MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2017'));
        $this->view_component->setBreadcrumbs("第" . $thisSpeedway->id . "站" . $thisSpeedway->station_name, '');
        $this->view_component->seo('title','2017 MotoGP ' . $thisSpeedway->station_name . '賽事概要');
        $this->view_component->seo('description',$thisSpeedway->station_name . '賽程表、轉播時間、賽道簡介及比賽紀錄、車手車隊名單及目前積分排行等訊息，預測2017MotoGP各分站前三名，猜對即可贏得免費點數，並有機會贏得AGV K3 SV Bollo 46 賽車安全帽、Webike套裝紀念貼紙及300元禮券。');
        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/speedway/'.$thisSpeedway->name.'.jpg'));
        $this->view_component->render();
        return view('benefit.event.motodetail_v1', (array)$this->view_component);
    }

    public function motogpGivepoint()
    {
        if(request()->input('switch') == 'on') {
            $now = date('Y-m-d H:i:s');
            echo "2019MotoGP check program start at " . $now . "<br>";

            $now_speedway = \Everglory\Models\Motogp\Table2019\Speedway::where('active', 1)->orderby('raced_at_local', 'DESC')->first();
            if ($now_speedway) {
                if ($now > $now_speedway->closested_at) {
                    $now_speedway->active = 2;
                    $now_speedway->save();
                }
            }
//            $set_mission = \Ecommerce\Service\MissionService::motogpMissionComplete($now_speedway->name);

            $last_speedway = \Everglory\Models\Motogp\Table2019\Speedway::where('active', 2)->orderby('raced_at_local', 'DESC')->first();
            if ($last_speedway) {
                if ($last_speedway->number_one == 0 and $last_speedway->number_two == 0 and $last_speedway->number_three == 0) {
                    $choices = \Everglory\Models\Motogp\Table2019\Choice::where('speedway_id', $last_speedway->id)->whereNull('customer_id')->orderby('score', 'DESC')->get();
                    foreach ($choices as $key => $choice) {
                        if ($key == 0) {
                            $last_speedway->number_one = $choice->racer_id;
                        } else if ($key == 1) {
                            $last_speedway->number_two = $choice->racer_id;
                        } else if ($key == 2) {
                            $last_speedway->number_three = $choice->racer_id;
                        }
                        $last_speedway->save();
                    }
                    echo "2019MotoGP check program insert real score at " . date('Y-m-d H:i:s') . "<br>";
                }

                if ($last_speedway->number_one and $last_speedway->number_two and $last_speedway->number_three and $last_speedway->reward_flg == 0) {
                    $this->givePointsBySpeedway($last_speedway);
                    echo "2019MotoGP check program give speedway_id = " . $last_speedway->id . " winer point at " . date('Y-m-d H:i:s') . "<br>";

                    $next_speedway = \Everglory\Models\Motogp\Table2019\Speedway::where('active', 0)->orderby('raced_at_local')->first();
                    if ($next_speedway) {
                        $next_speedway->active = 1;
                        $next_speedway->save();
                        echo "2019MotoGP check program open speedway_id = " . $next_speedway->id . " game at " . date('Y-m-d H:i:s') . "<br>";
                    }
                }
            }
            echo "2019MotoGP check program end at " . date('Y-m-d H:i:s') . "<br>";
        }else{
            dd('hello');
        }
    }
    // public function motogpcancelGivepoint(){

    //     $customer_ids = \Everglory\Models\Motogp\Table2018\Choice::where('speedway_id','=',12)->whereNotNull('customer_id')->groupBy('customer_id')->pluck('customer_id');
        
    //     $noget_customer = ['24725','24096'];

    //     $customers = Customer::whereIn('id', $customer_ids)->whereNotIn('id',$noget_customer)->get();

    //     $mission = Mission::where('id','=',704)->first();
        
    //     foreach ($customers as $customer) {
    //         \Ecommerce\Service\PointService::givePoint($mission, $customer, 10);
    //     }
    //     echo "2018MotoGP Givepoint OK";
        
    // }
    private function givePointsBySpeedway( $speedway )
    {
        if( $speedway and $speedway->winerGroup and count($speedway->winerGroup) ){
            $score_map = [1 => 25, 2 => 20, 3 => 16];
            $all_customer_ids = [];
            foreach ($speedway->winerGroup as $key => $winer) {
                if($winer){
                    $customer_ids = \Everglory\Models\Motogp\Table2019\Choice::where('speedway_id', $speedway->id)->where('racer_id', $winer->id)->where('score', $score_map[$key])->whereNotNull('customer_id')->pluck('customer_id');
                    $all_customer_ids = array_merge($all_customer_ids,$customer_ids->toArray());
                    $mission = Mission::where('category', '2019MotoGP')->where('name', 'like', '%'.$speedway->station_name.'%')->where('note', 'like', '%'.$score_map[$key].'點現金點數%')->first();
                    $customers = Customer::whereIn('id', $customer_ids)->get();
                    foreach ($customers as $customer) {
                        \Ecommerce\Service\PointService::givePoint($mission, $customer, $score_map[$key]);
                    }
                }
            }

            $other_choice_customer_ids = \Everglory\Models\Motogp\Table2019\Choice::where('speedway_id', $speedway->id)->whereNotNull('customer_id')->whereNotIn('customer_id',$all_customer_ids)->groupBy('customer_id')->get()->pluck('customer_id');
            $mission = Mission::where('category', '2019MotoGP')->where('name', 'like', '%'.$speedway->station_name.'%')->where('note', 'like', '%10點現金點數%')->first();
            $other_customers = Customer::whereIn('id',$other_choice_customer_ids->toArray())->get();
            foreach($other_customers as $other_customer){
                \Ecommerce\Service\PointService::givePoint($mission, $other_customer, 10);
            }
        }
    }

    public function motoGp2018()
    {
        if (useCache()) {
            \Cache::forget('2018-racerinfo-blade');
            \Cache::forget('2018-speedwayinfo');
            \Cache::forget('2018-stationinfo-blade');
            \Cache::forget('2018-nowspeedwayinfo');
        }

        $this->view_component->choose = false;
        $this->view_component->customer_choices = null;
        $this->view_component->customer_finish = false;
        $this->view_component->customer_choose_racers_score = null;
        $this->view_component->stationNewsContents = [];
        $this->view_component->stationNews = [];
        $this->view_component->stationNewsimgs = [];

        $repository = new MotogpRepository2018;

        $this->view_component->now_speedway = \Cache::rememberForever('2018-nowspeedwayinfo',function() use($repository){
            return $repository->getNowSpeedway();
        });
        $this->view_component->racers = $repository->getRacers();

        $this->view_component->choices = [];
        if(\Auth::check()){
            $this->view_component->choice_result = $repository->choice_result();
            $this->view_component->choices = $repository->getChoices();

            $customer_choices = $repository->getCustomerChoices();
            $real_choices = $repository->getreal_choices();
            if( $customer_choices and count($customer_choices) ){
                $this->view_component->speedway_choices = $customer_choices;
                $this->view_component->speedway_real_choices = $real_choices;
            }
            $this->view_component->times = array(25 => 0, 20 => 0, 16 => 0);
            $this->view_component->points_current = $repository->getMotoGpPoint();
            $this->view_component->my_champigns = $repository->getMyChampigns();
            $this->view_component->customer = \Auth::user();
            $this->view_component->customer_choices = $repository->getCustomerChoices();
            $this->view_component->customer_choose_racers_score = $repository->getCustomerChooseRacersScore(3);
        }

        $sort = true;
        if($this->view_component->now_speedway and $this->view_component->now_speedway->id == 1){
            $this->view_component->top10 = $this->view_component->racers->take(10)->all();
            $sort = false;
        }else{
            $this->view_component->top10 = $this->view_component->racers->sortByDesc('total_scores')->take(10)->all();
        }

        $this->view_component->top10_score = $repository->getTopsScore();
        $this->view_component->teamData = $repository->getTeamData($sort);
        $this->view_component->makerData = $repository->getMakerData($sort);

        $speedwaies = $repository->getspeedwaies();
        $this->view_component->speedwaies = $speedwaies;

        if($this->view_component->is_phone){
            $stationNews = $repository->getMotogpTagNews('2018','MotoGP',1);
        }else{
            $stationNews = $repository->getMotogpTagNews('2018','MotoGP',5);
        }
        $this->view_component->stationNew = $stationNews;
        $this->view_component->stationNewsContent = null;
        if(count($stationNews)){
            foreach($stationNews as $key => $stationNew){
                $stationNewsContent = strip_tags($stationNew->post_content);//过滤换行
                $stationNewsContent = str_replace("\r\n","",$stationNewsContent);//过滤换行
                $stationNewsContent = str_replace("&nbsp;","",$stationNewsContent);//过滤换行
                if($key == 0){
                    $this->view_component->stationNewsContents[] = str_limit( $stationNewsContent,150);
                }elseif($this->view_component->is_phone){
                    $this->view_component->stationNewsContents[] = str_limit( $stationNewsContent,100);
                }else{
                    $this->view_component->stationNewsContents[] = str_limit( $stationNewsContent,50);
                }

                $this->view_component->stationNews[] = $stationNew;

                $dom = new \DOMDocument();
                $dom->loadHTML($stationNew->post_content);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $key => $value) {
                    if(strpos($value->getAttribute('src'), 'promobanner') === false){
                        if($key == 0){
                            $this->view_component->stationNewsimgs[] = $value->getAttribute('src');
                        }
                    }
                }
            }
        }
        $route_name = \Route::currentRouteName();
        $session_redirect_key = 'register-complete-redirect-route';
        $session_url_key = 'register-complete-redirect-url';
        if(!\Auth::check()){
            session([$session_redirect_key => $route_name]);
            session([$session_url_key => \URL::route('benefit-event-motogp-2018')]);
        }else{
            session()->forget($session_redirect_key);
        }

        $this->view_component->setBreadcrumbs('2018 MotoGP冠軍大預測');

        $this->view_component->seo('title','2018 MotoGP 冠軍大預測-站站有獎，最大獎萬元購物金等你帶回家，還有更多好康優惠!!');
        $this->view_component->seo('description','2018 MotoGP 冠軍大預測-站站有獎，最大獎萬元購物金等你帶回家，連續19站免費參加，沒有猜中也不要灰心，今年特別新增安慰獎，參加通通有獎。另外更幫您整理2018 MotoGP最詳細排位賽、正賽、賽程表、轉播時間表、車手資訊、歷年紀錄、賽道資訊、最快單圈、最新排名、MotoGP新聞....更能掌握最新、最豐富全方位的2018 MotoGP資訊。');
        $this->view_component->seo('image', assetRemote('shopping/image/banner/motogp-mobile.jpg'));
    	$this->view_component->seo('keywords','Dovizioso, Dovi, Zarco, Petrucci, Luthi, Bautista, 巴弟, 捌弟, Morbideli, Vinales, Pedrosa, 侍, 小丹尼, 中上貴晶, Nakagami, Rossi, 羅西, VR46, 羅老闆, Marc Marquez, MM93, 馬克斯, Lorenzo, 羅倫佐, L99, 杜漢, Doohan, Stoner, 石頭人, 力豹士, Repsol HONDA, MOVISTAR YAMAHA, Tech3, LCR, SKY, RED BULL, MARC VDS, 卡達, 阿根廷, 美國, 西班牙, 法國, 英國, 義大利, 加泰隆尼亞, 阿森, 德國, 捷克, 奧地利, 紅牛, 銀石, 聖馬利諾, 亞拉岡, 泰國, 武里南, 日本, 茂木, 澳洲, 菲利浦島, 馬來西亞, 雪邦, 瓦倫西亞, PULL&BEAR, GIVI, OAKLEY, RED BULL, 米其林, 登陸普, Michelin, DUNLOP, 倍耐力');
    	$this->view_component->render();

        return view('response.pages.benefit.event.motogp.2018.home.index', (array)$this->view_component);
    }

    public function postMotoGp2018()
    {
        $scroe_map = [
            1 => 25,
            2 => 20,
            3 => 16,
            4 => 13,
            5 => 11,
            6 => 10,
            7 => 9,
            8 => 8,
            9 => 7,
            10 =>6,
            11 =>5,
            12 =>4,
            13 =>3,
            14 =>2,
            15 =>1,
        ];
        $repository = new MotogpRepository2018;

        $racers = $repository->getRacers();
        $racers_compare = $racers->filter(function($racer){
            return in_array($racer->id, \Request::get('my_choice'));
        });

        if(count($racers_compare) !== 3){
            return \Redirect::route('benefit-event-motogp-2018');
        }

        if(!\Auth::check()){
            return \Redirect::back()->with('danger','沒登入!');
        }else{
            $now_speedway = $repository->getNowSpeedway();
            if(!$now_speedway){
                return \Redirect::back()->with('danger','目前非賽事中!');
            }elseif($now_speedway and strtotime(date('Y-m-d H:i:s')) > strtotime($now_speedway->closested_at)){
                return \Redirect::back()->with('danger','正賽開始，投票已截止!');
            }else{
                $customer = \Auth::user();
                if( $customer->role_id == 1 ){
                    return \Redirect::back()->with('danger','須完成"完整註冊"。');
                }

                $choice_check = $repository->postchoices();
                if(count($choice_check) == 3){
                    \Redirect::back()->with('danger','你已經選完車手!');
                }else{
                    $num = 1;
                    foreach (\Request::get('my_choice') as $value) {
                        $choice = new \Everglory\Models\Motogp\Table2018\Choice;
                        $choice->speedway_id = $now_speedway->id;
                        $choice->racer_id = $value;
                        if(array_key_exists($num, $scroe_map)){
                            $choice->score = $scroe_map[$num];
                        }else{
                            $choice->score = 0;
                        }
                        $choice->customer_id = $customer->id;
                        $choice->save();
                        $num++;
                    }
                }
                return \Redirect::route('benefit-event-motogp-2018', ['finish'=>'true']);
            }
        }
    }

    public function postRateChoose()
    {
        $repository = new MotogpRepository2018;
        $racers = \Cache::rememberForever('2018-racersinfo',function() use($repository){
            $racers = $repository->getRacers();
            return $racers;
        });
        $service = new MotoGpService;
        $customer_choose_racers_rate = $repository->getAllCustomerChooseRacersRate();
        $random_racers = $service->getRandomRacers($racers,$customer_choose_racers_rate,3);

        return json_encode($random_racers);
    }
    public function motoGprider2018($racer_number)
    {
        if (!useCache()) {
            \Cache::tags('motogp-2018')->flush();
        }

        $repository = new MotogpRepository2018;
        $this->view_component->now_speedway = \Cache::rememberForever('2018-nowspeedwayinfo',function() use($repository){
            return $repository->getNowSpeedway();
        });

        $this->view_component->racers = $repository->getRacers();

        $this->view_component->racerInfos = MotoGpService::getRacerInfo($racer_number, 'number');
        if($this->view_component->now_speedway and $this->view_component->now_speedway->id == 1){
            $this->view_component->top10 = MotogpRepository2018::getRacers()->all();
        }else{
            $this->view_component->top10 = MotogpRepository2018::getRacers()->sortByDesc('total_scores')->all();
        }

        $this->view_component->top10_score = MotogpRepository2018::getTopsScore();

        $this->view_component->setBreadcrumbs('2018 MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2018'));
        $this->view_component->setBreadcrumbs( $this->view_component->racerInfos->racer->number. ' ' .$this->view_component->racerInfos->racer->name . '車手資訊');
        $this->view_component->seo('title','2018 MotoGP - ' .$this->view_component->racerInfos->racer->team_name. ' 車隊 ' .$this->view_component->racerInfos->racer->name . ' 車手簡歷');
       $this->view_component->seo('description','2018 MotoGP - ' .$this->view_component->racerInfos->racer->name .  ' 車手簡歷。詳細生日、身高、體重、歷年紀錄、即時成績一次全部囊括。另外還有生涯概要、歷年成績、目前成績、各分站積分趨勢圖、2018車手積分排行...等詳細資訊盡在2018 MotoGP 冠軍大預測。');
       $this->view_component->seo('keywords','Dovizioso, Dovi, Zarco, Petrucci, Luthi, Bautista, 巴弟, 捌弟, Morbideli, Vinales, Pedrosa, 侍, 小丹尼, 中上貴晶, Nakagami, Rossi, 羅西, VR46, 羅老闆, Marc Marquez, MM93, 馬克斯, Lorenzo, 羅倫佐, L99, 杜漢, Doohan, Stoner, 石頭人');
//        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/speedway/'.$thisSpeedway->name.'.jpg'));
        $this->view_component->render();
        return view('response.pages.benefit.event.motogp.2018.home.rider', (array)$this->view_component);
    }

    public function motoGpInfo2018()
    {   
        $this->view_component->setBreadcrumbs('2018 MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2018'));
        $this->view_component->setBreadcrumbs('活動說明');
        $this->view_component->seo('title','2018 MotoGP 冠軍大預測 活動說明');
        $this->view_component->seo('description','2018 MotoGP 冠軍大預測 活動說明、遊戲規則、活動辦法、獲獎條件、遊戲時間...想拿大獎一定要看。2018 MotoGP 冠軍大預測，連續19站免費參加、站站有獎，預測總冠軍萬元購物金等你帶回家。');
        $this->view_component->seo('keywords','Dovizioso, Dovi, Zarco, Petrucci, Luthi, Bautista, 巴弟, 捌弟, Morbideli, Vinales, Pedrosa, 侍, 小丹尼, 中上貴晶, Nakagami, Rossi, 羅西, VR46, 羅老闆, Marc Marquez, MM93, 馬克斯, Lorenzo, 羅倫佐, L99, 杜漢, Doohan, Stoner, 石頭人, 力豹士, Repsol HONDA, MOVISTAR YAMAHA, Tech3, LCR, SKY, RED BULL, MARC VDS, 卡達, 阿根廷, 美國, 西班牙, 法國, 英國, 義大利, 加泰隆尼亞, 阿森, 德國, 捷克, 奧地利, 紅牛, 銀石, 聖馬利諾, 亞拉岡, 泰國, 武里南, 日本, 茂木, 澳洲, 菲利浦島, 馬來西亞, 雪邦, 瓦倫西亞, PULL&BEAR, GIVI, OAKLEY, RED BULL, 米其林, 登陸普, Michelin, DUNLOP, 倍耐力');
        $this->view_component->render();

        return view('response.pages.benefit.event.motogp.2018.home.info', (array)$this->view_component);
    }

    public function motoGpDetail2018($station)
    {
        $this->view_component->stationNewsimg = [];

        $repository = new \Ecommerce\Repository\MotoGp\Gp2018\MotogpRepository;
        $speedwaies = $repository->getspeedwaies();
        $this->view_component->speedwaies = $speedwaies;

        $repository = new MotogpRepository2018;
        $thisSpeedway = $repository->thisSpeedway($station);
        // dd($thisSpeedway);
        $stationNew = $repository->getMotogpTagNews('2018',$station,'1')->first();
        $this->view_component->stationNew = $stationNew;
        $this->view_component->stationNewsContent = null;
        if($stationNew){
            $stationNewsContent = strip_tags($stationNew->post_content);//过滤换行
            $stationNewsContent = str_replace("\r\n","",$stationNewsContent);//过滤换行
            $stationNewsContent = str_replace("&nbsp;","<br>",$stationNewsContent);//过滤换行
            $this->view_component->stationNewsContent = str_limit( $stationNewsContent,500);
            $this->view_component->stationNew = $stationNew;

            $dom = new \DOMDocument();
            $dom->loadHTML($stationNew->post_content);
            $images = $dom->getElementsByTagName('img');
            $this->view_component->stationNewsimg = [];
            foreach ($images as $key => $value) {
                if(strpos($value->getAttribute('src'), 'promobanner') === false){
                    $this->view_component->stationNewsimg[] = $value->getAttribute('src');
                }
            }
        }
        $this->view_component->thisSpeedway = $thisSpeedway;
        if(!$thisSpeedway){
            return abort('404');
        }
        $this->view_component->racers = $repository->getRacers();
        $this->view_component->top10 = $this->view_component->racers->sortByDesc('total_scores')->take(10)->all();
        $this->view_component->top10_ids = $this->view_component->racers->sortByDesc('total_scores')->take(10)->map(function($racer, $key){
            return $racer->id;
        })->toArray();
        // $this->view_component->top10_score = $repository->gettop10_score();
        $this->view_component->teamData = $repository->getteamData();
        $this->view_component->makerData = $repository->getmakerData();
        $this->view_component->speedwaies = $repository->getspeedwaies();
        $this->view_component->now_speedway = $repository->getNowSpeedway();
        $this->view_component->other_champigns = $repository->getotherchampigns();
        $now_speedway = $this->view_component->now_speedway;
        
        $this->view_component->setBreadcrumbs('2018 MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2018'));
        $this->view_component->setBreadcrumbs("第" . $thisSpeedway->id . "站" . $thisSpeedway->station_name, '');
        $this->view_component->seo('title','2018 MotoGP ' . $thisSpeedway->station_name . ' ' . $thisSpeedway->speedway_name . '資訊');
        $this->view_component->seo('description','2018 MotoGP ' . $thisSpeedway->station_name . ' ' . $thisSpeedway->speedway_name . '資訊，歷史紀錄、最快單圈、去年正賽成績、電視轉播、LIVE時間一次囊括。 另外還有歷年頒獎台成績、賽道紀錄、最高速度、2017年紀錄、賽後報導...等詳細資訊盡在2018 MotoGP 冠軍大預測。');
        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/2018/speedway/'.$thisSpeedway->id.'.jpg'));
        $this->view_component->seo('keywords','卡達, 阿根廷, 美國, 西班牙, 法國, 英國, 義大利, 加泰隆尼亞, 阿森, 德國, 捷克, 奧地利, 紅牛, 銀石, 聖馬利諾, 亞拉岡, 泰國, 武里南, 日本, 茂木, 澳洲, 菲利浦島, 馬來西亞, 雪邦, 瓦倫西亞');
        $this->view_component->render();

        return view('response.pages.benefit.event.motogp.2018.home.Speedway', (array)$this->view_component);
    }

       public function motoGp2019()
    {        
        if (useCache()) {
            \Cache::forget('2019-racerinfo-blade');
            \Cache::forget('2019-speedwayinfo');
            \Cache::forget('2019-stationinfo-blade');
            \Cache::forget('2019-nowspeedwayinfo');
            \Cache::forget('2019-racersinfo');
        }

        $this->view_component->choose = false;
        $this->view_component->customer_choices = null;
        $this->view_component->customer_finish = false;
        $this->view_component->customer_choose_racers_score = null;
        $this->view_component->stationNewsContents = [];
        $this->view_component->stationNews = [];
        $this->view_component->stationNewsimgs = [];

        $repository = new MotogpRepository2019;

        $this->view_component->now_speedway = \Cache::rememberForever('2019-nowspeedwayinfo',function() use($repository){
            return $repository->getNowSpeedway();
        });
        $this->view_component->racers = $repository->getRacers();

        $this->view_component->choices = [];
        if(\Auth::check()){
            $this->view_component->choice_result = $repository->choice_result();
            $this->view_component->choices = $repository->getChoices();

            $customer_choices = $repository->getCustomerChoices();
            $real_choices = $repository->getreal_choices();
            if( $customer_choices and count($customer_choices) ){
                $this->view_component->speedway_choices = $customer_choices;
                $this->view_component->speedway_real_choices = $real_choices;
            }
            $this->view_component->times = array(25 => 0, 20 => 0, 16 => 0);
            $this->view_component->points_current = $repository->getMotoGpPoint();
            $this->view_component->my_champigns = $repository->getMyChampigns();
            $this->view_component->customer = \Auth::user();
            $this->view_component->customer_choices = $repository->getCustomerChoices();
            $this->view_component->customer_choose_racers_score = $repository->getCustomerChooseRacersScore(3);
        }

        $sort = true;
        if($this->view_component->now_speedway and $this->view_component->now_speedway->id == 1){
            $this->view_component->top10 = $this->view_component->racers->take(10)->all();
            $sort = false;
        }else{
            $this->view_component->top10 = $this->view_component->racers->sortByDesc('total_scores')->take(10)->all();
        }

        $this->view_component->top10_score = $repository->getTopsScore();
        $this->view_component->teamData = $repository->getTeamData($sort);
        $this->view_component->makerData = $repository->getMakerData($sort);

        $speedwaies = $repository->getspeedwaies();
        $this->view_component->speedwaies = $speedwaies;

        if($this->view_component->is_phone){
            $stationNews = $repository->getMotogpTagNews('2019','MotoGP',1);
        }else{
            $stationNews = $repository->getMotogpTagNews('2019','MotoGP',5);
        }
        $this->view_component->stationNew = $stationNews;
        $this->view_component->stationNewsContent = null;
        if(count($stationNews)){
            foreach($stationNews as $key => $stationNew){
                $stationNewsContent = strip_tags($stationNew->post_content);//过滤换行
                $stationNewsContent = str_replace("\r\n","",$stationNewsContent);//过滤换行
                $stationNewsContent = str_replace("&nbsp;","",$stationNewsContent);//过滤换行
                if($key == 0){
                    $this->view_component->stationNewsContents[] = str_limit( $stationNewsContent,150);
                }elseif($this->view_component->is_phone){
                    $this->view_component->stationNewsContents[] = str_limit( $stationNewsContent,100);
                }else{
                    $this->view_component->stationNewsContents[] = str_limit( $stationNewsContent,50);
                }

                $this->view_component->stationNews[] = $stationNew;

                $dom = new \DOMDocument();
                $dom->loadHTML($stationNew->post_content);
                $images = $dom->getElementsByTagName('img');
                foreach ($images as $key => $value) {
                    if(strpos($value->getAttribute('src'), 'promobanner') === false){
                        if($key == 0){
                            $this->view_component->stationNewsimgs[] = $value->getAttribute('src');
                        }
                    }
                }
            }
        }
        $route_name = \Route::currentRouteName();
        $session_redirect_key = 'register-complete-redirect-route';
        $session_url_key = 'register-complete-redirect-url';
        if(!\Auth::check()){
            session([$session_redirect_key => $route_name]);
            session([$session_url_key => \URL::route('benefit-event-motogp-2019')]);
        }else{
            session()->forget($session_redirect_key);
        }

        $this->view_component->thisSpeedway = $this->view_component->now_speedway;

        $this->view_component->setBreadcrumbs('2019 MotoGP冠軍預測');

        $this->view_component->seo('title','2019 MotoGP 冠軍預測-站站有獎，最大獎萬元購物金等你帶回家，還有更多好康優惠!!');
        $this->view_component->seo('description','2019 MotoGP 冠軍大預測-站站有獎，最大獎萬元購物金等你帶回家，連續19站免費參加，沒有猜中也不要灰心，今年特別新增安慰獎，參加通通有獎。另外更幫您整理2019 MotoGP最詳細排位賽、正賽、賽程表、轉播時間表、車手資訊、歷年紀錄、賽道資訊、最快單圈、最新排名、MotoGP新聞....更能掌握最新、最豐富全方位的2019 MotoGP資訊。');
        $this->view_component->seo('image', assetRemote('shopping/image/banner/motogp-mobile.jpg'));
        $this->view_component->seo('keywords','Dovizioso, Dovi, Zarco, Petrucci, Luthi, Bautista, 巴弟, 捌弟, Morbideli, Vinales, Pedrosa, 侍, 小丹尼, 中上貴晶, Nakagami, Rossi, 羅西, VR46, 羅老闆, Marc Marquez, MM93, 馬克斯, Lorenzo, 羅倫佐, L99, 杜漢, Doohan, Stoner, 石頭人, 力豹士, Repsol HONDA, MOVISTAR YAMAHA, Tech3, LCR, SKY, RED BULL, MARC VDS, 卡達, 阿根廷, 美國, 西班牙, 法國, 英國, 義大利, 加泰隆尼亞, 阿森, 德國, 捷克, 奧地利, 紅牛, 銀石, 聖馬利諾, 亞拉岡, 泰國, 武里南, 日本, 茂木, 澳洲, 菲利浦島, 馬來西亞, 雪邦, 瓦倫西亞, PULL&BEAR, GIVI, OAKLEY, RED BULL, 米其林, 登陸普, Michelin, DUNLOP, 倍耐力');
        $this->view_component->render();

        return view('response.pages.benefit.event.motogp.2019.home.index', (array)$this->view_component);
    }

    public function postMotoGp2019()
    {
        $scroe_map = [
            1 => 25,
            2 => 20,
            3 => 16,
            4 => 13,
            5 => 11,
            6 => 10,
            7 => 9,
            8 => 8,
            9 => 7,
            10 =>6,
            11 =>5,
            12 =>4,
            13 =>3,
            14 =>2,
            15 =>1,
        ];
        $repository = new MotogpRepository2019;

        $racers = $repository->getRacers();
        $racers_compare = $racers->filter(function($racer){
            return in_array($racer->id, \Request::get('my_choice'));
        });

        if(count($racers_compare) !== 3){
            return \Redirect::route('benefit-event-motogp-2019');
        }

        if(!\Auth::check()){
            return \Redirect::back()->with('danger','沒登入!');
        }else{
            $now_speedway = $repository->getNowSpeedway();
            if(!$now_speedway){
                return \Redirect::back()->with('danger','目前非賽事中!');
            }elseif($now_speedway and strtotime(date('Y-m-d H:i:s')) > strtotime($now_speedway->closested_at)){
                return \Redirect::back()->with('danger','正賽開始，投票已截止!');
            }else{
                $customer = \Auth::user();
                if( $customer->role_id == 1 ){
                    return \Redirect::back()->with('danger','須完成"完整註冊"。');
                }

                $choice_check = $repository->postchoices();
                if(count($choice_check) == 3){
                    \Redirect::back()->with('danger','你已經選完車手!');
                }else{
                    $num = 1;
                    foreach (\Request::get('my_choice') as $value) {
                        $choice = new \Everglory\Models\Motogp\Table2019\Choice;
                        $choice->speedway_id = $now_speedway->id;
                        $choice->racer_id = $value;
                        if(array_key_exists($num, $scroe_map)){
                            $choice->score = $scroe_map[$num];
                        }else{
                            $choice->score = 0;
                        }
                        $choice->customer_id = $customer->id;
                        $choice->save();
                        $num++;
                    }
                }
                return \Redirect::route('benefit-event-motogp-2019', ['finish'=>'true']);
            }
        }
    }

    public function motoGp2019postRateChoose()
    {
        $repository = new MotogpRepository2019;
        $racers = \Cache::rememberForever('2019-racersinfo',function() use($repository){
            $racers = $repository->getRacers();
            return $racers;
        });
        $service = new MotoGpService2019;
        $customer_choose_racers_rate = $repository->getAllCustomerChooseRacersRate();
        $random_racers = $service->getRandomRacers($racers,$customer_choose_racers_rate,3);

        return json_encode($random_racers);
    }
    public function motoGprider2019($racer_number)
    {
        if (!useCache()) {
            \Cache::tags('motogp-2019')->flush();
        }

        $repository = new MotogpRepository2019;
        $this->view_component->now_speedway = \Cache::rememberForever('2019-nowspeedwayinfo',function() use($repository){
            return $repository->getNowSpeedway();
        });

        $this->view_component->racers = $repository->getRacers();

        $this->view_component->racerInfos = MotoGpService2019::getRacerInfo($racer_number, 'number');
        if($this->view_component->now_speedway and $this->view_component->now_speedway->id == 1){
            $this->view_component->top10 = MotogpRepository2019::getRacers()->all();
        }else{
            $this->view_component->top10 = MotogpRepository2019::getRacers()->sortByDesc('total_scores')->all();
        }

        $this->view_component->top10_score = MotogpRepository2019::getTopsScore();

        $this->view_component->setBreadcrumbs('2019 MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2019'));
        $this->view_component->setBreadcrumbs( $this->view_component->racerInfos->racer->number. ' ' .$this->view_component->racerInfos->racer->name . '車手資訊');
        $this->view_component->seo('title','2019 MotoGP - ' .$this->view_component->racerInfos->racer->team_name. ' 車隊 ' .$this->view_component->racerInfos->racer->name . ' 車手簡歷');
       $this->view_component->seo('description','2019 MotoGP - ' .$this->view_component->racerInfos->racer->name .  ' 車手簡歷。詳細生日、身高、體重、歷年紀錄、即時成績一次全部囊括。另外還有生涯概要、歷年成績、目前成績、各分站積分趨勢圖、2019車手積分排行...等詳細資訊盡在2019 MotoGP 冠軍大預測。');
       $this->view_component->seo('keywords','Dovizioso, Dovi, Zarco, Petrucci, Luthi, Bautista, 巴弟, 捌弟, Morbideli, Vinales, Pedrosa, 侍, 小丹尼, 中上貴晶, Nakagami, Rossi, 羅西, VR46, 羅老闆, Marc Marquez, MM93, 馬克斯, Lorenzo, 羅倫佐, L99, 杜漢, Doohan, Stoner, 石頭人');
//        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/speedway/'.$thisSpeedway->name.'.jpg'));
        $this->view_component->render();
        return view('response.pages.benefit.event.motogp.2019.home.rider', (array)$this->view_component);
    }

    public function motoGpInfo2019()
    {   
        $this->view_component->setBreadcrumbs('2019 MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2019'));
        $this->view_component->setBreadcrumbs('活動說明');
        $this->view_component->seo('title','2019 MotoGP 冠軍大預測 活動說明');
        $this->view_component->seo('description','2019 MotoGP 冠軍大預測 活動說明、遊戲規則、活動辦法、獲獎條件、遊戲時間...想拿大獎一定要看。2019 MotoGP 冠軍大預測，連續19站免費參加、站站有獎，預測總冠軍萬元購物金等你帶回家。');
        $this->view_component->seo('keywords','Dovizioso, Dovi, Zarco, Petrucci, Luthi, Bautista, 巴弟, 捌弟, Morbideli, Vinales, Pedrosa, 侍, 小丹尼, 中上貴晶, Nakagami, Rossi, 羅西, VR46, 羅老闆, Marc Marquez, MM93, 馬克斯, Lorenzo, 羅倫佐, L99, 杜漢, Doohan, Stoner, 石頭人, 力豹士, Repsol HONDA, MOVISTAR YAMAHA, Tech3, LCR, SKY, RED BULL, MARC VDS, 卡達, 阿根廷, 美國, 西班牙, 法國, 英國, 義大利, 加泰隆尼亞, 阿森, 德國, 捷克, 奧地利, 紅牛, 銀石, 聖馬利諾, 亞拉岡, 泰國, 武里南, 日本, 茂木, 澳洲, 菲利浦島, 馬來西亞, 雪邦, 瓦倫西亞, PULL&BEAR, GIVI, OAKLEY, RED BULL, 米其林, 登陸普, Michelin, DUNLOP, 倍耐力');
        $this->view_component->render();

        return view('response.pages.benefit.event.motogp.2019.home.info', (array)$this->view_component);
    }

    public function motoGpDetail2019($station)
    {
        $this->view_component->stationNewsimg = [];

        $repository = new \Ecommerce\Repository\MotoGp\Gp2019\MotogpRepository;
        $speedwaies = $repository->getspeedwaies();
        $this->view_component->speedwaies = $speedwaies;

        $repository = new MotogpRepository2019;
        $thisSpeedway = $repository->thisSpeedway($station);
        // dd($thisSpeedway);
        $stationNew = $repository->getMotogpTagNews('2019',$station,'1')->first();
        $this->view_component->stationNew = $stationNew;
        $this->view_component->stationNewsContent = null;
        if($stationNew){
            $stationNewsContent = strip_tags($stationNew->post_content);//过滤换行
            $stationNewsContent = str_replace("\r\n","",$stationNewsContent);//过滤换行
            $stationNewsContent = str_replace("&nbsp;","<br>",$stationNewsContent);//过滤换行
            $this->view_component->stationNewsContent = str_limit( $stationNewsContent,500);
            $this->view_component->stationNew = $stationNew;

            $dom = new \DOMDocument();
            $dom->loadHTML($stationNew->post_content);
            $images = $dom->getElementsByTagName('img');
            $this->view_component->stationNewsimg = [];
            foreach ($images as $key => $value) {
                if(strpos($value->getAttribute('src'), 'promobanner') === false){
                    $this->view_component->stationNewsimg[] = $value->getAttribute('src');
                }
            }
        }
        $this->view_component->thisSpeedway = $thisSpeedway;
        if(!$thisSpeedway){
            return abort('404');
        }
        $this->view_component->racers = $repository->getRacers();
        $this->view_component->top10 = $this->view_component->racers->sortByDesc('total_scores')->take(10)->all();
        $this->view_component->top10_ids = $this->view_component->racers->sortByDesc('total_scores')->take(10)->map(function($racer, $key){
            return $racer->id;
        })->toArray();
        // $this->view_component->top10_score = $repository->gettop10_score();
        $this->view_component->teamData = $repository->getteamData();
        $this->view_component->makerData = $repository->getmakerData();
        $this->view_component->speedwaies = $repository->getspeedwaies();
        $this->view_component->now_speedway = $repository->getNowSpeedway();
        $this->view_component->other_champigns = $repository->getotherchampigns();
        $now_speedway = $this->view_component->now_speedway;
        
        $this->view_component->setBreadcrumbs('2019 MotoGP冠軍大預測', \URL::route('benefit-event-motogp-2019'));
        $this->view_component->setBreadcrumbs("第" . $thisSpeedway->id . "站" . $thisSpeedway->station_name, '');
        $this->view_component->seo('title','2019 MotoGP ' . $thisSpeedway->station_name . ' ' . $thisSpeedway->speedway_name . '資訊');
        $this->view_component->seo('description','2019 MotoGP ' . $thisSpeedway->station_name . ' ' . $thisSpeedway->speedway_name . '資訊，歷史紀錄、最快單圈、去年正賽成績、電視轉播、LIVE時間一次囊括。 另外還有歷年頒獎台成績、賽道紀錄、最高速度、2017年紀錄、賽後報導...等詳細資訊盡在2019 MotoGP 冠軍大預測。');
        $this->view_component->seo('image', assetRemote('image/benefit/event/motogp/2019/speedway/'.$thisSpeedway->id.'.jpg'));
        $this->view_component->seo('keywords','卡達, 阿根廷, 美國, 西班牙, 法國, 英國, 義大利, 加泰隆尼亞, 阿森, 德國, 捷克, 奧地利, 紅牛, 銀石, 聖馬利諾, 亞拉岡, 泰國, 武里南, 日本, 茂木, 澳洲, 菲利浦島, 馬來西亞, 雪邦, 瓦倫西亞');
        $this->view_component->render();

        return view('response.pages.benefit.event.motogp.2019.home.Speedway', (array)$this->view_component);
    }
    
}
?>