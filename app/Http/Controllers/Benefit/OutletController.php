<?php
namespace App\Http\Controllers\Benefit;

use App\Http\Controllers\Controller;
use Ecommerce\Service\NavigationService;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\Search\ConvertService;
use App\Components\View\SimpleComponent;
use Everglory\Models\Product;
use Ecommerce\Support\LengthAwarePaginator;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Repository\MotorRepository;
use URL;

class OutletController extends Controller
{

    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);

        app()->singleton('EmptyProduct', function () {
            return new Product();
        });

        $this->view_component->setBreadcrumbs('會員好康', URL::route('benefit'));
    }

    public function index()
    {
        /*
         * use solr search get results
         * put rules in request
         * at finish, remove preload value
         */
        $service = app()->make(SearchService::class);
        $convertService = app()->make(ConvertService::class);
        $convertService->addParameterToRequest(request()->all());
        $preload = ['product_type' => 5];
        request()->request->add($preload);
        if(!request()->input('sort')){
            request()->request->add(['sort' => 'new']); //強制新上架排序
        }
        if(!request()->has('limit')){
            request()->request->add(['limit' => 40]); //強制預設40項
        }
        $request = request();
        $search_response = $service->selectList($request);

        $convertService->restoreRequestParameter();
        $convertService->removeRequestParameter($preload);
        foreach ($service->getBaseResult($search_response) as $key => $value){
            $this->view_component->$key = $value;
        }
        //pager
        $options['path'] = '';
        foreach (request()->except('page') as $key => $value){
            $options['query'][$key] = $value;
        }
        $this->view_component->pager = new LengthAwarePaginator($this->view_component->products, $this->view_component->count, request()->input('limit'), request()->input('page'), $options);

        $title = '';
        if(request()->input('ca')){
            $category = CategoryRepository::find(request()->input('ca'), 'url_path');
            if($category){
                $this->view_component->current_category = $category;
                $title .= $category->name . ($category->synonym ? '(' . $category->synonym . ')' : '');
                $this->view_component->chief = 'current_category';
            }
        }
        if(request()->input('br')){
            $manufacturer = ManufacturerRepository::find(request()->input('br'), 'url_rewrite');
            if($manufacturer){
                $this->view_component->current_manufacturer = $manufacturer;
                $title .= $manufacturer->name . ($manufacturer->synonym ? '(' . $manufacturer->synonym . ')' : '');
                $this->view_component->chief = 'current_manufacturer';
            }
        }
        if(request()->input('mt')){
            $motor = MotorRepository::find(request()->input('mt'), 'url_rewrite');
            if($motor){
                $this->view_component->current_motor = $motor;
                $title .= $motor->name . ($motor->synonym ? '(' . $motor->synonym . ')' : '');
                $this->view_component->chief = 'current_motor';
            }
        }

        //set interface block
        $this->view_component->interface = ['rules', 'motors', 'brands', 'sort', 'limit', 'products'];
        $this->view_component->setBreadcrumbs('Outlet', route('outlet'));
        $this->view_component->setBreadcrumbs($title . 'Outlet');
        $this->view_component->seo('title',$title . 'Outlet');
        $this->view_component->seo('description','全台最大摩托百貨，每周都會推出一連串會員限定優惠活動，不論是折扣特賣、現金點數回饋加倍、COUPON折價券、或是免費點數集點活動...每周最新訊息與優惠活動也請詳閱會員電子報!');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.benefit.outlet.index', (array)$this->view_component);
    }
}

