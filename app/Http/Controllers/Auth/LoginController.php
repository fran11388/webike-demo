<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Ecommerce\Core\Wmail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Components\View\SimpleComponent;
use Everglory\Models\Customer;
use Everglory\Constants\LoginImage;

use Ecommerce\Core\Auth\Hasher;
use Ecommerce\Service\AccountService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/shopping';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $this->middleware('guest', ['except' => 'logout']);

        $this->middleware(function ($request, $next) {
            request()->session()->keep(['pending_data','pending_route']);
            if(session()->has('previous_url')){
                $this->redirectTo = session('previous_url');
            }

            if(session()->has('pending_route')){
                $this->redirectTo = route(session('pending_route'));
            }
            return $next($request);
        });
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $customer = \Auth::user();
            $customer_email = $customer->email;
            //If email verify success.And created date < 2019-5-23.Then pass.
            if($customer->email_validate || activeEmailValidate($customer->created_at)){
                session()->forget('customer_email');
                session()->forget('need_verify');
                return $this->sendLoginResponse($request);
            }

            $wmail = new Wmail;
            $wmail->customerEmailVerify($customer);
            $this->logout();
            return redirect()->route('login')->with(['need_verify' => true,'customer_email' => $customer_email]);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    /**
     * Rewrite AuthenticatesUsers
     */
    public function showLoginForm()
    {
        $gallery = LoginImage::GALLERY;
        $this->view_component->background_image = $gallery[rand(0, 4)];
        $this->view_component->seo('title','會員登入');
        $this->view_component->seo('description','即時更新摩托百貨、摩托車市、摩托新聞最新情報~Webike滿足您的摩托人生!');
        if(\URL::previous()){
            $this->redirectTo = session()->put('previous_url', \URL::previous());
        }
        return view('response.pages.auth.login-v1', (array)$this->view_component );
    }

    public function getFacebook(AccountService $service)
    {
        session()->reflash();
        if( $this->view_component->current_customer ){
            return redirect()->route('customer');
        }
        $url = $service->getFaceBookAuthUrl();
        return redirect()->to($url);
    }

    public function getFacebookValidate(AccountService $service)
    {

        session()->reflash();
        if( $this->view_component->current_customer ){
            return redirect()->route('customer');
        }

        $result = $service->validateFacebook(request());
        if($result->redirect){
            return redirect()->to($result->redirect_url);
        }else{
            $gallery = LoginImage::GALLERY;
            $this->view_component->background_image = $gallery[rand(0, 4)];
            return view($result->blade, (array)$this->view_component );
        }
    }

    /**
     * Rewrite AuthenticatesUsers
     */
    public function logout()
    {

        if(\URL::previous()){
            $this->redirectTo = \URL::previous();
        }

        $this->guard()->logout();

        request()->session()->flush();

        request()->session()->regenerate();

        return redirect($this->redirectTo);
    }

    public function loginApi(Request $request)
    {
        $result = getBasicAjaxObject();
        if ($this->attemptLogin($request)) {
            $customer = \Auth::user();
            $result->success = true;
            $result->redirect_url = "";
            $result->redirect_check = false;
            if(!$customer->email_validate && !activeEmailValidate($customer->created_at)){
                $result->success = false;
                $result->redirect_check = true;
                $result->redirect_url = \URL::route('login');
            }
            $this->login($request);
            if(\Auth::check()){
                $customer = \Auth::user();
                $customer->last_login = date('Y-m-d');
                $customer->save();
            }
        }else{
            $validate = $this->sendFailedLoginResponse($request);
            $errors = $validate->getSession()->get('errors')->getBag('default')->all();
            $result->messages = $errors;
            $result->errors++;
        }

        return json_encode($result);
    }
}
