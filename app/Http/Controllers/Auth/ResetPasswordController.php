<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Ecommerce\Core\Validate\Email\Flow\Customer;
use Ecommerce\Core\Validate\ValidateEmail;
use Ecommerce\Core\Wmail;
use Ecommerce\Service\AccountService;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Components\View\SimpleComponent;
use Illuminate\Http\Request;
use Ecommerce\Core\Auth\Hasher;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        $this->view_component->render();

        return view('auth.passwords.reset',(array)$this->view_component)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $hasher = new Hasher;

        $user->forceFill([
            'password' => $hasher->make($password),
            'remember_token' => Str::random(60),
        ])->save();

        AccountService::syncMotorMarketPassword($user->id, $user->password);


        if(!activeEmailValidate($user->created_at)){
            $email_verify_customer = new Customer($user);
            ValidateEmail::success($email_verify_customer);
        }

//        if(!$user->email_validate && !activeEmailValidate($user->created_at)){
//
//            $wmail = new Wmail;
//            $wmail->customerEmailVerify($user);
//            return redirect()->route('login')->with(['need_verify' => true,'customer_email' => $user->email]);
//        }
        $this->guard()->login($user);
    }
}
