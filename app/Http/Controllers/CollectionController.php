<?php
namespace App\Http\Controllers;

use App\Components\View\SimplePagerComponent;
use Ecommerce\Repository\CollectionRepository;
use Ecommerce\Service\AssortmentService;
use Ecommerce\Service\BikeNewsService;
use Illuminate\Http\Request;
use URL;
use Log;
use StdClass;

class CollectionController extends Controller
{
    protected $assortmentService;

    public function __construct(SimplePagerComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('collection');
        $component->setBreadcrumbs('流行特輯', $component->base_url);
        $this->assortmentService = new AssortmentService;
    }

    public function index()
    {
        $this->view_component->collection = $this->assortmentService->getAssortmentByPublishAt('all',[],'DESC','',40);
        $this->view_component->types = $this->assortmentService->getCollectionTypesByUrlRewrite();
        $this->view_component->types = $this->assortmentService->filterActiveTypes($this->view_component->types);
//        $this->view_component->assortments = $this->assortmentService->getTypeAssortments($this->view_component->types);
//        $this->view_component->collection = $this->assortmentService->filterActive($this->view_component->assortments);

        $this->view_component->seo('title', '【Webike流行特輯】，全球知名品牌流行同步');
        $this->view_component->seo('description', '進口重機、機車改裝零件、騎士用品流行特輯，為您囊括2015最新商品資訊，請您千萬不要錯過-「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,騎士用品,流行企劃,車輛特輯,全球知名品牌');
        $this->view_component->render();
        return view('response.pages.collection.index', (array)$this->view_component);
    }

    public function type($url_rewrite)
    {
        $current_type = $this->assortmentService->getCollectionTypesByUrlRewrite($url_rewrite, [])->first();
        if(!$current_type){
            app()->abort(404);
        }
        $seovalue =  $this->assortmentService->getseovalue($current_type->id);

        $this->view_component->current_type= $current_type;
//        $this->view_component->assortments = $this->assortmentService->filterActive($current_type->assortments);
        $this->view_component->assortments = $this->assortmentService->getAssortmentByPublishAt('all',[],'DESC',$current_type->id,40);

        $types = $this->assortmentService->getCollectionTypesByUrlRewrite();
        $types = $this->assortmentService->filterActiveTypes($types);
        $this->view_component->types = $types;

        $this->view_component->setBreadcrumbs($current_type->name);
        $this->view_component->seo('title', '【Webike'.$current_type->name.'】，全球知名品牌流行同步');
        $this->view_component->seo('description', $seovalue['description']);
        $this->view_component->seo('keywords', $seovalue['keywords']);

        $this->view_component->render();

        return view('response.pages.collection.type', (array)$this->view_component);
    }

    public function getDetailData()
    {

        $data = [];
        $data = new \stdClass();
//        $data->single_html = "<div class=\"width-full box\"><img src=\"https://img.webike.tw/assets/images/user_upload/collection/motobatt/banner-motobatt.png\" alt='【MOTOBATT 電池規格】GEL 膠體電池'></div>";
        $data->single_html = "<div class=\"row link-container\"><div class=\"col-md-4 col-sm-4 col-xs-12 box\"><a class='link1 active' href='javascript:void(0)'><img class=\"width-full\" src=\"https://img.webike.tw/assets/images/user_upload/collection/motobatt/agm.png\" alt='【MOTOBATT 電池規格】AGM 閥控式強效電池 - 「Webike-摩托百貨」 '></a></div><div class=\"col-md-4 col-sm-4 col-xs-12 box\"><a class='link2 link-opacity' href='javascript:void(0)'><img class=\"width-full\" src=\"https://img.webike.tw/assets/images/user_upload/collection/motobatt/gel.png\" alt='【MOTOBATT 電池規格】GEL 膠體電池 - 「Webike-摩托百貨」 '></a></div><div class=\"col-md-4 col-sm-4 col-xs-12 box\"><a class='link3 link-opacity' href='javascript:void(0)'><img class=\"width-full\" src=\"https://img.webike.tw/assets/images/user_upload/collection/motobatt/pro.png\" alt='【MOTOBATT 電池規格】PRO Lithium 賽事級鋰鐵電池  - 「Webike-摩托百貨」 '></a></div></div>";
//        $data = [
//            'data'=>[
//            'AGM' =>
//            [
//                't00065212' => [
//                    'model' => 'MB12U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065212.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：160\n\n尺寸\n長 (L)：134 mm\n寬 (W)：80 mm\n高 (H)：161 mm\n',
//                    'batteries' =>	['YB12AA','YB12AAS','YB12AAWS','YB12ALA','YB12ALA2','YB12AB','YB12CA','12N124A','12N12A4A1']
//                ],
//                't00065214' => [
//                    'model' => 'MB16AU',
//                    'image' => 'https://img.webike.tw/product/1358/t00065214.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：230\n\n尺寸：\n長 (L)：207 mm\n寬 (W)：72 mm\n高 (H)：164 mm\n',
//                    'batteries' =>	['YB16ALA2']
//                ],
//                't00065216' => [
//                    'model' => 'MB51814',
//                    'image' => 'https://img.webike.tw/product/1358/t00065216.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：220\n\n尺寸\n長 (L)：183 mm\n寬 (W)：80 mm\n高 (H)：170 mm\n',
//                    'batteries' =>	['51814','51913']
//                ],
//                't00065218' => [
//                    'model' => 'MBT12B4',
//                    'image' => 'https://img.webike.tw/product/1358/t00065218.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：150\n\n尺寸\n長 (L)：15​​0 mm\n寬 (W)：70 mm\n高 (H)：130 mm\n',
//                    'batteries' =>	['YT12BBS','YT12B4']
//                ],
//                't00065220' => [
//                    'model' => 'MBTX12U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065220.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：200\n\n尺寸\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：130 mm\n',
//                    'batteries' =>	['YB12BB2','YTX12BS','YTX14BS','YTX14HBS','YTX14LBS','KMX14BS','YTX15L-BS']
//                ],
//                't00065222' => [
//                    'model' => 'MBTX16U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065222.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：250\n\n尺寸\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：161 mm\n',
//                    'batteries' =>	['YTX16BS','YTX16BS1','YTX20A-BS','YTX20CHBS']
//                ],
//                't00065224' => [
//                    'model' => 'MBTX20U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065224.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：310\n\n尺寸\n長 (L)：175 mm\n寬 (W)：87 mm\n高 (H)：155 mm\n',
//                    'batteries' =>	['12N163A','12N163B','12N164A','12N164B','YTX20BS','YTX20HBS','YTX20LBS','YTX20HLBS','YTX20HLBSPW','YB16A','YB16A2','YB16B','YB16B2','YB16HLACX','SYB16LB','YB16LA','YB16LA2','YB16LB','YB16LB2','YB16CB','YB16CLB','YB16BCX','GT16L-BS','GT16-BS','CTX19L-BS','CTX19-BS','GYZ20L','GYZ20HL']
//                ],
//                't00065226' => [
//                    'model' => 'MBTX20UHD',
//                    'image' => 'https://img.webike.tw/product/1358/t00065226.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：310\n\n尺寸\n長 (L)：175 mm\n寬 (W)：87 mm\n高 (H)：155 mm\n',
//                    'batteries' =>	['12N163A','12N163B','12N164A','12N164B','YTX20BS','YTX20HBS','YTX20LBS','YTX20HLBS','YTX20HLBSPW','YB16A','YB16A2','YB16B','YB16B2','YB16HLACX','SYB16LB','YB16LA','YB16LA2','YB16LB','YB16LB2','YB16CB','YB16CLB','YB16BCX','GT16L-BS','GT16-BS','CTX19L-BS','CTX19-BS','GYZ20L','GYZ20HL']
//                ],
//                't00065228' => [
//                    'model' => 'MBTX24U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065228.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：300\n\n尺寸\n長 (L)：205 mm\n寬 (W)：87 mm\n高 (H)：162 mm\n',
//                    'batteries' =>	['12N183','12N183A','Y50N18AA','Y50N18LA','Y50N18LA2','Y50N18LA3','Y50N18LACX','SY50N18LA','SY50N18LAT','CTX18-BS','CTX18L-BS','GTX18L-BS','YTX24HLBS','YTX24HL']
//                ],
//                't00065230' => [
//                    'model' => 'MBTX30U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065230.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：385\n\n尺寸\n長 (L)：166 mm\n寬 (W)：126 mm\n高 (H)：175 mm\n',
//                    'batteries' =>	['Y60N24A','Y60N24ALB','Y60N24LA','Y60N24LA2','12N243','12N243A','12N244','12N244A','YIX30L','YB30LB','Y30CLB','Y60N30LA','Y60N30LB','53030']
//                ],
//                't00065232' => [
//                    'model' => 'MBTX30UHD',
//                    'image' => 'https://img.webike.tw/product/1358/t00065232.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：385\n\n尺寸\n長 (L)：166 mm\n寬 (W)：126 mm\n高 (H)：175 mm\n',
//                    'batteries' =>	['Y60N24A','Y60N24ALB','Y60N24LA','Y60N24LA2','12N243','12N243A','12N244','12N244A','YIX30L','YB30LB','Y30CLB','Y60N30LA','Y60N30LB','53030']
//                ],
//                't00065234' => [
//                    'model' => 'MBTX7U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065234.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：115\n\n尺寸\n長 (L)：114 mm\n寬 (W)：70  mm\n高 (H)：128 mm\n',
//                    'batteries' =>	['YTX7LBSSRP']
//                ],
//                't00065236' => [
//                    'model' => 'MBTX9U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065236.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：160\n\n尺寸\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：110 mm\n',
//                    'batteries' =>	['YTX9BS','YT12ABS','YTZ12S','YTZ14S']
//                ],
//                't00065238' => [
//                    'model' => 'MBTZ10S',
//                    'image' => 'https://img.webike.tw/product/1358/t00065238.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：140\n\n尺寸\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：95 mm\n',
//                    'batteries' =>	['YTX7ABS','YTZ10S']
//                ],
//                't00065240' => [
//                    'model' => 'MBTZ7S',
//                    'image' => 'https://img.webike.tw/product/1358/t00065240.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：100\n\n尺寸\n長 (L)：114  mm\n寬 (W)：70 mm\n高 (H)：107 mm\n',
//                    'batteries' =>	['YTX5L-BS','YTZ6S','YTZ7SSRP']
//                ],
//                't00065242' => [
//                    'model' => 'MBYZ16H',
//                    'image' => 'https://img.webike.tw/product/1358/t00065242.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：240\n\n尺寸\n長 (L)：151 mm\n寬 (W)：87  mm\n高 (H)：145 mm\n',
//                    'batteries' =>	['GYZ16H','YTX14BS','YTX14HBS','YTX14LBS','KMX14BS']
//                ],
//                't00065244' => [
//                    'model' => 'MT4R',
//                    'image' => 'https://img.webike.tw/product/1358/t00065244.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n啟動電流 (CCA)：45\n\n尺寸\n長 (L)：114 mm\n寬 (W)：49 mm\n高 (H)：86 mm\n',
//                    'batteries' =>	['YTR4ABS']
//                ],
//            ],
//            'GEL' =>
//            [    't00065246' => [
//                    'model' => 'MTX7A',
//                    'image' => 'https://img.webike.tw/product/1358/t00065246.jpg',
//                    'regular' => '規格\n\n安培數 (Ah)：7 \n啟動電流 (CCA)110\n\n長 (L)：151 mm\n寬 (W)：88 mm\n高 (H)：95 mm\n',
//                    'batteries' =>	[]
//                ],
//                't00065248' => [
//                    'model' => 'MTX7B',
//                    'image' => 'https://img.webike.tw/product/1358/t00065248.jpg',
//                    'regular' => '規格\n\n安培數 (Ah)：7 \n啟動電流 (CCA)110\n\n長 (L)：151 mm\n寬 (W)：65 mm\n高 (H)：95 mm\n',
//                    'batteries' =>	[]
//                ],
//                't00065250' => [
//                    'model' => 'MTX9',
//                    'image' => 'https://img.webike.tw/product/1358/t00065250.jpg',
//                    'regular' => '規格\n\n安培數 (Ah)：9 \n啟動電流 (CCA)140\n\n長 (L)：151 mm\n寬 (W)：88 mm\n高 (H)：107 mm\n',
//                    'batteries' =>	[]
//                ],
//                't00065252' => [
//                    'model' => 'MTZ6S',
//                    'image' => 'https://img.webike.tw/product/1358/t00065252.jpg',
//                    'regular' => '規格\n\n安培數 (Ah)：6 \n啟動電流 (CCA)：80\n\n長 (L)：113 mm\n寬 (W)：70 mm\n高 (H)：109 mm\n',
//                    'batteries' =>	[]
//                ]
//            ],
//            'PRO' =>
//            [    't00065254' => [
//                    'model' => 'MPLTZ7S',
//                    'image' => 'https://img.webike.tw/product/1358/t00065254.jpg',
//                    'regular' => '規格\n\n啟動電流 (CCA)：165\n\n 長 (L)：114 mm \n寬 (W)：70 mm \n高 (H)：107 mm\n',
//                    'batteries' =>	['YTX5L-BS','YTZ6S','YTZ7S']
//                ],
//                't00065262' => [
//                    'model' => 'MPLX4U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065262.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特\n安培數 (Ah)：2.2 \n啟動電流 (CCA)：140\n\n長 (L)：114 mm\n寬 (W)：70 mm\n高 (H)：87 mm\n',
//                    'batteries' =>	['YTX4LBS','YT4LBS','YB4LA','YB4LB']
//                ],
//                't00065256' => [
//                    'model' => 'MPLX9U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065256.jpg',
//                    'regular' => '規格\n\n安培數 (Ah)：3.0\n  啟動電流 (CCA)：230 \n\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：105 mm\n',
//                    'batteries' =>	[]
//                ],
//                't00065258' => [
//                    'model' => 'MPLZ10S',
//                    'image' => 'https://img.webike.tw/product/1358/t00065258.jpg',
//                    'regular' => '規格\n\n安培數 (Ah)：4.0 \n啟動電流 (CCA)：280 \n\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：95 mm\n',
//                    'batteries' =>	[]
//                ],
//                't00065260' => [
//                    'model' => 'MPLZ12U',
//                    'image' => 'https://img.webike.tw/product/1358/t00065260.jpg',
//                    'regular' => '規格\n\n電壓：12 伏特  \n  安培數 (Ah)：14 \n啟動電流 (CCA)：200 \n\n長 (L)：151 mm\n寬 (W)：87 mm\n高 (H)：130 mm\n',
//                    'batteries' =>	[]
//                ]
//            ]
//                ]
//        ];
        dd(json_encode($data));

//        $data['interface'] = ['brands', 'filters', 'sort', 'limit', 'products'];
//        $data['segments'] = ['motor' => '13468'];
//        $data['chief'] = 'current_motor';
//        $data['hide'] = 'current_motor';
       
        // $data['description'] = '體積小、攜帶方便，可以使用附設轉接頭，直接連接電瓶供電使用。充飽12吋輪胎所需時間約為8分鐘內，最高輸出胎壓可達50PSI，是外出旅遊時不可多得的好幫手。隨機都有附贈收納袋，方便收納、整理。打氣機也附有LED背光壓力表，胎壓判讀簡單方便！';
        // $data['manufacturer_url_rewrites'] = ['666', '49', 't1961', 't2011'];
        // {"title":"\u9019\u662f\u4e00\u5806\u706b\u661f\u585e","description":"\u53ef\u662f\u88e1\u9762\u6709\u4e00\u500b\u84cb\u5b50","product_url_rewrites":["1953706","1955971","1508539","t00013688"],"product_cautions":{"t00013688":"\u53ef\u662f\u88e1\u9762\u6709\u4e00\u500b\u84cb\u5b50"},"product_summaries":{"t00013688":"\u6b38\uff0c\u9019\u662f\u84cb\u5b50"}}
          // $data['title'] = '其他連結';
          // $data['link'] = ['風勁霸官方網址'=>'http://www.allfirst.com.tw/index.asp?lang=1','風勁霸官方粉絲團'=>'https://www.facebook.com/Volcano.Air.Compressor.Tw/?fref=ts','風勁霸品牌頁面'=>'http://www.webike.tw/br/t2158'];
        //   $data['image'] = ['http://img.webike.tw/assets/images/user_upload/collection/phoneginba1.png','http://img.webike.tw/assets/images/user_upload/collection/phoneginba2.png','http://img.webike.tw/assets/images/user_upload/collection/phoneginba3.png'];
        //   $data['content'] = '<tr>
          // $data['image'] = 'http://img.webike.tw/assets/images/user_upload/collection/brand-volcano.png';
          // $data['url_rewrite'] = ['21443046','21443046'];
//        $data['product_url_rewrites'] = ['t00045961'];
// //        $data['product_descriptions'] = ['t00013688' => '可是裡面有一個蓋子'];
//         $data['product_cautions'] = ['t00045961' => '此組商品多加了補胎套件。當處在郊區，或是前不著村後不著店的尷尬位置時，縱使有打氣機也是於事無補。所以此組商品包含補胎條、補胎槍及萬用折疊鉗，讓意外發生時也能從容解決窘境。'];
//        $data['product_summaries'] = ['t00045961' => '※本商品補胎套件內含補胎條、補胎槍及萬用折疊鉗。'];
       // $data['title'] = '其他連結';
       // $data['rows'] = [];
          // $data['title'] = '問卷調查-Webike問卷調查結果';
        // $data['title'] = '購買此次評比商品';
        // $data['title'] = '人氣商品-本次票選結果最具人氣商品';
          // $data['title'] = '商品實測-人氣商品的實力展現!';
        // $data['title'] = '遺珠之憾-叫好不叫座的商品';
        // $data['title'] = '購買此次評比商品';
        // $data['title'] = '挑選標準-哪些是最適合自己的款式?';
        // $data['title'] = '規格介紹-各安全帽規格及檢驗介紹';
        // $data['title'] = '堅固-絕佳的安心感';
        // $data['title'] = '商品分類';
        $data['title'] = '<p><span class="size-10rem>各位大家好，我是浪速的輪胎人。</span></p><p><span class="size-10rem>這次要為大家介紹的是長途旅行騎乘的可靠夥伴「馬鞍箱」！</span></p><p><span class="size-10rem>到了旅行季節時想要狠下心來，來趟大約一個星期長途旅行的騎士！抱著這樣想法的人沒想到其實還不少。</span></p><p><span class="size-10rem>正在規劃長途旅行騎乘的您一定要試試這款『旅行半硬式馬鞍箱』。</span></p><p><span class="size-10rem>「不只替換衣物，也想帶各地特產回家……，想要更大的容量！」針對這樣需求的您，選用馬鞍箱擴充愛車的裝載容量如何呢？</span></p>
{"images":"http://img.webike.net/catalogue/images/7837/mfk-195_02_s.jpg","title":"TANAX motofizz: 旅行半硬式馬鞍箱 MFK-196 40L（單側20L）&其他尺寸的相關商品","html":"<p><span class="size-10rem>各位大家好，我是浪速的輪胎人。</span></p><p><span class="size-10rem>這次要為大家介紹的是長途旅行騎乘的可靠夥伴「馬鞍箱」！</span></p><p><span class="size-10rem>到了旅行季節時想要狠下心來，來趟大約一個星期長途旅行的騎士！抱著這樣想法的人沒想到其實還不少。</span></p><p><span class="size-10rem>正在規劃長途旅行騎乘的您一定要試試這款『旅行半硬式馬鞍箱』。</span></p><p><span class="size-10rem>「不只替換衣物，也想帶各地特產回家……，想要更大的容量！」針對這樣需求的您，選用馬鞍箱擴充愛車的裝載容量如何呢？</span></p>';
          // $data['title'] = '調查5-目前使用的煞車來令片的品牌是什麼？';
          $cols = [
              
//               [
//               'scale' => '5',
//               'content' => '<div style="background: url(http://www.webike.tw/assets/images/collection/protection/ymc_common_title_bg.png) no-repeat;width: 80%;padding-bottom: 16%;">
//       <img src="http://img.webike.tw/assets/images/user_upload/collection/polls/protection/ymc_common_title1.png" class="block_one_url" title="運動企劃 - 「Webike-摩托百貨」" alt="運動企劃 - 「Webike-摩托百貨」" >
//       <p style="margin-left: 11%;">
//         從問卷調查結果發掘出「理想零件」研究室。
// 這次的研究主題是「剎車來令片」
// 尋找出滿足騎士們的剎車來令片，
// 就開始以下的調查吧!
//       </p>
//       </div>
//     '
//            ],
//            [
//                 'scale' => '7',
//                 'content' => '<img src="http://www.webike.tw/assets/images/user_upload/collection/160317_headlamp_main_image.png" class="block_one_url2" title="剎車來令片 - 「Webike-摩托百貨」" alt="剎車來令片 - 「Webike-摩托百貨」">'
//            ],
      /**************************************************************************************************************************************/    
      //      [
      //           'scale' => '4',
      //           'content' => '<div class="article">
      //   <img src="http://www.webike.tw/assets/images/user_upload/collection/160317_ymc_graph1.png" class="statistics_one" title="調查統計圖表 - 「Webike-摩托百貨」" alt="調查統計圖表 - 「Webike-摩托百貨」">
      // </div>'
      //      ],
      //      [
      //           'scale' => '8',
      //           'content' => '<div class="article">
      //   <p>
      // 從以上的結果可以看出，騎士把煞車來令片更換成非正廠的意識並不太高，更換過的人和沒更換過的人，大約占一半一半左右，不過似乎也有不少騎士，是因為正廠煞車來令片磨損到無法使用後，才更換成正廠煞車來令片。
      //   </p>
      // </div>'
      //      ],
    /**************************************************************************************************************************************/
//       [
//                 'scale' => '8',
//                 'content' => '<div class="article">
//         <p>
//       煞車來令片的材質種類大致可以分為4 種。
// 從調查結果可以知道，騎士們對於煞車來令片材質的知識並不太清楚，這可能是因為，嘗試使用非正廠煞車來令片的機會不多，因此也就造成了騎士對煞車來令片的材質不太感興趣的情況。
//         </p>
//       </div>'
//            ],
//       [
//                 'scale' => '4',
//                 'content' => '<div class="article">
//         <img src="http://www.webike.tw/assets/images/user_upload/collection/160317_ymc_graph2.png" class="statistics_two" title="調查統計圖表 - 「Webike-摩托百貨」" alt="調查統計圖表 - 「Webike-摩托百貨」">
//       </div>'
//            ],
    /**************************************************************************************************************************************/
      // [
      //           'scale' => '8',
      //           'content' => '<div class="article">
      //   <p>
      // 回答預算在「3萬台幣內」佔總調查人數的4分之3。<br>
      // 由此可見，對於初次改裝排氣管的騎士們來說，即便改裝的慾望很高，但是礙於預算的考量，對於價格高的全段排氣管改裝很難能一次到位。
      //   </p>
      // </div>'
      //      ],
      // [
      //           'scale' => '4',
      //           'content' => '<div class="article">
      //   <img src="http://www.webike.tw/assets/images/collection/exhaust/ymc_vol3_graph3.png" class="statistics_two" alt="改裝預算調查 - 「Webike-摩托百貨」">
      // </div>'
      //      ],     

      /**************************************************************************************************************************************/
    //         [
    //           'scale' => '4',
    //            'content' => '<div class="left">
    //   <img src="http://www.webike.tw/assets/images/collection/exhaust/ymc_vol3_between_image.png" class="popular" alt="消音器 - 「Webike-摩托百貨」">
    // </div>',
    //         ],
    //         [ 'scale' => '8',
    //           'content' => '<div class="right"><h2>一想到改裝的話，首先就是排氣管！</h2><br>
    //    <span>騎士們考慮改裝時最先想到的改裝候選零件就是排氣管了吧，尤其一旦換了排氣管，車子的外觀也會跟著改變，聲音和引擎輸出特性也會出現確實的變化，和原廠排氣管相比，不少改裝品牌的排氣管比較輕等種種原因，讓騎士想要進行改裝，不過就像在文章一開頭提到的，排氣管可以說是屬於花費效益高的改裝零件。</span>
    //   </div>'
    //         ]
       /**************************************************************************************************************************************/
      //     [
      //          'scale' => '6',
      //          'content' => '<div><table class="table table-bordered">
      // <tbody><tr><td>1</td><td><a href="http://www.webike.tw/genuineparts" title="煞車皮(來令片) 正廠品 - 「Webike-摩托百貨」" target="_blank">正廠品</a></td><td>191人</td></tr><tr><td>2</td><td><a href="http://www.webike.tw/parts/ca/1000-1010-1011/br/177" title="煞車皮(來令片) DAYTONA - 「Webike-摩托百貨」" target="_blank">DAYTONA</a></td><td>143人</td></tr><tr><td>3</td><td><a href="http://www.webike.tw/parts/ca/1000-1010-1011/br/808" title="煞車皮(來令片) Vesrah - 「Webike-摩托百貨」" target="_blank">Vesrah</a></td><td>43人</td></tr><tr><td>4</td><td><a href="http://www.webike.tw/parts/ca/1000-1010-1011/br/625" title="煞車皮(來令片) RK - 「Webike-摩托百貨」" target="_blank">RK</a></td><td>40人</td></tr><tr><td>5</td><td><a href="http://www.webike.tw/parts/ca/1000-1010-1011/br/117" title="煞車皮(來令片) Brembo - 「Webike-摩托百貨」" target="_blank">Brembo</a></td><td>28人</td></tr><tr><td>6</td><td><a href="http://www.webike.tw/parts/ca/1000-1010-1011/br/861" title="煞車皮(來令片) ZCOO - 「Webike-摩托百貨」" target="_blank">ZCOO</a></td><td>25人</td></tr>   </tbody></table></div>',
      //      ],
      //      [
      //         'scale' => '6',
      //         'content' => '<p>因為有不少騎士總是使用正廠煞車來令片，不考慮非正廠的來令片，所以正廠的煞車來令片佔最多數，不過，從結果來看DAYTONA的煞車來令片有不小的人氣，其中Golden pad等低價位，但卻擁有高性能的來令片似乎受到不少關注。</p>'
      //      ],
      //     [
      //          'scale' => '12',
      //          'content' => '<span style="color:red;float:left"><h2>研究結果</h2></span><h2>-最「理想」的排氣管/消音器零件!!</h2><br>',
      //      ],
      //      [
      //          'scale' => '12',
      //          'content' => '<img src="http://www.webike.tw/assets/images/collection/exhaust/ymc_vol3_conclusion_image.png" class="biggest" alt=" - 「Webike-摩托百貨」" title=" - 「Webike-摩托百貨」">',
      //      ],
      //      [
      //          'scale' => '6',
      //          'content' => '<div><p><h2>●排氣管</h2></p><br><table class="table table-bordered">
      // <tbody><tr><td>1</td><td>鈦金屬</td><td>349人</td></tr><tr><td>2</td><td>鈦金屬</td><td>114人</td></tr><tr><td>3</td><td>鋼鐵</td><td>21人</td></tr>         </tbody></table></div>',
      //      ],
      //      [
      //          'scale' => '6',
      //          'content' => '<div><p><h2>●排氣管尾段</h2></p><br><table class="table table-bordered">
      // <tbody><tr><td>1</td><td>鈦金屬</td><td>171人</td></tr><tr><td>2</td><td>碳纖維</td><td>153人</td></tr><tr><td>3</td><td>具有鈦金屬色</td><td>75人</td></tr><tr><td>4</td><td>不鏽鋼</td><td>47人</td></tr><tr><td>5</td><td>鋁合金</td><td>25人</td></tr><tr><td>6</td><td>鋼鐵</td><td>16人</td></tr>         </tbody></table></div>',
      //      ],
      //      [
      //          'scale' => '12',
      //          'content' => '<span>從以上的回答可以看到，在材質方面排氣管／排氣管尾段同樣都是鈦金屬擁有高支持度，這是因為騎士們大多想要追求“輕量”吧，另外，從Yoshimura、AKRAPOVIC、Moriwaki等擁有紮實技術的品牌佔有前幾名也顯示騎士們追求排氣管／排氣管尾段性能的指標，不過從以上的各項調查來看，想要花3萬台幣以下買到想要的全段排氣管可以說相當困難，所以入門的騎士們改裝首選應該還是以尾段會是優先考量吧。</span>',
      //      ],

      /**************************************************************************************************************************************/
      //      [
      //           'scale' => '4',
      //           'content' => '<div class="article">
      //   <img src="http://www.webike.tw/assets/images/collection/protection/ymc_vol3_between_image.png" class="popular" alt=" - 「Webike-摩托百貨」" title=" - 「Webike-摩托百貨」">
      // </div>'
      //      ],
      //      [
      //           'scale' => '8',
      //           'content' => '<h2>考量價格和性能，種類豐富，增加改裝樂趣的消耗品</h2><br><span>
      //     近年隨著陽極處理加工、鋁合金等技術的普遍，在這次的問卷調查中約60%的騎士也裝備了這類人氣零件。
      //     而駕訓班採用的鋼管式保險桿，主張防護性強、賽車常用的精巧型防護零件由於具備多樣性，也是吸引騎士配備防護零件榜上名單之一。
      //     在價格方面，依品牌、形狀以及材質等各有不同，根據調查結果，花費3000元以上購買防護零件類的騎士也不少，這應該主要是為了避免車子在突然摔倒時可能產生的損害，
      //     尤其引擎附近的損傷修復、更換零件價格都來的高且費時，所以配備引擎防護零件的比例也相對地來得高。
      // </span>'
      //      ],

      //       [
      //          'scale' => '6',
      //          'content' => '<div class="container"><table class="table table-bordered">
      // <tbody><tr><td>1</td><td>智慧型手機</td><td>118人</td></tr><tr><td>2</td><td>衛星導航(不包括手機)</td><td>86人</td></tr><tr><td>3</td><td>行車紀錄器</td><td>51人</td></tr><tr><td>4</td><td>測速器</td><td>38人</td></tr><tr><td>5</td><td>無線電</td><td>8人</td></tr><tr><td>6</td><td>平板電腦</td><td>7人</td></tr>   </tbody></table></div>',
      //      ],
      //      [
      //          'scale' => '6',
      //          'content' => '<span>
      //   第一名當然是智慧型手機，放置衛星導航雖然也很普遍，但是相關產品的支架大多是專屬品，所以為了避免失手買到不適合的產品還是建議購買專屬的支架，比如相機也建議使用摩托車專用的固定支架。
      // </span>',
      //      ],
//            [
//                'scale' => '12',
//                'content' => '<div style="border: #ccc 5px solid;padding: 10px;border-radius: 20px;margin-bottom: 20px;background: #fff;"><p><h2>你的煞車有沒有問題呢？煞車來令片失敗經驗談
// </h2></p><br><p>● 發生過活塞凸到工具幾乎無法插入，為了推回活塞只好取出煞車油的窘境。</p><p>● 接著換了來令片後，竟然還發出怪聲音。</p><p>● 曾經從正廠來令片換成金屬類的來令片，沒想到使用壽命竟然比想像中來得短，價格和使用壽命不成正比。</p><p>● 因為是配備ABS的車款，更換來令片時只能到車廠把ABS的空氣放掉，實在很麻煩。</p></div>',
//            ],
//            [
//               'scale' => '12',
//               'content' => '<span style="color:red;float:left"><h2>研究結果</h2></span><h2>-低價位的來令片最受歡迎</h2><br><p>
//       煞車來令片的摩擦材質不同，特色也各有不相同，基本上煞車來令片的主要性能在於制動性，不過也有來令片煞車手感的密合度、磨損壽命、因為溫度影響效能變化等各種不同的性能。<br>
//       <br>
//       <br>
//       從這次的調査結果中，意外地發現從正廠更換成非正廠來令片的騎士竟然比想像中來得少，而且大部分的騎士也都不太知道來令片材質的種類，也因此相較於性能，CP值佳的來令片最受歡迎。<br>
//       <br>
//       <br>
//       騎士們購買來令片的預算一般多選擇600元～1000元前後，低價位的來令片。<br>
//       高價位的來令片，比如有可以在賽道上發揮超高性能，價格接近3000元的高價位來令片等。不過，如果對主要以享受旅途騎成為主的騎士而言，3000元是很難出手的高價。<br>
//       <br>
//       <br>
//       另外，感覺起來雖然騎士們也會自己進行煞車相關的維修保養工作，但是對於這方面的維修保養沒有自信的騎士好像還蠻多的，比如就有不少是因為空氣沒有確實地抽出等失敗的例子，由於煞車來令片是與生命安全密切相關的零件，因此建議您到維修保養場請專業的人員確實地整備維修。
//     </p>'
//            ],





// [
//               'scale' => '6',
//               'content' => '<p>
//         Yoshimura獲得技壓其他品牌的第１名，第2名則是AKRAPOVIC，不過值得注意的是，第3名的原廠品牌，這幾年原廠改裝的排氣管確實擁有很高的完成度呢！
//       </p><br>
//       <div style="border: #ccc 3px solid;border-radius: 10px;padding: 10px;overflow: auto;">
//         <div style="float:left;width: 58%;">
//         <p>
//           保護引擎的樹脂製保護滑塊雖然是防護零件的主流，不過最近鋁合金製設計性高的產品也頗受歡迎，陽極處理過後增加了防護零件豐富多樣的顏色，選擇性也跟著增加。</p>
//         </div>
//         <div style="padding: 10px;background-color: #fff;border: #ccc 1px solid;float:left;width: 39%;text-align: center;">
//           <a href="http://www.webike.tw/ca/1000-1059-1117" target="_blank" title="引擎防護滑塊 - 「Webike-摩托百貨」">
//             <img src="http://img.webike.net/catalogue/11550/006-sbm04s.jpg" title="引擎防護滑塊 - 「Webike-摩托百貨」" alt="引擎防護滑塊 - 「Webike-摩托百貨」">引擎防護滑塊
//           </a>
//         </div>
//       </div>'
//            ],








    //        [
    //           'scale' => '12',
    //           'content' => '<span>雖然買了便宜的防護零件但是沒想到作工、品質比想像的來得低，只要稍微碰撞一下就變形了，在防護功能上可以說是完全失敗， 因為設計或價格考量下購買的防護零件，結果因為倒車時卻完全沒有發揮防護的作用，結果造成了車子損傷。</span><br><br><span style="color:red;float:left"><h2>研究結果</h2></span><h2>-最「理想」的車體保護零件!!</h2>'
    //        ],
    //        [
    //           'scale' => '5',
    //           'content' => '<div><img src="http://www.webike.tw/assets/images/collection/protection/ymc_vol3_conclusion_image.png" class="knight" alt=" - 「Webike-摩托百貨」" title=" - 「Webike-摩托百貨」">
    //   </div>
    // '
    //        ],
    //        [
    //             'scale' => '7',
    //             'content' => '<span>從這次的問卷調查結果可以知道，招牌的樹脂製引擎防護零件最受騎士的歡迎，設計雖然不是全部，不過沒想到價格不低的防護零件竟然也頗受歡迎… 光是從各品牌推出類似款式的防護零件這點看來，就可知道防護零件也算是品牌間的必爭之地，但購買前也需要好好評比一番，從使用者的失敗經驗談中， 也可以看到不少實際上無法達到防護效果，或是強度不足的經驗，所以建議仔細考慮、評比加裝的結構和位置後再購買需要的防護零件。</span><br>'
    //        ],
    //        [
    //           'scale' => '12',
    //           'content' => '<span style="margin-left:20%">●絵=有野篤</span>'
    //        ],

       ];
       // $data['product_url_rewrites'] = ['21113575','21226477','21226482','21226487','21226492','21226497','21226502','21290885','21290889','21290893','21312191','21382233','21404118','21406087','21406091','21406095','21443075','21732869','21826050','9698532'];
       // $data['product_url_rewrites'] = ['21752330','21764084','21764090','21764096'];
       // $data['ca_url_path'] = ['1011','1013','1014','1015','1018','1019'];
       // $cols = [
       //     [
       //         'scale' => '8',
       //         'content' => '<img src="/image/productdetail/img-collection2.jpg" alt="images">',
       //     ],
       //     [
       //         'scale' => '4',
       //         'content' => '<strong>「了解摔車的類型」向1000人進行問卷調查</strong><br><br>

       // 在學習不摔車的知識之前，首先思考「究竟為什麼會摔車？」透過webike會員在網路上進行問卷調查，總共取得了1032名騎士的回覆，並且得到以下的結果，首先曾經摔過車的比例竟然高達97％！另外，在各種情況下，因為失去平衡與滑倒，而摔車的比例也佔了８成。<br>',
       //     ],
       // ];
       // $dataCols = [];
       // foreach ($cols as $col){
       //     $colData = new StdClass;
       //     foreach ($col as $key => $value){
       //         $colData->$key = $value;
       //     }
       //     $dataCols[] = $colData;
       // }
       // $data['rows'][] = collect($dataCols);
       // dd(json_decode('{"title":"\u4eba\u6c23\u5546\u54c1-\u4eba\u6c23\u5546\u54c1\u7684\u5be6\u529b\u5c55\u73fe!","rows":[[{"scale":"12","content":"<div class=\"webike2_box\">\u9019\u6b21\u6e2c\u8a66\u8a55\u6bd4\u7684\u662f\u64c1\u6709\u9ad8\u4eba\u6c23\u7684TANAX motofizz\u7684\u5ea7\u588a\u5305\u3002<\/div>"},{"scale":"4","content":"<a href=\"http:\/\/www.webike.tw\/parts\/br\/983?q=%E8%BF%B7%E4%BD%A0\" title=\"\u3010TANAX motofizz\u3011MINI FIELD\u5ea7\u588a\u5305 19-27L - \u300cWebike-\u6469\u6258\u767e\u8ca8\u300d\" target=\"_blank\">\r\n                <img src=\"\r\nhttp:\/\/img.webike.tw\/assets\/images\/user_upload\/collection\/20150720_item01.jpg\" alt=\"\u3010TANAX motofizz\u3011MINI FIELD\u5ea7\u588a\u5305 19-27L - \u300cWebike-\u6469\u6258\u767e\u8ca8\u300d\">\r\n              <\/a>"},{"scale":"8","content":"\r\n               <div class=\"content\">\r\n                <ul style=\"list-style-type:none\">\r\n                  <li class=\"brand\"><h2>TANAX motofizz<\/h2><\/li>\r\n                  <li class=\"name\">\r\n                    <a class=\"text-primary\" href=\"http:\/\/www.webike.tw\/parts\/br\/983?q=%E8%BF%B7%E4%BD%A0\" title=\"\u3010TANAX motofizz\u3011MINI FIELD\u5ea7\u588a\u5305 - \u300cWebike-\u6469\u6258\u767e\u8ca8\u300d\" target=\"_blank\"><h2>\r\n                    MINI FIELD\u5ea7\u588a\u5305 19-27L<\/h2>\r\n                    <\/a>\r\n                  <\/li>\r\n                  <li class=\"description\">\r\n                    \u5de6\u53f3\u5404\u7d047 \u339d\u5bec\uff0c\u662f\u5bb9\u91cf\u53ef\u4f38\u7e2e\u8b8a\u5316\u7684\u985e\u578b\uff0c\u6536\u7d0d\u5bb9\u91cf\u7d04\u300c\u7576\u5929\u4f86\u56de\uff5e\u4f4f\u5bbf1\u665a\u300d\u5de6\u53f3\uff0c\u662f\u53ef\u7528\u9014\u591a\u6a23\u6469\u6258\u8eca\u65c5\u884c\u7684\u5ea7\u588a\u5305\u3002<br>\r\n                    (\u9019\u6b3e\u5ea7\u588a\u5305\u4eca\u5e744\u670813\u65e5\u624d\u525b\u63a8\u51fa\u92b7\u552e\u3002)<br>\r\n                    \u4ee4\u4eba\u958b\u5fc3\u7684\u662f\uff0c\u6709\u65b9\u4fbf\u597d\u7528\u7684\u53e3\u888b\u548c\u9644\u6709\u675f\u53e3\u7e69\u7684\u9632\u6c34\u7f69\u3002\r\n                  <\/li>\r\n                <\/ul>\r\n              <\/div>"}]]}'));
        dd(json_encode($data));
    }

    public function detail($type_url_rewrite, $assortment_url_rewrite)
    {
        set_time_limit(60 * 5);
        $assortment = $this->assortmentService->getAssortmentByUrl([$type_url_rewrite, $assortment_url_rewrite]);
        if(!$assortment or !$assortment->active or !strtotime($assortment->publish_at) > strtotime('now')){
            return abort(404);
        }
        $blocks = $this->assortmentService->getAssortmentView($assortment);
        if(!count($blocks)){
            return abort(404);
        }

        $this->view_component->layout = $assortment->layout->path;
        $this->view_component->blocks = $blocks;
        $this->view_component->setBreadcrumbs($assortment->type->name, route('collection-type', [$type_url_rewrite]));
        $this->view_component->setBreadcrumbs($assortment->name);
        if($assortment_url_rewrite == 'motobattshowwall'){
            $this->view_component->seo('image', assetRemote('image/collection/motobatt/mo628.jpg'));
        }
        $this->view_component->seo('title', $assortment->meta_title);
        $this->view_component->seo('description', '【' . $assortment->meta_title . '】' . $assortment->meta_description);
//        $this->view_component->seo('keyword', $assortment->keyword);
        $this->view_component->render();
        return view('response.pages.collection.detail', (array)$this->view_component);
    }

    public function video()
    {
        cacheControlProcess(['assortments-video']);

        $type = \Cache::tags(['assortments-video'])->rememberForever('assortments-video-all',function(){
            return $this->assortmentService->getCollectionTypesByUrlRewrite('video')->first();
        });

        if(!$type){
            app()->abort(404);
        }

        $this->view_component->assortments = $this->assortmentService->getActiveAssortmentByType($type->id,null,8);

        $this->view_component->assortments_histories = $this->assortmentService->getAssortmentByPublishAt(8,[],'DESC',$type->id);

//        $activeAssortments = $this->assortmentService->filterActive($current_type->assortments);
//        $this->view_component->assortments_history = $activeAssortments->sortByDesc('publish_at')->take(8);
//
//        $videoAssortments = [];
//        foreach($activeAssortments as $activeAssortment){
//          $videoAssortments[] = $activeAssortment->url_rewrite;
//        }
//
//        $service = new AssortmentService();
//        $assortments = $service->getAssortments($videoAssortments, ['items', 'type'], 10);

//        $this->view_component->assortments = $assortments;
        $this->view_component->setBreadcrumbs('動態影音特輯');
        $this->view_component->render();

        return view('response.pages.collection.webikeTv',(array)$this->view_component);
    }

    public function getSuzuka2017()
    {
      $bikeNewsService = new BikeNewsService();
      $suzukaNews = $bikeNewsService->getSuzukaTagNews(6);
      if($suzukaNews){
        foreach($suzukaNews as $key => $news){
          $suzukaNewsContent = strip_tags($news->post_content);//过滤换行
          $suzukaNewsContent = str_replace("\r\n","",$suzukaNewsContent);//过滤换行
          $suzukaNewsContent = str_replace("&nbsp;","<br>",$suzukaNewsContent);//过滤换行
          $this->view_component->suzukaNewsContents[] = str_limit( $suzukaNewsContent,150);
          $this->view_component->suzukaNewTitles[] = $news->post_title;
          $this->view_component->suzukaLink[] = $news->guid;

          $dom = new \DOMDocument();
          $dom->loadHTML($news->post_content);
          $images = $dom->getElementsByTagName('img');
          foreach ($images as $key => $value) {
              if(strpos($value->getAttribute('src'), 'promobanner') === false){
                  if($key == 0){
                    $this->view_component->suzukaNewsimgs[] = $value->getAttribute('src');
                  }
              }
          }
        }
      $this->view_component->setBreadcrumbs('2017鈴鹿8耐');
      $this->view_component->seo('title', '2017鈴鹿8耐賽事特輯');
      $this->view_component->seo('description', '夏季賽車盛會，一年一度邁入第40屆的鈴鹿8耐將於7/27-30點燃戰火，鈴鹿8耐，門票販售、歷史介紹、觀賽重點、周邊活動、鈴鹿賽道介紹，最豐富與詳細的鈴鹿8耐內容皆在Webike台灣。');
      $this->view_component->seo('image', assetRemote('image/suzuka/2017/banner.jpg'));
      $this->view_component->seo('keyword', '日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,騎士用品,流行企劃,車輛特輯,全球知名品牌');
      $this->view_component->render();
      }
      return view('response.pages.collection.suzuka8hours.2017',(array)$this->view_component);
    }

    public function getSuzuka2018()
    {
      $bikeNewsService = new BikeNewsService();
      $suzukaNews = $bikeNewsService->getSuzukaTagNews(6);
      if($suzukaNews){
        foreach($suzukaNews as $key => $news){
          $suzukaNewsContent = strip_tags($news->post_content);//过滤换行
          $suzukaNewsContent = str_replace("\r\n","",$suzukaNewsContent);//过滤换行
          $suzukaNewsContent = str_replace("&nbsp;","<br>",$suzukaNewsContent);//过滤换行
          $this->view_component->suzukaNewsContents[] = str_limit( $suzukaNewsContent,150);
          $this->view_component->suzukaNewTitles[] = $news->post_title;
          $this->view_component->suzukaLink[] = $news->guid;

          $dom = new \DOMDocument();
          $dom->loadHTML($news->post_content);
          $images = $dom->getElementsByTagName('img');
          foreach ($images as $key => $value) {
              if(strpos($value->getAttribute('src'), 'promobanner') === false){
                  if($key == 0){
                    $this->view_component->suzukaNewsimgs[] = $value->getAttribute('src');
                  }
              }
          }
        }
      $this->view_component->setBreadcrumbs('2018鈴鹿8耐');
      $this->view_component->seo('title', '2018鈴鹿8耐賽事特輯');
      $this->view_component->seo('description', '2018摩托車的夏日盛會！邁入第41屆的鈴鹿8耐即將點燃戰火！鈴鹿8耐門票販售、歷史介紹、觀賽重點、周邊活動、鈴鹿賽道介紹，最豐富與詳細的鈴鹿8耐內容皆在Webike台灣。');
      $this->view_component->seo('image', assetRemote('image/suzuka/2017/banner.jpg'));
      $this->view_component->seo('keyword', '日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,騎士用品,流行企劃,車輛特輯,全球知名品牌');
      $this->view_component->render();
      }
      return view('response.pages.collection.suzuka8hours.2018',(array)$this->view_component);
    }

    public function getNgkStock($type = null)
    {
        $repository = new CollectionRepository;
        $ngkProducts_all = $repository->getNgkStockProducts()->KeyBy('sku');
        $this->view_component->ngkProducts = $ngkProducts_all;
        switch ($type) {
            case 'normal':
                $type_name = '標準型';
                break;
            case 'ir':
                $type_name = '銥合金';
                break;
            case 'race':
                $type_name = '競賽型';
                break;
            default:
                $type = 'normal';
                $type_name = '標準型';
                break;
        }

        $this->view_component->ngkStockDatas = $repository->getNgkStockTypeData($type);

        $this->view_component->setBreadcrumbs('注目品牌特輯', route('collection-type', ['brand']));
        $this->view_component->setBreadcrumbs('NGK'.$type_name.'火星塞全品項販售中');
        $this->view_component->seo('title', 'NGK'.$type_name.'火星塞全品項販售中');
        $this->view_component->seo('description', '世界級品質的火星塞－NGK，提供多種價數的火星塞，滿足各層面的使用需求。更有銥合金與競技型火星塞，讓引擎發揮更好的效能！');
        $this->view_component->seo('keyword', '日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,騎士用品,流行企劃,車輛特輯,全球知名品牌');
        $this->view_component->render();

        return view('response.pages.collection.ngk.ngk_'.$type ,(array)$this->view_component);
    }



    public function getTrickstar()
    {
      $bikeNewsService = new BikeNewsService();
      $suzukaNews = $bikeNewsService->getSuzukaTagNews(6);
      if($suzukaNews){
        foreach($suzukaNews as $key => $news){
          $suzukaNewsContent = strip_tags($news->post_content);//过滤换行
          $suzukaNewsContent = str_replace("\r\n","",$suzukaNewsContent);//过滤换行
          $suzukaNewsContent = str_replace("&nbsp;","<br>",$suzukaNewsContent);//过滤换行
          $this->view_component->suzukaNewsContents[] = str_limit( $suzukaNewsContent,150);
          $this->view_component->suzukaNewTitles[] = $news->post_title;
          $this->view_component->suzukaLink[] = $news->guid;

          $dom = new \DOMDocument();
          $dom->loadHTML($news->post_content);
          $images = $dom->getElementsByTagName('img');
          foreach ($images as $key => $value) {
              if(strpos($value->getAttribute('src'), 'promobanner') === false){
                  if($key == 0){
                    $this->view_component->suzukaNewsimgs[] = $value->getAttribute('src');
                  }
              }
          }
        }
      $this->view_component->setBreadcrumbs('注目品牌特輯', route('collection-type', ['brand']));
      $this->view_component->seo('title', '賽道經驗的實際回饋！TRICK STAR 部品，襲來！');
      $this->view_component->seo('description', 'TRICK STAR 商品介紹！以賽事為根基所打造的商品，也是另一種帥氣氛圍！以參加世界耐力錦標賽 EWC為重心的“TRICKSTAR”車隊，用＂騎士真正的需求＂為基底、再揉合自身於賽場的高水平戰鬥經驗，提供騎士各種商品製作與服務。');
      $this->view_component->seo('keyword', '日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,騎士用品,流行企劃,車輛特輯,全球知名品牌');
      $this->view_component->render();
      }
      
      return view('response.pages.collection.trickstar.trickstar',(array)$this->view_component);
    }
}