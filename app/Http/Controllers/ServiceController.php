<?php
namespace App\Http\Controllers;

use App\Components\View\SimpleComponent;
use Everglory\Constants\CustomerRole;

class ServiceController extends Controller
{
	public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        // $component->base_url = route('motor');
        // $component->setBreadcrumbs('會員好康', $component->base_url);
    }

    public function getYfcShipping()
    {
    	$this->view_component->setBreadcrumbs('安心快速的物流服務');
        $this->view_component->seo('title','安心快速的物流服務');
        $this->view_component->seo('description','Webike是以電子商務為核心的摩托車商品供應商。要如何提供客戶最好的購物環境?除了前台的網站與服務之外，後勤的物流系統也是非常重要的環節，以下就帶大家到「Webike橫濱物流中心」認識一下吧!');
        $this->view_component->seo('keywords', 'Webike, 物流中心, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA');
        $this->view_component->render();
        return view('response.pages.service.yfcshipping', (array)$this->view_component);
    }

    public function getJoinSupplier()
    {
        $this->view_component->hsaNewOffice = hsaNewOffice();
    	$this->view_component->setBreadcrumbs('品牌大募集');
        $this->view_component->seo('title','品牌大募集，歡迎機車摩托車改裝零件騎士用品廠商免費刊登');
        $this->view_component->seo('description','台灣第一摩托車電子商務購物平台品牌，提供進口正廠機車零件、國產正廠機車零件、機車改裝零件、機車騎士用品網路購物，機車零件製造商、機車零件供應商、機車零配件通路商、設計師等相關產業B2B、B2C 網路平台銷售、代銷，免上架費、成交費、資料處理費、免自行管理，金流簡單，出貨方便，專人處理客服問題，歡迎廠商經銷業務合作。');
        $this->view_component->seo('keywords', '進口機車,國產機車,通路,銷售,免費刊登,行銷宣傳,關鍵字,網路購物,廠商合作,製造商,進口商,總代理,總經銷,批發商,區域經銷,免費上架,大盤商,中盤商,網購,,B2B,B2C,改裝零件,機車百貨,網路通路');
        $this->view_component->render();
        return view('response.pages.service.supplier.join', (array)$this->view_component);
    }

    public function getJoinSupplierform()
    {
        $this->view_component->hsaNewOffice = hsaNewOffice();
    	$this->view_component->setBreadcrumbs('品牌大募集',\URL::route('service-supplier-join'));
    	$this->view_component->setBreadcrumbs('基本資料填寫');
        $this->view_component->seo('title','基本資料填寫');
        $this->view_component->seo('description','台灣第一摩托車電子商務購物平台品牌，提供進口正廠機車零件、國產正廠機車零件、機車改裝零件、機車騎士用品網路購物，製造商、供應商、機車零配件通路商B2B、B2C 廠商經銷業務合作。');
        $this->view_component->seo('keywords', '進口機車,國產機車,通路,銷售,免費刊登,行銷宣傳,關鍵字,網路購物,廠商合作,製造商,進口商,總代理,總經銷,批發商,區域經銷,免費上架,大盤商,中盤商,網購,B2B,B2C,改裝零件');
        $this->view_component->render();
        return view('response.pages.service.supplier.joinform', (array)$this->view_component);
    }

    public function getJoinDealer()
    {
        $this->view_component->hsaNewOffice = hsaNewOffice();
    	$this->view_component->setBreadcrumbs('Webike經銷商大募集');
        $this->view_component->seo('title','超過30萬項海內外專業摩托車零件部品販售、歡迎各摩托車相關產業免費加入。車輛維修：進口重機維修，三陽、光陽、山葉、台鈴一般機車維修車行。周邊商品販賣：人身部品專賣店、安全帽專賣店、改裝用品店、機車材料行、潮流服飾店。車輛販賣業：重機專賣店、車輛代理商、二手車行。機車服務業：駕訓班、車輛租賃、機車美容、烤漆服務。如您有任何疑問，歡迎您直接與我們聯繫，將會有專員替您服務，感謝。');
        $this->view_component->seo('description','台灣第一摩托車電子商務購物平台品牌，提供進口正廠機車零件、國產正廠機車零件、機車改裝零件、機車騎士用品網路購物，製造商、供應商、機車零配件通路商B2B、B2C 廠商經銷業務合作。');
        $this->view_component->seo('keywords', '大盤商,批發商,零售,代理, 經銷, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質,進貨,原廠,部品,材料行, honda, yamaha, suzuki, kawasaki, 三陽SYM, 光陽KYMCO, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA,PGO,宏佳騰AEON,比雅久,哈特佛,CPI捷穎');
        $this->view_component->render();
        return view('response.pages.service.joindealer', (array)$this->view_component);
    }

    public function getJoinDealerForm()
    {
    	$this->view_component->setBreadcrumbs('Webike經銷商大募集',\URL::route('service-dealer-join'));
    	$this->view_component->setBreadcrumbs('申請表格');
        $this->view_component->seo('title','申請表格');
        $this->view_component->seo('description','注意事項：1.申請文件：營利事業登記證及名片。2.營利事業登記證或名片影本，請傳真至02-29041083，或是以照片形式EMAIL回傳：biz@webike.tw，我們收到資料後會盡速與您聯繫，謝謝。');
        $this->view_component->seo('keywords', '大盤商,批發商,零售,代理, 經銷, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質,進貨,原廠,部品,材料行, honda, yamaha, suzuki, kawasaki, 三陽SYM, 光陽KYMCO, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA,PGO,宏佳騰AEON,比雅久,哈特佛,CPI捷穎');
        $this->view_component->render();
        return view('response.pages.service.joindealerform', (array)$this->view_component);
    }

    /* Temp move from CheckoutController ( logout Error) */
    public function finish(){
        $order_increment_id = session('order_increment_id');
        if(!$order_increment_id){
            \Log::error('找不到訂單編號。', session()->all());
            return abort(404);
        }
        $this->view_component->order_increment_id = $order_increment_id;
        $this->view_component->setBreadcrumbs('訂單完成(' . $order_increment_id . ')');
        $this->view_component->seo('title', '訂購查詢完成');
        $this->view_component->render();
        return view('response.pages.checkout.done', (array)$this->view_component);
    }

    public function getDealerGradeInfo()
    {
        $this->view_component->hsaNewOffice = hsaNewOffice();
        $this->view_component->setBreadcrumbs('Webike經銷商升級制度');
        $this->view_component->seo('title','Webike經銷商升級制度');
        $this->view_component->seo('description','超過30萬項海內外專業摩托車零件部品販售、歡迎各摩托車相關產業免費加入，升級制度提供經銷商三種會員等級：黃金會員、白銀會員、標準會員，共有不同的回饋點數機制，最多2倍現金點數回饋。車輛維修：進口重機維修，三陽、光陽、山葉、台鈴一般機車維修車行。周邊商品販賣：人身部品專賣店、安全帽專賣店、改裝用品店、機車材料行、潮流服飾店。車輛販賣業：重機專賣店、車輛代理商、二手車行。機車服務業：駕訓班、車輛租賃、機車美容、烤漆服務。如您有任何疑問，歡迎您直接與我們聯繫，將會有專員替您服務。');
        $this->view_component->seo('keywords', '盤商,批發商,零售,代理, 經銷, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質,進貨,原廠,部品,材料行, honda, yamaha, suzuki, kawasaki, 三陽SYM, 光陽KYMCO, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA,PGO,宏佳騰AEON,比雅久,哈特佛,CPI捷穎');
        $this->view_component->render();
        return view('response.pages.service.dealergradeinfo', (array)$this->view_component);
    }

    public function getDealerInstallmentInfo()
    {
        if(!$this->view_component->current_customer or ($this->view_component->current_customer->role_id != CustomerRole::WHOLESALE and $this->view_component->current_customer->role_id != CustomerRole::STAFF)){
            return abort(404);
        }
        $this->view_component->hsaNewOffice = hsaNewOffice();
        $this->view_component->setBreadcrumbs('Webike經銷商分期付款服務');
        $this->view_component->seo('title','Webike經銷商分期付款服務');
        $this->view_component->seo('description','Webike台灣的經銷商會員們，只要您在Webike摩托百貨消費的最後結帳金額超過3千元，即可享有信用卡分3期與6期的分期付款服務。');
        $this->view_component->seo('keywords', '盤商,批發商,零售,代理, 經銷, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質,進貨,原廠,部品,材料行, honda, yamaha, suzuki, kawasaki, 三陽SYM, 光陽KYMCO, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA,PGO,宏佳騰AEON,比雅久,哈特佛,CPI捷穎');
        $this->view_component->render();
        return view('response.pages.service.dealerinstallmentinfo', (array)$this->view_component);
    }
}