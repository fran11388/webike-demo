<?php

namespace App\Http\Controllers\Customer;

use Ecommerce\Service\ProductService;
use URL;
use Auth;
use App\Http\Controllers\Controller;
use App\Components\View\SimplePagerComponent;
use Ecommerce\Service\ContactService;
use Ecommerce\Service\HistoryService;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;
use Illuminate\Http\Request as Requests;
use Ecommerce\Service\Contact\FaqService;

class ContactController extends Controller
{
    public function __construct(SimplePagerComponent $component)
    {
        $this->middleware(function ($request, $next) {
            $this->service = app()->make(ContactService::class);
            return $next($request);
        });
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心', $component->base_url);
        $component->setBreadcrumbs('線上客服諮詢');
    }

    public function message()
    {
        $types = $this->service->getTypes();
        $collection = $this->service->getRepliedList();
        $listIntegration = new ListIntegration($collection, 'Ticket');
        $this->view_component->types = $types;
        $this->view_component->setCollection($listIntegration->collection->sortByDesc('response_at'), true);
        $this->view_component->setBreadcrumbs('新訊息通知');
        $this->view_component->seo('title', '新訊息通知');
        $this->view_component->render();

        return view('response.pages.customer.contact.history', (array)$this->view_component);
    }

    public function history()
    {
        $types = $this->service->getTypes();
        $collection = $this->service->getList();

        $listIntegration = new ListIntegration($collection, 'Ticket');
        $this->view_component->types = $types;
        $this->view_component->setCollection($listIntegration->collection->sortByDesc('created_at'), true);
        $this->view_component->setBreadcrumbs('問題紀錄');
        $this->view_component->seo('title', '問題紀錄');
        $this->view_component->render();

        return view('response.pages.customer.contact.history', (array)$this->view_component);
    }

    public function proposal()
    {
        $this->view_component->departments = $this->service->getDepartmentTypes();
        $this->view_component->setBreadcrumbs('問題提問');
        $this->view_component->seo('title', '問題提問');
        $this->view_component->render();

        return view('response.pages.customer.contact.proposal', (array)$this->view_component);
    }

    public function create($ticket_type)
    {
        $types = $this->service->getTypes($ticket_type);
        $type = $types->first();
        if (!$type) {
            return abort(404);
        }
        if ($type->order_flg) {
            $historyService = new HistoryService;
            $this->view_component->orders = $collection = $historyService->getOrders();
        }
        $this->view_component->types_layer = json_encode($this->service->getTypesLayer());
        $this->view_component->relations = $this->service->getRelations();
        $this->view_component->type = $type;
        $this->view_component->setBreadcrumbs('問題提問', URL::route('customer-service-proposal'));
        $this->view_component->setBreadcrumbs($type->name);
        $this->view_component->seo('title', $type->name);
        $this->view_component->render();

        return view('response.pages.customer.contact.create', (array)$this->view_component);
    }

    public function postCreate()
    {
        if (!request()->has('category_id') or !is_numeric(request()->input('category_id')) or request()->input('category_id') <= 0) {
            return redirect()->back()->with('msg', '尚未選擇分類');
        }
        if (!request()->get('content')) {
            return redirect()->back()->with('msg', '尚未輸入內容');
        }
        $ticket = $this->service->insertTicket();
        return redirect()->route('customer-service-history-dialog', $ticket->url_rewrite);
    }

    public function dialog($url_rewrite)
    {
        $ticket = $this->service->loadTicket($url_rewrite);
        if (!$ticket) {
            return abort(404);
        }
        $this->view_component->ticket = $ticket;
        $this->view_component->relations = $this->service->rebuildRelations($ticket->relations);
        $this->view_component->setBreadcrumbs('問題紀錄', URL::route('customer-service-history'));
        $this->view_component->setBreadcrumbs('案件編號：' . $url_rewrite);
        $this->view_component->seo('title', '案件編號：' . $url_rewrite);
        $this->view_component->render();

        return view('response.pages.customer.contact.dialog', (array)$this->view_component);
    }

    public function renderDialogs($url_rewrite)
    {
        $ticket = $this->service->loadTicket($url_rewrite);
        if (!$ticket) {
            return null;
        }
        $this->view_component->replies = $ticket->replies;

        return view('response.pages.customer.contact.partials.dialogs', (array)$this->view_component)->render();
    }

    public function reply()
    {
        if (!request()->get('content')) {
            return redirect()->back()->with('msg', '尚未輸入內容');
        }
        $this->service->replyTicket();
        return;
    }

    public function read()
    {
        $this->service->readTicket();
        return;
    }

    public function getfaq(Requests $request)
    {

//$sku=$request->get('skus');
//$obj=new \Ecommerce\Service\Contact\check\FromProductHasOptionsHasStock();
//        dd($obj->check());
        
//        $filters = [
//            'NotFromProduct' => 'ShoppingShippingDateNotFromProductFaq',
//            'FromProductHasOptionsHasStock' => 'ShoppingShippingDateFromProductHasOptionsHasStockFaq',
//            'FromProductNoOptionsHasStock' => 'ShoppingShippingDateFromProductNoOptionsHasStockFaq',
//            'FromProductHasOptionsNoStock' => 'ShoppingShippingDateFromProductHasOptionsNoStockFaq',
//            'FromProductNoOptionsNoStock' => 'ShoppingShippingDateFromProductNoOptionsNoStockFaq',
//            'FromProductHasOptionsSoldOut' => 'ShoppingShippingDateFromProductHasOptionsSoldOutFaq',
//            'FromProductNoOptionsSoldOut' => 'ShoppingShippingDateFromProductNoOptionsSoldOutFaq',
//            'FromProductHasOptionsDiscontinued' => 'ShoppingShippingDateFromProductHasOptionsDiscontinuedFaq',
//            'FromProductNoOptionsDiscontinued' => 'ShoppingShippingDateFromProductNoOptionsDiscontinuedFaq',
//        ];
//        $params = null;
//        $result=[];
//        foreach ($filters as $check_name => $filter) {
//            $class_name = '\Ecommerce\Service\Contact\check\\'. $check_name;
//            $check_class = new $class_name;
//            $check = $check_class->check($params);
//            $result[$check_name]=$check;
//        }
//dd($result);

        $category_id = request()->get('category_id');
        $FaqService = new FaqService;
        $category_name = '\Ecommerce\Service\Contact\categories\\' . $FaqService->getFaqCategoryClassName($category_id);
//        dd($category_name);
        $answer = null;
        if (class_exists($category_name)) {
            $category_class = new $category_name;
            $category_class->setParams(request()->all());
            $answer = $category_class->getfaq();
        }

        if (!$answer) {
            $answer = ["faq_text" => "如果您有任何問題或是需求，請直接在下面的問題敘述提出。", "default" => 0];
        }

        $this->view_component->answer = $answer;

        $html = view('response.pages.customer.contact.partials.faq', (array)$this->view_component)->render();
        return json_encode(['html' => $html]);
    }
}