<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Components\View\SimpleComponent;
use Ecommerce\Service\WishlistService;
use Ecommerce\Service\CartService;

class WishlistController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心',$component->base_url);
    }

    public function index(WishlistService $service)
    {
        $this->view_component->carts = $service->getItems();

        $this->view_component->setBreadcrumbs('待購清單', '');
        $this->view_component->seo('title','「Webike-我的待購清單」');
        $this->view_component->seo('description','【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keywords','進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.wishlist.index', (array)$this->view_component);
    }
    
    public function move(CartService $cartService , WishlistService $wishlistService )
    {
        //Add to cart
        $cartService->updateItem(request()->get('sku'), request()->get('qty'));
        //remove from wishlist
        $wishlistService->removeItem(request()->get('code'));
        return redirect()->route('cart');
    }

    public function remove(WishlistService $wishlistService )
    {
        $wishlistService->removeItem(request()->get('code'));
        return redirect()->back();
    }

    public function update(WishlistService $wishlistService )
    {
        $wishlistService->updateItem(request()->get('sku'), request()->get('qty'));
        return redirect()->route('customer-wishlist');
    }



}