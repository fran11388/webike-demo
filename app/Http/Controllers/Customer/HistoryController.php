<?php

namespace App\Http\Controllers\Customer;

use Carbon\Carbon;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\Contact\categories\OrderItem;
use Ecommerce\Service\GenuinepartsService;
use Ecommerce\Service\OrderService;
use Ecommerce\Service\ProductService;
use Everglory\Models\Customer;
use Everglory\Models\Order;
use GuzzleHttp\Client;
use URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\Components\View\SimplePagerComponent;
use Ecommerce\Service\HistoryService;
use Everglory\Constants\EstimateType;
use Ecommerce\Service\ReturnService;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;
use Ecommerce\Repository\ReviewRepository;
use Ecommerce\Service\ContactService;
use Illuminate\Foundation\Auth\User;
use Ecommerce\Service\AccountService;
use Ecommerce\Service\CartService;
use Ecommerce\Core\TableFactory\src\App\Excel\Service as TableFactory;
use Everglory\Models\Product;
use Everglory\Models\Customer\Gift as CustomerGifts;
use Ecommerce\Service\GiftService;
use Ecommerce\Accessor\ProductAccessor;

class HistoryController extends Controller
{
    public function __construct(SimplePagerComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心', $component->base_url);
    }

    public function genuinepartsHistory(HistoryService $historyService)
    {
        $estimates = $historyService->getTypeEstimate([EstimateType::IMPORT, EstimateType::DOMESTIC], true);
        $this->view_component->setCollection($estimates, true);
        $this->view_component->setBreadcrumbs('正廠零件查詢履歷');
        $this->view_component->seo('title', 'Webike-正廠零件查詢履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.history.genuineparts', (array)$this->view_component);
    }

    public function genuinepartsDetailHistory(HistoryService $historyService, $estimate_code)
    {
//        dd(config('api.delivery_date_query'));


        $estimate_items = $historyService->getEstimateItem($estimate_code);
//dd($estimate_items);
        if ($estimate_item = $estimate_items->first()) {
            $estimate = $estimate_item->estimate;
            if ($estimate->status_id != 10) {
                return redirect()->route('customer-history-genuineparts');
            }
        } else {
            app()->abort(404);
        }

        $historyService->setRead($estimate);
        $listIntegration = new ListIntegration(new Collection([$estimate]));
        $this->view_component->document = $listIntegration->collection->first();
        $model_numbers = $estimate_items->pluck('model_number')->toArray();
        $productRepository = new ProductRepository();
        $this->view_component->model_numbers = implode(',', $model_numbers);
//        $outletProducts = $productRepository->getOutletProductByModelNumber($model_numbers);
        $listIntegration = new ListIntegration();
//        $listIntegration->setPackage('outletProducts', $outletProducts);

        $listIntegration->bulidCollection($estimate_items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('正廠零件查詢履歷', URL::route('customer-history-genuineparts'));
        $this->view_component->setBreadcrumbs($estimate_code);
        $this->view_component->seo('title', $estimate_code . '正廠零件查詢履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();


        return view('response.pages.customer.history.genuineparts-detail', (array)$this->view_component);
    }


    public function mitumoriHistory(HistoryService $historyService)
    {
        $collection = $historyService->getValidMitumori();
        $listIntegration = new ListIntegration($collection);
        $this->view_component->setCollection($listIntegration->collection, true);
        $this->view_component->setBreadcrumbs('未登錄商品查詢履歷');
        $this->view_component->seo('title', '未登錄商品查詢履歷');
        $this->view_component->render();

        return view('response.pages.customer.history.mitumori', (array)$this->view_component);
    }

    public function mitumoriDetailHistory(HistoryService $historyService, $estimate_code)
    {

        $collection = $historyService->getValidMitumori($estimate_code);
        $mitumori = $collection->first();
        if (!$mitumori) {
            app()->abort(404);
        }

        $historyService->setRead($mitumori);
        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
        $this->view_component->document = $document;
        $listIntegration = new ListIntegration($mitumori->items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('未登錄商品查詢履歷', URL::route('customer-history-mitumori'));
        $this->view_component->setBreadcrumbs($estimate_code . '未登錄商品查詢履歷');
        $this->view_component->seo('title', $estimate_code . '未登錄商品查詢履歷');
        $this->view_component->render();

        return view('response.pages.customer.history.mitumori-detail', (array)$this->view_component);
    }

    public function groupbuyHistory(HistoryService $historyService)
    {
        $collection = $historyService->getValidGroupBuy();
        $listIntegration = new ListIntegration($collection);
        $this->view_component->setCollection($listIntegration->collection, true);
        $this->view_component->setBreadcrumbs('團購商品查詢履歷');
        $this->view_component->seo('title', '團購商品查詢履歷');
        $this->view_component->render();

        return view('response.pages.customer.history.groupbuy', (array)$this->view_component);
    }

    public function groupbuyDetailHistory(HistoryService $historyService, $estimate_code)
    {
        $collection = $historyService->getValidGroupBuy($estimate_code);
        $groupbuy = $collection->first();
        if (!$groupbuy) {
            app()->abort(404);
        }
        $historyService->setRead($groupbuy);
        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
        $this->view_component->document = $document;
        $listIntegration = new ListIntegration($groupbuy->items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('團購商品查詢履歷', URL::route('customer-history-groupbuy'));
        $this->view_component->setBreadcrumbs($estimate_code . '團購商品查詢履歷');
        $this->view_component->seo('title', $estimate_code . '團購商品查詢履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.history.groupbuy-detail', (array)$this->view_component);
    }


    public function orderHistory(HistoryService $historyService)
    {
        $collection = $historyService->getOrdersWithPaginate();
        $this->view_component->setCollection($collection, true);
        $this->view_component->title_text = '訂單動態及歷史履歷';
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷');
        $this->view_component->seo('title', '訂單動態及歷史履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.history.order', (array)$this->view_component);

    }

    public function orderNotFinishHistory(HistoryService $historyService)
    {
        $collection = $historyService->getNotFinishOrdersWithPaginate();
        $this->view_component->setCollection($collection, true);
        $this->view_component->title_text = '正在進行中的訂單';
        $this->view_component->setBreadcrumbs('正在進行中的訂單');
        $this->view_component->seo('title', '正在進行中的訂單');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.history.order', (array)$this->view_component);
    }

    public function orderPreview(HistoryService $historyService)
    {
        $collection = $historyService->getOrders();
        $listIntegration = new ListIntegration($collection, 'Order\\Preview');
        $this->view_component->collection = $listIntegration->collection;
        return view('response.pages.customer.partials.order-preview', (array)$this->view_component);
    }

    public function orderDetailHistory(HistoryService $historyService, $increment_id)
    {

        $collection = $historyService->getOrders($increment_id);
        $order = $collection->first();
        if (!$order) {
            app()->abort(404);
        }
        $orderService = new OrderService();
        $this->view_component->process = $orderService->getStatusProcess($order);
        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
//        dd($document->state);
        $this->view_component->document = $document;
        $listIntegration = new ListIntegration($order->items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷', URL::route('customer-history-order'));
        $this->view_component->setBreadcrumbs($increment_id . '訂單詳細內容');
        $this->view_component->seo('title', $increment_id . '訂單詳細內容');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.history.order-detail', (array)$this->view_component);
    }

    public function estimateHistory(HistoryService $historyService)
    {
        $estimates = $historyService->getTypeEstimate([EstimateType::GENERAL], true);
        $this->view_component->setCollection($estimates, true);
        $this->view_component->setBreadcrumbs('商品交期查詢履歷');
        $this->view_component->seo('title', '商品交期查詢履歷');
        $this->view_component->render();

        return view('response.pages.customer.history.estimate', (array)$this->view_component);
    }

    public function reviewHistory(HistoryService $historyService, ReviewRepository $reviewRepository)
    {
        $collection = $reviewRepository->getCustomerOrderItemWriteReview($this->view_component->current_customer);
        $listIntegration = new ListIntegration($collection);
        $this->view_component->setCollection($listIntegration->collection, true);
        $this->view_component->setBreadcrumbs('撰寫評論記錄');
        $this->view_component->seo('title', 'Webike-撰寫評論記錄');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.history.review', (array)$this->view_component);
    }

    public function orderDetailHistoryReturn(HistoryService $historyService, $increment_id)
    {

        $collection = $historyService->getOrders($increment_id);
        $order = $collection->first();
        if (!$order) {
            app()->abort(404);
        }
        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
        $this->view_component->document = $document;
        if (!$this->view_component->document->return_avaliable) {
            return redirect()->route('customer-history-order-detail', $increment_id)->withErrors('此訂單目前無法進行退換貨');
        }
        $listIntegration = new ListIntegration($order->items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷', URL::route('customer-history-order'));
        $this->view_component->setBreadcrumbs($increment_id . '訂單詳細內容', URL::route('customer-history-order-detail', $increment_id));
        $this->view_component->setBreadcrumbs('訂單' . $increment_id . '退換貨申請');
        $this->view_component->seo('title', '訂單' . $increment_id . '退換貨申請');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.history.return.index', (array)$this->view_component);
    }

    public function orderDetailHistoryReturnForm(HistoryService $historyService, $increment_id)
    {
        $validates = [
            'type' => 'required|in:return,exchange,return_all',
        ];
        $attributes = [
            'type.required' => '請選擇退貨或換貨',
            'type.in' => '所選的 :attribute 無效',
        ];


        $this->view_component->type = request()->input('type');
        $this->view_component->type_name = ($this->view_component->type == 'exchange' ? '換貨' : '退貨');
        $this->view_component->type_name_eng = ($this->view_component->type == 'exchange' ? 'exchange' : 'return');
        $this->view_component->address = $this->view_component->current_customer->address()->where('is_default', 1)->first();
        $this->view_component->situations = \Everglory\Models\Stock\Situation::whereNotIn('id', [2, 4])->get();
        $this->view_component->select_item = [];

        foreach (request()->input('validate') as $key => $id) {
            $this->view_component->select_item[$id] = request()->input('quantity.' . $key);
        }
        $collection = $historyService->getOrders($increment_id);
        $order = $collection->first();
        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
        $this->view_component->document = $document;
        $items = $order->items->filter(function ($item, $key) use (&$validates, &$attributes) {
            $num = $key + 1;
            $validates['quantity.' . $num] = 'required_with:validate.' . $num . '|integer|between:1,' . $item->quantity;
            $attributes['quantity.' . $num . '.between'] = '商品' . $num . '的數量須在 :min與 :max之間';
            return array_key_exists($item->id, $this->view_component->select_item);
        });

        $this->validate(request(), $validates, $attributes);

        $this->view_component->reasons = \Everglory\Models\Rex\Reason::where('active_' . $this->view_component->type_name_eng, '1')->orderBy('sort')->get();
        $listIntegration = new ListIntegration($items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷', URL::route('customer-history-order'));
        $this->view_component->setBreadcrumbs($increment_id . '訂單詳細內容', URL::route('customer-history-order-detail', $increment_id));
        $this->view_component->setBreadcrumbs('訂單' . $increment_id . '填寫' . ($this->view_component->type == 'exchange' ? '換貨' : '退貨') . '申請書');
        $this->view_component->seo('title', '訂單' . $increment_id . '填寫' . ($this->view_component->type == 'exchange' ? '換貨' : '退貨') . '申請書');
        $this->view_component->render();
        return view('response.pages.customer.history.return.form', (array)$this->view_component);
    }

    public function orderDetailHistoryReturnFormCreate(ReturnService $service, $increment_id)
    {
        $rex = $service->create(request(), $increment_id);
        session()->flash('code', $increment_id);
        return redirect()->route('customer-history-order-detail-return-create-done', $increment_id);
    }

    public function orderDetailHistoryReturnFormCreateDone($increment_id)
    {
        if (!session()->has('code')) {
            return abort(404);
        }
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷', URL::route('customer-history-order'));
        $this->view_component->setBreadcrumbs($increment_id . '訂單詳細內容', URL::route('customer-history-order-detail', $increment_id));
        $this->view_component->setBreadcrumbs('訂單' . $increment_id . '退換貨申請完成');
        $this->view_component->seo('title', '訂單' . $increment_id . '退換貨申請完成');
        $this->view_component->render();

        return view('response.pages.customer.history.return.done', (array)$this->view_component);
    }


    public function returnHistory(HistoryService $historyService)
    {
        $collection = $historyService->getRexes();
        $listIntegration = new ListIntegration($collection);
        $this->view_component->setCollection($listIntegration->collection, true);
        $this->view_component->setBreadcrumbs('退換貨申請履歷');
        $this->view_component->seo('title', '退換貨申請履歷');
        $this->view_component->render();
        return view('response.pages.customer.history.return', (array)$this->view_component);
    }

    public function returnDetailHistory(HistoryService $historyService, $increment_id)
    {
        $collection = $historyService->getRexes($increment_id);
        $rex = $collection->first();

        if (!$rex) {
            app()->abort(404);
        }
        $returnService = new ReturnService();
        $this->view_component->process = $returnService->getStatusProcess($rex);

        $this->view_component->contact_link = URL::route('customer-service-proposal-create', 'support');
        $contactService = new ContactService();
        $ticket_code = $contactService->getRelationTickets(['relations' => ['rex' => $rex->id]])->first();
        if ($ticket_code) {
            $this->view_component->contact_link = URL::route('customer-service-history-dialog', $ticket_code);
        }

        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
        $this->view_component->document = $document;
        $listIntegration = new ListIntegration($rex->items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('退換貨申請履歷', URL::route('customer-history-return'));
        $this->view_component->setBreadcrumbs('退換貨申請' . $increment_id);
        $this->view_component->seo('title', '退換貨申請' . $increment_id);
        $this->view_component->render();

        return view('response.pages.customer.history.return-detail', (array)$this->view_component);
    }

    public function orderDetailHistoryCancel(HistoryService $historyService, $increment_id, OrderService $orderService)
    {
        $collection = $historyService->getOrders($increment_id);
        $order = $collection->first();
        if (!$order) {
            app()->abort(404);
        }

        $process = $orderService->getStatusProcess($order);
        if (!$process->can_cancel) {
            return redirect()->route('customer-history-order-detail', $increment_id)->withErrors('此訂單目前無法進行取消');
        }

        $listIntegration = new ListIntegration($collection);
        $document = $listIntegration->collection->first();
        $this->view_component->document = $document;
        $listIntegration = new ListIntegration($order->items);
        $this->view_component->setCollection($listIntegration->collection);
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷', URL::route('customer-history-order'));
        $this->view_component->setBreadcrumbs($increment_id . '訂單詳細內容', URL::route('customer-history-order-detail', $increment_id));
        $this->view_component->setBreadcrumbs('訂單' . $increment_id . '申請取消');
        $this->view_component->seo('title', '訂單' . $increment_id . '申請取消');
        $this->view_component->render();
        return view('response.pages.customer.history.cancel.index', (array)$this->view_component);
    }

    public function orderDetailHistoryCancelSubmit(HistoryService $historyService, $increment_id, OrderService $orderService)
    {
        $collection = $historyService->getOrders($increment_id);
        $order = $collection->first();

        $process = $orderService->getStatusProcess($order);
        if (!$process->can_cancel) {
            return redirect()->route('customer-history-order-detail', $increment_id)->withErrors('此訂單目前無法進行取消');
        }

        if ($order->customer_id == $this->view_component->current_customer->id) {
            $orderAccessor = new \Ecommerce\Accessor\OrderAccessor($order);
            $status = \Everglory\Models\Sales\Status::where('id', '16')->firstOrFail();
            $orderAccessor->changeStatus($status, '客人取消訂單申請');
            session()->flash('code', $order->increment_id);
            return redirect()->route('customer-history-order-detail-cancel-done', $increment_id);
        } else {
            abort(405);
        }
    }

    public function orderDetailHistoryCancelDone($increment_id)
    {
        if (!session()->has('code')) {
            return abort(404);
        }
        $this->view_component->setBreadcrumbs('訂單動態及歷史履歷', URL::route('customer-history-order'));
        $this->view_component->setBreadcrumbs($increment_id . '訂單詳細內容', URL::route('customer-history-order-detail', $increment_id));
        $this->view_component->setBreadcrumbs('訂單' . $increment_id . '申請取消完成');
        $this->view_component->seo('title', '訂單' . $increment_id . '申請取消完成');
        $this->view_component->render();

        return view('response.pages.customer.history.cancel.done', (array)$this->view_component);
    }

    public function orderDetailHistoryReturnFormCreateImage()
    {

        $result = new \stdClass();
        $result->reload = false;
        $result->item_id = request()->input('item_id');
        if (!\Auth::check()) {
            $result->success = false;
            $result->message = '請重新登入';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        } else {
            $customer = \Auth::user();
        }


        $img_file = request()->file('photo');
        if ($img_file->getSize() > 5 * 1024 * 1024) {
            $result->success = false;
            $result->message = '上傳檔案需小於 5 MB';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }

        if (!in_array($img_file->getMimeType(), ['image/jpeg', 'image/png', 'image/gif'])) {
            $result->success = false;
            $result->message = '只允許上傳 JPEG / PNG / GIF 格式檔案';
            $result = json_encode($result);
            echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
            die();
        }

        $destinationPath = public_path() . '/assets/photos/return/';
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777);
        }
        $size = $img_file->getSize();
        $mimetype = $img_file->getMimeType();
        $resolution = getimagesize($img_file->getRealPath());
        $image_key = getFileKey();
        while (file_exists($destinationPath . $image_key . '.' . $img_file->getClientOriginalExtension())) {
            $image_key = getFileKey();
        }
        $filename_tmp = $image_key . '_tmp.' . $img_file->getClientOriginalExtension();
        $location = $destinationPath . $filename_tmp;
        $img_file->move($destinationPath, $filename_tmp);
        $filename = str_replace('_tmp.' . $img_file->getClientOriginalExtension(), '.' . $img_file->getClientOriginalExtension(), $filename_tmp);
        ImageResize($location, $destinationPath . $filename);

        $result->success = true;
        $result->key = $filename;
        $result = json_encode($result);
        echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
        //unlink($location);
        die();

    }


    public function pointsHistory(HistoryService $historyService)
    {
        $collection = $historyService->getRewardsWithPaginate();
        $this->view_component->setCollection($collection, true);
        $this->view_component->setBreadcrumbs('點數獲得及使用履歷');
        $this->view_component->seo('title', '點數獲得及使用履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.history.reward', (array)$this->view_component);
    }

    public function quotes($increment_id)
    {
        $orderService = new OrderService();
        $orderService->getQuotes($increment_id);
    }

    public function giftsHistory()
    {
        $products = [];
        $customerGifts = GiftService::getActiveCustomerGiftBuilder([
            'gift',
            'gift.origProduct',
            'gift.origProduct.images',
            'gift.origProduct.prices',
            'gift.origProduct.motors',
            'gift.origProduct.options',
            'gift.origProduct.fitModels',
        ])->select('customer_gifts.id as id', 'customer_gifts.gift_id', 'customer_gifts.status')
            ->join('gifts as gf', 'customer_gifts.gift_id', '=', 'gf.id')
            ->where('customer_id', '=', $this->view_component->current_customer->id)
            ->where('type', '=', GiftService::PRODUCT)
            ->get();

        if (count($customerGifts)) {
            foreach ($customerGifts as $customerGift) {
                $origProduct = $customerGift->gift->origProduct;
                if ($origProduct) {
                    $products[$origProduct->id] = new ProductAccessor($origProduct);
                }
            }
        }

        $this->view_component->customerGifts = $customerGifts;
        $this->view_component->products = $products;
        $this->view_component->setBreadcrumbs('贈品兌換');
        $this->view_component->seo('title', '「Webike-贈品兌換清單」');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.history.gifts', (array)$this->view_component);

    }

    public function postGiftsHistory()
    {
        // $gift = GiftService::changeCustomerGiftStatus(1,2,3);
        // $Models = CustomerGifts::get();
        $customer_id = $this->view_component->current_customer->id;
        // $customer_gift_id= request()->input('join_cart')
        // $customer_gift_id=$_POST['join_cart']
        // request()->all();
        // request()->input('join_cart')

        //  <form class="box" method="post" action="{{ URL::route('customer-history-gifts')}}">
        //     <button type='submit' name="join_cart" value="{{$gift->id}}" class='btn btn-danger btn-full'>加入購物車兌換</button>
        // </form>
        // request()->input('join_cart')
        // $_POST['join_cart']

        $redirect = request()->input('redirect');
        $action = request()->input('action');
        $customer_gift_id = request()->input('gift');
        if (!GiftService::giftActive($customer_gift_id)) {
            if ($action == 'remove_cart') {
                GiftService::changeCustomerGiftStatus(GiftService::REWARD, $customer_id, $customer_gift_id);
            } else if ($action == 'bought') {
                $customerGifts = GiftService::getCustomerGiftsByStep('checkout');
                GiftService::changeCustomerGiftStatus(GiftService::CHECKOUT, $customer_id, $customerGifts->pluck('id')->toArray());
            } else {
                if (!GiftService::hasGiftInCart()) {
                    $changegift = GiftService::changeCustomerGiftStatus(GiftService::IN_CART, $customer_id, $customer_gift_id);
                    return redirect()->route('cart');
                }
            }
        }

        if (request()->ajax()) {
            $result = getBasicAjaxObject();
            $result->success = true;
            return json_encode($result);
        }

        if ($redirect) {
            return redirect()->route($redirect);
        }
        return redirect()->back();
    }

    public function genuinepartsDetailHistoryMatch()
    {
        $model_numbers = explode(',', request()->input('model_numbers'));
        $manufacturer_id = explode(',', request()->input('manufacturer_id'));
        $service = new ProductService;
        $match_products = $service->getMatchProductsByModelNumber($model_numbers, $manufacturer_id);

        return json_encode($match_products);
    }

    public function purchasedHistory(HistoryService $historyService)
    {
        ini_set("memory_limit", "2048M");

        $collection = $historyService->getOrdersWithPaginate();
        $this->view_component->setCollection($collection, true);
        $this->view_component->title_text = '購入商品履歷';
        $this->view_component->setBreadcrumbs('購入商品履歷');
        $this->view_component->seo('title', '購入商品履歷');
        $this->view_component->seo('description', '【全球知名品牌，超過30萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城「Webike-摩托百貨」!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        $manufactures = GenuinepartsService::getManufacturers();
        $this->view_component->manufacture_suppliers = $manufactures->pluck('supplier_id', 'manufacturer_id')->all();

        $customer_id = \Auth::user()->id;

        $items = HistoryService::getMystyleOrderItems($customer_id);
        $this->view_component->collection = $items;
        $this->view_component->sku_grouped_order_items = HistoryService::getMystyleGroupedOrderItems($items);
        $this->view_component->manufacturers = HistoryService::itemsExtractManufactures($customer_id);
        $this->view_component->first_layer_categories = HistoryService::itemsExtractFirstLayerCategories($customer_id);
        $this->view_component->sortingWays = HistoryService::getMyStyleSortingWay();


        if (!\Ecommerce\Core\Agent::isNotPhone()) {
            //手機板購買履歷
            return view('mobile.pages.customer.history.product-history', (array)$this->view_component);
        } else {
            // 桌機板購買履歷
            return view('response.pages.customer.history.product-history', (array)$this->view_component);
        }

    }

    public function getOrderItems()
    {
        $customer_id = \Auth::user() ? \Auth::user()->id : null;
        $ids = explode(',', request()->get('ids'));
        $items = Order\Item::whereIn('id', $ids)
            ->where('customer_id', $customer_id)
            ->get();
        $result = [];
        foreach ($items as $item) {
            unset($item->cost);
            $result[$item->id] = $item;
        }

        return response()->json($result, 200);
    }

    public function putOrderItem()
    {
        $customer_id = \Auth::user() ? \Auth::user()->id : null;
        $remark = request()->get('remark');
        $item = Order\Item::where('id', request()->get('id'))
            ->where('customer_id', $customer_id)
            ->first();

        if ($item == null) return response()->json(['status' => 'false'], 400);

        $item->remark = $remark;
        $item->save();
        return response()->json(['status' => 'success'], 200);
    }


}