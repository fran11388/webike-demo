<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Components\View\SimpleComponent;
use Ecommerce\Core\Validate\ValidateEmail;
use Ecommerce\Core\Wmail;
use Ecommerce\Repository\CustomerRepository;
use Ecommerce\Service\AccountService;
use Ecommerce\Service\Customer\ValidateCustomerEmailService;
use Ecommerce\Service\CustomerService;
use Everglory\Models\Customer;
use Illuminate\Http\Request;
use Everglory\Constants\CustomerRole;
use Ecommerce\Repository\MotorRepository;
use Everglory\Constants\LoginImage;
use URL;

class AccountController extends Controller
{
    private $redirect_route;

    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心',$component->base_url);
        $this->redirect_complete_route = ['benefit-event-motogp-2017', 'benefit-event-webikeguess-get', 'benefit-event-webikewanted', 'benefit-event-webikequiz'];

        $this->middleware(function ($request, $next) {
            request()->session()->keep(['pending_data','pending_route']);
            return $next($request);
        });
    }

    public function create()
    {

        if($this->view_component->current_customer){
            return redirect()->route('customer');
        }else {
            $gallery = LoginImage::GALLERY;
            $this->view_component->background_image = $gallery[rand(0, 4)];
            $this->view_component->setBreadcrumbs('快速註冊');
            $this->view_component->seo('title','5秒鐘快速註冊! 免費獲得機車、重機、改裝商品與騎士用品最新情報');
            $this->view_component->seo('description','免費加入會員即享有以下服務:1.正廠機車零件查詢、報價。2.未登錄機車零件查詢、報價系統。3.團購正廠零件、改裝零件、騎士用品各式機車零件查詢、報價系統。4.立刻獲得100元禮券。「Webike-摩托百貨」');
            $this->view_component->render();

            return view('response.pages.customer.account.create-v1', (array)$this->view_component);
        }
    }

    public function store(Request $request,AccountService $service)
    {
        if($this->view_component->current_customer){
            if($request->ajax()){
                $result = getBasicAjaxObject();
                $result->success = true;
                return json_encode($result);
            }
            return redirect()->route('customer');
        }else{
            $validatorRules = [
                'email' => 'required|unique:customers|max:50|email',
                'password' => 'required|min:6',
                'password_confirm' => 'same:password',
            ];
            $result = getBasicAjaxObject();
            if($request->ajax()){
                $validator = $this->validate($request, $validatorRules,[], [], true);
                if($validator->fails()){
                    $result->redirect_url = "";
                    $result->redirect_check = false;
                    $errorMessages = $validator->getMessageBag()->all();
                    $result->errors = count($errorMessages);
                    $result->messages = $errorMessages;
                    return json_encode($result);
                }
            }else{
                $this->validate($request, $validatorRules,[], []);
            }

            $customer = $service->createAccount($request);
            $wmail = new Wmail;
            $wmail->customerEmailVerify($customer);

            if($request->ajax()){
                $result->success = false;
                $result->redirect_check = true;
                $result->redirect_url = \URL::route('login');
                session()->flash('need_verify' , true);
                session()->flash('customer_email',$customer->email);
                return json_encode($result);
            }

            return redirect()->route('login')->with(['need_verify' => 'true','customer_email' => $customer->email]);

        }
    }


    public function complete(AccountService $service)
    {
        if($this->view_component->current_customer->role_id != CustomerRole::REGISTERED){
            return redirect()->route('customer');
        }
        $this->view_component->address = $service->getDefaultAddress();
        $this->view_component->motors = $service->getMyBike();
        $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();
        $this->view_component->setBreadcrumbs('完整註冊');
        $this->view_component->seo('title','完整註冊，讓您享有更多更完整的服務');
        $this->view_component->seo('description','詳細註冊享有完整客戶服務功能：1.生日禮券。2.現金集點功能。3.摩托車市刊登物件。4.車款流行資訊。');

        return view('response.pages.customer.account.complete', (array)$this->view_component);
    }

    public function account(AccountService $service)
    {
        if($this->view_component->current_customer->role_id == CustomerRole::REGISTERED){
            return redirect()->route('customer-account-complete');
        }
        if(session()->has('account-edit')){
            session()->keep(['account-edit']);
            $this->view_component->address = $service->getDefaultAddress();
            $this->view_component->motors = $service->getMyBike();
            $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();

            $this->view_component->setBreadcrumbs('我的帳號');
            $this->view_component->seo('title', '會員中心');
            $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
            $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
            $this->view_component->render();

            return view('response.pages.customer.account.index', (array)$this->view_component);
        }else{
            return redirect()->route('customer-account-preview');
        }
    }

    public function preview(AccountService $service)
    {
        
        $this->view_component->address = $service->getDefaultAddress();
        $this->view_component->motors = $service->getMyBike();
        $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();
        $this->view_component->setBreadcrumbs('我的帳號');
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.account.preview', (array)$this->view_component);
    }
    
    public function confirm()
    {
        $this->view_component->setBreadcrumbs('我的帳號');
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.account.confirm', (array)$this->view_component);
    }
    
    public function validateConfirm(AccountService $service)
    {
        if($service->validate(request())){
            session()->flash('account-edit','ok');
            return redirect()->route('customer-account');
        }else{
            return redirect()->back()->withErrors('密碼錯誤');
        }

    }


    public function update(Request $request,AccountService $service)
    {
        if(session()->has('account-edit')) {
            session()->keep(['account-edit']);
        }
        $this->validate($request, [
            'email' => 'email|unique:eg_zero.customers,email,' . $this->view_component->current_customer->id . '|max:50',
            'password' => 'min:5',
            'password_confirm' => 'required_with:password|same:password',
            'nickname' => 'required|max:50',
            'realname' => 'required|max:50',
            'birthday-year' => 'required_without:email|not_in:0',
            'birthday-month' => 'required_without:email|not_in:0',
            'birthday-day' => 'required_without:email|not_in:0',
            'address.firstname' => 'required',
            'address.mobile' => 'required',
            'address.phone' => 'required',
            'address.zipcode' => 'required',
            'address.address' => 'required',
        ],[
            // base setting at resources\lang\zh-tw\validation.php
            // here can rewrite the display
        ]);

        $message = $service->updateAccount($request);
        session()->forget('account-preview');

        if(session()->has('register-complete-redirect-route')){
            return redirect()->route(session()->get('register-complete-redirect-route'));
        }
        return redirect()->route($message->route)->with('info',$message->information);
    }

    public function done()
    {
        $this->view_component->setBreadcrumbs('我的帳號');
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        // both create and complete done , decide at view
        return view('response.pages.customer.account.done', (array)$this->view_component);
    }

    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [], $callback = false)
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);
        if($callback){
            return $validator;
        }
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
    }

    public function verifyEmail()
    {
        $uuid = request()->get('code');
        $id = request()->get('number');

        if($uuid && $id){
            $repository = new CustomerRepository;
            $customer = $repository->getCustomerByUuid($uuid)
                ->continueQuery()
                ->where('id',$id)
                ->first();

            if(!$customer){
                app()->abort(404);
            }
            if($customer->email_validate){
                return redirect()->route('home');
            }

            if(ValidateEmail::validate($customer) === true){
                $customer_verify = new \Ecommerce\Core\Validate\Email\Flow\Customer($customer);
                ValidateEmail::success($customer_verify);

//                $redirectRoute = session()->get('register-complete-redirect-route');
//                $redirectURL = session()->get('register-complete-redirect-url');
//                if($request->ajax()){
//                    $result->success = true;
//                    return json_encode($result);
//                }else
//                if(in_array($redirectRoute, $this->redirect_complete_route)){
//                    return redirect()->route('customer-account-complete');
//                }else if($redirectRoute){
//                    if($redirectURL){
//                        return redirect($redirectURL);
//                    }
//                    return redirect()->route($redirectRoute);
//                }

                return redirect()->route('customer-account-create-done');
            }
        }

    }

}