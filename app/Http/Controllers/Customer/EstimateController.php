<?php
namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Components\View\SimpleComponent;
use Ecommerce\Service\EstimateService;
use Ecommerce\Service\CartService;

class EstimateController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心',$component->base_url);
    }

    public function index(EstimateService $service)
    {
        $this->view_component->carts = $service->getItems();
        return view('response.pages.customer.estimate.index', (array)$this->view_component);
    }

    public function estimate(EstimateService $service)
    {
        $estimate = $service->estimateGeneral();
        if ($estimate->error_message == '') {
            $estimate_code = $estimate->estimate_id;
            $service->clear();
        } else {
            \Log::error($estimate->error_message );
            return redirect()->back()->withErrors(['伺服器忙碌中...請稍後重新再試一次。']);
        }
        session()->flash('code', $estimate_code);

        return redirect()->route('customer-estimate-done');
    }
    
    public function update(EstimateService $service )
    {
        $service->updateItem(request()->get('sku'), request()->get('qty'));
        return redirect()->route('customer-estimate');
    }

    public function remove(EstimateService $service )
    {
        $service->removeItem(request()->get('code'));
        return redirect()->back();
    }



    public function move(CartService $cartService , EstimateService $service )
    {
        //remove from wishlist
        $service->removeItem(request()->get('code'));
        return redirect()->back();
    }

    public function done()
    {
        if(!session()->has('code')){
            return abort(404);
        }
        $this->view_component->setBreadcrumbs('查詢完成(' . session('code') . ')');
        $this->view_component->seo('title', '【見積系統】查詢完成');
        $this->view_component->render();
        return view('response.pages.customer.estimate.done', (array)$this->view_component);
    }

  
    



}