<?php

namespace App\Http\Controllers;

use App\Components\View\SimpleComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\GiftService;
use Ecommerce\Service\Promotion\PromoService;
use Ecommerce\Service\PromotionService;
use Everglory\Constants\CustomerRole;
use Everglory\Models\Category;
use Everglory\Models\Customer\Guess as Customer_Guess;
use Everglory\Models\Customer\Mission as Customer_Mission;
use Everglory\Models\Customer\Reward as Customer_Reward;
use Everglory\Models\Gifts;
use Everglory\Models\Motor;
use Everglory\Models\Mptt;
use Everglory\Models\Order;
use Everglory\Models\Order\Reward as Order_Reward;
use Everglory\Models\Guess;
use Everglory\Models\Mission;
use Everglory\Models\Product;
use Everglory\Models\Promotion;
use Everglory\Models\Promotion\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App;
use Ecommerce\Service\SummaryService;
use Ecommerce\Service\Search\ConvertService;
use Illuminate\Http\Request as Requests;
use Ecommerce\Service\SearchService;
use Ecommerce\Repository\ManufacturerRepository;
use Illuminate\Support\Facades\DB;
use Everglory\Models\Manufacturer;
use Ecommerce\Service\CouponService;
use Ecommerce\Service\MitumoriService;
class BenefitController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('benefit');
        $component->setBreadcrumbs('會員好康', $component->base_url);

        $component->is_phone = false;
        if(!\Ecommerce\Core\Agent::isNotPhone()){
            $component->is_phone = true;
        }
    }

    public function getIndex()
    {
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $this->view_component->seo('title', '天天有回饋! 周周有好康! 免費加入會員A好康!');
        $this->view_component->seo('description', '全台最大摩托百貨，每周都會推出一連串會員限定優惠活動，不論是折扣特賣、現金點數回饋加倍、COUPON折價券、或是免費點數集點活動...每周最新訊息與優惠活動也請詳閱會員電子報!');
        $this->view_component->seo('keywords', 'honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 精品, 騎士用品, 人身部品, 特惠, 特賣, 折扣, 打折, 促銷, 優惠活動');
        $this->view_component->render();
        return view('response.pages.benefit.index', (array)$this->view_component);
    }

    public function getWebikeguess()
    {
        $route_name = \Route::currentRouteName();
        $session_redirect_key = 'register-complete-redirect-route';
        if (!$this->view_component->current_customer or ($this->view_component->current_customer and $this->view_component->current_customer->role_id == CustomerRole::REGISTERED)) {
            session([$session_redirect_key => $route_name]);
        } else {
            session()->forget($session_redirect_key);
        }


        $calender_start = '2018-10-01';
        $calender_end = '2018-10-31';


        // $guesses = \Guess::where('date', '>=', $calender_start)->where('date', '<=', $calender_end)->where('type_id', 1)->orderBy('date')->get();
        // dd(Auth::user()->id);
        $this->view_component->today = date("Y-m-d");
        $this->view_component->photo_path = 'http://img.webike.tw/assets/images/user_upload/WebikeGuess/';
        $this->view_component->photo_path1 = '/image/benefit/event/WebikeGuess/';
        $this->view_component->tail = ' - 「Webike-摩托百貨」';

        $this->view_component->calender_start = $calender_start;
        $this->view_component->calender_end = $calender_end;
        $this->view_component->guesses = Guess::where('date', '>=', $calender_start)->where('date', '<=', $calender_end)->where('type_id', 1)->orderBy('date')->get();
        $this->view_component->question = Guess::where('date', date('Y-m-d'))->where('type_id', 1)->first();
        if ($this->view_component->question) {
            if (Auth::check()) {
                $this->view_component->customer_guess = Customer_Guess::where('customer_id', Auth::user()->id)->where('guess_id', $this->view_component->question->id)->first();

                if ($this->view_component->customer_guess != NULL) {
                    $this->view_component->customer_mission = Customer_Mission::where('customer_id', Auth::user()->id)->where('mission_id', $this->view_component->question->mission_id)->first();
                }
            }
        }
        $this->view_component->finish = 'true';
        $this->view_component->setBreadcrumbs('WebikeGuess');
        $this->view_component->seo('title', 'WebikeGuess');
        $this->view_component->seo('description', '您可以在下方的【今日任務】區域中看到一張圖片，圖片為每日的任務題目，並且有數字1~9組成的九宮格區塊，點選【START】按鈕即可開始撥放，圖片中將會有某個區塊會產生變化，請點選您找到變化的地方並按下送出即可。※您可以選擇ON或OFF來開啟或關閉九宮格區塊的虛線顯示。');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.event.webikeguess', (array)$this->view_component);
    }

    public function postWebikeguess($type_id)
    {
        switch ($type_id) {
            case 1:
                $type = 'WebikeGuess';
                break;
            case 2:
                $type = 'WebikeWanted';
                break;

            // default:
            //     # code...
            //     break;
        }
        if (!Auth::check()) {
            return \Redirect::guest('login');
        }
        $today = date('Y-m-d');
        $date = date('Y-m');
        if (Request::get('ans_date') > $today) {
            if ($type_id == 1) {
                return redirect()->route('benefit-event-webikeguess-get')->with('msg', Request::get('ans_date') . '回答時間已截止。');
            } else {
                return redirect()->route('benefit-event-webikewanted')->with('msg', Request::get('ans_date') . '回答時間已截止。');
            }

        }
        $customer = Auth::user();
        $guess = Guess::where('date', '=', $today)->where('type_id', $type_id)->first();
        $customer_answers = Request::get('customer_ans');
        $customer_guess = Customer_Guess::where('customer_id', $customer->id)->where('guess_id', $guess->id)->first();
        if (!$customer_guess) {
            $customer_guess = new Customer_Guess;
            $customer_guess->customer_id = $customer->id;
            $customer_guess->guess_id = $guess->id;
            $customer_guess->answer = $customer_answers . ',';
        }
        $customer_guess->save();
        $answer = $guess->answer;

        if ($answer == $customer_answers) {

            $mission = Mission::where('id', $guess->mission_id)->first();
            $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where('mission_id', $guess->mission_id)->first();

            if (!$customer_mission) {
                $customer_mission = new Customer_Mission;
                $customer_mission->customer_id = $customer->id;
                $customer_mission->mission_id = $guess->mission_id;
                $customer_mission->reward_status = 0;
            }


            if ($customer_mission->reward_status == 0) {
                $this->givePoint($customer, $mission->name, 10);
            }

            $customer_mission->reward_status = 1;
            $customer_mission->save();

            $dayOfMonth = date('t');
            $month_start = '2019-03-28';
            $month_end = '2019-05-02';
            if ($today == $month_end) {
                $verify = Mission::where('name', 'like', $date . '%')->where('mode', 'numberLimit')->where('category', $type)->first();

                if ($verify != NULL) {
                    $user_ans_count = count(Guess::join('eg_zero.customer_mission as cm', 'guess.mission_id', '=', 'cm.mission_id')->where('cm.customer_id', $customer->id)->where('guess.type_id', $type_id)->whereBetween('guess.date', array($month_start, $month_end))->get());
                    $total_ans_count = count(Guess::where('type_id', $type_id)->whereBetween('date', array($month_start, $month_end))->get());
                    // $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where( 'mission_id', $month_mission->id )->first(); 

                    if ($user_ans_count == $total_ans_count) {
                        $customer_month_mission = Customer_Mission::where('customer_id', $customer->id)->where('mission_id', $verify->id)->first();

                        if (!$customer_month_mission) {
                            $customer_month_mission = new Customer_Mission;
                            $customer_month_mission->customer_id = $customer->id;
                            $customer_month_mission->mission_id = $verify->id;
                            $customer_month_mission->reward_status = 0;
                        }

                        if ($customer_month_mission->reward_status == 0) {
                            $this->givePoint($customer, $verify->name, 200);
                        }

                        $customer_month_mission->reward_status = 1;
                        $customer_month_mission->save();
                    }
                }
            }
        }
        $this->view_component->finish = 'true';

        if ($type_id == 1) {
            return redirect()->route('benefit-event-webikeguess-get');
        } else {
            return redirect()->route('benefit-event-webikewanted')->with('finish', 'true');
        }


    }

    private function givePoint($customer, $mission_name, $give)
    {
        //give point
        $point = $customer->points;
        if (!$point) {
            $point = new Customer_Reward();
            $point->customer_id = $customer->id;
            $point->points_collected = 0;
            $point->points_used = 0;
            $point->points_waiting = 0;
            $point->points_current = 0;
            $point->points_lost = 0;
        }

        $rewards = new Order_Reward();
        $rewards->customer_id = $customer->id;
        $rewards->description = $mission_name;
        $points = $give;

        $rewards->points_current = $points;
        $rewards->start_date = date("Y-m-d");
        $rewards->end_date = date("Y-m-d", strtotime("+1 year"));
        $rewards->save();

        $point->points_current += $points;
        $point->points_collected += $points;
        $point->save();
    }

    public function getMonthPromotion($code)
    {
        $promotion = Promotion::with(['items', 'items.type'])->where('promotions.url_rewrite', $code)->first();

        if (!$promotion) {
            abort(404);
        }
        $type_group = $promotion->items->groupby('type_id');
        $types = Type::where('is_anchor', 1)->get();

        $types = $types->filter(function ($type) use ($type_group) {
            foreach ($type_group as $type_id => $items) {
                if ($type->id == $type_id) {
                    return true;
                }
            }
        });
        $this->view_component->types = $types;
        $this->view_component->promotion = $promotion;
        $date = date("Y-m-d H:i:s");
        if (!$promotion || $promotion->start_at > $date || $promotion->end_at < $date) {
            return App::abort(404);
        }
        $month = date('n', strtotime($promotion->url_rewrite));
        $this->view_component->finish = 'true';
        $this->view_component->setBreadcrumbs($promotion->name);
        $this->view_component->seo('title', $month . '月精選SALE');
        $this->view_component->seo('description', '全台最大摩托百貨，每周都會推出一連串會員限定優惠活動，不論是折扣特賣、現金點數回饋加倍、COUPON折價券、或是免費點數集點活動...每周最新訊息與優惠活動也請詳閱會員電子報!');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.month-promotion', (array)$this->view_component);
    }

    public function getWebikequiz()
    {
        $today = date("Y-m-d");
        $question = Guess::with(['product', 'product.manufacturer'])->where('date', $today)->where('type_id', 3)->first();

        $calender_start = '2018-12-24';
        $calender_end = '2019-01-31';
        $guesses = Guess::with(['product', 'product.manufacturer'])->where('date', '>=', $calender_start)->where('date', '<=', $calender_end)->where('type_id', 3)->orderby('date')->get();
        $customer_guess = null;
        $customer_mission = null;
        if (Auth::check() and $question) {
            $customer_guess = Customer_Guess::where('customer_id', Auth::user()->id)->where('guess_id', $question->id)->first();
            if ($customer_guess != NULL) {
                $customer_mission = Customer_Mission::where('customer_id', Auth::user()->id)->where('mission_id', $question->mission_id)->first();
            }
        }

        $this->view_component->question = $question;
        $this->view_component->guesses = $guesses;
        $this->view_component->customer_guess = $customer_guess;
        $this->view_component->customer_mission = $customer_mission;
        $this->view_component->calender_start = $calender_start;
        $this->view_component->calender_end = $calender_end;
        $this->view_component->today = $today;
        $this->view_component->photo_path = 'https://img.webike.tw/assets/images/user_upload/WebikeGuess/';
        $this->view_component->photo_path1 = '/image/benefit/event/WebikeGuess/';

        $this->view_component->setBreadcrumbs('滑滑9宮格');
        $this->view_component->seo('title', '滑滑9宮格');
        $this->view_component->seo('description', '您可以在下方的【今日任務】區域中看到一張圖片，圖片為每日的任務題目，並且有數字1~9組成的九宮格區塊，點選【START】按鈕即可開始撥放，圖片中將會有某個區塊會產生變化，請點選您找到變化的地方並按下送出即可。※您可以選擇ON或OFF來開啟或關閉九宮格區塊的虛線顯示。');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.event.webikequiz', (array)$this->view_component);
    }

    public function postWebikequiz()
    {
        $result = new \StdClass;
        $result->success = false;
        $result->messages = array();
        $result->errors = 0;

        // $this->getGuess($today, $type_id);
        // function getGuess($today, $type_id){
        //     //do test 
        //     return $guess;
        // }
        // $test = $this->testing($guess->type_id);
        // function testing($type_id){
        //     //do test 
        //     switch ($type_id) {
        //         case 1:
        //             # code...
        //             break;
        //         case 2:
        //             # code...
        //             break;

        //         default:
        //             # code...
        //             break;
        //     }
        //     return true or false;
        // }

        // $result = new \StdClass;
        // $result->success = false;
        // $result->messages = array();
        // $result->errors = 0;

        // if($test){
        //     $point = 20;
        //     $this->reward($point);
        //     $result->messages[] = '<font color="#ff7777" style="font-size:18px;">※您已成功作答此次問題，'.$customer_guess->answer.'</font><br><font color="black" style="font-size:18px;">恭喜您獲得10點現金點數。</font><br><font color="black" style="font-size:18px;">您可以至</font><a href="http://www.webike.tw/customer/history/points" target="_blank" title="點數獲得及使用履歷{{$tail}}" style="font-size:18px;">點數獲得及使用履歷</a> <font color="black" style="font-size:18px;">確認您獲得的現金點數。</font><br>';
        // }else{
        //     $result->messages[] = '<font color="#ff7777" style="font-size:18px;">任務失敗</font><br><font color="black" style="font-size:18px;">您的作答時間超過5分鐘，無法獲得點數。您的紀錄為：'.$customer_guess->answer;'</font>';
        // }
        // $this->view_component->finish = 'true';

        // // return Request::get('aa');
        // return json_encode($result);

        if (!Auth::check()) {
            return 'false';
        }
        $today = date("Y-m-d");
        $date = date('Y-m');
        $result->msg = '';
        $dateLine = Request::get('ans_date');
        if ($dateLine > $today) {
            $result->msg = $dateLine . '回答時間已截止。';
            return json_encode($result);
        }
        $customer = Auth::user();
        $guess = Guess::where('date', '=', $today)->where('type_id', '3')->first();
        $customer_answers = Request::get('answer');
        $customer_guess = Customer_Guess::where('customer_id', $customer->id)->where('guess_id', $guess->id)->first();
        if (!$customer_guess) {
            $customer_guess = new Customer_Guess;
            $customer_guess->customer_id = $customer->id;
            $customer_guess->guess_id = $guess->id;
            $customer_guess->answer = $customer_answers . ',';
        }
        $customer_guess->save();
        $answer = base64_decode(Request::get('result'));
        if ($answer == 'true') {
            $result->messages[] = '<font color="#ff7777" style="font-size:18px;">※您已成功作答此次問題，' . $customer_guess->answer . '</font><br><font color="black" style="font-size:18px;">恭喜您獲得10點現金點數。</font><br><font color="black" style="font-size:18px;">您可以至</font><a href="http://www.webike.tw/customer/history/points" target="_blank" title="點數獲得及使用履歷{{$tail}}" style="font-size:18px;">點數獲得及使用履歷</a> <font color="black" style="font-size:18px;">確認您獲得的現金點數。</font><br>';
            $mission = Mission::where('id', $guess->mission_id)->first();
            $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where('mission_id', $guess->mission_id)->first();
            if (!$customer_mission) {
                $customer_mission = new Customer_Mission;
                $customer_mission->customer_id = $customer->id;
                $customer_mission->mission_id = $guess->mission_id;
                $customer_mission->reward_status = 0;
            }

            if ($customer_mission->reward_status == 0) {
                $this->givePoint($customer, $mission->name, 10);
            }

            $customer_mission->reward_status = 1;
            $customer_mission->save();

            $dayOfMonth = date('t');
            // $month_end = date('Y-m-t');
            // $month_start = date('Y-m-1');
            $month_start = '2018-12-28';
            $month_end = '2019-01-31';
            if ($today == $month_end) {
                $verify = Mission::where('name', 'like', $date . '%')->where('mode', 'numberLimit')->where('category', 'WebikeQuiz')->first();
                if ($verify != NULL) {
                    $customer_month_mission = Customer_Mission::where('customer_id', $customer->id)->where('mission_id', $verify->id)->first();
                    if (!$customer_month_mission) {
                        $customer_month_mission = new Customer_Mission;
                        $customer_month_mission->customer_id = $customer->id;
                        $customer_month_mission->mission_id = $verify->id;
                        $customer_month_mission->reward_status = 0;
                    }

                    $user_ans_count = count(Guess::join('eg_zero.customer_mission as cm', 'guess.mission_id', '=', 'cm.mission_id')->where('cm.customer_id', $customer->id)->where('guess.type_id', 3)->whereBetween('guess.date', array($month_start, $month_end))->get());
                    $total_ans_count = count(Guess::where('type_id', 3)->whereBetween('date', array($month_start, $month_end))->get());
                    // $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where( 'mission_id', $month_mission->id )->first();
                    if ($user_ans_count == $total_ans_count) {
                        $this->givePoint($customer, $mission->name, 200);
                    }

                    $customer_month_mission->reward_status = 1;
                    $customer_month_mission->save();
                }
            }
        } else {
            $result->messages[] = '<font color="#ff7777" style="font-size:18px;">任務失敗</font><br><font color="black" style="font-size:18px;">您的作答時間超過5分鐘，無法獲得點數。您的紀錄為：' . $customer_guess->answer;
            '</font>';
        }

        $this->view_component->finish = 'true';

        // return Request::get('aa');
        return json_encode($result);
    }

    public function getWebikewanted()
    {
        $question = Guess::with(['product', 'product.manufacturer'])->where('date', date('Y-m-d'))->where('type_id', 2)->first();
        $calender_start = '2019-03-25';
        $calender_end = '2019-05-02';
        $guesses = Guess::with(['product', 'product.manufacturer'])->where('date', '>=', $calender_start)->where('date', '<=', $calender_end)->where('type_id', 2)->orderby('date')->get();
        $customer_guess = null;
        $customer_mission = null;
        if (Auth::check() and $question) {
            $customer_guess = Customer_Guess::where('customer_id', Auth::user()->id)->where('guess_id', $question->id)->first();
            if ($customer_guess != NULL) {
                $customer_mission = Customer_Mission::where('customer_id', Auth::user()->id)->where('mission_id', $question->mission_id)->first();
            }
        }

        $this->view_component->question = $question;
        $this->view_component->guesses = $guesses;
        $this->view_component->customer_guess = $customer_guess;
        $this->view_component->customer_mission = $customer_mission;
        $this->view_component->calender_start = $calender_start;
        $this->view_component->calender_end = $calender_end;
        $this->view_component->today = date('Y-m-d');
        $this->view_component->photo_path = 'http://img.webike.tw/assets/images/user_upload/WebikeGuess/';
        $this->view_component->photo_path1 = '/image/benefit/event/WebikeGuess/';

        $this->view_component->setBreadcrumbs('WebikeWanted');
        $this->view_component->seo('title', 'WebikeWanted');
        $this->view_component->seo('description', '您可以在下方的【今日任務】區域中看到一張圖片，圖片為每日的任務題目，並且有數字1~9組成的九宮格區塊，點選【START】按鈕即可開始撥放，圖片中將會有某個區塊會產生變化，請點選您找到變化的地方並按下送出即可。※您可以選擇ON或OFF來開啟或關閉九宮格區塊的虛線顯示。');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.event.webikewanted', (array)$this->view_component);
    }

    public function postWebikewanted()
    {
        if (!Auth::check()) {
            return \Redirect::guest('login');
        }
        $today = date('Y-m-d');
        $date = date('Y-m');
        if (Request::get('ans_date') > $today) {
            return redirect()->route('benefit-event-webikewanted')->with('msg', \Request::get('ans_date') . '回答時間已截止。');
        }

        $customer = Auth::user();
        $guess = Guess::where('date', '=', $today)->where('type_id', '2')->first();
        $customer_answers = Request::get('customer_ans');
        $customer_guess = Customer_Guess::where('customer_id', $customer->id)->where('guess_id', $guess->id)->first();

        if (!$customer_guess) {
            $customer_guess = new Customer_Guess;
            $customer_guess->customer_id = $customer->id;
            $customer_guess->guess_id = $guess->id;
            $customer_guess->answer = $customer_answers . ',';
        }
        $customer_guess->save();
        $answer = $guess->answer;

        if ($answer == $customer_answers) {

            $mission = Mission::where('id', $guess->mission_id)->first();
            $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where('mission_id', $guess->mission_id)->first();
            if (!$customer_mission) {
                $customer_mission = new Customer_Mission;
                $customer_mission->customer_id = $customer->id;
                $customer_mission->mission_id = $guess->mission_id;
                $customer_mission->reward_status = 0;
            }

            if ($customer_mission->reward_status == 0) {
                $this->givePoint($customer, $mission->name, 10);
            }

            $customer_mission->reward_status = 1;
            $customer_mission->save();

            $dayOfMonth = date('t');
            $month_end = '2019-05-02';
            $month_start = '2019-03-28';
            if ($today == $month_end) {
                $verify = Mission::where('name', 'like', $date . '%')->where('mode', 'numberLimit')->where('category', 'WebikeWanted')->first();
                if ($verify != NULL) {
                    $user_ans_count = count(Guess::join('eg_zero.customer_mission as cm', 'guess.mission_id', '=', 'cm.mission_id')->where('cm.customer_id', $customer->id)->where('guess.type_id', 2)->whereBetween('guess.date', array($month_start, $month_end))->get());
                    $total_ans_count = count(Guess::where('type_id', 2)->whereBetween('date', array($month_start, $month_end))->get());
                    // $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where( 'mission_id', $month_mission->id )->first(); 
                    if ($user_ans_count == $total_ans_count) {
                        $this->givePoint($customer, $mission->name, 200);
                    }
                }
            }
        }

        return redirect()->route('benefit-event-webikewanted')->with('finish', 'true');
    }

    public function getEventOpenmail()
    {
        $this->view_component->setBreadcrumbs('週週開信拿現金點數');
        $this->view_component->seo('title', '週週開信拿現金點數');
        $this->view_component->seo('description', '會員好康新登場，週週開信拿現金點數。快速簡單三步驟，點數輕鬆入袋!');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.event.openmail', (array)$this->view_component);
    }

    public function getEventMybike()
    {
        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->seo('title', '「MyBike」登錄您的愛車');
        $this->view_component->seo('description', '為了提供給您更好的購物體驗，在本次「Webike2.0」改版我們更新了這個功能，目的為了讓您能更容易的找到您愛車對應的商品。在您登錄「Mybike」之後，我們會紀錄您車子的資料，您之後搜尋商品時可以很輕鬆的搭配「Mybike」功能快速的找到對應您愛車的商品，並且日後如有車型對應折扣活動或是相關新聞我們也會email通知您。');
        $this->view_component->seo('keywords', '對應車型, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.event.mybike', (array)$this->view_component);
    }

    public function get2017BigPromotion()
    {
        $type = '2017-fall';
//        $this->view_component->categories_discount = \Ecommerce\Service\PromotionService::getCategoriesDiscount($type);
//        $this->view_component->categories = \Ecommerce\Service\PromotionService::getCategoriesItem();
        $this->view_component->discount_category = PromotionService::getDiscountCategories($type);
        $this->view_component->discount_date = [['2017-09-29', '2017-10-07', '3000-3001-3002', '10/1~10/7 全罩安全帽'], ['2017-10-08', '2017-10-14', '1000-1311-1080', '10/8~10/14 懸吊&避震器'], ['2017-10-15', '2017-10-21', '3000-3260-1331', '10/15~10/21 車身用包包'], ['2017-10-22', '2017-10-31', '3000-3020-3021', '10/22~10/31 夾克&防摔衣']];
        $this->view_component->date = date("Y-m-d");
        $this->view_component->rel_parameter = 'rel=2017-10-2017Fall';
        $this->view_component->recommends = \Ecommerce\Repository\BigPromotionRepository::getProductRecommends($type);

        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->type = $type;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt(6);
        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->seo('title', '17th周年慶');
        $this->view_component->seo('description', '「Webike摩托百貨」每月精選品牌、分類、車型優惠實施中，選購改裝零件、人身部品正是時候，更有多項點數回饋與優惠活動:滑滑九宮格、愛用國貨點數現折、正廠零件95折、開信拿點數、1元加價購...');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2017.main.promotion', (array)$this->view_component);
    }

    public function get2017BigPromotionCategory($category)
    {
        $promotionName = '2017-fall';
        $this->view_component->categories = \Ecommerce\Repository\BigPromotionRepository::getCategories($promotionName, $category)->groupby('ca_rewrite');
        $this->view_component->discounts = \Ecommerce\Service\PromotionService::getCategoryBrandDiscounts($promotionName);
        $this->view_component->rel_parameter = 'rel=2017-10-2017Fall';

        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt(6);
        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->seo('title', '2017秋季SALE');
        $this->view_component->seo('description', '「Webike摩托百貨」每月精選品牌、分類、車型優惠實施中，選購改裝零件、人身部品正是時候，更有多項點數回饋與優惠活動:滑滑九宮格、愛用國貨點數現折、正廠零件95折、開信拿點數、1元加價購...');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2017.main.promotion-category', (array)$this->view_component);
    }

    public function get2017BigPromotionDomestic()
    {
        $this->view_component->rel_parameter = 'rel=2017-10-2017Fall';
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt(6);
        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->seo('title', '2017秋季SALE');
        $this->view_component->seo('description', '「Webike摩托百貨」每月精選品牌、分類、車型優惠實施中，選購改裝零件、人身部品正是時候，更有多項點數回饋與優惠活動:滑滑九宮格、愛用國貨點數現折、正廠零件95折、開信拿點數、1元加價購...');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2017.main.promotion-domestic', (array)$this->view_component);
    }

    public function get2017BigPromotionSpecialDiscount(Requests $request)
    {
        $this->view_component->rel_parameter = 'rel=2017-10-2017Fall';
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt(6);

        $searchService = app()->make(SearchService::class);
        $preload = ['fl' => 'special-price'];
        $request->request->add($preload);

        $search_response = $searchService->selectList($request);
        $convertService = app()->make(ConvertService::class);
        $manufacturers = \Cache::tags(['Discount'])->remember('BigPromoDiscountManufacturer', 86400, function () use ($search_response, $convertService) {

            $manufacturers = $convertService->convertFacetFieldToManufacturers($search_response);

            return $manufacturers;
        });

        $this->view_component->manufacturers = $manufacturers->groupBy(function ($item, $key) {
            $first_char = substr($item->name, 0, 1);
            if (preg_match('/[^A-Za-z0-9]/', $first_char)) // '/[^a-z\d]/i' should also work.
            {
                return '其他';
            } elseif (is_numeric($first_char)) {
                return '數字';
            } else {
                return strtoupper($first_char);
            }
        })->sortBy(function ($product, $key) {
            if ($key == '其他') {
                return 9999;
            } elseif ($key == '數字') {
                return 999;
            } else {
                return $key;
            }

        });

        $this->view_component->manufacturers_keys = $this->view_component->manufacturers->keys();

        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->seo('title', '2017秋季SALE');
        $this->view_component->seo('description', '「Webike摩托百貨」每月精選品牌、分類、車型優惠實施中，選購改裝零件、人身部品正是時候，更有多項點數回饋與優惠活動:滑滑九宮格、愛用國貨點數現折、正廠零件95折、開信拿點數、1元加價購...');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2017.main.promotion-bigDiscount', (array)$this->view_component);
    }

    public function getPointInfo()
    {
        $this->view_component->seo('title', '愛用國貨，POINT點數現折!!活動日期10/01(一)~10/30(三)');
        $this->view_component->seo('description', '凡是在「Webike-摩托百貨」購物網站內購買商品，皆可獲得「點數」回饋，一般點數獲得需要等到訂單流程結束後才會將點數發送至您的帳號內，「點數現折」意思即為在您將商品加入購物車後，直接將目前商品的點數直接在購物車中馬上折抵，不會進行累積。');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.pointinfo', (array)$this->view_component);
    }

    public function getFatherDay(Requests $request)
    {
        $year = '2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getFatherDayPromo($year);
        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $category_id = '4';
        $products = $promotion->getFatherDayProduct($category_id);
        
        $this->view_component = $promotion->promotion();
        $this->view_component->products = $products;
        return view('response.pages.benefit.sale.project.2018.fatherday.fatherday', (array)$this->view_component);
    }

    public function getProductByCategory(Requests $request)
    {
        $category_id = request()->get('category_id');
        $year = '2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getFatherDayPromo($year);
        $promotion->setViewComponent($this->view_component);
        $products = $promotion->getFatherDayProduct($category_id);


        $this->view_component->products = $products;

        $html = view('response.pages.benefit.sale.project.2018.fatherday.father_choose_product', (array)$this->view_component)->render();

        return json_encode(['html' => $html]);
    }

    public function getDouble11()
    {
        $customer = \Auth::user();
        $date = date('Y-m-d');
        $my_gift = null;
        $this->view_component->customer_finish = null;
        $this->view_component->game_active_result = null;
        $this->view_component->reward_result = null;

        if ( (date('Y-m-d') >= '2018-11-08') and (date('Y-m-d') <= '2018-11-11')) {
                $service = new GiftService;
                $activity = '2018double11';
                $activity_rewards =  $service->getGiftByActivity($activity);
                $this->view_component->reward_lists = $service->rewardList($activity_rewards);
            if($customer){
                $customer_not_reward_data = GiftService::getActiveCustomerGift(GiftService::NOT_REWARD, null, $date);         
                $customer_reward = $service->getTodayGift();
                $game_active_array = [1 => 350, 2 => 310, 3 => 275, 4 => 240, 5 => 200, 6 => 160, 7 => 120, 8 => 80, 9 => 50, 10 => 20];
                if (!count($customer_not_reward_data) and !$customer_reward) {
                    $gift = $service->getRandomGifts($activity,date('Y-m-d'));
                    $service->InsertCustomerGift($gift, '2018-12-01');
                    $my_gift_sort = $gift->sort;
                    $this->view_component->game_active_result = $game_active_array[$my_gift_sort];

                    $this->view_component->reward_result = $my_gift_sort;
                    $this->view_component->reward = $gift;
                } elseif (count($customer_not_reward_data)) {
                    $customer_gift = $customer_not_reward_data->first();
                    $my_gift_sort = $customer_gift->gift->sort;

                    $this->view_component->game_active_result = $game_active_array[$my_gift_sort];
                    $this->view_component->reward_result = $my_gift_sort;
                    $this->view_component->reward = $customer_gift->gift;
                } else {
                    $this->view_component->reward = $customer_reward->gift;
                    $this->view_component->customer_finish = true;
                }
            }
        } else {
            return redirect()->route('home');
        }

        if ($customer) {
            $this->view_component->reward_point = true;
            if ($this->view_component->reward->type == GiftService::TYPES['free_product']) {
                $this->view_component->reward_point = false;
            }
        }

        $this->view_component->customer = $customer;

        $this->view_component->setBreadcrumbs('2018 1111購物節');
        $this->view_component->seo('title', '1111購物節，111元禮券馬上拿，1111現金點數天天抽');
        $this->view_component->seo('description', '限時111元現金禮券馬上拿、每日輪盤中獎率100%，最大獎1111元現金點數。更多優惠好康都在Webike台灣。');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2017.1111promo.1111promo', (array)$this->view_component);

    }


    public function createGift()
    {
        $giftService = new GiftService();
        $customerGift = $giftService->implementCustomerGiftProduct();

        if ($num = request()->get('_num')) {
            $giftService->updateNewYearLocation($num);
        }

        $result = getBasicAjaxObject();
        if ($customerGift) {
            $result->success = true;
        } else {
            $result->success = false;
        }

        return json_encode($result);
    }

    public function get2018Christmas()
    {


        $this->view_component->setBreadcrumbs('2018Christmas');
        $this->view_component->seo('title', 'Webike耶誕騎機，兩日限定！全館免運費！');
        $this->view_component->seo('description', 'HO~HO~HO，耶誕老公公送貨，兩日限定，全館免運費！Webike 2018 聖誕節，為你畫下今年完美句點！');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2017.2017Christmas.2017Christmas', (array)$this->view_component);
    }

    public function get2018BigPromotion(Requests $request)
    {
        request()->request->add(['manufacturer' => 't10']);

        $promo_name = 'Fall2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);
        
        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $this->view_component = $promotion->promotion();

        
        $manufacturerUrl_rewrite = "635";
       
        $ca_id = ["290","292","305","321","373","1597"];

        $promoProducts = $promotion->getFallProduct($manufacturerUrl_rewrite,$ca_id);

        $specialManufacturerUrl_rewrite = 541;

        $specialProducts = $promotion->getSpecialPriceProduct($specialManufacturerUrl_rewrite);

        $this->view_component->riderProducts = $promoProducts;
        $this->view_component->specialProducts = $specialProducts;
        // $sku_datas = $this->getSkuData();

        // $allSku = [];
        // $sku = [];
        // foreach ($sku_datas as $sku_data) {
        //     foreach ($sku_data as $s2) {
        //         foreach ($s2 as $s3) {
        //             $allSku[] = $s3;
        //         }

        //     }
        // }
        // $sku_random = array_rand($allSku, 10);
        // foreach ($sku_random as $key) {
        //     $sku[] = $allSku[$key];
        // }

        // $new_products = [];
        // $product_data = ProductRepository::findDetail($sku, false, 'url_rewrite', ['prices', 'manufacturer', 'images', 'points']);
        // foreach ($product_data as $key => $product) {
        //     $product_accessor = new ProductAccessor($product);
        //     $new_products[] = $product_accessor;
        // }
        // $this->view_component->new_products = $new_products;
        // dd($product_data);


        return view('response.pages.benefit.sale.project.2018.spring.main.promotion', (array)$this->view_component);
    }

    protected function getSkuData()
    {
        $sku_data = [
            635 =>
                [
                    '3000-3020-3021' => ['23643839', '23643802', '23643805', '23643810', '23643815', '23643821', '23643827', '23643831', '23643833', '23643837', '23643840', '23643846', '23643852', '23643858', '23643864', '23643869', '23643872', '23643877', '23643882', '23643942', '23643947', '23643952', '23643954', '23643959', '23643961', '23643963', '23643968', '23643970', '23643975', '23643981', '23643987', '23643885', '23643894', '23643898', '23643907', '23643916', '23643923', '23643929', '23643936',],
                    '3000-3020-3060' => ['23644063', '23644069', '23644075', '23644036', '23644029', '23644002', '23644007', '23644012', '23644019', '23644024', '23644031', '23644038', '23644043', '23644048', '23644053', '23644058', '23644064', '23644070', '23644076', '23644081', '23644086', '23644091', '23644095', '23644099', '23644103', '23644107', '23644112', '23644117', '23644122', '23644127', '23644132', '23644137', '23644141', '23644146', '23644151', '23644155', '23644160', '23644166', '23644168', '23644174', '23644180', '23644186', '23644188', '23644194', '23644196', '23644202', '23644207', '23644211', '23644017'],
                    '3000-3020-3071' => ['23644358', '23644361', '23644372', '23644379', '23644390', '23644401', '23644368', '23644386', '23644397',],
                    '3000-3020-3024' => ['23644333', '23644338', '23644343', '23644348', '23644353', '23644215', '23644220', '23644225', '23644227', '23644232', '23644237', '23644239', '23644244', '23644250', '23644256', '23644262', '23644268', '23644274', '23644280', '23644306', '23644311', '23644316', '23644321', '23644326', '23644329', '23644334', '23644339', '23644344', '23644349'],
                    '3000-3020-3034' => ['23644286', '23644291', '23644296', '23644301'],
                    'other' => ['23644354', '23643993', '23644355'],
                ],
            2162 =>
                [
                    '3000-3020-3021' => ['23653985', '23653992', '23653999', '23653829', '23653834', '23653840', '23653846', '23653852', '23653858', '23653864', '23653868', '23653872', '23653878', '23653884', '23653890', '23653890', '23653895', '23653895', '23653900', '23653900', '23653905', '23653911', '23653917', '23653931', '23653937', '23653943', '23653949', '23653955', '23653961', '23653967', '23653973', '23653979', '23653986', '23653993', '23654000', '23654006', '23654014', '23654022', '23654030', '23654038', '23654044', '23654050', '23654055', '23654060', '23654064', '23654068', '23654073', '23654078', '23654095', '23654101', '23654107', '23654113', '23654119', '23654320', '23654325', '2365827'],
                    '3000-3020-3060' => ['23654179', '23654183', '23654187', '23654191', '23654197', '23654203', '23654209', '23654213', '23654217', '23654221', '23654227', '23654233', '23654239', '23654245', '23654251', '23654257', '23654263', '23654269', '23654275', '23654281', '23654287', '23654293', '23654299', '23654305', '23654310', '23654315', '23654395', '23654399', '23654403'],
                    '3000-3020-3071' => ['23654131', '23654141', '23654125', '23654132', '23654135', '23654138', '23654142', '23654145', '23654151', '23654159', '23654167', '23654173', '23654425',],
                    '3000-3020-3024' => ['23654332', '23654339', '23654346', '23654330', '23654337', '23654344'],
                    '3000-3020-3034' => ['23664000', '23664001', '23654363', '23654366', '23654371', '23654376', '23654369', '23654374',],
                    'other' => ['23654379', '23654380', '23654381', '23654433', '23654434', '23654351', '23654357', '23654387', '23654391', '23654407', '23654413', '23654419',]

                ],
            1418 =>
                [
                    '3000-3020-3021' => ['23643590', '23643592', '23643594', '23643596', '23643598', '23643600', '23643606', '23643611', '23643616', '23643621', '23643626', '23643630', '23643634', '23643638', '23643642', '23643646', '23643650', '23643654', '23643658', '23643662', '23643666', '23643670', '23643674', '23643690', '23643694', '23643698', '23643702', '23643706', '23643710', '23643774', '23643778', '23643782', '23643786', '23643790', '23643794'],
                    '3000-3020-3060' => ['23643602', '23643603', '23643604', '23643605', '23643750', '23643754', '23643758', '23643762', '23643766', '23643770',],
                    '3000-3020-3034' => ['23643678', '23643682', '23643686', '23643714', '23643718', '23643722', '23643726', '23643730', '23643734', '23643738', '23643742', '23643746',],
                    'other' => ['23643798', '23643799', '23643800', '23643801'],

                ],
            1375 => [
                '3000-3020-3021' => ['23635733', '23663715', '23663719', '23663724', '23663729', '23663732', '23663735', '23663738', '23663741', '23663746', '23663749', '23663752', '23663755', '23663759', '23663764', '23663769', '23663772', '23663775', '23663778', '23663721', '23663726', '23663743', '23663761', '23663766', '23663855', '23663860', '23663863', '23663868', '23663853', '23663858', '23663866',],
                '3000-3020-3060' => ['23663826', '23663830', '23663834', '23663838', '23663841', '23663844', '23663847', '23663850', '23663871', '23663875', '23663879', '23663882'],
                '3000-3020-3071' => ['23663802', '23663808', '23663811', '23663814', '23663817', '23663820', '23663823', '23663804'],
                'other' => ['23663781', '23663784', '23663787', '23663790', '23663793', '23663796', '23663799',],

            ],
            211 =>
                [
                    '3000-3020-3021' => ['23635833', '23702724', '23702729', '23702734', '23702739', '23702018', '23702021', '23702024', '23702762', '23702768', '23702027', '23702030', '23702033', '23702036', '23702039', '23702043', '23702046', '23702816', '23702822', '23702050', '23702834', '23702052', '23702056',],
                    '3000-3020-3060' => ['23702066', '23702899', '23702071', '23702905', '23702073', '23702911', '23702075', '23702077', '23702079', '23702926', '23702930', '23702933', '23702936', '23702939', '23702081', '23702083', '23702951', '23702063', '23702068', '23702085',],
                    '3000-3020-3071' => ['23702060', '23702859',],
                    'other' => ['23702866', '23702872', '23702878', '23702884'],

                ],
        ];

        return $sku_data;
    }

    public function getCategory(Requests $request)
    {

        $br = $request->input('brand');
        $category = [
            '635' => ['3000-3020-3021' => '夾克・防摔衣', '3000-3020-3060' => '手套', '3000-3020-3071' => '防摔褲', '3000-3020-3024' => '內穿服', '3000-3020-3034' => 'T恤'],
            '2162' => ['3000-3020-3021' => '夾克・防摔衣', '3000-3020-3060' => '手套', '3000-3020-3071' => '防摔褲', '3000-3020-3024' => '內穿服', '3000-3020-3034' => 'T恤'],
            '1418' => ['3000-3020-3021' => '夾克・防摔衣', '3000-3020-3060' => '手套', '3000-3020-3034' => 'T恤'],
            '1375' => ['3000-3020-3021' => '夾克・防摔衣', '3000-3020-3060' => '手套', '3000-3020-3071' => '防摔褲'],
            '211' => ['3000-3020-3021' => '夾克・防摔衣', '3000-3020-3060' => '手套', '3000-3020-3071' => '防摔褲']
        ];

        return response()->json($category[$br]);

    }

    public function getItem(Requests $request)
    {
        $br = $request->input('br');
        $ca = $request->input('ca');

        $sku_data = $this->getSkuData();


        $sku = [];
        if ($br && $ca) {
            if (isset($sku_data[$br][$ca]) and count($sku_data[$br][$ca]) >= 10) {
                $sku_random = array_rand($sku_data[$br][$ca], 10);
                foreach ($sku_random as $sku_key) {
                    $sku[] = $sku_data[$br][$ca][$sku_key];
                }
            } elseif (count($sku_data[$br][$ca]) < 10) {
                $sku = $sku_data[$br][$ca];
            }
        }

        $products = [];
        $product_data = ProductRepository::findDetail($sku, false, 'url_rewrite', ['prices', 'manufacturer', 'images', 'points']);
        foreach ($product_data as $key => $product) {
            $product_accessor = new ProductAccessor($product);
            $products[] = $product_accessor;
        }

        $result = new \StdClass;
        $result->products = $products;
        $result->ca_url_rewrite = $ca;
        $result->br_url_rewrite = $br;

        $this->view_component->new_products  = $products;
        $this->view_component->rel_parameter = 'rel=2018-03-2018Spring';
        $html = view('response.pages.benefit.sale.project.2018.spring.otherblock.new_spring_product', (array)$this->view_component)->render();
        return json_encode(['html' => $html]);
    }

    public function get2018BigPromotionCategory($category)
    {
        $promo_name = 'Fall2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);
        $promotion->setViewComponent($this->view_component);
        $promotion->setCategory($category);
        $this->view_component = $promotion->promotionCategory();

        return view('response.pages.benefit.sale.project.2018.spring.main.promotion-category', (array)$this->view_component);
    }

    public function get2018BigPromotionDomestic()
    {
        $promo_name = 'Summer2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);
        $promotion->setViewComponent($this->view_component);
        $this->view_component = $promotion->promotionDomestic();

        return view('response.pages.benefit.sale.project.2018.spring.main.promotion-domestic', (array)$this->view_component);
    }

    public function get2018BigPromotionSpecialDiscount(Requests $request)
    {
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt(6);

        $searchService = app()->make(SearchService::class);
        $preload = ['fl' => 'special-price'];
        $request->request->add($preload);

        $search_response = $searchService->selectList($request);
        $convertService = app()->make(ConvertService::class);
        $manufacturers = \Cache::tags(['Discount'])->remember('BigPromoDiscountManufacturer', 86400, function () use ($search_response, $convertService) {

            $manufacturers = $convertService->convertFacetFieldToManufacturers($search_response);

            return $manufacturers;
        });

        $this->view_component->manufacturers = $manufacturers->groupBy(function ($item, $key) {
            $first_char = substr($item->name, 0, 1);
            if (preg_match('/[^A-Za-z0-9]/', $first_char)) // '/[^a-z\d]/i' should also work.
            {
                return '其他';
            } elseif (is_numeric($first_char)) {
                return '數字';
            } else {
                return strtoupper($first_char);
            }
        })->sortBy(function ($product, $key) {
            if ($key == '其他') {
                return 9999;
            } elseif ($key == '數字') {
                return 999;
            } else {
                return $key;
            }

        });

        $this->view_component->manufacturers_keys = $this->view_component->manufacturers->keys();


        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->seo('title', '2018 New Year SALE');
        $this->view_component->seo('description', '「Webike 2018 New Year SALE」NOW ON ! 全館優惠實施中，熱銷商品總整理，是全新一年選購改裝零件、人身部品的最佳時候，更有猜猜哪裡不一樣？遊戲拿點數、愛用國貨點數現折、正廠零件95折...多項好康等著你！');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.benefit.sale.project.2018.spring.main.promotion-bigDiscount', (array)$this->view_component);
    }

    public function get2018BigPromotionRanking(Requests $request)
    {
        \Debugbar::disable();
        if (is_numeric($request->input('root'))) {
            $root = 'category';
        } else {
            $root = 'motor';
        }

        $request->request->add([$root => $request->input('value'), 'sort' => 'year', 'limit' => 12]);
        $cache_key = http_build_query(request()->all());

        $this->view_component->ranking_products = \Cache::tags(['summary'])->remember('promotion-ranking-' . $cache_key, 86400 * 7, function () use ($request) {
            $searchService = app()->make(SearchService::class);
            $convertService = app()->make(ConvertService::class);
            $search_response = $searchService->selectRanking($request);
            return $convertService->convertQueryResponseToProducts($search_response);
        });

        $html = view('response.pages.benefit.sale.project.2018.spring.otherblock.category-ranking-content', (array)$this->view_component)->render();
        return json_encode(['html' => $html]);
    }

    public function getProductByBrand(Requests $request)
    {
        $brand_url_rewrite = request()->get('brand_url_rewrite');
        $request->request->add(['manufacturer' => $brand_url_rewrite, 'limit' => 20]);
        $convertService = app()->make(ConvertService::class);
        $search_service = app()->make(SearchService::class);
        $ranking_response_sales = $search_service->selectRanking($request);
        $products = $convertService->convertQueryResponseToProducts($ranking_response_sales);
        $skus = [];
        foreach ($products as $product) {
            $skus[] = $product->sku;
        }

        $request->request->replace(['manufacturer' => $brand_url_rewrite, 'limit' => 12]);
        $service = app()->make(SearchService::class);
        $search_response = $service->selectList($request);
        $coverProducts = $convertService->convertQueryResponseToProducts($search_response);
        $coverProducts = $coverProducts->filter(function ($coverProduct) use ($skus) {
            if (!in_array($coverProduct->sku, $skus)) {
                return true;
            }
        });
        $products = $products->merge($coverProducts);
        $this->view_component->products = $products;
        $this->view_component->rel_parameter = 'rel=2018-07-2018Summer';

        $html = view('response.pages.benefit.sale.project.2018.spring.otherblock.country_discount_product', (array)$this->view_component)->render();

        return json_encode(['html' => $html]);
    }

    public function getProductByBrandAndCategory(Requests $request)
    {
        $brand_url_rewrite = request()->get('brand_url_rewrite');

        $ca_id = ["290","292","305","321","373","1597"];

        $promo_name = 'Fall2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);
        $promotion->setViewComponent($this->view_component);

        $riderProducts = $promotion->getFallProduct($brand_url_rewrite,$ca_id);

        $this->view_component->riderProducts = $riderProducts;
        $this->view_component->rel_parameter = 'rel=2018-10-2018Fall';

        $html = view('response.pages.benefit.sale.project.2018.spring.otherblock.summer_ride_product', (array)$this->view_component)->render();

        return json_encode(['html' => $html]);

    }
    public function getSpecialPrice(Requests $request)
    {
        $brand_url_rewrite = request()->get('brand_url_rewrite');


        $promo_name = 'Fall2018';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);
        $promotion->setViewComponent($this->view_component);

        $Products = $promotion->getSpecialPriceProduct($brand_url_rewrite);

        $this->view_component->specialProducts = $Products;
        $this->view_component->rel_parameter = 'rel=2018-10-2018Fall';

        $html = view('response.pages.benefit.sale.project.2018.spring.otherblock.special_price_product', (array)$this->view_component)->render();

        return json_encode(['html' => $html]);

    }

    public function getNewYear()
    {
        $date = date('Y-m-d');
        if ($date >= '2018-02-21') {
            return redirect()->route('home');
        }
        $customer = null;
        if (\Auth::check()) {
            $customer = \Auth::user();
        }
        $route_name = \Route::currentRouteName();
        $session_redirect_key = 'register-complete-redirect-route';
        $session_url_key = 'register-complete-redirect-url';
        if (!$this->view_component->current_customer or ($this->view_component->current_customer and $this->view_component->current_customer->role_id == CustomerRole::REGISTERED)) {
            session([$session_redirect_key => $route_name]);
            session([$session_url_key => \URL::route('benefit-sale-newyear', ['year' => '2018'])]);
        } else {
            session()->forget($session_redirect_key);
        }

        $my_gift = null;
        $this->view_component->activity_open = false;
        $this->view_component->reward = null;
        $this->view_component->customer_finish = null;
        $this->view_component->game_active_result = null;
        $this->view_component->reward_result = null;
        $this->view_component->location = null;
        $this->view_component->customer_gifts_data = null;
        $this->view_component->customer_not_reward_data = null;
        $days = ['2018-02-15', '2018-02-16', '2018-02-17', '2018-02-18', '2018-02-19', '2018-02-20'];
        $this->view_component->days = $days;
        $this->view_component->days = $days;
        $customer_gifts_data = [];

        if ($date >= '2018-02-15') {
            $this->view_component->activity_open = true;
        }

        if ($this->view_component->activity_open and $customer) {
            $service = new GiftService;
            $customer_not_reward_data = GiftService::getActiveCustomerGift(GiftService::NOT_REWARD, null, $date);
            $customer_reward = $service->getTodayGift();
            $customerGifts = $service->getCustomerNewYearGifts();
            $this->view_component->customer_not_reward_data = $customer_not_reward_data;
            foreach ($customerGifts as $customer_gift) {
                $customer_gifts_data[$customer_gift->date] = $customer_gift->name;
            }

            if (!count($customer_not_reward_data) and !$customer_reward) {
                $activity = '2018newyear';
                $gift = $service->getRandomGifts($activity);
                $service->InsertCustomerGift($gift, '2018-04-01');
                $my_gift_sort = $gift->sort;

                $this->view_component->reward_result = $my_gift_sort;
                $this->view_component->reward = $gift;
            } elseif (count($customer_not_reward_data)) {
                $customer_gift = $customer_not_reward_data->first();
                $my_gift_sort = $customer_gift->gift->sort;

                $this->view_component->reward_result = $my_gift_sort;
                $this->view_component->reward = $customer_gift->gift;
            } else {
                $this->view_component->reward = $customer_reward->gift;
                $this->view_component->customer_finish = true;
                $this->view_component->location = $customer_reward->location;
            }
        }

        $this->view_component->customer_gifts_data = $customer_gifts_data;

        if ($customer and $this->view_component->activity_open) {
            $this->view_component->reward_point = true;
            if ($this->view_component->reward->type == GiftService::TYPES['free_product']) {
                $this->view_component->reward_point = false;
            }
        }

        $this->view_component->customer = $customer;

        $this->view_component->setBreadcrumbs('狗年來旺，紅包天天送');
        $this->view_component->seo('title', '狗年來旺，紅包天天送 -「Webike」會員好康');
        $this->view_component->seo('description', '除夕-初五6天紅包天天送，好禮8抽1中獎機率100%，最大888元現金禮券。更多優惠好康都在Webike台灣。');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.benefit.sale.new-year.2018', (array)$this->view_component);
    }

    public function get5year(){
        if(activeValidate("2018-08-30","2018-09-30")){

            $activity = "5-th";
            $date = date('Y-m-d');
            $dates = array('2018-05-30', '2018-06-09');
            $customer = null;
            $result_text = null;
            $customer_no_login = false;
            $customer_choose = false;
            $giftService = new GiftService;
            $gifts = $giftService->getGiftByActivity($activity);
            foreach ($gifts as $key => $gift) {
                $productUrl = $giftService->getGiftProductUrl($gift->orig_product_id);
                if($productUrl){
                    $gifts[$key]->productUrl = $productUrl->url_rewrite;
                }
            }

            if (\Auth::check()) {
                $customer = \Auth::user();
            }else{
                $customer_no_login = true;
            }

            $route_name = \Route::currentRouteName();
            $session_redirect_key = 'register-complete-redirect-route';
            $session_url_key = 'register-complete-redirect-url';
            if (!$this->view_component->current_customer or ($this->view_component->current_customer and $this->view_component->current_customer->role_id == CustomerRole::REGISTERED)) {
                session([$session_redirect_key => $route_name]);
                session([$session_url_key => \URL::route('benefit-sale-5year')]);
            } else {
                session()->forget($session_redirect_key);
            }

            if($customer){
                $customer_gift = $giftService->getCustomerRewardGift($dates);
                if($customer_gift){
                    $customer_choose = true;
                }            
            }

            $this->view_component->seo('title', 'Webike在台灣5周年了，好禮直接送給您！');
            $this->view_component->seo('description', 'Webike在台灣5周年了，從2013年開站以來，始終秉持著「愛車、愛人及這片土地」的精神，往後我們將繼續提供更好的服務與功能給大家！在這特別的時刻我們準備的5項好禮要送給大家，5周年限量旺旺鑰匙圈、3M濃縮洗車精、Soft99色彩純棉毛巾、Webike Garage鍊條刷、100元折價券...等你來領取！');
            $this->view_component->customer_no_login = $customer_no_login;
            $this->view_component->customer_choose = $customer_choose;
            $this->view_component->gifts = $gifts;
            return view('response.pages.benefit.sale.anniversary.5-th.index', (array)$this->view_component);
        }else{
            return redirect()->route('home');
        }
    }

    public function addGift(Requests $request){

        $gift_id = request()->get('gift');
        if($gift_id){
            if(!Auth::check()){
                $result = ["false" => '請登入帳號'];
            }
            $date =date('Y-m-d');
            if(!activeValidate("2018-05-30","2018-06-09")){
                $result = ["false" => '不在活動時間內'];
            }else{
                $giftService = new GiftService;
                $expired_at = '2018-07-01';
                $activity = "5-th";
                $dates = array('2018-05-30', '2018-06-09');
                $result = $giftService->insertCustomerAnniversaryGift($gift_id,$expired_at,$activity,$dates);
                
            }
        }else{
            $result = ["false" => '請選擇好禮'];
        }

        return json_encode(['result' => $result]);
    }

    public function inserFatherDayCoupon(){
        $customers = \Everglory\Models\Customer::select('id')->get();
        $CouponBase = \Everglory\Models\Coupon\Base::where('code','=','2018Happyfathersday')->first();
        $CouponBase_id = $CouponBase->id;
        $expired_at = "2018-09-01 00:00:00";
        foreach ($customers as $customer) {
            $coupon_service = new CouponService(\Auth::user());
            $coupon_service->assign($CouponBase_id,$customer,$expired_at,2);
        }
    }

    public function get2019BigPromotion(Requests $request){
        
        $promo_name = 'Spring2019';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);

        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $this->view_component = $promotion->promotion();

        return view('response.pages.benefit.sale.project.2019.promotion', (array)$this->view_component);
    }

    public function get2019PromotionPreview(Requests $request){
        $promo_name = 'Spring2019';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);

        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $this->view_component = $promotion->promotionPreview();
        return view('response.pages.benefit.sale.project.2019.promotion-preview', (array)$this->view_component);
    }
    public function get2019PromotionBrand(Requests $request){
        $promo_name = 'Spring2019';
        $promo_service = new PromoService();
        $promotion = $promo_service->getSeasonPromo($promo_name);

        $promotion->setViewComponent($this->view_component);
        $promotion->setRequest($request);
        $this->view_component = $promotion->promotionBrand();
        return view('response.pages.benefit.sale.project.2019.promotion-brand', (array)$this->view_component);   
    }

    public function getWeeklyPromotion(){
        $rel_parameter = 'weekly';
        $this->view_component->rel_parameter = $rel_parameter;
        return view('response.pages.benefit.sale.weekly.promotion', (array)$this->view_component);   
    }
}

