<?php

namespace App\Http\Controllers;


use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Solr\Searcher;
use Ecommerce\Core\Wmail;
use Ecommerce\Repository\CustomerRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\Search\Suggest\CategoryBrandSuggest;
use Ecommerce\Service\Search\Suggest\CategoryMotorSuggest;
use Ecommerce\Support\DisplacementConverter;
use Everglory\Constants\CustomerEmailValidate;
use Everglory\Models\Manufacturer;
use Everglory\Models\Motor;
use Everglory\Models\MotorManufacturer;
use Illuminate\Routing\Controller as BaseController;
use Ecommerce\Core\Disp\Program as DispProgram;
use Ecommerce\Service\Search\ConvertService;
use Everglory\Models\Qa\Question;
use Everglory\Models\Qa\Source;
use Everglory\Models\Qa\Relation;
use Everglory\Models\Qa\Answer;
use Illuminate\Http\Request;

class APIController extends BaseController
{

    protected $basic;

    public function __construct()
    {
        $this->basic = new \StdClass;
        $this->basic->success = false;
        $this->basic->errors = 0;
        $this->basic->messages = [];
        $this->basic->datas = [];
        \Debugbar::disable();
    }

    public function getMotorManufacturerDisplacement()
    {
        if (request()->has('manufacturer')) {
            $motorManufacturer = MotorManufacturer::where('url_rewrite', request()->input('manufacturer'))->first();
            if ($motorManufacturer) {
                $_displacements = Motor::select('displacement')
                    ->where('manufacturer_id', $motorManufacturer->id)
                    ->groupBy('displacement')
                    ->get()
                    ->pluck('displacement');
                return response()->json($_displacements->toArray());
            }
        } else {
            return response()->json([]);
        }
    }

    public function getMotorDisplacementModel()
    {
        if (request()->has('manufacturer') and request()->has('displacement')) {
            $motorManufacturer = MotorManufacturer::where('url_rewrite', request()->input('manufacturer'))->first();
            $motors = Motor::
            select(['url_rewrite as key', 'name'])
                ->where('manufacturer_id', $motorManufacturer->id)
                ->where('displacement', request()->input('displacement'))
                ->get();

            return response()->json($motors->toArray());
        } else {
            return response()->json([]);
        }

    }

    public function getMotorManufacturerModel()
    {
        $key = 'url_rewrite';
        if (request()->has('key')) {
            $key = request()->input('key');
        }

        if (request()->has('manufacturer') and request()->has('motor_name')) {
            $motorManufacturer = MotorManufacturer::where('url_rewrite', request()->input('manufacturer'))->first();
            if ($motorManufacturer) {
                $motors = Motor::
                select([$key . ' as key', 'name'])
                    ->where('manufacturer_id', $motorManufacturer->id)
                    ->where('name', 'like', '%' . request()->input('motor_name') . '%')
                    ->get();

                return response()->json($motors->toArray());
            }
        } else if (request()->has('motor_name')) {
            $motors = Motor::
            select([$key . ' as key', 'name'])
                ->where('name', 'like', '%' . request()->input('motor_name') . '%')
                ->get();

            return response()->json($motors->toArray());
        } else {
            return response()->json([]);
        }
    }

    public function getProductManufacturerModel()
    {
        $key = 'url_rewrite';
        if (request()->has('key')) {
            $key = request()->input('key');
        }

        if (request()->has('manufacturer_name')) {
            $motors = Manufacturer::
            select([$key . ' as key', 'name'])
                ->where('name', 'like', '%' . request()->input('manufacturer_name') . '%')
                ->get();

            return response()->json($motors->toArray());
        } else {
            return response()->json([]);
        }
    }

    public function readDisp()
    {
        $dispProgram = new DispProgram;
        if ($dispProgram->deleteCustomerDisps()) {
            $this->basic->success = true;
        }

        return json_encode($this->basic);
    }

    public function countDisp()
    {
        $dispProgram = new DispProgram;
        if ($disps = $dispProgram->getCustomerDisps()) {
            $this->basic->datas = $dispProgram->getGroupCount($disps);
            $this->basic->success = true;
        }

        return json_encode($this->basic);
    }

    public function suggest()
    {
        $result = [];
        if (request()->input('q')) {
            //config(['solr.hostname' => '153.120.83.216']);
            //config(['solr.path' => 'solr/webike_suggest']);
            //config(['solr.port' => env('SOLR_SUGGEST_PORT' , 8080)]);
            $service = app()->make(\Ecommerce\Service\SearchService::class);

            $searcher = new Searcher('solr/webike_suggest', env('SOLR_SUGGEST_PORT', 8080));

            $service->setSearcher($searcher);

            $convert_service = app()->make(ConvertService::class);


            $categoryBrandSuggest = new CategoryBrandSuggest();
            $data = $categoryBrandSuggest->get(request()->get('q'));
            if ($data)
                $result = array_merge($result, $data);
            else {
                $categoryMotorSuggest = new CategoryMotorSuggest();
                $data = $categoryMotorSuggest->get(request()->get('q'));
                if ($data)
                    $result = array_merge($result, $data);
            }

            $search_response = $service->selectSuggest(request());
            if ($search_response) {
                $data = $convert_service->convertQueryResponseToSuggest($search_response, request()->input('parts'),
                    request()->input('url'));
                $result = array_merge($result, $data);
            }

        }

        return json_encode($result);
    }

    public function getStock()
    {
        $result = [];
        if ($skus = explode(',', request()->input('sku')) and count($skus)) {
            $stockInformations = ProductService::getEgZeroStockInfoByApi(['barcode' => implode('_', $skus)]);
            foreach ($skus as $sku) {
                try {
                    $product = ProductRepository::findDetail($sku, false, 'url_rewrite', []);
                    if (isset($stockInformations->{$product->sku})) {
                        $result[$product->sku] = ProductService::getStockInfo($product, $stockInformations->{$product->sku});
                    } else {
                        $result[$product->sku] = ProductService::getStockInfo($product);
                    }
                } catch (\Exception $e) {
                }
            }

//            $url = "https://www.webike.net/wbs/stock-cooperation-sd-json.html?sku=". request()->input('sku');
//            if(get_http_response_code($url) == "200"){
//                $stock_status = iconv("shift-jis", "utf-8", file_get_contents($url));
//                $result = json_decode($stock_status);
//            }
        }
        return json_encode($result);
    }

    public function testProductSkus()
    {
        if (request()->has('skus')) {
            $this->basic->success = true;
            $results = collect(request()->get('skus'));
            $skus = ProductRepository::getProductWithRelations(request()->get('skus'), '')->pluck('sku');
            $this->basic->datas = $results->filter(function ($result) use ($skus) {
                return !in_array($result, $skus);
            })->toArray();
        }
        return json_encode($this->basic);;
    }

    public function qaCenterCreate()
    {
        $product_id = request()->get('product_id');
        $question_ids = explode(',', request()->get('question'));
        $answer_ids = explode(',', request()->get('answer'));
        $ticket_id = request()->get('ticket_id');
        $ticket_category = request()->get('ticket_category');
        $increment_id = request()->get('increment_id');

        if (!($question_ids && $product_id && $answer_ids && $ticket_id && $ticket_category && $increment_id)) {
            return json_encode($this->basic);
        }

        $questions = \DB::connection('eg_zero')->table('ticket_replies')
            ->whereIN('id', $question_ids)
            ->get();
        $question_content = [];
        foreach ($questions as $question) {

            $question_content[] = $question->content;
        }
        $question_content = implode("<br>", $question_content);

        $answers = \DB::connection('eg_zero')->table('ticket_replies')
            ->whereIN('id', $answer_ids)
            ->get();

        foreach ($answers as $answers) {
            $answers_content[] = $answers->content;
        }
        $answers_content = implode("<br>", $answers_content);

        $Question = new Question;
        $Question->group_id = $ticket_category;
        $Question->content = $question_content;
        $Question->active = 0;
        $Question->save();

        $question_id = $Question->id;

        $Source = new Source;
        $Source->source_type = "MorphTicket";
        $Source->source_id = $ticket_id;
        $Source->question_id = $question_id;
        $Source->save();

        $Relation = new Relation;
        $Relation->product_id = $product_id;
        $Relation->save();

        $relation_id = $Relation->id;

        $Answer = new Answer;
        $Answer->question_id = $question_id;
        $Answer->relation_id = $relation_id;
        $Answer->content = $answers_content;
        $Answer->save();

        $this->basic->success = true;

        return json_encode($this->basic);
    }

    public function qaCenterCheck()
    {
        $product_id = request()->get('product_id');
        $ticket_id = request()->get('ticket_id');
        if (!($product_id && $ticket_id)) {
            return json_encode($this->basic);
        }

        $Source = Source::join('qa_questions as q', 'qa_sources.question_id', '=', 'q.id')->
        join('qa_answers as a', 'q.id', '=', 'a.question_id')
            ->join('qa_relations as r', 'a.relation_id', '=', 'r.id')
            ->where('source_id', $ticket_id)
            ->where('r.product_id', $product_id)
            ->first();


        if ($Source) {
            $this->basic->success = true;
        }

        return json_encode($this->basic);
    }

    public function qaCenterQadata()
    {
        $ticket_id = request()->get('ticket_id');

        if (!$ticket_id) {
            return json_encode($this->basic);
        }

        $Source = Source::select(\DB::raw('q.id as question_id ,q.content as question,q.updated_at as question_edit_day ,a.id as answer_id,a.content as answer,a.updated_at as answer_edit_day'))
            ->leftjoin('qa_questions as q', 'qa_sources.question_id', '=', 'q.id')
            ->leftjoin('qa_answers as a', 'q.id', '=', 'a.question_id')
            ->where('qa_sources.source_id', $ticket_id)
            ->where('qa_sources.source_type', 'MorphTicket')
            ->first();

        if ($Source) {
            $this->basic->success = true;
            $this->basic->Source = $Source;
        }

        return json_encode($this->basic);
    }

    public function qaCenterUpdate(Request $request)
    {

        $post_data = $request->all();

        if (!($post_data['question_id'] && $post_data['answer_id'])) {
            return json_encode($this->basic);
        }

        $Question = Question::where('id', $post_data['question_id'])->first();
        $Question->content = $post_data['question'];
        $Question->save();

        $Answer = Answer::where('id', $post_data['answer_id'])->first();
        $Answer->content = $post_data['answer'];
        $Answer->save();

        $this->basic->success = true;

        return json_encode($this->basic);
    }

    public function qaCenterUpdateQaSort(Request $request)
    {

        $data = $request->all();

        $Question = Question::whereIn('id', $data['all_ids'])->update(['active' => 0]);

        foreach ($data['open_ids'] as $sort => $question_id) {
            $Question = Question::where('id', $question_id)->first();
            $Question->sort = $sort;
            $Question->active = 1;
            $Question->save();
        }

        $this->basic->success = true;

        return json_encode($this->basic);
    }

    public function qaCenterDeleteQa(Request $request)
    {

        $question_id = request()->get('question_id');
        $answer_id = request()->get('answer_id');

        $Answer = Answer::where('id', $answer_id)->first();

        Question::where('id', $question_id)->delete();
        Source::where('question_id', $question_id)->where('source_type', 'MorphTicket')->delete();
        Relation::where('id', $Answer->relation_id)->delete();
        Answer::where('id', $answer_id)->delete();

        $this->basic->success = true;

        return json_encode($this->basic);

    }


    public function productOptions()
    {
        $sku = request()->get('sku');
        $product = ProductRepository::findDetail($sku);
        $productAccessor = new ProductAccessor($product, true);
        $options = $productAccessor->getGroupSelectsAndOptions();
        return response()->json($options, 200);
    }

    public function sendVerifyMail()
    {
        $email = null;

        if(session()->get('customer_email')){
            $email = session()->get('customer_email');
        }elseif(request()->get('customer_email')){
            $email = request()->get('customer_email');
        }

        if($email){
            $repository = new CustomerRepository;
            $customer = $repository->getCustomerByEmail($email)->firstWithRelation();

            if($customer && $customer->email_validate === CustomerEmailValidate::VALIDATE_FAILED_EMAIL){
                $wmail = new Wmail();
                $wmail->customerEmailVerify($customer);
                return "success";
            }
            return "already validate";
        }
        return "failed";

    }

}