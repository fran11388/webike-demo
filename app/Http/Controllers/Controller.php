<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Customer\ContactController;
use Ecommerce\Service\Backend\PropagandaService;
use Everglory\Constants\Propaganda\Position;
use Everglory\Constants\Propaganda\Type;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Components\View\BaseComponent;
use Ecommerce\Core\Disp\Program as dispProgram;
use Ecommerce\Service\HistoryService;
use Auth;
use Route;
use Cache;
use Ecommerce\Accessor\CustomerAccessor;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $view_component;

    public function __construct(BaseComponent $component)
    {
        $this->view_component = $component;
        $this->middleware(function( $request , $next) use(&$component){
            start_measure('render','Controller');

            if(Auth::check()){
                $current_customer = Auth::user()->load('motors');
                if(strtotime($current_customer->last_login) < strtotime(date('Y-m-d'))){
                    $current_customer->last_login = date('Y-m-d');
                    $current_customer->save();
                }
                $component->current_customer = new CustomerAccessor($current_customer);
            }else{
                $component->current_customer = null;
                if (!session()->has('first-enter') and !session()->has('invite-join')) {
                    session()->put(['first-enter' => true]);
                }else if(session()->get('first-enter') and !session()->has('invite-join')){
                    session()->put(['invite-join' => true]);
                }
            }
            stop_measure('render');
            start_measure('render','Controller2');
            if($component->current_customer){
                if($component->current_customer->blacklist){
                    Auth::logout();
                    return redirect()->route('lock');
                }
                if(!session()->has('palette')){
                    session(['palette' => \Everglory\Constants\Palette::CUSTOMER[rand(0, 9)]]);
                }
            }
            stop_measure('render');

            start_measure('render','Controller');
            $rejectDispRouteNames = ['shopping'];
            $route_name = Route::currentRouteName();
            $component->disps_active = false;
            $component->disps = $this->getDisps();
            if(!in_array($route_name, $rejectDispRouteNames)){
                $component->disps_active = true;
            }

            if(strpos($route_name, 'customer') === 0 and $component->current_customer){
                $historyService = new HistoryService;
                $component->order_ing_count = $historyService->getNotFinishOrders()->count();
            }

            $component->bar_obvious = false;
            if(activeValidate('2019-02-01 00:00:00','2020-02-15')){
                $component->bar_obvious = true;
                if($component->current_customer and in_array($component->current_customer->role_id, [\Everglory\Constants\CustomerRole::WHOLESALE]) and activeValidate('2019-02-01 00:00:00','2020-02-15')){
                    $component->bar_obvious = false;
                }
            }elseif(activeValidate('2018-12-23 00:00:00','2018-12-25') and (strpos(Route::currentRouteName(), '2018Christmas') === false)){
                $component->bar_obvious = true;
                // if($component->current_customer and in_array($component->current_customer->role_id, [\Everglory\Constants\CustomerRole::WHOLESALE])){
                //     $component->bar_obvious = false;
                // }
            }

            
            if(session()->has('pop-ups-full-screen') and session()->get('pop-ups-full-screen') !== date('Y-m-d')){
                session()->forget('pop-ups-full-screen');
            }

            stop_measure('render');
            $this->view_component->propagandas = PropagandaService::getAvailablePropagandas(); //注意擺放位置，避免 \Auth::user() ==null
            return $next($request);
        });
    }

    public function getDisps()
    {
        $program = new dispProgram;
        return $program->getCustomerDisps();
    }
}
