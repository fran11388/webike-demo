<?php
namespace App\Http\Controllers;

use Everglory\Models\Customer;
use Everglory\Models\Product;
use URL;
use Illuminate\Support\Collection;
use App\Components\View\SimpleComponent;
use Everglory\Constants\EstimateType;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;
use Ecommerce\Support\LengthAwarePaginator;
use Ecommerce\Service\AccountService;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Service\HistoryService;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\CartService;
use Ecommerce\Service\WishlistService;
use Ecommerce\Service\ContactService;
use Ecommerce\Service\GiftService;

class CustomerController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心', $component->base_url);
    }

    public function index()
    {


        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.index', (array)$this->view_component);
    }

    public function mybike(AccountService $service)
    {
        $this->view_component->motors = $service->getMyBike();
        $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();

        $this->view_component->setBreadcrumbs('我的帳號',route('customer-account'));
        $this->view_component->setBreadcrumbs('MyBike登錄');
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();

        return view('response.pages.customer.mybike.index', (array)$this->view_component);
    }

    public function addMybike($url_rewrite,AccountService $service)
    {
        $hasMyBikeComplete = \Ecommerce\Service\MissionService::hasMissionComplete(1, \Auth::user()->id);
        $service->addMybike($url_rewrite);
        if(request()->ajax()){
            $result = getBasicAjaxObject();
            if(!$hasMyBikeComplete){
                $result->messages[] = '<span class="font-color-red size-08rem">※您已完成My Bike登錄活動，獲得100點現金點數，您可以前往"<a href="'. route('customer-history-points') .'" target="_blank">點數獲得及使用履歷</a>"進行查詢。</span>';
            }
            $result->success = true;
            return json_encode($result);
        }
        return redirect()->route('customer-mybike')->with('success','Mybike 新增完成。');
    }

    public function updateMybike(AccountService $service)
    {
        $service->updateMyBike(request()->input('my-bike'));
        return redirect()->back()->with('success','Mybike 更新完成。');
    }

    public function fixer()
    {
        // both create and complete done , decide at view
        return view('response.pages.customer.fixer', (array)$this->view_component);
    }

    public function refresh()
    {
        // both create and complete done , decide at view
        return redirect()->to('login')->with(['status'=>'系統已經嘗試修復。']);
    }

    public function getRule($rule_code)
    {
        $rule = $rule_code;
        if( isset( $rule ) ){
            switch ( $rule ) {
                case 'pointinfo':
                    $breadcrumbs = '現金點數說明';
                    $title = '現金點數說明';
                    $description = 'Webike會員專屬現金集點活動，一點可扣抵一元，集越多省越多';
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    break;
                case 'member_rule_1':
                    $breadcrumbs = 'webike摩托百貨-購物流程';
                    $title = '購物流程';
                    if(hasChangedLogistic()){
                        $description = '進口重機、機車改裝零件與騎士用品百貨購物流程，簡單、方便、快速又安全，滿足您的摩托人生-【支援3、6及12分期付款】!';
                    }else{
                        $description = '進口重機、機車改裝零件與騎士用品百貨購物流程，簡單、方便、快速又安全，滿足您的摩托人生-【購物滿2千免運費】!';
                    }
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    break;
                case 'member_rule_2':
                    $breadcrumbs = '購物及服務說明';
                    $title = '購物及服務說明';
                    $description = '請您詳閱相關購物說明，保障您的購物權益';
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    if(!hasChangedLogistic()){
                        $rule = 'member_rule_2_old';
                    }
                    break;
                case 'member_rule_3':
                    $breadcrumbs = '會員條約-會員條約說明';
                    $title = '會員條約-會員條約說明';
                    $description = '請您詳閱相關會員條約說明，保障您的購物權益';
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    break;
                case 'member_rule_4':
                    $breadcrumbs = '綜合條約';
                    $title = '綜合條約';
                    $description = '請您詳閱相關綜合條約，保障您的購物權益';
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    break;
                case 'member_rule_5':
                    $breadcrumbs = '購買條約';
                    $title = '購買條約';
                    $description = '請您詳閱相關購買條約，保障您的購物權益';
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    break;
                case 'member_rule_8':
                    $breadcrumbs = '會員條約-隱私權保護政策';
                    $title = '會員條約-隱私權保護政策';
                    $description = '請您詳閱相關隱私權保護政策，保障您的購物權益';
                    $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';
                    break;
                default:
                    abort(404);

            }
        }
        $this->view_component->customer = \Auth::user();
        $this->view_component->setBreadcrumbs($breadcrumbs);
        $this->view_component->seo('title', $title);
        $this->view_component->seo('description',$description);
        $this->view_component->seo('keyword', $keywords);
        $this->view_component->render();

        return view('response.pages.customer.rule.'.$rule, (array)$this->view_component);
    }

    public function getNewsletterPoint()
    {

        if(!empty( request()->input('user') ) && !empty( request()->input('vol') ) && !empty( request()->input('AID') )){

            $customer = \Auth::user();
            $messages = '';
            $customer_id = $customer->id;
            if($customer_id == request()->input('user')){ //check customer
                $vol = request()->input('vol');
                $queue = \Everglory\Models\Email\Newsletter\Queue::find($vol);

                $code = '';

                if($queue){
                    $code = md5('Webike'.$queue->newsletter_id.'newsletter');

                }
                if($code == request()->input('AID')){ //check AID
                    $item = \Everglory\Models\Email\Newsletter\Item::where('customer_id',$customer_id)->where('queue_id',$queue->id)->first();
                    if(!empty($item->get_point)){ //repeat
                        $messages .= '親愛的'.$customer->realname.'會員您好<br/>';
                        $messages .= '對不起，您已經參加過本次的開信拿點數活動<br/>';
                        $messages .= '歡迎您持續參加開信拿點數的活動<br/>';
                        $messages .= '「Webike-摩托百貨」祝您購物愉快<br/>';
                        //$messages .= '本頁將於10秒後轉到首頁';

                    }else{ //no repeat,insert point


                        \DB::transaction(function()use( &$customer,&$item,&$messages ){
                            $point = $customer->points;
                            if ( !$point ){
                                $point = new \Everglory\Models\Customer\Reward();
                                $point->customer_id = $customer->id;
                                $point->points_collected = 0;
                                $point->points_used = 0;
                                $point->points_waiting = 0;
                                $point->points_current = 0;
                                $point->points_lost = 0;
                            }

                            $rewards = new \Everglory\Models\Order\Reward();
                            $rewards->customer_id = $customer->id;
                            $rewards->description = '電子報回饋點數';

                            //give point
                            $points = 10;
                            $rewards->points_current = $points;
                            $rewards->start_date = date("Y-m-d");
                            $rewards->end_date = date("Y-m-d",strtotime("+1 year"));
                            $rewards->status = 1;
                            $rewards->save();

                            $point->points_current += $points;
                            $point->points_collected  += $points;
                            $point->save();

                            $item->get_point = date("Y-m-d");
                            $item->save();
                            $messages .= '親愛的'.$customer->realname.'會員您好<br/>';
                            $messages .= '感謝您參加本次的開信拿點數活動<br/>';
                            $messages .= '您本次點數 10 點已經匯入您的帳號內了<br/>';
                            $messages .= '您可以至會員中心的<a href="'. \URL::route('customer-history-points') .'">「點數獲得履歷」</a>查看您的點數<br/>';
                            $messages .= '歡迎您下週持續參加開信拿點數的活動<br/>';
                            $messages .= '「Webike-摩托百貨」祝您購物愉快<br/>';
                        });
                    }

                }else{
                    //echo "即將轉址:CODE錯誤";
                    return redirect()->route('customer');
                }

            }else{
                \Auth::logout();
                return redirect()->route('login')->with('pointMessage', '請登入與信箱相符的帳號');
            }

            $this->view_component->messages = $messages;
            $this->view_component->setBreadcrumbs('開信拿點數');
            $this->view_component->seo('title', '開信拿點數');
            $this->view_component->seo('description','開信拿點數');
            $this->view_component->seo('keyword', '開信拿點數');
            $this->view_component->render();

            return view('response.pages.customer.newsletter.point',(array)$this->view_component);

        }else{
            //echo "即將轉址:錯誤!";
            return redirect()->route('customer');
        }
    }

    public function editNewsletter()
    {
        $this->view_component->setBreadcrumbs('我的帳號',route('customer-account'));
        $this->view_component->setBreadcrumbs('電子報訂閱');
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.customer.newsletter.edit',(array)$this->view_component);
    }

    public function updateNewsletter(AccountService $service)
    {
        if($service->updateNewsletter(request()->input('newsletter'))){
            return redirect()->back()->with('success','電子報訂閱 更新完成。');
        }else{
            return redirect()->back()->with('success','電子報訂閱 更新失敗！');
        }
    }
    
    
    public function getRecentView()
    {
        $result = new \stdClass();
        $result->success = false;
        $current_customer = \Auth::user();
        $recent_views = ProductService::getRecentViews($current_customer);
        $result->html = view('response.common.list.view-history-horizon',compact('current_customer','recent_views'))->render();
        $result->success = true;
        return json_encode($result);
    }

    public function info()
    {
        $this->view_component->setBreadcrumbs('線上客服諮詢');
        $this->view_component->setBreadcrumbs('客服資訊');
        $this->view_component->seo('title', '客服資訊');
        $this->view_component->render();
        return view('response.pages.customer.contact.info', (array)$this->view_component);
    }

    public function setSession($session_name){
        // Set session by API , make sure to add to case.
        switch ($session_name) {
            case 'bannerbar-read':
                session()->put($session_name, 'on');
                return 'success';
                break;
            case 'invite-join':
                session()->put([$session_name => false]);
                return 'success';
                break;
            case 'pop-ups-full-screen':
                session()->put([$session_name => date('Y-m-d')]);
                return 'success';
                break;
            case 'bannerbar-close':
                session()->put([$session_name => date('Y-m-d H:i:s',strtotime('+3 hours'))]);
                return 'success';
                break;
            default:
                \Log::warning('Not Validate Session Name');
                return 'fail';
        }
    }

    public function getHeaderCount()
    {
        $result = new \stdClass();
        $result->success = false;
        \Debugbar::disable();

        if(\Auth::check()){
            if(!session()->has('cart_count')){
                app()->make(CartService::class);
            }
            if(!session()->has('wishlist_count')){
                app()->make(WishlistService::class);
            }
            if(!session()->has('gift_count')){
                app()->make(GiftService::class);
            }
            $datas = [];
            $datas['cart'] = session()->get('cart_count') ? session()->get('cart_count') : 0;
            $datas['wishlist'] = session()->get('wishlist_count') ? session()->get('wishlist_count') : 0;
            $datas['gift'] = session()->get('gift_count') ? session()->get('gift_count') : 0;
            $contactService = app()->make(ContactService::class);
            $datas['new_message'] = count($contactService->getRepliedList());
            $result->datas = $datas;
            $result->success = true;
        }
        return json_encode($result);
    }

        public function getPointinfoAmp()
    {

            $breadcrumbs = '現金點數說明';
            $page = 'mobile.pages.customer.rule.amppointinfo';
            $title = '現金點數說明';
            $description = 'Webike會員專屬現金集點活動，一點可扣抵一元，集越多省越多';
            $keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';


        $this->view_component->customer = \Auth::user();
        $this->view_component->setBreadcrumbs($breadcrumbs);
        $this->view_component->seo('title', $title);
        $this->view_component->seo('description',$description);
        $this->view_component->seo('keyword', $keywords);
        $this->view_component->render();

        return view($page, (array)$this->view_component);
     }  

}