<?php
namespace App\Http\Controllers;

use App\Components\View\SimpleComponent;

class UcrrController extends Controller{

	public function __construct(SimpleComponent $component)
	{
		parent::__construct($component);
	}

	public function getIndex()
	{
		$this->view_component->seo('title','Webike支持贊助UCRR大專校際道路機車賽');
        $this->view_component->seo('description','2017UCRR大專校際道路機車賽(University Campus of Road Racing)，提供熱愛機車賽事的大專生、大學生能夠有機會參加賽車運動，賽制：計時賽、統規賽、爭先賽、耐久賽，車種：Bike檔車、Scooter速克達，組別：MiniBike、Supermoto、125cc、offroad等原廠組、改裝組...將在桃園極限賽車場及台南台糖賽車場舉辦，Webike也贊助UCRR，提供參賽者優惠、現場攤位活動，歡迎熱愛摩托車的朋友一同共襄盛舉。');
        $this->view_component->seo('keywords', 'UCRR, 大專校際道路機車賽, University Campus of Road Racing, 桃園極限賽車場, 台南台糖賽車場, 賽車運動, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口');
        return view('response.pages.ucrr.index', (array)$this->view_component);
	}
}

