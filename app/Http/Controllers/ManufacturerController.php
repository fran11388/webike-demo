<?php
namespace App\Http\Controllers;

use URL;
use App\Components\View\SimpleComponent;
use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Repository\ReviewRepository;

class ManufacturerController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);

    }

    public function info($url_rewrite)
    {
        // dd(\Ecommerce\Repository\MotorRepository::MotorCount());
        request()->request->add(['manufacturer' => $url_rewrite ]);
        $this->cache_key = http_build_query(request()->only(['motor','category','manufacturer']));

        $manufacturer = ManufacturerRepository::getManufacturerWithRelations($url_rewrite);
        if(!$manufacturer){
            return abort(404);
        }
        $this->view_component->current_category = null;
        $this->view_component->current_manufacturer = $manufacturer;
        $this->view_component->current_motor = null;
        $this->view_component->manufacturer_info = $manufacturer->info;
        if(!$this->view_component->manufacturer_info or !$this->view_component->manufacturer_info->active){
            return abort(404);
        }
        $this->view_component->url_path = request()->path();
        $this->view_component->ranking_avg =  \Cache::tags(['summary','review'])->remember('summary-ranking_avg-'.$this->cache_key, 86400 * 7, function () {
            return ReviewRepository::getAvgRanking(
                $this->view_component->current_category,
                $this->view_component->current_manufacturer,
                $this->view_component->current_motor
            );
        });
        $this->view_component->base_url = route('summary', 'br/' . $url_rewrite);
        $this->view_component->setBreadcrumbs($manufacturer->name, $this->view_component->base_url);
        $this->view_component->setBreadcrumbs($manufacturer->name . '品牌介紹');
        $this->view_component->seo('title', $manufacturer->name . '品牌介紹');
        $this->view_component->seo('description', mb_substr($manufacturer->description, 0, 160));
        $this->view_component->render();

        // if(str_contains(request()->getHost(),"amp")){
        //     return view('mobile.pages.amp.manufacturer.info',  (array)$this->view_component);
        // }else{
        //     return view('response.pages.manufacturer.info', (array)$this->view_component);
        // }
        return view('response.pages.manufacturer.info', (array)$this->view_component);
    }


}
