<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Ecommerce\Core\Wmail;

class WebhookController extends BaseController
{
    public function __construct()
    {
        \Debugbar::disable();
    }
    
    public function line()
    {
        $accesstoken = 'e/Ae1eq+tZ8ahVn9gSD3Vdivs+C81y0LM9Pb1uY1ctMFD74OXN8f/A90dnLl5iXhbKa420rxu3F3Wh6qXo/SM9pzaRkRK/eJ2E154Cy++TE/vlCFWvrlBOmtZgPawsdBlLhj6Ub1eeBMOZzKYUE2bAdB04t89/1O/w1cDnyilFU=';
        $events = request()->input('events');
        foreach($events as $event){
            $user_id = $event['source']['userId'];

            $supplier = \Everglory\Models\Supplier::where('line_api_id',$user_id)->first();
            $data['replyToken'] = $event['replyToken'];
            if($supplier){

                $data['messages'][] = ['type'=>'text','text'=>'此帳戶已驗證。'];
            }else{
                //verify
                $message = $event['message'];
                $supplier = \Everglory\Models\Supplier::where('line_api_verify',$message['text'])->first();
                if($supplier){
                    $supplier->line_api_id = $user_id;
                    $supplier->save();
                    $data['messages'][] = ['type'=>'text','text'=>'驗證成功!'];
                }else{
                    $data['messages'][] = ['type'=>'text','text'=>'驗證失敗，請洽Webike服務人員!'];
                }
            }

            $URL = 'https://api.line.me/v2/bot/message/reply';

            $header = array();
            $header[] = 'Content-type: application/json';
            $header[] = 'Authorization: Bearer ' . $accesstoken;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
            curl_setopt($ch, CURLOPT_URL,$URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POST,true);
            $result=curl_exec ($ch);
            curl_close ($ch);
            \Log::info($result);
        }
        exit();
    }
    
    public function pixel()
    {
        if( !empty( request()->input('log') ) &&  !empty( request()->input('user') ) && !empty( request()->input('subject') ) && !strpos(request()->input('user'),"http") )
        {

            if(request()->input('log') == 'true' || request()->input('log') == '3Dtrue'){
                $user = request()->input('user');
                $subject = request()->input('subject');
                $user = str_replace('3D','',$user);
                $subject = str_replace('3D','',$subject);

                if(request()->has('target')){
                    $target = request()->input('target');
                    $target = str_replace('3D','',$target);
                    if($target == 'aftersales'){
                        $item = \Everglory\Models\Email\Log\Service::where('customer_id',$user)->where('id',$subject)->first();
                        if($item){
                            if(!$item->get_open){
                                $item->get_open = date('Y-m-d H:i:s');
                                $item->save();
                            }
                        }
                    }

                }else{
                    // newsletter 
                    //check already open
                    $item = \Everglory\Models\Email\Newsletter\Item::where('customer_id',$user)->where('queue_id',$subject)->first();
                    if($item){
                        if(!$item->get_open){
                            $item->get_open = date('Y-m-d H:i:s');
                            $item->save();
                        }
                    }
                }

                header( 'Content-Type: image/gif' );

                //Get the http URI to the image
                $graphic_http = asset('http://www.webike.tw/blank.gif');

                //Get the filesize of the image for headers
                $filesize = filesize( 'blank.gif' );

                //Now actually output the image requested, while disregarding if the database was affected
                header( 'Pragma: public' );
                header( 'Expires: 0' );
                header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
                header( 'Cache-Control: private',false );
                header( 'Content-Disposition: attachment; filename="blank.gif"' );
                header( 'Content-Transfer-Encoding: binary' );
                header( 'Content-Length: '.$filesize );
                readfile( $graphic_http );

                exit;
            }
        }
        exit;
    }


    public function MMEmail($mailType)
    {
        $wmail = new Wmail();
        
        switch ($mailType) {
            case 'Question':
                $bike = new \stdClass();
                $bike->type = request()->input('type');
                $bike->seller = request()->input('seller');
                $bike->product_name = request()->input('product_name');
                $bike->customer_mail = request()->input('email');
                $bike->serial = request()->input('serial');
                $bike->asker = request()->input('asker');
                $bike->content = request()->input('content');
                $bike->content_reply = request()->input('content_reply');
                
                $wmail->motomarket($mailType,$bike);
                break;

            case 'AutoFollow':
                // $collection = [
                //     0 => [ 
                //         "customer_mail" => "waynevmw@gmail.com",
                //         "type" => "AutoFollow",
                //         "customer_name" => "黃竟豪",
                //         "product_name" => "MAXSYM 400i",
                //         "products" => array( 
                //             "4505" => [
                //                 '<a href="http://www.webike.tw/motomarket/detail/20150924D004625" target="_blank">【黃竟豪】電洽 台中市 黃牌重型機車(本月購車可享0頭</a>',
                //                 '<a href="http://www.webike.tw/motomarket/detail/20141216D001440" target="_blank">【黃竟豪】電洽 新竹市 ABS 整車原漆無事故 可貸款</a>'
                //             ],
                //             "4508" => [
                //                 '<a href="http://www.webike.tw/motomarket/detail/20150924D004625" target="_blank">【黃竟豪】電洽 台中市 黃牌重型機車(本月購車可享0頭</a>',
                //                 '<a href="http://www.webike.tw/motomarket/detail/20141216D001440" target="_blank">【黃竟豪】電洽 新竹市 ABS 整車原漆無事故 可貸款</a>'
                //             ]
                //         ),
                //         "motors" => array(
                //             "4505" => "MAXSYM 400i",
                //             "4508" => "test bububu"
                //         )
                //     ],
                //     1 => [ 
                //         "customer_mail" => "wayne_huang@everglory.asia",
                //         "type" => "AutoFollow",
                //         "customer_name" => "黃竟豪2",
                //         "product_name" => "MAXSYM 400i",
                //         "products" => array( 
                //             "4505" => [
                //                 '<a href="http://www.webike.tw/motomarket/detail/20150924D004625" target="_blank">【黃竟豪】電洽 台中市 黃牌重型機車(本月購車可享0頭</a>',
                //                 '<a href="http://www.webike.tw/motomarket/detail/20141216D001440" target="_blank">【黃竟豪】電洽 新竹市 ABS 整車原漆無事故 可貸款</a>'
                //             ]
                //         ),
                //         "motors" => array(
                //             "4505" => "MAXSYM 400i"
                //         )
                //     ]
                // ];
                $collection = request()->input('collection');
                if( $collection ){
                    foreach ($collection as $key => $bike) {
                        
                        $wmail->motomarket($mailType,$bike);
                    }
                }
                \App::abort(404);
                break;

            default:
                \App::abort(404);
                break;
        }
    }
    
    public function virtualbank()
    {
        if(request()->has('Data')){
            \DB::connection('ec_dbs')->table('virtualbank_post_log')->insert([
                'data_string' => request()->input('Data'),
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            exit;
        }else{
            app()->abort( 404 );
        }
    }
}