<?php
namespace App\Http\Controllers;

use Ecommerce\Service\EstimateService;
use App\Components\View\SimpleComponent;
use Auth;

class MitumoriController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('mitumori');
        $component->setBreadcrumbs('未登錄商品查詢購買系統',$component->base_url);
    }

    public function index()
    {
        $this->view_component->seo('title','未登錄商品查詢購買系統');
        $this->view_component->seo('description','未登錄商品查詢購買系統');
        $this->view_component->seo('keyword','未登錄商品查詢購買系統');
        $this->view_component->render();
        return view('response.pages.mitumori.index', (array)$this->view_component);
    }

    public function estimate()
    {
        if (Auth::check()){
            $items = [];
            $syouhin_maker   = request()->input('syouhin_maker');
            $syouhin_name    = request()->input('syouhin_name');
            $syouhin_code = request()->input('syouhin_code');
            $syouhin_type = request()->input('syouhin_type');
            $syouhin_taiou_maker = request()->input('syouhin_taiou_maker');
            $syouhin_taiou_syasyu = request()->input('syouhin_taiou_syasyu');
            $syouhin_kosuu = request()->input('syouhin_kosuu');
            $syouhin_bikou = request()->input('syouhin_bikou');

            for($i = 0; $i < count($syouhin_name); $i++) {
                if ($syouhin_maker[$i] != '' && $syouhin_name[$i] != '' && $syouhin_kosuu[$i] != '') {
                    $items[$i] = [
                        'syouhin_maker' => $syouhin_maker[$i],
                        'syouhin_name' => $syouhin_name[$i],
                        'syouhin_code' => $syouhin_code[$i],
                        'syouhin_type' => $syouhin_type[$i],
                        'syouhin_taiou_maker' => $syouhin_taiou_maker[$i],
                        'syouhin_taiou_syasyu' => $syouhin_taiou_syasyu[$i],
                        'syouhin_kosuu' => $syouhin_kosuu[$i],
                        'syouhin_bikou' => $syouhin_bikou[$i],
                    ];
                }
            }
            $estimate_service = new EstimateService(true);
            $mitumori = $estimate_service->estimateMitumori($items);
            $this->view_component->mitumori = $mitumori;
            //email
            $wmail = new \Ecommerce\Core\Wmail();
            $wmail->mitumori($mitumori);
            session()->flash('code', $mitumori->estimate_code);
            return redirect()->route('mitumori-estimate-done');
        }else{
            return redirect()->route('login');
        }
    }

    public function done()
    {
        if(!session()->has('code')){
            return abort(404);
        }

        $this->view_component->seo('title','未登錄商品查詢購買系統');
        $this->view_component->seo('description','未登錄商品查詢購買系統');
        $this->view_component->seo('keyword','未登錄商品查詢購買系統');
        $this->view_component->setBreadcrumbs('完成');
        $this->view_component->render();
        return view('response.pages.mitumori.done', (array)$this->view_component);
    }

}