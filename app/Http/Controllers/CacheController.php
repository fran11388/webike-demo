<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Cache;

class CacheController extends BaseController
{
    public function product()
    {
        if(request()->has('skus')){
            $skus = explode('_',request()->input('skus'));
            $key_pre = 'Ecommerce\Service\ProductService@getProductDetail?';
            foreach ($skus as $sku) {
                $key = $key_pre.base64_encode(serialize([$sku]));
                Cache::forget($key);
            }
            return 'success';
        }else{
            return 'no skus';
        }
    }

    public function summary()
    {
        $tags = ['summary','review'];
        Cache::tags($tags)->flush();
    }

    public function ranking()
    {
        $tags = ['ranking'];
        Cache::tags($tags)->flush();
    }


    public function opcache()
    {
        opcache_reset();
        return 'success';
    }

    public function staticFile()
    {
        Cache::forget(STATIC_FILE_CACHE_KEY);
        Cache::forever(STATIC_FILE_CACHE_KEY, strtotime('now'));
        return 'Static file timestamp is updated.';
    }

    public function cdn()
    {
        $send_url = "https://api.cdn77.com/v2.0/data/purge-all";
        $post_data = array(
            "cdn_id" => '77146',
            "login" => 'sg_admin@rivercrane.com',
            "passwd" => 'JxmFORUwfzrLCps3Z0cD5gM2Yyk4KPj6'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $send_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1); // 不直接出現回傳值
        curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ( $info['http_code'] == '200' ) {
            echo "更新成功！";
        }else{
            echo "更新失敗！請洽系統部門";
        }
    }

    public function tagFlush()
    {
        if(request()->has('tag')){
            Cache::tags(request()->input('tag'))->flush();
            return 'success clear '.request()->input('tag');
        }
    }

    public function forget()
    {
        if(request()->has('key')){
            Cache::forget(request()->input('key'));
            return 'success clear '.request()->input('key');
        }
    }
}