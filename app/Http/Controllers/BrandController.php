<?php
namespace App\Http\Controllers;

use URL;
use App\Components\View\SimpleComponent;

class BrandController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('customer');
        $component->setBreadcrumbs('會員中心', $component->base_url);
    }

    public function index()
    {
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.brand.index', (array)$this->view_component);
    }

    public function indexca()
    {
        $this->view_component->seo('title', '會員中心');
        $this->view_component->seo('description', '提供「Webike-摩托百貨」與「Webike-摩托車市」服務，滿足您的摩托人生歡迎免費加入!');
        $this->view_component->seo('keyword', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.brand.ca', (array)$this->view_component);
    }

}