<?php
namespace App\Http\Controllers;

use Ecommerce\Service\EstimateService;
use App\Components\View\SimpleComponent;
use Ecommerce\Core\Auth\Hasher;
use Ecommerce\Service\GroupBuyService;
use Auth;

class GroupBuyController extends Controller
{
    public function __construct(SimpleComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('groupbuy');
        $component->setBreadcrumbs('團購報價系統',$component->base_url);
    }

    public function index()
    {
        $this->view_component->products = GroupBuyService::getRequestProduct();
        $this->view_component->seo('title','團購報價系統');
        $this->view_component->seo('description','團購報價系統');
        $this->view_component->seo('keyword','團購報價系統');
        $this->view_component->render();
        return view('response.pages.groupbuy.index', (array)$this->view_component);
    }

    public function estimate()
    {
        if (Auth::check()){
            $items = GroupBuyService::getRequestEstimates();
            $estimate_service = new EstimateService(true);
            $groupbuy = $estimate_service->estimateGroupbuy($items);
            $this->view_component->mitumori = $groupbuy;
            //email
            $wmail = new \Ecommerce\Core\Wmail();
            $wmail->groupbuy($groupbuy);
            session()->flash('code', $groupbuy->estimate_code);
            return redirect()->route('groupbuy-estimate-done');
        }else{
            return redirect()->route('login');
        }
    }

    public function done()
    {
        if(!session()->has('code')){
            return abort(404);
        }
        $this->view_component->seo('title','團購報價系統完成');
        $this->view_component->seo('description','團購報價系統完成');
        $this->view_component->seo('keyword','團購報價系統完成');
        $this->view_component->setBreadcrumbs('完成');
        $this->view_component->render();
        return view('response.pages.groupbuy.done', (array)$this->view_component);
    }
}