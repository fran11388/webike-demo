<?php
namespace App\Http\Controllers;

use Ecommerce\Service\GenuinepartsService;
use Ecommerce\Service\EstimateService;
use App\Components\View\GenuineComponent;
use Everglory\Constants\CountryGroup;
use Illuminate\Http\Request;
use Log;

class GenuinepartsController extends Controller
{
    protected $countryService;

    public function __construct(GenuineComponent $component)
    {
        parent::__construct($component);
        $component->base_url = route('genuineparts');
        $component->setBreadcrumbs('正廠零件報價購買系統', $component->base_url);
    }

    public function index()
    {
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $brands = [];
        foreach($this->view_component->country_manufacturers as $name => $collection){
            if(isset($collection['group'])){
                foreach($collection['group'] as $genuine_manufacturer){
                    $brands[] = $genuine_manufacturer->display_name;
                }
            }
        }
        $import_brands = [];
        if(isset($this->view_component->import_manufacturers['進口']['group'])) {
            foreach ($this->view_component->import_manufacturers['進口']['group'] as $import_brand) {
                $import_brands[] = $import_brand->display_name;
            }
        }
        $this->view_component->seo('title', '【正廠零件報價購買系統】' . implode(', ', $import_brands));
        $this->view_component->seo('description', '訂購重機、機車正廠零件不求人，簡單又容易!本系統全年無休24小時提供線上服務。另外，還提供各廠牌正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。' . implode(', ', $brands));
        $this->view_component->seo('keywords', '機車零件,訂購,查詢,報價,本田,山葉,鈴木,川崎,哈雷,原裝,原廠零件,純正部品'.implode(', ', $brands));

        $this->view_component->render();
        return view('response.pages.genuineparts.index', (array)$this->view_component);
    }

    public function divide($divide, $manufacturer_name = NULL)
    {
        if ($divide != CountryGroup::DOMESTIC['url_code'] and $divide != CountryGroup::IMPORT['url_code']) {
            return app()->abort(404);
        }
        $view_component = $this->view_component;
        $view_component->divide = $divide;
        $divide_attribute = strtoupper($divide);
        $divide_data = CountryGroup::getConst($divide_attribute);
        $view_component->divide_name = $divide_data['name'];
        $view_component->target_sents = $view_component->sect_manufacturers;
        if ($divide == CountryGroup::IMPORT['url_code']) {
            unset($view_component->target_sents[CountryGroup::TW['name']]);
            //get brands for seo
            $brands = [];
            foreach($this->view_component->import_manufacturers['進口']['group'] as $brand){
                $brands[] = $brand->display_name;
            }
            $this->view_component->setBreadcrumbs(CountryGroup::IMPORT['name'] . '正廠零件', route('genuineparts-divide', [$divide]));
            $this->view_component->seo('title', '【' . CountryGroup::IMPORT['name'] . '正廠零件報價購買系統】' . implode(', ',$brands));
            $this->view_component->seo('description', '購買' . CountryGroup::IMPORT['name'] . '原廠零件、純正部品不求人，簡單又容易!本系統全年無休24小時線上服務。另外，還提供' . CountryGroup::IMPORT['name'] . '正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。');
        } else {
            foreach ($view_component->target_sents as $sent_name => $target_sent) {
                if ($sent_name != CountryGroup::TW['name']) {
                    unset($view_component->target_sents[$sent_name]);
                }
            }
            //get brands for seo
            $brands = [];
            foreach($this->view_component->import_manufacturers['國產']['group'] as $brand){
                $brands[] = $brand->display_name;
            }
            $this->view_component->setBreadcrumbs(CountryGroup::DOMESTIC['name'] . '正廠零件', route('genuineparts-divide', [$divide]));
            $this->view_component->seo('title', '【' . CountryGroup::DOMESTIC['name'] . '正廠零件報價購買系統】' . implode(', ',$brands));
            $this->view_component->seo('description', '購買' . CountryGroup::DOMESTIC['name'] . '原廠零件、純正部品不求人，簡單又容易!本系統全年無休24小時線上服務。另外，還提供' . CountryGroup::DOMESTIC['name'] . '正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。');
        }

        //when has target manufacturer
        if ($manufacturer_name) {
            //get manufacturer and filter manufacturer name
            $genuine_manufacturers = GenuinepartsService::getManufacturersByNames([$manufacturer_name]);
            if (!$genuine_manufacturers->count()) {
                return app()->abort(404);
            }
            $genuine_manufacturer = $genuine_manufacturers->first();
            $view_component->target_manufacturer = $genuine_manufacturer;

            //get manufacturer sent code
            $countryServices = CountryGroup::getConsts(['JP', 'EU_US', 'TH', 'TW']);
            foreach ($countryServices as $groupCode => $data) {
                if (in_array($genuine_manufacturer->manufacturer->country, $data['group'])) {
                    $view_component->target_sent_code = $data['code'];
                }
            }
            $this->view_component->setBreadcrumbs($genuine_manufacturer->display_name . '正廠零件', route('genuineparts-divide', [$divide, $genuine_manufacturer->api_usage_name]));
            $this->view_component->seo('title', '【' . $genuine_manufacturer->display_name . '正廠零件報價購買系統】');
            $this->view_component->seo('description', '購買' . $genuine_manufacturer->display_name . '原廠零件、純正部品不求人，簡單又容易!本系統全年無休24小時線上服務。另外，還提供' . $genuine_manufacturer->display_name . '正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。');
        }

        $name_compares = [];
        foreach ($view_component->target_sents as $name => $collection) {
            foreach ($collection['group'] as $genuine_manufacturer) {
                $name_compares[$genuine_manufacturer->api_usage_name] = $genuine_manufacturer->display_name;
            }
        }

        $view_component->name_compares = json_encode($name_compares);
        $view_component->seo('keyword', '機車零件,訂購,查詢,報價,本田,山葉,鈴木,川崎,哈雷,原裝,原廠零件,純正部品, genuine parts, honda, yamaha, suzuki, kawasaki,DUCATI,HARLEY-DAVIDSON, cb400, cb1300, cb1100, cb750, cb, cbr, cbr1000, vtr1000, vfr, ftr, cr, crf, vt, goldwing, vtr, xr, cub, monkey, zoomer, ape, nc700, vmax, tmax, tmax530, xjr1300, yzf-r1, fz1, fz6, fz8, yz, serrow, tw225, wr250, xt250, xg, sr400, xj6, xv, gsr600, gsx1300r, gsx-r1000, sv1000, dl1000, 109r, skywave, an650, an250, tu250, drz400, rm, rm-z, address, vanvan, gladius, burgman, bandit, zrx1200r, 男子漢, zx10r, zx6r, 忍者, ninja, ninja250, er6, versys, klx, kx250, d-tracker, w650, w800, 西風, zephyr, gpz, vulcan, z, 250tr, MOSTER, MULTISTRADA HYPERMOTARD, DIAVEL, SUPERBIKE, HYPERSTRADA, SPORTSTER, Dyna, SOFTAIL, TOURING, V-ROD');
        $view_component->render();
        return view('response.pages.genuineparts.divide', (array)$view_component);
    }

    public function realTimeDivide($divide, $manufacturer_name = NULL)
    {
        if ($divide != CountryGroup::DOMESTIC['url_code'] and $divide != CountryGroup::IMPORT['url_code']) {
            return app()->abort(404);
        }
        $view_component = $this->view_component;
        $view_component->divide = $divide;
        $divide_attribute = strtoupper($divide);
        $divide_data = CountryGroup::getConst($divide_attribute);
        $view_component->divide_name = $divide_data['name'];
        $view_component->target_sents = $view_component->sect_manufacturers;
        if ($divide == CountryGroup::IMPORT['url_code']) {
            unset($view_component->target_sents[CountryGroup::TW['name']]);
            //get brands for seo
            $brands = [];
            foreach($this->view_component->import_manufacturers['進口']['group'] as $brand){
                $brands[] = $brand->display_name;
            }
            $this->view_component->setBreadcrumbs(CountryGroup::IMPORT['name'] . '正廠零件', route('genuineparts-divide', [$divide]));
            $this->view_component->seo('title', '【' . CountryGroup::IMPORT['name'] . '正廠零件報價購買系統】' . implode(', ',$brands));
            $this->view_component->seo('description', '購買' . CountryGroup::IMPORT['name'] . '原廠零件、純正部品不求人，簡單又容易!本系統全年無休24小時線上服務。另外，還提供' . CountryGroup::IMPORT['name'] . '正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。');
        } else {
            foreach ($view_component->target_sents as $sent_name => $target_sent) {
                if ($sent_name != CountryGroup::TW['name']) {
                    unset($view_component->target_sents[$sent_name]);
                }
            }
            //get brands for seo
            $brands = [];
            foreach($this->view_component->import_manufacturers['國產']['group'] as $brand){
                $brands[] = $brand->display_name;
            }
            $this->view_component->setBreadcrumbs(CountryGroup::DOMESTIC['name'] . '正廠零件', route('genuineparts-divide', [$divide]));
            $this->view_component->seo('title', '【' . CountryGroup::DOMESTIC['name'] . '正廠零件報價購買系統】' . implode(', ',$brands));
            $this->view_component->seo('description', '購買' . CountryGroup::DOMESTIC['name'] . '原廠零件、純正部品不求人，簡單又容易!本系統全年無休24小時線上服務。另外，還提供' . CountryGroup::DOMESTIC['name'] . '正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。');
        }

        //when has target manufacturer
        if ($manufacturer_name) {
            //get manufacturer and filter manufacturer name
            $genuine_manufacturers = GenuinepartsService::getManufacturersByNames([$manufacturer_name]);
            if (!$genuine_manufacturers->count()) {
                return app()->abort(404);
            }
            $genuine_manufacturer = $genuine_manufacturers->first();
            $view_component->target_manufacturer = $genuine_manufacturer;

            //get manufacturer sent code
            $countryServices = CountryGroup::getConsts(['JP', 'EU_US', 'TH', 'TW']);
            foreach ($countryServices as $groupCode => $data) {
                if (in_array($genuine_manufacturer->manufacturer->country, $data['group'])) {
                    $view_component->target_sent_code = $data['code'];
                }
            }
            $this->view_component->setBreadcrumbs($genuine_manufacturer->display_name . '正廠零件', route('genuineparts-divide', [$divide, $genuine_manufacturer->api_usage_name]));
            $this->view_component->seo('title', '【' . $genuine_manufacturer->display_name . '正廠零件報價購買系統】');
            $this->view_component->seo('description', '購買' . $genuine_manufacturer->display_name . '原廠零件、純正部品不求人，簡單又容易!本系統全年無休24小時線上服務。另外，還提供' . $genuine_manufacturer->display_name . '正廠料號查詢、維修或零件手冊購買及車主使用手冊的下載連結。');
        }

        $name_compares = [];
        foreach ($view_component->target_sents as $name => $collection) {
            foreach ($collection['group'] as $genuine_manufacturer) {
                $name_compares[$genuine_manufacturer->api_usage_name] = $genuine_manufacturer->display_name;
            }
        }

        $view_component->name_compares = json_encode($name_compares);
        $view_component->seo('keyword', '機車零件,訂購,查詢,報價,本田,山葉,鈴木,川崎,哈雷,原裝,原廠零件,純正部品, genuine parts, honda, yamaha, suzuki, kawasaki,DUCATI,HARLEY-DAVIDSON, cb400, cb1300, cb1100, cb750, cb, cbr, cbr1000, vtr1000, vfr, ftr, cr, crf, vt, goldwing, vtr, xr, cub, monkey, zoomer, ape, nc700, vmax, tmax, tmax530, xjr1300, yzf-r1, fz1, fz6, fz8, yz, serrow, tw225, wr250, xt250, xg, sr400, xj6, xv, gsr600, gsx1300r, gsx-r1000, sv1000, dl1000, 109r, skywave, an650, an250, tu250, drz400, rm, rm-z, address, vanvan, gladius, burgman, bandit, zrx1200r, 男子漢, zx10r, zx6r, 忍者, ninja, ninja250, er6, versys, klx, kx250, d-tracker, w650, w800, 西風, zephyr, gpz, vulcan, z, 250tr, MOSTER, MULTISTRADA HYPERMOTARD, DIAVEL, SUPERBIKE, HYPERSTRADA, SPORTSTER, Dyna, SOFTAIL, TOURING, V-ROD');
        $view_component->render();
        return view('response.pages.genuineparts.realtime_divide', (array)$view_component);
    }

    public function estimate(Request $request)
    {
        if(!$this->view_component->current_customer){
            return redirect()->route('login');
        }
        $divide = $request->input('divide');
        if($divide == 'domestic'){
            $is_lock = \Everglory\Models\Customer\Lock\Route::where('route', 'genuineparts-domestic')
                ->where('customer_id', $this->view_component->current_customer->id)
                ->first();
            if($is_lock){
                return view('errors.service-shutdown');
            }
        }
        
        $syouhin_code = $request->input('syouhin_code');
        $qty = $request->input('qty');
        $note = $request->input('note');
        $api_usage_name = $request->input('api_usage_name');
//        $model_name = $request->input('model_name');

        $genuine_manufacturers = GenuinepartsService::getManufacturersByNames([$api_usage_name]);
        $genuine_manufacturer = $genuine_manufacturers->first();
        if (!$genuine_manufacturer) {
            return redirect()->back()->withErrors(['伺服器忙碌中...請稍後重新再試一次。']);
        }

        $curl_para = [];
        foreach ($syouhin_code as $key => $code) {
            if (trim($code) and (int)trim($qty[$key])) {
                $curl_para['item'][$key]['model_number'] = preg_replace('/\s(?=)/', '', trim($code));
                $curl_para['item'][$key]['quantity'] = round(preg_replace('/\s(?=)/', '', trim($qty[$key])));
                if($curl_para['item'][$key]['quantity'] > 99) {
                    $curl_para['item'][$key]['quantity'] = 99;
                }
                $curl_para['item'][$key]['manufacturer_id'] = $genuine_manufacturer->manufacturer_id;
                $curl_para['item'][$key]['product_type'] = 1;
                $curl_para['item'][$key]['note'] = $note[$key];
//                $curl_para['item'][$key]['model_name'] = $model_name;
            }
        }


        $estimateService = new EstimateService(true);
        $result = $estimateService->estimateProductUseApi($divide, $curl_para);
        if (is_null($result)) {
            \Log::error('正廠零件錯誤，請至EG_ZERO查看Log');
            return redirect()->back()->withErrors(['伺服器忙碌中...請稍後重新再試一次。']);
        }

        if ($result->error_message == '') {
            $estimate_code = $result->estimate_id;
        } else {
            Log::error($result->error_message);
            return redirect()->back()->withErrors(['伺服器忙碌中...請稍後重新再試一次。']);
        }

        session()->flash('code', $estimate_code);
        return redirect()->route('genuineparts-estimate-done');
    }

    public function realTimeEstimate(Request $request)
    {
        if($request->input('step') == 'true') {
            $estimateService = new EstimateService(true);

            $step = $estimateService->estimateProductUseApiRealTime($request->input('divide'), $request->input('curl_para'), $step = 2, $request->input('estimate_code'));
            $real_time = $step->real_time;
            $code = $step->code;

            return json_encode(['real_time' => $real_time,'code' => $code]);
//                return redirect()->route('customer-history-genuineparts-detail', ['code' => $request->input('estimate_code')]);

        }

        if(!$this->view_component->current_customer){
            return json_encode(['success'=>false,'type'=>'not_login']);
        }
        $divide = $request->input('divide');
        if($divide == 'domestic'){
            $is_lock = \Everglory\Models\Customer\Lock\Route::where('route', 'genuineparts-domestic')
                ->where('customer_id', $this->view_component->current_customer->id)
                ->first();
            if($is_lock){
                return view('errors.service-shutdown');
            }
        }

        $syouhin_code = $request->input('syouhin_code');
        $qty = $request->input('qty');
        $note = $request->input('note');
        $api_usage_name = $request->input('api_usage_name');
//        $model_name = $request->input('model_name');

        $genuine_manufacturers = GenuinepartsService::getManufacturersByNames([$api_usage_name]);
        $genuine_manufacturer = $genuine_manufacturers->first();
        if (!$genuine_manufacturer) {
            return json_encode(['success'=>false,'type'=>'error','message'=>'伺服器忙碌中...請稍後重新再試一次。']);
        }

        $curl_para = [];
        foreach ($syouhin_code as $key => $code) {
            if (trim($code) and (int)trim($qty[$key])) {
                $curl_para['item'][$key]['model_number'] = preg_replace('/\s(?=)/', '', trim($code));
                $curl_para['item'][$key]['quantity'] = round(preg_replace('/\s(?=)/', '', trim($qty[$key])));
                if($curl_para['item'][$key]['quantity'] > 99) {
                    $curl_para['item'][$key]['quantity'] = 99;
                }
                $curl_para['item'][$key]['manufacturer_id'] = $genuine_manufacturer->manufacturer_id;
                $curl_para['item'][$key]['product_type'] = 1;
                $curl_para['item'][$key]['note'] = $note[$key];
//                $curl_para['item'][$key]['model_name'] = $model_name;
            }
        }


        $estimateService = new EstimateService(true);

        if($request->input('step') != 'true'){
            $result = $estimateService->estimateProductUseApiRealTime($divide, $curl_para);

            if (is_null($result)) {
                \Log::error('正廠零件錯誤，請至EG_ZERO查看Log');
                return json_encode(['success'=>false,'type'=>'error','message'=>'伺服器忙碌中...請稍後重新再試一次。']);
//                return redirect()->back()->withErrors(['伺服器忙碌中...請稍後重新再試一次。']);
            }

            if ($result->error_message == '') {
                $estimate_code = $result->estimate_id;
            } else {
                Log::error($result->error_message);
                return json_encode(['success'=>false,'type'=>'error','message'=>'伺服器忙碌中...請稍後重新再試一次。']);
            }
            session()->flash('code', $estimate_code);

            return json_encode(['success'=>true,'code' => $estimate_code,'curl_para' => $curl_para,'divide' => $divide]);
        }

    }

    public function done()
    {
        if (!session()->has('code')) {
            return abort(404);
        }
        $this->view_component->setBreadcrumbs('查詢完成(' . session('code') . ')');
        $this->view_component->seo('title', '【正廠零件報價購買系統】查詢完成');
        $this->view_component->render();
        return view('response.pages.genuineparts.done', (array)$this->view_component);
    }

    public function redirectEstimateDone()
    {
        if(request()->has('code')){
            session()->flash('code', request()->input('code'));
        }

        return redirect()->route('genuineparts-estimate-done');
    }

    public function step()
    {
        $brands = [];
        foreach($this->view_component->country_manufacturers as $name => $collection){
            foreach($collection['group'] as $genuine_manufacturer){
                $brands[] = $genuine_manufacturer->display_name;
            }
        }

        $this->view_component->setBreadcrumbs('正廠零件查詢購買系統操作說明');
        $this->view_component->seo('title', '【正廠零件購買說明】' . implode(', ', $brands));
        $this->view_component->seo('description', '訂購' . implode('、', $brands) . '重機、機車正廠零件，簡單又容易!本系統全年無休24小時提供線上服務，以下我們將引導您如何簡單、快速的查詢及購買。');
        $this->view_component->seo('keywords', '機車零件,訂購,查詢,說明,使用方法,本田,山葉,鈴木,川崎,哈雷,原裝,原廠零件,純正部品, genuine parts, ' . implode(', ', $brands));
        $this->view_component->render();
        return view('response.pages.genuineparts.step', (array)$this->view_component);
    }

    
    public function trans()
    {
        $this->view_component->setBreadcrumbs('專有名詞對照表');
        $this->view_component->seo('title', '【中英日正廠零件專有名詞對照表】HONDA‧YAMAHA‧SUZUKI‧KAWASAKI‧哈雷‧HARLEY-DAVIDSON‧BMW');
        $this->view_component->seo('description', '支援日本HONDA、YAMAHA、SUZUKI、KAWASAKI、美國HARLEY-DAVIDSON、義大利DUCATI正廠零件、德國BMW，提供各廠牌正廠料號查詢、專有名詞翻譯、對照表、維修或零件手冊購買及車主使用手冊的下載連結。-正廠零件查詢購買系統!');
        $this->view_component->seo('keywords', '機車零件,訂購,查詢,說明,使用方法,本田,山葉,鈴木,川崎,哈雷,原裝,原廠零件,純正部品, genuine parts, honda, yamaha, suzuki, kawasaki,DUCATI,HARLEY-DAVIDSON, BMW, cb400, cb1300, cb1100, cb750, cb, cbr, cbr1000, vtr1000, vfr, ftr, cr, crf, vt, goldwing, vtr, xr, cub, monkey, zoomer, ape, nc700, vmax, tmax, tmax530, xjr1300, yzf-r1, fz1, fz6, fz8, yz, serrow, tw225, wr250, xt250, xg, sr400, xj6, xv, gsr600, gsx1300r, gsx-r1000, sv1000, dl1000, 109r, skywave, an650, an250, tu250, drz400, rm, rm-z, address, vanvan, gladius, burgman, bandit, zrx1200r, 男子漢, zx10r, zx6r, 忍者, ninja, ninja250, er6, versys, klx, kx250, d-tracker, w650, w800, 西風, zephyr, gpz, vulcan, z, 250tr, MOSTER, MULTISTRADA HYPERMOTARD, DIAVEL, SUPERBIKE, HYPERSTRADA, SPORTSTER, Dyna, SOFTAIL, TOURING, V-ROD');
        $this->view_component->render();
        return view('response.pages.genuineparts.trans', (array)$this->view_component);
    }

    public function manual()
    {
        if(!$this->view_component->current_customer or !in_array($this->view_component->current_customer->role_id,[\Everglory\Constants\CustomerRole::WHOLESALE,\Everglory\Constants\CustomerRole::STAFF])){
            app()->abort('404');
        }


        $this->view_component->setBreadcrumbs('正廠零件手冊閱覽');
        $this->view_component->seo('title', '機車正廠零件手冊');
        $this->view_component->seo('description', '【全球知名品牌，超過38萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城!');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();
        return view('response.pages.genuineparts.manual', (array)$this->view_component);
    }
    public function getManual($manufacturer,$displacement)
    {

        if(!$this->view_component->current_customer or !in_array($this->view_component->current_customer->role_id,[\Everglory\Constants\CustomerRole::WHOLESALE,\Everglory\Constants\CustomerRole::STAFF])){
            app()->abort('404');
        }

        if(\Request::get('part')){
            $displacement = $displacement.\Request::get('part');
        }
        $displacements = ['501','502','50-125','126-400','400'];
        if(!in_array($displacement, $displacements)){
            return app()->abort(404);
        }

        $this->view_component->setBreadcrumbs('正廠零件手冊閱覽',\URL::route('genuineparts-manual'));
        $this->view_component->setBreadcrumbs($manufacturer);
        $this->view_component->seo('title', '機車正廠零件手冊');
        $this->view_component->seo('description', '【全球知名品牌，超過38萬項商品】改裝零件、正廠零件、人身部品應有盡有!全台最大機車專門網路購物商城!');
        $this->view_component->seo('keywords', '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場');
        $this->view_component->render();


        return view('response.pages.genuineparts.displacement.'.$displacement, (array)$this->view_component);
    }



}
