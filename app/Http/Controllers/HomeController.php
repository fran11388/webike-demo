<?php

namespace App\Http\Controllers;

use App\Console\Commands\OutletCheck;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Agent;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Repository\ReviewRepository;
use Ecommerce\Service\MitumoriService;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\SummaryService;
use Ecommerce\Service\GenuinepartsService;
use Everglory\Models\Assortment;
use Everglory\Models\Campaign\Recommend;
use Everglory\Models\Customer;
use Everglory\Models\Gift;
use Everglory\Models\MotorManufacturer;
use Everglory\Models\Product;
use Everglory\Models\Review;
use Everglory\Models\Motor;
use Everglory\Models\Stock;
use Everglory\Models\WarehouseProduct;
use Illuminate\Http\Request;
use App\Components\View\SimpleComponent;
use Ecommerce\Service\ProductService;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Service\AssortmentService;
use Ecommerce\Core\TableFactory\src\App\Excel\Service as TableFactory;
use Ecommerce\Service\CartService;
use Ecommerce\Service\HistoryService;
use Ecommerce\Service\AccountService;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;
use Everglory\Models\Email\Newsletter\Item;
use Everglory\Models\Email\Newsletter\Queue;
use Everglory\Models\Newsletter;
use Illuminate\Support\Facades\DB;
use Ecommerce\Service\GiftService;
use Everglory\Models\Campaign;
use Everglory\Models\Qa\Source;
use Everglory\Models\Qa\Answer;
use Everglory\Models\Qa\Relation as qaRelation;
use Everglory\Models\PsQaFinalization;

class HomeController extends Controller
{
    protected $conditions = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SimpleComponent $component)
    {
        //Component and User( User must use after construct!!)
        parent::__construct($component);
    }

    /**
     * Webike home page.Build this function at 2017-04-24 for UIUX
     *
     * restore version 2.0
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Mobile speed up
        $limit = Agent::limitByDevice([
            'reviews' => [20, 5],
            'new_motors' => [10, 5],
            'new_product_brands' => [null, 10],
            'assortments' => [9, 5],
        ]);
        if (Agent::isNotPhone()) {
            $this->view_component->active_station = \Ecommerce\Repository\MotoGp\Gp2019\MotogpRepository::getActiveStation();
            $this->view_component->popular_motors = MotorRepository::getPopularMotors();
            $this->view_component->gp_racers = \Ecommerce\Repository\MotoGp\Gp2019\MotogpRepository::getRank(5);
        }

        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $this->view_component->summary_title = '最新';
        $display_categories = ['1000', '3000', '4000', '1210', '9000'];
        $this->view_component->display_categories = ['安全帽' => '3000-3001', '騎士服裝' => '3000-3020', '車用包包/行李箱' => '3000-3260-1331', '改裝零件' => '1000', '排氣系統' => '1000-1001', '外觀零件' => '1000-1110', '保養耗材' => '4000', '機車工具' => '8000', '正廠零件' => 'genuineparts'];
        $this->view_component->main_categories = \Ecommerce\Repository\CategoryRepository::find($display_categories)
            ->sortBy(function ($item) use ($display_categories) {
                return current(array_keys($display_categories, $item->url_rewrite));
            });
        $this->view_component->new_motors = \Ecommerce\Service\MotorProductService::getNewestMotorProductOfCustomer($limit->new_motors);
        $this->view_component->main_manufacturers = \Ecommerce\Repository\MotorRepository::selectManufacturersByUrlRewrite(\Everglory\Constants\ViewDefine::MAIN_MOTOR_MANUFACTURERS);
        $this->view_component->posts = \Ecommerce\Service\BikeNewsService::getPickupCategoryNews(5, [70, 75, 76, 77, 78, 73], 'exclude');
        $this->view_component->reviews = \Ecommerce\Repository\ReviewRepository::get(null, null, null, $limit->reviews);
        $this->view_component->new_product_brands = \Ecommerce\Repository\ManufacturerRepository::getNewProductsBrands($limit->new_product_brands);


        // collections
        $assortService = new AssortmentService;
        $this->view_component->assortments = $assortService->getAssortmentByPublishAt($limit->assortments);
        $this->view_component->motor_manufacturers = MotorRepository::selectAllManufacturer();
        $this->view_component->setBreadcrumbs([['name' => 'Webike-台灣', 'url' => route('home')]]);
        $this->view_component->seo('title', '「Webike-台灣」進口重機、機車改裝用品情報，新車中古車資訊，全球摩托車新聞滿載!!');
        $this->view_component->seo('description', '即時更新摩托百貨、摩托車市、摩托新聞最新情報~Webike滿足您的摩托人生!');
        $this->view_component->render();
        return view('response.pages.index', (array)$this->view_component);
    }

    /**
     *  Recover this function at 2017-04-24 for UIUX
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    /*
    public function index()
    {

        $service = new \Ecommerce\Service\FeedService;
        $result = $service->getFeedsNew(1, $this->conditions);
        $this->view_component->html = $result['html'];
        $this->view_component->popular_motors = MotorRepository::getPopularMotors();
        $this->view_component->top = $result['top'];
        $this->view_component->gp_racers = \Ecommerce\Repository\MotogpRepository::getRank(5);
        $this->view_component->active_station = \Ecommerce\Repository\MotogpRepository::getActiveStation();

        $this->view_component->breadcrumbs = [];
        $this->view_component->setBreadcrumbs([['name' => 'Webike-台灣', 'url' => route('home')]]);
        $this->view_component->seo('title','「Webike-台灣」進口重機、機車改裝用品情報，新車中古車資訊，全球摩托車新聞滿載!!');
        $this->view_component->seo('description','即時更新摩托百貨、摩托車市、摩托新聞最新情報~Webike滿足您的摩托人生!');
        $this->view_component->render();
        
        return view('response.pages.index-personalize',  (array)$this->view_component);
    }
    */

    public function newProduct(Request $request)
    {
        $current_customer = $this->view_component->current_customer;
        $url_rewrite = $request->input('url_rewrite');
        $result = new \StdClass;
        $products = \Ecommerce\Repository\ProductRepository::getProductsByBrands($url_rewrite);
        $pd = [];
        foreach ($products as $product) {
            $pd[] = new ProductAccessor($product);
        }

        $result->html = view('response.common.list.view-products-rotation', compact('pd', 'current_customer'))->render();
        $result->productCount = count($pd);
        return json_encode($result);
    }

    /**
     * @param Request $request
     * @return string
     * feeds ajax
     */
    public function feeds(Request $request)
    {
        $service = new \Ecommerce\Service\FeedService;
        $result = $service->getFeedsNew($request->input('page'), $this->conditions);

        return json_encode($result);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * shopping page products
     */
    public function shopping2()
    {
        ini_set('max_execution_time', 28800);
        ini_set("memory_limit", "2048M");

        cacheControlProcess(['shopping']);

        $search_service = app()->make(SearchService::class);
        $convert_service = app()->make(ConvertService::class);
        $summary_service = app()->make(SummaryService::class);
        $genuineparts_service = app()->make(GenuinepartsService::class);

        $this->view_component->suppliers = $genuineparts_service->getMybikeFitManufacturer($this->view_component->current_customer);

        //AW or SS collection
        $riding_gears = \Cache::tags(['shopping'])->remember('shopping-awss', 86400, function () use ($convert_service, $search_service) {

//            $date = '2017-01-01';
            $date = \Carbon\Carbon::now()->subWeeks(4)->toDateString();
            request()->request->add(['created_from' => $date]);
            $riding_gear_search_response = $search_service->selectRidingGear(request());
            request()->request->remove('created_from');
            return $convert_service->convertQueryResponseToProducts($riding_gear_search_response);
        });

        $this->view_component->riding_gears = $riding_gears->shuffle();

        //outlet
        $outlet_collection = \Cache::tags(['shopping'])->remember('shopping-outlet', 86400 * 7, function () use ($convert_service, $summary_service) {
            request()->request->add(['type' => 5]);
            $search_response = $summary_service->selectNewProduct(request());
            request()->request->remove('type');
            return $convert_service->convertQueryResponseToProducts($search_response);
        });

        $this->view_component->outlet_collection = $outlet_collection->shuffle();


        //銷售排行商品

        $this->view_component->top_sales_products = \Cache::tags(['shopping'])->remember('shopping-top-sales', 86400 * 7, function () use ($convert_service, $summary_service) {
            $top_sales_search_response = $summary_service->selectTopSales(request());
            return $convert_service->convertQueryResponseToProducts($top_sales_search_response);
        });


        //main category
        $this->view_component->categories = [
            1000 => CategoryRepository::selectChildren('1000', 'url_rewrite')->filter(function ($category, $key) {
                return $category->mptt->depth == 2 and substr($category->mptt->url_path, 0, 9) != '1000-4000';
            })->random(1)
            ,
            4000 => CategoryRepository::find('4000'),
            3000 => CategoryRepository::selectChildren('3000', 'url_rewrite')->filter(function ($category, $key) {
                return $category->mptt->depth == 2 and !in_array($category->url_rewrite, ['1212', 't005']);
            })->random(1),
        ];

        $this->view_component->collections = [];
        foreach ($this->view_component->es as $url_rewrite => $category) {
            $this->view_component->collections[$url_rewrite] = \Cache::tags(['shopping'])->remember('shopping-category-' . $category->url_rewrite, 86400 * 7, function () use ($convert_service, $summary_service, $url_rewrite, $category) {
                request()->request->add(['category' => $category->mptt->url_path]);
                $search_response = $summary_service->selectSummary(request());
                return $convert_service->convertQueryResponseToProducts($search_response);
            })->shuffle();
        }

        //modify name
        $_category = $this->view_component->categories['4000'];
        $_category->name = '維護耗材及工具';
        $this->view_component->categories['4000'] = $_category;


        //amazons
        $this->view_component->amazons = ProductService::getAmazonProduct();

//
        // collections
        $assortService = new AssortmentService;
        $this->view_component->assortments = $assortService->getAssortmentByPublishAt(9);

        // recent view
        $this->view_component->render();

        return view('response.pages.shopping2', (array)$this->view_component);
    }

    /**
     * Webike shopping page function
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shopping()
    {
        cacheControlProcess(['shopping']);


        // ATS initial
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);

        request()->request->add(['limit' => 30]);
        $carousel_configs = [
            'riding_gear' => (object)['collection' => null, 'params' => (object)['key' => 'riding_gear', 'call' => 'riding_gear', 'blade' => 'a', 'limit' => '5', 'parameter' => null, 'shuffle' => 'on','rows' => '100']],
            'outlet' => (object)['collection' => null, 'params' => (object)['key' => 'outlet', 'call' => 'outlet', 'blade' => 'b', 'limit' => '5', 'parameter' => null, 'shuffle' => 'on']],
        ];

        $this->view_component->categories = [
            1000 => CategoryRepository::selectChildren('1000', 'url_rewrite')->filter(function ($category, $key) {
                return $category->mptt->depth == 2 and substr($category->mptt->url_path, 0, 9) != '1000-4000';
            })->random(1)
            ,
            4000 => CategoryRepository::find('4000'),
            3000 => CategoryRepository::selectChildren('3000', 'url_rewrite')->filter(function ($category, $key) {
                return $category->mptt->depth == 2 and !in_array($category->url_rewrite, ['1212', 't005', '1210']);
            })->random(1),
            4020 => CategoryRepository::find('4020'),
        ];
        foreach ($this->view_component->categories as $root_url_rewrite => $category) {
            $carousel_configs['category_' . $root_url_rewrite] = (object)['collection' => null, 'params' => (object)['key' => 'category_' . $root_url_rewrite, 'call' => 'category', 'blade' => 'b', 'limit' => '6', 'parameter' => ['url_path' => $category->mptt->url_path], 'shuffle' => 'on']];
        }
        $carouselService = new \Ecommerce\Service\CarouselService($carousel_configs);

        foreach ($carouselService->call() as $key => $carouselData) {
            $this->view_component->$key = $carouselData;
        }


        $genuineparts_service = app()->make(GenuinepartsService::class);

        $this->view_component->suppliers = $genuineparts_service->getMybikeFitManufacturer($this->view_component->current_customer);


        //modify name
        $_category = $this->view_component->categories['4000'];
        $_category->name = '維護耗材及工具';
        $this->view_component->categories['4000'] = $_category;


        //amazons
        $this->view_component->amazons = ProductService::getAmazonProduct();

//
        // collections
        $assortService = new AssortmentService;
        $this->view_component->assortments = $assortService->getAssortmentByPublishAt(9);

        $this->view_component->customer = \Auth::user();

        // recent view
        $this->view_component->render();


        return view('response.pages.shopping', (array)$this->view_component);
    }

    public function getdata()
    {
       $ca = request()->get('ca');
       $q = request()->get('q');
       $mt = request()->get('mt');
       $url = \URL::route('parts');
       if($ca && $mt ){
            $url = \URL::route('parts') . "/mt/" . $mt  . "/ca/" . $ca. "?q=" . $q;
        }elseif($ca){
            $url = \URL::route('parts',['section' => "ca/" . $ca]) . "?q=" . $q;
        }elseif($mt){
            $url = \URL::route('parts',['section' => "mt/" . $mt]) . "?q=" . $q;
        }else{
            $url = \URL::route('parts') . "?q=" . $q;
        }
        return redirect($url);
    }

    public function product_block()
    {
        $summary_service = app()->make(SummaryService::class);
        $convert_service = app()->make(ConvertService::class);
        $url_rewrite = request('category', '3000-3001');
        request()->request->add(['category' => $url_rewrite]);
        $search_response = $summary_service->selectSummary(request());
        $outlet_collection = $convert_service->convertQueryResponseToProducts($search_response);
        $this->view_component->products = $outlet_collection;
        $this->view_component->owl = request('owl', '1');
        $this->view_component->blade_type = request('blade', 'a');
        return view('response.common.product.list', (array)$this->view_component);
    }

    public function owl()
    {

        $carouselService = new \Ecommerce\Service\CarouselService(
            [
                'riding_gear' => (object)['collection' => null, 'params' => (object)['key' => 'riding_gear', 'blade' => 'a', 'limit' => '6', 'parameter' => null, 'shuffle' => 'on']],
            ]
        );

        foreach ($carouselService->call() as $key => $carouselData) {
            $this->view_component->$key = $carouselData;
        }


        return view('response.common.product.owl', (array)$this->view_component);
    }

    public function carousel()
    {
        $result = new \stdClass();
        $result->success = false;
        $result->data = '';

        if (request()->has(['ajax_data.key', 'ajax_data.call', 'ajax_data.blade'])) {
            $carousel_condition = [
                request('ajax_data.key') => (object)['collection' => null, 'params' => (object)['blade' => request('ajax_data.blade'), 'call' => request('ajax_data.call'), 'limit' => request('ajax_data.total'), 'parameter' => request('ajax_data.parameter'), 'shuffle' => request('ajax_data.shuffle'), 'page' => request('ajax_data.page'),'rows' => '100']],
            ];
            $carouselService = new \Ecommerce\Service\CarouselService($carousel_condition);

            $carousels = $carouselService->call(request('ajax_data.key'));
            if (request('ajax_data.page')) {
                $collection = $carousels->collection[request('ajax_data.page')];
            } else {
                $collection = $carousels->collection;
            }

            $params = $carousels->params;

            if (count($collection)) {
                $html_array = [];
                $expects = [];
                if (request('expects')) {
                    $expects = request('expects');
                }
                $current_customer = $this->view_component->current_customer;
                foreach ($collection as $product) {
                    $search_key = array_search($product->sku, $expects);
                    if (false !== $search_key) {
                        // skip
                    } else {
                        $html = view('response.common.product.' . $params->blade, compact('product', 'current_customer'))->render();
                        array_push($html_array, $html);
                    }
                }

                $result->data = $html_array;
                $result->success = true;
            }
        }
        return json_encode($result);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \League\Flysystem\Exception
     */
    public function test()
    {
        set_time_limit(0);
        $customer_ids = null;
        $queue_id = 349;
        if (strpos($customer_ids, '_') !== false) {
            $customer_ids = explode('_', $customer_ids);
        }
        $newsletter = Newsletter::where('id', '=', '470')->first();
        if ($queue_id) {
            $queue = Queue::where('id', '=', $queue_id)->first();
            $queue->status_id = 1;
        } else {
            $queue = new Queue;
            $queue->status_id = 0;
        }

        $queue->save();

        if (count($customer_ids)) {
            if ($queue->send_method == 0) {
                $queue->items()->delete();
            }
            if (!is_array($customer_ids)) {
                throw new \Exception('This function need to use array in customer_ids case.');
            }
            if (!count($customer_ids)) {
                $customer_ids = Customer::select('id')->where('newsletter', 1)->get()->pluck('id')->toArray();
            }
            foreach ($customer_ids as $customer_id) {
                $item = new Item;
                $item->customer_id = $customer_id;
                $item->queue_id = $queue_id;
                $item->save();
            }
        }
        dd(123);
        $ob = new OutletCheck;
        $ob->handle();
        die();
        $product = ProductRepository::findDetail('t00055602');
        $pd = new ProductAccessor($product);
        $npd = $pd->getMainProduct();
        dd();
//        dd(unserialize('a:6:{s:6:"_token";s:40:"pzaHJqhpo2RcadsCkbIGveAjLaKuIC6Q23D1Wlo4";s:9:"_previous";a:1:{s:3:"url";s:20:"http://www.webike.tw";}s:6:"_flash";a:2:{s:3:"old";a:0:{}s:3:"new";a:0:{}}s:12:"previous_url";s:21:"http://www.webike.tw/";s:38:"login_82e5d2c56bdd0811318f0cf078b78bfc";i:561;s:9:"_sf2_meta";a:3:{s:1:"u";i:1489566131;s:1:"c";i:1489566125;s:1:"l";s:5:"43200";}}'));
        $sess = request()->input('sess');
        $key = \Config::get('app.key');
        $cipher = \Config::get('app.cipher');
//        dd(\App\Encryption\McryptEncrypter::supported($key, $cipher));
        $encrypter = new \App\Encryption\McryptEncrypter($key, $cipher);

        $cookie = $sess;
        $sessionId = $encrypter->decrypt($cookie);
        dd($sessionId);

        dd('cache flush finish');
        \Cache::tags(['shopping'])->flush();
        dd('cache flush finish');
//        ini_set("memory_limit","2048M");
//        $feeder = new \Ecommerce\Core\Feed\Shopping\NewReview();
//        $feeder->import();

//        $banner = \Everglory\Models\Ats\Banner::first();
//        dd($banner->alt);
        dd('123');
        $accesstoken = 'ya29.El_XA-vcihrJlpyDJZGkK8nmlNhSBeRse3lfAbf0zvotXaww8fNYzTdYo9yVvXB-6wOzazQ4zdzlMT4iqYyFBzNj6o31U5hg5nMKWaWJRP9NfFDuOkMkPz6BYVxdWisScw';
        $ch = curl_init();
        $post_header = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $accesstoken,
        ];
        //$curl_para['q'] = '<br /><b>・Z-7のインプレッション特設ページはこちら</b><br /><a href=http://www.webike.net/adv/z-7/><img src=/catalogue/99999/z-7_imp_rank.jpg alt=Z-7インプレッションランキング></a><br /><br />構造：AIM＋(Advanced Integrated Matrix)<br />強靱なガラス繊維を基に、あらかじめ3次元形状とした有機繊維を複合積層化。高い剛性を確保しながらも軽量化を可能とした、SHOEI独自のシェル構造。<br />規格：JIS規格<br />付属品：ブレスガード、チンカーテン、シリコンオイル、CWR-1 PINLOCK&reg;fog-free sheet、布袋<br /><br /><table border=0 cellpadding=0 cellspacing=0 width=600><tr><td><img src=/catalogue/10348/z-7-1.jpg width=600 height=218 alt=z-7 /><br /><br />軽量かつコンパクトなフォルムが進化した新たなZシリーズ、「Z-7」<br />軽く。小さく。<br />シールドシステムをはじめとする全てを刷新し生まれ変わった、新たな「Zシリーズ」。<br />シリーズのコンセプトである、軽量かつコンパクトフォルムをさらに進化させ、躍動感あふれるエアロフォルム、高効率なベンチレーションシステム、静粛性を高め快適なかぶり心地を極めた内装、それらすべてが創り出す新たなピュアスポーツフルフェイス。<br />Z-7が新しいスポーツライディングの世界を開きます。<br /><br /><div style=background-color:#adadad;>Compact&nbsp;&amp;&nbsp;Light&nbsp;Weight</div><br />コンパクト&amp;ライトウェイト<br /><br /><b>ヘルメットのひとつの理想形、「軽くて小さい」を形にする</b><br />Zシリーズのコンセプトのひとつである「コンパクト」を躍動感あふれるエアロフォルムで結実。Z-7ではSサイズ以下に専用のシェルを追加し、ミニマムサイズをさらに小さく、全4サイズのシェル構成としました。また、構成するパーツ一つ一つの軽量化をさらに進め、スポーツフルフェイスとして極限の軽量化を達成、理想のコンパクト&amp;ライトウェイトを実現しました。<br /><img src=/catalogue/10348/z-7-2.jpg width=573 height=158 alt=size /><br clear=all /><br /><b>エアロフォルム<br />デザインと機能性を両立した、洗練されたエアロフォルム</b><br /><br />大気を切り裂いて走るハイスピードライディング。<br />ヘルメットもライダー・マシンと一体になり、大気の壁に突き進んでいきます。<br />ヘルメットの空力性能は特に高速走行時におけるライダーの首への負担に直接影響します。<br />SHOEIではシェルデザインを、大型風洞実験設備で繰り返し検証を重ねることで、より確実で実践的なエアロフォルムに形造っています。<br /><img src=/catalogue/10348/z-7-3.jpg width=599 height=230 alt=空力性能 /><br clear=all /><br /><b>静粛性<br />長時間のライディングほど実感できる「静かさ＝快適」</b><br />長時間の高速走行でライダーにかかる負担はなにも風による風圧だけではありません。<br />ヘルメット内部に侵入する風切音は、ライダーの集中力を削ぎ落とすとともに、それが長時間に及ぶと確実に疲労として積み重なってきます。<br />エアロフォルムと共に密閉性を高めたシールドシステムなど、まずは風切音の発生そのものを最小限に抑え、発生した風切音も、密着性を高めた内装システムにより音の侵入を防止。さらにヘルメット内部のイヤースペースには着脱可能なイヤーパッドを装備することで大幅な風切音の低減を実現しました。<br />音の発生から侵入まで、音への二重三重の対策が、長時間のライディングを楽しむライダーの疲労を格段に抑えます。<br /><img src=/catalogue/10348/z-7-4.jpg width=599 height=275 alt=静粛性 /><br clear=all /><br /><b>シールドシステム</b><br /><img src=/catalogue/10348/z-7-5.jpg width=250 height=234 alt=シールド align=left style=float:left; /><b>自然でクリアな視界を確保するCWR-1&nbsp;シールド</b><br />新開発の「CWR-1 シールド」は、シールド全体に単一の曲率や厚みを持たせるのではなく、部位に応じて曲率や厚みを最適化することで光学特性を高め、歪みを徹底的に抑えることで、自然でクリアな視界を提供します。さらにシールド上下端にリブを設けシールド自体の剛性を高め、風圧によるシールドのたわみや開閉時のよじれを解消。高速走行時の密閉性やシールド開閉の操作感においても高いクォリティーを達成しました。<br /><br clear=all /><br /><b>シールドの曇りを防ぐPINLOCK&reg;&nbsp;fog-free&nbsp;sheet</b><br /><br />低温時や降雨時に発生するシールドの曇りを防ぐことは、快適なライディングには必須な条件といえます。<br />Z-7ではシールドの曇りを防ぐPINLOCK&reg;&nbsp;fog-free&nbsp;sheetを標準装備。<br />視界のほぼ全てをカバーする、PINLOCK&reg;&nbsp;fog-free&nbsp;sheetが曇りを抑えあらゆるコンディションで快適な視界を確保します。<br /><img src=/catalogue/10348/z-7-6.jpg width=539 height=202 alt=シールド2 /><br clear=all /><br /><b>さらに進化したシールド開閉システム</b><br />簡単確実な着脱、そして可変軸ダブルアクション機構で高い密閉性を確保するシールドシステムも、Z-7では新シールドとともに全てを刷新しました。<br />シールドベースのスプリングレートを見直すことで、シールドの節度あるスムーズな開閉と、全閉時の密着性を大幅に高めました。またシールドを全閉にすると、シールドノブ部裏側のフックがシェル側にロックされ、走行時の風圧による不意な開放やシールドの動きを防ぎ高い密閉性を維持します。<br /><img src=/catalogue/10348/z-7-7.jpg width=599 height=210 alt=開閉 /><br clear=all /><br /><b>内装<br />心地よいフィッティングと確かなホールド性能が上質なライディングを作り出す</b><br />快適なライディングに必要不可欠な心地よい被り心地。それらを生み出すのがZ-7で新たに採用した内装システムです。<br />ヘルメットを着脱する際、シェル下端の形状や剛性によってきつく感じることがあります。<br />特徴的なシェルサイドのカットラインと一体化されたチークパッドにより、ヘルメットの着脱でシェルを広げて被る際の窮屈感を和らげるとともに、後方確認などの際の首周りの運動性を高めました。また下端にボリューム感を持たせたチークパッドは、3D形状のウレタンパッドの採用で頬を下からやさしくサポート、首周りへのソフトな追従性で心地よいフィット感を作りながらも風や音の侵入を防ぎます。<br />センターパッドは3DフルサポートインナーTYPE&nbsp;IVを採用。着脱の際、肌が接触する面にはソフトな感触の起毛素材を、着用時に効果的に汗の吸収を早めたい部分には吸放湿性能の高い先進素材をハイブリッドで採用することで、ソフトな被り心地と着用時の快適性を両立しました。また、部分的に赤いメッシュ素材をアクセントとして使用し、グレー＆レッドでスポーティーな雰囲気を表現しました。<br /><img src=/catalogue/10348/z-7-8.jpg width=599 height=204 alt=内装 /><br clear=all /><br /><b>E.Q.R.S.</b><br /><img src=/catalogue/10348/z-7-9.jpg width=316 height=248 alt=E.Q.R.S. align=left style=float:left; />万が一の際に第三者がライダーのヘルメットを取り外すことは大変困難な作業です。<br />Z-7では、チークパッドに専用のタブを装備。救護者はこのタブを引くことでチークパッドを引き抜くことができ、頬のホールドをなくすことで容易にヘルメットを取り外すことが可能となります。<br /><br />※普段、メンテナンスなどの目的でチークパッドを取り外すときにはE.Q.R.S.を使用しないでください。<br clear=all/><br /><b>ベンチレーションシステム<br />走るほどに、風を感じる、涼しさを感じる</b><br />前頭部中央と、上方の左右に装備する3か所のインテークホールからライナー内部まで走行風をストレートに導入するエアインテーク構造は、ヘルメットの中であってもライダーが風を体感することができる、高いベンチレーション効果を発揮します。さらにスポイラー効果を兼ね備えたエアアウトレットは、高速走行時のヘルメットの安定性とともに、内部に設けた4か所のアウトレットホールから効果的に熱気を排出します。<br /><img src=/catalogue/10348/z-7-10.jpg width=599 height=156 alt=ベンチレーションシステム /></td></tr></table><br /><br /><br /><iframe width=560 height=315 src=//www.youtube.com/embed/zxo6ldBozJs frameborder=0 allowfullscreen></iframe>';
//        $curl_para['q'] = 'エントリーモデルながら優れたプロテクションと履き心地に拘ったTCXのスタンダードオフロードブーツ。エンデューロにもどうぞ。イタリアのモーターサイクルフットウエアブランドが放つオフロードブーツ。安全に高い拘りを持つメーカーのオフロードブーツは、エントリーモデルながら、上位モデル譲りのプロテクションを装備し、快適な履き心地を実現。インソールは新設計で衝撃を吸収します。エントリーモデルとしてお勧めできる最適の一足です。・トゥ&ヒール&アンクルガード(ポリウレタン)・ヒートガード(インサイド)・アジャスタブルバックル(ポリウレタン)';
        $curl_para['q'] = 'TCX \'s standard off - road boots, which was stuck to excellent protection and comfort while using an entry model. Please also to Enduro. Italian motorcycle footwear brands offroad boots. Manufacturer\'s offroad boots that are safe and highly regarded, as well as entry models, equipped with protection from the top model, realizing comfortable comfort. Insole is new design to absorb shock. It is an optimal pair that you can recommend as an entry model. · To & Heel & Uncle Guard (Polyurethane) · Heat Guard (Inside) · Adjustable Buckle (Polyurethane)';
//        $curl_para['q'] = 'So let us begin anew--remembering on both sides that civility is not a sign of
//weakness, and sincerity is always subject to proof. Let us never negotiate out
//of fear. But let us never fear to negotiate.';
        $curl_para['source'] = 'en';
        $curl_para['target'] = 'zh-TW';
        $curl_para['model'] = 'nmt';
//        $curl_para['format'] = 'text';
        curl_setopt($ch, CURLOPT_URL, "https://translation.googleapis.com/language/translate/v2");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curl_para));
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        var_dump($result);
        exit();
        return $result;
    }

    public function query()
    {
        //review motor
//        $reviews = Review::with(['product','product.motors'])->get();
//        foreach($reviews as $review){
//            $product = $review->product;
//            if($product){
//                $motors = $product->motors;
//                foreach($motors as $motor){
//                     $review->motors()->create([
//                         'motor_id' => $motor->id,
//                         'customer_selected_flg' => ($motor->id == $review->motor_id) ? 1 : 0,
//                     ]);
//                }
//            }
//        }
//
//        dd('123finish');

        //estimate cart
        echo 'Estimate Cart update protect code' . '<br/>';
        $carts = \Everglory\Models\Estimate\Cart::whereNull('protect_code')->get();
        foreach ($carts as $cart) {
            $cart->protect_code = (string)\Uuid::generate(1);
            $cart->save();
        }

        //motor manufacturer
        \Everglory\Models\MotorManufacturer::where('id', 4)->update(['image' => '/image/factory/KAWASAKI/m-logo-K.png']);

        //customer reward
        $customers = \Everglory\Models\Customer::doesntHave('points')->get();
        foreach ($customers as $customer) {
            $point = new \Everglory\Models\Customer\Reward();
            $point->customer_id = $customer->id;
            $point->points_collected = 0;
            $point->points_used = 0;
            $point->points_waiting = 0;
            $point->points_current = 0;
            $point->points_lost = 0;
            $point->save();
        }

        dd('finish');
    }

    public function google()
    {
        $this->view_component->breadcrumbs = [];
        $this->view_component->setBreadcrumbs([['name' => 'Webike-台灣', 'url' => route('home')], ['name' => '搜尋結果', 'url' => route('google')]]);
        $this->view_component->seo('title', '「Webike-台灣」 - ' . TransferToUtf8(request()->input('q')) . '搜尋結果');
        $this->view_component->seo('description', '【' . TransferToUtf8(request()->input('q')) . '】商品一覽、新車中古車一覽、車輛規格介紹、改裝特輯、車友影片。');
        $this->view_component->render();
        return view('response.pages.google', (array)$this->view_component);
    }

    public function mitumoriCreate()
    {
        $mitumoriService = new MitumoriService();
        $gifts = Gift::where('activity', '=', '2018newyear')->where('type', '=', '1')->where('id', '<>', '15')->get();
        foreach ($gifts as $gift) {
            $products = $mitumoriService->add($gift)->get();
        }
    }

    public function arrangeOutletProduct()
    {
        $all_need_delete_sku[] = null;
        $all_need_update_sku[] = null;
        $products = \DB::select('select st.sku,st.model_number,count(st.model_number),ROUND(AVG(st.cost)*0.8) as avg from 
            (
                select st.sku,st.model_number,st.cost from stocks as st
                join stock_outlets as so
                on st.id = so.stock_id and so.status_id = 3
                where st.purchase_id is null and st.purchase_code is null
                group by st.sku,st.model_number
                order by model_number
            ) as st
            group by st.model_number
            having count(st.model_number) > 1
            order by st.model_number
          '
        );

        $outlet_group_products = collect($products);
        foreach($outlet_group_products as $outlet_group_product){
            $stocks = Stock::select('stocks.id','stocks.sku','stocks.model_number','stocks.product_id')->join('stock_outlets as so','stocks.id','=','so.stock_id')
                ->where('stocks.model_number','=',$outlet_group_product->model_number)
                ->whereNull('stocks.purchase_id')
                ->whereNull('stocks.purchase_code')
                ->where('so.status_id','=','3')
                ->get();
//            dd($stocks);

            $first_stock_product_id = null;
            $first_stock_sku = null;
            $delete_stock_sku = [];
            $delete_stock_product_ids = [];
            $update_stock_ids = [];
            foreach($stocks as $key => $stock){
                if($key == 0){
                    $all_need_update_sku[] = $stock->sku;
                    $active_product = Product::where('sku','=',$stock->sku)->first();
                    $active_product->cost = $outlet_group_product->avg;
                    $active_product->active = 1;
                    $active_product->save();

                    $warehouse_active_product = WarehouseProduct::where('sku','=',$stock->sku)->first();
                    $warehouse_active_product->cost = $outlet_group_product->avg;
                    $warehouse_active_product->status_id = 9;
                    $warehouse_active_product->save();

                    $first_stock_product_id = $stock->product_id;
                    $first_stock_sku = $stock->sku;
                }else{
                    $all_need_delete_sku[] = $stock->sku;
                    $update_stock_ids[] = $stock->id;
                    $delete_stock_sku[] = $stock->sku;
                    $delete_stock_product_ids[] = $stock->product_id;

                    $stock->bar_code = $first_stock_sku;
                    $stock->product_id = $first_stock_product_id;
                    $stock->sku = $first_stock_sku;
                    $stock->save();
                }
            }
            \DB::table('eg_zero.stock_histories')->whereIn('stock_id',$update_stock_ids)->update(['bar_code' => $first_stock_sku,'product_id' => $first_stock_product_id,'sku' => $first_stock_sku]);

            Product::whereIn('sku',$delete_stock_sku)->forceDelete();
            WarehouseProduct::whereIn('sku',$delete_stock_sku)->forceDelete();
            Stock\Outlet::whereIn('product_id',$delete_stock_product_ids)->forceDelete();
            \DB::connection('smax_pricemanager')->table('products_regular')->whereIn('product_id',$delete_stock_product_ids)->delete();
            \DB::connection('smax_pricemanager')->table('products_regular_execute')->whereIn('product_id',$delete_stock_product_ids)->delete();

//            dd('finish');
        }
        echo '執行Solr移除下列SKU: <br>'.implode("_",$all_need_delete_sku).'<br>';
        echo '執行Solr更新下列SKU: <br>'.implode("_",$all_need_update_sku).'<br>';
        echo '記得清快取。';
        die();
    }

    public function testtest()
    {
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        $this->view_component->seo('title', '天天有回饋! 周周有好康! 免費加入會員A好康!');
        $this->view_component->seo('description', '全台最大摩托百貨，每周都會推出一連串會員限定優惠活動，不論是折扣特賣、現金點數回饋加倍、COUPON折價券、或是免費點數集點活動...每周最新訊息與優惠活動也請詳閱會員電子報!');
        $this->view_component->seo('keywords', 'honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 精品, 騎士用品, 人身部品, 特惠, 特賣, 折扣, 打折, 促銷, 優惠活動');
        $this->view_component->render();
        return view('response.pages.benefit.index-test', (array)$this->view_component);
        echo 'test';
        $this->view_component->advertiser = app()->make(\Ecommerce\Core\Ats\Advertiser::class);
        return view('response.pages.testtest', (array)$this->view_component);
        $cartService = new CartService;
        $info = new \StdClass;
        $accountService = new AccountService();
        $account_info = $accountService->getDefaultAddress();
        $info->filename = $account_info->company;
        $info->title = $account_info->company;
        $info->creator = '榮芳興業有限公司';
        $info->company = '榮芳興業有限公司';
        $service = new TableFactory($info);
        $service->setAllColsWidth(5);
        $service->setAllColsHeight(20);
        $data = [
            'positions' => [
                ['col' => 'C', 'row' => '3'],
                ['col' => 'P', 'row' => '6']
            ],
            'value' => 'wewebike - 報價單',
            'setborder' => 'true'
        ];
        $service->setProgram('addCenterBox', $data);
        $name = '承辦人：' . $account_info->firstname . $account_info->lastname;
        $phone = '電話：' . $account_info->mobile;
        $address = '地址：' . $account_info->address;
        $date = '報價日期：' . date("Y-m-d");
        $customer_info = [$name, $phone, $address, $date];
        $num = 3;
        foreach ($customer_info as $key => $info) {
            $customer = [
                'positions' => [
                    ['col' => 'R', 'row' => $num],
                    ['col' => 'AA', 'row' => $num]
                ],
                'value' => $info
            ];
            $service->setProgram('addBox', $customer);
            $num++;
        }

        $column_config = [
            ['from' => 'B', 'to' => 'D'],
            ['from' => 'E', 'to' => 'S'],
            ['from' => 'T', 'to' => 'V'],
            ['from' => 'W', 'to' => 'Y'],
            ['from' => 'Z', 'to' => 'AB']
        ];

        $collection = collect();

        $titles = ['項次', '品名', '數量', '單價', '小計'];
        $collection->push($titles);
        $carts = $cartService->getItems();
        foreach ($carts as $key => $cart) {
            $current_customer = \Auth::user();
            $product = $cart->product;
            $row = [
                $key + 1,
                $product->full_name,
                $cart->quantity,
                $product->getFinalPrice($current_customer),
                $product->getCartPrice($current_customer) * $cart->quantity,
            ];
            $collection->push($row);
        }
        $num = 8;
        foreach ($collection as $key => $data) {
            foreach ($column_config as $column_number => $config) {
                $cartdata = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $num],
                        ['col' => $config['to'], 'row' => $num]
                    ],
                    'value' => $data[$column_number],
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $cartdata);
            }
            $num++;
        }

        $rowInfo = $service->getPosition();
        $maxRow = $rowInfo['maxRow'];
        $remark = [
            'positions' => [
                ['col' => 'B', 'row' => $maxRow + 1],
                ['col' => 'V', 'row' => $maxRow + 6]
            ],
            'value' => '備註:',
            'setborder' => 'true'
        ];
        $service->setProgram('addBox', $remark);

        $column_config_other = [
            ['from' => 'W', 'to' => 'Y'],
            ['from' => 'Z', 'to' => 'AB']
        ];

        $other_title = ['零件金額', '安裝工資', '費用總計'];
        $totalBegin = $maxRow + 1;
        $totalEnd = $maxRow + 3;
        $other_info = ['=SUM(Z9:Z' . "$maxRow" . ')', '', '=SUM(Z' . "$totalBegin" . ':Z' . "$totalEnd" . ')'];
        foreach ($other_title as $key => $info) {
            foreach ($column_config_other as $columnkey => $config) {
                if ($columnkey == 0) {
                    $value = $info;
                } else {
                    $value = $other_info[$key];
                }
                $title = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $maxRow + 1],
                        ['col' => $config['to'], 'row' => $maxRow + 2]
                    ],
                    'value' => $value,
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $title);
            }
            $maxRow += 2;
        }

        $rowInfo = $service->getPosition();
        $end = 'AB' . $rowInfo['maxRow'];
        $range = 'B2:' . $end;
        $outline = [
            'positions' => [
                ['col' => 'B2', 'row' => 2],
                ['col' => 'B2', 'row' => 2]
            ],
            'range' => $range
        ];
        $service->setProgram('setOutLine', $outline);

        $service->download();
    }

    public function test1111()
    {
       /* $service = new GiftService;
        $gifts = $service->getTestGift();
        
        $mitumoriService = new MitumoriService;
        foreach($gifts as $gift){
            $products[] = $mitumoriService->add($gift)->get();            
        }

           dd($products);*/

        return view('response.pages.benefit.sale.project.2017.1111promo.1111promo', (array)$this->view_component);
    }

    public function testtest1()
    {
        $cartService = new CartService;
        $accountService = new AccountService();
        $account_info = $accountService->getDefaultAddress();
        $info = new \StdClass;
        $info->filename = $account_info->company;
        $info->title = $account_info->company;
        $info->creator = '榮芳興業有限公司';
        $info->company = '榮芳興業有限公司';
        $service = new TableFactory($info);
        $service->setAllColsWidth(5.5);
        $service->setAllColsHeight(20);
        $data = [
            'positions' => [
                ['col' => 'C', 'row' => '2'],
                ['col' => 'P', 'row' => '2']
            ],
            'value' => 'wewebike - 訂購明細表'
        ];
        $service->setProgram('addCenterBox', $data);

        $column_config_other = [
            ['from' => 'R', 'to' => 'S'],
            ['from' => 'T', 'to' => 'Y']
        ];
        $historyService = new HistoryService();
        $collection_order = $historyService->getOrders('WT170113S0121-1');
        $order = $collection_order->first();
        if (!$order) {
            app()->abort(404);
        }
        $listIntegration = new ListIntegration($order->items);

        $order_title = ['訂單號碼:', '訂購日期:'];
        $num = 2;
        $order_info = [$listIntegration->collection[0]->code, $listIntegration->collection[0]->created_at];
        foreach ($order_title as $key => $info) {
            foreach ($column_config_other as $columnkey => $config) {
                if ($columnkey == 0) {
                    $value = $info;
                } else {
                    $value = $order_info[$key];
                }
                $title = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $num],
                        ['col' => $config['to'], 'row' => $num]
                    ],
                    'value' => $value
                ];

                $service->setProgram('addBox', $title);
            }
            $num++;
        }

        $column_config = [
            ['from' => 'B', 'to' => 'C'],
            ['from' => 'D', 'to' => 'N'],
            ['from' => 'O', 'to' => 'P'],
            ['from' => 'Q', 'to' => 'R'],
            ['from' => 'S', 'to' => 'T'],
            ['from' => 'U', 'to' => 'V'],
            ['from' => 'W', 'to' => 'Y'],
            ['from' => 'Z', 'to' => 'AB'],
            ['from' => 'AC', 'to' => 'AE']
        ];

        $collection = collect();

        $titles = ['項次', '品名', '數量', '一般單價', '經銷單價', '一般小計', '成本小計', '經銷利益', '經銷毛利'];
        $collection->push($titles);
        $current_customer = \Auth::user();

        $quantity = $listIntegration->collection[0]->quantity;
        $row = [
            $key,
            '【' . $listIntegration->collection[0]->manufacturer_name . '】' . $listIntegration->collection[0]->product_name,
            $listIntegration->collection[0]->quantity,
            str_replace('NT$', '', $listIntegration->collection[0]->normal_price),
            str_replace('NT$', '', $listIntegration->collection[0]->price),
            str_replace('NT$', '', $listIntegration->collection[0]->normal_price) * $quantity,
            str_replace('NT$', '', $listIntegration->collection[0]->price) * $quantity,
            '=U5-W5',
            '=Z5/U5'
        ];

        $collection->push($row);
        $num = 4;
        foreach ($collection as $key => $data) {
            foreach ($column_config as $column_number => $config) {
                $cartdata = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $num],
                        ['col' => $config['to'], 'row' => $num]
                    ],
                    'value' => $data[$column_number],
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $cartdata);
            }
            $num++;
        }

        $rowInfo = $service->getPosition();
        $maxRow = $rowInfo['maxRow'];
        $remark = [
            'positions' => [
                ['col' => 'B', 'row' => $maxRow + 1],
                ['col' => 'V', 'row' => $maxRow + 4]
            ],
            'value' => '備註:',
            'setborder' => 'true'
        ];
        $service->setProgram('addBox', $remark);

        $column_config_other = [
            ['from' => 'W', 'to' => 'AB'],
            ['from' => 'AC', 'to' => 'AE']
        ];

        $other_title = ['一般定價總價', '成本總價', '經銷利益總計', '經銷總毛利'];
        $other_info = ['=SUM(U5:V5)', '=SUM(W5:Y5)', '=AC6-AC7', '=AC8/AC6'];
        foreach ($other_title as $key => $info) {
            foreach ($column_config_other as $columnkey => $config) {
                if ($columnkey == 0) {
                    $value = $info;
                } else {
                    $value = $other_info[$key];
                }
                $title = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $maxRow + 1],
                        ['col' => $config['to'], 'row' => $maxRow + 1]
                    ],
                    'value' => $value,
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $title);
            }
            $maxRow++;
        }

        $rowInfo = $service->getPosition();
        $end = 'AE' . $rowInfo['maxRow'];
        $range = 'B2:' . $end;
        $outline = [
            'positions' => [
                ['col' => 'B2', 'row' => 2],
                ['col' => 'B2', 'row' => 2]
            ],
            'range' => $range
        ];
        $service->setProgram('setOutLine', $outline);

        $service->download();
        /*
        $titles = [1 => '項次',4 => '品名',19 => '數量',22 => '單價',25 => '小計'];
        foreach($titles as $key => $info){
            if($info == '品名'){
                $end = 14;
            }else{
                $end = 2;
            }
            $titleinfo = [
                'positions' => [
                    ['col' => \PHPExcel_Cell::stringFromColumnIndex($key), 'row' => 8 ],
                    ['col' => \PHPExcel_Cell::stringFromColumnIndex($key+$end), 'row' => 8]
                ],
                'value' => $info
            ];
            $service->setProgram('addCenterBox', $titleinfo);
        }
        $num = 9;
        $begin = 1;
        $end = 2;
        foreach($cartService->getItems() as $key => $cart){
            $current_customer = \Auth::user();
            $product = $cart->product;
            $cartdata = [
                'positions' => [
                    ['col' => 'B', 'row' => $num ],
                    ['col' => 'D', 'row' => $num]
                ],
                'value' => $key+1
            ];
            $service->setProgram('addCenterBox', $cartdata);
            $cartdata = [
                'positions' => [
                    ['col' => 'E', 'row' => $num ],
                    ['col' => 'S', 'row' => $num]
                ],
                'value' => $product->full_name
            ];
            $service->setProgram('addCenterBox', $cartdata);
            $cartdata = [
                'positions' => [
                    ['col' => 'T', 'row' => $num ],
                    ['col' => 'V', 'row' => $num]
                ],
                'value' => $cart->quantity
            ];
            $service->setProgram('addCenterBox', $cartdata);
            $cartdata = [
                'positions' => [
                    ['col' => 'W', 'row' => $num ],
                    ['col' => 'Y', 'row' => $num]
                ],
                'value' => number_format($product->getFinalPrice($current_customer))
            ];
            $service->setProgram('addCenterBox', $cartdata);
            $cartdata = [
                'positions' => [
                    ['col' => 'Z', 'row' => $num ],
                    ['col' => 'AB', 'row' => $num]
                ],
                'value' => number_format($product->getCartPrice($current_customer) * $cart->quantity )
            ];
            $service->setProgram('addCenterBox', $cartdata);
            $num++;
        }
        */

        ///


        // $service->setColsWidth([1,3]);


        // \Excel::create('Filename', function($excel) {

        //     $excel->sheet('Sheetname', function($sheet) {

        //         // Sheet manipulation

        //     });

        // })->export('xls');

        // $test = 
        // \DB::select("select col.id as id,type.name as name1,col.type_id,col.url_rewrite as name,type.url_rewrite as url_rewrite,col.banner,col.meta_title,col.meta_description,col.name as name2,col.status as active,col._blank as blank ,col.post_time as post,col.created_at as crea,col.updated_at as updat
        //     from ec_dbs.collections as col 
        //     join ec_dbs.assortment_types as type
        //     on col.type_id = type.id
        //     where col.template = 'search'
        //     order by col.id");

        // foreach($test as $a){
        //     $data = new \StdClass;
        //     $data->segments = new \StdClass;
        //     $left = new \StdClass;
        //     $left->segments = new \StdClass;
        //     unset($url);
        //     unset($dataCols);
        //     unset($data1);
        //     $data->interface =  [
        //         0 => "brands",
        //         1 => "rules",
        //         2 => "sort",
        //         3 => "limit",
        //         4 => "products"
        //     ];
        //     $id = $a->id;
        //     $type_id = $a->type_id;
        //     $name = $a->name2;
        //     $url_rewrite = $a->name;
        //     $banner = $a->banner;
        //     $meta_title = $a->meta_title;
        //     $meta_description = $a->meta_description;
        //     $link = '/collection/'.$a->url_rewrite.'/'.$a->name;
        //     $blank = $a->blank;
        //     $active = $a->active;
        //     $publish = $a->post;
        //     $create = $a->crea;
        //     $update = $a->updat;
        //     $insert = \DB::table('ec_dbs.assortments')->insertGetId(
        //         ['layout_id' => '1','type_id' => $type_id,'name' => $name,'url_rewrite' => $url_rewrite,'banner' => $banner,'meta_title' =>$meta_title,'meta_description' => $meta_description,'link'=> $link,'keywords'=>'','_blank' =>$blank,'active' =>$active,'publish_at'=>$publish,'created_at'=>$create,'updated_at'=>$update]
        //     );

        //     $url_rewrite = $a->url_rewrite;
        //     $assortment_id = $insert;

        //     $right_plugin = 4;
        //     $right_template = 5;
        //     $left_plugin = 5;
        //     $left_template =6;
        //     $data->chief = $url_rewrite;
        //     $items = 
        //     \DB::select("select * 
        //         from ec_dbs.collection_search as sh
        //         where sh.collection_id = $id
        //         and sh.type != 'model'
        //         and sh.type != 'point_discount'");

        //     foreach($items as $item){
        //         if($item->type == 'motor'){$type = 'mt';}
        //         if($item->type == 'category'){$type = 'ca';}
        //         $sh_id = explode(',',$item->value);
        //     }
        //     foreach($sh_id as $motorid){
        //         if($type == 'mt'){
        //             $result = \DB::select("select mt.url_rewrite from
        //             eg_product.motors as mt
        //             where mt.id = $motorid");
        //             foreach($result as $rewrite){
        //                 $url[] = $rewrite->url_rewrite;
        //             }
        //         }else{
        //             $result = \DB::select("select ca.url_rewrite from
        //             eg_product.categories as ca
        //             where ca.id = $motorid");
        //             foreach($result as $rewrite){
        //                 $url[] = $rewrite->url_rewrite;
        //             }
        //         }

        //     }

        //     $data1['title'] = $a->name2;

        //     $cols = [
        //     [
        //        'scale' => '12',
        //        'content' => "<img src=$banner alt=$a->name2>",
        //     ]];
        //     $dataCols = [];
        //     foreach ($cols as $col){
        //        $colData = new \StdClass;
        //        foreach ($col as $key => $value){
        //            $colData->$key = $value;
        //        }
        //        $dataCols[] = $colData;
        //     }
        //     $data1['rows'][] = $dataCols;
        //     $rightdata1 = json_encode($data1);


        //     $data->segments = array($type => implode(',', $url));
        //     $data->chief = 'current_'.$url_rewrite;
        //     $left->segments = array($type => implode(',', $url));
        //     $left->chief = 'current_'.$url_rewrite;
        //     $rightdata = json_encode($data);
        //     $leftdata = json_encode($left);

        //     \DB::table('ec_dbs.assortment_items')->insert(
        //         ['assortment_id' => $assortment_id,'plugin_id' => 1,'template_id' => 1,'section' => 'right','data' => $rightdata1,'sort' => 1,'active' => 1]
        //     );
        //     \DB::table('ec_dbs.assortment_items')->insert(
        //         ['assortment_id' => $assortment_id,'plugin_id' => $right_plugin,'template_id' => $right_template,'section' => 'right','data' => $rightdata,'sort' => 2,'active' => 1]
        //     );
        //     \DB::table('ec_dbs.assortment_items')->insert(
        //         ['assortment_id' => $assortment_id,'plugin_id' => $left_plugin,'template_id' => $left_template,'section' => 'left','data' => $leftdata,'sort' => 1,'active' => 1]
        //     );
        // }
    }

    public function test2()
    {
        dd($_SERVER['REMOTE_ADDR']);
        // $a = \DB::select("select * from ec_dbs.assortment_items where assortment_id =1072 group by assortment_id ");
        // foreach($a as $b){

        //     \DB::table('ec_dbs.assortment_items')->insert(
        //         ['assortment_id' => $b->assortment_id,'plugin_id' => 7,'template_id' => 8,'section' => 'left','data' => '','sort' => 2,'active' => 1]
        //     );
        //     \DB::table('ec_dbs.assortment_items')->insert(
        //         ['assortment_id' => $b->assortment_id,'plugin_id' => 10,'template_id' => 13,'section' => 'left','data' => '','sort' => 1,'active' => 1]
        //     );
        // }

        // die();
        // $collections = \DB::select("select *,col.url_rewrite as url_rewrite,col.name as name1,type.url_rewrite as typerewrite,col.created_at as crea, col.updated_at as updat,col._blank as blank,gal.id as galid,gal.name as name2,gal.sort as sortt
        //    from ec_dbs.collections as col
        //    join ec_dbs.collection_gallery as gal
        //    on col.id = gal.collection_id
        //    join ec_dbs.assortment_types as type
        //    on col.type_id = type.id
        //    where col.type_id < 5
        //    and col.template = 'gallery' group by col.id
        //    order by col.id,gal.sort");
        $collections = \DB::select("select *,col.url_rewrite as url_rewrite,col.name as name1,type.url_rewrite as typerewrite,col.created_at as crea, col.updated_at as updat,col._blank as blank,gal.id as galid,gal.name as name2,gal.sort as sortt
            from ec_dbs.collections as col
            join ec_dbs.collection_gallery as gal
            on col.id = gal.collection_id 
            join ec_dbs.assortment_types as type
            on col.type_id = type.id
            where col.type_id < 5
            and col.template = 'gallery'
            order by col.id,gal.sort");
        $num = '';
        $sortt = '';
        foreach ($collections as $collection) {
            $type_id = $collection->type_id;
            $name = $collection->name1;
            $url_rewrite = $collection->url_rewrite;
            $banner = $collection->banner;
            $meta_title = $collection->meta_title;
            $meta_description = $collection->meta_description;
            $typerewrite = $collection->typerewrite;
            $link = '/collection/' . $typerewrite . '/' . $url_rewrite;
            $blank = $collection->blank;
            $active = $collection->active;
            $publish = $collection->post_time;
            $create = $collection->crea;
            $update = $collection->updat;
            $galid = $collection->galid;
            $title1 = $collection->name2;
            $sort = $collection->sortt;
            $sort = $sort + 1;
            if ($num != $url_rewrite) {
                unset($data1);
                $sortt = '';
                $assortment = \DB::table('ec_dbs.assortments')->insertGetId(
                    ['layout_id' => '1', 'type_id' => $type_id, 'name' => $name, 'url_rewrite' => $url_rewrite, 'banner' => $banner, 'meta_title' => $meta_title, 'meta_description' => $meta_description, 'link' => $link, 'keywords' => '', '_blank' => $blank, 'active' => $active, 'publish_at' => $publish, 'created_at' => $create, 'updated_at' => $update]
                );
                $data1['title'] = $name;
                $data1['align'] = 'center';
                $cols = [
                    [
                        'scale' => '12',
                        'content' => "<img src=$banner alt=$name>",
                    ]];
                $dataCols = [];
                foreach ($cols as $col) {
                    $colData = new \StdClass;
                    foreach ($col as $key => $value) {
                        $colData->$key = $value;
                    }
                    $dataCols[] = $colData;
                }
                $data1['rows'][] = $dataCols;
                $rightdata1 = json_encode($data1);
                \DB::table('ec_dbs.assortment_items')->insert(
                    ['assortment_id' => $assortment, 'plugin_id' => 1, 'template_id' => 1, 'section' => 'right', 'data' => $rightdata1, 'sort' => 1, 'active' => $active]
                );
                \DB::table('ec_dbs.assortment_items')->insert(
                    ['assortment_id' => $assortment, 'plugin_id' => 7, 'template_id' => 8, 'section' => 'left', 'data' => '', 'sort' => 2, 'active' => 1]
                );
                \DB::table('ec_dbs.assortment_items')->insert(
                    ['assortment_id' => $assortment, 'plugin_id' => 10, 'template_id' => 13, 'section' => 'left', 'data' => '', 'sort' => 1, 'active' => 1]
                );
            }
            if ($sort == $sortt || $sort <= $sortt) {
                if ($sort <= $sortt) {
                    $sort = $sortt + 1;
                } else {
                    $sort++;
                }
            }
            $collection_items = \DB::select("select * from 
                ec_dbs.collection_gallery_items 
                where gallery_id = $galid");
            $assortment_id = $assortment;
            $num1 = '';

            foreach ($collection_items as $items) {
                if ($num1 != $items->gallery_id) {
                    unset($data);
                    unset($data2);
                    unset($caution);
                    unset($summary);
                    unset($a);
                    unset($data2Cols);
                    $b = '';
                    $c = '';
                    switch ($collection->block_type) {
                        case 'description':

                            // 
                            if ($title1 != NULL) {
                                $data2['title'] = $title1;
                                $a = '';
                                foreach ($collection_items as $item1) {
                                    $href = '';
                                    $description = $item1->description;
                                    $short_description = $item1->short_description;
                                    $href = $item1->href;
                                    $colData = new \StdClass;
                                    $colData->scale = 12;
                                    if ($href != '') {
                                        $a[] = "<img src=$href alt=$title1><br>" . $description . $short_description;
                                    } else {
                                        $a[] = $description . $short_description;
                                    }
                                }
                                $b = implode(',', $a);
                                $c = str_replace(',', '', $b);
                                $colData->content = $c;
                                // dd($description);
                                $data2Cols[] = $colData;
                                $data2['rows'][] = $data2Cols;
                                $scalebox1 = json_encode($data2);
                                // dd($scalebox1);
                                \DB::table('ec_dbs.assortment_items')->insert(
                                    ['assortment_id' => $assortment_id, 'plugin_id' => 1, 'template_id' => 1, 'section' => 'right', 'data' => $scalebox1, 'sort' => $sort, 'active' => 1]
                                );
                            }
                            break;
                        case 'designer':
                            $description = $items->description;
                            $short_description = $items->short_description;
                            $href = $items->href;
                            if ($title1 != NULL) {
                                $data['title'] = $title1;
                                $cols = [
                                    [
                                        'scale' => '4',
                                        'content' => "<img src=$href alt=$title1>"
                                    ],
                                    [
                                        'scale' => '8',
                                        'content' => $description . $short_description
                                    ]
                                ];
                                $dataCols = [];
                                foreach ($cols as $col) {
                                    $colData = new \StdClass;
                                    foreach ($col as $key => $value) {
                                        $colData->$key = $value;
                                    }
                                    $dataCols[] = $colData;
                                }
                                $data['rows'][] = $dataCols;
                                $scalebox = json_encode($data);
                                \DB::table('ec_dbs.assortment_items')->insert(
                                    ['assortment_id' => $assortment_id, 'plugin_id' => 1, 'template_id' => 1, 'section' => 'right', 'data' => $scalebox, 'sort' => $sort, 'active' => 1]
                                );
                            }
                            break;
                        case 'product':
                            $caution = array();
                            $summary = array();
                            $data = '';
                            $description = $items->description;
                            $shortdescription = $items->short_description;
                            $product_sku = $items->product_id;

                            $gallerys = \DB::select("select * from ec_dbs.collection_gallery_items where gallery_id=$galid");
                            foreach ($gallerys as $pd) {
                                $skuu = $pd->product_id;
                                $product = \DB::select("select * from eg_product.products as pd
                                where pd.sku = '$skuu'");
                                foreach ($product as $info) {
                                    $product_name = $info->name;
                                    $url_rewrite1 = $info->url_rewrite;
                                    $sku = $info->sku;
                                    $data['product_url_rewrites'][] = $url_rewrite1;
                                    $caution[$sku] = $description;
                                    $summary[$sku] = $shortdescription;
                                }
                            }
                            $cautions = new \StdClass;
                            foreach ($caution as $key => $cau) {
                                $cautions->$key = $cau;
                            }
                            $summarys = new \StdClass;
                            foreach ($summary as $key1 => $sum) {
                                $summarys->$key1 = $sum;
                            }
                            if ($data != '') {
                                $data['product_cautions'] = $cautions;
                                $data['product_summaries'] = $summarys;
                                $data['title'] = $product_name;
                                $data['description'] = $description;

                                $mainproduct = json_encode($data);
                                \DB::table('ec_dbs.assortment_items')->insert(
                                    ['assortment_id' => $assortment_id, 'plugin_id' => 3, 'template_id' => 4, 'section' => 'right', 'data' => $mainproduct, 'sort' => $sort, 'active' => 1]
                                );
                            }
                            break;
                        case 'products':
                            $data = '';
                            $gal = \DB::select("select * from ec_dbs.collection_gallery_items where gallery_id=$galid");
                            foreach ($gal as $pd) {
                                $skuu = $pd->product_id;
                                $product = \DB::select("select * from eg_product.products as pd
                                where pd.sku = '$skuu'");
                                foreach ($product as $info) {
                                    $url_rewrite1 = $info->url_rewrite;
                                    $data['product_url_rewrites'][] = $url_rewrite1;
                                }
                            }
                            if ($data != '') {
                                $data['title'] = $title1;
                                $main = json_encode($data);
                                \DB::table('ec_dbs.assortment_items')->insert(
                                    ['assortment_id' => $assortment_id, 'plugin_id' => 3, 'template_id' => 14, 'section' => 'right', 'data' => $main, 'sort' => $sort, 'active' => 1]
                                );
                            }
                            break;
                        case 'link':
                            $data = '';
                            $data['title'] = $title1;
                            $gallerys = \DB::select("select * from ec_dbs.collection_gallery_items where gallery_id=$galid");
                            foreach ($gallerys as $link) {
                                $href = $link->href;
                                $hrefname = $link->name;
                                $data['link'][$hrefname] = $href;
                            }
                            if ($data != '') {
                                $links = json_encode($data);
                                \DB::table('ec_dbs.assortment_items')->insert(
                                    ['assortment_id' => $assortment_id, 'plugin_id' => 9, 'template_id' => 12, 'section' => 'right', 'data' => $links, 'sort' => $sort, 'active' => 1]
                                );
                            }
                            // $data['link'] = ['風勁霸官方網址'=>'http://www.allfirst.com.tw/index.asp?lang=1','風勁霸官方粉絲團'=>'https://www.facebook.com/Volcano.Air.Compressor.Tw/?fref=ts','風勁霸品牌頁面'=>'http://www.webike.tw/br/t2158'];
                            break;
                        case 'anchor':
                            $data = '';
                            $data['title'] = $title1;
                            $gallerys = \DB::select("select * from ec_dbs.collection_gallery_items where gallery_id=$galid");
                            foreach ($gallerys as $link) {
                                $href = $link->href;
                                $hrefname = $link->name;
                                $data['link'][$hrefname] = $href;
                            }
                            if ($data != '') {
                                $links = json_encode($data);
                                \DB::table('ec_dbs.assortment_items')->insert(
                                    ['assortment_id' => $assortment_id, 'plugin_id' => 9, 'template_id' => 12, 'section' => 'right', 'data' => $links, 'sort' => $sort, 'active' => 1]
                                );
                            }
                            break;
                    }
                }
                $num1 = $items->gallery_id;
            }
            $sortt = $sort;
            $num = $url_rewrite;
        }
        // return $url_rewrite1;
    }

    public function lock()
    {
        return view('errors.lock');
    }

    public function findSummary()
    {

        $summariess = ["SD-165", "SD-172", "SD-172", "SD-183", "SD-186", "SD-236", "SD-239", "SD-247", "SD-248", "SD-250", "SD-253", "SD-254", "SD-255", "SD-256", "SD-260", "SD-261", "SD-263", "SD-265", "SD-266", "SD-267", "SD-269", "SD-272", "SD-273", "SD-282", "SD-283", "SD-287", "SD-291", "SD-248", "SD-250", "SD-250", "SD-250", "SD-266", "SD-269", "SD-340", "SD-341", "SD-342", "SD-346", "SD-351", "SD-353", "SD-356", "SD-357", "SD-340", "SD-340", "SD-340", "SD-342", "SD-425", "SD-427", "SD-427", "SD-907", "SD-911", "SD-912", "SD-920", "SD-925", "SD-928", "SD-930", "SD-931", "SD-932", "SD-937", "SD-942", "SD-943", "SD-945", "SD-946", "SD-947", "SD-948", "SD-949", "SD-953", "SD-954", "SD-955", "SD-958", "SD-960", "SD-962", "SD-964", "SD-968", "SD-974", "SD-980", "SD-981", "SD-983", "SD-987", "SD-988", "SD-990", "SD-991", "SD-994", "SD-995", "SD-996", "SD-999", "SD-9001", "SD-9002", "SD-9003", "SD-9004", "SD-9005", "SD-9012", "SD-9013", "SD-9015", "SD-9044", "SD-9045", "SD-911", "SD-911", "SD-911", "SD-947", "SD-947", "SD-948", "VB-329", "VB-127", "VB-132S", "VB-404", "VB-153", "VB-130S", "VB-235", "SD-161", "VB-138", "VB-147", "VB-124S", "VB-142S", "VB-231", "VB-315", "VB-159S", "SF-2006", "SD-944", "VB-333S", "VB-413S", "VD-164JL", "VD-352JL", "VD-156JL", "VD-165JL", "VD-172JL", "VD-156", "VD-156", "VD-165", "VD-165", "VD-165", "VD-172", "VD-172", "VD-252JL", "VD-255JL", "VD-258JL", "VD-263JL", "VD-265JL", "VD-269JL", "VD-282JL", "VD-289JL", "VD-252", "VD-426JL", "VD-430JL", "VD-438JL", "VD-441JL", "VD-442JL", "VD-915JL", "VD-942JL", "VD-958JL", "VD-959JL", "VD-984JL", "VD-993JL", "VD-9009JL", "VD-9017JL", "VD-9031JL", "VD-9035JL", "VD-269", "SD-259", "SD-908", "SD-909", "SD-910", "SD-924", "SD-9009", "SD-9010", "SD-9017", "SD-9036", "SD-9037", "SD-9061", "SD-9062", "VB-117", "VB-405", "VB-146S", "VB-241", "VB-326", "VB-412", "VB-419", "VB-230", "SD-164", "SD-185", "SD-165", "SD-258", "SD-347", "SD-352", "SD-426", "SD-430", "SD-435", "SD-438", "SD-441", "SD-442", "SD-435", "SD-435", "SD-915", "SD-927", "SD-929", "SD-9019", "SD-9022", "SD-9031", "SD-9070", "VB-422", "VD-960JL", "VB-129S", "VB-225", "VB-306", "VB-206", "VB-331", "VB-332", "VB-158", "VB-308", "VD-427JL", "VD-435JL", "VD-427", "VD-435", "VD-435", "VD-956JL", "VD-957JL", "VB-227", "VB-240", "VD-9068JL", "VD-9070JL", "VD-112JL", "VD-117JL", "VD-123JL", "VD-129JL", "VD-130JL", "VD-131JL", "VD-134JL", "VD-138JL", "VD-139JL", "VD-142JL", "VD-143JL", "VD-144JL", "VD-147JL", "VD-152JL", "VD-153JL", "VD-154JL", "VD-161JL", "VD-163JL", "VD-166JL", "VD-167JL", "VD-168JL", "VD-169JL", "VD-170JL", "VD-171JL", "VD-174JL", "VD-175JL", "VD-177JL", "VD-178JL", "VD-181JL", "VD-182JL", "VD-112", "VD-123", "VD-123", "VD-123", "VD-129", "VD-134", "VD-134", "VD-138", "VD-139", "VD-143", "VD-144", "VD-144", "VD-147", "VD-154", "VD-163", "VD-166", "VD-205JL", "VD-228JL", "VD-235JL", "VD-236JL", "VD-239JL", "VD-240JL", "VD-242JL", "VD-244JL", "VD-246JL", "VD-247JL", "VD-248JL", "VD-250JL", "VD-257JL", "VD-260JL", "VD-262JL", "VD-264JL", "VD-266JL", "VD-267JL", "VD-268JL", "VD-270JL", "VD-271JL", "VD-272JL", "VD-273JL", "VD-276JL", "VD-280JL", "VD-242", "VD-248", "VD-250", "VD-250", "VD-250", "VD-262", "VD-266", "VD-306JL", "VD-322JL", "VD-324JL", "VD-327JL", "VD-329JL", "VD-331JL", "VD-332JL", "VD-340JL", "VD-342JL", "VD-343JL", "VD-344JL", "VD-345JL", "VD-346JL", "VD-348JL", "VD-349JL", "VD-351JL", "VD-355JL", "VD-357JL", "VD-358JL", "VD-359JL", "VD-306", "VD-322", "VD-324", "VD-329", "VD-340", "VD-340", "VD-340", "VD-342", "VD-343", "VD-344", "VD-344", "VD-359", "VD-420JL", "VD-425JL", "VD-428JL", "VD-432JL", "VD-433JL", "VD-434JL", "VD-436JL", "VD-437JL", "VD-439JL", "VD-440JL", "VD-443JL", "VD-420", "VD-425", "VD-432", "VD-436", "VD-907JL", "VD-919JL", "VD-945JL", "VD-947JL", "VD-953JL", "VD-962JL", "VD-964JL", "VD-967JL", "VD-971JL", "VD-972JL", "VD-976JL", "VD-981JL", "VD-983JL", "VD-986JL", "VD-988JL", "VD-994JL", "VD-995JL", "VD-996JL", "VD-997JL", "VD-998JL", "VD-999JL", "VD-9000JL", "VD-9006JL", "VD-9010JL", "VD-9012JL", "VD-9022JL", "VD-9023JL", "VD-9025JL", "VD-9026JL", "VD-9030JL", "VD-9033JL", "VD-9034JL", "VD-9036JL", "VD-907", "VD-947", "VD-947", "VD-967", "VD-173JL", "VD-974JL", "VD-444JL", "VD-9053JL", "SD-917", "SD-956", "SD-957", "SD-9075", "SD-9076", "SD-9082", "VD-179JL", "VD-354JL", "VB-421S", "VD-277JL", "SD-101", "SD-102", "SD-108", "SD-109", "SD-110", "SD-112", "SD-117", "SD-119", "SD-129", "SD-112", "SD-129", "SD-202", "SD-204", "SD-205", "SD-213", "SD-215", "SD-216", "SD-221", "SD-222", "SD-228", "SD-234", "SD-301", "SD-309", "SD-312", "SD-314", "SD-318", "SD-320", "SD-322", "SD-324", "SD-322", "SD-401", "SD-403", "SD-412", "SD-413", "SD-414", "SD-416", "SD-417", "SD-419", "SD-420", "SD-423", "SD-420", "SD-901", "SD-904", "SD-906", "SD-907", "VB-244S", "SD-179", "SD-277", "SD-354", "SD-444", "SD-986", "VB-113", "ZD-144CT", "ZD-154CT", "ZD-156CT", "ZD-161CT", "ZD-166CT", "ZD-170CT", "ZD-172CT", "ZD-177CT", "ZD-154", "ZD-156", "ZD-156", "ZD-165", "ZD-166", "ZD-172", "ZD-248CT", "ZD-250CT", "ZD-262CT", "ZD-289CT", "ZD-250", "ZD-262", "ZD-344CT", "ZD-349CT", "ZD-352CT", "ZD-355CT", "ZD-435CT", "ZD-352CT", "ZD-945CT", "ZD-9031C", "ZD-9068C", "ZD-9070C", "VB-144S", "VB-148S", "VB-149S", "ZD-277CT", "ZD-354CT", "ZD-444CT", "ZD-9053C", "VB-420"];
        $summaries = implode('+OR+', $summariess);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://153.120.83.216:8080/solr/webike/select?q=summary%3A(" . $summaries . ")&rows=2000&wt=json&indent=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        foreach ($result->response->docs as $doc) {
//            foreach($summariess as $summary){
            if (preg_match('/(SD-165|SD-167|SD-169|SD-172|SD-173|SD-177|SD-180|SD-181|SD-183|SD-186|SD-127|SD-127|SD-134|SD-134|SD-144|SD-144|SD-156|SD-156|SD-162|SD-165|SD-165|SD-172|SD-172|SD-183|SD-186|SD-236|SD-239|SD-247|SD-248|SD-250|SD-253|SD-254|SD-255|SD-256|SD-260|SD-261|SD-263|SD-265|SD-266|SD-267|SD-269|SD-272|SD-273|SD-282|SD-283|SD-287|SD-291|SD-248|SD-250|SD-250|SD-250|SD-266|SD-269|SD-340|SD-341|SD-342|SD-346|SD-351|SD-353|SD-356|SD-357|SD-340|SD-340|SD-340|SD-342|SD-425|SD-427|SD-427|SD-907|SD-911|SD-912|SD-920|SD-925|SD-928|SD-930|SD-931|SD-932|SD-937|SD-942|SD-943|SD-945|SD-946|SD-947|SD-948|SD-949|SD-953|SD-954|SD-955|SD-958|SD-960|SD-962|SD-964|SD-968|SD-974|SD-980|SD-981|SD-983|SD-987|SD-988|SD-990|SD-991|SD-994|SD-995|SD-996|SD-999|SD-9001|SD-9002|SD-9003|SD-9004|SD-9005|SD-9012|SD-9013|SD-9015|SD-9044|SD-9045|SD-911|SD-911|SD-911|SD-947|SD-947|SD-948|VB-329|VB-127|VB-132S|VB-404|VB-153|VB-130S|VB-235|SD-161|VB-138|VB-147|VB-124S|VB-142S|VB-231|VB-315|VB-159S|SF-2006|SD-944|VB-333S|VB-413S|VD-164JL|VD-352JL|VD-156JL|VD-165JL|VD-172JL|VD-156|VD-156|VD-165|VD-165|VD-165|VD-172|VD-172|VD-252JL|VD-255JL|VD-258JL|VD-263JL|VD-265JL|VD-269JL|VD-282JL|VD-289JL|VD-252|VD-426JL|VD-430JL|VD-438JL|VD-441JL|VD-442JL|VD-915JL|VD-942JL|VD-958JL|VD-959JL|VD-984JL|VD-993JL|VD-9009JL|VD-9017JL|VD-9031JL|VD-9035JL|VD-269|SD-259|SD-908|SD-909|SD-910|SD-924|SD-9009|SD-9010|SD-9017|SD-9036|SD-9037|SD-9061|SD-9062|VB-117|VB-405|VB-146S|VB-241|VB-326|VB-412|VB-419|VB-230|SD-164|SD-185|SD-165|SD-258|SD-347|SD-352|SD-426|SD-430|SD-435|SD-438|SD-441|SD-442|SD-435|SD-435|SD-915|SD-927|SD-929|SD-9019|SD-9022|SD-9031|SD-9070|VB-422|VD-960JL|VB-129S|VB-225|VB-306|VB-206|VB-331|VB-332|VB-158|VB-308|VD-427JL|VD-435JL|VD-427|VD-435|VD-435|VD-956JL|VD-957JL|VB-227|VB-240|VD-9068JL|VD-9070JL|VD-112JL|VD-117JL|VD-123JL|VD-129JL|VD-130JL|VD-131JL|VD-134JL|VD-138JL|VD-139JL|VD-142JL|VD-143JL|VD-144JL|VD-147JL|VD-152JL|VD-153JL|VD-154JL|VD-161JL|VD-163JL|VD-166JL|VD-167JL|VD-168JL|VD-169JL|VD-170JL|VD-171JL|VD-174JL|VD-175JL|VD-177JL|VD-178JL|VD-181JL|VD-182JL|VD-112|VD-123|VD-123|VD-123|VD-129|VD-134|VD-134|VD-138|VD-139|VD-143|VD-144|VD-144|VD-147|VD-154|VD-163|VD-166|VD-205JL|VD-228JL|VD-235JL|VD-236JL|VD-239JL|VD-240JL|VD-242JL|VD-244JL|VD-246JL|VD-247JL|VD-248JL|VD-250JL|VD-257JL|VD-260JL|VD-262JL|VD-264JL|VD-266JL|VD-267JL|VD-268JL|VD-270JL|VD-271JL|VD-272JL|VD-273JL|VD-276JL|VD-280JL|VD-242|VD-248|VD-250|VD-250|VD-250|VD-262|VD-266|VD-306JL|VD-322JL|VD-324JL|VD-327JL|VD-329JL|VD-331JL|VD-332JL|VD-340JL|VD-342JL|VD-343JL|VD-344JL|VD-345JL|VD-346JL|VD-348JL|VD-349JL|VD-351JL|VD-355JL|VD-357JL|VD-358JL|VD-359JL|VD-306|VD-322|VD-324|VD-329|VD-340|VD-340|VD-340|VD-342|VD-343|VD-344|VD-344|VD-359|VD-420JL|VD-425JL|VD-428JL|VD-432JL|VD-433JL|VD-434JL|VD-436JL|VD-437JL|VD-439JL|VD-440JL|VD-443JL|VD-420|VD-425|VD-432|VD-436|VD-907JL|VD-919JL|VD-945JL|VD-947JL|VD-953JL|VD-962JL|VD-964JL|VD-967JL|VD-971JL|VD-972JL|VD-976JL|VD-981JL|VD-983JL|VD-986JL|VD-988JL|VD-994JL|VD-995JL|VD-996JL|VD-997JL|VD-998JL|VD-999JL|VD-9000JL|VD-9006JL|VD-9010JL|VD-9012JL|VD-9022JL|VD-9023JL|VD-9025JL|VD-9026JL|VD-9030JL|VD-9033JL|VD-9034JL|VD-9036JL|VD-907|VD-947|VD-947|VD-967|VD-173JL|VD-974JL|VD-444JL|VD-9053JL|SD-917|SD-956|SD-957|SD-9075|SD-9076|SD-9082|VD-179JL|VD-354JL|VB-421S|VD-277JL|SD-101|SD-102|SD-108|SD-109|SD-110|SD-112|SD-117|SD-119|SD-129|SD-112|SD-129|SD-202|SD-204|SD-205|SD-213|SD-215|SD-216|SD-221|SD-222|SD-228|SD-234|SD-301|SD-309|SD-312|SD-314|SD-318|SD-320|SD-322|SD-324|SD-322|SD-401|SD-403|SD-412|SD-413|SD-414|SD-416|SD-417|SD-419|SD-420|SD-423|SD-420|SD-901|SD-904|SD-906|SD-907|VB-244S|SD-179|SD-277|SD-354|SD-444|SD-986|VB-113|ZD-144CT|ZD-154CT|ZD-156CT|ZD-161CT|ZD-166CT|ZD-170CT|ZD-172CT|ZD-177CT|ZD-154|ZD-156|ZD-156|ZD-165|ZD-166|ZD-172|ZD-248CT|ZD-250CT|ZD-262CT|ZD-289CT|ZD-250|ZD-262|ZD-344CT|ZD-349CT|ZD-352CT|ZD-355CT|ZD-435CT|ZD-352CT|ZD-945CT|ZD-9031C|ZD-9068C|ZD-9070C|VB-144S|VB-148S|VB-149S|ZD-277CT|ZD-354CT|ZD-444CT|ZD-9053C|VB-420)/', $doc->summary, $matches, PREG_OFFSET_CAPTURE)) {
                echo $doc->sku . "\t" . $doc->name . "\t" . $doc->summary . "\t" . $matches[0][0] . "<br>";
            }
//                $text = '/^SF-3001/';
//                if(preg_match($text,$doc->summary)){
//                    echo $doc->sku .'-摘要->'.$doc->summary. "<br>";
//                }
//            }
        }
        die();

    }

    public function device()
    {
        //do something when pc.
    }

    public function testamp()
    {
        return view('response.pages.testamp', (array)$this->view_component);
    }

    public function motor()
    {
        $products = [
            "3373429" => ["sku" => "t00065264", "motor" => "1341,1951,1551,460,2008"],
            "3373377" => ["sku" => "t00065212", "motor" => "1341,1951,1551,460,2008"],
            "3373430" => ["sku" => "t00065265", "motor" => "1341,1951,1551,460,2008"],
            "3373378" => ["sku" => "t00065213", "motor" => "1341,1951,1551,460,2008"],
            "3373431" => ["sku" => "t00065266", "motor" => "1341,1951,1551,460,2008"],
            "3373379" => ["sku" => "t00065214", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3373432" => ["sku" => "t00065267", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3373380" => ["sku" => "t00065215", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3385956" => ["sku" => "t00065505", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3385948" => ["sku" => "t00065497", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3385957" => ["sku" => "t00065506", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3385949" => ["sku" => "t00065498", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3373433" => ["sku" => "t00065268", "motor" => "1106,3927,3928,1128,1136,5420,1101,932"],
            "3373381" => ["sku" => "t00065216", "motor" => "1034,1033,1034,1033,1035,1036,1035,1040,1041,1042,1043,1047,1048,1049,1053,1558,1558,2064,2168,2168,2424"],
            "3373434" => ["sku" => "t00065269", "motor" => "1034,1033,1034,1033,1035,1036,1035,1040,1041,1042,1043,1047,1048,1049,1053,1558,1558,2064,2168,2168,2424"],
            "3373382" => ["sku" => "t00065217", "motor" => "1034,1033,1034,1033,1035,1036,1035,1040,1041,1042,1043,1047,1048,1049,1053,1558,1558,2064,2168,2168,2424"],
            "3705214" => ["sku" => "t00082564", "motor" => "1034,1033,1034,1033,1035,1036,1035,1040,1041,1042,1043,1047,1048,1049,1053,1558,1558,2064,2168,2168,2424"],
            "3705202" => ["sku" => "t00082552", "motor" => "1103,3313,2816,3782,3940,3941,2482,1352,3138,4048"],
            "3705213" => ["sku" => "t00082563", "motor" => "1103,3313,2816,3782,3940,3941,2482,1352,3138,4048"],
            "3705201" => ["sku" => "t00082551", "motor" => "1103,3313,2816,3782,3940,3941,2482,1352,3138,4048"],
            "3705220" => ["sku" => "t00082570", "motor" => "1103,3313,2816,3782,3940,3941,2482,1352,3138,4048"],
            "3705208" => ["sku" => "t00082558", "motor" => "859"],
            "3705219" => ["sku" => "t00082569", "motor" => "859"],
            "3705207" => ["sku" => "t00082557", "motor" => "859"],
            "3373435" => ["sku" => "t00065270", "motor" => "859"],
            "3373383" => ["sku" => "t00065218", "motor" => "1345,1345,1640,1640,1790,1798,1956,1983,2027,3312,2036,2046,2082,2083,2108,2271,2364,2393,2737,2738,3311,3781,3935,5014,5022,1982,1787,3314,3934,2108,2796,2797,3310,3321,3389,5726,464,464,1791,3135,3388,5605,882,883,1969,897,917,923"],
            "3373436" => ["sku" => "t00065271", "motor" => "1345,1345,1640,1640,1790,1798,1956,1983,2027,3312,2036,2046,2082,2083,2108,2271,2364,2393,2737,2738,3311,3781,3935,5014,5022,1982,1787,3314,3934,2108,2796,2797,3310,3321,3389,5726,464,464,1791,3135,3388,5605,882,883,1969,897,917,923"],
            "3373384" => ["sku" => "t00065219", "motor" => "1345,1345,1640,1640,1790,1798,1956,1983,2027,3312,2036,2046,2082,2083,2108,2271,2364,2393,2737,2738,3311,3781,3935,5014,5022,1982,1787,3314,3934,2108,2796,2797,3310,3321,3389,5726,464,464,1791,3135,3388,5605,882,883,1969,897,917,923"],
            "3385960" => ["sku" => "t00065509", "motor" => "1345,1345,1640,1640,1790,1798,1956,1983,2027,3312,2036,2046,2082,2083,2108,2271,2364,2393,2737,2738,3311,3781,3935,5014,5022,1982,1787,3314,3934,2108,2796,2797,3310,3321,3389,5726,464,464,1791,3135,3388,5605,882,883,1969,897,917,923"],
            "3385954" => ["sku" => "t00065503", "motor" => "1512,920,925,934,3513,941,942,1512,2054,934,3422,3513,941,3225"],
            "3385961" => ["sku" => "t00065510", "motor" => "1512,920,925,934,3513,941,942,1512,2054,934,3422,3513,941,3225"],
            "3385955" => ["sku" => "t00065504", "motor" => "1512,920,925,934,3513,941,942,1512,2054,934,3422,3513,941,3225"],
            "3705212" => ["sku" => "t00082562", "motor" => "1512,920,925,934,3513,941,942,1512,2054,934,3422,3513,941,3225"],
            "3705200" => ["sku" => "t00082550", "motor" => "859,859"],
            "3705211" => ["sku" => "t00082561", "motor" => "859,859"],
            "3705199" => ["sku" => "t00082549", "motor" => "859,859"],
            "3705218" => ["sku" => "t00082568", "motor" => "859,859"],
            "3705206" => ["sku" => "t00082556", "motor" => "1622,4396,4396,5271,876,893,893"],
            "3705217" => ["sku" => "t00082567", "motor" => "1622,4396,4396,5271,876,893,893"],
            "3705205" => ["sku" => "t00082555", "motor" => "1622,4396,4396,5271,876,893,893"],
            "3373437" => ["sku" => "t00065272", "motor" => "1622,4396,4396,5271,876,893,893"],
            "3373385" => ["sku" => "t00065220", "motor" => "1340,1341,1342,1713,1813,1833,1953,1954,3340,2222,2366,2901,2902,970,992,986,987,987,1055,1055,1055,1644,1644,1731,1817,1818,2015,2038,2038,2015,1046,2272,2273,2274,2274,2279,2399,2831,3195,3808,5016,2748,5555,2059,294,307,311,314,287,315,315,322,1553,1732,1988,2079,2210,2855,3807,424,426,433,448,450,452,3263,3264,452,3263,3264,469,472,473,474,963,963,1552,1701,3193,470,472,2747,644,660,665,665,669,687,968,4107,1270,1277,1271,1273,1278,1285,1290,1292,1293,1791,1791,2084,2184,2953,3135,3135,3388,3390,5007,1271,1271,3390,3135,3135,1292,1290,1291,2124,5189,3529,892"],
            "3373438" => ["sku" => "t00065273", "motor" => "1340,1341,1342,1713,1813,1833,1953,1954,3340,2222,2366,2901,2902,970,992,986,987,987,1055,1055,1055,1644,1644,1731,1817,1818,2015,2038,2038,2015,1046,2272,2273,2274,2274,2279,2399,2831,3195,3808,5016,2748,5555,2059,294,307,311,314,287,315,315,322,1553,1732,1988,2079,2210,2855,3807,424,426,433,448,450,452,3263,3264,452,3263,3264,469,472,473,474,963,963,1552,1701,3193,470,472,2747,644,660,665,665,669,687,968,4107,1270,1277,1271,1273,1278,1285,1290,1292,1293,1791,1791,2084,2184,2953,3135,3135,3388,3390,5007,1271,1271,3390,3135,3135,1292,1290,1291,2124,5189,3529,892"],
            "3373386" => ["sku" => "t00065221", "motor" => "1340,1341,1342,1713,1813,1833,1953,1954,3340,2222,2366,2901,2902,970,992,986,987,987,1055,1055,1055,1644,1644,1731,1817,1818,2015,2038,2038,2015,1046,2272,2273,2274,2274,2279,2399,2831,3195,3808,5016,2748,5555,2059,294,307,311,314,287,315,315,322,1553,1732,1988,2079,2210,2855,3807,424,426,433,448,450,452,3263,3264,452,3263,3264,469,472,473,474,963,963,1552,1701,3193,470,472,2747,644,660,665,665,669,687,968,4107,1270,1277,1271,1273,1278,1285,1290,1292,1293,1791,1791,2084,2184,2953,3135,3135,3388,3390,5007,1271,1271,3390,3135,3135,1292,1290,1291,2124,5189,3529,892"],
            "3705222" => ["sku" => "t00082572", "motor" => "1340,1341,1342,1713,1813,1833,1953,1954,3340,2222,2366,2901,2902,970,992,986,987,987,1055,1055,1055,1644,1644,1731,1817,1818,2015,2038,2038,2015,1046,2272,2273,2274,2274,2279,2399,2831,3195,3808,5016,2748,5555,2059,294,307,311,314,287,315,315,322,1553,1732,1988,2079,2210,2855,3807,424,426,433,448,450,452,3263,3264,452,3263,3264,469,472,473,474,963,963,1552,1701,3193,470,472,2747,644,660,665,665,669,687,968,4107,1270,1277,1271,1273,1278,1285,1290,1292,1293,1791,1791,2084,2184,2953,3135,3135,3388,3390,5007,1271,1271,3390,3135,3135,1292,1290,1291,2124,5189,3529,892"],
            "3705210" => ["sku" => "t00082560", "motor" => "1342,970,277,427,442,454,1287"],
            "3705221" => ["sku" => "t00082571", "motor" => "1342,970,277,427,442,454,1287"],
            "3705209" => ["sku" => "t00082559", "motor" => "1342,970,277,427,442,454,1287"],
            "3373439" => ["sku" => "t00065274", "motor" => "1342,970,277,427,442,454,1287"],
            "3373387" => ["sku" => "t00065222", "motor" => "1763,1764,1943,2810,3441,2856,2857,2858,3027,3278,3280,478,479,478,1617,1764,3278,3280,2856,2857,2858,3027,690,2020,1770,2033,4109"],
            "3373440" => ["sku" => "t00065275", "motor" => "1763,1764,1943,2810,3441,2856,2857,2858,3027,3278,3280,478,479,478,1617,1764,3278,3280,2856,2857,2858,3027,690,2020,1770,2033,4109"],
            "3373388" => ["sku" => "t00065223", "motor" => "1763,1764,1943,2810,3441,2856,2857,2858,3027,3278,3280,478,479,478,1617,1764,3278,3280,2856,2857,2858,3027,690,2020,1770,2033,4109"],
            "3373441" => ["sku" => "t00065276", "motor" => "1763,1764,1943,2810,3441,2856,2857,2858,3027,3278,3280,478,479,478,1617,1764,3278,3280,2856,2857,2858,3027,690,2020,1770,2033,4109"],
            "3373389" => ["sku" => "t00065224", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373442" => ["sku" => "t00065277", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373390" => ["sku" => "t00065225", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373443" => ["sku" => "t00065278", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373391" => ["sku" => "t00065226", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373444" => ["sku" => "t00065279", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373392" => ["sku" => "t00065227", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373445" => ["sku" => "t00065280", "motor" => "1152,1165,1167,1168,1153,1160,1161,1162,1163,2353,5610,2183,2315,2330,2315,2275,2412,2413,326,327,1756,2205,3332,3332,1995,3455,935,939,941"],
            "3373393" => ["sku" => "t00065228", "motor" => "324,471"],
            "3373446" => ["sku" => "t00065281", "motor" => "324,471"],
            "3373394" => ["sku" => "t00065229", "motor" => "324,471"],
            "3373447" => ["sku" => "t00065282", "motor" => "324,471"],
            "3373395" => ["sku" => "t00065230", "motor" => "2357,2357,2357,2357"],
            "3373448" => ["sku" => "t00065283", "motor" => "2357,2357,2357,2357"],
            "3373396" => ["sku" => "t00065231", "motor" => "2357,2357,2357,2357"],
            "3373449" => ["sku" => "t00065284", "motor" => "2357,2357,2357,2357"],
            "3373397" => ["sku" => "t00065232", "motor" => "2357,2357,2357,2357"],
            "3373450" => ["sku" => "t00065285", "motor" => "2357,2357,2357,2357"],
            "3373398" => ["sku" => "t00065233", "motor" => "2357,2357,2357,2357"],
            "3385962" => ["sku" => "t00065511", "motor" => "2357,2357,2357,2357"],
            "3385950" => ["sku" => "t00065499", "motor" => "984,371,1195,2237,2242,3396,1207,1336,1653,2234,2238,2243,2484,3394,2224,3395,2369,2242"],
            "3385963" => ["sku" => "t00065512", "motor" => "984,371,1195,2237,2242,3396,1207,1336,1653,2234,2238,2243,2484,3394,2224,3395,2369,2242"],
            "3385951" => ["sku" => "t00065500", "motor" => "984,371,1195,2237,2242,3396,1207,1336,1653,2234,2238,2243,2484,3394,2224,3395,2369,2242"],
            "3705216" => ["sku" => "t00082566", "motor" => "984,371,1195,2237,2242,3396,1207,1336,1653,2234,2238,2243,2484,3394,2224,3395,2369,2242"],
            "3705204" => ["sku" => "t00082554", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3705215" => ["sku" => "t00082565", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3705203" => ["sku" => "t00082553", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3373451" => ["sku" => "t00065286", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3373399" => ["sku" => "t00065234", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3373452" => ["sku" => "t00065287", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3373400" => ["sku" => "t00065235", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3373453" => ["sku" => "t00065288", "motor" => "154,182,2088,2286,4964,4967,364,369,2794,3788,3788"],
            "3373401" => ["sku" => "t00065236", "motor" => "2273,5006,2060,1910,2052,2057,2058,2257,2404,2405,2404,2405,2410,2418,286,294,311,321,5265,2851,275,289,2059,289,2059,289,2059,308,307,1793,1799,2080,2221,2276,2415,2416,2855,3282,3524,424,424,424,424,425,426,458,2276,1338,1655,1657,1809,1811,2359,2487,5650,1203,1204,1212,1812,1975,3797,2166,635,653,669,687,2025,1284,1294,1272,1352,3138,1786,5052,1786,5052,5675,5676,2061,4044,2201,2030,2389,920,2061,4044"],
            "3373454" => ["sku" => "t00065289", "motor" => "2273,5006,2060,1910,2052,2057,2058,2257,2404,2405,2404,2405,2410,2418,286,294,311,321,5265,2851,275,289,2059,289,2059,289,2059,308,307,1793,1799,2080,2221,2276,2415,2416,2855,3282,3524,424,424,424,424,425,426,458,2276,1338,1655,1657,1809,1811,2359,2487,5650,1203,1204,1212,1812,1975,3797,2166,635,653,669,687,2025,1284,1294,1272,1352,3138,1786,5052,1786,5052,5675,5676,2061,4044,2201,2030,2389,920,2061,4044"],
            "3373402" => ["sku" => "t00065237", "motor" => "2273,5006,2060,1910,2052,2057,2058,2257,2404,2405,2404,2405,2410,2418,286,294,311,321,5265,2851,275,289,2059,289,2059,289,2059,308,307,1793,1799,2080,2221,2276,2415,2416,2855,3282,3524,424,424,424,424,425,426,458,2276,1338,1655,1657,1809,1811,2359,2487,5650,1203,1204,1212,1812,1975,3797,2166,635,653,669,687,2025,1284,1294,1272,1352,3138,1786,5052,1786,5052,5675,5676,2061,4044,2201,2030,2389,920,2061,4044"],
            "3373455" => ["sku" => "t00065290", "motor" => "2273,5006,2060,1910,2052,2057,2058,2257,2404,2405,2404,2405,2410,2418,286,294,311,321,5265,2851,275,289,2059,289,2059,289,2059,308,307,1793,1799,2080,2221,2276,2415,2416,2855,3282,3524,424,424,424,424,425,426,458,2276,1338,1655,1657,1809,1811,2359,2487,5650,1203,1204,1212,1812,1975,3797,2166,635,653,669,687,2025,1284,1294,1272,1352,3138,1786,5052,1786,5052,5675,5676,2061,4044,2201,2030,2389,920,2061,4044"],
            "3373403" => ["sku" => "t00065238", "motor" => "1727,1728,2045,3398,3399,3400,1729,1730,2060,2024,2267,2268,2270,2414,2415,254,255,257,298,299,302,2267,2851,295,1587,1793,2735,464,1211,1213,1809,1932,2873,3499,1261,1977,1978,1979,2193,2361,2362,2362,2387,2419,2745,3407,5186,2193,2909,2361,1977,1978,1259,1303,1792,1979,2055,2055,2201,2388,2417,5015,5024,823,876,897,5024,2388,5015,2389,3789,2389,3789"],
            "3373456" => ["sku" => "t00065291", "motor" => "2273,5006,2060,1910,2052,2057,2058,2257,2404,2405,2404,2405,2410,2418,286,294,311,321,5265,2851,275,289,2059,289,2059,289,2059,308,307,1793,1799,2080,2221,2276,2415,2416,2855,3282,3524,424,424,424,424,425,426,458,2276,1338,1655,1657,1809,1811,2359,2487,5650,1203,1204,1212,1812,1975,3797,2166,635,653,669,687,2025,1284,1294,1272,1352,3138,1786,5052,1786,5052,5675,5676,2061,4044,2201,2030,2389,920,2061,4044"],
            "3373404" => ["sku" => "t00065239", "motor" => "1727,1728,2045,3398,3399,3400,1729,1730,2060,2024,2267,2268,2270,2414,2415,254,255,257,298,299,302,2267,2851,295,1587,1793,2735,464,1211,1213,1809,1932,2873,3499,1261,1977,1978,1979,2193,2361,2362,2362,2387,2419,2745,3407,5186,2193,2909,2361,1977,1978,1259,1303,1792,1979,2055,2055,2201,2388,2417,5015,5024,823,876,897,5024,2388,5015,2389,3789,2389,3789"],
            "3385958" => ["sku" => "t00065507", "motor" => "1727,1728,2045,3398,3399,3400,1729,1730,2060,2024,2267,2268,2270,2414,2415,254,255,257,298,299,302,2267,2851,295,1587,1793,2735,464,1211,1213,1809,1932,2873,3499,1261,1977,1978,1979,2193,2361,2362,2362,2387,2419,2745,3407,5186,2193,2909,2361,1977,1978,1259,1303,1792,1979,2055,2055,2201,2388,2417,5015,5024,823,876,897,5024,2388,5015,2389,3789,2389,3789"],
            "3385952" => ["sku" => "t00065501", "motor" => "2208,2831,1794,2053,2256,2363,2390,2391,2395,2734,310,320,4988,4391,4392,4930,2363,320,956,2044,3801,1733,3494,621,1512,2026,2200,937,1512,2054"],
            "3385959" => ["sku" => "t00065508", "motor" => "2208,2831,1794,2053,2256,2363,2390,2391,2395,2734,310,320,4988,4391,4392,4930,2363,320,956,2044,3801,1733,3494,621,1512,2026,2200,937,1512,2054"],
            "3385953" => ["sku" => "t00065502", "motor" => "2208,2831,1794,2053,2256,2363,2390,2391,2395,2734,310,320,4988,4391,4392,4930,2363,320,956,2044,3801,1733,3494,621,1512,2026,2200,937,1512,2054"],
            "3373457" => ["sku" => "t00065292", "motor" => "2208,2831,1794,2053,2256,2363,2390,2391,2395,2734,310,320,4988,4391,4392,4930,2363,320,956,2044,3801,1733,3494,621,1512,2026,2200,937,1512,2054"],
            "3373405" => ["sku" => "t00065240", "motor" => "2266,2035,1513,2255,302,1784,464,1195,1199,1653,1208,1208,1336,1344,2223,2226,2245,2248,2484,5643,2246,2239,1201,1778,1779,823,840,841,843,871,871,946"],
            "3373458" => ["sku" => "t00065293", "motor" => "2266,2035,1513,2255,302,1784,464,1195,1199,1653,1208,1208,1336,1344,2223,2226,2245,2248,2484,5643,2246,2239,1201,1778,1779,823,840,841,843,871,871,946"],
            "3373406" => ["sku" => "t00065241", "motor" => "2266,2035,1513,2255,302,1784,464,1195,1199,1653,1208,1208,1336,1344,2223,2226,2245,2248,2484,5643,2246,2239,1201,1778,1779,823,840,841,843,871,871,946"],
            "3373459" => ["sku" => "t00065294", "motor" => "2266,2035,1513,2255,302,1784,464,1195,1199,1653,1208,1208,1336,1344,2223,2226,2245,2248,2484,5643,2246,2239,1201,1778,1779,823,840,841,843,871,871,946"],
            "3373407" => ["sku" => "t00065242", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"],
            "3373460" => ["sku" => "t00065295", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"],
            "3373408" => ["sku" => "t00065243", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"],
            "3373471" => ["sku" => "t00065306", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"],
            "3373419" => ["sku" => "t00065254", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"],
            "3373472" => ["sku" => "t00065307", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"],
            "3373420" => ["sku" => "t00065255", "motor" => "3489,1795,1036,1795,1036,1171,1970,5856,5857,325,1799,2252,1799,1799,1937,1290"]
        ];
        foreach ($products as $product_id => $product) {
            $product_sku = $product['sku'];
            $motor_ids = explode(",", $product['motor']);
            $motors = \DB::connection('smax_warehouse')->table('core_motors')->select('id', 'identifier')->whereIn('id', $motor_ids)->get();
            foreach ($motors as $key => $motor) {
                $motor_url_rewrite = $motor->identifier;
                $idx = \DB::connection('smax_warehouse')->table('idx_product_motor')
                    ->where('product_id', $product_id)
                    ->where('motor_id', $motor->id)
                    ->where('identifier_sku', $product_sku)
                    ->where('identifier_motor', $motor_url_rewrite)
                    ->first();
                if (!$idx) {
                    $product_motots = ['product_id' => $product_id, 'motor_id' => $motor->id, 'identifier_sku' => $product_sku, 'identifier_motor' => $motor_url_rewrite, 'status_id' => '9'];
                    $product_motots_insert = \DB::connection('smax_warehouse')->table('idx_product_motor')->insert($product_motots);
                }
            }
        }

    }

    public function queryProduct(Request $request)
    {
        $sku = "";
        $result = false;
        if ($request->has('sku')) {
            $sku = $request->get('sku');
            $result = DB::connection('eg_product')->table('products')
                ->join('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
                ->join('product_images', 'products.id', '=', 'product_images.product_id')
                ->select('products.name as product_name', 'manufacturers.name as brand_name', 'product_images.image')
                ->where('products.sku', '=', $sku)
                ->first();
        }

        return view('backend.pages.barcode.product', ['result' => $result, 'sku' => $sku]);
    }

    public function barcode()
    {
        $result = DB::connection('eg_product')->table('product_barcodes')->get();

        return view('backend.pages.barcode.product_barcodes', ['result' => $result]);
    }

    public function insertBarcode(Request $request)
    {
        $sku = $request->get('sku');
        $barcode = $request->get('barcode');

        if ($sku != '' && $barcode != '') {
            $test = DB::connection('eg_product')->table('product_barcodes')
                ->insert(['sku' => $sku, 'barcode' => $barcode]);
        }

        return redirect()->route('backend-barcode');
    }

    public function testWeekly(){
        $week =  date('W')+1;
        $Campaign = Campaign::select('url_rewrite')
        ->where('week',$week)
        ->where('active',1)
        ->first();

        $url = "https://www.webike.tw/campaigns/racing?noCache=true";        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public function testqa(){

        // $Source = Source::where('source_type','=','MorphFinalization')->get();

        // foreach($Source as $qa){

        //     $ticket = PsQaFinalization::select(\DB::raw('qats.ticket_ids'))
        //     ->join('content_qa_tasks as qat','content_qa_finalizations.task_id','=','qat.id')
        //     ->join('content_qa_ticket_sources as qats','qat.source_id','=','qats.id')
        //     ->where('content_qa_finalizations.id',$qa->source_id)
        //     ->first();

        //     Source::insert(['source_type' => 'MorphTicket','source_id' => $ticket->ticket_ids,'question_id' => $qa->question_id,'created_at' => date('Y-m-d H:i::s'),'updated_at' => date('Y-m-d H:i::s')]);

        // }
        // echo "Ok";

        // ini_set("memory_limit","3072M");
        // set_time_limit(0);

        // $Answers = Answer::select(\DB::raw('question_id,id'))->get();
        // foreach($Answers as $Answer){
        //     $Source = Source::select(\DB::raw('source_id','id'))
        //     ->where('question_id',$Answer->question_id)
        //     ->where('source_type','=','MorphFinalization')
        //     ->first();

        //     $ticket = PsQaFinalization::select(\DB::raw('qats.ticket_ids'))
        //     ->join('content_qa_tasks as qat','content_qa_finalizations.task_id','=','qat.id')
        //     ->join('content_qa_ticket_sources as qats','qat.source_id','=','qats.id')
        //     ->where('content_qa_finalizations.id',$Source->source_id)
        //     ->first();

        //     $relation = DB::connection('eg_zero')->table('ticket_relations')
        //     ->select('relation_id')
        //     ->where('relation_type', '=', 'MorphProduct')
        //     ->where('ticket_id',$ticket->ticket_ids)
        //     ->first();

        //     $qaRelation = new qaRelation;
        //     $qaRelation->product_id = $relation->relation_id;
        //     $qaRelation->save();

        //     $A = Answer::where('id',$Answer->id)->update(['relation_id' => $qaRelation->id]);
        // }

        echo 'ok';
    }

    public function thproduct(){
        dd(123);
        ini_set("memory_limit","3072M");
        set_time_limit(0);
        $products = \Cache::tags(['th_procduct'])->remember('th_procduct', 86400, function () {
              $products = \DB::connection('smax_warehouse')->select('select pm.product_id as th_product_id ,cp.status_id as rcj_status ,pm.`code`,cp.id as rcj_product_id FROM `product_modify_text1` as pm
                    join core_product as cp
                    on pm.`code` = cp.`code`
                    where cp.p_country <> "B"
                    and cp.p_country <> "T"
                    and pm.rewrite_text = cp.brand_id
                    and cp.type_id = 0 ');
                return $products;
        });


      foreach($products as $product){
            // $this->arrangeProductDescription($product);
            $this->arrangeProductImage($product);
            // $this->arrangeProductMotor($product);
        }
        echo "Ok";
    }

    private  function arrangeProductDescription($product){
            
            $rcj_product_description = \DB::connection('smax_warehouse')
                ->table('ref_product_description')
                ->where('product_id',$product->rcj_product_id)
                ->first();

            $th_product_description = \DB::connection('smax_warehouse')
                ->table('ref_product_description')
                ->where('product_id',$product->th_product_id)
                ->first();

            $description_data = [];
            if($rcj_product_description && $product->rcj_status >= 6){

                if($rcj_product_description->remarks_trans_flg == 9 or $rcj_product_description->remarks_trans_flg == 5){
                    $description_data['remarks'] = $rcj_product_description->remarks;
                    $description_data['remarks_trans_flg'] = $rcj_product_description->remarks_trans_flg;
                }
                if($rcj_product_description->summary_trans_flg == 9 or $rcj_product_description->summary_trans_flg == 5){
                    $description_data['summary'] = $rcj_product_description->summary;
                    $description_data['summary_trans_flg'] = $rcj_product_description->summary_trans_flg;
                }
                if($rcj_product_description->sentence_trans_flg == 9 or $rcj_product_description->sentence_trans_flg == 5){
                    $description_data['sentence'] = $rcj_product_description->sentence;
                    $description_data['sentence_trans_flg'] = $rcj_product_description->sentence_trans_flg;
                }
                if($rcj_product_description->caution_trans_flg == 9 or $rcj_product_description->caution_trans_flg == 5){
                    $description_data['caution'] = $rcj_product_description->caution;
                    $description_data['caution_trans_flg'] = $rcj_product_description->caution_trans_flg;
                }

                $description_data['updated_at'] = date('Y-m-d H:i:s');

                if($description_data){
                    if($th_product_description){
                        $th_product_description = \DB::connection('smax_warehouse')
                        ->table('ref_product_description')
                        ->where('product_id',$product->th_product_id)
                        ->update($description_data);
                    }else{
                        $description_data['product_id'] = $product->th_product_id;
                        $description_data['status_id'] = 9;
                        $description_data['created_at'] = date('Y-m-d H:i:s');

                        $th_product_description = \DB::connection('smax_warehouse')
                        ->table('ref_product_description')
                        ->insert($description_data);

                    }
                }

            }

    }

    private function arrangeProductImage($product){

        $rcj_product_images = \DB::connection('smax_warehouse')
            ->table('ref_product_images')
            ->where('product_id',$product->rcj_product_id)
            ->get();

        $th_product_images = \DB::connection('smax_warehouse')
            ->table('ref_product_images')
            ->where('product_id',$product->th_product_id)
            ->get();


        if($rcj_product_images){
            if($th_product_images){
                $th_product_images = \DB::connection('smax_warehouse')
                ->table('ref_product_images')
                ->where('product_id',$product->th_product_id)
                ->delete();
            }
            $this->insetImage($rcj_product_images,$product->th_product_id);
        }

    }

    private function insetImage($rcj_product_images,$th_product_id){

        $image_data = [];
        foreach($rcj_product_images as $rcj_images){
            foreach($rcj_images as $image_key => $images){
                if($image_key != 'id'){
                    $image_data[$image_key] = $images;
                }
            }

            $image_data['product_id'] = $th_product_id;
            $image_data['created_at'] = date('Y-m-d H:i:s');
            $image_data['updated_at'] = date('Y-m-d H:i:s');
            
            if($image_data){
                $th_product_image = \DB::connection('smax_warehouse')
                ->table('ref_product_images')
                ->insert($image_data);
            }
        }
    }

    private function arrangeProductMotor($product){

        $rcj_product_motor = \DB::connection('smax_warehouse')
            ->table('idx_product_motor')
            ->where('product_id',$product->rcj_product_id)
            ->get();

        $th_product_motor = \DB::connection('smax_warehouse')
            ->table('idx_product_motor')
            ->where('product_id',$product->th_product_id)
            ->get();


        $rcj_product_motor_ids = [];
        if($rcj_product_motor){
            $rcj_product_motor_ids = array_pluck($rcj_product_motor,'motor_id');
        }

        $th_product_motor_ids = [];
        if($th_product_motor){
            $th_product_motor_ids = array_pluck($th_product_motor,'motor_id');
        }
        
        if($rcj_product_motor_ids){

            if($th_product_motor_ids){
                foreach($rcj_product_motor_ids as $motor_ids_key =>  $rcj_product_motor_id){
                    if(!in_array($rcj_product_motor_id,$th_product_motor_ids)){
                        $motor_data = [];
                        foreach($rcj_product_motor[$motor_ids_key] as $motor_key => $rcj_motor){
                            if($motor_key != 'id'){
                                $motor_data[$motor_key] = $rcj_motor;
                            }
                        }

                        $this->insertProductMotor($motor_data,$product->th_product_id);
                    }
                }

            }else{
                foreach($rcj_product_motor as $rcj_motor){
                    $motor_data = [];
                    foreach($rcj_motor as $motor_key => $motor){
                       if($motor_key != 'id'){
                            $motor_data[$motor_key] = $motor;
                        }

                        $this->insertProductMotor($motor_data,$product->th_product_id);
                    }
                }

            }

        }
        
    }

    private function insertProductMotor($motor_data,$th_product_id){
            
        $motor_data['product_id'] = $th_product_id;
        $motor_data['created_at'] = date('Y-m-d H:i:s');
        $motor_data['updated_at'] = date('Y-m-d H:i:s');

        \DB::connection('smax_warehouse')
        ->table('idx_product_motor')
        ->insert($motor_data);
    }

}