<?php

namespace App\Providers;

use Ecommerce\Core\Auth\CustomUserProvider;
use Ecommerce\Core\Auth\Hasher as MyHasher;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        \Auth::provider('custom', function() {
            return new CustomUserProvider( new MyHasher() , config('auth.providers.users.model'));
        });

    }
}
