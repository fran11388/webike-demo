<?php

namespace App\Providers;


use App\Observers\OrderObserver;
use App\Observers\ProductStockObserver;
use Everglory\Models\Order;
use Illuminate\Support\ServiceProvider;
use Everglory\Models\Order\Item as OrderItem;
use App\Observers\OrderItemObserver;
use Everglory\Models\Product\Stock as ProductStock;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);
        OrderItem::observe(OrderItemObserver::class);

        Relation::morphMap([
            'MorphOrder' => \Everglory\Models\Order::class,
            'MorphRex' => \Everglory\Models\Rex::class,
            'MorphOrderItem' => \Everglory\Models\Order\Item::class,
            'MorphEstimate' => \Everglory\Models\Estimate::class,
            'MorphEstimateItem' => \Everglory\Models\Estimate\Item::class,
//            'MorphGenuinepart' => \Everglory\Models\Estimate\Item::class,
            'MorphProduct' => \Everglory\Models\Product::class,
            'MorphMitumori' => \Everglory\Models\Mitumori::class,
            'MorphGroupbuy' => \Everglory\Models\Groupbuy::class,
            'customer' => \Everglory\Models\MotoMarket\Customer::class,
        ]);


        if(isset($_SERVER['REMOTE_ADDR'])){
            if( ($_SERVER['REMOTE_ADDR'] == '122.116.103.32')  or ($_SERVER['REMOTE_ADDR'] == '49.158.20.135') or (strpos($_SERVER['REMOTE_ADDR'], '10.0.') !== false) ){
                if(request()->get('debugbar')){
                    $this->app['config']->set('debugbar.enabled' , true);
                }else{
                    $this->app['config']->set('debugbar.enabled' , false);
                }
            }
        }
        if(isset($_GET['debug-off'])){
            $this->app['config']->set('debugbar.enabled' , false);
        }
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }




    }
}
