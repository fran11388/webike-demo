<?php

namespace App\Observers;

use Everglory\Models\Mission;
use Everglory\Models\Customer\Mission as CustomerMission;
use Everglory\Models\Order;
use Everglory\Models\Unusual;
use Everglory\Models\Coupon;
use Everglory\Models\Customer\Reward as CustomerReward;
use Everglory\Models\Order\Reward as OrderReward;

class OrderObserver
{
    public function created(Order $order)
    {
        $customer = $order->customer;
        $cartPoints = $order->points_current;

        if($customer and !$order->edit_increment ){
            $points = $customer->points;
            if ( !$points ){
                $points = new CustomerReward();
                $points->customer_id = $customer->id;
            }

            $points->points_current -= $order->rewardpoints_amount;
            $points->points_used += $order->rewardpoints_amount;
            $points->points_waiting +=$cartPoints;
            $points->save();

            if( $points->points_current < 0 ){
                $unusual = new Unusual();
                $unusual->unusual_id = $customer->id;
                $unusual->unusual_type = 'customer';
                $unusual->info = '[客戶點數異常]客戶點數:'.$points->points_current.'點';
                $unusual->created_at = date('Y-m-d H:i:s');
                $unusual->save();
            }

            if ($order->rewardpoints_amount > 0 ){
                $rewards = new OrderReward();
                $rewards->customer_id  = $customer->id;
                $rewards->order_id = $order->id;
                $rewards->points_spend = $order->rewardpoints_amount;
                $rewards->save();

                $currents = OrderReward::where('customer_id',$customer->id)->where('status',1)->where('points_current','>',0)->whereRaw('points_current <> points_usage')->where('end_date','>',date('Y-m-d'))->orderBy('end_date')->get();
                //points_usage calculate (prevent self order)
                pointUsageCalculate($currents,$rewards);
            }

            if( $order->rewardpoints_amount < 0 ){
                $unusual = new Unusual();
                $unusual->unusual_id = $order->id;
                $unusual->unusual_type = 'order';
                $unusual->info = '[訂單點數異常]訂單點數:'.$order->rewardpoints_amount.'點';
                $unusual->created_at = date('Y-m-d H:i:s');
                $unusual->save();
            }

            if ( $cartPoints >  0 ){
                $rewards = new OrderReward();
                $rewards->customer_id  = $customer->id;
                $rewards->order_id = $order->id;
                $rewards->points_current = $cartPoints;
                $dt = new \DateTime();
                $rewards->start_date = $dt->format('Y-m-d');
                $dt->add(\DateInterval::createFromDateString('+1 years'));
                $rewards->end_date = $dt->format('Y-m-d');
                $rewards->status = 0;
                $rewards->save();
            }

            if ($order->coupon_uuid){
                $coupon = Coupon::where( 'customer_id' , $order->customer_id)
                    ->where('uuid' , $order->coupon_uuid)
                    ->first();
                if ($coupon){
                    $coupon->order_id = $order->id;
                    $coupon->save();
                }
            }
        }
    }


    public function updating( Order $order){
        $customer = $order->customer;
        $original = $order->getOriginal();
        if($customer){
            $points = $customer->points;
            if ( !$points ){
                $points = new CustomerReward();
                $points->customer_id = $customer->id;
            }

            if ($order->status_id != $original['status_id']) {

                if ($original['status_id'] == 0){
                    $points->points_current -= $order->rewardpoints_amount;
                    $points->points_used += $order->rewardpoints_amount;
                    $points->points_waiting +=$order->points_current;
                    $points->save();
                }

            }
        }
    }
}