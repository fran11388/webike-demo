<?php

namespace App\Observers;

use Everglory\Constants\ProductType;
use Everglory\Models\Order\Item;
use Everglory\Models\Order\Item\Detail as ItemDetail;
use Ecommerce\Accessor\ProductAccessor;

class OrderItemObserver
{
    public function created(Item $item)
    {
        \DB::transaction(function()use( &$item ){
            $attributes = [
                'category_name_path',
                'category_url_path',
                'manufacturer_name',
                'manufacturer_url_rewrite',
                'models',
                'motor_names',
                'motor_url_rewrites',
                'original_price',
                'normal_price',
                'product_name',
                'product_options',
                'product_thumbnail'
            ];


            ItemDetail::where('item_id', $item->id)->delete();
            $product = new ProductAccessor($item->product, false);
            $product_model = $product;
            if($item->product_type == ProductType::ADDITIONAL){
                $product_model = new ProductAccessor($item->mainProduct, false);
            }
            $category = $product_model->getMpttCategory();
            $manufacturer = $product->manufacturer;
            $fitModels = $product->getFitModels(false);
            $motors = $product->motors->pluck('name','url_rewrite')->all();
            if( $motors ){
                list($motor_url_rewrites, $motor_names) = array_divide($motors);
            }

            foreach ($attributes as $key => $attribute) {
                $detail = new ItemDetail;

                switch ($attribute) {
                    case 'category_name_path':
                        $detail->value = ($category ? $category->name_path : '');
                        break;
                    case 'category_url_path':
                        $detail->value = ($category ? $category->url_path : '');
                        break;
                    case 'manufacturer_name':
                        $detail->value = ($manufacturer ? $manufacturer->name : '');
                        break;
                    case 'manufacturer_url_rewrite':
                        $detail->value = ($manufacturer ? $manufacturer->url_rewrite : '');
                        break;
                    case 'models':
                        $models = array();
                        if($fitModels){
                            foreach ($fitModels as &$fitModel){
                                $models[] = $fitModel->maker . '$' . $fitModel->model . '$' . $fitModel->style;
                            }
                        }
                        $detail->value = (count($models) ? implode('|', $models) : '');
                        break;
                    case 'motor_url_rewrites':
                    case 'motor_names':
                        $detail->value = (isset($$attribute) ? implode('|', $$attribute) : '');
                        break;
                    case 'original_price':
                        $detail->value = ($product ? $product->getPrice() : '');
                        break;
                    case 'normal_price':
                        $detail->value = ($product ? $product->getNormalPrice() : '');
                        break;
                    case 'product_name':
                        $detail->value = ($product ? $product->name : '');
                        break;
                    case 'product_options':
                        $options = array();
                        if($product){
                            foreach ($product->options as &$option){
                                $options[] = $option->label->name . '$' . $option->name;
                            }
                        }
                        $detail->value = ( ($product and count($options)) ? implode('|', $options) : '');
                        break;
                    case 'product_thumbnail':
                        $detail->value = $product->getThumbnail();
                        break;
                    default:
                        break;
                }
                $detail->item_id = $item->id;
                $detail->attribute = $attribute;
                $detail->save();

            }

        });
    }
}