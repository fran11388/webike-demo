<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('/product/manufacturer/model', array('uses' => 'APIController@getProductManufacturerModel', 'as' => 'api-product-manufacturer-model'));

Route::any('motor/info', array('uses' => 'APIController@motor_info'));

Route::get('/motor/displacements', array('uses' => 'APIController@getMotorManufacturerDisplacement'));

Route::get('/motor/model', array('uses' => 'APIController@getMotorDisplacementModel'));

Route::get('/motor/manufacturer/model', ['uses' => 'APIController@getMotorManufacturerModel', 'as' => 'api-motor-manufacturer-model']);


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('suggest', array('as' => 'api-suggest', 'uses' => 'APIController@suggest'));


Route::get('stock', array('as' => 'api-stock', 'uses' => 'APIController@getStock'));

Route::get('/qa-center/create', array('uses' => 'APIController@qaCenterCreate'));

Route::get('/qa-center/check', array('uses' => 'APIController@qaCenterCheck'));

Route::get('/qa-center/qadata', array('uses' => 'APIController@qaCenterQadata'));

Route::post('/qa-center/update', array('uses' => 'APIController@qaCenterUpdate'));

Route::post('/qa-center/updateQaSort', array('uses' => 'APIController@qaCenterUpdateQaSort'));

Route::get('/qa-center/ticketQa', array('uses' => 'APIController@qaCenterTicketQa'));


Route::post('/qa-center/deleteQa', array('uses' => 'APIController@qaCenterDeleteQa'));

Route::get('/product-options', array('as' => 'api-product-options', 'uses' => 'APIController@productOptions')); //選擇肢API

Route::get('/account/sendVerifyMail', array(  'uses' => 'APIController@sendVerifyMail', 'as'=>'api-send-verify-email' ));

