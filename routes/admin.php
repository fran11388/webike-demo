<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses' => 'Backend\IndexController@getIndex','as' => 'backend']);

Route::group( array( 'prefix' => 'assortment' ), function(){
    Route::get('/', ['uses' => 'Backend\AssortmentController@getIndex','as' => 'backend-assortment']);
    Route::get('/edit', ['uses' => 'Backend\AssortmentController@getAssortmentEdit','as' => 'backend-assortment-edit']);
    Route::get('/edit/block', ['uses' => 'Backend\AssortmentController@getAssortmentEdit','as' => 'backend-assortment-edit']);
    Route::post('/edit', ['uses' => 'Backend\AssortmentController@getAssortmentEdit','as' => 'backend-assortment-edit']);
});

Route::group(['prefix' => 'qa'], function(){
    Route::get('/', ['uses' => 'Backend\QaController@index','as' => 'backend-qa']);
    Route::get('/search', ['uses' => 'Backend\QaController@search','as' => 'backend-qa-search']);
    Route::get('/edit', ['uses' => 'Backend\QaController@edit','as' => 'backend-qa-edit']);
    Route::post('/edit', ['uses' => 'Backend\QaController@write','as' => 'backend-qa-edit']);

    Route::group(['prefix' => 'tickets'], function(){
        Route::get('/', ['uses' => 'Backend\QaController@tickets','as' => 'backend-qa-tickets']);
        Route::get('/import', ['uses' => 'Backend\QaController@import','as' => 'backend-qa-tickets-import']);
    });
});
