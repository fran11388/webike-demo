<?php
$namespace = null;
if (!\Ecommerce\Core\Agent::isNotPhone()) {
    $namespace = 'Mobile';
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//HomeController ( new home page / shopping home page)

    if(str_contains(request()->getHost(),"amp")){
        if(config('app.production')){
            \URL::forceSchema('https');
        }
        \URL::forceRootUrl(env('APP_URL', 'https://www.webike.tw'));
    }


    Route::post('/device', ['uses' => 'HomeController@device', 'as' => 'home-device']);
    Route::get('/test', ['uses' => 'HomeController@test', 'as' => 'test']);
    Route::get('/testamp',['uses' => 'HomeController@testamp', 'as' => 'testamp' ]);
    Route::get('getdata', ['uses' => 'HomeController@getdata', 'as' => 'amp-getdata']);





Route::group(['namespace' => $namespace], function () {


    // Route::get('/mt/6545/amp',['uses' => 'SummaryController@indexAmp','as' => 'Summary-amp']);
    // Route::get('/ca/4000/amp',['uses' => 'SummaryController@indexAmp','as' => 'Summary-amp-ca4000']);
    // Route::get('/ca/8000/amp',['uses' => 'SummaryController@indexAmp','as' => 'Summary-amp-ca8000']);
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
    Route::get('/shopping', ['uses' => 'HomeController@shopping', 'as' => 'shopping']);
    Route::get('/sd/info', ['uses' => 'ProductDetailController@info', 'as' => 'product-detail-info']);
    Route::get('/sd/{url_rewrite}', ['uses' => 'ProductDetailController@index', 'as' => 'product-detail']);
    Route::post('/getAreaByCount', ['uses' => 'ProductDetailController@getAreaByCount', 'as' => 'getAreaByCountry']);

    Route::post('/getShippingInfo', ['uses' => 'ProductDetailController@getShippingInfo', 'as' => 'getShippingInfo']);
    Route::post('/deleteWishlist', ['uses' => 'ProductDetailController@deleteWishlist', 'as' => 'product-delete-wishlist']);
    Route::post('/insertWishlist', ['uses' => 'ProductDetailController@insertWishlist', 'as' => 'product-insert-wishlist']);
    Route::post('/CheckWishlistItem', ['uses' => 'ProductDetailController@CheckWishlistItem', 'as' => 'product-CheckWishlistItem']);
    Route::get('/parts/{section?}', ['uses' => 'ListController@index', 'as' => 'parts'])->where(['section' => '.*']);

    Route::get('motor', ['uses' => 'MotorController@index', 'as' => 'motor']);
    Route::get('mf/{manufacturer}/{displacement}', ['uses' => 'MotorController@manufacturer', 'as' => 'motor-manufacturer']);

    
    Route::get('/amp/mt/{url_rewrite}/tab/service', ['uses' => 'MotorController@serviceAmp','as'=>'amp-motor-service']);
    Route::get('/amp/mt/{url_rewrite}/tab/service/{motor_catalogue_id}/amp', ['uses' => 'MotorController@serviceAmp','as'=>'amp-motor-service-catalogue']);
    Route::get('/amp/mt/{url_rewrite}/tab/review', ['uses' => 'MotorController@review','as'=>'amp-motor-review']);
    Route::get('/amp/mt/{url_rewrite}/tab/top', ['uses' => 'MotorController@top','as'=>'amp-motor-top']);
    
    Route::get('/mt/{url_rewrite}/tab/service', ['uses' => 'MotorController@service','as'=>'motor-service']);
    Route::get('mt/{url_rewrite}/tab/service/{motor_catalogue_id?}', ['uses' => 'MotorController@service','as'=>'motor-service']);
    Route::get('mt/{url_rewrite}/tab/review', ['uses' => 'MotorController@review','as'=>'motor-review']);
    Route::get('mt/{url_rewrite}/tab/top', ['uses' => 'MotorController@top','as'=>'motor-top']);

    Route::group(['prefix' => 'review' ], function(){

        Route::get('/list/{url_path?}', ['uses' => 'ReviewController@search', 'as' => 'review-search']);
        Route::post('/upload', ['uses' => 'ReviewController@upload', 'as' => 'review-upload'])->middleware('auth');

        Route::get('/article/{id}', ['uses' => 'ReviewController@show', 'as' => 'review-show']);
        Route::get('/writing/done', ['uses' => 'ReviewController@writingDone', 'as' => 'review-writing-done'])->middleware('auth');
        Route::get('/writing/{url_rewrite}', ['uses' => 'ReviewController@create', 'as' => 'review-create'])->middleware('auth');
        Route::get('/', ['uses' => 'ReviewController@index', 'as' => 'review']);
    });
    Route::group(['prefix' => 'groupbuy'], function () {
        Route::get('/', ['uses' => 'GroupBuyController@index', 'as' => 'groupbuy']);
        Route::get('/done', ['uses' => 'GroupBuyController@done', 'as' => 'groupbuy-estimate-done']);
        Route::post('/estimate', ['uses' => 'GroupBuyController@estimate', 'as' => 'groupbuy-estimate']);
    });
    Route::group(['prefix' => 'mitumori'], function () {
        Route::get('/', ['uses' => 'MitumoriController@index', 'as' => 'mitumori']);
        Route::post('/estimate', ['uses' => 'MitumoriController@estimate', 'as' => 'mitumori-estimate']);
        Route::get('/done', ['uses' => 'MitumoriController@done', 'as' => 'mitumori-estimate-done']);
    });

    
    Route::group( ['prefix' => 'customer'], function() {


        Route::get('/rule/pointinfo/amp', ['uses' => 'CustomerController@getPointinfoAmp', 'as' => 'customer-pointinfo-amp']);

        Route::group( ['prefix' => 'history'], function() {
            Route::group(['middleware' => 'auth'], function() {

                Route::group(['prefix'=>'purchased'],function(){
//                    dd(4444);
//                    Route::get('/', ['uses' => 'Customer\HistoryController@purchasedHistory', 'as' => 'customer-history-purchased']);
                });

            });
        });
    });


    Route::get('/cart', ['uses' => 'CartController@index', 'as' => 'cart'])->middleware('auth');
    Route::post('/cart/setCoupon', ['uses' => 'CartController@coupon', 'as' => 'cart-coupon']);
    Route::post('/cart/setPoints', ['uses' => 'CartController@points', 'as' => 'cart-points']);
    Route::post('/cart/updateItem', ['uses' => 'CartController@update', 'as' => 'cart-update'])->middleware('auth_post');
    Route::get('/cart/updateItem', ['uses' => 'CartController@update'])->middleware('auth_post');
    Route::post('/cart/removeItem', ['uses' => 'CartController@remove', 'as' => 'cart-remove']);
    Route::post('/cart/checkProduct', ['uses' => 'CartController@checkProduct', 'as' => 'cart-check-product']);
    Route::post('/cart/reloadAddition', ['uses' => 'CartController@reloadAddition', 'as' => 'cart-reload-addition']);
    Route::post('/cart/joinAdditionProduct', ['uses' => 'CartController@joinAdditionProduct', 'as' => 'cart-join-addition-product']);
    Route::get('/cart/quotes', ['uses' => 'CartController@quotes', 'as' => 'cart-quotes'])->middleware('auth');
    Route::any('/cart/installment', ['uses' => 'CartController@installment', 'as' => 'cart-installment'])->middleware('auth');
    Route::post('/cart/get-total-price', ['uses' => 'CartController@getTotalPrice', 'as' => 'cart-get-total-price']);


    Route::group(['prefix' => 'amp'], function () {
        // Route::get('/genuineparts', ['uses' => 'GenuinepartsController@indexAmp', 'as' => 'amp-genuineparts']);
    });


    Route::group(['prefix' => 'genuineparts'], function () {
        Route::get('/', ['uses' => 'GenuinepartsController@index', 'as' => 'genuineparts']);
        Route::get('/done', ['uses' => 'GenuinepartsController@done', 'as' => 'genuineparts-estimate-done']);
        Route::get('/redirect-estimate-done', ['uses' => 'GenuinepartsController@redirectEstimateDone', 'as' => 'genuineparts-estimate-redirect-estimate-done']);
        Route::post('/estimate', ['uses' => 'GenuinepartsController@estimate', 'as' => 'genuineparts-estimate']);
        Route::post('/real_time/estimate', ['uses' => 'GenuinepartsController@realTimeEstimate', 'as' => 'realTime-genuineparts-estimate']);
        Route::get('/step', ['uses' => 'GenuinepartsController@step', 'as' => 'genuineparts-step']);
        Route::get('/trans', ['uses' => 'GenuinepartsController@trans', 'as' => 'genuineparts-trans']);
        Route::get('/manual', ['uses' => 'GenuinepartsController@manual', 'as' => 'genuineparts-manual']);
        Route::get('/manual/{manufacturer}/{displacement}', ['uses' => 'GenuinepartsController@getManual', 'as' => 'genuineparts-manual-list']);
        Route::get('/{divide}/{genuine_manufacturer?}', ['uses' => 'GenuinepartsController@realTimeDivide', 'as' => 'genuineparts-divide']);
        Route::get('/real_time/{divide}/{genuine_manufacturer?}', ['uses' => 'GenuinepartsController@realTimeDivide', 'as' => 'realTime-genuineparts-divide']);
    });


    Route::group(['prefix' => 'benefit'], function () {
        Route::group(['prefix' => 'sale'], function () {
            Route::group(array('prefix' => '2019'), function () {
                Route::group(['prefix' => 'spring'], function () {
                    Route::get('/', array('as' => 'benefit-sale-2019-promotion', 'uses' => 'BenefitController@get2019BigPromotion'));
                    Route::get('/preview', array('as' => 'benefit-sale-2019-promotion-preview', 'uses' => 'BenefitController@get2019PromotionPreview'));
                    Route::get('/brand', array('as' => 'benefit-sale-2019-promotion-brand', 'uses' => 'BenefitController@get2019PromotionBrand'));
                });
            });
            Route::group(['prefix' => 'weekly'], function () {
                Route::get('/', array('as' => 'benefit-sale-weekly-promotion', 'uses' => 'BenefitController@getWeeklyPromotion'));
            });
        });
    });

    Route::group(['prefix' => 'campaigns'], function () {
        Route::get('{promotion_name}', array('as' => 'campaigns-promotion', 'uses' => 'CampaignController@getCampaignPromotion'));
    });

    Route::group(['prefix' => 'checkout'], function () {
        Route::get('/', ['uses' => 'CheckoutController@index', 'as' => 'checkout'])->middleware('auth');
        //    Route::get('/done',['uses' => 'CheckoutController@finish' , 'as'=>'checkout-done']);
        Route::post('/save', ['uses' => 'CheckoutController@save', 'as' => 'checkout-save']);
        Route::get('/payment', ['uses' => 'CheckoutController@payment', 'as' => 'checkout-payment'])->middleware('auth');
        Route::any('/payment/verify', ['uses' => 'CheckoutController@verify', 'as' => 'checkout-payment-verify']);
    });

    Route::get('/outlet/{section?}', ['uses' => 'ListController@index', 'as' => 'outlet'])->where(['section' => '.*']);




});


// Route::get('/shopping', ['uses' => 'HomeController@shopping', 'as' => 'shopping']);

// Route::get('/parts/{section?}', ['uses' => 'ListController@index' , 'as'=>'parts'])->where(['section' => '.*']);

Route::post('/new-product', ['uses' => 'HomeController@newProduct', 'as' => 'home-newProduct']);

Route::post('/feeds', ['uses' => 'HomeController@feeds', 'as' => 'home-feeds']);


Route::get('/search', ['uses' => 'HomeController@google', 'as' => 'google']);
Route::get('/query', ['uses' => 'HomeController@query', 'as' => 'query']);
Route::get('/lock', ['uses' => 'HomeController@lock', 'as' => 'lock']);


Route::get('/carousel', ['uses' => 'HomeController@carousel', 'as' => 'carousel']);
Route::get('/carousel', ['uses' => 'HomeController@carousel', 'as' => 'carousel']);

Route::any('/login/api', ['uses' => 'Auth\LoginController@loginApi', 'as' => 'login-api']);

//Test , Be sure to put here
// Route::any('/login/api', ['uses' => 'Auth\LoginController@loginApi', 'as' => 'login-api']);
Route::group(['middleware' => 'webike'], function () {
    
//    Route::get('/test', ['uses' => 'HomeController@test', 'as' => 'test']);
    Route::get('/testtest', ['uses' => 'HomeController@testtest']);
    Route::get('/arrange-outlet-product', ['uses' => 'HomeController@arrangeOutletProduct']);
    Route::get('/test2', ['uses' => 'HomeController@test2']);
    Route::get('/test1111', ['uses' => 'HomeController@test1111']);
    Route::get('/findSummary', ['uses' => 'HomeController@findSummary']);
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('/motogp/2019/givepoint', ['uses' => 'Benefit\EventController@motogpGivepoint', 'as' => 'benefit-event-motogp-givepoint']);
    // Route::get('/motogp/2018/cancelgivepoint',['uses' => 'Benefit\EventController@motogpcancelGivepoint', 'as' => 'benefit-event-motogp-cancel-givepoint']);
    // Route::get('/motor-update',['uses' => 'HomeController@motor']);
    Route::group(['prefix' => 'magic'], function () {
        Route::get('/login-with-email', ['uses' => 'MagicController@loginWithEmail', 'as' => 'magic-loginWithEmail']);
    });


    Route::group(['prefix' => 'cache'], function () {
        Route::get('product', ['uses' => 'CacheController@product', 'as' => 'cache-product']);
        Route::get('summary', ['uses' => 'CacheController@summary', 'as' => 'cache-summary']);
        Route::get('ranking', ['uses' => 'CacheController@ranking', 'as' => 'cache-ranking']);
        Route::get('opcache', ['uses' => 'CacheController@opcache', 'as' => 'cache-opcache']);
        Route::get('forget', ['uses' => 'CacheController@forget', 'as' => 'cache-forget']);
        Route::get('tag-flush', ['uses' => 'CacheController@tagFlush', 'as' => 'cache-tag-flush']);
        Route::get('static-file', ['uses' => 'CacheController@staticFile', 'as' => 'cache-staticFile']);
        Route::get('cdn', ['uses' => 'CacheController@cdn', 'as' => 'cache-cdn']);
    });
    Route::get('/product', ['uses' => 'HomeController@product_block', 'as' => 'product_block']);

    Route::get('/shopping/windows', ['uses' => 'HomeController@shopping2', 'as' => 'shopping-windows']);
    Route::get('/owl', ['uses' => 'HomeController@owl', 'as' => 'owl']);
});


//Route::get('/motomarket', ['as' => 'motomarket']);
//Route::get('/bikenews', ['as' => 'bikenews']);
Route::get('/home', function () {
    return redirect()->route('home');
});


Route::group(array('prefix' => 'ucrr'), function () {
    Route::get('/', array('as' => 'ucrr', 'uses' => 'UcrrController@getIndex'));
});

//WebhookController
Route::post('line', array('as' => 'webhook-line', 'uses' => 'WebhookController@line'));
Route::get('top', array('as' => 'webhook-pixel', 'uses' => 'WebhookController@pixel'));
Route::get('motomail/{mailType}', array('as' => 'webhook-mmemail', 'uses' => 'WebhookController@MMEmail'));
Route::get('/VIrtUAL808sErViCe', function () {
    app()->abort(404);
});
Route::post('VIrtUAL808sErViCe', array('as' => 'virtualbank', 'uses' => 'WebhookController@virtualbank'))->middleware('bank');


Route::get('/logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);
Route::get('login/facebook', ['uses' => 'Auth\LoginController@getFacebook', 'as' => 'login-facebook']);
Route::get('login/facebook/validate', ['uses' => 'Auth\LoginController@getFacebookValidate', 'as' => 'login-facebook-validate']);


// Route::get('/test', function () {
//    $mybikes = ['t2629','5701'];
//    dd(Everglory\Models\Motor::whereIn('url_rewrite',$mybikes)->pluck('id'));
// });

//Route::get('/br', ['uses' => 'BrandController@index','as'=>'brand']);
//Route::get('/ca/{url_rewrite}', ['uses' => 'BrandController@indexca']);


// Route::get('/sd/info', ['uses' => 'ProductDetailController@info', 'as' => 'product-detail-info']);
Route::post('/sd/alsoBuy', ['uses' => 'ProductDetailController@getAlsoBuy', 'as' => 'product-detail-alsoBuy']);
// Route::get('/sd/{url_rewrite}', ['uses' => 'ProductDetailController@index', 'as' => 'product-detail']);
Route::post('/sd/{url_rewrite}/review', ['uses' => 'ProductDetailController@review', 'as' => 'product-detail-review']);
Route::post('/sd/{url_rewrite}/qa/praise/count', ['uses' => 'ProductDetailController@qaPraiseCount', 'as' => 'product-detail-qa-praise-count']);
Route::group(['middleware' => 'auth'], function () {
    Route::post('/sd/{url_rewrite}/qa/praise', ['uses' => 'ProductDetailController@qaPraise', 'as' => 'product-detail-qa-praise']);
});

// Route::group(['prefix' => 'genuineparts'], function () {
//     Route::get('/', ['uses' => 'GenuinepartsController@index', 'as' => 'genuineparts']);
//     Route::get('/done', ['uses' => 'GenuinepartsController@done', 'as' => 'genuineparts-estimate-done']);
//     Route::get('/redirect-estimate-done', ['uses' => 'GenuinepartsController@redirectEstimateDone', 'as' => 'genuineparts-estimate-redirect-estimate-done']);
//     Route::post('/estimate', ['uses' => 'GenuinepartsController@estimate', 'as' => 'genuineparts-estimate']);
//     Route::post('/real_time/estimate', ['uses' => 'GenuinepartsController@realTimeEstimate', 'as' => 'realTime-genuineparts-estimate']);
//     Route::get('/step', ['uses' => 'GenuinepartsController@step', 'as' => 'genuineparts-step']);
//     Route::get('/trans', ['uses' => 'GenuinepartsController@trans', 'as' => 'genuineparts-trans']);
//     Route::get('/manual', ['uses' => 'GenuinepartsController@manual', 'as' => 'genuineparts-manual']);
//     Route::get('/manual/{manufacturer}/{displacement}', ['uses' => 'GenuinepartsController@getManual', 'as' => 'genuineparts-manual-list']);
//     Route::get('/{divide}/{genuine_manufacturer?}', ['uses' => 'GenuinepartsController@realTimeDivide', 'as' => 'genuineparts-divide']);
//     Route::get('/real_time/{divide}/{genuine_manufacturer?}', ['uses' => 'GenuinepartsController@realTimeDivide', 'as' => 'realTime-genuineparts-divide']);
// });

//Route::get('motor', ['uses' => 'MotorController@index', 'as' => 'motor']);
//Route::get('mf/{manufacturer}/{displacement}', ['uses' => 'MotorController@manufacturer', 'as' => 'motor-manufacturer']);
Route::get('motor/licenses/info', ['uses' => 'MotorController@licensesInfo', 'as' => 'motor-licenses-info']);
// Route::get('mt/{url_rewrite}/tab/top', ['uses' => 'MotorController@top','as'=>'motor-top']);
// Route::get('mt/{url_rewrite}/tab/service/{motor_catalogue_id?}', ['uses' => 'MotorController@service','as'=>'motor-service']);
// Route::get('mt/{url_rewrite}/tab/review', ['uses' => 'MotorController@review','as'=>'motor-review']);
Route::get('mt/{url_rewrite}/tab/video', ['uses' => 'MotorController@video', 'as' => 'motor-video']);

Route::group(['prefix' => 'mitumori'], function () {
    //Route::get('/', ['uses' => 'MitumoriController@index', 'as' => 'mitumori']);
    Route::post('/estimate', ['uses' => 'MitumoriController@estimate', 'as' => 'mitumori-estimate']);
    Route::get('/done', ['uses' => 'MitumoriController@done', 'as' => 'mitumori-estimate-done']);
});

Route::group(['prefix' => 'groupbuy'], function () {
    // Route::get('/', ['uses' => 'GroupBuyController@index', 'as' => 'groupbuy']);
    Route::get('/done', ['uses' => 'GroupBuyController@done', 'as' => 'groupbuy-estimate-done']);
    Route::post('/estimate', ['uses' => 'GroupBuyController@estimate', 'as' => 'groupbuy-estimate']);
});
Route::group(['prefix' => 'customer'], function () {
    Route::get('/', ['uses' => 'CustomerController@index', 'as' => 'customer']);
    Route::post('session/{session_name}', ['uses' => 'CustomerController@setSession', 'as' => 'customer-session']);
    Route::get('/rule/{rule_code}', ['uses' => 'CustomerController@getRule', 'as' => 'customer-rule']);
    //disp
    Route::group(array('prefix' => 'disp'), function () {
        Route::post('/read', ['uses' => 'APIController@readDisp', 'as' => 'customer-read-disp']);
        Route::post('/count', ['uses' => 'APIController@countDisp', 'as' => 'customer-count-disp']);
    });

    Route::group(['prefix' => 'mybike', 'middleware' => 'auth'], function () {
        Route::get('/', ['uses' => 'CustomerController@mybike', 'as' => 'customer-mybike']);
        Route::post('/', ['uses' => 'CustomerController@updateMybike', 'as' => 'customer-mybike']);
        Route::get('/add/{url_rewrite}', ['uses' => 'CustomerController@addMybike', 'as' => 'customer-mybike-add']);
    });

    Route::group(array('prefix' => 'account'), function () {
        Route::get('/create', ['uses' => 'Customer\AccountController@create', 'as' => 'customer-account-create']);
        Route::post('/create', ['uses' => 'Customer\AccountController@store', 'as' => 'customer-account-create']);

        Route::get('/verify-email', ['uses' => 'Customer\AccountController@verifyEmail', 'as' => 'customer-verify-email']);

        Route::group(['middleware' => 'auth'], function() {
            Route::get('/', ['uses' => 'Customer\AccountController@account', 'as' => 'customer-account']);
            Route::get('/preview', ['uses' => 'Customer\AccountController@preview', 'as' => 'customer-account-preview']);
            Route::get('/confirm', ['uses' => 'Customer\AccountController@confirm', 'as' => 'customer-account-confirm']);
            Route::post('/confirm', ['uses' => 'Customer\AccountController@validateConfirm', 'as' => 'customer-account-confirm']);
            Route::get('/create/done', ['uses' => 'Customer\AccountController@done', 'as' => 'customer-account-create-done']);
            Route::get('/complete', ['uses' => 'Customer\AccountController@complete', 'as' => 'customer-account-complete']);
            Route::post('/update', ['uses' => 'Customer\AccountController@update', 'as' => 'customer-account-update']);
            Route::get('/complete/done', ['uses' => 'Customer\AccountController@done', 'as' => 'customer-account-complete-done']);
        });
    });

    Route::group(['prefix' => 'history'], function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/order', ['uses' => 'Customer\HistoryController@orderHistory', 'as' => 'customer-history-order']);

            //tai william
            // Route::get('/order', ['uses' => 'Customer\HistoryController@orderHistory', 'as' => 'customer-history-product']);

            Route::get('/order/ing', ['uses' => 'Customer\HistoryController@orderNotFinishHistory', 'as' => 'customer-history-order-ing']);
            Route::get('/order/preview', ['uses' => 'Customer\HistoryController@orderPreview', 'as' => 'customer-history-order-preview']);
            Route::get('/order/detail/{code}', ['uses' => 'Customer\HistoryController@orderDetailHistory', 'as' => 'customer-history-order-detail']);
            Route::get('/order/detail/{code}/return', ['uses' => 'Customer\HistoryController@orderDetailHistoryReturn', 'as' => 'customer-history-order-detail-return']);
            Route::post('/order/detail/{code}/return', ['uses' => 'Customer\HistoryController@orderDetailHistoryReturnForm', 'as' => 'customer-history-order-detail-return']);
            Route::post('/order/detail/{code}/return/create', ['uses' => 'Customer\HistoryController@orderDetailHistoryReturnFormCreate', 'as' => 'customer-history-order-detail-return-create']);
            Route::post('/order/detail/{code}/return/create/image', ['uses' => 'Customer\HistoryController@orderDetailHistoryReturnFormCreateImage', 'as' => 'customer-history-order-detail-return-create-image']);
            Route::get('/order/detail/{code}/return/create/done', ['uses' => 'Customer\HistoryController@orderDetailHistoryReturnFormCreateDone', 'as' => 'customer-history-order-detail-return-create-done']);
            Route::get('/order/detail/{code}/cancel', ['uses' => 'Customer\HistoryController@orderDetailHistoryCancel', 'as' => 'customer-history-order-detail-cancel']);
            Route::post('/order/detail/{code}/cancel', ['uses' => 'Customer\HistoryController@orderDetailHistoryCancelSubmit', 'as' => 'customer-history-order-detail-cancel']);
            Route::get('/order/detail/{code}/cancel/done', ['uses' => 'Customer\HistoryController@orderDetailHistoryCancelDone', 'as' => 'customer-history-order-detail-cancel-done']);
            Route::get('/order/quotes/{code}', ['uses' => 'Customer\HistoryController@quotes', 'as' => 'customer-history-order-quotes']);

            Route::get('/return', ['uses' => 'Customer\HistoryController@returnHistory', 'as' => 'customer-history-return']);
            Route::get('/return/detail/{code}', ['uses' => 'Customer\HistoryController@returnDetailHistory', 'as' => 'customer-history-return-detail']);

            Route::get('/genuineparts', ['uses' => 'Customer\HistoryController@genuinepartsHistory', 'as' => 'customer-history-genuineparts']);
            Route::get('/genuineparts/detail/{code}', ['uses' => 'Customer\HistoryController@genuinepartsDetailHistory', 'as' => 'customer-history-genuineparts-detail']);
            Route::post('/genuineparts/detail/match', ['uses' => 'Customer\HistoryController@genuinepartsDetailHistoryMatch', 'as' => 'customer-history-genuineparts-detail-match']);
            Route::get('/mitumori', ['uses' => 'Customer\HistoryController@mitumoriHistory', 'as' => 'customer-history-mitumori']);
            Route::get('/mitumori/detail/{code}', ['uses' => 'Customer\HistoryController@mitumoriDetailHistory', 'as' => 'customer-history-mitumori-detail']);
            Route::get('/groupbuy', ['uses' => 'Customer\HistoryController@groupbuyHistory', 'as' => 'customer-history-groupbuy']);
            Route::get('/groupbuy/detail/{code}', ['uses' => 'Customer\HistoryController@groupbuyDetailHistory', 'as' => 'customer-history-groupbuy-detail']);
            Route::get('/estimate', ['uses' => 'Customer\HistoryController@estimateHistory', 'as' => 'customer-history-estimate']);

            Route::get('/review', ['uses' => 'Customer\HistoryController@reviewHistory', 'as' => 'customer-history-review']);

            Route::get('/points', ['uses' => 'Customer\HistoryController@pointsHistory', 'as' => 'customer-history-points']);

            Route::get('/gifts', ['uses' => 'Customer\HistoryController@giftsHistory', 'as' => 'customer-history-gifts']);

            Route::post('/gifts', ['uses' => 'Customer\HistoryController@postGiftsHistory', 'as' => 'customer-history-gifts']);

            Route::group(['prefix'=>'purchased'],function(){
                Route::get('/', ['uses' => 'Customer\HistoryController@purchasedHistory', 'as' => 'customer-history-purchased']);
                Route::get('/order-items',['as'=>'rest-order-items','uses'=>'Customer\HistoryController@getOrderItems']);
                Route::put('/order-items',['as'=>'rest-order-items','uses'=>'Customer\HistoryController@putOrderItem']);
            });

        });
    });


    Route::group(array('prefix' => 'wishlist'), function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/', ['uses' => 'Customer\WishlistController@index', 'as' => 'customer-wishlist']);
            Route::post('/updateItem', ['uses' => 'Customer\WishlistController@update', 'as' => 'customer-wishlist-update']);
            Route::post('/moveItem', ['uses' => 'Customer\WishlistController@move', 'as' => 'customer-wishlist-move']);
            Route::post('/removeItem', ['uses' => 'Customer\WishlistController@remove', 'as' => 'customer-wishlist-remove']);
        });
    });

    Route::group(['prefix' => 'estimate', 'middleware' => 'auth'], function () {
        Route::get('/', ['uses' => 'Customer\EstimateController@index', 'as' => 'customer-estimate']);
        Route::post('/', ['uses' => 'Customer\EstimateController@estimate', 'as' => 'customer-estimate']);
        Route::get('/done', ['uses' => 'Customer\EstimateController@done', 'as' => 'customer-estimate-done']);
        Route::post('/updateItem', ['uses' => 'Customer\EstimateController@update', 'as' => 'customer-estimate-update']);
        Route::post('/removeItem', ['uses' => 'Customer\EstimateController@remove', 'as' => 'customer-estimate-remove']);
        Route::post('/moveItem', ['uses' => 'Customer\EstimateController@move', 'as' => 'customer-estimate-move']);
    });

    Route::group(['prefix' => 'service'], function () {
        Route::get('/information', ['uses' => 'CustomerController@info', 'as' => 'customer-service-information']);
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/message', ['uses' => 'Customer\ContactController@message', 'as' => 'customer-service-message']);
            Route::get('/history', ['uses' => 'Customer\ContactController@history', 'as' => 'customer-service-history']);
            Route::get('/history/dialog/{code}', ['uses' => 'Customer\ContactController@dialog', 'as' => 'customer-service-history-dialog']);
            Route::post('/dialog/reply/', ['uses' => 'Customer\ContactController@reply', 'as' => 'customer-service-reply']);
            Route::post('/dialog/read/', ['uses' => 'Customer\ContactController@read', 'as' => 'customer-service-read']);
            Route::post('/dialogs/render/dialogs/{code}', ['uses' => 'Customer\ContactController@renderDialogs', 'as' => 'customer-service-render-dialogs']);
            Route::get('/proposal', ['uses' => 'Customer\ContactController@proposal', 'as' => 'customer-service-proposal']);
            Route::get('/proposal/{ticket_type}/create', ['uses' => 'Customer\ContactController@create', 'as' => 'customer-service-proposal-create']);
            Route::post('/proposal/{ticket_type}/create', ['uses' => 'Customer\ContactController@postCreate', 'as' => 'customer-service-proposal-create']);
            Route::any('/proposal/getfaq', ['uses' => 'Customer\ContactController@getfaq', 'as' => 'customer-service-proposal-getfaq']);
        });
    });

    Route::get('fixer', ['uses' => 'CustomerController@fixer', 'as' => 'customer-fixer']);
    Route::get('fixer/autofix', ['uses' => 'CustomerController@refresh', 'as' => 'customer-fixer-refresh']);

    Route::get('forgotpassword', ['uses' => 'Auth\ForgotPasswordController@showLinkRequestForm', 'as' => 'customer-forgot-password']);
    Route::get('resetpassword', ['uses' => 'Auth\ResetPasswordController@showResetForm', 'as' => 'customer-reset-password']);


    Route::get('newsletter', ['uses' => 'CustomerController@editNewsletter', 'as' => 'customer-newsletter'])->middleware('auth');
    Route::post('newsletter', ['uses' => 'CustomerController@updateNewsletter', 'as' => 'customer-newsletter'])->middleware('auth');

    Route::get('newsletter/point', ['uses' => 'CustomerController@getNewsletterPoint', 'as' => 'customer-newsletter-point'])->middleware('auth');


    Route::post('recentView', array('uses' => 'CustomerController@getRecentView'));

    Route::get('header-count', ['uses' => 'CustomerController@getHeaderCount', 'as' => 'customer-headerCount']);
    Route::post('header-count', ['uses' => 'CustomerController@getHeaderCount', 'as' => 'customer-headerCount']);

});

// if(config('app.production')){
Route::group(['prefix' => 'checkout'], function () {
//        Route::get('/',['uses' => 'CheckoutController@index', 'as'=>'checkout'])->middleware('auth');
//    //    Route::get('/done',['uses' => 'CheckoutController@finish' , 'as'=>'checkout-done']);
    Route::get('/done', ['uses' => 'ServiceController@finish', 'as' => 'checkout-done']);
//        Route::post('/save',['uses' => 'CheckoutController@save', 'as'=>'checkout-save']);
//        Route::get('/payment',['uses' => 'CheckoutController@payment', 'as'=>'checkout-payment'])->middleware('auth');
//        Route::any('/payment/verify',['uses' => 'CheckoutController@verify', 'as'=>'checkout-payment-verify']);
});
// }


//Route::get('/list' ,[ 'uses' => 'ListController@index']);
//Route::get('/parts', ['uses' => 'ListController@index']);

Route::get('/ranking/{section?}', ['uses' => 'ListController@ranking', 'as' => 'ranking'])->where(['section' => '.*']);

Auth::routes();


Route::group(['prefix' => 'collection'], function () {
    Route::get('/', ['uses' => 'CollectionController@index', 'as' => 'collection']);
    Route::get('/video', ['uses' => 'CollectionController@video', 'as' => 'collection-video']);
    Route::get('brand/trickstar',['uses' => 'CollectionController@getTrickstar', 'as' => 'collection-trickstar']);
    Route::get('/brand/NGKstock/{type?}', ['uses' => 'CollectionController@getNgkStock', 'as' => 'collection-ngkStock']);
    Route::group(['prefix' => 'Suzuka8hours'], function () {
        Route::get('/2017', ['uses' => 'CollectionController@getSuzuka2017', 'as' => 'collection-suzuka2017']);
        Route::get('/2018', ['uses' => 'CollectionController@getSuzuka2018', 'as' => 'collection-suzuka2018']);
    });
    Route::get('/data', ['uses' => 'CollectionController@getDetailData', 'as' => 'collection-testdata']);
    Route::get('/{type}/{url_rewrite}', ['uses' => 'CollectionController@detail', 'as' => 'collection-type-detail']);
    Route::get('/{type}', ['uses' => 'CollectionController@type', 'as' => 'collection-type']);
});


Route::group(['prefix' => 'review'], function () {
    // Route::get('/list/{url_path?}', ['uses' => 'ReviewController@search', 'as' => 'review-search']);
    Route::post('/store', ['uses' => 'ReviewController@store', 'as' => 'review-store'])->middleware('auth');
    Route::get('/step', ['uses' => 'ReviewController@getStep', 'as' => 'review-step']);
    Route::post('/comment/{id}', array('as' => 'review-comment-post', 'uses' => 'ReviewController@comment'))->middleware('auth');

});

//Route::get('/outlet', ['uses' => 'ListController@index']);
//Route::get('/outlet/{section?}', ['uses' => 'ListController@index' , 'as'=>'outlet'])->where(['section' => '.*']);

Route::group(['prefix' => 'benefit'], function () {
    Route::get('/', array('as' => 'benefit', 'uses' => 'BenefitController@getIndex'));
    // Route::get('/outlet', array( 'as' =>'benefit-outlet' , 'uses' => 'Benefit\OutletController@index' ));

    Route::group(['prefix' => 'event'], function () {
        Route::get('/openmail', ['uses' => 'BenefitController@getEventOpenmail', 'as' => 'benefit-event-openmail']);
        Route::get('/mybike', ['uses' => 'BenefitController@getEventMybike', 'as' => 'benefit-event-mybike']);
        Route::get('/webikeguess', ['uses' => 'BenefitController@getWebikeguess', 'as' => 'benefit-event-webikeguess-get']);
        Route::post('/webikeguesspost/{type_id}', ['uses' => 'BenefitController@postWebikeguess', 'as' => 'benefit-event-webikeguess-post']);
        Route::get('/webikequiz', array('uses' => 'BenefitController@getWebikequiz', 'as' => 'benefit-event-webikequiz'));
        Route::post('/webikequiz', array('uses' => 'BenefitController@postWebikequiz', 'as' => 'benefit-event-webikequiz'));
        Route::get('/webikewanted', array('uses' => 'BenefitController@getWebikewanted', 'as' => 'benefit-event-webikewanted'));
        Route::post('/webikewanted', array('uses' => 'BenefitController@postWebikewanted', 'as' => 'benefit-event-webikewanted'));

        Route::group(['prefix' => 'motogp'], function () {
            Route::group(['prefix' => '2017'], function () {
                Route::get('/', ['uses' => 'Benefit\EventController@motoGp2017', 'as' => 'benefit-event-motogp-2017']);
                Route::post('/', ['uses' => 'Benefit\EventController@postMotoGp2017', 'as' => 'benefit-event-motogp-2017']);
                Route::get('/info', ['uses' => 'Benefit\EventController@motoGpInfo2017', 'as' => 'benefit-event-motogp-2017info']);
                Route::get('/{station}', ['uses' => 'Benefit\EventController@motoGpDetail2017', 'as' => 'benefit-event-motogp-2017detail']);
            });
            Route::group(['prefix' => '2018'], function () {
                Route::get('/', ['uses' => 'Benefit\EventController@motoGp2018', 'as' => 'benefit-event-motogp-2018']);
                Route::post('/', ['uses' => 'Benefit\EventController@postMotoGp2018', 'as' => 'benefit-event-motogp-2018']);
                Route::get('/rider/{rider_number}', ['uses' => 'Benefit\EventController@motoGprider2018', 'as' => 'benefit-event-motogprider-2018']);
                Route::post('/rate-choose', ['uses' => 'Benefit\EventController@postRateChoose', 'as' => 'benefit-event-motogp-2018-rate-choose']);
                Route::get('/info', ['uses' => 'Benefit\EventController@motoGpInfo2018', 'as' => 'benefit-event-motogp-2018info']);
                Route::get('/speedway', ['uses' => 'Benefit\EventController@motoGpSpeedway2018', 'as' => 'benefit-event-motogp-2018Speedway']);
                Route::get('/{station}', ['uses' => 'Benefit\EventController@motoGpDetail2018', 'as' => 'benefit-event-motogp-2018detail']);
            });
            Route::group(['prefix' => '2019'], function () {
                Route::get('/', ['uses' => 'Benefit\EventController@motoGp2019', 'as' => 'benefit-event-motogp-2019']);
                Route::post('/', ['uses' => 'Benefit\EventController@postMotoGp2019', 'as' => 'benefit-event-motogp-2019']);
                Route::get('/rider/{rider_number}', ['uses' => 'Benefit\EventController@motoGprider2019', 'as' => 'benefit-event-motogprider-2019']);
                Route::post('/rate-choose', ['uses' => 'Benefit\EventController@motoGp2019postRateChoose', 'as' => 'benefit-event-motogp-2019-rate-choose']);
                Route::get('/info', ['uses' => 'Benefit\EventController@motoGpInfo2019', 'as' => 'benefit-event-motogp-2019info']);
                Route::get('/speedway', ['uses' => 'Benefit\EventController@motoGpSpeedway2019', 'as' => 'benefit-event-motogp-2019Speedway']);
                Route::get('/{station}', ['uses' => 'Benefit\EventController@motoGpDetail2019', 'as' => 'benefit-event-motogp-2019detail']);
            });
        });
    });
    Route::group(['prefix' => 'sale'], function () {
        Route::get('/pointinfo', array('as' => 'benefit-sale-pointinfo', 'uses' => 'BenefitController@getPointInfo'));
        Route::get('/2018Christmas', array('uses' => 'BenefitController@get2018Christmas', 'as' => 'benefit-sale-2018Christmas'));
        Route::post('/2018Christmas', array('uses' => 'BenefitController@post2018Christmas', 'as' => 'benefit-sale-2018Christmas'));
        Route::get('/{code}', array('as' => 'benefit-sale-month-promotion', 'uses' => 'BenefitController@getMonthPromotion'));
        Route::group(array('prefix' => '2017'), function () {
            Route::get('/fall', array('as' => 'benefit-sale-2017-promotion', 'uses' => 'BenefitController@get2017BigPromotion'));
            Route::get('/fall/ca/{category}', array('as' => 'benefit-sale-2017-promotion-category', 'uses' => 'BenefitController@get2017BigPromotionCategory'));
            Route::get('/fall/domestic', array('as' => 'benefit-sale-2017-promotion-domestic', 'uses' => 'BenefitController@get2017BigPromotionDomestic'));
            Route::get('/fall/special-discount', array('as' => 'benefit-sale-2017-promotion-specialDiscount', 'uses' => 'BenefitController@get2017BigPromotionSpecialDiscount'));
        });
        Route::group(['prefix' => 'anniversary'], function () {
            Route::group(array('prefix' => '5-th'), function () {
                Route::get('/', array('uses' => 'BenefitController@get5year', 'as' => 'benefit-sale-5year'));
                Route::post('/', array('uses' => 'BenefitController@addGift', 'as' => 'benefit-sale-addGift'));
            });
        });
        Route::group(array('prefix' => '2018'), function () {
            Route::group(['prefix' => 'fall'], function () {
                Route::get('/', array('as' => 'benefit-sale-2018-promotion', 'uses' => 'BenefitController@get2018BigPromotion')); /*yiiyu*/
                Route::post('/category', array('as' => 'benefit-sale-2018-promotion-getCategory', 'uses' => 'BenefitController@getCategory')); /*yiiyu*/
                Route::get('/brand', array('uses' => 'BenefitController@getBrand')); /*yiiyu*/
                Route::post('/products', array('as' => 'benefit-sale-2018-promotion-getItem', 'uses' => 'BenefitController@getItem')); /*yiiyu*/
            });

            Route::post('/fall/ranking', array('as' => 'benefit-sale-2018-promotion-specialDiscount-ranking', 'uses' => 'BenefitController@get2018BigPromotionRanking'));

            Route::get('/fall/ca/{category}', array('as' => 'benefit-sale-2018-promotion-category', 'uses' => 'BenefitController@get2018BigPromotionCategory'));

            Route::get('/fall/domestic', array('as' => 'benefit-sale-2018-promotion-domestic', 'uses' => 'BenefitController@get2018BigPromotionDomestic'));

            Route::get('/fall/special-discount', array('as' => 'benefit-sale-2018-promotion-specialDiscount', 'uses' => 'BenefitController@get2018BigPromotionSpecialDiscount'));

            Route::post('/', array('as' => 'benefit-getProductByBrand', 'uses' => 'BenefitController@getProductByBrand'));

            Route::post('/getProductByBrandAndCategory', array('as' => 'benefit-getProductByBrandAndCategory', 'uses' => 'BenefitController@getProductByBrandAndCategory'));

            Route::post('/getProductByBrandCategories', array('as' => 'benefit-getProductByBrandCategories', 'uses' => 'BenefitController@getProductByBrandCategories'));

            Route::post('/getSpecialPriceProduct', array('as' => 'benefit-getSpecialPriceProduct', 'uses' => 'BenefitController@getSpecialPrice'));
        });
        /*Route::group( array( 'prefix' => '2019' ), function(){
            Route::group(['prefix'=>'newyear'],function(){
                Route::get('/', array( 'as' =>'benefit-sale-2019-promotion' , 'uses' => 'BenefitController@get2019BigPromotion' ));
                Route::get('/preview', array( 'as' =>'benefit-sale-2019-promotion-preview' , 'uses' => 'BenefitController@get2019PromotionPreview' )); 
                Route::get('/brand', array( 'as' =>'benefit-sale-2019-promotion-brand' , 'uses' => 'BenefitController@get2019PromotionBrand' )); 
            });
        });*/


        Route::get('/fatherday/{year}', array('uses' => 'BenefitController@getFatherDay', 'as' => 'benefit-event-fatherday'));
        Route::post('/fatherday/getProduct', array('uses' => 'BenefitController@getProductByCategory', 'as' => 'benefit-event-fatherday-getProduct'));
        Route::get('/fatherday/2018/coupon', array('uses' => 'BenefitController@inserFatherDayCoupon', 'as' => 'benefit-event-inserFatherDayCoupon'));
        Route::post('/double11/createGift', array('uses' => 'BenefitController@createGift', 'as' => 'benefit-sale-double11-createGift'));
        Route::get('/double11/{year}', array('uses' => 'BenefitController@getDouble11', 'as' => 'benefit-sale-double11'));
        Route::post('/double11/{year}', array('uses' => 'BenefitController@postDouble11', 'as' => 'benefit-sale-double11'));
        Route::get('/newyear/{year}', ['uses' => 'BenefitController@getNewYear', 'as' => 'benefit-sale-newyear']);

    });

});

Route::group(array('prefix' => 'service'), function () {
    Route::get('/yfcshipping', ['uses' => 'ServiceController@getYfcShipping', 'as' => 'service-yfcshipping']);
    Route::get('/supplier/join', ['uses' => 'ServiceController@getJoinSupplier', 'as' => 'service-supplier-join']);
    Route::get('/supplier/join/form', ['uses' => 'ServiceController@getJoinSupplierform', 'as' => 'service-supplier-join-form']);
    Route::get('/dealer/join', ['uses' => 'ServiceController@getJoinDealer', 'as' => 'service-dealer-join']);
    Route::get('/dealer/join/form', ['uses' => 'ServiceController@getJoinDealerForm', 'as' => 'service-dealer-join-form']);
    Route::get('/dealer/grade/info', ['uses' => 'ServiceController@getDealerGradeInfo', 'as' => 'service-dealer-grade-info']);
    Route::get('/dealer/installment/info', ['uses' => 'ServiceController@getDealerInstallmentInfo', 'as' => 'service-dealer-installment-info']);
});

//Route::get( '/information/dealer' , array( 'as' =>'dealer' ));

Route::get('/information/dealer', ['as' => 'dealer', function () {
    return redirect(DEALER_URL);
}]);


Route::get('/br', ['uses' => 'SummaryController@brand','as'=>'brand']);
// Route::get('/br/{section}/info', ['uses' => 'ManufacturerController@info','as'=>'brand-info']);
// Route::get('/br/{section}/info-2col', ['uses' => 'ManufacturerController@info','as'=>'brand-info-2col']);


Route::post('fit_motors', ['uses' => 'SummaryController@getFitMotorResult', 'as' => 'get-fit-motors']);

Route::group(['namespace' => $namespace], function () {
    
    Route::get('/br/{section}/info', ['uses' => 'ManufacturerController@info','as'=>'brand-info']);
    Route::get('/br/{section}/info-2col', ['uses' => 'ManufacturerController@info','as'=>'brand-info-2col']);
    Route::get('/amp/br/{section}/info', ['uses' => 'ManufacturerController@info','as'=>'amp-brand-info']);

    Route::get('amp/{section}',
        [
            'uses' => 'SummaryController@index','as' => 'amp-summary'
        ])
        ->where(['section' => '^(ca|br|mt)\/.*']);

    Route::get('/{section}',
        [
            'uses' => 'SummaryController@index', 'as' => 'summary'
        ])
        ->where(['section' => '^(ca|br|mt)\/.*']);

});

Route::post('/{section}/lazy',
    [
        'uses' => 'SummaryController@lazy', 'as' => 'summary-lazy'
    ])
    ->where(['section' => '^(ca|br|mt)\/.*']);


Route::group(['prefix' => 'WeBiKeAdMiN', 'middleware' => 'webike'], function () {
    Route::get('/', ['uses' => 'Backend\IndexController@getIndex', 'as' => 'backend']);

    Route::group(array('prefix' => 'api'), function () {
        Route::post('/customer-names', ['uses' => 'Backend\ApiController@getCustomerNamesByText', 'as' => 'backend-api-customerNames']);

    });

    Route::group(array('prefix' => 'assortment'), function () {
        Route::get('/', ['uses' => 'Backend\AssortmentController@getIndex', 'as' => 'backend-assortment']);
        Route::post('/', ['uses' => 'Backend\AssortmentController@postIndex', 'as' => 'backend-assortment-post']);
        Route::get('/insert/{assortment_id?}', ['uses' => 'Backend\AssortmentController@getAssortmentInsert', 'as' => 'backend-assortment-insert']);
        Route::post('/insert/{assortment_id?}', ['uses' => 'Backend\AssortmentController@postAssortmentInsert', 'as' => 'backend-assortment-insert-post']);
        Route::get('/insertBlock/{assortment_id?}/{assortmentItem_id?}', ['uses' => 'Backend\AssortmentController@getAssortmentInsertBlock', 'as' => 'backend-assortment-insert-block']);
        Route::post('/insertBlock/{assortment_id?}/{assortment_item_id?}', ['uses' => 'Backend\AssortmentController@postAssortmentInsertBlock', 'as' => 'backend-assortment-insert-block-post']);
        Route::post('/editOtherChanges/{assortment_id?}', ['uses' => 'Backend\AssortmentController@postOtherAssortmentItemChange', 'as' => 'backend-assortment-edit-post']);
        Route::post('/columns', ['uses' => 'Backend\AssortmentController@postColumns', 'as' => 'backend-assortment-columns']);
        Route::post('/edit', ['uses' => 'Backend\AssortmentController@getAssortmentEdit', 'as' => 'backend-assortment-edit']);
        Route::post('/upload', ['uses' => 'Backend\AssortmentController@upload', 'as' => 'backend-assortment-upload']);
    });

    Route::group(['prefix' => 'qa'], function () {
        Route::get('/', ['uses' => 'Backend\QaController@index', 'as' => 'backend-qa']);
        Route::get('/search', ['uses' => 'Backend\QaController@search', 'as' => 'backend-qa-search']);
        Route::get('/weekly', ['uses' => 'Backend\QaController@weekly', 'as' => 'backend-qa-weekly']);
        Route::any('/edit', ['uses' => 'Backend\QaController@edit', 'as' => 'backend-qa-edit']);
        Route::post('/write', ['uses' => 'Backend\QaController@write', 'as' => 'backend-qa-write']);

        Route::group(['prefix' => 'ticket'], function () {
            Route::get('/import', ['uses' => 'Backend\QaController@import', 'as' => 'backend-qa-ticket-import']);
        });
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::get('/calendar', ['uses' => 'Backend\SettingController@calendar', 'as' => 'backend-setting-calendar']);
        Route::post('/calendar', ['uses' => 'Backend\SettingController@postCalendar', 'as' => 'backend-setting-calendar']);
        Route::post('/calendar/events', ['uses' => 'Backend\SettingController@postEvent', 'as' => 'backend-setting-calendar-events']);
        Route::group(array('prefix' => 'cache'), function () {
            Route::get('/', array('uses' => 'Backend\setting\CacheController@getIndex', 'as' => 'backend-setting-cache'));
            Route::post('/clear', array('uses' => 'Backend\setting\CacheController@clearCache', 'as' => 'backend-setting-cache-clear'));
        });
    });

    Route::group(['prefix' => 'review'], function () {
        Route::get('/', ['uses' => 'Backend\ReviewController@index', 'as' => 'backend-review']);
        Route::get('/{type}', ['uses' => 'Backend\ReviewController@index', 'as' => 'backend-review-type']);
//        Route::get('/validate',['use' => 'Backend\ReviewController@getValidate','as' => 'backend-review-validate']);
//        Route::get('/violation',['use' => 'Backend\ReviewController@getViolation','as' => 'backend-review-violation']);
        Route::post('/review', ['uses' => 'Backend\ReviewController@postReview', 'as' => 'backend-review']);
    });

    Route::group(['prefix' => 'newsletter'], function () {
        Route::get('/', ['uses' => 'Backend\NewsletterController@index', 'as' => 'backend-newsletter']);
        Route::get('/insert/{id?}', ['uses' => 'Backend\NewsletterController@insertNewsletter', 'as' => 'backend-newsletter-insert']);
        Route::post('/insert/{id?}', ['uses' => 'Backend\NewsletterController@postInsertNewsletter', 'as' => 'backend-newsletter-insert']);
        Route::get('/queue', ['uses' => 'Backend\NewsletterController@getQueue', 'as' => 'backend-newsletter-queue']);
        Route::get('/queue/insert/{id?}', ['uses' => 'Backend\NewsletterController@getQueueInsert', 'as' => 'backend-newsletter-queue-insert']);
        Route::post('/queue/insert/{id?}', ['uses' => 'Backend\NewsletterController@postQueueInsert', 'as' => 'backend-newsletter-queue-insert']);
        Route::post('/delete', ['uses' => 'Backend\NewsletterController@delete', 'as' => 'backend-newsletter-delete']);

        Route::get('/newEdit', ['uses' => 'Backend\NewsletterController@getNewsEdit', 'as' => 'backend-newsletter-edit']);
        Route::post('/newEdit', ['uses' => 'Backend\NewsletterController@postNewsEdit', 'as' => 'backend-newsletter-edit']);
        Route::get('/setNewsletterCustomer', ['uses' => 'Backend\NewsletterController@setNewsletterCustomer', 'as' => 'backend-newsletter-customer']);

        Route::get('/install', ['uses' => 'Backend\NewsletterController@getInstall', 'as' => 'backend-newsletter-install']);
        Route::post('/install', ['uses' => 'Backend\NewsletterController@install', 'as' => 'backend-newsletter-install']);
        Route::post('/into-newsletter', ['uses' => 'Backend\NewsletterController@intoNewsletter', 'as' => 'backend-newsletter-intoNewsletter']);


    });

    Route::group(['prefix' => 'coupon'], function () {
        Route::get('/', ['uses' => 'Backend\CouponController@getIndex', 'as' => 'backend-coupon']);
        Route::post('history', ['uses' => 'Backend\CouponController@postHistory', 'as' => 'backend-coupon-history-post']);
        Route::get('filter', ['uses' => 'Backend\CouponController@getFilter', 'as' => 'backend-coupon-filter']);
        Route::get('create', ['uses' => 'Backend\CouponController@getCreate', 'as' => 'backend-coupon-create']);
        Route::post('create', ['uses' => 'Backend\CouponController@postCreate', 'as' => 'backend-coupon-create']);
        Route::get('{id}', ['uses' => 'Backend\CouponController@getEdit', 'as' => 'backend-coupon-edit']);
        Route::post('{id}', ['uses' => 'Backend\CouponController@postEdit', 'as' => 'backend-coupon-edit']);
    });

    Route::group(['prefix' => 'disp'], function () {
        Route::get('/', ['uses' => 'Backend\DispController@Index', 'as' => 'backend-disp']);
        Route::get('massage', ['uses' => 'Backend\DispController@Index', 'as' => 'backend-disp-massage']);
        Route::post('massage', ['uses' => 'Backend\DispController@postDisp', 'as' => 'backend-postdisp-massage']);
        Route::post('testmassage', ['uses' => 'Backend\DispController@postDispA', 'as' => 'backend-testpostdisp-massage']);
    });

    Route::group(array('prefix' => 'solr'), function () {
        Route::get('/', ['uses' => 'Backend\SolrController@index', 'as' => 'backend-solr']);
        Route::get('/status/command', ['uses' => 'Backend\SolrController@commandStatus', 'as' => 'backend-solr-status-command']);
        Route::group(array('prefix' => 'product'), function () {
            Route::get('/', ['uses' => 'Backend\SolrController@product', 'as' => 'backend-solr-product']);
            Route::get('/update', ['uses' => 'Backend\SolrController@productUpdate', 'as' => 'backend-solr-product-update']);
            Route::get('/remove', ['uses' => 'Backend\SolrController@productRemove', 'as' => 'backend-solr-product-remove']);
            Route::group(array('prefix' => 'stock'), function () {
                Route::get('/reset', ['uses' => 'Backend\SolrController@resetStock', 'as' => 'backend-solr-product-stock-reset']);
                Route::post('/update', ['uses' => 'Backend\SolrController@updateStock', 'as' => 'backend-solr-product-stock-update']);
            });
        });
    });

    Route::group(['prefix' => 'promo'], function () {
        Route::get('/', ['uses' => 'Backend\PromoController@index', 'as' => 'backend-promo']);
        Route::post('promoPreview', ['uses' => 'Backend\PromoController@promoPreview', 'as' => 'backend-promo-promoPreview']);
        Route::post('promoData', ['uses' => 'Backend\PromoController@promoData', 'as' => 'backend-promo-promoData']);
    });

    Route::group(['prefix' => 'tool'], function () {
        Route::get('imageUpload', ['uses' => 'Backend\ToolController@imageUploadIndex', 'as' => 'backend-tool-imageUpload']);
        Route::post('photoUpolad', ['uses' => 'Backend\ToolController@upload', 'as' => 'backend-tool-upload']);
        Route::post('photoStore', ['uses' => 'Backend\ToolController@store', 'as' => 'backend-tool-store']);
        Route::get('searchPhoto', ['uses' => 'Backend\ToolController@searchPhoto', 'as' => 'backend-tool-searchPhoto']);
    });

    Route::group(array('prefix' => 'guess'), function () {
        Route::get('/', array('as' => 'backend-guess', 'uses' => 'Backend\GuessController@getIndex'));
        Route::post('/', array('as' => 'backend-guess-post', 'uses' => 'Backend\GuessController@postIndex'));
        Route::get('/edit/{id}', array('as' => 'backend-guess-edit', 'uses' => 'Backend\GuessController@getEdit'));
        Route::get('/insert', array('as' => 'backend-guess-insert', 'uses' => 'Backend\GuessController@getEdit'));
        Route::post('/edit', array('as' => 'backend-guess-edit-post', 'uses' => 'Backend\GuessController@postEdit'));
        Route::post('/delete', array('as' => 'backend-guess-delete', 'uses' => 'Backend\GuessController@postDelete'));
        Route::get('/list', array('as' => 'backend-guess-list', 'uses' => 'Backend\GuessController@getList'));
        Route::get('/log', array('as' => 'backend-guess-log', 'uses' => 'Backend\GuessController@getLog'));
        Route::get('/rewards', array('as' => 'backend-guess-rewards', 'uses' => 'Backend\GuessController@guessReward'));
        Route::get('/monthRewards', array('as' => 'backend-guess-rewards-month', 'uses' => 'Backend\GuessController@guessMonthReward'));

        Route::get('/test', array('as' => '', 'uses' => 'Backend\GuessController@test'));
    });

    Route::group(array('prefix' => 'log'), function () {
        Route::get('/sales', array('as' => 'backend-log-sales', 'uses' => 'Backend\LogController@getSalesLog'));
        Route::get('/genuineparts', array('as' => 'backend-log-genuineparts', 'uses' => 'Backend\LogController@getGenuinePartsLog'));
        Route::get('/creditcard', array('as' => 'backend-log-creditcard', 'uses' => 'Backend\LogController@getCreditCard'));
//        Route::get('/mybike', array( 'as' =>'backend-log-mybike' , 'uses' => 'Backend\LogController@getMybikeLog' ));
        Route::get('/points', array('as' => 'backend-log-points', 'uses' => 'Backend\LogController@getPointsLog'));
        Route::get('/coupons', array('as' => 'backend-log-coupons', 'uses' => 'Backend\LogController@getCouponsLog'));
        Route::get('/virtualbank', array('as' => 'backend-log-virtualbank', 'uses' => 'Backend\LogController@getVirtualBankLog'));
//        Route::get('/newsviews', array( 'as' =>'backend-log-newsviews' , 'uses' => 'Backend\LogController@getNewsViews' ));
    });

    Route::group(array('prefix' => 'product'), function () {
        Route::get('/', array('as' => 'backend-product', 'uses' => 'HomeController@queryProduct'));
        Route::get('/barcode', array('as' => 'backend-barcode', 'uses' => 'HomeController@barcode'));
        Route::post('/insert', array('as' => 'backend-insert-barcode', 'uses' => 'HomeController@insertBarcode'));
    });


    Route::group(array('prefix' => 'mitumori'), function () {
        Route::get('/', array('as' => 'backend-mitumori', 'uses' => 'Backend\MitumoriController@getIndex'));
        Route::post('/', array('as' => 'backend-mitumori-post', 'uses' => 'Backend\MitumoriController@postIndex')); //日本查詢單號
        Route::get('{id}/edit', array('as' => 'backend-mitumori-edit', 'uses' => 'Backend\MitumoriController@getEdit'));
        Route::post('{id}/edit', array('as' => 'backend-mitumori-edit-post', 'uses' => 'Backend\MitumoriController@postEdit')); //提交
        Route::post('item/{id}', array('as' => 'backend-mitumori-item-post', 'uses' => 'Backend\MitumoriController@postItem')); //儲存
        Route::post('/rate', array('as' => 'backend-mitumori-change-rate', 'uses' => 'Backend\MitumoriController@changeRate'));
        Route::post('/delete', array('as' => 'backend-mitumori-delete-post', 'uses' => 'Backend\MitumoriController@postDelete'));

    });

    Route::group(array('prefix' => 'groupbuy'), function () {
        Route::get('/', array('as' => 'backend-groupbuy', 'uses' => 'Backend\GroupBuyController@getIndex'));
        Route::post('/', array('as' => 'backend-groupbuy-post', 'uses' => 'Backend\GroupBuyController@postIndex')); //日本查詢單號
        Route::get('{id}/edit', array('as' => 'backend-groupbuy-edit', 'uses' => 'Backend\GroupBuyController@getEdit')); //建立商品
        Route::post('{id}/edit', array('as' => 'backend-groupbuy-edit-post', 'uses' => 'Backend\GroupBuyController@postEdit'));//提交
        Route::post('item/{id}', array('as' => 'backend-groupbuy-item-post', 'uses' => 'Backend\GroupBuyController@postItem'));//儲存
        Route::post('/rate', array('as' => 'backend-groupbuy-change-rate', 'uses' => 'Backend\GroupBuyController@changeRate'));
        Route::post('/delete', array('as' => 'backend-groupbuy-delete-post', 'uses' => 'Backend\GroupBuyController@postDelete'));
    });

    Route::group(array('prefix' => 'campaigns'), function () {
        Route::get('/', array('as' => 'backend-campaigns', 'uses' => 'Backend\CampaignController@getCampaignPromotion'));
        Route::get('/list', array('as' => 'backend-campaigns-list', 'uses' => 'Backend\CampaignController@getCampaignPromotionList'));
        Route::post('/import-data', array('as' => 'backend-import-data', 'uses' => 'Backend\CampaignController@postImportData'));
        Route::post('/postcampaign', array('as' => 'backend-post-campaign', 'uses' => 'Backend\CampaignController@postCampaign'));
//        Route::post('/', array('as' => 'backend-groupbuy-post', 'uses' => 'Backend\CampaignController@postIndex')); //日本查詢單號
    });

    Route::group(array('prefix' => 'propagandas'), function () {
        Route::get('/' , ['as' => 'backend-propagandas', 'uses'=>'Backend\PropagandaController@getIndex']);
        Route::delete('/' , ['as' => 'backend-propagandas', 'uses'=>'Backend\PropagandaController@deleteIndex']);
        Route::get('create', ['uses' => 'Backend\PropagandaController@getCreate', 'as' => 'backend-propagandas-create']);
        Route::post('create', ['uses' => 'Backend\PropagandaController@postCreate', 'as' => 'backend-propagandas-create']);
        Route::get('{id}', ['uses' => 'Backend\PropagandaController@getEdit', 'as' => 'backend-propagandas-edit']);
        Route::post('{id}', ['uses' => 'Backend\PropagandaController@postEdit', 'as' => 'backend-propagandas-edit']);
    });

});


