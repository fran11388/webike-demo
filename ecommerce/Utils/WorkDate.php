<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/10/30
 * Time: 上午 11:28
 */

namespace Ecommerce\Utils;

use Cache;
use Carbon\Carbon;
use DB;

class WorkDate
{

    private static function _getCalendar(){

        $today = Carbon::today('Asia/Taipei');
        $dateOfNextMonth = Carbon::today('Asia/Taipei')->addMonth(1);
        $query = "
select title,start,end from calendar 
where start >= ? and end <= ? order by start       
        ";
        $calendar = [];
        $result = DB::connection('ec_dbs')->select($query, [$today->format("Y-m-d"), $dateOfNextMonth->format("Y-m-d")]);
        foreach ($result as $entity) {
            $start = Carbon::parse($entity->start, 'Asia/Taipei');
            $end = Carbon::parse($entity->end, 'Asia/Taipei')->addDay(-1);
            $length = $end->diffInDays($start);
            for ($i = 0; $i <= $length; $i++) {
                $date = $start;
                $date = $date->addDay($i);
                $object = new \stdClass();
                $object->taiwan = true;
                $object->japan = true;
                if ($entity->title === '日本休假日' ){
                    $object->taiwan = false;
                }
                $calendar[$date->format("Y-m-d")] = $object;
            }

        }

        Cache::put('workdate-calendar', $calendar, 60);
        return $calendar;
    }

    public static function getCalendar()
    {
        return Cache::remember('workdate-calendar', 60, function() {
            return self::_getCalendar();
        });
    }

    private static function isHoliday($date, $country)
    {
        $calendar = self::getCalendar();
        if (array_key_exists($date, $calendar) && $calendar[$date]->$country === true)
            return true;
        return false;
    }



    public static function getJapanMakerDeliveryDate($datetime, $interval)
    {

        $deliveryDate = Carbon::parse($datetime, 'Asia/Taipei');
        $startDate = Carbon::parse($datetime, 'Asia/Taipei')->format("Y-m-d");

        if (!self::isHoliday($deliveryDate->format("Y-m-d") , 'japan') &&
            Carbon::parse($startDate . " 06:00:00", 'Asia/Taipei')->lessThan($datetime)
        ){
            $interval += 1;
        }
        if ($interval < 0)
            $interval = 0;

        $deliveryDate->addDay($interval);
        return self::getJapanDeliveryDate($deliveryDate->format("Y-m-d 15:00:00") , 0);

    }
    public static function getJapanDeliveryDate($datetime, $interval)
    {

        $deliveryDate = Carbon::parse($datetime, 'Asia/Taipei');
        $startDate = Carbon::parse($datetime, 'Asia/Taipei')->format("Y-m-d");

        if (!self::isHoliday($deliveryDate->format("Y-m-d") , 'japan') &&
            Carbon::parse($startDate . " 06:00:00", 'Asia/Taipei')->lessThan(Carbon::parse($datetime, 'Asia/Taipei'))
        ){
            $interval += 1;
        }

        $interval += 2;

        if ($interval < 0)
            $interval = 0;

        while (  $interval > 0  || self::isHoliday( $deliveryDate->format("Y-m-d"),'japan')){
            if (!self::isHoliday( $deliveryDate->format("Y-m-d"),'japan')){
                $interval -=1;
                $deliveryDate->addDay(1);
            } else {
                $deliveryDate->addDay(1);
            }
        }

//        $deliveryDate->addDay(2);

        return self::getTaiwanDeliveryDate($deliveryDate->format("Y-m-d 15:00:00") , 0);
    }


    public static function getTaiwanDeliveryDate($datetime, $interval)
    {

        $deliveryDate = Carbon::parse($datetime, 'Asia/Taipei');
        $startDate = Carbon::parse($datetime, 'Asia/Taipei')->format("Y-m-d");

        if (!self::isHoliday($deliveryDate->format("Y-m-d") , 'taiwan') &&
            Carbon::parse($startDate . " 16:00:00", 'Asia/Taipei')->lessThan(Carbon::parse($datetime, 'Asia/Taipei'))
        ){
            $interval += 1;
        }
        if ($interval < 0)
            $interval = 0;

        while (  $interval > 0  || self::isHoliday( $deliveryDate->format("Y-m-d"),'taiwan')){
            if (!self::isHoliday( $deliveryDate->format("Y-m-d"),'taiwan')){
                $interval -=1;
                $deliveryDate->addDay(1);
            } else {
                $deliveryDate->addDay(1);
            }
        }

        return $deliveryDate->format("Y-m-d 15:00:00");
    }

    public static function getTaiwanShippingDate($datetime, $interval)
    {
        $deliveryDate = Carbon::parse($datetime, 'Asia/Taipei');

        while (  $interval > 0  || self::isHoliday( $deliveryDate->format("Y-m-d"),'taiwan')){
            if (!self::isHoliday( $deliveryDate->format("Y-m-d"),'taiwan')){
                $interval -=1;
                $deliveryDate->addDay(1);
            } else {
                $deliveryDate->addDay(1);
            }
        }
        return $deliveryDate->format("Y-m-d");
    }


}
