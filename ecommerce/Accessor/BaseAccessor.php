<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:07
 */

namespace Ecommerce\Accessor;

abstract class BaseAccessor
{
    const CACHE_LIFETIME = 5;
    protected $model;

    abstract public function __construct($model);


    public function __get($name)
    {
        return $this->model[$name];
    }

}