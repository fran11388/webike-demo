<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:07
 */

namespace Ecommerce\Accessor;


use Ecommerce\Service\CartService;
use Ecommerce\Service\ProductService;
use Ecommerce\Shopping\Payment\CoreInstallment;
use Ecommerce\Service\QaService;
use Everglory\Constants\CustomerRole;
use Everglory\Constants\PointType;
use Everglory\Constants\ProductType;
use Everglory\Constants\SEO;
use Everglory\Models\Stock\Additional as AdditionalItem;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use Everglory\Models\Review;
use Illuminate\Support\Collection;
use App;
use Illuminate\Support\Facades\Cache;
use stdClass;

class ProductAccessor extends BaseAccessor
{
    const CACHE_LIFETIME = 5;

    private $option_data;

    public function __construct($product, $fetch = false)
    {
        if ($product instanceof Product) {
            $this->model = $product;
            if ($fetch) {
                $this->fetchSelectsAndOptions();
            }
            if($product->type == ProductType::GENUINEPARTS){
                $this->model->full_name = '【' . $product->manufacturer->name . '正廠零件】' . $product->name;
            }else{
                $this->model->full_name = '【' . $product->manufacturer->name . '】' . $product->name;
            }
            $this->model->in_stock_flg = null;
            return;
        } else if ($product instanceof stdClass) {
            $this->model = $this->objectToEloquent($product);
            $this->model->full_name = '【' . $product->manufacturer_name . '】' . $product->name;
        } else {
            return null;
        }
    }

    private function objectToEloquent($object)
    {

        $eloquent = clone app()->make('EmptyProduct');

        if(isset($object->category_id)){
            $eloquent->category_id = last($object->category_id);
        }else{
            $eloquent->category_id = 1;
        }
        $eloquent->name = $object->name;
        $eloquent->description = "";
        $eloquent->manufacturer_id = $object->manufacturer_id;
        $eloquent->url_rewrite = $object->sku;
        $eloquent->sku = $object->sku;
        $eloquent->model_number = $object->model_number;
        $eloquent->cost = $object->cost;
        $eloquent->price = $object->price;
        $eloquent->p_country = $object->p_country;
        if(!property_exists($object,'price_diff_2')){
            $eloquent->price_diff_2 = 0;
            \Log::warning('sku: '.$eloquent->sku.' price_diff_2 setting does not exist!' );
        }else{
            $eloquent->price_diff_2 = $object->price_diff_2;
        }
        if(!property_exists($object,'point_0')){
            $eloquent->point = 0;
            \Log::warning('sku: '.$eloquent->sku.' point setting does not exist!' );
        }else{
            $eloquent->point = $object->point_0;
        }
        $eloquent->type = $object->type;
        $eloquent->attribute_tag = null;
        if(isset($object->attribute_tag)){
            $eloquent->attribute_tag = $object->attribute_tag;
        }
        $eloquent->in_stock_flg = (isset($object->in_stock_flg)  ? $object->in_stock_flg : null);

        $eloquent->points = new Collection();
        $eloquent->prices = new Collection();
        $eloquent->images = new Collection();
        $eloquent->fit_models = new Collection();
        $eloquent->motor_names = new Collection();
        $eloquent->options = new Collection(); //now only outlet product

        $eloquent->manufacturer = new \stdClass();

        for ($i = 0; $i < 5; $i++) {
            $attribute = 'price_' . $i;
            if (property_exists($object, $attribute)) {
                $price = new \stdClass();
                $price->role_id = $i;
                $price->price = $object->$attribute;
                $eloquent->prices->put(null, $price);
            }
        }

        for ($i = 0; $i < 5; $i++) {
            $attribute = 'point_' . $i;
            if (property_exists($object, $attribute)) {
                $point = new \stdClass();
                $point->role_id = $i;
                $point->point = $object->$attribute;
                $eloquent->points->put(null, $point);
            }
        }
        $eloquent->point_type = 0;
        if (property_exists($object, 'point_type')) {
            $eloquent->point_type = $object->point_type;
        }

        if (property_exists($object, 'thumbnails')) {
            $image_count = count($object->thumbnails);
            for ($i = 0; $i < $image_count; $i++) {
                $image = new \stdClass();
                $image->thumbnail = $object->thumbnails[$i];
                $image->image = '';
                if (property_exists($object, 'images')) {
                    $image->image = $object->images[$i];
                }
                $eloquent->images->put(null, $image);
            }
        }

        if (property_exists($object, 'fit_models')) {
            foreach ($object->fit_models as $fit_model) {
                $model = new \stdClass();
                $model->model = $fit_model;
                $eloquent->fit_models->put(null, $model);
            }
        }

        if (property_exists($object, 'motor_name')) {
            foreach ($object->motor_name as $motor_name) {
                $model = new \stdClass();
                $model->name = $motor_name;
                $eloquent->motor_names->put(null, $model);
            }
        }


        if (property_exists($object, 'options')) {
            foreach ($object->options as $option) {
                $model = new \stdClass();
                $model->name = $option;
                $eloquent->options->put(null, $model);
            }
        }

        


        $eloquent->manufacturer->name = $object->manufacturer_name;
        $eloquent->manufacturer->website = $object->manufacturer_name;
        list(
            $eloquent->manufacturer->name,
            $eloquent->manufacturer->url_rewrite,
            $eloquent->manufacturer->short_description,
            $eloquent->manufacturer->image) = explode('@', $object->manufacturer_name_url);
        $eloquent->manufacturer->country = $object->manufacturer_country;

        if (property_exists($object, 'model_name_group'))
            $eloquent->model_name_group = $object->model_name_group;
        else
            $eloquent->model_name_group = '';

        if (property_exists($object, 'summary'))
            $eloquent->summary = $object->summary;
        else
            $eloquent->summary = '';


        $review_info = new \stdClass();
        if (property_exists($object, 'ranking'))
            $review_info->average = round($object->ranking);
        else
            $review_info->average = 0;



        if (property_exists($object, 'review_count'))
            $review_info->count = $object->review_count;
        else
            $review_info->count = 0;

        $eloquent->review_info = $review_info;




        if (property_exists($object, 'model_name'))
            $eloquent->model_name = $object->model_name;




        $eloquent->relation_product_id = $object->id;
        if (property_exists($object, 'relation_product_id'))
        $eloquent->relation_product_id = $object->relation_product_id;

        if (property_exists($object, '_numFound'))
            $eloquent->_numFound = $object->_numFound;

        return $eloquent;
    }

    public function getPrice()
    {
        $price = $this->model->price;
        if($this->model->type == 1){
            // genuineparts using estimate detail
            $item = \Everglory\Models\Estimate\Item::where('product_id',$this->model->id)
                ->join('estimate_item_details', function ($join) {
                    $join->on('estimate_items.id', '=', 'estimate_item_details.item_id')
                        ->where('estimate_item_details.attribute', '=', 'original_price');
                })->first();
            if($item and $item->value){
                return $item->value;
            }
        }

        return $price;
    }

    /**
     * @param Customer|null $customer
     * @param null $return_price_name
     * @return mixed
     */
    public function getDifferencePrice($customer)
    {
        $price = 0;
        $customer_role_id = CustomerRole::GENERAL;
        if ($customer) {
            $customer_role_id = $customer->role_id;
        }

        if ($customer_role_id == CustomerRole::WHOLESALE or $customer_role_id == CustomerRole::STAFF or $customer_role_id == CustomerRole::BUYER) {
            return null;
        }
        if(strtotime('now') < strtotime('2017-07-01') or strtotime('now') >= strtotime('2017-08-01')){
            return null;
        }
        if(isset($this->model->price_diff_2)){
            $price = $this->model->price_diff_2;
        }else{
            $_price = $this->model->priceDifference;
            if($_price){
                $price = $_price->price_final;
            }
        }
        /*
         * now product price need > prev month discount price
         */
        if(!$price or $this->price < $price){
            return null;
        }
        return $price;
    }

    public function getFinalPrice($customer, &$return_price_name = null)
    {
        $customer_role_id = CustomerRole::GENERAL;
        if ($customer) {
            $customer_role_id = $customer->role_id;
        }

        $price = $this->model->price;

        if ($this->model->prices) {

            if ($customer_role_id == CustomerRole::STAFF or $customer_role_id == CustomerRole::BUYER) {
                $customer_role_id = CustomerRole::WHOLESALE;
            }elseif($customer_role_id == CustomerRole::REGISTERED) {
                $customer_role_id = CustomerRole::GENERAL;
            }

            $_price = $this->model->prices->where('role_id', $customer_role_id)->first();

            if ($_price) {
                $price = $_price->price;
                if (isset($return_price_name)) {
                    if (property_exists($price, 'name'))
                        $return_price_name = $_price->name;
                }
            }

        }

        return $price;
    }

    public function getNormalPrice()
    {
        $price = $this->model->price;
        if($this->model->type == 1){
            // genuineparts using estimate detail
            $item = \Everglory\Models\Estimate\Item::where('product_id',$this->model->id)
                ->join('estimate_item_details', function ($join) {
                    $join->on('estimate_items.id', '=', 'estimate_item_details.item_id')
                        ->where('estimate_item_details.attribute', '=', 'normal_price');
                })->first();
            if($item and $item->value){
                return $item->value;
            }

        }else{
            if ($this->model->prices) {
                $_price = $this->model->prices->where('role_id', CustomerRole::GENERAL)->first();
                if ($_price) {
                    return $_price->price;
                }
            }
        }

        return $price;
    }

    public function getPoint()
    {
        $point = $this->model->point;
        return $point;
    }

    public function getFinalPoint($customer)
    {
        $customer_role_id = CustomerRole::GENERAL;
        if ($customer) {
            $customer_role_id = $customer->role_id;
        }
        $point = $this->model->point;
        if ($this->model->points) {
            if ($customer_role_id == CustomerRole::STAFF or $customer_role_id == CustomerRole::BUYER) {
                $customer_role_id = CustomerRole::WHOLESALE;
            } elseif($customer_role_id == CustomerRole::REGISTERED) {
                $customer_role_id = CustomerRole::GENERAL;
            }

            $_point = $this->model->points->where('role_id', $customer_role_id)->first();
            if ($_point) {
                $point = $_point->point;
            }

        }
        return $point;
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public function checkPointDiscount($customer = null)
    {
        $customer_role_id = 0;
        if ($customer) {
            $customer_role_id = $customer->role_id;
        }
        if (!(in_array($customer_role_id, [CustomerRole::REGISTERED, CustomerRole::GENERAL, CustomerRole::STAFF,0]))) {
            return false;
        }
        if($this->model->type != ProductType::OUTLET and strtotime('now') >= strtotime('2019-03-29') and strtotime('now') < strtotime('2019-04-04')){
            if (in_array($this->model->manufacturer_id, [10,14,56,146,894]) and $this->model->p_country == "T") {
                return true;
            }
        }elseif($this->model->type != ProductType::OUTLET and strtotime('now') >= strtotime('2019-04-05') and strtotime('now') < strtotime('2019-04-11')){
            if (in_array($this->model->manufacturer->url_rewrite, ['t2514','t38','t1994','t1961','t1895']) and $this->model->p_country == "T") {
                return true;
            }
        }elseif($this->model->type != ProductType::OUTLET and strtotime('now') >= strtotime('2019-04-12') and strtotime('now') < strtotime('2019-04-18')){
            if (in_array($this->model->manufacturer->url_rewrite, ['t2289','t1914','t3477','t2223','t1886']) and $this->model->p_country == "T") {
                return true;
            }
        }elseif($this->model->type != ProductType::OUTLET and strtotime('now') >= strtotime('2019-04-19') and strtotime('now') < strtotime('2019-04-25')){
            if (in_array($this->model->manufacturer->url_rewrite, ['2030','t2222','1916','t2570','t750'])) {
                return true;
            }
        }elseif($this->model->type != ProductType::OUTLET and strtotime('now') >= strtotime('2019-04-26') and strtotime('now') < strtotime('2019-05-02')){
            if (in_array($this->model->manufacturer->url_rewrite, ['t2198','t2758','t3227','t2012','t1828'])) {
                return true;
            }
        }

        return false;
    }

    public function getFitCataloguesHtml()
    {

        $fitCatalogues = [];
        if ($this->model->model_name_group) {
            foreach ($this->model->model_name_group as $key => $model_names) {
                $model_name_array = explode('@', $model_names);
                $motor = new \stdClass();
                $motor->name = $model_name_array[0];
                $motor->model_names = isset($model_name_array[1]) ? $model_name_array[1] : '';
                $fitCatalogues[] = $motor;
                if ($key == 3) {
                    break;
                }
            }
        }

        $result = '';
        $result_model = [];
        foreach ($fitCatalogues as $key => $fitCatalogue) {
            $model_name_text = '';
            if ($fitCatalogue->name) {
                if ($fitCatalogue->model_names) {
                    $model_name_text .= '(';
                    $model_name_text .= $fitCatalogue->model_names;
                    $model_name_text .= ')';
                }

                $result_model[] = trim($fitCatalogue->name . $model_name_text);
            } else {
                $result_model[] = trim($model_name_text);
            }
        }
        if(count(array_filter($result_model))){
            $result = '<div class="motor">對應:'.(implode('、',$result_model)) . '</div>';
        }
        return $result;

    }

    public function isNextYear()
    {
        //TODO 測測
        //$nextYear = date('Y') + 1;
        //$nextYearProduct = \Product\Attribute\String::where('product_id', $product->id)->where('attribute_string_values', 'like', '%' . $nextYear . '%')->first();
        return false;
    }


    /**
     * @return int|mixed
     */
    /*
    public function getTWStock()
    {
        $cache_key = get_class() . '@' . __FUNCTION__ . '?sku=' . $this->model->sku;

        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $result = \DB::connection('eg_zero')
            ->table('stocks')
            ->where('bar_code', '=', $this->model->sku)
            ->where('location_id', '=', 1)
            ->where('status_id', '=', 1)
            ->where('situation_id', '=', 1)
            ->count();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }
    */

    public function getInStockFlgForList()
    {
        $in_stock_flg = false;
        if(!is_null($this->model->in_stock_flg)){
            $in_stock_flg = $this->model->in_stock_flg;
        }else if(strpos(\Route::currentRouteName(), 'product-detail') !== false){
            $apiResult = ProductService::getStockInfo($this->model);
            if($apiResult and isset($apiResult->stock) and $apiResult->stock and !$apiResult->is_maker_stock){
                $in_stock_flg = $apiResult->stock_type;
            }
        }else{
            $apiResult = ProductService::getStockFlgForList($this->model->sku);
            if($apiResult and isset($apiResult->{$this->model->sku})){
                $in_stock_flg = $apiResult->{$this->model->sku};
            }
        }
        return $in_stock_flg;
    }

    public function getReviewInfo()
    {
        if(!$review_info = $this->model->review_info){
            $reviews = $this->model->reviews->where('view_status', 1);
            $review_info = new \stdClass();
            $review_info->average = round($reviews->avg('ranking'),0);
            $review_info->count = count($reviews);
        }
        return $review_info;

    }

    public function getRelationReviews()
    {
        $relation_key = $this->model->relationKey;


        $cache_key = get_class() . '@' . __FUNCTION__ . '?relation_key=' . ($relation_key ? $relation_key->relation_product_id : $this->model->id) ;

        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
        if($relation_key){
            $result = Review::join('eg_product.product_relations','reviews.product_id','product_relations.product_id')
                ->where('product_relations.relation_product_id',$relation_key->relation_product_id)
                ->where('reviews.view_status', 1)
                ->select('reviews.*')
                ->orderby('created_at', 'DESC')
                ->get();
        }else{
            $result = Review::where('product_id',$this->model->id)->orderby('created_at', 'DESC')->get();
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function getReviews(){

        $cache_key = get_class() . '@' . __FUNCTION__ . '?relation_key=' . ( $this->model->id) ;

        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Review::where('product_id',$this->model->id)->orderby('created_at', 'DESC')->get();


        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function getFitModels($groupByMaker = true)
    {
        $fitModels = $this->model->fitModels;
        if($groupByMaker){
            $fitModels = $fitModels->groupBy('maker');
        }
        return $fitModels;
    }

    public function getMotors($groupByMaker = true)
    {
        if($motors = $this->model->motor_names){
            return $motors; 
        }else if($motors = $this->motors){
            return $motors;
        }else{
            return collect();
        }
    }

    public function getOutletOptions()
    {
        if($options = $this->model->options){
            return $options;
        }else{
            return collect();
        }
    }


    public function isPreorder()
    {
        if ($this->model->preorderItem and strtotime(date('Y-m-d')) <= strtotime($this->model->preorderItem->ended_at))
            return true;
        return false;
    }


    private function fetchSelectsAndOptions()
    {
        $data = Product\Option::join('products', 'product_option.product_id', '=', 'products.id')
            ->join('selects', 'product_option.select_id', '=', 'selects.id')
            ->where('manufacturer_id', $this->model->manufacturer->id)->where('group_code', $this->model->group_code)->where('active', 1)
            ->groupBy('select_id')
            ->groupBy('name')
            ->select('product_option.*', 'products.sku','products.group_code','manufacturer_id', 'selects.name as select_name', 'selects.sort as select_sort')
            ->orderBy('selects.sort', 'product_option.sort')
            ->get();

        $result = [];
        $data = $data->groupBy('select_id')->sortBy(function ($product, $key) {
            return $key;
        });
        foreach ($data as $key => $options) {
            $select = new \stdClass();
            $select->options = $options->sortBy('sort');
            $select->id = $options->first()->select_id;
            $select->name = $options->first()->select_name;

            $result[] = $select;
        }


        if ((count($result) == 1) and (!strpos($this->model->sku, 't'))) {
            $_products = Product::whereIn('id', $result[0]->options->pluck('product_id'))->select(['id', 'sku'])->get();
            foreach ($result[0]->options as &$option) {
                $obj = $_products->where('id', $option->product_id)->first();
                $option->product_sku = $obj->sku;
            }
            $result[0]->skus = $_products->implode('sku', ',');
        }

        $this->option_data = $result;
    }


    /**
     * @return mixed
     * return same group products's selects and options
     */
    public function getGroupSelectsAndOptions()
    {
        return $this->option_data;
    }

    /**
     * @return mixed
     * return self product's selects and options
     */
    public function getSelectsAndOptions()
    {
        $collection = collect();
        foreach($this->model->options as $option) {
            $object = new \stdClass();
            $object->label = $option->label->name;
            $object->option = $option->name;
            $collection->push($object);
        }
        return $collection;
    }


    public function getThumbnail()
    {
        if ($image = $this->model->images->first() and $image->thumbnail) {
            return cdnTransform(str_replace('http:','',$image->thumbnail));
        } else {
            return cdnTransform(NO_IMAGE);
        }
    }

    public function getImage()
    {
        if ($image = $this->model->images->first() and $image->image) {
            return fixHttpText(cdnTransform($image->image));
        } else {
            return cdnTransform(NO_IMAGE);
        }
    }

    public function getOrigImage()
    {
        if ($image = $this->model->images->first() and $image->image) {
            return fixHttpText($image->image);
        } else {
            return NO_IMAGE;
        }
    }

    public function getAllImages()
    {
        $package = new \StdClass;
        $images = [];
        $thumbnails = [];
        foreach ($this->model->images as $image){
            $images[] = fixHttpText(cdnTransform($image->image));
            $thumbnails[] = fixHttpText(cdnTransform($image->thumbnail));
        }
        if (!count($images)) {
            $images = [NO_IMAGE];
        }
        if (!count($thumbnails)) {
            $thumbnails = [NO_IMAGE];
        }
        $package->images = $images;
        $package->thumbnails = $thumbnails;

        return $package;
    }

    public function getCartPrice($customer, &$price_name = null)
    {
        if (!$customer and !\Auth::check()) {
            return $this->getFinalPrice($customer);
        } else if (!$customer and \Auth::check()) {
            $customer = \Auth::user();
        }


        if ($this->checkPointDiscount($customer)) {
            return $this->getFinalPrice($customer, $price_name) - $this->getFinalpoint($customer);
        } else {
            return $this->getFinalPrice($customer, $price_name);
        }

    }


    public function getCartPoint( $customer = null )
    {

        if (!$customer and !\Auth::check()) {
            return $this->getFinalPoint($customer);
        } else if (!$customer and \Auth::check()) {
            $customer = \Auth::user();
        }

        if($customer){
            if( $this->checkPointDiscount($customer) ){
                return false;
            }else{
                return $this->getFinalPoint($customer);
            }
        }
    }


    public function getMainProduct()
    {
        if($this->model->type == 4){
            $additional = AdditionalItem::where('product_id', $this->model->id)->first();
            if( $additional ){
                return Product::where('id', $additional->reflect_id)->first();
            }else{
                return $this;
            }
        }else if($this->model->is_main == 1){
            return $this;
        }else{
            return Product::where('manufacturer_id' , $this->model->manufacturer_id)->where( 'group_code' , $this->model->group_code)->orderBy('id')->first();
        }
    }

    public function getManufacturerName()
    {
        return $this->model->manufacturer->name;
    }

    public function getAdditionalData()
    {
        if($this->model->type == 4){
            $object = new \stdClass();

            $additional_item = $this->model->reflect()->with(['area','reflect'])->first();
            if($additional_item){
                $area = $additional_item->area;
                if($area){
                    $tw_stock = ProductService::getStockInfo($additional_item->reflect);
                    if($tw_stock and $tw_stock->stock and !$tw_stock->sold_out){
                        $object->trigger_price = $area->trigger_price;
                        $object->stock_quantity = $tw_stock->stock;
                        return  $object;
                    }
                }
            }
        }
        return null;
    }
    
    public function getProductDescription()
    {
        $content_cols = ['summary', 'sentence', 'remarks', 'caution'];
        $productDescription = $this->model->productDescription;
        return $this->descriptionOutputTransform($productDescription, $content_cols);
    }

    public function hasNotTransDescription()
    {
        $content_cols = ['summary', 'sentence', 'remarks', 'caution'];
        $productDescription = $this->model->descriptionOrig;
        return $this->descriptionOutputTransform($productDescription, $content_cols);
        /*
        if($this->model->productDescription){
            $description_array = $this->model->productDescription->toArray();
            foreach ($description_array as $key => $value){
                if((strpos($key, 'flg') !== false) and $value < 8){
                    return true;
                }
            }
        }
        return false;
        */
    }

    private function descriptionOutputTransform($description_model, $content_cols)
    {
        if($description_model){
            foreach ($content_cols as $content_col){
                $description_model->$content_col = str_replace('※','※ ',str_replace('=/catalogue','=//www.webike.net/catalogue/',str_replace('"/catalogue/', '"//www.webike.net/catalogue/', str_replace('http://','//',$description_model->$content_col))));
                preg_match_all('/<(table?)(.*?)(width?).*?>/', $description_model->$content_col, $matches);
                if(isset($matches[0])){
                    foreach ($matches[0] as $matche){
                        $convert_text = str_replace('width', 'data-width', $matche);
                        $convert_text = str_replace('height', 'data-height', $convert_text);
                        $description_model->$content_col = str_replace($matche, $convert_text, $description_model->$content_col);
                    }
                }
                preg_match_all('/<(iframe?)(.*?)(width?).*?>/', $description_model->$content_col, $matches);
                if(isset($matches[0])){
                    foreach ($matches[0] as $matche){
                        $convert_text = str_replace('width', 'data-width', $matche);
                        $convert_text = str_replace('height', 'data-height', $convert_text);
                        $description_model->$content_col = str_replace($matche, $convert_text, $description_model->$content_col);
                    }
                }
                preg_match_all('/<(img?)(.*?)(height?).*?>/', $description_model->$content_col, $matches);
                if(isset($matches[0])){
                    foreach ($matches[0] as $matche){
                        $convert_text = str_replace('height', 'data-height', $matche);
                        $description_model->$content_col = str_replace($matche, $convert_text, $description_model->$content_col);
                    }
                }
                $description_model->$content_col = preg_replace('/(alt)=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']/', 'alt="' . $this->full_name . ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT . '"', $description_model->$content_col);
                if(!hasHtml($description_model->$content_col)){
                    $description_model->$content_col = nl2br($description_model->$content_col);
                }
            }
        }
        return $description_model;
    }
    
    public function getInformation($group_selected = true)
    {
        $result = new \stdClass();
        $result->stock = null;
        $result->estimate_flg = false;
        $result->buy_flg = false;
        if(is_null($this->group_code) or $group_selected){
            $result->stock = ProductService::getStockInfo($this->model);
            $result->estimate_flg = $result->stock->can_estimate;

            if(!$result->stock->sold_out and (isset($result->stock->discontinued) and !$result->stock->discontinued)){

                $result->buy_flg = true;
            }
        }
        return $result;
    }

    public function getInstallments($customer)
    {
        $coreInstallment = new CoreInstallment();
        $customer_role_id = CustomerRole::GENERAL;
        if ($customer) {
            $customer_role_id = $customer->role_id;
        }
        $price = $this->getFinalPrice($customer);
        return $coreInstallment->getPublishInstallments($customer_role_id, $price);
    }

    public function getTagsWithoutStock($customer)
    {
        $tags = [];
        $final_price = $this->getFinalPrice($customer);
        if($this->getDifferencePrice($customer)){
            $tags['特別折扣'] = 'tag label label-urgent';
        }else if($final_price and round(($final_price / $this->price) * 100 ) < 95){
            $tags['SALE'] = 'tag label label-danger';
        }
        if($this->type == ProductType::OUTLET){
            $tags['Outlet'] = 'tag label label-warning';
        }
        if (activeShippingFree() and $this->getFinalPrice($customer) >= FREE_SHIP){
            $tags['免運費'] = 'tag label label-normal';
        }

        if($this->checkPointDiscount($customer)){
            $tags['點數現折'] = 'tag label label-purple';
        }

        $final_point = $this->getFinalPoint($customer);
        if ($final_price > 0 and round($final_point / ($final_price * 0.01)) >= 2) {
            $tags['點數' . round($final_point / ($final_price * 0.01)) . '倍'] = 'tag label label-success';
        }

        if (strpos($this->attribute_tag, date('Y', strtotime(date('Y') . ' +1 year'))) !== false) {
            if(strpos($this->attribute_tag, '秋冬') !== false){
                $tags[$this->attribute_tag] = 'tag label label-winter';
            }
        }
        
        if(strpos($this->attribute_tag,'送潑水紙巾') !== false){
            $tags[$this->attribute_tag] = 'tag label label-helmet';
        }

        if(strpos($this->attribute_tag,'送涼感頭套') !== false){
            $tags[$this->attribute_tag] = 'tag label label-cold-man';
        }
        
        if((strpos($this->attribute_tag,'送驗電筆') !== false) or (strpos($this->attribute_tag,'送側柱墊板') !== false)){
            $tags[$this->attribute_tag] = 'tag label label-motobatt';
        }
        
        if (strpos($this->attribute_tag, date('Y')) !== false) {
            if(strpos($this->attribute_tag, '春夏') !== false){
                $tags[$this->attribute_tag] = 'tag label label-cold';
            }else{
                $tags[$this->attribute_tag] = 'tag label label-normal';
            }
        }
        
        return $tags;
    }

    public function getTags($customer)
    {
        $tags = self::getTagsWithoutStock($customer);
        if($stock_type = $this->getInStockFlgForList()){
            $stockTag = self::getStockTagByType($stock_type);
            $tags = array_merge($tags, $stockTag);
        }
//        if(isset($data['jp_stocks'][$this->url_rewrite]) and $data['jp_stocks'][$this->url_rewrite]){
//            $tags['庫存OK'] = 'tag label label-primary';
//        }
        return $tags;
    }

    public function getStockTagByInStockFlg( $product ){
        $tag = null;
        if ($product->in_stock_flg != null){
            switch ((int)$product->in_stock_flg){
                case -1:
                    $tag = ['售完' => 'tag label label-default'];
                    break;
                case 0:
                    break;
                case 1:
                    $tag = ['有庫存' => 'tag label label-primary'];
                    break;
                case 2:
                    $tag = ['有庫存' => 'tag label label-primary'];
                    break;
                case 3:
                    $tag = ['有庫存' => 'tag label label-primary'];
                    break;

            }
        }

        return $tag;
    }



    public function getStockTagByType($stock_type)
    {   
        $tags = [
            'tw_stock' => '有庫存',
            'rcj_stock' => '有庫存'
        ];
        $html_class = 'tag label label-primary';

        if(isset($tags[$stock_type])){
            return [$tags[$stock_type] => $html_class];
        }else{
            return ['庫存OK' => $html_class];
        }
    }

    public function getStockTag()
    {   
        return ['庫存OK' => 'tag label label-primary'];
    }

    public function getExtraDescription()
    {
        $extraDescriptions = [];
        if(strpos($this->attribute_tag, date('Y') . '春夏') !== false or strpos($this->attribute_tag, date('Y') . '秋冬') !== false){
            $extraDescriptions[] = '<span class="font-color-red font-bold">※' . $this->attribute_tag . '新品。</span><br>';
        }

        return count($extraDescriptions) ? $extraDescriptions : null;
    }

    public function getMpttCategory()
    {
        return $this->model->categories()->join('mptt_categories_flat as mptt','category_product.category_id','=','mptt.id')->select('mptt.*')->orderBy('depth','desc')->first();
    }

    public function getQa()
    {
        if (!useCache()) {
            \Cache::forget('product-qa-' . $this->model->url_rewrite);
        }

        $answers = Cache::remember('product-qa-' . $this->model->url_rewrite, 60, function() {
            $answers = QaService::getQuestionAnswers($this->model)->filter(function (&$answer) {
                return $answer->question->active;
            })->sortBy(function ($answer) {
                // $counter = QaService::getPraiseCount($answer->id)->first();
                return $answer->question->sort;
            });
            return $answers;
        });
        return $answers;
    }

}