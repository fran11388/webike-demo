<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:07
 */

namespace Ecommerce\Accessor;


use Everglory\Models\Order;
use Everglory\Models\Sales\History;

class OrderAccessor extends BaseAccessor
{
    const CACHE_LIFETIME = 5;


    public function __construct($order)
    {
        if ($order instanceof Order) {
            $this->model = $order;
            return;
        } else {
            return null;
        }
    }

    public function changeStatus( $status , $comment){
        $history = new History();
        $history->status_id = $status->id;
        $history->order_id = $this->id;
        $history->comment = $comment;
        $history->save();
        $this->model->status_id = $status->id;
        if ($this->model->isDirty('status_id')){
            $this->model->save();
        }

    }

}