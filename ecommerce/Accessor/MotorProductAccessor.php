<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:07
 */

namespace Ecommerce\Accessor;

use Everglory\Models\MotoMarket\Product;
use Illuminate\Support\Collection;
use App;

class MotorProductAccessor extends BaseAccessor
{
    const CACHE_LIFETIME = 5;
    const IMAGE_PATH_TW = 'https://img.webike.tw/moto_photo/';
    const PRODUCT_PATH = 'http://www.webike.tw/motomarket/detail/';
    const UNIT_TEXT = '萬';
    const UNIT_VALUE = 10000;
    const DISCUSS_TEXT = '電洽';

    public function __construct($product)
    {
        if ($product instanceof Product) {
            $this->customer = $product->customer;
            $this->loadColumns($product);
        } else {
            return null;
        }
    }

    public function loadColumns(Product $product)
    {
        $this->model = $product;
        $this->importBasicData();
    }

    public function importBasicData()
    {
        $this->setImage();
        $this->setProductLink();
        $this->setPrice();
        $this->setProfiles();
        $this->setMotorModel();
        $this->setManufacturer();
    }

    public function setImage()
    {
        $photo_group = unserialize($this->model->motor_photos);
        $image = NO_IMAGE;
        if( isset($photo_group[0]) ){
            $obj = $photo_group[0];
            $image = self::IMAGE_PATH_TW . $obj->filename;
        }
        $this->image = $image;
        return $image;
    }

    public function setProductLink()
    {
        $link = self::PRODUCT_PATH . $this->model->serial;
        $this->link = $link;
        return $link;
    }

    public function setPrice()
    {
        if($this->model->price >= self::UNIT_VALUE){
            $this->model->price = $this->model->price / self::UNIT_VALUE;
            $this->model->save();
        }

        if($this->model->price){
            $this->price_text = sprintf('%g', number_format($this->model->price, 2)) . self::UNIT_TEXT;
        }else{
            $this->price_text = self::DISCUSS_TEXT;
        }
        return $this->price_text;
    }

    public function setMotorModel()
    {
        $this->motor_model = $this->model->motorModel;
        $this->motor_model_name = null;
        if($this->motor_model){
            $this->motor_model_name = $this->model->motorModel->title;
        }
    }

    public function setManufacturer()
    {
        $this->manufacurer = null;
        if($this->motor_model){
            $this->manufacurer = $this->motor_model->manufacturer;
        }
    }

    public function getFullName()
    {
        if($this->manufacurer and $this->motor_model_name){
            return $this->manufacurer . ' ' . $this->motor_model_name;
        }else{
            return null;
        }
    }

    public function setProfiles()
    {
        $this->profiles = null;
        if($this->customer){
            $this->profiles = $this->getProfileDetail($this->customer->profiles);
        }
    }

    private function getProfileDetail($profiles)
    {
        $details = $profiles->keyBy('attribute');
        $data = new \stdClass();
        foreach ($details as $key => $detail) {
            $data->$key = $detail->value;
        }
        return $data;
    }

}