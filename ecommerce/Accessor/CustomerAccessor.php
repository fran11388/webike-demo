<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:07
 */

namespace Ecommerce\Accessor;

use Ecommerce\Repository\CustomerLevelRepository;
use Everglory\Models\Customer;
use Everglory\Models\Coupon;
use Everglory\Constants\CustomerRole;
use Ecommerce\Service\CartService;

class CustomerAccessor extends BaseAccessor
{
    const CACHE_LIFETIME = 5;

    private $current_level = null;
    private $next_level = null;
    private $level_repository;
    private $load_level = false;
    /*
     * load customer
     *
     * */
    public function __construct($customer)
    {
        if ($customer instanceof Customer) {
            $this->model = $customer;
//            $attributes = [
//                'visits',
//                'address',
//                'orders',
//                'points',
//                'role',
//                'blacklist',
//                'motors',
//            ];
//            foreach ($attributes as $attribute){
//                $this->$attribute = $this->$attribute();
//            }
//            if($this->model->role_id == CustomerRole::WHOLESALE){
//                $this->loadLevel();
//            }
            return;
        } else {
            return null;
        }
    }

    /*
     * 取得客人當前擁有之點數
     */
    public function getCurrentPoints()
    {
        if($this->model){
            $points = $this->model->points;
            if($points){
                return $points->points_current;
            }
        }
        return 0;
    }

    /*
     * 取得客人當前擁有之Coupon
     */
    public function getCurrentCoupons()
    {
        $collection = collect();
        if($this->model) {
            $collection = Coupon::whereNull('order_id')
                ->where('customer_id', $this->model->id)
                ->where('expired_at', '>', date('Y-m-d'))
                ->get();
        }

        return $collection;
    }

    /*
     * 讀取等級資料(經銷)
     */
    private function loadLevel()
    {
        if ($this->model->role_id == CustomerRole::WHOLESALE) {
            $this->level_repository = new CustomerLevelRepository;
            $this->current_level = $this->level_repository->getCurrentLevel($this->model);
            $this->next_level = $this->level_repository->getNextLevel($this->model);
        }
    }

    public function getCurrentLevelData()
    {
        $level_data = null;
        $this->checkLevel();
        if($this->current_level){
            $level = $this->current_level->level;
            $level_data = CustomerLevelRepository::LEVEL[$level];
        }
        
        return $level_data;
    }

    /*
     * 確認等級資料是否已載入(經銷)
     */
    private function checkLevel()
    {
        if(!$this->load_level){
            $this->loadLevel();
            $this->load_level = true;
        }
    }

    /*
     * 取得客人當前等級顯示名稱(經銷)
     */
    public function getCurrentLevelName()
    {
        $this->checkLevel();
        $display_name = null;
        if ($this->model->role_id == CustomerRole::WHOLESALE) {
            //default
            $display_name = CustomerLevelRepository::LEVEL[1]['display_name'];
        }

        if($this->current_level){
            $display_name = CustomerLevelRepository::LEVEL[$this->current_level->level]['display_name'];
        }

        return $display_name;
    }

    /*
     * 取得客人當前等級css class(經銷)
     */
    public function getCurrentLevelCssClass()
    {
        $this->checkLevel();
        $css_class = null;
        if ($this->model->role_id == CustomerRole::WHOLESALE) {
            //default
            $css_class = CustomerLevelRepository::LEVEL[1]['css_class'];
        }

        if($this->current_level){
            $css_class = CustomerLevelRepository::LEVEL[$this->current_level->level]['css_class'];
        }

        return $css_class;
    }

    /*
     * 取得客人當前回饋倍數(經銷)
     */
    public function getCurrentLevelRate()
    {
        $this->checkLevel();
        $rate = null;
        if ($this->model->role_id == CustomerRole::WHOLESALE) {
            //default
            $rate = CustomerLevelRepository::LEVEL[1]['rate'] * 100;
        }

        if($this->current_level){
            $rate = CustomerLevelRepository::LEVEL[$this->current_level->level]['rate'] * 100;
        }

        return $rate;
    }

    /*
     * 取得客人還需多少金額可升級(經銷)
     */
    public function getNextLevelAmount()
    {
        $this->checkLevel();
        return $this->level_repository->toNextLevel($this->model);
    }

    /*
     * 取得客人目前累計金額(經銷)
     */
    public function getCurrentAmount()
    {
        $this->checkLevel();
        $performance = 0;
        if($this->next_level){
            $performance = $this->next_level->performance;
        }
        return $performance;
    }

    public function visits()
    {
        return $this->model->visits;
    }

    public function address()
    {
        return $this->model->address;
    }

    public function orders()
    {
        return $this->model->orders;
    }

    public function points()
    {
        return $this->model->points;
    }

    public function role()
    {
        return $this->model->role;
    }

    public function blacklist()
    {
        return $this->model->blacklist;
    }

    public function motors()
    {
        return $this->model->motors;
    }
}