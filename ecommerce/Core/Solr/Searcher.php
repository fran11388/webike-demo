<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\Solr;


use Ecommerce\Core\Solr\Param\CommonParam;
use Ecommerce\Core\Solr\Param\EdismaxQueryParam;
use Ecommerce\Core\Solr\Param\FacetFieldParam;
use Ecommerce\Core\Solr\Param\FacetQueryParam;
use Ecommerce\Core\Solr\Param\FacetRangeParam;
use Ecommerce\Core\Solr\Param\FilterQueryParam;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Log;
use SolrClient;
use SolrQuery;
use SolrDisMaxQuery;

class Searcher
{
    /**
     * @var CommonParam[] $common
     **/
    private $common;

    /**
     * @var FacetFieldParam[] $facetFields
     **/
    private $facetFields = [];

    /**
     * @var FacetQueryParam[] $facetQueries
     **/
    private $facetQueries = [];

    /**
     * @var FacetRangeParam[] $facetRanges
     **/
    private $facetRanges = [];

    /**
     * @var FilterQueryParam[] $filterQueries
     **/
    private $filterQueries = [];

    /**
     * @var SolrClient $client
     **/
    private $client;

    private $groupField = '';
    private $groupFacet = false;
    private $groupSort = '';
    private $groupSortDirection = SolrQuery::ORDER_DESC;
    private $groupLimit = '1';

    private $edismaxQueryParam;

    /**
     * @return string
     */
    public function getGroupLimit()
    {
        return $this->groupLimit;
    }

    /**
     * @param string $groupLimit
     */
    public function setGroupLimit($groupLimit)
    {
        $this->groupLimit = $groupLimit;
    }

    /**
     * @return string
     */
    public function getGroupField()
    {
        return $this->groupField;
    }



    /**
     * @return string
     */
    public function getGroupSort()
    {
        return $this->groupSort;
    }

    /**
     * @param string $groupLimit
     */

    public function setGroupFacet($value)
    {
        $this->groupFacet = $value;
    }

    /**
     * @return int
     */
    public function getGroupSortDirection()
    {
        return $this->groupSortDirection;
    }

    /**
     * @param int $groupSortDirection
     */
    public function setGroupSortDirection($groupSortDirection)
    {
        $this->groupSortDirection = $groupSortDirection;
    }

    /**
     * @param string $groupField
     */
    public function setGroupField($groupField)
    {
        $this->groupField = $groupField;
    }

    /**
     * @param string $groupSort
     */
    public function setGroupSort($groupSort)
    {
        $this->groupSort = $groupSort;
    }


    public function __construct( $path = "" , $port = null)
    {
        $params['hostname'] = config('solr.hostname');
        $params['port'] = config('solr.port');
        if ($path){
            $params['path'] = $path;
        } else {
            $params['path'] = config('solr.path');
        }
        $params['timeout'] = config('solr.timeout');

        if ($port)
            $params['port'] = $port;


        $this->client = new SolrClient($params);

        $this->client->setResponseWriter("json");
        //$this->client->setServlet(SolrClient::SEARCH_SERVLET_TYPE, 'edismax');


        $this->common = new CommonParam();
    }



    /**
     * @return SearchResponse
     */
    public function query()
    {
        $query = new \SolrDisMaxQuery ();
        $query->setQuery($this->common->getQuery());
        $query->setRows($this->common->getRows());
        if(count($this->common->getSortFields())){
            foreach ( $this->common->getSortFields() as $sortField => $sortDirection ) {
                $query->addSortField($sortField , $sortDirection );
            }
        }else{
            if ($this->common->getSortField()) {
                $query->addSortField($this->common->getSortField(), $this->common->getSortDirection());
            }
        }

        $query->setStart($this->common->getStart());

        if (count($this->filterQueries)) {
            foreach ($this->filterQueries as $q) {
                if($q->getQuery()){
                    $condition = sprintf("{!tag=%s}%s:%s", $q->getTag(), $q->getField(), $q->getQuery());
                }else{
                    if($q->getTag()){
                        $condition = sprintf("{!tag=%s}%s", $q->getTag(), $q->getField());
                    }else{
                        $condition = sprintf("%s", $q->getField());
                    }

                }
                $query->addFilterQuery($condition);

            }
        }

        if (count($this->facetQueries)) {
            foreach ($this->facetQueries as $q) {
                $tag = $this->getTag($q);
                $query->addFacetQuery($tag . $q->getField() . ":" . $q->getQuery());
            }
        }


        if (count($this->facetFields)) {
            $query->setFacet(true);
            foreach ($this->facetFields as $q) {
                $tag = $this->getTag($q);
                $query->addFacetField($tag . $q->getField());
                if ($q->getLimit() !== 0) {
                    $query->setFacetLimit($q->getLimit(), $q->getField());
                }
                if ($q->getOffset()) {
                    $query->setFacetOffset($q->getOffset(), $q->getField());
                }
                if ($q->getSort()) {
                    $query->setFacetSort($q->getSort(), $q->getField());
                }
                if ($q->getPrefix()) {
                    $query->setFacetPrefix($q->getPrefix(), $q->getField());
                }
                if ($q->getMinCount()) {
                    $query->setFacetMinCount($q->getMinCount(), $q->getField());
                }


            }
        }

        if (count($this->facetRanges)) {
            foreach ($this->facetRanges as $q) {
                $tag = $this->getTag($q);
                $query->add('facet.range', $tag . $q->getField());
                $query->add('f.' . $q->getField() . '.facet.range.start', $q->getStart());
                $query->add('f.' . $q->getField() . '.facet.range.end', $q->getEnd());
                $query->add('f.' . $q->getField() . '.facet.range.gap', $q->getGap());

                if ($q->getOther()) {
                    $query->add('f.' . $q->getField() . '.facet.range.other', $q->getOther());
                }
            }
        }

        if ($this->groupField){
            $query->setGroup(true);
            $query->addGroupField($this->groupField);
            $query->setGroupLimit($this->groupLimit);
            if($this->groupSort){
                $query->addGroupSortField($this->groupSort, $this->groupSortDirection);
            }
            if($this->groupFacet){
                $query->setGroupFacet(true);
            }
        }


        if ($this->edismaxQueryParam){
            $query->setBoostFunction( $this->edismaxQueryParam->getBf() );
        }


        //$query->addGroupField('');
        //$query->addGroupSortField('', 1);


        start_measure('solr', 'Time for searching');
        try{
            $response = $this->client->query($query);
        } catch (\Exception $ex){
            Log::error('solr: '.$query->toString());
            Log::error('solr: '.$ex);
            abort(500);
        }

        stop_measure('solr');
        $trace = debug_backtrace();
        $class = $trace[1]['class'];
        $function = $trace[1]['function'];
        $key = $class . '@' . $function;
        \Debugbar::addMessage($key, 'solr');
        \Debugbar::addMessage(
            'http://' . config('solr.hostname') . ':' . config('solr.port') . '/' . config('solr.path') . '/select?' . urldecode($response->getRawRequest()),
            'solr');

        $responseObject = json_decode($response->getRawResponse());
        return new SearchResponse($responseObject);

    }

    /**
     * @param FacetFieldParam|FacetRangeParam|FacetQueryParam $object
     * @return string
     */
    private function getTag($object)
    {
        $tag[] = 'key=' . $object->getKey();
        if (count($object->getExcludeTags())) {
            $tag[] = 'ex=' . implode($object->getExcludeTags(), ',');
        }
        return '{!' . implode($tag, ' ') . '}';
    }

    /**
     * @return CommonParam
     */
    public function getCommon()
    {
        return $this->common;
    }

    /**
     * @param CommonParam $common
     */
    public function setCommon($common)
    {
        $this->common = $common;
    }

    /**
     * @return FacetFieldParam[]
     */
    public function getFacetFields()
    {
        return $this->facetFields;
    }

    /**
     * @param FacetFieldParam[] $facetFields
     */
    public function setFacetFields($facetFields)
    {
        $this->facetFields = $facetFields;
    }

    /**
     * @return FacetRangeParam[]
     */
    public function getFacetRanges()
    {
        return $this->facetRanges;
    }

    /**
     * @param FacetRangeParam[] $facetRanges
     */
    public function setFacetRanges($facetRanges)
    {
        $this->facetRanges = $facetRanges;
    }

    /**
     * @return FilterQueryParam[]
     */
    public function getFilterQueries()
    {
        return $this->filterQueries;
    }

    /**
     * @param FilterQueryParam[] $filterQueries
     */
    public function setFilterQueries($filterQueries)
    {
        $this->filterQueries = $filterQueries;
    }

    /**
     * @return FacetQueryParam[]
     */
    public function getFacetQueries()
    {
        return $this->facetQueries;
    }

    /**
     * @param FacetQueryParam[] $facetQueries
     */
    public function setFacetQueries($facetQueries)
    {
        $this->facetQueries = $facetQueries;
    }


    public function setEdismax(EdismaxQueryParam $edismaxQueryParam){
        $this->edismaxQueryParam = $edismaxQueryParam;
    }



}