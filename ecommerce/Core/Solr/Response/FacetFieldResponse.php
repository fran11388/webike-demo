<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 12:27
 */

namespace Ecommerce\Core\Solr\Response;

use Illuminate\Support\Collection;

class FacetFieldResponse
{
    private $field = '';
    private $docs;

    public function __construct( $object , $field )
    {
        $this->field = $field;
        $docs = $object->facet_counts->facet_fields->$field;
        $_docs = [];

        for ($i = 0 ; $i < count($docs) ; $i++){
            $_docs[(string) $docs[$i] ] = $docs[$i+1];
            $i++;
        }

        $this->docs = collect($_docs);

    }

    /**
     * @return string
     */
    public function getField(){
        return $this->field;
    }

    /**
     * @return Collection|array[string]int
     */
    public function getDocs(){
        return $this->docs;
    }
    /**
     * @return int
     */
    public function getCount(){
        return $this->docs->count();
    }

}