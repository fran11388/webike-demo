<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 12:27
 */

namespace Ecommerce\Core\Solr\Response;


use SolrObject;

class GroupResponse
{
    private $count = 0;
    private $docs = [];
    private $start = 0;
    private $maches = 0;

    /**
     * QueryResponse constructor.
     * @param SolrObject $object
     */
    public function __construct($object)
    {
        $field = 'group.field';
        $field = $object->responseHeader->params->$field;

        $this->maches = $object->grouped->$field->matches;

        $docs = [];

        foreach ($object->grouped->$field->groups as $group) {
            foreach ($group->doclist->docs as $doc) {
                $doc->{'_numFound'} = $group->doclist->numFound;
                $docs[] = $doc;
            }
        }

        if (count($docs))
            $this->docs = collect($docs);
        $this->count = count($docs);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->maches;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getDocs()
    {
        return $this->docs;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return 0;
    }
}