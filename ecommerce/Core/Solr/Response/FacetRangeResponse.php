<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 12:27
 */

namespace Ecommerce\Core\Solr\Response;

use SolrObject;

class FacetRangeResponse
{
    private $field = '';
    private $gap;
    private $start;
    private $end;
    private $after;
    private $counts;


    /**
     * QueryResponse constructor.
     * @param SolrObject $object
     * @param string $field
     */
    public function __construct($object, $field)
    {
        $this->field = $field;
        $this->gap = $object->facet_counts->facet_ranges->$field->gap;
        $this->start = $object->facet_counts->facet_ranges->$field->start;
        $this->end = $object->facet_counts->facet_ranges->$field->end;
        $this->after = $object->facet_counts->facet_ranges->$field->after;

        $docs = $object->facet_counts->facet_ranges->$field->counts;
        $_docs = [];
        for ($i = 0 ; $i < count($docs) ; $i++){
            $_docs[(string) $docs[$i] ] = $docs[$i+1];
            $i++;
        }

        $this->counts = collect($_docs);
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return float
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * @return float
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @return float
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return float|int
     */
    public function getAfter()
    {
        return $this->after;
    }


    /**
     * @return int
     */
    public function getCount()
    {
        return $this->counts;
    }


}