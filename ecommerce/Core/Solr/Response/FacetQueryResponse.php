<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 12:27
 */

namespace Ecommerce\Core\Solr\Response;

class FacetQueryResponse
{
    private $field = '';
    private $count;

    public function __construct( $object , $field )
    {
        $this->field = $field;
        $this->count = $object->facet_counts->facet_queries->$field;
    }

    /**
     * @return string
     */
    public function getField(){
        return $this->field;
    }

    /**
     * @return int
     */
    public function getCount(){
        return $this->count;
    }

}