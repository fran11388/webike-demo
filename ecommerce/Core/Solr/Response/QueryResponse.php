<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 12:27
 */

namespace Ecommerce\Core\Solr\Response;


use SolrObject;

class QueryResponse
{
    private $numFound = 0;
    private $docs;
    private $start = 0;

    /**
     * QueryResponse constructor.
     * @param SolrObject $object
     */
    public function __construct($object )
    {
        $this->numFound = $object->response->numFound;
        if ($object->response->docs) {
            $this->docs = collect($object->response->docs);
        } else {
            $this->docs = [];
        }
        $this->start = $object->response->start;
    }

    /**
     * @return int
     */
    public function getCount(){
        return $this->numFound;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getDocs(){
        return $this->docs;
    }

    /**
     * @return int
     */
    public function getStart(){
        return $this->start;
    }
}