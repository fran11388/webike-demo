<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 12:27
 */

namespace Ecommerce\Core\Solr\Response;


use SolrObject;

class SearchResponse
{
    private $queryResponse;
    private $facetFieldResponses = [];
    private $facetQueryResponses = [];
    private $facetRangeResponses = [];
    private $groupResponse;


    public function __construct($object)
    {
        if (property_exists($object, 'response')) {
            $this->queryResponse = new QueryResponse($object);
        }
        if (property_exists($object, 'grouped')) {
            $this->groupResponse = new GroupResponse($object);
        }

        if (property_exists($object, 'facet_counts')) {
            $facet_counts = $object->facet_counts;
            if (property_exists($facet_counts, 'facet_queries')) {
                foreach ($facet_counts->facet_queries as $key => $query) {
                    $this->facetQueryResponses[$key] = new FacetQueryResponse($object, $key);
                }
            }

            if (property_exists($facet_counts, 'facet_fields')) {
                foreach ($facet_counts->facet_fields as $key => $query) {
                    $this->facetFieldResponses[$key] = new FacetFieldResponse($object, $key);
                }
            }

            if (property_exists($facet_counts, 'facet_ranges')) {
                foreach ($facet_counts->facet_ranges as $key => $query) {
                    $this->facetRangeResponses[$key] = new FacetRangeResponse($object, $key);
                }
            }
        }

    }

    /**
     * @return QueryResponse|null
     */
    public function getQueryResponse()
    {
        return $this->queryResponse;
    }

    /**
     * @return FacetFieldResponse[]
     */
    public function getFacetFieldResponses()
    {
        return $this->facetFieldResponses;
    }

    /**
     * @return FacetQueryResponse[]
     */
    public function getFacetQueryResponses()
    {
        return $this->facetQueryResponses;
    }

    /**
     * @return FacetRangeResponse[]
     */
    public function getFacetRangeResponses()
    {
        return $this->facetRangeResponses;
    }


    /**
     * @return GroupResponse
     */
    public function getGroupResponse()
    {
        return $this->groupResponse;
    }
}