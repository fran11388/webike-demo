<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 08:20
 */

namespace Ecommerce\Core\Solr\Param;


use SolrQuery;

class CommonParam
{
    private $query = '*:*';
    private $start = 0;
    private $rows = 10;
    private $sort_field = '';
    private $sort_fields = [];
    private $sort_direction = SolrQuery::ORDER_DESC;


    public function parseKeyword( $keyword  , $for = null , $value_only = false)
    {
        $keyword = trim($keyword);
        if (!$keyword)
            return '*:*';

        $keywords = explode(' ', $keyword);
        $keyword = implode(' ', array_filter($keywords));

        $luceneReservedCharacters = preg_quote('+-&|!(){}[]^"~*?:\\') . '\/';
        $keyword = preg_replace_callback(
            '/([' . $luceneReservedCharacters . '])/',
            function($matches) {
                return '\\' . $matches[0];
            },
            $keyword);

        $keyword = str_replace(' ' , ' AND ' , $keyword);
        if(strpos($keyword, '(') !== 1 and strpos($keyword, ')') + 1 !== strlen($keyword)){
            $keyword = '(' . $keyword . ')';
        }
        return $keyword;


    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int $start
     */
    public function setStart($start)
    {
        if($start >= 0) {
            $this->start = $start;
        }
    }

    /**
     * @return int
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @param int $rows
     */
    public function setRows($rows)
    {
        $this->rows = $rows;
    }

    /**
     * @return string
     */
    public function getSortField()
    {
        return $this->sort_field;
    }

    /**
     * @return string
     */
    public function getSortFields()
    {
        return $this->sort_fields;
    }

    /**
     * @param string $sort_field
     */
    public function setSortField($sort_field)
    {
        $this->sort_field = $sort_field;
    }

    /**
     * @return string
     */
    public function getSortDirection()
    {
        return $this->sort_direction;
    }

    /**
     * @param string $sort_direction
     */
    public function setDirection($sort_direction)
    {
        $this->sort_direction = $sort_direction;
    }


    /**
     * @param $sort_field
     * @param $sort_direction
     */
    public function setSortFields($sort_field, $sort_direction = SolrQuery::ORDER_DESC)
    {
        $this->sort_fields[$sort_field] = $sort_direction;
    }

    public function resetSortFields(){
        $this->sort_fields = [];
    }
}