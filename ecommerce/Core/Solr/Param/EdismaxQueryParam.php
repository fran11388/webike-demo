<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 08:20
 */

namespace Ecommerce\Core\Solr\Param;


class EdismaxQueryParam
{
    var $bf;

    public function __construct()
    {
        $this->bf = 'in_stock_flg^10 two_month_popularity^1';
    }

    public function getBf(){
        return $this->bf;
    }
}