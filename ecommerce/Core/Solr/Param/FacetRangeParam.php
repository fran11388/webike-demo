<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 08:20
 */

namespace Ecommerce\Core\Solr\Param;


class FacetRangeParam
{
    private $field = '';
    private $key = '';
    private $start = 0;
    private $end=20000;
    private $gap = 500;
    private $other = 'after';
    private $exclude_tags = [];

    /**
     * FacetRange constructor.
     * @param string $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }


    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }
    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int $start
     */
    public function setStart($start)
    {
        if($start >= 0){
            $this->start = $start;
        }
    }

    /**
     * @return int
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param int $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function getGap()
    {
        return $this->gap;
    }

    /**
     * @param int $gap
     */
    public function setGap($gap)
    {
        $this->gap = $gap;
    }

    /**
     * @return string
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param string $other
     */
    public function setOther($other)
    {
        $this->other = $other;
    }

    /**
     * @return array
     */
    public function getExcludeTags()
    {
        return $this->exclude_tags;
    }

    /**
     * @param array $exclude_tags
     */
    public function setExcludeTags($exclude_tags)
    {
        $this->exclude_tags = $exclude_tags;
    }

}