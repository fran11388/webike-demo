<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 08:20
 */

namespace Ecommerce\Core\Solr\Param;


class FilterQueryParam
{
    private $field = '';
    private $query = '';
    private $tag = '';

    public function __construct( $field = null , $query = null, $tag = null)
    {
        if ($field){
            $this->setField($field);
        }
        if ($query){
            $this->setQuery($query);
        }
        if ($tag){
            $this->setTag($tag);
        }
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
        if (!$this->tag){
            $this->tag = $field;
        }
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

}