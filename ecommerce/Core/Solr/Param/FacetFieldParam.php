<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 08:20
 */

namespace Ecommerce\Core\Solr\Param;


class FacetFieldParam
{
    private $field = '';
    private $prefix = '';
    private $sort; // The default is count if facet.limit is greater than 0, otherwise, the default is index.
    private $limit = -1;
    private $offset = 0;
    private $mincount = 1;
    private $exclude_tags = [];
    private $key = '';

    /**
     * FacetField constructor.
     * @param string $field
     */
    public function __construct($field = null)
    {
        if ($field){
            $this->setField($field);
        }

    }


    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
        if (!$this->key){
            $this->key = $field;
        }
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getMinCount()
    {
        return $this->mincount;
    }

    /**
     * @param int $mincount
     */
    public function setMinCount($mincount)
    {
        $this->mincount = $mincount;
    }

    /**
     * @return array
     */
    public function getExcludeTags()
    {
        return $this->exclude_tags;
    }

    /**
     * @param array $exclude_tags
     */
    public function setExcludeTags($exclude_tags)
    {
        $this->exclude_tags = $exclude_tags;
    }

    /**
     * @param string $exclude_tag
     */
    public function addExcludeTag($exclude_tag)
    {
        $this->exclude_tags[] = $exclude_tag;
    }

    public function unsetExcludeTags()
    {
        $this->exclude_tags = [];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }


}