<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 08:20
 */

namespace Ecommerce\Core\Solr\Param;


class FacetQueryParam
{
    private $field = '';
    private $exclude_tags = [];
    private $key = '';
    private $query = '';

    /**
     * FacetField constructor.
     * @param string $field
     */
    public function __construct($field = null)
    {
        if ($field){
            $this->setField($field);
        }

    }


    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
        if (!$this->key){
            $this->key = $field;
        }
    }

    /**
     * @return array
     */
    public function getExcludeTags()
    {
        return $this->exclude_tags;
    }

    /**
     * @param array $exclude_tags
     */
    public function setExcludeTags($exclude_tags)
    {
        $this->exclude_tags = $exclude_tags;
    }

    /**
     * @param string $exclude_tag
     */
    public function addExcludeTag($exclude_tag)
    {
        $this->exclude_tags[] = $exclude_tag;
    }

    public function unsetExcludeTags()
    {
        $this->exclude_tags = [];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }


}