<?php
namespace Ecommerce\Core\Feed\Motomarket;

use Ecommerce\Service\FeedService;
use Carbon\Carbon;
use \Everglory\Models\MotoMarket\Product;
use \Everglory\Models\MotoMarket\Follow;


class Popular extends BaseFeed
{
    public function __construct()
    {

    }

    public function import()
    {
        $service = new FeedService;

        $dt = Carbon::now();
        $now = $dt->toDateTimeString();
        $dt->addDay();
        $dt->addMinutes(2);
        $deadline = $dt->toDateTimeString();
        
        $motor_type = ['0'=>'人氣國產車','1'=>'人氣進口車'];
        foreach($motor_type as $code => $type_name){
            $service->feed($this->website,$type_name);
            $motors = \Everglory\Models\MotoMarket\Product::with('motorModel')
                ->where('status' ,'=','1')
                ->where('is_import' , '=' , $code)
                ->whereBetween('products.created_at',array(date("Y-m-d",strtotime("-21 days")),date("Y-m-d")))
                ->whereNotIn('customer_id',array(2,44))
                ->orderBy('views','desc')
                ->take(15)->get();
            foreach($motors as $key => $motor) {
                $attributes = [];
                $attributes['motor_id'] = [$motor->id];
                $photo_group = unserialize($motor->motor_photos);
                $photo_url = '//img.webike.net/catalogue/noimage.gif';
                if( isset($photo_group[0]) ){
                    $obj = $photo_group[0];
                    $photo_url = '//img.webike.tw/moto_photo/'.$obj->filename;
                }
                $data = [
                    'module' => 'ad',
                    'data_id' => $motor->id,
                    'title' =>  $motor->product_description,
                    'description' => strip_tags($motor->other_description),
                    'image_url' => $photo_url,
                    'display_date' => $now,
                    'deadline' => $deadline,
                    'url' =>  'http://www.webike.tw/motomarket' .'/detail/' . $motor->serial,
                    'attributes' => $attributes,
                ];
                $service->add($data);
            }

        }
        
        $service->feed($this->website,'人氣車商');
        $follows = Follow::join('customers', 'customers.id', '=', 'target_id')
            ->where('target_type' ,'customer')
            ->where('target_id','<>',2)
            ->with('target.profiles')
            ->groupBy('target_id')
            ->orderByRaw('count(target_id) desc, target_id asc')
            ->having('customers.group_id', '=', 3)->take(5)->get();

        foreach ($follows as $follow){
            $dealer = $follow->target;
            $profiles = $this->getProfileDetail($dealer->profiles);
            $description = '地址：' . $profiles->zipcode . $profiles->county . $profiles->district . $profiles->county . $profiles->street . '<br/>';
            $description .= '電話：' . $profiles->phone .'<br/>';
            $description .= isset($profiles->description_1) ? $profiles->description_1 : '';
            $photo_url = 'https://img.webike.net/garage_img/no_photo.jpg';
            if(isset($profiles->main_photo)){
                $main_photo = unserialize($profiles->main_photo);
                if($main_photo){
                    $photo_url = '//img.webike.tw' . '/moto_profile/' . $main_photo->filename;
                }
            }

            $attributes = [];
            $attributes['zipcode'] = [$profiles->zipcode];

//            $attributes['county'] = [$profiles->county];
//            $attributes['district'] = [$profiles->district];
//            $attributes['street'] = [$profiles->street];

            $data = [
                'module' => 'ad',
                'data_id' => $dealer->id,
                'title' =>  $profiles->username,
                'description' => $description,
                'image_url' => $photo_url,
                'display_date' => $now,
                'deadline' => $deadline,
                'url' => 'http://www.webike.tw/motomarket' . '/shop/' . $dealer->id,
                'attributes' => $attributes,
            ];

            $service->add($data);
        }

        
    }

    public function update()
    {

    }


    private function getProfileDetail($profiles)
    {

        $details = $profiles->keyBy('attribute');
        $data = new \stdClass();
        foreach ($details as $key => $detail) {
            $data->$key = $detail->value;
        }
        return $data;
    }
}