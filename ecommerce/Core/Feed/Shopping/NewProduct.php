<?php
namespace Ecommerce\Core\Feed\Shopping;

use Ecommerce\Service\FeedService;
use Carbon\Carbon;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;

class NewProduct extends BaseFeed
{
    public function __construct()
    {

    }

    public function import()
    {
        // one day has two data for a category
        //Product :  2
        $products = ProductRepository::selectNewestProducts()->shuffle();
        $service = new FeedService;
        $service->feed($this->website,'新商品');
        $dt = Carbon::now();
        $now = $dt->toDateTimeString();
        $dt->addMonth();
        $deadline = $dt->toDateTimeString();
        foreach($products as $key => $product){
            $product = new ProductAccessor($product);
            $description = '品牌：' . $product->getManufacturerName() .'<br/>';
            $fitModels = $product->getFitModels(false);
            if(count($fitModels)){
                $description .= '對應車種：';
                foreach($fitModels as $key => $model ){
                    $description .= $model->model . ($model->style ? ' '.$model->style : '' );
                    if($key == 2){
                        break;
                    }else{
                        $description .= ' \ ';
                    }
                }
                $description .='<br/>';
            }
            $productDescription = $product->getProductDescription();
            if($productDescription){
                if($productDescription->summary){
                    $description .= '要點：'.strip_tags($productDescription->summary) . '<br/>';
                }else{
                    $description .= '敘述：'.str_limit(strip_tags($productDescription->description),300,'...') . '<br/>';
                }
            }

            $final_price = $product->getNormalPrice();
            $discount = round(($final_price / $product->price) * 100,1) ;
            $description .= '售價：NT$' . number_format($final_price) . '(' . $discount  .'%)';
            

            $image = $product->images->first();
            // add attribute
            $attributes = [];
            $attributes['product_id'] = [$product->id];
            if(count($product->motors)){
                $attributes['motor_id'] = $product->motors->pluck('id')->toArray();
            }
            $attributes['category_id'] = $product->categories->pluck('id')->toArray();
            $attributes['manufacturer_id'] = [$product->manufacturer_id];

            // data
            $data = [
                'module' => 'ad',
                'data_id' => $product->id,
                'title' => $product->name,
                'description' => $description,
                'image_url' => $image->image,
                'display_date' => $now,
                'deadline' => $deadline,
//                'url' => route('product-detail',$product->url_rewrite),
                'url' => '/sd/' . $product->url_rewrite,
                'attributes' => $attributes,
            ];

            $service->add($data);
        }  
        
    }

    public function update()
    {

    }
    
}