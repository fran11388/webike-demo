<?php
namespace Ecommerce\Core\Feed\Shopping;

use Ecommerce\Service\FeedService;
use Carbon\Carbon;
use Everglory\Models\Feed;

class BaseFeed
{
    protected $website = \Everglory\Constants\Website::SHOPPING;
}