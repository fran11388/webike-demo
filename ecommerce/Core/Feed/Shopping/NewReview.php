<?php
namespace Ecommerce\Core\Feed\Shopping;

use Ecommerce\Service\FeedService;
use Carbon\Carbon;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;
use Everglory\Models\Review;

class NewReview extends BaseFeed
{
    public function __construct()
    {

    }

    public function import()
    {
       $reviews =Review::where('view_status',1)->orderBy('created_at','desc')->take(100)->get();
       foreach($reviews as $review){
            $this->importByEntity($review);
       }
    }
    
    public function importByEntity($review)
    {
        $service = new FeedService;
        $service->feed($this->website,'最新評論');
        $dt = Carbon::now();
        $now = $dt->toDateTimeString();

        $attributes = [];
        $attributes['product_id'] = [$review->product_id];
        if($review->motor_id){
            $attributes['motor_id'] = [$review->motor_id];
        }
        $attributes['category_id'] = [$review->category];
        $attributes['manufacturer_id'] = [$review->product_manufacturer_id];
        
        $image = $review->photo_key ? '//img.webike.tw/review/'.$review->photo_key : $review->product_thumbnail ;
        
        // data
        $data = [
            'module' => 'normal',
            'data_id' => $review->id,
            'title' => $review->title,
            'description' => $review->content,
            'image_url' => $image,
            'display_date' => $review->created_at,
            'deadline' => null,
            'url' => '/review/article/' . $review->id,
            'attributes' => $attributes,
            'deleted_at' => ($review->view_status == 1 ? null : $now),
        ];

        $service->add($data);
    }

    public function update()
    {

    }
    
}