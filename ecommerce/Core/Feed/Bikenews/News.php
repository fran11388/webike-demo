<?php
namespace Ecommerce\Core\Feed\Bikenews;

use Ecommerce\Service\FeedService;
use Carbon\Carbon;
use Everglory\Models\Feed;
use \Everglory\Models\News\Post;

class News extends BaseFeed
{
    public function __construct()
    {

    }

    public function import()
    {
        $posts = \Everglory\Models\News\Post::leftJoin('wp_postmeta AS mt', function ($join) {
            $join->on('wp_posts.ID','=','mt.post_id')
                ->where('mt.meta_key', '=', '_thumbnail_id');
        })
            ->leftJoin('wp_posts AS mtpo','mt.meta_value','=','mtpo.ID')
            ->leftJoin('wp_postmeta AS mtsd', function ($join) {
                $join->on('wp_posts.ID','=','mtsd.post_id')
                    ->where('mtsd.meta_key', '=', 'short_description');
            })
            ->where('wp_posts.post_status','publish')
            ->where('wp_posts.post_date','>','2016-09-01')
            ->select('wp_posts.ID','wp_posts.post_title','wp_posts.post_date','wp_posts.post_content','wp_posts.post_name','mtpo.guid' ,'mtsd.meta_value AS short_description' )->get();
        $service = new FeedService;
        $service->feed('News','News');
        foreach($posts as $post) {
            $photo_path = 'http://www.webike.tw/bikenews/wp-content/themes/opinion_tcd018/img/common/no_image2.jpg';
            if( $post->guid ){
                $photo_path = $post->guid;
            }
            $description = $post->short_description;
            if(!$description){
                $description = strip_tags($post->post_content);
            }

            $attributes = [];
            $timestamp = strtotime($post->post_date);
            $y = date('Y',$timestamp);
            $m = date('m',$timestamp);
            $d = date('d',$timestamp);
            $data = [
                'module' => 'normal',
                'data_id' => $post->ID,
                'title' => $post->post_title,
                'description' => $description,
                'image_url' => $photo_path,
                'display_date' => $post->post_date,
                'url' => 'http://www.webike.tw/bikenews/' . $y . '/' . $m . '/' . $d . '/' . $post->post_name,
                'attributes' => $attributes,
            ];
            $service->add($data);
        }
    }

    public function update()
    {
        //check schedule news
        $dt = Carbon::now();
        $dt->addMinute();
        $feeds = Feed::where('data_category','摩托新聞')
            ->where('display_date','<',$dt->toDateTimeString())
            ->onlyTrashed()
            ->get();
        foreach ($feeds as $feed){
            $post = Post::where('ID',$feed->data_id)->first();
            if(!$post or $post->post_status == 'trash'){
                $feed->forceDelete();
            }elseif($post->post_status == 'publish'){
                $feed->restore();
            }
        }
    }
}