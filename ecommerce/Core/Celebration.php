<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2017/12/13
 * Time: 上午 01:00
 */

namespace Ecommerce\Core;


use Ecommerce\Service\CartService;
use Everglory\Constants\CustomerRole;
use Everglory\Models\Customer;

class Celebration
{
    /* @var $customer Customer */
    var $customer;

    var $description_in_cart;
    var $description_in_checkout;
    var $title_in_checkout;

    var $discount = 0;
    var $range = 0;
    var $level_up_price = 0;
    var $next_level_discount_price = 0;
    var $next_level_price = 0;

    public function getDiscount(){
        return $this->discount;
    }

    public function getLevelUpPrice(){
        return $this->level_up_price;
    }

    public function getNextLevelDiscountPrice(){
        return $this->next_level_discount_price;
    }

    public function getNextLevelPrice(){
        return $this->next_level_price;
    }

    public function getDescriptionInCart(){
        return sprintf($this->description_in_cart , $this->range , $this->discount );
    }
    public function getDescriptionInCheckout(){
        return sprintf($this->description_in_checkout, $this->discount );
    }
    public function getTitleInCheckout(){
        if (!$this->canUsePromotion()){
            return null;
        }else{
            return sprintf($this->title_in_checkout  , $this->range , $this->discount );
        }
    }


    public function __construct()
    {
        $this->customer = \Auth::user();
        $this->description_in_cart = '耶誕滿額%s折扣%s元，將於結帳頁面顯示。';
        $this->title_in_checkout = '耶誕活動-結帳金額滿%s現折%s元';
        $this->description_in_checkout = '滿額折抵: %s元';

        $this->setDiscount();
        if($this->range == '10000'){
            $this->description_in_cart = '耶誕滿%s折%s元，將於結帳頁面顯示。';
        }
    }

    function canUsePromotion(){
        if ( in_array($this->customer->role->id,[CustomerRole::WHOLESALE,CustomerRole::STAFF,CustomerRole::BUYER]) || !activeValidate('2017-12-21 00:00:00','2017-12-26'))
            return false;
        return true;
    }

    function setDiscount()
    {
        $this->discount = 0;

        if (!$this->canUsePromotion())
            return;

        $service = app()->make(CartService::class);
        $amount = $service->getFee() + $service->getSubtotal();

        if($amount < 1000){
            $this->level_up_price = 1000 - $amount;
            $this->next_level_discount_price = 50;
            $this->next_level_price = 1000;
        }

        if ($amount <= 2999 && $amount >= 1000) {
            $this->range = 1000;
            $this->discount = 50;
            $this->level_up_price = 3000 - $amount;
            $this->next_level_discount_price = 200;
            $this->next_level_price = 3000;
        }


        if ($amount <= 4999 && $amount >= 3000) {
            $this->range = 3000;
            $this->discount = 200;
            $this->level_up_price = 5000 - $amount;
            $this->next_level_discount_price = 400;
            $this->next_level_price = 5000;
        }

        if ($amount <= 7999 && $amount >= 5000)
        {
            $this->range = 5000;
            $this->discount = 400;
            $this->level_up_price = 8000 - $amount;
            $this->next_level_discount_price = 700;
            $this->next_level_price = 8000;
    }

        if ($amount <= 9999 && $amount >= 8000 )
        {
            $this->range = 8000;
            $this->discount = 700;
            $this->level_up_price = 10000 - $amount;
            $this->next_level_discount_price = 1000;
            $this->next_level_price = 10000;
        }


        if ($amount >= 10000 ){
            $this->range = 10000;
            $this->discount = 1000;
        }


    }

}