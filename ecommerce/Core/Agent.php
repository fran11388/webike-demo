<?php

namespace Ecommerce\Core;

use \Jenssegers\Agent\Agent as AgentPlugin;

/**
 * Agent filter.
 * Class Agent
 * @package Ecommerce\Core
 */
class Agent
{
    /**
     * Filter by device.
     * @param array $conditions EX: ['products' => [20,5]]; 20 is pc limit , 5 is mobile limit.
     * @return object
     */
    public static function limitByDevice($conditions)
    {
        $agent = new AgentPlugin();
        $key = 0;
        if($agent->isMobile()){
            $key = 1;
        }
        foreach ($conditions as &$condition){
            $condition = $condition[$key];
        }
        return (object) $conditions;
    }

    public static function isNotPhone()
    {
        $agent = new AgentPlugin();
        if(!$agent->isPhone()){
            return true;
        }else{
            return false;
        }
    }
}