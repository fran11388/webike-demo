<?php namespace Ecommerce\Core;

class Image
{
    //image server
    protected $host;
    protected $uploader;
    protected $path;
    protected $folder;
    // local image
    protected $local_file_path; //image local path
    protected $filename;    //image local filename
    protected $MimeType;    //image mime type



    public function __construct()
    {
        $this->host = 'img.webike.tw';
        $this->MimeType = 'image/jpeg';
        $this->uploader = 'http://' . $this->host . '/wiembaigkee/uploader';
    }

    private function upload()
    {
        // $destinationPath = public_path() . '\assets\photos\review\\';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->uploader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        //  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data['upload'] = 'Upload';
        if($this->folder){
            $data['folder'] = $this->folder;
        }

        $data['my_file'] = new \CurlFile($this->local_file_path , $this->MimeType, $this->filename);
        // $data['path'] = 'review';
        $data['path'] = $this->path;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function review($local_file_path,$mime = null,$remote_filename)
    {
        $this->path = 'review';
        $this->local_file_path = $local_file_path;
        $this->MimeType = $mime;
        $this->filename = $remote_filename;
        return $this->upload();
    }
    public function imageUpload($local_file_path,$mime = null,$remote_filename)
    {
        
        $this->path = 'imageUpload';
        $this->local_file_path = $local_file_path;
        $this->MimeType = $mime;
        $this->filename = $remote_filename;
        return $this->upload();
    }

    public function collection($local_file_path,$mime = null,$remote_filename)
    {
        $this->path = 'collection';
        $this->local_file_path = $local_file_path;
        $this->MimeType = $mime;
        $this->filename = $remote_filename;
        return $this->upload();
    }

    public function rex($local_file_path,$mime = null,$remote_filename)
    {
        $this->path = 'return';
        $this->local_file_path = $local_file_path;
        $this->MimeType = $mime;
        $this->filename = $remote_filename;
        return $this->upload();
    }


    public function asset($local_file_path, $mime, $remote_filename, $folder)
    {
        $this->path = 'assets';
        $this->local_file_path = $local_file_path;
        $this->MimeType = $mime;
        $this->filename = $remote_filename;
        $this->folder = $folder;
        return $this->upload();
    }


    public function url($url)
    {
        if(strpos($url, 'http') !== false){
            if(strpos($url, 'img.webike.tw') !== false){
                $url = str_replace('img.webike.tw',$host,$url);
            }
        }else{

        }

        return $url;
    }
}