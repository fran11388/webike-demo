<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2016/12/05
 * Time: 下午 02:32
 */

namespace Ecommerce\Core\WarehouseFactory;

use Everglory\Models\Product;
use Everglory\Models\Supplier\Product as SupplierProduct;
use Log;
use StdClass;
use Everglory\Models\WarehouseQueue;

class Program
{
    Const SYNC_API = 'http://153.120.25.92/productStation/public/index.php/warehouse/output';

    public function __construct()
    {

    }

    public function sync($parameters)
    {
        if(\Config::get('app.production') === true) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::SYNC_API);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // $para = file_get_contents("php://input");
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
            $result = curl_exec($ch);
            $result = json_decode($result);
            curl_close($ch);
        }else{
            $result = new \stdClass;
            $result->success = true;
        }
        return $result;
    }

    public function addWarehouseQueue($data)
    {
        $result = null;
        $warehouseQueue = new WarehouseQueue;
        $columns = $warehouseQueue->getTableColumns();
        $id_key = array_search('id', $columns);
        unset($columns[$id_key]);
        if($data instanceof \Eloquent){
            $attributes = $data->getAttributes();
            foreach ($attributes as $attribute => $value){
                if(!in_array($attribute, $columns)){
                    unset($data->$attribute);
                }
            }
            $result = $data->save();
        }else if(is_array($data)){
            $attributes = $data;
            foreach ($attributes as $attribute => $value){
                if(!in_array($attribute, $columns)){
                    $warehouseQueue->$attribute = $value;
                }
            }
            $warehouseQueue->save();
            $result = $warehouseQueue;
        }else if(is_object($data)){
            $attributes = $data;
            foreach ($attributes as $attribute => $value){
                if(in_array($attribute, $columns)){
                    $warehouseQueue->$attribute = $value;
                }
            };
            $warehouseQueue->save();
            $result = $warehouseQueue;
        }
        return $result;
    }

    public function reloadWarehouseQueue($warehouseQueue_ids)
    {
        $warehouseQueues = null;
        if(is_array($warehouseQueue_ids)){
            $warehouseQueues = WarehouseQueue::onWriteConnection()->whereIn('id', $warehouseQueue_ids)->get();
        }else if(is_numeric($warehouseQueue_ids)){
            $warehouseQueues = WarehouseQueue::onWriteConnection()->where('id', $warehouseQueue_ids)->first();
        }
        return $warehouseQueues;
    }
}