<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/01/25
 * Time: 上午 09:51
 */

namespace Ecommerce\Core\Disp;

use Auth;
use Ecommerce\Service\BaseService;
use Everglory\Models\Disp as Disp;
use Ecommerce\Core\Disp\Config;
use Illuminate\Support\Collection;

class Program extends BaseService
{
    protected $sort;
    protected $compare;

    public function __construct()
    {
        $this->sort = Config::SORT;
        $this->compare = Config::COMPARE;
    }

    public function getGroupCount($collection)
    {
        $result = [];
        $result['sum'] = $collection->count();
        $group = $collection->groupby('group');
        foreach ($group as $group_name => $items) {
            $result[$group_name] = count($items);
        }
        return $result;
    }

    public function getCustomerDisps($customer_id = null)
    {
        if(!$customer_id){
            if(Auth::check()){
                $customer_id = Auth::user()->id;
            }else{
                return null;
            }
        }

        $disps = Disp::where('customer_id', $customer_id)->where(function($query){
            $query->where(function($query){
                $query->whereNull('expire_at')
                    ->orWhere('expire_at', '>=', date('Y-m-d H:i:s', strtotime('now')));
            })->where(function($query){
                $query->whereNull('started_at')
                    ->orWhere('started_at', '<=', date('Y-m-d H:i:s', strtotime('now')));
            });
        })->get();
        return $this->load($disps);
    }

    public function deleteCustomerDisps($customer_id = null)
    {
        if(!$customer_id){
            if(Auth::check()){
                $customer_id = Auth::user()->id;
            }else{
                return null;
            }
        }

        $queue = explode('|', request()->input('queue'));
        foreach ($queue as $item){
            $arr = explode('@', $item);
            if(count($arr) == 2){
                Disp::where('customer_id', $customer_id)->where('object_type', $this->compare[$arr[0]])->where('increment_id', $arr[1])->delete();
            }
        }
        return true;
    }

    public function load($sources)
    {
        $result = [];
        if(is_array($sources) or ($sources instanceof Collection)){
            foreach ($sources as $source){
                $class_name = __NAMESPACE__ .'\\Models\\'. $source->object_type;
                $substitute = $this->getValidateModel($class_name, $source);
                $substitute->sort = $this->getSort($substitute);
                $result[] = $substitute;
            }
        }else{
            $source = $sources;
            $class_name = __NAMESPACE__ .'\\Models\\'. $source->object_type;
            $substitute = $this->getValidateModel($class_name, $source);
            $substitute->sort = $this->getSort($substitute);
            $result[] = $substitute;
        }
        return $this->reorganize(new Collection($result));
    }

    public function getValidateModel($class_name, $disp)
    {
        if (class_exists($class_name)) {
            return new $class_name($disp);
        }
        return null;
    }

    public function reorganize($collection)
    {
        return $collection->sortBy('sort');
    }

    public function getSort($substitute)
    {
        return array_search($substitute->group, $this->sort);
    }

    public function addDisp($entity){
        $disp = new Disp;
        $disp->customer_id = $entity->customer_id;
        $disp->object_id = $entity->id;
        $disp->object_type = 'MorphMitumori';
        $disp->increment_id = $entity->estimate_code;
        $disp->info = '未登錄報價：'.$entity->estimate_code;
        $disp->sort = 5;
        $disp->save();
    }
}