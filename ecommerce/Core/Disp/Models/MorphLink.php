<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/02/02
 * Time: 上午 14:53
 */

namespace Ecommerce\Core\Disp\Models;

class MorphLink extends DispBasic
{

    public function __construct($model)
    {
        parent::__construct($model);
        $this->group = 'link';
        $this->link = $model->increment_id;
    }
}