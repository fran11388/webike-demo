<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/01/25
 * Time: 上午 09:51
 */

namespace Ecommerce\Core\Disp\Models;

use URL;
use Everglory\Constants\EstimateType;

class MorphEstimate extends DispBasic
{

    public function __construct($model)
    {
        parent::__construct($model);
        $this->object = $model->object;
        if($this->object->type_id == EstimateType::GENERAL){
            $this->group = 'estimate-general';
            $this->link = URL::route('customer-history-estimate') . '#' . $model->increment_id;
        }else{
            $this->group = 'estimate-genuine';
            $this->link = URL::route('customer-history-genuineparts-detail', $model->increment_id);
        }
    }
}