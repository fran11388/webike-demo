<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/01/25
 * Time: 上午 09:51
 */

namespace Ecommerce\Core\Disp\Models;

use URL;

class MorphGroupbuy extends DispBasic
{

    public function __construct($model)
    {
        parent::__construct($model);
//        $this->object = $model->object;
        $this->group = 'groupbuy';
        $this->link = URL::route('customer-history-groupbuy-detail', $model->increment_id);
    }
}