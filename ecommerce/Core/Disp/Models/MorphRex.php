<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/01/25
 * Time: 上午 09:51
 */

namespace Ecommerce\Core\Disp\Models;

use URL;

class MorphRex extends DispBasic
{

    public function __construct($model)
    {
        parent::__construct($model);
//        $this->object = $model->object;
        $this->group = 'rex';
        $this->link = URL::route('customer-history-return-detail', $model->increment_id);
    }
}