<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/02/02
 * Time: 上午 14:53
 */

namespace Ecommerce\Core\Disp\Models;

use URL;

class MorphTicket extends DispBasic
{

    public function __construct($model)
    {
        parent::__construct($model);
        $this->group = 'ticket';
        $this->link = URL::route('customer-service-history-dialog', $model->increment_id);
    }
}