<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/01/25
 * Time: 上午 09:51
 */

namespace Ecommerce\Core\Disp\Models;

class DispBasic
{
    protected $model;
    protected $object;
    public $object_type;
    public $code;
    public $sort;
    public $link;
    public $name;

    public function __construct($model)
    {
        $this->model = $model;
        $this->object_type = $model->object_type;
        $this->name = $model->info;
        $this->code = $model->increment_id;
    }

    public function __get($name)
    {
        return (isset($this->$name) ? $this->$name : null);
    }


}