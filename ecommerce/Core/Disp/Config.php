<?php
namespace Ecommerce\Core\Disp;

class Config
{
    const SORT = [
        'order',
        'rex',
        'ticket',
        'estimate-genuine',
        'estimate-general',
        'groupbuy',
        'mitumori',
        'link',
    ];

    const COMPARE = [
        'order' => 'MorphOrder',
        'rex' => 'MorphRex',
        'ticket' => 'MorphTicket',
        'estimate-genuine' => 'MorphEstimate',
        'estimate-general' => 'MorphEstimate',
        'groupbuy' => 'MorphGroupbuy',
        'mitumori' => 'MorphMitumori',
        'link' => 'MorphLink',
    ];

    public function __construct()
    {

    }

}