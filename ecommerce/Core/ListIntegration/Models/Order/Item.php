<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\Order;

use URL;
use Everglory\Constants\ClientOrderStatus;
use Ecommerce\Service\OrderService;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Order\Item as EgOrderItem;

class Item extends ItemBasic
{
    public $quantity;
    public $price;
    public $model_number;
    public $sku;

    public function __construct(EgOrderItem $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $order = $source->order;
        $this->url_rewrite = $source->sku;
        $this->created_at = date( "Y/m/d", strtotime($source->created_at));
        $this->link = URL::route('product-detail', ($source->gift and $source->gift->origProduct) ? $source->gift->origProduct->sku : $this->url_rewrite );
        $orderService = new OrderService();
        $detail = $orderService->getItemDetail($source);
        $this->product_name = $detail->product_name;
        $this->code = $order->increment_id;
        $this->manufacturer_name = $detail->manufacturer_name;
        $this->total = $this->currency . number_format($this->price * $this->quantity);
        $this->original_price =  $this->currency . number_format($detail->original_price);
        $this->normal_price = $this->currency . number_format($detail->normal_price);
        $this->price =  $this->currency . number_format($this->price);
        $this->options = str_replace('$', ' - ', str_replace('|', '<br>', $detail->product_options));
        $this->thumbnail = ($detail->product_thumbnail ? $detail->product_thumbnail : NO_IMAGE);

        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
//            $this->read_tip = '(未讀)';
        }
    }

}