<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\Order;

use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Order\Reward as EgReward;

class Reward extends ItemBasic
{
    public $comment;

    public function __construct(EgReward $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $this->order = $source->order;
        $this->link_html = '<span class="font-16px">系統</span>';
        $this->description = $source->description;
        if($this->order){
            $this->link_html ='<a href="'.route('customer-history-order-detail',$this->order->increment_id).'"><span class="font-16px blue-text">'.$this->order->increment_id.'</span></a>';
            $this->description = $this->order->status->name;
            if($source->description){
                $this->description .= '(' . $source->description . ')';
            }
        }
        if($source->created_at->timestamp < 0){
            $this->date = '-';
        }else{
            $this->date = $source->created_at->toDateString();
        }

        $this->points_current = '-';
        if($source->points_current){
            $this->points_current = $source->points_current;
        }

        $this->points_spend_html = '-';
        if($source->points_spend){
            $this->points_spend_html = '<span class="font-16px font-color-red">-'.$source->points_spend.'</span>';
        }

        $this->end_date = '-';
        if($source->end_date){
            $this->end_date = $source->end_date;
        }
    }
}