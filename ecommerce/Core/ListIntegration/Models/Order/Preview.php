<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\Order;

use URL;
use Everglory\Constants\ClientOrderStatus;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Order as EgOrder;
use Ecommerce\Service\OrderService;
use StdClass;

class Preview extends ItemBasic
{
    public $comment;

    public function __construct(EgOrder $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $this->shipping_amount = $this->currency . number_format($source->shipping_amount);
        $this->payment_amount = $this->currency . number_format($source->payment_amount);
        $this->coupon_amount = $this->currency . number_format($source->coupon_amount);
        $this->rewardpoints_amount = $this->currency . number_format($source->rewardpoints_amount);
        $this->grand_total = $this->currency . number_format($source->grand_total);
        $this->code = $source->increment_id;
        $this->url_rewrite = $source->increment_id;
        $this->payment = $source->payment->name;
        $this->created_at = date( "Y/m/d", strtotime($source->created_at));
        $this->total = $this->currency . number_format($source->subtotal);
        $this->state = $this->status;
        $items = [];
        $orderService = new OrderService();
        foreach($source->items as $item){
            $detail = $orderService->getItemDetail($item);
            $obj = new StdClass;
            $obj->sku = $item->url_rewrite;
            $obj->url_rewrite = $item->sku;
            $obj->price = $this->currency . number_format($item->price);
            $obj->model_number = $item->model_number;
            $obj->quantity = $item->quantity;
            $obj->name = '【' . $detail->manufacturer_name . '】' . $detail->product_name;
            $obj->options = str_replace('$', ' - ', str_replace('|', ' / ', $detail->product_options));
            $items[] = $obj;
        }
        $this->items = $items;

    }
}