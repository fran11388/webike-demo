<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models;

use URL;
use Config;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Constants\TicketInteraction;

class Ticket extends ItemBasic
{
    private $interaction_new = '新提問';
    private $interaction_replied = '再提問';
    private $interaction_finish = '已回覆';

    public $url_rewrite;
    public $interaction;
    public $replies;
    public $relations;
    public $type;
    public $type_url_rewrite;
    public $family;
    public $family_url_rewrite;
    public $category;
    public $category_url_rewrite;

    public function __construct($source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $this->code = $source->increment_id;
        $this->created_at = date( "Y/m/d", strtotime($source->created_at)) . '<br>' . date( "H:i:s", strtotime($source->created_at));
        $this->response_at = $source->interaction == $this->interaction_finish ?  date( "Y/m/d", strtotime($source->last_timestamp)) . '<br>' . date( "H:i:s", strtotime($source->last_timestamp)) : '尚未回覆';
        $this->latest_reply = '';
        $this->link = $source->client_link;
//        if(!Config::get('app.debug')){
//            $this->link = str_replace('www.webike.tw', '' , str_replace('http://','//',$source->client_link));
        $this->link = str_replace('http://www.webike.tw', '//www.webike.tw'.env('APP_URLMODIFY_PATH','') , $source->client_link);
//        }

        $latest_reply = current($source->replies);
        if($latest_reply){
            $this->latest_reply = str_limit($latest_reply->excerpt, 30);
        }
        $this->note = '';
        foreach ($source->relations as $relation){
            switch ($relation->object_type){
                case 'WebikeOrder':
                    $this->note = '<span>'. $relation->frontText . '</span><br><span class="history-genuine-table-content-blue"><a href="' . URL::route('customer-history-order-detail', $relation->url_rewrite) . '" target="_blank">' . $relation->name . '</a></span>';
                    break;
                case 'WebikeProduct':
                    $this->note = $relation->frontText . '<br><a href="' . URL::route('product-detail', $relation->url_rewrite) . '" target="_blank">' . $relation->name . '</a>';
                    break;
            }
        }
//            $this->has_read = true;
//            $this->has_read = false;
//            $this->read_tip = '(未讀)';
    }
}