<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\GroupBuy;

use URL;
use Auth;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\GroupBuy\Item as EgGroupBuyItem;
use Everglory\Constants\CustomerRole;

class Item extends ItemBasic
{

    public function __construct(EgGroupBuyItem $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $groupbuy = $source->groupbuy;
        if($source->product){
            $this->url_rewrite = $source->product->url_rewrite;
        }
        $this->code = $groupbuy->estimate_code;
        $this->link = $source->exist_sku ? URL::route('product-detail',$source->exist_sku) : '';
        $this->created_at = date( "Y/m/d", strtotime($source->request_date));
        $this->response_at = $groupbuy->status ?  date( "Y/m/d", strtotime($groupbuy->response_date) ) : '尚未回覆';
        $this->expiration =  $groupbuy->status ?  date( "Y/m/d", strtotime( "+15 day", strtotime( $groupbuy->response_date ) ) ) : '尚未回覆';
        $this->note = '';
        if($source->product_bikou_x){
            $this->note = '<br/>備註：'.$source->product_bikou_x;
        }
        $this->syouhin_bikou = $source->syouhin_bikou;
        $this->quantity = 1;
        $this->can_sell = (!$source->nouki or strpos($source->product_nouki,'A') === 0 or $source->exist_sku) ? false : true;

        if( $groupbuy->status and empty($source->nouki) ){
            $this->delivery = '請重新查詢';
        }else{
            $this->delivery = $source->nouki ? $source->nouki->nouki_text : '尚未回覆';
        }


        $this->price_html = '<span>團購價：' . $this->currency . number_format($source->product_price) . '</span></br>';


        $this->name = "[" . ($source->product_manufacturer_name ? $source->product_manufacturer_name : $source->syouhin_maker). "]"  . '：' . ($source->product_name ? $source->product_name : $source->syouhin_name) . '<br>' . $source->product_code . '(' . $source->syouhin_kosuu . '個)';

        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
            $this->read_tip = '(未讀)';
        }
    }
}