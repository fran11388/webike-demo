<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models;

use Everglory\Constants\Payment;
use URL;
use Everglory\Constants\ClientOrderStatus;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Order as EgOrder;
use Ecommerce\Service\OrderService;
use Ecommerce\Service\ReturnService;

class Order extends ItemBasic
{
    public $coupon_name;
    public $coupon_code;
    public $comment;
    public $mobile;
    public $phone;
    public $company;
    public $vat_number;

    public function __construct(EgOrder $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
//        $service = new OrderService;
//        $this->process = $service->getStatusProcess($source);
        $this->shipping_amount = $this->currency . number_format($source->shipping_amount);
        $this->payment_amount = $this->currency . number_format($source->payment_amount);
        $this->coupon_amount = $this->currency . number_format($source->coupon_amount);
        $this->rewardpoints_amount = $this->currency . number_format($source->rewardpoints_amount);
        $this->grand_total = $this->currency . number_format($source->grand_total);
        $this->code = $source->increment_id;
        $this->url_rewrite = $source->increment_id;
        $this->payment = $source->payment->name;
        $this->fee_name = '手續費';
        if($source->payment_method == Payment::CASH_ON_DELIVERY){
            $this->fee_name = '貨到付款手續費';
        }else if(in_array($source->payment_method, [Payment::CREDIT_CARD_LAYAWAY_ESUN, Payment::CREDIT_CARD_LAYAWAY_NCCC])){
            $this->fee_name = '分期付款手續費';
        }
        $this->created_at = date( "Y/m/d", strtotime($source->created_at));
        $this->link = URL::route('customer-history-order-detail', $this->code);
        $this->total = $this->currency . number_format($source->subtotal);
        $this->quantity = $source->items->sum('quantity');
        $this->state = $this->status;

        $this->full_quota_discount = number_format($source->celebration_amount);

//        if($source->status->show_date == 1){
//            foreach (ClientOrderStatus::getSelfConsts() as $status){
//                if(in_array($source->status_id, $status['status_ids'])){
//                    $this->state = parseBladeCode($status['state'], ['date' => date( "Y/m/d", strtotime($source->updated_at. "+1 days")), 'shipping_code' => $source->tw_shipping]);
//                }
//            }
//        }
        if($source->address){
            $this->address = $source->address->county . $source->address->district . $source->address->address;
            $this->customer_name = $source->address->firstname.$source->address->lastname;
            $this->mobile = $source->address->mobile;
            $this->phone = $source->address->phone;
            $this->company = $source->address->company;
            $this->vat_number = $source->address->vat_number;
        }

        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
//            $this->read_tip = '(未讀)';
        }

        $returnService = new ReturnService();
        $this->return_types = $returnService->getOrderReturnType($source);
        $this->rex = $source->rex;
        $this->return_avaliable = false;
        if($this->rex or count($this->return_types)){
            $this->return_avaliable = true;
        }

        $this->virtual_account = '';
        if($virtualbank = $source->virtualbank){
            $this->virtual_account = $virtualbank->virtual_account;
            $this->virtual_deadline = date("Y \年 m \月 d \日",strtotime($virtualbank->deadline));
            $this->virtual_active = 0;
            if(date('Y-m-d') < date('Y-m-d', strtotime($virtualbank->deadline)) and !$virtualbank->bank_deal_date){
                $this->virtual_active = 1;
            }
            $this->virtual_finish = 0;
            if($virtualbank->bank_deal_date){
                $this->virtual_finish = 1;
//                $this->virtual_deadline = date(" m \月 d \日",strtotime($virtualbank->virtual_account));
            }
        }

    }
}