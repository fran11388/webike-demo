<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\Rex;

use URL;
use Everglory\Constants\ClientOrderStatus;
use Ecommerce\Service\ReturnService;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Rex\Item as EgRexItem;

class Item extends ItemBasic
{
    public $quantity;
    public $price;
    public $model_number;
    public $sku;

    public function __construct(EgRexItem $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $rex = $source->rex;
        $this->url_rewrite = $source->sku;
        $this->created_at = date( "Y/m/d", strtotime($source->created_at));
        $this->link = URL::route('product-detail', $this->url_rewrite);
        $returnService = new ReturnService();
        $detail = $returnService->getItemDetail($source);
        $this->product_name = $detail->product_name;
        $this->manufacturer_name = $detail->manufacturer_name;
        $this->code = $rex->increment_id;
        $this->total = $this->currency . number_format($this->price * $this->quantity);
        $this->original_price =  $this->currency . number_format($detail->original_price);
        $this->normal_price = $this->currency . number_format($detail->normal_price);
        $this->price =  $this->currency . number_format($this->price);
        $this->options = str_replace('$', ' - ', str_replace('|', '<br>', $detail->product_options));
        $this->thumbnail = $detail->product_thumbnail;


        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
//            $this->read_tip = '(未讀)';
        }
    }

}