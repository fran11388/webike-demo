<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models;

use Illuminate\Support\Collection;
use URL;
use StdClass;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Estimate as EgEstimate;
use Ecommerce\Service\EstimateService;
use Ecommerce\Service\ProductService;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;

class Estimate extends ItemBasic
{
    private $genuine_expiration_limit = "15";
    private $general_expiration_limit = "7";

    public function __construct(EgEstimate $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $this->code = $source->increment_id;
        $this->url_rewrite = $source->increment_id;
        $this->created_at = date( "Y/m/d", strtotime($source->created_at));
        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
            $this->read_tip = '(未讀)';
        }

        if(in_array($source->type_id, [1, 2])){
            $this->addGenuinepartCols($source);
        }else{
            $this->addEstimateCols($source);
        }
    }

    public function addGenuinepartCols($source){
        $this->link = route('customer-history-genuineparts-detail', $this->url_rewrite);
        $this->response_at = $source->status_id == 10 ?  date( "Y/m/d", strtotime($source->updated_at) ) : '尚未回覆';
        $this->expiration =  $source->status_id == 10 ?  date( "Y/m/d", strtotime( '+' . $this->genuine_expiration_limit . ' days', strtotime( $source->updated_at ) ) ) : '尚未回覆';
    }

    public function addEstimateCols($source){
        $items = $source->items;
        $this->items = [];
        $products = new Collection([]);
        $no_details = $items->filter(function($item){
            return !$item->details->count();
        });
        if(count($no_details)){
            $products = ProductRepository::findEstimateDetailUseId($no_details->pluck('product_id')->all());
        }

        foreach ($items as $item){
            $obj = new \StdClass;
            $obj->cache_id = $item->id;
            $obj->quantity = $item->quantity;
            $obj->delivery = $this->determineDelivery($item);
            $obj->can_sell = $this->determineCanSell($item);
            $obj->url_rewrite = $item->sku;
            $obj->model_number = $item->model_number;
            $obj->code = $item->sku;
            $obj->thumbnail = NO_IMAGE;
            $obj->link = '';
            $obj->name = '';
            $obj->summary = '';
            $obj->options = array();
            $obj->models = array();

            $estimateService = new EstimateService(true);
            $obj->details = $estimateService->getEstimateItemDetail($item);

            if(count((array)$obj->details)){
                $obj->url_rewrite = $item->sku;
                $obj->model_number = $item->model_number;
                $obj->code = $obj->url_rewrite;
                $obj->link = URL::route('product-detail', $obj->url_rewrite);

                if(isset($obj->details->original_price) and $item->price == round($obj->details->original_price)){
                    $obj->price_html = '<span>定價：'. number_format($item->price) . 'x' .  $item->quantity . '=' . $this->currency . number_format($item->price * $item->quantity) . '</span></br>';
                }else if(isset($obj->details->original_price)){
                    $obj->price_html = '<span><s>定價：' . number_format($obj->details->original_price) . '</s>　特價：'. number_format($item->price) . ' x ' .  $item->quantity . ' = ' . $this->currency . number_format($item->price * $item->quantity) . '</span></br>';
                }else{
                    $obj->price_html = '<span>特價：'. number_format($item->price) . ' x ' .  $item->quantity . ' = ' . $this->currency . number_format($item->price * $item->quantity) . '</span></br>';
                }

                $obj->name = '【' . $obj->details->manufacturer_name . '】' . $obj->details->product_name;
                $options = array();
                $options_arr = explode('|', $obj->details->product_options);
                foreach ($options_arr as $options_str) {
                    $model_arr = explode('$', $options_str);
                    if(count($model_arr) > 1){
                        $options[] = '【'.$model_arr[0].'】'.$model_arr[1];
                    }
                }
                $obj->options = $options;
                $models = array();
                $models_arr = explode('|', $obj->details->models);
                foreach ($models_arr as $models_str) {
                    $model_arr = explode('$', $models_str);
                    if(count($model_arr) > 1){
                        $models[] = '【'.$model_arr[0].'】'.$model_arr[1];
                    }
                    if(count($models) == 5){
                        break;
                    }
                }
                $obj->models = $models;
                $obj->summary = $obj->details->product_summary;
                $obj->thumbnail = $obj->details->product_thumbnail;
            }else{
                $product_id = $item->product_id;

                $product = $products->filter(function($product) use($product_id){
                    return $product->id == $product_id;
                })->first();

                if($product){
                    $product = new ProductAccessor($product , false);

                    $obj->url_rewrite = $product->url_rewrite;
                    $obj->model_number = $product->model_number;
                    $obj->code = $product->url_rewrite;
                    $obj->link = URL::route('product-detail', $product->url_rewrite);
                    $obj->name = '【' . $product->manufacturer->name . '】' . $product->name;
                    $options = array();
                    $options_arr = explode('|', $product->optionFlat);
                    foreach ($options_arr as $options_str) {
                        $model_arr = explode('$', $options_str);
                        if(count($model_arr) > 1){
                            $options[] = '【'.$model_arr[0].'】'.$model_arr[1];
                        }
                    }
                    $obj->options = $options;

                    $models = array();
                    if($product->fitModels and count($product->fitModels)){
                        foreach ($product->fitModels->groupBy('maker')->take(3) as $fitModels){
                            $first_model = $fitModels->first();
                            foreach ($fitModels->take(3) as $model){
                                $models[] = '【' . $first_model->maker . '】' . $model->model . ' ' . $model->style;
                                echo count($models);
                                if(count($models) == 5){
                                    break 2;
                                }
                            }
                        }
                    }
                    $obj->models = $models;

                    if($item->price == $product->price){
                        $obj->price_html = '<span>定價：'. number_format($item->price) . 'x' .  $item->quantity . '=' . $this->currency . number_format($item->price * $item->quantity) . '</span></br>';
                    }else if($product->getNormalPrice()){
                        $obj->price_html = '<span><s>定價：' . number_format($item->price) . '</s>　特價：'. number_format($product->getNormalPrice()) . ' x ' .  $item->quantity . ' = ' . $this->currency . number_format($product->getNormalPrice() * $item->quantity) . '</span></br>';
                    }else{
                        $obj->price_html = '<span>特價：'. number_format($item->price) . ' x ' .  $item->quantity . ' = ' . $this->currency . number_format($item->price * $item->quantity) . '</span></br>';
                    }

                    $summary = $product->productDescription->summary;
                    $obj->summary = str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $summary);
                    $image = $product->images->first();
                    $obj->thumbnail = $image ? $image->thumbnail : NO_IMAGE;
                }
            }
            $this->items[] = $obj;
        }

        $this->response_at = $source->status_id == 10 ?  date( "Y/m/d", strtotime($source->updated_at) ) : '尚未回覆完畢';
        $this->expiration =  $source->status_id == 10 ?  date( "Y/m/d H:i:s", strtotime( '+' . $this->general_expiration_limit . ' days', strtotime( $source->updated_at ) ) ) : '回覆後' . $this->general_expiration_limit .'天';
    }

    public function determineDelivery($item)
    {
        $purchase = $item->purchases->first();
        if($item->status_id == 4){
            if(!$purchase){
                $text = '已下架';
            }else{
                if($this->determineCanSell($item)){
                    if($purchase->delivery_days){
                        $text = '約' . $purchase->delivery_days . '個工作天發貨';
                    }else{
                        if($purchase->delivery_code == '000' or $purchase->delivery_code == '001'){
                            $text = '約2個工作天發貨';
                        }else{
                            if($delivery = $purchase->delivery){
                                $text = $purchase->delivery->text;
                            }else{
                                $text = '約'. substr($purchase->delivery_code, 1) .'個工作天發貨';
                            }

                        }
                    }
                }else{
                    if(strpos($purchase->delivery_code, 'A') !== false){
                        $text = $purchase->delivery->text;
                    }else{
                        $text = '請重新查詢';
                    }
                }
            }
        }else{
            $text = '尚未回覆';
        }
        return $text;
    }

    public function determineCanSell($item)
    {
        $estimate = $this->source;
        $purchase = $item->purchases->first();
        $has_cart_btn = true;
        if(
            $item->status_id != 4 or
            (strtotime(date('Y-m-d', strtotime($estimate->updated_at . '+' . $this->general_expiration_limit . ' days'))) - strtotime(date('Y-m-d', strtotime('now')))) < 0 or
            !$purchase or
            !$purchase->delivery_code or
            strpos($purchase->delivery_code, 'A') !== false
        ){
            $has_cart_btn = false;
        }
        return $has_cart_btn;
    }
}