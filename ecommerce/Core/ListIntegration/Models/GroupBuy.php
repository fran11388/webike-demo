<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models;

use URL;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\GroupBuy as EgGroupBuy;

class GroupBuy extends ItemBasic
{

    public function __construct(EgGroupBuy $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $this->code = $source->estimate_code;
        $this->url_rewrite = $source->estimate_code;
        $this->created_at = date( "Y/m/d", strtotime($source->request_date));
        $this->response_at = $source->status == 1 ?  date( "Y/m/d", strtotime($source->response_date) ) : '尚未回覆';
        $this->expiration =  $source->status == 1 ?  date( "Y/m/d", strtotime( "+7 day", strtotime( $source->response_date ) ) ) : '尚未回覆';
        $this->link = URL::route('customer-history-groupbuy-detail', $this->code);
        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
            $this->read_tip = '(未讀)';
        }
    }
}