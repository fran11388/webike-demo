<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\Estimate;

use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\Quote\Exception;
use Everglory\Models\Product;
use URL;
use Auth;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Estimate\Item as EgEstimateItem;
use Ecommerce\Service\EstimateService;

class Item extends ItemBasic
{
    public $model_number;
    public $delivery_code;
    public $note;
    public $quantity;
    public $price;
    public $outletProduct = null;
    private $genuine_expiration_limit = "15";
    private $general_expiration_limit = "7";

    public function __construct(EgEstimateItem $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
//        $outletProducts = $this->package->outletProducts;
//        $outletProduct = $outletProducts->first(function($outletProduct) use($source){
//            return $outletProduct->model_number == $source->model_number;
//        });
        $estimate = $source->estimate;
        $this->url_rewrite = $source->sku;
        $this->manufacturer_id = $source->manufacturer_id;
        $this->code = $estimate->increment_id;
        $this->cache_id = $source->id;
        $this->created_at = date("Y/m/d", strtotime($source->created_at));
        $this->response_at = $estimate->status_id == 10 ? date("Y/m/d", strtotime($estimate->updated_at)) : '尚未回覆';
        $this->expiration = $estimate->status_id == 10 ? date("Y/m/d", strtotime("+" . $this->genuine_expiration_limit . " days", strtotime($estimate->updated_at))) : '尚未回覆';
        $this->delivery = $this->determineDelivery($source);

        $this->link = '';
        if ($this->can_sell) {
            $this->link = URL::route('product-detail', $this->url_rewrite);
        }

        $this->manufacturer = $source->manufacturer;
        $this->name = "[" . $this->manufacturer->name . "]" . $this->model_number;
        $estimateService = new EstimateService(true);
        $this->details = $estimateService->getEstimateItemDetail($source);
        if ($this->details) {
            if (isset($this->details->original_price) and $source->price == round($this->details->original_price)) {
                $this->price_html = '定價：' . number_format($source->price) . 'x' . $source->quantity . '=' . $this->currency . number_format($source->price * $source->quantity) . '</br>';
            } else if (isset($this->details->original_price)) {
                $this->price_html = '<s>定價：' . number_format($this->details->original_price) . '</s>　特價：' . number_format($source->price) . ' x ' . $source->quantity . ' = ' . $this->currency . number_format($source->price * $source->quantity) . '</br>';
            } else {
                $this->price_html = '特價：' . number_format($source->price) . ' x ' . $source->quantity . ' = ' . $this->currency . number_format($source->price * $source->quantity) . '</br>';
            }
//            if($outletProduct){
//                $this->outletProduct = new ProductAccessor($outletProduct);
//                $discount = '';
//                if(($this->outletProduct->getFinalPrice($source->customer)/$source->price) < 1){
//                    $discount = ' ('.number_format(($this->outletProduct->getFinalPrice($source->customer)/$source->price)*100).'%)';
//                }
//
//                $outlet_html_begin = '<span>';
//                $outlet_html_end = "</span><div style='padding-top: 15px;border-top: 1px solid #dcdee3;margin-top: 15px;' ><a href='".\URL::route('product-detail',$this->outletProduct->url_rewrite)."' target='_blank' class='size-085rem'>此項零件有出清品 <span class='font-bold'>NT$ ".number_format($this->outletProduct->getFinalPrice($source->customer)). $discount ."</span></a></div>";
//                $this->price_html = $outlet_html_begin.$this->price_html.$outlet_html_end;
//            }

            if (isset($this->details->syouhin_op6)) {
                if ($this->details->syouhin_op6 != $this->model_number) {
                    $this->name .= '(' . $this->details->syouhin_op6 . ')';
                }
            }
        }

        if ($source->read) {
            $this->has_read = true;
        } else {
            $this->has_read = false;
//            $this->read_tip = '(未讀)';
        }

        if (in_array($estimate->type_id, [1, 2])) {
            $this->addGenuinepartCols($estimate);
        } else {
            $this->addEstimateCols($estimate);
        }
    }

    public function addGenuinepartCols($estimate)
    {

    }

    public function addEstimateCols($estimate)
    {
    }

    public function determineDelivery($item, $outletProduct = null)
    {
        $purchase = $item->purchases->first();
        $estimate = $item->estimate;
        $text = '';
        if ($item->status_id == 4) {
            if ($this->can_sell = $this->determineCanSell($item)) {
                try {
                    $begin_date = $estimate->created_at;

                    $working_days = $purchase->delivery_code % 100;
                    if ($purchase->supplier_id == 1) $working_days += 3;

                    $api_result = file_get_contents(config('api.delivery_date_query') . '?days=' . $working_days . "&begin_date=" . $begin_date);
                    $result = json_decode($api_result, true);
                    $text = $result['data']['date'];

                } catch (\Exception $e) {
                    $message = "delivery date api get some problem, plz contact Frank. error message:" . $e->getMessage();
                    \Log::critical($message);

                    if ($purchase->delivery_days) {
                        $text = '約' . $purchase->delivery_days . '個工作天發貨';
                    } else {
                        if ($purchase->delivery_code == '000' or $purchase->delivery_code == '001') {
                            $text = '約2個工作天發貨';
                        } else {
                            $text = $purchase->delivery->text;
                        }
                    }
                }

            } else {
                if (strpos($purchase->delivery_code, 'A') !== false) {
                    $text = $purchase->delivery->text;
                    $text = $this->deliveryStringConversion($text);
                } else {
                    $text = '請重新查詢';
                }
            }
        } else {
            $text = '尚未回覆';
        }
        if ($outletProduct) {
            $sku = $outletProduct->sku;
            $html_begin = "<div style='margin-bottom:15px;line-height:46px;'><span>";
            $html_end = "</span></div><div style='padding-top: 15px;border-top: 1px solid #dcdee3;'><a href='" . \URL::route('product-detail', $sku) . "' target='_blank'><span class='size-085rem'>購買請直接到商品頁</span></a></div>";
            $text = $html_begin . $text . $html_end;
        }


        return $text;
    }

    private function deliveryStringConversion($string)
    {
        switch ($string) {
            case '交期未定':
                return '交期未定(廠商缺貨中，暫時無法確認出貨日期，建議兩週後再重新查詢)';
                break;
            case '不明':
                return '不明(您提供的料號可能不完全或是有誤，請再次確認)';
                break;
            default:
                return $string;
        }
    }

    public function determineCanSell($item)
    {
        $estimate = $item->estimate;
        $purchase = $item->purchases->first();
        $has_cart_btn = true;
        if (
            $item->status_id != 4 or
            (strtotime(date('Y-m-d', strtotime($estimate->updated_at . '+' . $this->genuine_expiration_limit . ' days'))) - strtotime(date('Y-m-d', strtotime('now')))) < 0 or
            !$purchase or
            !$purchase->delivery_code or
            strpos($purchase->delivery_code, 'A') !== false) {
            $has_cart_btn = false;
        }
        return $has_cart_btn;
    }
}