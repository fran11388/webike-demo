<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models\Mitumori;

use URL;
use Auth;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Mitumori\Item as EgMitumoriItem;
use Everglory\Constants\CustomerRole;

class Item extends ItemBasic
{

    public function __construct(EgMitumoriItem $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $mitumori = $source->mitumori;
        if($source->product){
            $this->url_rewrite = $source->product->url_rewrite;
        }else{
            $this->url_rewrite = $source->exist_sku;
        }
        
        $this->code = $mitumori->estimate_code;


        $this->link = '';
        if(!starts_with($source->product_nouki,'A')){
            $this->link = route('customer-history-mitumori-detail', $this->code);
        }
        
//        $this->link = $source->exist_sku ? URL::route('product-detail',$source->exist_sku) : '';
        
        $this->created_at = date( "Y/m/d", strtotime($source->request_date));
        $this->response_at = $mitumori->status ?  date( "Y/m/d", strtotime($mitumori->response_date) ) : '尚未回覆';
        $this->expiration =  $mitumori->status ?  date( "Y/m/d", strtotime( "+15 day", strtotime( $mitumori->response_date ) ) ) : '尚未回覆';
        $this->note = $source->product_bikou_x;
        $this->quantity = $source->syouhin_kosuu;
        $this->can_sell = (!$source->nouki or strpos($source->product_nouki,'A') === 0 or $source->exist_sku or !$source->product_id) ? false : true;

        $this->product_link = '';
        if($source->exist_sku){
            $this->product_link = route('product-detail',$source->exist_sku);
        }


        if( $mitumori->status and empty($source->nouki) ){
            $this->delivery = '請重新查詢';
        }else{
            $this->delivery = $source->nouki ? $source->nouki->nouki_text : '尚未回覆';
        }

        if(in_array(Auth::user()->role_id, [CustomerRole::WHOLESALE, CustomerRole::STAFF])){
            $this->price_html = '<span><s>會員價：' . $this->currency . number_format($source->product_price_general) . '</s>　經銷價：' . $this->currency . number_format($source->product_price) . ' x ' . $source->syouhin_kosuu . '個 = ' . $this->currency . number_format($source->product_price * $source->syouhin_kosuu) . '</span>';
        }else{
            $this->price_html = '<span>會員價：' . $this->currency . number_format($source->product_price) .  ' x ' . $source->syouhin_kosuu . '個 = ' . $this->currency . number_format($source->product_price * $source->syouhin_kosuu) . '</span></br>';
        }

        $this->name = ($source->product_manufacturer_name ? $source->product_manufacturer_name : $source->syouhin_maker) . '：' . ($source->product_name ? $source->product_name : $source->syouhin_name) . '<br>' . $source->product_code;

        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
            $this->read_tip = '(未讀)';
        }
    }
}