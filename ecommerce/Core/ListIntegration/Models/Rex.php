<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration\Models;

use URL;
use Everglory\Constants\ClientOrderStatus;
use Ecommerce\Core\ListIntegration\ItemBasic;
use Everglory\Models\Rex as EgRex;

class Rex extends ItemBasic
{
    public $coupon_name;
    public $coupon_code;
    public $comment;
    public $mobile;
    public $phone;
    public $company;
    public $vat_number;

    public function __construct(EgRex $source)
    {
        $this->source = $source;
    }

    public function init()
    {
        $source = $this->source;
        $this->shipping_amount = $this->currency . number_format($source->shipping_amount);
        $this->type_name = ($source->type == 'exchange' ? '換貨' : '退貨' );
        $this->payment_amount = $this->currency . number_format($source->payment_amount);
        $this->coupon_amount = $this->currency . number_format($source->coupon_amount);
        $this->rewardpoints_amount = $this->currency . number_format($source->rewardpoints_amount);
        $this->grand_total = $this->currency . number_format($source->grand_total);
        $this->code = $source->increment_id;
        $this->url_rewrite = $source->increment_id;
//        $this->payment = $source->payment->name;
        $this->created_at = date( "Y/m/d", strtotime($source->created_at));
        $this->link = URL::route('customer-history-return-detail', $this->code);
        $this->total = $this->currency . number_format($source->subtotal);
        $this->quantity = $source->items->sum('quantity');
        $this->state = $this->status;
        if($source->status->show_date == 1){
            foreach (ClientOrderStatus::getSelfConsts() as $status){
                if(in_array($source->status_id, $status['status_ids'])){
                    $this->state = parseBladeCode((is_array($status['state']) ? $status['state'][hasChangedLogistic()] : $status['state']), ['date' => date( "Y/m/d", strtotime($source->updated_at. "+1 days")), 'shipping_code' => $source->tw_shipping]);
                }
            }
        }
        if($source->address){
            $this->address = $source->address->county . $source->address->district . $source->address->address;
            $this->customer_name = $source->address->firstname.$source->address->lastname;
            $this->mobile = $source->address->mobile;
            $this->phone = $source->address->phone;
            $this->company = $source->address->company;
            $this->vat_number = $source->address->vat_number;
        }

        if($source->read){
            $this->has_read = true;
        }else{
            $this->has_read = false;
//            $this->read_tip = '(未讀)';
        }
    }
}