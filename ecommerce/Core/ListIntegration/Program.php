<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2016/12/05
 * Time: 下午 02:32
 */

namespace Ecommerce\Core\ListIntegration;

use Log;
use StdClass;
use ReflectionClass;
use ReflectionProperty;
use Illuminate\Support\Collection;

class Program
{
    protected $collection;
    protected $package;

    public function __construct($collection = NULL, $appoint = NULL)
    {
        $this->package = new StdClass;
        $this->collection = array();
        if($collection){
            $this->bulidCollection($collection, $appoint);
        }

        $this->collection = new Collection($this->collection);
    }

    public function bulidCollection($collection, $appoint = NULL)
    {
        $cols = [];
        $first_item = $collection->first();
        if($first_item){
            if(!$appoint){
                $target_class = get_class($first_item);
                $partition = 'Models\\';
                if(strpos($target_class, 'Entities\\')){
                    $partition = 'Entities\\';
                }
                $start_postition = strpos($target_class, $partition) + strlen($partition);
                $end_postition = strlen($target_class);
                $class_path = substr($target_class, $start_postition, $end_postition);
                $class_name = __NAMESPACE__ .'\\Models\\'. $class_path;
                $cols = $collection->first()->getTableColumns();
            }else{
                $class_name = __NAMESPACE__ .'\\Models\\'. $appoint;
                if (class_exists($class_name)) {
                    $cols = array_keys((array)$first_item);
                }
            }

            if (class_exists($class_name)) {
                foreach ($collection as $item){
                    $list_object = new $class_name($item);
                    $this->autoLoadCols($item, $list_object, $cols);
                    if($this->package){
                        $list_object->receivePackage($this->package);
                    }
                    $list_object->init();
                    $this->collection[] = $list_object;
                }
            } else {
                Log::error("class {$class_name} don't exist!");
            }
        }
    }

    public function setPackage($name, $package)
    {
        $this->package->$name = $package;
    }

    public function __get($name)
    {
        return (isset($this->$name) ? $this->$name : null);
    }

    public function autoLoadCols($form_object, $import_object, $cols = NULL)
    {
        if(is_array($cols) and count($cols)){
            $needles = $cols;
        }else{
            $needles = $form_object->getTableColumns();
        }
        $reflect = new ReflectionClass($import_object);
        $props = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);
        $props = new Collection($props);
        foreach ($props->pluck('name') as $key => $value) {
            if(in_array($value, $needles)){
                $import_object->$value = $form_object->$value;
                if($value == 'status_id' and $form_object->status){
                    $import_object->status = $form_object->status->name;
                }
            }
        }
    }
}