<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\ListIntegration;

class ItemBasic
{
    public $code;
    public $url_rewrite;
    public $has_read = false;
    public $status_id;
    public $created_at;
    public $response_at;
    public $expiration;
    public $source;
    public $link;
    public $currency = 'NT$';
    public $read_tip;
    public $package;

    public function __get($name)
    {
        return (isset($this->$name) ? $this->$name : null);
    }

    public function receivePackage($package)
    {
        $this->package = $package;
    }
}