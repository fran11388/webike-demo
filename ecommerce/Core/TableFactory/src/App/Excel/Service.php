<?php 
namespace Ecommerce\Core\TableFactory\src\App\Excel;

use Ecommerce\Core\TableFactory\src\App\Data;
use Ecommerce\Core\TableFactory\src\App\Excel\Action;
use Excel;

class Service {

	private $fileInfo = array();
	private $infoItems = array('filename', 'title', 'creator', 'company');
	private $colWidth = array();
	private $colHeight = array();
	private $program = array();
	private $position = [
				'maxCol' => 'A',
				'minCol' => 'A',
				'maxRow' => '1',
				'minRow' => '1'
			];
	private $hasHeader = true;
	private $excelData = array();
	public $brChar;

	public function __construct( $info = null )
	{
		$brChar = chr(13);
		if( isset($info) ){
			foreach ($this->infoItems as $infoItem) {
				$this->fileInfo[$infoItem] = $info->$infoItem;
			}
		}else{
			foreach ($this->infoItems as $infoItem) {
				$this->fileInfo[$infoItem] = 'no setting';
			}
		}
	}

	public function download()
	{
		Excel::create($this->fileInfo['filename'], function($excel){

			$excel->setTitle($this->fileInfo['title']);
            $excel->setCreator($this->fileInfo['creator']);
            $excel->setCompany($this->fileInfo['company']);

			$excel->sheet($this->fileInfo['title'], function($sheet){

				$sheet->setHeight($this->colsHeight);
				$sheet->setWidth($this->colsWidth);
				
				// $sheet->setBorder('B2:AA4','thin');

				$action = new Action;
				$program = $this->program;
				foreach ($program as $thing) {
					foreach ($thing as $method => $data){
						$action->$method($sheet, $data);
					}
				}

			});
		})->export('xlsx');
	}
	
	public function store($path , $extension = 'xlsx')
	{
		Excel::create($this->fileInfo['filename'], function($excel){

			$excel->setTitle($this->fileInfo['title']);
			$excel->setCreator($this->fileInfo['creator']);
			$excel->setCompany($this->fileInfo['company']);

			$excel->sheet($this->fileInfo['title'], function($sheet){

				$sheet->setWidth($this->colsWidth);

				$action = new Action;
				$program = $this->program;
				foreach ($program as $thing) {
					foreach ($thing as $method => $data){
						$action->$method($sheet, $data);
					}
				}

			});

		})->store($extension, $path);
	}

	public function setInfo( $info )
	{
		foreach ($this->infoItems as $infoItem) {
			$fileInfo[$infoItem] = $info->$infoItem;
		}
	}

	public function setAllColsWidth( $width )
	{
		$colsArray = [];
		$range = range('A', 'Z');
		foreach ($range as $value_1) {
			$colsArray[$value_1] = $width;
			foreach ($range as $value_2) {
				$colsArray[$value_1.$value_2] = $width;
			}
		}
		$this->colsWidth = $colsArray;
	}

	public function setColsWidth( $array )
	{
		$this->colsWidth = $array;
	}

	public function setAllColsHeight( $height )
	{
		$colsArray = [];
		$range = range('1', '51');
		foreach ($range as $value_1) {
			$colsArray[$value_1] = $height;
		}
		$this->colsHeight = $colsArray;
	}

	public function setColsHeight( $array )
	{
		$this->colsHeight = $array;
	}

	public function setBorder( $borderRange ){
		$this->colsBorder = $borderRange;
	}

	public function setProgram($actionMethod, $data)
	{
		$this->resetPosition($data['positions']);
		$this->program[] = array($actionMethod => $data);

	}

	public function resetProgram()
	{
		$position = [
			'maxCol' => 'A',
			'minCol' => 'A',
			'maxRow' => '1',
			'minRow' => '1'
		];

		$this->position = $position;
		$this->program = array();
	}

	public function getPosition()
	{
		return $this->position;
	}

	private function resetPosition( $positions )
	{
		$chars = range('A', 'Z');
		foreach ($positions as $key => $position) {
			if( array_search($position['col'], $chars) > array_search($this->position['maxCol'], $chars) ){
				$this->position['maxCol'] = $position['col'];
			}else if( array_search($position['col'], $chars) < array_search($this->position['minCol'], $chars) ){
				$this->position['minCol'] = $position['col'];
			}

			if( $position['row'] > $this->position['maxRow'] ){
				$this->position['maxRow'] = $position['row'];
			}else if( $position['row'] < $this->position['minRow'] ){
				$this->position['minRow'] = $position['row'];
			}
		}
	}

	
}