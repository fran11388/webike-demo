<?php 
namespace Ecommerce\Core\TableFactory\src\App\Excel;

use Excel;

class Action {

	public function __construct( )
	{

	}

	public function addCenterBox($sheet, $data)
	{
		$this->dataCheck($data, ['positions', 'value']);
		$positions = $data['positions'];
		$cols = $this->getCols($positions);

		$this->setBreakLine($sheet, $data);
		if(isset($data['setborder'])){
			$sheet->mergeCells( $cols )
		        ->setBorder( $cols )
		        ->cell( $positions[0]['col'] . $positions[0]['row'] , function( $cell ) use($data) {
		            $cell->setValue( $data['value'] );
		            $cell->setAlignment('center');
		            $cell->setValignment('center');
		        });
	    }else{
	    	$sheet->mergeCells( $cols )
		        ->cell( $positions[0]['col'] . $positions[0]['row'] , function( $cell ) use($data) {
		            $cell->setValue( $data['value'] );
		            $cell->setAlignment('center');
		            $cell->setValignment('center');
		        });
	    }
	}

	public function addBox($sheet, $data)
	{
		$this->dataCheck($data, ['positions', 'value']);
		$positions = $data['positions'];
		$cols = $this->getCols($positions);

		$this->setBreakLine($sheet, $data);
		if(isset($data['setborder'])){
			$sheet->mergeCells( $cols )
		        ->setBorder( $cols )
		        ->cell( $positions[0]['col'] . $positions[0]['row'] , function( $cell ) use($data) {
		            $cell->setValue( $data['value'] );
		            $cell->setValignment('top');
		        });
	    }else{
	    	$sheet->mergeCells( $cols )
		        ->cell( $positions[0]['col'] . $positions[0]['row'] , function( $cell ) use($data) {
		            $cell->setValue( $data['value'] );
		            $cell->setValignment('top');
		        });
	    }
	}

	public function setOutLine($sheet,$data)
	{
		$range = $data['range'];
		$sheet->cells($range, function($cells) {
		      $cells->setBorder('thin', 'thin', 'thin', 'thin');
		});
	}

	public function setBreakLine($sheet, $data)
	{
		$this->dataCheck($data, ['positions']);
		$positions = $data['positions'];
		$cols = $this->getCols($positions);
		$sheet->getStyle($cols)->getAlignment()->setWrapText(true);
	}

	public function toLeft($sheet, $data)
	{
		$this->dataCheck($data, ['positions']);
		$positions = $data['positions'];
		$cols = $this->getCols($positions);
		$sheet->cells($cols, function($cells){
			$cells->setAlignment('left');
		});
	}

	public function setFontRed($sheet, $data)
	{
		$this->dataCheck($data, ['positions']);
		$positions = $data['positions'];
		$cols = $this->getCols($positions);
		$sheet->cells($cols, function($cells){
			$cells->setFontColor('#FF0000');
		});
	}

	public function dataCheck($data, $attributes)
	{
		$keys = array_keys($data);
		foreach ($attributes as $key => $attribute) {
			if( !in_array($attribute, $keys) ){
				throw new Exception("this function ".$attribute." is Request");
			}
		}
	}

	public function getCols($positions)
	{
		if(isset($positions[1])){
			$cols = $positions[0]['col'] . $positions[0]['row'] . ':' . $positions[1]['col'] . $positions[1]['row'];
		}else{
			$cols = $positions[0]['col'] . $positions[0]['row'] . ':' . $positions[0]['col'] . $positions[0]['row'];
		}

		return $cols;
	}
}