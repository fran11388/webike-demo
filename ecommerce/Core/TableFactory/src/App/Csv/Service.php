<?php 
namespace TableFactory\App\Csv;

use TableFactory\App\Data;
use Excel;

class Service {

	private $filename;
	private $data;
	private $excelData = array();
	private $text = '';
	private $hasHeader = true;

	public function __construct( Data $data, $filename = null )
	{
		// if( $data->check() ){
		// 	$this->data = $data;
		// }
		$this->data = $data;

		if( isset($filename) ){
			$this->filename = $filename;
		}else{
			$this->filename = date('Y-m-d H:i:s').'-export';
		}
		
	}

	public function noHeader()
	{
		$this->hasHeader = false;
		return $this;
	}

	public function download()
	{
		$this->toExcelData();

		Excel::create($this->filename, function($excel) {

			$excel->sheet('Sheetname', function($sheet) {
				$sheet->rows($this->excelData);
			});

		})->export('csv');
	}

	private function toExcelData()
	{
		$dataHeader = $this->data->getHeader();
		if( count($dataHeader) > 1 and gettype(current($dataHeader)) == 'array' ){
			foreach ($dataHeader as $h_key => $header) {
				if( $this->hasHeader ){
					$this->excelData[] = $header;
				}
				foreach ($this->data->getRows()[$h_key] as $row) {
					$this->excelData[] = $row;
				}
			}
		}else{
			if( $this->hasHeader ){
				$this->excelData[] = $dataHeader;
			}
			
			foreach ($this->data->getRows() as $key => $row) {
				$this->excelData[] = $row;
			}
		}
	}

	private function toText()
	{
		$this->text .= implode( ',', $this->data->getHeader() )."\n";
		foreach ($this->data->getRows() as $key => $row) {
			$this->text .= implode( ',', $row )."\n";
		}
	}

	public function importCsv($file)
	{
	}

	// public function setMode( Array $mode )
	// {
	// 	if( count($mode) != 1 ){
	// 		throw new Exception("The mode parameter need to be an array with count is 1");
	// 	}
	// 	if( !array_key_exists(key($mode[0]), $this->modes) ){
	// 		throw new Exception("CsvService is not has '".$mode."' mode.");
	// 	}
	// 	$this->modes[key($mode[0])] = $mode[0];
	// 	$this->mode = key($mode[0]);
	// }

	// private function setHeader( $collection )
	// {

	// }

	// private function setContent( Array $collection )
	// {
	// 	foreach ($collection as $key => $raw) {
	// 		if( $raw instanceof Array ){
	// 			$text .= implode(',', $raw)."\n";
	// 		}else{

	// 		}
			
	// 	}
		
	// }
}