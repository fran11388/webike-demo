<?php 
namespace TableFactory\App;

class Data {

	private $header = array();
	private $rows = array();


	public function setHeader( Array $header )
	{
		$this->header = $header;
	}

	public function setRows( Array $rows )
	{
		$this->rows = $rows;
	}

	public function getHeader()
	{
		return $this->header;
	}

	public function getRows()
	{
		return $this->rows;
	}

	public function getSize()
	{
		return count($this->rows);
	}

	public function check()
	{
		$cell_count = sizeof($this->rows, 1) - count($this->rows);
		if( $cell_count / count($this->header) == count($this->rows) ){
			return true;
		}
		throw new Exception("This data header cells with row cells is not the same, note[ data row can't has array ]");
	}
}