<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\Ticket\Models;

class Base
{

    public function __construct()
    {
    }

    public function __get($name)
    {
        return (isset($this->$name) ? $this->$name : null);
    }

    public function getAllAttribute()
    {
        return get_object_vars($this);
    }

    public function import($data)
    {
        $attributes = $this->getAllAttribute();
        $attribute_keys = array_keys($attributes);
        foreach ($attribute_keys as $key){
            if(isset($data->$key)){
                $this->$key = $data->$key;
            }
        }
    }
}