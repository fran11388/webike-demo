<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\Ticket\Models;

use Ecommerce\Core\Ticket\Models\Base;
use Ecommerce\Core\Ticket\Models\Config;
use Exception;
use ReflectionClass;
use ReflectionProperty;

class PostData extends Base
{
    protected $host;
    protected $email;
    protected $user_group;
    protected $user_name;
    protected $validates = [
        'insert' => ['content', 'category_id'],
        'reload' => ['increment_id'],
        'reply' => ['increment_id', 'content'],
        'read' => ['increment_id'],
        'relationTickets' => ['relations'],
    ];

    function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __construct($import_data)
    {
        $this->host = Config::TICKET_HOST;
        $this->import($import_data);
    }

    public function validate($type)
    {
        if(isset($this->validates[$type])){
            $cols = $this->validates[$type];
            $attributes = [];
            $reflect = new ReflectionClass($this);
            $props = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);
            foreach ($props as $prop) {
                $attributes[] = $prop->getName();
            }
            $compare = array_intersect($attributes, $cols);
            if(count($compare) == count($cols)){
                return true;
            }
            return false;
        }else{
            throw new Exception('not setting ' . $type . 'attributes.');
        }
    }

    public function convert($type, $data)
    {
        if(isset($this->validates[$type])){
            $cols = $this->validates[$type];
            foreach ($cols as $col) {
                if(!isset($data[$col])){
                    throw new Exception('$data array request not has ' . $col . ' attributes. ' . $col . ' is require.');
                }
            }
            foreach ($data as $key => $value){
                $this->$key = $value;
            }
        }else{
            throw new Exception('not setting ' . $type . 'attributes.');
        }
    }


    public function toArray()
    {
        return get_object_vars ($this);
    }

}