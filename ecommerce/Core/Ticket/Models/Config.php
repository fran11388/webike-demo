<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\Ticket\Models;

use Ecommerce\Core\Ticket\Models\Base;
use URL;

class Config extends Base
{
    const TICKET_HOST = 'Webike';
    const TEST_SERVER = 'http://zero-dev.everglory.asia/';
//    const TEST_SERVER = 'http://122.116.103.32:8085/';
    const PUBLISH_SERVER = 'http://153.120.83.199/';
    const PORTS = [
        'list' => 'api/crm/service/list',
        'insert' => 'api/crm/service/insert',
        'reply' => 'api/crm/service/reply',
        'reload' => 'api/crm/service/reload',
        'types' => 'api/crm/service/types',
        'typesLayer' => 'api/crm/service/types/layer',
        'read' => 'api/crm/service/has-read',
        'relationTickets' => 'api/crm/service/relation/tickets',
    ];

    protected $ssl;
    protected $server;

    public function __construct()
    {
        $this->server = self::TEST_SERVER;
        $this->hasSSL();
    }

    public function hasSSL($has_ssl = false)
    {
        $this->ssl = 'http';
        if($has_ssl){
            $this->ssl = 'https';
        }
    }

    public function setServer($server_name = NULL){
        if($server_name){
            if(strpos($server_name, '://') === false){
                $server_name = $this->ssl . '://' . $server_name;
            }else{
                $server_name = substr($server_name, strpos($server_name, '://') + 3, strlen($server_name));
                $server_name = $this->ssl . '://' . $server_name;
            }
            $this->server = $server_name;
        }else{
            $this->server = self::TEST_SERVER;
        }
    }

    public function getPort($key = NULL)
    {
        $ports = self::PORTS;
        if(is_null($key)){
            return $ports;
        }else{
            if(is_array($key)){
                $arr = [];
                foreach ($key as $item){
                    if(isset($ports[$item])){
                        $arr[] = $ports[$item];
                    }
                }
                return $arr;
            }else if(isset($ports[$key])){
                return $ports[$key];
            }else{
                return null;
            }
        }
    }

}