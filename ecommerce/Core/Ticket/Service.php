<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\Ticket;

use Ecommerce\Core\Ticket\Models\Config;
use Ecommerce\Core\Ticket\Models\PostData;

class Service
{
    private $config;
    private $user_data;
    public $post_data;

    public function __construct($user_data = NULL)
    {
        $this->config = new Config;
        if($user_data){
            $this->setUser($user_data);
        }
    }

    public function setUser($data)
    {
        $this->user_data = new PostData($data);
        $this->post_data = new PostData($data);
    }

    public function setUserGroup($name)
    {
        $this->user_data->user_group = $name;
        $this->post_data->user_group = $name;
    }

    public function setUserName($name)
    {
        $this->user_data->user_name = $name;
        $this->post_data->user_name = $name;
    }

    public function setConfig($data)
    {
        $this->config = new PostData($data);
    }

    public function usePublishServer()
    {
        $this->config->setServer(Config::PUBLISH_SERVER);
    }

    public function getPort($port_name)
    {
        $port = $this->config->getPort($port_name);
        return $this->config->server . $port;
    }

    public function getList()
    {
        $result = $this->send($this->getPort('list'), true);
        return $result;
    }

    public function getTypes()
    {
        $result = $this->send($this->getPort('types'));
        return $result;
    }

    public function getTypesLayer()
    {
        $result = $this->send($this->getPort('typesLayer'));
        return $result;
    }

    public function getRelationTickets($data)
    {
        $this->post_data->convert('relationTickets', $data);
        $result = $this->send($this->getPort('relationTickets'), true);
        return $result;
    }

    public function insert($data)
    {
        $this->post_data->convert('insert', $data);
        if(isset($this->post_data->skus) and is_array($this->post_data->skus)){
            $this->post_data->skus = implode('_', $this->post_data->skus);
        }
        if(isset($this->post_data->orders) and is_array($this->post_data->orders)){
            $this->post_data->orders = implode('_', $this->post_data->orders);
        }
        if(isset($this->post_data->rexs) and is_array($this->post_data->rexs)){
            $this->post_data->rexs = implode('_', $this->post_data->rexs);
        }
        $result = $this->send($this->getPort('insert'), true);
        return $result;
    }

    public function reload($data)
    {
        $this->post_data->convert('reload', $data);
        $result = $this->send($this->getPort('reload'), true);
        return $result;
    }

    public function reply($data)
    {
        $this->post_data->convert('reply', $data);
        $result = $this->send($this->getPort('reply'), true);
        return $result;
    }

    public function read($data)
    {
        $this->post_data->convert('read', $data);
        $result = $this->send($this->getPort('read'), true);
        return $result;
    }

    public function send($url, $is_post = false)
    {
        $post_data = clone $this->post_data;
        $post_data  = $this->encryption($post_data);

        $ch = curl_init();
        if($is_post){
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_URL, "http://www.webike.tw/iopu-gp-bridge.php");
            curl_setopt($ch, CURLOPT_POST, $is_post);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data->toArray()));
        }else{
            curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($post_data->toArray()));
        }
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
//            echo $result;
//            dd($url. '<br>' .http_build_query($post_data->toArray()));
        
        $result = json_decode($result);

        return $result;
    }

    private function encryption($data)
    {
        $cols = ['host', 'email'];
        foreach ($cols as $col){
            $data->$col = base64_encode($data->$col);
        }
        return $data;
    }
}