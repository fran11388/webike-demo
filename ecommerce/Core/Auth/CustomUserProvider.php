<?php

namespace Ecommerce\Core\Auth;

use Illuminate\Auth\EloquentUserProvider;
use Ecommerce\Core\Auth\Hasher as MyHasher;

class CustomUserProvider extends EloquentUserProvider
{
    /**
     * Create a new database user provider.
     *
     * @param  MyHasher $hasher
     * @param  string  $model
     */
    public function __construct(MyHasher $hasher, $model)
    {
        $this->model = $model;
        $this->hasher = $hasher;
    }
}
