<?php

namespace Ecommerce\Core\Auth;

use Illuminate\Auth\SessionGuard as BaseSessionGuard;

class SessionGuard extends BaseSessionGuard
{

    /**
     * Get a unique identifier for the auth session value.
     *
     * @return string
     */
    public function getName()
    {
        //Illuminate\Auth\Guard
        return 'login_82e5d2c56bdd0811318f0cf078b78bfc';
    }

    /**
     * Get the name of the cookie used to store the "recaller".
     *
     * @return string
     */
    public function getRecallerName()
    {
        return 'remember_82e5d2c56bdd0811318f0cf078b78bfc';
    }

    public function getRequest()
    {
        return request();
    }
}
