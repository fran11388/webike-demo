<?php

namespace Ecommerce\Core\Auth;

class Hasher
{
    /**
     * Default crypt cost factor.
     *
     * @var int
     */
    protected $rounds = 10;

    /**
     * Hash the given value.
     *
     * @param  string $value
     * @param int $len
     * @return string
     */
    public function make($value , $len = 2)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz' . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . '0123456789';
        for ($i = 0, $salt = '', $lc = strlen( $chars ) - 1 ; $i < $len ; $i++) {
            $salt .= $chars[ mt_rand( 0, $lc ) ];
        }
        return md5( $salt . $value ) . ':' . $salt ;
    }

    /**
     * Check the given plain value against a hash.
     *
     * @param  string  $value
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function check($value, $hashedValue, array $options = [])
    {

        $hashArr = explode( ':', $hashedValue );
        if (md5( $hashArr[ 1 ]  . $value ) === $hashArr[ 0 ]) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function needsRehash($hashedValue, array $options = [])
    {
        return false;
    }

    /**
     * Set the default password work factor.
     *
     * @param  int  $rounds
     * @return $this
     */
    public function setRounds($rounds)
    {
        $this->rounds = (int) $rounds;

        return $this;
    }
}
