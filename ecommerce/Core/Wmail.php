<?php namespace Ecommerce\Core;

class Wmail
{
    protected $sender;
    protected $recipient;
    protected $data = array();
    protected $template;
    protected $subject;
    protected $bcc;
    protected $sender_list = array();

    public function __construct()
    {
        $this->sender = new \stdClass();
        $this->sender->address = 'service@webike.tw';
        $this->sender->name = 'Webike Service';
        $this->bcc = false;

        // $this->sender->address = 'order@webike.tw';
        // $this->sender->name = 'Webike Order-吳筱玲';
        // $this->sender->address = 'biz@webike.tw';
        // $this->sender->name = 'Webike Business-蘇店長';
        // $this->sender->address = 'mailmag@webike.tw';
        // $this->sender->name = 'Webike 電子報';
        // $this->sender->address = 'support@webike.tw';
        // $this->sender->name = 'Webike Order-楊千慧';
        $this->recipient = new \stdClass();
        

    }

    private function send()
    {
        try{
            if($this->bcc){
                \Mail::send($this->template, $this->data, function($message)
                {
                    $message->from($this->sender->address, $this->sender->name);
                    $message->to($this->recipient->address, $this->recipient->name);
                    $message->bcc($this->sender->address,$this->sender->name);
                    $message->subject($this->subject);

                });
            }else{
                \Mail::send($this->template, $this->data, function($message)
                {
                    $message->from($this->sender->address, $this->sender->name);
                    $message->to($this->recipient->address, $this->recipient->name);
                    $message->subject($this->subject);

                });
            }
        } catch (\Exception $e){
            \Cache::remember('sendingMailRejected', 60, function(){
                \Log::warning('Sending Mail was rejected by mail service.');
                return true;
            });
        }

    }

    public function blank($sender = null,$recipient,$title,$text)
    {
        $this->bcc = false;
        $data['text'] = $text;
        $this->data = $data;
        if($sender){
            $this->sender->address = $sender->address;
            $this->sender->name = $sender->name;    
        }
        $this->recipient->address = $recipient->address;
        $this->recipient->name = $recipient->name;

        $this->subject = $title;
        $this->template = 'emails.blank';
        $this->send();
    }

    public function review($review,$comment = null)
    {
        $this->bcc = false;
        $data['review'] = $review;
        $this->data = $data;
        $this->recipient->address = $review->customer->email;
        $this->recipient->name = $review->customer->realname;
        $code = '25';
        if($comment){
            $code .= 3;
            $data['comment'] = $comment;
            $this->data = $data;
        }else{
            if($review->view_status == 0){
                $code .= '1';
            }else if($review->view_status == 1){
                $code .= '2';
            }else if($review->view_status == 9){
                $code .= '4';
            }else{
                return null;
            }
        }

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->subject = $mail_category->title;
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }

    public function customer($customer,$password = null)
    {
        $this->bcc = false;
        $data['customer'] = $customer;
        $data['password'] = $password;
        $this->data = $data;
        $this->recipient->address = $customer->email;
        $this->recipient->name = $customer->realname;
        $code = '22';
        if($customer->role_id == 1){
            $code .= '1';
        }else{
            $code .= '2';
        }

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->subject = $mail_category->title;
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }
    public function genuinepart($genuinepart)
    {
        $this->bcc = false;
        $data['genuinepart'] = $genuinepart;
        $this->data = $data;
        $this->recipient->address = $genuinepart->customer->email;
        $this->recipient->name = $genuinepart->customer->realname;
        $code = '15';
        if($genuinepart->status == 0){
            $code .= '1';
        }else{
            $code .= '2';
        }

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->sender->address = 'order@webike.tw';
            $this->sender->name = 'Webike Order-吳筱玲';
            $this->subject = str_replace('{{$estimate_code}}', $genuinepart->estimate_code, $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }

    public function mitumori($mitumori)
    {
        $this->bcc = false;
        $data['mitumori'] = $mitumori;
        $this->data = $data;
        $this->recipient->address = $mitumori->customer->email;
        $this->recipient->name = $mitumori->customer->realname;
        $code = '16';
        if($mitumori->status == 0){
            $code .= '1';
        }else{
            $code .= '2';
        }

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->sender->address = 'order@webike.tw';
            $this->sender->name = 'Webike Order-吳筱玲';
            $this->subject = str_replace('{{$estimate_code}}', $mitumori->estimate_code , $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }

    public function groupbuy($groupbuy)
    {
        $this->bcc = false;
        $data['groupbuy'] = $groupbuy;
        $this->data = $data;
        $this->recipient->address = $groupbuy->customer->email;
        $this->recipient->name = $groupbuy->customer->realname;
        $code = '17';
        if($groupbuy->status == 0){
            $this->bcc = true;
            $code .= '1';
        }else{
            $code .= '2';
        }

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->sender->address = 'order@webike.tw';
            $this->sender->name = 'Webike Order-吳筱玲';
            $this->subject = str_replace('{{$estimate_code}}', $groupbuy->estimate_code , $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }

    public function order($order)
    {

        $order = \Everglory\Models\Order::onWriteConnection()->with(['items'=>function($query){$query->useWritePdo();} , 'items.details'=>function($query){$query->useWritePdo();} ,'customer', 'address'=>function($query){$query->useWritePdo();}])->find($order->id);



        $this->bcc = false;
        $data['order'] = $order;
        $this->data = $data;
        $this->recipient->address = $order->customer->email;
        $this->recipient->name = $order->customer->realname;

        //choice template
        $code = '';
        $code_1 = 1; // 1 is order
        if($order->payment_method == 4 or $order->payment_method == 5){ 
            $code_2 = 1;    // 4 & 5 are installment 
        }else{
            $code_2 = $order->payment_method; // 1:credit-card 2:cash-on-delivery 3:bank-transfer
        }
        $code_3 = '';
        switch ($order->status_id) {
            case '1':
                $code_3 = 1;
                $this->bcc = true;
                break;
            case '3':
                $code_3 = 2;
                $data['deadline'] =  \Sales\VirtualBank\Log::where('order_id',$order->id)->firstOrFail()->deadline;
                $this->data = $data;
                break;
            case '4':
                $code_3 = 3;
                break;
            case '5':
                $code_3 = 6;
                break;
            case '6':
                if($order->payment_method != 3){
                    $code_3 = 2;
                }
                break;
            case '0':
                $code_3 = 7;
                break;
            default:
                # code...
                break;
        }
        $code = $code_1 . $code_2 . $code_3;

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->sender->address = 'order@webike.tw';
            $this->sender->name = 'Webike Order-吳筱玲';
            $this->subject = str_replace('{{$increment_id}}', $order->increment_id, $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }


    }

    public function shipping($order)
    {
        $this->bcc = true;
        $data['order'] = $order;
        $this->data = $data;
        $this->recipient->address = $order->customer->email;
        $this->recipient->name = $order->customer->realname;

        $code = 311;

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->sender->address = 'support@webike.tw';
            $this->sender->name = 'Webike Support-楊千慧';
            $this->subject = str_replace('{{$increment_id}}', $order->increment_id, $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }

    }

    public function creditcard($cc,$order)
    {
        $this->bcc = false;
        $data['cc'] = $cc;
        $data['order'] = $order;
        $this->data = $data;

        $this->recipient->address = $order->customer->email;
        $this->recipient->name = $order->customer->realname;

        //choice template
        $code = 116;

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->subject = str_replace('{{$increment_id}}', $order->increment_id, $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }

    public function point($order)
    {
        $this->bcc = false;
        $data['order'] = $order;
        $point = \Sales\Reward::where('order_id',$order->id)->orderBy('created_at','desc')->where('points_current','<>',0)->first();
        if($point){
            $data['point'] = \Sales\Reward::where('order_id',$order->id)->orderBy('created_at','desc')->where('points_current','<>',0)->first();
            $this->data = $data;

            $this->recipient->address = $order->customer->email;
            $this->recipient->name = $order->customer->realname;

            //choice template
            $code = 241;

            $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
            if($mail_category){
                $this->subject = str_replace('{{$increment_id}}', $order->increment_id, $mail_category->title);
                $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
                $this->send();
            }
        }
    }

    public function afterSales($log)
    {
        $order = $log->order;
        if($order and $order->customer){

            $this->bcc = false;
            $customer = $order->customer;
            $data['order'] = $order;
            $data['customer'] = $customer;
            $data['open_image'] = 'http://www.webike.tw/top?log=true&target=aftersales&subject='.$log->id.'&user='.$order->customer->id;
            $this->data = $data;

            $this->recipient->address = $customer->email;
            $this->recipient->name = $customer->realname;

            //choice template
            $code = 182;

            $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
            if($mail_category){
                $this->subject = str_replace('{{$increment_id}}', $order->increment_id, $mail_category->title);
                $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
                $this->send();
            }
        }
    }

    public function newsletter($object,$email = null)
    {
        $this->bcc = false;

        if(get_class($object) == 'Everglory\Models\Email\Newsletter\Queue'){
            $queue = $object;
            $this->sender->address = 'mailmag@webike.tw';
            $this->sender->name = 'Webike 電子報';
            $this->subject = $queue->title;
            if($queue->send_method == 2) {
                $this->template = 'emails.mailmag.515';
            }else{
                $this->template = 'emails.mailmag.514';    
            }
            

            $items = $queue->items()->whereNull('sent_at')->take(200)->get();
            foreach ($items as &$item) {
                if($this->newsletter_send($queue,$item)){
                    $item->sent_at = date('Y-m-d H:i:s');
                    $item->save();
                }
            }

        }elseif(get_class($object) == 'Email\Newsletter\Item'){
            $item = $object;
            $queue = $item->queue;
            $this->sender->address = 'mailmag@webike.tw';
            $this->sender->name = 'Webike 電子報';
            $this->subject = $queue->newsletter->title;
            if($queue->send_method == 2) {
                $this->template = 'emails.mailmag.515';
            }else{
                $this->template = 'emails.mailmag.514';
            }
            

            if($this->newsletter_send($queue,$item)){
                $item->sent_at = date('Y-m-d H:i:s');
                $item->save();
            }
        }elseif(get_class($object) == 'Email\Newsletter' and $email){
            //test send
            $newsletter = $object;
            $data['content'] = $newsletter->content;

            $this->data = $data;

            $this->sender->address = 'mailmag@webike.tw';
            $this->sender->name = 'Webike 電子報';

            $this->recipient->address = $email;
            $this->recipient->name = $email;

            $this->subject = $newsletter->title;
            $this->template = 'emails.mailmag.514';
            $this->send();

        }


    }

    public function customerEmailVerify($customer)
    {
        $this->bcc = false;
        $data['uuid'] = $customer->email_validate_uuid;
        $data['id'] = $customer->id;
        $this->data = $data;
        $this->recipient->address = $customer->email;
        $this->recipient->name = $customer->realname;
        $code = '293';

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->subject = '【重要通知】Webike台灣 會員驗證信件';
            $this->template = 'emails.service.293';
            $this->send();
        }

    }

    private function newsletter_send($queue,$item)
    {
        $customer = $item->customer;
        if($customer){
            $this->recipient->address = $customer->email;
            $this->recipient->name = $customer->realname;
            if($queue->send_method == 2) {
                $data['open_image'] = 'http://www.webike.tw/top?log=true&subject='.$item->queue_id.'&user='.$customer->id;
            }else{
                $give_point_url = \URL::route('customer-newsletter-point',['user'=>$customer->id,'vol'=>$queue->id,'AID'=>md5('Webike'.$queue->newsletter_id.'newsletter')]);
                $give_point_url = str_replace('http://localhost','https://www.webike.tw',$give_point_url);
                $content = $queue->content_tracking;
                $open_image = 'http://www.webike.tw/top?log=true&subject='.$item->queue_id.'&user='.$customer->id;
                
                if(!$content){
                    $content = $queue->content;
                    $data['content'] = str_replace('{{$give_point_url}}', $give_point_url, $content );
                    $data['content'] = str_replace('{{$open_image}}', $open_image, $data['content']);
                }else{
                    $data['content'] = str_replace('%7B%7B%24give_point_url%7D%7D', $give_point_url, $content );
                    $data['content'] = str_replace('%7B%7B%24open_image%7D%7D', $open_image, $data['content']);
                }
            }
            $data['customer'] = $customer;
            $this->data = $data;
            $this->send();
            return true;
        }
        return false;

    }

    public function motomarket($mailType,$bike)
    {
        if( gettype($bike) == 'array' ){
            $bike = (object)$bike;
        }

        $this->bcc = false;
        $data['bike'] = $bike;
        $this->data = $data;

        $this->recipient->address = $bike->customer_mail;

        if($bike->type == 'submit'){
            $code = '283';
            $this->recipient->name = $bike->seller;
        }elseif( $bike->type == 'AutoFollow' ){
            $code = '285';
            $this->recipient->name = $bike->customer_name;
        }else{
            $code = '284';
            $this->recipient->name = $bike->asker;
        }

        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            // $this->sender->address = 'support@webike.tw';
            // $this->sender->name = 'Webike Support-楊千慧';
            $this->subject = str_replace('{{$product_name}}', $bike->product_name, $mail_category->title);
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }

    }

    public function coupon($coupon,$customer)
    {
        $this->bcc = false;
        $data['coupon'] = $coupon;
        $data['customer'] = $customer;
        $this->data = $data;
        $this->recipient->address = $customer->email;
        $this->recipient->name = $customer->realname;
        $code = $coupon->mail_template_code;
    
        $mail_category = \DB::connection('ec_dbs')->table('mail_category')->where( 'code', $code  )->first();
        if($mail_category){
            $this->subject = $mail_category->title;
            $this->template = 'emails.' . $mail_category->attribute . '.' . $code ;
            $this->send();
        }
    }
}