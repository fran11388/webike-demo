<?php namespace Ecommerce\Core\Validate\Email\Flow;
use Ecommerce\Core\Validate\ValidateFlow;
use Ecommerce\Service\AccountService;
use Everglory\Constants\CustomerEmailValidate;

/**
 * Created by PhpStorm.
 * User: EG-PC-004
 * Date: 2019/5/15
 * Time: 下午 04:24
 */

class Customer extends ValidateFlow {

    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function success()
    {
        if(!$this->model->email_validate){
            $customer_service = new AccountService;
            $customer_service->InitialAccount($this->model);


            $this->model->email_validate = CustomerEmailValidate::VALIDATE_SUCCESS_EMAIL;
            $this->model->email_validate_at = date('Y-m-d H:i:s');
            $this->model->save();
        }

        \Auth::login($this->model,true);
    }
}