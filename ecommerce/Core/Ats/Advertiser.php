<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Core\Ats;



class Advertiser
{

    private $initial = false;
    private $phpAds_context = [];
    private $is_not_phone = false;

    public function __construct()
    {
        if(str_contains(request()->getHost(),"amp")){
            return false;
        }

        $testConnection = testServerConnection('tracker');
        if($testConnection->success){
            if(@include_once(ATS_MAX_PATH . '/www/execute/alocal.php')) {
                $this->initial = true;
            }
        }

        date_default_timezone_set("Asia/Taipei");

        if(\Ecommerce\Core\Agent::isNotPhone()){
            $this->is_not_phone = true;
        }
    }

    public function call($zone_id,$block = true,$lazy_load = true)
    {
        $html = '';
        if($this->initial){
            $phpAds_context = $this->phpAds_context;
            sort($phpAds_context);

            $phpAds_raw = view_local('', $zone_id, 0, 0, '', '', '0', $phpAds_context, '');
            if($block){
                if(isset($phpAds_raw['context'])){
                    foreach ($phpAds_raw['context'] as $banner_array){
                        foreach ($banner_array as $banner_id_text){
                            //for multi
                            $banner_detail = explode(':',$banner_id_text);
                            //double prevent
                            $this->phpAds_context[$banner_detail[1]] = array('!=' => $banner_id_text);
                        }
                    }
                    $this->phpAds_context[$phpAds_raw['bannerid']] = array('!=' => 'bannerid:'.$phpAds_raw['bannerid']);
                }
            }

            if(isset($phpAds_raw['html'])){
                $html = $phpAds_raw['html'];
            }


            $doc = new \DOMDocument();
            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
            $final_images = [];
            $images = $doc->getElementsByTagName('img');
            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(strpos($old_src,'https://www.webike.tw/gallery/www/images') !== false){
                    if(!$this->is_not_phone) {
                        $new_src_url = ThumborBuildWithCdn($old_src,400, 123);
                        $image->setAttribute('src',$new_src_url);
                    }else{
                        $new_src_url = ThumborBuildWithCdn($old_src,985, 303);
//                        $new_src_url = str_replace('http:', '', \Thumbor\Url\Builder::construct(config('phumbor.server'), config('phumbor.secret'), unicodeConvertForSpace($old_src))->fitIn(400, 123)->build());
                        $image->setAttribute('src',$new_src_url);
                    }
                    if($lazy_load){
                        $image->setAttribute('src', 'https://img.webike.net/sys_images/bg.png');
                        $image->setAttribute('data-src',$new_src_url);
                    }
                }
                $final_images[] = $image;
            }

            foreach ($final_images as $final_image){
                if(strpos($final_image->getAttribute('src'),'ace.php') !== false){
                    $final_image->parentNode->removeChild($final_image);
                }
            }
            $html = $doc->saveHTML();

        }
        date_default_timezone_set("Asia/Taipei");
        return $html;
    }

    public function callTest($zone_id,$block = true,$lazy_load = true)
    {
        $html = '';
        if($this->initial){
            $phpAds_context = $this->phpAds_context;
            sort($phpAds_context);

            $phpAds_raw = view_local('', $zone_id, 0, 0, '', '', '0', $phpAds_context, '');
            if($block){
                if(isset($phpAds_raw['context'])){
                    foreach ($phpAds_raw['context'] as $banner_array){
                        foreach ($banner_array as $banner_id_text){
                            //for multi
                            $banner_detail = explode(':',$banner_id_text);
                            //double prevent
                            $this->phpAds_context[$banner_detail[1]] = array('!=' => $banner_id_text);
                        }
                    }
                    $this->phpAds_context[$phpAds_raw['bannerid']] = array('!=' => 'bannerid:'.$phpAds_raw['bannerid']);
                }
            }

            if(isset($phpAds_raw['html'])){
                $html = $phpAds_raw['html'];
            }

            $doc = new \DOMDocument();
            $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
            $final_images = [];
            $images = $doc->getElementsByTagName('img');
            foreach ($images as $image) {
                $old_src = $image->getAttribute('src');
                if(strpos($old_src,'https://www.webike.tw/gallery/www/images') !== false){
                    if(!$this->is_not_phone) {
                        $new_src_url = ThumborBuildWithCdn($old_src,400, 123);
                        $image->setAttribute('src',$new_src_url);
                    }else{
                        $new_src_url = ThumborBuildWithCdn($old_src,985, 303);
//                        $new_src_url = str_replace('http:', '', \Thumbor\Url\Builder::construct(config('phumbor.server'), config('phumbor.secret'), unicodeConvertForSpace($old_src))->fitIn(400, 123)->build());
                        $image->setAttribute('src',$new_src_url);
                    }
                    if($lazy_load){
                        $image->setAttribute('src', 'https://img.webike.net/sys_images/bg.png');
                        $image->setAttribute('data-src',$new_src_url);
                    }
                }
                $final_images[] = $image;
            }

            foreach ($final_images as $final_image){
                if(strpos($final_image->getAttribute('src'),'ace.php') !== false){
                    $final_image->parentNode->removeChild($final_image);
                }
            }
            $html = $doc->saveHTML();

        }
        date_default_timezone_set("Asia/Taipei");
        return $html;
    }
}