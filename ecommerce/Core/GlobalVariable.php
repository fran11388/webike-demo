<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/8/1
 * Time: 下午 10:22
 */

namespace Ecommerce\Core;


class GlobalVariable
{
    static $variable = [];

    public static function add( $type , $key , $value ){
        self::$variable[$type][$key] = $value;
    }

    public static function get( $type , $key ){
        if (
            array_key_exists($type , self::$variable)
            &&
            array_key_exists($key , self::$variable[$type])
        ){
            return self::$variable[$type][$key];
        }
        return false;

    }
}