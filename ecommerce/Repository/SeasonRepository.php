<?php
/**
 * Created by PhpStorm.
 * User: EG_EC
 * Date: 2018/10/23
 * Time: 下午 02:32
 */

namespace Ecommerce\Repository;


use Everglory\Models\Season\Item;

class SeasonRepository extends BaseRepository
{

    public static function allOfTheSeason($season, $quantity = null, $relations = null)
    {
        $queryBuilder = Item::where('season', $season);
        if($relations){
            $queryBuilder = Item::with($relations);
        }
        if($quantity){
            $queryBuilder = $queryBuilder->take($quantity);
        }
        return $queryBuilder->get();
    }
}