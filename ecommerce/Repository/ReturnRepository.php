<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Repository;


use Everglory\Models\Category;
use Everglory\Models\Order\Item;
use Everglory\Models\Review;
use Everglory\Models\ReviewComment;
use Ecommerce\Service\Search\ConvertService;
use Everglory\Models\Rex\Type;

class ReturnRepository extends BaseRepository
{
    const CACHE_LIFETIME = 30;

    /**
     * @param bool $useCache
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getType($useCache = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Type::all();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

}