<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;


class BaseRepository
{
    protected $queryBuild;
    protected $relation = [];

    const CACHE_LIFETIME = 60;

    /**
     * @param bool|string $args
     * @return string
     */
    static protected function getCacheKey($args = true )
    {
        $trace = debug_backtrace();
        $class = $trace[1]['class'];
        $function = $trace[1]['function'];
        $key = $class . '@'.  $function;
        if ($args === true ){
            $args = serialize($trace[1]['args']);
            $key .= '?' . base64_encode($args);
        } else {
            $key .= '?' . $args;
        }
        return $key;
    }

    public function firstWithRelation()
    {
        return $this->queryBuild->with($this->relation)->first();
    }

    public function continueQuery()
    {
        return $this->queryBuild;
    }

    public function getData()
    {
        return $this->queryBuild->get();
    }
}