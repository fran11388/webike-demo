<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;

use Everglory\Models\Manufacturer;
use Everglory\Models\Product;

class ManufacturerRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @param $value
     * @param string $column
     * @return Manufacturer|null
     */
    static public function find($value, $column = 'url_rewrite')
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Manufacturer::where('active', '1');
        if(is_array($value)){
            $result = $result->whereIn($column ,$value)->get();
        }else{
            $result = $result->where($column , '=' , $value)->first();
        }


        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }

    static public function all($active = 1)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Manufacturer::where('active', $active)->get();

        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }

    public static function getManufacturerWithRelations($url_rewrite, $relations = [])
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $manufacturers = Manufacturer::with($relations)->where('active', 1);

        if(is_array($url_rewrite)){
            $manufacturers = $manufacturers->whereIn('url_rewrite', $url_rewrite)->get();
        }else{
            $manufacturers = $manufacturers->where('url_rewrite', $url_rewrite)->firstOrFail();
        }

        \Cache::put($cache_key, $manufacturers, 5);

        return $manufacturers;
    }

    /**
     * Get brands of new products.
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getNewProductsBrands($limit = null)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $subQuery = 'SELECT manufacturer_id FROM eg_product.products WHERE active = 1 AND is_new = 1 AND is_main = 1 GROUP BY relation_product_id';


        $result = Manufacturer::select(\DB::raw('manufacturers.*'),\DB::raw('count(0) as products_count'))
            ->join(\DB::raw('(' . $subQuery . ') as pd'), function ($join) {
                $join->on('manufacturers.id', 'pd.manufacturer_id');
            })
            ->groupBy('pd.manufacturer_id')->orderBy('products_count','desc')->take($limit)->get();
        
        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }

        return $result;
    }

    public static function getManufacturerCount()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = 0;
        $manufacturer = Manufacturer::select(\DB::raw('count(0) AS count'))
            ->where('active', 1)
            ->first();
        if($manufacturer){
            $result = $manufacturer->count;
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getMaxPriceCompareWithLastMonth($url_rewrite)
    {
        $url_field = implode($url_rewrite,',');
        $url_array = '('.implode($url_rewrite,',').')';
        $result = \DB::connection('eg_product')->select('SELECT p.rewrite,max(dif.price_final-cpp.price) as price 
            FROM (
                SELECT pro.*, field(m.url_rewrite,'.$url_field.') AS sort,m.url_rewrite as rewrite FROM products AS pro
                INNER JOIN manufacturers AS m
                WHERE m.url_rewrite IN '. $url_array .'
                AND pro.active = 1
                AND pro.is_main = 1
                AND pro.type = 0
                AND pro.parent_id = 0
                AND m.active = 1
            ) as p
            join product_price_difference as dif
            on p.id = dif.product_id and dif.customer_group_id = 2
            join product_prices as cpp
            on p.id = cpp.product_id and cpp.role_id = 2 and cpp.rule_date = CURDATE()
            group by p.rewrite order by p.sort');
        dd($result);
        
    }
}