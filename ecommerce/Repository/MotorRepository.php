<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;

use Everglory\Models\Motor;
use Everglory\Models\MotorManufacturer;
use Everglory\Models\MotoMarket\MotorModel;
use Everglory\Models\MotoMarket\Product as MotorProduct;
use Illuminate\Support\Facades\DB;
use Ecommerce\Support\DisplacementConverter;
use Log;

/**
 * Class MotorRepository
 * @package Ecommerce\Repository
 */
class MotorRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * Get all motor's manufacturer
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function selectAllManufacturer(){
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = MotorManufacturer::whereIn('id',Motor::select(['manufacturer_id'])->groupBy('manufacturer_id')->get())->where('active',1)->orderBy('sort')->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    /**
     * Get motor's manufacturer by specific url_rewrite from database.
     *
     * @param string | array $url_rewrite search condition
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null|static|static[]
     */
    public static function selectManufacturersByUrlRewrite($url_rewrite){
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        if(is_array($url_rewrite)){
            $result = MotorManufacturer::whereIn('url_rewrite', $url_rewrite)->orderBy('sort')->get();
        }else{
            $result = MotorManufacturer::where('url_rewrite', $url_rewrite)->first();
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function selectProductMotors($manufacturer, $category)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $motors = Motor::
        join( 'product_motor' , 'motors.id' , '=' , 'product_motor.motor_id')
            ->join( 'products' ,'products.id' , '=','product_motor.product_id')
            ->join( 'motor_manufacturers' , 'motors.manufacturer_id' ,'=' ,'motor_manufacturers.id')

            ->where( 'products.parent_id' , '=' ,'0' )
            ->where('products.type',0)->where('products.is_main',1)->where('products.active',1)
            ->groupBy('motors.name')
            ->orderByRaw( "CASE WHEN motor_manufacturers.name ='HONDA' THEN 0 WHEN motor_manufacturers.name ='YAMAHA' THEN 1 WHEN motor_manufacturers.name ='SUZUKI' THEN 2 WHEN motor_manufacturers.name ='KAWASAKI' THEN 3 ELSE 4 + motor_manufacturers.name END ASC , motor_manufacturers.name,motors.displacement" )
            ->select('motors.id','motors.name','motor_manufacturers.name as m_name','motors.url_rewrite' , 'motor_manufacturers.image');

        if ($manufacturer){
            $motors = $motors->where( 'products.manufacturer_id', '=' , $manufacturer->id );
        }

        if ($category) {
            $motors = $motors
                ->join('category_product' , 'products.id', '=' , 'category_product.product_id')
                ->where( 'category_product.category_id' , $category->id );
        }

        $result = $motors->get();
        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }


    /**
     * get Motor's manufacturer and specifications data from database by specify column.
     *
     * @param string|array $value search condition
     * @param string $column search column
     * @return Motor|null
     */
    static public function find($value, $column = 'url_rewrite')
    {

        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Motor::with('specifications' , 'manufacturer');

        if(is_array($value)){
            $result = $result->whereIn($column, $value)->get();
        }else{
            $result = $result->where($column, $value)->first();
        }

            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }

    /**
     * get motors by displacement and manufacturer
     *
     * @param string $url_rewrite Motor Manufacturer's url_rewrite
     * @param int $displacement Motor's displacement
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getMotorsByDisplacementAndManufacturer($url_rewrite, $displacement)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $displacementMotors = MotorManufacturer::
        select('*','motors.url_rewrite as motorurl_rewrite','motor_manufacturers.url_rewrite as faurl_rewrite','motor_manufacturers.name as manufacturer','motors.name as name')
        ->join('motors','motor_manufacturers.id','=','motors.manufacturer_id')
        ->where('motor_manufacturers.url_rewrite',$url_rewrite)
        ->Orderby('displacement','asc')
        ->Orderby('motors.name','asc');
        
        switch ($displacement) {
            case 50:
                $displacementMotors->where('motors.displacement','<=',50);
                break;
            case 125:
                $displacementMotors->whereBetween('motors.displacement',[51,125]);
                break;
            case 250:
                $displacementMotors->whereBetween('motors.displacement',[126,250]);
                break;
            case 400:
                $displacementMotors->whereBetween('motors.displacement',[251,400]);
                break;
            case 750:
                $displacementMotors->whereBetween('motors.displacement',[401,750]);
                break;
            case 1000:
                $displacementMotors->whereBetween('motors.displacement',[751,1000]);
                break;
            case 1001:
                $displacementMotors->where('motors.displacement','>',1000);
                break;
            case '00':
                break;
        }

        $result = $displacementMotors->get();
        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        
        return $result;
    }

    /**
     * Get total number of motors on all manufacturers
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function MotorCount()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = MotorManufacturer::
        select(DB::raw('count(*) as count,motor_manufacturers.name,motor_manufacturers.country,motor_manufacturers.image,motor_manufacturers.country_code'))
        ->join('motors','motor_manufacturers.id','=','motors.manufacturer_id')
        ->groupBy('motor_manufacturers.id')->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        
        return $result;
    }

    public static function getNewMotorsByManufacturer($url_rewrite)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $testConnection = testServerConnection('motomarket');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $newmotors = MotorProduct::
        join('motor_model','motor_model.id','=','products.motor_model_id')
        ->orderBy('motor_model.created_at','desc')
        ->where('motor_model.manufacturer',$url_rewrite)
        ->where('status', 1)
        ->take(6);
        // dd($newmotors);
        $result = $newmotors->get();


        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        
        return $result;
    }

    public static function getNewMotors($motor_id = NULL, $limit = NULL)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $testConnection = testServerConnection('motomarket');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $newmotors = MotorProduct::with('motorModel')
            ->orderBy('products.created_at','desc')
            ->where('status', 1);

        if(is_array($motor_id)){
            $newmotors = $newmotors->whereIn('products.motor_model_id', $motor_id);
        }elseif($motor_id){
            $newmotors = $newmotors->where('products.motor_model_id', $motor_id);
        }
        if($limit){
            $newmotors = $newmotors->take($limit);
        }

        $result = $newmotors->get();


        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }

        return $result;
    }

    public static function getNewestMotorsUseGroup($limit = NULL, $group = null, $exclude = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $testConnection = testServerConnection('motomarket');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $newmotors = MotorProduct::with('motorModel','customer.profiles')
            ->orderBy('products.created_at','desc')
            ->where('status', 1);

        if($limit){
            $newmotors = $newmotors->take($limit);
        }

        if($group){
            $newmotors = $newmotors->groupBy($group);
        }

        if($exclude){
            $newmotors = $newmotors->whereNotIn('id', $exclude);
        }

        $result = $newmotors->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }

        return $result;
    }

    public static function getMotor($segments)
    {
        $motor = Motor::where('url_rewrite',$segments)->first();

        return $motor;       
    }

    public static function getMotorManufacturer($segments)
    {
        $getmanufacturer = Motor::with('manufacturer')->where('url_rewrite',$segments)->first();
        $manufacturer = $getmanufacturer->manufacturer->name;
        
        return $manufacturer;       
    }

    public static function getMotorManufacturers($url_rewrite = null)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $manufacturers = MotorManufacturer::where('active', 1);
        if(!$url_rewrite) {
            $result = $manufacturers->get();
        }else if(is_array($url_rewrite)){
            $result = $manufacturers->whereIn('url_rewrite', $url_rewrite)->get();
        }else{
            $result = $manufacturers->where('url_rewrite', $url_rewrite)->get();
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    /**
     * Get Popular Motors.
     *
     * @param int $limit
     * @param $motor
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getPopularMotors($limit = 10, $motor = null)
    {
        $popular_motors = Motor::where('name','not like','%其他%')->where('displacement','<>',0)->orderBy('visits','desc')->take($limit);
        if($motor){
            $disp = DisplacementConverter::find($motor->displacement);
            $popular_motors = $popular_motors->whereBetween('displacement',[$disp->min,$disp->max]);
        }
        return $popular_motors->get();
    }
}