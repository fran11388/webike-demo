<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Repository;


use Everglory\Models\Category;
use Everglory\Models\Order\Item;
use Everglory\Models\Review;
use Everglory\Models\ReviewComment;
use Ecommerce\Service\Search\ConvertService;

class ReviewRepository extends BaseRepository
{
    const CACHE_LIFETIME = 30;


    /**
     * @param Category $category
     * @param Manufacturer $manufacturer
     * @return $this|\Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getAvgRanking($category , $manufacturer ,$motor = null){


        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return ConvertService::converRankingAvgToObject($result);
        }

        $result = Review::groupBy('ranking')->where('view_status',1);

        if ($category)
            $result = $result->where('product_category_search' , 'like' , $category->mptt->url_path . '%');

        if ($manufacturer)
            $result = $result->where('product_manufacturer_id' , $manufacturer->id);

        if($motor){
            $result = $result->whereHas('motors', function ($query) use($motor) {
                $query->where('motor_id', '='  , $motor->id);
            });
        }

        $result = $result->select('ranking' , \DB::raw('count(*) as total'))->get();

        if ( count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return ConvertService::converRankingAvgToObject($result);
    }

    /**
     * Get review content by specific condition.
     *
     * @param $category
     * @param $manufacturer
     * @param $motor
     * @param $limit
     * @return $this|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|mixed|static|static[]
     */
    public static function get($category , $manufacturer , $motor , $limit){
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Review::with('customer')->orderBy('created_at','desc')->where("view_status", 1);
        if( $motor ){
            $result = $result->whereHas('motors', function ($query) use($motor) {
                $query->where('motor_id', '='  , $motor->id);
            });
        }
        if( $category ){
            if( $category->n_left+1 == $category->n_right ){
                $result = $result->where( 'product_category', $category->id );
            }else{
                $ca_children = Category::where('n_left', '>=', $category->n_left )->where('n_right', '<=', $category->n_right )->get()->pluck('id')->all();
                $result = $result->whereIn( 'product_category', $ca_children );
            }
        }
        if( $manufacturer ){
            $result = $result->where( 'product_manufacturer_id', $manufacturer->id );
        }
        $result = $result->take($limit)->get();

        if ( count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }


    public static function getNewReviews( $limit = 10){
        return Review::with('product','product.manufacturer' , 'customer'  , 'comments.customer')->where('view_status',1)->orderBy('created_at','desc')->take($limit)->get();
    }

    public static function find( $id ){
        return Review::with('product', 'product.manufacturer' , 'customer'  , 'comments.customer' ,'product.categories', 'motors', 'motors.motorModel', 'motors.motorModel.manufacturer')->where('view_status', 1)->where('id', $id)->first();
    }

    public static function getCategoryReviewCount( $searchKey ){

        $category = CategoryRepository::find($searchKey);
        $count = Review::where('view_status',1)->where('product_category_search','like', $searchKey. '%')->count();

        $reviews = Review::where('view_status',1)->where('product_category_search','like', $searchKey . '%')
            ->join('eg_product.mptt_categories_flat as mptt','reviews.product_category_search','=','mptt.url_path')
            ->join('eg_product.mptt_categories_flat as parent', \DB::raw('SUBSTRING_INDEX( reviews.product_category_search, "-", 2)'), '=', 'parent.url_path')
            ->select(\DB::Raw('count(*) AS count, SUBSTRING_INDEX(product_category_search,"-",2) AS category_path, parent.url_rewrite as url_rewrite, parent.name AS parent_name, mptt.sort'))
            ->groupBy(\DB::Raw('SUBSTRING_INDEX(product_category_search,"-",2)'))
            ->orderby('parent.sort')
            ->get();


        $object = new \stdClass();
        $object->count = $count;
        $object->reviews = $reviews;
        $object->category = $category;
        return $object;

    }

    public static function getGenuinePartsReviewCount($searchKey = 9000){

        $category = CategoryRepository::find($searchKey);
        $count = Review::where('view_status',1)->where('product_category_search','like', $searchKey . '%')->count();
        $reviews = Review::where('view_status',1)->where('product_category_search','like', $searchKey .'%')
            ->where('product_manufacturer','<>','')
            ->select(\DB::Raw('count(*) AS count, product_manufacturer , product_category_search as url_rewrite'))
            ->groupBy('product_manufacturer')
            ->orderby('count','desc')
            ->get();

        $object = new \stdClass();
        $object->count = $count;
        $object->reviews = $reviews;
        $object->category = $category;

        return $object;

    }

    public static function getNewReviewComment( $limit = 10){
        return ReviewComment::with('review' , 'review.product' , 'review.customer')->orderBy('created_at','desc')->take($limit)->get();
    }

    public static function search( $url_path ,$motor){
        $result = new \stdClass();
        $reviews = Review::where('view_status',1)->whereNotNull('product_category_search')->with('product','product.manufacturer' , 'customer' );
        if(request()->get('sort') == 'visits'){
            $reviews = $reviews->orderBy('ranking','desc');
        }else{
            $reviews = $reviews->orderBy('created_at','desc');
        }
        $url_level = 1;
        if($url_path){
            $reviews = $reviews->where('product_category_search','like','%' . $url_path . '%');
            $url_level = count(explode('-',$url_path));
            $url_level++;
        }



        if($q = TransferToUtf8(request()->get('q'))){
            $querys = explode('%',$q);
            foreach($querys as $query){
                $reviews = $reviews->where(\DB::Raw('CONCAT(nick_name,title,content,product_name,product_manufacturer,motor_name,motor_manufacturer)'),'like','%' . $query . '%');
            }
        }

        // Brand AND Motor Frontend filter
        if(request()->has('br')){
            $reviews = $reviews->where('product_manufacturer',request()->get('br'));
        }

        if(request()->has('ranking')){
            $reviews = $reviews->where('ranking',request()->get('ranking'));
        }


        if($motor){
//            $reviews = $reviews->where( 'motor_id', $motor->id );
            $reviews = $reviews->whereHas('motors', function ($query) use($motor) {
                $query->where('motor_id', '='  , $motor->id);
            });

        }

        if(request()->has('sku')){
            $reviews = $reviews->where('product_sku',request()->get('sku'));
        }
        $trees = [];
        $tree = clone $reviews;
        $manufacturers = clone $reviews;
        $ranking = clone $reviews;
        $result->reviews = $reviews->paginate(10);
        $tree = $tree->join('eg_product.mptt_categories_flat AS mptt','product_category_search','=','mptt.url_path')
            ->select('product_category_search','mptt.*')
            ->orderBy('mptt.sort')
            ->get()->groupBy(function ($item, $key) use($url_level){
            return substring_index($item->product_category_search,'-',$url_level);
        });

        foreach ($tree as $category_path => $categories){
            $object = new \stdClass();
            $object->count = count($categories);
            $object->url_path = $category_path;
            $path_array = explode('|',$categories->first()->name_path);

            if(!isset($path_array[$url_level - 1])){
                break;
            }else{
                $object->caregory_name = $path_array[$url_level-1];
            }

            $categories = $categories->groupBy(function ($item, $key) use($url_level){
                return substring_index($item->product_category_search,'-',$url_level+1);
            });
            $categories->forget($category_path);
            $object->subcategory = collect();
            foreach ($categories as $sub_category_path => $category){
                $sub_object = new \stdClass();
                $sub_object->count = count($category);
                $sub_object->url_path = $sub_category_path;
                $sub_object->caregory_name = explode('|',$category->first()->name_path)[$url_level];
                $object->subcategory[] = $sub_object;
            }
            $trees[] = $object;
        }

        $result->tree = collect($trees);
        $result->manufacturers = $manufacturers->groupBy('product_manufacturer')->select(\DB::Raw('product_manufacturer as name,count(0) as count'))->orderBy('count','desc')->get();
        $result->rankings = $ranking->groupBy('ranking')->select(\DB::Raw('ranking,count(0) as count'))->orderBy('ranking','desc')->get();

//        $substring_query = "SUBSTRING_INDEX(product_category_search,'-',".$url_level.") ";
//        $tree = $tree->groupBy(\DB::Raw($substring_query))
//            ->select(\DB::Raw("count(0) as subCount , ".$substring_query." as sub_category ,mptt.name_path"))
//            ->join('eg_product.mptt_categories_flat AS mptt',\DB::Raw($substring_query),'=','mptt.url_path')
//            ->where('mptt.active',1)
//            ->get();
//        dd($tree);

//        $result->tree =
        
        return $result;


    }

    public static function searchByProductId( $id , $limit = 20 ){
        return Review::where('product_id' , $id )
            ->with('customer')
            ->whereNull('deleted_at')
            ->where('view_status','=',1)
            ->take($limit)->get();
    }

    public static function getCustomerNotWriteReviewCount( $customer ){
        $count = 0;
        if (!$customer){
            return $count;
        }
        $items = Item::with('product', 'product.manufacturer', 'product.reviews')
            ->where('customer_id', $customer->id)
            ->whereHas('order', function($query){
                $query->where('status_id', 10);
            })
            ->groupBy('product_id')
            ->get();
        foreach ($items as $key => $item) {
            $product = $item->product;
            if( $product ){
                $mainProduct = $product;
                if( $product->mainProduct ){
                    $mainProduct = $product->mainProduct;
                }

                $reviews = $mainProduct->reviews;
                $reviews = $reviews->filter(function($review) use($customer){
                    if( $review->customer_id == $customer->id ){
                        return true;
                    }
                })->count();
                if( $reviews ){
                    $count++;
                }
            }
        }
        $count = $items->count() - $count;
        return $count;

    }

    public static function getCustomerOrderItemWriteReview( $customer , $not_writing_flg = false){
        $collection = [];
        if (!$customer){
            return collect($collection);
        }

        $service = new \Ecommerce\Service\OrderService;
        $items = Item::with(['details','mainProduct','order'])
            ->where('customer_id', $customer->id)
            ->whereNotNull('product_id')
//            ->whereHas('product', function($query){
//                $query->where('active', 1);
//            })
            ->whereHas('order', function($query){
                $query->where('status_id', 10);
            })->groupBy('main_product_id')
            ->orderBy('order_items.created_at','desc')
            ->get();
        $items->load('reviews');

        foreach ($items as $key => $item) {

//            $object = new \stdClass();
            $reviews = $item->reviews;
            if($not_writing_flg and count($reviews)){
                // not calculate
            }else{
                $itemDetail = $service->getItemDetail($item);
                $item->product_name = $itemDetail->product_name;
//                $object->sku = $item->sku;
                $item->manufacturer_name = $itemDetail->manufacturer_name;
                $item->manufacturer_url_rewrite = $itemDetail->manufacturer_url_rewrite;
                $item->reveiws = $reviews;
                $collection[] = $item;
            }
        }
        return collect($collection);
    }

    public static function getMostCommentReviews($during = 180 ,$limit = 6)
    {
        return Review::with(['comments'])->where('view_status',1)
            ->whereRaw('created_at > CURDATE() - INTERVAL ? DAY' , array($during))
            ->take($limit)
            ->get()->sortByDesc(function($review)
            {
                return $review->comments->count();
            });

    }

}