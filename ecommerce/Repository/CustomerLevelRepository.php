<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:25
 *
 */
namespace Ecommerce\Repository;

use Everglory\Constants\CustomerRole;
use Everglory\Models\Customer;
use Everglory\Models\Customer\Level;
use Everglory\Models\Order;
use Exception;

class CustomerLevelRepository extends BaseRepository
{
    const LEVEL = [
        1 => [
            'name' => 1,
            'performance' => ['min' => 0, 'max' => 29999],
            'rate' => 0.01,
            'display_name' => '標準會員',
            'css_class' => 'general'
        ],
        2 => [
            'name' => 2,
            'performance' => ['min' => 30000, 'max' => 59999],
            'rate' => 0.015,
            'display_name' => '白銀會員',
            'css_class' => 'sliver',
            'cart_text' => '※白銀會員，優惠運費'
        ],
        3 => [
            'name' => 3,
            'performance' => ['min' => 60000, 'max' => 9999999999],
            'rate' => 0.02,
            'display_name' => '黃金會員',
            'css_class' => 'golden',
            'cart_text' => '※黃金會員，優惠運費。'
        ],
    ];


    /**
     * 目前還差多少可以到下一個等級
     *
     * @param Customer $customer
     * @return int
     */
    public function toNextLevel(Customer $customer)
    {

        $_level = $this->getNextLevel($customer);

        if (!$_level) {
            return self::LEVEL[1]['performance']['max'];
        }

        foreach (self::LEVEL as $level) {
            if ($level['performance']['min'] > $_level->performance) {
                return $level['performance']['min'] - $_level->performance;
            }
        }

        // null is top level

        return null;
    }


    /**
     * 取得當日使用的 level 資料
     * 每月10日前，取 month - 2 的資料
     * 否則取 month -1 的資料
     *
     * @param Customer $customer
     * @return Level | null
     */
    public function getCurrentLevel(Customer $customer)
    {
        $days = date("j");
        $interval = -1;
        if ($days < 10)
            $interval = -2;

        return $this->getLevel($customer, $this->getLastYearMonth($interval));

    }


    /**
     * 取得下個月使用的 level 資料
     * 每月10日前，取 month - 1 的資料
     * 否則取 month -0 的資料
     *
     * @param Customer $customer
     * @return Level | null
     */
    public function getNextLevel(Customer $customer)
    {
        $days = date("j");
        $interval = 0;
        if ($days < 10)
            $interval = -1;

        return $this->getLevel($customer, $this->getLastYearMonth($interval));

    }

    /**
     * 當賣上更新時，呼叫該 function 來更新特定使用者該月份的 Level 資料
     * 用來提示目前還差多少可以到下一個等級
     *
     * @param Customer $customer
     * @throws Exception
     */
    public function updateSpecificCustomerInCurrentMonth(Customer $customer)
    {
        if ($customer->role->id != CustomerRole::WHOLESALE) {
            throw new Exception('Customer Level only for WHOLESALE');
        }
        $year_month = $this->getCurrentYearMonth();
        $days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($year_month . '01')), date('Y', strtotime($year_month . '01')));
        $result = Order::onWriteConnection()->join('eg_accounting.accounting_selled_queues', 'accounting_selled_queues.order_id', 'orders.id')
            ->where('orders.status_id', '<>', 0)
            ->where('orders.payment_method', '<>', 6)
            ->where('orders.customer_id', $customer->id)
            ->whereBetween('shipping_date',
                [
                    date('Y-m-d', strtotime($year_month . '01')),
                    date('Y-m-d', strtotime($year_month . $days)),
                ])
            ->groupBy('orders.customer_id')
            ->select(\DB::raw(" orders.customer_id as customer_id , sum(subtotal) as performance , '{$year_month}' as 'year_month'"))
            ->get()
            ->filter(function ($entity) {
                return $entity->performance > 0;
            });

        \DB::transaction(function () use ($year_month, $result, $customer) {
            if (!count($result)) {
                Level::whereCustomerId($customer->id)
                    ->whereYearMonth($year_month)
                    ->delete();
            }
            foreach ($result as &$entity) {
                foreach (self::LEVEL as $level) {
                    if ($level['performance']['min'] <= $entity->performance and $level['performance']['max'] >= $entity->performance) {
                        $entity->level = $level['name'];
                        $entity->rate = $level['rate'];
                        $_level = Level::whereCustomerId($entity->customer_id)
                            ->whereYearMonth($entity->year_month)
                            ->first();
                        if ($_level) {
                            $_level = new Level();
                        }

                        $_level->customer_id = $entity->customer_id;
                        $_level->year_month = $entity->year_month;
                        $_level->performance = $entity->performance;
                        $_level->level = $entity->level;
                        $_level->rate = $entity->rate;
                        $_level->save();

                        break;
                    }
                }
            }
        });
    }


    /**
     * 計算並更新(optional)特定月份(optional)的 Level 資料
     *
     * @param bool $update
     * @param string $year_month
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function calculate($update = false, $year_month = "")
    {
        if (!$year_month) {
            $days = date("j");
            $interval = 0;
            if ($days < 10){
                $interval = -1;
            }
            $year_month = $this->getLastYearMonth($interval);
        }
        $days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($year_month . '01')), date('Y', strtotime($year_month . '01')));

        $result = Order::onWriteConnection()->join('customers', 'orders.customer_id', 'customers.id')
            ->join('eg_accounting.accounting_selled_queues', 'accounting_selled_queues.order_id', 'orders.id')
            ->where('orders.status_id', '<>', 0)
            ->where('orders.payment_method', '<>', 6)
            ->where('customers.role_id', CustomerRole::WHOLESALE)
            ->whereBetween('shipping_date',
                [
                    date('Y-m-d', strtotime($year_month . '01')),
                    date('Y-m-d', strtotime($year_month . $days)),
                ])
            ->groupBy('orders.customer_id')
            ->select(\DB::raw(" orders.customer_id as customer_id , sum(subtotal) as performance , '{$year_month}' as 'year_month'"))
            ->get()
            ->filter(function ($entity) {
                return $entity->performance > 0;
            });

        \DB::transaction(function () use ($update, $year_month, $result) {
            if ($update) {
                Level::where('year_month', $year_month)
                    ->delete();
            }
            foreach ($result as &$entity) {
                foreach (self::LEVEL as $level) {
                    if ($level['performance']['min'] <= $entity->performance and $level['performance']['max'] >= $entity->performance) {
                        $entity->level = $level['name'];
                        $entity->rate = $level['rate'];
                        if ($update) {
                            $_level = new Level();
                            $_level->customer_id = $entity->customer_id;
                            $_level->year_month = $entity->year_month;
                            $_level->performance = $entity->performance;
                            $_level->level = $entity->level;
                            $_level->rate = $entity->rate;
                            $_level->save();
                        }
                        break;
                    }
                }
            }
        });


        return $result;
    }

    private function getCurrentYearMonth()
    {
        return date("Ym");
    }

    private function getLastYearMonth($interval = -1)
    {
        $date = date("Y-m-d");
        $date = strtotime(date("Y-m-d", strtotime($date)) . " $interval month");
        return date("Ym", $date);
    }

    private function getLevel(Customer $customer, $year_month)
    {
        return Level::whereCustomerId($customer->id)
            ->whereYearMonth($year_month)
            ->first();
    }
}