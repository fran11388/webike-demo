<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;


use Everglory\Constants\ProductType;
use Everglory\Models\Mptt;
use Everglory\Models\Product;
use Carbon\Carbon;
use Everglory\Models\Review;
use Everglory\Models\Manufacturer;
use Everglory\Models\Stock;

class ProductRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;


    /**
     * @param $url_rewrite
     * @param bool $useCache
     * @return Product|\Illuminate\Database\Eloquent\Builder
     */
    public static function findDetail($url_rewrite, $useCache = false ,$column = 'url_rewrite' , $with_columns = ['manufacturer',
        'images',
        'prices',
        'points',
        'motors',
        'options',
        'stocks',
        'reviews',
        'preorderItem',
        'attrString',
        'categories',
        'categories.mptt',
        'motors.manufacturer',
        'productDescription',
        'descriptionOrig',
        'fitModels',
        'fitModels.motorManufacturerByName',
        'outlet'] )
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::tags(['product'])->get($cache_key)) {
            return $result;
        }
        $product = Product::with($with_columns);

        if(is_array($url_rewrite)){
            $product = $product->whereIn($column, $url_rewrite)->get();
        }else{
            $product = $product->where($column, $url_rewrite)->firstOrFail();
        }

        $product->load('groups');

        \Cache::put($cache_key, $product, 5);

        return $product;
    }

    public static function getProductWithRelations($url_rewrite, $relations, $useCache = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $product = Product::with($relations);

        if(is_array($url_rewrite)){
            $product = $product->whereIn('url_rewrite', $url_rewrite)->get();
        }else{
            $product = $product->where('url_rewrite', $url_rewrite)->firstOrFail();
        }

        \Cache::put($cache_key, $product, 5);

        return $product;
    }

    public static function findEstimateDetailUseId($product_id, $useCache = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $product = Product::with([
            'manufacturer',
            'images',
            'prices',
            'optionFlat',
            'productDescription',
            'motors',
            'fitModels',
            'fitModels.motorManufacturerByName',
        ]);

        if(is_array($product_id)){
            $product = $product->whereIn('id', $product_id)->get();
        }else{
            $product = $product->where('id', $product_id)->firstOrFail();
        }

        $product->load('groups');

        \Cache::put($cache_key, $product, 5);

        return $product;
    }

    /**
     * @param $manufacturer_ids
     * @param $limit
     * @param bool $useCache
     * @return \Everglory\Models\Product[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function selectNewestProductsByManufacturerIds($manufacturer_ids, $limit, $useCache = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Product::with('manufacturer', 'images')
            ->whereIn('manufacturer_id', $manufacturer_ids)
            ->where('type', 0)
            ->where('is_main', 1)
            ->where('active', 1)
            ->where('parent_id', 0)
            ->groupBy('manufacturer_id')
            ->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function selectNewestProducts($useCache = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Product::with(['productDescription','images','motors','categories','fitModels','prices'])
            ->has('images')
            ->where('type', 0)
            ->where('is_main', 1)
            ->where('active', 1)
            ->where('parent_id', 0)
            ->where('is_new',1)
            ->groupBy('relation_product_id')
            ->groupBy('name')
            ->groupBy('manufacturer_id')
            ->select('products.*')
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function selectNewestProductsWithCategories($useCache = false)
    {
        $cache_key = self::getCacheKey(true);
//        \Cache::forget($cache_key);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }


        $ids = Product::has('images')
            ->where('type', 0)
            ->where('is_main', 1)
            ->where('active', 1)
            ->where('parent_id', 0)
            ->where('is_new',1)
            ->groupBy('name')
            ->groupBy('manufacturer_id')
            ->select('id')
            ->pluck('id')->toArray();

        $result =  Product::with(['images'])
            ->join('category_product','products.id','=','category_product.product_id')
            ->join('mptt_categories_flat','category_product.category_id','=','mptt_categories_flat.id')
            ->where('category_product.category_id','<>',' 1')
            ->where('mptt_categories_flat._active','=',' 1')
            ->whereIn('products.id',$ids)->select('products.*','category_product.category_id AS category','mptt_categories_flat.name as category_name','mptt_categories_flat.url_path as category_path')->get();
        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function selectProductsByCondition($conditions,$extras = [],$limit = 20,$useCache = false)
    {
        $result = Product::with(['images','manufacturer'])->has('images')
            ->where('is_main', 1)
            ->where('active', 1)
            ->where('parent_id', 0)
            ->groupBy('name')
            ->groupBy('manufacturer_id');
        $normal_type = true;
        foreach($conditions as $condition){
            foreach($condition as $c => $v){
                if($c == 'type'){
                    $normal_type = false;
                }
                if(is_array($v)){
                    $result = $result->whereIn($c,$v);
                }else{
                    $result = $result->where($c,$v);
                }
            }
        }
        foreach($extras as $extra) {
            if($extra == 'newest'){
                $dt = Carbon::now();
                $dt->subMonth();
                $result = $result->where('created_at','>',$dt->toDateTimeString());
            }
        }
        if($normal_type){
            $result = $result->where('type',0);
        }
        $result = $result->take($limit)->get();
        return $result;

    }

    /**
     * @param $category_id
     * @param $limit
     * @param bool $useCache
     * @return \Everglory\Models\Product[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function selectProductsByRank($category_id, $limit, $useCache = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $result = Product::where('products.parent_id', '=', 0)
            ->where('products.type', 0)
            ->where('products.active', 1)
            ->where('products.is_main', 1)
            ->join(\DB::connection('eg_zero')->raw("(SELECT count(1) as rank,order_items.sku as sku FROM eg_zero.`order_items` where sku is not null GROUP BY sku ORDER BY count(1) DESC ) as ir"), 'ir.sku', '=', 'products.sku')
            ->join('category_product', 'category_product.product_id', '=', 'products.id')
            ->where('category_product.category_id', '=', $category_id)
            ->orderBy('rank', 'desc')
            ->take($limit)
            ->with('manufacturer', 'images')
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }


    /**
     * @param $category_id
     * @param $limit
     * @param bool $useCache
     * @return \Everglory\Models\Product[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function selectProductsHasImageByCategoryId($category_id, $limit, $useCache = false)
    {

        $cache_key = self::getCacheKey(true);
        if (useCache() and $useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $result = Product::with('manufacturer', 'images')
            ->join('eg_product.category_product', 'products.id', '=', 'category_product.product_id')
            ->has('images')
            ->where('category_product.category_id', $category_id)/*->where('products.is_new', 1)*/
            ->where('active', 1)->where('is_main', 1)->where('type', 0)->where('parent_id', 0)
            ->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();
        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }


    public static function getMostReviewProduct( $during = 180 , $limit = 10 ){
        $product_ids = Review::select(\DB::raw( 'product_id as id , count(product_id) as count'))
            ->whereRaw('created_at > CURDATE() - INTERVAL ? DAY' , array($during))
            ->whereNull('deleted_at')
            ->where('view_status','=',1)
            ->groupBy('product_id')
            ->orderBy(\DB::raw('count(product_id)') , 'desc')
            ->take($limit)
            ->get();

        $products = Product::whereIn('id' , $product_ids->pluck('id'))
            ->with(
                'manufacturer',
                'images'
            )->with(['reviews' => function($query){
                $query->whereNull('deleted_at')
                    ->where('view_status','=',1);
            }])
            ->get();

        $products = $products->map(function ($value) use ($product_ids){
            $value->review_count = $product_ids->where('id',$value->id)->first()->count;
            return $value;
        })->sortByDesc('review_count');

        return $products;


    }

    public static function getMainProduct($url_rewrite,$with_attributes)
    {
        $product = Product::with($with_attributes)->where('url_rewrite', $url_rewrite)->first();
        if($product){
            if($product->type == 4){
                $additional = \Everglory\Models\Stock\Additional::where('product_id', $product->id)->first();
                if( $additional ){
                    return Product::with($with_attributes)->where('id', $additional->reflect_id)->first();
                }else{
                    return $product;
                }
            }else if($product->is_main == 1){
                return $product;
            }else{
                return Product::with($with_attributes)->where('manufacturer_id' , $product->manufacturer->id)->where( 'group_code' , $product->group_code)->orderBy('id')->first();

            }
        }
        return null;
    }

    public static function getProductsByBrands($url_rewrite,$series = true,$limit = 30)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = collect();
        $manufacturer = Manufacturer::where('url_rewrite' ,'=',$url_rewrite)->first();
        if($manufacturer){
            $result = Product::with(['manufacturer','images', 'prices','motors'])->where('active','=','1')->where('is_new','=','1')->where('is_main','=','1')->where('manufacturer_id','=',$manufacturer->id);
            if($series){
                $result = $result->groupBy('relation_product_id');
            }
            if($limit){
                $result = $result->take($limit);
            }
            $result = $result->get();
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getMainProductCount()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = 0;
        $product = Product::select(\DB::raw('count(0) AS count'))
            ->where('active', 1)
            ->where('is_main', 1)
            ->where('type', 0)
            ->where('parent_id', 0)
            ->first();
        if($product){
            $result = $product->count;
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function getProductByModelNumber($product_model_numbers,$type = ProductType::NORMAL, $manufacturer_ids = [])
    {
        $product_model_number_collection = [];
        foreach ($product_model_numbers as $product_model_number){
            if(strpos($product_model_number, '-') !== false){
                $product_model_number_collection[] = trim(str_replace('-', '', $product_model_number));
            }
            $product_model_number_collection[] = $product_model_number;
        }

        $products =  Product::with('prices')->where('type','=',$type)->where('active','=',1);
        if(is_array($manufacturer_ids) and count($manufacturer_ids)){
            $products = $products->whereIn('manufacturer_id', $manufacturer_ids);
        }else if(!is_array($manufacturer_ids) and is_numeric($manufacturer_ids)){
            $products = $products->where('manufacturer_id', $manufacturer_ids);
        }
        if($type == ProductType::OUTLET){
            $product_ids = Stock::whereIn('model_number',$product_model_number_collection)->where('type_id','8')->pluck('product_id')->toArray();
            $products = $products->whereIn('id',$product_ids);
        }else{
            $products = $products->whereIn('model_number',$product_model_number_collection);
        }

        $product = $products->get();
        return $product;
    }

    public function getChildCategoriesProducts($category,$child = false,$attribute_tag = false,$limit = false,$brands =[])
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
            
        $result =   Product::select('m.name as manufacturer_name','products.name as product_name','products.sku as url_rewrite','img.image')
        ->join('category_product as cp','products.id','=','cp.product_id')
        ->join('mptt_categories_flat','cp.category_id','=','mptt_categories_flat.id')
        ->join('manufacturers as m','products.manufacturer_id','=','m.id')
        ->join('product_images as img',function($query){
            $query->on('products.id','=','img.product_id')
                ->whereNotNull('img.image');
        })->where('products.active', '1')
        ->where('is_main','1');

        if($brands){
            $result = $result->whereIn('m.url_rewrite',$brands);    
        }
    
        if($attribute_tag){
            $result = $result->where('products.attribute_tag',$attribute_tag);
        }

        if($child and $category->n_right - $category->n_left != 1){
            $result = $result->where('mptt_categories_flat.parent_id',$category->id)
                ->where('mptt_categories_flat.depth',$category->depth + 1);
        }

        if($category->n_right - $category->n_left == 1) {
            $result = $result->where('mptt_categories_flat.id', $category->id);
        }

        if($limit){
            $result = $result->limit($limit);   
        }

         $result = $result->groupby('products.id')->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function active($ids, $column = 'product_id')
    {
        $product = null;
        if(is_array($ids)){
            $product = Product::whereIn($column, $ids);
        }else{
            $product = Product::where($column, $ids);
        }
        $product->update(['active' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
    }

    public static function nonActive($ids, $column = 'product_id')
    {
        $product = null;
        if(is_array($ids)){
            $product = Product::whereIn($column, $ids);
        }else{
            $product = Product::where($column, $ids);
        }
        $product->update(['active' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
    }

    public static function getGroupProduct($manufacturer_id, $group_code)
    {
        $products = Product::where('manufacturer_id', $manufacturer_id)
            ->where('group_code', $group_code)
            ->get();
        return $products;
    }

}