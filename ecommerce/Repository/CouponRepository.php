<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Repository;


use Everglory\Models\Coupon;

class CouponRepository extends BaseRepository
{
    const CACHE_LIFETIME = 30;

    /**
     * @param bool $useCache
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getTypeCoupons($code,$discount_type)
    {
        $coupons = Coupon::where('code','=',$code)->where('discount_type','=',$discount_type)->whereNull('order_id')->where('expired_at','>',date("Y-m-d"))->get();

        return $coupons;
    }

}
?>