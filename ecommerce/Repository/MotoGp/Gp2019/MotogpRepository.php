<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository\MotoGp\Gp2019;

use Ecommerce\Repository\BaseRepository;
use Everglory\Models\Motogp\Table2019\Choice;
use Everglory\Models\Motogp\Table2019\Racer;
use Everglory\Models\Motogp\Table2019\Speedway;
use Everglory\Models\Order\Reward;
use Illuminate\Support\Collection;

class MotogpRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @param $value
     * @param string $column
     * @return Manufacturer|null
     */
    public static  function choice_result()
    {
        $result = '';
        $customer = \Auth::user();
        if(\Auth::check()){
            $number = Choice::join('campaign_motogp2019_choice AS r_choice', function ($join) use($customer){
                $join->on('campaign_motogp2019_choice.speedway_id','=','r_choice.speedway_id')
                    ->on('campaign_motogp2019_choice.racer_id','=','r_choice.racer_id')
                    ->on('campaign_motogp2019_choice.score','=','r_choice.score')
                    ->where('campaign_motogp2019_choice.customer_id','=',$customer->id)
                    ->whereNull('r_choice.customer_id');
            })->groupby('r_choice.score')->select(\DB::RAW('r_choice.score , count(0) as count'))->pluck('count','score');
            $result = $number;
        }

        return $result;
    }

    public static  function getstations()
    {

        $result = '';
        $customer = \Auth::user();
        if(\Auth::check()){
            $stations = Choice::where('customer_id',$customer->id)->groupby('speedway_id')->get();
            $result = count($stations);
        }

        return $result;
    }

    public static function getMyChampigns()
    {
        $result = '';

        $customer = \Auth::user();
        if(\Auth::check()){
            $result = \DB::table(
                \DB::RAW("(SELECT sum(score) as scores,customer_id,racer_id FROM ec_dbs.campaign_motogp2019_choice where customer_id = ".$customer->id." GROUP BY racer_id ) as ranking")
            )
                ->orderBy('scores','desc')
                ->select('ranking.*','racer.name as racer_name','racer.number as racer_number')
                ->join('ec_dbs.campaign_motogp2019_racer as racer','ranking.racer_id','=','racer.id')
                ->get();
        }

        return $result;
    }

    public static function getotherchampigns()
    {
        $result = '';

        $result = \DB::select("select count(0) as champion,result.* ,racer.name as racer_name , racer.number as racer_number from 

            ( select * from 

            (SELECT sum(score) as scores,customer_id,racer_id FROM ec_dbs.`campaign_motogp2019_choice` 
            where customer_id is not null 
            GROUP BY customer_id,racer_id 
            ORDER BY customer_id,scores DESC)
             
            as ranking 

            GROUP BY customer_id ) 
            as result 
            JOIN ec_dbs.campaign_motogp2019_racer as racer
            on result.racer_id = racer.id
            GROUP BY racer_id
            order BY champion desc
            LIMIT 5;");

        return $result;
    }

    public static function getRacerScoreMap()
    {
        Racer::leftJoin(\DB::RAW("( SELECT c.*,
                              SUM(c.score) AS total_scores
                      FROM ec_dbs.campaign_motogp2019_choice AS c
                      WHERE c.customer_id IS NULL
                      GROUP BY c.racer_id
                      ORDER BY total_scores DESC ) as c"),
            function($join){
                $join->on('c.racer_id','=','ec_dbs.campaign_motogp2019_racer.id');
            })->orderBy('total_scores','desc')
            ->select('total_scores','ec_dbs.campaign_motogp2019_racer.*')
            ->get();
    }

    public static function getRacers()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $racers = Racer::leftJoin(\DB::RAW("( SELECT c.*,
                          SUM(c.score) AS total_scores
                  FROM ec_dbs.campaign_motogp2019_choice AS c
                  WHERE c.customer_id IS NULL
                  GROUP BY c.racer_id
                  ORDER BY total_scores DESC ) as c"),function($join){
            $join->on('c.racer_id','=','ec_dbs.campaign_motogp2019_racer.id');
        })->orderBy('total_scores','desc')->select('total_scores','ec_dbs.campaign_motogp2019_racer.*')->get();
        $ranking = 1;
        $scores = '';

        // foreach ($racers as $key => $racer) {
        //     echo $racer->name . $racer->total_scores.'<br>';
        // }
        foreach ($racers as $key => $racer) {
            if($scores == ''){
                if($racer->total_scores == 0){
                    $racer->ranking = 0;
                }else{
                    $racer->ranking = $ranking;
                    $scores = $racer->total_scores;
                }
            }else{
                if($racer->total_scores == 0){
                    $racer->ranking = 0;
                }else if($racer->total_scores == $scores){
                    $racer->ranking = $ranking;
                }else{
                    $racer->ranking = $key + 1;
                    $ranking = $key + 1;
                    $scores = $racer->total_scores;
                }
            }
        }
        $result = $racers->sortBy('id');

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }

        return $result;
    }

    public static function getspeedwaies()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Speedway::with(['numberOne', 'numberTwo', 'numberThree'])->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }


    public static function getTopsScore($target_racer_ids = null, $limit = 10)
    {
        $cache_key = self::getCacheKey((is_array($target_racer_ids) ? implode($target_racer_ids) : $target_racer_ids) . $limit);
        $result = \Cache::tags(['motogp-2019'])->remember('motogp-2019-'.$cache_key, 86400 * 7, function () use($target_racer_ids, $limit){
            $queryBuilder = Choice::with('speedway')->whereNull('customer_id');
            if(!$target_racer_ids){
                $target_racer_ids = self::getRacers()->sortByDesc('total_scores');
                if($limit){
                    $target_racer_ids = $target_racer_ids->take($limit);
                }
                $target_racer_ids = $target_racer_ids->map(function ($racer) {
                    return $racer->id;
                })->toArray();
            }
            $result = $queryBuilder->whereIn('racer_id', $target_racer_ids);
            $result = $result->get();
            return $result;
        });
        return $result;

    }

    public static function getTeamData($sort = true)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Racer::select('team_name', \DB::raw('SUM(score) AS sum'),'team_img')
            ->leftjoin('campaign_motogp2019_choice AS choices', function($query) {
                $query->on('choices.racer_id', '=', 'campaign_motogp2019_racer.id')
                    ->whereNull('choices.customer_id');
            })
            ->groupBy('team_name');
            if($sort){
                $result = $result->orderby('sum', 'DESC');
            }else{
                $result = $result->orderby('campaign_motogp2019_racer.id', 'ASC');
            }
            $result = $result->take(5)
            ->get();
//        dd($result);
//        $result = Choice::select('team_name', \DB::raw('SUM(score) AS sum'),'team_img')
//            ->join('ec_dbs.campaign_motogp2019_racer AS racer', 'racer.id', '=', 'racer_id')
//            ->whereNull('customer_id')
//            ->groupBy('team_name')
//            ->orderby('sum', 'DESC')
//            ->take(5)
//            ->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getMakerData($sort = true)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Racer::select('motor_manufacturer_name', \DB::raw('SUM(score) AS sum'))
            ->leftjoin('ec_dbs.campaign_motogp2019_choice AS choices', function($query){
              $query->on('campaign_motogp2019_racer.id', '=', 'choices.racer_id')
              ->whereNull('choices.customer_id');
            })
            ->groupBy('motor_manufacturer_name');
            if($sort){
                $result = $result->orderby('sum', 'DESC');
            }else{
                $result = $result->orderby('campaign_motogp2019_racer.id', 'ASC');
            }
            $result = $result->take(5)->get();
//        dd($result);
//        $result = Choice::select('motor_manufacturer_name', \DB::raw('SUM(score) AS sum'))
//            ->leftjoin('ec_dbs.campaign_motogp2019_racer AS racer', 'racer.id', '=', 'racer_id')
//            ->whereNull('customer_id')
//            ->groupBy('motor_manufacturer_name')
//            ->orderby('sum', 'DESC')->take(5)->get();


        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getMotoGpPoint()
    {

        $result = 0;
        $customer = \Auth::user();
        if(\Auth::check()){
            $reward = Reward::where('customer_id',$customer->id)->where('description','like','2019MotoGP預測%')->groupby('customer_id')->select(\DB::RAW('SUM(points_current) as points_current'))->first();
            if($reward){
                $result = $reward->points_current;
            }
        }

        return $result;
    }
    public static function getManufacturerWithRelations($url_rewrite, $relations = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $manufacturers = Manufacturer::with($relations);

        if(is_array($url_rewrite)){
            $manufacturers = $manufacturers->whereIn('url_rewrite', $url_rewrite)->get();
        }else{
            $manufacturers = $manufacturers->where('url_rewrite', $url_rewrite)->firstOrFail();
        }

        \Cache::put($cache_key, $manufacturers, 5);

        return $manufacturers;
    }

    public static function getChoices()
    {
        $result = [];
        $customer = \Auth::user();
        $now_speedway = self::getNowSpeedway();
        if(\Auth::check() and $now_speedway){
            if(count($now_speedway) != 0){
                $result = Choice::with('racer')->where('speedway_id',$now_speedway->id)->where('customer_id',$customer->id)->orderBy('score','desc')->get();
            }
        }
        return $result;
    }

    public static function getCustomerChoices()
    {
        $result = new \stdClass();
        $customer = \Auth::user();
        if(\Auth::check()){
            $result->one[] = '';
            $result->two[] = '';
            $result->three[] = '';
            $records = Choice::with('racer')->where('customer_id', $customer->id)->orderby('speedway_id','asc')->orderby('score','desc')->get();
            if(count($records) != 0){
                $records = $records->groupby('speedway_id');
                $result = new \stdClass();
                foreach($records as $record_data){
                    foreach($record_data as $key => $record){
                        switch ($key) {
                            case '0':
                                $result->one[$record->speedway_id] = [$record->racer->number];
                                break;

                            case '1':
                                $result->two[$record->speedway_id] = [$record->racer->number];
                                break;

                            case '2':
                                $result->three[$record->speedway_id] = [$record->racer->number];
                                break;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get active speedway in motoGP.
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function getActiveStation()
    {
        return  Speedway::where('active', 1)->first();
    }

    /**
     * Get list of popular players in motoGP.
     *
     * @param null $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getRank($limit = NULL)
    {
        $choice = Choice::with('racer')->select('racer_id', \DB::raw('SUM(score) AS total_score'))->whereNull('customer_id')->orderby('total_score', 'desc')->groupBy('racer_id');
        if($limit){
            $choice->take($limit);
        }
        return $choice->get();
    }

    public static function getreal_choices()
    {
        $result = new \stdClass();
        if(\Auth::check()){
            $result->one[] = '';
            $result->two[] = '';
            $result->three[] = '';
            $record1 = Choice::whereNull('customer_id')->orderby('speedway_id','asc')->orderby('score','desc')->get();
            if(count($record1) != 0){
                $records = $record1->groupby('speedway_id');
                $result = new \stdClass();
                foreach($records as $record1){
                    foreach($record1 as $key => $record){
                        switch ($key) {
                            case '0':
                                $result->one[$record->speedway_id] = [$record->racer->number];
                                break;

                            case '1':
                                $result->two[$record->speedway_id] = [$record->racer->number];
                                break;

                            case '2':
                                $result->three[$record->speedway_id] = [$record->racer->number];
                                break;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function getNowSpeedway()
    {
        $result = Speedway::with('numberOne','numberTwo','numberThree')->orderBy('raced_at','desc')->where('active', 1)->first();
        return $result;
    }

    public static function thisSpeedway($station)
    {
        $result = Speedway::with('numberOne','numberTwo','numberThree')->where('station_name', $station)->first();

        return $result;
    }

    public static function postspeedway()
    {
        $result = Speedway::orderBy('raced_at','desc')->where('active','1')->first();
        return $result;
    }

    public static function postchoices()
    {
        $customer = \Auth::user();
        $now_speedway = Speedway::orderBy('raced_at','desc')->where('active','1')->first();
        $result = Choice::where('speedway_id',$now_speedway->id)->where('customer_id',$customer->id)->get();

        return $result;
    }
    public static function getParticipantsCustomerCount()
    {

        $result = Choice::groupby('customer_id')->get()->count();

        return $result;
    }

    public static function getMotogpTagNews($year,$station,$limit)
    {
        $testConnection = testServerConnection('eg_news');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $result = \Everglory\Models\News\Post::join('wp_term_relationships', 'ID', '=', 'wp_term_relationships.object_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
            ->where('post_status', 'publish')
            ->where('post_type', 'post')
            ->where('term_order', 0)
            ->where('wp_terms.name', 'like' ,'%'.$station)
            ->where('post_date','like',$year.'%')
            ->orderBy('post_date', 'DESC')
            ->groupBy('ID');
        if($limit){
            $result = $result->limit($limit);
        }

        return $result->get();
    }

    public static function getAllCustomerChooseRacersRate()
    {
        $all_choice = Choice::select(\DB::raw('sum(score) as score_total'))->first();
        $racers_choices =  Choice::select(\DB::raw("sum(score) as racer_score"),'racer_id')->groupBy('racer_id')->orderBy('racer_id','asc')->get();
        $racers_choice_rate = [];
        foreach($racers_choices as $racers_choice){
            $racers_choice_rate[$racers_choice->racer_id] = number_format($racers_choice->racer_score/$all_choice->score_total*100,0);
        }
        return $racers_choice_rate;
    }

    public static function getCustomerChooseRacersScore($limit = null)
    {
        $result = Choice::with('racer')->select('racer_id',\DB::raw('sum(score) as score'))
            ->where('customer_id',\Auth::user()->id)
            ->groupBy('racer_id')
            ->orderBy('score','desc');

        if($limit){
            $result = $result->limit($limit);
        }

        return $result->get();
    }
}