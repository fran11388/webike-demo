<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository\MotoGp\Gp2017;

use Ecommerce\Repository\BaseRepository;
use Everglory\Models\Motogp\Choice;
use Everglory\Models\Motogp\Racer;
use Everglory\Models\Motogp\Speedway;
use Everglory\Models\Order\Reward;

class MotogpRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @param $value
     * @param string $column
     * @return Manufacturer|null
     */
    public static  function choice_result()
    {
        $result = '';
        $customer = \Auth::user();
        if(\Auth::check()){
            $number = Choice::join('campaign_motogp2017_choice AS r_choice', function ($join) use($customer){
                        $join->on('campaign_motogp2017_choice.speedway_id','=','r_choice.speedway_id')
                             ->on('campaign_motogp2017_choice.racer_id','=','r_choice.racer_id')   
                             ->on('campaign_motogp2017_choice.score','=','r_choice.score')
                             ->where('campaign_motogp2017_choice.customer_id','=',$customer->id)
                             ->whereNull('r_choice.customer_id');
                })->groupby('r_choice.score')->select(\DB::RAW('r_choice.score , count(0) as count'))->pluck('count','score');
            $result = $number;
        }

        return $result;
    }

    public static  function getstations()
    {

        $result = '';
        $customer = \Auth::user();
        if(\Auth::check()){
            $stations = Choice::where('customer_id',$customer->id)->groupby('speedway_id')->get();
            $result = count($stations);
        }

        return $result;
    }

    public static function getmychampigns()
    {
        $result = '';

        $customer = \Auth::user();
        if(\Auth::check()){
            $result = \DB::table(
                    \DB::RAW("(SELECT sum(score) as scores,customer_id,racer_id FROM ec_dbs.campaign_motogp2017_choice where customer_id = ".$customer->id." GROUP BY racer_id ) as ranking")
                )
                ->orderBy('scores','desc')
                ->select('ranking.*','racer.name as racer_name','racer.number as racer_number')
                ->join('ec_dbs.campaign_motogp2017_racer as racer','ranking.racer_id','=','racer.id')
                ->get();
        }

        return $result;
    }

    public static function getotherchampigns()
    {
        $result = '';

        $result = \DB::select("select count(0) as champion,result.* ,racer.name as racer_name , racer.number as racer_number from 

            ( select * from 

            (SELECT sum(score) as scores,customer_id,racer_id FROM ec_dbs.`campaign_motogp2017_choice` 
            where customer_id is not null 
            GROUP BY customer_id,racer_id 
            ORDER BY customer_id,scores DESC)
             
            as ranking 

            GROUP BY customer_id ) 
            as result 
            JOIN ec_dbs.campaign_motogp2017_racer as racer
            on result.racer_id = racer.id
            GROUP BY racer_id
            order BY champion desc
            LIMIT 5;");

        return $result;
    }

    public static function getRacers()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $racers = Racer::leftJoin(\DB::RAW("( SELECT c.*,
                              SUM(c.score) AS total_scores
                      FROM ec_dbs.campaign_motogp2017_choice AS c
                      WHERE c.customer_id IS NULL
                      GROUP BY c.racer_id
                      ORDER BY total_scores DESC ) as c"),function($join){
                $join->on('c.racer_id','=','ec_dbs.campaign_motogp2017_racer.id');
        })->orderBy('total_scores','desc')->select('total_scores','ec_dbs.campaign_motogp2017_racer.*')->get();
        $ranking = 1;
        $scores = '';
        // foreach ($racers as $key => $racer) {
        //     echo $racer->name . $racer->total_scores.'<br>';
        // }
        foreach ($racers as $key => $racer) {
            if($scores == ''){
                if($racer->total_scores == 0){
                    $racer->ranking = 0;
                }else{
                    $racer->ranking = $ranking;
                    $scores = $racer->total_scores;
                }
            }else{
                if($racer->total_scores == 0){
                    $racer->ranking = 0;
                }else if($racer->total_scores == $scores){
                    $racer->ranking = $ranking;
                }else{
                    $racer->ranking = $key + 1;
                    $ranking = $key + 1;
                    $scores = $racer->total_scores;
                }
            }
        }
        $result = $racers->sortBy('id');

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getspeedwaies()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Speedway::with(['numberOne', 'numberTwo', 'numberThree'])->get();
        
        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function gettop10_score()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $top10 = self::getRacers()->sortByDesc('total_scores')->take(10)->all();
        $top10_ids = self::getRacers()->sortByDesc('total_scores')->take(10)->map(function($racer, $key){
                return $racer->id;
            })->toArray();
        $result = Choice::whereNull('customer_id')->whereIn('racer_id', $top10_ids)->get();
        
        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
        
    }

    public static function getteamData()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Choice::select('team_name', \DB::raw('SUM(score) AS sum'))->join('ec_dbs.campaign_motogp2017_racer AS racer', 'racer.id', '=', 'racer_id')->whereNull('customer_id')->groupBy('team_name')->orderby('sum', 'DESC')->take(5)->get();
        
        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getmakerData()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Choice::select('motor_manufacturer_name', \DB::raw('SUM(score) AS sum'))->join('ec_dbs.campaign_motogp2017_racer AS racer', 'racer.id', '=', 'racer_id')->whereNull('customer_id')->groupBy('motor_manufacturer_name')->orderby('sum', 'DESC')->take(5)->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getmotogppoint()
    {

        $result = '';
        $customer = \Auth::user();
        if(\Auth::check()){
            $result = Reward::where('customer_id',$customer->id)->where('description','like','2017MotoGP預測%')->groupby('customer_id')->select(\DB::RAW('SUM(points_current) as points_current'))->first();
        }

        return $result;
    }
    public static function getManufacturerWithRelations($url_rewrite, $relations = false)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $manufacturers = Manufacturer::with($relations);

        if(is_array($url_rewrite)){
            $manufacturers = $manufacturers->whereIn('url_rewrite', $url_rewrite)->get();
        }else{
            $manufacturers = $manufacturers->where('url_rewrite', $url_rewrite)->firstOrFail();
        }

        \Cache::put($cache_key, $manufacturers, 5);

        return $manufacturers;
    }

    public static function getChoices()
    {
        $result = [];
        $customer = \Auth::user();
        $now_speedway = Speedway::orderBy('raced_at','desc')->where('active','<>',0)->first();
        if(\Auth::check()){

            if(count($now_speedway) != 0){
                $result = Choice::with('racer')->where('speedway_id',$now_speedway->id)->where('customer_id',$customer->id)->orderBy('score','desc')->get();
            }
        }
        return $result;
    }

    public static function getCustomerChoices()
    {
        $result = new \stdClass();
        $customer = \Auth::user();
        if(\Auth::check()){
            $result->one[] = '';
            $result->two[] = '';
            $result->three[] = '';
            $records1 = Choice::with('racer')->where('customer_id', $customer->id)->orderby('speedway_id','asc')->orderby('score','desc')->get();
            if(count($records1) != 0){
                $records = $records1->groupby('speedway_id');
                $result = new \stdClass(); 
                foreach($records as $record_data){
                    foreach($record_data as $key => $record){
                        switch ($key) {
                            case '0':
                                $result->one[$record->speedway_id] = [$record->racer->number];
                                break;
                            
                            case '1':
                                $result->two[$record->speedway_id] = [$record->racer->number];
                                break;

                            case '2':
                                $result->three[$record->speedway_id] = [$record->racer->number];
                                break;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get active speedway in motoGP.
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function getActiveStation()
    {
        return  Speedway::where('active', 1)->first();
    }

    /**
     * Get list of popular players in motoGP.
     *
     * @param null $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getRank($limit = NULL)
    {
        $choice = Choice::with('racer')->select('racer_id', \DB::raw('SUM(score) AS total_score'))->whereNull('customer_id')->orderby('total_score', 'desc')->groupBy('racer_id');
        if($limit){
            $choice->take($limit);
        }
        return $choice->get();
    }

    public static function getreal_choices()
    {
        $result = new \stdClass();
        if(\Auth::check()){
            $result->one[] = '';
            $result->two[] = '';
            $result->three[] = '';
            $record1 = Choice::whereNull('customer_id')->orderby('speedway_id','asc')->orderby('score','desc')->get();
            if(count($record1) != 0){
                $records = $record1->groupby('speedway_id');
                $result = new \stdClass();
                foreach($records as $record1){
                    foreach($record1 as $key => $record){
                        switch ($key) {
                            case '0':
                                $result->one[$record->speedway_id] = [$record->racer->number];
                                break;
                            
                            case '1':
                                $result->two[$record->speedway_id] = [$record->racer->number];
                                break;

                            case '2':
                                $result->three[$record->speedway_id] = [$record->racer->number];
                                break;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public static function getNowSpeedway()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Speedway::with('numberOne','numberTwo','numberThree')->orderBy('raced_at','desc')->where('active','<>',0)->first();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function thisSpeedway($station)
    {
        $result = Speedway::with('numberOne','numberTwo','numberThree')->where('station_name', $station)->first();

        return $result;
    }

    public static function postspeedway()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Speedway::orderBy('raced_at','desc')->where('active','1')->first();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function postchoices()
    {
        $customer = \Auth::user();
        $now_speedway = Speedway::orderBy('raced_at','desc')->where('active','1')->first();
        $result = Choice::where('speedway_id',$now_speedway->id)->where('customer_id',$customer->id)->get();

        return $result;
    }
    public static function getParticipantsCustomerCount()
    {
        
        $result = Choice::groupby('customer_id')->get()->count();

        return $result;
    }

    public static function getMotogpTagNews($year,$station)
    {
        $result = \Everglory\Models\News\Post::join('wp_term_relationships', 'ID', '=', 'wp_term_relationships.object_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
            ->where('post_status', 'publish')
            ->where('post_type', 'post')
            ->where('term_order', 0)
            ->where('wp_terms.name', 'like' ,'%'.$station)
            ->where('post_date','like',$year.'%')
            ->orderBy('post_date', 'DESC')
            ->groupBy('ID')
            ->get();

        return $result;
    }
}