<?php namespace Ecommerce\Repository;
use Everglory\Models\Customer;

/**
 * Created by PhpStorm.
 * User: EG-PC-004
 * Date: 2019/5/15
 * Time: 下午 02:45
 */
Class CustomerRepository extends BaseRepository {

    public function __construct()
    {
        $this->queryBuild = new Customer;
    }

    public function getCustomerByUuid($uuid)
    {
        $this->queryBuild = $this->queryBuild->where('email_validate_uuid','=',$uuid);

        return $this;
    }

    public function getCustomerByEmail($email)
    {
        $this->queryBuild = $this->queryBuild->where('email','=',$email);

        return $this;
    }

}