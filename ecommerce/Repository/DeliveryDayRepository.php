<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;

use Everglory\Models\DeliveryDay;

class DeliveryDayRepository extends BaseRepository
{

    public static function getOverDelivery()
    {
        return DeliveryDay::where('code', 'like', '1%')->orderBy('code')->first();
    }
}