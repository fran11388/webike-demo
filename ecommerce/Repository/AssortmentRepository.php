<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;

use Everglory\Models\Assortment\Layout;
use Everglory\Models\Assortment\Type;
use Everglory\Models\Assortment\Plugin;
use Everglory\Models\Assortment;
use Everglory\Models\Assortment\Item;
use Cache;

class AssortmentRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @param $value
     * @param string $column
     * @return Manufacturer|null
     */

    public static  function getLayouts()
    {
        // $cache_key = self::getCacheKey('promotionName=' . $promotionName . '&ca_parent=' . $ca_parent);
        // if (useCache() and $result = Cache::get($cache_key)) {
        //     return $result;
        // }

        // if(!useCache()){
        //     Cache::forget($cache_key);
        // }
        $result = Layout::get();

        // if (count($result)) {
        //     Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        // }
        return $result;
    }

    public static  function getTypes($type_id = NULL)
    {
        // $cache_key = self::getCacheKey('promotionName=' . $promotionName . '&ca_parent=' . $ca_parent);
        // if (useCache() and $result = Cache::get($cache_key)) {
        //     return $result;
        // }

        // if(!useCache()){
        //     Cache::forget($cache_key);
        // }
        if(is_array($type_id)) {
            $result = Type::whereIn('id', $type_id)->get();
        }else if($type_id){
            $result = Type::where('id', $type_id)->get();
        }else{
            $result = Type::get();
        }

        // if (count($result)) {
        //     Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        // }
        return $result;
    }

    public static  function getPluginTemplates()
    {
        // $cache_key = self::getCacheKey('promotionName=' . $promotionName . '&ca_parent=' . $ca_parent);
        // if (useCache() and $result = Cache::get($cache_key)) {
        //     return $result;
        // }

        // if(!useCache()){
        //     Cache::forget($cache_key);
        // }

        $plugins = Plugin::with('templates')->get();
        $result = $plugins->filter(function($plugin){
            return count($plugin->templates);
        });
        // if (count($result)) {
        //     Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        // }
        return $result;
    }

    public static  function getAssortmentById($assortment_id,$plugin = false)
    {
        if($plugin){
            $assortment = Assortment::with('type','layout','items','items.plugin.templates')->where('id',$assortment_id)->first();
        }else{
            $assortment = Assortment::with('type','layout','items')->where('id',$assortment_id)->first();
        }

        return $assortment;
    }

    public static  function getAssortmentItemById($assortmentItem_id)
    {
        $assortmentItem = Item::where('id',$assortmentItem_id)->first();
        return $assortmentItem;
    }

}