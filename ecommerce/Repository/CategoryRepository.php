<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;

use Ecommerce\Service\Quote\Exception;
use Everglory\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @param Category $category
     * @return \Everglory\Models\Category[]|\Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    static public function selectSiblings($category)
    {
        $cache_key = self::getCacheKey('id=' . $category->id );
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result =Category::with('mptt')
            ->where('parent_id', $category->parent_id)
            ->where('depth', $category->depth)
            ->where('id', '<>', $category->id)
            ->where('active', '1')
            ->orderBy('sort')
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    /**
     * Get category information by specific column and value from database.
     *
     * @param string | array $value search condition
     * @param string $column search column
     * @return Category|null
     */
    static public function find($value, $column = 'url_rewrite')
    {

        if (!$value)
            return null;
        $cache_key = self::getCacheKey(true);
        if (useCache()  and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result =  Category::with('mptt')->where('active', '1');

        if(is_array($value)){
            $func = function ($query) use ($value, $column) {
                return $query->whereIn($column, $value);
            };
            $result =  $result->whereHas('mptt', $func )->get();
        }else{
            $func = function ($query) use ($value, $column) {
                return $query->where($column, '=', $value);
            };
            $result =  $result->whereHas('mptt', $func )->first();
        }

        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        /** @var Category|null $result */
        return $result;
    }

    /**
     * Get specific category's subcategories
     *
     * @param int | array $node search condition
     * @return Collection|Category[]
     */
    static public function selectChildren($node , $column = 'id' )
    {
        if (is_numeric($node)) {
            $node = self::find($node, $column);
        }

        $cache_key = self::getCacheKey( $column . '=' . $node->id);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = Category::with('mptt')
            ->where('n_left' , '>' , $node->n_left)
            ->where('n_right' , '<' , $node->n_right)
            ->where('active' , '1')
            ->orderBy('depth' )
            ->orderBy('sort' )
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    /**
     * @param Category $node
     * @param bool $include
     * @return Collection|mixed|static[]
     */
    static public function getParents( $node , $include = false){

        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $paths  = explode("-" , $node->mptt->url_path);

        if (!$include){
            array_pop( $paths);
        }

        if (!count($paths))
            return [];

        $result = Category::whereIn('url_rewrite' , $paths )
            ->with('mptt')
            ->orderBy('depth')
            ->get();


        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;


    }

    static public function getCategory( $url_path ){

        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        // $result = Category::whereIn('url_rewrite' ,$url_path)
        //     ->with('mptt')
        //     ->with('products')
        //     ->orderBy('depth')
        //     ->get();
        $result = Category::whereIn('url_rewrite' ,$url_path)
            ->with('mptt')
            ->orderBy('depth')
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;


    }

    static public function getMpttByUrlPath( $url_path, $depth = NULL ){

        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = \Everglory\Models\Mptt::where('url_path', 'like', $url_path . '%')->where('_active', 1);
        if($depth){
            $result = $result->where('depth', $depth);
        }
        $result = $result->orderby('sort')->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;


    }


    /**
     * @param Category $category
     * @param null $depth
     * @return \Everglory\Models\Mptt|\Everglory\Models\Mptt[]|Collection|mixed
     * @throws \Exception
     */
    static public function getMpttByCategory($category, $depth = NULL){
        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

//        $result = \Everglory\Models\Mptt::where('url_path', 'like', $url_path . '%')->where('_active', 1);
       if(!$category) throw new \Exception('invailid category');
        $result=\Everglory\Models\Mptt::where('n_left','>=',$category->n_left)
            ->where('n_right','<=',$category->n_right)
            ->where('_active', 1);

        if($depth){
            $result = $result->where('depth', $depth);
        }
        $result = $result->orderby('sort')->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    static public function getCategoriesByDepths($depth){
        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $result = \Everglory\Models\Mptt::whereIn('depth',$depth)
            ->where('_active','=',1)
            ->orderBy('url_path','asc')
            ->get();

        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }
}