<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:14
 */

namespace Ecommerce\Repository;

use Everglory\Models\Correspond;
use Cache;

class BigPromotionRepository extends BaseRepository
{
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @param $value
     * @param string $column
     * @return Manufacturer|null
     */

    public static  function getCategories($promotionName, $ca_parent)
    {
        $cache_key = self::getCacheKey('promotionName=' . $promotionName . '&ca_parent=' . $ca_parent);
        if (useCache() and $result = Cache::get($cache_key)) {
            return $result;
        }

        if(!useCache()){
            Cache::forget($cache_key);
        }
        $result = Correspond::with('mptt')->with('manufacturer')->where('ca_parent',$ca_parent)->where('name',$promotionName)->get();

        if (count($result)) {
            Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getProductRecommends($type)
    {
        $result = \DB::connection('eg_product')->select("SELECT cr.type,cr.motor,pd.point_type,cr.sku,cr.name,pi.image,cr.manufacturer_name,cr.category_name_path,ppc.price as c_money,ppd.price as d_money,pptc.point,pptd.point,(1-round(ppc.price/pd.price,2))*100 as c_price,(1-round(ppd.price/pd.price,2))*100 as d_price,round(pptc.point/(pd.price*0.01),0) as c_point,round(pptd.point/(pd.price*0.01),0) as d_point
            FROM ec_dbs.campaign_recommends as cr
            join eg_product.products as pd
            on cr.sku = pd.sku
            join eg_product.product_prices as ppc
            on pd.id = ppc.product_id and ppc.role_id = 2
            join eg_product.product_prices as ppd
            on pd.id = ppd.product_id and ppd.role_id = 3
            join eg_product.product_points as pptc
            on pd.id = pptc.product_id and pptc.role_id = 2
            join eg_product.product_points as pptd
            on pd.id = pptd.product_id and pptd.role_id = 3
            join eg_product.product_images as pi
            on pd.id = pi.product_id
            where cr.date = '" . date('Y-m-d') . "'
            and campaign_code = '" . $type . "'
            and ppc.rule_date = '" . date('Y-m-d') . "'
            GROUP BY cr.sku");
        return collect($result)->groupBy('type');
    }

}