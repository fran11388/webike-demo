<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

class MainBanner extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.main-banner';



    public function __construct()
    {

    }

    public function getHtml($cols){
        $args = ['img' => str_replace( " ", "", $cols['1'])];
        $link = isset($cols['2']) ? $cols['2'] : " ";
        if($link){
            $args['link'] = $link;
        }


        $html = view($this->template, $args)->render();
     
        // import relate parameters to create HTML
        return $html;
    }
}