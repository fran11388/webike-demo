<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

interface InstallerInterface
{
    public function getHtml($cols);

}