<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

use Everglory\Models\Product;
use Ecommerce\Accessor\ProductAccessor;

class ProductList extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.ProductList';

    public function __construct()
    {

    }

    public function getHtml($cols){
      
      $sku = [];
      foreach ($cols as $key => $col) {
          if($key >= 1){
            $link = $col;
            $sku[] = str_replace("https://www.webike.tw/sd/", "", $link);
          }
      }
      $sku_sort = implode('\',\'',array_filter($sku));
      $products = Product::whereIn('sku',$sku)->orderByRaw(\DB::raw("FIELD(sku, '".$sku_sort."' )"))->get();
        
        foreach ($products as $key => $product) {
          $productAccessor = new ProductAccessor($product);
          $imgs[] = $productAccessor->getImage(); 
          $names[] = $productAccessor->name;
          $manufacturerNames[] = $product->manufacturer->name;
          $productSkus[] = $productAccessor->sku;

        }

          $args =['imgs' => $imgs, 'names' => $names, 'productSkus' => $productSkus, 'manufacturerNames' => $manufacturerNames]; 

        $html = view($this->template, $args)->render();
        return $html;
    }
}


