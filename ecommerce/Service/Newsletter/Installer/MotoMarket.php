<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

use Everglory\Models\MotoMarket\Product;
use Ecommerce\Accessor\MotoProductAccessor;
use DB;
class MotoMarket extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.MotoMarket';

    public function __construct()
    {

    }

    public function getHtml($cols){
      
      $serials = [];
      foreach ($cols as $key => $col){
            if($key >= 1){
              $link = $col;
              $serials[] = str_replace("http://www.webike.tw/motomarket/detail/", "", $link);
              
            }
      }
            $serials_sort = $sku_sort = implode('\',\'',array_filter($serials));
          $products = Product::with(['customer.group', 'motorModel', 'customer.profiles'])->whereIn('serial',$serials)->orderByRaw(\DB::raw("FIELD(serial, '".$serials_sort."' )"))->get();
          

        
          
        foreach ($products as $key => $product) {
          
          $urls[] = $product->serial;
          $groups = $product->customer->group->title;
          // dd($groups); 

          if($groups == 'corporation'){
          $profile = $product->customer->profiles->filter(function ($item) {
              return $item->attribute == "username";
          })->first();
          $names[] = $profile->value;
          }else{
            $names[] = "個人自售";
          }

          $motonames[] = $product->motorModel->title;
          $manufacturers[] = $product->motorModel->manufacturer;
          $photo = unserialize($product->motor_photos);
          if(isset($photo['0']) and isset($photo['0']->filename)){
            $imgs[] = "http://img.webike.tw/moto_photo/". $photo['0']->filename;
          }else{
            $imgs[] = NO_IMAGE;
          }


                   
        }
          $args =['imgs' => $imgs, 'motonames' => $motonames, 'manufacturers' => $manufacturers, 'names' => $names, 'urls' => $urls]; 
          // dd($args);

        $html = view($this->template, $args)->render();
        return $html;
    }
}


