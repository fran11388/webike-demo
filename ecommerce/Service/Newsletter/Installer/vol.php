<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

class vol extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.vol';


    public function __construct()
    {

    }

    public function getHtml($cols){

        $title = isset($cols['1'])? $cols['1'] : " ";
        if($title){
            $args['vol'] = $title;
        }
        $date = isset($cols['2'])? $cols['2'] : " ";
        if($date){
            $args['date'] = $date;
        }
        $textcolor = isset($cols['3'])? $cols['3'] : " ";
        if($textcolor){
            $args['textColor'] = $textcolor;
        }
       
        $textAlign = isset($cols['4'])? $cols['4'] : 3;
        if($textAlign == 1){
             $args['textAlign'] =  "style='text-align:left;'";
        }elseif($textAlign == 2){
             $args['textAlign'] =  "style='text-align:center;'";
        }elseif($textAlign == 3){
             $args['textAlign'] =  "style='text-align:right;'";
        }else{
            $args['textAlign'] =  "style='text-align:left;'";
        }

        $backgroundColor = isset($cols['5'])? $cols['5'] : " ";
        if($backgroundColor){
            $args['backgroundColor'] = $backgroundColor;
        }          

         // dd($args);

        $html = view($this->template , $args)->render();
        // dd($name);
        // import relate parameters to create HTML
        return $html;
    }
}


