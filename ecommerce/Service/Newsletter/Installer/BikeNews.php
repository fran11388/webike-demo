<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

use Everglory\Models\Product;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Service\BikeNewsService;

class bikenews extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.BikeNews';

    public function __construct()
    {

    }

    public function getHtml($cols){
      
      // dd($cols);
      $ids = NULL; 
      $url = [];
      foreach ($cols as $key => $col) {
        if($key >= 1 && !is_null($col)){
          $url = str_replace("&action=edit", "", $col);
          $url = parse_url($url);
          parse_str($url['query'], $url);
          
          // $ids[] = str_replace("https://www.webike.tw/bikenews/wp-admin/post.php?post=", "", $url);
            $ids[] =  isset($url['post']) ? $url['post'] :$url['p'];
        }
      }
      

      $BikeNewsService = new BikeNewsService();
      $posts = $BikeNewsService->getPostWithThumbnail($ids);
      $args = ['posts' => $posts];

      $html = view($this->template, $args)->render();
      return $html;
    }
}


