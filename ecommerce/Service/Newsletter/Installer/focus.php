<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

class focus extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.focus';

    public function __construct()
    {

    }

    public function getHtml($cols){
      if(isset($cols['1'])){
      	$args['titles'][1] = $cols['1'];
      	$args['texts'][1] = $cols['2'];
      	$args['imgs'][1] = str_replace( " ", "", $cols['3']);
      	$args['links'][1] = $cols['4'];
        }

        if(isset($cols['5'])){
        $args['titles'][2] = $cols['5'];
        $args['texts'][2] = $cols['6'];
        $args['imgs'][2] = str_replace( " ", "", $cols['7']);
        $args['links'][2] = $cols['8'];
        }


        $html = view($this->template, $args)->render();
        // import relate parameters to create HTML
        return $html;
    }
}


