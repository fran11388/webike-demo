<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;



class activity extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.activity';

    public function __construct()
    {

    }

    public function getHtml($cols){
     
      $img['1'] = str_replace( " ", "", $cols['1']);
      $link['1'] = $cols['2']; 
       
       if(isset($cols['3'])){   
          $img['2'] = str_replace( " ", "", $cols['3']);
          $link['2'] = $cols['4'];
       }
       
          $args =['imgs' => $img, 'links' => $link]; 

        $html = view($this->template, $args)->render();
        return $html;
    }
}


