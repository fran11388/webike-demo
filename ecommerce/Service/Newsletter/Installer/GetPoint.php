<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;
class GetPoint extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.get-point';


    public function __construct()
    {

    }

    public function getHtml($cols)
    {
        // dd($cols);
        
        
        $pointLink = '{{$give_point_url}}';

        $html = view($this->template, ['pointLink' => $pointLink])->render();

        // import relate parameters to create HTML
        // dd($html);
        return $html;
    }
}


