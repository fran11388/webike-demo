<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

class sale extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.sale';


    public function __construct()
    {

    }

    public function getHtml($cols){

        $img = str_replace( " ", "", $cols['1']);
        $link = $cols['2'];
        $args = ['img' => $img, 'link' => $link, 'text' => null];

        $text = isset($cols['3']) ? $cols['3'] : ' ';

        if($text){
            
            $args['text'] = $text;
        }
        

        $html = view($this->template, $args)->render();

        return $html;
    }
}


