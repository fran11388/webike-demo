<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

use Everglory\Models\Product;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Service\Newsletter\InstallerService;

class review extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.review';

    public function __construct()
    {

    }

    public function getHtml($cols){
      
      foreach ($cols as $key => $col) {
        if($key >= 1){
          $id[] = str_replace("https://www.webike.tw/review/article/", "", $col);
        }
      }

      
      // dd($url);
      $InstallerService = new InstallerService();
      $getReview = $InstallerService->getReview($id);
      // dd($getReview);
      foreach ($getReview as $key => $review) { 
            $name[] = $review->nick_name;
            $title[] = $review->title;
            $content[] = $review->content;
            $sku[] = $review->product_sku;
            $product_manufacturer[] = $review->product_manufacturer;
            $product_name[] = $review->product_name;
            // $product_img[] = str_replace("https:","",$review->product_thumbnail);
            $product_img[] = $review->photo_key;
            $reviewi_id[] = $review->id; 
          
        }

        
      
      $args = ['names' => $name, 'titles' => $title, 'contents' => $content, 'skus' => $sku, 'product_manufacturers' => $product_manufacturer, 'product_names' => $product_name, 'product_imgs' => $product_img, 'ids' => $reviewi_id];
      // dd($args);

        $html = view($this->template, $args)->render();
        return $html;
    }
}


