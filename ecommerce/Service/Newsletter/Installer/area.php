<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;

class area extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.area';


    public function __construct()
    {

    }

    public function getHtml($cols){

        $title = isset($cols['1'])? $cols['1'] : "&nbsp;";
        if($title == " "){
            $args['link'] = " ";
            $args['textColor'] = " ";
            $args['text'] = $title;
        }else{
            $textcolor = isset($cols['4'])? $cols['4'] : " ";
            if($textcolor){
                $args['textColor'] = $textcolor;
            }
            $link = isset($cols['3'])? $cols['3'] : " ";
            if($link == " "){
                $args['text'] = $title;
            }else{
                $args['text'] = "<a href=". $link ." target='_blank' style='text-decoration:none;'>". $title ."<img alt='' src=".'{{$open_image}}'." style='height:1px; line-height:18.5714px; width:1px' /></a>";
            }
        }

        $photo = isset($cols['2']) ?str_replace( " ", "", $cols['2']) : "x";
        if($photo == "x"){
            $args['img'] = " ";
        }elseif($photo == "o"){
            $args['img'] = "<img src='http://img.webike.tw/shopping/image/V-tag.png' style='height:15px; opacity:0.9; width:15px' />";
        }else{
            $args['img'] = "<img src=". $photo ." style='height:15px; opacity:0.9; width:15px' />";
        }

        $textAlign = isset($cols['5'])? $cols['5'] : 1;
        if($textAlign == 1){
             $args['textAlign'] =  "style='text-align:left;'";
        }elseif($textAlign == 2){
             $args['textAlign'] =  "style='text-align:center;'";
        }elseif($textAlign == 3){
             $args['textAlign'] =  "style='text-align:right;'";
        }else{
            $args['textAlign'] =  "style='text-align:left;'";
        }        

        $backgroundColor = isset($cols['6'])? $cols['6'] : " ";
        if($backgroundColor){
            $args['backgroundColor'] = $backgroundColor;
        }      



        $html = view($this->template , $args)->render();

        // import relate parameters to create HTML
        return $html;
    }
}


