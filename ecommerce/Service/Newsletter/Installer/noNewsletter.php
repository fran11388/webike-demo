<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter\Installer;
class noNewsletter extends BasicInstaller implements InstallerInterface
{
    public $name = '周報banner';
    protected $template = 'backend.pages.newsletter.install.installer.noNewsletter';


    public function __construct()
    {

    }

    public function getHtml($cols)
    {
        // dd($cols);
        
        

        $html = view($this->template)->render();

        // import relate parameters to create HTML
        // dd($html);
        return $html;
    }
}


