<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/28
 * Time: 上午 11:28
 */

namespace Ecommerce\Service\Newsletter;

use Ecommerce\Service\BaseService;
use Maatwebsite\Excel\Facades\Excel;
use Everglory\Models\Review;
use Everglory\Models\Newsletter;

class InstallerService extends BaseService
{
    protected $codes = [
        '版本' => 'vol',
        '週報banner' => 'MainBanner',
        '點數領取' => 'GetPoint',
        '精選SALE' => 'sale',
        '區塊標題' => 'area',
        '精選焦點話題' => 'focus',
        'Facebook-粉絲專頁' => 'sale',
        '推薦新品' => 'ProductList',
        'outletbanner' => 'sale',
        'OUTLET商品推薦' => 'ProductList',
        'BikeNews 海內外新聞' => 'BikeNews',
        'Review會員評論' => 'review',
        '摩托車市' => 'MotoMarket',
        '其他活動' => 'activity',
        '取消電子報' => 'noNewsletter'
    ];

    public function __construct()
    {

    }

    public function getInstaller($name) {
        $installer = null;
        // dd($name);
        if(isset($this->codes[$name])){
            $class_name = '\Ecommerce\Service\Newsletter\Installer\\' . $this->codes[$name];
            $installer = new $class_name;
        }else{
            $class_name = '\Ecommerce\Service\Newsletter\Installer\title';
            $installer = new $class_name;
        }
        return $installer;
    }

    public function getColsByExcelFile($file) {
        $excels = [];
        Excel::load($file, function($reader) use(&$excels){

                $objExcel = $reader->getExcel();
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                for ($row = 1; $row <= $highestRow; $row++)
                     {
                    //  Read a row of data into an array
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);
                    // str_replace( " ", "", $rowData[0][1]);
                    if(empty($rowData[0][0])){
                       // dd(123);
                    }else{
                     $excels[] = $rowData[0];   
                    }

                }
                

        }); 

            return $excels;
    }

    public function getReview($id){
        $reviews = Review::whereIN('id', $id)->get();
        return  $reviews;
    }

      
    public function addNewsletterByContent($content)
    {
        $date = date('Y-m-d H:i:s');
        $newsletter = new Newsletter;
        $newsletter->content = trim($content);
        $newsletter->name = "週報".$date;
        $newsletter->title = "週報".$date;
        $newsletter->save();
        return $newsletter;
    }
}