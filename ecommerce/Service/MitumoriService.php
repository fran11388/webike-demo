<?php

namespace Ecommerce\Service;

use Ecommerce\Core\WarehouseFactory\Program AS WarehouseFactory;
use Everglory\Models\Gift;
use Everglory\Models\Product;
use Everglory\Models\WarehouseQueue;

class MitumoriService extends BaseService
{
    Const CATEGORY_ID = 1627;
    Const SYNC_PARAM = [
        'type' => 'import'
    ];
    Const STATUS = [
        'untreated' => 0,
        'finish' => 1
    ];

    protected $warehouseQueues = [];
    protected $warehouseFactory;

    public function __construct()
    {
        $this->warehouseFactory = new WarehouseFactory;
    }

    public function add($data)
    {
        $queueData = $this->getQueueData($data);
        if(!$queueData){
            return false;
        }
        $this->warehouseQueues[] = $this->warehouseFactory->addWarehouseQueue($queueData);

        return $this;
    }

    public function get()
    {
        if(\Config::get('app.production') === true) {
            $result = $this->warehouseFactory->sync(self::SYNC_PARAM);
            if ($result->success) {
                $warehouseQueues = collect($this->warehouseQueues);
//                $warehouseQueues = WarehouseQueue::where('id', 1010)->get();
                $this->warehouseQueues = $this->warehouseFactory->reloadWarehouseQueue($warehouseQueues->pluck('id')->toArray());
                $supplierService = new SupplierService;
                $products = Product::with('supplier', 'categories')->whereIn('id', $this->warehouseQueues->pluck('product_id')->toArray())->get();
                foreach ($this->warehouseQueues as $warehouseQueue) {
                    $product = $products->first(function ($product) use ($warehouseQueue) {
                        return $product->id == $warehouseQueue->product_id;
                    });
                    $supplierService->addDomesticProductSupplier($product);
                }
                return $this->warehouseQueues;

            } else {
                return null;
            }
        }else{
            return Product::with('supplier')->where('active', 1)->where('is_main', 1)->orderBy('id', 'desc')->limit(3)->get();
        }
    }

    private function getQueueData($data)
    {
        $queueData = new \StdClass;
        $attributes = null;
        if($data instanceof Gift){
            $attributes = $data->getAttributes();
            $queueData->name = $data->name;
            $product = $data->origProduct;
            $queueData->model_number = $product->model_number;
            $queueData->manufacturer_id = $product->manufacturer_id;
            $queueData->custom_manufacturer = 0;
            $queueData->price = 0;
            $queueData->cost = $product->cost;

            foreach ($attributes as $attribute => $value){
                $queueData->$attribute = $value;
            }
        }else if($data instanceof \Eloquent){
            $attributes = $data->getAttributes();
            foreach ($attributes as $attribute => $value){
                $queueData->$attribute = $value;
            }
            $queueData->name = $queueData->name . '(未登錄商品)';
        }else if(is_array($data) or is_object($data)){
            $attributes = $data;
            foreach ($attributes as $attribute => $value){
                $queueData->$attribute = $value;
            }
            $queueData->name = $queueData->name . '(未登錄商品)';
        }

        if(empty($queueData)){
            return false;
        }


        $queueData->category_id = self::CATEGORY_ID;
        $queueData->status = self::STATUS['untreated'];
        return $queueData;
    }



}