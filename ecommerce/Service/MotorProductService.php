<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/04/26
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Accessor\MotorProductAccessor;
use Ecommerce\Repository\MotorRepository;

class MotorProductService extends BaseService
{

    public function __construct()
    {
    }

    public static function getNewestMotorProduct($limit = NULL, $motor_model_id = NULL)
    {
        $result = [];
        if($motor_model_id){
            $motorProductModels = MotorRepository::getNewMotors($motor_model_id, $limit);
        }else{
            $motorProductModels = MotorRepository::getNewMotors(null, $limit);
        }
        foreach ($motorProductModels as $motorProductModel){
            $result[] = new MotorProductAccessor($motorProductModel);
        }
        return collect($result);
    }

    public static function getNewestMotorProductOfCustomer($limit = NULL)
    {
        $result = [];
        //$exclude array is a bug not fix, we use this to pass & release
        $motorProductModels = MotorRepository::getNewestMotorsUseGroup($limit, 'customer_id', [10754, 10755, 10756, 10757, 10762, 10763]);
        foreach ($motorProductModels as $motorProductModel){
            $result[] = new MotorProductAccessor($motorProductModel);
        }
        return collect($result);
    }

    public static function getNewMotorsByManufacturer($url_rewrite)
    {
        $result = [];
        $motorProductModels = MotorRepository::getNewMotorsByManufacturer($url_rewrite);
        foreach ($motorProductModels as $motorProductModel){
            $result[] = new MotorProductAccessor($motorProductModel);
        }
        return collect($result);
    }

}
