<?php

namespace Ecommerce\Service\Contact\categories;


class OrderRemittance extends FaqCategoryBasic
{

    protected $filters = [ 'RemittancePay' => 'OrderRemittanceRemittancePayFaq','NORemittancePay' => 'OrderRemittanceNORemittancePayFaq'];


    public function getfaq()
    {
        if(isset($this->data['order']) && $this->data['order']){
            $filter = $this->filter($this->filters,$this->data['order']);
            $answer = $this->getAnswer($filter);
        }else{
            $answer = $this->getChooseAnswer(); 
        }
        return $answer;
    }

    
}

?>