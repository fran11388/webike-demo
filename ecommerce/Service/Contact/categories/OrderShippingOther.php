<?php

namespace Ecommerce\Service\Contact\categories;


class OrderShippingOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderShippingOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>