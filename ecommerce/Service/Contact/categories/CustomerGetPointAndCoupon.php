<?php

namespace Ecommerce\Service\Contact\categories;


class CustomerGetPointAndCoupon extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'CustomerGetPointAndCouponNoCheckFaq'];

    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>