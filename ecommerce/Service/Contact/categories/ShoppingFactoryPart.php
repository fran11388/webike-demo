<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 上午 10:57
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingFactoryPart extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingFactoryPartNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}