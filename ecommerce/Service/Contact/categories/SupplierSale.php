<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierSale extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierSaleNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>