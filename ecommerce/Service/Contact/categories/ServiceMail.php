<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceMail extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceMailNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>