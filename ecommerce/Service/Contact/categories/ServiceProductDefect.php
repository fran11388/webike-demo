<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceProductDefect extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceProductDefectNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>