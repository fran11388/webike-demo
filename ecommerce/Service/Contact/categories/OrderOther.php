<?php

namespace Ecommerce\Service\Contact\categories;


class OrderOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>