<?php

namespace Ecommerce\Service\Contact\categories;


class CustomerOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'CustomerOtherNoCheckFaq'];

    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>