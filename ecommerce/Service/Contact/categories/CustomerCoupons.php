<?php

namespace Ecommerce\Service\Contact\categories;


class CustomerCoupons extends FaqCategoryBasic
{

    protected $filters = [ 'LoginCheck' => 'CustomerCouponsLoginCheckFaq'];

    public function getfaq()
    {

        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        
        return $answer;
    }

    
}

?>