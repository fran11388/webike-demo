<?php

namespace Ecommerce\Service\Contact\categories;

use Ecommerce\Service\Quote\Exception;

class FaqCategoryBasic
{
    protected $data = [];
    
    public function filter($filters,$params = null)
    {
        foreach ($filters as $check_class => $faq_class) {
            $check_class_path = '\Ecommerce\Service\Contact\check\\'. $check_class;
            $check = new $check_class_path;
            $check_result = $check->check($params);
            if($check_result){
                return $faq_class;
            }
        }
        $category=get_called_class();
        $message="$category's every check been fail";
        \Log::critical($message);
        return null;
    }

    public function setParams($data)
    {

        $this->data = $data;
    }

    public function getAnswer($faq_class)
    {
        $answer = null;

        if($faq_class){
            $faq_class_path = '\Ecommerce\Service\Contact\faq\\'. $faq_class;
            if(class_exists($faq_class_path)){
                $faq = new $faq_class_path;
                $ticket_faq = $faq->getFaq();
                $faq->setParams($this->data);
                $answer = $faq->arrange($ticket_faq->faq_text);

            }
        }
        return $answer;
    }

    public function getChooseAnswer(){

        $answer = ["faq_text" => "請選擇欲詢問訂單，選擇完成後再次幫您查詢。","default" => 1,"choose" => '0'];
        return $answer;
    }


}

?>