<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceProductLack extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceProductLackNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>