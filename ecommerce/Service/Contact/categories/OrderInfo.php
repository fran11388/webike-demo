<?php

namespace Ecommerce\Service\Contact\categories;


class OrderInfo extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderInfoNocheckFaq'];


    public function getfaq()
    {
        if(isset($this->data['order']) && $this->data['order']){
            $filter = $this->filter($this->filters,$this->data['order']);
            $answer = $this->getAnswer($filter);
        }else{
            $answer = $this->getChooseAnswer(); 
        }
        return $answer;
    }

    
}

?>