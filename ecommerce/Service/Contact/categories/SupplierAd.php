<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierAd extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierAdNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>