<?php

namespace Ecommerce\Service\Contact\categories;


class DealerService extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'DealerServiceNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>