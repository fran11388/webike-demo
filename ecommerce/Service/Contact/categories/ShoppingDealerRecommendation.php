<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 上午 09:38
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingDealerRecommendation  extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingDealerRecommendationNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}