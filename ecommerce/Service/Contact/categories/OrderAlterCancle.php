<?php

namespace Ecommerce\Service\Contact\categories;


class OrderAlterCancle extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderAlterCancleNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>