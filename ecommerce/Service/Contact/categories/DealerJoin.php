<?php

namespace Ecommerce\Service\Contact\categories;


class DealerJoin extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'DealerJoinNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>