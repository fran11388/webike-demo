<?php

namespace Ecommerce\Service\Contact\categories;


class OrderCreditiCard extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderCreditiCardNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>