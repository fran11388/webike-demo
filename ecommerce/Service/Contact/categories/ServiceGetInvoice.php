<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceGetInvoice extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceGetInvoiceNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>