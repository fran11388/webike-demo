<?php

namespace Ecommerce\Service\Contact\categories;


class DealerOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'DealerOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>