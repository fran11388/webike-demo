<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceProductDamaged extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceProductDamagedNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>