<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceChangeInvoice extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceChangeInvoiceNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>