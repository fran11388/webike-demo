<?php

namespace Ecommerce\Service\Contact\categories;


class DealerOffer extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'DealerOfferNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>