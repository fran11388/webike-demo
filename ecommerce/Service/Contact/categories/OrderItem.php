<?php

namespace Ecommerce\Service\Contact\categories;


class OrderItem extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderItemNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>