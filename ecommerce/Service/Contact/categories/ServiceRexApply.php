<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceRexApply extends FaqCategoryBasic
{

    protected $filters = [ 'OrderCancle' => 'ServiceRexApplyOrderCancleFaq','OrderNoFinishAndNOCancle' => 'ServiceRexApplyOrderNoFinishAndNOCancleFaq','FreeReturn' => 'ServiceRexAccountingFreeReturnFaq','NoFreeReturn' => 'ServiceRexApplyNoFreeReturnFaq'];


    public function getfaq()
    {
    	if(isset($this->data['order']) && $this->data['order']){
	        $filter = $this->filter($this->filters,$this->data['order']);
	        $answer = $this->getAnswer($filter);
    	}else{
    		$answer = $this->getChooseAnswer();	
    	}
        return $answer;
    }

    
}

?>