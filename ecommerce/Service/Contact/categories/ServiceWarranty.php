<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceWarranty extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceWarrantyNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>