<?php

namespace Ecommerce\Service\Contact\categories;


class SystemDeviant extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SystemDeviantNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>