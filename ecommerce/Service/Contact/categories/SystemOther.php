<?php

namespace Ecommerce\Service\Contact\categories;


class SystemOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SystemOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>