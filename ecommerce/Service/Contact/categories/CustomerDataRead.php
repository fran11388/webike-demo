<?php

namespace Ecommerce\Service\Contact\categories;


class CustomerDataRead extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'CustomerDataReadNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>