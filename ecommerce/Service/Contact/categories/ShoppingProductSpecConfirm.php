<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 下午 03:59
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingProductSpecConfirm extends FaqCategoryBasic
{
    protected $filters = [
        'NotFromProduct' => 'ShoppingProductSpecConfirmNotFromProductFaq',
        'FromProductNoQa' => 'ShoppingProductSpecConfirmFromProductNoQaFaq',
        'FromProductHasQa' => 'ShoppingProductSpecConfirmFromProductHasQaFaq',
    ];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}