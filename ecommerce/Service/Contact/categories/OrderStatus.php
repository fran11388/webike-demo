<?php

namespace Ecommerce\Service\Contact\categories;


class OrderStatus extends FaqCategoryBasic
{

    protected $filters = [

        'NewOrder' => 'OrderStatusNewOrderFaq','OrderWaitPay' => 'OrderStatusOrderWaitPayFaq','OrderNoSale' => 'OrderStatusOrderNoSaleFaq','OrderInPurchase' => 'OrderStatusOrderInPurchaseFaq','OrderAberrant' => 'OrderStatusOrderAberrantFaq','OrderCancle' => 'OrderStatusOrderCancleFaq','OrderWaitShipping' => 'OrderStatusOrderWaitShippingFaq','OrderHctShipping' => 'OrderStatusOrderHctShippingFaq','OrderHctFinish' => 'OrderStatusOrderHctFinishFaq'];


    public function getfaq()
    {
        if(isset($this->data['order']) && $this->data['order']){
            $filter = $this->filter($this->filters,$this->data['order']);
            $answer = $this->getAnswer($filter);
        }else{
            $answer = $this->getChooseAnswer(); 
        }
        return $answer;
    }

    
}

?>