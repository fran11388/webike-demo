<?php

namespace Ecommerce\Service\Contact\categories;


class SystemSlow extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SystemSlowNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>