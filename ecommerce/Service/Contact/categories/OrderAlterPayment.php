<?php

namespace Ecommerce\Service\Contact\categories;


class OrderAlterPayment extends FaqCategoryBasic
{

    protected $filters = [ 'OrderCancle' => 'OrderAlterPaymentOrderCancleFaq','OrderShipping' => 'OrderAlterPaymentOrderShippingFaq','OrderNoShippingPaymethod' => 'OrderAlterPaymentOrderNoShippingPaymethodFaq', 'OrderPaid' => 'OrderAlterPaymentOrderPaidFaq'];


    public function getfaq()
    {
        if(isset($this->data['order']) && $this->data['order']){
            $filter = $this->filter($this->filters,$this->data['order']);
            $answer = $this->getAnswer($filter);
        }else{
            $answer = $this->getChooseAnswer(); 
        }
        return $answer;
    }

    
}

?>