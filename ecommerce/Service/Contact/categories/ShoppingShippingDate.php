<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/21
 * Time: 上午 11:13
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingShippingDate extends FaqCategoryBasic
{
    protected $filters = [
        'NotFromProduct' => 'ShoppingShippingDateNotFromProductFaq',
        'FromProductOtherOptionsHasStock' => 'ShoppingShippingDateFromProductOtherOptionsHasStockFaq',
        'FromProductHasStock' => 'ShoppingShippingDateFromProductHasStockFaq',
        'FromProductHasOptionsNoStock' => 'ShoppingShippingDateFromProductHasOptionsNoStockFaq',
        'FromProductNoOptionsNoStock' => 'ShoppingShippingDateFromProductNoOptionsNoStockFaq',
        'FromProductHasOptionsSoldOut' => 'ShoppingShippingDateFromProductHasOptionsSoldOutFaq',
        'FromProductNoOptionsSoldOut' => 'ShoppingShippingDateFromProductNoOptionsSoldOutFaq',
        'FromProductHasOptionsDiscontinued' => 'ShoppingShippingDateFromProductHasOptionsDiscontinuedFaq',
        'FromProductNoOptionsDiscontinued' => 'ShoppingShippingDateFromProductNoOptionsDiscontinuedFaq',
    ];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}