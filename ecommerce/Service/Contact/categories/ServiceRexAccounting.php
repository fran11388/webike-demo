<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceRexAccounting extends FaqCategoryBasic
{

    protected $filters = [ 'NoRex' => 'ServiceRexAccountingNoRexFaq','RexReturn' => 'ServiceRexAccountingRexReturnFaq','RexChange' => 'ServiceRexAccountingRexChangeFaq'];


    public function getfaq()
    {

    	if(isset($this->data['order']) && $this->data['order']){
	        $filter = $this->filter($this->filters,$this->data['order']);
	        $answer = $this->getAnswer($filter);
    	}else{
    		$answer = $this->getChooseAnswer();	
    	}
        return $answer;
    }

    
}

?>