<?php

namespace Ecommerce\Service\Contact\categories;


class OrderChangePayment extends FaqCategoryBasic
{

    protected $filters = ['OrderCancle' => 'OrderChangePaymentOrderCancleFaq','OrderShipping' => 'OrderChangePaymentOrderShippingFaq','OrderNoShippingDelivery' => 'OrderChangePaymentOrderNoShippingDeliveryFaq','OrderNoShippingBankTransfer' => 'OrderChangePaymentOrderNoShippingBankTransferFaq','OrderNoShippingCreditiCardPay' => 'OrderChangePaymentOrderNoShippingCreditiCardPayFaq','OrderNoShippingPaid' => 'OrderChangePaymentOrderNoShippingPaidFaq'];


    public function getfaq()
    {
        if(isset($this->data['order']) && $this->data['order']){
            $filter = $this->filter($this->filters,$this->data['order']);
            $answer = $this->getAnswer($filter);
        }else{
            $answer = $this->getChooseAnswer(); 
        }
        return $answer;
    }

    
}

?>