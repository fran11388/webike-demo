<?php

namespace Ecommerce\Service\Contact\categories;


class OrderSeparate extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderSeparateNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>