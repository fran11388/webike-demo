<?php

namespace Ecommerce\Service\Contact\categories;


class SystemLogin extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SystemLoginNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>