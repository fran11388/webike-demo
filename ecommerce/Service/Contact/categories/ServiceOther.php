<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>