<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceWinningInvoice extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceWinningInvoiceNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>