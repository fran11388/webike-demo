<?php

namespace Ecommerce\Service\Contact\categories;


class OrderAlterReward extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderAlterRewardNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>