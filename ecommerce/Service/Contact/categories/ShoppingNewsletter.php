<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 下午 03:20
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingNewsletter extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingNewsletterNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}