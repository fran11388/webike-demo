<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierPloy extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierPloyNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>