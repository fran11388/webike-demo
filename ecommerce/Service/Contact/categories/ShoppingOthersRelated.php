<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 下午 03:16
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingOthersRelated extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingOthersRelatedNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}