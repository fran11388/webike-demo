<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 下午 02:43
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingOfferRelated extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingOfferRelatedNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}