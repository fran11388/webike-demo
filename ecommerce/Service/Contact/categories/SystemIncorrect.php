<?php

namespace Ecommerce\Service\Contact\categories;


class SystemIncorrect extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SystemIncorrectNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>