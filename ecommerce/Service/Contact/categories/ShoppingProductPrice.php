<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 上午 11:00
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingProductPrice extends FaqCategoryBasic
{
    protected $filters = [
        'IsDealer' => 'ShoppingProductPriceIsDealerFaq',
        'NotDealer'=>'ShoppingProductPriceNotDealerFaq',
    ];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}