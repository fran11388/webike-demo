<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 上午 11:17
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingMethod extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingMethodNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}