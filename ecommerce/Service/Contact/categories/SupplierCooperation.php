<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierCooperation extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierRacingEventNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>