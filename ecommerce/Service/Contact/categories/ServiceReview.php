<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceReview extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceReviewNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>