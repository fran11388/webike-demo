<?php

namespace Ecommerce\Service\Contact\categories;


class CustomerPoint extends FaqCategoryBasic
{

    protected $filters = [ 'LoginCheck' => 'CustomerPointLoginCheckFaq'];

    public function getfaq()
    {

        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        
        return $answer;
    }

    
}

?>