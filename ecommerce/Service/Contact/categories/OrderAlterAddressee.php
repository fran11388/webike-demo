<?php

namespace Ecommerce\Service\Contact\categories;


class OrderAlterAddressee extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'OrderAlterAddresseeNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>