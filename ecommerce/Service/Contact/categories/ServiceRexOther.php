<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceRexOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceRexOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>