<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierOther extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierOtherNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>