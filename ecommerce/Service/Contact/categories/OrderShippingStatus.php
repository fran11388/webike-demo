<?php

namespace Ecommerce\Service\Contact\categories;


class OrderShippingStatus extends FaqCategoryBasic
{

    protected $filters = [ 'OrderNoShipping' => 'OrderShippingStatusOrderNoShippingFaq','OrderCancle' => 'OrderShippingStatusOrderCancleFaq','OrderWaitShipping' => 'OrderShippingStatusOrderWaitShippingFaq','OrderHctShipping' => 'OrderShippingStatusOrderHctShippingFaq','OrderHctFinish' => 'OrderShippingStatusOrderHctFinishFaq'];


    public function getfaq()
    {
        if(isset($this->data['order']) && $this->data['order']){
            $filter = $this->filter($this->filters,$this->data['order']);
            $answer = $this->getAnswer($filter);
        }else{
            $answer = $this->getChooseAnswer(); 
        }
        return $answer;
    }

    
}

?>