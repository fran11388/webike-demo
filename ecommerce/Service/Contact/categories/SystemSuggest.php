<?php

namespace Ecommerce\Service\Contact\categories;


class SystemSuggest extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SystemSuggestNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>