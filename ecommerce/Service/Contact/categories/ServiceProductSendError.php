<?php

namespace Ecommerce\Service\Contact\categories;


class ServiceProductSendError extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'ServiceProductSendErrorNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>