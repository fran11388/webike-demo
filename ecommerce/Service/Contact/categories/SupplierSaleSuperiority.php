<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierSaleSuperiority extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierSaleSuperiorityNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>