<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 下午 02:52
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingElectronicInvoice extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingElectronicInvoiceNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}