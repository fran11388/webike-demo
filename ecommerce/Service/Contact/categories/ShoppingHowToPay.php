<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/19
 * Time: 下午 02:38
 */

namespace Ecommerce\Service\Contact\categories;


class ShoppingHowToPay extends FaqCategoryBasic
{
    protected $filters = [ 'NoCheck' => 'ShoppingHowToPayNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }
}