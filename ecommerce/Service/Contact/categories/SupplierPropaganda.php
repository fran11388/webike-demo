<?php

namespace Ecommerce\Service\Contact\categories;


class SupplierPropaganda extends FaqCategoryBasic
{

    protected $filters = [ 'NoCheck' => 'SupplierPropagandaNoCheckFaq'];


    public function getfaq()
    {
        $filter = $this->filter($this->filters);
        $answer = $this->getAnswer($filter);
        return $answer;
    }

    
}

?>