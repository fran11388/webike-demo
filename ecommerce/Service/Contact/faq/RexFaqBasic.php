<?php

namespace Ecommerce\Service\Contact\faq;

use Everglory\Models\TicketFaq;
use Everglory\Models\Rex;

class RexFaqBasic extends FaqBasic
{
	protected function setRexHistoriesParams($data)
	{
		
    	$rex = Rex::where('parent_id',$data['order'])->with(['histories' => function($query){
    		$query->orderBy('created_at', 'desc');
    	}])->first();

    	$this->params['rex_histories'] = $rex->histories;
    	
	}


}