<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/21
 * Time: 上午 11:31
 */

namespace Ecommerce\Service\Contact\faq;


class ShoppingShippingDateFromProductNoOptionsHasStockFaq extends ProductFaqBasic
{
    protected $faq_code = 51;

    public function setParams($data){

    	$this->setProductStockInfoParams($data);

    }
}