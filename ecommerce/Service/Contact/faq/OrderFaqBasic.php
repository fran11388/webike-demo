<?php

namespace Ecommerce\Service\Contact\faq;

use Everglory\Models\TicketFaq;
use Everglory\Models\Order;
use Everglory\Constants\OrderStatus;
use Ecommerce\Service\ContactService;




class OrderFaqBasic extends FaqBasic
{
	protected function setOrderIncrementIDParams($data)
	{
    	$order = Order::where('id',$data['order'])->first();
    	
		$this->params['increment_id'] = $order->increment_id;

	}

	protected function setOrderHctShippingParams($data)
	{
    	$order = Order::where('id',$data['order'])->with(['histories' => function($query){
    		$query->where('status_id',OrderStatus::SHIPPING);
    	}])->first();

		$this->params['shipping_number'] = $order->tw_shipping;

		$this->params['day'] = $order->histories['0']->created_at;

	}

	protected function setOrderHctShippingFinishParams($data)
	{
    	$order = Order::where('id',$data['order'])->with(['histories' => function($query){
    		$query->where('status_id',OrderStatus::FINISH);
    	}])->first();

		$this->params['shipping_number'] = $order->tw_shipping;

		$this->params['day'] = $order->histories['0']->created_at;

	}

	protected function setOrderPaymentMethodParams($data)
	{
    	$order = Order::where('id',$data['order'])->with('payment')->first();
    	
		$this->params['payment_method'] = $order->payment->name;

	}

	protected function setOrderDeliveryDayParams($data)
	{
    	$order = Order::where('id',$data['order'])->with('payment')->first();
    	
		$this->params['delivery_day'] = $order->delivered_at;

	}

	protected function setOrderTicketParams()
	{

		\Request::replace(["type_url_rewrite" => "order"]);

    	$ContactService = new ContactService();
    	$collection = $ContactService->getList();



		$this->params['collection'] = $collection;

	}

}