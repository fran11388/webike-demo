<?php

namespace Ecommerce\Service\Contact\faq;

use Everglory\Models\TicketFaq;
use Ecommerce\Accessor\CustomerAccessor;

class PointFaqBasic extends FaqBasic
{
	protected function setPointParams()
	{
		$CustomerAccessor = new CustomerAccessor(\Auth::user());
    	$points_current = $CustomerAccessor->getCurrentPoints();
    	$this->params['points_current'] = $points_current;
	}


}