<?php

namespace Ecommerce\Service\Contact\faq;

use Everglory\Models\TicketFaq;
use Everglory\Models\Coupon;

class CouponFaqBasic extends FaqBasic
{
	protected function setCouponParams()
	{
    	$coupons = Coupon::whereNull('order_id')
        ->where('customer_id', \Auth::user()->id)
        ->where('expired_at', '>', date('Y-m-d'))
        ->get();
    	$this->params['coupons'] = $coupons;
	}


}