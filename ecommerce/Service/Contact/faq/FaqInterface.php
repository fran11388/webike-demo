<?php

namespace Ecommerce\Service\Contact\faq;

interface FaqInterface
{

	public function getFaq();

	public function setParams($data);

	public function arrange($text);
	


}