<?php

namespace Ecommerce\Service\Contact\faq;

use Everglory\Models\TicketFaq;
use Ecommerce\Service\ProductService;

class QaFaqBasic extends FaqBasic
{
	protected function setQaParams($data)
	{
		$sku = $data['skus'];
		$product = ProductService::getProductDetail($sku);
        $qa = $product->getQa();
        $this->params['sku'] = $sku;
        $this->params['answers'] = $qa;
        
	}


}