<?php

namespace Ecommerce\Service\Contact\faq;

use Everglory\Models\TicketFaq;

class FaqBasic Implements FaqInterface
{
	protected $faq_code = null;
	protected $params = [];
	protected $default = '';
	protected $data = [];

	public function getFaq(){
		$ticketFaq = TicketFaq::where('faq_code',$this->faq_code)->where('active',1)->first();
		$this->default = $ticketFaq->default;
		return $ticketFaq;
	}

	public function setParams($data)
	{
		$this->data = $data;
	}
	
	public function arrange($text){
		$faq_text = parseBladeCode($text,$this->params);
		$answer = ["faq_text" => $faq_text,"default" => $this->default];
    	return $answer;
	}


}