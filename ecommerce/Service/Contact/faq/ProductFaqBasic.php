<?php

namespace Ecommerce\Service\Contact\faq;

class ProductFaqBasic extends FaqBasic
{
	protected function setProductStockInfoParams($data)
	{
		$sku = $data['skus'];
    	$api_result = file_get_contents(config('api.stock_query') . "?sku=$sku");
        $api_result = json_decode($api_result);
        $this->params['stock_info'] = $api_result->$sku->stock_info;
	}


}