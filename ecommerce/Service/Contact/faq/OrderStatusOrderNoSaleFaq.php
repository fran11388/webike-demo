<?php

namespace Ecommerce\Service\Contact\faq;

use App\Components\View\SimplePagerComponent;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;

class OrderStatusOrderNoSaleFaq extends OrderFaqBasic
{
	protected $faq_code = 74;

	public function setParams($data)
	{
		$this->setOrderTicketParams();
	}

	public function arrange($text){


    	$listIntegration = new ListIntegration($this->params['collection'], 'Ticket');


    	$SimplePagerComponent = new SimplePagerComponent();
	    $SimplePagerComponent->setCollection($listIntegration->collection->sortByDesc('created_at'),true);
	    $this->params['collection'] = $SimplePagerComponent->collection;

		$faq_text = parseBladeCode($text,$this->params);
		$answer = ["faq_text" => $faq_text,"default" => $this->default];
    	return $answer;
	}


}