<?php

namespace Ecommerce\Service\Contact;

class FaqService
{
    protected $categories = [
        45 => 'CustomerDataRead',
        46 => 'CustomerDataUpdate',
        140 => 'CustomerPoint',
        141 => 'CustomerCoupons',
        142 => 'CustomerGetPointAndCoupon',
        51 => 'CustomerOther',
        52 => 'SystemLogin',
        53 => 'SystemSlow',
        54 => 'SystemIncorrect',
        55 => 'SystemDeviant',
        56 => 'SystemSuggest',
        137 => 'SystemOther',
        143 => "DealerJoin",
        144 => "DealerOffer",
        138 => "DealerService",
        64 => "DealerOther",
        147 => "SupplierSale",
        148 => "SupplierSaleSuperiority",
        70 => "SupplierAd",
        71 => "SupplierPropaganda",
        72 => "SupplierPloy",
        73 => "SupplierRacingEvent",
        74 => "SupplierCooperation",
        139 => "SupplierOther",
        132 => "ServiceProductDamaged",
        133 => "ServiceProductDefect",
        134 => "ServiceProductLack",
        135 => "ServiceProductSendError",
        112 => "ServiceRexProductStatus",
        113 => "ServiceRexApply",
        114 => "ServiceRexAccounting",
        115 => "ServiceRexOther",
        150 => "ServiceChangeInvoice",
        152 => "ServiceGetInvoice",
        153 => "ServiceWinningInvoice",
        41 => "ServiceOther",
        81 => "ServiceMail",
        136 => "ServiceWarranty",
        154 => "ServiceReview",
        2 => "OrderInfo",
        3 => "OrderStatus",
        157 => "OrderChangePayment",
        6 => "OrderCreditiCard",
        7 => "OrderRemittance",
        9 => "OrderItem",
        128 => "OrderSeparate",
        10 => "OrderAlterPayment",
        11 => "OrderAlterAddressee",
        12 => "OrderAlterReward",
        13 => "OrderAlterCancle",
        122 => "OrderShippingStatus",
        124 => "OrderShippingOther",
        15 => "OrderOther",
        20 => 'ShoppingDealerRecommendation',
        83 => 'ShoppingFactoryPart',
        22 => 'ShoppingMethod',
        23 => 'ShoppingLogisticsProcess',
        24 => 'ShoppingHowToPay',
        25 => 'ShoppingOfferRelated',
        156 => 'ShoppingElectronicInvoice',
        129 => 'ShoppingMotoMarket',
        130 => 'ShoppingOthersRelated',
        131 => 'ShoppingNewsletter',
        26 => 'ShoppingOtherProblems',
        155 => 'ShoppingProductPrice',
        18 => 'ShoppingProductSpecConfirm',
        19 => 'ShoppingShippingDate',
    ];

    public function __construct()
    {

    }

    public function getFaqCategoryClassName($category_id)
    {
        if (isset($this->categories[$category_id])){
            return $this->categories[$category_id];
        }

        return null;
    }


}

?>