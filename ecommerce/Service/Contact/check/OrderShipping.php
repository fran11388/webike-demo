<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class OrderShipping
{
    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if(in_array($order->status_id,[OrderStatus::WAIE_SHIPPING,OrderStatus::SHIPPING,OrderStatus::FINISH])){
 			return true;
    	}else{
    		return false;
    	}

    }
}