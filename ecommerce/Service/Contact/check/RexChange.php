<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Rex;

class RexChange
{

    public function check($order_id){
    	$rex = Rex::where('parent_id',$order_id)->first();
    	if($rex->type == 'exchange'){
    		return true;
    	}else{
    		return false;
    	}
    }
}