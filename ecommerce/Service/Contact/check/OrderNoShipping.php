<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class OrderNoShipping
{
    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if(in_array($order->status_id,[OrderStatus::NEW_ORDER,OrderStatus::ESTIMATE,OrderStatus::WAIT_PAY,OrderStatus::WAIT_REPLIER,OrderStatus::WAIT_PURCHASE,OrderStatus::PURCHASE,OrderStatus::INTO_STOCK,OrderStatus::NEW_ORDER_ACCEPT,OrderStatus::ESTIMATE_CHECK,OrderStatus::PAY_ABERRANT,OrderStatus::ORDER_ABERRANT,OrderStatus::ORDER_RETAIN])){
 			return true;
    	}else{
    		return false;
    	}

    }
}