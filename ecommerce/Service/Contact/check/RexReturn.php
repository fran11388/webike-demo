<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Rex;

class RexReturn
{

    public function check($order_id){
    	$rex = Rex::where('parent_id',$order_id)->first();
    	if($rex->type == 'return' || $rex->type == 'return_all' ){
    		return true;
    	}else{
    		return false;
    	}
    }
}