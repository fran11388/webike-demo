<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class OrderInPurchase
{

    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if(in_array($order->status_id, [OrderStatus::PURCHASE,OrderStatus::INTO_STOCK])){
 			return true;
    	}else{
    		return false;
    	}



    }
}