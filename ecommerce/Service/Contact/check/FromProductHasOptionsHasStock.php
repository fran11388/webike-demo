<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 下午 05:37
 */

namespace Ecommerce\Service\Contact\check;


use Everglory\Models\Stock;

class FromProductHasOptionsHasStock
{
    public function check()
    {

        if (request()->has('skus')) {
            $sku = request()->get('skus');
            $product = \Everglory\Models\Product::where('sku', $sku)->first();
            if (!$product->group_code) {
                return false;
            }
            $api_result = file_get_contents(config('api.stock_query') . "?sku=$sku");
            $api_result = json_decode($api_result);


            if ($api_result->$sku->stock > 0){
                return true;
            } 

        }
        return false;

    }
}