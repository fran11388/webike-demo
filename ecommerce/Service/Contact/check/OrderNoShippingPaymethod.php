<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\Payment;

use Ecommerce\Service\Contact\check\OrderNoShipping;

class OrderNoShippingPaymethod
{
    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	$OrderNoShipping = new OrderNoShipping;

    	$OrderNoShipping_check = $OrderNoShipping->check($order_id);


    	if($OrderNoShipping_check and in_array($order->payment_method,[Payment::CASH_ON_DELIVERY,Payment::Bank_Transfer,Payment::CREDIT_CARD_LAYAWAY_ESUN,Payment::CREDIT_CARD_LAYAWAY_NCCC])){
 			return true;
    	}else{
    		return false;
    	}


    }
}