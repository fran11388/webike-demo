<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/21
 * Time: 上午 10:48
 */

namespace Ecommerce\Service\Contact\check;




class FromProductNoOptionsSoldOut
{
    public function check()
    {
        if (request()->has('skus')) {
            $sku = request()->get('skus');
            $product = \Everglory\Models\Product::where('sku', $sku)->first();
            if ($product->group_code != null){
                return false;
            }
            
            $api_result = file_get_contents(config('api.stock_query') . "?sku=$sku");
            $api_result = json_decode($api_result);
            $sold_out = $api_result->$sku->sold_out;
            $discontinued = $api_result->$sku->discontinued;
            if (!$sold_out){
                return false;
            }

            return true;
        }
        return false;
    }
}