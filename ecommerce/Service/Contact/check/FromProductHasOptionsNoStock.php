<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/21
 * Time: 上午 09:24
 */

namespace Ecommerce\Service\Contact\check;



use Everglory\Models\Stock;

class FromProductHasOptionsNoStock
{
    public function check()
    {
        if (request()->has('skus')) {
            $sku = request()->get('skus');
            $product = \Everglory\Models\Product::where('sku', $sku)->first();
            if ($product->group_code == null) return false;

            $api_result = file_get_contents(config('api.stock_query') . "?sku=$sku");
            $api_result = json_decode($api_result);
            $can_estimate = $api_result->$sku->can_estimate;
            if (!$can_estimate) return false;

            return true;
        }
        return false;

    }
}