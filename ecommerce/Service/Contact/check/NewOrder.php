<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class NewOrder
{

    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if(in_array($order->status_id, [OrderStatus::NEW_ORDER,OrderStatus::NEW_ORDER_ACCEPT,OrderStatus::ESTIMATE,OrderStatus::ESTIMATE_CHECK,OrderStatus::WAIT_PURCHASE])){
 			return true;
    	}else{
    		return false;
    	}



    }
}