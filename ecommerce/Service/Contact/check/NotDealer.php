<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 上午 11:10
 */

namespace Ecommerce\Service\Contact\check;


class NotDealer
{
    public function check(){
        if(\Auth::check()){
            if(\Auth::user()->role_id==3) return false;
        }

        return true;
    }
}