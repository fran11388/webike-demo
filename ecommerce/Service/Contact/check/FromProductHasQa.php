<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 下午 03:50
 */

namespace Ecommerce\Service\Contact\check;


use Ecommerce\Service\ProductService;

class FromProductHasQa
{
    public function check()
    {
        if (request()->has('skus')) {
            $sku = request()->get('skus');
            $product = ProductService::getProductDetail($sku);
            $qa = $product->getQa();
            if (!$qa->isEmpty()) {
                return true;
            }
        }
        return false;

    }
}