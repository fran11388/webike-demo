<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 下午 03:37
 */

namespace Ecommerce\Service\Contact\check;


class NotFromProduct
{
    public function check()
    {
        if (request()->has('skus')){
        	return false;
        }
        return true;
    }
}