<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class OrderWaitShipping
{
    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if($order->status_id == OrderStatus::WAIE_SHIPPING){
 			return true;
    	}else{
    		return false;
    	}

    }
}