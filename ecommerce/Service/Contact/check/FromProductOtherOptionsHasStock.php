<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/20
 * Time: 下午 05:37
 */

namespace Ecommerce\Service\Contact\check;


use Everglory\Models\Stock;

class FromProductOtherOptionsHasStock
{
    public function check()
    {
        if (request()->has('skus')) {
            $sku = request()->get('skus');
            $stock = Stock::where('sku', $sku)->first();
            if ($stock != null) {
                return false;
            }

            $product = \Everglory\Models\Product::where('sku', $sku)->first();
            if ($product->group_code == null) {
                return false;
            } else {
                $products = \Everglory\Models\Product::where('group_code', $product->group_code)
                    ->where('sku','<>',$sku)
                    ->get();
                $stock = Stock::whereIn('product_id', $products->pluck('id'))->first();
                if($stock==null) {
                    return false;
                }
            }

            return true;
        }
        return false;

    }
}