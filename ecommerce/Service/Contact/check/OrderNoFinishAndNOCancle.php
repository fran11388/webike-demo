<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class OrderNoFinishAndNOCancle
{

    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if(in_array($order->status_id, [OrderStatus::ORDERCANCLE,OrderStatus::CUSTOMER_CANCLE,OrderStatus::FINISH])){
    		return false;
    	}else{
 			return true;
    	}



    }
}