<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\OrderStatus;


class OrderAberrant
{

    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if(in_array($order->status_id, [OrderStatus::PAY_ABERRANT,OrderStatus::ORDER_ABERRANT,OrderStatus::ORDER_RETAIN])){
 			return true;
    	}else{
    		return false;
    	}



    }
}