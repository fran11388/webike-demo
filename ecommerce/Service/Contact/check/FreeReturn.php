<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order\History;

use Everglory\Constants\OrderStatus;
use \Carbon\Carbon;

class FreeReturn
{

    public function check($order_id){

    	$finish_history = History::where('order_id',$order_id)->where('status_id',10)->first();

    	$dt = Carbon::parse('-10 days 00:00:00');

    	if(strtotime($finish_history->created_at->toDateString()) > strtotime($dt->toDateTimeString())){
    		return true;
    	}else{
    		return false;
    	}

    }
}