<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Rex;

class HasRex
{

    public function check($order_id){
    	$rex = Rex::where('parent_id',$order_id)->first();
    	
    	if($rex){
    		return true;
    	}else{
    		return false;
    	}
    }
}