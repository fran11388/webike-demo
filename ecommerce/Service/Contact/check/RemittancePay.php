<?php

namespace Ecommerce\Service\Contact\check;

use Everglory\Models\Order;

use Everglory\Constants\Payment;


class RemittancePay
{

    public function check($order_id){
    	$order = Order::where('id',$order_id)->first();

    	if($order->payment_method == Payment::Bank_Transfer){
 			return true;
    	}else{
    		return false;
    	}

    }
}