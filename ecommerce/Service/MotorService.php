<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Motor;
use Ecommerce\Repository\MotorRepository;
use Everglory\Models\Motor\Catalogue;
use URL;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Core\Solr\Response\SearchResponse;

class MotorService extends BaseService
{
    private $motor;

    /**
     * Return motor's information from database.
     *
     * MotorService constructor.
     * @param $url_rewrite Motor's url_rewrite
     * @param array $relations
     */
    public function __construct($url_rewrite, $relations = [])
    {
        $motor = Motor::where('url_rewrite',$url_rewrite);
        if($relations and count($relations)){
            $motor = $motor->with($relations);
        }
        $this->motor = $motor->firstOrFail();
    }

    public function getServiceData($catalogue_id)
    {
        $motor = $this->motor;
        $catalogue = Catalogue::with(['colors','strings','floats','flags','integers','groups']);
        if($catalogue_id){
            $catalogue = $catalogue->find($catalogue_id);
        }else{
            $catalogue = $catalogue->where('model_code',$motor->url_rewrite)->orderBy('model_release_year','desc')->first();
        }
        return $catalogue;
    }

    public function getSeries()
    {
        //can use cache
        $series = Catalogue::where('model_code',$this->motor->url_rewrite)
            ->orderBy('model_release_year','desc')
            ->orderBy('model_group_top','desc')
            ->orderBy('model_catalogue_id','desc')
            ->get()
            ->groupBy('model_release_year');
        return $series;
    }

    public function getSeriesByCatalogueImage($currentCatalogue, $seriesCollection)
    {
        $currentSeries = null;
        $current_images = [];
        if($currentCatalogue){
            foreach ($seriesCollection as $year => $currentSeries){
                foreach ($currentSeries as $currentCatalogue){
                    $current_images[$year] = [];
                    if($currentCatalogue->colors){
                        foreach ($currentCatalogue->colors as $catalogueColor){
                            $current_images[$year][] = $catalogueColor->image;
                        }
                    }else{
                        if(count($currentSeries)){
                            foreach ($currentSeries as $catalogue){
                                if($catalogue->model_image_file){
                                    $current_images[$year][] = RCJ_IMAGE_URL . $catalogue->model_image_url . 'L_' . $catalogue->model_image_file;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $current_images;
    }

    public function getCurrentCatalogueImages($currentCatalogue, $seriesCollection)
    {
        $currentSeries = null;
        $current_images = [];

        if($currentCatalogue){
            foreach ($seriesCollection as $series){
                $loopCurrentCatalogue = $series->first(function($catalogue) use($currentCatalogue){
                    return $catalogue->model_catalogue_id == $currentCatalogue->model_catalogue_id;
                });
                if($loopCurrentCatalogue){
                    $currentSeries = $series;
                }
            }

            if($currentCatalogue->colors){
                foreach ($currentCatalogue->colors as $catalogueColor){
                    $current_images[] = $catalogueColor->image;
                }
            }else{
                if(count($currentSeries)){
                    foreach ($currentSeries as $catalogue){
                        if($catalogue->model_image_file){
                            $current_images[] = RCJ_IMAGE_URL . $catalogue->model_image_url . 'L_' . $catalogue->model_image_file;
                        }
                    }
                }
            }
        }
        return $current_images;
    }

    public function getMotor()
    {
        return $this->motor;
    }

    public function getSpecifications()
    {
        return $this->motor->specifications->sortByDesc('release_year');
    }

    /**
     * Get the genuineparts of manufacturer link by specific motor.
     *
     * @return null|string
     */
    public function getGenuineSalesLink()
    {
        $motor = $this->motor;
        $genuinepartsService = new GenuinepartsService;
        $divides = $genuinepartsService->divideImportManufacturers();

        foreach ($divides as $divide => $data){
            $url_rewrite = $data['url_code'];
            $group = collect($data['group'])->pluck('api_usage_name')->toArray();

            if(in_array($motor->manufacturer->name, $group)){
                return URL::route('genuineparts-divide', [$url_rewrite, $motor->manufacturer->name]);
            }
        }
        return null;
    }

    public function getMotorInformation(SearchResponse $search_response,$new_product_count)
    {
        $result = new \stdClass();
        $result->total = $search_response->getGroupResponse()->getCount();
        $result->new = $new_product_count;
        $result->maintenance = 0;
        $maintenance_category  = CategoryRepository::find('4000');
        $maintenance = $search_response->getFacetFieldResponses()['url_path']->getDocs()->get($maintenance_category->mptt->url_path);
        if($maintenance){
            $result->maintenance = $maintenance;
        }
        return $result;
    }

    public function  getMotorFivePower()
    {
        $motor = $this->motor;
        $url_rewrite = $motor->url_rewrite;
        if(is_numeric($url_rewrite)){
            $modelInfo = \Cache::rememberForever('index.modelInfo[' . $url_rewrite . ']' ,function() use( &$url_rewrite) {
                $check_time = 0;
                $modelInfo = "";
                $url = "http://www.webike.net/api/modelInfo.json?model=" . $url_rewrite . "&charset=utf8";
                // 檢查錯誤
                $ch=curl_init();
                do {
                    $check_time++;
                    sleep($check_time);
                    curl_setopt($ch, CURLOPT_URL,$url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Your application name');
                    $modelInfo = curl_exec($ch);
                } while (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 and $check_time < 2 );

                curl_close($ch);
                $modelInfo = json_decode($modelInfo);
                return $modelInfo;
            });
        }else{
            $modelInfo = null;
        }
        return $modelInfo;

    }

    public function getMotorTagNews()
    {
        $testConnection = testServerConnection('eg_news');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $news = \Everglory\Models\News\Post::join('wp_term_relationships', 'ID', '=', 'wp_term_relationships.object_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
            ->where('post_status', 'publish')
            ->where('post_type', 'post')
            ->where('term_order', 0)
            ->where('wp_terms.name', trim($this->motor->name))
            ->orderBy('post_date', 'DESC')
            ->groupBy('ID')
            ->take(3)
            ->get();
        return $news;

    }

    public function getMotorVideo($current_motor,$manufacturer)
    {
        $results = new \stdClass();
        $countrys = array('tw' => 'zh-Hans','jp' => 'ja');
    
        $current_motor_name = $current_motor->name;
        $current_motor_synonym = $current_motor->synonym;
        $brand = $manufacturer;
        $current_motor_synonym = explode(',', $current_motor_synonym);
        foreach ($current_motor_synonym as $key => $value) {
           $current_motor_synonym[$key] = $brand.$value;
        }
        $current_motor_synonym = implode('|', $current_motor_synonym);
        $keyword = $current_motor_synonym.'|'.$brand.$current_motor_name;
        foreach($countrys as $country => $countrylanguage){
            switch ($country) {
                case 'tw':
                    $title[] = '台灣車友影片';
                    break;
                case 'jp':
                    $title[] = '日本車友影片';
                    break;
            }
            $api_key = 'AIzaSyCawMNQwZtvCaay7Y36xo9eVpa8vUM-QYo';
            $language = $countrylanguage;
            $limit = 20;
            $sortresult = \Request::get('sort');
            $sort[] = $sortresult;
            $request = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=".$keyword."&relevanceLanguage=".$language."&maxResults=".$limit."&key=".$api_key;

            if( $sortresult ){
                $request = $request."&order=".$sortresult;
            }
            if(get_http_response_code($request) == '200'){
                try{
                    $response = file_get_contents($request);
                    $data[] = json_decode($response, TRUE);
                    $result[] = 'true';
                    $results->data = $data;

                }catch(\Exception $e){
//                    \Log::warning(\Request::url());
//                    \Log::warning('YoutubeAPI失效');
                    $result[] = 'false';
                }
            }else{
                $result[] = 'false';
            }
        }
        $results->title = $title;
        $results->result = $result; 
        $results->sort = $sort;
        return (array)$results;
    }

    public function getPopularMotors($limit = 6){
        return MotorRepository::getPopularMotors($limit , $this->motor);
    }

    public function getActiveSpecification()
    {
        return $this->motor->specifications->filter(function($specification){
            return $specification->status;
        })->first();
    }
}