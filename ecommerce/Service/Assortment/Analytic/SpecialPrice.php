<?php

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Repository\ManufacturerRepository;
use Everglory\Constants\SEO;
use Everglory\Models\Mptt;

class SpecialPrice extends AnalyticBase implements AnalyticInterface
{
    protected $require = [
        'product_url_rewrites',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
                $ca_brands = $data->ca_brand;
                $results = new \StdClass;
                $results->layout = null;
                $results->categories = $data->categories;
                $data_categories = $data->categories;

                $results->tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                $results->manufacturers = $this->getManufacturersByUrlRewrite($data->manufacturer_url_rewrites);
                $category_repository = new CategoryRepository;
                $categories = $category_repository->getCategory($data->categories);
                
                $product_repository = new ProductRepository;
                foreach($categories as  $category){
                    $url_rewrite = $category->url_rewrite;
                    $cache_key = 'sprcailprice'. $url_rewrite;
                    $category_products[$url_rewrite] = \Cache::tags(['sprcailprice'])->remember($cache_key, 86400 , function () use($category,$product_repository,$data,$ca_brands,$url_rewrite){
                        return $product_repository->getChildCategoriesProducts($category,true,null,1000,$ca_brands->$url_rewrite);
                    });
                }
                if(isset($data->layout)){
                    $results->layout = $data->layout;
                }
                $results->products =  $category_products;

            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}