<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;

class RainGear extends AnalyticBase implements AnalyticInterface
{

    public function __construct($item)
    {
        parent::__construct($item);
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {   
        if($this->verifyData($data)){
            $cache_key = 'rainGear_assortment_item_'.$this->item->id;
            $result = new \stdClass;
            $products = \Cache::tags(['RainGear'])->remember($cache_key, 86400 , function () use($data){
                $page_type = new \stdClass;
                foreach($data->page_array as $type){
                    $page_group = $data->$type;
                    $products = [];
                    if(is_array($page_group)){
                        foreach($page_group as $key => $skus_group){
                            $skus_group_array = (array)$skus_group;
                            $skus = array_keys($skus_group_array);
                            $product_models = ProductRepository::getProductWithRelations($skus,['prices','productDescription','options']);
                            $product_models = $product_models->sortBy(function($product) use($skus){
                              return array_search($product->url_rewrite, $skus);
                            });
                            foreach ($product_models as $model_key => $product_model){
                                $product = new ProductAccessor($product_model,true);
                                $product_url_rewrite = $product_model->url_rewrite;
                                $product->link = $skus_group->$product_url_rewrite->link;
                                $product->colors = $skus_group->$product_url_rewrite->colors;
                                $product->main_images = $skus_group->$product_url_rewrite->main_images;
                                $product->other_images = $skus_group->$product_url_rewrite->other_images;
                                $product->description = $skus_group->$product_url_rewrite->description;
                                $products[$key][] = $product;
                            }
                        }
                        $page_type->$type = $products;
                    }
                }

                return $page_type;
            });
            $result->assortment = $this->item->assortment;
            $result->page_types = $data->page_array;
            $result->data = $products;
            $result->description = $data->description;
            $result->current_customer = \Auth::check() ? \Auth::user() : null;
            return view($template->blade, (array)$result)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}