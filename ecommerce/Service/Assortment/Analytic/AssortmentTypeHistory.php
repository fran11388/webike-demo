<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Service\AssortmentService;
use StdClass;

class AssortmentTypeHistory extends AnalyticBase implements AnalyticInterface
{
    protected $relations;

    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'type'
        ];
        $this->require_ch = [
            'type' => '特輯類型'
        ];
        $this->quantity = [
            'type' => ['特輯類型' => 'single']
        ];

    }

    public function getCols($template = null)
    {
        $getRequire = $this->getRequire();
        $result = new \stdClass();
        $result->columns = $getRequire->require_ch;
        $result->quantity = $getRequire->quantity;

        return $result;
    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $results = $data;
            if(!is_object($results)){
                $results = new StdClass;
            }

            if(!isset($results->title)){
                $results->title = '特輯總覽';
            }

            $service = new AssortmentService();
            $type = \Cache::tags(['assortments-video'])->rememberForever('assortments-video-type',function() use($service , $results) {
                return $service->getCollectionTypesByUrlRewrite($results->type, ['assortments'])->first();
            });

            $results->assortments_histories = $service->getAssortmentByPublishAt(8,[],'DESC',$type->id);
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }

    public function getExample($template, $cols)
    {
        $results = new \stdClass;
        foreach ($cols->quantity as $col => $info){
            if($col == 'product_url_rewrites'){
                $results->products = $this->getProductsByUrlRewrite(['22878861']);
            }elseif($col == 'image'){
                $results->image = 'https://img.webike.net/catalogue/images/7837/mfk-195_02.jpg';
            }else {
                $results->$col = array_keys($info)[0] . '範例';
            }

            $service = new AssortmentService();
            $type = \Cache::tags(['assortments-video'])->rememberForever('assortments-video-type',function() use($service) {
                return $service->getTypesByUrlRewrite('video', ['assortments'])->first();
            });
            $results->assortments_histories = $service->getAssortmentByPublishAt(8,[],'DESC',$type->id);
        }


        return view($template->blade, (array)$results)->render();
    }
}