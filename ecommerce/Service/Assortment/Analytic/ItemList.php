<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Everglory\Models\Product;
use Everglory\Models\Manufacturer;
use Ecommerce\Service\ProductService;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Repository\ManufacturerRepository;

class ItemList extends AnalyticBase implements AnalyticInterface
{
    protected $relations;

    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'title',
            'select',
            'url_rewrite'
        ];
        $this->relations = [
            // 'manufacturer',
            'images',
            // 'prices',
            // 'points',
//            'motors',
//            'options',
            'productDescription',
        ];
        $this->relation = [
            
        ];
    }

    public function getCols($template)
    {
        $results = $data;
        $results->current_customer = $this->current_customer;
        $results->products = [];
        $products = ProductRepository::getProductWithRelations($data->product_url_rewrites, $this->relations);
        $products = $products->sortBy(function($product) use($data){
            return array_search($product->url_rewrite, $data->product_url_rewrites);
        });
        foreach ($products as $product){
            $results->products[] = new ProductAccessor($product);
        }
        return view($template->blade, (array)$results)->render();

    }
    public function read($data, $template)
    {
        
        if($this->verifyData($data)){
            $results = $data;
            if($results->select == 'brand'){
                $infos = ManufacturerRepository::getManufacturerWithRelations($data->url_rewrite,$this->relation);
                $infos = $infos->sortBy(function($manufacturer) use($data){
                    return array_search($manufacturer->url_rewrite,$data->url_rewrite);
                });
                foreach($infos as $manufacturer){
                    $results->infos[] = $manufacturer;
                }
                // dd($results);
            }else{
                $infos = ProductRepository::getProductWithRelations($data->url_rewrite, $this->relations);
                $infos = $infos->sortBy(function($product) use($data){
                    return array_search($product->url_rewrite, $data->url_rewrite);
                });
                foreach ($infos as $product){
                    $results->infos[] = new ProductAccessor($product);
                }
            }
            // compact('a', 'b', 'c')
            // ['a' => $a, 'b' => $b, 'c' => $c];
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}