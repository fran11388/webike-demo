<?php

namespace Ecommerce\Service\Assortment\Analytic;

interface AnalyticInterface {

    public function getCols($template);
    public function read($data, $template);
    public function write($data);
    public function render($template);
}
