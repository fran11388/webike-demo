<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;

class Gallery extends AnalyticBase implements AnalyticInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'image',
            'description'
        ];
        $this->require_ch = [
            'image' => '圖片',
            'description' => '敘述'
        ];
        $this->quantity = [
            'image' => ['圖片' => 'mutiple'],
            'description' => ['敘述' => 'mutiple']
        ];
    }

    public function getCols($template = null)
    {
        $getRequire = $this->getRequire();
        $result = new \stdClass();
        $result->columns = $getRequire->require_ch;
        $result->quantity = $getRequire->quantity;

        return $result;
    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            return view($template->blade, (array)$data)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }

    public function getExample($template, $cols)
    {
        $results = new \stdClass;
        foreach ($cols->quantity as $col => $info){
            if($col == 'product_url_rewrites'){
                $results->products = $this->getProductsByUrlRewrite(['22878861']);
            }elseif($col == 'image'){
                $results->image[] = 'https://img.webike.net/catalogue/images/7837/mfk-195_02.jpg';
            }elseif($col == 'description'){
                $results->description[] = '敘述範例';
            }else {
                $results->$col = array_keys($info)[0] . '範例';
            }
        }

        return view($template->blade, (array)$results)->render();
    }
}