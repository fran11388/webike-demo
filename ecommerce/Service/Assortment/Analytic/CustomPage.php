<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;

class CustomPage extends AnalyticBase implements AnalyticInterface
{

    public function __construct()
    {
        parent::__construct();
        $this->require = [
        ];
        $this->require_ch = [
        ];
        $this->quantity = [
            'single_html' => ['任意輸入' => 'single'],
        ];
    }

    public function getCols($template = null)
    {
        $getRequire = $this->getRequire();
        $result = new \stdClass();
        $result->columns = $getRequire->require_ch;
        $result->quantity = $getRequire->quantity;

        return $result;
    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            if(isset($data->single_html)){
                return view('response/pages/collection/assortment/base/single-html', (array)$data)->render();    
            }
            return view($template->blade, (array)$data)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }

    public function getExample($template, $cols)
    {
        $results = new \stdClass;
        foreach ($cols->quantity as $col => $info){
            if($col == 'product_url_rewrites'){
                $results->products = $this->getProductsByUrlRewrite(['22878861']);
            }elseif($col == 'image'){
                $results->image = 'https://img.webike.net/catalogue/images/7837/mfk-195_02.jpg';
            }else {
                $results->$col = array_keys($info)[0] . '範例';

            }
        }

        return view($template->blade, (array)$results)->render();
    }
}