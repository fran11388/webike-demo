<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;

class ScaleBox extends AnalyticBase implements AnalyticInterface
{
    protected $require = [
            'rows.*.*.scale',
            'rows.*.*.content',
        ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            return view($template->blade, (array)$data)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}