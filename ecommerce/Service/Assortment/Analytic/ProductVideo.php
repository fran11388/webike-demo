<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;

class ProductVideo extends AnalyticBase implements AnalyticInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'video',
            'description'
        ];
        $this->require_ch = [
            'video' => '影片嵌入網址',
            'description' => '敘述'
        ];
        $this->quantity = [
            'video' => ['影片嵌入網址' => 'single'],
            'description' => ['敘述' => 'single']
        ];
    }

    public function getCols($template = null)
    {
        $getRequire = $this->getRequire();
        $result = new \stdClass();
        $result->columns = $getRequire->require_ch;
        $result->quantity = $getRequire->quantity;

        return $result;
    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            return view($template->blade, (array)$data)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }

    public function getExample($template, $cols)
    {
        $results = new \stdClass;
        foreach ($cols->quantity as $col => $info){
            if($col == 'video'){
                $results->video = 'https://www.youtube.com/embed/11H5_bz_VVs';
            }elseif($col == 'description'){
                $results->description = '敘述範例';
            }
        }

        return view($template->blade, (array)$results)->render();
    }
}