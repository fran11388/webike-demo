<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Repository\ManufacturerRepository;

class ManufacturerList extends AnalyticBase implements AnalyticInterface
{
    protected $relations;

    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'manufacturer_url_rewrites',
        ];
        $this->relations = [

        ];
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $results = $data;
            $results->products = [];
            if(!isset($results->title)){
                $results->title = '品牌列表';
            }
            $manufacturers = ManufacturerRepository::getManufacturerWithRelations($data->manufacturer_url_rewrites, $this->relations);
            $manufacturers = $manufacturers->sortBy(function($manufacturer) use($data){
                return array_search($manufacturer->url_rewrite, $data->manufacturer_url_rewrites);
            });
            $results->manufacturers = $manufacturers;
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}