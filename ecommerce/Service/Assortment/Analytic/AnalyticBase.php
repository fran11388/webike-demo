<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Auth;
use Cache;
use Ecommerce\Service\SearchService;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Repository\ManufacturerRepository;

class AnalyticBase
{
    protected $item;
    protected $cache_group;
    protected $require;
    protected $require_ch;
    protected $quantity;
    protected $solr_compare;
    protected $solr_service;
    protected $current_customer;

    public function __construct()
    {
        $this->require = [];
        $this->solr_compare = [
            'mt' => 'motor',
            'ca' => 'category',
            'br' => 'manufacturer',
        ];
        $this->current_customer = null;
        if(Auth::check()){
            $this->current_customer = Auth::user();
        }
    }

    public function getRequire()
    {
        $result = new \stdClass();
        $result->require_ch = $this->require_ch;
        $result->quantity = $this->quantity;
        return $result;
    }

    public function setItem($item) 
    {
        $this->item = $item;
    }

    public function verifyData($data)
    {
        $is_verify = true;
        foreach ($this->require as $column){
            $columns = explode('.',$column);
            $data_check  = clone $data;
            foreach ($columns as $col){
                if($col == '*'){
                    $data_check = current($data_check);
                    continue;
                }else if(is_object($data_check)){
                    if(isset($data_check->$col)) {
                        $data_check = $data_check->$col;
                    }else{
                        $is_verify = false;
                    }
                }else if(is_array($data_check)){
                    if(isset($data_check[$col])) {
                        $data_check = $data_check[$col];
                    }else{
                        $is_verify = false;
                    }
                }
            }
            if(!$is_verify){
                return $is_verify;
            }
        }
        return $is_verify;
    }

    public function getCacheKey($parameters = null)
    {
        $assortment = $this->item->assortment;
        $role_id = 0;
        if(Auth::check()){
            $role_id = Auth::user()->role_id;
        }
        return 'assortment.' . $assortment->type->url_rewrite . '.' . $assortment->url_rewrite . '.' . 'role' . $role_id . '.' . http_build_query($parameters);
    }

    public function useSolrService()
    {
        $this->solr_service = app()->make(SearchService::class);
    }

    public function getSolrResponseWithCache()
    {
        $request = request();
        $search_response = $this->solr_service->selectList($request);
//        if(!Cache::get($this->getCacheKey($request->all()))){
//            $search_response = $this->solr_service->selectList($request);
//            Cache::add($this->getCacheKey($request->all()), $search_response, 1);
//        }else{
//            $search_response = Cache::get($this->getCacheKey($request->all()));
//        }
        return $search_response;
    }

    public function getProductsByUrlRewrite($product_url_rewrites, $relations = NULL)
    {
        if(!$relations){
            $relations = [
                'manufacturer',
                'images',
                'prices',
                'points',
//                'motors',
//                'options',
                'productDescription',
            ];
        }
        $results = [];
        $products = ProductRepository::getProductWithRelations($product_url_rewrites, $relations);
        $products = $products->sortBy(function($product) use($product_url_rewrites){
            return array_search($product->url_rewrite, $product_url_rewrites);
        });
        foreach ($products as $product){
            $results[] = new ProductAccessor($product);
        }
        return $results;
    }

    public function getManufacturersByUrlRewrite($manufacturer_url_rewrites, $relations = [])
    {
        $results = ManufacturerRepository::getManufacturerWithRelations($manufacturer_url_rewrites, $relations)->sortBy(function($manufacturer) use($manufacturer_url_rewrites){
            return current(array_keys($manufacturer_url_rewrites, $manufacturer->url_rewrite));
        });
        return $results;

    }
}