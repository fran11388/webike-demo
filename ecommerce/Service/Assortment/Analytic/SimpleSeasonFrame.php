<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Repository\ManufacturerRepository;
use Everglory\Constants\SEO;
use Everglory\Models\Mptt;

class SimpleSeasonFrame extends AnalyticBase implements AnalyticInterface
{
    protected $require = [
        'product_url_rewrites',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            if($data->type == '春夏'){
                $results = $data;
                $results->tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                $results->products = $this->getProductsByUrlRewrite($data->product_url_rewrites);
                $results->manufacturers = $this->getManufacturersByUrlRewrite($data->manufacturer_url_rewrites);
            }elseif($data->type == '秋冬'){
                $record_skus = [];
                $results = new \StdClass;
                $results->layout = null;
                $results->tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                $results->manufacturers = $this->getManufacturersByUrlRewrite($data->manufacturer_url_rewrites);
                $category_repository = new CategoryRepository;
                $categories = $category_repository->getCategory($data->categories);
                $product_repository = new ProductRepository;

                $categories_sort = $data->categories;
                $categories = $categories->sortBy(function($category,$key) use($categories_sort){
                    return array_search($category->url_rewrite,$categories_sort);
                });
                foreach($categories as $category){
                    $cache_key = '2017AW_data'.$category->id;
                    $category_products[$category->url_rewrite] = \Cache::tags(['2017AW'])->remember($cache_key, 86400 , function () use($category,$product_repository){
                        return $product_repository->getChildCategoriesProducts($category,true,'2018秋冬',1000);
                    });

                    if(count($category_products[$category->url_rewrite]) and !in_array($category->url_rewrite,[3022,3266,3059,1327])){
                        $random_product = $category_products[$category->url_rewrite]->random();
                        $record_skus[$random_product->url_rewrite] = $random_product->url_rewrite;
                        $results->products[] = $random_product;
                    }
                }

                foreach($categories as $category){
                    if(count($category_products[$category->url_rewrite]) < 4){
                        $max = count($category_products[$category->url_rewrite]);
                    }else if(in_array($category->url_rewrite,['3059','3266'])) {
                        $max = 2;
                    }else{
                        $max = 4;
                    }

                    if(count($category_products[$category->url_rewrite]) and count($category_products[$category->url_rewrite]) > $max){
                        for($num = 1;$num <= $max;$num++){
                            $random_product = $category_products[$category->url_rewrite]->random();
                            while(in_array($random_product->url_rewrite,$record_skus)){
                                $random_product = $category_products[$category->url_rewrite]->random();
                            }
                            $record_skus[$random_product->url_rewrite] = $random_product->url_rewrite;
                            $results->products[] = $random_product;
                        }
                    }
                }
                if(isset($data->layout)){
                    $results->layout = $data->layout;
                }

            }elseif($data->type == '2018春夏'){
                $record_skus = [];
                $results = new \StdClass;
                $results->layout = null;
                $results->tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                $results->manufacturers = $this->getManufacturersByUrlRewrite($data->manufacturer_url_rewrites);
                $category_repository = new CategoryRepository;
                $categories = $category_repository->getCategory($data->categories);
                $product_repository = new ProductRepository;
                
                $categories_sort = $data->categories;
                $categories = $categories->sortBy(function($category,$key) use($categories_sort){
                    return array_search($category->url_rewrite,$categories_sort);
                });

                foreach($categories as $category){
                    $cache_key = '2018SS_data'.$category->id;
                    $category_products[$category->url_rewrite] = \Cache::tags(['2018SS'])->remember($cache_key, 86400 , function () use($category,$product_repository,$data){
                            $attribute_tag = '2018春夏';
                            $brands = [];
                            if(in_array($category->id,[1597,305,290])){
                                $brands = $data->manufacturer_url_rewrites;
                            }elseif($category->id == 1609){
                                $attribute_tag = false;
                            }
                        return $product_repository->getChildCategoriesProducts($category,true,$attribute_tag,1000,$brands);
                    });
                }
                
                if(isset($data->layout)){
                    $results->layout = $data->layout;
                }
                $results->products =  $category_products;
            }elseif($data->type == '2019秋冬'){
                $record_skus = [];
                $results = new \StdClass;
                $results->layout = null;
                $results->tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                $results->manufacturers = $this->getManufacturersByUrlRewrite($data->manufacturer_url_rewrites);
                $category_repository = new CategoryRepository;
                $categories = $category_repository->getCategory($data->categories);
                $product_repository = new ProductRepository;
                
                $categories_sort = $data->categories;
                $categories = $categories->sortBy(function($category,$key) use($categories_sort){
                    return array_search($category->url_rewrite,$categories_sort);
                });
                foreach($categories as $category){
                    $cache_key = '2019FF_data'.$category->id;
                    $category_products[$category->url_rewrite] = \Cache::tags(['2019FF'])->remember($cache_key, 86400 , function () use($category,$product_repository,$data){
                            $attribute_tag = '2019秋冬';
                            $brands = [];
                            if(in_array($category->id,[1597,305])){
                                $brands = $data->manufacturer_url_rewrites;
                            }elseif(in_array($category->id,[321,373,292,290])){
                                $attribute_tag = false;
                            }
                        return $product_repository->getChildCategoriesProducts($category,true,$attribute_tag,1000,$brands);
                    });
                }
                
                if(isset($data->layout)){
                    $results->layout = $data->layout;
                }
                $results->products =  $category_products;
            }elseif($data->type == '2019春夏'){
                $record_skus = [];
                $results = new \StdClass;
                $results->layout = null;
                $results->tail = ' - ' . SEO::WEBIKE_SHOPPING_LOGO_TEXT;
                $results->manufacturers = $this->getManufacturersByUrlRewrite($data->manufacturer_url_rewrites);
                $category_repository = new CategoryRepository;
                $categories = $category_repository->getCategory($data->categories);
                $product_repository = new ProductRepository;
                
                $categories_sort = $data->categories;
                $categories = $categories->sortBy(function($category,$key) use($categories_sort){
                    return array_search($category->url_rewrite,$categories_sort);
                });
                foreach($categories as $category){
                    $cache_key = '2019Spring_data'.$category->id;
                    $category_products[$category->url_rewrite] = \Cache::tags(['2019Spring_data'])->remember($cache_key, 86400 , function () use($category,$product_repository,$data){
                            $attribute_tag = '2019春夏';
                                $brands = $data->manufacturer_url_rewrites;
                            if(in_array($category->id,[1609,296])){
                                $attribute_tag = false;
                            }
                        return $product_repository->getChildCategoriesProducts($category,true,$attribute_tag,1000,$brands);
                    });
                }
                
                if(isset($data->layout)){
                    $results->layout = $data->layout;
                }
                $results->products =  $category_products;
            }

            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}