<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use App\Components\View\ListComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Service\ProductService;
use Ecommerce\Service\SupplierService;
use Ecommerce\Support\LengthAwarePaginator;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Support\PriceConverter;

class SolrProductSearch extends AnalyticBase implements AnalyticInterface
{

    public function __construct()
    {
        parent::__construct();
        $this->cache_group = 'SolrProductSearch';
        $this->require = [
            'interface',
            'segments', //for default filter
        ];
        $this->useSolrService();
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {

        if($this->verifyData($data)){

            $convertService = new ConvertService;
            $convertService->addParameterToRequest($data->segments);

            $categorylink = 0;
            if(request()->get('categorylink')){
                $categorylink = request()->get('categorylink');
                if(!request()->get('ca')){
                    request()->request->add(['price' => '1000-4000','sort' => 'visits','ca' => 3000]);
                }
            }

            $mybike = 0;
            if(request()->get('mybike')){
                $mybike = request()->get('mybike');
            }
            if(!request()->get('sort')){
                $convertService->addParameterToRequest(['sort' => 'new']);
            }
            $convertService->addParameterToRequest(request()->all());

            if(!request()->has('limit')){
                request()->request->add(['limit' => 40]); //強制預設40項
            }
            $search_response = $this->getSolrResponseWithCache();

            $convertService->restoreRequestParameter();
            $convertService->removeRequestParameter($data->segments);

            $results = $this->solr_service->getBaseResult($search_response);
            $listCompoment = new ListComponent;
            foreach(collect($listCompoment)->toArray() as $key => $value){
                if(!array_key_exists($key,collect($results)->toArray())){
                    $results->$key = $value;
                }
            }
            $results->current_customer = \Auth::user();
            $results->current_customer_mybike = null;
            if($results->current_customer && isset($results->current_customer->motors[0])){
                $results->current_customer_mybike = $results->current_customer->motors[0]->url_rewrite;
            }

            $results->categorylink = $categorylink;
            $results->mybike = $mybike;
            $results->products = $this->convertQueryResponseToProducts($search_response);
            $results->price_range = $this->convertFacetRangeResponseToPrices($search_response);
            $results->countries = $this->convertFacetRangeResponseToCountry($search_response);
            if(!isset($this->skus)){
                if($template->id == 6){
                    return null;
                }
                return view('response.pages.collection.assortment.base.solr.other-block.no-result', (array)$results)->render();
            }
            $domesticResult = SupplierService::isDomesticSupplier($this->skus, 'sku');
            $rcjSkus = [];
            foreach ($domesticResult as $sku => $isDomesticSupplier){
                if(!$isDomesticSupplier){
                    $rcjSkus[] = $sku;
                }
            }
            $results->tagData['jp_stocks'] = ProductService::getJpStockStatus($rcjSkus);
            $results->stock_informations = ProductService::getStockFlgForList($rcjSkus);
            $attributes = get_object_vars($data);
            unset($attributes['segments']);
            foreach ($attributes as $key => $value){
                $results->$key = $value;
            }

            $results->current_customer = $this->current_customer;
            if(in_array('products', $data->interface)){
                $options['path'] = '';
                foreach (request()->except('page') as $key => $value){
                    $options['query'][$key] = $value;
                }
                $results->pager = new LengthAwarePaginator($results->products, $results->count, request()->input('limit'), request()->input('page'), $options);
            }

            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }

    private function convertQueryResponseToProducts($search_response , $series = false)
    {
        if($docs =$search_response->getGroupResponse()){
            $docs = $docs->getDocs();
        }else{
            $docs = $search_response->getQueryResponse()->getDocs();
        }

        $result = [];
        foreach ($docs as $doc) {
            $this->skus[] = $doc->sku;
            $result[] = new ProductAccessor($doc);

        }

        if($series){
            return collect($result)->keyBy(function ($item, $key) {

                return $item->relation_product_id;
            });
        }

        return collect($result);
    }

    /**
     * @param SearchResponse $search_response
     * @return \Illuminate\Support\Collection
     */
    private function convertFacetRangeResponseToPrices($search_response)
    {

        $collection = PriceConverter::all();

        foreach ($collection as &$price) {

            foreach ($search_response->getFacetRangeResponses()['price_range']->getCount() as $key => $counts) {

                if ($price->min <= $key and $price->max >= $key) {
                    $price->count += $counts;
                }
            }
        }
        return $collection;

    }

    private function convertFacetRangeResponseToCountry($search_response)
    {
        $result = [];
        $object = new \stdClass();
        $object->name = '進口商品';
        $object->count = $search_response->getFacetQueryResponses()['import']->getCount();
        $object->key = 'import';
        $result[$object->key] = $object;

        $object = new \stdClass();
        $object->name = '國產商品';
        $object->count = $search_response->getFacetQueryResponses()['taiwan']->getCount();
        $object->key = 'taiwan';
        $result[$object->key] = $object;

        return collect($result);
    }

}