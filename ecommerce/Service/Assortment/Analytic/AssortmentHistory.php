<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Service\AssortmentService;
use StdClass;

class AssortmentHistory extends AnalyticBase implements AnalyticInterface
{
    protected $relations;

    public function __construct()
    {
        parent::__construct();
        $this->require = [
        ];

    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $results = $data;
            if(!is_object($results)){
                $results = new StdClass;
            }

            if(!isset($results->title)){
                $results->title = '特輯總覽';
            }
            $service = new AssortmentService();
            $assortments = $service->getAssortments(null, ['items', 'type'], true);
            $assortmentsGroup = $assortments->groupby(function($item, $key){
                return date('Y-m', strtotime($item->publish_at));
            });
            $results->assortments = $assortmentsGroup;
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}