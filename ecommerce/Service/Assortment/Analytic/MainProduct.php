<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Everglory\Models\Product;
use Ecommerce\Service\ProductService;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;

class MainProduct extends AnalyticBase implements AnalyticInterface
{
    protected $relations;

    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'product_url_rewrites',
        ];
        $this->require_ch = [
            'product_url_rewrites' => 'sku'
        ];
        $this->quantity = [
            'product_url_rewrites' => ['sku' => 'single'],
            'title' => ['標題' => 'single'],
            'image' => ['圖片' => 'single'],
            'description' => ['敘述' => 'single']
        ];
        $this->relations = [
            'manufacturer',
            'images',
            'prices',
            'points',
//            'motors',
//            'options',
            'productDescription',
        ];
    }

    public function getCols($template = null)
    {
        $getRequire = $this->getRequire();
        $result = new \stdClass();
        $result->columns = $getRequire->require_ch;
        $result->quantity = $getRequire->quantity;

        return $result;
    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $results = $data;
            $results->current_customer = $this->current_customer;
            $results->products = $this->getProductsByUrlRewrite($data->product_url_rewrites);
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }

    public function getExample($template, $cols)
    {
        $results = new \stdClass;
        $results->current_customer = \Auth::user();
        foreach ($cols->quantity as $col => $info){
            if($col == 'product_url_rewrites'){
                $results->products = $this->getProductsByUrlRewrite(['22878861']);
            }elseif($col == 'image'){
                $results->image = 'https://img.webike.net/catalogue/images/7837/mfk-195_02.jpg';
            }else {
                $results->$col = array_keys($info)[0] . '範例';
            }
        }

        return view($template->blade, (array)$results)->render();
    }
}