<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Repository\CategoryRepository;

class SolrCategoryList extends AnalyticBase implements AnalyticInterface
{

    public function __construct()
    {
        parent::__construct();
        $this->cache_group = 'SolrProductSearch';
        $this->require = [
            'segments', //for default filter
        ];
        $this->useSolrService();
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $convertService = new ConvertService;

            $convertService->addParameterToRequest($data->segments);
            $convertService->addParameterToRequest(request()->all());
            if(!request()->has('limit')){
                request()->request->add(['limit' => 40]); //強制預設40項
            }
            $search_response = $this->getSolrResponseWithCache();
            $convertService->restoreRequestParameter();
            $convertService->removeRequestParameter($data->segments);

            $results = $this->solr_service->getCategoryListResult($search_response);
            if(request()->input('ca')){
                $category = CategoryRepository::find(request()->input('ca'), 'url_path');
                $results->category = $category;
            }
            $attributes = get_object_vars($data);
            unset($attributes['segments']);
            foreach ($attributes as $key => $value){
                $results->$key = $value;
            }
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}