<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Everglory\Models\Product;
use Ecommerce\Service\ProductService;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Repository\CategoryRepository;

class CategoryList extends AnalyticBase implements AnalyticInterface
{
    protected $relations;

    public function __construct()
    {
        parent::__construct();
        $this->require = [
            'ca_url_path',
        ];
        $this->relations = [
        ];
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $results = $data;
            $results->current_customer = $this->current_customer;
            $results->categorys = [];
            $categorys = CategoryRepository::getCategory($data->ca_url_path, $this->relations);
            $categorys = $categorys->sortBy(function($category) use($data){
                return array_search($category->url_path, $data->ca_url_path);
            });
            foreach ($categorys as $category){
                $results->categorys[] = $category;
            }
            return view($template->blade, (array)$results)->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}