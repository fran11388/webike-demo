<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service\Assortment\Analytic;

use Ecommerce\Service\Assortment\Analytic\AnalyticBase;
use Ecommerce\Service\Assortment\Analytic\AnalyticInterface;
use Request;
use Everglory\Models\Assortment;

class ParentLink extends AnalyticBase implements AnalyticInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCols($template)
    {

    }
    public function read($data, $template)
    {
        if($this->verifyData($data)){
            $type = $this->item->assortment->type;
            return view($template->blade, ['type' => $type])->render();
        }
        return false;
    }
    public function write($data)
    {

    }
    public function render($template)
    {

    }
}