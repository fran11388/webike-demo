<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\SearchService;
use Ecommerce\Service\SummaryService;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class CarouselService extends BaseService
{
    const CACHE_LIFETIME = 86400;

    private $data;

    private $useCache = true;

    protected $search_service;
    protected $convert_service;
    protected $summary_service;
    protected $storage_data = [];

    public function __construct($data = [],&$storage_data = null)
    {
        $this->data = $data;

        $this->search_service = app()->make(SearchService::class);
        $this->convert_service = app()->make(ConvertService::class);
        $this->summary_service =  app()->make(SummaryService::class);

        foreach($data as $key => &$information){
            $params = $information->params;

            $call = $params->call;
            // EX: $call = riding_gear ; Execute riding_gear() function (=$this->riding_gear())
            $information->collection = $this->$call($params->parameter);
            $params->total = count($information->collection);
            
            if($params->shuffle == 'on'){
                $information->collection = $information->collection->shuffle();
            }
            if(isset($params->page) and $params->page){
                $collection = $information->collection;
                $information->collection = [];
                if(isset($params->key)){ // non ajax (first call)
                    if($params->total >= $params->limit * $params->page){
                        $quantity_per_row = round($params->total / $params->page);
                        $params->total = $quantity_per_row;
                        for($i=1;$i<=$params->page;$i++){
                            if($params->limit){
                                $information->collection[$i] = $collection->slice($quantity_per_row * ($i - 1), $params->limit);
                            }else{
                                $information->collection[$i] = $collection->slice($quantity_per_row * ($i - 1),$params->total);
                            }
                        }
                    }else{
                        $params->page = 1;
                        if($params->limit){
                            $information->collection[1] = $collection->take($params->limit);
                        }else{
                            $information->collection[1] = $collection;
                        }
                    }
                }else{
                    $information->collection[$params->page] = $collection->slice($params->limit * ($params->page - 1),$params->limit);
                }
            }else{
                unset($params->page); // total less then limit, cancel page funtion
                if($params->limit){
                    $information->collection = $information->collection->take($params->limit);
                }
            }
        }
        $storage_data = $this->storage_data;
        $this->data = $data;
    }


    public function call($key = null){
        if($key){
            return $this->data[$key];
        }else{
            return $this->data;    
        }
        
    }

    /**
     * Get ridingGear products in the previous 4 weeks.
     *
     * @param null $parameter
     * @return Collection|mixed|static
     */
    private function riding_gear($parameter = null)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $this->useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }
//        $date = '2018-07-01';
//        $date = \Carbon\Carbon::now()->subWeeks(4)->toDateString();
//        request()->request->add(['created_from' => $date ]);
        $riding_gear_search_response  = $this->search_service->selectRidingGear(request());
//        request()->request->remove('created_from');
        $result = $this->convert_service->convertQueryResponseToProducts($riding_gear_search_response);
        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }

    /**
     * Get new outlet products.
     *
     * @param null $parameter
     * @return Collection|mixed|static
     */
    private function outlet($parameter = null)
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $this->useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }

        request()->request->add(['type' => 5 ]);
        $search_response  = $this->summary_service->selectNewProduct(request());
        request()->request->remove('type');
        $result = $this->convert_service->convertQueryResponseToProducts($search_response);


        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }

    /**
     * Get specific category products.
     *
     * @param null $parameter
     * @return Collection|mixed|static
     */
    private function category($parameter = null)
    {

        $cache_key = self::getCacheKey(true);
        if (useCache() and $this->useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $url_path = $parameter['url_path'];

        request()->request->add(['category' => $url_path ]);
//        $search_response  = $this->summary_service->selectSummary(request());
        $search_response  = $this->summary_service->selectTopSales(request());
        $result = $this->convert_service->convertQueryResponseToProducts($search_response);


        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }

    private function new_products($parameter = null)
    {

        foreach ($parameter  as $key => $value ) {
            request()->request->add([$key => $value ]);
        }
        $cache_key = self::getCacheKey(true);
        if (useCache() and $this->useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $new_product_search_response = $this->summary_service->selectNewProduct(request());

        $new_product_count = $new_product_search_response->getGroupResponse()->getCount();
        $this->storage_data['new_product_count'] = $new_product_count;

        if($new_product_count < 28){
            $result = $this->convert_service->convertQueryResponseToProducts($this->summary_service->selectNewProduct(request(),false));
        }else{
            $result = $this->convert_service->convertQueryResponseToProducts($new_product_search_response);
        }

        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }
    
    private function popular_products($parameter = null)
    {
        foreach ($parameter  as $key => $value ) {
            request()->request->add([$key => $value ]);
        }
        request()->request->add(['limit' => 30]);

        $cache_key = self::getCacheKey(true);
        if (useCache() and $this->useCache and $result = \Cache::get($cache_key)) {
            return $result;
        }

        $new_product_search_response = $this->summary_service->selectTopSales(request());

        $new_product_count = $new_product_search_response->getGroupResponse()->getCount();
        $this->storage_data['new_product_count'] = $new_product_count;

        if($new_product_count < 28){
            $result = $this->convert_service->convertQueryResponseToProducts($this->summary_service->selectTopSales(request(),false));
        }else{
            $result = $this->convert_service->convertQueryResponseToProducts($new_product_search_response);
        }
        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        return $result;
    }



}