<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:25
 */

namespace Ecommerce\Service;

use Ecommerce\Core\Solr\Param\CommonParam;
use Ecommerce\Core\Solr\Param\EdismaxQueryParam;
use Ecommerce\Core\Solr\Param\FacetFieldParam;
use Ecommerce\Core\Solr\Param\FacetQueryParam;
use Ecommerce\Core\Solr\Param\FacetRangeParam;
use Ecommerce\Core\Solr\Param\FilterQueryParam;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Ecommerce\Core\Solr\Searcher;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Repository\MotorRepository;
use Ecommerce\Repository\SeasonRepository;
use Ecommerce\Service\Quote\Exception;
use Everglory\Models\Category;
use Everglory\Models\Customer;
use Everglory\Models\Manufacturer;
use Everglory\Models\Motor;
use Illuminate\Http\Request;
use SolrQuery;
use Ecommerce\Service\Search\ConvertService;

class SearchService extends BaseService
{
    const CACHE_LIFETIME = 24 * 60;

    private $searcher;
    private $customer;

    private $category;
    private $motor;
    private $manufacturer;
    private $keyword = '';
    private $not_found_message = 'Condition Not found';

    public function __construct(Searcher $searcher, Customer $customer)
    {
        $this->searcher = $searcher;
        $this->customer = $customer;
        if (\Auth::check()) {
            $this->customer = \Auth::user();
        }
    }

    public function setSearcher( Searcher $searcher ){
        $this->searcher = $searcher;
    }


    public function urlValidate($request)
    {
        if ($request->has('motor')) {
            $motor = MotorRepository::find($request->get('motor'), 'url_rewrite');
            $this->motor = $motor;
            if (!$motor) {
                return false;
            }
        }

        if ($request->has('category')) {

            //REDIRECT
            foreach ( \Everglory\Constants\Category::REDIRECTS as $redirect_from => $redirect_to ) {
                if (false !== strpos($request->input('category'), $redirect_from)) {
                    return modifyPartsUrl(['ca' => str_replace($redirect_from, $redirect_to, $request->input('category'))]);
                }
            }

            $category = CategoryRepository::find($request->get('category'), 'url_path');
            $this->category = $category;
            if (!$category) {
                return false;
            }
        }

        if ($request->has('manufacturer')) {
            $manufacturer = ManufacturerRepository::find($request->get('manufacturer'), 'url_rewrite');
            $this->manufacturer = $manufacturer;
            if (!$manufacturer) {
                return false;
            }
        }

        return true;
    }

    public function getTitle()
    {

        $titles = ['mt'=>'','ca'=>'','br'=>''];

        if($this->motor){
            $titles['mt'] = $this->motor->manufacturer->name . ' ' . $this->motor->name. ($this->motor->synonym ? '('.$this->motor->synonym.')' : '');
        }
        if($this->category){
            $titles['ca'] = $this->category->name. ( $this->category->synonym ? '('.$this->category->synonym.')' : '' );
        }
        if($this->manufacturer){
            $titles['br'] = $this->manufacturer->name. ($this->manufacturer->synonym ? '('.$this->manufacturer->synonym.')' : '');
        }
        $this->titles = $titles;
        return implode(' & ' ,array_filter($titles));
    }

    /**
     * @param Request $request
     * @param null $keyword
     * @return SearchResponse
     */
    public function selectList(Request $request, $keyword = null)

    {
        $price_field = 'price_1';
        if ((int)$this->customer->role_id !== 0) {
            $price_field = 'price_' . (int)$this->customer->role_id;
        }

        $common = new CommonParam();
        $facetFields = [];
        $facetQueries = [];
        $facetRanges = [];
        $filterQueries = [];


        $rows = $request->get('limit', 40);
        $common->setRows($rows);

        $sort_fields = ['visits' => 'visits', 'new' => 'created_at', 'price' => $price_field, 'manufacturer' => 'manufacturer_name', 'ranking' => 'ranking', 'sales' => 'div(' . $price_field . ',price)', 'score' => 'score'];
        if (\Route::currentRouteName() == 'outlet') {
            $sort_field = $request->get('sort', 'new');
        } else {
            $sort_field = $request->get('sort', '');
        }

        if (array_key_exists($sort_field, $sort_fields)) {
            $sort_field = $sort_fields[$sort_field];
        } else {
            $sort_field = 'visits';
        }


        $direction = $request->get('order');

        if ($sort_field){
            if ($direction == 'asc') {
                $common->setSortFields($sort_field, SolrQuery::ORDER_ASC);
            } else {
                $common->setSortFields($sort_field);
            }
        }



        $pager = $request->get('page');
        if (!$pager)
            $pager = 1;

        $start = ($pager - 1) * $rows;
        $common->setStart($start);

        if ($keyword){
            $q = TransferToUtf8($keyword);
        } else {
            $q = TransferToUtf8($request->get('q', ''));
        }

        $for = $request->get('for', null);
        $query = $common->parseKeyword($q, $for);
        $common->setQuery($query);

        if ($query) {
            //limit low score

            //sub query need double slash
            $sub_query = str_replace('\\', '\\\\', $query);


            if ($sort_field == 'visits') {
                $common->resetSortFields();
            }
        }
        if($sort_field != 'visits'){
            $common->setSortFields('created_at');
        }


        $this->searcher->setCommon($common);
        $this->keyword = $q;

        // prevent remove item
        $filterQuery = new FilterQueryParam();
        $filterQuery->setField('sku');
        $filterQuery->setQuery('[* TO *]');
        $filterQueries[] = $filterQuery;

        $category = $request->get('category', null);

        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            if ($category) {
                $this->category = $category;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('url_path');
                $filterQuery->setTag('category');
                $filterQuery->setQuery($category->mptt->url_path);
                $filterQueries[] = $filterQuery;

                $url_path = implode("-", explode("-", $category->mptt->url_path, -1));
                if ($url_path) {
                    $facetQuery = new FacetQueryParam();
                    $facetQuery->setField('url_path');
                    $facetQuery->setQuery($url_path);
                    $facetQueries[] = $facetQuery;
                }
            }
        }

        $categories = $request->get('categories', null);
        if ($categories) {
            $categories = CategoryRepository::find(explode(',', $categories), 'url_path');
            $url_paths = [];
            foreach ( $categories as $category ) {
                $url_paths[] = $category->mptt->url_path;
            }
            $this->category = $categories->sortByDesc('sort')->first();
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setQuery('(' . implode(' OR ', $url_paths) . ')');
            $filterQueries[] = $filterQuery;
        }


        $facetField = new FacetFieldParam();
        $facetField->setField('url_path');
        $facetField->setExcludeTags(['category']);
        $facetFields[] = $facetField;

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            if ($manufacturer) {
                $this->manufacturer = $manufacturer;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('manufacturer_url');
                $filterQuery->setTag('manufacturer');
                $filterQuery->setQuery($manufacturer->url_rewrite);
                $filterQueries[] = $filterQuery;
            }
        }

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            if ($motor) {
                $this->motor = $motor;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('motor_id');
                $filterQuery->setTag('motor');
                $filterQuery->setQuery($motor->id);
                $filterQueries[] = $filterQuery;
            }
        }

        $motors = $request->get('motors', null);
        if ($motors) {
            $motors = MotorRepository::find(explode(',', $motors), 'url_rewrite');
            $this->motor = $motors->sortByDesc('created_at')->first();
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setQuery('(' . implode(' OR ', $motors->pluck('id')->all()) . ')');
            $filterQueries[] = $filterQuery;
        }


        $price = $request->get('price', null);
        if ($price) {
            $range = explode('-', $price);
            if(count($range) > 1){
                $start = 0;
                $end = '*';
                if (isset($range[0]) and is_numeric($range[0])) {
                    $start = (int)$range[0];
                }
                if (isset($range[1]) and is_numeric($range[1])) {
                    $end = (int)$range[1];
                }

                $filterQuery = new FilterQueryParam();
                $filterQuery->setField($price_field);
                $filterQuery->setTag('price');
                $filterQuery->setQuery('[' . $start . ' TO ' . $end . ']');
                $filterQueries[] = $filterQuery;
            }
        }

        $in_stock = $request->get('in_stock', null);
        if(!is_null($in_stock) and is_numeric($in_stock)){
            $in_stock_field = ($in_stock ? 'in_stock_flg' : '-in_stock_flg');
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField($in_stock_field);
            $filterQuery->setQuery('[ 1 TO * ]');
            $filterQueries[] = $filterQuery;
        }

        $relation = $request->get('relation', null);
        if($relation){
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('relation_product_id');
            $filterQuery->setQuery($relation);
            $filterQueries[] = $filterQuery;
        }
        $country = $request->get('country', null);
        if ($country == 'taiwan') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('country');
            $filterQuery->setField('manufacturer_country');
            $filterQuery->setQuery('台灣');
            $filterQueries[] = $filterQuery;
        } elseif ($country == 'import') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('country');
            $filterQuery->setField('-manufacturer_country');
            $filterQuery->setQuery('台灣');
            $filterQueries[] = $filterQuery;
        }

        $point_type = ($request->get('point_type') ? 1 : 0);
        if ($point_type) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('point_type');
            $filterQuery->setField('point_type');
            $filterQuery->setQuery($point_type);
            $filterQueries[] = $filterQuery;
        }

        $point_multiple = ($request->get('point_five_double') ? 1 : 0);
        if ($point_multiple) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('{!frange l=5 u=6}div(point_2, point_3)');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('attribute_tag:2019秋冬');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('type:0');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
        }

        $fatherday_dad = ($request->get('fatherday_dad') ? 1 : 0);
        if ($fatherday_dad) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('category_id:(4 OR 79 OR 45 OR 59 OR 1120 OR 67 OR 60)');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('type:0');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
        }

        $fatherday_kid = ($request->get('fatherday_kid') ? 1 : 0);
        if ($fatherday_kid) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('category_id:(277 OR 289 OR 305 OR 1545 OR 321 OR 300 OR 296 OR 1581 OR 1577)');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('type:0');
            $filterQuery->setQuery('');
            $filterQuery->setTag('');
            $filterQueries[] = $filterQuery;
        }

        if (\Route::currentRouteName() == 'outlet') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('product_type');
            $filterQuery->setField('type');
            $filterQuery->setQuery(5);
            $filterQueries[] = $filterQuery;
        } else {
            $product_type = $request->get('product_type', null);
            if ($product_type) {
                $filterQuery = new FilterQueryParam();
                $filterQuery->setTag('product_type');
                $filterQuery->setField('type');
                $filterQuery->setQuery($product_type);
                $filterQueries[] = $filterQuery;
            }
        }

        $filter = $request->get('fl', null);
        if ($filter == 'special-price') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('price_diff_2');
            $filterQuery->setQuery('[1 TO *]');
            $filterQueries[] = $filterQuery;

            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('{!frange l=1 }sub(price,price_diff_2)');
            $filterQuery->setTag('');
            $filterQuery->setQuery('');
            $filterQueries[] = $filterQuery;
        }

        $filter_new_product = $request->get('fn', null);
        if ($filter_new_product) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('created_at');
            $filterQuery->setQuery('[NOW-180DAY/DAY TO NOW]');
            $filterQueries[] = $filterQuery;
        }

        $facetQuery = new FacetQueryParam();
        $facetQuery->setExcludeTags(['country']);
        $facetQuery->setKey('taiwan');
        $facetQuery->setField('manufacturer_country');
        $facetQuery->setQuery('台灣');
        $facetQueries[] = $facetQuery;

        $facetQuery = new FacetQueryParam();
        $facetQuery->setExcludeTags(['country']);
        $facetQuery->setKey('import');
        $facetQuery->setField('-manufacturer_country');
        $facetQuery->setQuery('台灣');
        $facetQueries[] = $facetQuery;

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['manufacturer']);
        $facetField->setField('manufacturer_name_url');
        $facetField->setSort(SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;


        $facetRange = new FacetRangeParam($price_field);
        $facetRange->setStart(0);
        $facetRange->setEnd(20000);
        $facetRange->setGap(500);
        $facetRange->setOther('after');
        $facetRange->setExcludeTags(['price']);
        $facetRange->setKey('price_range');
        $facetRanges[] = $facetRange;

        $this->searcher->setFilterQueries($filterQueries);
        $this->searcher->setFacetFields($facetFields);
        $this->searcher->setFacetQueries($facetQueries);
        $this->searcher->setFacetRanges($facetRanges);

        if ($sort_field == '') {

            $para = new EdismaxQueryParam();

            try{

                $this->searcher->setEdismax($para);
            }catch (\Exception $ex){
                dd($ex);
            }


        }


        return $this->searcher->query();

    }


    /**
     * @return Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return Motor|null
     */
    public function getMotor()
    {
        return $this->motor;
    }

    /**
     * @return Manufacturer|null
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    public function getBaseResult(SearchResponse $search_response)
    {
        $result = new \StdClass;
        $convertService = new ConvertService;
        $result->manufacturer = $convertService->convertFacetFieldToManufacturers($search_response);
        $result->count = $search_response->getQueryResponse()->getCount();
        $result->import_count = $search_response->getFacetQueryResponses()['import']->getCount();
        $result->domestic_count = $search_response->getFacetQueryResponses()['taiwan']->getCount();
        $result->tree = NavigationService::getNavigation($search_response);
        $result->current_category = $this->getCategory();
        $result->current_manufacturer = $this->getManufacturer();
        $result->current_motor = $this->getMotor();
        $result->products = $convertService->convertQueryResponseToProducts($search_response);
        $result->price_range = $convertService->convertFacetRangeResponseToPrices($search_response);
        $result->category = $this->getCategory();
        $result->keyword = $this->getKeyword();
        return $result;
    }

    public function getCategoryListResult(SearchResponse $search_response)
    {
        $result = new \StdClass;
        $convertService = new ConvertService;
        $result->tree = NavigationService::getNavigation($search_response);
        $result->current_category = $this->getCategory();
        $result->current_manufacturer = $this->getManufacturer();
        $result->current_motor = $this->getMotor();
        $result->price_range = $convertService->convertFacetRangeResponseToPrices($search_response);
        $result->category = $this->getCategory();
        $result->keyword = $this->getKeyword();

        return $result;
    }

    /**
     * Get ridingGear products from Solr.
     *
     * @param Request $request get html Request information
     * @return SearchResponse
     */
    public function selectRidingGear(Request $request)
    {
        $searcher = clone $this->searcher;
        $common = new CommonParam();
        $filterQueries = [];
        $rows = 30;
        if(request()->get('limit') && !request()->get('rows')){
            $rows = request()->get('limit');
        }
        if(request()->get('rows')){
            $rows = request()->get('rows');
        }
        $common->setRows($rows);
        $common->setQuery('type:0');
        $common->setSortField('created_at');
        $searcher->setCommon($common);

        $created_from = $request->input('created_from', null);
        if ($created_from) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('created_at');
            $filterQuery->setQuery('[' . $created_from . 'T00:00:00Z TO NOW]');
            $filterQueries[] = $filterQuery;
        }

        /*
         * 2018 AW
         */
//        $url_paths = ['3000-3027', '3000-1325', '3000-3020', '3000-3001', '3000-3260', '3000-1327'];
        /*
         * 2018 SS
         */
        /*
        $url_paths = ['3000-3020-3021', '3000-3020-3060', '3000-3020-3071', '3000-3020-3220', '3000-3020-3040', '3000-3020-3024', '3000-3020-3026', '3000-3020-3034', '3000-3020-4072', '3000-3020-3225', '3000-3020-3619', '3000-3020-3263', '3000-3020-3028', '3000-3020-3266', '3000-3020-1328', '3000-3020-3123', '3000-3020-1325', '3000-3020-1045', '3000-1327'];

        $filterQuery = new FilterQueryParam();
        $filterQuery->setField('url_path');
        $filterQuery->setTag('category');
//        $filterQuery->setQuery(3000);
        $filterQuery->setQuery('(' . implode(' OR ', $url_paths) . ')');
        $filterQueries[] = $filterQuery;
        */

        /*
         * 2019 AW
         */
       /* $items = SeasonRepository::allOfTheSeason('2019秋冬');
        $skus = implode(' OR ', $items->pluck('sku')->toArray());
        $filterQuery = new FilterQueryParam();
        $filterQuery->setField('sku');
        $filterQuery->setQuery('(' . $skus . ')');
        $filterQueries[] = $filterQuery;*/
        /*2019 SS*/
        
        $filterQuery = new FilterQueryParam();
        $filterQuery->setField('attribute_tag');
        $filterQuery->setQuery('2019春夏');
        $filterQueries[] = $filterQuery;


        $searcher->setFilterQueries($filterQueries);

        //group result
//        $searcher->setGroupField('manufacturer_id');
        $searcher->setGroupSort('visits');


        return $searcher->query();
    }


    public function selectSuggest(Request $request)
    {
        $searcher = clone $this->searcher;
        $common = new CommonParam();;

        $q = $common->parseKeyword($request->input('q'), null, true);
        if ($q != '*:*') {

            $common->setRows(50);

            $common->setQuery('search_text:' . $q);
            $searcher->setCommon($common);

            //group result
            $searcher->setGroupField('data_type');
            $searcher->setGroupLimit(5);

            return $searcher->query();
        }
        return false;
    }

    public function selectRanking(Request $request)
    {
        $price_field = 'price_1';
        if ((int)$this->customer->role_id !== 0) {
            $price_field = 'price_' . (int)$this->customer->role_id;
        }

        $common = new CommonParam();
        $facetFields = [];
        $facetQueries = [];
        $facetRanges = [];
        $filterQueries = [];

        $rows = $request->get('limit', 100);
        if($rows > 100){
            $rows = 100;
        }
        $common->setRows($rows);


        $sort_fields = [
            'month_sales' => 'month_sales',
            'two_month_sales'=>'two_month_sales',
            'year_sales'=>'year_sales',
            'month_brands' => 'month_sales',
            'two_month_brands'=>'two_month_sales',
            'year_brands'=>'year_sales',
            'month_popularity' => 'month_popularity',
            'two_month_popularity'=>'two_month_popularity',
            'year_popularity'=>'year_popularity'
        ];

        $sort_field = $request->get('sort', 'two_month') . '_' . $request->get('type', 'sales');


        if (array_key_exists($sort_field, $sort_fields)) {
            $sort_field = $sort_fields[$sort_field];
        } else {
            $sort_field = 'two_month_sales';
        }

        if(!$request->get('motor', null)){
            $sort_field .= '_group';
        }
        $common->setSortFields($sort_field);

        $pager = 1;

        $start = ($pager - 1) * $rows;
        $common->setStart($start);

        $common->setQuery($sort_field.':{0 TO *}');
        $this->searcher->setCommon($common);

        // prevent remove item
        $filterQuery = new FilterQueryParam();
        $filterQuery->setField('sku');
        $filterQuery->setQuery('[* TO *]');
        $filterQueries[] = $filterQuery;

        $category = $request->get('category', null);

        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            if ($category) {
                $this->category = $category;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('url_path');
                $filterQuery->setTag('category');
                $filterQuery->setQuery($category->mptt->url_path);
                $filterQueries[] = $filterQuery;

                $url_path = implode("-", explode("-", $category->mptt->url_path, -1));
                if ($url_path) {
                    $facetQuery = new FacetQueryParam();
                    $facetQuery->setField('url_path');
                    $facetQuery->setQuery($url_path);
                    $facetQueries[] = $facetQuery;
                }
            }
        }

        $categories = $request->get('categories', null);
        if ($categories) {
            $categories = CategoryRepository::find(explode(',', $categories), 'url_path');
            $url_paths = [];
            foreach ( $categories as $category ) {
                $url_paths[] = $category->mptt->url_path;
            }
            $this->category = $categories->sortByDesc('sort')->first();
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setQuery('(' . implode(' OR ', $url_paths) . ')');
            $filterQueries[] = $filterQuery;
        }


        $facetField = new FacetFieldParam();
        $facetField->setField('url_path');
        $facetField->setExcludeTags(['category']);
        $facetFields[] = $facetField;

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            if ($manufacturer) {
                $this->manufacturer = $manufacturer;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('manufacturer_url');
                $filterQuery->setTag('manufacturer');
                $filterQuery->setQuery($manufacturer->url_rewrite);
                $filterQueries[] = $filterQuery;
            }
        }

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            if ($motor) {
                $this->motor = $motor;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('motor_id');
                $filterQuery->setTag('motor');
                $filterQuery->setQuery($motor->id);
                $filterQueries[] = $filterQuery;
            }
        }

        $motors = $request->get('motors', null);
        if ($motors) {
            $motors = MotorRepository::find(explode(',', $motors), 'url_rewrite');
            $this->motor = $motors->sortByDesc('created_at')->first();
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setQuery('(' . implode(' OR ', $motors->pluck('id')->all()) . ')');
            $filterQueries[] = $filterQuery;
        }


        $price = $request->get('price', null);
        if ($price) {
            $range = explode('-', $price);
            $start = (int)$range[0];
            $end = (int)$range[1];
            if (!$end) {
                $end = '*';
            }

            $filterQuery = new FilterQueryParam();
            $filterQuery->setField($price_field);
            $filterQuery->setTag('price');
            $filterQuery->setQuery('[' . $start . ' TO ' . $end . ']');
            $filterQueries[] = $filterQuery;
        }

        $country = $request->get('country', null);
        if ($country == 'taiwan') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('country');
            $filterQuery->setField('manufacturer_country');
            $filterQuery->setQuery('台灣');
            $filterQueries[] = $filterQuery;
        } elseif ($country == 'import') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('country');
            $filterQuery->setField('-manufacturer_country');
            $filterQuery->setQuery('台灣');
            $filterQueries[] = $filterQuery;
        }

        $point_type = ($request->get('point_type') ? 1 : 0);
        if ($point_type) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('point_type');
            $filterQuery->setField('point_type');
            $filterQuery->setQuery($point_type);
            $filterQueries[] = $filterQuery;
        }

        if (\Route::currentRouteName() == 'outlet') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setTag('product_type');
            $filterQuery->setField('type');
            $filterQuery->setQuery(5);
            $filterQueries[] = $filterQuery;
        } else {
            $product_type = $request->get('product_type', null);
            if ($product_type) {
                $filterQuery = new FilterQueryParam();
                $filterQuery->setTag('product_type');
                $filterQuery->setField('type');
                $filterQuery->setQuery($product_type);
                $filterQueries[] = $filterQuery;
            }
        }

        $filter = $request->get('fl', null);
        if ($filter == 'special-price') {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('price_diff_2');
            $filterQuery->setQuery('[1 TO *]');
            $filterQueries[] = $filterQuery;

            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('{!frange l=1 }sub(price,price_diff_2)');
            $filterQuery->setTag('');
            $filterQuery->setQuery('');
            $filterQueries[] = $filterQuery;
        }

        $filter_new_product = $request->get('fn', null);
        if ($filter_new_product) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('created_at');
            $filterQuery->setQuery('[NOW-180DAY/DAY TO NOW]');
            $filterQueries[] = $filterQuery;
        }

        $facetQuery = new FacetQueryParam();
        $facetQuery->setExcludeTags(['country']);
        $facetQuery->setKey('taiwan');
        $facetQuery->setField('manufacturer_country');
        $facetQuery->setQuery('台灣');
        $facetQueries[] = $facetQuery;

        $facetQuery = new FacetQueryParam();
        $facetQuery->setExcludeTags(['country']);
        $facetQuery->setKey('import');
        $facetQuery->setField('-manufacturer_country');
        $facetQuery->setQuery('台灣');
        $facetQueries[] = $facetQuery;

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['manufacturer']);
        $facetField->setField('manufacturer_name_url');
        $facetField->setSort(SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;


        $facetRange = new FacetRangeParam($price_field);
        $facetRange->setStart(0);
        $facetRange->setEnd(20000);
        $facetRange->setGap(500);
        $facetRange->setOther('after');
        $facetRange->setExcludeTags(['price']);
        $facetRange->setKey('price_range');
        $facetRanges[] = $facetRange;

        $this->searcher->setFilterQueries($filterQueries);
        $this->searcher->setFacetFields($facetFields);
        $this->searcher->setFacetQueries($facetQueries);
        $this->searcher->setFacetRanges($facetRanges);

        //Not finish
//        if($request->get('type') == 'brands'){
//            //group result
//            $this->searcher->setGroupField('manufacturer_id');
////            $this->searcher->setGroupSort('visits');
//            $this->searcher->setGroupFacet(true);
//        }else{
            if(!$motor){
                $this->searcher->setGroupField('relation_product_id');
                $this->searcher->setGroupSort($sort_field);
//                $this->searcher->setGroupSort($sort_field);
                $this->searcher->setGroupFacet(true);
            }
//        }


        return $this->searcher->query();
    }


    /**
     * Get ridingGear products from Solr.
     *
     * @param Request $request get html Request information
     * @return SearchResponse
     */
    public function selectFacetByManufacturers($manufacturer_urls,$request,$main_category = '1000')
    {
        $searcher = clone $this->searcher;
        $common = new CommonParam();
        $filterQueries = [];

        $common->setRows(1);
        $searcher->setCommon($common);

        if (count($manufacturer_urls)) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setQuery('(' . implode(' OR ', $manufacturer_urls) . ')');
            $filterQueries[] = $filterQuery;
        }else{
            return null;
        }

        $filterQuery = new FilterQueryParam();
        $filterQuery->setField('url_path');
        $filterQuery->setTag('category');
        $filterQuery->setQuery($main_category);
        $filterQueries[] = $filterQuery;

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            if ($motor) {
                $this->motor = $motor;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('motor_id');
                $filterQuery->setTag('motor');
                $filterQuery->setQuery($motor->id);
                $filterQueries[] = $filterQuery;
            }
        }

        $searcher->setFilterQueries($filterQueries);

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['manufacturer']);
        $facetField->setField('manufacturer_name_url');
        $facetField->setSort(SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;

        $searcher->setFacetFields($facetFields);
        return $searcher->query();
    }
}