<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/24
 * Time: 上午 11:41
 */

namespace Ecommerce\Service;

use Exception;
use Everglory\Models\Assortment;
use Everglory\Models\Assortment\Type;
use Illuminate\Support\Collection;
use Everglory\Models\Assortment\Plugin;
use Everglory\Models\Assortment\Template;
use Ecommerce\Repository\AssortmentRepository;

class AssortmentService extends BaseService
{
    protected $source;

    public function __construct()
    {
    }

    public function getseovalue($id)
    {
        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        switch ($id) {
            case '1':
                $result['description'] = '2015最新車款流行特輯，囊括日本、義大利、美國精選品牌，為您的愛車美麗加分-「Webike-摩托百貨」';
                $result['keywords'] = '車款改裝特輯,日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,流行企劃,車輛特輯,全球知名品牌';  
                break;
            
            case '2':
                $result['description'] = '2015最新騎士用品特輯，不論是日系、歐系或是美式騎士服裝與配件因有盡有-「Webike-摩托百貨」';
                $result['keywords'] = '騎士用品特輯,日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,流行企劃,車輛特輯,全球知名品牌';
                break;
            case '3':
                $result['description'] = '2015重機、機車注目品牌特輯，進口、國產知名品牌最新商品介紹，請千萬不要錯過-「Webike-摩托百貨」';
                $result['keywords'] = '注目品牌特輯,日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,流行企劃,車輛特輯,全球知名品牌';
                break;
            case '5':
                $result['description'] = '2015最新動態影音特輯，透過影片介紹各種豐富摩托車生活的便利、舒適產品的專門特輯！ 每次都有不同的主題去介紹各種商品，每項都是Webike超推薦的嚴選商品。-「Webike-摩托百貨」';
                $result['keywords'] = '動態影音特輯,日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,流行企劃,車輛特輯,全球知名品牌';
                break;
            case '6':
                $result['description'] = '2015最新民調中心，日本老牌重機雜誌「BIG MACHINE」與「Webike」的聯合企畫，針對日本車友提出問卷，主要是對於騎士們平時會遇到的問題進行討論，並且發表相關的解決方案，提供給台灣車友參考。-「Webike-摩托百貨」';
                $result['keywords'] = '民調中心,日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,流行企劃,車輛特輯,全球知名品牌';
                break;
            case '9':
                $result['description'] = '帶你體驗不同風格的摩托文化，從最平易近人的小檔車、輕檔車、速克達；追求速度、極限的仿賽車、越野車；嬉皮文青風格的復古車、美式車，各種摩托車的特點、歷史、緣由與熱門改裝商品、騎士用品一併收集，帶你一同享受摩托車帶給你的騎士人生。';
                $result['keywords'] = 'Webike風格主題,日本,進口,機車,重機,摩托車,商品,改裝,零件,正廠零件,人身部品,流行企劃,車輛特輯,全球知名品牌';
                break;
            default :
                $result['description'] = array();
                $result['keywords'] = array();
                break;

        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function getCollectionTypesByUrlRewrite($url_rewrite = null, $relations = [])
    {

        $result = Type::with($relations)->where('path', 'collection');

        if(is_array($url_rewrite)){
            $result = $result->whereIn('url_rewrite', $url_rewrite);
        }else if($url_rewrite){
            $result = $result->where('url_rewrite', $url_rewrite);
        }else{

        }
        $result = $result->where('active', 1)->whereHas('assortments', function($query){
            $query->where('active', 1)->where('publish_at', '<=', date('Y-m-d H:i:s'));
        })->get();

        return $result;
    }

    public function filterActiveTypes($types)
    {
        return $types->filter(function($type){
            return $type->assortments->count();
        })->sortBy('sort');

    }

    public function getTypeAssortments($types)
    {
        $assortments = collect([]);
        foreach ($types as $key => $type){
            $assortments = $assortments->merge($type->assortments)->sortByDesc('publish_at');
        }

        return $assortments;
    }

    public function filterActive($collection,$paginate = null)
    {
        $results = $collection->filter(function($item){
            return $item->active;
        });

        return $results;
    }

    public function getAssortments($url_rewrite, $relations = [], $paginate = false)
    {
        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        $result = Assortment::with($relations);

        if(is_array($url_rewrite)){
            $result = $result->whereIn('url_rewrite', $url_rewrite);
        }else if($url_rewrite){
            $result = $result->where('url_rewrite', $url_rewrite);
        }else{

        }

        $result = $result->orderby('publish_at', 'DESC');
        if($paginate){
            if(is_numeric($paginate)){
                $result = $result->paginate($paginate);
            }else{
                $result = $result->paginate(20);
            }
        }else{
            $result = $result->get();
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function getAssortmentByUrl($url_rewrites)
    {
        if(is_array($url_rewrites) and count($url_rewrites) == 2){
            $result = Assortment::with(['type', 'items', 'items.plugin', 'items.template', 'tags'])->whereHas('type', function($query) use($url_rewrites){
                $query->where('url_rewrite', $url_rewrites[0]);
            })
            ->where('url_rewrite', $url_rewrites[1])
            ->first();
        }else{
            $result = null;
        }

        return $result;
    }

    /**
     * Get assortment Sort by publish.
     *
     * @param string $quantity Limit of assortment.
     * @param array $relations Relation models.
     * @param string $sort
     * @param null $type_id Assortment type.
     * @return $this|\Illuminate\Database\Eloquent\Builder|static
     */
    public function getAssortmentByPublishAt($quantity = 'all', $relations = [], $sort = 'DESC' , $type_id = null ,$paginate = null)
    {
        $result = Assortment::orderby('publish_at', $sort);
        if (count($relations)) {
            $result = $result->with($relations);
        }
        $result = $result->where('active', 1)->where('publish_at', '<=', date('Y-m-d H:i:s'));

        if ($type_id) {
            $result = $result->where('type_id', $type_id);
        }

        if ($quantity !== 'all' and is_numeric($quantity)) {
            $result = $result->take($quantity)->get();
        }elseif($paginate){
            $result = $result->paginate($paginate);
        }else{
            $result = $result->get();
        }

        return $result;
    }

    public function getAssortmentsByLayout($layout,$relations = [])
    {
        $result = Assortment::with('layout')->where('layout_id','=',$layout);
        if(count($relations)){
            $result = $result->with($relations);
        }
        $result = $result->orderBy('publish_at','desc')->get();
        return $result;
    }

    public function getAssortmentView($assortment)
    {
        $views = [];
        $items = $assortment->items->filter(function($item){
            return $item->active;
        });
        foreach ($items as $item){
            $plugin = $item->plugin;
            $analytic_class = __NAMESPACE__ . $plugin->analytic;
            if (class_exists($analytic_class)) {
                $analytic = new $analytic_class($item);
                $analytic->setItem($item);
                $views[$item->section][$item->sort] = $analytic->read(json_decode($item->data), $item->template);
            }else{
                throw new Exception('use not exist analytic class in getAssortmentView function');
            }
        }
        return $views;
    }

    public function getLayoutSections($layout)
    {
        return json_decode($layout->sections);
    }

    public function getActiveAssortmentByType($type_id , $relations = [] , $paginate = 20 , $sort = 'DESC')
    {
        if(!$type_id){
            return collect();
        }

        $result = Assortment::where('type_id',$type_id)->orderby('publish_at', $sort);
        if(count($relations)){
            $result = $result->with($relations);
        }
        //active
        $result = $result->where('active', 1)->where('publish_at', '<=', date('Y-m-d H:i:s'));

        if($paginate){
            $result = $result->paginate($paginate);
        }else{
            $result = $result->get();
        }

        return $result;
    }

    public function getPluginTemplateColumns($plugin_id,$template_id){

        $plugin = Plugin::where('id',$plugin_id)->first();
        $template = Template::where('id',$template_id)->first();
        $analytic_class = __NAMESPACE__ . $plugin->analytic;
        if (class_exists($analytic_class)) {
            $analytic = new $analytic_class();
            $result = $analytic->getCols();
            if(count($template->other_columns)) {
                $result->other_columns = json_decode($template->other_columns);
            }
            $result->example = $analytic->getExample($template, $result);
        }
        return $result;
    }

    public function insertAssortment($datas,$assortment)
    {
        $datas = array_except($datas,['_token','editFinish','photo_preview','photo_key']);
        foreach($datas as $column => $data){
            $assortment->$column = $data;
        }

        $assortment->meta_title = $datas['name'];
        $repository = new AssortmentRepository;
        if(!$datas['type_id'] or !$datas['url_rewrite']) {
            echo '必填項未填，請回上一頁。';
            die();
        }

        $type = $repository->getTypes($datas['type_id'])->first();
        $type_url_rewrite = $type->url_rewrite;
        $assortment->link = '/collection/' . $type_url_rewrite . '/' . $datas['url_rewrite'];
        $assortment->save();
    }

    public function insertAssortmentItems($datas,$plugin_template,$assortment_id,$section,$sort, $assortmentItem)
    {
        $plugin_template = explode(',',$plugin_template);
        $plugin_id = $plugin_template[0];
        $template_id = $plugin_template[1];
        $service = new AssortmentService;
        $pluginTemplates = $service->getPluginTemplateColumns($plugin_id,$template_id);
        $quantity = $pluginTemplates->quantity;

        $assortmentItem->assortment_id = $assortment_id;
        $assortmentItem->plugin_id = $plugin_id;
        $assortmentItem->template_id = $template_id;
        $assortmentItem->section = $section;

        $datas = array_except($datas,['newBlock','section','plugin_template','_token','photo_preview','photo_key']);
        $result = new \stdClass();

        foreach($datas as $column => $data){
            if(array_values($quantity[$column])[0] == 'mutiple'){
                $result->$column = $data;
            }else{
                if($column == 'product_url_rewrites'){
                    if(is_array($data)){
                        $data = explode(',', str_replace("\r\n", ',', $data)[0]);
                    }else{
                        $data = explode(',', str_replace("\r\n", ',', $data));
                    }

                    $result->$column = $data;
                }else {
                    $result->$column = $data[0];
                }
            }
//            dd();
        }
        $assortmentItem->data = json_encode($result);
        $assortmentItem->sort = $sort;
        $assortmentItem->active = 1;
        $assortmentItem->save();

        return json_encode($result);
    }

    public function insertWeeklyAssortment($data)
    {
        $data = json_decode($data);
        
        $assortment = Assortment::where('url_rewrite',$data->url_rewrite)->where('name',$data->assortment_name)->first();

        if(!$assortment){
            $assortment = new Assortment;
        }        


        $assortment->layout_id = 1;
        $assortment->type_id = 9;
        $assortment->name = $data->assortment_name;
        $assortment->url_rewrite = $data->url_rewrite;
        $assortment->banner = $data->assortment_banner;
        $assortment->meta_title = $data->seo_title;
        $assortment->meta_description = $data->assortment_description;
        $assortment->link = "/campaigns/" . $data->url_rewrite;
        $assortment->_blank = 1;
        $assortment->active = 1;
        $assortment->publish_at = $data->start . " 00:00:00:";
        $assortment->save();
        
        return $assortment->id;
    }
    public function colseAssortment($assortment_id)
    {
        if(!$assortment_id){
            return false;
        }

        $assortment = Assortment::where('id',$assortment_id)->first();
        
        $assortment->active = 0;
        $assortment->save();
        return true;
    }

    public function deleteAssortment($assortment_id)
    {
        if(!$assortment_id){
            return false;
        }

        $assortment = Assortment::where('id',$assortment_id)->delete();
        
        return true;
    }
}