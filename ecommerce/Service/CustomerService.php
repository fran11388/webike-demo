<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/12/6
 * Time: 下午 02:47
 */

namespace Ecommerce\Service;

use Everglory\Models\Customer;
use Carbon\Carbon;
use Uuid;
use Auth;
use Illuminate\Support\Collection;

class CustomerService extends BaseService
{

    public static function getCustomerNamesByPointSearch($text)
    {
        $result = [];
        $customers = Customer::select('id', 'email', 'realname', 'nickname')
            ->where('email', 'like', '%' . $text . '%')
            ->orWhere('realname', 'like', '%' . $text . '%')
            ->orWhere('nickname', 'like', '%' . $text . '%')
            ->get();
        foreach ($customers as $customer){
            $data = new \StdClass;
            $data->id = $customer->id;
            $data->text = $customer->realname . ($customer->nickname ? '(' . $customer->nickname . ')' : '') . '：' .  $customer->email;
            $result[] = $data;
        }
        return $result;
    }
}