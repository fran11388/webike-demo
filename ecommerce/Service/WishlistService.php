<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Customer;
use Everglory\Models\Wishlist;
use Everglory\Models\Product;
use Ecommerce\Accessor\ProductAccessor;
use \Uuid;
use Everglory\Constants\ProductType;
use Everglory\Models\Stock;

class WishlistService extends BaseService
{
    /**
     * @var $carts Collection|Cart[]
     **/
    private $carts = [];
    /**
     * @var $customer Customer
     **/
    private $customer;
    
    private $is_check = false;

    public function __construct( $debug = false )
    {
        $this->is_check = $debug;
        $this->customer = \Auth::user();
        if (!$this->is_check){
            $this->getItems();
            $this->refreshCount();
            while( !$this->checkItems() ){
                $this->carts = null;
                $this->getItems();
            }
        }
    }

    public function removeItem( $protect_code ){
        if ($this->is_check)
            return;
        Wishlist::where('customer_id' , $this->customer->id)
            ->where('protect_code' , $protect_code)
            ->delete();
        $this->refreshCount();        
    }
    public function updateItem( $sku , $qty , $cache_id = null){
        if ($this->is_check)
            return;
        if(is_array($sku)){
            $products = Product::whereIn('url_rewrite' , $sku)->get();
        }else if ( is_string($sku) || is_int($sku)){
            $products = Product::where('url_rewrite' , $sku)->get();
            $sku = [$sku];
            $qty = [$qty];
        }else{
            $products = collect();
        }

        if (!$products->count()){
            return;
        }
        // modify exist item
        $items =  Wishlist::where('customer_id' , $this->customer->id) -> whereIn( 'product_id' , $products->pluck('id')->all() )->get();
        $products = $products->keyBy('id');
        foreach ($items as $key => $item){
            $product = $products->pull($item->product_id);
            $sku_key = array_keys($sku, $product->sku)[0];
            // cannot edit or add qty
            if($product->type != ProductType::GROUPBUY and
                $product->type != ProductType::ADDITIONAL and
                $product->type != ProductType::OUTLET){
                $item->quantity += array_get($qty,$sku_key);
            }
            $item->save();
        }
        // add not exist item
        foreach ($products as $product) {
            $sku_key = array_keys($sku, $product->sku)[0];
            $item = new Wishlist();
            $item->customer_id = $this->customer->id;
            $item->product_id = $product->id;
            $item->quantity = array_get($qty,$sku_key);
            $item->protect_code = (string)Uuid::generate(1);
            $item->save();
        }
        $this->refreshCount();
        return $item;
    }

    /**
     * 判斷購物車內商品是否是被允許的
     * 不行的話 就用 removeItem 刪除該筆後 回傳 false
     * @return boolean
     */
    public function checkItems()
    {
        $check_flg = true;
        foreach ( $this->carts as $entity ) {
            $product = $entity->product;
            if($product and $product->active == 1){
                /*
                 * change for outlet require stock.
                 */
                if($product->type == 5){
                    $tw_stock = ProductService::getStockInfo($product);
                    if($tw_stock and !$tw_stock->stock){
                        $this->removeItem($entity->protect_code);
                        $check_flg = false;
                    }
                }
            }else{
                $this->removeItem($entity->protect_code);
                $check_flg = false;
            }
        }
        return $check_flg;
    }

    public function setItems($carts){
        if (!$this->is_check)
            throw new \Exception('check 模式下才能呼叫setItems');

        foreach ($carts as $key=>&$cart){
            $cart->product_id = $key;
            $product = Product::with(
                'prices' ,
                'fitModels' ,
                'manufacturer' ,
                'preorderItem')
                ->where('id' , $cart->product_id)->first();
            $product = new ProductAccessor($product);
            /** @var Object $cart */
            $cart->product = $product;
        }
        $this->refreshCount();
    }
    /**
     * @return Cart[]|\Illuminate\Database\Eloquent\Collection|Collection|static[]
     */
    public function getItems(){
        if (!$this->carts && !$this->is_check){
            $carts = Wishlist::with(
                'product',
                'product.prices' ,
                'product.fitModels' ,
                'product.manufacturer' ,
                'product.options' ,
                'product.productDescription',
                'product.preorderItem')
                ->where('customer_id' , $this->customer->id)
                ->get();
            foreach ($carts as &$cart){
                if($cart->product){
                    $product = new ProductAccessor($cart->product);
                    /** @var Object $cart */
                    $cart->product = $product;
                }
            }
            $this->carts = $carts;
        }
        return $this->carts;
    }

    public function getItemByProductId($product_id){
        $Item = Wishlist::where('product_id',$product_id)->where('customer_id',$this->customer->id)->first();
        return $Item;
    }

    public function getCount()
    {
        return count($this->carts);
    }

    public function refreshCount()
    {
        session(['wishlist_count' => $this->getCount()]);
    }
}