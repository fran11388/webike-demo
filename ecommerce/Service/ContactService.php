<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

// use League\Flysystem\Exception;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;
use URL;
use Auth;
use Config;
use Log;
use Illuminate\Support\Collection;
use Everglory\Models\Customer;
use Ecommerce\Core\Ticket\Service as TicketService;
use Everglory\Models\Product;
use Everglory\Models\Order;

class ContactService extends BaseService
{
    const CACHE_LIFETIME = 24 * 60;

    private $customer;
    private $ticketService;
    private $maximum_text = 800;

    public function __construct()
    {
        $this->customer = Auth::user();
        $this->ticketService = new TicketService($this->customer);
        $this->ticketService->setUserName($this->customer->realname);
        if(Config::get('app.production')){
            $this->ticketService->usePublishServer();
        }
    }

    public function rebulidPostData()
    {
        $this->ticketService->setUser($this->customer);
    }

    public function getList()
    {
        $result = $this->ticketService->getList();
        $collection = $this->checkResult($result);
        $filters = request()->except('_token', 'page');
        if($filters and count($filters)){
            foreach ($filters as $key => $value){
                if(strlen($value)){
                    $collection = $collection->filter(function($item) use($key, $value){
                        return $item->$key == $value;
                    });
                }
            }
        }
        return $collection;
    }

    public function getRepliedList()
    {
        $result = $this->ticketService->getList();
        $collection = $this->checkResult($result);
        $collection = $collection->filter(function($ticket){
            $replies = collect($ticket->replies);
            $firstReply = $replies->sortByDesc('timestamp')->first();
            return $ticket->interaction === '已回覆' and $firstReply->replyer->type == 'service' and !$firstReply->read;
        })->sortByDesc('created_at');
        $filters = request()->except('_token', 'page');
        if($filters and count($filters)){
            foreach ($filters as $key => $value){
                if(strlen($value)){
                    $collection = $collection->filter(function($item) use($key, $value){
                        return $item->$key == $value;
                    });
                }
            }
        }
        return $collection->sortByDesc('timestamp');
    }

    public function getDepartmentTypes()
    {
        $result = $this->ticketService->getTypes();
        $collection = $this->checkResult($result);
        return $collection;
    }

    public function getTypes($url_rewrite = NULL)
    {
        $result = $this->ticketService->getTypes();
        $departments = $this->checkResult($result);
        $collection = [];
        $departments->filter(function($department) use($url_rewrite, &$collection){
            foreach ($department->types as $type){
                if(is_array($url_rewrite)){
                    if(in_array($type->url_rewrite, $url_rewrite)) {
                        $collection[] = $type;
                    }
                }else if($url_rewrite){
                    if($type->url_rewrite == $url_rewrite){
                        $collection[] = $type;
                    }
                }else{
                    $collection[] = $type;
                }
            }
        });
        $collection = new Collection($collection);
        return $collection;
    }

    public function getTypesLayer()
    {
        $result = $this->ticketService->getTypesLayer();
        $collection = $this->checkResult($result);
        return $collection;
    }

    public function getRelationTickets($data)
    {
        if(!is_array($data) or !isset($data['relations'])){
            throw new Exception('When get relation ticket, not find relations require key.');
        }

        $result = $this->ticketService->getRelationTickets($data);
        $collection = $this->checkResult($result);
        return $collection;
    }


    public function getRelations()
    {
        $relations = ['html' => [], 'javascript' => []];

        if(request()->has('skus')) {
            $url_rewrite = request()->input('skus');
            $url_rewrite = explode('_', $url_rewrite);
            $products = Product::with('manufacturer')->whereIn('url_rewrite', $url_rewrite)->where('active', 1)->where('parent_id', 0)->get();
            foreach($products as $product){
                if(!$product->is_main){
                    $product = Product::with('manufacturer')->where('group_code', $product->group_code)->where('active', 1)->where('parent_id', 0)->where('is_main',1)->first();
                }
                $relations['html'][] =
                    '<tr>' .
                    '<td class="col-xs-2 col-xs-2 col-xs-2">詢問商品 : </td>' .
                    '<td class="col-xs-10 col-xs-10 col-xs-10">' .
                        '<a href="' . URL::route('product-detail', $product->url_rewrite) . '" target="_blank">' . '【' . $product->manufacturer->name . '】' . $product->name . '</a><input type="hidden" name="skus[]" value="' . $product->url_rewrite . '"><br>' .
                    '</td>' .
                    '</tr>';
            }
            $relations['javascript'][] =
                '<script type="text/javascript">
                    setFamily("商品諮詢");
                </script>';
        }


        if(request()->has('orders')) {
            $increment_ids = request()->input('orders');
            $increment_ids = explode('_', $increment_ids);
            $orders = Order::where('customer_id', $this->customer->id)->whereIn('increment_id', $increment_ids)->get();
            foreach($orders as $order){
                $relations['javascript'][] =
                    '<script type="text/javascript">
                        setOrderSelect("'. $order->increment_id .'");
                    </script>';
            }
        }

        if(request()->has('family_id')) {
            $family_id = request()->input('family_id');
            $relations['javascript'][] =
                '<script type="text/javascript">
                    setFamily("'. $family_id .'");
                </script>';
        }

        if(request()->has('category_id')) {
            $category_id = request()->input('category_id');
            $relations['javascript'][] =
                '<script type="text/javascript">
                    setCategory("'. $category_id .'");
                </script>';
        }

        return $relations;
    }

    public function rebuildRelations($relations)
    {
        $rebuild = [];
        $url_compare = [
            'WebikeOrder' => [
                'route' => 'customer-history-order-detail',
            ],
            'WebikeRex' => [
                'route' => 'customer-history-return-detail',
            ],
            'WebikeProduct' => [
                'route' => 'product-detail',
            ],
        ];
        foreach ($relations as $relation){
            if(isset($url_compare[$relation->object_type])){
                $route_name = $url_compare[$relation->object_type]['route'];
                $rebuild[] = $relation->frontText . '<a href="' . URL::route($route_name, $relation->url_rewrite) . '" target="_blank">' . $relation->name . '</a>';
            }
        }
        return $rebuild;
    }

    public function insertTicket()
    {
        $result = $this->ticketService->insert(request()->all());
        return $this->checkResult($result, 'data', false);
    }

    public function loadTicket($url_rewrite = NULL)
    {
        $result = $this->ticketService->reload(['increment_id' => $url_rewrite]);
        $result = $this->checkResult($result, 'datas', false);
        return ($result and is_array($result)) ? current($result) : null;
    }

    public function replyTicket()
    {
        $this->ticketService->reply(request()->all());
    }

    public function readTicket()
    {
        $this->ticketService->read(request()->all());
    }

    private function checkResult($result, $property = 'datas', $collect = true)
    {
        if($result and $result->success){
            if($collect){
                return new Collection($result->$property);
            }else{
                return $result->$property;
            }
        }else{
            if($result){
                Log::warning('ContactService get result is not success.', (Array)$result);
            }else{
                Log::warning('ContactService get result is not success. and get null ');
            }
            if($collect) {
                return new Collection([]);
            }else{
                return null;
            }
        }
    }
}