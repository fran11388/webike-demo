<?php 

namespace Ecommerce\Service\PromoCms;

use Everglory\Models\Promotion;
use Ecommerce\Service\BaseService;
use Maatwebsite\Excel\Facades\Excel;
use Everglory\Models\Promotion\Item;


class PromoService extends BaseService
{
    public function __construct()
    {
       
    }

	public function getColsByExcelFile($file) 
	{
        $excels = [];
        Excel::load($file, function($reader) use(&$excels){

                $objExcel = $reader->getExcel();
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                for ($row = 1; $row <= $highestRow; $row++)
                     {
                    //  Read a row of data into an array
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);
                    // str_replace( " ", "", $rowData[0][1]);
                    if(empty($rowData[0][0])){
                       // dd(123);
                    }else{
                     $excels[] = $rowData[0];   
                    }

                }
        }); 
            
            return $excels;
    }

    public function getpromoPreview($promoDates)
    {
        foreach ($promoDates as $key => $promoDate) {
            if($key >= 3){

                $promotionItem[] = $this->getPromotionItemData($promoDate);

            }
            
        }
            return $promotionItem;

    }
    private function intoPromotionGetData($promo)
    {   
        $year = $promo['1'];
        $month = $promo['2'];
        if($month < 10){
            $month = '0'.$promo['2'];
        }
        $url_rewrite = $year.'-'.$month;
        $Promotions = Promotion::where('url_rewrite',$url_rewrite)->first();
        if(isset($Promotions)){
            //編輯
            $data = $this->getPromotionData($promo);
            Promotion::where('url_rewrite',$url_rewrite)->delete();
             $PromotionsId =  promotion::insertGetId($data);

        }else{
            
            $data = $this->getPromotionData($promo);

            $PromotionsId = promotion::insertGetId($data);
            
        }

        $result =['PromotionsId' => $PromotionsId, 'url_rewrite' => $url_rewrite];
        return $result;
    }

    public function getPromotionData($promo)
    {
        
        $year = $promo['1'];
        $month = $promo['2'];
        if($month < 10){
            $month = '0'.$promo['2'];
        }
        $banner = $promo['3'];
        $main_color = $promo['4'];
        $font_color = $promo['5'];
        $link_hover_font_color = $promo['6'];
        $brand_title_color = $promo['7'];
        $brand_border_color = $promo['8'];
        $category_button_backgroundcolor = $promo['9'];
        $category_button_hover_color = $promo['10'];
        $category_button_text_color = $promo['11'];
        $category_button_border_color = $promo['12'];
        $brand_font_color = $promo['13'];
        $name = $year.'年'.$month.'月促銷活動';
        $start_at = date('Y-m-d H:i:s');
        $end_month = date('Y-'.$month);
        $time = strtotime($end_month);
        $end_at = date('Y-m-01 00:00:00',strtotime('next month',$time));
        $url_rewrite = $year.'-'.$month;
        $result = ['url_rewrite' => $url_rewrite, 'banner' => $banner, 'main_color' => $main_color, 'font_color' => $font_color, 'link_hover_font_color' => $link_hover_font_color, 'name' => $name, 'start_at' => $start_at, 'end_at' => $end_at, 'brand_title_color' => $brand_title_color,'brand_border_color' => $brand_border_color,'category_button_backgroundcolor' => $category_button_backgroundcolor,'category_button_hover_color' => $category_button_hover_color,'category_button_text_color' => $category_button_text_color, 'category_button_border_color' => $category_button_border_color, 'brand_font_color' => $brand_font_color,'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];

        return $result;
        
    }
    public function intoPromotionsItem($promoItems)
    {

        $promotionData =  $this->intoPromotionGetData($promoItems['1']);
        $promotionId = $promotionData['PromotionsId'];
        $promotionUrl_rewrite = $promotionData['url_rewrite'];
        
        foreach ($promoItems as $key => $promoItem) {
            if($key >= 3){
                // dd($promoItem);
                
                
                $url_rewrite = substr($promoItem['2'],strripos($promoItem['2'],"/")+1);

                $promotionItem = Item::where('promotion_id',$promotionId)->where('url_rewrite',$url_rewrite)->first();
                
                if(isset($promotionItem)){
                    //編輯
                    $promotionItemId =  $promotionItem->id;
                    $data = $this->getPromotionItemData($promoItem);
                    Item::where('id',$promotionItemId)->where('url_rewrite',$url_rewrite)->delete();
                    $promoItemInsert = Item::insert($data);

                }else{
                    
                    $data = $this->getPromotionItemData($promoItem, $promotionId);
                    $promoItemInsert = Item::insert($data);
                }
            }
        }

            return $promotionUrl_rewrite;
    }


    public function getPromotionItemData($itemData, $promotionId = null)
    {
        $type = $itemData['0'];
        $name = $itemData['1'];
        $url_rewrite = substr($itemData['2'],strripos($itemData['2'],"/")+1);
        $link = $itemData['2'];
        $image = $itemData['3'];
        $sub_image = $itemData['4'];
        switch ($type) {
            case '長期促銷':
                    $type_id = '1';
                    $note = '大特價';
                break;
            case '精選品牌':
                    $type_id = '2';
                    $note = '大特價';
                break;
            case '加倍品牌':
                    $type_id = '3';
                    $note = '現金點數5倍';
                break;
            case '商品分類':
                    $type_id = '4';
                    $note = '大特價';
                break;
            case '流行車種':
                    $type_id = '5';
                    $note = '現金點數5倍';
                break;
            default:
                    $type_id = '5';
                    $note = '現金點數5倍';
                break;
        }

        $result = ['promotion_id' => $promotionId,'type_id' => $type_id, 'name' => $name ,'note' => $note, 'url_rewrite' => $url_rewrite, 'link' => $link, 'image' => $image, 'sub_image' => $sub_image, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];

        
        return $result;


    }


}



















































?>
