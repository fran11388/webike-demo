<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Constants\CountryGroup;
use Everglory\Constants\EstimateType;
use Auth;
use Everglory\Models\Mitumori;
use Everglory\Models\Mitumori\Item as MitumoriItem;
use Everglory\Models\GroupBuy;
use Everglory\Models\GroupBuy\Item as GroupBuyItem;
use Everglory\Models\Estimate\Item as EstimateItem;
use Everglory\Models\Estimate\Cart;
use Everglory\Models\Product;
use Ecommerce\Accessor\ProductAccessor;
use Everglory\Constants\ProductType;
use \Uuid;

class EstimateService extends BaseService
{
    /**
     * @var $carts Collection|Cart[]
     **/
    private $carts = [];
    /**
     * @var $customer Customer
     **/
    private $customer;

    private $is_check = false;

    public function __construct( $debug = false )
    {
        $this->is_check = $debug;
        $this->customer = \Auth::user();
        if (!$this->is_check){
            $this->getItems();
            while( !$this->checkItems() ){
                $this->carts = null;
                $this->getItems();
            }
        }
    }

    public function removeItem( $protect_code ){
        if ($this->is_check)
            return;
        Cart::where('customer_id' , $this->customer->id)
            ->where('protect_code' , $protect_code)
            ->delete();
    }
    public function updateItem( $sku , $qty , $cache_id = null){
        if ($this->is_check)
            return;
        if(is_array($sku)){
            $products = Product::whereIn('url_rewrite' , $sku)->get();
        }else if ( is_string($sku) || is_int($sku)){
            $products = Product::where('url_rewrite' , $sku)->get();
            $sku = [$sku];
            $qty = [$qty];
        }else{
            $products = collect();
        }

        if (!$products->count()){
            return;
        }
        // modify exist item
        $items =  Cart::where('customer_id' , $this->customer->id) -> whereIn( 'product_id' , $products->pluck('id')->all() )->get();
        $products = $products->keyBy('id');
        foreach ($items as $key => $item){
            $product = $products->pull($item->product_id);
            $sku_key = array_keys($sku, $product->sku)[0];
            // cannot edit or add qty
            if($product->type != ProductType::GROUPBUY and
                $product->type != ProductType::ADDITIONAL and
                $product->type != ProductType::OUTLET){
                $item->quantity = array_get($qty,$sku_key);
            }
            $item->save();
        }
        // add not exist item
        foreach ($products as $product) {
            $sku_key = array_keys($sku, $product->sku)[0];
            $item = new Cart();
            $item->customer_id = $this->customer->id;
            $item->product_id = $product->id;
            $item->quantity = array_get($qty,$sku_key);
            $item->protect_code = (string)Uuid::generate(1);
            $item->save();
        }
    }

    /**
     * 判斷購物車內商品是否是被允許的
     * 不行的話 就用 removeItem 刪除該筆後 回傳 false
     * @return boolean
     */
    public function checkItems()
    {
        $check_flg = true;
        foreach ( $this->carts as $entity ) {
            $product = $entity->product;
            if($product and $product->active == 1){
                /*
                 * change for outlet require stock.
                 */
                if($product->type == 5){
                    $tw_stock = ProductService::getStockInfo($product);
                    if($tw_stock and !$tw_stock->stock){
                        $this->removeItem($entity->protect_code);
                        $check_flg = false;
                    }
                }

            }else{
                $this->removeItem($entity->protect_code);
                $check_flg = false;
            }
        }
        return $check_flg;
    }

    public function setItems($carts){
        if (!$this->is_check)
            throw new \Exception('check 模式下才能呼叫setItems');

        foreach ($carts as $key=>&$cart){
            $cart->product_id = $key;
            $product = Product::with(
                'prices' ,
                'fitModels' ,
                'manufacturer' ,
                'preorderItem')
                ->where('id' , $cart->product_id)->first();
            $product = new ProductAccessor($product);
            /** @var Object $cart */
            $cart->product = $product;
        }

    }
    /**
     * @return Cart[]|\Illuminate\Database\Eloquent\Collection|Collection|static[]
     */
    public function getItems(){
        if (!$this->carts && !$this->is_check){
            $carts = Cart::with(
                'product',
                'product.prices' ,
                'product.fitModels' ,
                'product.manufacturer' ,
                'product.options' ,
                'product.productDescription',
                'product.preorderItem')
                ->where('customer_id' , $this->customer->id)
                ->get();
            foreach ($carts as &$cart){
                if($cart->product){
                    $product = new ProductAccessor($cart->product);
                    /** @var Object $cart */
                    $cart->product = $product;
                }
            }
            $this->carts = $carts;
        }
        return $this->carts;
    }

    public function estimateProductUseApi($type, $curl_para)
    {
        if(!isset($curl_para['customer_id']) or !$curl_para['customer_id']){
            $curl_para['customer_id'] = $this->customer->id;
        }
        $curl_para['type_id'] = EstimateType::getConst(strtoupper($type));

        $ch = curl_init();
        $post_header = [
            'Content-Type: application/json',
            'Accept: application/json',
        ];
        if(\Config::get('app.production')){
            curl_setopt($ch, CURLOPT_URL, "http://153.120.83.199/api/estimate/create");
        }else{
            curl_setopt($ch, CURLOPT_URL, "http://zero-dev.everglory.asia/api/estimate/create");

        }
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curl_para));
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        return $result;
    }

    public function estimateProductUseApiRealTime($type, $curl_para ,$step = 1,$estimate_code = null)
    {
        $curl_para['customer_id'] = $this->customer->id;
        $curl_para['type_id'] = EstimateType::getConst(strtoupper($type));
        $curl_para['step'] = $step;
        $curl_para['estimate_code'] = $estimate_code;

        $ch = curl_init();
        $post_header = [
            'Content-Type: application/json',
            'Accept: application/json',
        ];

        if(\Config::get('app.production')){
            curl_setopt($ch, CURLOPT_URL, "http://153.120.83.199/api/estimate/real_time/create");
        }else{
            curl_setopt($ch, CURLOPT_URL, "http://zero-dev.everglory.asia/api/estimate/real_time/create");
        }

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curl_para));
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        return $result;
    }
    
    public function estimateMitumori($items)
    {
        $today_count = Mitumori::where(\DB::Raw('DATE(request_date)'),\DB::Raw('CURDATE()'))->count();
        $estimate_code = $this->createCommonCode($today_count, 'TW', 'U');
        $mitumori = Mitumori::create([
            'estimate_code' => $estimate_code,
            'customer_id' => $this->customer->id,
            'status' => 0,
        ]);

        foreach ($items as $item){
            $item['mitumori_id'] = $mitumori->id;
            MitumoriItem::create($item);
        }

        return $mitumori;
    }

    public function estimateGroupbuy($items)
    {
        $today_count = GroupBuy::where(\DB::Raw('DATE(request_date)'),\DB::Raw('CURDATE()'))->count();
        $estimate_code = $this->createCommonCode($today_count, 'TW', 'G');
        $groupbuy = GroupBuy::create([
            'estimate_code' => $estimate_code,
            'customer_id' => $this->customer->id,
            'status' => 0,
            'response'
        ]);

        foreach ($items as $item){
            $item['groupbuy_id'] = $groupbuy->id;
            GroupBuyItem::create($item);
        }

        return $groupbuy;
    }

    public function estimateGeneral()
    {

        $curl_para = [];
        $curl_para['customer_id'] = $this->customer->id;
        $curl_para['type_id'] = 3;

        $items = Cart::with(['product'])->where('customer_id', $this->customer->id)->get();
        if(count($items)){
            foreach ($items as $key => $item){
                $product = $item->product;
                $curl_para['item'][$key]['product_id'] = $product->id;
                $curl_para['item'][$key]['quantity'] = $item->quantity;
                $curl_para['item'][$key]['manufacturer_id'] = $product->manufacturer_id;
                $curl_para['item'][$key]['product_type'] = 0;
            }

            $ch = curl_init();
            $post_header = [
                'Content-Type: application/json',
                'Accept: application/json',
            ];
            if(\Config::get('app.production')){
                curl_setopt($ch, CURLOPT_URL, "http://153.120.83.199/api/estimate/create");
            }else{
                curl_setopt($ch, CURLOPT_URL, "http://zero-dev.everglory.asia/api/estimate/create");

            }

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curl_para));
            $result = curl_exec($ch);
            curl_close($ch);

            $result = json_decode($result);
        }else{
            $result = new \stdClass();
            $result->error_message = 'estimate cart does not have items';
        }

        return $result;

    }

    public function createCommonCode($today_count, $front_char, $end_char)
    {
        $UID = $today_count;
        $UID++;
        $UID = sprintf("%04d", $UID ); //format 1 => 0001
        $date_str = substr(date('Ymd'),2,6);
        return $front_char.$date_str.$end_char.$UID; //TW150212U0001
    }

    public function getEstimateItemDetail($estimate_item)
    {
        $details = $estimate_item->details->keyBy('attribute');
        $data = new \stdClass();
        foreach ($details as $key => $detail) {
            $data->$key = $detail->value;
        }
        return $data;
    }
    
    public function clear()
    {
        Cart::where('customer_id' , $this->customer->id)->delete();
    }

    public static function getEstimateNote($product_id)
    {
        return EstimateItem::select('note')->where('product_id', $product_id)->where('customer_id', Auth::user()->id)->first();
    }
        
}