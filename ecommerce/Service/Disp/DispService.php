<?php

namespace Ecommerce\Service\Disp;


use Everglory\Models\Disp;
use Ecommerce\Service\BaseService;


class DispService extends BaseService
{
	public function __construct()
	    {

	    }
    public function TestDisp($disp)
    {


		$select_method = $disp['select_method'];
    	$info = $disp['info'];
    	$link = $disp['link'];
    	$customer_ids = $disp['customer_ids'];
		$object_type = "MorphLink";
		$datetime = date('Y-m-d H:i:s');

    	$disp = [];
    	foreach ($customer_ids as $customer_id){
            if($customer_id == null){
                 return redirect()->route("login");
            }else{
            	if($info == null){
            		return redirect('backend-disp-massage')->with('error_log','請輸入文字');
            	}else{
            		if($select_method == 1){
            			$customer_id = $customer_id->id;
            		}
                	$disp[] = [
                		'increment_id' => $link,
                		'info' => $info,
                		'customer_id' => $customer_id,
                		'object_type' => $object_type,
                		'created_at' => $datetime,
                		'updated_at' => $datetime
                	];
					Disp::insert($disp);
            	}
    		}
    	}

    			return true;
	}
	
    public function IntoDisp($data,$customer_ids)
    {	

	    	$start = $data['start'];
	    	$maturity = $data['maturity'];
	    	$info = $data['info'];
	    	$link = $data['link'];
	    	$submitbtn = $data['submitbtn'];
   			$object_type = "MorphLink";
   			$datetime = date('Y-m-d H:i:s');
   			$select_method = $data['select_method'];
        foreach ($customer_ids as  $customer_id){
            if($customer_id == null){
                 return redirect()->route("login");
            }else{
            	if($start == null or $maturity == null){
            		return redirect('backend-disp-massage')->with('error_log','請輸入時間');
            	}elseif($info == null){
            		return redirect('backend-disp-massage')->with('error_log','請輸入文字');
            	}else{
                    $disp = new Disp;
                    $disp->increment_id = $link;
                    $disp->info = $info;
                    $disp->customer_id = $customer_id;
                    $disp->object_type = $object_type;
                    $disp->started_at = $start;
                    $disp->expire_at = $maturity;
                    $disp->created_at = $datetime;
                    $disp->updated_at = $datetime;
                    $disp->save();
            	}
            }
    	}		
		return true;

    }
			

}



?>