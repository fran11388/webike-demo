<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Auth;
use Ecommerce\Core\TableFactory\src\App\Excel\Service as TableFactory;
use Ecommerce\Core\ListIntegration\Program as ListIntegration;

/**
 * Class OrderService
 * @package Ecommerce\Service
 */
class OrderService extends BaseService
{
    /**
     *
     */
    const CACHE_LIFETIME = 24 * 60;

    /**
     * @var \Everglory\Models\Customer|null
     */
    private $customer;

    /**
     * OrderService constructor.
     */
    public function __construct()
    {
        $this->customer = Auth::user();
    }

    /**
     * Get item detail data(order_item_details)
     * @param $order_item
     * @return \stdClass
     */
    public function getItemDetail($order_item)
    {
        $details = $order_item->details->keyBy('attribute');
        $data = new \stdClass();
        foreach ($details as $key => $detail) {
            $data->$key = $detail->value;
        }
        return $data;
    }


    /**
     * Get current status in order(for order detail page's status bar).
     * @param $order
     * @return \stdClass
     */
    public function getStatusProcess($order)
    {
        $status_service = new StatusService;
        $process = $status_service->getProcess($order);
        return $process;
    }

    /**
     * Output file with order data.
     * @param $increment_id
     */
    public function getQuotes($increment_id)
    {
        $cartService = new CartService;
        $accountService = new AccountService();
        $account_info = $accountService->getDefaultAddress();
        $info = new \StdClass;
        $customer =  \Auth::user();
        $doc_type = ' - 訂購明細表';
        $filename = $customer->realname;
        if($account_info->company){
            $filename = $account_info->company;
        }
        $info->filename = $filename.$doc_type.date('(Y-m-d)');
        $info->title = $filename . $doc_type;
        $info->creator = $customer->realname;
        $info->company = $account_info->company;
        $service = new TableFactory($info);
        $service->setAllColsWidth(5.5);
        $service->setAllColsHeight(20);
        $data = [
            'positions' => [
                ['col' => 'C', 'row' => '2'],
                ['col' => 'P', 'row' => '2']
            ],
            'value' => $filename . $doc_type
        ];
        $service->setProgram('addCenterBox', $data);

        $column_config_other = [
            ['from'=>'R', 'to'=>'S'],
            ['from'=>'T', 'to'=>'Y']
        ];
        $historyService = new HistoryService();
        $collection_order = $historyService->getOrders($increment_id);
        $order = $collection_order->first();
        if(!$order){
            app()->abort(404);
        }
        $listIntegration = new ListIntegration($order->items);

        $order_title = ['訂單號碼:','訂購日期:'];
        $num = 2;
        $order_info = [$listIntegration->collection[0]->code,$listIntegration->collection[0]->created_at];
        foreach($order_title as $key => $info){
            foreach($column_config_other as $columnkey => $config){
                if($columnkey == 0){
                    $value = $info;
                }else{
                    $value = $order_info[$key];
                }
                $title = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $num],
                        ['col' => $config['to'], 'row' => $num]
                    ],
                    'value' => $value
                ];

                $service->setProgram('addBox', $title);
            }
            $num++;
        }

        $column_config = [
            ['from'=>'B', 'to'=>'C'],
            ['from'=>'D', 'to'=>'N'],
            ['from'=>'O', 'to'=>'P'],
            ['from'=>'Q', 'to'=>'R'],
            ['from'=>'S', 'to'=>'T'],
            ['from'=>'U', 'to'=>'V'],
            ['from'=>'W', 'to'=>'Y'],
            ['from'=>'Z', 'to'=>'AB'],
            ['from'=>'AC', 'to'=>'AE']
        ];

        $collection = collect();

        $titles = ['項次','品名','數量','一般單價','經銷單價','一般小計','成本小計','經銷利益','經銷毛利'];
        $collection->push($titles);
        $current_customer = \Auth::user();
        $num = 5;
        foreach($listIntegration->collection as $key => $order_info){
            $quantity = $order_info->quantity;
            $row = [
                $key+1,
                '【'.$order_info->manufacturer_name.'】' .$order_info->product_name,
                $quantity,
                str_replace(',','',str_replace('NT$','',$order_info->normal_price)),
                str_replace(',','',str_replace('NT$','',$order_info->price)),
                '=Q'.$num.'*O'.$num,
                '=S'.$num.'*O'.$num,
                '=U'.$num.'-W'.$num,
                '=Z'.$num.'/U'.$num
            ];
            $collection->push($row);
            $num++;
        }


        
        $num = 4;

        foreach ($collection as $key => $data){
            foreach ( $column_config as $column_number => $config ) {
                $cartdata = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $num ],
                        ['col' => $config['to'], 'row' => $num]
                    ],
                    'value' => $data[$column_number],
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $cartdata);    
            }
            $num++;
        }

        $rowInfo = $service->getPosition();
        $maxRow = $rowInfo['maxRow'];
        $remark = [
            'positions' => [
                ['col' => 'B', 'row' => $maxRow + 1 ],
                ['col' => 'V', 'row' => $maxRow + 4]
            ],
            'value' => '備註:',
            'setborder' => 'true'
        ];
        $service->setProgram('addBox', $remark);

        $column_config_other = [
            ['from'=>'W', 'to'=>'AB'],
            ['from'=>'AC', 'to'=>'AE']
        ];
        $num = 4 + count($listIntegration->collection);
        $numAd1 = $num+1;
        $numAd2 = $num+2;
        $numAd3 = $num+3;
        $totalProfit = '=AC'.$numAd1.'-AC'.$numAd2;
        $totalFp = '=AC'.$numAd3.'/AC'.$numAd1;
        $other_title = ['一般定價總價','成本總價','經銷利益總計','經銷總毛利'];
        $other_info = ['=SUM(U5'.':U'.$num.')','=SUM(W5'.':W'.$num.')',$totalProfit,$totalFp];
        foreach($other_title as $key => $info){
            foreach($column_config_other as $columnkey => $config){
                if($columnkey == 0){
                    $value = $info;
                }else{
                    $value = $other_info[$key];
                }
                $title = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $maxRow+1],
                        ['col' => $config['to'], 'row' => $maxRow+1]
                    ],
                    'value' => $value,
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $title);
            }
            $maxRow ++;
        }
        
        $rowInfo = $service->getPosition();
        $end = 'AE'.$rowInfo['maxRow']; 
        $range = 'B2:'.$end;
        $outline = [
            'positions' => [
                        ['col' => 'B2', 'row' => 2],
                        ['col' => 'B2', 'row' => 2]
                    ],
            'range' => $range
        ];
        $service->setProgram('setOutLine', $outline);

        $service->download();
    }
}