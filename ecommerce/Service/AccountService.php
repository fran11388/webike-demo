<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Core\Auth\Hasher;
use Ecommerce\Core\Validate\ValidateEmail;
use Everglory\Models\Customer;
use Everglory\Models\Customer\Address;
use Everglory\Models\Motor;
use Everglory\Models\Customer\Motor AS CustomerMotor;
use Everglory\Constants\Coupon;
use Ecommerce\Service\CouponService;
use Everglory\Models\Customer\Reward as CustomerReward;
use Ecommerce\Core\Wmail;
use Everglory\Constants\CustomerRole;
use Everglory\Models\Customer\Facebook;
use Ecommerce\Service\MissionService;

class AccountService extends BaseService
{
    private $customer;
    private $redirect_uri;
    private $facebook_client_id;
    private $facebook_client_secret;

    public function __construct( $debug = false )
    {
        $this->customer = \Auth::user();
        $this->redirect_uri = route('login-facebook-validate');
        $this->facebook_client_id = '1151450101544941';
        $this->facebook_client_secret = 'f01ef3894c712a9c658eb7fa41ffba71';
//        $this->facebook_client_id = '1285609008129049';
//        $this->facebook_client_secret = 'c7d22ef4d51f8a1a2d8721cc598cccff';
    }

    public function createAccount($request,$password = null)
    {
        // Create account
        $customer = new Customer();
        $customer->email = $request->input('email');
        $hasher = new Hasher;
        $customer->password = $hasher->make($request->input('password'));
        $customer->role_id = '1';
        $customer->newsletter = '1';
        $customer->email_validate_uuid = (string)\Uuid::generate(1);
        $customer->save();

        if(\Config::get('app.production')){
            /*
             * Create MotoMarket Account
             */
            $mm_customer = \Everglory\Models\MotoMarket\Customer::where('email',$customer->email)->first();
            if(!$mm_customer){
                \DB::connection('motomarket')->insert('insert into customers (id, email,password,group_id,created_at,updated_at) values (?, ? ,? ,? , ? , ?)', [$customer->id, $customer->email,$customer->password,1,$customer->created_at,$customer->updated_at]);
            }
        }

//        $wmail->customer($customer,$password);

        return $customer;
    }

    public function InitialAccount($customer)
    {
        $coupon_service = new CouponService($customer);
        $coupon_service->assign(Coupon::NEW_CUSTOMER);
        //Other coupon setting
        if(activeValidate('2018-12-24 00:00:00','2018-12-25 23:59:59')){
            $coupon_service->assign(34,null,'2018-12-26',1);
        }

        $point = new CustomerReward();
        $point->customer_id = $customer->id;
        $point->points_collected = 0;
        $point->points_used = 0;
        $point->points_waiting = 0;
        $point->points_current = 0;
        $point->points_lost = 0;
        $point->save();

        /*
         * black list email
         */

        $lockRoute = \Everglory\Models\Customer\Lock\Route::where('email', $customer->email)->first();
        if($lockRoute){
            $lockRoute->customer_id = $customer->id;
            $lockRoute->save();
        }

        /*
         * Mail
         */
        $wmail = new Wmail();
        $wmail->customer($customer,$customer->real_password);
        if(isset($customer->real_password)){
            unset($customer->real_password);
        }

    }

    public static function syncMotorMarketPassword($customer_id, $hashPassword)
    {
        if(\Config::get('app.production')){
            /*
             * MotoMarket update
             */
            try{
                \DB::connection('motomarket')->update('update customers set password = ? where id = ?', [$hashPassword, $customer_id]);
            } catch (\PDOException $e){
                $db_connect = false;
                $data = new \StdClass;
                $data->log = "connection motomarket is dead, customer_id = " . $customer_id . " change password failed.\r\n".$e->getMessage();
                $data->mail['title'] = '摩托車市陣亡通知';
                $data->mail['content'] = '<div style="margin:100px 0; text-align:center;">您的摩托車市已經陣亡。<br>customer_id=' . $customer_id . '，修改密碼失敗。'.$e->getMessage().'<br><a href="http://www.webike.tw/motomarket/">摩托車市</a></div>';
                \Log::warning($data);
//                    ecNotice($data);
            }
        }
    }

    public function updateAccount($request)
    {
        $message = new \stdClass();
        $customer = $this->customer;
        $customer->nickname = $request->input('nickname');
        $customer->realname = $request->input('realname');
        $customer->gender = (int) $request->input('gender');
        $customer->newsletter = $request->input('newsletter');


        if ( $request->has('password')  ){
            $hasher = new Hasher;
            $customer->password = $hasher->make($request->input('password'));
            self::syncMotorMarketPassword($customer->id, $customer->password);
        }

        $address = Address::where( 'customer_id' , $customer->id )->where('is_default' , '1')->first();
        if (!$address){
            $address = new Address();
        }

        $address->customer_id = $customer->id;
        $address->is_default = '1';
        $address->zipcode = $request->input('address.zipcode');
        $address->county = $request->input('address.county');
        $address->district = $request->input('address.district');
        $address->address = $request->input('address.address');
        $address->firstname = $request->input('address.firstname');
        $address->company = $request->input('address.company');
        $address->vat_number = $request->input('address.vat_number');
        $address->phone = $request->input('address.phone');
        $address->mobile = $request->input('address.mobile');

        //check if customer does not have point data
        if(!$customer->points){
            $point = new CustomerReward();
            $point->customer_id = $customer->id;
            $point->points_collected = 0;
            $point->points_used = 0;
            $point->points_waiting = 0;
            $point->points_current = 0;
            $point->points_lost = 0;
            $point->save();
        }



        if($customer->role_id == CustomerRole::REGISTERED){
            $message->route = 'customer-account-complete-done';
            $message->information = '恭喜你完成完整註冊！';
            $customer->birthday = $request->input('birthday-year') . '-' .$request->input('birthday-month') . '-' . $request->input('birthday-day') ;
            $customer->role_id = 2;
            /*
             * Mail
             */
            $wmail = new Wmail();
            $wmail->customer($customer);
        }else{
            session()->flash('account-edit-success','ok');
            $message->route = 'customer-account-preview';
            $message->information = '會員資料修改成功！';
        }

        $customer->save();
        $address->save();

        /*
         * black list email
         */
//        $lockRoute = \Customer\Lock\Route::where(function($query) use($customer, $address){
//            $query->where('county', $address->county)
//                ->Where('district', $address->district)
//                ->Where('address', $address->address);
//        })->orWhere(\DB::raw('REPLACE(phone, "-", "")'), str_replace('-', '', $address->phone))
//            ->orWhere(\DB::raw('REPLACE(mobile, "-", "")'), str_replace('-', '', $address->mobile))
//            ->first();
//
//        if($lockRoute){
//            $lockRoute->customer_id = $customer->id;
//            $lockRoute->county = $address->county;
//            $lockRoute->zipcode = $address->zipcode;
//            $lockRoute->district = $address->district;
//            $lockRoute->address = $address->address;
//            $lockRoute->phone = $address->phone;
//            $lockRoute->mobile = $address->mobile;
//            $lockRoute->save();
//        }


        $this->updateMyBike($request->input('my-bike'));
        return $message;
    }

    public function updateMyBike($mybikes)
    {
        CustomerMotor::where('customer_id',$this->customer->id)->delete();
        //using url_rewrite
        if($mybikes){
            $ids = Motor::whereIn('url_rewrite',$mybikes)->pluck('id')->toArray();
            //$mybikes

            foreach ($ids as $id) {
                CustomerMotor::create(array(
                    'customer_id' => $this->customer->id,
                    'motor_id' =>$id,
                ));
            }
            /*
             * Mission
            */
            MissionService::mybikeMissionComplete($this->customer);
        }
    }
    public function addMybike($url_rewrite){
        $mybikes = array_merge([$url_rewrite],$this->getMyBike()->pluck('url_rewrite')->toArray());
        $this->updateMyBike(array_unique($mybikes));
    }
    
    public function updateNewsletter($value)
    {
        if($value){
            $this->customer->newsletter = 1;
        }else{
            $this->customer->newsletter = 0;
        }
        return $this->customer->save();
    }

    public function getDefaultAddress()
    {
        $address = $this->customer->address()->where('is_default',1)->first();
        if(!$address){
            $address = new Address();
        }
        return $address;
    }

    public function getMyBike()
    {
        $motor_ids = CustomerMotor::where('customer_id', $this->customer->id)->get()->pluck('motor_id')->all();
        return Motor::whereIn('id', $motor_ids)->get();
    }

    public function getFaceBookAuthUrl()
    {
        if(session()->has('previous_url')){
            session()->put('REDIRECT_TO_FB', session()->get('previous_url'));
        }
        return 'https://www.facebook.com/dialog/oauth?client_id='.$this->facebook_client_id.'&scope=email,public_profile&redirect_uri='. $this->redirect_uri;
    }

    public function validateFacebook($request)
    {
        $return_result = new \stdClass();
        $return_result->message = '';
        $return_result->redirect = false;
        $return_result->redirect_url = route('customer');
        $return_result->blade = 'response.pages.auth.login';
        if($request->has('code')){
            $code = $request->input('code');
            $access_url = 'https://graph.facebook.com/oauth/access_token?client_id='.$this->facebook_client_id.'&redirect_uri='. $this->redirect_uri . '&client_secret='.$this->facebook_client_secret.'&code=' . $code ;

            //Get customer data from facebook after auth success
            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, $access_url );
            //curl_setopt($ch, CURLOPT_URL, "http://www.webike.tw/iopu-gp-bridge.php");

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            curl_close($ch);

            if ( false !==  strpos($result, 'access_token')  ){
                $access_data = json_decode($result);
                if(property_exists($access_data,'access_token')){
                    $data_url = 'https://graph.facebook.com/me?fields=email,name&access_token=' . $access_data->access_token ;
                    $ch     = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $data_url );
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                    $result = curl_exec($ch);
                    curl_close($ch);
                    $result = json_decode($result);

                    if(property_exists($result,'email')){
                        $email = $result->email;
                        $customer_name = $result->name;

                        $customer = Customer::where('email',$email)->first();
                        if(!$customer){
                            //Register
                            $password = str_random(8);
                            $request->request->add(['email' => $email ]);
                            $request->request->add(['password' => $password ]);
                            $customer = $this->createAccount($request,$password);
                            $customer->real_password = $password;
                            $customer_email_verify = new \Ecommerce\Core\Validate\Email\Flow\Customer($customer);
                            ValidateEmail::success($customer_email_verify);
                            $return_result->redirect_url = route('customer-account-create-done');
                        }

                        // has customer will login directly
                        \Auth::login($customer,true);
                        $customer->last_login = date('Y-m-d');
                        $customer->save();

                        // Insert to database
                        Facebook::updateOrCreate(['customer_id' => $customer->id],[
                            'name' => $customer_name,
                            'access_token'=> $access_data->access_token,
                            'expired_at'=> date('Y-m-d H:i:s' , strtotime(date('Y-m-d H:i:s')) + $access_data->expires_in),
                        ]);

                        $return_result->redirect = true;
                        if ( session()->has('REDIRECT_TO_FB') ){
                            $return_result->redirect_url = session()->get('REDIRECT_TO_FB');
                            session()->forget('REDIRECT_TO_FB');
                        }

                    }else{
                        $return_result->message = 'FB驗證失敗，請重試或改以一般登入(004)';
                    }
                }else{
                    $return_result->message = 'FB驗證失敗，請重試或改以一般登入(003)';
                }
            }else{
                $return_result->message = 'FB驗證失敗，請重試或改以一般登入(002)';
            }


        }else{
            $return_result->message = 'FB驗證失敗，請重試或改以一般登入(001)';
        }

        return $return_result;
    }
    
    public function validate($request)
    {
        if(\Auth::attempt(['email'=>$request->input('email'),'password'=> $request->input('password')] , true) ){
           return true; 
        }else{
           return false;
        } 
        
        
    }

    
}