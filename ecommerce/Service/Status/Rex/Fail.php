<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Rex;

use Ecommerce\Service\BaseService;
use Everglory\Models\Order\History;

class Fail extends BaseService
{
    private $name = '未通過';
    public function __construct(&$bar , $rex)
    {
        if(is_null($rex->status->group_name)){
            $process = [
                'name'=> $this->name,
                'active'=> 1 ,
                'text' => '',
                'function' => '' ,
                'function_link' => '',
            ];
            $bar->failed = true;
            $last_history = $rex->histories->sortByDesc('created_at')->first(function ($history, $key) {
                return !in_array($history->status_id,['0']);
            });

            if($last_history){
                $process['text'] = '取消日：'.$last_history->created_at->toDateString();
                $bar->prev_group_name = $last_history->status->group_name;
            }else{
                $bar->prev_group_name = '';
            }


            //Put current status to process
            $bar->process[] = $process;
        }
    }
}