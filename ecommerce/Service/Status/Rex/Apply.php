<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Rex;

use Ecommerce\Service\BaseService;

class Apply extends BaseService
{
    private $name = '申請中';
    public function __construct(&$bar , $rex)
    {

        $process = [
            'name'=> $this->name,
            'active'=>0 ,
            'text' => '',
            'function' => '' ,
            'function_link' => ''
        ];

        if($rex->status->group_name == $this->name){
            $process['active'] = 1;
        }else{

        }



        //Put current status to process
        $bar->process[] = $process;

        //prepare cancel status
        if($bar->failed and $bar->prev_group_name == $this->name){
            $cancel = array_shift($bar->process);
            $bar->process[] = $cancel;
        }
    }
}