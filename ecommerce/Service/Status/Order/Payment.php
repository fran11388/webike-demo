<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Order;

use Ecommerce\Service\BaseService;
use Everglory\Constants\Payment as PaymentConst;

class Payment extends BaseService
{
    private $name = '等待付款中';
    public function __construct(&$bar , $order)
    {
        if($order->payment_method == PaymentConst::Bank_Transfer){
            $process = [
                'name'=> $this->name,
                'active'=>0 ,
                'text' => '',
                'function' => '' ,
                'function_link' => '',
                'function_blank' => false,
            ];


                $process['active'] = 0;

            if($order->status->group_name == $this->name){

                $process['active'] = 1;
//                $process['text'] = '匯款到期日：';
//                if($order->virtual_active){
//                    if($order->virtual_finish){
//                        $process['text'] = '匯款日：';
//                    }else{
//                        $process['text'] = '到期日：'.$order->virtual_deadline;
//                    }
//                }else{
//                    $process['text'] = '已過期';
//                }

                $bar->can_cancel = 1;
                $process['function'] = '取消訂單';
                $process['function_link'] = route('customer-history-order-detail-cancel',$order->increment_id);

            }



            //Put current status to process
            $bar->process[] = $process;

            //prepare cancel status
            if($bar->canceled and $bar->prev_group_name == $this->name){
                $cancel = array_shift($bar->process);
                $bar->process[] = $cancel;
            }
        }
    }
}