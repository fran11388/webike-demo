<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Order;

use Ecommerce\Service\BaseService;

class Estimate extends BaseService
{
    private $name = '交期確認中';
    public function __construct(&$bar , $order)
    {

        $process = [
            'name'=> $this->name,
            'active'=>0 ,
            'text' => '',
            'function' => '' ,
            'function_link' => '',
            'function_blank' => false,
        ];
        if($order->status->group_name == $this->name){
            $bar->can_cancel = 1;
            $process['function'] = '取消訂單';
            $process['function_link'] = route('customer-history-order-detail-cancel',$order->increment_id);
            $process['active'] = 1;
        }else{
            $bar->can_cancel = 0;
        }



        //Put current status to process
        $bar->process[] = $process;

        //prepare cancel status
        if($bar->canceled and $bar->prev_group_name == $this->name){
            $cancel = array_shift($bar->process);
            $bar->process[] = $cancel;
        }
    }
}