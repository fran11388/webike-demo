<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Order;

use Ecommerce\Service\BaseService;
use \Carbon\Carbon;

class Shipping extends BaseService
{
    private $name = '出貨中';
    public function __construct(&$bar , $order)
    {
        $process = [
            'name'=> $this->name,
            'active'=>0 ,
            'text' => '',
            'function' => '' ,
            'function_link' => '',
            'function_blank' => false,
        ];
        if($order->status->group_name == $this->name){
//            $order->histories
            $process['active'] = 1;
        }

        $last_history = $order->histories->sortByDesc('created_at')->first(function ($history, $key) {
            return in_array($history->status_id,[8,9,14]);
        });
        if($last_history){
            $process['text'] = '預定到貨日：';
            $dt = Carbon::now();
            $arrived_at = $last_history->created_at->addDays(1);


            $process['text'] .= $arrived_at->toDateString();

//            if($dt->gt($arrived_at)){
//                $process['text'] .= $dt->addDay(1)->toDateString();
//            }else{
//                $process['text'] .= $arrived_at->toDateString();
//            }
        }

        if($order->tw_shipping){
            $process['function'] = '宅配單號('.$order->tw_shipping.')';
            if(strlen($order->tw_shipping) == 10){
                $process['function_link'] = 'https://www.hct.com.tw/Search/SearchGoods_n.aspx';
            }else{
                $process['function_link'] = 'http://query2.e-can.com.tw/self_link/id_link_c.asp?txtMainid=' . $order->tw_shipping;
            }
            $process['function_blank'] = true;
        }


        //Put current status to process
        $bar->process[] = $process;

        //prepare cancel status
        if($bar->canceled and $bar->prev_group_name == $this->name){
            $cancel = array_shift($bar->process);
            $bar->process[] = $cancel;
        }
    }
}