<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Order;

use Ecommerce\Service\BaseService;
use \Carbon\Carbon;

class Finish extends BaseService
{
    private $name = '配送完成';
    public function __construct(&$bar , $order)
    {

        $process = [
            'name'=> $this->name,
            'active'=>0 ,
            'text' => '',
            'function' => '' ,
            'function_link' => '',
            'function_blank' => false,
            'invoice_number' => '',
            'random_number' => '',
        ];

        $bar->can_return = 0;
        $finish_history = $order->histories->sortByDesc('created_at')->first(function ($history, $key) {
            return $history->status_id == 10;
        });
        if($order->status->group_name == $this->name){
            $process['active'] = 1;


            // $dt = Carbon::now()->subWeek();
            $dt = Carbon::parse('-10 days 00:00:00');

            if($rex = $order->rex and ($rex->type === 'return_all' or !in_array($rex->status_id, [0,15])) ){
                $process['function'] = '已申請('.$rex->increment_id.')';
                $process['function_link'] = route('customer-history-return-detail',$rex->increment_id);
            }else{
                if(strtotime($finish_history->created_at->toDateString()) > strtotime($dt->toDateTimeString())){
                    $process['function'] = '退換貨申請';
//                    if(env('APP_DEBUG', false)){
//                        $process['function'] = '退換貨申請(無七天)';
//                    }
                    $process['function_link'] = route('customer-history-order-detail-return',$order->increment_id);
                    $bar->can_return = 1;
                }
            }

            $accounting_invoices = \DB::connection('eg_accounting')->table("accounting_invoices")->select('invoice_number','random_number')->where('order_id', '=', $order->id)->first();

            if($accounting_invoices){
                $process['invoice_number'] = $accounting_invoices->invoice_number;
                $process['random_number'] = $accounting_invoices->random_number;
            }

        }

        if($finish_history){
            $process['text'] = '完成日：'. $finish_history->created_at->toDateString();
        }
        
        //Put current status to process
        $bar->process[] = $process;

        //prepare cancel status
        if($bar->canceled and $bar->prev_group_name == $this->name){
            $cancel = array_shift($bar->process);
            $bar->process[] = $cancel;
        }
    }
}