<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Status\Order;

use Ecommerce\Service\BaseService;
use \Carbon\Carbon;

class Stocking extends BaseService
{
    private $name = '商品備貨';
    public function __construct(&$bar , $order)
    {

        $process = [
            'name'=> $this->name,
            'active'=>0 ,
            'text' => '',
            'function' => '' ,
            'function_link' => '',
            'function_blank' => false,
        ];


        if($order->status->group_name == $this->name){
            $process['active'] = 1;
        }
        if(!is_null($order->delivered_at)){
            $last_history = $order->histories->sortByDesc('created_at')->first(function ($history, $key) {
                return in_array($history->status_id,[6,7,8,9,10,14,15]);
            });

            $shipping_history = $order->histories->sortByDesc('created_at')->first(function ($history, $key) {
                return $history->status_id == 9;
            });

            if(in_array($last_history->status_id,[8,14])) {
                $dt = Carbon::now();
                $process['text'] = '預定出貨日：' . $dt->toDateString();
            }elseif(in_array($last_history->status_id,[9,10])){
                $process['text'] = '出貨日：' . $shipping_history->created_at->toDateString();
            }else{
//                if(in_array($order->status_id,[6,7])){
                $process['text'] = '預定出貨日：'.$order->delivered_at;
//                }
            }
        }

        //Put current status to process
        $bar->process[] = $process;

        //prepare cancel status
        if($bar->canceled and $bar->prev_group_name == $this->name){
            $cancel = array_shift($bar->process);
            $bar->process[] = $cancel;
        }
    }
}