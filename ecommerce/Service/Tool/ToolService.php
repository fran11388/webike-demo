<?php

namespace Ecommerce\Service\Tool;


use Everglory\Models\cms_file;
use Ecommerce\Service\BaseService;


class ToolService extends BaseService
{
	public function __construct()
	    {

	    }

	public function ImageUpload($photos,$names)
	{
		
		foreach($photos as $key => $photo){
			$name = $names[$key];
			$url = "//img.webike.tw/imageUpload/".$photo;
			$created_at = date('Y-m-d H:i:s');
			$file = ["name" => $name, "url" => $url,"created_at" => $created_at];
			$file = cms_file::insert($file);
		}
			
		return $file;

	}		

}



?>