<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Accessor\OrderAccessor;
use Ecommerce\Core\Wmail;
use Ecommerce\Shopping\Payment\PaymentManager;
use Everglory\Constants\Payment;
use Everglory\Models\Sales\Quote;
use Everglory\Models\Sales\Status;
use Everglory\Models\CreditCardCode;


/**
 * Class PaymentService
 * @package Ecommerce\Service
 */
class PaymentService extends BaseService
{
    /**
     * @var
     */
    private $creditcard;
    /**
     * @var \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private $quote;
    /**
     * @var \Everglory\Models\Customer|null
     */
    private $customer;
    /**
     * @var
     */
    private $error_message;

    /**
     * PaymentService constructor.
     * @param $creditcard
     */
    public function __construct($creditcard)
    {
        $this->creditcard = $creditcard;
        $this->quote = Quote::find($creditcard->quote_id);
        $this->customer= \Auth::user();
    }

    /**
     * Store payment method.
     * @return mixed
     */
    public function store()
    {
        if ($this->quote->payment_method == Payment::CREDIT_CARD ||
            $this->quote->payment_method == Payment::CREDIT_CARD_LAYAWAY_ESUN
        ) {
            $this->storeEsun();
        } else {
            $this->storeNccc();
        }
        return $this->creditcard;
    }

    /**
     * Check payment method.
     * @return bool
     */
    public function verify()
    {
        $payment_method = PaymentManager::getPaymentMethodById($this->quote->payment_method);
        if (!$payment_method->isAvailable()){
            return false;
        }


        if ($this->quote->payment_method == Payment::CREDIT_CARD ||
            $this->quote->payment_method == Payment::CREDIT_CARD_LAYAWAY_ESUN
        ) {
            return $this->verifyEsun();
        } else {
            return $this->verifyNccc();
        }
    }

    /**
     * Check Nccc payment result is success.
     * @return bool
     */
    private function verifyNccc()
    {
        $codes = ['00', '08', '11'];

        if (in_array(request()->get('ResponseCode'), $codes)) {
            return true;
        }
        $this->setErrorMessage('nccc',request()->get('ResponseCode'));
        return false;
    }

    /**
     * Check Esun payment result is success.
     * @return bool
     */
    private function verifyEsun()
    {
        if (request()->get('RC') === '00') {
            return true;
        }
        $this->setErrorMessage('esun',request()->get('RC'));
        return false;
    }

    /**
     * Save Nccc result into database.
     */
    private function storeNccc()
    {
        $this->creditcard->RC = request()->get('ResponseCode');
        $this->creditcard->MID = request()->get('MerchantID');
        $this->creditcard->LTD = request()->get('TransDate');
        $this->creditcard->LTT = request()->get('TransTime');
        $this->creditcard->AN = request()->get('PAN');
        $this->creditcard->AIR = request()->get('ApproveCode');
        $this->creditcard->remark = serialize(request()->all());
        $this->creditcard->save();
    }

    /**
     * Save Esun result into database.
     */
    private function storeEsun()
    {
        $this->creditcard->RC = request()->get('RC');
        $this->creditcard->MID = request()->get('MID');
        $this->creditcard->ONO = request()->get('ONO');
        $this->creditcard->LTD = request()->get('LTD');
        $this->creditcard->LTT = request()->get('LTT');
        $this->creditcard->RRN = request()->get('RRN');
        $this->creditcard->AIR = request()->get('AIR');
        $this->creditcard->AN = request()->get('AN');
        $this->creditcard->remark = serialize(request()->all());
        $this->creditcard->save();
    }

    /**
     * @return mixed
     */
    private function getInstallFromRequest(){
        if (request()->has('Install')){
            return request()->get('Install');
        } else {
            return request()->get('IP');
        }
    }

    /**
     * Deal with credit cart payment process.
     * @return \Everglory\Models\Order
     */
    public function process(){
        $quoteService = new QuoteService($this->quote->id);
        $order = $quoteService->convert();
        if ($this->checkRisk()){
            $order->comment .= "\r\n" . '*風險卡號';
            $order->save();
            $mail_text = '訂單編號：' . $order->increment_id . '卡號被列為風險客戶！訂單已加註備註。';
            $recipient = new \StdClass();
            $recipient->address = 'order@webike.tw';
            $recipient->name = 'Webike Order-吳筱玲';
            $wmail = new Wmail();
            $wmail->blank(null,$recipient,'信用卡風險帳號',$mail_text);
        }
        $status = Status::where('id','1')->firstOrFail();
        $_order = new OrderAccessor($order);
        $_order->changeStatus($status , $this->creditcard->ono);

        if ($this->creditcard){
            $this->creditcard->order_id = $order->id;
            $this->creditcard->save();
        }

        return $order;
    }

    /**
     * Check Risk credit card.
     * @return bool
     */
    private function checkRisk(){
        $risk = false;
        if ($this->quote->payment_method == Payment::CREDIT_CARD ||
            $this->quote->payment_method == Payment::CREDIT_CARD_LAYAWAY_ESUN
        ) {
            if(request()->get('RC') == 'TG'){
                $risk = true;
            }
        } else if ($this->quote->payment_method == Payment::CREDIT_CARD_LAYAWAY_NCCC){
            if(request()->get('RiskMark') == 'Y'){
                $risk = true;
            }
        }
        return $risk;
    }


    /**
     * Set error message
     * @param $method
     * @param $code
     */
    public function setErrorMessage($method, $code)
    {
        if($method == 'esun' or $method == 'nccc'){
            $creditCardCode = CreditCardCode::where('company_code', $method)->where('code', $code)->first();
            $this->error_message = '信用卡授權失敗：';
            if($creditCardCode){
                $this->error_message .= $creditCardCode->description;
            }else{
                $this->error_message .= '請重新結帳';
            }
        }
    }

    /**
     * Get error message
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }

}