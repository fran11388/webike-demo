<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Supplier\Genuinepart;
use Everglory\Constants\CountryGroup;

class GenuinepartsService extends BaseService
{

    const CACHE_LIFETIME = 24 * 60;

    private $manufacturers;

    public function __construct()
    {

    }

    public static function getManufacturersByNames($api_usage_names)
    {
        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        $result = Genuinepart::with('manufacturer')->where('display', 1)->whereIn('api_usage_name', $api_usage_names)->orderby('display_sort')->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;

    }

    /**
     * Get all manufacturers of genuineparts.
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getManufacturers(){

        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        $result = Genuinepart::with(['manufacturer','motor_manufacturer'])->where('display', 1)->orderby('display_sort')->get();

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function divideCountryManufacturers()
    {
        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        //defined result's content and sort
        $result = [
            CountryGroup::TW['name'] => [],
        ];
        //data arrange
        $genuine_manufacturers = self::getManufacturers();
        foreach ($genuine_manufacturers as $genuine_manufacturer){
            $manufacturer = $genuine_manufacturer->manufacturer;
            $result[$manufacturer->country]['group'][] = $genuine_manufacturer;
            if(in_array($manufacturer->country, CountryGroup::DOMESTIC['group'])){
                $result[$manufacturer->country]['url_code'] =CountryGroup::DOMESTIC['url_code'];
            }else{
                $result[$manufacturer->country]['url_code'] = CountryGroup::IMPORT['url_code'];
            }
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    /**
     * All genuineparts of manufacturers was divide into import and domestic.
     *
     * @return array|mixed
     */
    public function divideImportManufacturers()
    {
        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        //defined result's content and sort
        $result = [];
        $result[CountryGroup::IMPORT['name']]['url_code'] = CountryGroup::IMPORT['url_code'];
        $result[CountryGroup::DOMESTIC['name']]['url_code'] = CountryGroup::DOMESTIC['url_code'];

        //data arrange
        $genuine_manufacturers = self::getManufacturers();
        foreach ($genuine_manufacturers as $genuine_manufacturer){
            $manufacturer = $genuine_manufacturer->manufacturer;
            if(in_array($manufacturer->country, CountryGroup::DOMESTIC['group'])){
                $result[CountryGroup::DOMESTIC['name']]['group'][] = $genuine_manufacturer;
            }else{
                $result[CountryGroup::IMPORT['name']]['group'][] = $genuine_manufacturer;
            }
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }

        return $result;
    }

    public function divideSectManufacturers()
    {
        $cache_key = self::getCacheKey();
        if ( useCache() and $result = \Cache::get($cache_key) ) {
            return $result;
        }

        //defined result's content and sort
        $result = [
            CountryGroup::TW['name'] => ['icons' => CountryGroup::TW['icons'], 'code' => CountryGroup::TW['code']],
            CountryGroup::JP['name'] => ['icons' => CountryGroup::JP['icons'], 'code' => CountryGroup::JP['code']],
            CountryGroup::EU_US['name'] => ['icons' => CountryGroup::EU_US['icons'], 'code' => CountryGroup::EU_US['code']],
//            CountryGroup::TH['name'] => ['icons' => CountryGroup::TH['icons'], 'code' => CountryGroup::TH['code']],
        ];
        //data arrange
        $genuine_manufacturers = self::getManufacturers();
        foreach ($genuine_manufacturers as $genuine_manufacturer){
            $manufacturer = $genuine_manufacturer->manufacturer;
            if(in_array($manufacturer->country, CountryGroup::EU_US['group'])){
                $result[CountryGroup::EU_US['name']]['url_code'] = CountryGroup::IMPORT['url_code'];
                $result[CountryGroup::EU_US['name']]['group'][] = $genuine_manufacturer;
            }else{
                $result[$manufacturer->country]['group'][] = $genuine_manufacturer;
                if(in_array($manufacturer->country, CountryGroup::DOMESTIC['group'])){
                    $result[$manufacturer->country]['url_code'] =CountryGroup::DOMESTIC['url_code'];
                }else{
                    $result[$manufacturer->country]['url_code'] = CountryGroup::IMPORT['url_code'];
                }
            }
        }

        if ($result) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    /**
     * Get genuineparts of manufacturer by cusomter mybike motors.
     *
     * @param $customer
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static|static[]
     */
    public static function getMybikeFitManufacturer($customer)
    {
        //get genuineparts manufacturer
        $suppliers = self::getManufacturers();
        if($customer){
            $motor_manufacturers = $customer->motors->keyBy('manufacturer_id');
            $motor_manufacturers_keys = array_keys($motor_manufacturers->toArray());

            $_suppliers = $suppliers->filter(function ($manufacturer, $key) use($motor_manufacturers_keys) {
                if($manufacturer->motor_manufacturer){
                    return in_array($manufacturer->motor_manufacturer->id,$motor_manufacturers_keys) ;
                }
                return false;
            });

            if(count($_suppliers)){
                $suppliers = $_suppliers;
            }
        }

        return $suppliers;
    }
}