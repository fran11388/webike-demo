<?php

namespace Ecommerce\Service;

use Everglory\Constants\CustomerRole;
use Everglory\Models\Campaign;
use Maatwebsite\Excel\Facades\Excel;

class CampaignService
{
    public function __construct()
    {

    }

    public function getRenderCampaign($url_rewrite,$default_data)
    {
        $campaign_data = Campaign::with('items','layout','items.template')
            ->where('url_rewrite','=',$url_rewrite);

         if(!$default_data->current_customer || $default_data->current_customer->role_id != CustomerRole::STAFF){
             $campaign_data = $campaign_data->where('publish_at','<=',date('Y-m-d h:i:s'));
         }


        $campaign_data = $campaign_data->first();

        if(!$campaign_data){
            abort(404);
        }

        $item_block = $this->getRenderItemBlock($campaign_data);
        $data = (array)json_decode($campaign_data->data);
        $default_data->seo('title', $data['seo_title']);
        $default_data->seo('description', $data['seo_description']);

        return $this->getRenderLayout($campaign_data,$item_block,$default_data);
    }

    public function getRenderItemBlock($campaign_data)
    {
        // $this->save();
        $result = null;
        $promotion_layout = $campaign_data->layout;
        $main_data = (array)json_decode($campaign_data->data);

        foreach($campaign_data->items->sortBy('sort') as $item){
            $data = array_merge($main_data,(array)json_decode($item->data));
            $data['blade'] = $item->template->blade;
            $data['week_active'] = strtotime(date('Y-m-d h:i:s')) <= strtotime("+7 day",strtotime($campaign_data->publish_at)) ? true : false;
            $data['rel_parameter'] = "rel_parameter=".$campaign_data->year.$campaign_data->url_rewrite;
            if($item->active){
                $class_name = __NAMESPACE__.'\\Campaign\\' . $promotion_layout->code .'\\' .$item->block_code;
                if(class_exists($class_name)){
                    $item_class = new $class_name();
                    $result .= $item_class->read($data);
                }
            }
        }

        return $result;
    }

    private function getRenderLayout($campaign_data,$item_block,$default_data)
    {
        $item_view['item_block'] = $item_block;

        $main_data = (array)json_decode($campaign_data->data);
        $data = array_merge($main_data,$item_view);
        $data = array_merge($data,(array)$default_data);
        $data['blade'] = $campaign_data->layout->blade;
        $data['rel_parameter'] = "rel_parameter=".$campaign_data->year.$campaign_data->url_rewrite;
        $result = null;
        $class_name = __NAMESPACE__.'\\Campaign\\' . $campaign_data->layout->code .'\\Layout';
        if(class_exists($class_name)){
            $item_class = new $class_name();
            $result .= $item_class->read($data);
        }

        return $result;
    }

    public function getValueByExcelFile($file)
    {
        $result = new \StdClass;
        $excels = [];
        Excel::load($file, function($reader) use(&$excels,&$result){

            $objExcel = $reader->getExcel();
            $sheet = $objExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 1; $row <= $highestRow; $row++)
            {
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);
                // str_replace( " ", "", $rowData[0][1]);
                if(!empty($rowData[0][3])){
                    $excels[] = $rowData[0];
                }

            }
        });

        return $excels;
    }

    public function importCampaignData($data)
    {
        $layout = $this->getUseLayout($data);
        $campaign_id = $this->importCampaignMainData($data,$layout);
        $this->importCampaignItemData($data,$layout,$campaign_id);
    }

    public function importCampaignMainData($data,$layout)
    {
        $class_name = __NAMESPACE__.'\\Campaign\\' . $layout .'\\Layout';
        if(class_exists($class_name)){
            $item_class = new $class_name();
            $result = $item_class->write($data);

            return $result;
        }
    }

    public function importCampaignItemData($data,$layout,$campaign_id)
    {
        $use_blades = $this->getUseBladeData($data);
        foreach($use_blades as $use_blade){
            $class_name = __NAMESPACE__.'\\Campaign\\' . $layout .'\\'.$use_blade;
            if(class_exists($class_name)){
                $item_class = new $class_name();
                $item_class->write($data,$campaign_id,$use_blade);
            }
        }
    }

    private function getUseLayout($data)
    {
        $layout = null;
        foreach($data as $first_key => $data_arr){
            if($data_arr[1] == '版型'){
                $layout = $data_arr[3];
            }
        }

        return $layout;

    }

    private function getUseBladeData($data)
    {
        $result = [];
        foreach($data as $first_key => $data_arr){
            if($data_arr[0] == 'O'){
                $result[] = $data_arr[2];
            }
        }

        return $result;
    }


    
    public function save()
    {
               $data = ['main_color_r' => 252,'main_color_g' => 255,'main_color_b' => 0,'anchor_background_color' => '#f1c232','anchor_color' => '#000000','anchor_background_hover_color' => '#f1e597','main_color_text'=>'#000000','promotion_title'=> 'Mini Bike專屬週','promotion_introduce'=> '從1964年開始市售的HONDA Monkey50掀起的MINI BIKE旋風，在台灣直到MSX125、Z125PRO的出現，騎士們才感受到小型打檔車的魅力。MINI BIKE泛指輪框尺寸在12吋以下的小型打檔車，不僅車身輕巧且充滿趣味，其無窮的改裝潛力也是許多玩家入手的原因，Scrambler、Café Racer、Offroad...不管是怎樣的風格都有許多改裝品可以套用；知名的改裝大廠如SP武川、KITACO等也推出了許多改裝套件，舉凡加大汽缸套件、油冷套件、五速變速箱套件...等，你的MINI BIKE該是如何，由你自己定義！','url_rewrite'=> 'Minibike','seo_title' => 'Webike Mini Bike主題周｜小檔車必備改裝分類、品牌 玩樂無極限','seo_description' => 'MINI BIKE 小檔車泛指輪框尺寸在12吋以下的小型打檔車，不僅車身輕巧且充滿趣味，其無窮的改裝潛力也是許多玩家入手的原因，Scrambler、Café Racer、Offroad...不管是怎樣的風格都有許多改裝品可以套用；知名的改裝大廠如SP武川、KITACO等也推出了許多改裝套件，舉凡加大汽缸改缸套件、油冷套件、五速變速箱套件、腳踏後移、排氣管、避震、短牌架...等，你的MINI BIKE該是如何，由你自己定義！'];

//         $data = [
// 'title' => '主題區塊',
// 'images' => [
//     'https://img.webike.tw/imageUpload/15471980151547198015831.jpg',
//     'https://img.webike.tw/imageUpload/15471980681547198068966.png',
//     'https://img.webike.tw/imageUpload/15471980981547198098767.jpg',
//     'https://img.webike.tw/imageUpload/15471981131547198113858.jpg',
//     'https://img.webike.tw/imageUpload/15471981371547198137612.jpg'
//     ],
// 'mobile_image' => "https://img.webike.tw/imageUpload/15471980981547198098767.jpg"
// ];

       // $data = [
       //     'title' => '錨點區塊',
       //     'name' => [
       //         '熱血的你，從這裡下手',
       //         '仿賽車TOP品牌',
       //         '猜你也會喜歡',
       //         '車主專區',
       //     ],
       //     'anchor' => [
       //              'CategoryDouble',
       //              'BrandDiscount',
       //              'BrandDouble',
       //              'MotorDouble',
       //          ]
       //      ];
        

       // $data = [
       //     'title' => '仿賽車TOP品牌',
       //     'url_rewrites' => [
       //      35,
       //      854,
       //      2135,
       //      72,
       //      2056,
       //      2062,
       //      1519,
       //      466,
       //      863,
       //      666,
       //      2224,
       //      2480,
       //      635,
       //      40,
       //      1206
       //     ],
       // ];

       // $data = [
       //     'title' => '猜你也會喜歡',
       //     'url_rewrites' => [
       //          '1624',
       //          't3226',
       //          't2836',
       //          't2570',
       //          't1895'
       //     ],
       // ];

       // $data = [
       //     'title' => '熱血的你，從這裡下手',
       //     'url_rewrites' => [
       //          '1000-1001-1003',
       //          '1000-1110-1109',
       //          '1000-1311-1050-6340',
       //          '1000-1311-1080-1083',
       //          '1000-1110-1101-1310',
       //          '1000-1180-1316-1120',
       //          '1000-1110-1112',
       //          '1000-1030-1034-1041',
       //          '3000-3020-3060-3061',
       //          '3000-3001-3002',
       //          '1000-1110-1127-3208',
       //          '1000-1150-1152',
       //          '3000-3020-3025',
       //          '3000-1325-3084',
       //          '3000-3020-3024',
       //          '3000-3027'
       //     ],
       //     'names' => [
       //          '排氣管尾段',
       //          '頭罩、擋風鏡',
       //          '腳踏後移',
       //          '懸吊避震器',
       //          '坐墊整流罩',
       //          '引擎外蓋',
       //          '整流罩',
       //          '防甩頭',
       //          '賽車手套',
       //          '全罩式安全帽',
       //          '油箱保護貼',
       //          '齒盤',
       //          '連身皮衣',
       //          '道路車靴',
       //          '內穿服',
       //          '護具'
       //     ],
       //     'images' => [
       //          'https://img.webike.tw/imageUpload/15471989541547198954569.jpg',
       //          'https://img.webike.tw/imageUpload/15471988641547198864559.JPG',
       //          'https://img.webike.net/catalogue/images/35120/ss-aa2134b_1.jpg',
       //          'https://img.webike.net/catalogue/10364/TTX-GP_01.jpg',
       //          'https://img.webike.net/catalogue/images/47457/99994066754X-131285623137308572.jpg',
       //          'https://img.webike.net/catalogue/images/40911/EC-R6-2008-SET-GBR.jpg',
       //          'https://img.webike.net/catalogue/images/55699/9738N.jpg',
       //          'https://img.webike.net/catalogue/10367/SDRT_image-1.jpg',
       //          'https://img.webike.tw/imageUpload/15471988331547198833730.jpg',
       //          'https://img.webike.tw/imageUpload/15488149101548814910100.jpg',
       //          'https://img.webike.net/catalogue/images/41060/EVO928BL_01.jpg',
       //          'https://img.webike.net/catalogue/10470/afam_aluminium.jpg',
       //          'https://img.webike.tw/imageUpload/15488149321548814932838.jpg',
       //          'https://img.webike.net/catalogue/images/21932/97b98dcd11c9c383536d069a97420afb.jpg',
       //          'https://img.webike.net/catalogue/images/46340/RSU307BK01_01.jpg',
       //          'https://img.webike.net/catalogue/images/35149/1921-01_01.jpg'
       //     ],
       //     'tag_colors' => [
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000'
       //     ],
       //     'tag_text' => [
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍'
       //     ]
       // ];

       // $data = [
       //     'title' => '車主專區',
       //     'url_rewrites' => [
       //          303,
       //          926,
       //          672,
       //          465,
       //          896,
       //          256
       //     ],
       //     'names' => [
       //         'CBR1000RR','YZF-R1','GSX-R1000','ZX-10R','YZF-R6','CBR600RR'
       //     ],
       //     'images' => [
       //         'https://img.webike.tw/imageUpload/15472017151547201715761.jpg',
       //         'https://img.webike.tw/imageUpload/15472016621547201662395.jpg',
       //         'https://img.webike.tw/imageUpload/15472016461547201646981.jpg',
       //         'https://img.webike.tw/imageUpload/15472016331547201633174.jpg',
       //         'https://img.webike.tw/imageUpload/15472016751547201675863.jpg',
       //         'https://img.webike.tw/imageUpload/15472017021547201702466.jpg'
       //     ],
       //     'tag_colors' => [
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //          '#ff0000',
       //     ],
       //     'tag_text' => [
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //          '點數2倍',
       //     ]
       // ];

       //      $data = ['title' => "What's NEW 流行特輯"];
       dd(json_encode($data));

    }




}