<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Customer;
use Everglory\Models\Coupon;
use Everglory\Models\Coupon\Base;
use Everglory\Models\Coupon\History;
use Carbon\Carbon;
use Uuid;
use Auth;
use Illuminate\Support\Collection;

class CouponService extends BaseService
{
    private $customer;
    private $unlimit_time;

    public function __construct( Customer $customer )
    {
        $dt = Carbon::now();
        $dt->addYears(100);
        $this->unlimit_time = $dt->toDateString();
        $this->customer = $customer;
    }

    public function assign($coupon_base_id,$customer = null ,$time = null,$quantity = 1)
    {
        if(!$customer){
            $customer = $this->customer;
        }
        if(!$time){
            $time = $this->unlimit_time;
        }
        $base = Base::findOrFail($coupon_base_id);
        for ($i = 0; $i < $quantity; $i++) {
            $history = new History;
            $history->base_id = $base->id;
            $history->tag = date('Y') . '-' . (int)date('m');
            $history->name = $base->name;
            $history->customer_ids = $customer->id;
            $history->discount = $base->discount;
            $history->time_limit = $base->time_limit;
            $history->time_limit_unit = $base->time_limit_unit;
            $history->save();

            $coupon = new Coupon();
            $coupon->customer_id = $customer->id;
//            $coupon->base_id = $base->id;
            $coupon->uuid = (string)Uuid::generate(1);
            $coupon->name = $base->name;
            $coupon->code = $base->code;
            $coupon->discount = $base->discount;
            $coupon->discount_type = $base->discount_type;
            $coupon->history_id = $history->id;
            $coupon->expired_at = $time;
            $coupon->save();
        }
    }
}