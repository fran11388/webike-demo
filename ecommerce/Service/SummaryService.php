<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:25
 */

namespace Ecommerce\Service;

use Ecommerce\Core\Solr\Param\CommonParam;
use Ecommerce\Core\Solr\Param\FacetFieldParam;
use Ecommerce\Core\Solr\Param\FacetQueryParam;
use Ecommerce\Core\Solr\Param\FacetRangeParam;
use Ecommerce\Core\Solr\Param\FilterQueryParam;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Ecommerce\Core\Solr\Searcher;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Repository\MotorRepository;
use Everglory\Models\Category;
use Everglory\Models\Customer;
use Everglory\Models\Manufacturer;
use Everglory\Models\Motor;
use Illuminate\Http\Request;
use SolrQuery;

class SummaryService extends BaseService
{
    private $searcher;
    private $customer;

    private $category;
    private $motor;
    private $manufacturer;
    private $keyword = '';
    private $titles;
    private $not_found_message = 'Condition Not found';

    const CACHE_LIFETIME = 30;

    public function __construct(Searcher $searcher, Customer $customer )
    {
        $this->searcher = $searcher;
        $this->customer = $customer;
        if(\Auth::check()){
            $this->customer = \Auth::user();
        }
    }


    /**
     * @return Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return Motor|null
     */
    public function getMotor()
    {
        return $this->motor;
    }

    /**
     * @return Manufacturer|null
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }


    /**
     * Validate url and assign data .Otherwise return false.
     *
     * @param Request $request get html Request information
     * @return bool|string
     */
    public function urlValidate($request)
    {
//        if($request->has('category') && $request->has('manufacturer') && $request->has('motor')){
//            return modifySummaryUrl(null);
//        }

        if($request->has('motor')) {
            $motor = MotorRepository::find($request->get('motor'), 'url_rewrite');
            $this->motor = $motor;
            if(!$motor){
                \App::abort(404,$this->not_found_message);
            }
        }

        if($request->has('category')){

            //REDIRECT
            foreach(\Everglory\Constants\Category::REDIRECTS as $redirect_from => $redirect_to){
                if(false !== strpos($request->input('category'),$redirect_from)){
                    return modifySummaryUrl(['ca'=>str_replace($redirect_from,$redirect_to,$request->input('category'))]);
                }
            }

            $category = CategoryRepository::find($request->get('category'), 'url_path');
            $this->category = $category;

            if(!$category or !$mptt = $category->mptt or !in_array(explode('-',$mptt->url_path)[0],\Everglory\Constants\Category::TOP)){
                \App::abort(404,$this->not_found_message);
            }
        }

        if($request->has('manufacturer')) {
            $manufacturer = ManufacturerRepository::find($request->get('manufacturer'), 'url_rewrite');
            $this->manufacturer = $manufacturer;
            if(!$manufacturer){
                \App::abort(404,$this->not_found_message);
            }
        }

        return false;
    }

    /**
     * Get the title of specific summary page.
     *
     * @return string
     */
    public function getSummaryTitle()
    {

        $titles = ['mt'=>'','ca'=>'','br'=>''];

        if($this->motor){
            $titles['mt'] = $this->motor->manufacturer->name . ' ' . $this->motor->name. ($this->motor->synonym ? '('.$this->motor->synonym.')' : '');
        }
        if($this->category){
            $titles['ca'] = $this->category->name. ( $this->category->synonym ? '('.$this->category->synonym.')' : '' );
        }
        if($this->manufacturer){
            $titles['br'] = $this->manufacturer->name. ($this->manufacturer->synonym ? '('.$this->manufacturer->synonym.')' : '');
        }
        $this->titles = $titles;
        return implode(' & ' ,array_filter($titles));
    }

    /**
     * Validate which type of request.And get new products within the set number of days from Solr.
     *
     * @param Request $request  get html Request information
     * @param bool $date_limit
     * @return SearchResponse
     */
    public function selectNewProduct(Request $request , $date_limit = true)
    {
        $searcher = clone $this->searcher;
        $common = new CommonParam();

        $filterQueries = [];

        $common->setRows(100);
        $common->setSortField('created_at');

        // New product : 180 days
        if($date_limit){
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('created_at');
            $filterQuery->setQuery('[NOW-180DAY/DAY TO NOW]');
            $filterQueries[] = $filterQuery;
        }

        $searcher->setCommon($common);

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            $this->motor = $motor;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setTag('motor');
            $filterQuery->setQuery($motor->id);
            $filterQueries[] = $filterQuery;
        }

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            $this->manufacturer = $manufacturer;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setTag('manufacturer');
            $filterQuery->setQuery($manufacturer->url_rewrite);
            $filterQueries[] = $filterQuery;
        }

        $category = $request->get('category', null);
        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            $this->category = $category;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setTag('category');
            $filterQuery->setQuery($category->mptt->url_path);
            $filterQueries[] = $filterQuery;
        }

        $type = $request->get('type', null);
        if(!is_null($type)){
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('type');
            $filterQuery->setQuery($type);
            $filterQueries[] = $filterQuery;
        }


        $searcher->setFilterQueries($filterQueries);

        //group result
        $searcher->setGroupField('relation_product_id');
        $searcher->setGroupSort('created_at');

        return $searcher->query();
    }

    /**
     * Validate which type of request.And get on sales products from Solr.
     *
     * @param Request $request get html Request information
     * @return SearchResponse
     */
    public function selectOnSales(Request $request){

        $searcher = clone $this->searcher;
        $common = new CommonParam();

        $filterQueries = [];

        $common->setRows(7);

        $price_field = 'price_1';
        if((int)$this->customer->role_id !== 0){
            $price_field = 'price_' . (int)$this->customer->role_id;
        }

        $cache_key = get_class() . '@' . __FUNCTION__ . '?'.http_build_query($request->all(),'','&') . '&' .$price_field;
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }


        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            $this->motor = $motor;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setTag('motor');
            $filterQuery->setQuery($motor->id);
            $filterQueries[] = $filterQuery;
        }

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            $this->manufacturer = $manufacturer;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setTag('manufacturer');
            $filterQuery->setQuery($manufacturer->url_rewrite);
            $filterQueries[] = $filterQuery;
        }

        $category = $request->get('category', null);
        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            $this->category = $category;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setTag('category');
            $filterQuery->setQuery($category->mptt->url_path);
            $filterQueries[] = $filterQuery;
        }

        $searcher->setFilterQueries($filterQueries);

        //group result
        $searcher->setGroupField('relation_product_id');
        $searcher->setGroupSort('div('.$price_field.',price)');

        return $searcher->query();
    }


    /**
     * Validate which type of request.Get recommends products and manufacturers information from solr.
     *
     * @param Request $request get html Request information
     * @return SearchResponse
     */
    public function selectSummary(Request $request ){
        $searcher = clone $this->searcher;
        $common = new CommonParam();
        $facetFields = [];
        $facetQueries = [];
        $facetRanges = [];
        $filterQueries = [];


        $common->setRows(30);
        $common->setSortField('visits');
        $searcher->setCommon($common);

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            $this->motor = $motor;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setTag('motor');
            $filterQuery->setQuery($motor->id);
            $filterQueries[] = $filterQuery;
        }

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            $this->manufacturer = $manufacturer;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setTag('manufacturer');
            $filterQuery->setQuery($manufacturer->url_rewrite);
            $filterQueries[] = $filterQuery;
        }

        $category = $request->get('category', null);

        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            $this->category = $category;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setTag('category');
            $filterQuery->setQuery($category->mptt->url_path);
            $filterQueries[] = $filterQuery;

            $url_path = implode("-", explode("-", $category->mptt->url_path, -1));
            if ($url_path) {
                $facetQuery = new FacetQueryParam();
                $facetQuery->setField('url_path');
                $facetQuery->setQuery($url_path);
                $facetQueries[] = $facetQuery;
            }
        }

        $country = $request->get('country', null);

        if ($country) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_country');
            $filterQuery->setTag('country');
            $filterQuery->setQuery($country);
            $filterQueries[] = $filterQuery;
        }

        $facetField = new FacetFieldParam();
        $facetField->setField('manufacturer_country');
        $facetField->setExcludeTags(['country']);
        $facetField->setSort( SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;

        $facetField = new FacetFieldParam();
        $facetField->setField('url_path');
        $facetField->setExcludeTags(['category']);
        $facetFields[] = $facetField;

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['manufacturer']);
        $facetField->setField('manufacturer_name_url');
        $facetField->setSort( SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['motor_name_detail']);
        $facetField->setField('motor_name_detail');
        $facetField->setSort( SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;


        $searcher->setFilterQueries($filterQueries);
        $searcher->setFacetFields( $facetFields);
        $searcher->setFacetQueries( $facetQueries);
        $searcher->setFacetRanges( $facetRanges );

        //group result
        $searcher->setGroupField('relation_product_id');
        $searcher->setGroupSort('visits');

        return $searcher->query();
    }

    public function selectFitMotorIds( Request $request , $motor_ids = null ){
        $searcher = clone $this->searcher;
        if (!is_null($motor_ids) and !count($motor_ids)){
            return null;
        }

        $common = new CommonParam();

        $filterQueries = [];

        $common->setRows(30);
        $common->setSortField('visits');
        if($motor_ids){
            $common->setQuery( 'motor_id:(' . implode(" OR " , $motor_ids) . ')');
        }
        $searcher->setCommon($common);

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            $this->manufacturer = $manufacturer;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setTag('manufacturer');
            $filterQuery->setQuery($manufacturer->url_rewrite);
            $filterQueries[] = $filterQuery;
        }

        $category = $request->get('category', null);
        if ($category) {
            if($category === '*'){
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('url_path');
                $filterQuery->setTag('category');
                $filterQuery->setQuery('(' . implode(" OR " , \Everglory\Constants\Category::TOP) . ')');
                $filterQueries[] = $filterQuery;
            }else{
                $category = CategoryRepository::find($category, 'url_path');
                $this->category = $category;
                $filterQuery = new FilterQueryParam();
                $filterQuery->setField('url_path');
                $filterQuery->setTag('category');
                $filterQuery->setQuery($category->mptt->url_path);
                $filterQueries[] = $filterQuery;
            }
        }

        $searcher->setFilterQueries($filterQueries);

        $facetField = new FacetFieldParam();
        $facetField->setField('motor_name_detail');
        $facetFields[] = $facetField;
        $searcher->setFacetFields( $facetFields);

        //group result
        $searcher->setGroupField('relation_product_id');
        $searcher->setGroupSort('visits');

        return $searcher->query();
    }

    /**
     * Validate which type of request.Get top sales products and manufacturers information from solr.
     *
     * @param Request $request get html Request information
     * @return SearchResponse
     */
    public function selectTopSales(Request $request )
    {
        $searcher = clone $this->searcher;
        $common = new CommonParam();

        $filterQueries = [];

        $limit = 5;
        if($request->get('limit', null)){
            $limit = $request->get('limit', null);
        }
        $common->setRows($limit);
        $common->setSortField('popular');
        $searcher->setCommon($common);

        // New product : 180 days

        /*
         * Some category will get under limit quantity with this filter.
         */
//        $filterQuery = new FilterQueryParam();
//        $filterQuery->setField('popular');
//        $filterQuery->setQuery('[0 TO *]');
//        $filterQueries[] = $filterQuery;

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            $this->motor = $motor;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setTag('motor');
            $filterQuery->setQuery($motor->id);
            $filterQueries[] = $filterQuery;
        }

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            $this->manufacturer = $manufacturer;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setTag('manufacturer');
            $filterQuery->setQuery($manufacturer->url_rewrite);
            $filterQueries[] = $filterQuery;
        }

        $category = $request->get('category', null);
        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            $this->category = $category;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setTag('category');
            $filterQuery->setQuery($category->mptt->url_path);
            $filterQueries[] = $filterQuery;
        }

        $country = $request->get('country', null);

        if ($country) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_country');
            $filterQuery->setTag('country');
            $filterQuery->setQuery($country);
            $filterQueries[] = $filterQuery;
        }

        $char = $request->get('char', null);

        if($char){
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_name');
            $filterQuery->setTag('manufacturer_name');

            if($char == '數字'){
                $filterQuery->setQuery('/([0-9])(.*?)/');
            }elseif($char == '其他'){
                $filterQuery->setQuery('/([^a-zA-Z0-9])(.*?)/');
            }else{
                $filterQuery->setQuery($char . '*');
            }

            $filterQueries[] = $filterQuery;
        }

        $searcher->setFilterQueries($filterQueries);

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['manufacturer']);
        $facetField->setField('manufacturer_name_url');
        $facetField->setSort( SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;
        $searcher->setFacetFields( $facetFields);

        //group result
        $searcher->setGroupField('relation_product_id');
        $searcher->setGroupSort('popular');


        return $searcher->query();
    }

    /**
     * @param Request $request
     * @return SearchResponse
     */
    public function selectManufacturerSummary(Request $request ){
        $cache_key = self::getCacheKey($request->fullUrl());
        if (useCache() and $result = \Cache::tags(['solr'])->get($cache_key)) {
            return $result;
        }

        $searcher = clone $this->searcher;
        $common = new CommonParam();
        $facetFields = [];
        $facetQueries = [];
        $facetRanges = [];
        $filterQueries = [];


        $common->setRows(1);
        $searcher->setCommon($common);

        $motor = $request->get('motor', null);
        if ($motor) {
            $motor = MotorRepository::find($motor, 'url_rewrite');
            if(!$motor){
                return false;
            }
            $this->motor = $motor;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('motor_id');
            $filterQuery->setTag('motor');
            $filterQuery->setQuery($motor->id);
            $filterQueries[] = $filterQuery;
        }

        $manufacturer = $request->get('manufacturer', null);
        if ($manufacturer) {
            $manufacturer = ManufacturerRepository::find($manufacturer, 'url_rewrite');
            $this->manufacturer = $manufacturer;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_url');
            $filterQuery->setTag('manufacturer');
            $filterQuery->setQuery($manufacturer->url_rewrite);
            $filterQueries[] = $filterQuery;
        }

        $category = $request->get('category', null);

        if ($category) {
            $category = CategoryRepository::find($category, 'url_path');
            $this->category = $category;
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('url_path');
            $filterQuery->setTag('category');
            $filterQuery->setQuery($category->mptt->url_path);
            $filterQueries[] = $filterQuery;

            $url_path = implode("-", explode("-", $category->mptt->url_path, -1));
            if ($url_path) {
                $facetQuery = new FacetQueryParam();
                $facetQuery->setField('url_path');
                $facetQuery->setQuery($url_path);
                $facetQueries[] = $facetQuery;
            }
        }

        $country = $request->get('country', null);

        if ($country) {
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_country');
            $filterQuery->setTag('country');
            $filterQuery->setQuery($country);
            $filterQueries[] = $filterQuery;
        }

        $char = $request->get('char', null);

        if($char){
            $filterQuery = new FilterQueryParam();
            $filterQuery->setField('manufacturer_name');
            $filterQuery->setTag('manufacturer_name');

            if($char == '數字'){
                $filterQuery->setQuery('/([0-9])(.*?)/');
            }elseif($char == '其他'){
                $filterQuery->setQuery('/([^a-zA-Z0-9])(.*?)/');
            }else{
                $filterQuery->setQuery($char . '*');
            }

            $filterQueries[] = $filterQuery;
        }


        $facetField = new FacetFieldParam();
        $facetField->setField('manufacturer_country');
        $facetField->setExcludeTags(['country']);
        $facetField->setSort( SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;

        $facetField = new FacetFieldParam();
        $facetField->setField('url_path');
        $facetField->setExcludeTags(['category']);
        $facetFields[] = $facetField;

        $facetField = new FacetFieldParam();
        $facetField->setExcludeTags(['manufacturer']);
        $facetField->setField('manufacturer_name_url');
        $facetField->setSort( SolrQuery::FACET_SORT_COUNT);
        $facetFields[] = $facetField;


        $searcher->setFilterQueries($filterQueries);
        $searcher->setFacetFields( $facetFields);
        $searcher->setFacetQueries( $facetQueries);
        $searcher->setFacetRanges( $facetRanges );

        //group result
        $searcher->setGroupField('manufacturer_id');
        $searcher->setGroupSort('visits');
        $searcher->setGroupFacet(true);

        $result = $searcher->query();
        if ($result) {
            \Cache::tags(['solr'])->put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public function importSegmentsToRequest()
    {
        $segments = request()->segments();
        try {
            for ($i = 0; $i < count($segments) ; $i += 2) {
                $key = $segments[$i];
                $value = $segments[$i + 1];
                switch ($key) {
                    case 'ca':
                        request()->request->add(['category' => $value ]);
                        break;
                    case 'br':
                        request()->request->add(['manufacturer' => $value ]);
                        break;
                    case 'mt':
                        request()->request->add(['motor' => $value ]);
                        break;
                }

            }
        }catch (\Exception $e) {
        }
    }

}