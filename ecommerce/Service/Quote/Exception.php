<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/08/15
 * Time: 上午 10:37
 */

namespace Ecommerce\Service\Quote;


class Exception extends \Exception
{
    public $check;
    public $redirect;
    public $message = [];
    public $increment_id;

}