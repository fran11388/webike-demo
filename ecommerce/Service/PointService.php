<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Customer\Guess as Customer_Guess;
use Everglory\Models\Customer\Mission as Customer_Mission;
use Everglory\Models\Customer\Reward as Customer_Reward;
use Everglory\Models\Order\Reward as Order_Reward;
use Everglory\Models\Guess;
use Everglory\Models\Mission;


class PointService extends BaseService
{
    public function __construct($creditcard)
    {
        $this->customer= \Auth::user();
    }

    public static function givePoint( $mission, $customer, $points )
    {
        $customer_mission = Customer_Mission::where('customer_id', $customer->id)->where('mission_id', $mission->id)->first();
        if (!$customer_mission) {
            $customer_mission = new Customer_Mission;
            $customer_mission->customer_id = $customer->id;
            $customer_mission->mission_id = $mission->id;
            $customer_mission->reward_status = 0;
        }
        //give point
        if ($customer_mission->reward_status == 0) {
            $point = $customer->points;
            if (!$point) {
                $point = new Customer_Reward();
                $point->customer_id = $customer->id;
                $point->points_collected = 0;
                $point->points_used = 0;
                $point->points_waiting = 0;
                $point->points_current = 0;
                $point->points_lost = 0;
            }

            $rewards = new Order_Reward();
            $rewards->customer_id = $customer->id;
            $rewards->description = $mission->name;

            $rewards->points_current = $points;
            $rewards->start_date = date("Y-m-d");
            $rewards->end_date = date("Y-m-d", strtotime("+1 year"));
            $rewards->save();

            $point->points_current += $points;
            $point->points_collected += $points;
            $point->save();

            $customer_mission->reward_status = 1;
            $customer_mission->save();

            return true;
        }
    }

    public static function givePointWithoutMission( $customer, $description, $give, $end_at = null )
    {
        //give point
        $point = $customer->points;
        if ( !$point ){
            $point = new Customer_Reward();
            $point->customer_id = $customer->id;
            $point->points_collected = 0;
            $point->points_used = 0;
            $point->points_waiting = 0;
            $point->points_current = 0;
            $point->points_lost = 0;
        }

        $rewards = new Order_Reward();
        $rewards->customer_id = $customer->id;
        $rewards->description = $description;
        $points = $give;

        $rewards->points_current = $points;
        $rewards->start_date = date("Y-m-d");
        if($end_at){
            $rewards->end_date = $end_at;
        }else{
            $rewards->end_date = date("Y-m-d",strtotime("+1 year"));
        }
        $rewards->save();

        $point->points_current += $points;
        $point->points_collected  += $points;
        $point->save();
    }
}