<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;


use Everglory\Models\Feed;
use Everglory\Models\Feed\Attribute;
use Carbon\Carbon;
use Everglory\Models\News\Meta;

class FeedService extends BaseService
{

    const CACHE_LIFETIME = 5;

    private $data;
    private $data_type;
    private $top_feed_avoid_types;

    public function __construct()
    {

        $this->top_feed_avoid_types = ['新服務', '車輛召回', '業界情報', '系統公告'];
    }

    public function feed($data_category,$data_type)
    {
        $this->data['data_category'] = $data_category;
        $this->data_type = $data_type;
    }

    public function add($data = [])
    {
        $attributes = array_pull($data,'attributes');
        $data_id = array_pull($data,'data_id');
        $news_feed = Feed::updateOrCreate(['data_id'=> $data_id ,'data_type'=> $this->data_type ],array_merge($data,$this->data));

        $attribute_data = [];
        foreach ( $attributes as $attribute_type => $attribute ) {
            foreach($attribute as $attribute_value){
                $attribute_data[] = ['feed_id'=>$news_feed->id , 'attribute_type' => $attribute_type, 'attribute_id' => $attribute_value];
            }
        }
        if(count($attribute_data)){
            Attribute::insert($attribute_data);
        }
    }

    public function getFeeds($from,$to,$condition = null)
    {
//        $feeds = Feed::whereNull('deleted_at')->where('display_date','<',Carbon::now()->toDateTimeString())->where('display_date','>',Carbon::create(2016,10,31,0)->toDateTimeString())->orderBy('display_date','desc')->get();
        $feeds = Feed::whereNull('deleted_at')->where('display_date','<',Carbon::create(2016,11,01,0)->toDateTimeString())->where('display_date','>',Carbon::create(2016,10,31,0)->toDateTimeString())->orderBy('display_date','desc')->get();
        if($condition){
            dd($feeds);
            $result = collect();
            foreach($feeds as $feed) {
                $attributesGroup = $feed->attributes->groupBy('attribute_type');
                foreach($attributesGroup as $type => $attributes){

                }

            }
        }else{
            return $feeds;
        }
    }

    public function checkFeedsSituation($data_type = array(),$hours,$check_dt,$check_number = 1)
    {
        // 在 [$data_type] 中 ， [$check_dt] 前 [$hours] 小時內 如果出現多於 [$check_number] 的數量 則回傳 True
        $dt = clone $check_dt;
        $display_time[] = $dt->toDateTimeString();
        $dt->subHours($hours);
        $display_time[] = $dt->toDateTimeString();
        echo 'check range:'.implode(',',$display_time)."\r\n";
        $counts = Feed::whereNull('deleted_at')->whereIn('data_type',$data_type)->whereBetween('display_date',array_reverse($display_time))->count();

        if($counts > $check_number){
            return true;
        }else{
            return false;
        }
    }

    public function getQueueFeeds($data_type , $limit , $conditions = null)
    {
        $dt = Carbon::create(2016, 11, 1, 8);
        return Feed::whereNull('deleted_at')->where('data_type',$data_type)->where('display_date','>',$dt->toDateTimeString())->take($limit)->get();
    }

    public function getFeedsNew($page,$conditions = [])
    {
        //        \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        $token = csrf_token();
        \Debugbar::addMessage($token , 'feed');
//        $token = 'abdc';
        $cache_initial =  $token  . '?key=initial';
        $next = 0;

        if(\Cache::has($cache_initial)){
            $variable = \Cache::get($cache_initial);
//            \Debugbar::addMessage($variable , 'feed');
            $normal_feeds = $variable->normal_feeds;
            $sticky_feeds = $variable->sticky_feeds;
            $ad_feeds_group = $variable->ad_feeds_group;
            $normal_count = rand($variable->normal_count_min,$variable->normal_count_max);
        }else{

            $variable = new \stdClass();

            //頂端
            $variable->top_feed = null;

            //每頁請求數
            $variable->num_per_request = 10;
            //總請求數
            $variable->max_requests = 200;

            $variable->total_page = $variable->max_requests / $variable->num_per_request;
            // clear all page cache;
            for ($i = 1; $i <= $variable->total_page; $i++) {
                \Cache::forget( $token  . '?page='.$i);
            }

            //目前時間
            $variable->dt = Carbon::now();
            $now = $variable->dt->toDateTimeString();
            //本次feeds的normal最大、最小數量
            $variable->normal_count_min = 115;
            $variable->normal_count_max = 125;
            //本次feeds的normal隨機最終數量
            $normal_count = rand($variable->normal_count_min,$variable->normal_count_max);

            //取得Normal/Sticky/Ad三種種類
            $normal_feeds = Feed::where('module','normal')
                ->where('display_date','<',$now)
                ->where(function ($query) use ($now) {
                    $query->whereNull('deadline')
                        ->orWhere('deadline', '>', $now);
                })
                ->whereNull('sticky_sort')
                ->orderBy('display_date','desc')->take($normal_count + $variable->num_per_request)->get();

            $sticky_feeds = Feed::whereNotNull('sticky_sort')
                ->where(function ($query) use ($now) {
                    $query->whereNull('deadline')
                        ->orWhere('deadline', '>', $now);
                })
                ->orderBy('sticky_sort','asc')->get();
            

            $sticky_count = count($sticky_feeds);
            if($sticky_count){
                $variable->top_feed = $sticky_feeds->shift();    
            }



            // Custom AD by conditions
            /*
                Example:
                $conditions['motor_id'] = [2218,336];
                $conditions['category_id'] = [132];
             */

            if(count($conditions)){
                $select_query = ' SUM( CASE ';
                foreach ($conditions as $attribute_type => $attribute_ids){
                    $select_query .= ' WHEN attribute_type = "'. $attribute_type .'" and attribute_id in ('.implode(",",$attribute_ids).')  THEN 1 ';
                }

                $select_query .= 'ELSE 0 END ) as score , feeds.*';
            }else{
                $select_query = '0 as score,feeds.*';
            }

            $ad_feeds = Feed::where('module','ad')
                ->leftJoin('feed_attributes','feeds.id','=','feed_attributes.feed_id')
                ->select(\DB::raw($select_query))
                ->groupBy('feeds.id')
                ->orderBy('score','desc')
                ->where(function ($query) use ($now) {
                    $query->whereNull('deadline')
                        ->orWhere('deadline', '>', $now);
                })
                ->whereNull('sticky_sort')
                ->orderBy('display_date','desc')
                //->take($variable->max_requests - $normal_count - $sticky_count + $variable->num_per_request )
                ->get();

            //shuffle
//            $ad_feeds = $ad_feeds->shuffle();

            $ad_feeds = $ad_feeds->groupBy('data_type');
            $ad_feeds_group = collect();
            foreach ($ad_feeds as $type => $ad_feed){
                $ad_feeds_group = $ad_feeds_group->merge($ad_feed->take(50));
            }
            $ad_feeds_group = $ad_feeds_group->shuffle();

        }

        $cache_page = $token  . '?page='.$page;
//        \Cache::forget($cache_page);
        if(\Cache::has($cache_page)){
            $result =  \Cache::get($cache_page);
        }else{

            $sticky_count_current = count($sticky_feeds);
//            dd($sticky_count_current);
            //normal display range

            $max = ceil(  ( $normal_count / $variable->num_per_request ) / ($variable->max_requests / 100) );
            $min = floor(  ( $normal_count / $variable->num_per_request ) / ($variable->max_requests / 100) );

            $num_remain_block = $variable->num_per_request - $sticky_count_current;
            $result = [];
            if($num_remain_block <= 0){
                \Debugbar::addMessage('全置頂' , 'feed');
                for ($i = 1; $i <= $variable->num_per_request; $i++) {
                    $result[$i] = $sticky_feeds->shift();
                }
            }else{
                \Debugbar::addMessage('剩餘區塊'.($num_remain_block) , 'feed');

                $normal_display_num = round( rand($min,$max) * $num_remain_block / $variable->num_per_request);
                \Debugbar::addMessage('Normal區塊'.($normal_display_num) , 'feed');

                $ad_display_num = $num_remain_block - $normal_display_num;

                $result_sticky = [];
                for ($i = 1; $i <= $variable->num_per_request; $i++) {
                    if(count($sticky_feeds)){
                        $result_sticky[$i] = $sticky_feeds->shift();
                    }else{
//                        \Debugbar::addMessage('normal rate:' .$normal_display_num / $num_remain_block , 'feed');
//                        \Debugbar::addMessage('ad rate:' .$ad_display_num / $num_remain_block , 'feed');
                        $set = [
                            'normal' => $normal_display_num / $num_remain_block,
                            'ad' => $ad_display_num / $num_remain_block,
                        ];
                        $rand = $this->checkWithSet($set);
//                        echo 'random result:'.$rand.'<br/>';
                        if($rand == 'normal'){
                            if(!$variable->top_feed) {
                                $variable->top_feed = $normal_feeds->first(function ($feed, $key) use($normal_feeds){
                                    if(!in_array($feed->data_type,$this->top_feed_avoid_types) and $feed->data_category == '摩托新聞' ){
                                        unset($normal_feeds[$key]);
                                        return true;
                                    }
                                });
                            }else{
                                $result[$i] =  $normal_feeds->shift();
                            }
                            $normal_display_num--;
                        }else{
//                            // list key
//
//                            $ad_feeds_types = $ad_feeds_group->keys()->toArray();
//
//                            //rand type
//                            $rand_type = $ad_feeds_types[rand(0,count($ad_feeds_types)-1)];
//                            //get group
//                            $choice_ad_feeds = $ad_feeds_group[$rand_type];
//                            //get content
//                            $result[$i] = $choice_ad_feeds->shift();
//                            //check if collection take over
//                            if(count($choice_ad_feeds) == 0){
//                                unset($ad_feeds_group[$rand_type]);
//                            }

                            $result[$i] = $ad_feeds_group->shift();
                            $ad_display_num--;
                        }
                        $num_remain_block--;
                    }
                }
    //            $result = $this->shuffle_assoc($result);
                $result = array_merge($result_sticky + $result);
            }
            $variable->normal_feeds = $normal_feeds;
            $variable->sticky_feeds = $sticky_feeds;
            $variable->ad_feeds_group = $ad_feeds_group;


            \Cache::put($cache_page, $result, self::CACHE_LIFETIME);
        }
        \Cache::put($cache_initial, $variable, self::CACHE_LIFETIME);
        $html = '';
        foreach($result as $feed){
            $html .= view('response.pages.home.partials.feed', compact('feed'))->render();
        }
        if($page < $variable->total_page ){
            $next = $page + 1;
        }
//        dd(\Cache::get($cache_initial));
        return ['html'=>$html,'next'=>$next , 'top' => $variable->top_feed ];
    }

    private function shuffle_assoc($list) {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $list[$key];
        }
        return $random;
    }

    private function checkWithSet(array &$set, $length=10000)
    {
        $left = 0;
        foreach($set as $num=>$right)
        {
            $set[$num] = $left + $right*$length;
            $left = $set[$num];
        }
        $test = mt_rand(1, $length);
        $left = 1;
        foreach($set as $num=>$right)
        {
            if($test>=$left && $test<=$right)
            {
                return $num;
            }
            $left = $right;
        }
        return null;//debug, no event realized
    }

    public function UpdateScheduleNews()
    {
        $dt = Carbon::now();
        $dt->addMinute();
        $feeds = Feed::where('data_category','摩托新聞')->where('display_date','<',$dt->toDateTimeString())->onlyTrashed()->get();
        foreach ($feeds as $feed){
            $post = \Everglory\Models\News\Post::where('ID',$feed->data_id)->first();
            if(!$post or $post->post_status == 'trash'){
                $feed->forceDelete();
            }elseif($post->post_status == 'publish'){
                $feed->restore();
            }
        }
    }
}

/*
 *
 SELECT * from (
SELECT
(UNIX_TIMESTAMP('2016-11-06 00:00:00') / 604800  - UNIX_TIMESTAMP(display_date) / 604800 ) as date_score,
EXP(-8 * (UNIX_TIMESTAMP('2016-11-06 00:00:00') / 604800  - UNIX_TIMESTAMP(display_date) / 604800 ) * (UNIX_TIMESTAMP('2016-11-06 00:00:00') / 604800  - UNIX_TIMESTAMP(display_date) / 604800 )) as score,
feeds_copy.* FROM feeds_copy where data_category = 'Product') AS tmp
where date_score > 0 order by score desc;

 */