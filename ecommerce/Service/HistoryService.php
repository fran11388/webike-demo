<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Auth;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Repository\ProductRepository;

use Everglory\Models\Customer;
use Everglory\Models\Order;
use Everglory\Models\Rex;
use Everglory\Models\Estimate;
use Everglory\Models\Estimate\Item as EstimateItem;
use Everglory\Models\GroupBuy;
use Everglory\Models\Mitumori;
use Everglory\Constants\EstimateType;
use Everglory\Models\Order\Reward;
use Illuminate\Support\Collection;

class HistoryService extends BaseService
{
    const CACHE_LIFETIME = 24 * 60;
    const PAGINATE_LIMIT = 20;

    private $customer;

    public function __construct()
    {
        $this->customer = Auth::user();
    }

    public function getOrders($increment_id = 'all', $customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        $queryBuilder = $this->getOrderQueryBulider($increment_id, $id);
        return $queryBuilder->get();
    }

    public function getOrdersWithPaginate($increment_id = 'all', $customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        $queryBuilder = $this->getOrderQueryBulider($increment_id, $id);
        return $queryBuilder->paginate(self::PAGINATE_LIMIT);
    }

    private function getOrderQueryBulider($increment_id = 'all', $customer_id)
    {
        $orders = Order::with(['status', 'items', 'items.details', 'items.gift', 'address', 'payment', 'histories', 'histories.status', 'rex', 'virtualbank'])->whereNull('relation_id')->orderby('created_at', 'DESC');

        if ($increment_id !== 'all') {
            if (is_array($increment_id)) {
                $orders = $orders->whereIn('increment_id', $increment_id);
            } else {
                $orders = $orders->where('increment_id', $increment_id);
            }
        }

        if (is_array($customer_id)) {
            $orders = $orders->whereIn('customer_id', $customer_id);
        } else {
            $orders = $orders->where('customer_id', $customer_id);
        }
        return $orders;
    }

    public function getNotFinishOrders($customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        return $this->getNotFinishOrdersQueryBuilder($id)->get();
    }

    public function getNotFinishOrdersWithPaginate($customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        return $this->getNotFinishOrdersQueryBuilder($id)->paginate(self::PAGINATE_LIMIT);
    }

    private function getNotFinishOrdersQueryBuilder($customer_id)
    {
        $orders = Order::with(['status', 'items', 'address', 'payment'])->whereNotIn('status_id', [0, 10])->orderby('created_at', 'DESC');

        if (is_array($customer_id)) {
            $orders = $orders->whereIn('customer_id', $customer_id);
        } else {
            $orders = $orders->where('customer_id', $customer_id);
        }

        return $orders;
    }

    public function getTargetEstimate($estimate_code, $customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        $estimates = Estimate::orderby('created_at', 'DESC')->where(function ($query) {
            $query->where(function ($query) {
                $query->where('updated_at', '>=', \Carbon\Carbon::now()->subDays(15)->format('Y-m-d'))
                    ->where('created_at', '>=', '2016-09-13');
            });
        });

        if (is_array($estimate_code)) {
            $estimates = $estimates->whereIn('increment_id', $estimate_code);
        } else {
            $estimates = $estimates->where('increment_id', $estimate_code);
        }

        if (is_array($id)) {
            $estimates = $estimates->whereIn('customer_id', $id)->get();
        } else {
            $estimates = $estimates->where('customer_id', $id)->get();
        }

        return $estimates;
    }

    public function getTypeEstimate($type_id = 'all', $paginate = false)
    {
        $id = $this->customer->id;
        $estimates = Estimate::orderby('created_at', 'DESC')->where(function ($query) {
            $query->where(function ($query) {
                $query->where('updated_at', '>=', \Carbon\Carbon::now()->subDays(15)->format('Y-m-d'))
                    ->where('created_at', '>=', '2016-09-13');
            });
        });

        $perPage = self::PAGINATE_LIMIT;
        if ($type_id !== 'all') {
            if (is_array($type_id)) {
                if (in_array(EstimateType::GENERAL, $type_id)) {
                    $perPage = 5;
                    $estimates = $estimates->with(['status', 'items', 'items.details', 'items.purchases', 'items.purchases.delivery']);
                }
                $estimates = $estimates->whereIn('type_id', $type_id);
            } else {
                if (EstimateType::GENERAL == $type_id) {
                    $estimates = $estimates->with('status', 'items', 'items.details', 'items.purchases', 'items.purchases.delivery');
                }
                $estimates = $estimates->where('type_id', $type_id);
            }
        }

        if (request()->get('increment_id')) {
            $estimates = $estimates->where('increment_id', request()->get('increment_id'));
        }

        if ($paginate) {
            $estimates = $estimates->where('customer_id', $id)->paginate($perPage);
        } else {
            $estimates = $estimates->where('customer_id', $id)->get();
        }
        return $estimates;
    }

    public function getEstimateItem($estimate_code = 'all', $customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }

        $items = EstimateItem::with(['estimate', 'details', 'manufacturer', 'purchases', 'purchases.delivery'])->orderby('created_at', 'DESC')
            ->whereHas('estimate', function ($query) use ($id, $estimate_code) {
                $query->where(function ($query) {
                    $query->where('updated_at', '>=', \Carbon\Carbon::now()->subDays(15)->format('Y-m-d'))
                        ->where('created_at', '>=', '2016-09-13');

                });
                if (is_array($id)) {
                    $query->whereIn('customer_id', $id);
                } else {
                    $query->where('customer_id', $id);
                }

                if ($estimate_code !== 'all') {
                    if (is_array($estimate_code)) {
                        $query->whereIn('increment_id', $estimate_code);
                    } else {
                        $query->where('increment_id', $estimate_code);
                    }
                }
            })->get();

        return $items;
    }

    public function getValidGroupBuy($estimate_code = 'all', $customer_id = NULL)
    {
        $id = $this->customer->id;
        $groupbuy = GroupBuy::with('items')->where('response_date', '>', date('Y-m-d', strtotime('-7 days')))->orderby('request_date', 'DESC');
        if ($customer_id) {
            $id = $customer_id;
        }

        if ($estimate_code !== 'all') {
            if (is_array($estimate_code)) {
                $groupbuy = $groupbuy->whereIn('estimate_code', $estimate_code);
            } else {
                $groupbuy = $groupbuy->where('estimate_code', $estimate_code);
            }
        }

        if (is_array($id)) {
            $groupbuy = $groupbuy->whereIn('customer_id', $id)->get();
        } else {
            $groupbuy = $groupbuy->where('customer_id', $id)->get();
        }

        return $groupbuy;
    }

    public function getValidMitumori($estimate_code = 'all', $customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        $mitumori = Mitumori::with('items')->where('response_date', '>', date('Y-m-d', strtotime('-7 days')))->orderby('request_date', 'DESC');

        if ($estimate_code !== 'all') {
            if (is_array($estimate_code)) {
                $mitumori = $mitumori->whereIn('estimate_code', $estimate_code);
            } else {
                $mitumori = $mitumori->where('estimate_code', $estimate_code);
            }
        }

        if (is_array($id)) {
            $mitumori = $mitumori->whereIn('customer_id', $id)->get();
        } else {
            $mitumori = $mitumori->where('customer_id', $id)->get();
        }

        return $mitumori;
    }

    public function getRexes($increment_id = 'all', $customer_id = NULL)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }
        $rexes = Rex::with(['status', 'items', 'address'])->whereNull('relation_id')->orderby('created_at', 'DESC');

        if ($increment_id !== 'all') {
            if (is_array($increment_id)) {
                $rexes = $rexes->whereIn('increment_id', $increment_id);
            } else {
                $rexes = $rexes->where('increment_id', $increment_id);
            }
        }

        if (is_array($id)) {
            $rexes = $rexes->whereIn('customer_id', $id)->get();
        } else {
            $rexes = $rexes->where('customer_id', $id)->get();
        }

        return $rexes;
    }

    public function getRewards($customer_id = null)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }

        return $this->getRewardQueryBuilder($id)->get();
    }

    public function getRewardsWithPaginate($customer_id = null)
    {
        $id = $this->customer->id;
        if ($customer_id) {
            $id = $customer_id;
        }

        return $this->getRewardQueryBuilder($id)->paginate(self::PAGINATE_LIMIT);
    }

    private function getRewardQueryBuilder($customer_id)
    {
        $rewards = Reward::with(['order', 'order.status'])->where('status', 1)->orderby('created_at', 'DESC');
//        $rewards = Reward::with(['order', 'order.status'])
//            ->where(function($query){
//                $query
//                    ->where(function($query){
//                        $query
//                            ->whereNull('order_id')
//                            ->whereNotNull('description');
//                    })
//                    ->orWhere(function($query){
//                        $query
//                            ->whereHas('order', function($query){
//                                $query
//                                    ->where('orders.status_id','10');
//                            });
//                    });
//            })
//            ->orderby('created_at', 'DESC');

        if (is_array($customer_id)) {
            $rewards = $rewards->whereIn('customer_id', $customer_id);
        } else {
            $rewards = $rewards->where('customer_id', $customer_id);
        }

        return $rewards;
    }

    public function setRead($object, $record_col = 'read')
    {
        $object->$record_col = 1;
        $object->save();
    }


    public static function itemsExtractManufactures($customer_id)
    {
        $order_items = Order\Item::with('manufacturer')
            ->where('customer_id', $customer_id)
            ->whereHas('order', function ($query) {
                $query->whereNull('relation_id');
                $query->where('status_id', 10);
            })
            ->whereNotNull('sku')
            ->groupBy('manufacturer_id')
            ->get();
        $m = [];
        foreach ($order_items as $order_item) {
            if ($order_item->manufacturer) {
                $m[] = $order_item->manufacturer;
            }
        }
        return ($m);
    }

    public static function itemsExtractFirstLayerCategories($customer_id)
    {
        $order_items = Order\Item::with('details')
            ->where('customer_id', $customer_id)
            ->whereHas('order', function ($query) {
                $query->whereNull('relation_id');
                $query->where('status_id', 10);
            })
            ->whereNotNull('sku')
            ->groupBy('sku')
            ->get();

        $categories = [];
        foreach ($order_items as $order_item) {
            $category_name_path = $order_item->details->filter(function ($value, $key) {
                return $value->attribute == 'category_name_path';
            })->first()->value;
            $category_url_path = $order_item->details->filter(function ($value, $key) {
                return $value->attribute == 'category_url_path';
            })->first()->value;
            $url_rewrite = explode('-', $category_url_path)[0];
            $name = explode('|', $category_name_path)[0];
            if ($url_rewrite and $name) {
                $categories[$url_rewrite] = $name;
            }
        }
        return $categories;
    }

    public static function getMyStyleSortingWay()
    {
        return [
            'date_n_o' => '購買日期 新~舊',
            'date_o_n' => '購買日期 舊~新',
            'fq_h_l' => '購買頻率 高~低',
            'fq_l_h' => '購買頻率 低~高',
            'times_m_f' => '購買次數 多~少',
            'times_f_m' => '購買次數 少~多',
            'count_m_f' => '商品數量 多~少',
            'count_f_m' => '商品數量 少~多',
        ];
    }

    public static function parseProductOptions($string)
    {
        if($string=='') return [];
        $options = [];

        $ignore_answers = [
            '是的，我已經了解了。',
        ];

        foreach (explode('|', $string) as $option) {
            $parsed = explode('$', $option);
            $question = $parsed[0];
            $answer = $parsed[1];

//            $question=str_replace('請選擇','',$question);
            if (in_array($answer, $ignore_answers)) continue;
            $options[$question] = $answer;
        }
        return $options;
    }

    public static function orderItemsAttachAccessor($items){
        foreach ($items as $item) {
            if ($item->product) {
                $item->productAccessor = new ProductAccessor($item->product);
            } else {
                $item->productAccessor = null;
            }
        }
    }

    public static function getMystyleOrderItems($customer_id){
        $items = Order\Item::with('manufacturer', 'details', 'order', 'customer', 'product.prices', 'product.images', 'product.manufacturer')
            ->select(\DB::raw('order_items.*,
	count( * ) AS buy_times,
	sum( order_items.quantity ) AS buy_count,
	DATEDIFF( max( order_items.created_at ), min( order_items.created_at ) ) / count( * ) AS buy_fq,
	max(order_items.created_at) as last_buy,
	ei.note
	'))
            ->where('order_items.customer_id', $customer_id)
            ->leftJoin('estimate_items as ei', 'ei.id', '=', 'order_items.cache_id')
            ->whereHas('order', function ($query) {
                $query->whereNull('relation_id');
                $query->where('status_id', 10);
            })
            ->whereNotNull('order_items.sku');

        if (request()->has('m_id')) {
            $m_id = request()->get('m_id');
            $items = $items->where('order_items.manufacturer_id', $m_id);
        }


        if (request()->has('c_identifier')) {
            $c_identifier = request()->get('c_identifier');
            $items = $items->whereHas('details', function ($query) use ($c_identifier) {
                $query->where('attribute', '=', 'category_url_path');
                $query->where('value', 'like', $c_identifier . '%');
            });
        }


        if (request()->has('keyword')) {
            $keyword = request()->get('keyword');
            $items = $items->where(function ($whereQuery) use ($keyword) {
                $whereQuery->whereHas('details', function ($query) use ($keyword) {
                    $query->where('attribute', '=', 'product_name');
                    $query->where('value', 'like', '%' . $keyword . '%');
                })->orWhere('remark', 'like', '%' . $keyword . '%');
            });
        }


        switch (request()->get('sort')) {
            case 'date_n_o':
                $items = $items->orderBy('last_buy', 'desc');
                break;
            case 'date_o_n':
                $items = $items->orderBy('last_buy');
                break;
            case 'fq_h_l':
                $items = $items->orderBy('buy_fq');
                break;
            case 'fq_l_h':
                $items = $items->orderBy('buy_fq', 'desc');
                break;
            case 'times_m_f':
                $items = $items->orderBy('buy_times', 'desc');
                break;
            case 'times_f_m':
                $items = $items->orderBy('buy_times');
                break;
            case 'count_m_f':
                $items = $items->orderBy('buy_count', 'desc');
                break;
            case 'count_f_m':
                $items = $items->orderBy('buy_count');
                break;
            default:
                $items = $items->orderBy('last_buy', 'desc');
        }
        $items = $items->groupBy('order_items.product_id')->paginate(20);
        HistoryService::orderItemsAttachAccessor($items);
        return $items;
    }

    public static function getMystyleGroupedOrderItems($items){
        $sku_grouped_order_items = Order\Item::with('manufacturer', 'details', 'order', 'customer', 'product.prices', 'product.images', 'product.manufacturer')
            ->whereIn('customer_id', $items->pluck('customer_id'))
            ->whereIn('sku', $items->pluck('sku'))
            ->whereHas('order', function ($query) {
                $query->where('relation_id', '=', null);
                $query->where('status_id', 10);
            })
            ->whereNotNull('sku')
            ->get();
        HistoryService::orderItemsAttachAccessor($sku_grouped_order_items);
        $sku_grouped_order_items = $sku_grouped_order_items
            ->groupBy('sku')
            ->map(function ($item, $key) {
                return $item->sortByDesc('order.created_at');
            });

        return $sku_grouped_order_items;
    }

}