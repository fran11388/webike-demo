<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/11/2
 * Time: 上午 11:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Product;
use Everglory\Models\Supplier\Product as SupplierProduct;

class SupplierService extends BaseService
{
    public function addDomesticProductSupplier($product)
    {
        if($product and !$product->supplier){
            $supplierProduct = new SupplierProduct;
            $supplierProduct->product_id = $product->id;
            $supplierProduct->supplier_id = 1;
            $supplierProduct->sort = 1;
            $supplierProduct->status_id = 1;
            $supplierProduct->orig_cost = $product->cost;
            $supplierProduct->rate = 1;
            $supplierProduct->cost = $product->cost;
            $supplierProduct->save();
        }
    }

    public static function isDomesticSupplier($value, $column = 'product_id')
    {
        $product_id = null;
        $column_basic = ['product_suppliers.product_id', 'product_suppliers.supplier_id'];
        $supplierProductQuery = SupplierProduct::with('supplier');

        if($column == 'sku'){
            if(is_array($value)){
                $supplierProductQuery->join('products', 'product_suppliers.product_id', '=', 'products.id')->whereIn('products.sku', $value);
            }else{
                $supplierProductQuery->join('products', 'product_suppliers.product_id', '=', 'products.id')->where('products.sku', $value);
            }
            $column_basic[] = 'products.sku';
        }else{
            if(is_array($value)){
                $supplierProductQuery->whereIn($column, $value);
            }else{
                $supplierProductQuery->where($column, $value);
            }
        }
        $result = $supplierProductQuery->select($column_basic)->get()->pluck('supplier_id', $column)->toArray();
        foreach ($result as $key => $supplier_id){
            if($supplier_id != 1){
                $result[$key] = true;
            }else{
                $result[$key] = false;
            }
        }
        return $result;
    }
}