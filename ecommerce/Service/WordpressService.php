<?php

namespace Ecommerce\Service;

class WordpressService extends BaseService
{
    const CACHE_LIFETIME = 24 * 60;

    static public function selectDealerCategories()
    {
        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $result = \DB::connection('wordpress')->table('webikewp_2_terms')
            ->join('webikewp_2_term_taxonomy', 'webikewp_2_terms.term_id', '=', 'webikewp_2_term_taxonomy.term_id')
            ->get();
        $result = collect($result);
        if (count($result)) {
            \Cache::put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }
}