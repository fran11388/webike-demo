<?php

namespace Ecommerce\Service;

use Everglory\Models\Mission;
use Everglory\Models\Customer\Mission as CustomerMission;
use Everglory\Models\Customer\Reward as CustomerReward;
use Everglory\Models\Order\Reward as OrderReward;

class MissionService extends BaseService
{
    public static function mybikeMissionComplete($customer)
    {
        $today = date('Y-m-d');
        $target_date = date('2015-04-30');

        if( strtotime($today) >= strtotime($target_date) ){
            
            $mission = CustomerMission::
                select('reward_status')
                ->where('customer_id', $customer->id)
                ->where('mission_id', 1)
                ->first();

            if( !$mission ){

                CustomerMission::insert(
                    array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'customer_id' => $customer->id,
                        'mission_id' => 1
                    )
                );
            }
            if( !$mission || $mission->reward_status == 0 ){
                $point = $customer->points;
                if ( !$point ){
                    $point = new CustomerReward();
                    $point->customer_id = $customer->id;
                    $point->points_collected = 0;
                    $point->points_used = 0;
                    $point->points_waiting = 0;
                    $point->points_current = 0;
                    $point->points_lost = 0;
                }
                $rewards = new OrderReward();
                $rewards->customer_id = $customer->id;
                $rewards->description = '完成My Bike登錄活動';

                //give point
                $points = 100;
                $rewards->points_current = $points;
                $rewards->start_date = date("Y-m-d");
                $rewards->end_date = date("Y-m-d",strtotime("+1 year"));
                $rewards->save();

                $point->points_current += $points;
                $point->points_collected  += $points;
                $point->save();

                CustomerMission::
                    where('customer_id', $customer->id)
                    ->where('mission_id', 1)
                    ->update(
                        array(
                            'reward_status' => 1
                        )
                    );

                return true;
            }

        }
        return false;
    }

    public static function hasMissionComplete($mission_id, $customer_id)
    {
        $customer_mission = CustomerMission::select(\DB::raw('COUNT(0) AS count'))
            ->where('customer_id', $customer_id)
            ->where('mission_id', $mission_id)
            ->where('reward_status', 1)
            ->first();
        if($customer_mission->count){
            return true;
        }
        return false;
    }

    public static function motogpMissionComplete($speedway_name){
        $score_map = [1 => 25, 2 => 20, 3 => 16];
        for($key = 1;$key <= 3 ;$key++) {
            $mission = Mission::where('category', '2017MotoGP')->where('name', 'like', '%' . $speedway_name . '%')->where('note', 'like', '%' . $score_map[$key] . '點現金點數%')->first();
            if (!$mission) {
                $mission = new Mission;
                $mission->name = '2017MotoGP預測活動 - '.$speedway_name.' 預測冠軍';
                $mission->category = '2017MotoGP';
                $mission->note = '2017MotoGP預測活動 - '.$speedway_name.' 預測冠軍正確，贈予'.$score_map[$key].'點現金點數。';
                $mission->save();
            }
        }
    }

    public static function missionFinish($customer,$mission_id,$point)
    {
        $mission = Mission::where('id',$mission_id)->first();
        $customer_mission = \Everglory\Models\Customer\Mission::where('customer_id',$customer->id)->where('mission_id',$mission_id)->first();

        if(!$customer_mission){
            $customer_mission = new \Everglory\Models\Customer\Mission();
            $customer_mission->customer_id = $customer->id;
            $customer_mission->mission_id = $mission_id;
            $customer_mission->reward_status = 0;
        }


        if( $customer_mission->reward_status == 0 ){
            self::givePoint( $customer, $mission->name, $point);
        }

        $customer_mission->reward_status = 1;
        $customer_mission->save();
    }

    private function givePoint( $customer, $mission_name, $give )
    {
        //give point
        $point = $customer->points;
        if ( !$point ){
            $point = new Customer_Reward();
            $point->customer_id = $customer->id;
            $point->points_collected = 0;
            $point->points_used = 0;
            $point->points_waiting = 0;
            $point->points_current = 0;
            $point->points_lost = 0;
        }

        $rewards = new Order_Reward();
        $rewards->customer_id = $customer->id;
        $rewards->description = $mission_name;
        $points = $give;

        $rewards->points_current = $points;
        $rewards->start_date = date("Y-m-d");
        $rewards->end_date = date("Y-m-d",strtotime("+1 year"));
        $rewards->save();

        $point->points_current += $points;
        $point->points_collected  += $points;
        $point->save();
    }
}