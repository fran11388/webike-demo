<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Shopping\Shipping\EcanShipping;
use Ecommerce\Shopping\Shipping\HctShipping;
use Everglory\Constants\ProductType;
use Everglory\Models\Customer;
use Everglory\Models\Product;
use Everglory\Models\Sales\Cart;
use Everglory\Models\Coupon;
use Illuminate\Support\Collection;
use \Uuid;
use Everglory\Models\Stock\Additional\Area;
use stdClass;
use Everglory\Models\Stock;
use Ecommerce\Core\TableFactory\src\App\Excel\Service as TableFactory;

/**
 * Class CartService
 * @package Ecommerce\Service
 */
class CartService extends BaseService
{
    /**
     * Cart
     * @var $carts Collection|Cart[]
     **/
    private $carts = [];
    /**
     * @var $customer Customer
     **/
    private $customer;

    /**
     * Customer usage point for cart(Storage by session).
     * @var int
     */
    private $usage_points = 0;

    /**
     * Customer usage coupon for cart(Storage by session).
     * @var
     */
    private $usage_coupon;

    /**
     * When any items are update or delete, reset this flag to false to recalculate the information(e.g., shipping fee).
     * @var bool
     */
    private $is_check = false;

    /**
     * @var bool
     */
    private $has_point_discount = false;

    /**
     * CartService constructor.
     * @param bool $debug
     */
    public function __construct($debug = false )
    {
        $this->is_check = $debug;
        $this->customer = \Auth::user();
        if (!$this->is_check){
            $this->getItems();
            $this->refreshCount();

            while( !$this->checkItems() ){
                $this->carts = null;
                $this->getItems();
            }

            $this->setUsageCoupon(request()->session()->get('coupon-code', ''));
            $this->setUsagePoints(request()->session()->get('usage-points', 0));
        }
    }

    /**
     * Remove Item in database.
     * @param $protect_code
     */
    public function removeItem($protect_code ){
        if ($this->is_check)
            return;
        Cart::where('customer_id' , $this->customer->id)
            ->where('protect_code' , $protect_code)
            ->delete();

        $this->refreshCount();
    }

    /**
     * Update Item in database.
     *
     * @param $sku
     * @param $qty
     * @param null $cache
     */
    public function updateItem($sku , $qty , $cache = null){
        if ($this->is_check)
            return;
        if(is_array($sku)){
            $products = Product::whereIn('url_rewrite' , $sku)->get();
        }else if ( is_string($sku) || is_int($sku)){
            $products = Product::where('url_rewrite' , $sku)->get();
            $sku = [$sku];
            $qty = [$qty];
            $cache = [$cache];
        }else{
            $products = collect();
        }

        if (!$products->count()){
            return;
        }
        // modify exist item
        $items =  Cart::where('customer_id' , $this->customer->id) -> whereIn( 'product_id' , $products->pluck('id')->all() )->get();
        $products = $products->keyBy('id');
        foreach ($items as $key => $item){
            $product = $products->pull($item->product_id);
            if(!$product){
                abort(404);
            }
            $sku_key = array_keys($sku, $product->sku)[0];

            // cannot edit or add qty
            if($product->type != ProductType::GROUPBUY and
                $product->type != ProductType::ADDITIONAL){
                $item->quantity = (int)array_get($qty,$sku_key);
                $item->cache_id = null;
            }
            if($item->quantity > 0){
                $item->save();
            }else{
                $this->removeItem($item->protect_code);
            }
        }
        // add not exist item
        foreach ($products as $product) {
            $productAccessor = new ProductAccessor($product);
            $final_price = $productAccessor->getFinalPrice($this->customer);
            if(!$final_price || $final_price == 0 || ($final_price <= 1 and !in_array($product->type, [ProductType::UNREGISTERED, ProductType::ADDITIONAL]))){
                continue;
            }
            $sku_key = array_keys($sku, $product->sku)[0];
            $customer_id = $this->customer->id;
            $product_id = $product->id;
            $quantity = (int)array_get($qty, $sku_key);
            $protect_code = (string)Uuid::generate(1);
            $cache_id = array_get($cache, $sku_key) ? array_get($cache, $sku_key) : null;
            //if(ProductService::checkOnlyOne($product))
            if($product->type == ProductType::ADDITIONAL){
                $select_element = $cache_id ? [$customer_id,$product_id,$quantity,"'$protect_code'",$cache_id] : [$customer_id,$product_id,$quantity,"'$protect_code'"];
                $select_query = implode(',',$select_element);
                $insert_query = $cache_id ? "`customer_id`,`product_id`,`quantity`,`protect_code`,`cache_id`" : "`customer_id`,`product_id`,`quantity`,`protect_code`";
                \DB::insert(
                    "
                        INSERT INTO carts (".$insert_query.")
                        select ".$select_query."
                        from carts
                        where not exists
                        (
                            SELECT * FROM `carts` 
                            where product_id = ".$product_id."
                            and customer_id = ".$customer_id."
                        )
                        limit 1
                    "
                );
            }else{
                $item = new Cart();
                $item->customer_id = $customer_id;
                $item->product_id = $product_id;
                $item->quantity = $quantity;
                $item->protect_code = $protect_code;
                $item->cache_id = $cache_id;
                if ($item->quantity > 0) {
                    $item->save();
                }
            }
        }

        $this->refreshCount();
    }

    /**
     * Get shipping fee.
     * @return int|string
     */
    public function getFee(){
        //change logistics
        activeShippingFree();
        $ecanService = new HctShipping($this);
//        $ecanService = new EcanShipping($this);
        return $ecanService->getFee();
    }

    /**
     * Check items in cart is allow or not.
     * If disallow, using removeItem to delete and return false
     * @return boolean
     */
    public function checkItems(){

        $base_total_without_add = $this->getBaseTotalWithOutAdditional();
        $check_flg = true;
        foreach ( $this->carts as $entity ) {
            $product = $entity->product; //ProductAccessor
            if($product and $product->active == 1 and ($product->getFinalPrice($this->customer) > 1 or in_array($product->type, [ProductType::UNREGISTERED, ProductType::ADDITIONAL]))){
                if($product->type == 4){

                    if($entity->quantity <> 1){
                        Cart::where('customer_id' , $this->customer->id)->where('product_id', $product->id)->update(['quantity' => 1]);
                        $check_flg = false;
                    }

                    $additionalData = $product->getAdditionalData();
                    if(!$additionalData or ($additionalData->trigger_price > $base_total_without_add) or ($additionalData->stock_quantity) == 0){
                        $this->removeItem($entity->protect_code);
                        $check_flg = false;
                    }
                }
                /*
                 * change for outlet require stock.
                 */
                else if($product->type == 5){
                    $tw_stock = ProductService::getStockInfo($product);
                    if($tw_stock){
                        if(!$tw_stock->stock){
                            $this->removeItem($entity->protect_code);
                            $check_flg = false;
                        }else if($entity->quantity > $tw_stock->stock){
                            Cart::where('customer_id' , $this->customer->id)->where('product_id', $product->id)->update(['quantity' => 1]);
                            $check_flg = false;
                        }
                    }
                }
            }else{
                $this->removeItem($entity->protect_code);
                $check_flg = false;
            }
        }
        return $check_flg;
    }

    /**
     * Set cart items data.
     * @param $carts
     * @throws \Exception
     */
    public function setItems($carts){
        if (!$this->is_check)
            throw new \Exception('Only check mode can call setItems method');

        foreach ($carts as $key=>&$cart){
            $cart->product_id = $key;
            $product = Product::with(
                'prices' ,
                'fitModels' ,
                'manufacturer' ,
                'preorderItem')
                ->where('id' , $cart->product_id)->first();
            $product = new ProductAccessor($product);
            /** @var Object $cart */
            $cart->product = $product;
        }
        $this->refreshCount();
    }
    /**
     * Get cart items data
     * @return Cart[]|\Illuminate\Database\Eloquent\Collection|Collection|static[]
     */
    public function getItems(){
        if (!$this->carts && !$this->is_check){
            $carts = Cart::with(
                'product',
                'product.prices' ,
                'product.fitModels' ,
                'product.manufacturer' ,
                'product.preorderItem',
                'product.points',
                'product.categories',
                'product.images',
                'product.options',
                'product.motors')
                ->where('customer_id' , $this->customer->id)
                ->get();

            foreach ($carts as &$cart){
                if($cart->product){
                    $product = new ProductAccessor($cart->product);
                    /** @var Object $cart */
                    $cart->product = $product;
                }
            }
            $this->carts = $carts;
        }
        return $this->carts;
    }
    /**
     * Get cart items total price.
     * @return int
     */
    public function getBaseTotal(){
        $amount = 0;

        foreach ($this->carts as $cart){
            $product = $cart->product;
            $amount += $product->getCartPrice($this->customer)  * $cart->quantity;

        }

        return (int)$amount;
    }

    /**
     * Get cart items total price without additional items.
     * @return int
     */
    public function getBaseTotalWithOutAdditional(){
        $amount = 0;

        foreach ($this->carts as $cart){
            $product = $cart->product;
            if($product and $product->type != 4){
                $amount += $product->getCartPrice($this->customer)  * $cart->quantity;
            }
        }

        return (int)$amount;
    }

    /**
     * Get discount total price (BaseTotal - SubTotal)
     * @return int
     */
    public function getDiscountTotal(){
        $total = $this->getBaseTotal() - $this->getSubtotal();
        return (int)$total;
    }

    /**
     * Get cart final total price include point discount.
     * @param bool $include_discount
     * @return int
     */
    public function getSubtotal( $include_discount = true ,$exclude_point = false){
        $total = 0;

        foreach ($this->carts as $cart){
            /** @var ProductAccessor $product */
            $product = $cart->product;
            $total += $product->getCartPrice($this->customer)  * $cart->quantity;
        }

        if ($include_discount){
            if ($this->usage_coupon){
                if($this->usage_coupon->discount_type == 'shippingfee'){
                    // shippingfee coupon not discount other price
                }else{
                    $total -= $this->usage_coupon->discount;
                }
            }

            if ($this->usage_points && !$exclude_point){
                $total -= $this->usage_points;
            }
        }

        if ($total < 0 )
            $total = 0;

        return (int)$total;
    }


    /**
     * Get cart final receive point exclude point discount.
     * @return int
     */
    public function getReceivedPoints(){
        $amount = 0;
        foreach ($this->carts as $cart){

            /** @var ProductAccessor $product */
            $product = $cart->product;
            if($product->getCartPoint($this->customer) !== false){
                $amount += $product->getCartPoint($this->customer)  * $cart->quantity;
            }else{
                $this->has_point_discount = true;
            }
        }
        return (int)$amount;
    }

    /**
     * Check cart has point discount product or not.
     * @return int
     */
    public function hasPointDiscount(){
        return $this->has_point_discount;
    }

    /**
     * Check cart has preorder product or not.
     * @return bool
     */
    public function includePreorderItem(){
        foreach ($this->carts as $item){
            if( $item->product->isPreorder()){
                return true;
            }
        }
        return false;
    }

    /**
     * Check cart has groupbuy product or not.
     * @return bool
     */
    public function includeGroupbuyItem(){
        foreach ($this->carts as $item){
            if( $item->product->type == ProductType::GROUPBUY){
                return true;
            }
        }
        return false;
    }

    /**
     * Check cart has outlet product or not.
     * @return bool
     */
    public function includeOutletItem(){
        foreach ($this->carts as $item){
            if( $item->product->type == ProductType::OUTLET){
                return true;
            }
        }
        return false;
    }

    /**
     * Get usage point in cart.
     * @return int
     */
    public function getUsagePoints(){
        return (int) $this->usage_points;
    }

    /**
     * Set usage point in cart
     * @param $points
     */
    public function setUsagePoints($points ){
        $points = (int) $points;
        $maxAllowPoints = $this->getMaxAllowPoints();
        if ($points > $maxAllowPoints)
            $points = $maxAllowPoints;
        if($this->customer->points and $points > $this->customer->points->points_current)
            $points = $this->customer->points->points_current;
        if ($points < 0 )
            $points = 0;
        $this->usage_points = $points;
        if ($this->is_check)
            return;
        request()->session()->put('usage-points' , $points);
    }

    /**
     * Get usage coupon in cart.
     * @return mixed
     */
    public function getUsageCoupon(){
        return $this->usage_coupon;
    }

    /**
     * Set usage coupon in cart.
     * @param $code
     */
    public function setUsageCoupon($code){
        $coupon = null;
        if ($code){
            $coupon = Coupon::whereNull('order_id')
                ->where('customer_id', $this->customer->id)
                ->where('expired_at', '>', date('Y-m-d'))
                ->where('uuid' , $code )
                ->first();
        }
        if ($coupon){
            $this->usage_coupon = $coupon;
            if ($this->is_check)
                return;
            request()->session()->put('coupon-code' , $code);
        } else {
            if ($this->is_check)
                return;
            request()->session()->forget ('coupon-code');
        }
    }


    /**
     * Get maximum point allow to use.
     * 取得最大可用的點數
     * @return int
     */
    public function getMaxAllowPoints()
    {
        $total = $this->getSubtotal(true,true);
        return floor($total * 0.7);
    }


    /**
     * Get additional products list.
     * @return Collection
     */
    public function getAdditionalProducts()
    {
        $collection = collect();
        $grand_total = $this->getBaseTotalWithOutAdditional();
        $areas = Area::with(['items','items.product','items.reflect', 'items.reflect.images', 'items.reflect.manufacturer'])
            ->orderby('trigger_price','DESC')->get();
        foreach ($areas as $area){
            $sale_flg = true;
            if($area->trigger_price > $grand_total){
                $sale_flg = false;
            }
            $entity = new stdClass;
            $entity->title = $area->name . ( $sale_flg ? '' : '(尚未達成條件)' );
            $entity->sale_flg = $sale_flg;
            $entity->items = collect();
            $items = $area->items;
            foreach ($items as $item){
                $object = new stdClass;
                $product = $item->product;
                if(!is_null($product)) {
                    $object->disabled_flg = false;
                    $object->selected_flg = false;
                    $reflect = new ProductAccessor($item->reflect);
                    $match = null;
                    if ($sale_flg) {
                        $match = $this->carts->first(function ($entity, $key) use ($product) {
                            return $entity->product_id == $product->id;
                        });
                        if ($match) {
                            $object->selected_flg = true;
                            $object->disabled_flg = true;
                        }
                    }else{
                        $object->disabled_flg = true;
                    }

                    if($reflect){
                        $stockInfo = ProductService::getStockInfo($reflect);
                        if (!$stockInfo or $stockInfo->sold_out or !$stockInfo->stock) {
                            $object->disabled_flg = true;
                        }
                    }
                    $object->icon_flg = $item->icon_flg;
                    $object->url = route('product-detail', $reflect->url_rewrite);
                    $object->manufacturer_name = $reflect->getManufacturerName();
                    $object->old_price = $reflect->price;
                    $object->thumbnail = $reflect->getThumbnail();
                    $object->product_name = $product->name;
                    $object->url_rewrite = $product->url_rewrite;
                    $object->price = $product->price;
                    $object->point = $product->point;
                    $object->discount = number_format(round((100 - $item->discount) / 10, 2), 1);
                    $entity->items[] = $object;
                }
            }

            $collection->push($entity);
        }
        return $collection;
    }

    /**
     * Get total item data number.
     * @return int
     */
    public function getCount()
    {
        return count($this->carts);
    }

    /**
     * Refresh total item data number.
     */
    public function refreshCount()
    {
        session(['cart_count' => $this->getCount()]);
    }

    /**
     * Output file with cart items data.
     */
    public function getQuotes()
    {
        $info = new \StdClass;
        $accountService = new AccountService();
        $account_info = $accountService->getDefaultAddress();
        $customer =  \Auth::user();
        $doc_type = ' - 報價單';
        $filename = $customer->realname;
        if($account_info->company){
            $filename = $account_info->company;
        }
        $info->filename = $filename.$doc_type.date('(Y-m-d)');
        $info->title = $filename . $doc_type;
        $info->creator = $customer->realname;
        $info->company = $account_info->company;
        $service = new TableFactory($info);
        $service->setAllColsWidth(5);
        $service->setAllColsHeight(20);
        $data = [
            'positions' => [
                ['col' => 'C', 'row' => '3'],
                ['col' => 'P', 'row' => '6']
            ],
            'value' => $filename . $doc_type,
            'setborder' => 'true'
        ];
        $service->setProgram('addCenterBox', $data);
        $name = '承辦人：'.$account_info->firstname.$account_info->lastname;
        $phone = '電話：'.$account_info->mobile;
        $address = '地址：'.$account_info->address;
        $date = '報價日期：'.date("Y-m-d");
        $customer_info = [$name,$phone,$address,$date];
        $num = 3;
        foreach($customer_info as $key => $info){
            $customer = [
                'positions' => [
                    ['col' => 'R', 'row' => $num ],
                    ['col' => 'AA', 'row' => $num]
                ],
                'value' => $info
            ];
            $service->setProgram('addBox', $customer);
            $num++;
        }

        $column_config = [
            ['from'=>'B', 'to'=>'D'],
            ['from'=>'E', 'to'=>'S'],
            ['from'=>'T', 'to'=>'V'],
            ['from'=>'W', 'to'=>'Y'],
            ['from'=>'Z', 'to'=>'AB']
        ];

        $collection = collect();

        $titles = ['項次','品名','數量','單價','小計'];
        $collection->push($titles);
        $carts = $this->getItems();
        foreach($carts as $key => $cart){
            $current_customer = \Auth::user();
            $product = $cart->product;
            $row = [
                $key+1,
                $product->full_name,
                $cart->quantity,
                $product->getCartPrice($current_customer),
                $product->getCartPrice($current_customer) * $cart->quantity,
            ];
            $collection->push($row);
        }
        $num = 8;
        foreach ($collection as $key => $data){
            foreach ( $column_config as $column_number => $config ) {
                $cartdata = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $num ],
                        ['col' => $config['to'], 'row' => $num]
                    ],
                    'value' => $data[$column_number],
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $cartdata);    
            }
            $num++;
        }

        $rowInfo = $service->getPosition();
        $maxRow = $rowInfo['maxRow'];
        $remark = [
            'positions' => [
                ['col' => 'B', 'row' => $maxRow + 1 ],
                ['col' => 'V', 'row' => $maxRow + 6]
            ],
            'value' => '備註:',
            'setborder' => 'true'
        ];
        $service->setProgram('addBox', $remark);

        $column_config_other = [
            ['from'=>'W', 'to'=>'Y'],
            ['from'=>'Z', 'to'=>'AB']
        ];

        $other_title = ['零件金額','安裝工資','費用總計'];
        $totalBegin = $maxRow+1;
        $totalEnd = $maxRow+3;
        $other_info = ['=SUM(Z9:Z'."$maxRow".')','','=SUM(Z'."$totalBegin".':Z'."$totalEnd".')'];
        foreach($other_title as $key => $info){
            foreach($column_config_other as $columnkey => $config){
                if($columnkey == 0){
                    $value = $info;
                }else{
                    $value = $other_info[$key];
                }
                $title = [
                    'positions' => [
                        ['col' => $config['from'], 'row' => $maxRow+1],
                        ['col' => $config['to'], 'row' => $maxRow+2]
                    ],
                    'value' => $value,
                    'setborder' => 'true'
                ];

                $service->setProgram('addCenterBox', $title);
            }
            $maxRow += 2;
        }

        $rowInfo = $service->getPosition();
        $end = 'AB'.$rowInfo['maxRow']; 
        $range = 'B2:'.$end;
        $outline = [
            'positions' => [
                        ['col' => 'B2', 'row' => 2],
                        ['col' => 'B2', 'row' => 2]
                    ],
            'range' => $range
        ];
        $service->setProgram('setOutLine', $outline);
        
        $service->download();
    }

    public function getItemDeliveryInfo($carts)
    {
        $cart_products = [];
        $product = null;
        $result = [];
        $product_stocks = [];

        if(count($carts)){
            foreach($carts as $cart){
                if($cart->product){
                    $product = $cart->product;
                    $cart_products[$product->type][$product->sku] = $product;
                }
            }

            if($cart_products){
                foreach($cart_products as $type => $products){
                    $productService = new ProductService;
                    $product_stocks[$type] = $productService->getDeliveryInfoByType($type,$products);
                }
            }

            if($product_stocks){
                foreach($product_stocks as $products){
                    foreach($products as $sku => $product){
                        $result[$sku]  = $product;
                    }
                }
            }
        }

        return $result;
    }
}