<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2017/9/1
 * Time: 上午 10:12
 */

namespace Ecommerce\Service;

use Ecommerce\Core\Solr\Param\CommonParam;
use Ecommerce\Core\Solr\Param\FilterQueryParam;
use Ecommerce\Core\Solr\Searcher;
use Ecommerce\Service\Search\ConvertService;
use Everglory\Models\Product;
use Illuminate\Support\Collection;

class RecommendToBuyService extends BaseService
{

    /**
     * @param Product[] $products
     * @return array
     */
    public static function getRecommend( $products )
    {
        $recommendProducts = self::query($products);
        $recommendProducts = self::filter($recommendProducts, $products);
        return $recommendProducts;
    }

    /**
     * @param Collection $products
     * @return array
     */
    private static function query($products)
    {
        $searcher = new Searcher('solr/webike_order' ,  env('SOLR_ORDER_PORT' , 8080));
        $common = new CommonParam();
        $searcher->setCommon($common);

        $fq = [];
        $product_fq = new FilterQueryParam();
        $product_fq->setField('id');
        $product_fq->setTag('product');
        if( get_class($products) == 'Ecommerce\Accessor\ProductAccessor'){
            $product_fq->setQuery('(' . $products->id . ')');
        }else{
            $product_fq->setQuery('(' . $products->implode('id', ' OR ') . ')');
        }
        $fq[] = $product_fq;

        $searcher->setFilterQueries($fq);
        $search_response  = $searcher->query();

        $product_ids = [];
        $products = $search_response->getQueryResponse()->getDocs();
        if(count($products)){
            $products->pluck('id')->each(function ($item, $key) use( &$product_ids) {
                if (is_array($item))
                    $product_ids = array_merge($item,$product_ids);
                else
                    $product_ids[] =$item;
            });
        }
        $product_ids = array_unique($product_ids);
        $productId_count = [];
        foreach ($product_ids as $product_id){
            if (array_key_exists( $product_id , $productId_count))
                $productId_count[$product_id] += 1;
            else
                $productId_count[$product_id] = 1;
        }

        return $productId_count;
    }


    /**
     * @param $productId_count
     * @return array
     */
    private static function filter($recommendProducts, $products)
    {
        $count = 0;
        $productIds = [];
        foreach ($recommendProducts as $key => $value){
            if( get_class($products) == 'Ecommerce\Accessor\ProductAccessor'){
                if($key != $products->id){
                    $productIds[] = $key;
                }
            }else{
                if(!in_array($key, $products->pluck('id')->toArray())){
                    $productIds[] = $key;
                }
            }
            if($count >= 30){
                break;
            }
            $count++;
        }
        return $productIds;
    }
}