<?php

namespace Ecommerce\Service;

class BaseService
{
    const CACHE_LIFETIME = 10 * 60;

    /**
     * @param bool|string $args
     * @return string
     */
    static protected function getCacheKey($args = true )
    {
        $trace = debug_backtrace();
        $class = $trace[1]['class'];
        $function = $trace[1]['function'];
        $key = $class . '@'.  $function;
        if ($args === true ){
            $args = serialize($trace[1]['args']);
            $key .= '?' . base64_encode($args);
        } else {
            $key .= '?' . $args;
        }
        return $key;
    }
}