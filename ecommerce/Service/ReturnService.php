<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Core\Auth\Hasher;
use Everglory\Models\Customer;
use Everglory\Models\Customer\Address;
use Everglory\Models\Order;
use Everglory\Constants\Coupon;
use Ecommerce\Service\CouponService;
use Ecommerce\Core\Wmail;
use Everglory\Constants\CustomerRole;
use Everglory\Models\Customer\Facebook;
use Everglory\Models\Rex;
use Ecommerce\Repository\ReturnRepository;
use Ecommerce\Service\ContactService;
use \Everglory\Constants\ProductType;
use Ecommerce\Core\Image;

class ReturnService extends BaseService
{
    private $customer;

    public function __construct( $debug = false )
    {
        $this->customer = \Auth::user();
    }

    public function create($request,$increment_id)
    {

//        $rules = $request->input('rule');
        $order = Order::with(['items','items.purchases'])->where('increment_id',$increment_id)->firstOrFail();
        $rex = new Rex;
        $rex->type = $request->input('type');
        $rex->customer_id = $order->customer_id;

        $address = new Address();
        $address->customer_id = $rex->customer_id;
        $address->is_default = '0';
        $address->zipcode = $request->input('address.zipcode');
        $address->county = $request->input('address.county');
        $address->district = $request->input('address.district');
        $address->address = $request->input('address.address');
        $address->firstname = $request->input('address.firstname');
        $address->phone = $request->input('address.phone');
        $address->mobile = $request->input('address.mobile');
        $address->shipping_time = $request->input('address.shipping_time');
        $address->shipping_comment = $request->input('address.shipping_comment');

        $address->save();


        $rex->address_id = $address->id;
        $rex->status_id = 1;
        $rex->shipping_method = $order->shipping_method;
        $rex->payment_method = $order->payment_method;
        $rex->shop_id = $order->shop_id;
        $rex->comment = $request->input('comment');
        $rex->type = $request->input('type');
        $rex->account_bank = $request->input('account_bank');
        $rex->account_name = $request->input('account_name');
        $rex->account_number = $request->input('account_number');
        $rex->account_branch = $request->input('account_branch');

        $rex->parent_id = $order->id;
        $rex->increment_id = 'WTR' . explode('WT',$increment_id)[1];
        $rex->save();
        //item
        $items = $order->items;
        $points_current = 0;
        $image = new Image();
        foreach ( $request->input('items') as $item_id ) {
            $data = [];
            $item = $items->filter(function ($item) use($item_id) {
                return $item->id == $item_id;
            })->first();
            if($item){
                $data['item_id'] = $item->id;
                $data['status_id'] = 1;
                $purchase = $item->purchases->first();
                if($purchase){
                    $data['purchase_id'] = $purchase->id;
                }
                $data['product_id'] = $item->product_id;
                $data['main_product_id'] = $item->main_product_id;
                $data['manufacturer_id'] = $item->manufacturer_id;
                $data['price'] = $item->price;
                $data['point'] = $item->point;
                $points_current += $item->point;
                $data['sku'] = $item->sku;
                $data['model_number'] = $item->model_number;
                $data['product_type'] = $item->product_type;
                $data['quantity'] = $request->input('quantity.'.$item_id);

                $data['reason_id'] = $request->input('reason.'.$item_id);

                $data['url'] = $request->input('url.'.$item_id);
                $data['spec'] = $request->input('spec.'.$item_id);
                $data['note'] = $request->input('note.'.$item_id);
                if($data['reason_id'] == 4){
                    $data['situation_id'] = 1;
                }else{
                    $data['situation_id'] = $request->input('situation.'.$item_id);
                }

                $data['image_url'] = $request->input('image.'.$item_id);
                if($data['image_url']){
                    $image->rex( public_path().'/assets/photos/return/'.$data['image_url'],null,$data['image_url']);
                }
                
                $rex_items = $rex->items()->create($data);
                $details = $item->details;
                foreach($details as $detail){
                    $detail_data = $detail->toArray();
                    unset($detail_data['item_id']);
                    $rex_items->details()->create($detail_data);
                }

                $rex->total_qty++;
                $rex->grand_total += $data['price'] * $data['quantity'];
            }else{
                abort(500,'Item Not found!');
            }
        }

        if($rex->type == 'return_all'){
            $rex->rewardpoints_amount = $order->rewardpoints_amount;
            $rex->coupon_amount = $order->coupon_amount;
            $rex->shipping_amount = $order->shipping_amount;
            $rex->payment_amount = $order->payment_amount;
            $rex->points_current = $order->points_current;
            $rex->coupon_uuid = $order->coupon_uuid;
            $rex->coupon_code = $order->coupon_code;
            $rex->coupon_name = $order->coupon_name;
            $rex->coupon_code = $order->coupon_code;
            $rex->subtotal = $order->subtotal;
        }else{
            $rex->rewardpoints_amount = 0;
            $rex->coupon_amount = 0;
            $rex->shipping_amount = 0;
            $rex->payment_amount = 0;
            $rex->points_current = 0;
            $rex->subtotal = $rex->grand_total;
            if($rex->type == 'return'){
                $rex->points_current = $points_current;
            }
        }
        $rex->save();
        $rex->histories()->create([
            'status_id' => $rex->status_id,
            'comment' => '前台客戶提交申請需求',
        ]);

        //Ticket Send
        // customer comment : $rex->comment

        if($rex->type == 'exchange'){
            request()->request->add(['category_id' => 112]);
        }else{
            request()->request->add(['category_id' => 116]);
        }
        $content = $rex->comment;

        if(!$content){
            $content = '訂單編號：'.$order->increment_id.' 申請退換貨';
        }

        request()->request->add(['content' => $content]);
        request()->request->add(['orders' => $order->increment_id]);
        request()->request->add(['rexs' => $rex->increment_id]);
        $contactService = new ContactService;
        $contactService->insertTicket();
        return $rex;
    }

    public function getItemDetail($item)
    {
        $details = $item->details->keyBy('attribute');
        $data = new \stdClass();
        foreach ($details as $key => $detail) {
            $data->$key = $detail->value;
        }
        return $data;
    }


    public function getStatusProcess($rex)
    {
        $status_service = new StatusService;
        $process = $status_service->getProcess($rex);
        return $process;
    }


    public function getOrderReturnType(Order $order)
    {
        // get product type count situation in order
        $product_types_class = new \ReflectionClass('\Everglory\Constants\ProductType');
        $types = array_flip($product_types_class->getConstants());
        $group_items = $order->items->groupBy('product_type');
        foreach ( $types as $type => &$value ) {
            $value = count($group_items->get($type));
        }

        // set return types
       /* $normal_count = $types[ProductType::NORMAL] + $types[ProductType::UNREGISTERED] + $types[ProductType::GROUPBUY] + $types[ProductType::OUTLET] + $types[ProductType::GENUINEPARTS];

        $total_count = $normal_count + $types[ProductType::GENUINEPARTS];
        $genuinepart_limit = true;
        if(in_array(\Auth::user()->role_id,[\Everglory\Constants\CustomerRole::WHOLESALE,\Everglory\Constants\CustomerRole::STAFF]{
            $genuinepart_limit = false;
        }
        if ($normal_count > 0) {
            $return_types = ReturnRepository::getType(true);
            if ($types[ProductType::GENUINEPARTS] and $genuinepart_limit) {
                $return_types->pull(2);
            }
            if ($normal_count == 1 and $total_count == 1 and $order->items->sum('quantity') == 1) {
                $return_types->pull(1);
            }
        } else {
            $return_types = collect();
        }
        return $return_types;*/

        //以下安心購服務提案開放一般會員可以退正廠零件

        $normal_count = $types[ProductType::NORMAL] + $types[ProductType::UNREGISTERED] + $types[ProductType::GROUPBUY] + $types[ProductType::OUTLET] + $types[ProductType::GENUINEPARTS];

        $return_types = ReturnRepository::getType(true);

        if ($normal_count == 1 and $order->items->sum('quantity') == 1) {
            $return_types->pull(1);
        }
        
        return $return_types;
    }
}