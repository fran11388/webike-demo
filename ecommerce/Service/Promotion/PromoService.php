<?php

namespace Ecommerce\Service\Promotion;


class PromoService
{
    public $view_component;

//    public function __construct(PromoData $data)
//    {
//        if(!$data->checkAttribute($data)){
//            throw new \Exception('promo_type is required');
//        }
//    }

//    public function verify($data)
//    {
//        $promo_type = $data->promo_type;
//        $class_name = '\BigPromotion\\'.$promo_type.'Service';
//        if(class_exists($class_name)){
//            return new $class_name;
//        }
//    }

    public function getSeasonPromo($promo_name)
    {
        $class_name = __NAMESPACE__.'\\BigPromotion\\' . $promo_name;
        if(class_exists($class_name)){
            return new $class_name();
        }
    }

    public function getFatherDayPromo($year)
    {
        $class_name = __NAMESPACE__.'\\FatherDay\\' . 'FatherDay'.$year;
        if(class_exists($class_name)){
            return new $class_name();
        }
//        $class_name = '\BigPromotion\\' . 'FatherDay' . $year;
//        if(class_exists($class_name)){
//            return new $class_name;
//        }
    }


}