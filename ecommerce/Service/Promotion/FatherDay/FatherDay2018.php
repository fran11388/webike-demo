<?php

namespace Ecommerce\Service\Promotion\FatherDay;

use Ecommerce\Service\Promotion\Base\PromoBase;
use Everglory\Models\Product;
use App\Components\View\BaseComponent;
use Everglory\Models\Mptt;
use Everglory\Models\Motor;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\SearchService;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;

class FatherDay2018 extends PromoBase
{
    protected $request;
    protected $category;

    public function __construct()
    {

    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    protected function setSeo()
    {
        $this->breadcrumbs = ['88節免運費活動說明'];
        $this->seo_title = '88節免運費活動!!活動日期08/03(四)~08/07(一)';
        $this->seo_description = '「Webike」祝各位帥氣的老爸~佳節愉快！在8月3日~8月7日一連四天，全館購物不限金額免運費，在您的”購物車”折價券欄位中，將會有兩張$100元的折價券，使用期限至8月7日止。';
//        $this->seo_keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';

        $this->buildSeo();
    }

    public function setRelParameter($parameter)
    {
        return $this->view_component->rel_parameter = 'rel=2018-03-2018Spring';
    }

    public function getTop10Product()
    {
        $products = Product::take(10);
        $this->seo_title = implode('_', $products->pluck('name'));
    }



//    private function verifySeo(){
//        foreach($this->seo_requires as $seo_require){
//            if(!isset($this->view_component->$seo_require) || !$this->view_component->$seo_require){
//                throw new \Exception($seo_require.' is required');
//            }
//        }
//    }

    public function promotion()
    {
        $this->setSeo();
        $this->setType();
        $this->setRecommends();
        $this->setDate();
        $this->setRel_parameter();
        $this->setCollections(12);

//        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->render();

        return $this->view_component;
    }

    protected function setType()
    {
        $type = '2018-spring';
        $this->view_component->type = $type;
    }

    protected function setRecommends()
    {
        $type = '2018-spring';
        $this->view_component->recommends = \Ecommerce\Repository\BigPromotionRepository::getProductRecommends($type);
    }

    protected function setDate()
    {
        $this->view_component->date = date("Y-m-d");
    }

    protected function setRel_parameter()
    {
        $this->view_component->rel_parameter = 'rel=2018-04-2018Spring';
    }

    protected function setCollections($num)
    {
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt($num);
    }

    public function promotionCategory()
    {
        $this->setCategories();
        $this->setDiscounts();
        $this->setRel_parameter();
        $this->setCollections(6);
        $this->setSeo();

        $this->view_component->render();
        return $this->view_component;
    }

    protected function setCategories()
    {
        $promotionName = '2018-spring';
        $this->view_component->categories = \Ecommerce\Repository\BigPromotionRepository::getCategories($promotionName, $this->category)->groupby('ca_rewrite');
    }

    protected function setDiscounts()
    {
        $promotionName = '2018-spring';
        $this->view_component->discounts = \Ecommerce\Service\PromotionService::getCategoryBrandDiscounts($promotionName);
    }

    public function promotionDomestic()
    {
        $this->setRel_parameter();
        $this->setCollections(6);
        $this->setSeo();
        $this->view_component->render();

        return $this->view_component;
    }

    public function getFatherDayProduct($category_id)
    {
        $productSkus = Product::select('products.sku')
        ->join('category_product' ,'products.id','=','category_product.product_id')
        ->where('category_product.category_id',$category_id)
        ->where('products.type','=',0)
        ->where('products.active','=',1)
        ->where('products.is_main','=',1)
        ->inRandomOrder()
        ->limit(21)
        ->get();

        foreach ($productSkus as  $productSku) {
            $products[] = ProductRepository::findDetail($productSku->sku, false, 'url_rewrite', ['prices', 'manufacturer', 'images', 'points']);
        }
        foreach ($products as $key => $product) {
            $productAccessor = new ProductAccessor($product);
            $products[$key]->final_price = $productAccessor->getFinalPrice($this->view_component->current_customer);
            $products[$key]->final_point = $productAccessor->getFinalPoint($this->view_component->current_customer);
        }

        return $products;
    }

}