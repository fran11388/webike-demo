<?php

namespace Ecommerce\Service\Promotion\BigPromotion;

use Ecommerce\Service\Promotion\Base\PromoBase;
use Everglory\Models\Product;
use App\Components\View\BaseComponent;
use Everglory\Models\Mptt;
use Everglory\Models\Motor;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\SearchService;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;
use Everglory\Models\Order\Item;
use Everglory\Models\Manufacturer;
use Everglory\Models\Accounting\Received\Queues;
class Spring2019 extends PromoBase
{

    protected $request;
    protected $category;

    public function __construct()
    {

    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    protected function setSeo()
    {
        $this->breadcrumbs = ['2019', 'spring'];
        $this->seo_title = 'Webike 2019 spring sale';
        $this->seo_description = 'Webike SPRING SALE 春季優惠！春夏騎士用品、改裝零件加倍回饋，各大品牌精選現折，正廠零件全面95折，完成每日任務再送你現金點數！';
//        $this->seo_keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';

        $this->buildSeo();
    }

    public function setRelParameter($parameter)
    {
        return $this->view_component->rel_parameter = 'rel=2019-04-2019spring';
    }

    public function getTop10Product()
    {
        $products = Product::take(10);
        $this->seo_title = implode('_', $products->pluck('name'));
    }



//    private function verifySeo(){
//        foreach($this->seo_requires as $seo_require){
//            if(!isset($this->view_component->$seo_require) || !$this->view_component->$seo_require){
//                throw new \Exception($seo_require.' is required');
//            }
//        }
//    }

    public function promotion()
    {
        $this->setSeo();
        $this->setType();
        $this->setDate();
        $this->setRel_parameter();
        $this->setCollections(4);
        $this->setPreviewBrand();
        $this->view_component->week_key = $this->getDayWeek();
//        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->render();

        return $this->view_component;
    }


    protected function setRankingCategories()
    {
        $this->view_component->ranking_categories = [
            '1000' => ['title' => '改裝零件', 'sub' => ''],
            '3000' => ['title' => '騎士用品', 'sub' => ''],
            '4000' => ['title' => '保養耗材', 'sub' => ''],
            '8000' => ['title' => '機車工具', 'sub' => ''],
        ];

        $categories = Mptt::where('depth', 2)->where('_active', 1)->whereNotIn('url_path', ['3000-1122', '3000-7994', '4000-4051', '8000-4064', '8000-4067', '8000-4070', '8000-7940', '8000-8850', '8000-8046', '8000-8067', '8000-8300', '8000-8083', '8000-8900', '8000-8109', '8000-8846'])->orderBy('sort')->get()->groupBy(function ($category, $key) {
            return substr($category->url_path, 0, 4);
        });

        foreach ($categories as $root => $category) {
            $this->view_component->ranking_categories[$root]['sub'] = $category;
        }

        $motors = Motor::with('manufacturer')->whereIn('url_rewrite', ['6371', 't5099', '6320', '311', '13468', '215', '13691', '6536', '6443', '672', '6557', '952', '13697', '6339'])->orderBy(\DB::Raw("FIELD(url_rewrite,'6371','t5099','6320','311','13468','215','13691','6536','6443','672','6557','952','13697','6339')"))->select(\DB::Raw('*,url_rewrite as url_path'))->take(14)->get();
        $this->view_component->ranking_categories['motor'] = ['title' => '車型', 'sub' => $motors];
    }

    protected function setType()
    {
        $type = 'Spring2019';
        $this->view_component->type = $type;
    }

    protected function setRecommends()
    {
        $type = 'Spring2019';
        $this->view_component->recommends = \Ecommerce\Repository\BigPromotionRepository::getProductRecommends($type);
    }

    protected function setProducts()
    {
        $convertService = app()->make(ConvertService::class);
        $search_service = app()->make(SearchService::class);
        $ranking_response_sales = $search_service->selectRanking($this->request);
        $products = $convertService->convertQueryResponseToProducts($ranking_response_sales);

        $this->view_component->products = $products;
    }

    protected function setDate()
    {
        $this->view_component->date = date("Y-m-d");
    }

    protected function setRel_parameter()
    {
        $this->view_component->rel_parameter = 'rel=2019-04-2019spring';
    }

    protected function setCollections($num)
    {
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt($num);
    }

    public function promotionPreview(){
        $this->setRel_parameter();
        $this->setCollections(4);
        $this->setSeo();
        $this->setPreviewBrand();
        $this->view_component->week_key = $this->getDayWeek();

        $this->view_component->render();
        return $this->view_component;   
    }
    public function promotionBrand()
    {
        $this->setRel_parameter();
        $this->setCollections(4);
        $this->setSeo();

        $this->view_component->render();
        return $this->view_component;
    }

    protected function setCategories()
    {
        $promotionName = 'Spring2019';
        $this->view_component->categories = \Ecommerce\Repository\BigPromotionRepository::getCategories($promotionName, $this->category)->groupby('ca_rewrite');
    }

    protected function setDiscounts()
    {
        $promotionName = 'Spring2019';
        $this->view_component->discounts = \Ecommerce\Service\PromotionService::getCategoryBrandDiscounts($promotionName);
    }

    public function promotionDomestic()
    {
        $this->setRel_parameter();
        $this->setCollections(6);
        $this->setSeo();
        $this->view_component->render();

        return $this->view_component;
    }

    public function setSaleBrand()
    {

        $week = $this->getDayWeek();

        $sale_brands = $this->getSaleBrand($week);

        return $this->view_component->sale_brands = $sale_brands;
            
    }

    protected function getSaleBrand($week){
        // \Cache::tags('big-promotion-sale_brands'. $week)->flush();
        $sale_brands = \Cache::tags(['big-promotion-sale_brands'. $week])->remember('big-promotion-sale_brands'. $week, 86400, function () use ($week) {

            $brands = [
                [   '300' => ['name' => 'HONDA','image' => '//img.webike.net/sys_images/brand/brand_300.gif'],
                    '354' => ['name' => 'KIJIMA','image' => '//img.webike.net/sys_images/brand/brand_354.gif'],
                    '529' => ['name' => 'OHLINS','image' => '//img.webike.net/sys_images/brand/brand_529.gif'],
                    '29' => ['name' => 'AGRAS','image' => '//img.webike.net/sys_images/brand/brand_29.gif'],
                    '203' => ['name' => 'EASYRIDERS','image' => '//img.webike.net/sys_images/brand/brand_203.gif'],
                    '2358' => ['name' => 'US YAMAHA','image' => '//img.webike.net/sys_images/brand/brand_2358.gif'],
                    '742' => ['name' => 'SW-MOTECH','image' => '//img.webike.net/sys_images/brand/brand_742.gif'],
                    '152' => ['name' => 'CLEVERWOLF','image' => '//img.webike.net/sys_images/brand/brand_152.gif'],
                    '361' => ['name' => 'KN企劃','image' => '//img.webike.net/sys_images/brand/brand_361.gif'],
                    '1519' => ['name' => 'domino','image' => '//img.webike.net/sys_images/brand/brand_1519.gif'],
                    '666' => ['name' => 'SHOEI','image' => '//img.webike.net/sys_images/brand/brand_666.gif'],
                    '847' => ['name' => 'YAMAHA','image' => '//img.webike.net/sys_images/brand/brand_847.gif'],
                    '179' => ['name' => 'DEGNER','image' => '//img.webike.net/sys_images/brand/brand_179.gif'],
                    '298' => ['name' => 'HJC','image' => '//img.webike.net/sys_images/brand/brand_298.gif'],
                    '40' => ['name' => 'alpinestars','image' => '//img.webike.net/sys_images/brand/brand_40.gif']],
                [   '704' => ['name' => 'SP武川','image' => '//img.webike.net/sys_images/brand/brand_704.gif'],
                    '847' => ['name' => 'YAMAHA','image' => '//img.webike.net/sys_images/brand/brand_847.gif'],
                    '345' => ['name' => 'KAWASAKI','image' => '//img.webike.tw/brand/brand_345.gif'],
                    '10' => ['name' => 'A-TECH','image' => '//img.webike.net/sys_images/brand/brand_10.gif'],
                    '1210' => ['name' => 'Wunderlich','image' => '//img.webike.net/sys_images/brand/brand_1210.gif'],
                    '825' => ['name' => 'WirusWin','image' => '//img.webike.net/sys_images/brand/brand_825.gif'],
                    '1365' => ['name' => 'P&A International','image' => '//img.webike.net/sys_images/brand/brand_1365.gif'],
                    '74' => ['name' => 'BAGSTER','image' => '//img.webike.net/sys_images/brand/brand_74.gif'],
                    '1559' => ['name' => 'ai-net','image' => '//img.webike.net/sys_images/brand/brand_1559.gif'],
                    '85' => ['name' => 'BEET','image' => '//img.webike.net/sys_images/brand/brand_85.gif'],
                    '364' => ['name' => 'KOMINE','image' => '//img.webike.net/sys_images/brand/brand_364.gif'],
                    '300' => ['name' => 'HONDA','image' => '//img.webike.net/sys_images/brand/brand_300.gif'],
                    '983' => ['name' => 'TANAX motofizz','image' => '//img.webike.net/sys_images/brand/brand_983.gif'],
                    '289' => ['name' => 'HenlyBegins','image' => '//img.webike.net/sys_images/brand/brand_289.gif'],
                    '267' => ['name' => 'GOLDWIN','image' => '//img.webike.net/sys_images/brand/brand_267.gif']],
                [   '854' => ['name' => 'YOSHIMURA', 'image' => '//img.webike.net/sys_images/brand/brand_854.gif'],
                    '357' => ['name' => 'KITACO', 'image' => '//img.webike.net/sys_images/brand/brand_357.gif'],
                    '2095' => ['name' => 'KTM POWER PARTS', 'image' => '//img.webike.net/sys_images/brand/brand_2095.gif'],
                    '2097' => ['name' => 'ENDURANCE', 'image' => '//img.webike.net/sys_images/brand/brand_2097.gif'],
                    '2136' => ['name' => 'COBRA', 'image' => '//img.webike.net/sys_images/brand/brand_2136.gif'],
                    '180' => ['name' => 'DELTA', 'image' => '//img.webike.net/sys_images/brand/brand_180.gif'],
                    '703' => ['name' => 'SP忠男', 'image' => '//img.webike.net/sys_images/brand/brand_703.gif'],
                    '828' => ['name' => 'WM', 'image' => '//img.webike.net/sys_images/brand/brand_828.gif'],
                    '1553' => ['name' => 'BPC', 'image' => '//img.webike.tw/brand/brand_1553.gif'],
                    '510' => ['name' => 'NitroHeads', 'image' => '//img.webike.net/sys_images/brand/brand_510.gif'],
                    '635' => ['name' => 'RS TAICHI', 'image' => '//img.webike.net/sys_images/brand/brand_635.gif'],
                    '527' => ['name' => 'OGK KABUTO', 'image' => '//img.webike.net/sys_images/brand/brand_527.gif'],
                    '345' => ['name' => 'KAWASAKI', 'image' => '//img.webike.tw/brand/brand_345.gif'],
                    '260' => ['name' => 'GIVI', 'image' => '//img.webike.net/sys_images/brand/brand_260.gif'],
                    '1902' => ['name' => 'TSDESIGN', 'image' => '//img.webike.net/sys_images/brand/brand_1902.gif']],
                [   '541' => ['name' => 'OVER','image' => '//img.webike.net/sys_images/brand/brand_541.gif'],
                    '304' => ['name' => 'HURRICANE','image' => '//img.webike.net/sys_images/brand/brand_304.gif'],
                    '339' => ['name' => 'K-FACTORY','image' => '//img.webike.net/sys_images/brand/brand_339.gif'],
                    '1302' => ['name' => 'T-REV','image' => '//img.webike.net/sys_images/brand/brand_1302.gif'],
                    '785' => ['name' => 'TRICK STAR','image' => '//img.webike.net/sys_images/brand/brand_785.gif'],
                    '856' => ['name' => 'YSS','image' => '//img.webike.net/sys_images/brand/brand_856.gif'],
                    '327' => ['name' => 'JB POWER(BITO R&D)','image' => '//img.webike.net/sys_images/brand/brand_327.gif'],
                    '260' => ['name' => 'GIVI','image' => '//img.webike.net/sys_images/brand/brand_260.gif'],
                    '2106' => ['name' => 'Webike MODE','image' => '//img.webike.net/sys_images/brand/brand_2106.gif'],
                    '1401' => ['name' => 'HEALTECH ELECTRONICS','image' => '//img.webike.net/sys_images/brand/brand_1401.gif'],
                    '2162' => ['name' => 'HONDA RIDING GEAR','image' => '//img.webike.net/sys_images/brand/brand_2162.gif'],
                    '177' => ['name' => 'DAYTONA','image' => '//img.webike.net/sys_images/brand/brand_177.gif'],
                    '632' => ['name' => 'ROUGH＆ROAD','image' => '//img.webike.net/sys_images/brand/brand_632.gif'],
                    '1776' => ['name' => 'PREMIER','image' => '//img.webike.net/sys_images/brand/brand_1776.gif'],
                    '1859' => ['name' => 'DOPPELGANGER OUTDOOR','image' => '//img.webike.net/sys_images/brand/brand_1859.gif']],
                [   '177' => ['name' => 'DAYTONA','image' => '//img.webike.net/sys_images/brand/brand_177.gif'],
                    '625' => ['name' => 'RK','image' => '//img.webike.net/sys_images/brand/brand_625.gif'],
                    '72' => ['name' => 'BABYFACE','image' => '//img.webike.net/sys_images/brand/brand_72.gif'],
                    '35' => ['name' => 'AKRAPOVIC','image' => '//img.webike.net/sys_images/brand/brand_35.gif'],
                    '802' => ['name' => 'VANCE＆HINES','image' => '//img.webike.net/sys_images/brand/brand_802.gif'],
                    '2150' => ['name' => 'RDmoto','image' => '//img.webike.net/sys_images/brand/brand_2150.gif'],
                    '2468' => ['name' => 'JARDINE','image' => '//img.webike.net/sys_images/brand/brand_2468.gif'],
                    '1084' => ['name' => 'YAMAHA EUROPE','image' => '//img.webike.net/sys_images/brand/brand_1084.gif'],
                    '1975' => ['name' => 'POLISPORT','image' => '//img.webike.net/sys_images/brand/brand_1975.gif'],
                    '1393' => ['name' => 'MADMAX','image' => '//img.webike.net/sys_images/brand/brand_1393.gif'],
                    '49' => ['name' => 'Arai','image' => '//img.webike.net/sys_images/brand/brand_49.gif'],
                    '354' => ['name' => 'KIJIMA','image' => '//img.webike.net/sys_images/brand/brand_354.gif'],
                    '740' => ['name' => 'SUZUKI','image' => '//img.webike.net/sys_images/brand/brand_740.gif'],
                    '284' => ['name' => 'HARLEY-DAVIDSON','image' => '//img.webike.net/sys_images/brand/brand_284.gif'],
                    '2480' => ['name' => 'RST','image' => '//img.webike.net/sys_images/brand/brand_2480.gif']]
            ];
            
            $sale_brands = $this->getBrandsdate($brands[$week]);
            return $sale_brands;
        });
        
        return $sale_brands;
    }

    public function setDiscountBrand(){

        $week = $this->getDayWeek();

        $discount_brands = $this->getDiscountBrand($week);

        return $this->view_component->discount_brands = $discount_brands;
    }

    protected function getDiscountBrand($week){
        // \Cache::tags('big-promotion-discount_brands'. $week)->flush();
        $discount_brands = \Cache::tags(['big-promotion-discount_brands'. $week])->remember('big-promotion-discount_brands'. $week, 86400, function () use ($week) {

                $brands = [
                     [  't10' => ['name' => 'MOS','image' => '//img.webike.tw/brand/brand_t10.png'],
                        '1624' => ['name' => 'SIMOTA','image' => '//img.webike.net/sys_images/brand/brand_1624.gif'],
                        '501' => ['name' => 'NHRC','image' => '//img.webike.net/sys_images/brand/brand_501.gif'],
                        '30' => ['name' => 'AGV','image' => '//img.webike.net/sys_images/brand/brand_30.gif'],
                        '260' => ['name' => 'GIVI','image' => '//img.webike.net/sys_images/brand/brand_260.gif']],
                    [   't2514' => ['name' => 'DK design 達卡設計','image' => '//img.webike.tw/brand/brand_t2514.jpg'],
                        't38' => ['name' => 'WUKAWA','image' => '//img.webike.tw/brand/brand_t38.jpg'],
                        't1994' => ['name' => 'WRRP','image' => '//img.webike.tw/brand/brand_t1994.jpg'],
                        't1961' => ['name' => 'ZEUS 瑞獅','image' => '//img.webike.tw/brand/brand_t1961.png'],
                        't1895' => ['name' => 'SPEED-R','image' => '//img.webike.tw/brand/brand_t1895.jpg']],
                    [   't2289' => ['name' => 'DOG HOUSE 惡搞手工廠','image' => '//img.webike.tw/brand/brand_t2289.jpg'],
                        't1914' => ['name' => 'EPIC','image' => '//img.webike.tw/brand/brand_t1914.png'],
                        't3477' => ['name' => 'DART','image' => '//img.webike.tw/brand/brand_t3477.jpg'],
                        't2223' => ['name' => 'BENKIA','image' => '//img.webike.tw/brand/brand_t2223.jpg'],
                        't1886' => ['name' => 'EXUSTAR','image' => '//img.webike.tw/brand/brand_t1886.png']],
                    [   '2030' => ['name' => 'KOSO','image' => '//img.webike.net/sys_images/brand/brand_2030.gif'],
                        't2222' => ['name' => '庫帆GarageSaiL','image' => '//img.webike.tw/brand/brand_t2222.jpg'],
                        '1916' => ['name' => 'NCY','image' => '//img.webike.net/sys_images/brand/brand_1916.gif'],
                        't2570' => ['name' => 'AUGI','image' => '//img.webike.tw/brand/brand_t2570.jpg'],
                        't750' => ['name' => 'KAPPA','image' => '//img.webike.tw/brand/brand_t750.gif']],
                    [   't2198' => ['name' => 'ST.LUN 聖崙','image' =>'//img.webike.tw/brand/brand_t2198.jpg'],
                        't2758' => ['name' => 'WIZH 欣炫','image' =>'//img.webike.tw/brand/brand_t2758.jpg'],
                        't3227' => ['name' => 'MOTONE','image' =>'//img.webike.tw/brand/brand_t3227.jpg'],
                        't2012' => ['name' => 'SOL','image' =>'//img.webike.tw/brand/brand_t2012.jpg'],
                        't1828' => ['name' => 'ASTONE','image' =>'//img.webike.tw/brand/brand_t1828.jpg']]
                ];

                $discount_brands = $this->getBrandsdate($brands[$week]);

                return $discount_brands;
        });

        return $discount_brands;
    }

    public function setPreviewBrand(){
        $week_date = ['3/28-4/4','4/5-4/11','4/12-4/18','4/19-4/25','4/26-5/2'];
        foreach ($week_date as $week => $week_day) {            
            $week_sale_brands[] = $this->getSaleBrand($week);
            $week_discount_brands[] = $this->getDiscountBrand($week);
        }

        $this->view_component->week_sale_brands = $week_sale_brands;
        $this->view_component->week_discount_brands = $week_discount_brands;

    }

    protected function getDayWeek(){
        $day = strtotime(date('Y-m-d'));

        $week_date = [
            ['start' => '2019-3-25','end' => '2019-4-4'],
            ['start' => '2019-4-5','end' => '2019-4-11'],
            ['start' => '2019-4-12','end' => '2019-4-18'],
            ['start' => '2019-4-19','end' => '2019-4-25'],
            ['start' => '2019-4-26','end' => '2019-5-2']
        ];
        $week = 0;
        $this->view_component->start_day = date('m/d',strtotime(date('2019-3-25')));
        $this->view_component->end_day = date('m/d',strtotime(date('2019-4-4')));
        foreach ($week_date as $week_key => $week_day) {
            if((strtotime($week_day['start']) <= $day) && (strtotime($week_day['end']) >= $day)){
                $this->view_component->start_day = date('m/d',strtotime($week_day['start']));
                $this->view_component->end_day = date('m/d',strtotime($week_day['end']));
                $week = $week_key;
            }

        }

        return $week;
    }

    protected function getBrandsdate($brands){

        $received_queues_orders = \Cache::tags(['big-promotion-received_queues'])->remember('big-promotion-received_queues', 86400, function () {
                $received_queues =  Queues::select('order_id')->where('date','>','2018-01-01')->where('date','<','2018-12-31')->get();
                $received_queues_order =  array_pluck($received_queues,'order_id');
                return implode(',',$received_queues_order);
           });

        foreach($brands as $brand_url_rewrite => $brand){
                $result = \DB::connection('eg_zero')->select('select pim.image as product_image,o.* from eg_product.product_images as pim
                    join
                    (    
                        select * from
                        (
                        select oid.item_id,oid.`value`,sum(o.quantity) as qty,o.product_id 
                        from order_item_details as oid
                        join
                        (
                            select oi.id,oi.quantity,oi.product_id from order_items as oi
                            join order_item_details as oid
                            on oi.id = oid.item_id
                            where oid.attribute = "manufacturer_url_rewrite"
                            and oi.product_type = 0
                            and oid.value = "'. $brand_url_rewrite . '"
                            and oi.order_id in ('. $received_queues_orders  .')
                        ) as o
                        on oid.item_id = o.id 
                        and oid.attribute = "category_url_path"
                        group by oid.value,o.product_id
                        order by sum(o.quantity) desc
                        ) o
                        group by o.`value`
                        order by sum(o.qty) desc
                        limit 3
                    ) as o
                    on pim.product_id = o.product_id 
                    GROUP BY pim.product_id');

                $product_image = array_pluck($result,'product_image'); 
                $ca_url_path = array_pluck($result,'value');
                $ca_url_path_str = implode(",",$ca_url_path);
                $ca_name = Mptt::select('name','url_path')
                ->whereIn('url_path',$ca_url_path)
                ->orderByRaw(\DB::raw("FIELD(url_path,'$ca_url_path_str')"))
                ->get();

                $brand_date[$brand_url_rewrite]['brand_image'] = $brand['image'];
                $brand_date[$brand_url_rewrite]['brand_name'] = $brand['name'];
                $brand_date[$brand_url_rewrite]['product_image'] = $product_image;
                $brand_date[$brand_url_rewrite]['ca_name'] = $ca_name;


        }

        return $brand_date;
    }

}