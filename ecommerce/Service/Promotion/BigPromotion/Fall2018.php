<?php

namespace Ecommerce\Service\Promotion\BigPromotion;

use Ecommerce\Service\Promotion\Base\PromoBase;
use Everglory\Models\Product;
use App\Components\View\BaseComponent;
use Everglory\Models\Mptt;
use Everglory\Models\Motor;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\SearchService;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;

class Fall2018 extends PromoBase
{

    protected $request;
    protected $category;

    public function __construct()
    {

    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    protected function setSeo()
    {
        $this->breadcrumbs = ['2018', 'fall'];
        $this->seo_title = 'Webike 摩托百貨18周年慶';
        $this->seo_description = 'Webike 18周年慶！秋冬騎士用品、改裝零件加倍回饋，各大品牌精選現折，正廠零件全面95折，完成每日任務再送你現金點數！';
//        $this->seo_keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';

        $this->buildSeo();
    }

    public function setRelParameter($parameter)
    {
        return $this->view_component->rel_parameter = 'rel=2018-10-2018fall';
    }

    public function getTop10Product()
    {
        $products = Product::take(10);
        $this->seo_title = implode('_', $products->pluck('name'));
    }



//    private function verifySeo(){
//        foreach($this->seo_requires as $seo_require){
//            if(!isset($this->view_component->$seo_require) || !$this->view_component->$seo_require){
//                throw new \Exception($seo_require.' is required');
//            }
//        }
//    }

    public function promotion()
    {
        $this->setSeo();
        $this->setRankingCategories();
        $this->setType();
        $this->setRecommends();
        $this->setProducts();
        $this->setDate();
        $this->setRel_parameter();
        $this->setCollections(12);

//        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->render();

        return $this->view_component;
    }


    protected function setRankingCategories()
    {
        $this->view_component->ranking_categories = [
            '1000' => ['title' => '改裝零件', 'sub' => ''],
            '3000' => ['title' => '騎士用品', 'sub' => ''],
            '4000' => ['title' => '保養耗材', 'sub' => ''],
            '8000' => ['title' => '機車工具', 'sub' => ''],
        ];

        $categories = Mptt::where('depth', 2)->where('_active', 1)->whereNotIn('url_path', ['3000-1122', '3000-7994', '4000-4051', '8000-4064', '8000-4067', '8000-4070', '8000-7940', '8000-8850', '8000-8046', '8000-8067', '8000-8300', '8000-8083', '8000-8900', '8000-8109', '8000-8846'])->orderBy('sort')->get()->groupBy(function ($category, $key) {
            return substr($category->url_path, 0, 4);
        });

        foreach ($categories as $root => $category) {
            $this->view_component->ranking_categories[$root]['sub'] = $category;
        }

        $motors = Motor::with('manufacturer')->whereIn('url_rewrite', ['6371', 't5099', '6320', '311', '13468', '215', '13691', '6536', '6443', '672', '6557', '952', '13697', '6339'])->orderBy(\DB::Raw("FIELD(url_rewrite,'6371','t5099','6320','311','13468','215','13691','6536','6443','672','6557','952','13697','6339')"))->select(\DB::Raw('*,url_rewrite as url_path'))->take(14)->get();
        $this->view_component->ranking_categories['motor'] = ['title' => '車型', 'sub' => $motors];
    }

    protected function setType()
    {
        $type = '2018-fall';
        $this->view_component->type = $type;
    }

    protected function setRecommends()
    {
        $type = '2018-fall';
        $this->view_component->recommends = \Ecommerce\Repository\BigPromotionRepository::getProductRecommends($type);
    }

    protected function setProducts()
    {
        $convertService = app()->make(ConvertService::class);
        $search_service = app()->make(SearchService::class);
        $ranking_response_sales = $search_service->selectRanking($this->request);
        $products = $convertService->convertQueryResponseToProducts($ranking_response_sales);

        $this->view_component->products = $products;
    }

    protected function setDate()
    {
        $this->view_component->date = date("Y-m-d");
    }

    protected function setRel_parameter()
    {
        $this->view_component->rel_parameter = 'rel=2018-10-2018fall';
    }

    protected function setCollections($num)
    {
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt($num);
    }

    public function promotionCategory()
    {
        $this->setCategories();
        $this->setDiscounts();
        $this->setRel_parameter();
        $this->setCollections(6);
        $this->setSeo();

        $this->view_component->render();
        return $this->view_component;
    }

    protected function setCategories()
    {
        $promotionName = '2018-fall';
        $this->view_component->categories = \Ecommerce\Repository\BigPromotionRepository::getCategories($promotionName, $this->category)->groupby('ca_rewrite');
    }

    protected function setDiscounts()
    {
        $promotionName = '2018-fall';
        $this->view_component->discounts = \Ecommerce\Service\PromotionService::getCategoryBrandDiscounts($promotionName);
    }

    public function promotionDomestic()
    {
        $this->setRel_parameter();
        $this->setCollections(6);
        $this->setSeo();
        $this->view_component->render();

        return $this->view_component;
    }

    public function getFallProduct($manufacturerUrl_rewrite,$ca_id)
    {        
        $ProductSkus = product::select('products.sku')
        ->join('category_product' ,'products.id','=','category_product.product_id')
        ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
        ->whereIn('category_product.category_id',$ca_id)
        ->where( 'manufacturers.url_rewrite','=', $manufacturerUrl_rewrite)
        ->where('products.is_main','=',1)
        ->where('products.type','=',0)
        ->where('products.active','=',1)
        ->inRandomOrder()
        ->limit(20)
        ->get();
        foreach ($ProductSkus as  $ProductSku) {
            $rideProducts[] = ProductRepository::findDetail($ProductSku->sku, false, 'url_rewrite', ['prices', 'manufacturer', 'images', 'points']);
        }
        foreach ($rideProducts as $key => $rideProduct) {
            $ProductAccessor = new ProductAccessor($rideProduct);
            $rideProducts[$key]->final_price = $ProductAccessor->getFinalPrice($this->view_component->current_customer);
            $rideProducts[$key]->final_point = $ProductAccessor->getFinalPoint($this->view_component->current_customer);
        }
        return $rideProducts;
    }

    public function getSpecialPriceProduct($manufacturerUrl_rewrite)
    {
        $manufacturer_ca = [
            '541' =>[ '1059','1058'],'854' =>[ '1059','1058'],'49' =>['278','279'],'364' =>['289','1545','321'],'635' =>['289','1545','321'],'300' =>['79','1568','1581'],'177' =>['79','67'],'411' =>['67'],'354' =>['79','67'],'2162' =>['289','1545','321'],'983' =>['1581','321'],'289' =>['1581'],'179' =>['321','289'],'732' =>['99','9'],'186' =>['100'],'1302' =>['1566'],'209' =>['32','28'],'1293' =>['99'],'1601' =>['21'],'427' =>['63'],'1635' =>['1577'],'23' =>['12']
        ];

        $ca_id = $manufacturer_ca[$manufacturerUrl_rewrite];
        
        $ProductSkus = product::select('products.sku')
        ->join('category_product' ,'products.id','=','category_product.product_id')
        ->join('manufacturers','products.manufacturer_id','=','manufacturers.id')
        ->whereIn('category_product.category_id',$ca_id)
        ->where('manufacturers.url_rewrite','=', $manufacturerUrl_rewrite)
        ->where('products.is_main','=',1)
        ->where('products.type','=',0)
        ->where('products.active','=',1)
        ->inRandomOrder()
        ->limit(20)
        ->get();
        foreach ($ProductSkus as  $ProductSku) {
            $Products[] = ProductRepository::findDetail($ProductSku->sku, false, 'url_rewrite', ['prices', 'manufacturer', 'images', 'points']);
        }
        foreach ($Products as $key => $Product) {
            $ProductAccessor = new ProductAccessor($Product);
            $Products[$key]->final_price = $ProductAccessor->getFinalPrice($this->view_component->current_customer);
            $Products[$key]->final_point = $ProductAccessor->getFinalPoint($this->view_component->current_customer);
        }
        return $Products;

    }
}