<?php

namespace Ecommerce\Service\Promotion\BigPromotion;

use Ecommerce\Service\Promotion\Base\PromoBase;
use Everglory\Models\Product;
use App\Components\View\BaseComponent;
use Everglory\Models\Mptt;
use Everglory\Models\Motor;
use Ecommerce\Service\Search\ConvertService;
use Ecommerce\Service\SearchService;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;
use Everglory\Models\Order\Item;
use Everglory\Models\Manufacturer;
use Everglory\Models\Accounting\Received\Queues;
class NewYear2019 extends PromoBase
{

    protected $request;
    protected $category;

    public function __construct()
    {

    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    protected function setSeo()
    {
        $this->breadcrumbs = ['2019', 'newyear'];
        $this->seo_title = 'Webike 2019newyear';
        $this->seo_description = 'Webike NEW YEAR SALE！秋冬騎士用品、改裝零件加倍回饋，各大品牌精選現折，正廠零件全面95折，完成每日任務再送你現金點數！﻿';
//        $this->seo_keywords = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';

        $this->buildSeo();
    }

    public function setRelParameter($parameter)
    {
        return $this->view_component->rel_parameter = 'rel=2019-01-2019newyear';
    }

    public function getTop10Product()
    {
        $products = Product::take(10);
        $this->seo_title = implode('_', $products->pluck('name'));
    }



//    private function verifySeo(){
//        foreach($this->seo_requires as $seo_require){
//            if(!isset($this->view_component->$seo_require) || !$this->view_component->$seo_require){
//                throw new \Exception($seo_require.' is required');
//            }
//        }
//    }

    public function promotion()
    {
        $this->setSeo();
        $this->setType();
        $this->setDate();
        $this->setRel_parameter();
        $this->setCollections(4);
        $this->setSaleBrand();
        $this->setDiscountBrand();
//        $this->view_component->setBreadcrumbs('「MyBike」登錄您的愛車');
        $this->view_component->render();

        return $this->view_component;
    }


    protected function setRankingCategories()
    {
        $this->view_component->ranking_categories = [
            '1000' => ['title' => '改裝零件', 'sub' => ''],
            '3000' => ['title' => '騎士用品', 'sub' => ''],
            '4000' => ['title' => '保養耗材', 'sub' => ''],
            '8000' => ['title' => '機車工具', 'sub' => ''],
        ];

        $categories = Mptt::where('depth', 2)->where('_active', 1)->whereNotIn('url_path', ['3000-1122', '3000-7994', '4000-4051', '8000-4064', '8000-4067', '8000-4070', '8000-7940', '8000-8850', '8000-8046', '8000-8067', '8000-8300', '8000-8083', '8000-8900', '8000-8109', '8000-8846'])->orderBy('sort')->get()->groupBy(function ($category, $key) {
            return substr($category->url_path, 0, 4);
        });

        foreach ($categories as $root => $category) {
            $this->view_component->ranking_categories[$root]['sub'] = $category;
        }

        $motors = Motor::with('manufacturer')->whereIn('url_rewrite', ['6371', 't5099', '6320', '311', '13468', '215', '13691', '6536', '6443', '672', '6557', '952', '13697', '6339'])->orderBy(\DB::Raw("FIELD(url_rewrite,'6371','t5099','6320','311','13468','215','13691','6536','6443','672','6557','952','13697','6339')"))->select(\DB::Raw('*,url_rewrite as url_path'))->take(14)->get();
        $this->view_component->ranking_categories['motor'] = ['title' => '車型', 'sub' => $motors];
    }

    protected function setType()
    {
        $type = 'NewYear2019';
        $this->view_component->type = $type;
    }

    protected function setRecommends()
    {
        $type = 'NewYear2019';
        $this->view_component->recommends = \Ecommerce\Repository\BigPromotionRepository::getProductRecommends($type);
    }

    protected function setProducts()
    {
        $convertService = app()->make(ConvertService::class);
        $search_service = app()->make(SearchService::class);
        $ranking_response_sales = $search_service->selectRanking($this->request);
        $products = $convertService->convertQueryResponseToProducts($ranking_response_sales);

        $this->view_component->products = $products;
    }

    protected function setDate()
    {
        $this->view_component->date = date("Y-m-d");
    }

    protected function setRel_parameter()
    {
        $this->view_component->rel_parameter = 'rel=2019-01-2019newyear';
    }

    protected function setCollections($num)
    {
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $this->view_component->collections = $assortmentService->getAssortmentByPublishAt($num);
    }

    public function promotionPreview(){
        $this->setRel_parameter();
        $this->setCollections(4);
        $this->setSeo();
        $this->setPreviewBrand();
        $this->view_component->week_key = $this->getDayWeek();

        $this->view_component->render();
        return $this->view_component;   
    }
    public function promotionBrand()
    {
        $this->setRel_parameter();
        $this->setCollections(4);
        $this->setSeo();

        $this->view_component->render();
        return $this->view_component;
    }

    protected function setCategories()
    {
        $promotionName = 'NewYear2019';
        $this->view_component->categories = \Ecommerce\Repository\BigPromotionRepository::getCategories($promotionName, $this->category)->groupby('ca_rewrite');
    }

    protected function setDiscounts()
    {
        $promotionName = 'NewYear2019';
        $this->view_component->discounts = \Ecommerce\Service\PromotionService::getCategoryBrandDiscounts($promotionName);
    }

    public function promotionDomestic()
    {
        $this->setRel_parameter();
        $this->setCollections(6);
        $this->setSeo();
        $this->view_component->render();

        return $this->view_component;
    }

    public function setSaleBrand()
    {

        $week = $this->getDayWeek();

        $sale_brands = $this->getSaleBrand($week);

        return $this->view_component->sale_brands = $sale_brands;
            
    }

    protected function getSaleBrand($week){
        // \Cache::tags('newyear-promotion-sale_brands'. $week)->flush();
        $sale_brands = \Cache::tags(['newyear-promotion-sale_brands'. $week])->remember('newyear-promotion-sale_brands'. $week, 86400, function () use ($week) {

            $brands = [
                ['300' => ['name' => 'HONDA', 'image' => '//img.webike.net/sys_images/brand/brand_300.gif'],
                        '1209' => ['name' => 'RIZOMA', 'image' => '//img.webike.net/sys_images/brand/brand_1209.gif'],
                        '411' => ['name' => 'Magical Racing', 'image' => '//img.webike.net/sys_images/brand/brand_411.gif'],
                        '147' => ['name' => 'CHIC DESIGN', 'image' => '//img.webike.net/sys_images/brand/brand_147.gif'],
                        '1253' => ['name' => 'Puig', 'image' => '//img.webike.net/sys_images/brand/brand_1253.gif'],
                        '597' => ['name' => 'R&G', 'image' => '//img.webike.net/sys_images/brand/brand_597.gif'],
                        '193' => ['name' => 'DRC', 'image' => '//img.webike.net/sys_images/brand/brand_193.gif'],
                        '1945' => ['name' => 'BIKERS', 'image' => '//img.webike.net/sys_images/brand/brand_1945.gif'],
                        '1124' => ['name' => '月木RACING', 'image' => '//img.webike.net/sys_images/brand/brand_1124.gif'],
                        '666' => ['name' => 'SHOEI', 'image' => '//img.webike.net/sys_images/brand/brand_666.gif'],
                        '179' => ['name' => 'DEGNER', 'image' => '//img.webike.net/sys_images/brand/brand_179.gif'],
                        '740' => ['name' => 'SUZUKI', 'image' => '//img.webike.net/sys_images/brand/brand_740.gif'],
                        '2480' => ['name' => 'RST', 'image' => '//img.webike.net/sys_images/brand/brand_2480.gif'],
                        '1850' => ['name' => 'ICON', 'image' => '//img.webike.net/sys_images/brand/brand_1850.gif'],
                        '386' => ['name' => 'LEAD', 'image' => '//img.webike.net/sys_images/brand/brand_386.gif']],
                ['704' => ['name' => 'SP武川', 'image' => '//img.webike.net/sys_images/brand/brand_704.gif'],
                        '703' => ['name' => 'SP忠男', 'image' => '//img.webike.net/sys_images/brand/brand_703.gif'],
                        '249' => ['name' => 'G-Craft', 'image' => '//img.webike.net/sys_images/brand/brand_249.gif'],
                        '937' => ['name' => '才谷屋', 'image' => '//img.webike.net/sys_images/brand/brand_937.gif'],
                        '290' => ['name' => 'HEPCO＆BECKER', 'image' => '//img.webike.net/sys_images/brand/brand_290.gif'],
                        '186' => ['name' => 'DID', 'image' => '//img.webike.net/sys_images/brand/brand_186.gif'],
                        '209' => ['name' => 'EFFEX', 'image' => '//img.webike.net/sys_images/brand/brand_209.gif'],
                        '1898' => ['name' => 'MUSTANG', 'image' => '//img.webike.net/sys_images/brand/brand_1898.gif'],
                        '791' => ['name' => 'TSR', 'image' => '//img.webike.net/sys_images/brand/brand_791.gif'],
                        '364' => ['name' => 'KOMINE', 'image' => '//img.webike.net/sys_images/brand/brand_364.gif'],
                        '983' => ['name' => 'TANAX motofizz', 'image' => '//img.webike.net/sys_images/brand/brand_983.gif'],
                        '632' => ['name' => 'ROUGH＆ROAD', 'image' => '//img.webike.net/sys_images/brand/brand_632.gif'],
                        '1776' => ['name' => 'PREMIER', 'image' => '//img.webike.net/sys_images/brand/brand_1776.gif'],
                        '40' => ['name' => 'alpinestars', 'image' => '//img.webike.net/sys_images/brand/brand_40.gif'],
                        '1930' => ['name' => 'KTM', 'image' => '//img.webike.net/sys_images/brand/brand_1930.gif']],
                ['854' => ['name' => 'YOSHIMURA', 'image' => '//img.webike.net/sys_images/brand/brand_854.gif'],
                        '2056' => ['name' => 'LighTech', 'image' => '//img.webike.net/sys_images/brand/brand_2056.gif'],
                        '2095' => ['name' => 'KTM POWER PARTS', 'image' => '//img.webike.net/sys_images/brand/brand_2095.gif'],
                        '721' => ['name' => 'STRIKER', 'image' => '//img.webike.net/sys_images/brand/brand_721.gif'],
                        '21' => ['name' => 'ACTIVE', 'image' => '//img.webike.net/sys_images/brand/brand_21.gif'],
                        '740' => ['name' => 'SUZUKI', 'image' => '//img.webike.net/sys_images/brand/brand_740.gif'],
                        '2020' => ['name' => 'DUCABIKE', 'image' => '//img.webike.net/sys_images/brand/brand_2020.gif'],
                        '306' => ['name' => 'HYPERPRO', 'image' => '//img.webike.net/sys_images/brand/brand_306.gif'],
                        '1862' => ['name' => 'CNC Racing', 'image' => '//img.webike.net/sys_images/brand/brand_1862.gif'],
                        '635' => ['name' => 'RS TAICHI', 'image' => '//img.webike.net/sys_images/brand/brand_635.gif'],
                        '527' => ['name' => 'OGK KABUTO motofizz', 'image' => '//img.webike.net/sys_images/brand/brand_527.gif'],
                        '345' => ['name' => 'KAWASAKI', 'image' => '//img.webike.tw/brand/brand_345.gif'],
                        '284' => ['name' => 'HARLEY-DAVIDSON', 'image' => '//img.webike.net/sys_images/brand/brand_284.gif'],
                        '267' => ['name' => 'GOLDWIN', 'image' => '//img.webike.net/sys_images/brand/brand_267.gif'],
                        '697' => ['name' => 'SPIDI', 'image' => '//img.webike.net/sys_images/brand/brand_697.gif']],
                ['541' => ['name' => 'OVER', 'image' => '//img.webike.net/sys_images/brand/brand_541.gif'],
                        '558' => ['name' => 'PIAA', 'image' => '//img.webike.net/sys_images/brand/brand_558.gif'],
                        '72' => ['name' => 'BABYFACE', 'image' => '//img.webike.net/sys_images/brand/brand_72.gif'],
                        '1963' => ['name' => 'IMPACT', 'image' => '//img.webike.net/sys_images/brand/brand_1963.gif'],
                        '497' => ['name' => 'NGK', 'image' => '//img.webike.net/sys_images/brand/brand_497.gif'],
                        '451' => ['name' => 'MORIWAKI', 'image' => '//img.webike.net/sys_images/brand/brand_451.gif'],
                        '1282' => ['name' => 'OUTEX', 'image' => '//img.webike.net/sys_images/brand/brand_1282.gif'],
                        '1480' => ['name' => '田中商會', 'image' => '//img.webike.net/sys_images/brand/brand_1480.gif'],
                        '1365' => ['name' => 'P&A International', 'image' => '//img.webike.net/sys_images/brand/brand_1365.gif'],
                        '2162' => ['name' => 'HONDA RIDING GEAR', 'image' => '//img.webike.net/sys_images/brand/brand_2162.gif'],
                        '298' => ['name' => 'HJC', 'image' => '//img.webike.net/sys_images/brand/brand_298.gif'],
                        '1859' => ['name' => 'DOPPELGANGER OUTDOOR', 'image' => '//img.webike.net/sys_images/brand/brand_1859.gif'],
                        '1902' => ['name' => 'TSDESIGN', 'image' => '//img.webike.net/sys_images/brand/brand_1902.gif'],
                        '2494' => ['name' => 'BELL', 'image' => '//img.webike.net/sys_images/brand/brand_2494.gif'],
                        '851' => ['name' => 'YELLOW CORN', 'image' => '//img.webike.net/sys_images/brand/brand_851.gif']],
                ['177' => ['name' => 'DAYTONA', 'image' => '//img.webike.net/sys_images/brand/brand_177.gif'],
                        '2468' => ['name' => 'JARDINE', 'image' => '//img.webike.net/sys_images/brand/brand_2468.gif'],
                        '339' => ['name' => 'K-FACTORY', 'image' => '//img.webike.net/sys_images/brand/brand_339.gif'],
                        '644' => ['name' => 'r’s gear', 'image' => '//img.webike.net/sys_images/brand/brand_644.gif'],
                        '651' => ['name' => 'SBS', 'image' => '//img.webike.net/sys_images/brand/brand_651.gif'],
                        '1384' => ['name' => 'MINIMOTO', 'image' => '//img.webike.net/sys_images/brand/brand_1384.gif'],
                        '26' => ['name' => 'AELLA', 'image' => '//img.webike.net/sys_images/brand/brand_26.gif'],
                        '266' => ['name' => 'GOLDMEDAL', 'image' => '//img.webike.net/sys_images/brand/brand_266.gif'],
                        '508' => ['name' => 'NISSIN', 'image' => '//img.webike.net/sys_images/brand/brand_508.gif'],
                        '49' => ['name' => 'Arai', 'image' => '//img.webike.net/sys_images/brand/brand_49.gif'],
                        '289' => ['name' => 'HenlyBegins', 'image' => '//img.webike.net/sys_images/brand/brand_289.gif'],
                        '2026' => ['name' => 'DAINESE', 'image' => '//img.webike.tw/brand/brand_2026.gif'],
                        '2224' => ['name' => 'SPEED AND STRENGTH', 'image' => '//img.webike.net/sys_images/brand/brand_2224.gif'],
                        '242' => ['name' => 'FOX', 'image' => '//img.webike.net/sys_images/brand/brand_242.gif'],
                        '2943' => ['name' => 'MotoGP APPAREL', 'image' => '//img.webike.net/sys_images/brand/brand_2943.gif']]
            ];
            
            $sale_brands = $this->getBrandsdate($brands[$week]);
            return $sale_brands;
        });
        
        return $sale_brands;
    }

    public function setDiscountBrand(){

        $week = $this->getDayWeek();

        $discount_brands = $this->getDiscountBrand($week);

        return $this->view_component->discount_brands = $discount_brands;
    }

    protected function getDiscountBrand($week){
        // \Cache::tags('newyear-promotion-discount_brands'. $week)->flush();
        $discount_brands = \Cache::tags(['newyear-promotion-discount_brands'. $week])->remember('newyear-promotion-discount_brands'. $week, 86400, function () use ($week) {

                $brands = [
                     ['t2222' => [ 'name' => '庫帆GarageSaiL', 'image' => '//img.webike.tw/brand/brand_t2222.jpg'],
                            't2758' => [ 'name' => 'WIZH 欣炫', 'image' => '//img.webike.tw/brand/brand_t2758.jpg'],
                            '2123' => [ 'name' => 'SCORPION', 'image' => '//img.webike.net/sys_images/brand/brand_2123.gif'],
                            '30' => [ 'name' => 'AGV', 'image' => '//img.webike.net/sys_images/brand/brand_30.gif'],
                            't1895' => [ 'name' => 'SPEED-R', 'image' => '//img.webike.tw/brand/brand_t1895.jpg']],
                    ['t2514' => [ 'name' => 'DK design 達卡設計' , 'image' => 'https://img.webike.tw/brand/brand_t2514.jpg'],
                            '1624' => [ 'name' => 'SIMOTA' , 'image' => '//img.webike.net/sys_images/brand/brand_1624.gif'],
                            '501' => [ 'name' => 'NHRC' , 'image' => '//img.webike.net/sys_images/brand/brand_501.gif'],
                            't1961' => [ 'name' => 'ZEUS 瑞獅' , 'image' => '//img.webike.tw/brand/brand_t1961.png'],
                            't1886' => [ 'name' => 'EXUSTAR' , 'image' => '//img.webike.tw/brand/brand_t1886.png']],
                    ['t2289' => [ 'name' => 'DOG HOUSE 惡搞手工廠', 'image' => 'https://img.webike.tw/brand/brand_t2289.jpg'],
                            't1994' => [ 'name' => 'WRRP', 'image' => '//img.webike.tw/brand/brand_t1994.jpg'],
                            '463' => [ 'name' => 'MOTOREX', 'image' => '//img.webike.net/sys_images/brand/brand_463.gif'],
                            't2223' => [ 'name' => 'BENKIA', 'image' => '//img.webike.tw/brand/brand_t2223.jpg'],
                            't1828' => [ 'name' => 'ASTONE', 'image' => '//img.webike.tw/brand/brand_t1828.jpg']],
                    ['t1914' => [ 'name' => 'EPIC' , 'image' => '//img.webike.tw/brand/brand_t1914.png'],
                            't3477' => [ 'name' => 'DART' , 'image' => '//img.webike.tw/brand/brand_t3477.jpg'],
                            '1319' => [ 'name' => 'STOMPGRIP' , 'image' => '//img.webike.net/sys_images/brand/brand_1319.gif'],
                            't2570' => [ 'name' => 'AUGI' , 'image' => '//img.webike.tw/brand/brand_t2570.jpg'],
                            't2012' => [ 'name' => 'SOL' , 'image' => 'https://img.webike.tw/brand/brand_t2012.jpg']],
                    ['t3227' => [ 'name' => 'MOTONE' ,'image' => 'https://img.webike.tw/brand/brand_t3227.jpg'],
                            '1916' => [ 'name' => 'NCY' ,'image' => '//img.webike.net/sys_images/brand/brand_1916.gif'],
                            't1997' => [ 'name' => 'CODO' ,'image' => '//img.webike.tw/brand/brand_t1997.gif'],
                            't2452' => [ 'name' => 'HIGHWAY 1' ,'image' => '//img.webike.tw/brand/brand_t2452.jpg'],
                            't750' => [ 'name' => 'KAPPA' ,'image' => '//img.webike.tw/brand/brand_t750.gif']]
                ];

                $discount_brands = $this->getBrandsdate($brands[$week]);

                return $discount_brands;
        });

        return $discount_brands;
    }

    public function setPreviewBrand(){
        $week_date = ['12/28-1/3','1/4-1/10','1/11-1/17','1/18-1/24','1/25-1/31'];
        foreach ($week_date as $week => $week_day) {            
            $week_sale_brands[] = $this->getSaleBrand($week);
            $week_discount_brands[] = $this->getDiscountBrand($week);
        }

        $this->view_component->week_sale_brands = $week_sale_brands;
        $this->view_component->week_discount_brands = $week_discount_brands;

    }

    protected function getDayWeek(){
        $day = strtotime(date('Y-m-d'));

        $week_date = [
            ['start' => '2018-12-28','end' => '2019-1-3'],
            ['start' => '2019-1-4','end' => '2019-1-10'],
            ['start' => '2019-1-11','end' => '2019-1-17'],
            ['start' => '2019-1-18','end' => '2019-1-24'],
            ['start' => '2019-1-25','end' => '2019-1-31']
        ];
        $week = 0;
        $this->view_component->start_day = date('m/d',strtotime(date('2018-12-28')));
        $this->view_component->end_day = date('m/d',strtotime(date('2019-1-3')));
        foreach ($week_date as $week_key => $week_day) {
            if((strtotime($week_day['start']) <= $day) && (strtotime($week_day['end']) >= $day)){
                $this->view_component->start_day = date('m/d',strtotime($week_day['start']));
                $this->view_component->end_day = date('m/d',strtotime($week_day['end']));
                $week = $week_key;
            }

        }

        return $week;
    }

    protected function getBrandsdate($brands){

        $received_queues_orders = \Cache::tags(['newyear-promotion-received_queues'])->remember('newyear-promotion-received_queues', 86400, function () {
                $received_queues =  Queues::select('order_id')->where('date','>','2018-01-01')->where('date','<','2018-12-31')->get();
                $received_queues_order =  array_pluck($received_queues,'order_id');
                return implode(',',$received_queues_order);
           });

        foreach($brands as $brand_url_rewrite => $brand){
                $result = \DB::connection('eg_zero')->select('select pim.image as product_image,o.* from eg_product.product_images as pim
                    join
                    (    
                        select * from
                        (
                        select oid.item_id,oid.`value`,sum(o.quantity) as qty,o.product_id 
                        from order_item_details as oid
                        join
                        (
                            select oi.id,oi.quantity,oi.product_id from order_items as oi
                            join order_item_details as oid
                            on oi.id = oid.item_id
                            where oid.attribute = "manufacturer_url_rewrite"
                            and oi.product_type = 0
                            and oid.value = "'. $brand_url_rewrite . '"
                            and oi.order_id in ('. $received_queues_orders  .')
                        ) as o
                        on oid.item_id = o.id 
                        and oid.attribute = "category_url_path"
                        group by oid.value,o.product_id
                        order by sum(o.quantity) desc
                        ) o
                        group by o.`value`
                        order by sum(o.qty) desc
                        limit 3
                    ) as o
                    on pim.product_id = o.product_id 
                    GROUP BY pim.product_id');

                $product_image = array_pluck($result,'product_image'); 
                $ca_url_path = array_pluck($result,'value');
                $ca_url_path_str = implode(",",$ca_url_path);
                $ca_name = Mptt::select('name','url_path')
                ->whereIn('url_path',$ca_url_path)
                ->orderByRaw(\DB::raw("FIELD(url_path,'$ca_url_path_str')"))
                ->get();

                $brand_date[$brand_url_rewrite]['brand_image'] = $brand['image'];
                $brand_date[$brand_url_rewrite]['brand_name'] = $brand['name'];
                $brand_date[$brand_url_rewrite]['product_image'] = $product_image;
                $brand_date[$brand_url_rewrite]['ca_name'] = $ca_name;


        }

        return $brand_date;
    }

}