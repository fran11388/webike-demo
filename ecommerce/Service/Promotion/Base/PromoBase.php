<?php

namespace Ecommerce\Service\Promotion\Base;


abstract class PromoBase
{
    public $view_component;
    public $seo_requires = [
        'breadcrumbs',
        'seo_title',
        'seo_description',
        'seo_keywords'
    ];
    public $breadcrumbs = [];
    public $seo_title;
    public $seo_description;
//    public $seo_keywords;

    public function buildSeo(){
        if(!count($this->breadcrumbs) or !$this->seo_title or !$this->seo_description){
            throw new \Exception('seo undefined');
        }

        foreach ($this->breadcrumbs as $breadcrumb){
            $this->view_component->setBreadcrumbs($breadcrumb);
        }
        $this->view_component->seo('title', $this->seo_title);
        $this->view_component->seo('description',$this->seo_description);
//        $this->view_component->seo('keywords',$this->seo_keywords);

        return $this->view_component;
    }

    public function setViewComponent($view_component){
        $this->view_component = $view_component;
    }

}