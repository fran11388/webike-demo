<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service\Promotion;

use Ecommerce\Service\BaseService;
use Everglory\Models\Correspond;
use Cache;
use Everglory\Models\Customer\Gift;


class NewYearService extends BaseService
{
    public $year = null;

    public function __construct($year)
    {
        $this->year = $year;
    }

    public function get2018()
    {
        $className = '\Ecommerce\Service\Promotion\NewYear\NewYear' . $this->year;
        return new $className;
    }
}