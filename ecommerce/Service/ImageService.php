<?php
namespace Ecommerce\Service;

use App\Components\View\ReviewComponent;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Wmail;
use Ecommerce\Repository\CategoryRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Repository\ReviewRepository;
use Everglory\Constants\SEO;
use Everglory\Models\Product;
use Everglory\Models\Review;
use Ecommerce\Core\Image;
use Ecommerce\Repository\MotorRepository;
use Everglory\Models\ReviewComment;
use Ecommerce\Service\ProductService;

class ImageService extends BaseService
{
    public function __construct()
    {
    }

    public function upload($path,$img_file)
    {
        $result = new \stdClass();
        $result->success =  false;
        $validate = $this->validateImageFile($img_file);
        if(!$validate->success){
            $result->message = $validate->message;
            $result = json_encode($result);
            $this->getStopJavaScript($result);
            die();
        }

        $destinationPath = public_path() . $path;
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0777);
        }
        $size = $img_file->getSize();
        $mimetype = $img_file->getMimeType();
        $resolution = getimagesize($img_file->getRealPath());

        $filename_tmp = $this->getTmpFile($destinationPath,$img_file);

        $location = $destinationPath . $filename_tmp;
        $img_file->move($destinationPath, $filename_tmp);
        $filename = str_replace('_tmp.' . $img_file->getClientOriginalExtension(), '.' . $img_file->getClientOriginalExtension(), $filename_tmp);
        ImageResize($location, $destinationPath . $filename);

        $result->success = true;
        $result->key = $filename;
        $result = json_encode($result);
        echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
        //unlink($location);
        die();
    }

    public function validateImageFile($img_file)
    {
        $result = new \stdClass();
        $result->success = true;

        if(!$img_file){
            $result->success = false;
            $result->message = '檔案上傳失敗！請檢查格式是否正確';
            return $result;
        }
        if ($img_file->getSize() > 5 * 1024 * 1024) {
            $result->success = false;
            $result->message = '上傳檔案需小於 5 MB';
            return $result;
        }
        if (!in_array($img_file->getMimeType(),['image/jpeg','image/png','image/gif'])) {
            $result->success = false;
            $result->message = '只允許上傳 JPEG / PNG / GIF 格式檔案';
            return $result;
        }
        return $result;
    }

    public function getStopJavaScript($result)
    {
        echo "<script language='javascript' type='text/javascript'>window.top.window.stopUpload({$result});</script>";
    }

    public function getTmpFile($destinationPath = null,$img_file)
    {
        $image_key = getFileKey();
        if($destinationPath){
            while (file_exists($destinationPath . $image_key . '.' . $img_file->getClientOriginalExtension())) {
                $image_key = getFileKey();
            }
        }
        return $image_key . '_tmp.' . $img_file->getClientOriginalExtension();
    }

    public function store($images)
    {
        if(is_array($images)) {
            foreach($images as $image) {
                $imageService = new Image();
                if ($images) {
                    $imageService->collection(public_path() . '/assets/images/user_upload/collection/' . $image, null, $image);
                }
            }
        }else{
            $image = new Image();
            if ($images) {
                $image->collection(public_path() . '/assets/images/user_upload/collection/' . $images, null, $images);
            }
        }
    }
}