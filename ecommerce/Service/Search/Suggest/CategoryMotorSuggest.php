<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2019/3/14
 * Time: 下午 01:54
 */

namespace Ecommerce\Service\Search\Suggest;


use Ecommerce\Core\Solr\Param\CommonParam;
use Ecommerce\Core\Solr\Searcher;

class CategoryMotorSuggest
{
    var $service;
    var $searcher;

    public function __construct(){
        $this->service = app()->make(\Ecommerce\Service\SearchService::class);
        $this->searcher = new Searcher('solr/webike_suggest_category_motor' , env('SOLR_SUGGEST_PORT' , 8080) );
    }

    public function get($keyword)
    {
        $common = new CommonParam();
        $keyword = $common->parseKeyword($keyword, null, true);
        if ($keyword != '*:*') {

            $common->setRows(6);
            $common->setQuery('search_text:' . $keyword);
            $common->setSortField('sale_count');
            $this->searcher->setCommon($common);
            return $this->convert($this->searcher->query()->getQueryResponse()->getDocs());
        }
        return null;
    }

    private function convert( $docs ){
        $result = [];
        foreach ($docs as $doc){
            $obj = new \stdClass();
            $obj->value = $doc->motor_name . ":" . $doc->category_name;
            $obj->label = '';
            $obj->icon = '';
            $obj->href = route('parts','ca/'.$doc->category_url_path . '/mt/' . $doc->motor_url_rewrite);
            $result[] = $obj;
        }
        if ($docs && count($docs)){
            $obj = new \stdClass();
            $obj->value = '';
            $obj->label = '';
            $obj->icon = '';
            $obj->href = '';
            $result[] = $obj;
        }
        return $result;
    }
}