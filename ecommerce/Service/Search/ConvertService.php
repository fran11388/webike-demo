<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/26
 * Time: 上午 12:25
 */

namespace Ecommerce\Service\Search;

use Ecommerce\Repository\ManufacturerRepository;
use Ecommerce\Repository\MotorRepository;
use Everglory\Models\Manufacturer;
use Illuminate\Http\Request;
use Ecommerce\Service\BaseService;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Support\PriceConverter;
use Ecommerce\Core\Solr\Response\SearchResponse;
use Everglory\Models\Product;
use SolrQuery;

class ConvertService extends BaseService
{
    protected $solr_compare;

    public function __construct()
    {
        app()->singleton('EmptyProduct', function () {
            return new Product();
        });

        $this->solr_compare = [
            'mt' => 'motor',
            'mt_array' => 'motors',
            'ca' => 'category',
            'ca_array' => 'categories',
            'br' => 'manufacturer',
        ];
    }

    /**
     * Get the result of solr which type is collection.And return the result of manufacturers information.
     *
     * @param SearchResponse $search_response The result from solr
     * @return \Illuminate\Support\Collection
     */
    public function convertFacetFieldToManufacturers(SearchResponse $search_response)
    {
        $manufacturer_name_url = $search_response->getFacetFieldResponses()['manufacturer_name_url'];
        $manufacturer_name_url = $manufacturer_name_url->getDocs();

        $result = [];
        foreach ($manufacturer_name_url as $key => $count) {
            $object = new \stdClass();
            list ($object->name, $object->url_rewrite, $object->synonym, $object->image) = explode("@", $key);
            $object->count = $count;
            $result[] = $object;
        }

        return collect($result);
    }

    public function convertFacetFieldToManufacturersCountry($search_response)
    {
        $manufacturer_country = $search_response->getFacetFieldResponses()['manufacturer_country'];
        $manufacturer_country = $manufacturer_country->getDocs();
        $result = [];
        foreach ($manufacturer_country as $key => $count) {
            $object = new \stdClass();
            $object->name = $key;
            $object->count = $count;
            $result[] = $object;
        }

        return collect($result);
    }

    /**
     * Get the result of solr which type is collection.And get products information from productAccessor.
     *
     * @param SearchResponse $search_response The result of products from solr
     * @param bool $series
     * @return \Illuminate\Support\Collection|static
     */
    public function convertQueryResponseToProducts($search_response , $series = false)
    {
        if($docs =$search_response->getGroupResponse()){
            $docs = $docs->getDocs();
        }else{
            $docs = $search_response->getQueryResponse()->getDocs();
        }

        $result = [];
        foreach ($docs as $doc) {
            $result[] = new ProductAccessor($doc);

        }

        if($series){
            return collect($result)->keyBy(function ($item, $key) {

                return $item->relation_product_id;
            });
        }

        return collect($result);
    }


    public function convertFacetRangeResponseToPrices(SearchResponse $search_response)
    {

        $collection = PriceConverter::all();

        foreach ($collection as &$price) {

            foreach ($search_response->getFacetRangeResponses()['price_range']->getCount() as $key => $counts) {

                if ($price->min <= $key and $price->max >= $key) {
                    $price->count += $counts;
                }
            }
        }
        return $collection;

    }


    /**
     * @param \Illuminate\Support\Collection $ranking_avg
     * @return \stdClass
     */
    public static function converRankingAvgToObject($ranking_avg){
        $object = new \stdClass();
        $object->counts = $ranking_avg->sum('total');
        $object->ranking_1 = ((  $rank = $ranking_avg->where('ranking' , 1)->first() ) ? $rank->total : 0 );
        $object->ranking_2 = ((  $rank = $ranking_avg->where('ranking' , 2)->first() ) ? $rank->total : 0 );
        $object->ranking_3 = ((  $rank = $ranking_avg->where('ranking' , 3)->first() ) ? $rank->total : 0 );
        $object->ranking_4 = ((  $rank = $ranking_avg->where('ranking' , 4)->first() ) ? $rank->total : 0 );
        $object->ranking_5 = ((  $rank = $ranking_avg->where('ranking' , 5)->first() ) ? $rank->total : 0 );
        if ($object->counts){
            $object->avg = (( $object->ranking_1 * 1 ) +
                    ( $object->ranking_2 * 2 ) +
                    ( $object->ranking_3 * 3 ) +
                    ( $object->ranking_4 * 4 ) +
                    ( $object->ranking_5 * 5 )) / $object->counts;
        } else {
            $object->avg = 0;
        }

        return $object;


    }

    public function convertFacetFieldToMotors($search_response , $motors = null)
    {
        $result = [];
        $exclude = [];
        if($motors){
            $exclude = $motors->pluck('id')->toArray();
        }

        if(isset($search_response->getFacetFieldResponses()['motor_name_detail']) and $search_response->getFacetFieldResponses()['motor_name_detail']){
            $relate_motors = $search_response->getFacetFieldResponses()['motor_name_detail']->getDocs()->toArray();
            foreach ($relate_motors as $key => $count){
                $data = explode('@', $key);
                $object = new \StdClass;
                $object->id = $data[0];
                $object->url_rewrite = $data[1];
                $object->name = $data[2];
                $object->displacement = $data[3];
                $object->manufacturer_url_rewrite = isset($data[4]) ? $data[4] : '';
                $object->manufacturer = isset($data[5]) ? $data[5] : '';
                $object->count = $count;
                if(!count($exclude) or in_array($object->id, $exclude)){
                    $result[] = $object;
                }
            }
        }

//        if(!$motors){
//            $motors = MotorRepository::find(array_keys($motor_ids), 'id');
//        }
//
//        foreach ($motors as $model){
//            $object = $model;
//            $object->count = (array_key_exists( $model->id , $motor_ids) ?  $motor_ids[$model->id] : 0);
//            if($object->count){
//                $result[] = $object;
//            }
//        }

        return collect($result);
    }

    public function addParameterToRequest($data)
    {
        foreach ($data as $key => $value){
            if(isset($this->solr_compare[$key])){
                request()->request->remove($key);
                request()->request->add([$this->solr_compare[$key] => $value]);
            }else{
                request()->request->add([$key => $value]);
            }
        }
    }

    public function restoreRequestParameter()
    {
        foreach (request()->all() as $key => $value) {
            if (in_array($key, $this->solr_compare)) {
                request()->request->remove($key);
                request()->request->add([array_search($key, $this->solr_compare) => $value]);
            }
        }
    }

    public function removeRequestParameter($data)
    {
        foreach ($data as $key => $segment){
            request()->request->remove($key);
        }
    }

    public function setSolrCompare($key, $value)
    {
        if(isset($this->solr_compare[$key])){
            $this->solr_compare[$key] = $value;
        }
    }


    public function convertQueryResponseToSuggest($search_response ,$is_parts = false ,$url = '')
    {
        $docs = $search_response->getGroupResponse()->getDocs();
        $result = [];
        $motor = new \StdClass();
        $manufacturer = new \StdClass();
        $category = new \StdClass();
        $motor->label = '車型';
        $motor->class = 'label label-primary';
        $motor->url = 'model';
        $manufacturer->label = '品牌';
        $manufacturer->class = 'label label-info';
        $manufacturer->url = 'brand';
        $category->label = '分類';
        $category->class = 'label label-danger';
        $category->url = 'category';
        $map = ['1'=>$motor,'2'=>$manufacturer,'3'=>$category];
        $map_url = ['1'=>'mt','2'=>'br','3'=>'ca'];

        foreach ($docs as $doc) {
            $map_model = $map[$doc->data_type];
            $suggest = new \StdClass();
            $suggest->value = $doc->text;
            $suggest->label = $doc->text;

            $suggest->icon = '<label class="'.$map_model->class.'">'.$map_model->label.'</label>';
            if($is_parts){
                $suggest->href = modifyPartsUrl(['page' => '','q'=>''],[$map_url[$doc->data_type]=> $doc->url_rewrite ],'parts',$url);
            }else{
                if($doc->is_part){
                    $suggest->href = route('parts','ca/'.$doc->url_rewrite);
                }else{
                    $suggest->href = route('summary',$map_url[$doc->data_type] . '/' . $doc->url_rewrite);
                }
            }
            $result[] = $suggest;
        }
        return $result;
    }

    public function convertQueryResponseToManufacturesRanking($search_response,$period = 'two_month')
    {
        if($docs =$search_response->getGroupResponse()){
            $docs = $docs->getDocs();
        }else{
            $docs = $search_response->getQueryResponse()->getDocs();
        }
        $ranking_field = $period.'_sales';
        if(!request()->get('motor', null)){
            $ranking_field .= '_group';
        }
        $result = [];
        foreach ($docs as $doc) {
            if(!isset($result[$doc->manufacturer_url])){
                $result[$doc->manufacturer_url] = new \stdClass();
                $result[$doc->manufacturer_url]->url_rewrite = $doc->manufacturer_url ;
                $result[$doc->manufacturer_url]->image = 'https://img.webike.tw/brand/noimage.png';
                $result[$doc->manufacturer_url]->full_name = $doc->manufacturer_name;
                $result[$doc->manufacturer_url]->sales_ranking = 0 ;
                $result[$doc->manufacturer_url]->review_count = 0 ;
                $result[$doc->manufacturer_url]->review_score = 0 ;
                $result[$doc->manufacturer_url]->custom_count = 0 ;
                $result[$doc->manufacturer_url]->riding_count = 0 ;
                $result[$doc->manufacturer_url]->review_score_count = 0 ;
            }
            $result[$doc->manufacturer_url]->sales_ranking += $doc->{$ranking_field} ;
            $result[$doc->manufacturer_url]->review_count += $doc->review_count ;
            $result[$doc->manufacturer_url]->review_score += $doc->ranking ;
            if($doc->ranking){
                $result[$doc->manufacturer_url]->review_score_count++;
            }
        }

        return collect($result);
    }

    public function convertQueryResponseToManufacturesRankingProductCount(&$manufacturers,$response,$type)
    {
        $manufacturer_name_url = $response->getFacetFieldResponses()['manufacturer_name_url'];
        $manufacturer_name_url = $manufacturer_name_url->getDocs();

        foreach ($manufacturer_name_url as $key => $count) {
            $object = new \stdClass();
            list ($object->name, $object->url_rewrite, $object->synonym, $object->image) = explode("@", $key);
            $object->count = $count;
            if(isset($manufacturers[$object->url_rewrite])){
                $manufacturer = $manufacturers[$object->url_rewrite];
                $manufacturer->{$type.'_count'} = $object->count;
//                $manufacturer->image = $object->image;
                $manufacturer->full_name = $object->name;
                if($object->synonym){
                    $manufacturer->full_name .= '('.$object->synonym.')';
                }
            }
        }
    }

    public function convertQueryResponseToManufacturesRankingResult(&$manufacturers)
    {
        $manufacturer_models = ManufacturerRepository::find($manufacturers->keys()->toArray(), 'url_rewrite')->keyBy('url_rewrite');
        $manufacturers = $manufacturers->sortByDesc(function ($manufacturer, $key) use($manufacturer_models){
            $model = $manufacturer_models[$manufacturer->url_rewrite];
            $manufacturer->description = $model->description;
            $manufacturer->image = $model->image;
            return $manufacturer->sales_ranking;
        });
    }
}