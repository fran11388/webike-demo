<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/27
 * Time: 上午 12:46
 */

namespace Ecommerce\Service;


use Ecommerce\Core\Solr\Response\SearchResponse;
use Ecommerce\Repository\CategoryRepository;


class NavigationService extends BaseService
{
    /**
     * @param SearchResponse $searchResponse
     * @return mixed
     */
    public static function getNavigation($searchResponse)
    {
        $facet_data = $searchResponse->getFacetFieldResponses()['url_path']->getDocs()->toArray();
        $baseTree = self::baseTree();

        foreach ($baseTree as $category) {
            self::makeTree($category, $facet_data);
        }
        return $baseTree;
    }

    protected static function baseTree()
    {
        $cache_key = self::getCacheKey(true);
        if (useCache() and $result = \Cache::get($cache_key)) {
            return $result;
        }
        $siblings_categories = [];

        foreach(\Everglory\Constants\Category::TOP as $top_category_url_rewrite){
            $siblings_categories[] = CategoryRepository::find($top_category_url_rewrite, 'url_rewrite');
        }

        $baseTree = collect();
        foreach ($siblings_categories as $category) {
            $nest_categories = CategoryRepository::selectChildren($category);
            $baseTree->push(self::buildBaseTree(clone $category, $nest_categories));
        }

        if (count($baseTree)) {
            \Cache::put($cache_key, $baseTree, 24 * 60);
        }
        return $baseTree;
    }

    /**
     * Get top categories of all subcategories.
     *
     * @param $root
     * @param $nest
     * @return mixed
     */
    protected static function buildBaseTree($root, $nest)
    {
        $collection = $nest->where('parent_id', $root->id)->sortBy('sort');
        foreach ($collection as $node) {
            self::buildBaseTree($node, $nest);
        }
        $root->nodes = $collection;
        return $root;
    }

    /**
     * @param $root
     * @param [] $data
     * @return mixed
     */
    protected static function makeTree($root, $data)
    {
        $field = $root->mptt->url_path;
        if(env('REBUILDING')) {
            if( false !== strpos($field,"4000-5000") ){
                $field = str_replace('4000-5000','1000-5000',$field);
            }elseif( false !== strpos($field,"4000") ){
                $field = str_replace('4000','1000-4000',$field);
            }elseif( false !== strpos($field,"8000") ){
                $field = str_replace('8000','1000-8000',$field);
            }
        }

        $root->count = array_key_exists($field, $data) ? $data[$field] : 0;
        foreach ($root->nodes as $node) {
            $field = $node->mptt->url_path;
            if(env('REBUILDING')) {
                if( false !== strpos($field,"4000-5000") ){
                    $field = str_replace('4000-5000','1000-5000',$field);
                }elseif( false !==  strpos($field,"4000") ){
                    $field = str_replace('4000','1000-4000',$field);
                }elseif( false !== strpos($field,"8000") ){
                    $field = str_replace('8000','1000-8000',$field);
                }
            }
            $node->count = isset($data->$field) ? $data->$field : 0;
            if ($node->nodes) {
                self::makeTree($node, $data);
            }
        }
        return $root;
    }

}