<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Accessor\ProductAccessor;
use Everglory\Models\Customer\Gift as CustomerGift;
use Everglory\Models\Gift;
use Everglory\Models\Product;
use Auth;

class GiftService extends BaseService
{
    const NOT_REWARD = 0;
    const REWARD = 1;
    const IN_CART = 2;
    const CHECKOUT = 3;

    const POINT = 0;
    const PRODUCT = 1;
    const COUPON = 2;

    const TYPES = [
        'points' => 0,
        'free_product' => 1,
        'coupon' => 2,
    ];

    public function __construct( $debug = false )
    {
        $this->customer = \Auth::user();
        if($this->customer){
            $customer_gift_count = self::getActiveCustomerGift([self::REWARD,self::IN_CART])->count();
            session(['gift_count' => $customer_gift_count]);
        }
    }

    public function implementCustomerGiftProduct($customer_id = null)
    {
        if(!$customer_id and $this->customer){
            $customer_id = $this->customer->id;
        }

        $customerGift = self::getActiveCustomerGift(self::NOT_REWARD, null, date('Y-m-d'))->first();
        if($customerGift) {
            $gift = $customerGift->gift;
            if ($gift->type == self::TYPES['free_product'] and $gift->origProduct) {
               $mitumoriService = new MitumoriService;
               $products = $mitumoriService->add($gift)->get();
               foreach($products as $product){
                $this->rewordCustomerGift($customerGift, $gift->product_id);
               }
            } elseif ($gift->type == self::TYPES['coupon']){
                $coupon_service = new CouponService(\Auth::user());
                $coupon_service->assign($gift->coupon_id,null,'2018-11-30',1);
                $this->changeCustomerGiftStatus(self::CHECKOUT, $this->customer->id,$customerGift->id);
            }else{
                PointService::givePointWithoutMission($this->customer, $customerGift->date . '獲得：' . $gift->name, $gift->points, '2018-11-30');
                $this->changeCustomerGiftStatus(self::CHECKOUT, $this->customer->id,$customerGift->id);
            }
            return $customerGift;
        }else{
            \Log::warning('Can not create mitumori gift! customer_id = ' . $customer_id . ($customerGift ? ', customer_gift_id = ' . $customerGift->id : ''));
        }
    }

    public static function changeCustomerGiftStatus($status, $customer_id, $customer_gift_ids)
    {
        $customer_gifts = CustomerGift::where('customer_id', '=', $customer_id);
        if(is_array($customer_gift_ids)){
            $customer_gifts = $customer_gifts->whereIn('id', $customer_gift_ids);
        }else{
            $customer_gifts = $customer_gifts->where('id', $customer_gift_ids);
        }

        foreach ($customer_gifts->get() as $customer_gift){
            $customer_gift->status = $status;
            $customer_gift->save();
        }
    }

    public static function getRandomGifts($activity,$date = null)
    {
        $gifts_probability = [];
        $gifts_list = [];
        if($date){
            $gifts = \Everglory\Models\Gift::where('activity','=',$activity)->where('date','=',$date)->get();    
        }else{
            $gifts = \Everglory\Models\Gift::where('activity','=',$activity)->get();
        }
        foreach($gifts as $gift){
            $gifts_probability[$gift->sort] = $gift->probability;
            $gifts_list[$gift->sort] = $gift;
        }

        $gifts_random_array = [];
        foreach ($gifts_probability as $sort=>$value)
        {
            $gifts_random_array = array_merge($gifts_random_array, array_fill(0, $value, $sort));
        }
        shuffle($gifts_random_array);
        $my_gift_random = $gifts_random_array[array_rand($gifts_random_array)];
        $my_gift = $gifts_list[$my_gift_random];

        return $my_gift;
    }


    public function rewordCustomerGift(CustomerGift $customerGift, $product_id = null)
    {
        $customerGift->product_id = $product_id;
        $customerGift->status = self::REWARD;
        $customerGift->save();
    }

    public static function getCustomerGiftsByStep($view)
    {
        $customerGifts = collect([]);
        $statuses = [];
        if(Auth::check()) {
            if($view == 'cart'){
                $statuses = [self::IN_CART, self::REWARD];
            }elseif($view == 'checkout'){
                $statuses = [self::IN_CART];
            }elseif($view == 'checkout-finish'){
                $statuses = [self::NOT_REWARD, self::REWARD, self::IN_CART, self::CHECKOUT];
            }
            $customerGifts = self::getActiveCustomerGiftBuilder()
                ->whereIn('status', $statuses)
                ->whereHas('gift', function ($query) {
                    $query->where('type', '=', self::TYPES['free_product']);
                })
                ->orderBy('status', 'DESC')
                ->get();
        }
        return $customerGifts;
    }

    public function getCustomerGiftsProduct($customerGifts = null)
    {

        $gifts = collect([]);
        if(!$customerGifts){
            $customerGifts = self::getCustomerGiftsByStep('cart');
        }

        foreach ($customerGifts as $customerGift){
            if ($customerGift->gift->origProduct) {
                $productAccessor = new ProductAccessor($customerGift->gift->origProduct);
            } else {
                $productAccessor = new ProductAccessor($customerGift->product);
            }
            $productAccessor->name = $customerGift->gift->name;
            $productAccessor->full_name = '【' . $productAccessor->getManufacturerName() . '】' . $customerGift->gift->name;
            $productAccessor->customer_gift_status = $customerGift->status;
            $productAccessor->customer_gift_id = $customerGift->id;
            $gifts[] = $productAccessor;
        }
        return $gifts;
    }

    public function InsertCustomerGift($my_gift, $expired_at = null)
    {
        $customer_gifts = new CustomerGift;
        $customer_gifts->customer_id = \Auth::user()->id;
        $customer_gifts->gift_id = $my_gift->id;
        $customer_gifts->date = date('Y-m-d');
        $customer_gifts->expired_at = $expired_at;
        $customer_gifts->status = 0;
        $customer_gifts->save();
    }

    public static function hasGiftInCart($customerGifts = null)
    {
        if(Auth::check()){
            if(!$customerGifts){
                $customerGifts = self::getActiveCustomerGift([self::REWARD, self::IN_CART]);
            }

            $inCartCount = $customerGifts->filter(function($customerGift){
                return $customerGift->status == self::IN_CART;
            })->count();
            $cartService = new CartService;

            if($inCartCount == 1 and $cartService->getCount() > 0){
                return true;
            }else if($inCartCount > 1){
                foreach ($customerGifts as $customerGift){
                    $customerGift->status = self::REWARD;
                    $customerGift->save();
                }
                return null;
            }
        }
        return false;
    }

    public static function getTodayGift()
    {
        return self::getActiveCustomerGift([GiftService::REWARD,GiftService::IN_CART,GiftService::CHECKOUT], null, date('Y-m-d'))->first();
    }

    public static function giftActive($customer_gift_id)
    {
        return self::getActiveCustomerGift([GiftService::NOT_REWARD,GiftService::CHECKOUT], $customer_gift_id)->first();
    }

    public static function getActiveCustomerGift($status, $customer_gift_id = null, $date = null)
    {
        $customerGift = self::getActiveCustomerGiftBuilder();
        if(is_array($status)){
            $customerGift = $customerGift->whereIn('status', $status);
        }else{
            $customerGift = $customerGift->where('status', $status);
        }

        if($customer_gift_id){
            $customerGift = $customerGift->where('id', $customer_gift_id);
        }

        if($date){
            $customerGift = $customerGift->where('date' , $date);
        }
        return $customerGift->get();

    }

    public static function getActiveCustomerGiftBuilder($with = null)
    {   

        $builder = CustomerGift::where('customer_id' , Auth::user()->id)->where(function($query){
            $query->whereNull('expired_at')
                ->OrWhere('expired_at', '>', date('Y-m-d H:i:s'));
        });

        if(is_array($with)){
            $builder = $builder->with($with);
        }else{
            $builder = $builder->with(['gift', 'gift.origProduct', 'product']);
        }
        
        return $builder;
    }
    public function updateNewYearLocation($num)
    {
        $location = $num;
        $updateLocation = CustomerGift::where('customer_id' , Auth::user()->id)
            ->where('date', date('Y-m-d'))
            ->update(['location' => $location]);
        return true;
    }
    public function getCustomerNewYearGifts()
    {
        $customerGifts = CustomerGift::select('gifts.name','customer_gifts.date')->join('gifts','gifts.id','=','customer_gifts.gift_id')
        ->where('customer_gifts.customer_id', Auth::user()->id)
        ->where('customer_gifts.status','>','0')
        ->where('gifts.activity','2018newyear')
        ->groupBy('customer_gifts.date')
        ->get();
        return $customerGifts;
    }

    public function getGiftByActivity($activity){
        $gifts = \Everglory\Models\Gift::where('activity','=',$activity)->get();
        return $gifts;
    }
    public function getGiftProductUrl($priductsId){
        $producturl = \Everglory\Models\Product::select('url_rewrite')->where('id','=',$priductsId)->first();
        return $producturl;
    }
    public function getTestGift(){
        $gifts = \Everglory\Models\Gift::where('activity','=','2018double11')->where('type','=',1)->where('product_id','=',0)->get();
        return $gifts;   
    }
    public function getCustomerRewardGift($dates){
        if(Auth::check()){
            $customer_id = \Auth::user()->id;
            $customer_gift = CustomerGift::where('customer_id' , $customer_id)->whereBetween('date',$dates)->first();
            return $customer_gift;
        }else{
            return false;    
        }
    }
    public function insertCustomerAnniversaryGift($gift_id,$expired_at,$activity,$dates){
        $date =date('Y-m-d');
        $customer_gift = self::getCustomerRewardGift($dates);
        $gift = \Everglory\Models\Gift::where('id','=',$gift_id)->where('activity','=',$activity)->first();

        if($customer_gift){
            $result = ["false" => '您已選擇好禮'];
            return $result;
        }
        
        if(!$gift){
            $result = ["false" => '商品不存在'];
             return $result;
        }
        $result = \DB::transaction(function () use($gift_id,$expired_at) {
            $gift = \Everglory\Models\Gift::where('id','=',$gift_id)->lockForUpdate()->first();
            
            if($gift->remain == 0 and $gift->type != 2){
                $result = ["false" => '好禮已領完'];
                 return $result;
            }
            $customer_id = \Auth::user()->id;
            $customer_gift = new CustomerGift;
            $customer_gift->customer_id = $customer_id;
            $customer_gift->gift_id = $gift->id;
            $customer_gift->product_id = $gift->product_id;
            $customer_gift->date = date('Y-m-d');
            $customer_gift->expired_at = $expired_at;
            if($gift->type == 2){
                $coupon_service = new CouponService(\Auth::user());
                $coupon_service->assign($gift->coupon_id,null,$expired_at,1);
                $customer_gift->status = 3;
            }else{
                $customer_gift->status = 2;
            }

            $customer_gift->save();

            $gift->remain = $gift->remain - 1;
            $gift->save();
            $result = ["true" => '完成'];
            return $result;
        });
        
        return $result;
       
    }

    public function rewardList($activity_rewards){
        $reward_num = [1 => "一獎", 2 => "二獎", 3 => "三獎", 4 => "四獎", 5 => "五獎", 6 => "六獎", 7 => "七獎", 8 => "八獎", 9 => "九獎",10 => "十獎"];
        $reward_list = [];
        foreach ($activity_rewards as $activity_reward) {
            $reward_list[$activity_reward->date][$activity_reward->sort] = ['reward_num' => $reward_num[$activity_reward->sort],'reward_name' => $activity_reward->name,'sort' => $activity_reward->sort]; 
        }
        return $reward_list;
    }

}