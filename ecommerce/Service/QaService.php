<?php
/**
 * Created by PhpStorm.
<<<<<<< HEAD
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
=======
 * User: wayne
 * Date: 2017/06/30
 * Time: 下午 16:30
>>>>>>> Q&A
 */

namespace Ecommerce\Service;

use Auth;
use Ecommerce\Repository\ProductRepository;
use Everglory\Models\Product;
use Everglory\Models\PsQaFinalization;
use Everglory\Models\Qa\Answer;
use Everglory\Models\Qa\Group as QaGroup;
use Everglory\Models\Qa\Source as QaSource;
use Everglory\Models\Estimate\Item as EstimateItem;
use \Everglory\Models\Qa\Relation as QaRelation;
use \Everglory\Models\Qa\Answer as QaAnswer;
use \Everglory\Models\Qa\Question as QaQuestion;
use \Everglory\Models\Qa\Praise as QaPraise;
use Ecommerce\Accessor\ProductAccessor;
use Illuminate\Support\Collection;
use Validator;

class QaService extends BaseService
{
    const CACHE_LIFETIME = 24 * 60;
    const SOURCE_TYPE = 'MorphFinalization';
    const RELATION_PRIORITY = [
        'product_id',
        'series_id',
        'motor_id',
        'manufacturer_id',
        'category_id'
    ];

    public static function getQuestionAnswers( $product )
    {
        $relations = self::findRelationsByProduct($product);
        $answers = self::findAnswers( $relations );
        return $answers;
    }

    public static function search( $data , $paginate = false){

        $questions = QaQuestion::with(['answers','answers.relation','answers.relation.product','answers.relation.manufacturer','answers.relation.category','answers.relation.manufacturer']);
        if(isset($data['question_id'])) {
            $questions = $questions->whereIn('id', $data['question_id']);
        }
        if(isset($data['active'])){
            $questions = $questions->where('active', $data['active'] ? $data['active'] : 0);
        }

        if(isset($data['skus']) and $data['skus']){
            $data['skus'] = explode("\r\n", $data['skus']);
            $products = Product::where('sku', $data['skus'])->get();
            if(count($products)){
                $questions = $questions->whereHas('answers.relation',function($query) use($data, $products){
                    $query->whereIn('product_id', $products->pluck('id'))->orWhereIn('series_id', $products->pluck('id'));
                });
            }
        }

        if(isset($data['manufacturer_ids']) and count($data['manufacturer_ids'])){
            $questions = $questions->whereHas('answers.relation',function($query) use($data){
                $query->whereIn('manufacturer_id', $data['manufacturer_ids']);
            });
        }

        if($paginate and is_numeric($paginate)){
            return $questions->paginate($paginate);
        }else{
            return $questions->get();
        }

    }

    public static function getValidator($requires)
    {
        $require_one_test = false;
        $require_col = current($requires);
        foreach (request()->all() as $key => $value){
            if($value and array_key_exists($key, $requires)){
                $require_one_test = true;
                $require_col = $key;
                break;
            }
        }
        $rules = [
            'question_content' => 'required',
            'answer_content' => 'required',
        ];
        if(!$require_one_test){
            $rules[$require_col] = 'required';
        }

        return Validator::make(request()->all(), $rules);
    }

    public function addQuestionAnswer( $data )
    {
        if(!isset($data['question_content']) or !isset($data['answer_content']) or !isset($data['group_id'])){
            return false;
        }
        $relations = $this->addRelations( $data );
        $questionData['content'] = $data['question_content'];
        $questionData['group_id'] = $data['group_id'];
        $question = $this->addQuestion( $questionData );
        $answerData['question_id'] = $question->id;
        $answerData['content'] = $data['answer_content'];
        $this->addAnswers( $relations, $answerData );
        return $question;
    }

    public function addRelations( $data )
    {
        $relations = [];
        if(isset($data['skus'])){
            $skus = explode("\r\n", $data['skus']);
            $products = ProductRepository::findDetail($skus, false, 'url_rewrite', []);
            foreach ($products as $product){
                $relation = new QaRelation;
                $relation->product_id = $product->id;
                $relation->save();
                $relations[] = $relation;
            }
        }

        if(isset($data['motor_ids'])){
            foreach ($data['motor_ids'] as $motor_id){
                $relation = new QaRelation;
                $relation->motor_id = $motor_id;
                $relation->save();
                $relations[] = $relation;
            }
        }

        if(isset($data['manufacturer_ids'])){
            foreach ($data['manufacturer_ids'] as $manufacturer_id){
                $relation = new QaRelation;
                $relation->manufacturer_id = $manufacturer_id;
                $relation->save();
                $relations[] = $relation;
            }
        }

        return collect($relations);
    }

    public function addQuestion( $data )
    {
        $question = new QaQuestion;
        foreach ($data as $key => $value){
            $question->$key = $value;
        }
        $question->active = 1;
        $question->save();
        return $question;
    }

    public function addAnswers( $relations, $data )
    {
        $answers = [];
        foreach ($relations as $relation){
            $answer = new QaAnswer;
            $answer->relation_id = $relation->id;
            foreach ($data as $key => $value){
                $answer->$key = $value;
            }
            $answer->save();
            $answers[] = $answer;
        }
        return collect($answers);
    }

    public static function findQaInfoByAnswer( $answer_id )
    {
        return QaAnswer::with('relation', 'question')->where('id', $answer_id)->first();
    }

    public static function getWeekStartSunday()
    {
        if(strtotime( "now" ) > strtotime( "sunday" )){
            return date('Y-m-d 00:00:00', strtotime( "sunday" ));
        }else{
            return date('Y-m-d 00:00:00', strtotime( "last sunday" ));
        }
    }

    public static function getWeekEndSaturday()
    {
        if(strtotime( "now" ) > strtotime( "saturday" )){
            return date('Y-m-d 00:00:00', strtotime( "next saturday" ));
        }else{
            return date('Y-m-d 00:00:00', strtotime( "saturday" ));
        }
    }

    public static function findThisWeekQa($start = null, $end = null)
    {
        if(!$start){
            $start = self::getWeekStartSunday();
        }
        if(!$end){
            $end = self::getWeekEndSaturday();
        }
//        dd($start . '<br>' . $end);

        $answers = QaAnswer::with('question', 'relation', 'relation.product', 'relation.series')->where('created_at', '>=', $start)->where('created_at', '<=', $end)->orderBy('created_at', 'DESC')->paginate(30);
        return $answers;
    }

    public static function getRelationProducts($answers){
        $collection = [];
        $answers->filter(function($answer) use(&$collection){
            $model = new \StdClass;
            $products = [];
            if($product = $answer->relation->product){
                if($product->is_main){
                    $products[] = new ProductAccessor($product);
                }
            }else if(count($answer->relation->series)){
                foreach ($answer->relation->series as $item){
                    if($item->is_main) {
                        $products[] = new ProductAccessor($item);
                    }
                }
            }
            $model->answer = $answer;
            $model->products = collect(array_filter($products));
            $collection[] = $model;
        });
        return collect($collection);
    }

    public function autoTransformToQa( $source_name )
    {
        $function_name = $source_name . 'AutoTransformToQa';
        if(method_exists($this, $function_name)){
            set_time_limit (60 * 60 * 3);
            $this->$function_name();
            return true;
        }else{
            throw new \Exception('QaService is not provide ' . $function_name . ' transform function.');
        }
    }

    private function ticketAutoTransformToQa()
    {
        $finalizations = PsQaFinalization::where('status_id', '>=', 8)->get();
        foreach ($finalizations as $finalization){
            $source = QaSource::where('source_type', self::SOURCE_TYPE)->where('source_id', $finalization->id)->first();
            if($source){
                continue;
            }else{
                $source = new QaSource;
                $source->source_type = self::SOURCE_TYPE;
                $source->source_id = $finalization->id;
                $source->save();
            }

            $group = QaGroup::where('ticket_category_id', $finalization->ticket_category_id)->first();
            if(!$group){
                $group = new QaGroup;
                $group->ticket_category_id = $finalization->ticket_category_id;
                $group->name = $finalization->ticket_category_name;
                $group->active = 1;
                $group->save();
            }

            $relation = null;
            $relation_col = null;
            foreach (self::RELATION_PRIORITY as $priority){
                if($finalization->$priority){
                    $relation = QaRelation::where($priority, $finalization->$priority)->first();
                    if(!$relation_col){
                        $relation_col = $priority;
                    }
                    if($relation){
                        break;
                    }
                }
            }
            if(!$relation and $relation_col){
                $relation = new QaRelation;
                $relation->$relation_col = $finalization->$relation_col;
                $relation->save();
            }

            $question = new QaQuestion;
            $question->group_id = $group->id;
            $question->content = $finalization->question_content;
            $question->active = 1;
            $question->save();

            $answer = new QaAnswer;
            $answer->question_id = $question->id;
            $answer->relation_id = $relation->id;
            $answer->content = $finalization->answer_content;
            $answer->save();

            $source->question_id = $question->id;
            $source->save();
        }
    }

    public static function praise($answer_id)
    {
        $praise = QaPraise::where('answer_id', $answer_id)->where('customer_id', \Auth::user()->id)->first();
        if(!$praise){
            $praise = new QaPraise;
            $praise->answer_id = $answer_id;
            $praise->customer_id = \Auth::user()->id;
            $praise->save();
        }
        return $praise;
    }

    public static function getPraiseCount($answer_id, $customer_id = null)
    {
        $praise = QaPraise::select('answer_id', \DB::raw('count(0) AS count'));
        if($customer_id){
            $praise = $praise->where('customer_id', $customer_id);
        }
        if(is_array($answer_id)){
            $praise = $praise->whereIn('answer_id', $answer_id);
        }elseif(strpos($answer_id, '_') !== false){
            $praise = $praise->whereIn('answer_id', explode('_', $answer_id));
        }else{
            $praise = $praise->where('answer_id', $answer_id);
        }
        return $praise->groupby('answer_id')->get();
    }

    /**
     * @param Collection $relations
     * @return \Illuminate\Database\Eloquent\Collection|static | QaAnswer[]
     */
    private static function findAnswers( $relations )
    {
        /*
        $questions = QaQuestion::join('qa_answers' , 'qa_questions.id' , '=' , 'question_id')
            ->whereIn('qa_answers.relation_id' , $relations->pluck('id') )
            ->get();
        */
        $collection =  QaAnswer::with('question')
            ->select(\DB::raw('qa_answers.*'), 'qa_relations.motor_id', 'qa_relations.category_id', 'qa_relations.product_id', 'qa_relations.series_id', 'qa_relations.manufacturer_id', 'qa_relations.tag')->with('Question')
            ->join('qa_relations' , 'qa_answers.relation_id','=','qa_relations.id')
            ->whereIn('qa_answers.relation_id' , $relations->pluck('id') )
            ->orderBy('question_id');
        foreach (self::RELATION_PRIORITY as $priority){
            $collection = $collection->orderByRaw('-qa_relations.' . $priority . ' desc');
        }
        $collection = $collection->get()->groupBy('question_id');

        $answers = [];
        foreach($collection as $groupBy){
            $answers[] = $groupBy->first();
        }

        return collect($answers);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Support\Collection
     */
    private static function findRelationsByProduct( $product )
    {
        $product_ids = [$product->id];
        if($product->group_code){
            $product_ids = Product::where('group_code',$product->group_code)->where('manufacturer_id', $product->manufacturer_id)->pluck('id');
        }

        if(count($product_ids)){
            $builder = QaRelation::whereIn( 'product_id' , $product_ids);
        }else{
            $builder = QaRelation::where('series_id' , $product->relation_product_id)
                ->orWhere('manufacturer_id' , $product->manufacturer_id)
                ->orWhereIn('category_id' , $product->categories->pluck('id'));
            if ( count($product->fitModels))
                $builder = $builder->orWhereIn('motor_id' , $product->fitModels->pluck('motor_id'));
            if ( count($product->series))
                $builder = $builder->orWhereIn('series_id' , $product->series->pluck('id'));
        }

        return $builder->get();

    }


}