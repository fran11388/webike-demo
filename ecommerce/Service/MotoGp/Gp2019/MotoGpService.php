<?php

namespace Ecommerce\Service\MotoGp\Gp2019;

use Ecommerce\Repository\MotoGp\Gp2019\MotogpRepository;
use Ecommerce\Service\BaseService;
use Everglory\Models\Motogp\Table2019\Racer;
use Everglory\Models\Motogp\Table2019\Racer\Detail as RacerDetail;
use Everglory\Models\Motogp\Table2019\Speedway;

class MotoGpService extends BaseService
{
    public function __construct()
    {

    }

    public static function getRandomRacers($origin_racers_rate,$now_racers_rate,$random_total = 1)
    {
        $racers_probability = [];
        $racers_list = [];
        $random_racers = [];
        $result = [];
        foreach($origin_racers_rate as $origin_racer_rate){
            if(isset($now_racers_rate[$origin_racer_rate->id])){
                $racers_probability[$origin_racer_rate->id] = number_format($origin_racer_rate->rate * 0.5 + $now_racers_rate[$origin_racer_rate->id] * 0.5);
            }else{
                $racers_probability[$origin_racer_rate->id] = number_format($origin_racer_rate->rate * 0.5);
            }
        }

        $racers_random_array = [];
        foreach ($racers_probability as $racer_id=>$value)
        {
            $racers_random_array = array_merge($racers_random_array, array_fill(0, $value, $racer_id));
        }
        shuffle($racers_random_array);
        if(count($racers_random_array)){
            while(count($random_racers) < $random_total){
                $random = $racers_random_array[array_rand($racers_random_array)];
                if(!in_array($random,$random_racers)){
                    $random_racers[] = $random;
                }
            }
        }
        foreach(Racer::select('id','number')->whereIn('id',$random_racers)->get() as $racer){
            $result[] = ['img' => assetRemote('image/benefit/event/motogp/2019/choose/after').'/'.$racer->number.'.png',
                'id' => $racer->id
            ];
        };

        return $result;
    }

    public static function getRacerInfo($racer_id, $column = 'id')
    {
        $result = new \StdClass;
        $target = Racer::with('details')->where($column, $racer_id)->first();
        $result->racer = $target;
        $result->infoData = [];
        $total_ranks = Speedway::select('total_rank')->where('active','2')->get();
        $speedway_total_ranks = [];
        if(count($total_ranks)){
            foreach($total_ranks as $speedway_id => $total_rank){
                $speedway_total_ranks[$speedway_id+1] = array_combine(range(1, count(explode(',',$total_rank->total_rank))),array_values(explode(',',$total_rank->total_rank)));
            }
        }

        if($result->racer){
            foreach ($target->details->groupby('type') as $type => $detailGroup){
                foreach ($detailGroup->sortby('sort') as $detail){
                    $result->infoData[$type][] = explode(',', $detail->content);
                }
            }

            $rank = [25, 20, 16, 13, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0.16,0.17,0.18,0.19,0.2,0.21,0.22,0.23,0.24,0.25,0.99,0.98];
            $racerDatas = MotogpRepository::getTopsScore([$target->id], null);
            $speedway_ids = [];
            foreach($racerDatas as $racerData){
                $speedway_ids[] = $racerData->speedway->id;
            }
            $speedway_datas = Speedway::whereNotIn('id',$speedway_ids)->get();
            $racerDatas = $racerDatas->sortBy('speedway_id')->map(function($racerData, $key) use($target, $rank ,$speedway_total_ranks){
                $map = [];
                if($key == 0){
                    $map[] = '開幕戰';
                }else{
                    $map[] = '第 ' . $racerData->speedway_id . ' 站';
                }
                $map[] = date('m月d日', strtotime($racerData->speedway->raced_at_local));
                $map[] = $racerData->speedway->name;
                if($racerData->score == 0.99 ){
                    $map[] = '未參賽';
                }elseif($racerData->score == 0.98){
                    $map[] = '-';
                }else{
                    $map[] = array_search($racerData->score, $rank) + 1;
                }

                if($racerData->score == 0.98){
                    $map[] = '-';
                }elseif($racerData->score < 1){
                    $map[] = 0;
                }else{
                    $map[] = $racerData->score;
                }
                if(isset($speedway_total_ranks[$racerData->speedway->id])){
                    $map[] = $speedway_total_ranks[$racerData->speedway->id][$racerData->racer_id];
                }else{
                    $map[] = '-';
                }
                return $map;
            })->toArray();
            foreach($speedway_datas as $key => $speedway_data){
                $map = [];
                if($speedway_data->id == 1){
                    $map[] = '開幕戰';
                }else{
                    $map[] = '第 ' . $speedway_data->id . ' 站';
                }
                $map[] = date('m月d日', strtotime($speedway_data->raced_at_local));
                $map[] = $speedway_data->name;
                $map[] = '-';
                $map[] = '-';
                $map[] = '-';
                $racerDatas[] = $map;
            }
            array_unshift($racerDatas, ['站次', '正賽時間', '比賽站名', '名次', '積分', '總積分名次']);
            if(count($racerDatas)){
                $result->infoData['目前成績'] = $racerDatas;
            }
        }
        return $result;
    }

}