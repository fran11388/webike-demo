<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

class ReviewService extends BaseService
{
    private $customer;

    public function __construct( $debug = false )
    {
        $this->customer = \Auth::user();
    }

    public static function getImageUrl($reviewModel)
    {
        if(starts_with($reviewModel->photo_key, 'http')){
            $src = $reviewModel->photo_key;
        }else{
            $src = $reviewModel->photo_key ? '//img.webike.tw/review/' . $reviewModel->photo_key : $reviewModel->product_thumbnail;
            $src = fixHttpText($src);
        }
        return $src;
    }
}