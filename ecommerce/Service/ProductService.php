<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Carbon\Carbon;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Solr\Param\CommonParam;
use Ecommerce\Core\Solr\Param\FacetFieldParam;
use Ecommerce\Core\Solr\Param\FilterQueryParam;
use Ecommerce\Core\Solr\Searcher;
use Ecommerce\Repository\DeliveryDayRepository;
use Ecommerce\Repository\ProductRepository;
use Ecommerce\Utils\WorkDate;
use Everglory\Constants\ProductType;
use Everglory\Models\Customer\Visit;
use Everglory\Models\Mitumori;
use Everglory\Models\Product;
use Everglory\Models\Product\OptionFlat;
use Everglory\Models\Stock;
use Ecommerce\Service\Search\ConvertService;

class ProductService extends BaseService
{

    const CACHE_LIFETIME = 24 * 60;

    public static function getProductRelationRanking($product, $sort_field, $limit = 5)
    {

        $searcher = new Searcher();
        $common = new CommonParam();
        $common->setRows($limit);
        $common->setSortField($sort_field);
        $searcher->setCommon($common);
        $last_category = $product->categories->last();
        $cache_key = '?sort_field=' . $sort_field;
        $fq = [];
        if ($last_category and $last_category->mptt) {
            $category_fq = new FilterQueryParam();
            $category_fq->setField('url_path');
            $category_fq->setQuery($last_category->mptt->url_path);
            $fq[] = $category_fq;
            $cache_key .= '&url_path=' . $last_category->mptt->url_path;
        }

        if (count($product->motors) == 1) {
            $motor_fq = new FilterQueryParam();
            $motor_fq->setField('motor_id');
            $motor_fq->setTag('motor');
            $motor_fq->setQuery('(' . $product->motors->implode('id', ' OR ') . ')');
            $fq[] = $motor_fq;
        }

        if (count($fq)) {
            $searcher->setFilterQueries($fq);
            $search_response = $search_response = $searcher->query();

            $convertService = new ConvertService;
            return $convertService->convertQueryResponseToProducts($search_response, true);
        }

        return collect([]);
    }

    public static function getRecentViews($customer, $limit = 42)
    {
        if (!$customer or !$customer->id) {
            return collect([]);
        }
        $visits = Visit::where('customer_id', $customer->id)->orderBy('updated_at', 'desc')->select('product_id')->take($limit)->get();

        $products = ProductRepository::findDetail($visits->pluck('product_id')->toArray(), false, 'id', ['manufacturer', 'images', 'prices', 'points', 'reviews', 'motors']);

        $products = $products->sort(function($val) use($visits){
            $visit_product_ids = $visits->pluck('product_id');
            return array_search($val->id,$visit_product_ids->toArray());
        });

        $result = [];

        foreach ($products as $product) {
            if ($product->active) {
                $result[] = new ProductAccessor($product, false);
            }
        }
        return array_filter($result);

    }


    public static function getOtherCustomerViews($product, $category_id)
    {

        $query = "
            select products.id  , se.relation_product_id  as unq_ancestor from `eg_zero`.`customer_visits`
            inner join `eg_product`.`products`
            on products.id = product_id
            
            inner join `eg_product`.`category_product`
            on products.id = category_product.product_id
            
            
            inner join eg_product.product_relations se
            on products.id = se.product_id
            
            where customer_id in (
            select customer_id
            from customer_visits
            where product_id = '" . $product->id . "'
            and created_at > '" . Carbon::now()->subMonth(1)->toDateString() . "'
            group by customer_id)
            and category_product.category_id = '" . $category_id . "'
            order by customer_visits.created_at desc
        ";
        $products = \DB::select(\DB::raw($query));

        $products = Product::whereIn('id', collect($products)->unique('unq_ancestor')->take(20)->pluck('id')->toArray())
            ->with('images', 'prices', 'points')
            ->get();
        $result = [];
        foreach ($products as $product) {
            $result[] = new ProductAccessor($product, false);
        }

        return collect($result);
    }

    public static function getProductDetail($url_rewrite)
    {
        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::tags(['product'])->get($cache_key)) {
            return $result;
        }

        $result = ProductRepository::findDetail($url_rewrite);
        $result = new ProductAccessor($result, true);
        if ($result) {
            \Cache::tags(['product'])->put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getRelationCategories($product)
    {
        $searcher = new Searcher();
        $common_param = new CommonParam();
        $common_param->setRows(0);
        $searcher->setCommon($common_param);

        $_category = $product->categories[count($product->categories) - 2];
        $filter_query = new FilterQueryParam();
        $filter_query->setField('url_path');
        $filter_query->setQuery($_category->mptt->url_path);
        $searcher->setFilterQueries([$filter_query]);

        $facet_field = new FacetFieldParam();
        $facet_field->setField('name_url');
        $searcher->setFacetFields([$facet_field]);
        $search_response = $searcher->query();

        $docs = $search_response->getFacetFieldResponses()['name_url']->getDocs();
        $result = [];
        foreach ($docs as $key => $count) {
            list($names, $path) = explode("@", $key);
            $names = explode("|", $names);

            $data = new \stdClass();
            $data->names = $names;
            $data->depth = count($names);
            $data->name = last($names);
            $data->count = $count;
            $data->url = $path;
            $result[] = $data;
        }

        return collect($result);
    }

    public static function selectProductFromOptionFlat($name, $group_code, $manufacturer_id)
    {
        $flat = OptionFlat::where('name', $name)
            ->where('group_code', $group_code)
            ->where('manufacturer_id', $manufacturer_id)
            ->with('product', 'product.prices', 'product.points', 'product.stocks')
            ->first();
        if ($flat) {
            return $flat->product;
        }
        return null;
    }

    public static function reindexProductFromOptionFlat($group_code, $manufacturer_id)
    {
        \DB::connection('eg_product')->statement("
            REPLACE INTO `product_option_flat`
            SELECT 
            products.id,
            GROUP_CONCAT(product_option.name ORDER BY product_option.select_id ASC  SEPARATOR '|'),
            products.group_code,
            products.manufacturer_id 
            from products inner join
            product_option
            on products.id = product_option.product_id
            where products.manufacturer_id = " . $manufacturer_id . " and products.group_code = " . $group_code . "
            GROUP BY products.id
        ;");

    }

    public static function getEgZeroStockInfoByApi($post_data)
    {
        $is_post = false;
        $url = EAGLE_EG_ZERO_STOCK_INFO;
        $ch = curl_init();
        if (\Config::get('app.production')) {
            $url = EG_ZERO_STOCK_INFO;
        }
        if ($is_post) {
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_URL, "http://www.webike.tw/iopu-gp-bridge.php");
            curl_setopt($ch, CURLOPT_POST, $is_post);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
        } else {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($post_data));
        }
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);


        return json_decode($result);
    }

    public static function getStockFlgForListByApi($post_data)
    {
        $is_post = false;
        $url = EAGLE_EG_ZERO_STOCK_FLG_FOR_LIST;
        $ch = curl_init();
        if (\Config::get('app.production')) {
            $url = EG_ZERO_STOCK_FLG_FOR_LIST;
        }
        if ($is_post) {
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_URL, "http://www.webike.tw/iopu-gp-bridge.php");
            curl_setopt($ch, CURLOPT_POST, $is_post);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
        } else {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($post_data));
        }
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public static function getStockFlgForList($sku)
    {
        $result = [];
        if (is_array($sku)) {
            $result = self::getStockFlgForListByApi(['barcode' => implode('_', $sku)]);
        } else if ($sku) {
            $result = self::getStockFlgForListByApi(['barcode' => $sku]);
        }
        return $result;
    }

    public static function getProductGroup($product)
    {
        $result = collect([$product]);
        if ($product->group_code) {
            $result = ProductRepository::getGroupProduct($product->manufacturer_id, $product->group_code);
        }
        return $result;
    }


    public static function getStockInfo($select_product, $tw_stock = null)
    {
        $result = new \stdClass();
        if (is_null($tw_stock)) {
            $tw_stock = \Cache::remember('tw_stock_' . $select_product->sku, 30, function () use ($select_product) {
                $result = self::getEgZeroStockInfoByApi(['barcode' => $select_product->sku]);
                if ($result) {
                    return $result;
                }
                return null;
            });
        }

        $result->sku = $select_product->sku;
        $result->sold_out = 0;
        $result->is_maker_stock = false;
        $result->stock = 0;
        $result->stock_info = '';
        $result->can_estimate = false;
        $result->ship_day = '';
        $result->stock_type = false;
        $result->discontinued = false;

        $api_delivery_weight = 4;
        $estimate_text = '訂單成立後，約1~2個工作日回覆出貨日。';
        $estimate_time = '<span class="estimate_time"> (' . date('m/d H:i', strtotime(date('Y-m-d H:i:s'))) . ')</span>';
        if ($tw_stock and isset($tw_stock->scheduled) and $tw_stock->scheduled and isset($tw_stock->delivery_date) and $tw_stock->delivery_date) {
            $result->stock_info = '此商品為預購商品，預定交貨日為' . date('Y', strtotime($tw_stock->delivery_date)) . '年' . date('m', strtotime($tw_stock->delivery_date)) . '月底。';
        } elseif ($tw_stock and ($tw_stock->discontinued || $tw_stock->sold_out || $tw_stock->none_delivery_date) and count($select_product->suppliers->pluck('supplier_id')) == 1) {
            if($tw_stock->sold_out || $tw_stock->none_delivery_date){
                $result->stock_info = '商品已售完。';
                $result->sold_out = 1;
            }elseif($tw_stock->discontinued){
                $result->stock_info = '商品已停產。';
                $result->discontinued = 1;
            }

        } elseif ($tw_stock and isset($tw_stock->quantity) and $tw_stock->quantity) {
            $deliveryDate = WorkDate::getTaiwanDeliveryDate(Carbon::now('Asia/Taipei'), 0);
            $result->stock = $tw_stock->quantity;

            $result->stock_info = '庫存' . $tw_stock->quantity . '個，現在訂購<b> ' .
                Carbon::parse($deliveryDate, 'Asia/Taipei')->format('Y-m-d') . " 台灣在庫" . '</b> 出貨!'
                . $estimate_time;

//            $formatter = "%s現在：Webike在庫 %s 個<br>現在購買此商品，預計將於%s出貨。(測試用: %s個工作日)";
            $formatter = "%s現在：Webike在庫 %s 個。<br>現在購買此商品，預計將於%s出貨。";
            $result->stock_info = sprintf($formatter,
                date('m月d日 H點i分'),
                $tw_stock->quantity,
                Carbon::parse($deliveryDate, 'Asia/Taipei')->format('m月d日')
//                0
            );
            $result->ship_day = $deliveryDate;
            $result->sold_out = ($tw_stock->sold_out or $tw_stock->undiscovered) ? 1 : 0;
            $result->stock_type = 'tw_stock';
        } else if ($tw_stock and isset($tw_stock->maker_stock) and isset($tw_stock->maker_stock->quantity) and $tw_stock->maker_stock->quantity) {
            $deliveryDate = WorkDate::getTaiwanDeliveryDate(Carbon::now('Asia/Taipei'),
                $tw_stock->maker_stock->delivery_text);
            $result->is_maker_stock = true;
            $result->stock = $tw_stock->maker_stock->quantity;

//            $formatter = "%s現在：Webike在庫 %s 個<br>現在購買此商品，預計將於%s出貨。(測試用: %s個工作日)";
            $formatter = "%s現在：Webike在庫 %s 個。<br>現在購買此商品，預計將於%s出貨。";
            $result->stock_info = sprintf($formatter,
                date('m月d日 H點i分'),
                $tw_stock->quantity,
                Carbon::parse($deliveryDate, 'Asia/Taipei')->format('m月d日')
//                $tw_stock->maker_stock->delivery_text
            );
            $result->ship_day = $deliveryDate;
            $result->sold_out = ($tw_stock->sold_out or $tw_stock->undiscovered) ? 1 : 0;
            $result->stock_type = 'tw_stock';
        } else if ($select_product->active == 0 or ($select_product->type == ProductType::OUTLET and !$result->stock)) {
            $result->stock = 0;
            $result->stock_info = '商品已售完。';
            $result->sold_out = 1;
        } else if ($select_product->stock_management == 0 and
            $select_product->type == 0 and
            $supplier_ids = $select_product->suppliers->pluck('supplier_id')->toArray() and
            in_array(1, $supplier_ids)) {

            if (useCache() and \Cache::get('rcj_stock_' . $select_product->sku)) {
                $result = \Cache::get('rcj_stock_' . $select_product->sku);
            } else {
                // API
                $url = "https://www.webike.net/wbs/stock-cooperation-sd-json.html?sku=" . $select_product->sku;
                if (get_http_response_code($url) == "200") {
                    $stock_status = iconv("shift-jis", "utf-8", file_get_contents($url));
                    $stock_status = json_decode($stock_status);


                    if (isset($stock_status[0])) {
                        $stock_status = $stock_status[0];
                        $result->stock_info = $estimate_text;
                        $submit_delivery_weight = $api_delivery_weight - 1;

                        if ($stock_status) {
                            $delivery_time = (int)$stock_status->delivery_time;
                            if ($result->stock = $stock_status->webike_stock and $stock_status->webike_stock > 0) {
                                if($delivery_time == 1){
                                    $delivery_time = 0;
                                }
                                $deliveryDate = WorkDate::getJapanDeliveryDate(Carbon::now('Asia/Taipei'), $delivery_time);


//                                $formatter = "%s現在：協力廠商在庫 %s 個<br>現在購買此商品，預計將於%s出貨。(測試用: 日本回覆工作日%s天，需再加運送日3天)";
                                $formatter = "%s現在：協力廠商在庫 %s 個。<br>現在購買此商品，預計將於%s出貨。";
                                $result->stock_info = sprintf($formatter,
                                    date('m月d日 H點i分'),
                                    $stock_status->webike_stock,
                                    Carbon::parse($deliveryDate, 'Asia/Taipei')->format('m月d日')
//                                    $delivery_time
                                );

                                $result->ship_day = $deliveryDate;
                                $result->stock_type = 'rcj_stock';
                            } elseif ($result->stock = $stock_status->maker_stock and $stock_status->maker_stock > 0) {
                                $deliveryDate = WorkDate::getJapanMakerDeliveryDate(Carbon::now('Asia/Taipei'), $delivery_time);
                                $result->is_maker_stock = true;

//                                $formatter = "%s現在：協力廠商在庫 %s 個<br>現在購買此商品，預訂將於%s出貨。(測試用: 日本回覆工作日%s天，需再加運送日3天)";
                                $formatter = "%s現在：協力廠商在庫 %s 個。<br>現在購買此商品，預訂將於%s出貨。";

                                $result->stock_info = sprintf($formatter,
                                    date('m月d日 H點i分'),
                                    $stock_status->maker_stock,
                                    Carbon::parse($deliveryDate, 'Asia/Taipei')->format('m月d日')
//                                    $delivery_time
                                );
                                $result->ship_day = $deliveryDate;
                                $result->can_estimate = true;
                                $result->stock_type = 'rcj_stock';
                            } else if (count($supplier_ids) > 1) {
                                $result->stock_info = '訂單成立後，約1~2個工作日回覆出貨日。';
                                $result->can_estimate = true;
                            } else if ($stock_status->soldout_flg) {
                                $result->stock_info = '商品已售完。';
                                $result->sold_out = 1;
                            } elseif ($stock_status->stockout_flg) {
                                $result->stock_info = '商品缺貨中。若廠商庫存確定，資訊即時更新。';
                                $result->sold_out = 1;
                            } elseif ($stock_status->to_be_stock and $stock_status->to_be_stock->scheduled) {
                                $delivery_days = round((strtotime($stock_status->to_be_stock->date) - strtotime('now')) / (60 * 60 * 24)) + $submit_delivery_weight;
                                if ($delivery_days > 30) {
                                    $delivery_month_decimal = $delivery_days / 30;
                                    $delivery_month = floor($delivery_month_decimal);
                                    if ((($delivery_month + 0.5) - $delivery_month_decimal) < 0) {
                                        $result->stock_info = '補貨中，預計<b>' . $delivery_month . '</b>個半月入庫。';
                                    } else {
                                        $result->stock_info = '補貨中，預計<b>' . $delivery_month . '</b>個月入庫。';
                                    }
                                } else if ($delivery_days < 0) {
                                    $result->stock_info = $estimate_text;
                                } else {
                                    $result->stock_info = '補貨中，預計<b>' . $delivery_days . '</b>天入庫。';
                                }
                                $result->can_estimate = true;
                            } elseif (!$stock_status->webike_stock and !$stock_status->maker_stock and $delivery_time) {
                                $overDelivery = DeliveryDayRepository::getOverDelivery();
                                if ($delivery_time > ($overDelivery->code - 100)) {
                                    $result->stock_info = '最快<b>' . round(($delivery_time + $submit_delivery_weight) / 7) . '</b>週出貨，訂單成立後約1~2個工作日回覆出貨日。';
                                    $result->can_estimate = true;
                                } else {
                                    $result->stock_info = '訂單成立後，約1~2個工作日回覆出貨日。';
                                    $result->can_estimate = true;
                                }
                            }
                        }
                    } else if (count($supplier_ids) > 1) {
                        $result->stock_info = '訂單成立後，約1~2個工作日回覆出貨日。';
                        $result->can_estimate = true;
                    } else {
                        $result->stock = 0;
                        $result->stock_info = '商品已售完。';
                        $result->sold_out = 1;
                    }
                } else {
                    $result->stock = 0;
                    $result->stock_info = $estimate_text;
                }
                \Cache::put('rcj_stock_' . $select_product->sku, $result, 30);
            }
            /*
             * change for outlet require stock.
             */
        } else if ($select_product->type == 5) {
            $result->stock = 0;
            $result->stock_info = $estimate_text;
            $result->sold_out = 0;
        } else {
            $result->stock_info = $estimate_text;
            $result->stock = 0;
        }
        if ($result->stock_info == $estimate_text) {
            $result->can_estimate = true;
        }

        return $result;
    }

    public static function getNewProductsByCategory($number, $categories = [], $limit = 10, $expect = [])
    {
        $result = [];
        $new_products = ProductRepository::selectNewestProductsWithCategories(true);
        $group_new_products = $new_products->groupBy('category');

        // remove count under limit
        foreach ($group_new_products as $key => $group_new_product) {
            if (count($group_new_product) < $limit) {
                unset($group_new_products[$key]);
            }
        }
        if (count($categories)) {
            // assign categories
            foreach ($categories as $category) {
                if (isset($group_new_products[$category])) {
                    $new_products = $group_new_products[$category];
                    $collection = [];
                    foreach ($new_products as $product) {
                        $collection[] = new ProductAccessor($product, false);
                    }
                    $result[] = collect($collection);
                }
            }

        } else {
            // random categories
            $choice_new_products = $group_new_products->random($number);
            foreach ($choice_new_products as $new_products) {
                $collection = [];
                foreach ($new_products as $product) {
                    $collection[] = new ProductAccessor($product, false);
                }
                $result[] = collect($collection);
            }
        }
        return $result;
    }

    public static function getProductsHtmlByCollections($collections = [], $template, $current_customer)
    {
        $result = [];
        foreach ($collections as $collection) {
            $result[] = view('response.pages.shopping.partials.product.' . $template, compact('collection', 'current_customer'))->render();
        }
        return $result;
    }

    public static function getProductsByConditions($conditions = [], $extras = [], $limit = 20)
    {
        $products = ProductRepository::selectProductsByCondition($conditions, $extras, $limit);
        $collection = [];
        foreach ($products as $product) {
            $collection[] = new ProductAccessor($product, false);
        }
        $result[] = collect($collection);
        return $result;
    }

    public static function pageValidate(ProductAccessor $product)
    {
        if ($product->active == 0) {
            \App::abort(404);
        }

        if ($product->type == 2 or $product->type == 3) {
            if (!\Auth::check() or !$product->queue or \Auth::user()->id != $product->queue->customer_id) {
                \App::abort(404);
            }
        } else if ($product->type == 4) {
            $reflect = \Everglory\Models\Stock\Additional::where('product_id', $product->id)->first();
            if ($reflect) {
                $reflect = $reflect->reflect;
                return route('product-detail', $reflect->url_rewrite);
            } else {
                \App::abort(404);
            }
        }

        if (\Auth::check()) {

            $visit = \Everglory\Models\Customer\Visit::where('customer_id', \Auth::user()->id)->where('product_id', $product->id)->first();
            if ($visit) {
                $visit->count += 1;
            } else {
                $visit = new \Everglory\Models\Customer\Visit();
                $visit->customer_id = \Auth::user()->id;
                $visit->product_id = $product->id;
                $visit->count += 1;
            }
            $visit->save();
        }
    }

    /**
     * Get the ten new amazon magazines and books.
     *
     * @return array
     */
    public static function getAmazonProduct()
    {
        $magazines = \Cache::rememberForever('shopping-magazines', function () {

            $magazines = Product::join('eg_product.category_product', 'products.id', '=', 'category_product.product_id')->with('images')->has('images')->where('category_product.category_id', 1639)->where('type', 0)
                ->where('is_main', 1)
                ->where('active', 1)
                ->where('parent_id', 0)->orderby('created_at', 'DESC')->take(10)->get();
            $result = [];
            foreach ($magazines as $product) {
                $result[] = new ProductAccessor($product, false);
            }
            return collect($result);
        });

        $books = \Cache::rememberForever('shopping-books', function () {
            $books = Product::join('eg_product.category_product', 'products.id', '=', 'category_product.product_id')->with('images')->has('images')->where('category_product.category_id', 134)->where('type', 0)
                ->where('is_main', 1)
                ->where('active', 1)
                ->where('parent_id', 0)->orderby('created_at', 'DESC')->take(10)->get();
            foreach ($books as $product) {
                $result[] = new ProductAccessor($product, false);
            }
            return collect($result);
        });
        return compact('magazines', 'books');
    }

    public static function getJpStockStatus($skus)
    {
        $result = [];
        $skus = array_filter($skus, function ($sku) use (&$result) {
            $result[$sku] = false;
            if (strpos($sku, 't') === false) {
                return true;
            }
        });

        $url = STOCK_API_JAPAN . implode(',', $skus);
        try {

            if (count($skus) and get_http_response_code($url) == "200") {
                $stocks_status = iconv("shift-jis", "utf-8", file_get_contents($url));
                $stocks_status = json_decode($stocks_status);
                foreach ($stocks_status as $stock_status) {
                    $result[$stock_status->s] = $stock_status->webike_stock_flg ? true : false;
                }
            }
        } catch (\Exception $e) {
            \Log::warning(\Request::url());
            \Log::warning('日本庫存API失效');
        }

        return $result;
    }

    public function getMatchProductsByModelNumber($model_numbers, $manufacturer_id = null)
    {
        $result = [];
        $product = null;
        $repository = new ProductRepository();
        $customer = \Auth::user();

        $outlet_products = $repository->getProductByModelNumber($model_numbers, ProductType::OUTLET, $manufacturer_id);
        $normal_products = $repository->getProductByModelNumber($model_numbers, ProductType::NORMAL, $manufacturer_id);
        if (count($outlet_products) || count($normal_products)) {
            foreach ($model_numbers as $model_number) {
                $reset_model_number = trim(str_replace('-', '', $model_number));
                if (in_array($model_number, $outlet_products->pluck('model_number')->toArray()) or in_array($reset_model_number, $outlet_products->pluck('model_number')->toArray())) {
                    $product = $this->filterModelNumber($outlet_products, $model_number);
                    if (!$product) {
                        $product = $this->filterModelNumber($outlet_products, $reset_model_number);
                    }
                    if ($product) {
                        $product_data = new ProductAccessor($product);
                        $price = $product_data->getFinalPrice($customer);
                        $discount = ' (' . number_format(($price / $product->price) * 100) . '%)';
                        $sku = $product->sku;
                        $result[$model_number]['model_number'] = "</span><div style='padding-top: 15px;border-top: 1px solid #dcdee3;' ><a href='" . \URL::route('product-detail', $product->url_rewrite) . "' target='_blank' class='size-10rem'><span class='font-bold'>此項零件有出清品 NT$" . number_format($price) . $discount . "</span></a></div>";
                        $result[$model_number]['delivery'] = "<div style='padding-top: 15px;border-top: 1px solid #dcdee3;'><a href='" . \URL::route('product-detail', $sku) . "' target='_blank'><span class='size-10rem'>購買請直接到商品頁</span></a></div>";
                        $result[$model_number]['note'] = "<div style='padding-top: 15px;border-top: 1px solid #dcdee3;'><a href=\"" . \URL::route('product-detail', $sku) . "\" target=\"_blank\" class=\"size-10rem\">商品頁面</a></div>";
                    }
                } else {
                    $product = $this->filterModelNumber($normal_products, $model_number);
                    if (!$product) {
                        $product = $this->filterModelNumber($normal_products, $reset_model_number);
                    }
                    if ($product) {
                        $sku = $product->sku;
                        $product_data = new ProductAccessor($product);
                        $price = $product_data->getFinalPrice($customer);
                        $result[$model_number]['model_number'] = "</span><div style='padding-top: 15px;border-top: 1px solid #dcdee3;' ><a href='" . \URL::route('product-detail', $product->url_rewrite) . "' target='_blank' class='size-10rem'><span class='font-bold'>此項商品有商品頁面 NT$" . number_format($price) . "</span></a></div>";
                        $result[$model_number]['delivery'] = "<div style='padding-top: 15px;border-top: 1px solid #dcdee3;'><a href='" . \URL::route('product-detail', $sku) . "' target='_blank'><span class='size-10rem'>購買請直接到商品頁</span></a></div>";
                        $result[$model_number]['note'] = "<div style='padding-top: 15px;border-top: 1px solid #dcdee3;'><a href=\"" . \URL::route('product-detail', $sku) . "\" target=\"_blank\" class=\"size-10rem\">商品頁面</a></div>";
                    }
                }
            }
        }

        return $result;
    }

    private function filterModelNumber($products, $model_number)
    {
        $product = $products->first(function ($product) use ($model_number) {
            if ($model_number == $product->model_number) {
                return true;
            }
        });

        return $product;
    }

    public function getDeliveryInfoByType($type, $products)
    {
        $sku = [];
        $result = [];
        $delivery_items = [];
        $product_ids = [];
        $estimate_time = '<span class="estimate_time"> (' . date('m/d H:i', strtotime(date('Y-m-d H:i:s'))) . ')</span>';

        foreach ($products as $product) {
            $sku[] = $product->sku;
            $product_ids[] = $product->id;
        }

        switch ($type) {
            case ProductType::NORMAL:
                foreach ($products as $product) {
                    if ($delivery_info = $product->getInformation() and $product->getInformation()->stock) {
                        $result[$product->sku]['delivery_info'] = $delivery_info->stock->stock_info;
                        $result[$product->sku]['sold_out'] = $delivery_info->stock->sold_out;
                        $result[$product->sku]['discontinued'] = isset($delivery_info->stock->discontinued) ? $delivery_info->stock->discontinued : 0;
                        $result[$product->sku]['stock'] = 999;
                        $result[$product->sku]['stock_type'] = $delivery_info->stock->stock_type;
                    }
                }
                break;
            case ProductType::GENUINEPARTS:
                $delivery_items = \Everglory\Models\Estimate\Item::select('estimate_items.quantity', 'pur.delivery_days', 'estimate_items.created_at', 'estimate_items.sku')
                    ->join('purchases as pur', function ($query) {
                        $query->on('estimate_items.id', '=', 'pur.item_id')
                            ->where('pur.item_type', '=', 'MorphEstimateItem');
                    })->whereIn('estimate_items.sku', $sku)
                    ->get();

                foreach ($delivery_items as $delivery_item) {
                    $estimate_time = '<span class="estimate_time"> (您於' . date('m/d', strtotime($delivery_item->created_at)) . '查詢)</span>';
                    $result[$delivery_item->sku]['delivery_info'] = $delivery_item->delivery_days . '個工作日出貨!' . $estimate_time;
                    $result[$delivery_item->sku]['sold_out'] = 0;
                    $result[$delivery_item->sku]['discontinued'] = 0;
                    $result[$product->sku]['stock'] = $delivery_item->quantity;
                    $result[$product->sku]['stock_type'] = 'rcj_stock';
                }
                break;
            case ProductType::UNREGISTERED:
                $delivery_items = Mitumori\Item::with('product')->select('mitumori_items.syouhin_kosuu', 'mit.request_date as created_at', 'product_nouki', 'product_id')
                    ->join('mitumori as mit', 'mitumori_items.mitumori_id', '=', 'mit.id')
                    ->whereIn('mitumori_items.product_id', $product_ids)
                    ->get();
                foreach ($delivery_items as $delivery_item) {
                    $estimate_time = '<span class="estimate_time"> (您於' . date('m/d', strtotime($delivery_item->created_at)) . '查詢)</span>';
                    $result[$delivery_item->product->sku]['delivery_info'] = str_replace('大約', '', $delivery_item->nouki->nouki_text) . '出貨!' . $estimate_time;
                    $result[$delivery_item->product->sku]['sold_out'] = 0;
                    $result[$delivery_item->product->sku]['discontinued'] = 0;
                    $result[$product->sku]['stock'] = 999;
                    $result[$product->sku]['stock_type'] = 'tw_stock';
                }
                break;
            case ProductType::GROUPBUY:
                $delivery_items = \Everglory\Models\GroupBuy\Item::with('product')->select('groupbuy_items.syouhin_kosuu', 'gro.request_date as created_at', 'product_nouki', 'product_id')
                    ->join('groupbuy as gro', 'groupbuy_items.groupbuy_id', '=', 'gro.id')
                    ->whereIn('groupbuy_items.product_id', $product_ids)
                    ->get();
                foreach ($delivery_items as $delivery_item) {
                    $estimate_time = '<span class="estimate_time"> (您於' . date('m/d', strtotime($delivery_item->created_at)) . '查詢)</span>';
                    $result[$delivery_item->product->sku]['delivery_info'] = str_replace('大約', '', $delivery_item->nouki->nouki_text) . '出貨!' . $estimate_time;
                    $result[$delivery_item->product->sku]['sold_out'] = 0;
                    $result[$delivery_item->product->sku]['discontinued'] = 0;
                    $result[$product->sku]['stock'] = $delivery_item->syouhin_kosuu;
                    $result[$product->sku]['stock_type'] = 'tw_stock';
                }
                break;
            case ProductType::ADDITIONAL:
            case ProductType::OUTLET:
                foreach ($products as $product) {
                    if ($delivery_info = $product->getInformation() and $product->getInformation()->stock) {
                        $result[$product->sku]['delivery_info'] = $delivery_info->stock->stock_info;
                        $result[$product->sku]['sold_out'] = $delivery_info->stock->sold_out;
                        $result[$product->sku]['discontinued'] = isset($delivery_info->stock->discontinued) ? $delivery_info->stock->discontinued : 0;
                        $result[$product->sku]['stock'] = $delivery_info->stock->stock;
                        $result[$product->sku]['stock_type'] = $delivery_info->stock->stock_type;
                    }
                }
                break;
        }

        return $result;

    }
}
