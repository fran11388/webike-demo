<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;


use Everglory\Models\Motor;

class JapanService extends BaseService
{
    /**
     * @param $sku
     * @return array|mixed|null|string
     */
    public static function getImpreList($sku)
    {
        $url = "http://www.webike.net/api/impreList.json?offset=0&limit=10&s=" . $sku;
        $ch = curl_init();
        $check_time = 0;
        $jp_reviews = null;
        do {
            $check_time++;
            sleep($check_time);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'WebikeTW API');
            $jp_reviews = curl_exec($ch);
        } while (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 and $check_time < 2);

        curl_close($ch);
        if ($jp_reviews) {
            try {
                $jp_reviews = iconv("shift-jis", "UTF-8//TRANSLIT", $jp_reviews);
            } catch (\Exception $e) {
                $jp_reviews = iconv("shift-jis", "UTF-8//IGNORE", $jp_reviews);
            }
            $jp_reviews = json_decode($jp_reviews);
        }

        return $jp_reviews;
    }

    /**
     * @param Motor $motor
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getMotorInfo($motor)
    {
        if ($model = $motor->specifications) {
            $model = $model->sortByDesc('release_year')->first();
        }
        $modelInfo = \Cache::rememberForever('index.modelInfo[' . $motor->url_rewrite . ']', function () use ($motor) {
            $check_time = 0;
            $modelInfo = "";
            $url = "http://www.webike.net/api/modelInfo.json?model=" . $motor->url_rewrite . "&charset=utf8";
            // 檢查錯誤
            $ch = curl_init();
            do {
                $check_time++;
                sleep($check_time);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_USERAGENT, 'WebikeTW API');
                $modelInfo = curl_exec($ch);
            } while (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 and $check_time < 2);

            curl_close($ch);
            $modelInfo = json_decode($modelInfo);
            return $modelInfo;
        });

        // $model_jp_img = explode(',', $modelInfo->mybike_image_sources);
        $html = \View::make('desktop.pages.intro.partials.splash.block-motor-content', compact('model', 'modelInfo'))->render();
        return \Response::json(array('html' => $html));
    }

}