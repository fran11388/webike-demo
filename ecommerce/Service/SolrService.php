<?php

namespace Ecommerce\Service;

use Everglory\Models\Product;

class SolrService
{
    const PUBLISH_SOLR_HOSTNAME = '153.120.83.216';
    const PUBLISH_SOLR_PATH = 'solr/webike';
    const WHITE_SOLR_HOSTNAME = '1.34.196.123';
    const WHITE_SOLR_PATH = 'solr/test';

    protected $config = array(
        'endpoint' => '',
        //'endpoint' => '127.0.0.1',
        'port' => '8080',
        'output_format' => 'json',
        'indent' => false,
    );

    protected $baseurl;
    protected $core;
    protected $output_format;
    protected $indent;
    protected $kraken;
    protected $result = '';


    const TIMEOUT_SECONDS = 600;

    public function __construct()
    {
        try {
            $this->config['endpoint'] = env('SOLR_HOSTNAME', self::PUBLISH_SOLR_HOSTNAME);
            $this->config['port'] = env('SOLR_PORT', 8080);

            $this->setCore(env('SOLR_PATH', self::PUBLISH_SOLR_PATH));
            $this->output_format = $this->config['output_format'];
            $this->indent = $this->config['indent'];
            $this->kraken = FALSE;


            return $this;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            throw new \Exception('Exception no Construtor' . $e->getMessage() . "\n\n" . $e->getTraceAsString());
        }

    }

    public function setCore($core)
    {
        $this->core = $core;
        $this->baseurl = "http://" . $this->config['endpoint'] . ':' . $this->config['port'] . '/' . $this->core;
        return $this;
    }

    public function getSuggest($query)
    {
        // $url = $this->buildSolrUrl();
        // $url = 'http://153.120.83.216:8080/solr/webike/suggest?suggest=true&suggest.dictionary=manufacturer&suggest.dictionary=motor&suggest.dictionary=category&wt=json&suggest.q='.urlencode($query);
        $url = 'http://153.120.83.216:8080/solr/webike/suggest?suggest=true&suggest.dictionary=manufacturer&suggest.dictionary=motor&wt=json&suggest.q='.urlencode($query);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return from curl_exec rather than echoing
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Follow redirects
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true); // Always ensure the connection is fresh
        if ($this->kraken) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); // Wait for connection for long periods
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_SECONDS); // Increase timeout for looooooooong results, ensure you have enough memory
        }
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $errno = curl_errno($ch);
            $error = curl_error($ch);
            curl_close($ch);
            throw new \Exception($error, $errno);
        }
        curl_close($ch);
        return $result;
    }

    public function removeDocumentsByProduct($product_id)
    {
        $url = $this->baseurl .  '/update?wt=json';
        if($product_id){
            $json_array = [];

            //basic
            $json_array['id'] = $product_id;
            $data = json_encode(['add'=>['doc'=>$json_array,"boost"=>1.0,'commitWithin'=>1000]]);

            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result);
        }
    }


    public function updateDocumentsByProduct($product)
    {
        $url = $this->baseurl .  '/update?wt=json';
        if($product){
            $json_array = [];

            //basic
            $json_array['id'] = $product->id;
            $json_array['sku'] = ['set'=>$product->sku];
            $json_array['price'] = ['set'=> (is_null($product->price) ? 0 : $product->price)];
            $json_array['point'] = ['set'=> (is_null($product->point) ? 0 : $product->point)];
            $json_array['cost'] = ['set'=> (is_null($product->cost) ? 0 : $product->cost)];
            $json_array['model_number'] = ['set'=>$product->model_number];
            $json_array['type'] = ['set'=>$product->type];
            $json_array['name'] = ['set'=>$product->name];
            $json_array['active'] = ['set'=>$product->active];
            $json_array['p_country'] = ['set'=>$product->p_country];
            $json_array['manufacturer_id'] = ['set'=>$product->manufacturer_id];
            $json_array['visits'] = ['set'=>$product->visits];
            $time =$product->created_at->subHours('9')->toIso8601String();
            $created_at = explode('+',$time) ;
            $json_array['created_at'] = ['set'=>$created_at[0] . 'Z'];
            $json_array['point_type'] = ['set'=>$product->point_type];
            $json_array['relation_product_id'] = ['set'=>$product->relation_product_id];
            if($product->attribute_tag){
                $json_array['attribute_tag'] = ['set'=>$product->attribute_tag];
            }
            $json_array['in_stock_flg'] = ['set' => 0];


            //prices and point
            $prices = $product->prices;
            foreach($prices as $price){
                if($price->role_id == 2){
                    $json_array['price_0'] = ['set'=> (is_null($price->price) ? 0 : $price->price)];
                    $json_array['price_1'] = ['set'=> (is_null($price->price) ? 0 : $price->price)];
                    $json_array['price_2'] = ['set'=> (is_null($price->price) ? 0 : $price->price)];
                }else if($price->role_id == 3){
                    $json_array['price_3'] = ['set'=> (is_null($price->price) ? 0 : $price->price)];
                    $json_array['price_4'] = ['set'=> (is_null($price->price) ? 0 : $price->price)];
                }
            }
            $points = $product->points;
            foreach($points as $point){
                if($point->role_id == 2){
                    $json_array['point_0'] = ['set'=> (is_null($point->point) ? 0 : $point->point)];
                    $json_array['point_1'] = ['set'=> (is_null($point->point) ? 0 : $point->point)];
                    $json_array['point_2'] = ['set'=> (is_null($point->point) ? 0 : $point->point)];
                }else if($point->role_id == 3){
                    $json_array['point_3'] = ['set'=> (is_null($point->point) ? 0 : $point->point)];
                    $json_array['point_4'] = ['set'=> (is_null($point->point) ? 0 : $point->point)];
                }
            }

            $price_diff_2 = 0;
            $price_diff = \DB::connection('eg_product')->table('product_price_difference')->where('customer_group_id',2)->where('product_id',$product->id)->first();
            if($price_diff){
                $price_diff_2 = $price_diff->price_final;
            }
            $json_array['price_diff_2'] = ['set'=>$price_diff_2];
            //manufacturer
            $manufacturer = $product->manufacturer;
            $json_array['manufacturer_active'] = ['set'=>$manufacturer->active];
            $json_array['manufacturer_url'] = ['set'=>$manufacturer->url_rewrite];
            $json_array['manufacturer_synonym'] = ['set'=>$manufacturer->synonym];
            $json_array['manufacturer_country'] = ['set'=>$manufacturer->country];
            $json_array['manufacturer_name'] = ['set'=>$manufacturer->name];
            $manufacturer_name_url = $manufacturer->name . '@' . $manufacturer->url_rewrite . '@' . $manufacturer->synonym  . '@' . $manufacturer->image;
            $json_array['manufacturer_name_url'] = ['set'=>$manufacturer_name_url];




            //description
            $description = $product->productDescription;
            if($description) {
                $json_array['summary'] = ['set' => $description->summary];
                $json_array['caution'] = ['set' => $description->caution];
                $json_array['sentence'] = ['set' => $description->sentence];
                $json_array['remarks'] = ['set' => $description->remarks];
            }else if($description = $product->descriptionOrig){
                $json_array['summary'] = ['set' => $description->summary];
                $json_array['caution'] = ['set' => $description->caution];
                $json_array['sentence'] = ['set' => $description->sentence];
                $json_array['remarks'] = ['set' => $description->remarks];
            }else{
                echo 'description error<br>';
                dd($product);
            }

            //image
            $thumbnails = [];
            $_images = [];
            $images = $product->images;
            foreach ($images as $key => $image) {
                $_images[] = $image->image;
                $thumbnails[] = $image->thumbnail;
            }
            $json_array['thumbnails'] = ['set'=>$thumbnails];
            $json_array['images'] = ['set'=>$_images];

            $_options = [];
            $options = $product->options;
            foreach ($options as $key => $option) {
                $_options[] = $option->name;
            }
            $json_array['options'] = ['set'=>$_options];


            //category
            $categories = $product->categories->pluck('name','id')->toArray();
            array_keys($categories);
            $json_array['category_id'] = ['set'=>array_keys($categories)];
            $json_array['category_name'] = ['set'=>array_values($categories)];

            //categories_flat
            $categories_flats = \DB::connection('eg_product')->select("select
category_product.product_id,
GROUP_CONCAT( categories.url_rewrite order by categories.depth  SEPARATOR '/' ) as flat_url
,
GROUP_CONCAT( categories.name order by categories.depth  SEPARATOR '/' ) as flat_name
from category_product
inner join categories
on category_id  = categories.id
where product_id = ?
and categories.id > 1
and categories.active = 1
group by product_id",[$product->id]);
            foreach($categories_flats as $categories_flat){
                $json_array['breadcrumbs_url'] = ['set'=>$categories_flat->flat_url];
                $json_array['breadcrumbs_name'] = ['set'=>$categories_flat->flat_name];
            }

            //sell count
            $populars = \DB::connection('eg_zero')->select("select count(*) as counter from eg_zero.order_items
inner join eg_zero.orders on order_items.order_id = orders.id
where orders.status_id = 10 and product_type in (0,5) and order_items.main_product_id = ? ",[$product->id]);
            foreach ($populars as $popular) {
                $json_array['popular'] = ['set'=>$popular->counter];
            }

            //categories_path
            $url_path = [];
            $name_path = [];
            $name_url = [];
            $name_url_detail = [];

            $categories_paths = \DB::connection('eg_product')->select("SELECT url_path,
   name_path,
   concat( name_path , '@' , url_path )as name_url,
   concat( name_path , '@' , url_path , '@' , n_left , '@' , n_right ,'@', sort )as name_url_detail
FROM   category_product
   INNER JOIN mptt_categories_flat
           ON category_product.category_id = mptt_categories_flat.id
WHERE  product_id = ?
AND mptt_categories_flat._active = 1
ORDER  BY mptt_categories_flat.depth  ",[$product->id]);
            foreach($categories_paths as $categories_path){
                $url_path[] = $categories_path->url_path;
                $name_path[] = $categories_path->name_path;
                $name_url[] = $categories_path->name_url;
                $name_url_detail[] = $categories_path->name_url_detail;
            }

            $json_array['url_path'] = ['set'=>$url_path];
            $json_array['name_path'] = ['set'=>$name_path];
            $json_array['name_url'] = ['set'=>$name_url];
            $json_array['name_url_detail'] = ['set'=>$name_url_detail];

            //motor
            $motors = $product->motors->pluck('name','id')->toArray();
            array_keys($motors);
            $json_array['motor_id'] = ['set'=>array_keys($motors)];
            $json_array['motor_name'] = ['set'=>array_values($motors)];


            //fit_models
            $models = [];
            $fit_models = \DB::connection('eg_product')->select("select concat( maker  , ' ' , model , ' ' , style ) as models from product_fit_models where product_id= ? ",[$product->id]);
            foreach($fit_models as $fit_model){
                $models[] =  $fit_model->models;
            }
            $json_array['fit_models'] = ['set'=>$models];

            //reviews
            $ranking = 0;
            $review_count = 0;
            $reviews = \DB::connection('eg_product')->select("select COALESCE(sum(ranking) / count(*) , 0) as ranking , count(*) as review_count from ec_dbs.reviews where product_id= ? and view_status = 1  ",[$product->id]);
            foreach($reviews as $review){
                $ranking =  $review->ranking;
                $review_count =  $review->review_count;
            }
            $json_array['ranking'] = ['set'=>$ranking];
            $json_array['review_count'] = ['set'=>$review_count];

            //stock
            $situations = [];
            $stocks = \DB::connection('eg_zero')->select("SELECT stock_situations.name as situations FROM eg_zero.stocks
                    JOIN eg_zero.stock_situations
                                        on stocks.situation_id = stock_situations.id
                    where product_id= ? and bar_code is not null and bar_code != '' ",[$product->id]);
            foreach($stocks as $stock){
                $situations[] = $stock->situations;
            }
            $json_array['situations'] = ['set'=>$situations];

            $data = json_encode(['add'=>['doc'=>$json_array,"boost"=>1.0,'commitWithin'=>1000]]);

            $ch     = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result);

            if($result->responseHeader->status != 0){
                return 'ERROR : '. $result->error->msg;
            }else{
                return 'SUCCESS!';
            }

        }else{
            return 'ERROR : Product not found';
        }
    }

    public function getCommandStatus()
    {
        $url = $this->baseurl . '/dataimport';
        $parameters = [
            'command' => 'status',
            'wt' => 'json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    public function updateRows($rows)
    {
        $unique_keys = ['id', 'sku', 'barcode'];
        $url = $this->baseurl .  '/update?commit=true';
        $update_data = [];
        foreach ($rows as $row){
            $update_item = new \StdClass;
            foreach ($row as $column_name => $column_value){
                if(in_array($column_name, $unique_keys)){
                    $update_item->$column_name = $column_value;
                }else{
                    $update_item->$column_name = new \StdClass;
                    $update_item->$column_name->set = $column_value;
                }
            }
            $update_data[] = $update_item;
        }
//        $update_item->id = 127779;
//        $update_item->review_count = new \StdClass;
//        $update_item->review_count->set = 33;
//        $update_data[] = $update_item;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($update_data));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }

    public static function resetInStockFlg()
    {
        if(config('app.production')){
            $server = EG_ZERO;
        }else{
            $server = EAGLE_EG_ZERO;
        }
        $url = $server . '/api/wms/sim/stock-current/reset';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([]));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public function formatUpdateInStockFlgRows($barcodeCollection, $sold_out = false)
    {
        $stockRows = [];
        $in_stock_flg = 1;
        if($sold_out){
            $in_stock_flg = 0;
        }
        if(is_null($barcodeCollection)){
            return false;
        }
        $product_ids = Product::select('id', 'sku')->whereIn('sku', $barcodeCollection)->where('is_main', 1)->get()->pluck('id', 'sku')->toArray();
        foreach ($barcodeCollection as $barcode){
            if(isset($product_ids[$barcode])){
                $stockRows[] = ['id' => $product_ids[$barcode], 'in_stock_flg' => $in_stock_flg];
            }
        }

        return $stockRows;
    }
}