<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Auth;

class BikeNewsService extends BaseService
{
    const CACHE_LIFETIME = 30;
    /**
     * Get News by specific categories.
     *
     * @param int $limit set news limit.
     * @param array $categories Specific categories for news search
     * @param string $method Search the news include or exclude in categories
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function getPickupCategoryNews($limit, $categories = [], $method = 'include')
    {
        $cache_key = self::getCacheKey();
        if (useCache() and $result = \Cache::tags(['bike_news'])->get($cache_key)) {
            return $result;
        }
        $testConnection = testServerConnection('eg_news');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $news = \Everglory\Models\News\Post::select(\DB::raw('wp_posts.*'), \DB::raw('thumbnail.guid as thumbnail'))
            ->join('wp_term_relationships', 'wp_posts.ID', '=', 'wp_term_relationships.object_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')

            ->join('wp_postmeta', function($join){
                $join->on('wp_postmeta.post_id', '=', 'wp_posts.ID')
                    ->where('wp_postmeta.meta_key', '=', '_thumbnail_id');
            })
            ->join('wp_postmeta as pickup', function($join){
                $join->on('pickup.post_id', '=', 'wp_posts.ID')
                    ->where('pickup.meta_key', '=', 'pickup_post')
                    ->where('pickup.meta_value', '=', 'on');
            })
            ->join('wp_posts as thumbnail', 'thumbnail.ID', '=', 'wp_postmeta.meta_value')
            ->where('wp_posts.post_status', 'publish')
            ->where('wp_posts.post_type', 'post')
            ->where('wp_term_taxonomy.taxonomy', 'category')
            ->where('wp_term_relationships.term_order', 0)
            ->orderBy('wp_posts.post_date', 'DESC')
            ->groupBy('wp_posts.ID');

        if(count($categories) and $method === 'include'){
            $news = $news->whereIn('wp_term_taxonomy.term_taxonomy_id', $categories);
        }else if(count($categories) and $method === 'exclude'){
            $news = $news->whereNotIn('wp_term_taxonomy.term_taxonomy_id', $categories);
        }

        if($limit){
            $news = $news->take($limit);
        }

        $result = $news->get();
        if ($result) {
            \Cache::tags(['bike_news'])->put($cache_key, $result, self::CACHE_LIFETIME);
        }
        return $result;
    }

    public static function getPostWithThumbnail($post_ids)
    {
        $collection = [];
        // dd($post_ids);
        $posts = \Everglory\Models\News\Post::with('metas', 'metas.thumbnail')->whereIn('ID',$post_ids)->get();

        foreach($posts as $key => $post){

            $result = new \StdClass;
            $result->title = $post->post_title;
            $result->content = mb_substr(strip_tags($post->post_content),0,100);
            // dd($result->content);
            $result->name = $post->post_name;
            $result->url = $post->guid;
            $result->meta_value = null;
            $result->img = null;
            $meta = $post->metas->filter(function ($meta) use($post){
                if($meta->meta_key == "_thumbnail_id" and $post->ID == $meta->post_id){
                    return true;
                }
            })->first();

            if($meta){
                $result->meta_value = $meta->meta_value;
                $image = $meta->thumbnail;
                if( $post->ID == $result->meta_value){
                    return true;
                }
                $result->img = $image->guid;
            }

            $collection[] = $result;
        }

        // foreach ($wpnews_metas as $key => $wpnews_meta) {
        //    $result-> = $wpnews_meta->meta_value; 
        // }

        // $wpnews_meta_value_ids = \Everglory\Models\News\Post::whereIn('ID',$wpnews_meta_value)->get();

        // foreach ($wpnews_meta_value_ids as $key => $wpnews_meta_value_id) {
        //     $result->img = $wpnews_meta_value_id->guid;
        // }

        return $collection;



    }

    public static function getSuzukaTagNews($limit)
    {
        $testConnection = testServerConnection('eg_news');
        if(!$testConnection->success){
            return $testConnection->callback;
        }

        $result = \Everglory\Models\News\Post::join('wp_term_relationships', 'ID', '=', 'wp_term_relationships.object_id')
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->join('wp_terms', 'wp_terms.term_id', '=', 'wp_term_taxonomy.term_id')
            ->where('post_status', 'publish')
            ->where('post_type', 'post')
            ->where('term_order', 0)
            ->where('wp_terms.name', '=' ,'Suzuka8hours')
            // ->where('post_date','like',$year.'%')
            ->orderBy('post_date', 'DESC')
            ->groupBy('ID')
            ->limit($limit)
            ->get();

        return $result;
    }

}