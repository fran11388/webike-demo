<?php
/**
 * Created by PhpStorm.
 * User: wayne
 * Date: 2017/03/16
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Order;

class TrackDataService extends BaseService
{
    const CONVERSION_ID = 983559217;
    const CONVERSION_LABEL = 'DjD6CLjCr2EQsdj_1AM';
    const CONVERSION_CURRENCY = 'TWD';

    public function getCheckoutTrackingData(Order $order)
    {
        $orderService = new OrderService;
        $order_track_data = $this->getOrderDataBasic();
        $order_track_data->id = $order->id;
        $order_track_data->increment_id = $order->increment_id;
        $order_track_data->revenue = $order->subtotal;
        $order_track_data->subtotal = $order->grand_total;
        $order_track_data->shipping = $order->shipping_amount;
        $order_track_data->discount = $order->coupon_amount + $order->rewardpoints_amount;
        if($order->items){
            foreach ($order->items as $item){
                $product_track_data = $this->getItemDataBasic();
                $item_detail = $orderService->getItemDetail($item);
                $product_track_data->id = $order->id;
                $product_track_data->name = str_replace('"', '', $item_detail->product_name);
                $product_track_data->sku = $item->sku;
                $product_track_data->price = $item->price;
                $product_track_data->quantity = $item->quantity;
                if($item_detail->category_name_path){
                    $name_paths = explode('|', $item_detail->category_name_path);
                    $product_track_data->categories = $name_paths;
                    $product_track_data->category = end($name_paths);
                }
                $product_track_data->quantity = $item->quantity;
                $order_track_data->items[] = $product_track_data;
                $order_track_data->item_ids[] = $item->product_id;
            }
        }
        $order_track_data->items = collect($order_track_data->items);

        return $order_track_data;
    }

    public function getOrderDataBasic()
    {
        $data = new \stdClass();
        $properties = [
            'id' => null,
            'affiliation' => 'Webike',
            'revenue' => 0,
            'shipping' => 0,
            'tax' => 0,
            'subtotal' => 0,
            'discount' => 0,
            'currency' => self::CONVERSION_CURRENCY,
            'conversion_id' => self::CONVERSION_ID,
            'conversion_label' => self::CONVERSION_LABEL,
            'items' => [],
            'item_ids' => [], //int
        ];
        foreach ($properties as $key => $value){
            $data->$key = $value;
        }
        return $data;
    }

    public function getItemDataBasic()
    {
        $data = new \stdClass();
        $properties = [
            'id' => null,
            'name' => null,
            'sku' => 0,
            'price' => 0,
            'quantity' => 0,
            'category' => null,
            'currency' => self::CONVERSION_CURRENCY,
        ];
        foreach ($properties as $key => $value){
            $data->$key = $value;
        }
        return $data;
    }

}