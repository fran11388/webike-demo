<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Accessor\OrderAccessor;
use Ecommerce\Accessor\ProductAccessor;
use Ecommerce\Core\Celebration;
use Everglory\Constants\ProductType;
use Everglory\Models\Customer;
use Everglory\Models\Customer\Address;
use Everglory\Models\Order;
use Everglory\Models\Product;
use Everglory\Models\Sales\Quote;
use Everglory\Models\Sales\Status;
use Everglory\Models\Sequence;
use Everglory\Models\Order\Item;
use Everglory\Models\Coupon;
use Uuid;
use Everglory\Models\Sales\Cart;
use Exception;
use Log;

/**
 * Quote is flash checkout information.
 * Using quote to prevent duplicate or error checkout.
 * Class QuoteService
 * @package Ecommerce\Service
 */
class QuoteService extends BaseService
{

    /**
     * @var Quote $quote
     **/
    private $quote;

    /**
     *
     * QuoteService constructor.
     * @param $quote_id
     */
    public function __construct($quote_id)
    {
        $this->quote = Quote::onWriteConnection()->with('customer', 'customer.role', 'coupon')->findOrFail($quote_id);
    }

    /**
     * Create quote and save into database
     * @param CartService $cartService
     * @return Quote
     */
    public static function createQuote($cartService)
    {
        $customer = \Auth::user();
        $quote = new Quote();
        $quote->customer_id = $customer->id;
        $quote->code = (string)Uuid::generate(4);
        $cart_items = [];
        foreach ($cartService->getItems() as $_item) {
            $item = new \stdClass();
            $item->quantity = $_item->quantity;
            $item->cache_id = $_item->cache_id;
            $cart_items[$_item->product_id] = $item;
        }

        /*
         * add gift to checkout
         * */
        $customerGifts = GiftService::getCustomerGiftsByStep('checkout');
        if(GiftService::hasGiftInCart($customerGifts)){
            foreach ($customerGifts as $_gift) {
                $item = new \stdClass();
                $item->quantity = 1;
                $item->cache_id = null;
                $cart_items[$_gift->product_id] = $item;
            }
        }

        $coupon = $cartService->getUsageCoupon();
        $quote->coupon_amount = (($coupon and $coupon->discount_type != 'shippingfee') ? $coupon->discount : 0);
        $quote->cart_items = serialize(collect($cart_items));
        $quote->used_coupon_id = ($coupon ? $coupon->id : null);
        $quote->used_points = $cartService->getUsagePoints();
        $quote->received_points = $cartService->getReceivedPoints();
        $quote->cart_subtotal = $cartService->getSubtotal(false);
        $quote->discount_total = $cartService->getDiscountTotal();
        $quote->shipping_amount = $cartService->getFee();

        $celebration = new Celebration();
        $quote->celebration_description = $celebration->getTitleInCheckout();
        $quote->celebration_amount = $celebration->getDiscount();
        $quote->save();
        return $quote;
    }

    /**
     * Check quote content.
     * @param Quote $quote
     * @return bool
     */
    public function preCheck($quote){
        $service= new CartService(true);
        $service->setItems( unserialize($quote->cart_items) );
        if ($service->checkItems() === false)
            return false;

        $customer = Customer::find($quote->customer_id);

        $points_current = 0;
        if($customer_points = $customer->points){
            $points_current = $customer_points->points_current;
        }

        // if used points is 0 , it must pass( when $points_current < 0 )
        if ($quote->used_points != 0 and $quote->used_points > $points_current)
        {
            return false;
        }

        if(!$this->existCoupon($quote)){
            return false;
        }
        return true;

    }
    /**
     * Convert Quote content to order and save into database.
     * @return Order
     */
    public function convert()
    {
        $quote = $this->quote;
        if (!$this->preCheck($quote)){
            Log::warning('Quote ID : '.$quote->id . ' 之結帳過程發生錯誤!');
            abort(500 , '結帳過程發生錯誤');
        }

        $order = new Order();
        \DB::transaction(function () use ($quote, $order) {
            $request = unserialize($quote->request);
            $customer = Customer::find($quote->customer_id);
            $coupon = $quote->coupon;
            $cart_items = unserialize($quote->cart_items);

            $char = substr(strtoupper($customer->role->name), 0, 1);
            $sequence_name = 'ORDER-' . $char;
            $sequence = Sequence::onWriteConnection()->where('name', $sequence_name)->first();
            if (!$sequence) {
                $sequence = new Sequence();
                $sequence->name = $sequence_name;
                $sequence->value = 1;
            } else if (strtotime(date("Y-m-d") . ' 00:00:00') >= strtotime($sequence->updated_at)) {
                $sequence->value = 121;
            }

            $order_number = 'WT' . (new \DateTime)->format("ymd") . $char . str_pad($sequence->value, 4, "0", STR_PAD_LEFT);;
            $sequence->value += 1;
            $sequence->save();
            $sequence->touch();
            $address = new Address();
            $address->customer_id = $customer->id;
            $address->is_default = 0;
            $address->zipcode = $request->get('zipcode', '');
            $address->county = $request->get('county', '');
            $address->district = $request->get('district', '');
            $address->address = $request->get('street', '');
            $address->shipping_time = $request->get('shipping_time', 'anytime');
            $address->shipping_comment = $request->get('shipping_comment', '');
            $address->firstname = $request->get('name');
            $address->lastname = '';
            if ($request->get('vat-require')) {
                $address->company = $request->get('company');
                $address->vat_number = $request->get('vat_number');
                $default_address = Address::where('customer_id', $customer->id)->where('is_default', 1)->first();
                if ($default_address) {
                    $default_address->company = $address->company;
                    $default_address->vat_number = $address->vat_number;
                    $default_address->save();
                }
            }
            $address->phone = $request->get('phone');
            $address->mobile = $request->get('mobile');
            $address->save();
            /* ------------------------- */

            $status = Status::where('id', '1')->firstOrFail();

            $order->customer_id = $customer->id;
            $order->address_id = $address->id;
            $order->comment = $request->get('comment');
            $order->shop_id = $request->get('shop_id', 1);
            $order->status_id = $status->id;

            if ($coupon) {
                $order->coupon_code = $coupon->code;
                $order->coupon_name = $coupon->name;
                $order->coupon_uuid = $coupon->uuid;
                $order->coupon_amount = $quote->coupon_amount;
            } else {
                $order->coupon_code = '';
                $order->coupon_name = '';
                $order->coupon_amount = 0;
            }

            $order->total_qty = $cart_items->sum('quantity');
            $order->shipping_method = $quote->shipping_method;
            $order->payment_method = $quote->payment_method;
            $order->shipping_amount = $quote->shipping_amount;
            $order->payment_amount = $quote->payment_amount;
            $order->rewardpoints_amount = $quote->used_points;

            $order->grand_total = $quote->cart_subtotal;
            $order->subtotal = $quote->subtotal;
            $order->increment_id = $order_number;
            $order->remote_addr = request()->server('REMOTE_ADDR');
            $order->points_current = $quote->received_points;

            $order->celebration_description = $quote->celebration_description;
            $order->celebration_amount = $quote->celebration_amount;

            $order->save();

            $orderAccessor = new OrderAccessor($order);
            $orderAccessor->changeStatus($status, $order->comment);


            $customerGifts = GiftService::getCustomerGiftsByStep('checkout-finish');
            $products = Product::whereIn('id', $cart_items->keys())->get();
            foreach ($cart_items as $id => $item) {
                $product = $products->first(function($product) use($id){
                    return $product->id == $id;
                });
                $product = new ProductAccessor($product);
                if ($product->type == ProductType::GROUPBUY) {
                    $has_groupbuy_item = true;
                }
                $targetCustomerGifts = $customerGifts->filter(function($customerGift) use($id){
                    return $customerGift->product_id == $id;
                });
                if(count($targetCustomerGifts)){
                    $customerGift = $targetCustomerGifts->first(function($customerGift){
                        return $customerGift->status == GiftService::IN_CART;
                    });
                    if($customerGift){
                        GiftService::changeCustomerGiftStatus(GiftService::CHECKOUT, $customer->id, $customerGift->id);
                    }else{
                        // change gift status by customer after checkout
                        $order->total_qty = $order->total_qty - 1;
                        $order->save();
                        continue;
                    }
                }
                $orderItem = new Item();
                $orderItem->status_id = 1;
                $orderItem->customer_id = $customer->id;
                $orderItem->role_id = $customer->role_id;
                $orderItem->order_id = $order->id;
                $orderItem->product_id = $product->id;
                $orderItem->sku = $product->sku;
                $orderItem->product_type = $product->type;
                $orderItem->model_number = $product->model_number;
                $orderItem->main_product_id = $product->getMainProduct()->id;
                $orderItem->manufacturer_id = $product->manufacturer_id;
                $orderItem->quantity = $item->quantity;
                $orderItem->cache_id = $item->cache_id;
                $orderItem->cost = $product->cost;
                $priceName = '';
                $orderItem->price = $product->getCartPrice($customer, $priceName);
                if($product->getCartPoint($customer) === false) {
                    $orderItem->point = 0;
                }else{
                    $orderItem->point = $product->getCartPoint($customer);
                }

                // $serialize = new \stdClass();
                // $serialize->priceName = $priceName;
                // $orderItem->product_serialized = \serialize($serialize);
                $orderItem->save();
            }

            if(hasChangedInvoice()){
                $ecpay = new Order\Ecpay;
                $ecpay->order_id = $order->id;
                $ecpay->vat_number = $request->get('vat_number') ? $request->get('vat_number') : null;
                $ecpay->donat_code = $request->get('donat_code') ? $request->get('donat_code') : null;
                $ecpay->email = $request->get('final_email') ? $request->get('final_email') : $customer->email;
                $ecpay->print = $request->get('vat_number') ? 1 : 0;
                $ecpay->carruer_type = $request->get('vat_number') ? 0 : 1;
                $ecpay->carruer_num = null;
                $ecpay->save();
            }

            $quote->order_id = $order->id;
            $quote->save();
            if ($coupon){
                $coupon->order_id = $order->id;
                $coupon->save();
            }



            Cart::where('customer_id',$customer->id)->delete();
        });

        //LOAD Order detail information
        $order = Order::onWriteConnection()->with(['items'=>function($query){$query->useWritePdo();} , 'items.details'=>function($query){$query->useWritePdo();} ,'customer','address'])->find($order->id);

        //SEND MAIL
        try{
            $wmail = new \Ecommerce\Core\Wmail();
            $wmail->order($order);
        }catch (Exception $e){
            \Log::warning('Order '.$order->increment_id.' Mail Fail:('.$e->getLine() .')'.$e->getMessage() . "\n" . $e->getTraceAsString());
        }

        try{
            $trackDataService = new TrackDataService();
            $trackData = $trackDataService->getCheckoutTrackingData($order);
            session()->flash('trackData', $trackData);
        }catch (Exception $e){
            Log::warning('trackData is dead' . $order->increment_id);
        }

        return $order;

    }

    /**
     * Check if include groupbuy item.
     * @param $quote
     * @return bool
     */
    public static function includeGroupbuyItem($quote)
    {
        $cart_items = unserialize($quote->cart_items);
        foreach ($cart_items as $id => $item) {
            if ($cart_items->type == ProductType::GROUPBUY) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if include outlet item.
     * @param $quote
     * @return bool
     */
    public static function includeOutletItem($quote)
    {
        $cart_items = unserialize($quote->cart_items);
        foreach ($cart_items as $id => $item) {
            if ($cart_items->type == ProductType::OUTLET) {
                return true;
            }
        }
        return false;
    }

    public function existCoupon($quote)
    {
        if ($quote->used_coupon_id){
            $coupon = Coupon::where('id' , $quote->used_coupon_id)
                ->where('customer_id', $quote->customer_id)
                ->where('expired_at', '>', date('Y-m-d'))
                ->whereNull('order_id')
                ->first();
            if (!$coupon){
                return false;
            }
        }
        return true;
    }
}