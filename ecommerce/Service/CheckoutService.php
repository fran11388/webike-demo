<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Service\Quote\Exception AS QuoteException;
use Ecommerce\Shopping\Payment\PaymentManager;
use Everglory\Models\Customer;
use Everglory\Models\Order;
use Everglory\Models\Sales\Quote;
use Everglory\Models\Sales\Shipping;

/**
 * Class CheckoutService
 * @package Ecommerce\Service
 */
class CheckoutService extends BaseService
{
    /**
     * @var Customer $request
     **/
    private $customer;
    /**
     * @var Quote $quote
     */
    private $quote;

    /**
     * It must exist quote before checkout.
     * CheckoutService constructor.
     */
    public function __construct()
    {
        $this->customer = \Auth::user();

        $this->quote = Quote::where('customer_id' , $this->customer->id)
            ->where('code' , request()->get('protect-code'))
            ->orderBy('id','desc')
            ->first();
        if (!$this->quote){
            $current_error = '找不到有效到報價資料(' . request()->get('protect-code') . ')';
            \Log::error($current_error);
            abort(500,$current_error);
        }

    }


    /**
     * @return QuoteException|Order|Quote
     */
    public function process()
    {
        $quote = $this->updateQuote($this->quote);

        $quoteService = new QuoteService($quote->id);
        if(!$quoteService->existCoupon($quote)){
            $quoteException = new QuoteException();
            $quoteException->redirect = route('checkout');
            $quoteException->message = ['您選擇的 Coupon 已經過期。'];
            return $quoteException;
        }

        if ( strpos(request()->get('payment-method') , 'credit-card') !== false ){
            return $quote;
        } else {

            return $quoteService->convert();
        }

    }


    /**
     * Update shipping and payment information to quote.
     * @param Quote $quote
     * @return quote
     */
    private function updateQuote($quote )
    {
        $shippingMethod = $this->getShippingMethod();
        $paymentMethod = $this->getPaymentMethod();
        $paymentMethod->setInstall(request()->get('install' ,0 ));
        $payment_amount = $paymentMethod->getFee();

        if (!$shippingMethod or !$paymentMethod){
            $current_error = '請選擇有效的配送方式及付款方式';
            \Log::error($current_error);
            abort(500,$current_error);
        }

        if ($payment_amount === false){
            $current_error = '無法使用的付款方式 : ' . $paymentMethod->name;
            \Log::error($current_error);
            abort(500,$current_error);
        }

        //$quote->subtotal = $quote->cart_subtotal - $quote->discount_total + $payment_amount + $quote->shipping_amount;

        $quote->subtotal = $quote->cart_subtotal - $quote->discount_total + $payment_amount + $quote->shipping_amount - intval($quote->celebration_amount);

        $quote->shipping_method = $shippingMethod->id;
        $quote->payment_method = $paymentMethod->getId();
        $quote->payment_amount = $payment_amount;
        $quote->install = $paymentMethod->getInstall();
        $quote->request = serialize(request()->request);
        $quote->save();
        return $quote;
    }

    /**
     * Get redirect url.
     * @return string
     */
    public function getRedirectUrl()
    {
        if ( strpos(request()->get('payment-method') , 'credit-card') !== false ){
            return route('checkout-payment');
        } else {
            return route('checkout-done');
        }
    }

    /**
     * Get shipping method.
     * @return Shipping|null
     */
    private function getShippingMethod()
    {
        $method = Shipping::where('code', request()->get('shipping-method'))->first();
        return $method;
    }

    /**
     * Get Payment method
     * @return \Ecommerce\Shopping\Payment\CorePayment
     */
    private function getPaymentMethod()
    {
        return PaymentManager::getPaymentMethod(request()->get('payment-method'));
    }

}