<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/8/20
 * Time: 上午 11:31
 */

namespace Ecommerce\Service\Backend;

use Ecommerce\Service\BaseService;
use Ecommerce\Core\Image;
class GuessService extends BaseService
{

    public function fileUpload($file, $date_name = NULL, $path = NULL)
    {
        $error=[];
        $default_path = '/assets/images/user_upload/WebikeGuess/';
        $final_path = public_path() . $default_path;

        if ($path) {
            $default_path = $path;
            $final_path = public_path() . $path;
        }

        $extension = $file->getClientOriginalExtension();
        $file_name = date("Y-m-d") . "Random" . str_random(5) . '.' . $extension;

        if ($date_name) {
            $file_name = $date_name . '.' . $extension;
        }

        if ($file) {

            $file_type = $file->getMimeType();
            if ($file->getSize() > 2 * 1024 * 1024) {
                $error[] = '上傳檔案需小於 2 MB';
            }
            if ($file_type != 'image/jpeg' and $file_type != 'image/png' and $file_type != 'image/gif') {
                $error[] = '僅允許上傳 jpg, png, gif 格式。';
            }

            $upload_success = $file->move($final_path, $file_name);

            $image=new Image;
            $image->asset($final_path . $file_name, $file_type, $file_name, 'images/user_upload/WebikeGuess/');
            echo "img upload success!";
            // return $path.$file_name;
            return 'http://img.webike.tw' . $default_path . $file_name;
        } else {
            echo "img upload failed!";
            die();
        }
    }

    public function getList($fillters){
        $guesses = \Everglory\Models\Guess::select('guess.*', 'products.name AS product')->leftJoin('eg_product.products', 'products.id', '=', 'guess.product_id')->orderBy('date', 'DESC');

        $limit = 20;

        foreach ($fillters as $key => $fillter) {
            if ($fillter) {
                switch ($key) {
                    case 'page':
                        break;
                    case 'limit':
                        $limit = $fillter;
                        break;
                    case 'product_type':
                        if ($fillter == 1) {
                            $guesses = $guesses->whereNotNull('product_id');
                        } elseif ($fillter == 2) {
                            $guesses = $guesses->whereNull('product_id');
                        }
                        break;
                    case 'date':
                        $min_date = $max_date = '';
                        if (isset($fillter[0]) and $fillter[0]) {
                            $min_date = $fillter[0];
                        }
                        if (isset($fillter[1]) and $fillter[1]) {
                            $max_date = $fillter[1];
                        }
                        if ($min_date and $max_date) {
                            $guesses = $guesses->where('guess.date', '<=', date('Y-m-d', strtotime($max_date . "+1 days")));
                            $guesses = $guesses->where('guess.date', '>=', $min_date);
                        }
                        break;
                    case 'product':
                        $guesses = $guesses->where('products.name', 'like', '%' . $fillter . '%');
                        break;
                    case 'created_at':
                        $min_created = $max_created = '';
                        if (isset($fillter[0]) and $fillter[0]) {
                            $min_created = $fillter[0];
                            $guesses = $guesses->where('guess.' . $key, '>=', $min_created);
                        }
                        if (isset($fillter[1]) and $fillter[1]) {
                            $max_created = $fillter[1];
                            $guesses = $guesses->where('guess.' . $key, '<=', date('Y-m-d', strtotime($max_created . "+1 days")));
                        }
                        break;
                    default:
                        dd('unknow col ' . $key);
                        break;
                }
            }
        }
        //dd($guesses->toSql());
        //dd(count($guesses));
        $guesses = $guesses->paginate($limit);
        return $guesses;
    }

    public function delete($id_group){
        foreach ($id_group as $key => $id) {
            $guess = \Everglory\Models\Guess::where('id', $id)->first();
            $mission = \Everglory\Models\Mission::where('id', $guess->mission_id)->first();
            if ($mission) {
                \Everglory\Models\Mission::where('id', $guess->mission_id)->delete();
            }
            if ($guess) {
                \Everglory\Models\Guess::where('id', $id)->delete();
            }
        }
    }

    public function getGuessById($guess_id = NULL){
        $guess = null;
        if ($guess_id!=null) {
            $guess = \Everglory\Models\Guess::where('id', $guess_id)->first();
        }
        return $guess;
    }

    public function getMissionById($mission_id=null){
        $mission = null;
        if($mission_id!=null){
            $mission = \Everglory\Models\Mission::where('id', $mission_id)->first();
        }
        return $mission;

    }

    public function csvOutput( $data, $title )
    {
        $main = "\"";
        $main = $main.implode( "\",\"", $title );
        $main .= "\"\n";
        foreach ($data as $num => $item) {
            $customer = $item->customer;
            $main = $main."\"{$customer->id}\",\"{$customer->realname}\",\"{$customer->email}\",\"{$item->created_at}\",\"{$item->answer}\"\n";
        }

        //dd($main);
        return $main;
    }

    public function getLogData($fillters){
        $missions = \Everglory\Models\Mission::where('category', 'WebikeGuess')->orWhere('category', 'WebikeWanted')->pluck('id');
        $main_table = 'customer_mission';
        $completes = \Everglory\Models\Customer\Mission::with('customer', 'mission')->whereIn('mission_id', $missions);
        $attends = \Everglory\Models\Customer\Guess::with(['customer']);
        //dd($customers->toSql());
        $errors=[];
        $limit = 20;
        if( $fillters ){

            foreach ($fillters as $key => $fillter) {
                if( $fillter ){
                    switch ($key) {
                        case 'page':
                        case 'csv_output':
                        case 'currently':
                            break;
                        case 'limit':
                            $limit = $fillter;
                            break;
                        case 'id':
                        case 'email':
                        case 'realname':
                            $attends = $attends->whereHas('customer', function($query) use($key, $fillter){
                                $query->where($key, 'like', '%'.$fillter.'%');
                            });
                            $completes = $completes->whereHas('customer', function($query) use($key, $fillter){
                                $query->where($key, 'like', '%'.$fillter.'%');
                            });
                            break;
                        case 'created_at':
                            if( isset($fillter[0]) and $fillter[0] ){
                                $completes = $completes->where($key, '>=', $fillter[0]);
                                $attends = $attends->where($key, '>=', $fillter[0]);
                            }
                            if( isset($fillter[1]) and $fillter[1] ){
                                $completes = $completes->where($key, '>=', $fillter[0]);
                                $attends = $attends->where($key, '<=', date('Y-m-d', strtotime( $fillter[1] . "+1 days")));
                            }
                            break;
                        default:
                            $errors[] = '無此項搜尋條件。';
                            break;
                    }
                }
            }
        }

        return [
            'limit'=>$limit,
            'attends'=>$attends,
            'completes'=>$completes,
            'errors'=>$errors
        ];
    }

    public function createOrUpdateGuess($request ){
        $guess_id = $request->get('id');
        $type_id = $request->get('type_id');
        $update_data = $request->all();
        $update = false;
        $errors=[];
        $result=[];

        $guess = $this->getGuessById($guess_id);
        if (!$guess) {
            if ($type_id == 1) {
                if (!$request->hasFile('before_img') or !$request->hasFile('after_img')) {
                    $errors[] = '尚未上傳圖片';
                    $result['errors']=$errors;
                    return $result;
                }
            } else if ($type_id == 2) {
                if (!$request->hasFile('before_img') or !$request->hasFile('after_img') or !$request->hasFile('ans_img')) {
                    $errors[] = '尚未上傳圖片';
                    $result['errors']=$errors;
                    return $result;
                }
            } else if ($type_id == 3) {
                if (!$request->hasFile('before_img')) {
                    $errors[] = '尚未上傳圖片';
                    $result['errors']=$errors;
                    return $result;
                }
            }
            $guess = new \Everglory\Models\Guess;
        } else {
            $update = true;
        }

        $type_name = 'WebikeGuess';
        if ($request->get('type_id') == 2) {
            $type_name = 'WebikeWanted';
        } else if ($request->get('type_id') == 3) {
            $type_name = 'WebikeQuiz';
        }

        if ($guess->mission_id) {
//            $mission = \Everglory\Models\Mission::where('id', $guess->mission_id)->first();
            $mission=$this->getMissionById($guess->mission_id);
            if ($mission) {
                $mission->name = $update_data['date'] . ' ' . $type_name . ' 答題正確';
                $mission->category = $type_name;
                $mission->note = '單日 ' . $type_name . ' 答題正確';
            }
        } else {
            $mission = new \Everglory\Models\Mission;
            $mission->name = $update_data['date'] . ' ' . $type_name . ' 答題正確';
            $mission->category = $type_name;
            $mission->note = '單日 ' . $type_name . ' 答題正確';
            $mission->save();
        }
        $mission_id = $mission->id;

        foreach ($update_data as $key => $value) {
            if ($value) {
                switch ($key) {
                    case 'answer':
                    case 'genuine_maker':
                    case 'genuine_name':
                    case 'description':
                    case 'type_id':
                        $guess->$key = $value;
                        break;
                    case 'date':
                        $errorCount = 0;
                        if ($guess->date != $value) {
                            if ($type_id == 1) {
                                if (!$request->hasFile('before_img') or !$request->hasFile('after_img')) {
                                    $errorCount++;
                                }
                            } else if ($type_id == 2) {
                                if (!$request->hasFile('before_img') or !$request->hasFile('after_img') or !$request->hasFile('ans_img')) {
                                    $errorCount++;
                                }
                            } else if ($type_id == 3) {
                                if (!$request->hasFile('before_img')) {
                                    $errorCount++;
                                }
                            }

                            if ($errorCount > 0) {
                                $errors[] = '修改日期必須重新上傳圖片';
                                $result['errors']=$errors;
                                return $result;
                            }
                        }
                        $guess->date = $value;
                        break;
                    case 'sku':
                        $product = \Everglory\Models\Product::where('url_rewrite', $value)->first();
                        $guess->product_id = $product->id;
                        break;
                    case 'before_img':
                        $file = $request->file($key);
                        $filename = $update_data['date'] . '_before';
                        $url = $this->fileUpload($file, $filename);
                        $guess->$key = $url;
                        break;
                    case 'after_img':
                        $file = $request->file($key);
                        $filename = $update_data['date'] . '_after';
                        $url = $this->fileUpload($file, $filename);
                        $guess->$key = $url;
                        break;
                    case 'ans_img':
                        $file = $request->file($key);
                        $filename = $update_data['date'] . '_ans';
                        $url = $this->fileUpload($file, $filename);
                        $guess->$key = $url;
                        break;
                    default:
                        # code...
                        break;
                }

            }
        }
        $guess->mission_id = $mission_id;
        $mission->save();
        $guess->save();

        $result['update']=$update;
        $result['guess']=$guess;
        $result['errors']=$errors;

        return $result;
    }


}