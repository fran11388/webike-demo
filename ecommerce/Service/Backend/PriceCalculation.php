<?php namespace Ecommerce\Service\Backend;

class PriceCalculation
{
    protected $result;
    protected $role_id;
    protected $price_general;
    protected $price;
    protected $rate_code;
    protected $old;
    protected $discount;
    protected $type = 1;
    protected $now;


    public function __construct()
    {
        $this->result = new \stdClass();
        if ( !\Auth::check() ){
            $this->role_id = 0;
        }else{
            $this->role_id = \Auth::user()->role_id;
        }

        $this->rate_code = 'JPY';
        $this->now = strtotime(date('Y-m-d H:i:s'));
    }

    private function setPrice( $price )
    {
        if( gettype($price) == 'object'){
            //Product Eloquent
            $this->price = $price->finalprice;
        }else{
            //string
            $this->price = $price;
        }
    }

    public function setCustomer( $customer ){
        if( gettype($customer) == 'object'){
            $this->role_id = $customer->role_id;
        }else{
            $this->role_id = $customer;
        }

        return $this;
    }

    public function setRate( $rate_code )
    {
        $this->rate_code = $rate_code;
        return $this;
    }

    public function setDiscount( $discount )
    {
        $this->discount = $discount;
        return $this;
    }

    public function getGenuinePart( $price , $manufacturer ,$old = null,$type = 1)
    {
        $this->setPrice($price);
        $this->type = $type;
        if($type == 1){
            // if($manufacturer == 758 or $manufacturer == 'DUCATI'){
            //     $this->price = $this->price * 1.1;
            // }else{
            if( $this->now >= strtotime('2016-06-01') ){
                switch ($manufacturer) {
                    case 'HONDA':
                    case '4':
                    case 'YAMAHA':
                    case '6':
                    case 'SUZUKI':
                    case '339':
                        if($this->now >= strtotime('2016-07-01')){
                            $this->price = $this->price * 0.87;
                        }else{
                            $this->price = $this->price * 0.88;
                        }

                        break;
                    case 'KAWASAKI':
                    case '187':
                        $this->price = $this->price * 0.85;
                        break;
                    case 'HARLEY-DAVIDSON':
                    case '163':
                        $this->price = $this->price * 0.9;
                        break;
                    case 'HUSQVARNA':
                    case '1923':
                    case 'BMW':
                    case '1915':
                        $this->price = $this->price * 1;
                        break;
                    default:
                        $this->price = $this->price * 1;
                        break;
                }
            }else{
                $this->price = $this->price * 0.89;
            }
            // }
        }else if($type == 2){
            switch ($manufacturer) {
                case 'SYM':
                case '2044':
                    $this->price = $this->price / 0.95 * 1.05;
                    break;
                case 'KYMCO':
                case '2005':
                    $this->price = $this->price / (1 - 0) * 1.05;
                    break;
                case 'TW_YAMAHA':
                case '2045':
                    $this->price = $this->price / (1 - 0) * 1.05;
                    break;
                case 'TW_SUZUKI':
                case '2046':
                case 'AEON':
                case '2047':
//                    $this->price = $this->price * 1.15;
                    //20160825 change to new supplier
                    $this->price = $this->price / (1 - 0) * 1.05;
                    break;
                default:
                    $this->price = $this->price * 1.3;
                    break;
            }
        }

        if($old){
            $this->old = true;
        }

        $this->flow($manufacturer);

        return $this->result;
    }

    public function getMitumori( $price )
    {

        $this->setPrice($price);
        $this->flow();
        return $this->result;
    }

    public function getGroupBuy( $price )
    {
        $this->setPrice($price);
        $this->flow();
        return $this->result;
    }

    public function flow($manufacturer = null)
    {
        //Only JPY Need run weight
        if($this->rate_code == 'JPY'){
            $this->weight();
        }

        $this->price_general = $this->price;

        $this->rate();
        $this->discount($manufacturer);

        $this->price_general = round($this->price_general);
        $this->price = round($this->price);

        $this->result->price_general = $this->price_general;
        $this->result->price_final = $this->price;

    }

    public function weight()
    {
        if( $this->now >= strtotime('2016-07-01')){
            if( $this->price >= 0 && $this->price < 300){
                $this->price = $this->price * 2.2;
            }else if ($this->price >= 300 && $this->price < 500){
                $this->price = $this->price * 1.95;
            }else if ($this->price >= 500 && $this->price < 1000){
                $this->price = $this->price * 1.85;
            }else if ($this->price >= 1000 && $this->price < 2000){
                $this->price = $this->price * 1.8;
            }else if ($this->price >= 2000 && $this->price < 3000){
                $this->price = $this->price * 1.73;
            }else if ($this->price >= 3000 && $this->price < 5000){
                $this->price = $this->price * 1.66;
            }else if ($this->price >= 5000 && $this->price < 7000){
                $this->price = $this->price * 1.59;
            }else if ($this->price >= 7000 && $this->price < 10000){
                $this->price = $this->price * 1.53;
            }else if ($this->price >= 10000 && $this->price < 20000){
                $this->price = $this->price * 1.47;
            }else if ($this->price >= 20000 && $this->price < 30000){
                $this->price = $this->price * 1.43;
            }else if ($this->price >= 30000 && $this->price < 40000){
                $this->price = $this->price * 1.39;
            }else if ($this->price >= 40000 && $this->price < 50000){
                $this->price = $this->price * 1.375;
            }else if ($this->price >= 50000 && $this->price < 100000){
                $this->price = $this->price * 1.36;
            }else if ($this->price >= 100000 && $this->price < 150000){
                $this->price = $this->price * 1.34;
            }else if($this->price >= 150000){
                $this->price = $this->price * 1.33;
            }
        }else{
            if( $this->price >= 0 && $this->price < 300){
                $this->price = $this->price * 2.2;
            }else if ($this->price >= 300 && $this->price < 500){
                $this->price = $this->price * 1.95;
            }else if ($this->price >= 500 && $this->price < 1000){
                $this->price = $this->price * 1.85;
            }else if ($this->price >= 1000 && $this->price < 2000){
                $this->price = $this->price * 1.8;
            }else if ($this->price >= 2000 && $this->price < 3000){
                $this->price = $this->price * 1.73;
            }else if ($this->price >= 3000 && $this->price < 5000){
                $this->price = $this->price * 1.66;
            }else if ($this->price >= 5000 && $this->price < 7000){
                $this->price = $this->price * 1.59;
            }else if ($this->price >= 7000 && $this->price < 10000){
                $this->price = $this->price * 1.53;
            }else if ($this->price >= 10000 && $this->price < 20000){
                $this->price = $this->price * 1.47;
            }else if ($this->price >= 20000 && $this->price < 30000){
                $this->price = $this->price * 1.43;
            }else if ($this->price >= 30000 && $this->price < 40000){
                $this->price = $this->price * 1.39;
            }else if ($this->price >= 40000 && $this->price < 50000){
                $this->price = $this->price * 1.37;
            }else if ($this->price >= 50000 && $this->price < 100000){
                $this->price = $this->price * 1.35;
            }else if ($this->price >= 100000 && $this->price < 150000){
                $this->price = $this->price * 1.33;
            }else if($this->price >= 150000){
                $this->price = $this->price * 1.32;
            }
        }
    }

    public function discount($manufacturer = null)
    {
        if($this->role_id == 3 or $this->role_id == 4){
            if($this->type == 2){
                switch ($manufacturer) {
                    case 'KYMCO':
                    case '2005':
                    case 'TW_YAMAHA':
                    case '2045':
                        $this->price = $this->price;
                        break;
                    default:
                        $this->price = $this->price  * 0.95;
                        break;
                }
            }else{
                if( $this->now >= strtotime('2016-10-01')){
                    if( $this->price >= 0 && $this->price < 227){
                        $this->price = $this->price * 0.74;
                    }else if ($this->price >= 227 && $this->price < 336){
                        $this->price = $this->price * 0.74;
                    }else if ($this->price >= 336 && $this->price < 638){
                        $this->price = $this->price * 0.74;
                    }else if ($this->price >= 638 && $this->price < 1241){
                        $this->price = $this->price * 0.75;
                    }else if ($this->price >= 1241 && $this->price < 1790){
                        $this->price = $this->price * 0.76;
                    }else if ($this->price >= 1790 && $this->price < 2863){
                        $this->price = $this->price * 0.77;
                    }else if ($this->price >= 2863 && $this->price < 3839){
                        $this->price = $this->price * 0.79;
                    }else if ($this->price >= 3839 && $this->price < 5278){
                        $this->price = $this->price * 0.81;
                    }else if ($this->price >= 5278 && $this->price < 10142){
                        $this->price = $this->price * 0.83;
                    }else if ($this->price >= 10142 && $this->price < 14800){
                        $this->price = $this->price * 0.85;
                    }else if ($this->price >= 14800 && $this->price < 19182){
                        $this->price = $this->price * 0.87;
                    }else if ($this->price >= 19182 && $this->price < 23632){
                        $this->price = $this->price * 0.87;
                    }else if ($this->price >= 23632 && $this->price < 46575){
                        $this->price = $this->price * 0.88;
                    }else if ($this->price >= 46575 && $this->price < 68827){
                        $this->price = $this->price * 0.88;
                    }else if($this->price >= 68827){
                        $this->price = $this->price * 0.88;
                    }
                }else{
                    if( $this->price >= 0 && $this->price < 227){
                        $this->price = $this->price * 0.75;
                    }else if ($this->price >= 227 && $this->price < 336){
                        $this->price = $this->price * 0.75;
                    }else if ($this->price >= 336 && $this->price < 638){
                        $this->price = $this->price * 0.75;
                    }else if ($this->price >= 638 && $this->price < 1241){
                        $this->price = $this->price * 0.76;
                    }else if ($this->price >= 1241 && $this->price < 1790){
                        $this->price = $this->price * 0.77;
                    }else if ($this->price >= 1790 && $this->price < 2863){
                        $this->price = $this->price * 0.78;
                    }else if ($this->price >= 2863 && $this->price < 3839){
                        $this->price = $this->price * 0.80;
                    }else if ($this->price >= 3839 && $this->price < 5278){
                        $this->price = $this->price * 0.82;
                    }else if ($this->price >= 5278 && $this->price < 10142){
                        $this->price = $this->price * 0.84;
                    }else if ($this->price >= 10142 && $this->price < 14800){
                        $this->price = $this->price * 0.86;
                    }else if ($this->price >= 14800 && $this->price < 19182){
                        $this->price = $this->price * 0.88;
                    }else if ($this->price >= 19182 && $this->price < 23632){
                        $this->price = $this->price * 0.88;
                    }else if ($this->price >= 23632 && $this->price < 46575){
                        $this->price = $this->price * 0.89;
                    }else if ($this->price >= 46575 && $this->price < 68827){
                        $this->price = $this->price * 0.89;
                    }else if($this->price >= 68827){
                        $this->price = $this->price * 0.89;
                    }
                }

            }
        }

        if($this->discount){
            $this->price_general = $this->price_general * $this->discount;
            $this->price = $this->price * $this->discount;
        }
    }

    public function rate()
    {
        if($this->old){
            $this->price_general = round($this->price_general * 0.34);
            $this->price = round($this->price * 0.34);
        }else{
            $this->price_general = $this->price_general * $this->getRate($this->rate_code);
            $this->price = $this->price * $this->getRate($this->rate_code);
        }

    }

    private function getRate($country)
    {

        if ($country == 'JPY') {
            $rate =  \DB::connection('smax_warehouse')->table('core_config')->where('title','=','exchange_rate_JPY')->first()->value;
        } else {
            $rate = 1;
        }

        return $rate;

    }
}