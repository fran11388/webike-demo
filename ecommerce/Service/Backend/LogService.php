<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/8/27
 * Time: 下午 05:02
 */

namespace Ecommerce\Service\Backend;
use Ecommerce\Service\BaseService;

class LogService extends BaseService
{
    public function getSalesLog($filter){
        $order_items = \Everglory\Models\Order\Item::select(\DB::raw('order_items.*,oid.value as product_option'))->with('order', 'order.status', 'order.customer', 'order.customer.role', 'purchases.supplier', 'product', 'manufacturer')
            ->join('order_item_details as oid', function ($query) {
                $query->on('order_items.id', '=', 'oid.item_id')
                    ->where('oid.attribute', '=', 'product_options');
            });

        $limit = 20;

        foreach ($filter as $key => $filter) {
            if ($filter) {
                switch ($key) {
                    case 'page':
                        break;
                    case 'limit':
                        $limit = $filter;
                        break;
                    case 'price':
                        if (isset($filter[0]) and $filter[0]) {
                            $order_items = $order_items->where('price', '>=', $filter[0]);
                        }
                        if (isset($filter[1]) and $filter[1]) {
                            $order_items = $order_items->where('price', '<=', $filter[1]);
                        }
                        break;
                    case 'created_at':
                        if (isset($filter[0]) and $filter[0]) {
                            $order_items = $order_items->where('order_items.created_at', '>=', $filter[0]);
                        }
                        if (isset($filter[1]) and $filter[1]) {
                            $order_items = $order_items->where('order_items.created_at', '<=', $filter[1]);
                        }
                        break;
                    case 'attribute_tag':
                        $order_items = $order_items->join('eg_product.products', 'order_items.product_id', '=', 'products.id')->where($key, 'like', '%' . $filter . '%');
                        break;
                    case 'increment_id':
                        $order_items = $order_items->whereHas('order', function ($query) use ($key, $filter) {
                            $query->where($key, 'like', '%' . $filter . '%');
                        });
                        break;
                    case 'status_id':
                        $order_items = $order_items->whereHas('order', function ($query) use ($key, $filter) {
                            $query->where($key, $filter);
                        });
                        break;
                    case 'email':
                    case 'realname':
                        $order_items = $order_items->whereHas('order.customer', function ($query) use ($key, $filter) {
                            $query->where($key, 'like', '%' . $filter . '%');
                        });
                        break;
                    case 'motors':
                        $key = 'motor_names';
                    case 'models':
                    case 'category_url_path':
                    case 'product_name':
                    case 'manufacturer_name':
                        $order_items = $order_items->whereHas('details', function ($query) use ($key, $filter) {
                            $query
                                ->where('attribute', $key)
                                ->where('value', 'like', '%' . $filter . '%');
                        });
                        break;
                    case 'category_name_path':
                        foreach ($filter as $category_name) {
                            if ($category_name) {
                                $order_items = $order_items->whereHas('details', function ($query) use ($key, $category_name) {
                                    $query
                                        ->where('attribute', $key)
                                        ->where('value', 'like', '%' . $category_name . '%');
                                });
                            }
                        }
                        break;
                    case 'supplier_id':
                        $order_items = $order_items->whereHas('purchases', function ($query) use ($key, $filter) {
                            $query->where($key, $filter);
                        });
                        break;
                    case 'sku':
                    case 'model_number':
                        $order_items = $order_items->where($key, 'like', '%' . $filter . '%');
                        break;
                    case 'csv_output':
                        break;
                    default:
                        $order_items = $order_items->where($key, $filter);
                        break;
                }
            }
        }

        return [
            'limit'=>$limit,
            'order_items'=>$order_items
        ];
    }

    /*pretend the $item's relation*/
    public function itemDetailRelation($item){
        $itemDetails = $item->details->keyBy('attribute');
        $collection = new \stdClass();
        foreach ($itemDetails as $key => $entity) {
            $collection->$key = $entity->value;
        }
        return $collection;
    }

    public function getCreditCard($args){
        $logs = \Everglory\Models\Sales\Creditcard::with('customer')->with('order');

        $limit = 20;

        foreach ($args as $key => $value) {
            if ($value) {
                switch ($key) {
                    case 'limit':
                        $limit = $value;
                        break;
                    case 'increment_id':
                        $logs = $logs->whereHas('order', function ($query) use ($key, $value) {
                            $query->where($key, 'like', '%' . $value . '%');
                        });
                        break;
                    case 'realname':
                    case 'email':
                        $logs = $logs->whereHas('customer', function ($query) use ($key, $value) {
                            $query->where($key, 'like', '%' . $value . '%');
                        });
                        break;
                    case 'created_at':
                        if (count($value)) {
                            if (isset($value[0]) and $value[0]) {
                                $logs = $logs->where('created_at', '>=', $value[0]);
                            }
                            if (isset($value[1]) and $value[1]) {
                                $logs = $logs->where('created_at', '<=', date('Y-m-d', strtotime($value[1] . '+1 days')));
                            }
                        }

                        break;
                    case 'company':
                        if ($value == '聯信') {
                            $logs->where('ONO', 'like', '%NCCC%');
                        } elseif ($value == '玉山') {
                            $logs->where('ONO', 'not like', '%NCCC%');
                        } else {

                        }
                        break;
                    case 'ONO':
                    case 'AN':
                    case 'TA':
                        $logs = $logs->where($key, 'like', '%' . $value . '%');
                        break;

                    default:
                        break;
                }
            }
        }

        return [
            'logs'=>$logs,
            'limit'=>$limit,
        ];
    }

    public function getVirtualBankLog($filter){
        $collection = \Everglory\Models\Order\VirtualBank::with('order', 'order.customer')->where('credit', 0);

        $limit = 20;

        foreach ($filter as $key => $filter) {
            if( $filter and $filter !== '' ){
                switch ( $key ) {
                    case 'page':
                    case 'csv_output':
                        break;
                    case 'limit':
                        $limit = $filter;
                        break;
                    case 'created_at':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_date = $filter[0];
                            $collection = $collection->where($key, '>=', date('Y-m-d', strtotime( $min_date )) );
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_date = $filter[1];
                            $collection = $collection->where($key, '<=', date('Y-m-d', strtotime( $max_date . "+1 days")) );
                        }
                        break;
                    case 'increment_id':
                        $collection = $collection->whereHas('order', function($query) use($key, $filter){
                            $query->where($key, 'like', '%'.trim($filter).'%');
                        });
                        break;
                    case 'realname':
                    case 'email':
                        $collection = $collection->whereHas('order.customer', function($query) use($key, $filter){
                            $query->where($key, 'like', '%'. trim($filter).'%');
                        });
                        break;
                    case 'virtual_account':
                        $collection = $collection->where($key, 'like', '%'.$filter.'%');
                        break;
                    case 'bank_deal_date':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_date = $filter[0];
                            $collection = $collection->where($key, '>=', date('Ymd', strtotime( $min_date . "-1 days")) );
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_date = $filter[1];
                            $collection = $collection->where($key, '<=', date('Ymd', strtotime( $max_date . "+1 days")) );
                        }
                        //$collection = $collection->where($key, 'like', '%'.$filter.'%');
                        break;
                    case 'credit_amount':
                        if( isset($filter[0]) and $filter[0] ){
                            $min = $filter[0];
                            $collection = $collection->where($key, '>=', $min);
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max = $filter[1];
                            $collection = $collection->where($key, '<=', $max);
                        }

                        break;
                    default:
                        $this->errors[] = $key.'搜尋條件對應失敗。';
                        break;
                }
            }
        }

        return [
            'collection'=>$collection,
            'limit'=>$limit,
        ];


    }

    public function getGenuinePartsLog($filter){
        $genuineparts = \DB::connection('eg_zero')->table('estimate_items AS items')
            ->select('items.created_at', 'c.realname', 'c.email', \DB::raw('r.name AS role'), \DB::raw('m.name AS manufacturer'), \DB::raw('items.model_number'), \DB::raw('pp.price AS product_price'), 'items.price', 'p.status_id', 'd.text')
            ->join('eg_product.manufacturers AS m', 'm.id', '=', 'items.manufacturer_id')
            ->join('eg_product.products AS pp', 'pp.id', '=', 'items.product_id')
            ->join('customers AS c', 'c.id', '=', 'items.customer_id')
            ->join('customer_roles AS r', 'c.role_id', '=', 'r.id')
            ->join('estimates AS e', 'e.id', '=', 'items.estimate_id')
            ->join('purchases AS p', function($join){
                $join->on('p.item_id', '=', 'items.id')
                    ->where('p.item_type', '=', 'MorphEstimateItem');
            })
            ->leftJoin('delivery_days AS d', 'd.code', '=', 'p.delivery_code')
            ->where('items.product_type', 1);

        $min_date = "2013-5-1";
        $max_date = date('Y-m-d', strtotime( date('Y/m/d') . "+1 days") );
        $limit = 20;
        foreach ($filter as $key => $filter) {
            if( is_numeric($filter) or $filter ){
                switch ( $key ) {
                    case 'page':
                        break;
                    case 'limit':
                        $limit = $filter;
                        break;
                    case 'created_at':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_date = $filter[0];
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_date = $filter[1];
                        }
                        $genuineparts = $genuineparts->where('items.created_at', '<=', date('Y-m-d', strtotime( $max_date . "+1 days") ));
                        $genuineparts = $genuineparts->where('items.created_at', '>=', $min_date);
                        break;
                    case 'realname':
                    case 'email':
                        $genuineparts = $genuineparts->where('c.'.$key, 'like', '%'.$filter.'%');
                        break;
                    case 'role_id':
                        $genuineparts = $genuineparts->where('c.'.$key, $filter);
                        break;
                    case 'status':
                        if($filter == 1){
                            $genuineparts = $genuineparts->where('p.status_id', '>=', 2);
                        }else{
                            $genuineparts = $genuineparts->where('p.status_id', 1);
                        }
                        break;
                    case 'type':
                        $genuineparts = $genuineparts->where('e.type_id', $filter);
                        break;
                    case 'text':
                        $genuineparts = $genuineparts->where('d.text', 'like', '%'.$filter.'%');
                        break;
                    case 'manufacturer':
                        $genuineparts = $genuineparts->where('m.name', 'like', '%'.$filter.'%');
                        break;
                    case 'csv_output':
                        break;

                    default:
                        $genuineparts = $genuineparts->where('items.'.$key, 'like', '%'.$filter.'%');
                        break;
                }
            }
        }

        return [
            'genuineparts'=>$genuineparts,
            'limit'=>$limit,
        ];
    }

    public function getCouponsLog($filter){
        $collection = \DB::table('eg_zero.coupons')
            ->select('coupons.name', 'orders.increment_id', 'orders.id AS order_id', 'customers.realname', 'customers.email', 'role.name AS role', 'coupons.created_at', 'coupons.expired_at', 'coupons.discount', 'coupon_produce.transcode')
            ->leftJoin('eg_zero.customers', 'customers.id', '=', 'coupons.customer_id')
            ->leftJoin('eg_zero.customer_roles AS role', 'role.id', '=', 'customers.role_id')
            ->leftJoin('eg_zero.coupon_produce', 'coupon_produce.coupon_id', '=', 'coupons.id')
            ->leftJoin('eg_zero.orders', 'orders.id', '=', 'coupons.order_id');

//        $filter = \Input::all();
        $min_discount = 0;
        $max_discount = 99999999;
        $min_date = "2013-06-01";
        $max_date = date('Y-m-d', strtotime( date('Y/m/d') . "+1 year") );
        $limit = 20;
        $errors=[];
        foreach ($filter as $key => $filter) {
            if( $filter and $filter !== '' ){
                switch ( $key ) {
                    case 'page':
                        break;
                    case 'limit':
                        $limit = $filter;
                        break;
                    case 'expired_at':
                    case 'created_at':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_date = $filter[0];
                            $collection = $collection->where('coupons.'.$key, '>=', $min_date);
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_date = $filter[1];
                            $collection = $collection->where('coupons.'.$key, '<=', date('Y-m-d', strtotime( $max_date . "+1 days") ));
                        }

                        //回復預設
                        $min_date = "2013-06-01";
                        $max_date = date('Y-m-d', strtotime( date('Y/m/d') . "+1 year") );
                        break;
                    case 'realname':
                    case 'email':
                        $collection = $collection->where('customers.'.$key, 'like', '%'.$filter.'%');
                        break;
                    case 'role_id':
                        $collection = $collection->where('customers.'.$key, $filter);
                        break;
                    case 'status':
                        if( $filter == 1 ){
                            $collection = $collection->whereNotNull('coupons.order_id');
                        }else{
                            $collection = $collection->whereNull('coupons.order_id');
                        }
                        break;
                    case 'name':
                        $collection = $collection->where('coupons.'.$key, 'like', '%'.$filter.'%');
                        break;
                    case 'discount':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_discount = $filter[0];
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_discount = $filter[1];
                        }
                        $collection = $collection->where('coupons.discount', '<=', $max_discount);
                        $collection = $collection->where('coupons.discount', '>=', $min_discount);
                        break;
                    case 'csv_output':
                        break;
                    default:
                        $errors[] = $key.'搜尋條件對應失敗。';
                        break;
                }
            }
        }

        return[
            'collection'=>$collection,
            'limit'=>$limit,
            'errors'=>$errors,
        ];
    }

    public function getPointsLog($filter){
        $collection = \DB::connection('eg_zero')->table('order_rewards')
            ->select('order_rewards.description', 'orders.increment_id', 'orders.id AS order_id', 'customers.realname', 'customers.email', 'role.name AS role', 'order_rewards.start_date', 'order_rewards.end_date', 'order_rewards.points_spend', 'order_rewards.points_current')
            ->join('customers', 'customers.id', '=', 'order_rewards.customer_id')
            ->join('customer_roles AS role', 'role.id', '=', 'customers.role_id')
            ->leftJoin('orders', 'orders.id', '=', 'order_rewards.order_id');


        $min_points = 0;
        $max_points = 99999999;
        $min_date = "2013-06-01";
        $max_date = date('Y-m-d', strtotime( date('Y/m/d') . "+1 year") );
        $limit = 20;
        $errors=[];
        foreach ($filter as $key => $filter) {
            if( $filter and $filter !== '' ){
                switch ( $key ) {
                    case 'page':
                        break;
                    case 'limit':
                        $limit = $filter;
                        break;
                    case 'end_date':
                    case 'start_date':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_date = $filter[0];
                            $collection = $collection->where('order_rewards.'.$key, '>=', $min_date);
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_date = $filter[1];
                            $collection = $collection->where('order_rewards.'.$key, '<=', date('Y-m-d', strtotime( $max_date . "+1 days") ));
                        }
                        //回復預設
                        $min_date = "2013-06-01";
                        $max_date = date('Y-m-d', strtotime( date('Y/m/d') . "+1 year") );
                        break;
                    case 'realname':
                    case 'email':
                        $collection = $collection->where('customers.'.$key, 'like', '%'.$filter.'%');
                        break;
                    case 'role_id':
                        $collection = $collection->where('customers.'.$key, $filter);
                        break;
                    case 'status':
                        if( $filter == 1 ){
                            $collection = $collection->where('order_rewards.points_current', '!=', 0);
                        }else{
                            $collection = $collection->where('order_rewards.points_spend', '!=', 0);
                        }
                        break;
                    case 'description':
                        if( \Input::get('type') == 1 ){
                            $collection = $collection->where('orders.increment_id', 'like', '%'.$filter.'%');
                        }elseif( \Input::get('type') == 2 ){
                            $collection = $collection->where('order_rewards.'.$key, 'like', '%'.$filter.'%');
                        }else{
                            $collection = $collection->where(function($query) use($key, $filter)
                            {
                                $query->where('orders.increment_id', 'like', '%'.$filter.'%')
                                    ->orWhere('order_rewards.'.$key, 'like', '%'.$filter.'%');
                            });
                        }
                        break;
                    case 'points':
                        if( isset($filter[0]) and $filter[0] ){
                            $min_points = $filter[0];
                        }
                        if( isset($filter[1]) and $filter[1] ){
                            $max_points = $filter[1];
                        }
                        $collection = $collection->where(function($query) use($max_points, $min_points)
                        {
                            $query
                                ->where(function($query1) use($max_points, $min_points){
                                    $query1->where('order_rewards.points_current', '<=', $max_points)
                                        ->where('order_rewards.points_current', '>=', $min_points);
                                })
                                ->orWhere(function($query2) use($max_points, $min_points)
                                {
                                    $query2->where('order_rewards.points_spend', '<=', $max_points)
                                        ->Where('order_rewards.points_spend', '>=', $min_points);
                                });

                        });
                        // $collection = $collection->where('order_rewards.points_current', '<=', $max_points);
                        // $collection = $collection->where('order_rewards.points_current', '>=', $min_points);
                        // $collection = $collection->orWhere('order_rewards.points_spend', '<=', $max_points);
                        // $collection = $collection->orWhere('order_rewards.points_spend', '>=', $min_points);
                        break;
                    case 'csv_output':
                    case 'type':
                        break;
                    default:
                        $errors[] = $key.'搜尋條件對應失敗。';
                        break;
                }
            }
        }

        return [
            'collection'=>$collection,
            'limit'=>$limit,
            'errors'=>$errors,
        ];
    }


}