<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/7
 * Time: 上午 09:44
 */

namespace Ecommerce\Service\Backend;


use Everglory\Constants\Propaganda\Audience;
use Everglory\Constants\Propaganda\Position;
use Everglory\Models\Propaganda;
use Illuminate\Support\Collection;


class PropagandaService
{

    public static function getAvailablePropagandas()
    {
        $now = date("Y-m-d H:i:s");
        $audience_ids = self::getAudienceIds();
        $current_url = \Route::getCurrentRequest()->url();
        $path = parse_url($current_url, PHP_URL_PATH);

        $propagandas = Propaganda::with('type', 'position', 'audience')
            ->where('start_time', '<=', $now)
            ->where('end_time', '>=', $now)
            ->whereIn('audience_id', $audience_ids)
            ->whereDoesntHave('bannedPaths', function ($query) use ($path) {
                $query->where('path', '=', $path);
            })
            ->get();

        return $propagandas;
    }

    public static function filterPropagandas(Collection $propagandas, $type_id, $position_ids)
    {
        $propagandas = $propagandas->where('type.id', $type_id);
        $propagandas = $propagandas->whereIn('position.id', $position_ids);
//        if ($propagandas->count() > 1) {
//            $log=Propaganda\Log::firstOrCreate(['url' => request()->url()]);
//            $log->message='propaganda conflict! plz check the following propaganda id: ' . implode(',', $propagandas->pluck('id')->all());
//            $log->save();
//        }
        return $propagandas->first();
    }

    private static function getAudienceIds()
    {
        $customer = \Auth::user();
        switch ($customer) {
            case null:  //無登入
                return [Audience::ALL, Audience::ALL_EXCEPT_DEALER];
                break;
            case in_array($customer->role->id, [\Everglory\Constants\CustomerRole::REGISTERED,  //一般會員(含STAFF)
                \Everglory\Constants\CustomerRole::GENERAL,
                \Everglory\Constants\CustomerRole::STAFF]):
                return [Audience::ALL, Audience::NORMAL, Audience::ALL_EXCEPT_DEALER];
                break;
            case in_array($customer->role->id, [\Everglory\Constants\CustomerRole::WHOLESALE]): //經銷商
                return [Audience::ALL, Audience::DEALER];
                break;
            case in_array($customer->role->id, [\Everglory\Constants\CustomerRole::BUYER]):  //外銷
                return [Audience::ALL, Audience::ALL_EXCEPT_DEALER];
                break;
            default:
                return [Audience::ALL, Audience::ALL_EXCEPT_DEALER];

        }
    }

}