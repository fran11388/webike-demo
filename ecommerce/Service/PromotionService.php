<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Everglory\Models\Correspond;
use Cache;
use Everglory\Models\Customer\Gift;


class PromotionService extends BaseService
{
    public function __construct($creditcard)
    {
        $this->customer= \Auth::user();
    }

    public static function getCategoriesDiscount($type = null)
    {
        $categories = array(
            '改裝零件'=>array(
                '外觀零件'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/外觀零件.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1110',
                    'note' => '最大22%off',
                ),
                '底盤'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/底盤.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1311',
                    'note' => '最大21%off',
                ),
                '引擎'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/引擎.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1180',
                    'note' => '最大23%off',
                ),
                '排氣系統'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/排氣系統.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1001',
                    'note' => '最大19%off',
                ),
                '轉向系統'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/轉向系統.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1030',
                    'note' => '最大23%off',
                ),
                '車台骨架'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/車台骨架.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1059',
                    'note' => '最大26%off',
                ),
                '傳動系統'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/傳動.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1150',
                    'note' => '最大20%off',
                ),
                '煞車'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/煞車系統.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1010',
                    'note' => '最大28%off',
                )
            ),
            '騎士用品'=>array(
                '安全帽'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/安全帽.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3001',
                    'note' => '最大20%off',
                ),
                '夾克・防摔衣'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/夾克防摔衣.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3021',
                    'note' => '最大17%off',
                ),
                '騎士手套'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/手套.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3060',
                    'note' => '最大21%off',
                ),
                '騎士鞋・車靴'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/騎士鞋.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'1325',
                    'note' => '最大21%off',
                ),
                '褲子・防摔褲'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/褲子防摔褲.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3071',
                    'note' => '最大17%off',
                ),
                '護具'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/護具.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3027',
                    'note' => '最大19%off',
                ),
                '包包'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/包包.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3123',
                    'note' => '最大23%off',
                ),
                '雨衣'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/雨衣.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3040',
                    'note' => '最大18%off',
                )
            )
        );

        foreach($categories as $title => $category){
            foreach($category as $info){
                $url_rewrites[] = $info['url_rewrite'];
                $url_rewrite = implode(",", $url_rewrites);
            }
        }

        $info = '('.$url_rewrite.')';
        $cache_kay = $type . '-discount';
        if(!useCache()){
            \Cache::forget($cache_kay);
        }

        $result = \Cache::rememberForever($cache_kay,function() use($info){
            $result = \DB::connection('eg_product')->select("SELECT ct.url_rewrite,ct.name,(1-ROUND(min(ppc.price/pd.price),2))*100 as discountc,(1-ROUND(min(ppd.price/pd.price),2))*100 as discountd 
                FROM eg_product.categories as ct
                join eg_product.category_product as cp
                on ct.id = cp.category_id
                join eg_product.products as pd
                on cp.product_id = pd.id
                join eg_product.product_prices as ppc
                on pd.id = ppc.product_id and ppc.role_id = 2 and ppc.rule_date = '" . date('Y-m-d') . "'
                join eg_product.product_prices as ppd
                on pd.id = ppd.product_id and ppd.role_id = 3 and ppd.rule_date = '" . date('Y-m-d') . "'
                where pd.type = 0 
                and pd.active = 1
                and pd.is_main = 1
                and pd.parent_id = 0
                and ct.url_rewrite in $info
                GROUP BY(ct.url_rewrite);");
            return $result;
        });
        return collect($result);
    }

    public static function getCategoriesItem()
    {
        $categories = array(
            '改裝零件'=>array(
                '外觀零件'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/外觀零件.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1110',
                    'note' => '最大22%off',
                ),
                '底盤'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/底盤.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1311',
                    'note' => '最大21%off',
                ),
                '引擎'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/引擎.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1180',
                    'note' => '最大23%off',
                ),
                '排氣系統'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/排氣系統.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1001',
                    'note' => '最大19%off',
                ),
                '轉向系統'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/轉向系統.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1030',
                    'note' => '最大23%off',
                ),
                '車台骨架'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/車台骨架.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1059',
                    'note' => '最大26%off',
                ),
                '傳動系統'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/傳動.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1150',
                    'note' => '最大20%off',
                ),
                '煞車'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/煞車系統.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>1000]),
                    'url_rewrite'=>'1010',
                    'note' => '最大28%off',
                )
            ),
            '騎士用品'=>array(
                '安全帽'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/安全帽.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3001',
                    'note' => '最大20%off',
                ),
                '夾克・防摔衣'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/夾克防摔衣.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3021',
                    'note' => '最大17%off',
                ),
                '騎士手套'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/手套.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3060',
                    'note' => '最大21%off',
                ),
                '騎士鞋・車靴'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/騎士鞋.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'1325',
                    'note' => '最大21%off',
                ),
                '褲子・防摔褲'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/褲子防摔褲.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3071',
                    'note' => '最大17%off',
                ),
                '護具'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/護具.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3027',
                    'note' => '最大19%off',
                ),
                '包包'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/包包.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3123',
                    'note' => '最大23%off',
                ),
                '雨衣'=>array(
                    'img'=>assetRemote('image/benefit/big-promotion/雨衣.png'),
                    'url'=>\URL::route('benefit-sale-2017-promotion-category',['category'=>3000]),
                    'url_rewrite'=>'3040',
                    'note' => '最大18%off',
                )
            )
        );
        
        $result = $categories;

        return $result;
    }

    public static function getCategoryBrandDiscounts($campaign_correspond_name)
    {
        $cache_kay = $campaign_correspond_name . '-CategoryBrandDiscounts';
        if(!useCache()){
            Cache::forget($cache_kay);
        }
        $discount_point = Cache::rememberForever($cache_kay, function() use($campaign_correspond_name){
            $today = date('Y-m-d');
//            $today = '2017-02-17';
            $discount_point = \DB::connection('eg_product')->select(
                "SELECT 
                    ct.url_rewrite as category_url_rewrite,
                    ma.url_rewrite as manufacturer_url_rewrite,
                    ROUND((1-ROUND(min(ppc.price/pd.price),2))*100,0) as discountc,
                    (1-ROUND(min(ppd.price/pd.price),2))*100 as discountd,
                    ROUND(max(pptc.point/(pd.price*0.01)),0) as c_point,
                    ROUND(max(pptd.point/(pd.price*0.01)),0) as d_point
                FROM eg_product.categories as ct
                join eg_product.category_product as cp
                on ct.id = cp.category_id
                join eg_product.products as pd
                on cp.product_id = pd.id
                join manufacturers as ma
                on pd.manufacturer_id = ma.id
                join eg_product.product_points as pptc
                on pd.id = pptc.product_id and pptc.role_id = 2 and pptc.rule_date = '" . $today . "'
                join eg_product.product_points as pptd 
                on pd.id = pptd.product_id and pptd.role_id = 3 and pptd.rule_date = '" . $today . "'
                join eg_product.product_prices as ppc
                on pd.id = ppc.product_id and ppc.role_id = 2 and ppc.rule_date = '" . $today . "'
                join eg_product.product_prices as ppd
                on pd.id = ppd.product_id and ppd.role_id = 3 and ppd.rule_date = '" . $today . "'
                join ec_dbs.campaign_corresponds as camp
                on camp.br_rewrite = ma.url_rewrite and camp.ca_rewrite = ct.url_rewrite and camp.name = '" . $campaign_correspond_name . "'
                where pd.type = 0 
                and pd.active = 1
                and pd.is_main = 1
                and pd.parent_id = 0
                GROUP BY ct.url_rewrite , ma.url_rewrite"
            );
            return $discount_point;
        });

        return collect($discount_point);
    }

    public static function getDiscountCategories($campaign_correspond_name)
    {
        $date = date('Y-m-d');
        $discount_category = Correspond::where('name','=',$campaign_correspond_name)->where('open_date','<=',$date)->where('close_date','>=',$date)->get();
        return $discount_category;
    }

}