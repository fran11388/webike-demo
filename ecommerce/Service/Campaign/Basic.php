<?php

namespace Ecommerce\Service\Campaign;

use Everglory\Models\Accounting\Received\Queues;
use Everglory\Models\Manufacturer;
use Everglory\Models\Mptt;
use Everglory\Models\Campaign\Item;
use Everglory\Models\Campaign\Template;


class Basic
{
    public function render($data)
    {
        $view_path = 'response.';
        if($this->isMobile()){
            $view_path = 'mobile.';
        }

        return view($view_path.$data['blade'],$data)->render();
    }

    public function initialData($data)
    {
        $result = new \StdClass;

        foreach($data as $key => $val){
            $result->$key = $val;
        }

        return $result;
    }

    private function isMobile()
    {
        if(strpos(request()->route()->getAction()['namespace'],'Mobile') !== false){
            return true;
        }
        return false;
    }

    public function setCache($cache_key){
        if(!useCache()){
           \Cache::tags($cache_key)->flush();
        }
    }

   public function getBrands($brands_url_rewrite,$cache_name){
        $cache_key = 'weekly-'.$cache_name.'-promotion-brands';
        $this->setCache($cache_key);

        $brands = \Cache::tags([$cache_key])->remember($cache_key, 86400, function () use ($brands_url_rewrite) {
                $brands = Manufacturer::whereIn('url_rewrite',$brands_url_rewrite)->get();
                return $brands;
           });
        return $brands;
    }
    public function getBrandSaleData($brand_ids,$cache_name){

        $cache_key = 'weekly-'.$cache_name.'brand_sale_data';
        $this->setCache($cache_key);

        $result = \Cache::tags([$cache_key])->remember($cache_key, 86400, function () use ($brand_ids) {
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,"http://pricing.everglory.asia/api/discount?brand_ids=".$brand_ids."&date=".date('Y-m-d'));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);
            
            return $result;
       });
        return json_decode($result);
    }
    public function getBrandCampaignData($brands,$brand_sale_data){
        
        $received_queues_orders = \Cache::tags(['newyear-promotion-received_queues'])->remember('newyear-promotion-received_queues', 86400, function () {
                $received_queues =  Queues::select('order_id')->where('date','>','2018-01-01')->where('date','<','2018-12-31')->get();
                $received_queues_order =  array_pluck($received_queues,'order_id');
                return implode(',',$received_queues_order);
           });

            foreach($brands as  $brand){
                    $result = \DB::connection('eg_zero')->select('select pim.image as product_image,o.* from eg_product.product_images as pim
                        join
                        (    
                            select * from
                            (
                            select oid.item_id,oid.`value`,sum(o.quantity) as qty,o.product_id 
                            from order_item_details as oid
                            join
                            (
                                select oi.id,oi.quantity,oi.product_id from order_items as oi
                                join order_item_details as oid
                                on oi.id = oid.item_id
                                where oid.attribute = "manufacturer_url_rewrite"
                                and oi.product_type = 0
                                and oid.value = "'. $brand->url_rewrite . '"
                                and oi.order_id in ('. $received_queues_orders  .')
                            ) as o
                            on oid.item_id = o.id 
                            and oid.attribute = "category_url_path"
                            group by oid.value,o.product_id
                            order by sum(o.quantity) desc
                            ) o
                            group by o.`value`
                            order by sum(o.qty) desc
                            limit 3
                        ) as o
                        on pim.product_id = o.product_id 
                        GROUP BY pim.product_id');


                    if(!$result){
                        $result = \DB::connection('eg_product')->select('select c.url_path as value,img.image as product_image FROM manufacturers as m
                                join products as p
                                on m.id = p.manufacturer_id
                                join category_product as cp
                                on p.id = cp.product_id
                                join mptt_categories_flat as c
                                on cp.category_id = c.id
                                join ( select product_id,image from product_images 
                                            GROUP BY product_id
                                ) as img
                                on p.id = img.product_id
                                where m.url_rewrite = "'. $brand->url_rewrite .'"
                                and c.n_right - c.n_left = 1
                                GROUP BY c.id
                                ORDER BY COUNT(*) desc
                                limit 1'
                            );
                    }

                    $product_image = array_pluck($result,'product_image'); 
                    $ca_url_path = array_pluck($result,'value');
                    $ca_url_path_str = implode(",",$ca_url_path);
                    $ca_name = Mptt::select('name','url_path')
                    ->whereIn('url_path',$ca_url_path)
                    ->orderByRaw(\DB::raw("FIELD(url_path,'$ca_url_path_str')"))
                    ->get();

                    
                    $brand_id = $brand->id;
                    $brand_date[$brand->url_rewrite]['offer'] = isset($brand_sale_data->$brand_id) ? $brand_sale_data->$brand_id : 0;
                    $brand_date[$brand->url_rewrite]['brand_image'] = $brand->image;
                    $brand_date[$brand->url_rewrite]['brand_name'] = $brand->name;
                    $brand_date[$brand->url_rewrite]['product_image'] = $product_image;
                    $brand_date[$brand->url_rewrite]['ca_name'] = $ca_name;


            }
            return $brand_date;
    }
    public function getBrandPointData($brand_ids){

        $cache_key = 'weekly-promotion-getBrandPointData';
        $this->setCache($cache_key);

            $result = \Cache::tags(['weekly-promotion-getBrandPointData'])->remember('weekly-promotion-getBrandPointData', 86400, function () use($brand_ids) {
                $result = \DB::connection('eg_product')->select('select manufacturer_id,SUM(pi.point)/(SUM(p.price)/100) as point_multiple from products as p
                                join product_prices as pr
                                on p.id = pr.product_id
                                join product_points as pi
                                on p.id= pi.product_id
                                where p.manufacturer_id in ('.$brand_ids.')
                                and p.type = 0
                                and pi.rule_date = "'.date('Y-m-d').'"
                                and pi.role_id = 2
                                GROUP BY manufacturer_id');
                return $result;
           });
        $brandPointData = new \stdClass();
        foreach ($result as $value) {
            $manufacturer_id = $value->manufacturer_id;
            $brandPointData->$manufacturer_id = number_format(round($value->point_multiple));
        }
        return $brandPointData;
    }

    public function getBladeData($block_data,$blade)
    {
        $data = [];
        foreach ($block_data as $data_arr) {
            if($data_arr[2]){
                $data_name = Explode(':', $data_arr[2]);
                if($data_name[0] == $blade){
                    $data[] = $data_arr; 
                }
            }
        }

        return $data;
    }

    public function getColumnKey($data,$key_word)
    {
      foreach ($data as $data_arr) {
        foreach($data_arr as $key => $column){
          if($column === $key_word){
            return $key;
          }
        }
      }
    }

    public function getItemData($block_data,$blade_data,$columns)
    {

        $block_code_key = $this->getColumnKey($block_data,"名稱(系統用)");
        $sort_key = $this->getColumnKey($block_data,"順序");
        $title_key = $this->getColumnKey($block_data,"title");
        $data_key = $this->getColumnKey($block_data,"data");

        $data = [];
        foreach ($blade_data as $data_arr) {
            $data_name = Explode(':', $data_arr[$block_code_key]);
            $code_name = isset($data_name[1])? $data_name[1] : null ;
            if(!$code_name){
                $data['block_code'] = $data_arr[$block_code_key];
                $data['title'] = $data_arr[$title_key];
                $data['sort'] = $data_arr[$sort_key];
            }elseif(in_array($code_name,$columns)){
                foreach($data_arr as $key => $images_data){                    
                    if($key >= $data_key && $images_data){
                        $data[$code_name][] = $images_data;
                    }
                }
            }
        }
        
        return $data;
    }

    public function writeItem($item_data,$campaign_id){

        $template = Template::where('block_code',$item_data['block_code'])->first();

        $campaign_item = Item::where('campaign_id',$campaign_id)->where('block_code',$item_data['block_code'])->first();

        if(!$campaign_item){
            $campaign_item = new Item;
        }

        $campaign_item->campaign_id = $campaign_id;
        $campaign_item->block_name = $item_data['title'];
        $campaign_item->block_code = $item_data['block_code'];
        $campaign_item->data = json_encode($item_data);
        $campaign_item->active = 1;
        $campaign_item->sort = $item_data['sort'];
        $campaign_item->template_id = $template->id;
        $campaign_item->created_at = date('Y-m-d H:i:s');

        $campaign_item->save();
    


    }

}