<?php

namespace Ecommerce\Service\Campaign\Weekly;

use Ecommerce\Service\Campaign\Basic;
use Everglory\Models\Campaign;

class Collection extends Basic
{
    public $columns = [
    ];

    public function __construct()
    {
    }

    public function read($data)
    {
        $block_data = $this->initialData($data);
        
        $block_data->collections =$this->setCollections(10,$data['url_rewrite']);
        $block_data->tail = " - 「Webike-摩托百貨」";
        return $this->render((array)$block_data);
    }

   public function write($block_data,$campaign_id,$blade)
    {
       
        $blade_data = $this->getBladeData($block_data,$blade);

        $data = $this->getItemData($block_data,$blade_data,$this->columns);
        
        $item = $this->writeItem($data,$campaign_id);
    }

    protected function setCollections($num,$url_rewrite)
    {
        $assortmentService = new \Ecommerce\Service\AssortmentService;
        $assortment = $assortmentService->getAssortmentByPublishAt($num);
        
        $result = $assortment->filter(function($assortment){
            return $assortment->type_id != 7;
        });
        
        $result = $assortment->filter(function($assortment) use ($url_rewrite){
            return $assortment->url_rewrite != $url_rewrite;
        });

        $result = $result->take(4);
        return $result;
    }
}