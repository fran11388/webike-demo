<?php

namespace Ecommerce\Service\Campaign\Weekly;

use Ecommerce\Service\Campaign\Basic;
use Everglory\Models\Campaign;

class BrandDouble extends Basic
{
    public $columns = [
        'url_rewrites'
    ];

    public function __construct()
    {
    }

    public function read($data)
    {
        $block_data = $this->initialData($data);
        
        $cache_name = $block_data->url_rewrite.'BrandDouble';

        $brands = $this->getBrands($block_data->url_rewrites,$cache_name);
        $brand_sale_data = $this->getBrandPointData($brands->implode('id',','));
        
        $this->setCache('weekly-promotion'.$cache_name);
        $brandDouble_datas = \Cache::tags(['weekly-promotion'. $cache_name])->remember('weekly-promotion'. $cache_name, 86400, function () use($brands,$brand_sale_data) {
                return $this->getBrandCampaignData($brands,$brand_sale_data);
           });

        $block_data->brandDouble_datas = $brandDouble_datas;

        return $this->render((array)$block_data);
    }

   public function write($block_data,$campaign_id,$blade)
    {
       
        $blade_data = $this->getBladeData($block_data,$blade);

        $data = $this->getItemData($block_data,$blade_data,$this->columns);
        
        $item = $this->writeItem($data,$campaign_id);
    }
}