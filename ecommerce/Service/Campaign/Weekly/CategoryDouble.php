<?php

namespace Ecommerce\Service\Campaign\Weekly;

use Ecommerce\Service\Campaign\Basic;
use Everglory\Models\Campaign;

class CategoryDouble extends Basic
{
    public $columns = [
        'url_rewrites',
        'names',
        'images',
        'tag_colors',
        'tag_text'
    ];

    public function __construct()
    {
    }

    public function read($data)
    {
        $block_data = $this->initialData($data);
        
        return $this->render((array)$block_data);
    }

    public function write($block_data,$campaign_id,$blade)
    {
       
        $blade_data = $this->getBladeData($block_data,$blade);

        $data = $this->getItemData($block_data,$blade_data,$this->columns);
        
        $item = $this->writeItem($data,$campaign_id);
    }
}