<?php

namespace Ecommerce\Service\Campaign\Weekly;

use Ecommerce\Service\Campaign\Basic;
use Everglory\Models\Campaign;
use Ecommerce\Service\AssortmentService;

class Layout extends Basic
{
    public $data_key = [
        'main_color_r',
        'main_color_g',
        'main_color_b',
        'main_color_text',
        'promotion_title',
        'promotion_introduct',
        'url_rewrite',
        'collection_name',
        'collection_image',
        'collection_desciprion'
    ];

    public $time_key = ['year','week','start'];

    public $columns = ['time_key','time','data_key','data'];

    public function __construct()
    {
    }

    public function read($data)
    {
        $block_data = $this->initialData($data);
        
        return $this->render((array)$block_data);
    }

    public function write($block_data)
    {
        $blade_data = $this->getBladeData($block_data,"Campaign");

        $campaign_data = $this->getCampaignData($blade_data);

        $data = $this->getBlockData($campaign_data['time_key'],$campaign_data['time'],$campaign_data['data_key'],$campaign_data['data']);

        $campaign_id = $this->writeCampaignsData($data);

        return $campaign_id;

    }

    private function getCampaignData($block_data){

       $data = [];
        foreach ($block_data as $data_arr) {
            if($data_arr[2]){
                $data_name = Explode(':', $data_arr[2]);
                $name_key = $data_name[1];
                if(in_array($name_key,$this->columns)){
                    $data[$name_key] = $data_arr;
                }
            }
        }

        return $data;

    }

    private function getBlockData($campaign_time_key,$campaign_time,$campaign_data_key,$campaign_data)
    {
        $data = [];
        foreach ($campaign_time_key as $key => $time_key) {
            if(in_array($time_key,$this->time_key)){
                $data[$time_key] = $campaign_time[$key];
            }
        }      
        foreach ($campaign_data_key as $key => $data_key) {
            if($data_key){
                $data[$data_key] = $campaign_data[$key];
            }
        }
        return $data;
    }


    private function getCampaignsData($block_data)
    {

        $data = [];
        $data['assortment_id'] = null;
        $data['title'] = $block_data['promotion_title'];
        $data['url_rewrite'] = $block_data['url_rewrite'];
        $data['year'] = $block_data['year'];
        $data['week'] = $block_data['week'];
        $data['data'] = json_encode($block_data);
        $data['active'] = 0;
        $data['publish_at'] = $block_data['start'] . " 00:00:00";

        return $data;
        
    }

    private function writeCampaignsData($data)
    {

        $campaign = Campaign::where('year',$data['year'])->where('week',$data['week'])->first();

        if(!$campaign){
            $campaign = new Campaign;
        }

        $campaign->assortment_id = null;
        $campaign->title = $data['promotion_title'];
        $campaign->url_rewrite = $data['url_rewrite'];
        $campaign->year = $data['year'];
        $campaign->week = $data['week'];
        $campaign->data = json_encode($data);
        $campaign->active = 0;
        $campaign->layout_id = 1;
        $campaign->publish_at = $data['start'] . " 00:00:00";
        $campaign->created_at = date('Y-m-d H:i:s');

        $campaign->save();
        
        return $campaign->id;
    }
}