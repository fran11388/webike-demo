<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;


class StatusService extends BaseService
{
    private $statuses;
    private $bar;
    public function __construct()
    {
//        $this->bar = new \stdClass();
        $this->statuses = [
            'Order' => [
                'Cancel',
                'Estimate',
                'Payment',
                'Stocking',
                'Shipping',
                'Finish',
            ],
            'Rex' => [
                'Fail',
                'Apply',
                'Recycling',
                'Shipping',
                'Payment',
                'Finish',
            ],
        ];
    }

    public function getProcess($order)
    {

        $namespace = explode('\\',get_class($order));
        $bar = new \stdClass();
        $bar->canceled = false;
        $bar->failed = false;
        $bar->can_cancel = 0;
        foreach($this->statuses[$namespace[2]] as $class){
            $status = __NAMESPACE__ . '\\Status\\'.$namespace[2].'\\' . $class;
            if( !class_exists( $status ) ){
                throw new \Exception('Class : ' . $status . ' does not exist ');
            }
            new $status($bar, $order);
//            $status_class->setting();
        }


        $bar->process = collect($bar->process);

        return $bar;
    }

}