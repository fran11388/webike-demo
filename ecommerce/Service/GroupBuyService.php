<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Ecommerce\Service;

use Ecommerce\Repository\ProductRepository;
use Ecommerce\Accessor\ProductAccessor;
use Illuminate\Support\Collection;

use Ecommerce\Core\WarehouseFactory\Program AS WarehouseFactory;
use Everglory\Models\Product;
use Everglory\Models\Gift;
class GroupBuyService extends BaseService
{

    const CACHE_LIFETIME = 24 * 60;

    Const CATEGORY_ID = 1628;
    Const SYNC_PARAM = [
        'type' => 'import'
    ];
    Const STATUS = [
        'untreated' => 0,
        'finish' => 1
    ];

    protected $warehouseQueues = [];
    protected $warehouseFactory;

    public function __construct()
    {
        $this->warehouseFactory = new WarehouseFactory;
    }

    public static function getRequestEstimates()
    {
        $items = [];
        $syouhin_maker = request()->input('syouhin_maker');
        $syouhin_name = request()->input('syouhin_name');
        $syouhin_code = request()->input('syouhin_code');
        $syouhin_type = request()->input('syouhin_type');
        $syouhin_kosuu = request()->input('syouhin_kosuu');
        $syouhin_bikou = request()->input('syouhin_bikou');

        for($i = 0; $i < count($syouhin_name); $i++) {
            if ($syouhin_maker[$i] != '' && $syouhin_name[$i] != '' && $syouhin_kosuu[$i] != '') {
                $items[$i] = [
                    'syouhin_maker' => $syouhin_maker[$i],
                    'syouhin_name' => $syouhin_name[$i],
                    'syouhin_code' => $syouhin_code[$i],
                    'syouhin_type' => $syouhin_type[$i],
                    'syouhin_kosuu' => $syouhin_kosuu[$i],
                    'syouhin_bikou' => $syouhin_bikou[$i],
                ];
            }
        }
        return $items;
    }

    public static function getRequestProduct()
    {
        $accessors = [];
        $url_rewrites = request()->input('skus');
        $url_rewrites = explode('_', $url_rewrites);
        $products = ProductRepository::getProductWithRelations($url_rewrites, ['manufacturer', 'options']);
        foreach ($products as $product){
            $accessors[] = new ProductAccessor($product);
        }
        return new Collection($accessors);
    }

    public function add($data)
    {
        $queueData = $this->getQueueData($data);
        if(!$queueData){
            return false;
        }
        $this->warehouseQueues[] = $this->warehouseFactory->addWarehouseQueue($queueData);

        return $this;
    }

    public function get()
    {
        if(\Config::get('app.production') === true) {
            $result = $this->warehouseFactory->sync(self::SYNC_PARAM);
            if ($result->success) {
                $warehouseQueues = collect($this->warehouseQueues);
//                $warehouseQueues = WarehouseQueue::where('id', 1010)->get();
                $this->warehouseQueues = $this->warehouseFactory->reloadWarehouseQueue($warehouseQueues->pluck('id')->toArray());
                $supplierService = new SupplierService;
                $products = Product::with('supplier', 'categories')->whereIn('id', $this->warehouseQueues->pluck('product_id')->toArray())->get();
                foreach ($this->warehouseQueues as $warehouseQueue) {
                    $product = $products->first(function ($product) use ($warehouseQueue) {
                        return $product->id == $warehouseQueue->product_id;
                    });
                    $supplierService->addDomesticProductSupplier($product);
                }
                return $this->warehouseQueues;

            } else {
                return null;
            }
        }else{
            return Product::with('supplier')->where('active', 1)->where('is_main', 1)->orderBy('id', 'desc')->limit(3)->get();
        }
    }

    private function getQueueData($data)
    {
        $queueData = new \StdClass;
        $attributes = null;
        if($data instanceof Gift){
            $attributes = $data->getAttributes();
            $queueData->name = $data->name;
            $product = $data->origProduct;
            $queueData->model_number = $product->model_number;
            $queueData->manufacturer_id = $product->manufacturer_id;
            $queueData->custom_manufacturer = 0;
            $queueData->price = 0;
            $queueData->cost = $product->cost;

            foreach ($attributes as $attribute => $value){
                $queueData->$attribute = $value;
            }
        }else if($data instanceof \Eloquent){
            $attributes = $data->getAttributes();
            foreach ($attributes as $attribute => $value){
                $queueData->$attribute = $value;
            }
        }else if(is_array($data) or is_object($data)){
            $attributes = $data;
            foreach ($attributes as $attribute => $value){
                $queueData->$attribute = $value;
            }
        }

        if(empty($queueData)){
            return false;
        }


        $queueData->category_id = self::CATEGORY_ID;
        $queueData->status = self::STATUS['untreated'];
        return $queueData;
    }

}