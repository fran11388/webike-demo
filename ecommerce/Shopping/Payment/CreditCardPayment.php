<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 08:08
 */

namespace Ecommerce\Shopping\Payment;


class CreditCardPayment extends CorePayment
{
    protected static $id = '1';
    protected static $name = '信用卡';
    protected static $code= 'credit-card';

    public function isAvailable(){
        return true;
    }


    public function getFee(){
        return 0;
    }
}