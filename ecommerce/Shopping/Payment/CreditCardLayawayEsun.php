<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 08:08
 */

namespace Ecommerce\Shopping\Payment;

use Ecommerce\Shopping\Payment\CoreInstallment;
use Everglory\Constants\CustomerRole;

class CreditCardLayawayEsun extends CorePayment
{
    protected static $id = '4';
    protected static $name = '玉山分期付款';
    protected static $code= 'credit-card-layaway-esun';
    protected $install= '3';
    protected $allow_install = [3, 6, 12];
    
    public function setInstall($value){
        $this->install= $value;
    }
    public function getInstall(){
        return $this->install;
    }

    public function isAvailable(){
        if ($this->cartService->includeGroupbuyItem() or $this->cartService->includeOutletItem())
            return false;
        $amount = $this->cartService->getSubtotal();
        $coreInstallment = new CoreInstallment;
        $decide = $coreInstallment->decideInstallment($this->customer->role_id, $this->install, $amount);
        if(!in_array($this->install, $this->allow_install)){
            $decide->success = false;
        }
        return $decide->success;
        /*
        switch ($this->customer->role_id) {
            case CustomerRole::GENERAL:
                if ($this->install == 3 )
                    if ($amount < 3000)
                        return false;

                if ($this->install == 6 )
                    if ($amount < 6000)
                        return false;

                return true;
            case CustomerRole::STAFF:
                return true;
            default:
                return false;
        }
        */

    }

    /**
     * 計算手續費
     * @return int
     */
    function getFee()
    {
        $multiples = CoreInstallment::decideMultiples($this->customer->role_id);
        if(isset($multiples[$this->install])){
            return round($this->cartService->getSubtotal() * ($multiples[$this->install] - 1));
        }
        return 0;
    }
}