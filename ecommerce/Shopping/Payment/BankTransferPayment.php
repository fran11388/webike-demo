<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 08:08
 */

namespace Ecommerce\Shopping\Payment;


use Everglory\Constants\CustomerRole;

class BankTransferPayment extends CorePayment
{
    protected static $id = '3';
    protected static $name = '匯款轉帳';
    protected static $code= 'bank-transfer';

    public function isAvailable(){
        return true;
    }


    public function getFee(){
        return 0;
    }
}