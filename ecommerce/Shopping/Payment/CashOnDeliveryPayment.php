<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 08:08
 */

namespace Ecommerce\Shopping\Payment;


use Everglory\Constants\CustomerRole;

class CashOnDeliveryPayment extends CorePayment
{
    protected static $id = '2';
    protected static $name = '貨到付款';
    protected static $code= 'cash-on-delivery';

    public function isAvailable(){
        if ($this->customer->role_id == CustomerRole::GENERAL and $this->cartService->getSubtotal() < 20000)
            return true;

        if (($this->customer->role_id == CustomerRole::WHOLESALE or
                $this->customer->role_id == CustomerRole::STAFF or
                $this->customer->role_id == CustomerRole::BUYER ) and
            $this->cartService->getSubtotal() < 50000)
            return true;

        return false;
    }

    /**
     * 計算手續費
     * @return int
     */
    function getFee()
    {
        $amount = $this->cartService->getSubtotal() + $this->cartService->getFee();


        if ( $this->customer->role_id == CustomerRole::GENERAL and $amount > 20000 ){
            return false;
        }

        if ( $this->customer->role_id == CustomerRole::WHOLESALE and $amount > 50000 ){
            return false;
        }

        if ($amount < 2000){
            return 30;
        }elseif ($amount < 5000){
            return 60;
        }elseif ($amount < 10000){
            return 90;
        }elseif ($amount < 20000){
            return 120;
        }elseif ($amount < 30000){
            return 150;
        }else{
            return 180;
        }

    }
}