<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 08:08
 */

namespace Ecommerce\Shopping\Payment;

use Everglory\Constants\CustomerRole;

class CoreInstallment
{
    const STAGES = [3, 6, 12];
    const CONSUMER_MULTIPLES = [
        3 => 1,
        6 => 1.035,
        12 => 1.065,
    ];
    const WHOLESALE_MULTIPLES = [
        3 => 1.025,
        6 => 1.035,
        12 => 1.065,
    ];
    const STAFF_MULTIPLES = [
        3 => 1,
        6 => 1.035,
        12 => 1.065,
    ];
    protected $basic_rules = [
        3 => 3000,
        6 => 6000,
        12 => 6000,
    ];
    protected $customer;



    public function __construct()
    {
    }

    public function decideInstallment($role_id, $stage, $price)
    {
        $result = new \StdClass;
        $result->success = true;
        $result->price = $price;

        switch ($role_id) {
            case CustomerRole::GENERAL:
                $multiples = self::CONSUMER_MULTIPLES;
                if(isset($multiples[$stage])){
                    $result->price = round($price * $multiples[$stage]);
                    if(self::inCampaign()){
                        $result->price = round($price * 1);
                    }
                }
                if(isset($this->basic_rules[$stage])){
                    if ($price < $this->basic_rules[$stage]){
                        $result->success = false;
                    }
                }
                break;
            case CustomerRole::STAFF:
                $multiples = self::STAFF_MULTIPLES;
                if(isset($multiples[$stage])){
                    $result->price = round($price * $multiples[$stage]);
                }
                break;
            case CustomerRole::WHOLESALE:
                $multiples = self::WHOLESALE_MULTIPLES;
                if(isset($multiples[$stage])){
                    $result->price = round($price * $multiples[$stage]);
                }
                if($price < 3000){
                    $result->success = false;
                }
                break;
            default:
                $result->success = false;
                break;
        }

        return $result;
    }

    public static function decideMultiples($role_id)
    {
        switch ($role_id){
            case CustomerRole::WHOLESALE:
                $multiples = self::WHOLESALE_MULTIPLES;
                break;
            case CustomerRole::STAFF:
                $multiples = self::STAFF_MULTIPLES;
                break;
            default:
                $multiples = self::CONSUMER_MULTIPLES;
                if(self::inCampaign()){
                    $multiples[6] = 1;
                }
                break;
        }
        return $multiples;
    }

    function getInfo($price, $stage, $role_id)
    {
        $multiples = $this->decideMultiples($role_id);
        $percentage = '('.(($multiples[$stage]-1) * 100).'%)';
        $remain = $price % $stage;
        $stage_price = (($price - $remain) / $stage);
        $installment = new \StdClass;
        $installment->name = $stage . '期'.$percentage;
        $installment->stage_num = $stage;
        $installment->stage_prices = [];
        $installment->stage_prices[] = $stage_price + $remain;
        $installment->stage_prices[] = $stage_price;
        $installment->stages = [];
        $installment->stages[] = '首期：' . formatPrice( 'normal-text', ($stage_price + $remain) );
        $installment->stages[] = '其他期：' . formatPrice( 'normal-text', $stage_price );
        return $installment;
    }

    public function getInstallmentHtml($installment, $setting)
    {
        $html = '
                    <ul class="col-sm-block col-xs-block clearfix">
                        <li class="col-lg-2 col-md-2 col-sm-' . $setting->col_sm . ' col-xs-12">
                            <span class="hidden-xs text-center">' . $installment->name . '</span>
                            <div class="row hidden-lg hidden-md hidden-sm">
                                <div class="col-xs-4">
                                    <span>' . $installment->name . '</span>
                                </div>
                                <div class="col-xs-8" style="border-left:1px #ccc solid">';
        foreach($installment->stages as $stage){
            $html .= '<span style="padding-right:10px">' . $stage . '</span><br>';
        }
        if(in_array($setting->customer_role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF])){
            $html .= '<span style="padding-right:10px">分期總價:' . formatPrice('normal-text', $installment->stage_prices[0] + ($installment->stage_prices[1] * ($installment->stage_num - 1) )) . '</span>';
        }
        $html .= '        </div>
                            </div>
                        </li>';
        if(in_array($setting->customer_role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF])){
            $html .= '<li class="col-lg-4 col-md-4 col-sm-' . $setting->col_sm . ' hidden-xs"><div>' . formatPrice('normal-text', $installment->stage_prices[0] + ($installment->stage_prices[1] * ($installment->stage_num - 1) )) . '</div></li>';
        }
        foreach($installment->stage_prices as $stage){
            $html .= '<li class="col-lg-'. $setting->col_lg[1] .' col-md-' . $setting->col_md[1] . ' col-sm-' . $setting->col_sm . ' hidden-xs">
                                <div class="row"><span>' . formatPrice('normal-text', $stage) . '</span></div>
                            </li>';
        }
        $html .= '</ul>';
        return $html;
    }

    public static function getInstallHtmlSetting($customer_role_id, $installment_title = null)
    {
        $setting = new \StdClass;
        $setting->customer_role_id = $customer_role_id;
        $setting->installment_title = '信用卡分期零利率(支持11大發卡銀行，請參閱"<a href="' . route('customer-rule', 'member_rule_2') . '#E_b" title="信用卡分期說明" target="_blank">信用卡分期說明</a>")';
        $setting->col_lg = [5,5];
        $setting->col_md = [5,5];
        $setting->col_sm = 4;
        if(in_array($setting->customer_role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF])){
            $setting->col_lg = [3,3];
            $setting->col_md = [3,3];
            $setting->col_sm = 3;
        }
        if($installment_title){
            $setting->installment_title = $installment_title;
        }
        return $setting;
    }

    public function getPublishInstallments($customer_role_id, $price, $other_fee = 0)
    {
        $result = [];
        $setting = self::getInstallHtmlSetting($customer_role_id);
        foreach (self::STAGES as $stage){
            $decide = $this->decideInstallment($customer_role_id, $stage, $price);
            if($decide->success){
                $installment = $this->getInfo($decide->price + $other_fee, $stage,$customer_role_id);
                $installment->html = $this->getInstallmentHtml($installment, $setting);
                $result[] = $installment;
            }
        }
        return $result;
    }

    private static function inCampaign()
    {
        if(strtotime('now') >= strtotime('2019-04-01') and strtotime('now') < strtotime('2019-05-01')){
            return true;
        }
        return false;
    }
}