<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 08:08
 */

namespace Ecommerce\Shopping\Payment;

use Ecommerce\Service\CartService;
use Everglory\Constants\CustomerRole;

abstract class CorePayment
{
    protected static $id;
    protected static $name ;
    protected static $code;
    protected $install = 0;
    protected $allow_install = [];
    /**
     * @var CartService
     */
    protected $cartService;
    protected $customer;
    public function __construct( CartService $service )
    {
        $this->cartService = $service;
        $this->customer = \Auth::user();
    }

    public static function getCode(){
        return static::$code;
    }

    /**
     * @return mixed
     */
    public static function getId()
    {
        return static::$id;
    }

    /**
     * @return mixed
     */
    public static function getName()
    {
        return static::$name;
    }

    /**
     * 依據購物車內容判斷是否可用該付款方式
     * @return boolean
     */
    abstract function isAvailable();

    /**
     * 計算手續費
     * @return int
     */
    abstract function getFee();

    public function setInstall($value){}

    public function getInstall(){
        return $this->install;
    }
}