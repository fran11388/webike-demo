<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/12/7
 * Time: 上午 09:22
 */

namespace Ecommerce\Shopping\Payment;

use Ecommerce\Service\CartService;

class PaymentManager
{
    public static $methods = [
        3 => BankTransferPayment::class,
        2 => CashOnDeliveryPayment::class,
        1 => CreditCardPayment::class,
        4 => CreditCardLayawayEsun::class,
        5 => CreditCardLayawayNccc::class,
    ];


    /**
     * @param $code
     * @return CorePayment
     * @throws \Exception
     */
    public static function getPaymentMethod($code)
    {
        foreach (self::$methods as $class_name) {
            if ($class_name::getCode() == $code)
                return app()->make($class_name);
        }

        throw new \Exception('無可用的付款方式 :' . $code);
    }

    /**
     * @param $id
     * @return CorePayment
     * @throws \Exception
     */
    public static function getPaymentMethodById($id)
    {
        foreach (self::$methods as $class_id => $class_name) {
            if ($class_id == $id)
                return app()->make($class_name);
        }

        throw new \Exception('無可用的付款方式 :' . $id);
    }

    public static function hasLayawayPayment()
    {
        $esunMethod = Self::getPaymentMethod('credit-card-layaway-esun');
        $ncccMethod = Self::getPaymentMethod('credit-card-layaway-nccc');
        if ($esunMethod->isAvailable() or $ncccMethod->isAvailable()){
            return true;
        }
        return false;
    }
}