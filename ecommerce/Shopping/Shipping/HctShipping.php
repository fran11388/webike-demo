<?php
/**
 * Created by PhpStorm.
 * User: Wayne
 * Description: 一律收運費，移除免運設定
 * Date: 2017/09/26
 * Time: 下午 05:27
 */

namespace Ecommerce\Shopping\Shipping;

use Auth;
use Ecommerce\Repository\CustomerLevelRepository;
use Ecommerce\Service\CartService;
use Everglory\Constants\CustomerRole;

class HctShipping
{

    const SHIPPING_FEE_C = 90;
    const SHIPPING_FEE_D = 85;
    const SHIPPING_FEE_D_2 = 75;
    const SHIPPING_FEE_D_3 = 65;

    protected $id = '3';
    protected $name = '新竹貨運';
    protected $code = 'hct';
    /**
     * @var CartService
     */
    protected $cartService;

    protected $free_shipping = FREE_SHIP;
    protected $shipping_fee = self::SHIPPING_FEE_C;
    protected $customer;

    public function __construct( CartService $service)
    {
        $this->cartService = $service;
        $this->customer = Auth::user();
        if(!shippfeeupdate()){
            $this->shipping_fee = 85;
        }
    }

    public function getFee()
    {

        $amount = $this->cartService->getBaseTotal();
        $usage_coupon = $this->cartService->getUsageCoupon();

        if ((in_array($this->customer->role_id, [CustomerRole::STAFF]) or (activeShippingFree() && $amount >= $this->free_shipping or ($usage_coupon and $usage_coupon->discount_type == 'shippingfee' )))) {
            return 0;
        } else {
            if($this->customer->role_id == CustomerRole::WHOLESALE){
                if(shippfeeupdate()){
                    $this->shipping_fee = self::SHIPPING_FEE_D;
                }else{
                    $this->shipping_fee = 80;
                    
                }
                if(activeValidate('2017-11-10')){
                    $customerLevelRepository = new CustomerLevelRepository;
                    $customerLevel = $customerLevelRepository->getCurrentLevel($this->customer);
                    if($customerLevel){
                        if($customerLevel->level == 2){
                            if(shippfeeupdate()){
                                $this->shipping_fee = self::SHIPPING_FEE_D_2;
                            }else{
                                $this->shipping_fee = 70;
                            }
                        }else if($customerLevel->level == 3){
                            if(shippfeeupdate()){
                                $this->shipping_fee = self::SHIPPING_FEE_D_3;
                            }else{
                                $this->shipping_fee = 60;
                            }
                        }
                    }
                }
            }
            return $this->shipping_fee;
        }


    }

}