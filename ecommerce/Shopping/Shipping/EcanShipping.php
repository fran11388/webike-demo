<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Description: 購物車金額滿2000免運費
 * Date: 2016/12/7
 * Time: 上午 08:42
 */

namespace Ecommerce\Shopping\Shipping;


use Ecommerce\Service\CartService;
use Everglory\Constants\CustomerRole;

class EcanShipping
{

    protected $id = '1';
    protected $name = '宅配通';
    protected $code = 'ecan';
    /**
     * @var CartService
     */
    protected $cartService;

    protected $free_shipping = FREE_SHIP;
    protected $shipping_fee = '100';
    protected $customer;

    public function __construct( CartService $service)
    {
        $this->cartService = $service;
        $this->customer = \Auth::user();
    }

    public function getFee()
    {

        $amount = $this->cartService->getBaseTotal();
        $usage_coupon = $this->cartService->getUsageCoupon();

        if ($this->customer->role_id == CustomerRole::STAFF or $amount >= $this->free_shipping or ($usage_coupon and $usage_coupon->discount_type == 'shippingfee' )) {
            return 0;
        } else {
            return $this->shipping_fee;
        }


    }

}