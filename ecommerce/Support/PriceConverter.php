<?php

namespace Ecommerce\Support;

use Illuminate\Support\Collection;

class PriceObject
{
    public $key = '';
    public $min = '';
    public $max = '';
    public $name = '';
    public $count = 0;
}

class PriceConverter
{

    private static $lists = [
        ['key' => '0-499', 'min' => '0', 'max' => '499.0', 'name' => '499元 以下'],
        ['key' => '500-999', 'min' => '500', 'max' => '999', 'name' => '500 - 999元'],
        ['key' => '1000-1999', 'min' => '1000', 'max' => '1999', 'name' => '1,000 - 1,999'],
        ['key' => '2000-2999', 'min' => '2000', 'max' => '2999', 'name' => '2,000 - 2,999'],
        ['key' => '3000-4999', 'min' => '3000', 'max' => '4999', 'name' => '3,000 - 4,999'],
        ['key' => '5000-9999', 'min' => '5000', 'max' => '9999', 'name' => '5,000 - 9,999'],
        ['key' => '10000-19999', 'min' => '10000', 'max' => '19999', 'name' => '10,000 - 19,999'],
        ['key' => '20000-', 'min' => '20000', 'max' => '9999999', 'name' => '20,000 以上'],
    ];

    public static function all()
    {
        $collection = new Collection();
        foreach (self::$lists as $item) {
            $collection->put(null, self::_convert($item));
        }
        return $collection;
    }

    public static function find($value)
    {
        foreach (self::$lists as $item) {
            if ($item['min'] >= $value) {
                return self::_convert($item);
            }
        }
        return null;
    }

    public static function findBy($value, $field)
    {
        foreach (self::$lists as $item) {
            if ($item[$field] == $value) {
                return self::_convert($item);
            }
        }
        return null;

    }

    private static function _convert($data)
    {
        $object = new PriceObject();
        $object->key = $data['key'];
        $object->min = $data['min'];
        $object->max = $data['max'];
        $object->name = $data['name'];
        return $object;
    }
}