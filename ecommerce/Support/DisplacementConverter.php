<?php

namespace Ecommerce\Support;

use Illuminate\Support\Collection;

class DisplacementObject
{
    public $key = '';
    public $min = '';
    public $max = '';
    public $name = '';
}

class DisplacementConverter
{
    private static $lists = [
        ['key' => '50', 'min' => '0', 'max' => '50', 'name' => '0 - 50cc'],
        ['key' => '125', 'min' => '51', 'max' => '125', 'name' => '51 - 125cc'],
        ['key' => '250', 'min' => '126', 'max' => '250', 'name' => '126 - 250cc'],
        ['key' => '400', 'min' => '251', 'max' => '400', 'name' => '251 - 400cc'],
        ['key' => '750', 'min' => '401', 'max' => '750', 'name' => '401 - 750cc'],
        ['key' => '1000', 'min' => '751', 'max' => '1000', 'name' => '751 - 1000cc'],
        ['key' => '1001', 'min' => '1001', 'max' => '9999', 'name' => '1001cc -'],
    ];

    public static function all()
    {
        $collection = new Collection();
        foreach (self::$lists as $item) {
            $collection->put(null, self::_convert($item));
        }
        return $collection;
    }

    /**
     * Find specific motor's displacement range.
     *
     * @param $value
     * @return DisplacementObject|null
     */
    public static function find($value)
    {
        foreach (self::$lists as $item) {
            if ($item['min'] <= $value and $item['max'] >= $value) {
                return self::_convert($item);
            }
        }
        return null;
    }

    public static function findBy($value, $field)
    {
        foreach (self::$lists as $item) {
            if ($item[$field] == $value) {
                return self::_convert($item);
            }
        }
        return null;
    }

    /**
     * Convert specific data to object.
     *
     * @param $data
     * @return DisplacementObject
     */
    private static function _convert($data)
    {
        $object = new DisplacementObject();
        $object->key = $data['key'];
        $object->min = $data['min'];
        $object->max = $data['max'];
        $object->name = $data['name'];
        return $object;
    }
}