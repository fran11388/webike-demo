<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密碼至少要有6個字元且與確認密碼相符',
    'reset' => '密碼重新設定完成，請使用新密碼登入。',
    'sent' => '我們已經將重設密碼的信件寄到該信箱。',
    'token' => '此token不允許重設密碼',
    'user' => "無法找到該Email",

];
