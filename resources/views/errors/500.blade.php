@extends('response.layouts.1column-error')
@section('style')
@stop
@section('middle')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h1>500 Oops!</h1>
                    <p>
                        伺服器發生錯誤<br>
                        非常抱歉，此頁面可能發生錯誤，請您稍後再嘗試。<br>
                        如果您急需使用此頁面，請您至<a href="{{URL::route( 'customer-service-proposal-create', 'system' )}}" title="系統及網頁問題回報 - 「Webike-摩托百貨」" target="_blank">【系統及網頁問題回報】</a>發送表單。我們將盡速為您處理。<br>
                        10秒後將自動導向回首頁。<br>
                        造成您的不便，敬請見諒。<br>
                    </p>
                    <div class="title-box-content">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <a class="btn btn-full btn-warning" href="javascript:void(0)" onclick="goBack();">回上一頁</a>
                            </div>
                            <div class="col-md-2 col-md-offset-8 col-sm-4 col-sm-offset-4 col-xs-6" href="{{URL::route('home')}}">
                                <a class="btn btn-full btn-warning">回首頁</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
@section('script')
    <script type="application/javascript">
        $(document).ready(function(){
            autoGoShoppingHome();
        });
    </script>
@stop