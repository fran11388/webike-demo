@extends('response.layouts.1column-error')
@section('style')
@stop
@section('middle')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h1>帳號已鎖定</h1>
                    <p>
                        <br>
                        這個帳號(IP)已經被鎖定無法登入<br>
                        1.曾經違反本站的相關規定<br>
                        2.違約的拒絕往來戶<br>
                        3.用不正當的方式攻擊本站<br>
                        若非上述狀況，請您隨即來信service@webike.tw與我們聯繫。<br>
                        10秒後將自動導向回首頁。<br>
                    </p>
                    <div class="title-box-content">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <a class="btn btn-full btn-warning" href="javascript:void(0)" onclick="goBack();">回上一頁</a>
                            </div>
                            <div class="col-md-2 col-md-offset-8 col-sm-4 col-sm-offset-4 col-xs-6" href="{{URL::route('home')}}">
                                <a class="btn btn-full btn-warning">回首頁</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
@section('script')
    <script type="application/javascript">
        $(document).ready(function(){
            autoGoShoppingHome();
        });
    </script>
@stop