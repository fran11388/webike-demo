<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
    @include ( 'response.layouts.partials.head' )
    @yield('style')
</head>
<body>
<div class="all-page">
    @include ( 'response.layouts.partials.header.shopping' )
    <!--Content -->
    <section id="contents top-shopping contents-all-page" class="ct-main top-shopping contents-all-page">
        <div class="container">
            <div class="breadcrumb-mobile">
                <ol class="breadcrumb breadcrumb-product-detail" itemscope itemtype="http://schema.org/BreadcrumbList">
                    {!! $breadcrumbs_html !!}
                </ol>
            </div>
            <!-- <a id="m-menu-execute" class="hidden-lg hidden-md hidden-sm" href="#m-menu"><i class="glyphicon glyphicon-book"></i></a> -->
            <div class="box-fix-column hidden-xs {{Route::currentRouteName() != 'motor-top' ? 'hidden-xs' : ''}}">
                @yield('top')
            </div>
            <div class="oem-part-page box-fix-column col-xs-12 col-sm-12 col-md-12">
                <div id="m-menu" class="hidden-xs">
                    <div>
                        <aside class="ct-left-search-list  col-xs-12 col-sm-4 col-md-3 col-lg-3">
                            @yield('left')
                        </aside>
                    </div>
                </div>
                <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    @yield('right')
                </div>
            </div>
        </div>
    </section>
    <!--End Content -->
</div>
@include ( 'response.layouts.partials.footer.shopping' )
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>