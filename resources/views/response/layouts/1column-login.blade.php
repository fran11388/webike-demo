<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
@include ( 'response.layouts.partials.head' )
<!-- Custom CSS -->
    @yield('style')
    <style type="text/css">
        html {
            height:100% !important;
        }
        body {
            height:100% !important;
            background-color: #f6f6f6;
        }
        .all-page{
            height: 100%;
            font-size: 0;
            text-align: center;
        }
        .all-page-noeffect{
            height: 100%;
            display: inline-block;
            vertical-align:  middle;
        }
        .container-login-page{
            display: inline-block;
            vertical-align: middle;
        }
        .container-footer{
            margin-top:  20px;
            text-align: center;
            color: #6c6c6c;
        }
    </style>
</head>
<body>
@if(session()->get('need_verify'))
    @include('response.common.login.pop-up-email-verify')
@endif
<div class="all-page">
    <!-- Header -->
    <!--End Header -->
    <div class="all-page-noeffect"></div>
    <!--Content -->
    <section id="contents" class="container-login-page">
        <div class="container">
            @yield('middle')
        </div>
        <div class="container-footer" style="text-align: center; color: #6c6c6c;">
            <span>Good ride, Good life! 台灣最大級摩托車線上百貨!</span>
        </div>
    </section>
    <!--End Content -->
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>