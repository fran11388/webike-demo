<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
@include ( 'response.layouts.partials.head' )
<!-- Custom CSS -->
    @yield('style')
</head>
<body>
<div class="all-page">
    <!-- Header -->
    <section class="header-login-page">
        <div class="container container-header-login">
            <a href="{{route('home')}}" id="logo-login-page">
                <img src="{!! assetRemote('image/login/logo-login-page.png') !!}" alt="logo login page">
            </a>
            <div class="container-header-login-right">
                @yield('page-title')
            </div>
        </div>
    </section>
    <!--End Header -->
    <!--Content -->
    <section id="contents" class="container-login-page">
        <div class="container">
            @yield('middle')
        </div>
    </section>
    <!--End Content -->
    <div id="footer-login-page">
        <div class="container">
            <img src="{!! assetRemote('image/login/img-ever-glory.png') !!}" alt="">
            <span>© {{ date('Y') }} EverGlory Motors. All Rights Reserved.</span>
            <span class="img-secured-footer">
                @include('response.common.ssl-logo')
            </span>
        </div>
    </div>
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>