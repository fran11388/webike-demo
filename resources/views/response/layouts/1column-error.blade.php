<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>{{ \Everglory\Constants\SEO::TITLE }}</title>
    <meta name="title" content="{{ \Everglory\Constants\SEO::TITLE }}"/>
    <meta name="description" content="{{ \Everglory\Constants\SEO::DESCRIPTION }}"/>
    <meta name="keywords" content="{{ \Everglory\Constants\SEO::KEYWORDS }}"/>
    <meta property="og:title" content="{{ \Everglory\Constants\SEO::TITLE }}"/>
    <meta property="og:description" content="{{ \Everglory\Constants\SEO::DESCRIPTION }}"/>
    <meta property="og:image" content="{{ \Everglory\Constants\SEO::IMAGE }}"/>
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="zh_TW"/>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/bootstrap.min.css') }}">
    <!-- Custom Fonts -->
<!-- <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/font.css') }}" media="none" onload="media='screen'"> -->
    <link href="{{ assetRemote('plugin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/owl-carousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('plugin/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('plugin/sweetalert2/sweetalert2.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/main.css?') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/css-rewrite.css?') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/common-script.css') }}">


    <!-- Custom CSS -->
    @yield('style')
</head>
<body>
@php
    $tail = ' - 「Webike-摩托百貨」';
    $current_customer = null;
    if(Auth::check()){
        $current_customer = Auth::user();
    }
    $bar_obvious = false;
@endphp
@include ( 'response.layouts.partials.header.shopping' )
<!--Content -->
<section id="contents top-shopping contents-all-page" class="ct-main top-shopping contents-all-page">
    <div class="container">
        <div class="breadcrumb-mobile">
            <ol class="breadcrumb breadcrumb-product-detail" itemscope itemtype="http://schema.org/BreadcrumbList">
            </ol>
        </div>
        @yield('middle')
    </div>
</section>
<!--End Content -->
@include ( 'response.layouts.partials.footer.shopping' )
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>