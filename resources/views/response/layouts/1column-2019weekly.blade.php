<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
@include ( 'response.layouts.partials.head' )
<!-- Custom CSS -->
    @yield('style')
</head>
<body>
<div class="all-page">
@include ( 'response.layouts.partials.header.weekly-header' ) 
<!--Content -->
    <div class="big-promotion">
        <div class="promotion-container">
               
                @yield('middle')
        </div>
    </div>
    @include('response.layouts.partials.footer.shopping')
</div>
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>