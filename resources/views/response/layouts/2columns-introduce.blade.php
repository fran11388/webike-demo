<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
    @include ( 'response.layouts.partials.head' )
    @yield('style')
    <style>
        .video-background {
            background-color:white;
            padding:0px 10px 10px 10px;
        }
        .all-page {
            background: #eee;
            padding-bottom:20px;
        }
        .top-shopping {
            margin:20px 20px 0px 20px !important;
        }
        section {
            margin-bottom: 0px;
        }
        .channel-introduction .container{
            margin:0px;
            padding:0px;
            width:100% !important;
            max-width:100% !important;
        }
        .breadcrumbs-container {
            background: #fff;
            padding: 0 10px 0 10px;
            margin-bottom:20px;
        }
        .breadcrumb-item a{
            color:#005fb3 !important;
        }
        .breadcrumb-item a:hover{
            color:#e61e25 !important;
        }
        .left-container {
            padding-left:10px;
            padding-right:10px;
        }
        .right-container {
            padding-left:10px;
            padding-right:10px;
        }
        .container .row-contain {
            margin-left:-10px;
            margin-right:-10px;
        }
        span {
            font-size:1rem;
        }
        .breadcrumb-mobile {
            margin-top:150px !important;
        }
    </style>
</head>
<body>
<div class="all-page">
@include ( 'response.layouts.partials.header.shopping' )
<!--Content -->
    <section id="contents top-shopping contents-all-page" class="ct-main top-shopping contents-all-page channel-introduction">
        <div class="container">
            <div class="breadcrumb-mobile breadcrumbs-container">
                <ol class="breadcrumb breadcrumb-product-detail" itemscope itemtype="http://schema.org/BreadcrumbList">
                    {!! $breadcrumbs_html !!}
                </ol>
            </div>
            <div class="row-contain">
                <aside class="col-xs-12 col-sm-12 col-md-9 col-lg-9 left-container">
                    @yield('left')
                </aside>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-container">
                    @yield('right')
                </div>
            </div>
        </div>
    </section>
    <!--End Content -->
</div>
@include ( 'response.layouts.partials.footer.shopping' )
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>