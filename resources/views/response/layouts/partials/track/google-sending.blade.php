<script type="text/javascript">
    $(document).ready(function(){
        @if(isset($current_customer) and $current_customer and $current_customer->role)
            var dimensionValue = '{!! $current_customer->role->name !!}';
            ga('send', 'pageview', {
                'dimension6': dimensionValue //customer-role
            });
        @else
            var dimensionValue = "Guest";
            ga('send', 'pageview', {
                'dimension6': dimensionValue //customer-role
            });
        @endif
    });
</script>