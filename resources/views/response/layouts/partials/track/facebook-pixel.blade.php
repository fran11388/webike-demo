<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '304234449734834');
    fbq('track', "PageView");

    @if(Route::currentRouteName() == 'product-detail' and isset($product))
      fbq('track', 'ViewContent', {
        content_type: 'product', //either 'product' or 'product_group'
        content_ids: ['{{$product->sku}}'], //array of one or more product ids in the page
        value: {!! intval($product->getFinalPoint($current_customer)) !!},  //OPTIONAL, but highly recommended
        currency: 'TWD' //REQUIRED if you a pass value
    });
    @endif

    @if(Route::currentRouteName() == 'customer-account-create-done')
      fbq('track', 'CompleteRegistration');
    @endif

</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=304234449734834&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->