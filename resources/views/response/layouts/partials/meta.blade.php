<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="author" content="">
<meta name="csrf-token" content="{!! csrf_token() !!}">
<title>{!! $seo_title !!}</title>
<meta name="title" content="{!! $seo_title !!}"/>
<meta name="description" content="{!! isset($seo_description) ? $seo_description : '' !!}"/>
<meta name="keywords" content="{!! isset($seo_keywords) ? $seo_keywords : '' !!}"/>
<meta property="og:title" content="{!! $seo_title !!}"/>
<meta property="og:description" content="{!! isset($seo_description) ? $seo_description : '' !!}"/>
<meta property="og:image" content="{!! isset($seo_image) ? $seo_image : '' !!}"/>
<meta property="og:url" content="{!! Request::url() !!}"/>
<meta property="og:type" content="website"/>
<meta property="og:locale" content="zh_TW"/>