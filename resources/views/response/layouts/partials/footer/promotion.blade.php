<footer>
    <div class="background">
        <div class="sitemap_ui">
            <ul>
                <li>
                    <dl>
                        <dt>摩托百貨</dt>
                        <dd>
                            <ul>
                                <li><a href="{{URL::route('summary', 'ca/1000')}}">改裝零件</a>
                                </li>
                                <li><a href="{{URL::route('genuineparts')}}">正廠零件</a>
                                </li>
                                <li><a href="{{URL::route('summary', 'ca/3000')}}">騎士用品</a>
                                </li>
                                <li><a href="{{URL::route('brand')}}">精選品牌</a>
                                </li>
                                <li><a href="{{URL::route('review')}}">商品評論</a>
                                </li>
                                <li><a href="{{URL::route('collection')}}">流行特輯</a>
                                </li>
                                <li><a href="{{URL::route('benefit')}}">會員好康</a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt>車型索引</dt>
                        <dd>
                            <ul>
                                <li><a href="{{URL::route('motor-manufacturer', ['HONDA', '50'])}}">HONDA</a>
                                </li>
                                <li><a href="{{URL::route('motor-manufacturer', ['YAMAHA', '50'])}}">YAMAHA</a>
                                </li>
                                <li><a href="{{URL::route('motor-manufacturer', ['SUZUKI', '50'])}}">SUZUKI</a>
                                </li>
                                <li><a href="{{URL::route('motor-manufacturer', ['KAWASAKI', '50'])}}">KAWASAKI</a>
                                </li>
                                <li><a href="{{URL::route('motor-manufacturer', ['HARLEY-DAVIDSON', '00'])}}">HARLEY-DAVIDSON</a>
                                </li>
                                <li><a href="{{URL::route('motor-manufacturer', ['BMW', '00'])}}">BMW</a></li>
                                <li><a href="{{URL::route('motor-manufacturer', ['APRILIA', '00'])}}">APRILIA</a></li>
                                <li><a href="{{URL::route('motor-manufacturer', ['DUCATI', '00'])}}">DUCATI</a></li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt>經銷據點</dt>
                        @php
                            $dealer_categories = getDealerPositionsLink();
                        @endphp
                        <dd>
                            <ul>
                                @foreach($dealer_categories as $dealer_category)
                                    <li>
                                        <a href="{{URL::route('dealer')}}/{{$dealer_category->slug}}/">{{$dealer_category->name}}({{$dealer_category->count}})</a>
                                    </li>
                                @endforeach
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt>流行特輯</dt>
                        @php
                            $assortment_types = getAssortmentTypesLink();
                        @endphp
                        <dd>
                            <ul>
                                @foreach($assortment_types as $assortment_type)
                                    <li><a href="{{route('collection-type', $assortment_type->url_rewrite)}}" title="{{$assortment_type->name . $tail}}">{{$assortment_type->name}}</a></li>
                                @endforeach
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li>
                    <dl>
                        <dt>摩托新聞</dt>
                        <dd>
                            <ul>
                                <li><a href="http://www.webike.tw/bikenews">獨家新聞</a>
                                </li>
                                <li><a href="http://www.webike.tw/bikenews/sysmsg/">系統公告</a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/WebikeTaiwan" target="_blank" rel="nofollow">「Webike台灣」粉絲版</a>
                                </li>
                                <li>
                                    <a href="http://www.jir.mc/" target="_blank" rel="nofollow">Teluru Team JiR Webike</a>
                                </li>
                                <li>
                                    <a href="http://norick.webike.net/" target="_blank" rel="nofollow">Webike Team Norick Yamaha</a>
                                </li>
                                <li>
                                    <a href="http://ameblo.jp/motoxtreme/" target="_blank" rel="nofollow">MotoExtrenme OGA</a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li class="big">
                    <dl>
                        <dt>合作夥伴</dt>
                        <dd class="photos">
                            <ul>
                                <li>
                                    <a rel="nofollow" href="http://www.dhl.com.tw/zt.html" target="_blank" title="DHL">
                                        <img src="https://www.webike.tw/assets/images/shared/footer_DHL.png" alt="DHL">
                                    </a>
                                </li>
                                <li>
                                    @if(hasChangedLogistic())
                                        <a rel="nofollow" href="https://www.hct.com.tw/Search/SearchGoods_n.aspx" target="_blank" title="新竹物流">
                                            <img src="https://www.webike.tw/assets/images/shared/ls-clear.png" alt="新竹物流">
                                        </a>
                                    @else
                                        <a rel="nofollow" href="http://www.e-can.com.tw/" target="_blank" title="台灣宅配通">
                                            <img src="https://www.webike.tw/assets/images/shared/ls-clear.png" alt="台灣宅配通">
                                        </a>
                                    @endif
                                </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt>安全防護</dt>
                        <dd class="photos">
                            <ul>
                                <li>
                                    <a rel="nofollow" href="http://www.geotrust.com/" target="_blank" title="安全防護Security-geotrust">
                                        <img src="https://www.webike.tw/assets/images/shared/geo-logo-clear.png" alt="GeoTrust">
                                    </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </li>
                <li class="big">
                    <dl>
                        <dt>加盟組織</dt>
                        <dd class="photos">
                            <ul>
                                <li>
                                    <a rel="nofollow" href="http://www.maat.org.tw/" target="_blank" title="台灣二輪部品同業促進協會">
                                        <img src="https://www.webike.tw/assets/images/shared/MAAT-clear.png" alt="台灣二輪部品同業促進協會">
                                    </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                    <dl>
                        <dt>營運公司</dt>
                        <dd class="photos">
                            <ul>
                                <li>
                                    <a rel="nofollow" href="http://www.everglory.asia/" target="_blank" title="榮芳興業有限公司">
                                        <img src="https://www.webike.tw/assets/images/shared/eg_logo-clear.png" alt="榮芳興業有限公司">
                                    </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <div class="tail">
        <p>
            <a href="http://www.everglory.asia/" target="_blank" rel="nofollow">
                EverGlory
            </a>
            © 2013 EverGlory Motors.All Rights Reserved.
        </p>
    </div>
</footer>