<footer id="footer" style="padding:0">
    <div class="col-xs-12 col-sm-12 col-md-12 ct-footer-top">
        <div class="container">
            <ul class="nav navbar-nav navbar-left menu-top">
                <li><a class="" href="{{route('home')}}"> Webike 台灣 </a></li>
                <li><a class="" href="{{route('shopping')}}"> 摩托百貨 </a></li>
                <li><a class="" href="{{MOTOMARKET}}"> 摩托車市 </a></li>
                <li><a class="" href="{{BIKENEWS}}"> 摩托新聞 </a></li>
                <li><a class="" href="{{MOTOCHANNEL}}"> 摩托頻道 </a></li>
                <li><span class="last-text">© {{ date('Y') }} EverGlory Motors. All Rights Reserved.</span></li>
            </ul>
        </div>
    </div>
</footer>