<style>
	@media (min-width: 768px) {
		.footer .li-product-sale {
	    	margin-left:5%;
	    	margin-bottom:40px;
	    }
	}
	.otherlink {
    	margin-top:10px;
    	height: 70px;
		overflow: auto;
    }
	.othertitle {
    	margin-bottom:10px;
    }
    .footer {
    	background-color: white;
    }
    .footer .collectiontext {
    	margin-top:40px;
    	margin-bottom:-20px;
    }
    .footer .btn {
    	width:200px;
    	height:40px;
    	padding-top:10px;
    }
    .allbtn {
    	margin-top:40px;
    }
    .allbtn ul{
    	list-style-type: none;
    }
    .footer span.dotted-text1{
    	display:block;
    	margin-bottom:10px;
    }
    .imgcontainer{
    	width:100% !important;
    }
	.dotted-text3.force-limit{
		height: 70px !important;
	}
	@media (max-width: 768px) {
	    .footer .content-box-sale {
	    	margin-bottom:0px;
	    }
	    .moreCollection {
			margin:20px 0 10px 0;
		}
		.loginbar li{
			margin:10px 0px;
			padding:0px;
		}
		.allbtn {
			margin-top:0px;
		}
	}
</style>
<div class="footer" id="footer">
	<div class="container">
		<div class="collectiontext text-center font-bold">
        	<h2>特輯情報</h2>
        </div>
        <div class="section clearfix">
			<div id="item1" class="ct-sale-page form-group clearfix">
			    <div class="content-box-sale">
					@php
						$num = 0;
				 	@endphp
			        <ul class="product-sale">
						@foreach($collections as $key => $collection)
							@if($num != 0 and $num % 2 == 0)
								</ul>
								<ul class="product-sale">
							@endif
							<li class="li-product-sale col-xs-12 col-sm-12 col-md-5">
								<span class=" dotted-text1 font-bold size-12rem text-center collection-title">{{$collection->name}}</span>
								<a href="{{$collection->link}}?rel=2017-10-2017Fall" title="{{$collection->name.$tail}}" target="_blank" class="clearfix">
									<div class="photo-box">
										<ul class="clearfix">
											<li class="col-xs-12 col-sm-12 col-md-12 imgcontainer">
												<figure class="zoom-image">
													<img src="{!! $collection->banner !!}" alt="{{$collection->name.$tail}}">
												</figure>
											</li>
										</ul>
									</div>
								</a>
								<div class="otherlink">
									<span class="dotted-text3 force-limit">{{$collection->meta_description}}</span>
								</div>
							</li>
						@php
							$num++;
						@endphp
						@endforeach
			        </ul>
			    </div>

			    <div class="col-xs-12 col-sm-12 col-md-12 text-center moreCollection">
					<a class="btn btn-info" href="{{URL::route('collection')}}?rel=2017-10-2017Fall" target="_blank">更多精選特輯</a>
				</div>
				@if(!$current_customer)
					<div class="allbtn col-xs-12 col-sm-12 col-md-12">
						<ul class="clearfix loginbar">
							<li class="col-xs-12 col-sm-4 col-md-4 text-center">
								<a class="btn btn-danger" href="{{URL::route('customer-account-create')}}?rel=2017-10-2017Fall" target="_blank">新會員募集</a>
							</li>
							<li class="col-xs-12 col-sm-4 col-md-4 text-center">
								<a class=" btn btn-default btn-warning border-radius-2 btn-signin" href="{{URL::route('login')}}?rel=2017-10-2017Fall" target="_blank">登入</a>
							</li>
							<li class="col-xs-12 col-sm-4 col-md-4 text-center">
								<a class="btn btn-default btn-primary border-radius-2 btn-login-with-face btn-full" href="{{URL::route('login-facebook')}}?rel=2017-10-2017Fall" target="_blank">Facebook 登入</a>
							</li>
						</ul>
					</div>
				@endif 
			</div>
		</div>
		
	</div>
</div>