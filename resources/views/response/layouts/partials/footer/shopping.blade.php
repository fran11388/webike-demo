<footer class="footer foot-shopping col-xs-12 col-sm-12 col-md-12">
    <div class="footer-links col-xs-12 col-sm-12 col-md-12">
        <div class="container">
            <aside class="col-xs-12 col-sm-12 col-md-4">
                <a href="{{route('home')}}" id="footer-logo">
                    {!! lazyImage( assetRemote('image/logo.png') , \Everglory\Constants\SEO::WEBIKE_TW_LOGO_TEXT )  !!}
                    {{--<img src="{!! assetRemote('image/logo.png') !!}" alt="{{\Everglory\Constants\SEO::WEBIKE_TW_LOGO_TEXT}}"/>--}}
                </a>

                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i>Good ride, Good life! 台灣最大級摩托車線上百貨</i></p>

                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i>營運公司：<a href="http://www.everglory.asia/" title="榮芳興業有限公司" target="_blank" rel="nofollow">榮芳興業有限公司</a></i></p>

                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'order'])}}">訂單及配送問題 </a></i></p>
                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'service'])}}">網站操作及商品諮詢</a></i></p>
                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'support'])}}">售後服務相關問題</a></i></p>
                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'biz'])}}">經銷夥伴招募</a></i></p>
                <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'supply'])}}">供應商合作洽談</a></i></p>

                <?php /*
                @if(Auth::check())
                    <a class="btn btn-warning btn-full" href="{{route('customer')}}" title="線上客服諮詢{{$tail}}">
                        會員中心
                    </a>
                @else
                    <a id="footer-signup" class="bg-red bg-color-red" href="{{route('customer-account-create')}}" title="註冊會員{{$tail}}">
                        註冊會員
                    </a>
                    <a class="btn-upload-moto" href="{{route('login')}}" title="會員登入{{$tail}}">
                        會員登入
                    </a>
                @endif
                 
                <a class="btn btn-asking btn-full" href="{{route('customer-service-proposal')}}" title="線上客服諮詢{{$tail}}">
                    線上客服諮詢 <i class="fa fa-comments-o size-12rem"></i>
                </a>*/
                ?>
            </aside>
            <div class="block col-xs-8 col-sm-8 col-md-8 visible-md visible-lg">
                <div class="column col-xs-3 col-sm-3 col-md-3 first">
                    <h5 class="title">
                        摩托百貨
                    </h5>
                    <ul>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('motor')}}" title="車型索引{{$tail}}">車型索引</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('summary', 'ca/1000')}}" title="改裝零件{{$tail}}">改裝零件</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('summary', 'ca/3000')}}" title="騎士用品{{$tail}}">騎士用品</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('genuineparts')}}" title="正廠零件{{$tail}}">正廠零件</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('outlet')}}" title="Outlet{{$tail}}">Outlet</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('collection')}}" title="流行特輯{{$tail}}">流行特輯</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('review')}}" title="商品評論{{$tail}}">商品評論</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('benefit')}}" title="會員好康{{$tail}}">會員好康</a></li>
                    </ul>
                </div>

                <div class="column col-xs-3 col-sm-3 col-md-3">
                    <h5 class="title">車型索引</h5>
                    <ul>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['HONDA', '50'])}}" title="HONDA{{$tail}}">HONDA</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['YAMAHA', '50'])}}" title="YAMAHA{{$tail}}">YAMAHA</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['SUZUKI', '50'])}}" title="SUZUKI{{$tail}}">SUZUKI</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['KAWASAKI', '50'])}}" title="KAWASAKI{{$tail}}">KAWASAKI</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['HARLEY-DAVIDSON', '00'])}}" title="HARLEY-DAVIDSON{{$tail}}">HARLEY-DAVIDSON</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['DUCATI', '00'])}}" title="DUCATI{{$tail}}">DUCATI</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['APRILIA', '00'])}}" title="APRILIA{{$tail}}">APRILIA</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['BMW', '00'])}}" title="BMW{{$tail}}">BMW</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['KYMCO', '00'])}}" title="光陽(KYMCO){{$tail}}">光陽</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['TW_YAMAHA', '00'])}}" title="台灣山葉{{$tail}}">台灣山葉</a></li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('motor-manufacturer', ['SYM', '00'])}}" title="三陽(SYM){{$tail}}">三陽</a></li>
                    </ul>

                </div>

                <div class="column col-xs-3 col-sm-3 col-md-3">
                    <h5 class="title">流行特輯</h5>
                    @php
                        $types = getAssortmentTypesLink();
                    @endphp
                    <ul>
                        @foreach($types as $type)
                            <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{route('collection-type', $type->url_rewrite)}}" title="{{$type->name . $tail}}">{{$type->name}}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="column col-xs-3 col-sm-3 col-md-3">
                    <h5 class="title">經銷據點</h5>
                    <?php
                        $dealer_categories = getDealerPositionsLink();
                    ?>
                    <ul>
                        @foreach ($dealer_categories as $category)
                            <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="{{URL::route('dealer')}}/{{$category->slug}}/">{{$category->name}}({{$category->count}})</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <hr>
            <div class="col-xs-12 col-sm-12 col-md-12 next-row">
                <div class="col-xs-12 col-sm-2 col-md-2 hidden-md hidden-sm hidden-lg">
                    <h5 class="title text-center">追蹤我們</h5>
                    <div>
                        <span class="channelicon">
                        <span><a href="https://www.facebook.com/WebikeTaiwan/" target="_blank" rel="nofollow"> <img src="{{assetRemote('image/channelicon/facebook.png')}}" style="width: 15%;"></a></span>
                        <span><a href="https://line.me/R/ti/p/%40uqv6347i" target="_blank" rel="nofollow"> <img src="{{assetRemote('image/channelicon/line.png')}}" style="width: 15%;"></a></span>
                        <span><a href="https://www.instagram.com/webike_taiwan/?utm_source=ig_profile_share&igshid=wwl8jdoybu8i" target="_blank" rel="nofollow"> <img src="{{assetRemote('image/channelicon/instagram.png')}}" style="width: 15%;"></a></span>
                        </span>
                    </div>
                </div> 
                <div class="col-xs-12 col-sm-2 col-md-1">
                    <h5 class="title">加盟組織</h5>
                    <a href="http://www.maat.org.tw/" title="台灣二輪部品同業促進協會" target="_blank" rel="nofollow">
                        {!! lazyImage( assetRemote('image/channelicon/footer-icon/MAAT-clear.png') , "台灣二輪部品同業促進協會",'' ,'','max-width:60px;')  !!}
                        {{--<img src="{{assetRemote('image/channelicon/footer-icon/MAAT-clear.png')}}" alt="台灣二輪部品同業促進協會" style="max-width:60px;">--}}
                    </a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2">
                    <h5 class="title text-center">安全防護</h5>
                    <div class="vertical-middle">
                        {!! lazyImage( assetRemote('image/channelicon/footer-icon/geo-logo-clear.png') , "台灣二輪部品同業促進協會",'' ,'','max-width:60px;')  !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 text-center">
                    <h5 class="title">本站法律顧問</h5>
                    <a href="http://www.giant-group.com.tw/index.html" title="本站法律顧問" target="_blank" rel="nofollow">
                        {!! lazyImage( assetRemote('image/channelicon/footer-icon/giant_logo.png') , "本站法律顧問",'' ,'','max-width:120px;')  !!}
                    </a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2">
                    <h5 class="title text-center">事業據點</h5>
                    <a href="https://www.rivercrane.com/" title="RiverCrane" target="_blank" rel="nofollow">
                        {!! lazyImage( assetRemote('image/channelicon/footer-icon/footer_rc_logo.gif') , "RiverCrane",'' ,'','max-width:200px;')  !!}
                    </a>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2">
                    <h5 class="title text-center">營運公司</h5>
                    <a href="http://www.everglory.asia/" title="EverGlory-榮芳興業有限公司" target="_blank" rel="nofollow">
                        {!! lazyImage( assetRemote('image/channelicon/footer-icon/eg_logo-clear.png') , "EverGlory-榮芳興業有限公司",'' ,'','max-width:160px;')  !!}
                    </a>                        
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 hidden-xs">
                    <h5 class="title text-center">追蹤我們</h5>
                    <div>
                        <span class="channelicon">
                        <span><a href="https://www.facebook.com/WebikeTaiwan/" target="_blank" rel="nofollow"> {!! lazyImage( assetRemote('image/channelicon/facebook.png') , '','' ,'','width: 24%;')  !!}</a></span>
                        <span><a href="https://line.me/R/ti/p/%40uqv6347i" target="_blank" rel="nofollow"> {!! lazyImage( assetRemote('image/channelicon/line.png') , '','' ,'','width: 24%;')  !!}</a></span>
                        <span><a href="https://www.instagram.com/webike_taiwan/?utm_source=ig_profile_share&igshid=wwl8jdoybu8i" target="_blank" rel="nofollow"> {!! lazyImage( assetRemote('image/channelicon/instagram.png') , '','' ,'','width: 24%;')  !!}</a></span>
                        </span>
                    </div>
                </div> 
                {{-- <div class="col-xs-12 col-sm-4 col-md-3">
                    <h5 class="title">追蹤我們</h5>
                    <div class="fb-like" data-href="https://www.facebook.com/WebikeTaiwan/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.8&appId=791319474279962";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="copyright col-xs-12 col-sm-12 col-md-12">
        <h3 class="text-center">EverGlory © {{ date('Y') }} EverGlory Motors.All Rights Reserved.</h3>
    </div>

    {{--<div class="copyright col-xs-12 col-sm-12 col-md-12">--}}
        {{--<div class="container">--}}
            {{--<div class="block col-xs-12 col-sm-12 col-md-6">--}}
                {{--<h5>Contact Information</h5>--}}

                {{--<p>--}}
                    {{--<i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                    {{--20th Floor, Havana building, 132 Ham Nghi st., Dist. 1, HCMC, Vietnam.--}}
                {{--</p>--}}

                {{--<p>--}}
                    {{--<i class="fa fa-envelope" aria-hidden="true"></i>--}}
                    {{--Email: <a class="footer-email" href="mailto:support@webike.vn">support@webike.vn</a>--}}
                {{--</p>--}}

                {{--<p><i class="fa fa-clock-o" aria-hidden="true"></i> From 2 - 6: 8:00 AM - 17:30 PM</p>--}}

                {{--<p class="footer-numberphone">--}}
                    {{--<i class="fa fa-phone" aria-hidden="true"></i>--}}
                    {{--Hotline: <a class="hotline" href="tel:+(84-8)-3821-0587">+(84-8)-3821-0587</a>--}}
                    {{-----}}
                    {{--<a class="hotline" href="tel:0932 979 060">0932 979 060</a>--}}
                    {{-----}}
                    {{--<a class="hotline" href="tel:0932 979 010">0932 979 010</a>--}}
                {{--</p>--}}
            {{--</div>--}}
            {{--<div class="block title-connectwebike right col-xs-3 col-sm-3 col-md-3 visible-md visible-lg">--}}
                {{--<h5>Connect with Webike VietNam</h5>--}}
                {{--<ul class="social">--}}
                    {{--<li><a href="https://www.facebook.com/webike.vn?fref=ts" class="facebook" title="Facebook"--}}
                           {{--target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                    {{--<li><a href="https://www.youtube.com/channel/UC1baB-6MDOlGxV7IlSuDtsQ" class="youtube"--}}
                           {{--title="Youtube" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>--}}
                {{--</ul>--}}
                {{--<a class="btn-mobile-view" href="#"><i class="fa fa-mobile" aria-hidden="true"></i>--}}
                    {{--Mobile version</a>--}}
            {{--</div>--}}
            {{--<div id="quick-contact" class="block right col-xs-3 col-sm-3 col-md-3 visible-md visible-lg">--}}
                {{--<div class="guide">--}}
                    {{--<h5>Telephone support</h5>--}}

                    {{--<p>Fill your phone number. We will call you within 24 hours.</p>--}}
                {{--</div>--}}

                {{--<form action="#" method="post" novalidate class="ng-pristine ng-valid">--}}
                    {{--<div class="form-group">--}}
                        {{--<input type="text" required="required" class="input-phone-number" name="phone_number"--}}
                               {{--placeholder="Phone number" value="">--}}
                        {{--<button type="submit" class="module-btn bnt-bg-red-hover">Submit</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

</footer>