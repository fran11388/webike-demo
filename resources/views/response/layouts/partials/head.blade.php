<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-language" content="zh-tw">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

<meta name="author" content="">
<meta name="csrf-token" content="{!! csrf_token() !!}">

<!-- #FAVICONS -->
<link rel="shortcut icon" href="{{ assetPathTranslator('favicon.ico', config('app.env')) }}" type="image/x-icon">
<link rel="icon" href="{{ assetPathTranslator('favicon.ico', config('app.env')) }}" type="image/x-icon">

@if(\Request::route()->getName() == 'summary' || \Request::route()->getName() == 'product-detail')
	<link rel="canonical" href="{{\URL::current()}}">
@endif

<title>{{ $seo_title }}</title>
<meta name="title" content="{{ $seo_title }}"/>
<meta name="description" content="{{ isset($seo_description) ? $seo_description : '' }}"/>
<meta itemprop="name" content="Webike-摩托百貨">
<meta itemprop="description" content="{{ isset($seo_description) ? $seo_description : '' }}">
<meta itemprop="image" content="{{ isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE }}">
<meta property="og:title" content="{{ $seo_title }}"/>
<meta property="og:description" content="{{ isset($seo_description) ? $seo_description : '' }}"/>
<meta property="og:image" content="{{ isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE }}"/>
<meta property="og:url" content="{!! Request::url() !!}"/>
<meta property="og:site_name" content="Webike-摩托百貨"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="Webike-摩托百貨"/>
<meta property="og:locale" content="zh_TW"/>
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="{{ $seo_title }}"/>
<meta name="twitter:description" content="{{ isset($seo_description) ? $seo_description : '' }}"/>
<meta name="twitter:image" content="{{ isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE }}"/>
<meta name="twitter:url" content="{!! Request::url() !!}"/>

<!-- Bootstrap Core CSS -->
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/basic/bootstrap.min.css') }}"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Custom Fonts -->
<!-- <link href="{{ asset('plugin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Font Awesome -->
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css">

<!-- Bootstrap Core CSS -->
<!-- <link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/owl-carousel/assets/owl.carousel.css') }}"> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
<!-- <link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/slick/slick.css') }}"> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">
<!-- <link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/slick/slick-theme.css') }}"> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css">

<!-- <link rel="stylesheet" href="{{ assetRemote('plugin/select2/css/select2.min.css') }}"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
<!-- <link rel="stylesheet" href="{{ assetRemote('plugin/sweetalert2/sweetalert2.min.css') }}"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.1.1/sweetalert2.min.css">
<link rel="stylesheet" href="{{ assetRemote('plugin/jQuery.mmenu-master/dist/jquery.mmenu.all.css') }}">

<!-- Custom CSS -->
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/main.css') }}">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/css-rewrite.css') }}">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/common-script.css') }}">

<!-- js preload -->
<?php /*<!--[if lt IE 9]>
<!-- <script src="{{ assetRemote('js/basic/jquery-1.10.2.min.js') }}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--[endif]-->
<!--[if gte IE 9]>
<!-- <script src="{{ assetRemote('js/basic/jquery-2.2.4.min.js') }}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!--[endif]-->
<!--[if !IE]> -->
<!-- jQuery -->
<!-- <script src="{{ assetRemote('js/basic/jquery.js') }}"></script> -->
 */ ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- owl carousel -->
<!-- <script src="{{ assetRemote('plugin/owl-carousel/owl.carousel.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>


<?php /*
@include('response.common.dfp.ad-script')
*/ ?>

<!-- Piwik -->
<!--
<script type="text/javascript">
	var _paq = _paq || [];
	@if(isset($current_customer) and $current_customer)
		_paq.push(['setUserId', 'UID-{{ ($current_customer->id + 9487 ) }}']);
	@endif
</script>
 -->
<!-- End Piwik Code -->