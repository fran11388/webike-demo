<head>
    <style>
        #mainNav .navbar-header button.active {
            color: #e61e25;
        }
    </style>
</head>
<!-- Header -->
@if($current_customer and $current_customer->role_id == \Everglory\Constants\CustomerRole::STAFF)
    <div class="hidden-xs staff-icon">
        Staff
    </div>
@endif
<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top affix-top header nav-down">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll header-top">
        <button type="button" class="navbar-toggle navbar-left icon-menu" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
            <i class="fa fa-bars size-19rem hidden-xs" aria-hidden="true"></i>
            <i class="fa fa-bars fa-bars-list-icon size-15rem hidden-lg hidden-md hidden-sm" aria-hidden="true"></i>
        </button>
        <a href="{{ route('shopping') }}" class="logo-mobile"><img src="{!! assetRemote('image/logo.png') !!}"
                                                                   alt="「Webike-摩托百貨」"/></a>
        <ul class="nav top-nav icon-top-page">
            @include('response.layouts.partials.header.login-icon')
            @include('response.layouts.partials.header.cart-icon')
            <li class="helpdesk">
                <a href="javascript:void(0)" class="icon-search">
                    <i class="glyphicon glyphicon-search size-19rem hidden-xs"></i>
                    <i class="glyphicon glyphicon-search size-15rem hidden-lg hidden-md hidden-sm"></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse header-top" id="bs-example-navbar-collapse-1">
        <div class="container">
            <div class="overflow-menu-ui">
                <ul class="nav navbar-nav navbar-left overflow-menu">
                    <li><a class="hidden-xs" href="{!! route('home') !!}"> Webike 台灣 </a></li>
                    <li class="navbar-active hidden-xs link-shopping">
                        <a href="{!! route('shopping') !!}"> 摩托百貨</a>
                    </li>
                    <li class="hidden-xs">
                        <a href="{!! MOTOMARKET !!}"> 摩托車市</a>
                    </li>
                    <li><a class="hidden-xs" href="{!! BIKENEWS !!}"> 摩托新聞 </a></li>
                    <li>
                        <a class="hidden-xs" href="{!! MOTOCHANNEL !!}" target="_blank">
                            摩托頻道
                        </a>
                    </li>
                    <li><a class="hidden-xs" href="{{ route('benefit-event-motogp-2019') }}" target="_blank"
                           onclick="ga('send', 'event', 'link', 'motogp-2019', 'header-pc');"> 2019 MotoGP</a></li>

                    <li><a class="hidden-xs" href="{{ route('collection-suzuka2018') }}" target="_blank"> 2018 鈴鹿8耐</a>
                    </li>
                    @if($current_customer)
                        @if(count($current_customer->motors))
                            <li class="dropdown hidden-sm hidden-md hidden-lg">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                                    MyBike快速搜尋<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    @foreach($current_customer->motors as $motor)
                                        <li>
                                            <a href="{{ route('parts',[ 'mt/'.$motor->url_rewrite ]) }}?sort=new">
                                                {{ $motor->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                    <li>
                                        <a href="{{ route('customer-mybike') }}">
                                            <i class="fa fa-edit" aria-hidden="true"></i> 編輯MyBike
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @else

                        @endif
                    @endif
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{{ route('motor') }}"> 車型索引 </a></li>

                    @foreach(\Everglory\Constants\Category::TOP as $top_category_url_rewrite)
                        <?php
                        $menu_category = \Ecommerce\Repository\CategoryRepository::find($top_category_url_rewrite);
                        ?>
                        <li class="dropdown hidden-sm hidden-md hidden-lg">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
                               aria-expanded="true"> {{$menu_category->name}}<span class="caret"></span></a>
                            @php
                                $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByUrlPath($menu_category->mptt->url_path, $menu_category->depth +1);
                            @endphp
                            <ul class="dropdown-menu">
                                @foreach($menu_subcategories as $menu_subcategory)
                                    <li>
                                        <a href="{{route('summary', ['ca/' . $menu_subcategory->url_path])}}"
                                           title="{{$menu_subcategory->name . $tail}}">
                                            {{$menu_subcategory->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{!! URL::route('genuineparts') !!}"> 正廠零件 </a>
                    </li>
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{!! URL::route('outlet') !!}"> Outlet </a></li>
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{{ route('benefit') }}" target="_blank">
                            會員好康 </a></li>
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{{ route('ranking') }}" target="_blank">
                            銷售排行 </a></li>
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{{URL::route('collection')}}"> 流行特輯 </a></li>
                    <li><a class="hidden-sm hidden-md hidden-lg" href="{{URL::route('dealer')}}"> 經銷據點 </a></li>
                    <li class="dropdown hidden-sm hidden-md hidden-lg">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"> 其他服務<span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="" href="{{ route('benefit-event-motogp-2019') }}" target="_blank"
                                   onclick="ga('send', 'event', 'link', 'motogp-2019', 'header-mobile');"> 2019
                                    MotoGP</a>
                            </li>
                            <li>
                                <a class="" href="{{ route('collection-suzuka2018') }}" target="_blank"> 2018 鈴鹿8耐</a>
                            </li>
                            <li>
                                <a name="review" href="{{route('review')}}" title="商品評論{{$tail}}">商品評論</a>
                            </li>
                            <li>
                                <a class="" href="{!! MOTOMARKET !!}"> 摩托車市 </a>
                            </li>
                            <li>
                                <a class="" href="{!! BIKENEWS !!}"> 摩托新聞 </a>
                            </li>
                            <li>
                                <a class="" href="{!! MOTOCHANNEL !!}" target="_blank">
                                    摩托頻道
                                </a>
                            </li>
                            <li>
                                <a class="" href="{!! route('home') !!}"> 「Webike 台灣」首頁 </a>
                            </li>
                        </ul>
                    </li>
                    @if(\Auth::check())
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('logout')}}"><i
                                        class="glyphicon glyphicon-log-out size-85rem"></i> 登出</a></li>
                    @else
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('login')}}"><i
                                        class="glyphicon glyphicon-user size-85rem"></i> 會員登入</a></li>
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('customer-account-create')}}"><i
                                        class="glyphicon glyphicon-plus-sign size-85rem"></i> 加入會員</a></li>
                    @endif
                </ul>
                <ul class="nav nav-pills overflow-menu-collect hidden-xs">
                    <li class="dropdown">
                        <a role="button" href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
                            More <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu overflow-sub-menu"></ul>
                    </li>
                </ul>
                <span class="goodlife visible-lg visible-md">
                        Good ride, Good life!<span class="hidden-md hidden-sm hidden-xs">  台灣最大級摩托車線上百貨</span>
                </span>
                <div class="hidden-md hidden-sm hidden-xs channel" style="width: 2%;"><a
                            href="https://www.facebook.com/WebikeTaiwan/" target="_blank" rel="nofollow"> <img
                                src="{{assetRemote('image/channelicon/facebook.png')}}"></a></div>
                <div class="hidden-md hidden-sm hidden-xs channel" style="width: 2%;"><a
                            href="https://line.me/R/ti/p/%40uqv6347i" target="_blank" rel="nofollow"> <img
                                src="{{assetRemote('image/channelicon/line.png')}}"></a></div>
                <div class="hidden-md hidden-sm hidden-xs channel" style="width: 2%;"><a
                            href="https://www.instagram.com/webike_taiwan/?utm_source=ig_profile_share&igshid=wwl8jdoybu8i"
                            target="_blank" rel="nofollow"> <img
                                src="{{assetRemote('image/channelicon/instagram.png')}}"></a></div>
            </div>
        </div>
    </div>
@include('response.layouts.partials.header.ad-bar')
<!-- /.navbar-collapse -->
    <div class="container container-header">
        <div class="col1-logo">
            <a class="btn-show-second-menu">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </a>
            <a href="{{ route('shopping') }}" class="logo logo-webike-shopping-69">

            </a>
        </div>
        <div class="col2-search">
            <div class="box-search col-md-12">
                @include('response.layouts.partials.header.searchbar')
            </div>
        </div>

        @include('response.common.plugin.suggest')

        @include('response.common.head-icon')

    </div>
    <div class="menu-shopping">
        <hr>
        <div class="container">
            <div class="overflow-menu-ui">
                <ul class="overflow-menu">
                    <li>
                        <a href="{{ route('motor') }}"><h3 class="text-second-menu">車型索引</h3></a>
                        @php
                            $motor_manufacturers = \Ecommerce\Repository\MotorRepository::selectManufacturersByUrlRewrite(\Everglory\Constants\ViewDefine::MAIN_MOTOR_MANUFACTURERS);
                        @endphp
                        <ul class="dropdown-second-menu">
                            @foreach($motor_manufacturers as $motor_manufacturer)
                                <li>
                                    <a href="{{route('motor-manufacturer', [$motor_manufacturer->url_rewrite, '00'])}}"
                                       title="{{$motor_manufacturer->name . $tail}}">
                                        {{$motor_manufacturer->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @foreach(\Everglory\Constants\Category::TOP as $top_category_url_rewrite)
                        <?php
                        $menu_category = \Ecommerce\Repository\CategoryRepository::find($top_category_url_rewrite);
                        ?>
                        <li>
                            <a href="{{ route('summary',[ 'ca/'.$menu_category->url_rewrite ]) }}"
                               title="{{$menu_category->name}}{{$tail}}"><h3
                                        class="text-second-menu">{{$menu_category->name}}</h3></a>
                            @php
                                $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByUrlPath($menu_category->mptt->url_path, $menu_category->depth +1);
                            @endphp
                            <ul class="dropdown-second-menu">
                                @foreach($menu_subcategories as $menu_subcategory)
                                    <li>
                                        <a href="{{route('summary', ['ca/' . $menu_subcategory->url_path])}}"
                                           title="{{$menu_subcategory->name . $tail}}">
                                            {{$menu_subcategory->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                    <li>
                        @include ( 'response.layouts.partials.header.tag-sale' )
                        <a href="{!! URL::route('genuineparts') !!}" title="正廠零件{{$tail}}"><h3 class="text-second-menu">
                                正廠零件</h3></a>
                        <ul class="dropdown-second-menu">
                            <li>
                                <a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::IMPORT['url_code']]) !!}"
                                   title="進口正廠零件 - 「Webike-摩托百貨」">進口正廠零件</a></li>
                            <li>
                                <a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::DOMESTIC['url_code']]) !!}"
                                   title="國產正廠零件 - 「Webike-摩托百貨」">國產正廠零件</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{!! URL::route('outlet') !!}" title="Outlet{{$tail}}"><h3 class="text-second-menu">
                                Outlet</h3></a>
                    </li>
                    <li>
                        <a href="{{route('brand')}}" title="精選品牌{{$tail}}"><h3 class="text-second-menu">精選品牌</h3></a>
                    </li>
                    <li>
                        <a href="{{route('ranking')}}" title="銷售排行{{$tail}}"><h3 class="text-second-menu">銷售排行</h3></a>
                    </li>
                    <li>
                        <a href="{{URL::route('collection')}}" title="流行特輯{{$tail}}"><h3 class="text-second-menu">
                                流行特輯</h3></a>
                    </li>
                    <li>
                        <a href="{{route('review')}}" title="商品評論{{$tail}}"><h3 class="text-second-menu">商品評論</h3></a>
                    </li>
                    <li>
                        <a href="{{route('benefit')}}" title="會員好康{{$tail}}"><h3 class="text-second-menu">會員好康</h3></a>
                    </li>
                    <li>
                        <a href="{{URL::route('dealer')}}" title="經銷據點{{$tail}}"><h3 class="text-second-menu">經銷據點</h3>
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-pills overflow-menu-collect">
                    <li class="dropdown">
                        <a role="button" href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
                            More <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu overflow-sub-menu"></ul>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
</nav>
<span id="bp_mobile" class="bp_checking"></span>
<script>
    // loading soon after show menu
    var current = window.location.href;
    var url_exp = false;
    var sub = '';
    switch (current.split('/')[3]) {
        case 'mf':
        case 'mt':
            current = document.location.origin + '/motor';
            break;
        case 'benefit':
            sub = window.location.href.split('/')[4];
            if (typeof sub != 'undefined' && sub.indexOf('outlet') !== -1) {
                url_exp = true;
                sub = 'outlet';
            }
            break;
        default:

    }

    $('#mainNav .navbar-header .navbar-toggle').click(function () {
        if (!$(this).hasClass('active')) {
            $('#mainNav .navbar-header .navbar-toggle').addClass('active');
        } else {
            $('#mainNav .navbar-header .navbar-toggle').removeClass('active')
        }
    });

    $('.overflow-menu-ui li:not(.hidden-xs)>a').each(function (key, a) {
        link = $(a).attr('href');
        if (link != document.location.origin && current.indexOf(link) !== -1) {
            if (url_exp) {
                if (link.indexOf(sub) !== -1) {
                    $(a).closest('li').addClass('current');
                }
            } else {
                if (window.matchMedia('(max-width: 767px)').matches) {
                    $(a).closest('li').addClass('current');
                } else {
                    if ($(a).parents('.menu-shopping').length) {
                        $(a).closest('li').addClass('current');
                    } else {
                        $(a).closest('li').addClass('navbar-active');
                    }
                }
            }

        }
    });

</script>

<div id="pagetop">
    <a href="javascript:void(0);">PAGE TOP</a>
    <i class="fa fa-angle-top" aria-hidden="true"></i>
</div>

<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::MOBILE_TAG, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
?>
@if($propaganda && (strpos(Route::currentRouteName(), 'product-detail') === false) and (strpos(Route::currentRouteName(), 'cart') === false))
    <div id="coupon">
        <a href="{{$propaganda->link}}" class="hidden-lg hidden-md hidden-sm ">
            <img src="{{$propaganda->pic_path}}" style="width: 140px;">
        </a>
    </div>
@endif


<script type="text/javascript">
    jQuery(function () {
        var coupon = $("#coupon");
        coupon.hide();
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 100) {
                coupon.fadeIn();
            } else {
                coupon.fadeOut();
            }
        });
    });
</script>
@if(!Auth::check()
    and (
        (strpos(Route::currentRouteName(), 'customer-fixer')  === false)
        and (Route::currentRouteName() !== 'customer-forgot-password')
        and (Route::currentRouteName() !== 'customer-reset-password')
    )
)
    @if(session()->has('invite-join') and session()->get('invite-join'))
        @include('response.common.login.pop-up')
    @endif
@endif


<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::POPUP, \Everglory\Constants\Propaganda\Position::PC_BLADE);
?>
@if(\Ecommerce\Core\Agent::isNotPhone() && $propaganda && (!session()->has('pop-ups-full-screen') and !session()->get('pop-ups-full-screen')))
    @include('response.common.pop-up-gp')
@elseif(!\Ecommerce\Core\Agent::isNotPhone())
    @include('response.layouts.partials.header.bannerbar-mobile') <!--橫條--!>
@endif



@if(
    (Route::currentRouteName() !== 'product-detail')
    and (Route::currentRouteName() !== 'cart')
    and (strpos(Route::currentRouteName(), 'benefit') === false)
)
    @include('response.common.bookmarks')
@endif
@include('response.common.disps')

        <!--End Header -->