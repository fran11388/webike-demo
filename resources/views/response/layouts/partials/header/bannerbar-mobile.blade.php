<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::STRIPE, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
?>
@if($propaganda && !session('bannerbar-close') && (strpos(Route::currentRouteName(), 'product-detail') === false) and (strpos(Route::currentRouteName(), 'cart') === false))
    <style type="text/css">
        .bannerbar-phone{
            position:fixed;
            bottom:0px;
            width:100%;
            z-index:9999;
        }
        .bannerbar-phone .fa-times-circle-o {
            position:absolute;
            z-index:2;
            right:0px;
            top:0px;
        }
        .bannerbar-phone .fa-times-circle-o:hover {
            cursor:pointer;
        }
        .bannerbar-phone .closebutton {
            position:absolute;
            width:45px;
            height:45px;
            right:0px;
            top:0px;
            cursor:pointer;
        }
    </style>
    {{--@if( $bar_obvious and strtotime('now') >= strtotime(date('2017-11-07')) and strtotime('now') < strtotime(date('2017-11-12')) and !session('bannerbar-read'))--}}
    <div class="visible-xs">
        <div class="bannerbar-phone">
            <div class="closebutton">
                <i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i>
            </div>
            {{-- @if(Route::currentRouteName() == 'cart')
                <a href="javascript:void(0)" onclick="slipTo('#cart')">
            @else --}}

            {{-- @endif --}}

                <a href="{{ $propaganda->link }}" target="_blank">
                    <img src="{{$propaganda->pic_path}}">
                </a>

        </div>
    </div>
    <script>
        $('.bannerbar-phone .fa-times-circle-o').click(function(){
            $.post("{{ route('customer-session','bannerbar-close') }}");
            $('.bannerbar-phone').fadeOut(500);
        });
    </script>
@endif


