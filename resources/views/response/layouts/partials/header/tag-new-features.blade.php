
<style>
    .tag-new-features{
        background: red;
        display: inline;
        position: absolute;
        right: -50%;
        top: 0;
        padding: 5px;
        opacity: 0.7;
        border-radius: 3px;
    }

    .tag-new-features:after{
        content: '';
        position: absolute;
        right: 100%;
        top: 7px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 7.5px 10px 7.5px 0;
        border-color: transparent #ff0000 transparent transparent;
    }

    .tag-new-features span{
        color: #fff
    }

    .icon-area .list li{
        position: relative;
    }
</style>

<div class="tag-new-features">
    <span>新功能</span>
</div>