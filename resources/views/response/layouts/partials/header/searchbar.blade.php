<!-- Google Website Search -->
<!--
<script>
    (function() {
        var cx = '006775458616325244567:qeruhud4weg';
        var gcse = document.createElement('script');
        gcse.type = 'text/javascript';
        gcse.async = true;
        gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gcse, s);
    })();
</script>
<gcse:searchbox-only cr="countryTW" resultsUrl="{{route('google')}}"></gcse:searchbox-only>
-->
<form method="GET" id="form_search">
    <?php
        $search_bars = [
            'Parts' => ['placeholder'=>'請輸入關鍵字(' . round(\Ecommerce\Repository\ProductRepository::getMainProductCount() / 10000) . '萬項商品、' . \Ecommerce\Repository\ManufacturerRepository::getManufacturerCount() . '家廠牌)','text'=>'找商品','current'=>false],
    'Reviews' => ['placeholder'=>'請輸入關鍵字(車友商品使用心得)','text'=>'找商品評論','current'=>false],
    'Moto Market' => ['placeholder'=>'請輸入關鍵字(摩托車專門交易網)','text'=>'找新車、中古車','current'=>false],
    'Moto News' => ['placeholder'=>'請輸入關鍵字(海內外最新摩托資訊)','text'=>'找新聞','current'=>false],
    'Google' => ['placeholder'=>'請輸入關鍵字(Webike全站所有內容)','text'=>'站內全文搜尋','current'=>false],
        ];

        switch (explode('-',\Route::currentRouteName())[0]) {
            case 'google':
                $search_bars['Google']['current'] = true;
                $placeholder = $search_bars['Google']['placeholder'];
                break;
            case 'review':
                $search_bars['Reviews']['current'] = true;
                $placeholder = $search_bars['Reviews']['placeholder'];
                break;
            default:
                $search_bars['Parts']['current'] = true;
                $placeholder = $search_bars['Parts']['placeholder'];
        }
    ?>
    <input type="text" id="search_bar" name="q" value="{{TransferToUtf8(request()->input('q'))}}" class="txt-search col-md-12" placeholder="{!! $placeholder !!}" autocomplete="off">
    <button class="btn btn-search bg-color-red animated" type="submit" title="搜尋"><i class="glyphicon glyphicon-search"></i><span> 搜尋</span></button>

    <select class="selection-search btn-label">
        @foreach($search_bars as $value => $option_data)
            <option class="selection-option" value="{{$value}}" data-placeholder="{!! $option_data['placeholder'] !!}" {{$option_data['current'] ? 'selected' : ''}}>{!! $option_data['text'] !!}</option>
        @endforeach
    </select>
    <script>
        $('#form_search').find('select').change(function(){
            $('#search_bar').attr('placeholder',$(this).find('option:selected').data('placeholder'));
        });
    </script>
</form>