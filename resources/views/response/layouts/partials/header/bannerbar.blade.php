<style>
  .bannerbar {
      margin-top: 150px;
      margin-bottom: 10px;
  }

  .breadcrumb-mobile {
      margin-top: 0px !important;
  }

  .bannerbar-container {
      width: 100%;
      height: 82px;

      <?php
      $propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::STRIPE, \Everglory\Constants\Propaganda\Position::PC_BLADE);
      ?>
      @if($propaganda)
    background: url({{cdnTransform($propaganda->pic_path)}});
      @endif

                      {{--@endif--}}
    background-position: center;
      background-repeat: no-repeat;
      position: relative;
      margin-bottom: 10px;
  }

  .homepage .bannerbar {
      margin-top: 0px;
  }

  @media (max-width: 767px) {
      .bannerbar {
          margin-top: 60px;
      }
  }
</style>
<div class="bannerbar">
    @if($propaganda)
        <a href="{{ $propaganda->link }}" target="_blank">
            <div class="bannerbar-container visible-md visible-lg visible-sm"></div>
        </a>
    @endif
</div>
