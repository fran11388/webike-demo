<!-- Header -->
<style>
    .container {
        width:1000px !important;
    }
    .search_bar {
        display:none;
    }
    .container-header li {
        list-style-type:none;
        float:left;
    }
    
    .searchHead .search {
        width: 100%;
        height: 30px;
    }
    
    .searchBtnHead .searchbtn{
        height: 30px;
        width: 100%;
        padding: 0;
    }
    
    @media (max-width: 768px) {
        .container {
            width:100% !important;
        }
        .container-header {
            background-color:black !important;
            padding: 15px 10px 15px 10px;
            width: 100%;
        }
        .search_bar {
            display:block;
        }
        .searchHead {
            width: 88%;
            margin-right: 3%;
        }
        .searchBtnHead {
            padding: 0px;
            width: 9%;
            margin-right: 10px;
        }
    }
    @media (max-width: 500px) {
        .container-header {
            background-color:black !important;
            padding: 16px 10px 10px 10px;
            width: 100%;
        }
        .search_bar {
            display:block;
        }
        .searchHead {
            padding: 0px;
            width: 78%;
            margin-right: 2%;
        }
        .searchBtnHead {
            padding: 0px;
            width: 20%;
            margin-right: 0px;
        }
    }

    @media (max-width: 1032px){
        .container {
            width:100% !important;
        }
    }

    .navbar-nav li a {
        padding:5px 10px;
    }
</style>
<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top affix-top header nav-down">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll header-top">
        <button type="button" class="navbar-toggle navbar-left icon-menu" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1"><i class="fa fa-bars size-15rem" aria-hidden="true"></i>
        </button>
        <a href="{{ route('shopping').'?'.$rel_parameter }}" class="logo-mobile"><img src="{!! assetRemote('image/logo.png') !!}" alt="logo"/></a>
        <ul class="nav top-nav icon-top-page">
            <li class="login"><a href="{{ $current_customer ? route('customer').'?'.$rel_parameter : route('login').'?'.$rel_parameter}}" class=""><i class="glyphicon glyphicon-user size-15rem"></i></a></li>
            <li><a href="{{ route('cart').'?'.$rel_parameter }}" class=""><i class="glyphicon glyphicon-shopping-cart size-15rem"></i></a></li>
            <li class="helpdesk"><a href="javascript:void(0)" class="icon-search"><i
                            class="glyphicon glyphicon-search size-15rem"></i></a></li>
        </ul>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse header-top" id="bs-example-navbar-collapse-1">
        <div class="container">
            <ul class="nav navbar-nav navbar-left width-full">
                <li><a class="" href="{!! route('shopping').'?'.$rel_parameter !!}" target="_blank">摩托百貨</a></li>
                <li>
                    <a name="motor"  href="{{ route('motor').'?'.$rel_parameter }}" title="車型索引{{$tail}}" target="_blank">車型索引</a>
                </li>
                <li>
                    <a name="summary" href="{{ route('summary',[ 'ca/1000' ]).'?'.$rel_parameter }}" title="改裝零件{{$tail}}" target="_blank">改裝零件</a>
                </li>
                <li>
                    <a name="summary" href="{{ route('summary',[ 'ca/3000' ]).'?'.$rel_parameter }}" title="騎士用品{{$tail}}" target="_blank">騎士用品</a>
                </li>
                <li>
                    <a name="genuineparts" href="{!! URL::route('genuineparts').'?'.$rel_parameter !!}" title="正廠零件{{$tail}}" target="_blank">正廠零件</a>
                </li>
                <li>
                    <a name="outlet" href="{!! URL::route('outlet').'?'.$rel_parameter !!}" target="_blank">Outlet</a>
                </li>
                <li>
                    <a name="brand" href="{{ route('brand').'?'.$rel_parameter }}" title="精選品牌{{$tail}}" target="_blank">精選品牌</a>
                </li>
                <li>
                    <a name="collection" href="{{  URL::route('collection').'?'.$rel_parameter }}" title="流行特輯{{$tail}}" target="_blank">流行特輯</a>
                </li>
                <li>
                    <a name="review" href="{{route('review').'?'.$rel_parameter}}" title="商品評論{{$tail}}" target="_blank">商品評論</a>
                </li>
                <li>
                    <a name="benefit" href="{{route('benefit').'?'.$rel_parameter}}" title="會員好康{{$tail}}" target="_blank">會員好康</a>
                </li>
                <li>
                    <a name="dealer" href="{{URL::route('dealer').'?'.$rel_parameter}}" target="_blank">經銷據點</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /.navbar-collapse -->
    <div class="container container-header">
        <div class="col1-logo">
            <img class="webike-logo" src="https://www.webike.tw/image/benefit/big-promotion/big-promotion-logo.gif">
        </div>
        <ul class="search_bar">
            <form class="clearfix" id="parts" action="{{URL::route('home').'/parts'}}" method="GET" target="_blank">
                <input type="hidden" name="mt" disabled="">
                <li class="searchHead"> 
                    <input class="search" type="text" name="q" placeholder="商品關鍵字搜尋..."> 
                </li>
                <li class="searchBtnHead">
                    <input class="btn btn-warning searchbtn" type="submit" value="搜尋"> 
                </li>
            </form>
        </ul>
    </div>
</nav>
<div id="pagetop">
    <a href="javascript:void(0);">PAGE TOP</a>
    <i class="fa fa-angle-top" aria-hidden="true"></i>
</div>
@include('response.common.disps')
<!--End Header -->