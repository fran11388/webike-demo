<style type="text/css">
    nav .nav-container{
        margin: 0 0 15px;
        background: #F20D43;
    }
    nav .header{
        background: #fff;
        height: 90px;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2);
        position: relative;
        z-index: 5;
    }
</style>

<nav>
    <div class="header">
        <div class="nav-container">
            <div class="container">
                <ul class="nav navbar-nav navbar-left width-full">
                    <li><a class="" href="{!! route('shopping').'?'.$rel_parameter !!}" target="_blank">摩托百貨</a></li>
                    <li>
                        <a name="motor"  href="{{ route('motor').'?'.$rel_parameter }}" title="車型索引{{$tail}}" target="_blank">車型索引</a>
                    </li>
                    <li>
                        <a name="summary" href="{{ route('summary',[ 'ca/1000' ]).'?'.$rel_parameter }}" title="改裝零件{{$tail}}" target="_blank">改裝零件</a>
                    </li>
                    <li>
                        <a name="summary" href="{{ route('summary',[ 'ca/3000' ]).'?'.$rel_parameter }}" title="騎士用品{{$tail}}" target="_blank">騎士用品</a>
                    </li>
                    <li>
                        <a name="genuineparts" href="{!! URL::route('genuineparts').'?'.$rel_parameter !!}" title="正廠零件{{$tail}}" target="_blank">正廠零件</a>
                    </li>
                    <li>
                        <a name="outlet" href="{!! URL::route('outlet').'?'.$rel_parameter !!}" target="_blank">Outlet</a>
                    </li>
                    <li>
                        <a name="brand" href="{{ route('brand').'?'.$rel_parameter }}" title="精選品牌{{$tail}}" target="_blank">精選品牌</a>
                    </li>
                    <li>
                        <a name="collection" href="{{  URL::route('collection').'?'.$rel_parameter }}" title="流行特輯{{$tail}}" target="_blank">流行特輯</a>
                    </li>
                    <li>
                        <a name="review" href="{{route('review').'?'.$rel_parameter}}" title="商品評論{{$tail}}" target="_blank">商品評論</a>
                    </li>
                    <li>
                        <a name="benefit" href="{{route('benefit').'?'.$rel_parameter}}" title="會員好康{{$tail}}" target="_blank">會員好康</a>
                    </li>
                    <li>
                        <a name="dealer" href="{{URL::route('dealer').'?'.$rel_parameter}}" target="_blank">經銷據點</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="sale-logo">
            <div class="block-container">
                <img src="//img.webike.net/sys_images/saimg/20170721_wp_sale_logo.gif" width="267" height="41" alt="webike sale">
            </div>
        </div>
    </div>
</nav>