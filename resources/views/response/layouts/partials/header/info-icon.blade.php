@if($current_customer)
    <li class="menu-icon helpdesk">
        <div class="icon-area btn-helpdesk font-color-red">
            <i class="glyphicon glyphicon-info-sign size-19rem hidden-xs"></i>
            <i class="glyphicon glyphicon-info-sign size-15rem hidden-lg hidden-md hidden-sm"></i>
                <ul class="list">
                    <li><a href="{{ route('customer-service-message') }}" title="新訊息{{$tail}}">新訊息<span class="icon-count new_message">(0)</span></a></li>
                    <li><a href="{{ route('customer-history-order') }}" title="訂單歷史{{$tail}}">訂單歷史</a></li>
                    <li><a href="{{ route('customer-service-proposal') }}" title="線上諮詢{{$tail}}">線上諮詢</a></li>
                    <li><a href="{{ route('customer') }}" title="會員中心{{$tail}}">會員中心</a></li>
                    <li><a href="{{ route('customer-history-purchased') }}" title="購入商品履歷{{$tail}}">購入商品履歷</a>
                        @include('response.layouts.partials.header.tag-new-features')
                    </li>
                </ul>
        </div>
    </li>
@else
    <li class="menu-icon helpdesk">
        <div class="icon-area btn-helpdesk  font-color-red">
            <i class="glyphicon glyphicon-info-sign size-19rem hidden-xs"></i>
            <i class="glyphicon glyphicon-info-sign size-15rem hidden-lg hidden-md hidden-sm"></i>
                <ul class="list">
                    <li><a href="{{ route('customer-account-create') }}" title="加入會員{{$tail}}">加入會員</a></li>
                    <li><a href="{{route( 'customer-rule', 'member_rule_2' )}}" title="購物說明{{$tail}}">購物說明</a></li>
                    <li><a href="{{ route('customer-service-information') }}" title="客服資訊{{$tail}}">客服資訊</a></li>
                    <li><a href="{{ route('customer') }}" title="會員中心{{$tail}}">會員中心</a></li>
                    <li><a href="{{ route('customer-history-purchased') }}" title="購入商品履歷{{$tail}}">購入商品履歷</a></li>
                </ul>
        </div>
    </li>
@endif


