Header -->
<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top affix-top header nav-down">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll header-top">
        <button type="button" class="navbar-toggle navbar-left icon-menu" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-bars size-15rem" aria-hidden="true"></i>
        </button>
        <a href="{{route('home')}}" class="logo-mobile"><img src="{!! assetRemote('image/logo.png') !!}" alt="「Webike-台灣」"/></a>
        <ul class="nav top-nav icon-top-page">
            @include('response.layouts.partials.header.login-icon')
            @include('response.layouts.partials.header.cart-icon')
            <li class="helpdesk"><a href="javascript:void(0)" class="icon-search"><i class="glyphicon glyphicon-search size-15rem"></i></a></li>
        </ul>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse header-top" id="bs-example-navbar-collapse-1">
        
        <div class="container">
            <div class="overflow-menu-ui">
                <ul class="nav navbar-nav navbar-left overflow-menu home-page">
                    <li><a class="" href="{!! route('shopping') !!}"> 摩托百貨 </a></li>
                    <li><a class="" href="{!! MOTOMARKET !!}"> 摩托車市 </a></li>
                    <li><a class="" href="{!! BIKENEWS !!}"> 摩托新聞 </a></li>
                    <li><a class="" href="{{ MOTOCHANNEL }}" target="_blank"> 摩托頻道 </a></li>
                    <li><a class="" href="{{ route('benefit-event-motogp-2019') }}" target="_blank" onclick="ga('send', 'event', 'link', 'motogp-2019', 'header-index');"> 2019 MotoGP</a></li>
                    <li><a class="" href="{{ route('collection-suzuka2018') }}" target="_blank"> 2018 鈴鹿8耐</a></li>
                    <li><a class="hidden-lg hidden-md" href="{{ route('benefit') }}" target="_blank"> 會員好康 </a></li>
                    <li><a class="hidden-lg hidden-md" href="{{ route('dealer') }}" target="_blank"> 經銷據點 </a></li>
                    @if(\Auth::check())
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('customer')}}"><i class="glyphicon glyphicon-info-sign size-85rem"></i> 會員中心</a></li>
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('logout')}}"><i class="glyphicon glyphicon-log-out size-85rem"></i> 登出</a></li>
                    @else
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('login')}}"><i class="glyphicon glyphicon-user size-85rem"></i> 會員登入</a></li>
                        <li class="hidden-lg hidden-md"><a class="" href="{{route('customer-account-create')}}"><i class="glyphicon glyphicon-plus-sign size-85rem"></i> 加入會員</a></li>
                    @endif
                </ul>

                <ul class="nav nav-pills overflow-menu-collect">
                    <li class="dropdown">
                        <a role="button" href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
                            More <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu overflow-sub-menu"></ul>
                    </li>
                </ul>
                <span class="goodlife visible-lg visible-md">Good ride, Good life!<span class="hidden-md hidden-sm hidden-xs">  摩托情報滿載、滿足您的摩托人生</span></span>
                <div class="hidden-md hidden-sm hidden-xs channel" style="width:2%;"><a href="https://www.facebook.com/WebikeTaiwan/" target="_blank" rel="nofollow"> <img src="{{assetRemote('image/channelicon/facebook.png')}}" ></a></div>
                <div class="hidden-md hidden-sm hidden-xs channel" style="width:2%;"><a href="https://line.me/R/ti/p/%40uqv6347i" target="_blank" rel="nofollow"> <img src="{{assetRemote('image/channelicon/line.png')}}"></a></div>
                <div class="hidden-md hidden-sm hidden-xs channel" style="width:2%;"><a href="https://www.instagram.com/webike_taiwan/?utm_source=ig_profile_share&igshid=wwl8jdoybu8i" target="_blank" rel="nofollow"> <img src="{{assetRemote('image/channelicon/instagram.png')}}"></a></div>
            </div>
        </div>
        
    </div>
    @include ( 'response.layouts.partials.header.ad-bar' )
    <!-- /.navbar-collapse -->
    <div class="container container-header">
        <div class="col1-logo">
            <a href="{{route('home')}}" class="logo">
                {{--<img class="img-top-logo" src="{!! assetRemote('image/logo.png') !!}" alt="logo"/>--}}
                <img class="img-top-logo" src="{!! assetRemote('image/logo/webike_logo.svg') !!}" alt="logo"/>
            </a>
        </div>
        <div class="col2-search">
            <div class="box-search col-md-12">
                @include('response.layouts.partials.header.searchbar')
            </div>
        </div>

        @include('response.common.head-icon')
    </div>
    <!-- /.container-fluid -->
</nav>
<span id="bp_mobile" class="bp_checking"></span>
<!--End Header