@if(!session()->has('cart_count') or session()->get('cart_count') == 0)
    <li class="cart menu-icon">
        <div class="icon-area">
            <i class="font-color-red glyphicon glyphicon-shopping-cart size-19rem"></i>
            <ul class="list">
                <li><a href="{{ route('cart') }}" title="購物車{{$tail}}">購物車<span class="icon-count cart"></span></a></li>
                <li><a href="{{ route('customer-wishlist') }}" title="待購清單{{$tail}}">待購清單<span class="icon-count wishlist"></span></a></li>
            </ul>
        </div>
    </li>
@else
    <li class="cart menu-icon">
        <div class="icon-area">
            <img src="{{assetRemote('image/shoppingcart.png')}}" alt="購物車{{$tail}}">
            <ul class="list">
                <li><a href="{{ route('cart') }}" title="購物車{{$tail}}">購物車<span class="icon-count cart"></span></a></li>
                <li><a href="{{ route('customer-wishlist') }}" title="待購清單{{$tail}}">待購清單<span class="icon-count wishlist"></span></a></li>
            </ul>
        </div>
    </li>
@endif