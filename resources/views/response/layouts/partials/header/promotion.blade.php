<nav id="mainNav" class="navbar navbar-default navbar-custom header nav-down">
    <div class="container-header">
        <button type="button" class="icon-menu navbar-toggle collapsed"  data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <i class="fa fa-bars size-15rem" aria-hidden="true"></i>
        </button>
        <div class="logo col-xs-6 col-sm-3 col-md-3">
            <a href="{{ route('shopping') }}" class="logo"><img class="img-top-logo" src="{{assetRemote('image/logo.png')}}" alt="logo"></a>
        </div>
        <div class="search col-xs-12 col-sm-9 col-md-9">
            <input type="search" name="" value="" class="txt-search" placeholder="Enter your text..." autocomplete="off">
            <button class="btn btn-warning btn-search fa-border" type="submit" title="Search"><i class="fa fa-search visible-xs" aria-hidden="true"></i><span class="visible-sm visible-md visible-lg">SEARCH</span></button>
        </div>
    </div>
    <div class="nav navbar-nav navbar-left main-menu" id="bs-example-navbar-collapse-1">
        <ul class="nav">
            <li>
                <a class="sub-menu-top-1" href="{{ route('shopping') }}" title="Home{{$tail}}" target="_blank">首頁</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{ route('motor') }}" target="_blank">車型索引</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{ route('summary',[ 'ca/1000' ]) }}" title="改裝零件{{$tail}}" target="_blank">改裝零件</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{ route('summary',[ 'ca/3000' ]) }}" title="騎士用品{{$tail}}" target="_blank">騎士用品</a>
            </li>
            <li class="dropdown">
                <a href="{!! URL::route('genuineparts') !!}" title="正廠零件{{$tail}}" class="dropdown-toggle sub-menu-top-1" data-toggle="dropdown" target="_blank">正廠零件 <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::IMPORT['url_code']]) !!}" title="進口正廠零件 - 「Webike-摩托百貨」" target="_blank">進口正廠零件</a></li>
                    <li><a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::DOMESTIC['url_code']]) !!}" title="國產正廠零件 - 「Webike-摩托百貨」" target="_blank">國產正廠零件</a></li>
                </ul>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{ route('summary',[ 'ca/3000-1210' ]) }}" target="_blank">書籍雜誌</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{ route('brand') }}" title="精選品牌{{$tail}}" target="_blank">精選品牌</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{  URL::route('collection') }}" title="流行特輯{{$tail}}" target="_blank">流行特輯</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{route('review')}}" title="商品評論{{$tail}}" target="_blank">商品評論</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{route('benefit')}}" title="會員好康{{$tail}}" target="_blank">會員好康</a>
            </li>
            <li>
                <a class="sub-menu-top-1" href="{{URL::route('dealer')}}" target="_blank">經銷據點</a>
            </li>
        </ul>
    </div>

</nav>