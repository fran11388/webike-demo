<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::DESK_TAG, \Everglory\Constants\Propaganda\Position::PC_BLADE);
?>
@if($propaganda)
    <style type="text/css">
        /*pc*/
        .sale_banner.pc{
            display: block;
        }
        .sale_banner.sp{
            display: none;
        }
        #sale_banner {
            width:100px;
            height:130px;
            position:fixed;
            left:-10px;
            bottom: 30px;
            z-index:999;
        }
        #sale_banner:hover .cat{
            left:0;
            transition: 0.5s;
        }
        #sale_banner a {
            line-height:130px;
            float:left;
            text-align:right;
            margin:0;
            padding:0;
        }
        #sale_banner a:hover img {
            opacity: 1;
        }
        #sale_banner .sale_18th_btn {
            position: absolute;
            bottom: 60px;
            left: 0px;
            width:100px;
            z-index:1;
        }
        #sale_banner .cat {
            position: absolute;
            bottom: 35px;
            left: -240px;
            width:120px;
            line-height:98px;
            float:left;
            transition: 0.5s;
            z-index: 1200;
        }
        @-webkit-keyframes spin {
            0% {-webkit-transform: rotate(0deg);}
            100% {-webkit-transform: rotate(360deg);}
        }
        @-moz-keyframes spin {
            0% {-moz-transform: rotate(0deg);}
            100% {-moz-transform: rotate(360deg);}
        }
        @-ms-keyframes spin {
            0% {-ms-transform: rotate(0deg);}
            100% {-ms-transform: rotate(360deg);}
        }
        @-o-keyframes spin {
            0% {-o-transform: rotate(0deg);}
            100% {-o-transform: rotate(360deg);}
        }
        @keyframes spin {
            0% {transform: rotate(0deg);}
            100% {transform: rotate(360deg);}
        }

        #sale_banner:hover .star{   -webkit-animation: spin 4s linear infinite;
            -moz-animation: spin 4s linear infinite;
            -ms-animation: spin 4s linear infinite;
            -o-animation: spin 4s linear infinite;
            animation: spin 4s linear infinite;
            opacity: 1;
            transition: 0.5s;
        }

        #sale_banner .star{
            width:85px;
            height:85px;
            position:absolute;
            display:inline-block;
            top: -10px;
            left: 7px;
            transition:0.5s;
            border-radius: 100px;
        }
        #sale_banner:hover .star .star_1,#sale_banner:hover .star .star_2,#sale_banner:hover .star .star_3,#sale_banner:hover .star .star_4{
            -webkit-animation: spin 4s linear infinite;
            -moz-animation: spin 4s linear infinite;
            -ms-animation: spin 4s linear infinite;
            -o-animation: spin 4s linear infinite;
            animation: spin 4s linear infinite;
            opacity: 1;
            transition: 0.5s;}
        #sale_banner .star .star_1{
            width: 40px;
            height: 40px;
            position: absolute;
            top: -20px;
            left:-20px;
            background:url(https://img.webike.net/sys_images/saimg/20181001_sale_btn_yellow_star.png) no-repeat;
            background-size:contain;
            opacity: 0;
        }
        #sale_banner .star .star_2{
            width: 30px;
            height: 30px;
            position: absolute;
            bottom:-20px;
            left:-20px;
            background:url(https://img.webike.net/sys_images/saimg/20181001_sale_btn_yellow_star.png) no-repeat;
            background-size:contain;
            opacity: 0;
        }
        #sale_banner .star .star_3{
            width: 30px;
            height: 30px;
            position: absolute;
            top:-20px;
            right:-20px;
            background:url(https://img.webike.net/sys_images/saimg/20181001_sale_btn_yellow_star.png) no-repeat;
            background-size:contain;
            opacity: 0;
        }
        #sale_banner .star .star_4{
            width: 40px;
            height: 40px;
            position: absolute;
            bottom:-20px;
            right:-20px;
            background:url(https://img.webike.net/sys_images/saimg/20181001_sale_btn_yellow_star.png) no-repeat;
            background-size:contain;
            opacity: 0;
        }
        /*PCend*/

        /* use tag*/
        #sale_banner{
            width:12%;
        }
        /* end use tag*/
        /*smartphone*/
        @media screen and (max-width:480px) {
            .sale_banner.sp{display: block;
                position:fixed;
                left:0;
                bottom: 30px;
                z-index:999;}
            .sale_banner.sp a img{width: 100px;}
            .sale_banner.pc{display: none;}
        }
        #sale_header{background: #680104;}

        #sale_header a{
            display: block;
            background: url(https://img.webike.net/sys_images/saimg/20181001_18th_sale_obi.jpg) center no-repeat;
            height: 45px;
        }
        /*smartphone end*/
    </style>
    <!--PC-->
    <div class="sale_banner pc visible-md visible-lg visible-sm" id="sale_banner">

        <a href="{{$propaganda->link}}" target="_blank">
            <img class="sale_18th_btn christmas" src="{{ cdnTransform($propaganda->pic_path) }}" alt="{{$propaganda->text}}" />
        </a>
    </div>

    <!--smartphone-->
    @php
        /*<div class="sale_banner sp"><a href="https://www.webike.net/sale/">
        <img src="https://img.webike.net/sys_images/saimg/20181001_sale_btn_sp.png" alt=""></a></div>*/
    @endphp


    <script>
        jQuery(function($){
            /*$('#sale_banner a').hover(function(){
                $('#sale_banner .cat').stop().animate({left:'5px'},100);
               },function(){
               $('#sale_banner .cat').stop().animate({left: '-180px'},200);
            });*/
            $('#sale_banner a').hover(function(){
                $('#sale_banner').stop().animate({left:'10px'},200);
            },function(){
                $('#sale_banner').stop().animate({left: '-10px'},200);
            });
            /*$('#sale_banner a').hover(function(){
                image = '{{assetRemote('image/banner/bar/littletag.png')}}';
            $('#sale_banner a img.christmas').attr('src',image);
           },function(){
            image = '{{assetRemote('image/banner/bar/littletag.png')}}';
            $('#sale_banner a img.christmas').attr('src',image);
        });*/
        });
    </script>
@endif


