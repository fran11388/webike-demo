@if($bar_obvious and activeValidate('2019-04-01 00:00:00','2019-05-01') )
	<style>
		.tag-sale{
			width: 50px;
			background: #e61e25;
			border-radius: 50px;
			position: absolute;
			top: -13px;
	    	left: 20px;
		}
		.tag-sale p{
			color: #fff;
			text-align: center;
		}

		.overflow-menu-ui ul.overflow-menu > li{
			position: relative;
		}


		@media(max-width: 416px){
			.tag-sale{
		        position: absolute !important;
		        background: #e61e25 !important;
		        border-radius: 3px !important;
		        width: 60% !important;
		        height: auto !important;
		        padding: 0 3px !important;
		        z-index: 10;
		        margin: 0 !important;
		        top: -12px !important;
		        left: 20% !important;
		        opacity: 0.8;
		    }
		    .tag-sale p{
		        width: auto !important;
		        height: auto !important;
		        margin: 0 !important;
		        position: initial !important;
		        color: #fff;
		    }
		}
		

	}
	</style>

	<div class="tag-sale">
		<p>95折</p>
	</div>
@endif