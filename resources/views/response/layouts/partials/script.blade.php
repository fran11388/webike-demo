
<!-- slick-->
<!-- <script src="{{ assetRemote('plugin/slick/slick.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
<!-- owl-carousel with responsive display-->
<script defer src="{{assetRemote('js/pages/owl.carousel-responsive.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<!-- <script src="{{ assetRemote('js/basic/bootstrap.min.js') }}"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- <script src="{{ assetRemote('plugin/select2/js/select2.full.min.js') }}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
<script defer src="{{ assetRemote('js/basic/swinch.min.js') }}"></script>.
<!-- <script type="text/javascript" src="{{ assetRemote('plugin/es6-promise-4.1.0/es6-promise.auto.min.js') }}"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.0/es6-promise.min.js"></script>
<!-- <script type="text/javascript" src="{{ assetRemote('plugin/sweetalert2/sweetalert2.min.js') }}"></script> -->

<script src="https://unpkg.com/promise-polyfill@7.1.0/dist/promise.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.1.1/sweetalert2.min.js"></script>
<!-- <script src="{{ assetRemote('plugin/jQuery.mmenu-master/dist/jquery.mmenu.all.min.js') }}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/6.0.2/jquery.mmenu.all.min.js"></script>
<!-- <script src="{{ assetRemote('plugin/jQuery.mmenu-master/src/core/screenreader/jquery.mmenu.screenreader.js') }}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/6.0.2/core/screenreader/jquery.mmenu.screenreader.min.js"></script>
{{--<script src="{{ assetRemote('plugin/jQuery.mmenu-master/dist/addons/fixedelements/jquery.mmenu.fixedelements.min.js') }}"></script>--}}

<script defer src="{{ assetRemote('js/basic/main.js') }}"></script>
<script src="{{ assetRemote('js/basic/common.js') }}"></script>
<script type="text/javascript" src="{{ cdnTransform("//img.webike.net/js/jquery/jquery.unveil.js") }}"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/unveil/1.3.0/jquery.unveil.min.js"></script> -->

<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "WebSite",
        "name": "「Webike-摩托百貨」",
        "description": "即時更新摩托百貨、摩托車市、摩托新聞最新情報，【超過1800個全球知名品牌、56萬項商品】進口國產最新改裝部品、24車廠正廠零件、當季新款人身部品應有盡有。免費加入會員享有更多好康，Webike滿足您的摩托人生!",
        "alternateName": "即時更新摩托百貨、摩托車市、摩托新聞最新情報，【超過1800個全球知名品牌、56萬項商品】進口國產最新改裝部品、24車廠正廠零件、當季新款人身部品應有盡有。免費加入會員享有更多好康，Webike滿足您的摩托人生!",
        "image": "https://img.webike.tw/assets/images/shared/webike_logo.png",
        "url": "https://www.webike.tw/",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.webike.tw/parts/?q={keyword}",
            "query-input": "required name=keyword"
        }
    }
</script>
<script type="application/ld+json">
    {
        "@context" : "http://schema.org",
        "@type" : "Organization",
        "name": "榮芳興業有限公司",
        "url" : "http://www.webike.tw",
        "contactPoint" : [
            { "@type" : "ContactPoint",
                "telephone" : "{!! OFFICE_GLOBAL_PHONE !!}",
                "contactType" : "customer service",
                "areaServed" : "TW"
            },{
                "@type" : "ContactPoint",
                "telephone" : "{!! OFFICE_GLOBAL_PHONE !!}",
                "contactType" : "Sales"
            }
        ]
    }
</script>
@include('response.layouts.partials.track.facebook-pixel')
<script src="{{ assetRemote('js/basic/track.js') }}"></script>
<script defer type="text/javascript">
    $(function(){
        $("img").unveil(200);
        $('.navbar-nav').find('a').removeClass('active-navbar').each(function(){
            if($(this).prop('name') && "{{Route::currentRouteName()}}".indexOf($(this).prop('name')) === 0){
                if($(this).prop('name') == 'summary'){
                    if("{{request()->path()}}".indexOf($(this).attr('data-name')) >= 0){
                        $(this).addClass('active-navbar');
                    }
                }else{
                    $(this).addClass('active-navbar');
                }
            }
        });
    });
    $(document).ready(function(){
       if($('.w-ad-bar').height() > 0){
            $('.contents-all-page').css('padding-top',$('.w-ad-bar').height() + 'px');
       }
    });
</script>

@if(isset($document) and isset($disps) and in_array($document->code, $disps->pluck('code')->all()))
    <script defer type="application/javascript">
        $(document).ready(function(){
            doReadDisp($('a.disp[name={{$document->code}}]'));
        });
    </script>
@endif
