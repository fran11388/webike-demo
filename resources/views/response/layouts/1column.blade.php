<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
@include ( 'response.layouts.partials.head' )

<!-- Custom CSS -->
    @yield('style')
</head>
<body>
<div class="all-page">
    @include ( 'response.layouts.partials.header.shopping' )
    <!--Content -->
    <section id="contents top-shopping " class="top-shopping contents-all-page">
        @include ( 'response.layouts.partials.header.bannerbar' )
        @include ( 'response.layouts.partials.header.promotion-tag' )
        <ul class="menu-left-mobile col-xs-12">
            <li class="col-xs-2-4">
                <a href="{{route('motor')}}">
                <!-- <img src="{!! assetRemote('image/icon-sale-2.png') !!}" alt="Sales"/> -->
                    <h3><p>車型<br>索引</p></h3>
                </a>
            </li>
            <li class="col-xs-2-4">
                <a href="{{route('summary', ['ca/1000'])}}">
                <!-- <img src="{!! assetRemote('image/icon-outlet-2.png') !!}" alt="Sales"/> -->
                    <h3><p>改裝<br>零件</p></h3>
                </a>
            </li>
            <li class="col-xs-2-4">
                <a href="{{route('summary', ['ca/3000'])}}">
                <!-- <img src="{!! assetRemote('image/icon-model-2.png') !!}" alt="Sales"/> -->
                    <h3><p>騎士<br>用品</p></h3>
                </a>
            </li>
            <li class="col-xs-2-4">
                <a href="{{ route('outlet')}}">
                <!-- <img src="{!! assetRemote('image/icon-motogp-2.png') !!}" alt="Sales"/> -->
                    <h3 style="height: 38px"><p>Outlet</p><span class="helper"></span></h3>
                </a>
            </li>
            <li class="col-xs-2-4">
                <a href="{{DEALER_URL}}">
                <!-- <img src="{!! assetRemote('image/icon-review-2.png') !!}" alt="Sales"/> -->
                    <h3><p>經銷<br>據點</p></h3>
                </a>
            </li>
        </ul>
        <div class="container">
            <div class="breadcrumb-mobile">
                <ol class="breadcrumb breadcrumb-product-detail" itemscope itemtype="http://schema.org/BreadcrumbList">
                    {!! $breadcrumbs_html !!}
                </ol>
            </div>
            <!-- <a id="m-menu-execute" class="hidden-lg hidden-md hidden-sm" href="#m-menu"><i class="glyphicon glyphicon-book"></i></a> -->
            @yield('middle')
        </div>
    </section>
    <!--End Content -->
    @include ( 'response.layouts.partials.footer.shopping' )
</div>
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>