<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
{{-- @include ( 'response.layouts.partials.head' ) --}}
@include ( 'response.layouts.partials.meta' )
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/bootstrap.min.css') }}">
<link href="{{ assetRemote('plugin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/templatepromotion.css') }}">
<script src="{{ assetRemote('js/basic/jquery.js') }}"></script>
@include('response.layouts.partials.track.google-analytic')
@include('response.layouts.partials.track.facebook-pixel')
<!-- Custom CSS -->
@yield('style')
</head>
<body>
<div class="all-page">
    @include ( 'response.layouts.partials.header.promotion' )
    <!--Content -->
    <section id="contents" class="content-promotion">
        {{-- <div class="container">
            <div class="breadcrumb-mobile">
                <ol class="breadcrumb breadcrumb-product-detail" itemscope itemtype="http://schema.org/BreadcrumbList">
                    {!! $breadcrumbs_html !!}
                </ol>
            </div>
        </div> --}}
        @yield('middle')
    </section>
    <!--End Content -->
    @include ( 'response.layouts.partials.footer.promotion' )
</div>
{{-- @include ( 'response.layouts.partials.script' ) --}}
<script>
    $('.visible-sm').click(function(){
        var value = $(this).closest('.search').find('.txt-search').val();
        window.open('/parts?q='+value,'_blank');
    });
</script>
<script src="{{ assetRemote('plugin/owl-carousel/owl.carousel.min.js') }}"></script>
<script src="{{ assetRemote('plugin/slick/slick.js') }}"></script>
<script src="{{ assetRemote('js/basic/bootstrap.min.js') }}"></script>
<script src="{{ assetRemote('plugin/select2/js/select2.min.js') }}"></script>
@include ( 'response.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
</body>
</html>