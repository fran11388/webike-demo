<!doctype html>
<html>
<head>
    @include ( 'response.layouts.partials.head' )
    <style type="text/css">
        .box-search{
            margin: 13px 0;
        }
        .col1-logo .logo{
            height: 75px;
        }
        .col2-search{
            padding: 0 355px 0 205px !important;
        }
        .gsc-search-box{
            padding: 0;
            margin: 0;
        }
        .gsc-search-box td.gsc-input{
            padding-right: 0 !important;
        }
        .gsc-search-box input.gsc-input,.gsc-search-box input.gsc-input{
            height: 38px;
            width: 100% !important;
        }
        .gsc-search-box input.gsc-search-button{
            height: 38px;
            margin-left: 0 !important;
            border-radius: 0 !important;
        }
    </style>
</head>
<body>

    @include ( 'response.layouts.partials.header.index' )
    <!-- Content -->
    <section id="contents" class="content-top-page">
        <ul class="menu-left-mobile col-xs-12">
            @include ( 'response.common.hotlinks-top' )
        </ul>
        <div class="container container-top-page">
            <div class="col1-ct">
                @include ( 'response.common.hotlinks-left' )
            </div>
            <div class="col2-ct">
                <div class="section technology-news">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 top-post">
                            <div class="post">
                                <a href="{{$top->url}}?rel={{base64_encode('homepage_v1')}}" class="entry-header" id="test" target="_blank">
                                    <div class="entry-thumbnail">
                                        <img class="img-responsive rounded-img" src="{!! $top->image_url !!}" alt=""/>
                                        <div class="bg-post-content"></div>
                                    </div>

                                    <div class="post-content-top">
                                        <h1 class="entry-title-top"><a href="{{$top->url}}?rel={{base64_encode('homepage_v1')}}" target="_blank">{{$top->title}}</a></h1>
                                        <span class="entry-content-top dotted-text2">{!! str_limit($top->description, 100, '...') !!}</span>
                                    </div>
                                </a>
                            </div>
                            <!--/post-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 below-post">
                            {!! $html !!}
                        </div>
                        <div class="text-center" id="loading" style="display: none;">
                            <img id="loading" style="display: none;" src="{!! assetRemote('image/loading.gif') !!}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col3-ct">
                @include('response.pages.partials.popular-motor')

                <div class="box col-xs-12 col-sm-12 col-md-12 motogp">
                    <h2 class="title-box text-center">2017 MotoGP車手排行榜</h2>

                    <div class="content-box col-xs-12 col-sm-12 col-md-12">
                        <ul class="Ranking-motogp">
                            @foreach($gp_racers as $key => $gp_racer)
                                <li class="clearfix">
                                    <a href="{{$gp_racer->racer->site}}" title="{{'【' . $gp_racer->racer->number . '】' . $gp_racer->racer->name}}" target="_blank" rel="nofollow">
                                        <label class="bg-color-red text-center">{{$key + 1}}</label>
                                        <span class="dotted-text">{{'#' . $gp_racer->racer->number . $gp_racer->racer->name . '(' . $gp_racer->total_score . ')'}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <h5 class="text-center content-last-box size-08rem box">
                            <a class="font-color-red" href="{{URL::route('benefit-event-motogp-2017')}}" title="{{date('Y年') . 'MotoGP預測總冠軍：第' . $active_station->name . '站' . $active_station->name . '活動進行中'}}">預測總冠軍：第{{$active_station->id}}站{{$active_station->name}}活動進行中</a>
                        </h5>
                    </div>
                </div>
                <div class="static-banners-fix box col-xs-12 col-sm-12 col-md-12">
                    <?php /*
                    <div class="box">
                        <div class="static_banners">
                            @include('response.common.plugin.weather')
                        </div>
                    </div>
                    @include('response.common.dfp.ad-300x250')
                    @include('response.common.dfp.ad-160x600')
                    */ ?>
                </div>
            </div>
        </div>
    </section>
    <!--End Content -->
    @include ( 'response.layouts.partials.footer.index' )
    @include ( 'response.layouts.partials.script' )
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var processing;
        var ajax_request = true;
        var next = 2;

        $(document).ready(function(){

            $(document).scroll(function(e){

                if (processing)
                    return false;

                if (next != 0 && $(window).scrollTop() >= ($(document).height() - $(window).height())*0.8){
                    processing = true;
                    $('#loading').show();
                    $.ajax(
                    {
                        url: "{!! route('home-feeds') !!}",
                        type: "POST",
                        data: {page:next},
                        success: function (data, textStatus, jqXHR) {
                            if (data) {
                                result = JSON.parse(data);
                                next = result.next;
                                $(".below-post").append(result.html);
                            }
                            $('#loading').hide();
                            processing = false;
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ajax_request = false;
                            $('#loading').hide();
                            processing = false;
                            console.log('error!');
                        }
                    });
//                    setTimeout(function(){
//                        $('.ct-below-post:hidden:lt(10)').show();
//                        $('#loading').hide();
//                        processing = false;
//                    }, 1000);
                }
            });
        });
    </script>
    {{--<script src="{{asset('plugin/simple-weather/jquery.simpleWeather.min.js')}}"></script>--}}
    {{--<script>--}}
        {{--// Docs at http://simpleweatherjs.com--}}

        {{--/* Does your browser support geolocation? */--}}
        {{--if ("geolocation" in navigator) {--}}
            {{--$('.js-geolocation').show();--}}
        {{--} else {--}}
            {{--$('.js-geolocation').hide();--}}
        {{--}--}}

        {{--/* Where in the world are you? */--}}
        {{--$('.js-geolocation').on('click', function() {--}}

            {{--navigator.geolocation.getCurrentPosition(function(position) {--}}
                {{--loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates--}}
            {{--});--}}
        {{--});--}}

        {{--/*--}}
         {{--* Test Locations--}}
         {{--* Austin lat/long: 30.2676,-97.74298--}}
         {{--* Austin WOEID: 2357536--}}
         {{--*/--}}
        {{--$(document).ready(function() {--}}
            {{--navigator.geolocation.getCurrentPosition(function(position) {--}}
                {{--loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates--}}
            {{--});--}}
        {{--});--}}

        {{--function loadWeather(location, woeid) {--}}
            {{--$.simpleWeather({--}}
                {{--location: location,--}}
                {{--woeid: woeid,--}}
                {{--unit: 'c',--}}
                {{--success: function(weather) {--}}
                    {{--html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';--}}
                    {{--html += '<ul><li>'+weather.city+', '+weather.region+'</li>';--}}
                    {{--html += '<li class="currently">'+weather.currently+'</li>';--}}
                    {{--html += '<li>'+weather.alt.temp+'&deg;C</li></ul>';--}}

                    {{--$("#weather").html(html);--}}
                {{--},--}}
                {{--error: function(error) {--}}
                    {{--$("#weather").html('<p>'+error+'</p>');--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}




    {{--</script>--}}

</body>
</html>