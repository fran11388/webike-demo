@extends('response.layouts.1column')
@section('style')
	<link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">
@endsection
@section('middle')
    @include('response.pages.summary.partials.top-info')
    <div class="box-content-brand-top-page">
        <div class="ct-right-search-list col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! $manufacturer_info->content !!}
        </div>
    </div>
@endsection

@section('script')
    <script src="{{assetRemote('js/pages/link-active.js')}}"></script>
@stop


