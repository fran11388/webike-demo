@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/cartpagedetail.css') !!}">
    <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/product-relieved-shopping.css') !!}">
    <style>
        @if ($celebration->getLevelUpPrice())
            .christmas-promo-text span.btn-label{
                display:block;
                padding:0px !important;
                height:25px !important;
            }
        @endif
    </style>
@stop
@section('middle')
    @include('response.common.message.error')
    <div class="cart-page-detail box-fix-column col-xs-12 col-sm-12 col-md-12">
        <div id="cart" class="title-main-box">
            <h1>我的購物車</h1>
        </div>
        <div class="cart-page-detail-left box-fix-with visible-lg visible-md">
            @if(in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
                <a class="visible-lg visible-md btn btn-primary box" href={{URL::route('cart-quotes')}}>下載報價單</a>
            @endif

            
            <div class="box-page-group">
                <div class="title-box-page-group title-box-cart-page-detail">
                    <h3>webike安心購</h3>
                </div>
                <div class="ct-box-page-group ct-btn-secured">
                    @include('response.pages.genuineparts.partials.genuineparts-relieved-shopping-banner')
                    
                </div>
            </div>

            <div class="box-page-group">
                <div class="title-box-page-group title-box-cart-page-detail">
                    <h3>加密安全防護</h3>
                </div>
                <div class="ct-box-page-group ct-btn-secured">
                    @include('response.common.ssl-logo')
                </div>
            </div>
            @include('response.common.list.view-history-vertical')

        </div>
        <div class="cart-page-detail-right box-fix-auto col-sm-full col-xs-full">
            @if($service->getCount())
                <div class="row block hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-12 cart-checkout">
                        <a class="btn btn-large btn-danger btn-full font-bold" href="{{route('checkout')}}">結帳 <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="row box hidden-xs">
                    <div class="col-xs-6 col-sm-3 col-md-2">
                        <a class="btn btn-default btn-full" href="{{route('shopping')}}"><span class=""><i class="fa fa-chevron-left" aria-hidden="true"></i> 繼續購物</span></a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7 hidden-xs"></div>
                    <div class="col-xs-6 col-sm-3 col-md-3">
                        <a class="pull-right btn btn-default btn-full" href="javascript:void(0)" onclick="slipTo('#additional')">精選加價購</a>
                    </div>
                </div>
            @endif

            
            @include('response.pages.cart.partials.carts')

            {{--@include('response.pages.cart.partials.genuineparts')--}}
            @if($service->getCount())
                <hr style="margin: 20px 0">
                <div class="hide" id="installment-table">
                    <h3>
                        @if($current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE)
                            以上結帳金額提供經銷商分期服務，若有需要請在結帳頁面選擇付款方式(詳細說明請參閱"<a href="{!! route( 'service-dealer-installment-info' ) !!}" title="經銷商信用卡分期服務{!! $tail !!}" target="_blank">經銷商信用卡分期服務</a>")。
                        @else
                            信用卡分期服務，若有需要請在結帳頁面選擇付款方式(詳細說明請參閱"<a href="{!! route('customer-rule', 'member_rule_2') !!}#E_b" title="信用卡分期說明{!! $tail !!}" target="_blank">信用卡分期說明</a>")。
                        @endif
                    </h3>
                    @include('response.common.installments-table')
                </div>
            @endif

            @include('response.pages.cart.partials.promotions')
            <div class="row box hidden-sm hidden-md hidden-lg">
                <div class="col-xs-12">
                    <a class="btn btn-default btn-full" href="{{route('shopping')}}" onclick="history.back();"><span class=""><i class="fa fa-chevron-left" aria-hidden="true"></i> 繼續購物</span></a>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        function importInstallHtml(installments){
            $("#installment-table").find('.history-table-content').html('');
            $.each(installments, function(key, object){
                $("#installment-table").find('.history-table-content').append(object.html);
            });
            $("#installment-table").removeClass('hide');
        }

        function initialInstallment(){
            $.get("./cart/installment", function (result) {
                if(result.length){
                    importInstallHtml(result);
                }
            });
        }
        initialInstallment();

        $('.animate-btn-down').click(function(){
            $(this).closest('.product-block').removeClass('fix-height');
            $(this).prev().css("overflow","visible");
            $(this).closest('.product-information').css("height","auto");
            $(this).prev().css("height","auto");
            $(this).next().removeClass('hidden');
            $(this).addClass('hidden');
        });

        $('.cart-checkout').click(function(e){
            if($('.product-block').hasClass('sold_out')){
                e.preventDefault();
                swal({
                    title: '提醒',
                    text: "請先刪除\"售完\"商品，即可進行結帳。",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33'
                }).then(function(result) {
                    slipTo1('#sold_out');
                });

//                return false;
            }
        });

        function slipTo1(element){
            if($(window).width() < 600) {
                var y = parseInt($(element).offset().top) - $('#sold_out').height() + 120;
                $('html,body').animate({scrollTop: y}, 400);
            }else{
                var y = parseInt($(element).offset().top) - $('#sold_out').height();
                $('html,body').animate({scrollTop: y}, 400);
            }
        }

        $('.animate-btn-up').click(function(){
            $(this).closest('.product-block').addClass('fix-height');
            $(this).prev().prev().css("overflow","hidden");
            $(this).closest('.product-information').css("height","70px");
            $(this).prev().prev().css("height","14px");
            $(this).prev().removeClass('hidden');
            $(this).addClass('hidden');
        });

        $(document).ready(function(){
            $('.history-table-content input[name=qty]').change(function(){
                var maxQuantity = parseInt($(this).prop('max'));
                var value = parseInt($(this).val());
                if(value > maxQuantity){
                    $(this).val(maxQuantity);
                }
            });
        });
    </script>
@stop
