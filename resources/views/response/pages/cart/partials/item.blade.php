@php
    $sold_out = (isset($carts_stock_info[$product->sku]) and $carts_stock_info[$product->sku]['sold_out']) ? true : false;
    $discontinued = (isset($carts_stock_info[$product->sku]) and $carts_stock_info[$product->sku]['discontinued']) ? true : false;
    $delivery = $carts_stock_info ? $carts_stock_info :null;
    $image_cover_types = [\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY];
    $estimate_time = date('m/d H:i',strtotime(date('Y-m-d H:i:s')));
    $max = 999;
    if(isset($delivery[$product->sku]) and isset($delivery[$product->sku]['stock_type']) and $delivery[$product->sku]['stock_type'] == 'tw_stock'){
        $max = $delivery[$product->sku]['stock'];
    }
@endphp
<ul class="col-sm-block col-xs-block clearfix {{ ($sold_out || $discontinued) ? 'sold_out' : ''}} fix-height product-block" id="{{ ($sold_out || $discontinued) ? 'sold_out' : ''}}">
    <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div>
            <div class="col-md-4 col-sm-4 col-xs-4 cart-image-block">
                <div class="cart-image">
                    <a class="thumbnail zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                        <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}" class="{{ in_array($product->type,$image_cover_types) ? 'cover' : '' }}">
                    </a>
                </div>
                <span class="helper"></span>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 full-name box">
                <a href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                    <h3>{{ $product->full_name }}</h3>
                </a>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                {{--<span title="{{ $product->manufacturer->name }}">品牌：{{ $product->manufacturer->name }}</span>--}}
                <div class="delivery-block size-08rem font-bold box hidden-lg hidden-sm hidden-md visible-xs">
                    @if($delivery)
                        @if($sold_out)
                            {{ '商品售完，請選購其他商品。' }}
                            <span class="estimate_time"> {!! $estimate_time !!}</span>
                        @elseif($discontinued)
                            {{ '商品已停產，請選購其他商品。' }}
                            <span class="estimate_time"> {!! $estimate_time !!}</span>
                        @else
                            {!!  $delivery[$product->sku]['delivery_info']  !!}
                        @endif
                    @endif
                </div>
                <span class="font-color-red hidden-lg hidden-md hidden-sm subtotal">
                                    小計：NT${{ number_format($product->getCartPrice($current_customer) * $cart->quantity ) }}
                                </span>
                <div class="toggle-box product-information">
                    <div class="toggle-button hidden-lg hidden-md hidden-sm text-right">
                        <a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊...</a>
                    </div>
                    <div class="toggle-content hidden-xs">
                        <div class="model_number">商品編號：{{ $product->model_number }}</div>
                        @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                            <div class="remark">備註：{{$estimate_item->note}}</div>
                        @endif
                        @if($options = $product->getSelectsAndOptions() and $options->count())
                            <div class="clearfix">
                                <div class="box option-block">
                                    {{--規格：<br>--}}
                                    @foreach($options as $option)
                                        @php
                                            $explode_sign = ['：',':'];
                                            foreach($explode_sign as $sign){
                                                $option_content = explode($sign,$option->option);
                                                if(isset($option_content[1])){
                                                    break;
                                                }
                                            }
                                            $option_title = str_replace('請選擇','',$option->label);
                                        @endphp
                                        @if(isset($option_content[1]))
                                            {{ $option->option }}<br>
                                        @else
                                            {{ $option_title }}：{{ $option->option }}<br>
                                        @endif
                                    @endforeach
                                </div>
                                @if(\Ecommerce\Core\Agent::isNotPhone() and count($options) > 1)
                                    <div class="option-show animate-btn-down vertical-align-middle">
                                        <i class="fa fa-angle-down font-bold" style="color:#333333;margin-left:5px;"></i>
                                    </div>
                                    <div class="option-show animate-btn-up hidden vertical-align-middle">
                                        <i class="fa fa-angle-up font-bold" style="color:#333333;margin-left:5px;"></i>
                                    </div>
                                @endif
                                <span class="helper"></span>
                            </div>
                        @endif
                        @php
                            $summary = null;
                            if($productDescription = $product->getproductDescription() and $productDescription->summary_trans_flg and $summary = $productDescription->summary){
                                $breaks = array("<br />","<br>","<br/>");
                                $summary = str_ireplace($breaks, " ", $summary);
                                $summary = str_replace('\r',' ',$summary);
                                $summary = str_replace('\n',' ',$summary);
                                $summary = str_replace('\r\n',' ',$summary);
                            }
                        @endphp
                        <div class="summary-block">
                            <span>
                                {!! $summary !!}
                            </span>
                        </div>
                    </div>
                </div>
                <div class="delivery-block size-08rem font-bold hidden-xs">
                    @if($delivery)
                        @if($sold_out)
                            {{ '商品售完，請選購其他商品。' }}
                            <span class="estimate_time"> {!! $estimate_time !!}</span>
                        @elseif($discontinued)
                            {{ '商品已停產，請選購其他商品。' }}
                            <span class="estimate_time"> {!! $estimate_time !!}</span>
                        @else
                            {!!  $delivery[$product->sku]['delivery_info']  !!}
                        @endif
                    @endif
                </div>
            </div>
        </div>

        <!-- 待刪除-->
        {{-- ATTATENION !! Follow is Mobile Block , Modify Both Block if needed --}}
        <div class="history-content-estimate-block visible-sm visible-xs row">
            <form class="form-horizontal" method="post" action="{{route('cart-update')}}">
            @if($product->type == Everglory\Constants\ProductType::GROUPBUY)
                <div class="col-sm-6 col-xs-6 flex-box">
                    <label class="btn-label label-small start-label text-nowrap">數量：</label>
                    <button class="btn btn-default btn-small btn-full" type="submit" disabled>數量無法修改</button>
                </div>
            </form>
            @elseif(in_array($product->type,[Everglory\Constants\ProductType::ADDITIONAL]))
                <div class="col-sm-6 col-xs-6 flex-box">
                    <label class="btn-label label-small start-label text-nowrap">數量：</label>
                    <button class="btn btn-default btn-small btn-full" type="submit" disabled>限一件</button>
                </div>
            </form>
            @else
                <div class="col-sm-6 col-xs-6 flex-box">
                    <label class="btn-label label-small start-label text-nowrap">數量：</label>
                    <input class="btn-label label-small width-full text-center" name="qty" type="number" min="0" max="{{$max}}" value="{{ $cart->quantity }}">
                    <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                </div>
                <div class="col-sm-3 col-xs-3">
                    <button class="btn btn-default btn-small btn-full" type="submit" {{ $sold_out ? 'disabled' : '' }}>修改</button>
                </div>
            </form>
            @endif
            <form method="post" action="{{route('cart-remove')}}">
                <div class="col-sm-3 col-xs-3">
                    <input name="code" type="hidden" value="{{ $cart->protect_code }}">
                    <button type="submit" class="btn btn-small btn-full btn-default delete-product">刪除</button>
                </div>
            </form>
        </div>
        {{-- Mobile Block END --}}
        <!-- 待刪除尾-->

        <?php
        $categories = $product->categories->sortBy('depth')->pluck('name');
        $categories->shift();
        ?>
        <!--
        <script type="text/javascript">
            _paq.push(['addEcommerceItem', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$categories->toArray()) !!}"],{{round($product->getCartPrice($current_customer))}},{{$cart->quantity}}]);
        </script>
        -->
    </li>
    {{-- ATTATENION !! Follow is PC Block , Modify Both Block if needed --}}
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        @if($product->checkPointDiscount($current_customer))
            <span><s>NT${{ number_format($product->getFinalPrice($current_customer)) }}</s></span>
            <span class="font-color-red font-bold">點數現折價</span>
            <span class="font-color-red font-bold subtotal">NT${{ number_format($product->getCartPrice($current_customer)) }}</span>
        @else
            <span class="subtotal">NT${{ number_format($product->getCartPrice($current_customer)) }}</span>
        @endif
    </li>
    <li class="col-lg-2 col-md-2 history-content-cart-option hidden-sm hidden-xs">
        @if($product->type == Everglory\Constants\ProductType::GROUPBUY)
            數量無法修改
        @elseif(in_array($product->type,[Everglory\Constants\ProductType::ADDITIONAL]))
            限購一件
        @else
            <form class="row" method="post" action="{{route('cart-update')}}">
                <div class="col-lg-12 col-md-12">
                    <input class="btn-label btn-full text-center qty-modify" name="qty" min="0" max="{{$max}}" type="number" value="{{ $cart->quantity }}">
                    <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                </div>
                <div class="col-lg-12 col-md-12">
                    <button class="btn btn-default qty-submit" type="submit" {{ $sold_out ? 'disabled' : '' }}>修改</button>
                </div>
            </form>
        @endif
        <form method="post" action="{{route('cart-remove')}}">
            <input name="code" type="hidden" value="{{ $cart->protect_code }}">
            <span class="visible-md visible-lg">
                <div class="col-lg-12 col-md-12">
                    <button type="submit" class="delete-product">刪除商品</button>
                </div>
            </span>
        </form>
    </li>
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <span class="grandtotal">NT${{ number_format($product->getCartPrice($current_customer) * $cart->quantity ) }}</span>
    </li>
    {{--<li class="col-lg-2 col-md-2 col-sm-12 col-xs-12">--}}
        {{--<form method="post" action="{{route('cart-remove')}}">--}}
            {{--<input name="code" type="hidden" value="{{ $cart->protect_code }}">--}}
            {{--<span class="visible-md visible-lg">--}}
                {{--<div class="col-lg-3 col-md-3"></div>--}}
                {{--<div class="col-lg-6 col-md-6">--}}
                    {{--<button type="submit" class="btn btn-default">刪除</button>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-md-3"></div>--}}
            {{--</span>--}}
        {{--</form>--}}
    {{--</li>--}}
    {{-- PC Block END --}}
</ul>