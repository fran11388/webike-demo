<ul class="col-sm-block col-xs-block gifts-cart-ui clearfix fix-height" {!! (count($gifts) > 1 and $giftKey == 0) ? 'style="border-bottom: 0px;"' : '' !!}>
    @if(!count($carts) or $product->customer_gift_status == \Ecommerce\Service\GiftService::REWARD)
        <div class="cover-box">

        </div>
        @if(!$hasGiftInCart)
            <div class="center-btn">
                @if(!count($carts))
                    <a class="btn btn-default btn-large size-10rem size-force font-color-red" href="javascript:void(0)"><span class="hidden-lg hidden-md">購買商品可免費兌換</span><span class="hidden-sm hidden-xs">購物車無商品-當您購買商品時可免費兌換此贈品</span></a>
                @else
                    <form method="post" action="{{route('customer-history-gifts')}}">
                        <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
                        <input name="action" type="hidden" value="add_cart">
                        <input name="redirect" type="hidden" value="cart">
                        <button class="btn btn-default btn-large size-10rem size-force" type="submit"><span class="hidden-lg hidden-md">點擊後結帳可免費兌換</span><span class="hidden-sm hidden-xs">點擊兌換-當您購買商品時可免費兌換此贈品</span></button>
                    </form>
                @endif
            </div>
        @else
            <div class="center-btn" >
                <a class="btn btn-default btn-large size-10rem size-force font-color-red" href="javascript:void(0)">每次結帳僅能兌換一項</a>
            </div>
        @endif
    @endif
    <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 cart-image-block text-center">
                <div class="cart-image">
                    <a class="thumbnail zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                        <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
                    </a>
                </div>
                <span class="helper"></span>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 full-name box">
                <a href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                    <h3>{{ $product->full_name }}</h3>
                </a>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
{{--                <span title="{{ $product->manufacturer->name }}">品牌：{{ $product->manufacturer->name }}</span>--}}
                <span class="font-color-red hidden-lg hidden-md hidden-sm">
                    小計：NT$0
                </span>
                <div class="toggle-box">
                    <div class="toggle-button hidden-lg hidden-md hidden-sm text-right">
                        <a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊...</a>
                    </div>
                    <div class="toggle-content hidden-xs">
                        <div class="model_number text-left">商品編號：{{ $product->model_number }}</div>
                        @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                            <div>備註：{{$estimate_item->note}}</div>
                        @endif
                        @if($options = $product->getSelectsAndOptions() and $options->count())
                            <div class="clearfix">
                                <div class="box option-block">
                                    {{--規格：<br>--}}
                                    @foreach($options as $option)
                                        @php
                                            $explode_sign = ['：',':'];
                                            foreach($explode_sign as $sign){
                                                $option_content = explode($sign,$option->option);
                                                if(isset($option_content[1])){
                                                    break;
                                                }
                                            }
                                            $option_title = str_replace('請選擇','',$option->label);
                                        @endphp
                                        @if(isset($option_content[1]))
                                            {{ $option->option }}<br>
                                        @else
                                            {{ $option_title }}：{{ $option->option }}<br>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="option-show animate-btn-down">
                                    <i class="fa fa-angle-down font-bold" style="color:  #333333;margin-left:  5px;"></i>
                                </div>
                                <div class="option-show animate-btn-up hidden">
                                    <i class="fa fa-angle-up font-bold" style="color:  #333333;margin-left:  5px;"></i>
                                </div>
                            </div>
                        @endif
                        @php
                            $summary = null;
                            if($productDescription = $product->getproductDescription() and $productDescription->summary_trans_flg and $summary = $productDescription->summary){
                                $breaks = array("<br />","<br>","<br/>");
                                $summary = str_ireplace($breaks, " ", $summary);
                                $summary = str_replace('\r',' ',$summary);
                                $summary = str_replace('\n',' ',$summary);
                                $summary = str_replace('\r\n',' ',$summary);
                            }
                        @endphp
                        <div class="summary-block box">
                            <span>
                                {!! $summary !!}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- ATTATENION !! Follow is Mobile Block , Modify Both Block if needed --}}
        <div class="history-content-estimate-block visible-sm visible-xs row">
            <form class="form-horizontal" method="post" action="{{route('cart-update')}}">
                <div class="col-sm-6 col-xs-6 flex-box">
                    <label class="btn-label label-small start-label text-nowrap">數量：</label>
                    <button class="btn btn-default btn-small btn-full" type="submit" disabled>無法修改</button>
                </div>
            </form>
            <form method="post" action="{{route('customer-history-gifts')}}">
                <div class="col-sm-5 col-xs-5">
                    <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
                    <input name="action" type="hidden" value="remove_cart">
                    <input name="redirect" type="hidden" value="cart">
                    <button type="submit" class="btn btn-small btn-full btn-default">下次再兌換</button>
                </div>
            </form>
        </div>
        {{-- Mobile Block END --}}
        <?php
        $categories = $product->categories->sortBy('depth')->pluck('name');
        $categories->shift();
        ?>
        <script type="text/javascript">
            _paq.push(['addEcommerceItem', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$categories->toArray()) !!}"],{{round($product->getCartPrice($current_customer))}},1]);
        </script>
    </li>
    {{-- ATTATENION !! Follow is PC Block , Modify Both Block if needed --}}
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <span>贈品 NT$0</span>
        <span><s>原價 NT${{ number_format($product->price) }}</s></span>
    </li>
    <li class="col-lg-2 col-md-2 history-content-cart-option hidden-sm hidden-xs">
        無法修改數量
        <form method="post" action="{{route('customer-history-gifts')}}">
            <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
            <input name="action" type="hidden" value="remove_cart">
            <input name="redirect" type="hidden" value="cart">
            <span class="visible-md visible-lg">
                <div class="col-lg-12 col-md-12">
                    <button type="submit" class="btn btn-default btn-full">下次再兌換</button>
                </div>
            </span>
        </form>
    </li>
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <span>NT$0</span>
    </li>
    {{-- PC Block END --}}
</ul>