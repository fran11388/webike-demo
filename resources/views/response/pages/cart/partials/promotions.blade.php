<div class="box-items-group" id="additional">
    @foreach($additional_collection as $area)
        <div class="title-main-box">
            <h2>{{ $area->title }}</h2>
        </div>
        <div class="ct-box-items-group">
            <ul class="ct-item-product-grid-car-page">
                @foreach($area->items as $item)
                    <li class="item-product-grid col-xs-6 col-sm-3 col-md-2-4 col-lg-2-4 {!! $item->disabled_flg ? 'disabled' : '' !!}">
                        <a href="{{ $item->url }}" target="_blank" title="{{ formatProductFullName($item->product_name, $item->manufacturer_name)  }}">
                            <figure class="zoom-image">
                                @if($item->icon_flg)
                                    <div class="sale-off-icon"><span><span class="hidden-xs">只要</span>${{ number_format($item->price) }}元</span></div>
                                @endif
                                <img src="{{ $item->thumbnail }}" alt="{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}">
                            </figure>
                        </a>
                        <div class="clearfix">
                            <a href="{{ $item->url }}" class="title-product-item dotted-text2 force-limit" target="_blank">{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}</a>
                            <label class="price">NT${{ number_format($item->price) }}</label>
                            <label class="price-old">NT${{ number_format($item->old_price) }}</label>
                            <label class="normal-text"><span class="hidden-xs">回饋</span>點數：{{ $item->point }}點</label>
                        </div>
                        <div class="content-last-box">
                            @if($item->disabled_flg)
                                <a class="btn btn-default" href="javascript:void(0)">
                                    @if($area->sale_flg)
                                        <span>{{ $item->selected_flg ? '已加入購物車' : '售完' }}</span>
                                    @else
                                        <span>未達加購標準</span>
                                    @endif
                                </a>
                            @else
                                <form method="post" action="{{ route('cart-update') }}">
                                    <input name="qty" type="hidden" value="1">
                                    <input name="sku" type="hidden" value="{{ $item->url_rewrite }}">
                                    <a class="btn btn-danger" href="javascript:void(0)" onclick="submitForm(this)">
                                        <span>加入購物車</span>
                                    </a>
                                </form>
                            @endif
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    @endforeach
</div>