@extends('response.layouts.2columns-introduce')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/isotope/style.css') !!}">
    {{-- <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/isotope-docs.css') !!}"> --}}
    <style>
        .fa {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing:antialiased;
        }
        .channel-contain {
            background-color:white;
            padding:10px;
            position:relative;
        }
        .item {
            /*width:24.9% !important;*/
            height:auto !important;
            /*margin:auto;*/
            background:none !important; 
            border:0px !important;
            color:black;
        }
        .channel-thumbimg {
            width:50px;
            height:50px;
            border: 3px solid #eee;
            position:absolute;
            margin:0 10px 0 0;
        }
        .channel-author {
            float:left;
            margin-left:60px;
        }
        .channel-author a {
            color:black;
        }
        .channel-container .date {
            margin-bottom:10px;
        }
        .channel-img{
            width:100%;
            height:auto;
            margin-bottom:10px;
        }
        .channel-img a{
            transition: all 0s ease !important;
        }
        .channel-img a:hover{
            opacity:0.8;
        }
        .channel-container h3 a:hover {
            color:#ff9900;
            text-decoration: underline;
            transition: all 0s ease;
        }
        .grid {
            background-color:#eee;
            border:0px;
            width:100%;
        }
        .return .btn-default {
            width:200px;
            height:40px;
        }
        .channel-contain h3{
            margin-top:0px;
        }
        .grid-cat {margin: 0px 0 0 0;
            color: #fff;
            font-weight: 700;
            font-size: 1rem;
            padding: 5px 10px;
            position: absolute;
            right: 0;
            top: 0;
            background: #ff9900;
        }
        .channel-contain .webiketv {
            background-color:#e61e25;
        }
    </style>
@stop
@section('left')
    <div class="grid isotope">
        @foreach($assortments as $assortment)
            @php
                $publish_at = date("Y年m月d日",strtotime($assortment->publish_at));
            @endphp
            <div class="channel-container clearfix item isotope-item">
                <div class="channel-contain">
                    @if($assortment->_blank == 0)
                        <p class="grid-cat">特輯</p>
                        @php
                            $link = URL::route('collection-type-detail',['type' => 'video','url_rewrite' => $assortment->url_rewrite]); 
                        @endphp
                    @else
                        <p class="grid-cat webiketv">影音</p>
                        @php
                            $link = $assortment->link;
                        @endphp
                    @endif
                    
                    <div class="date"> <i class="fa fa-clock-o"></i> {{$publish_at}} <br></div>
                    {{-- <div class="channel-thumbimg">
                        <img src="https://www.webike.net/buyers/wp-content/uploads/2015/05/%E3%81%A8%E3%82%82_avatar_1430471309-96x96.jpg">
                    </div>
                    <div class="channel-author">
                        <i class="fa fa-pencil"></i>
                        <a href="https://www.webike.net/buyers/author/wada/" title="浪速のタイヤマン の投稿" rel="author">浪速のタイヤマン</a>
                    </div> --}}
                    <div class="channel-img">
                        <a href="{{ $link }}">
                            <img src="{{$assortment->banner}}">
                        </a>
                    </div>
                    {{-- <h3 class="number-text-genuuepart-complate"><a class="font-color-normal" href="{{ $link }}">{{$assortment->meta_title}}</a></h3> --}}
                    <span class="size-10rem">
                        {{$assortment->meta_description}}
                    </span>
                </div>
            </div>
        @endforeach
    </div>
    {!! $assortments->render() !!}
@stop
@section('right')
    @include('response.pages.collection.webikeTv.video-list')
@stop
@section('script')
    <script src="{{assetRemote('js/pages/webikeTv/isotope-docs/isotope.pkgd.min.js')}}"></script>
    <script src="{{assetRemote('js/pages/webikeTv/isotope-docs/isotope.js')}}"></script>
    <script src="{{assetRemote('js/pages/webikeTv/isotope-docs/packery-mode.pkge.min.js')}}"></script>
    <script>
        (function ($) {
    var $container = $('.grid'),
        colWidth = function () {
            var w = $container.width(), 
                columnNum = 1,
                columnWidth = 0;
            if (w > 1200) {
                columnNum  = 4;
            } else if (w > 900) {
                columnNum  = 3;
            } else if (w > 600) {
                columnNum  = 2;
            } else if (w > 300) {
                columnNum  = 1;
            }
            columnWidth = Math.floor(w/columnNum);
            $container.find('.item').each(function() {
                var $item = $(this),
                    multiplier_w = $item.attr('class').match(/item-w(\d)/),
                    multiplier_h = $item.attr('class').match(/item-h(\d)/),
                    width = multiplier_w ? columnWidth*multiplier_w[1]-10 : columnWidth-10,
                    height = multiplier_h ? columnWidth*multiplier_h[1]*0.5-40 : columnWidth*0.5-40;
                $item.css({
                    width: width,
                    //height: height
                });
            });
            return columnWidth;
        },
        isotope = function () {
            $container.imagesLoaded( function(){
                $container.isotope({
                    resizable: false,
                    itemSelector: '.item',
                    masonry: {
                        columnWidth: colWidth(),
                        gutterWidth: 20
                    }
                });
            });
        };
        
    isotope();
    
    $(window).smartresize(isotope);
    
    // image fade
    // $('.item img').hide().one("load",function(){
    //     $(this).fadeIn(500);
    // }).each(function(){
    //     if(this.complete) $(this).trigger("load");
    // });
    
    // //tab sidebar
    // $('#myTab a').click(function (e) {
    //   e.preventDefault()
    //   $(this).tab('show')
    // })

    
}(jQuery));

        // $('.grid').isotope({
        //       layoutMode: 'packery',
        //       itemSelector: '.grid-item'
        //     });

    </script>
@stop