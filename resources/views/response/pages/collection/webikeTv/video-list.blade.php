<style>
    .update-container{
        width:100%;
        margin-bottom:20px;
    }
    .update-container-title{
        width:100%;
        background-color:#424242;
        padding:10px;
    }
    .update-container span a{
        color:white;
        font-size:1rem;
        margin-bottom:20px;
    }
    .update-container ul li{
        width:100%;
        padding:10px;
        border-top: 1px solid #f0efef;
    }
    .update-container ul li .thumbimg {
        float:left;
        width:20%;
        border: 3px solid #eee;
        margin: 0 10px 0 0;
    }
    .update-container .date {
        color:black;
    }
    .update-container .link {
        background-color:white;
        padding-left:0px;
    }
    .return .btn-default {
        width:100% !important;
    }
</style>
    <div class="update-container">
        <div class="update-container-title text-center">
            <span>
                <a href="javascript:void(0)" data-toggle="tab"> <i class="fa fa-clock-o"></i>最新影片</a>
            </span>
        </div>
        <ul class="link">
            @foreach($assortments_histories as $assortment)
                @php
                    $publish_at = date("Y年m月d日",strtotime($assortment->publish_at));
                @endphp
                <li>
                    <a class="clearfix" href="{{$assortment->_blank == 1 ? $assortment->link : URL::route('collection-type-detail',['type' => 'video','url_rewrite' => $assortment->url_rewrite])}}">
                        <div class="thumbimg">
                            <img src="{{ $assortment->banner }}">
                        </div>
                        <div class="date"><i class="fa fa-clock-o"></i> {{ $publish_at }} <br>
                            <span>{{ $assortment->meta_title }}</span>
                        </div>
                    </a>
                </li>   
            @endforeach
            {{-- <li>
                <a href="javascript:void(0)">
                    <img src="https://www.webike.net/buyers/wp-content/uploads/2017/05/MIDLAND-700x700.jpg">
                    <div class="date"><i class="fa fa-clock-o"></i> 2017年04月26日 <br>
                        <span>インカムで快適なツーリングをおくりましょう♪MIDLANDの次世代インカムをご紹介！『MIDLANDミッドランド：BT PROシリーズ』</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)">
                    <img src="https://www.webike.net/buyers/wp-content/uploads/2017/05/MIDLAND-700x700.jpg">
                    <div class="date"><i class="fa fa-clock-o"></i> 2017年04月26日 <br>
                        <span>インカムで快適なツーリングをおくりましょう♪MIDLANDの次世代インカムをご紹介！『MIDLANDミッドランド：BT PROシリーズ』</span>
                    </div>
                </a>
            </li> --}}
        </ul>
    </div>
    {{-- <div class="return text-center">
        <a class="btn btn-default" href="{{URL::route('home')}}">Webike摩托百貨</a>
    </div> --}}