<div class="ct-box-collection table">
    <div class="title-main-box">
        <h1>{{$title}}</h1>
    </div>
    <div class="container-box-collection">
        @if($image)
            <img src="{{$image}}" alt="{{$image}}" style="display:block;margin:8px auto">
        @endif
        @if(isset($content))
            <span style="font-size:17px">
                {{$content}}
            </span>
        @endif
    </div>
</div>