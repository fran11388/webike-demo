<div class="title-main-box">
  	<h1>{{$title}}</h1>
</div>
<div class="row container-box-collection box-brand-introduction box-sagawa-kentaro">
	<ul class="ul-box-product-tour col-xs-12 col-sm-12 col-md-12 col-lg-12">
		@foreach($link as $href => $titlename)
	        <li>
	            <a href="{{$titlename}}" title="{{$href}}" target="_blank">{{$href}}</a>
	        </li>
        @endforeach
       
    </ul>
</div>