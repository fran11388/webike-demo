<div class="title-main-page title-main-page-collection table">
    <h1>{{$title}}</h1>
</div>
<table class="table table-bordered">
	<tbody>
		<tr>
			@foreach($image as $img)
				<td>
					<span style="color:#DAA520"><img alt="" src="{{$img}}" style="height:249px; line-height:20.8px; opacity:0.9; text-align:center; width:250px" /></span>
				</td>
			@endforeach
		</tr>
		
	</tbody>
</table>
<table class="table table-bordered">
	<tbody>
		<tr>
			<td>額定電壓</td>
			<td>DC-12伏直流</td>
		</tr>
		<tr>
			<td>最高電流</td>
			<td>6安培</td>
		</tr>
		<tr>
			<td>一次性工作時間</td>
			<td>30分鐘</td>
		</tr>
		<tr>
			<td>最高輸出壓力</td>
			<td>3.5公斤/平方釐米(50PSI)</td>
		</tr>
		<tr>
			<td>電源線/空氣管長</td>
			<td>3.35米/1.0米</td>
		</tr>
		<tr>
			<td>充195/70R14輪胎時間為</td>
			<td>8分鐘(0~30PSI)</td>
		</tr>
		<tr>
			<td>外型尺寸(長*寬*高)</td>
			<td>120mm X 150mm X 50mm</td>
		</tr>
		<tr>
			<td>產品內容</td>
			<td>球針x1，長氣嘴短氣嘴x1，使用說明書x1，電瓶夾/電源線x1，R型端子電源線x1，雪茄頭/通用型電源線x1，儲存袋x1，。</td>
		</tr>
	</tbody>
</table>