<div class="webike2_banner" style="margin-bottom:5px">

	<a href="{{route('collection-type', $type->url_rewrite)}}" title="{{$type->name}}">
		<img src="{{assetRemote('image/collection/smallbanner_' . $type->url_rewrite . '.png')}}" alt="{{$type->name}}">
	</a>
</div>