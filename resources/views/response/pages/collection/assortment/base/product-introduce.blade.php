<style>
	.photo-contain {
		border: #ccc 1px solid;
		padding-left:0px;
		padding-right:0px;
		margin-left: auto;
    	margin-right: auto;
    	margin-bottom:10px;
	}
	.photo-contain img {
		width:100%;
	}
	.display-block {
		display:block;
		width:100%;
	}
	span {
		font-size: 1rem !important;
	}
</style>
<div class="video-background clearfix">
	<div class="photo-contain col-xs-12 col-sm-2 col-md-3">
		<img src="{{$image ? $image : $products[0]->images[0]->image}}" alt="{{$products[0]->name}}">
	</div>
	<div class="brand-container col-xs-12 col-sm-10 col-md-6">
		<div>
			<a class="display-block box" href="{{URL::route('product-detail',['url_rewrite' => $products[0]->sku ])}}">
				<span class="size-10rem">{{$title ? $title : $products[0]->name}}</span>
				<i class="fa fa-external-link"></i>
			</a>
		</div>
		@if(isset($description))
			<span class="size-10rem display-block">{!! $description !!}</span>
		@else
			<span class="size-10rem display-block">{!! $products[0]->productDescription->sentence !!}</span>
		@endif
	</div>
	<div class="col-xs-12 col-sm-2 col-md-3"></div>
</div>