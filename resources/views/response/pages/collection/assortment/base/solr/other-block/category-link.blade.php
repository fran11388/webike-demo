<div class="category-link">
	<ul class="clearfix">
		<li class="col-md-2 col-lg-2 col-xs-3">
			<a class="{{$category->url_rewrite == 3000 ? "active": ''}}" href="{{route('collection-type-detail',['type'=>'category','url_rewrite'=>'shippingfree'])."?price=1000-4000&sort=visits&limit=40&order=asc&ca=3000"}}">
				騎士用品
			</a>
		</li>
		<li class="col-md-2 col-lg-2 col-xs-3">
			<a class="{{$category->url_rewrite == 1000 ? "active": ''}}" href="{{$current_customer_mybike ? route('collection-type-detail',['type'=>'category','url_rewrite'=>'shippingfree'])."?price=1000-4000&sort=visits&limit=40&order=asc&ca=1000".'&mt=' .$current_customer_mybike : route('collection-type-detail',['type'=>'category','url_rewrite'=>'shippingfree'])."?price=1000-4000&sort=visits&limit=40&order=asc&ca=1000"
			}}">
					改裝零件
				</a>
			</li>
		<li class="col-md-2 col-lg-2 col-xs-3">
			<a class="{{$category->url_rewrite == 4000 ? "active": ''}}" href="{{route('collection-type-detail',['type'=>'category','url_rewrite'=>'shippingfree'])."?price=1000-4000&sort=visits&limit=40&order=asc&ca=4000"}}">
				保養耗材
			</a>
		</li>
		<li class="col-md-2 col-lg-2 col-xs-3">
			<a class="{{$category->url_rewrite == 8000 ? "active": ''}}" href="{{route('collection-type-detail',['type'=>'category','url_rewrite'=>'shippingfree'])."?price=1000-4000&sort=visits&limit=40&order=asc&ca=8000"}}">
				機車工具
			</a>
		</li>
	</ul>
</div>