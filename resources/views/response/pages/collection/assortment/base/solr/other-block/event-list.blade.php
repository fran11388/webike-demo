@if((!$current_customer or $current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE) and strtotime('now') >= strtotime(date('2017-07-01')) and strtotime('now') < strtotime(date('2017-08-01')))
    <li class="li-left-title col-md-12 col-sm-12 col-xs-12">
        <div class="slide-down-btn">
            <a href="javascript:void(0)" class="a-left-title"><span>特別折扣篩選</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
        </div>
        <ul class="ul-sub1 row">
            <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                <ul class="ul-sub2 row">
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                        <a class="a-sub2 {{request()->input('fl') == 'special-price' ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'fl' => 'special-price' ,'page' => '' ])}}">
                        <span>
                            只顯示特別折扣
                        </span>
                        </a>
                    </li>
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                        <a class="a-sub2 {{(!request()->has('fl') or request()->input('fl') != 'special-price') ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'fl' => '' ,'page' => '' ])}}">
                        <span>
                            全部商品
                        </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
@endif