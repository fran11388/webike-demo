<div class="row ct-right-top">
    <div class="ct-table">
        <div class="ct-table-row row">
            <div class="ct-table-cell cell-title cell-20"><span class="ct-table-cell-title">關鍵字</span></div>
            <div class="ct-table-cell clearfix">
                <div class="col-md-6 col-lg-6">
                    <form class="input-search-keyword-form" method="GET" action="{!! getCurrentExploreUrl() !!}">
                        <div class="cell-content">
                            <input class="btn-label col-md-8 col-sm-6 col-xs-8 input-search-keyword" type="text" placeholder="關鍵字搜尋..." value="{{ $keyword }}" id="input-search-keyword">
                            <button class="btn btn-danger" type="submit" title="搜尋">搜尋</button>
                        </div>
                    </form>
                </div>
            @if( (isset($mybike) && $mybike) && (isset($category) && isset($category->url_rewrite) && $category->url_rewrite == 1000))
                 <div class="col-md-6 col-lg-6 select-list-box">
                    @include('response.pages.collection.assortment.base.solr.other-block.change-mybike')
                </div>       
            @endif
            </div>
        </div>
        <div class="ct-table-row row">
            <div class="ct-table-cell cell-title cell-20"><span class="ct-table-cell-title">目前搜尋條件</span>
            </div>
            <div class="ct-table-cell">
                <ul class="ul-cell-dropdown-list container cover-bottom-gap-5">
                    @if ($keyword)
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>關鍵字：{{ $keyword }}</span>
                                <a href="{{ getCurrentExploreUrl(['q'=>'']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif
                    @if ( $current_category)
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>分類：{{ $current_category->name }}</span>
                                <a href="{{ getCurrentExploreUrl(['ca'=>'']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                    @if ( $current_manufacturer)
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>品牌：{{ $current_manufacturer->name }}</span>
                                <a href="{{ getCurrentExploreUrl(['br'=>'']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                    @if ( $current_motor)
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>車型：{{ $current_motor->name }}</span>
                                <a href="{{ getCurrentExploreUrl(['mt'=>'']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif
                    @if ( $price)
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>價格：{{ $price }}</span>
                                <a href="{{ getCurrentExploreUrl(['price'=> '']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                    @if ( $country)
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>商品屬性：{{ $country }}</span>
                                <a href="{{ getCurrentExploreUrl(['country'=> '']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                    @if (request()->get('fl') == 'special-price')
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>只顯示特別折扣</span>
                                <a href="{{ getCurrentExploreUrl(['fl'=> '']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                    @if (request()->get('relation'))
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>系列商品</span>
                                <a href="{{ getCurrentExploreUrl(['relation'=> '']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif

                    @if (request()->get('fn') == '1')
                        <li class="li-cell-dropdown-item li-cell-auto-width">
                            <div class="btn-label div-tag-item">
                                <span>只顯示新商品</span>
                                <a href="{{ getCurrentExploreUrl(['fn'=> '']) }}">
                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>