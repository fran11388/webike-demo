@if($current_customer && (isset($current_customer->motors[0]) && !empty($current_customer->motors[0])))
	<div class="change-mybike">
	    <select class="select2 select-mybike">
	        <option>切換mybike</option>
	        @foreach ($current_customer->motors as $motor)
	            <option value="{{$motor->url_rewrite}}">{{$motor->name}}</option>
	        @endforeach
	    </select>
	</div>

	<script type="text/javascript">
		$('.change-mybike .select-mybike').on('change',function(){
			value = $(this).find('option:selected').val();
			url = '{{route('collection-type-detail',['type'=>'category','url_rewrite'=>'shippingfree'])."?ca=1000"}}' + "&price=1000-4000" + "&mt=" + value;
			window.location = url;
		});
</script>
@else
	<div>
	    <a class="btn btn-primary width-full" href="{{ route('benefit-event-mybike') }}" target="_blank">新增mybike</a>
	</div>
@endif