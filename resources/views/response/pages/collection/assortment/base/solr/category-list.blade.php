<div class="hidden-lg hidden-md hidden-sm">
    @include('response.pages.list.partials.right-filter')
</div>
<aside class="ct-left-search-list ct-left-search-list-main-menu col-xs-12 col-sm-4 col-md-3 col-lg-3">

    <ul class="ul-left-title row">
        <!--Event-->
    @include('response.pages.collection.assortment.base.solr.other-block.event-list')
    <!--Filter-->
    @include('response.pages.collection.assortment.base.solr.other-block.filter-list')
    <!--Country-->
    @include('response.pages.collection.assortment.base.solr.other-block.country-list')
    <!--Category-->
    @include('response.pages.collection.assortment.base.solr.other-block.category-list')
    <!--/.category-->
        <!--Brands-->
    @include('response.pages.collection.assortment.base.solr.other-block.brand-list')
    <!--/.brands-->
        <!--Price Range-->
    @include('response.pages.collection.assortment.base.solr.other-block.price-range-list')
    <!--/.price range-->
        @if($current_motor)
            <li class="li-left-title col-md-12 col-sm-12 col-xs-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>{{$current_motor->name}}</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                        <ul class="ul-sub2 row">
                            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                <div class="slide-down-btn">
                                    <div class="a-sub2">
                                        <a href="{{URL::route('motor-top', $current_motor->url_rewrite)}}" title="車型首頁{{$tail}}"><span>車型首頁</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                <div class="slide-down-btn">
                                    <div class="a-sub2">
                                        <a href="{{URL::route('motor-service', $current_motor->url_rewrite)}}" title="{{$current_motor->name}}規格總覽{{$tail}}"><span>規格總覽</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                <div class="slide-down-btn">
                                    <div class="a-sub2">
                                        <a href="{{URL::route('summary', 'mt/' . $current_motor->url_rewrite)}}" title="改裝及用品{{$tail}}"><span>改裝及用品</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                <div class="slide-down-btn">
                                    <div class="a-sub2">
                                        <a href="{{URL::route('motor-review', 'mt/' . $current_motor->url_rewrite)}}" title="商品評論{{$tail}}"><span>商品評論</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                <div class="slide-down-btn">
                                    <div class="a-sub2">
                                        <a href="{{URL::route('motor-video',  $current_motor->url_rewrite)}}" title="車友影片{{$tail}}"><span>車友影片</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                <div class="slide-down-btn">
                                    <div class="a-sub2">
                                        <a href="http://www.webike.tw/motomarket/search/fulltext?q={{$current_motor->name}}" title="新車、中古車{{$tail}}" target="_blank"><span>新車、中古車</span></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <hr>
            </li>
    @endif
    <!--/.country-->
        <!--Color-->
    {{--@include('response.pages.list.partials.color-list')--}}
    <!--./color-->
        <!--Size-->
    {{--@include('response.pages.list.partials.size-list')--}}
    <!--/.size-->
        <!--Satisfaction-->
    {{--@include('response.pages.list.partials.satisfaction-list')--}}
    <!--/satisfaction-->
        <!--新商品-->
    {{--@include('response.pages.list.partials.new-lineup-list')--}}
    <!--/.新商品-->
        <li class="li-left-title hidden-lg hidden-md hidden-sm col-xs-12">
            <div class="slide-down-btn">
                <a href="javascript:void(0)" class="a-left-title" target="_top"><span>每頁顯示</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
            </div>
            <ul class="ul-sub1 row">
                <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                    <ul class="ul-sub2 row">
                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                            <div class="box">
                                <div class="clearfix content-last-box">
                                    <div class="col-xs-3 btn-label">顯示</div>
                                    <div class="col-xs-9">
                                        <select class="btn btn-full select-option-redirect">
                                            <option value="{{ getCurrentExploreUrl(['limit'=>'']) }}" {{ $rows == 40 ? 'selected' : '' }}>
                                                40
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['limit'=>'100']) }}" {{ $rows == 100 ? 'selected' : '' }}>
                                                100
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix content-last-box">
                                    <div class="col-xs-3 btn-label">排序</div>
                                    <div class="col-xs-9">
                                        <select class="sort btn btn-full select-option-redirect">
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'' , 'order'=>'']) }}" {{ $sort == '' ? 'selected' : '' }}>
                                                人氣商品
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'new', 'order'=>'']) }}" {{ $sort == 'new' ? 'selected' : '' }}>
                                                新上架排序
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'sales' , 'order' => 'asc']) }}" {{ ($sort == 'sales' && $direction == 'asc') ? 'selected' : '' }}>
                                                促銷排序
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'price' , 'order' => 'asc']) }}" {{ ($sort == 'price' && $direction == 'asc') ? 'selected' : '' }}>
                                                價格低排序
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'price', 'order'=>'']) }}" {{ $sort == 'price' && $direction == '' ? 'selected' : '' }}>
                                                價格高排序
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'manufacturer', 'order'=>'']) }}" {{ $sort == 'manufacturer' ? 'selected' : '' }}>
                                                品牌名稱
                                            </option>
                                            <option value="{{ getCurrentExploreUrl(['sort'=>'ranking', 'order'=>'']) }}" {{ $sort == 'ranking' ? 'selected' : '' }}>
                                                評價排序
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
    <div class="ct-left-banner row hidden-xs">
        @include('response.common.dfp.ad-160x600')
    </div>
</aside>