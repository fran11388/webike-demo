<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title">
            <span>商品分類</span>
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                @if ($category and $category->depth > 1 )
                    <?php
                    $url_path = explode('-', $category->mptt->url_path);
                    $target = $tree->where('url_rewrite', $url_path[0])->first();
                    if($target){
                        for ($i = 1; $i < count($url_path) - 1; $i++) {
                            $target = $target->nodes->where('url_rewrite', $url_path[$i])->first();
                        }
                        $parent = $target;
                        if($target){
                            $target = $target->nodes->reject(function ($node) {
                                return $node->count == 0;
                            });
                        }else{
                            $target = collect();
                        }
                    }else{
                        $target = collect();
                    }

                    ?>
                @else
                    <?php  $target = $tree ?>
                @endif
                @foreach ($target as $key => $node)
                    @if ($node->count)
                        @php
                            $expand = false;
                            if ($category)
                                $expand = ($node->id == $category->id) ? true : false;
                            elseif ($loop->first)
                                $expand = true;
                        @endphp
                    @endif
                    @php
                        $folder_opened = ($category and ($category->url_rewrite == $node->url_rewrite)) ? true : false;
                    @endphp
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                        <div class="slide-down-btn {!! $folder_opened ? '' : 'clicked' !!}">
                            <div class="a-sub2 {!! $folder_opened ? 'active' : '' !!}">
                                @if(count($node->nodes))
                                    <i class="fa fa-{!! $folder_opened ? 'minus' : 'plus' !!}" aria-hidden="true"></i>
                                @endif
                                <a href="{{getCurrentExploreUrl(['ca' => $node->mptt->url_path])}}" title="{{ $node->name . $tail }}">
                                    <span>{{ $node->name }}</span>
                                    @if(\Route::currentRouteName() !== 'ranking' )
                                        <span class="count">({{ $node->count }})</span>
                                    @endif
                                </a>
                            </div>
                        </div>
                        <ul class="ul-sub2 row" {!! $folder_opened ? '' : 'style="display: none;"' !!}>
                            @foreach ($node->nodes as $key=> $node_child)
                                @if ($node_child->count)
                                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12" {!! $folder_opened ? '' : 'style="display: none;"' !!}>
                                        <div class="slide-down-btn">
                                            <div class="a-sub2">
                                                <a href="{{getCurrentExploreUrl(['ca' => $node_child->mptt->url_path])}}" title="{{ $node_child->name . $tail }}">
                                                    <span>{{ $node_child->name }}</span>
                                                    @if(\Route::currentRouteName() !== 'ranking' )
                                                        <span class="count">({{ $node_child->count }})</span>
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </li>
    </ul>
</li>