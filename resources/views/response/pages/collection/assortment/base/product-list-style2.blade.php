@if(isset($title))
    <div class="title-main-box">
        <h1>{{$title}}</h1>
    </div>
@endif
<div class="ct-right-below thumb-box-border">
    <div class="row ct-right-below-body">
        @foreach ($products as $product)

            <div class="product-item product-item-style-2{{ isset($_COOKIE['listMode']) ? $_COOKIE['listMode'] : '' }}">
                <div class="product-img">
                    <a class="thumbnail search-list-img zoom-image"
                       href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                        <img src="{{ $product->getThumbnail() }}">
                    </a>
                </div>
                <div class="product-detail">
                    <div class="product-info-search-list">
                        <div class="vendor-name dotted-text">{{ $product->manufacturer->name }}</div>
                        <h3 class="product-name">
                            <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                                {{ $product->name }}
                            </a>
                        </h3>
                        <ul class="product-description">
                            <?php
                            $summary_text = '';
                            $description = $product->productDescription;
                            if(strpos($description->summary, '<br>') !== false){
                                $summaries = explode("<br>", $description->summary);
                            }else if(strpos($description->summary, '<br/>') !== false){
                                $summaries = explode("<br/>", $description->summary);
                            }else{
                                $summaries = explode("\n", $description->summary);
                            }
                            foreach ($summaries as $summary){
                                if($summary){
                                    $summary_text .= '<li class="dotted-text dotted-text1 product-info-model"><span>'.str_replace('■', '', str_replace('/catalogue/','http://www.webike.net/catalogue/',strip_tags($summary, '<br>'))).'</span></li>';
                                }
                            }

                            ?>

                            {!! $summary_text !!}

                            {{--<li class="dotted-text dotted-text1 product-info-model"><span>{!! $product->summary !!}</span>--}}
                            {{--</li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-certification"><span>MFJ certifi</span>--}}
                            {{--</li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-spec"><span>JIS spec</span></li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-kg"><span>2.5 kg</span></li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-system"><span>Pin-lock system</span>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                    <div class="product-state-search-list">
                        <?php $final_price = $product->getFinalPrice($current_customer); ?>

                        <label class="price">NT$ {{ number_format($final_price) }}</label>
                        <label class="price-old">NT$ {{ number_format($product->price) }}</label>
                        <label class="dotted-text dotted-text1">
                            @if ( activeShippingFree() and $final_price >= FREE_SHIP)
                                    免運費
                            @else
                                <br/>
                            @endif
                        </label>
                        <label class="dotted-text dotted-text1">庫存商品</label>
                        <a class="btn btn-warning btn-full box" href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">商品詳細</a>
                        <div class="rating col-md-12">
                            <div class="container">
                                <span class="icon-star star-{{ str_pad(round($product->ranking), 2,"0" , STR_PAD_LEFT)}}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
</div>