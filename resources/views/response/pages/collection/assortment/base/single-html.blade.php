<style>
	.video-container {
		width:100%;
		height:100%;
		margin:0 auto;
	}
	.iframe-container{
		position:relative;
	}
	.iframe-container .ratio{
		display:block;width:100%;height:auto;
	}
	iframe {
		position:absolute;
		top:0;
		left:0;
		width:100% !important ;
		height:100%;
	}
    .video-background{
        padding-top:10px !important;
    }
</style>
<div class="video-background">
	{!! $single_html !!}
</div>