<div class="title-main-box">
    <h1>{{$title}}</h1>
</div>
<div class="ct-right-below">
    <div class="row ct-right-below-body">
        @foreach ($products as $product)

            <div class="product-item {{ isset($_COOKIE['listMode']) ? $_COOKIE['listMode'] : '' }}">
                <div class="product-img">
                    <a class="thumbnail search-list-img zoom-image"
                       href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                        <img src="{{ $product->getThumbnail() }}">
                    </a>
                </div>
                <div class="product-detail">
                    <div class="product-info-search-list">
                        <h3 class="product-name">
                            <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                                    <span class="dotted-text2">
                                        {{ $product->name }}
                                    </span>
                            </a>
                        </h3>
                        <div class="vendor-name dotted-text1">{{ $product->manufacturer->name }}</div>
                        <ul class="product-description">
                            @if($extraDescriptions = $product->getExtraDescription())
                                @foreach($extraDescriptions as $extraDescription)
                                    {!! $extraDescription !!}
                                @endforeach
                                <br>
                            @endif
                            <?php
                            $summary_text = '';

                            if(strpos($product->summary, '<br>') !== false){
                                $summaries = explode("<br>", $product->summary);
                            }else if(strpos($product->summary, '<br/>') !== false){
                                $summaries = explode("<br/>", $product->summary);
                            }else{
                                $summaries = explode("\n", $product->summary);
                            }
                            foreach ($summaries as $summary){
                                if($summary){
                                    $summary_text .= '<li class="dotted-text dotted-text1 product-info-model"><span>'.str_replace('■', '', str_replace('/catalogue/','http://www.webike.net/catalogue/',strip_tags($summary, '<br>'))).'</span></li>';
                                }
                            }

                            ?>

                            {!! $summary_text !!}

                            {{--<li class="dotted-text dotted-text1 product-info-model"><span>{!! $product->summary !!}</span>--}}
                            {{--</li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-certification"><span>MFJ certifi</span>--}}
                            {{--</li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-spec"><span>JIS spec</span></li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-kg"><span>2.5 kg</span></li>--}}
                            {{--<li class="dotted-text dotted-text1 product-info-system"><span>Pin-lock system</span>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                    <div class="product-state-search-list">
                        <?php $final_price = $product->getFinalPrice($current_customer); ?>

                        <label class="price">NT$ {{ number_format($final_price) }}</label>
                        <label class="price-old">NT$ {{ number_format($product->price) }}</label>
                        <label class="dotted-text dotted-text1">
                            @if ( activeShippingFree() and $final_price >= FREE_SHIP)
                                免運費
                            @else
                                <br/>
                            @endif
                        </label>
                        <label class="dotted-text dotted-text1">庫存商品</label>
                        <div class="div-btn-check-detail container"><span>商品詳細</span></div>
                        <div class="rating col-md-12">
                            <div class="container">
                                <span class="icon-star star-{{ str_pad(round($product->ranking), 2,"0" , STR_PAD_LEFT)}}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
</div>