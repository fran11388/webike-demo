<style>
    .author-container {
        float:none;
    }
    .channel-category {
        position:absolute;
        top: 0px;
        right: 0px;
        background: #ff9900;
        padding:10px;
    }
    .channel-category a {
        color:white;
    }
    .author-img {
        width: 90px;
        height: 90px;
        border: 4px solid #eee;
        margin: 0 10px 0 0;
    }

</style>

<div class="author-container clearfix video-background">
    <h3 class="box">投稿者： <a href="https://www.webike.net/buyers/author/wada/" title="{{$author}}" rel="author">{{$author}}</a></h3>
    <div class="author-img top-ct-customer-qa">
        <img alt="{{$author}}" src="{{$thumbimg}}">
    </div>
    <div>
        <h3>2017年05月08日</h3>
        <span class="size-12rem font-bold">{{$title}}</span>
    </div>
</div>