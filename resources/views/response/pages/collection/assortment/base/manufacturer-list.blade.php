<div class="ct-box-collection table">
    <div class="title-main-box">
        <h1>{!! isset($title) ? $title : '' !!}</h1>
    </div>
    <div class="container-box-collection border-box">
        {!! isset($description) ? $description : '' !!}
    </div>
    <div class="container-box-collection border-box">
        <ul class="ul-related-categories">
            @foreach($manufacturers as $manufacturer)
                <li class="item-product-grid col-xs-6 col-sm-2 col-md-2 col-lg-2">
                    <a href="{!! URL::route('summary', '/br/' . $manufacturer->url_rewrite) !!}" target="_blank">
                        <figure class="zoom-image thumb-box-border">
                            <img src="{!! $manufacturer->image !!}" alt="{!! $manufacturer->name !!}">
                        </figure>
                    </a>
                    <a href="{!! URL::route('summary', '/br/' . $manufacturer->url_rewrite) !!}" class="title-product-item" target="_blank">
                        {!! $manufacturer->name !!}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>