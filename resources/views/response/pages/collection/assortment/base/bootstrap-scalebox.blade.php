<div class="table">
    @if(isset($title))
        <div class="title-main-box">
            <h1 id="{{$title}}">{{$title}}</h1>
        </div>
    @endif
    <div class="container-box-collection thumb-box-border">
        <!-- Text content -->
        @foreach($rows as $row)
        <ul class="ul-box-brand-sagawa2" style="list-style-type: none">
            @foreach($row as $cols)
                
            <li class="col-xs-12 col-sm-{{$cols->scale}} col-md-{{$cols->scale}} col-lg-{{$cols->scale}} {{isset($align) ? 'text-' . $align : ''}}">
                {!! $cols->content !!}
            </li>
            @endforeach
        </ul>
    @endforeach
    <!-- Text content -->
    </div>
</div>