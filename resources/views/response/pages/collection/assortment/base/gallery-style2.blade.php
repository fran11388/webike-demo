<style>
	.gallery-container-style2 h3{
		border-bottom: #ddd 1px solid;
	    border-left: none;
	    padding: 10px 0 10px 0;
	}
	.gallery-contain {
		border: #ccc 1px solid;
	}
	.gallery-contain img {
		position:relative;
		height: 281.66px;
	}
	.gallery-contain .description {
		padding:10px;
		height:200px;
		overflow:auto;
	}
	.picon {
		position: absolute;
		border-radius: 50%;
	    height: 60px;
	    width: 60px;
	    text-align: center;
	    background: #E61E25;
	    color: #fff;
	    font-size: 20px;
	    line-height: 1;
	    font-weight: bold;
	    left: 5px;
	    top: 0px;
	    padding: 10px 5px !important;
	    z-index: 1;
	    box-shadow: 1px 1px 5px #999;
	}
	.gallery-style2 {
		padding-left:5px;
		padding-right:5px;
	}
	.gallery-container-style2 .row{
		margin-right:-5px;
		margin-left:-5px;
	}
</style>
<div class="gallery-container-style2 video-background clearfix">
	<div class="row">
		@foreach($image as $key => $img)
			<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
				<div class="gallery-contain box">
					<p class="picon"><span>重點</span><br>{{$key+1}}</p>
					<div class="width-full text-center">
						<img src="{{$img}}">
					</div>
					<div class="description width-full">
						<span class='size-10rem'>{!! $description[$key] !!}</span>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>