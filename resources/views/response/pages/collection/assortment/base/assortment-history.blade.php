<ul class="ul-left-title">
    <!--Category-->
    <li class="li-left-title box-page-group col-md-12">
        <div class="title-box-page-group">
            <h3>{{$title}}</h3>
        </div>
        <ul class="ul-btn-collection-menu">
            <li class="col-xs-12 col-sm-6 col-md-6"><a class="border-radius-2" href="javascript:void(0)">折疊全部</a></li>
            <li class="col-xs-12 col-sm-6 col-md-6"><a class="border-radius-2" href="javascript:void(0)">打開全部</a></li>
        </ul>
        <ul class="ul-sub2-l ct-menu-left-collection">
            @foreach($assortments as $year => $yearAssortments)
                @php
                    $monthAssortments = $yearAssortments->groupby(function($item, $key){
                        return date('m', strtotime($item->publish_at));
                    });
                @endphp
                <li class="li-sub2-l">
                    <a class="a-sub2-l slide-down-btn" href="javascript:void(0)"><i class="fa fa-minus" aria-hidden="true"></i>&nbsp<span>{{$year}}</span></a>
                    <ul class="ul-sub3-l">
                        @foreach($monthAssortments as $month => $assortments)
                        <li class="li-sub3-l">
                            <a class="a-sub3-l slide-down-btn" href="javascript:void(0)"><i class="fa fa-minus" aria-hidden="true"></i>&nbsp<span>{{$month}}</span></a>
                            <ul class="ul-sub4-l">
                                @foreach($assortments as $assortment)
                                    <li class="li-sub4-l"><a class="a-sub4-l" href="{!! $assortment->link !!}"><span>{{$assortment->name}}</span></a></li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
            @endforeach
        </ul>
    </li>
    <!--/.category-->
</ul>