<style>
	.titleContainer {
		border-top: #ddd 1px solid;
		border-bottom: #ddd 1px solid;
		padding: 10px;
		background-color: #fff4f5;
	}
	.titleContainer span{
		padding: 0 0 0 10px;
    	border-left: #E61E25 6px solid;
	}
</style>
<div class="video-background">
	<div class="titleContainer author-container">
		<span class="size-12rem">{{$title}}</span>
	</div>
</div>