<style>
	.products-container li {
		list-style-type:none;
		margin-bottom:10px;
		/*position:relative;*/
	}
	.products-container .product-link{
		display:block;
		padding:10px;
		background: #f8f8f8;
		border: 1px solid #eee;
		color: #333;
		width:100%;
		transition: all 0s ease;
		position:relative;
		overflow:hidden;
		height:160px;
	}
	.products-container .product-link:hover{
		background: #fff;
	}
	.products-container .product-link:hover img{
		opacity:0.7;
	}
	.products-container .product-link:after {
		content: "\f105";
	    font-family: FontAwesome;
	    position: absolute;
	    color: #eee;
	    font-size: 60px;
	    margin-top: -48px;
	    top: 50%;
	    right: 20px;
	}
	.products-container a img{
		width:180px;
		float: left;
		margin: 0 10px 0 0;
		border: 1px solid #b3b3b3;
	}
	.products-container .brand {
		border-bottom: 1px dotted #ccc;
		padding-bottom:5px;
		overflow:hidden;
		display:block;
		margin-bottom:5px;
	}
	.products-container span {
		display:block;
	}
	.products-container .content{
		overflow:hidden;
	}
	.product-link .product-img {
		width:30%;
		float:left;
		margin-right:10px;
	}
	.product-link .product-description {
		width:66%;
	}
	@media(max-width:635px){
		.product-link .product-img {
			width:100%;
			float:left;
			margin-right:0px;
		}14.666666
		.product-link .product-description {
			width:100%;
		}
		.products-container .product-link {
			height:auto;
		}
	}
	.products-container .row {
		margin-left:-5px;
		margin-right:-5px;
	}
	.products-container .product-list {
		padding-left:5px;
		padding-right:5px;
	}
</style>
<div class="video-background">
	<div class="products-container">
		<ul class="clearfix row">
			@foreach($products as $product)
				<li class="col-xs-12 col-sm-6 col-md-6 product-list">
					<a class="product-link" href="{{ route('product-detail' , [ $product->url_rewrite ]) }}">
						@php
                            $image = $product->getThumbnail();
                            $image_obj = $product->images->first();
                            if($image_obj){
                                $image = $image_obj->image;
                            }
                        @endphp
                        <div class="product-img clearfix">
                    		<img src="{{ $image }}" alt="{{ $product->name }}">
                    	</div>
                    	<div class="content product-description">
		                    <span class="size-10rem brand">{{$product->manufacturer->name}}</span>
		                    <span class="size-10rem">{{ $product->name }}</span>
		                    <?php $final_price = $product->getFinalPrice($current_customer); ?>
			                <span class="price font-color-red">NT {{ number_format($final_price) }}</span>
			                <span class="price-old" style="text-decoration: line-through;">NT$ {{ number_format($product->price) }}</span>
		                </div>
					</a>
				</li>
			@endforeach
		</ul>
	</div>
</div>