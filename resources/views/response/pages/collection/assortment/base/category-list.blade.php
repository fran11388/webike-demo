<div class="ct-box-collection table" style="margin-top:20px">
	@if(isset($title))
        <div class="title-main-box">
            <h1>{{$title}}</h1>
        </div>
    @endif
    <div class="container-box-collection border-box">
        <ul class="ul-related-categories">
        	@foreach($categorys as $category)
	            <li class="item-product-grid col-xs-6 col-sm-2 col-md-2 col-lg-2">
	                <a href="{{ URL::route('summary', '/ca/' . $category->mptt->url_path)}}" target="_blank">
	                    <figure class="zoom-image thumb-box-border">
	                        <img src="//img.webike.net/sys_images/category/bm_{{$category->url_rewrite}}.gif" alt="{{$category->name}}">
	                    </figure>
	                </a>
	                <a href="{{ URL::route('summary', '/ca/' . $category->mptt->url_path)}}" class="title-product-item" target="_blank">
	                    {{-- {{$category->name.'('.count($category->products).')'}} --}}
	                    {{$category->name}}
	                </a>
	            </li>
	        @endforeach
        </ul>
    </div>
</div>