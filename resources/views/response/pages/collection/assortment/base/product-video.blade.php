<style>
    .video-container {
        width:100%;
        height:100%;
        margin:0 auto;
    }
    .iframe-container{
        position:relative;
    }
    .iframe-container .ratio{
        display:block;width:100%;height:auto;
    }
    iframe {
        position:absolute;
        top:0;
        left:0;
        width:100% !important ;
        height:100%;
    }
    span,p {
        font-size: 1rem !important;
    }
</style>
<div class="video-background">
    <div class='row video-container'>
        <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6 iframe-container'>
            <img class='ratio' src='https://placehold.it/16x9'>
            <iframe  src={{$video}} frameborder='0' allowfullscreen></iframe>
        </div>
        <span class='size-10rem col-xs-12 col-sm-6 col-md-6 col-lg-6'>
            {!!  $description !!}
        </span>
    </div>
</div>