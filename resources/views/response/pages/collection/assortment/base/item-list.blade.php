<div class="ct-box-collection table">
    <div class="title-main-box">
        <h1>{{$title}}</h1>
    </div>
    <div class="row container-box-collection box-brand-introduction">
        <ul class="ul-box-brand-introduction">
            @foreach($infos as $key => $info)
                @php
                    if($select == 'brand'){
                        $url = URL::route('summary', '/br/' . $info->url_rewrite);
                    }else{
                        $url = URL::route('product-detail',$info->url_rewrite);
                    }
                @endphp
                <li class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="ct-box-brand-introduction box-fix-column">
                        <div class="box-fix-with ct-box-brand-introduction-left">
                            <a href="{{$url}}">
                                <figure class="zoom-image thumb-box-border thumb-img">
                                    @if($select == 'brand')
                                        <img src="{{$info->image}}" alt="Name product">
                                    @else
                                        <img src="{{$info->images->first()->image}}" alt="Name product">
                                    @endif
                                </figure>
                            </a>
                        </div>
                        <div class="box-fix-auto ct-box-brand-introduction-right">
                            <div class="title">
                                <a href="{{$url}}">{{$info->name}}</a>
                            </div>
                            <span>
                                @if($select == 'brand')
                                    {{$info->description}}
                                @else
                                    {{$info->productDescription->sentence}}
                                @endif
                            </span>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>