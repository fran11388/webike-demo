<div class="table block">
    <div class="title-main-box">
        <h1>{{isset($title) ? $title : $products->first()->name}}</h1>
    </div>
    @if(isset($description))
        <div class="container-box-collection">
            {{$description}}
        </div>
    @endif

    @foreach($products as $key => $product)
        <div class="container-box-collection ">
            <div class="box-fix-with modeling-left">
                <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}">
                    <figure class="zoom-image thumb-box-border thumb-img">
                        @php
                            $image = $product->getThumbnail();
                            $image_obj = $product->images->first();
                            if($image_obj){
                                $image = $image_obj->image;
                            }
                        @endphp
                        <img src="{{ $image }}" alt="{{ $product->name }}">
                    </figure>
                </a>
            </div>
            <div class="box-fix-auto modeling-right">
                <span class="sale">SALE</span>
                <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" class="title-product-item">{{ $product->manufacturer->name }}</a>
                <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" class="title-product-item title-product-item-model">{{ $product->name }}</a>
                <?php $final_price = $product->getFinalPrice($current_customer); ?>
                <label class="price font-color-red">NT {{ number_format($final_price) }}</label>
                <label class="price-old" style="text-decoration: line-through;">NT$ {{ number_format($product->price) }}</label>
                <hr>
                <span class="infomation">
                    @if(isset($product_cautions->{$product->url_rewrite}))
                        {!! $product_cautions->{$product->url_rewrite} !!}
                    @else
                        {!! str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $product->productDescription->cautions)  !!}
                    @endif
                </span>
                <span class="point border-radius-2">
                    @if (isset($product_summaries->{$product->url_rewrite}))
                        {!! $product_summaries->{$product->url_rewrite} !!}
                    @else
                        @php
                            $summary = $product->productDescription->summary;
                            $summary = str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $summary)
                        @endphp
                        {!! nl2br($summary) !!}
                    @endif
                </span>
            </div>
        </div>
    @endforeach
</div>