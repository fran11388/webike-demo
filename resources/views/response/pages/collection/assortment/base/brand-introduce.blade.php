<style>
	.photo-contain {
		border: #ccc 1px solid;
		padding-left:0px;
		padding-right:0px;
		margin-left: auto;
    	margin-right: auto;
	}
	.photo-contain img {
		width:100%;
	}
	.display-block {
		display:block;
		width:100%;
	}
</style>
@foreach($manufacturers as $manufacturer)
	<div class="video-background clearfix">
		<div class="photo-contain col-xs-12 col-sm-2 col-md-2">
			<img src="{{isset($image) ? $image : $manufacturer->image}}" alt="{{$manufacturer->name}}">
		</div>
		<div class="brand-container col-xs-12 col-sm-10 col-md-10">
			<a class="display-block box" href="{{URL::route('summary',['section' => '/br/'.$manufacturer->url_rewrite])}}">
				<span class="size-10rem">{{isset($brand_title) ? $brand_title : $manufacturer->name}}</span>
				<i class="fa fa-external-link"></i>
			</a>
			<span class="size-10rem display-block">{{isset($brand_description) ? $brand_description : $manufacturer->description}}</span>
		</div>
	</div>
@endforeach