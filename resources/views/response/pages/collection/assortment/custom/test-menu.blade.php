<aside class="ct-left-search-list">
    <ul class="ul-left-title">
        <!--Category-->
        <li class="li-left-title box-page-group col-md-12">
            <div class="title-box-page-group">
                <h3>BIG MACHINE</h3>
            </div>
            <div class="ct-menu-left-collection">
                <img src="/image/banner/banner-collection-02.jpg" alt="name image banner">
            </div>
        </li>
        <!--/.category-->
    </ul>
    <div class="box-page-group col-md-12">
        <div class="ct-menu-left-collection">
            <h2>BiGMACHINE是？</h2>
            <span>擅長How to的專欄內容，是專為入門較晚的重機初學者提供各種提升騎乘技術的專業重型機車雜誌。本著新車款＆相關商品的購買指南書性質，全方位支援騎士的騎乘生活。毎月15日發售</span>
        </div>
    </div>
</aside>