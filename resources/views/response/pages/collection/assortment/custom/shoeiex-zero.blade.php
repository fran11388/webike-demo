<head>
	<style>
		.bookmark-ui{
			display: none!important;
		}
		section .container{
			max-width: 100%!important;
			padding: 0px;
		}
		.main-banner{
			padding: 0px;
			width: 100%;
		}
		.title-block li {
			margin-bottom:10px;
			list-style-type: none;
		}
		.main-col {
			width:100% !important;
			padding:0px !important;
		}
		.product-info {
			background: #F5F5F5;
		}
		.product-design {
			background: #ebebeb;
		}
		.intrinsic-container {
		  position: relative;
		  height: 0;
		  overflow: hidden;
		}
		 
		/* 16x9 Aspect Ratio */
		.intrinsic-container-16x9 {
		  padding-bottom: 56.25%;
		}
		 
		/* 4x3 Aspect Ratio */
		.intrinsic-container-4x3 {
		  padding-bottom: 75%;
		}
		 
		.intrinsic-container iframe {
		  position: absolute;
		  top:0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		}
		.interval {
			padding-bottom:20px;
		}
		.title-block{
			max-width: 1170px;
			margin: 20px auto;
			padding: 0 20px;
		}
		.title-text{
			display: block;
		    margin-bottom: 10px;
    		font-size: 2.5rem;
		}
		.product-block{
		    background: #fff3d4;
		}
		.product-section div:first-child{
			padding-left: 0px;
		}
		.product-section div:last-child{
			padding-right: 0px;
		}
		.link-menu-top-page .ul-menu-top-page.pc li a{
			border-radius: 2px;
			padding: 7px 10px;
			width: 100%;
			text-align: center;
			float: left;
			font-size: 1rem;
			min-height: 50px;
			background-size: 100% 100% !important;
		}
		.link-menu-top-page .ul-menu-top-page.mobile li{
			padding: 0px; 
		}
		.link-menu{
			margin-bottom: 40px;
			margin-top: 40px;
		}
		.link-menu-top-page .ul-menu-top-page .link-1{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/m_bttomA.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-1:hover{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/m_bttomB.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-2{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/n_bttomA.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-2:hover{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/n_bttomB.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-3{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/o_bttomA.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-3:hover{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/o_bttomB.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-4{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/p_bttomA.png')}});
		}
		.link-menu-top-page .ul-menu-top-page .link-4:hover{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/p_bttomB.png')}});
		}
		.product-link.pc-link{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/buyA.png')}});	
		 	background-size: 100% 100% !important;
    		min-height: 50px;
    		display: block;
		}
		.product-link.pc-link:hover{
			background-image: url({{assetRemote('image/collection/ex-zero/buttom/buyB.png')}});	
		}
		.product-text{
			border: 1px solid #CCC;
			margin-bottom: 20px;
			padding: 20px 20px 0px!important; 
		}
		.product-text span{
			display: block;
			margin-bottom: 20px;
		}
		.product-list{
			margin-top: 30px;
		}
		.item-list .item{
			display: inline-block;
		    vertical-align: middle;
		    margin-right: 2px;
		}
		.item-list .item:hover{
			outline:1px solid #dcca7a;
		}
		.item_active{
			outline:1px solid #dcca7a;
			box-sizing: border-box;
		} 
		.product-section .photo-container{
			display: inline-block;
			width: 33%;
			height: 100%;
			float: none;
			vertical-align: middle;
			padding-right: 15px;
		}
		.product-section .descripton-container{
			display: inline-block;
			width: 66%;
			vertical-align: middle;
			float: none;
			padding-left: 15px;
		}
		.product-item .product-section{
			max-width: 900px;
			margin: 20px auto;
			padding: 0 20px;
		}
		@media(max-width: 1024px){
			.video iframe{
				width: 100%;
			}
		}
		@media(max-width: 500px){
			.video iframe{
				width: 100%;
				height: 100%;
			}
			.item-list .item{
			    width: calc((100% / 6) + 20px);
			}
			.product-link{
			    min-height: 37px;
			}
			.product-section div:first-child{
				padding: 0px;
			}
			.product-section div:last-child{
				padding: 0px;
				margin: 10px 0px;
			}
			.title-text{
				font-size: 1rem;
				font-weight: bold;
			} 
			.product-section .photo-container{
				width: 100%;
			}
			.product-section .descripton-container{
				width: 100%;
			}
			.link-menu{
				margin-top: 20px;
			}

		}
		@media(max-width: 400px){
			.item-list .item{
			    width: calc((100% / 6) + 16px);
			}
		}
	</style>
	<script type="text/javascript">
		@php
			$customer_role = 2; 
			if(\Auth::check()){
				$customer_role = \Auth::user()->role_id;
			}
		@endphp
		$(document).ready(function(){
				var price = "預售價:NT$ 13,650(素色)";
				var equation_price = "預售價:NT$ 15,750(彩繪)";
			if({!! $customer_role !!} == 3 || {!! $customer_role !!} == 4){
				var price = "預售價:NT$ 13,000(素色)";
				var equation_price = "預售價:NT$ 15,000(彩繪)";
			}
			var products = {'item_1':{'name':'EX-ZERO (米白)','link':'https://www.webike.tw/sd/t00127006'},
							'item_2':{'name':'EX-ZERO (亮黑)','link':'https://www.webike.tw/sd/t00127011'},
							'item_3':{'name':'EX-ZERO (消光黑)','link':'https://www.webike.tw/sd/t00127016'},
							'item_4':{'name':'EX-ZERO (玄武岩灰)','link':'https://www.webike.tw/sd/t00127021'},
							'item_5':{'name':'EX-ZERO (鮮黃)','link':'https://www.webike.tw/sd/t00127031'},
							'item_6':{'name':'EX-ZERO (亮面紅)','link':'https://www.webike.tw/sd/t00127026'},
							'item_7':{'name':'EX-ZERO (彩繪)','link':'https://www.webike.tw/sd/t00127036'},
			};
			$(".product-list .item-list .item").click(function(){
				$(".product-list .item-list li").removeClass("item_active");
				$(this).addClass("item_active");
				var item_class = $(this).find("a").attr("class");
				var img_url = "{{assetRemote('image/collection/ex-zero/img')}}"; 
				var img = img_url + "/" + item_class + ".jpg";
				$(".product-item .product-img img").attr("src",img);
				
				var name = products[item_class]['name'];
				var link = products[item_class]['link'];

				$(".product-item .product-text .product-name").text(name);
				$(".product-item .link .product-link").attr("href",link);
				if(item_class == "item_7"){
					$(".product-item .product-text .product-price").text(equation_price);
				}else{
					$(".product-item .product-text .product-price").text(price);
				}
			});
		});
	</script>
</head>
<ul class="content-promotion-col">
	<li class="main-col">
		<img class="main-banner" src="{{assetRemote('image/collection/ex-zero/banner.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
		<div class="clearfix">
			<div class="title-block">
				<span class="title-text">PASSION NEVER GROWS OLD</span>
				<span class="size-10rem">
					EX-ZERO承襲SHOEI EX系列，是一款全新的全罩式安全帽，結合經典的越野風格，將功能性，舒適性和安全性融為一體。
				</span>
			</div>
		<div class=" title-block clearfix product-item">
			<div class="product-section clearfix">
				<div class=" col-xs-12 col-sm-6 col-md-6 product-img">
					<img src="{{assetRemote('image/collection/ex-zero/img/item_1.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
				</div>
				<div class="product-list hidden-md hidden-lg col-xs-12 ">
					<ul class="item-list">
						@for($i = 1; $i<=7;$i++)
							<li class="{{$i == 1? 'item_active': ''}} item">
								<a href="javascript:void(0)" class="item_{{$i}}">
									<img src="{{assetRemote('image/collection/ex-zero/product-img/' . $i . '.jpg')}}">
								</a>
							</li>
						@endfor
					</ul>
				</div>
				<div class="link hidden-md hidden-lg col-xs-12 btn-gap-top">
					<a class="product-link" href="https://www.webike.tw/sd/t00127006" target="_blank">
						<img src="{{assetRemote('image/collection/ex-zero/buttom/mobile_buy.png')}}">
					</a>
				</div>
				<div class=" col-xs-12 col-sm-6 col-md-6 text-left">
					<div class="product-text">
						<span class="size-10rem product-name">
							EX-ZERO (米白)
						</span>
						<span class="size-10rem">
							預定2018年11月底出貨
						</span>
						<span class="size-10rem product-price">
							{{ $customer_role  == 3 || $customer_role == 4 ? '預售價:NT$ 13,000(素色)' : '預售價:NT$ 13,650(素色)'}}
						</span>
						<span class="size-10rem">
							SIZE:S/M/L/XL/XXL
						</span>
					</div>
					<div class="link hidden-xs">
						<a class="product-link pc-link" href="https://www.webike.tw/sd/t00127006" target="_blank"></a>
					</div>
				</div>
			</div>
			<div class="product-list hidden-xs">
				<ul class="item-list">
					@for($i = 1; $i<=7;$i++)
						<li class="{{$i == 1? 'item_active': ''}} item">
							<a href="javascript:void(0)" class="item_{{$i}}">
								<img src="{{assetRemote('image/collection/ex-zero/product-img/' . $i . '.jpg')}}">
							</a>
						</li>
					@endfor
				</ul>
			</div>
		</div>
		<div class="clearfix product-block">
			<div class="title-block">
				<h1 class="title-text">EX-ZERO特點介紹</h1>
			</div>
			<div class=" title-block clearfix">
				<h1 class="interval text-left">EX-ZERO 外觀</h1>
				<div class="product-section clearfix">
					<div class="photo-container">
						<img src="{{assetRemote('image/collection/ex-zero/d_form.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
					</div>
					<div class="text-left descripton-container">
						<p class="size-10rem">
							新款全罩式安全帽「EX-ZERO」的設計靈感是來自於1980年代推出的SHOEI EX系列，並在外型加入了古典的越野風格。<br>
							「EX-ZERO」在後腦勺整體的鴨尾線條設計與配備鋁合金濾網的嘴部造型，雖然簡單卻十分有型。	
						</p>
					</div>
				</div>
			</div>
			<div class=" title-block  clearfix">
				<div class="interval text-left">
					<h1 class="">EX-ZERO CJ-3 Shield 內藏式鏡片</h1>
				</div>
				<div>
					<div class="interval">
						<p class="size-10rem">
							「EX-ZERO」的鏡片使用較不會變形的「CJ-3」內藏式鏡片來確保視野清晰，所以不只設計感十足，還能避免影響到配戴眼鏡或太陽眼鏡，並且可以降低在騎乘摩托車時產生的風壓。<a href="https://www.webike.tw/parts/ca/3000-3001-3018/br/666?q=CJ-3">點我查看商品</a>																						
						</p>
					</div>
					<div class="clearfix product-section">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<img src="{{assetRemote('image/collection/ex-zero/e_cj31.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<img src="{{assetRemote('image/collection/ex-zero/f_cj32.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
						</div>
					</div>
				</div>
			</div>
			<div class=" title-block  clearfix">
				<div class="interval text-left">
					<h1 class="">EX-ZERO  V-480 Visor 帽簷</h1>
				</div>
				<div>
					<div class="interval">
						<p class="size-10rem">
							為了配合古典的越野風格，新款「EX-ZERO」推出可供加購的專屬設計帽簷「V-480 Visor」，採用只要3個扣子就能簡單裝上的便利設計。	<a href="https://www.webike.tw/sd/23868198" target="_blank">點我查看商品</a>
						</p>
					</div>
					<div class="clearfix product-section">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<img src="{{assetRemote('image/collection/ex-zero/g_v4801.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<img src="{{assetRemote('image/collection/ex-zero/h_v4802.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
						</div>
					</div>
				</div>
			</div>
			<div class=" title-block  clearfix">
				<div class="interval text-left">
					<h1 class="">EX-ZERO 內襯</h1>
				</div>
				<div>
					<div class="interval">
						<p class="size-10rem">
							「EX-ZERO」的內襯全部都可以拆下清洗以保持乾淨，且表面全部都使用絨毛材質，讓騎士戴起來的感覺非常柔軟舒適。<a href="https://www.webike.tw/parts/ca/3000-3001-3019?q=EX-ZERO&sort=new">點我查看商品</a>
						</p>
					</div>
					<div class="clearfix product-section">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<img src="{{assetRemote('image/collection/ex-zero/i_interior1.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<img src="{{assetRemote('image/collection/ex-zero/j_interior2.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
						</div>
					</div>
				</div>
			</div>
			<div class="title-block">
				<h1 class="interval text-left">EX-ZERO E.Q.R.S.</h1>
				<div class="product-section clearfix">
					<div class="photo-container">
						<img src="{{assetRemote('image/collection/ex-zero/k_eqrs.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
					</div>
					<div class="text-left descripton-container">
						<p class="size-10rem">
							「EX-ZERO」配備了在發生意外時能夠快速拆掉安全帽的「E.Q.R.S.」系統，當騎士需要緊急救援時，讓救護人員可以迅速地拆掉安全帽進行急救工作。<br>
							「E.Q.R.S.」系統會在安全帽的臉頰內襯裝上專屬襯帶，在急救時只要拉下襯帶就可以輕鬆地將臉頰內襯整個拆下，安全帽就能快速地從騎士的頭上脫掉。<br>
							※一般拆卸的情況下請勿使用「E.Q.R.S.」系統拆除臉頰內襯。										
						</p>
					</div>
				</div>
			</div>
			<div class="title-block video">
				<iframe width="1170" height="720" src="https://www.youtube.com/embed/0M7mE3YNPPM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>
			<div class="title-block">
				<h1 class="interval text-left">EX-ZERO 相關新聞</h1>
				<div class="product-section clearfix">
					<div class=" col-xs-12 col-sm-4 col-md-4">
						<img src="{{assetRemote('image/collection/ex-zero/l_news.jpg')}}" alt="【SHOEI】EX-ZERO 全罩式安全帽【預購商品】">
					</div>
					<div class=" col-xs-12 col-sm-8 col-md-8 text-left">
						<p class="size-10rem">
							日本安全帽大廠「SHOEI」宣布將先在今年(2018)11月推出新款全罩式安全帽「EX-ZERO」的素色款，接著在12月推出彩繪版「EQUATION」。...<a href="https://www.webike.tw/bikenews/2018/08/16/shoei%E5%85%A8%E6%96%B0%E5%85%A8%E7%BD%A9%E5%BC%8F%E5%AE%89%E5%85%A8%E5%B8%BD%E6%AC%BE%E3%80%8Cex-zero%E3%80%8D%E5%8D%B3%E5%B0%87%E4%B8%8A%E5%B8%82/" target="_blank">Read more</a>
						</p>
					</div>
				</div>
			</div>
			<div class=" link-menu title-block text-center">
				<div class="link-menu-top-page">
					<ul class="ul-menu-top-page clearfix hidden-lg hidden-md mobile">
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="" href=https://www.webike.tw/parts?q=EX-ZERO&sort=new" title="更多 EX-ZERO 相關商品 - 「Webike-摩托百貨」" target="_blank"><img src="{{assetRemote('image/collection/ex-zero/buttom/m_bttomA.png')}}" ></a>
						</li>
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="" href="https://www.webike.tw/parts/br/666" title=" SHOEI商品一覽 - 「Webike-摩托百貨」" target="_blank">
								<img src="{{assetRemote('image/collection/ex-zero/buttom/n_bttomA.png')}}">
							</a>
						</li>
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="" href="https://www.webike.tw/bikenews/2018/08/16/shoei%E5%85%A8%E6%96%B0%E5%85%A8%E7%BD%A9%E5%BC%8F%E5%AE%89%E5%85%A8%E5%B8%BD%E6%AC%BE%E3%80%8Cex-zero%E3%80%8D%E5%8D%B3%E5%B0%87%E4%B8%8A%E5%B8%82/" title="EX-ZERO 詳細新聞報導 - 「Webike-摩托百貨」" target="_blank">
								<img src="{{assetRemote('image/collection/ex-zero/buttom/o_bttomA.png')}}">
							</a>
						</li>
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="" href="http://www.webike.tw/collection" title="其他流行特輯 - 「Webike-摩托百貨」" target="_blank">
								<img src="{{assetRemote('image/collection/ex-zero/buttom/p_bttomA.png')}}">
							</a>
						</li>
					</ul>
					<ul class="ul-menu-top-page clearfix hidden-xs pc">
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="link-1" href=https://www.webike.tw/parts?q=EX-ZERO&sort=new" title="更多 EX-ZERO 相關商品 - 「Webike-摩托百貨」" target="_blank"></a>
						</li>
						<li class="col-xs-12 col-sm-3 col-md-3  text-center ">
							<a class="link-2" href="https://www.webike.tw/parts/br/666" title=" SHOEI商品一覽 - 「Webike-摩托百貨」" target="_blank"></a>
						</li>
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="link-3" href="https://www.webike.tw/bikenews/2018/08/16/shoei%E5%85%A8%E6%96%B0%E5%85%A8%E7%BD%A9%E5%BC%8F%E5%AE%89%E5%85%A8%E5%B8%BD%E6%AC%BE%E3%80%8Cex-zero%E3%80%8D%E5%8D%B3%E5%B0%87%E4%B8%8A%E5%B8%82/" title="EX-ZERO 詳細新聞報導 - 「Webike-摩托百貨」" target="_blank"></a>
						</li>
						<li class="col-xs-6 col-sm-3 col-md-3  text-center ">
							<a class="link-4" href="http://www.webike.tw/collection" title="其他流行特輯 - 「Webike-摩托百貨」" target="_blank"></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</li>
</ul>
@section('script')
	<script src="{{ assetRemote('js/basic/jquery.js') }}"></script>
	<script src="{{ assetRemote('plugin/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ assetRemote('plugin/slick/slick.js') }}"></script>
	<script src="{{ assetRemote('js/basic/bootstrap.min.js') }}"></script>
	<script src="{{ assetRemote('plugin/select2/js/select2.min.js') }}"></script>
@stop
