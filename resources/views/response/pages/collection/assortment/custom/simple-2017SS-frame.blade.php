<link rel="stylesheet" href="{{assetRemote('css/pages/celebration/main.css')}}">
<link rel="stylesheet" href="{{assetRemote('css/pages/session-leaflet/leaflet.css')}}">
<link rel="stylesheet" href="{{assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.css')}}">
<div class="block">
    <h1>2017春夏新品</h1>
    <div class="rotation-ui">
        <ul class="bxslider">
            <li>
                <a href="{{URL::route('parts', 'br/364')}}?sort=new&rel=2017-03-2017SS" title="KOMINE{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_364.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('summary', ['section' => 'br/2162'])}}?sort=new&rel=2017-03-2017SS" title="HONDA{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_300.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/635')}}?sort=new&rel=2017-03-2017SS" title="RS TAICHI{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_635.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/1902')}}?sort=new&rel=2017-03-2017SS" title="TS DESIGN{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_1902.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/1453')}}?sort=new&rel=2017-03-2017SS" title="simpson{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_1453.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/40')}}?sort=new&rel=2017-03-2017SS" title="GOLDWIN{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_40.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/851')}}?sort=new&rel=2017-03-2017SS" title="POWERAGE{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_851.png">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/211')}}?sort=new&rel=2017-03-2017SS" title="elf{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_211.jpg">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/632')}}?sort=new&rel=2017-03-2017SS" title="elf{{$tail}}" target="_blank">
                    <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_brand_632.png">
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="block">
    <section>
        <!-- <legend>SEARCH</legend> -->
        <div class="brand-list">
            <h4>品牌一覽</h4>
            <ul>
                @foreach ($manufacturers as $key => $manufacturer)
                    <li>
                        <a href="{{URL::route('parts', 'br/'.$manufacturer->url_rewrite)}}?sort=new&rel=2017-03-2017SS" title="{{$manufacturer->name.$tail}}" target="_blank">
                            @if(strpos($manufacturer->url_rewrite, 't') === false)
                                <img src="//img.webike.net/sys_images/brand2/brand_{{$manufacturer->url_rewrite}}.gif" alt="{{$manufacturer->name.$tail}}">
                            @else
                                <img src="//assets/images/brands/promotion/brand_{{$manufacturer->url_rewrite}}.png" alt="{{$manufacturer->name.$tail}}">
                            @endif
                            <div>{{$manufacturer->name}}</div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
</div>
<div class="block">
    <section>
        <div class="category-list">
            <h4>分類一覽</h4>
            <ul>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3021-3053')}}?sort=new&rel=2017-03-2017SS" title="騎士夾克{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_jacket.png">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3060')}}?sort=new&rel=2017-03-2017SS" title="騎士手套{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_gloves.png">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3071')}}?sort=new&rel=2017-03-2017SS" title="騎士車褲{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_bottoms.png">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3021-3053')}}?q=女用&sort=new&rel=2017-03-2017SS" title="女騎士裝備{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_ladys.png">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3024')}}?sort=new&rel=2017-03-2017SS" title="内穿服{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_inner.png">
                    </a>
                </li>
            <!-- <li>
				    	<a href="{{URL::route('parts', 'ca/3000-3020-3123')}}?sort=new" title="包包{{$tail}}" target="_blank">
				    		<img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_bag.png">
				        </a>
			        </li> -->
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3034')}}?sort=new&rel=2017-03-2017SS" title="T恤{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_tshirt.png">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3263')}}?sort=new&rel=2017-03-2017SS" title="面罩、口罩{{$tail}}" target="_blank">
                        <img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_masks.png">
                    </a>
                </li>
            <!-- <li>
				    	<a href="{{URL::route('parts', 'ca/3000-1327')}}?sort=new" title="摩托車相關精品{{$tail}}" target="_blank">
				    		<img src="//img.webike.tw/assets/images/leaflet/2017SS/2017ss_apparel_relation.png">
				        </a>
			        </li> -->
            </ul>
        </div>
    </section>
</div>
<?php

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSLVERSION, 3);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_URL, "https://www.webike.net/wbs/stock-cooperation-sd-json.html?sku=" . implode(',', $product_url_rewrites));
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
$result = curl_exec($ch);
curl_close($ch);

foreach (json_decode($result) as $item) {
    if($item->stockout_flg){
        $key = current(array_keys($product_url_rewrites, $item->sku_code));
        if($key){
            unset($product_url_rewrites[$key]);
        }
    }
}

?>
<div class="block">
    <section>
        <div class="ranking-list">
            <h4>推薦商品</h4>
            <ul>
                <?php $n = 0; ?>
                @foreach ($products as $key => $product)
                    <?php
                    $n++;
                    ?>
                    <li>
                        <div class="content-box">
                            <span>{{ $n }}</span>
                            <div class="image-box">
                                <div class="helper-box">
                                    <a href="{{URL::route('product-detail', $product->url_rewrite)}}?rel=2017-03-2017SS" target="_blank">
                                        <img src="{{$product->getImage()}}"></div>
                                </a>
                            </div>
                            <div>
                                <a href="{{URL::route('product-detail', $product->url_rewrite)}}?rel=2017-03-2017SS" target="_blank">
                                    {{$product->manufacturer->name}}<br>{{$product->name}}
                                </a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
</div>
<!-- <div class="block">
    <section>
        <div class="showall">
            <a href="" title="" target="_blank">查看全部</a>
        </div>
    </section>
</div> -->
<script src="{{assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript">
    // スライダー
    jQuery(document).ready(function(){
        if(jQuery(".bxslider li").length > 1){
            var slideNum = jQuery('.bxslider li').size();
            var slider = jQuery('.bxslider').bxSlider({
                auto: true,
                autoHover: true,
                slideMargin: 20,
                nextText: '>',
                prevText: '<',
                onSliderLoad:function(currentIndex){
                    jQuery('.bxslider li').removeClass('active');
                    jQuery('.bxslider li:nth-child(3n-1)').addClass('active');
                },
                onSlideBefore: function($slideElement, oldIndex, newIndex){
                    var new_i = newIndex%3 - 1;
                    var nth = (new_i < 0) ? '3n-1' : '3n'+new_i;
                    jQuery('.bxslider li').removeClass('active');
                    jQuery('.bxslider li:nth-child('+nth+')').addClass('active');
                }
            });
        }else{
            jQuery(".bxslider li").addClass("active");
        }

        $('.bx-controls a').click(function(){
            slider.stopAuto();
            slider.startAuto();
        });
    });
</script>