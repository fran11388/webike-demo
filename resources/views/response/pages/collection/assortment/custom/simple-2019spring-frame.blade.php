@if(!$layout)
    <link rel="stylesheet" href="{{assetRemote('css/pages/celebration/main.css')}}">
@endif
<link rel="stylesheet" href="{{assetRemote('css/pages/session-leaflet/leaflet.css')}}">
<link rel="stylesheet" href="{{assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.css')}}">
@if($layout)
    <link rel="stylesheet" href="{{assetRemote('css/pages/collection/season/season-1columns.css')}}">
    <style>
        .rotation-ui .bx-controls-direction a.bx-prev {
            color: #fff;
            left: -70px;
            background: url('https://img.webike.tw/assets/images/shared/20160226_2016ss_apparel_controls.png') no-repeat 0 -32px;
        }
        .rotation-ui .bx-controls-direction a.bx-next {
            color: #fff;
            right: -70px;
            background: url('https://img.webike.tw/assets/images/shared/20160226_2016ss_apparel_controls.png') no-repeat -43px -32px;
        }
    </style>
@endif
<style>
    #pagetop {
        bottom: 55px;
        font-size: 10px;
        position: fixed;
        right: 10px;
        z-index: 9999; }
    #pagetop a {
        background: none repeat scroll 0 0 #666666;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        color: #FFFFFF;
        display: block;
        opacity: 0.8;
        padding: 15px;
        text-align: center;
        text-decoration: none; }
    #pagetop i {
        background: none no-repeat scroll 9px 9px #666666;
        background-image: url("//img-webike-tw-370429.c.cdn77.org/shopping/image/return_top.png");
        color: #FFFFFF;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        display: none;
        opacity: 0.8;
        padding: 15px;
        text-align: center;
        text-decoration: none; }
    .recommend .row{
        margin-left: 0px;
        margin-right: 0px;
    }
    .recommend h4{
        margin-bottom: 20px;
    }
    .recommend ul li {
        list-style: none;
        padding: 5px;
    }
    .recommend .owl-carousel .owl-item img{
        height: 300px;
    }
    .recommend .row .box-content{
        border: none;
    }
    .footer{
        margin-top: 100px;
    }
    .category-list ul li a img:hover{
        outline: 3px solid #ccc;
    }
    .block .brand-list ul li{
        width: 33%;
    }
    @media all and (max-width: 767px) {
        #pagetop a {
            display: none; }
        #pagetop i {
            display: block; }
        #pagetop {
            right:auto;
            left:280px;
        }
    }
</style>
<div class="block">
    <h1>2019春夏新品</h1>
    <div class="rotation-ui">
        <ul class="bxslider">
            <li>
                <a href="{{URL::route('parts', 'br/635')}}?sort=new&rel=2019-03-2019spring" title="RS TAICHI{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/RSTAICHI.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/1375')}}?sort=new&rel=2019-03-2019spring" title="POWERAGE{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/POWERAGE.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/1206')}}?sort=new&rel=2019-03-2019spring" title="FIVE{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/FIVE.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/1418')}}?sort=new&rel=2019-03-2019spring" title="BATES{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/BATES.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/211')}}?sort=new&rel=2019-03-2019spring" title="elf{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/elf.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/t3784')}}?sort=new&rel=2019-03-2019spring" title="307P{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/307P.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/1528')}}?sort=new&rel=2019-03-2019spring" title="URBANISM{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/URBANISM.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/267')}}?sort=new&rel=2019-03-2019spring" title="GOLDWIN{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/2019spring/GOLDWIN.jpg') }}">
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="block">
    <section>
        <!-- <legend>SEARCH</legend> -->
        <div class="brand-list">
            <h4>品牌一覽</h4>
            <ul>
                @foreach ($manufacturers as $key => $manufacturer)
                    <li>
                        <a href="{{URL::route('parts', 'br/'.$manufacturer->url_rewrite)}}?sort=new&rel=2019-03-2019spring" title="{{$manufacturer->name.$tail}}" target="_blank">
                            @if(strpos($manufacturer->url_rewrite, 't') === false)
                                <img src="//img.webike.net/sys_images/brand2/brand_{{$manufacturer->url_rewrite}}.gif" alt="{{$manufacturer->name.$tail}}">
                            @else
                                <img src="https://img.webike.tw/assets/images/brands/promotion/brand_t3784.jpg" alt="{{$manufacturer->name.$tail}}">
                            @endif
                            <div>{{$manufacturer->name}}</div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
</div>
<div class="block">
    <section>
        <div class="category-list">
            <h4>分類一覽</h4>
            <ul>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3021')}}?sort=new&rel=2019-03-2019spring" title="騎士夾克{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3021.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3060')}}?sort=new&rel=2019-03-2019spring" title="騎士手套{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3060.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3071')}}?sort=new&rel=2019-03-2019spring" title="騎士車褲{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3071.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-1325')}}?sort=new&rel=2019-03-2019spring" title="騎士靴{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/1325.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3123')}}?sort=new&rel=2019-03-2019spring" title="包包{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3123.jpg') }}">
                    </a>
                </li> 
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3034')}}?sort=new&rel=2019-03-2019spring" title="T-SHIRT{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3034.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3024')}}?sort=new&rel=2019-03-2019spring" title="內穿服{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3024.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3268')}}?sort=new&rel=2019-03-2019spring" title="涼感用品{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3268.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3028')}}?sort=new&rel=2019-03-2019spring" title="帽子{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/2019spring/ca/3028.jpg') }}">
                    </a>
                </li>
            </ul>
        </div>
    </section>
</div>

@if(isset($products))
    
    <div class="block">
        <section>
            <div class="recommend">
                <h4>推薦商品</h4>
                @foreach($products as $key => $product)
                    @if(count($product))
                        @php
                           switch ($key)
                                {
                                    case '3021':
                                        $ca_name = "騎士夾克";
                                        break;
                                    case '3060':
                                        $ca_name = "騎士手套";
                                        break;
                                    case '3071':
                                        $ca_name = "騎士車褲";
                                        break;
                                    case '1325':
                                        $ca_name = "騎士靴";
                                        break;
                                    case '3123':
                                        $ca_name = "包包";
                                        break;
                                    case '3034':
                                        $ca_name = "T-SHIRT";
                                        break;
                                    case '3024':
                                        $ca_name = "內穿服";
                                        break;
                                    case '3268':
                                        $ca_name = "涼感用品";
                                        break;
                                    case '3028':
                                        $ca_name = "帽子";
                                        break;
                                }
                        @endphp
                    <div class="row box-content"> 
                        <div class="title-main-box-clear"><h2>{{$ca_name}}</h2></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                            <ul class="owl-carouse-advertisement owl-carousel-4">
                                @php                                    
                                    if(count($product) <= 20){
                                        $count = count($product);
                                    }else{
                                        $count = 20;
                                    }
                                    $product_rands = $product->random($count);
                                    $key = 0;
                                @endphp
                                @foreach($product_rands as $key => $product_rand)
                                        <li>
                                            <div class="image-box text-center">
                                                <div class="helper-box">
                                                    <a href="{{URL::route('product-detail', $product_rand->url_rewrite)}}?rel=2019-03-2019spring" target="_blank">
                                                        <img src="{{ $product_rand->image }}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="size-10rem">
                                                <a href="{{URL::route('product-detail', $product_rand->url_rewrite)}}?rel=2019-03-2019spring" target="_blank">
                                                    {{$product_rand->manufacturer_name}}<br>{{$product_rand->product_name}}
                                                </a>
                                            </div>
                                        </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </section>
    </div>
@endif

<!-- <div class="block">
    <section>
        <div class="showall">
            <a href="" title="" target="_blank">查看全部</a>
        </div>
    </section>
</div> -->
<script src="{{assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript">
    // スライダー
    jQuery(document).ready(function(){
        if(jQuery(".bxslider li").length > 1){
            var slideNum = jQuery('.bxslider li').size();
            var slider = jQuery('.bxslider').bxSlider({
                auto: true,
                autoHover: true,
                slideMargin: 20,
                nextText: '>',
                prevText: '<',
                onSliderLoad:function(currentIndex){
                    jQuery('.bxslider li').removeClass('active');
                    jQuery('.bxslider li:nth-child(3n-1)').addClass('active');
                },
                onSlideBefore: function($slideElement, oldIndex, newIndex){
                    var new_i = newIndex%3 - 1;
                    var nth = (new_i < 0) ? '3n-1' : '3n'+new_i;
                    jQuery('.bxslider li').removeClass('active');
                    jQuery('.bxslider li:nth-child('+nth+')').addClass('active');
                }
            });
        }else{
            jQuery(".bxslider li").addClass("active");
        }

        $('.bx-controls a').click(function(){
            slider.stopAuto();
            slider.startAuto();
        });
    });
</script>
