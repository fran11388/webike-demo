
	<link type="text/css" rel="stylesheet" href="https://www.webike.tw/plugin/lightGallery/src/css/lightgallery.css" />
<style>
	.contents-all-page{
    	background-color: #081928;
	}
	.pages .product-container-block{
	    background-color: #414141;
	    border: 1px solid #e61e25;
	}
	div.link-page {
		margin-top: 20px;
	}
	#m-menu-execute{
		display: none;
	}
	.media .page-selector{
	    background-color: #414141;
	    box-shadow: 2px 2px 0px #040404;
	    color: #e61e25;
	    font-weight: bold;
	}
	.media .block-height{
		height: 126px;
	}

	.container .link-url {
		border: 1px solid;
    	padding: 15px;
    	border-radius: 5px;
    	color: #416090;
    	width:216px;
    	height:62px;
	}
	.container .closer-media {
		display:none;
	}
	.container ul {
		list-style-type:none;
	}
	.banner-container {

	}
	.link-container ul li a{
		min-height:61.7px;
		background-size: 100% 100% !important;
		margin-left:auto;
		margin-right:auto;
		display:block;
	}
	.import-container ul li a{
		min-height:123.7px;
		background-size: 100% 100% !important;
		margin-left:auto;
		margin-right:auto;
		display:block;
	}
	.container .other-page{
		display: none;
	}
	.container .engine-page{
		display: none;
	}
	.container .light-page{
		display: none;
	}
	.container .large-page{
		display: none;
	}
	.import-container .page-selector-sports {
		background:url({{assetRemote('image/collection/repsol/sports-1.jpg')}});
	}
	.import-container .page-selector-sports:hover{
		background:url({{assetRemote('image/collection/repsol/sports-2.jpg')}});
	}
	.import-container .page-selector-engine {
		background:url({{assetRemote('image/collection/repsol/engine-1.jpg')}});
	}
	.import-container .page-selector-engine:hover{
		background:url({{assetRemote('image/collection/repsol/engine-2.jpg')}});
	}
	.import-container .page-selector-light {
		background:url({{assetRemote('image/collection/repsol/light-1.jpg')}});
	}
	.import-container .page-selector-light:hover{
		background:url({{assetRemote('image/collection/repsol/light-2.jpg')}});
	}
	.import-container .page-selector-large {
		background:url({{assetRemote('image/collection/repsol/large-1.jpg')}});
	}
	.import-container .page-selector-large:hover{
		background:url({{assetRemote('image/collection/repsol/large-2.jpg')}});
	}
	.import-container .page-selector-other{
		background:url({{assetRemote('image/collection/repsol/other-1.jpg')}});
	}
	.import-container .page-selector-other:hover{
		background:url({{assetRemote('image/collection/repsol/other-2.jpg')}});
	}
	.link-page .link-url{
		background:url({{assetRemote('image/collection/repsol/more-01.jpg')}});
		min-height: 70px;
	    background-size: 100% 100% !important;
	    margin-left: auto;
	    margin-right: auto;
	    display: block;
	    border: 0px;	
	}
	.link-url:hover{
		background:url({{assetRemote('image/collection/repsol/more-02.jpg')}});
	}
	.link-container .lowprice{
		background:url({{assetRemote('image/collection/rainGear/2017/lowprice2.png')}});
	}
	.link-container .highperformance{
		background:url({{assetRemote('image/collection/rainGear/2017/highperformance2.png')}});
	}
	.link-container .unique{
		background:url({{assetRemote('image/collection/rainGear/2017/unique2.png')}});
	}
	.link-container .inner{
		background:url({{assetRemote('image/collection/rainGear/2017/inner2.png')}});
	}
	.link-container .maintenance{
		background:url({{assetRemote('image/collection/rainGear/2017/maintenance2.png')}});
	}
	.link-container .lowprice-main .lowprice:hover,.link-container .lowprice-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/lowprice1.png')}});
	}
	.link-container .highperformance-main .highperformance:hover,.link-container .highperformance-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/highperformance1.png')}});
	}
	.link-container .unique-main .unique:hover,.link-container .unique-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/unique1.png')}});
	}
	.link-container .inner-main .inner:hover,.link-container .inner-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/inner1.png')}});
	}
	.link-container .maintenance-main .maintenance:hover,.link-container .maintenance-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/maintenance1.png')}});
	}
	.description-container {
		color: white;
		padding:10px;
		text-align: center;
		vertical-align: middle;
	    display: inline-block;
	    float: none;
	}
	.product-container ul li .color ul li div {
		width: 18px;
	    height: 18px;
	    border: 1px solid #ccc;
	    padding: 1px;
	    box-sizing: content-box;
	}
	.product-container {
		border-bottom: 2px solid #eee;
		padding:20px 0px;
	}
	.product-container ul li .color ul li{
		float:left;
		margin-right:5px;
	}
	.product-container ul li .size ul li{
		float:left;
		margin-right:5px;
	}
	.product-container .select img{
		display:block;
		max-height: 292px;
		min-height:292px;
		margin-right: auto;
		margin-left:auto;
	}
	.product-container .small-photo{
		display:block;
		margin-right:auto;
		margin-left:auto;
		font-size:0px;
		height:142px;
	}
	.product-container .small-photo img{
		max-height:127px;
	}
	.container .blocknone {
		display:none;
	}
	.product-container .photos .main-photo .aniimated-thumbnials a{
		border:1px solid #ddd;
		display:none;
	}
	.product-container .photos .main-photo .aniimated-thumbnials .photo {
		padding:0px;
		background-color: #fff;
	}
	.product-container .photos .main-photo .select {
		display:block !important;
	}
	.product-container .photos .main-photo .small-select {
		display:block !important;
	}
	.information .button {
		width:140px;
		height:42px;
		background-size:100% 100%;
		background: url({{assetRemote('image/collection/rainGear/2017/button.png')}}) no-repeat;
		display:block;
	}
	.information .button:hover {
		background: url({{assetRemote('image/collection/rainGear/2017/button_hover.png')}}) no-repeat;
	}
	.import-container .media {
		display:none;
	}
	.brand-photo-container div.brand-photo{
		min-height: 123.7px;
	    background-size: 100% 100% !important;
	    margin-left: auto;
	    margin-right: auto;
	    display: block;
	}
	.brand-photo-container .brand-photo-sports{
		background:url({{assetRemote('image/collection/repsol/sports.jpg')}});
	}
	.brand-photo-container .brand-photo-engine{
		background:url({{assetRemote('image/collection/repsol/engine.jpg')}});
	}
	.brand-photo-container .brand-photo-light{
		background:url({{assetRemote('image/collection/repsol/light.jpg')}});
	}
	.brand-photo-container .brand-photo-large{
		background:url({{assetRemote('image/collection/repsol/large.jpg')}});
	}
	.brand-photo-container .brand-photo-other{
		background:url({{assetRemote('image/collection/repsol/other.jpg')}});
	}
	.brand-photo-container {
		margin: 0 10%;
		height: 100%;
		float: none;
		vertical-align: middle;
		display: inline-block;
	}
	.brand-block{
	    background: #e5002b;
	}
	.import-container ul li div.page-selector-block{
		height: 20px;
	}
	.import-container ul li div.page-selector-block.active{
	    background: #e5002b;
	}
	.import-container .page-selector-container{
		width: 20%;
		float: left;
		padding: 0 15px;
	}
	.product-container-block .product-photo{
		height: 100%;
		float: none;
		vertical-align: middle;
		display: inline-block;
		padding: 0px;
	}
	.product-container-block .information{
		vertical-align: middle;
		display: inline-block;
		float: none;
	} 
	@media (max-width: 767px) {
		.product-container .closer {
			display:none;
		}
		.product-container .closer-media {
			display:block;
		}
		.product-container .small-photo {
			max-height:230px !important;
			min-height:auto !important;
			height:auto;
		}
		.product-container .small-photo img{
			max-height:200px !important;
			min-height:auto !important;
			margin-right:auto;
			margin-left:auto;
		}
		.link-container li {
			margin-bottom:10px;
		}
		.product-container .select{
			margin:right:0px;
			margin-bottom:10px;
		}
		.product-container .select img{
			max-height: 230px;
			min-height: auto;
		}
		.product-container .button {
			margin-right:auto;
			margin-left:auto;
		}
		.import-container .media {
			display:block;
		}
		.import-container ul li a {
			overflow: hidden;
			min-height: auto;
		}
		.import-container .pc {
			display:none;
		}
		.brand-photo-container {
			margin: 0px;
		}
		.brand-photo-container div.brand-photo{
			width:50%;
		    min-height: 100px;
		}
		.pages .product-container-block{
			padding-top: 20px;
		}
	}
	.media .active {
		background-color: #757575 !important;
	}
	.media div.right li:nth-child(2){
		margin: 5px 0px;
	}
	.media div.left li:nth-child(2){
		margin-top: 5px;
	}
	.media .left a{
	    min-height: 59.5px;
	}
	.media .left .type-text {
		vertical-align: middle;
    	display: inline-block;
	}
	.media .left .type-text-row{
	    height: 54px;
   		vertical-align: middle;
    	display: inline-block;
	}
	.media .left{
		padding-right: 5px;
	}
	.media .right{
		padding-left: 5px;
	}
	.breadcrumb-item a{
		color: #ccc;
	}
	.breadcrumb>.active{
		color: #9a9a9a;
	}

</style>
	<div class="banner-container block">
		<img src="{{assetRemote('image/collection/repsol/repsol3702.jpg')}}" title="{!! $assortment->name !!}" alt="{!! $assortment->name !!}">
	</div>
	<div class="import-container ">
		<ul class="clearfix row pc">
			@foreach($page_types as $type_key => $page_type )
				<li class="page-selector-container">
					<a href="javascript:void(0)" class="page-selector page-selector-{!! $type_key !!} {!! $type_key !!}-link {{$type_key == 'sports' ? 'active' : ''}}" data-page-type="{!! $type_key !!}">
					</a>
					<div class=" page-selector-block {{$type_key == 'sports' ? 'active' : ''}}"></div>
				</li>
			@endforeach
		</ul>
		<ul class="clearfix row block media">
			<div class="col-md-6 col-sm-6 col-xs-6 left block-height">
				@foreach($page_types as $type_key => $page_type )
					@if($type_key == 'sports' or $type_key == 'light')
						<li>
							<a href="javascript:void(0)" class="btn btn-warning brand-block page-selector {!! $type_key !!}-link {{$type_key == 'sports' ? 'active' : ''}}" data-page-type="{!! $type_key !!}">
								<span class="type-text">{!! $page_type !!}</span>
								<span class="type-text-row"></span>
							</a>
						</li>
					@endif
				@endforeach
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6 right block-height">
				@foreach($page_types as $type_key => $page_type )
					@if($type_key == 'engine' or $type_key == 'large' or $type_key == 'other')
						<li class=""><a href="javascript:void(0)" class="btn btn-warning brand-block page-selector {!! $type_key !!}-link" data-page-type="{!! $type_key !!}">{!! $page_type !!}</a></li>
					@endif
				@endforeach
			</div>
		</ul>
	</div>
	@php 
		$num = 1;
		$i = 0;
	@endphp	
	@foreach($page_types as $type_key => $page_type )
		@php
			$products = $data->$page_type;
		@endphp
		<div class="type-pages {{$type_key.'-page' }}">
			@foreach($products as $page => $products_info)
				<div class="pages">
					<div class="clearfix brand-block">
						<div class="brand-photo-container col-md-2 col-sm-2 col-xs-12">
							<div  class="brand-photo brand-photo-{!! $type_key !!}"></div>
						</div>
						<div class="description-container col-md-7 col-sm-7 col-xs-12">
							<p class="size-10rem">{{$description[$i]['0']}}</p>
							<p class="size-10rem">{{$description[$i]['1']}}</p>
						</div>
					</div>
					@php
						$i ++;
					@endphp
					@foreach($products_info as $product)
						<div class="product-container wear_list">
							<ul>
								<li class="clearfix product-container-block">  
									<div class="closer product-photo col-md-5 col-sm-5 col-xs-12">
										<div class="photos">
											<ul class="clearfix">
												<li class=" main-photo">
													<div class="aniimated-thumbnials clearfix">
														@foreach($product->main_images as $key => $image)
															@if($key == 0)
																<a href="{{$image}}" class="select {{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo  text-center">
															@else
																<a href="{{$image}}" class="{{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo  text-center">
															@endif
																<img src="{{$image}}" title="repsol-{{$product->name}}">
															</a>
														@endforeach
														@foreach($product->other_images as $other_image)
															<a href="{{$other_image}}" class="col-md-4 col-sm-4 col-xs-12 small-select small-photo box text-center">
																<img src="{{$other_image}}" title="repsol-{{$product->name}}">
																<span class="helper"></span>
															</a>
														@endforeach
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class=" information col-md-6 col-sm-6 col-xs-12">
										<div class="name box">
											<a href="{{$product->link}}" target="_blank">
												<span class="size-10rem font-color-white">{{$product->full_name}}</span>
											</a>
										</div>
										<div class="price box">
											<span class="font-color-red size-10rem">販賣價格: NT$ {{round($product->getFinalPrice($current_customer))}}</span>
										</div>
										@if($product->colors)
											<div class="color box">
												<ul class="clearfix">
													@foreach($product->colors as $color)
														<li>
															<div style="background-color: {{$color}}"></div>
														</li>
													@endforeach
												</ul>
											</div>
										@endif
										@if($product->getGroupSelectsAndOptions())
											<div class="size box">
												<ul class="clearfix">
													@php 
														$size_groups = $product->getGroupSelectsAndOptions();
													@endphp
													@foreach($size_groups as $size_group)
														@foreach($size_group->options as $size)
															@php 
																$str_size = str_replace('尺寸：','',$size->name);
															@endphp
															@if($str_size == 'S')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/s.png')}}" title="repsol-{{$product->name.'-尺寸 S'}}">
																</li>
															@elseif($str_size == 'M')														
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/m.png')}}" title="repsol-{{$product->name.'-尺寸 M'}}">
																</li>
															@elseif($str_size == 'L')														
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/L.png')}}" title="repsol-{{$product->name.'-尺寸 L'}}">
																</li>
															@elseif($str_size == 'XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/XL.png')}}" title="repsol-{{$product->name.'-尺寸 XL'}}">
																</li>
															@elseif($str_size == '2XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/2XL.png')}}" title="repsol-{{$product->name.'-尺寸 2XL'}}">
																</li>
															@elseif($str_size == '3XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/3XL.png')}}" title="repsol-{{$product->name.'-尺寸 3XL'}}">
																</li>
															@elseif($str_size == '4XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/4XL.png')}}" title="repsol-{{$product->name.'-尺寸 4XL'}}">
																</li>
															@endif
														@endforeach
													@endforeach
												</ul>
											</div>
										@endif
										<div class="photos box closer closer-media col-md-5 col-sm-5 col-xs-12">
											<ul class="clearfix">
												<li class=" main-photo">
													<div class="aniimated-thumbnials clearfix">

														@foreach($product->main_images as $key => $image)
															@if($key == 0)
																<a href="{{$image}}" class="select {{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo text-center">
															@else
																<a href="{{$image}}" class="{{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo text-center">
															@endif
																<img src="{{$image}}" title="repsol-{{$product->name}}">
															</a>
														@endforeach
														@foreach($product->other_images as $other_image)
															<a href="{{$other_image}}" class="col-md-4 col-sm-4 col-xs-12 small-select small-photo box text-center">
																<img src="{{$other_image}}" title="repsol-{{$product->name}}"><span class="helper"></span>
															</a>
														@endforeach
													</div>
												</li>
											</ul>
										</div>
										<div class="text box">
											@if($product->description)
												<span class="font-color-white">
													@if($product->description != strip_tags($product->description))
														{!! $product->description !!}
													@else
														{!! nl2br($product->description) !!}
													@endif
												</span>
											@else
												<span>
													@php
														$product->noTranslateButton = true;
														$description_html = view('response.pages.product-detail.partials.info-detail-description', ['product' => $product])->render();
														echo strip_tags($description_html, '<br><br/><br />');
													@endphp
												</span>
											@endif
										</div>

										<div class="block">
											<a href="{{$product->link}}" class="button" target="_blank"></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					@endforeach
				</div>
				@php
				 $num++;
				@endphp
			@endforeach
			<div class="text-center width-full block link-page">
				<a class="link-url btn btn-default" target="_blank" href="https://www.webike.tw/parts/ca/4000-4001/br/t1975"></a>
			</div>
		</div>
	@endforeach


	<script src="{!! assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe-ui-default.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/elevatezoom-master/jquery.elevateZoom-3.0.8.min.js') !!}"></script>


{{--     <script src="{!! assetRemote('plugin/lightslider/src/js/lightslider.js') !!}"></script> --}}
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lightgallery.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lg-zoom.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lg-thumbnail.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lg-thumbnail.min.js?time=') !!}"></script>

	<script>
		$('.aniimated-thumbnials').lightGallery({
		    thumbnail:true
		});
	</script>
	<script>
		$('.import-container .media li').click(function(){
			$('.import-container ul li a').removeClass('active');
			$(this).children('a').addClass('active');
		});
		function slipTo1(element){
			if (window.matchMedia('(max-width: 767px)+').matches) {
		        var y = parseInt($(element).offset().top) - 80;
		        $('html,body').animate({scrollTop: y}, 400);
		    }
	    }
		$('.page-selector').click(function(){
			console.log(this);
			$(this).addClass('active');
			$('.page-selector-block').removeClass('active');
			$(this).closest('li').find('div').addClass('active');
			$(this).parent().siblings().children('.page-selector').removeClass('active');
			var pageType = $(this).attr('data-page-type');
			$('.' + pageType + ' .lowprice').addClass('active');
			$('.menu6').removeClass('blocknone');
			$('.type-pages').hide();
			$('.type-pages').find('a').removeClass('active');
			$('.type-pages').find('li:first-child').find('a').addClass('active');
			$('.type-page-links').hide();
			$('.type-page-links').find('a').removeClass('active');
			$('.type-page-links').find('li:first-child').find('a').addClass('active');
			$('.' + pageType + '-page').show();
			$('.' + pageType + '-links').show();
			$('.import').hide();
		});
		if (window.matchMedia('(max-width: 767px)').matches) {
        	$(".wear_list ul li .information .color ul li").click(function(){
				var num = $(this).index();
				$(this).closest('.information').find(".closer").find('.select').removeClass("select");
				$(this).closest('.information').find(".closer").find('.aniimated-thumbnials').children().eq(num).addClass("select");
			});	
	    } else {
	        $(".wear_list ul li .information .color ul li").hover(function(){
				var num = $(this).index();
				$(this).closest('.information').siblings(".closer").find('.select').removeClass("select");
				$(this).closest('.information').siblings(".closer").find('.aniimated-thumbnials').children().eq(num).addClass("select");
			});	
	    }

	</script>
