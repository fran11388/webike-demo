<aside class="ct-left-search-list">
    <ul class="ul-left-title">
        <!--Category-->
        <li class="li-left-title box-page-group col-md-12">
            <div class="title-box-page-group">
                <h3>Young Machine</h3>
            </div>
            <div class="ct-menu-left-collection">
                <img src="//www.webike.tw/assets/images/collection/bigmachine/Young_Machine.jpg">
            </div>
        </li>
        <!--/.category-->
    </ul>
    <div class="box-page-group col-md-12">
        <div class="ct-menu-left-collection">
            <h2>Young Machine是?</h2>
            <span>最強的2輪採購指南雜誌，從大型重機到小排氣量的摩托車、最新型的車款、周邊消息，提供最新的二輪情報。</span>
        </div>
    </div>
</aside>