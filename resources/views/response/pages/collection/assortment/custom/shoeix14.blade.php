<head>
	<style>
		li {
			margin-bottom:10px;
			list-style-type: none;
		}
		.main-col {
			width:100% !important;
			padding:0px !important;
		}
		.product-info {
			background: #F5F5F5;
		}
		.product-design {
			background: #ebebeb;
		}
		.intrinsic-container {
		  position: relative;
		  height: 0;
		  overflow: hidden;
		}
		 
		/* 16x9 Aspect Ratio */
		.intrinsic-container-16x9 {
		  padding-bottom: 56.25%;
		}
		 
		/* 4x3 Aspect Ratio */
		.intrinsic-container-4x3 {
		  padding-bottom: 75%;
		}
		 
		.intrinsic-container iframe {
		  position: absolute;
		  top:0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		}
		.interval {
			padding-bottom:20px;
		}
/*		div.col-sm-2 {
			padding:0px;
		}*/
	</style>
</head>
<ul class="content-promotion-col">
	<li class="main-col">
		<img class="col-xs-12 col-sm-12 col-md-12 " src="//www.webike.tw/assets/images/collection/brand/shoeix14/20160212_shoei_x14_top2.jpg" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽【預購商品】">
		<div class="col-xs-12 col-sm-12 col-md-12  text-center ">
			<h1 class="title-top">SHOEI X-14 全色系在庫販售中!</h1>
			<span class="size-12rem">
				在巔峰賽事Moto GP鍛鍊而成的SHOEI賽車款安全帽 「X-系列」。<br>
				除了擁有高安全性以外，即使在超過300km／h的高速騎乘下，也能保有安定空氣動力性能的革命性流線設計。<br>
				為了因應前傾騎乘時不舒服的姿勢，利用全新概念誕生而成的可調式內裝系統。<br>
			</span>
		<div>
		<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		<div class="col-xs-12 col-sm-10 col-md-10   text-center ">
			<div class="menu-top-page">
				<ul class="ul-menu-top-page ">
					<li class="col-xs-12 col-sm-4 col-md-4  text-center ">
						<img src="//img.webike.net/catalogue/images/21617/x14b.jpg" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽">
						<h5 class="size-12rem interval">黑色</h5>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="col-xs-12 col-sm-1 col-md-1"></div>
							<div class="class=col-xs-12 col-sm-10 col-md-10">
								<a href="http://www.webike.tw/sd/22605064" title="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽" target="_blank">
									立即購買
								</a>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1"></div>
						</div>
					</li>
					<li class="col-xs-12 col-sm-4 col-md-4  text-center ">
						<img src="//img.webike.net/catalogue/images/21617/x14mb.jpg" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽">
						<h5 class="size-12rem interval">消光黑</h5>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="col-xs-12 col-sm-1 col-md-1"></div>
							<div class="class=col-xs-12 col-sm-10 col-md-10">
								<a href="http://www.webike.tw/sd/22605070" title="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽" target="_blank">
									立即購買
								</a>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1"></div>
						</div>
					</li>
					<li class="col-xs-12 col-sm-4 col-md-4  text-center ">
						<img src="//img.webike.net/catalogue/images/21617/x14w.jpg" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽">
						<h5 class="size-12rem interval ">白色</h5>
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="col-xs-12 col-sm-1 col-md-1"></div>
							<div class="class=col-xs-12 col-sm-10 col-md-10">
								<a href="http://www.webike.tw/sd/22605076" title="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽" target="_blank">
									立即購買
								</a>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1"></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class=" col-xs-12 col-sm-1 col-md-1  text-center ">

		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12  text-center product-info">
			<div class=" col-xs-12 col-sm-12 col-md-12  text-center product-info">
				<h1 class="title-top interval">SHOEI X-14 商品資訊</h1>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
			<div class=" col-xs-12 col-sm-10 col-md-10  text-center ">
				<div class=" col-xs-12 col-sm-6 col-md-6  text-center interval">
					<h1>SHOEI X-14 官方影片</h1>
					<div class="intrinsic-container intrinsic-container-16x9">
						<iframe src="https://player.vimeo.com/video/142532175?title=0&amp;byline=0&amp;portrait=0" width="100%" height="" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
					</div>
				</div>
				<div class=" col-xs-12 col-sm-6 col-md-6  text-center interval">
					<h1>Webike TV SHOEI X-14 最新設計介紹</h1>
					<div class="intrinsic-container intrinsic-container-16x9">
						<iframe width="100%" height="" src="https://www.youtube.com/embed/-mlpzNSpEl4" frameborder="0" allowfullscreen=""></iframe>
					</div>
				</div>
				<div class=" col-xs-12 col-sm-12 col-md-12  text-center ">
					<h1 class="title-top interval">SHOEI X-14 ( X-FOURTEEN) 實品搶先開箱</h1>
				</div>
				<div class=" col-xs-12 col-sm-12 col-md-12  text-center interval">
					<a href="http://www.webike.tw/information/news/buyers-collection-shoei-x-14-x-fourteen-%E5%AF%A6%E5%93%81%E6%90%B6%E5%85%88%E9%96%8B%E7%AE%B1%E6%96%87/" title="SHOEI X-14 ( X-FOURTEEN) 實品搶先開箱" target="_blank">
						<img style="width:100%" src="//www.webike.tw/assets/images/collection/brand/shoeix14/SHOEIX14HEAD1040.png" alt="SHOEI X-14 ( X-FOURTEEN) 實品搶先開箱">
					</a>
				</div>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12 product-design interval">
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
			<div class=" col-xs-12 col-sm-10 col-md-10">
				<div class=" col-xs-12 col-sm-4 col-md-4">
					<img src="//www.webike.tw/assets/images/collection/brand/shoeix14/20160212_shoei_x14_image1.png" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽【預購商品】">
				</div>
				<div class=" col-xs-12 col-sm-8 col-md-8 text-left">
					<h1 class="title-top interval text-left">設計概念</h1>
					<p class="size-12rem">
						SHOEI頂級安全帽產品中的旗艦款-X系列，長久以來獲得世界各地頂尖選手的支持與好評，這次推出煥然一新的「X-14 (X-FOURTEEN)」是X系列的最新款式，將Marc Márquez、Bradley Smith等世界級頂尖賽車選手的反饋應用於商品研發，一點一滴的呈現在全新X-14(X-FOURTEEN)的細部構造上。<br>
						<br>
						在SHOEI最尖端的風洞測試設施裡，通過測試的X-14 (X-FOURTEEN) <br>流線設計，是專門為了減低選手在賽道騎乘時的空氣阻力，同時提高高速騎乘時的安定性所進行的設計，透過在風洞測試下達到最佳設計的X-14 (X-FOURTEEN) 透氣性能，與全新研發的面頰墊等，都具備引以為傲的高完成度，讓其他安全帽品牌無法望其項背極佳性能。<br>
						<br>
						增加了創新的透氣性能，加上全新 3D MAX-DRY系統內裝，使安全帽的貼和度更為提升，目前正在申請專利權的內裝調整系統，可調整安全帽內裝的位置、角度，進而能夠使賽道騎乘前傾姿勢時的上方視野擴大4度。<br>
						<br>
						SHOEI全面性的關注選手裝備，持續進行技術革新，其中X-14(X-FOURTEEN)所具備的次世代性能、調整機能，以及安全性能等，將成為帶領世界賽車選手通往頒獎台的新標準。<br>
					</p>
				</div>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12 interval">
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
			<div class=" col-xs-12 col-sm-10 col-md-10">
				<div class=" col-xs-12 col-sm-8 col-md-8 text-left">
					<h1 class="title-top interval">空氣動力性能</h1>
					<p class="size-12rem">
						曾經詢問PIT區裡的維修技師：「賽事中勝負的重要關鍵是什麼？」維修技師們一致回答：「空氣動力性能」。<br>
						<br>
						在SHOEI，我們的安全帽設計師也把這個回答奉為真理，設置在SHOEI的茨城工場內頂尖的風洞實驗設備，是SHOEI研發安全帽過程中，不可或缺的重要設備，X-14 (X-FOURTEEN) 所具備的空氣動力性能也在此誕生，SHOEI把在賽道騎乘時，在各種騎乘姿勢下最佳的空氣動力性能列為研發重點，不斷持續地鑽研，終於使X-14(X-FOURTEEN)擁有更安定的空氣動力性能。<br>
						<br>
						為了達到高速騎乘時最大的安定性，必須降低選手感受到的各種空氣阻力，X-14(X-FOURTEEN) 採用全新設計的外殼和後安定裝置，目前正在申請專利的後安定裝置系統，除了配備寬版以外，還有可依騎士喜好或根據賽道騎乘狀況調整的窄版可供選購。<br>
						<br>
						另外，沿著裝置在外殼上方和下顎部位的安定翼，使空氣能夠更有效率的通過，成功地將騎乘時產生的空氣阻力降到最低，在下顎部位更配備可拆裝的擾流板，提升高速騎乘的安定性，為次世代的安全帽帶來新的基準。<br>
					</p>
				</div>
				<div class=" col-xs-12 col-sm-4 col-md-4">
					<img src="//www.webike.tw/assets/images/collection/brand/shoeix14/20160212_shoei_x14_image2.png" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽【預購商品】">
				</div>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12 product-design interval">
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
			<div class=" col-xs-12 col-sm-10 col-md-10">
				<div class=" col-xs-12 col-sm-4 col-md-4">
					<img src="//www.webike.tw/assets/images/collection/brand/shoeix14/20160212_shoei_x14_image3.png" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽【預購商品】">
				</div>
				<div class=" col-xs-12 col-sm-8 col-md-8 text-left">
					<h1 class="title-top interval text-left">透氣性</h1>
					<p class="size-12rem">
						賽車時因悶熱造成安全帽裡的高溫，是導致選手疲勞的原因之一，透過次世代的透氣性能改善這個問題，是X-14(X-FOURTEEN) 研發概念中最重要的研發項目，同時也是SHOEI工程技師們一致的研發共識，X-14(X-FOURTEEN) 為了能夠發揮最大的排熱效果，在帽體的正面和背面分別設置了6個進氣口，達到其他品牌所沒有的高透氣性能。<br>
						<br>
						X-14(X-FOURTEEN) 配備的全新設計冷感內裝系統，可從安全帽正面的進氣口，將新鮮空氣送入騎士的兩頰，透過X-14(X-FOURTEEN) 全新設置側邊出氣口的透氣性能，能夠有效率地將騎士臉部周圍和安全帽內熱氣向外排出。<br>
						<br>
						為了達到在賽道騎乘因前傾使得臉部貼近擋風鏡時也能發揮效果， X-14(X-FOURTEEN) 配備的所有透氣性能裝置，在SHOEI自有的最先端風洞測試設施裡，都經過測試並且達到最佳性能。
					</p>
				</div>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12 interval">
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
			<div class=" col-xs-12 col-sm-10 col-md-10">
				<div class=" col-xs-12 col-sm-8 col-md-8 text-left">
					<h1 class="title-top interval">全新的擋風鏡</h1>
					<p class="size-12rem">
						X-14(X-FOURTEEN) 的CWR-F賽車用擋風鏡片，除了擁有在世界級賽事中所必須的廣闊視野，還能有效阻絕99%的有害紫外線，並透過SHOEI的3D射出成型程序，提供無扭曲的清晰騎乘視野，在鏡片的兩側加了翼肋，不僅提升了鏡片的強度，同時達到了高速騎乘時密閉性的創新設計，並搭配全新的擋風鏡片雙重鎖裝置，能夠有效的避免擋風鏡片鬆落的意外發生。<br>
						<br>
						CWR-F賽車用擋風鏡片，加上X-14(X-FOURTEEN) 擁有的創新QR-E接合部，能夠以前所未有的速度，迅速地更換擋風鏡片，並且透過重新調整安全帽鏡片座的彈簧，鏡片能夠更加順暢的開合，同時大幅提高完全關上時的緊密性，一旦關上鏡片，鏡片旋鈕處內側的扣環將會扣上外殼，以避免騎乘時因為風壓，導致鏡片打開或震動，以維持高密閉性，透過旋轉安全帽鏡片座的下方的調節器，鏡片能夠5段式的滑動，最大調整幅度可達1mm，鏡片因此能夠達到更高的密閉性。<br>
					</p>
				</div>
				<div class=" col-xs-12 col-sm-4 col-md-4">
					<img src="//www.webike.tw/assets/images/collection/brand/shoeix14/20160212_shoei_x14_image4.png" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽【預購商品】">
				</div>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		</div>
		<div class=" col-xs-12 col-sm-12 col-md-12 product-design interval">
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
			<div class=" col-xs-12 col-sm-10 col-md-10">
				<div class=" col-xs-12 col-sm-4 col-md-4">
					<img src="//www.webike.tw/assets/images/collection/brand/shoeix14/20160212_shoei_x14_image5.png" alt="【SHOEI】X-14 ( X-FOURTEEN) 全罩安全帽【預購商品】">
				</div>
				<div class=" col-xs-12 col-sm-8 col-md-8 text-left">
					<h1 class="title-top interval text-left">3D MAX-DRY系統內裝</h1>
					<p class="size-12rem">
						X-14(X-FOURTEEN)採用所有內裝裝備都可拆下清洗、更換的3D MAX-DRY 系統內裝，內裝材質選用刷毛和HYGRA的快乾合成纖維，再搭配上X-14(X-FOURTEEN)的高透氣性能，達到過去產品2倍的吸濕快乾性能，即使在激烈的賽事時大量流汗，也能夠達到安全帽內的涼快乾爽。<br>
						<br>
						沿著頭部輪廓設計而成的3D中央內襯，使騎士在高速騎乘時能夠更加集中專注，同時提供舒適的貼合度。另外，在頂部、前側、兩側、後側以及脖子等部位，都是配備可拆下的內裝裝備，同時還提供前後左右厚度5mm / 13mm的選購內裝（配備的標準為9mm），可依個人喜好進行部分調整。<br>
						<br>
						除此之外，X-14(X-FOURTEEN)採用全新設計的迴轉內裝設計，可以改變面頰墊和中央內襯的固定位置，迴轉內裝把標準的固定位置向前傾4度，以確保賽車前傾姿勢時必要且絕佳的視野。另外，為了更加提升舒適性，還另外提供耳罩、鼻罩、下巴內襯供選購。<br>
					</p>
				</div>
			</div>
			<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		</div>
		<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
		<div class="col-xs-12 col-sm-10 col-md-10   text-center ">
			<div class="menu-top-page">
				<ul class="ul-menu-top-page ">
					<li class="col-xs-12 col-sm-3 col-md-3  text-center ">
						<a href="http://www.webike.tw/information/news/buyers-collection-shoei-x-14-x-fourteen-%E5%AF%A6%E5%93%81%E6%90%B6%E5%85%88%E9%96%8B%E7%AE%B1%E6%96%87/" title="SHOEI X-14 實品開箱文 - 「Webike-摩托百貨」">SHOEI X-14 實品開箱文</a>
					</li>
					<li class="col-xs-12 col-sm-3 col-md-3  text-center ">
						<a href="http://www.webike.tw/parts/br/666" title=" - 「Webike-摩托百貨」">SHOEI 商品一覽</a>
					</li>
					<li class="col-xs-12 col-sm-3 col-md-3  text-center ">
						<a href="http://www.webike.tw/br/666" title="SHOEI 精選品牌 - 「Webike-摩托百貨」">SHOEI 精選品牌</a>
					</li>
					<li class="col-xs-12 col-sm-3 col-md-3  text-center ">
						<a href="http://www.webike.tw/collection" title="精選特輯 - 「Webike-摩托百貨」">精選特輯</a>
					</li>
				</ul>
			</div>
		</div>
		<div class=" col-xs-12 col-sm-1 col-md-1  text-center "></div>
	</li>
</ul>
@section('script')
	<script src="{{ assetRemote('js/basic/jquery.js') }}"></script>
	<script src="{{ assetRemote('plugin/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ assetRemote('plugin/slick/slick.js') }}"></script>
	<script src="{{ assetRemote('js/basic/bootstrap.min.js') }}"></script>
	<script src="{{ assetRemote('plugin/select2/js/select2.min.js') }}"></script>
	<script>
		$('.visible-sm').click(function(){
			alert(123);
		});

	</script>
@stop
