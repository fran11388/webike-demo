@if(!$layout)
    <link rel="stylesheet" href="{{assetRemote('css/pages/celebration/main.css')}}">
@endif
<link rel="stylesheet" href="{{assetRemote('css/pages/session-leaflet/leaflet.css')}}">
<link rel="stylesheet" href="{{assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.css')}}">
@if($layout)
    <link rel="stylesheet" href="{{assetRemote('css/pages/collection/season/season-1columns.css')}}">
    <style>
        .rotation-ui .bx-controls-direction a.bx-prev {
            color: #fff;
            left: -70px;
            background: url('https://img.webike.tw/assets/images/shared/20160226_2016ss_apparel_controls.png') no-repeat 0 -32px;
        }
        .rotation-ui .bx-controls-direction a.bx-next {
            color: #fff;
            right: -70px;
            background: url('https://img.webike.tw/assets/images/shared/20160226_2016ss_apparel_controls.png') no-repeat -43px -32px;
        }
    </style>
@endif
<style>
    #pagetop {
        bottom: 55px;
        font-size: 10px;
        position: fixed;
        right: 10px;
        z-index: 9999; }
    #pagetop a {
        background: none repeat scroll 0 0 #666666;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        color: #FFFFFF;
        display: block;
        opacity: 0.8;
        padding: 15px;
        text-align: center;
        text-decoration: none; }
    #pagetop i {
        background: none no-repeat scroll 9px 9px #666666;
        background-image: url("//img-webike-tw-370429.c.cdn77.org/shopping/image/return_top.png");
        color: #FFFFFF;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        display: none;
        opacity: 0.8;
        padding: 15px;
        text-align: center;
        text-decoration: none; }
    .recommend .row{
        margin-left: 0px;
        margin-right: 0px;
    }
    .recommend h4{
        margin-bottom: 20px;
    }
    .recommend ul li {
        list-style: none;
        padding: 5px;
    }
    .recommend .owl-carousel .owl-item img{
        height: 180px;
    }
    .recommend .row .box-content{
        border: none;
    }
    .footer{
        margin-top: 100px;
    }
    .category-list ul li a img:hover{
        outline: 3px solid #ccc;
    }
    .bookmark-ui{
        display: none;
    }
    @media all and (max-width: 767px) {
        #pagetop a {
            display: none; }
        #pagetop i {
            display: block; }
        #pagetop {
            right:auto;
            left:280px;
        }
    }
</style>
<div class="block">
    <h1>價格改定精選！騎士用品，改裝零件特輯</h1>
    <div class="rotation-ui">
        <ul class="bxslider">
            <li>
                <a href="{{URL::route('parts', 'br/666')}}?sort=new&rel=2018-11-specialprice" title="Shoei{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/specialprice/shoei.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/364')}}?sort=new&rel=2018-11-specialprice" title="KOMINE{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/specialprice/komine.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/49')}}?sort=new&rel=2018-11-specialprice" title="Arai{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/specialprice/arai.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/635')}}?sort=new&rel=2018-11-specialprice" title="RS TAICHI{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/specialprice/taichi.jpg') }}">
                </a>
            </li>
            <li>
                <a href="{{URL::route('parts', 'br/854')}}?sort=new&rel=2018-11-specialprice" title="YOSHIMURA{{$tail}}" target="_blank">
                    <img src="{{assetRemote('image/collection/specialprice/yishimura.jpg') }}">
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="block">
    <section>
        <!-- <legend>SEARCH</legend> -->
        <div class="brand-list">
            <h4>品牌一覽</h4>
            <ul>
                @foreach ($manufacturers as $key => $manufacturer)
                    <li>
                        <a href="{{URL::route('parts', 'br/'.$manufacturer->url_rewrite)}}?sort=new&rel=2018-11-specialprice" title="{{$manufacturer->name.$tail}}" target="_blank">
                            @if(strpos($manufacturer->url_rewrite, 't') === false)
                                <img src="//img.webike.net/sys_images/brand2/brand_{{$manufacturer->url_rewrite}}.gif" alt="{{$manufacturer->name.$tail}}">
                            @else
                                <img src="//assets/images/brands/promotion/brand_{{$manufacturer->url_rewrite}}.png" alt="{{$manufacturer->name.$tail}}">
                            @endif
                            <div>{{$manufacturer->name}}</div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
</div>
<div class="block">
    <section>
        <div class="category-list">
            <h4>分類一覽</h4>
            <ul>
                <li>
                    <a href="{{URL::route('parts', 'ca/1000-1001-1002')}}?sort=new&rel=2018-11-specialprice" title="全段排氣管{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/1.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/1000-1001-1003')}}?sort=new&rel=2018-11-specialprice" title="排氣管尾段{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/2.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/1000-1110-1121')}}?sort=new&rel=2018-11-specialprice" title="後貨架{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/3.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/1000-1110-1109')}}?sort=new&rel=2018-11-specialprice" title="頭罩、擋風鏡{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/4.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/1000-1110-1101')}}?sort=new&rel=2018-11-specialprice" title="坐墊相關零件{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/5.jpg') }}">
                    </a>
                </li> 
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3021')}}?sort=new&rel=2018-11-specialprice" title="夾克・防摔衣{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/6.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3020-3123-3128')}}?sort=new&rel=2018-11-specialprice" title="後背包{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/7.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3260-1331-3135')}}?sort=new&rel=2018-11-specialprice" title="後行李箱{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/8.jpg') }}">
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('parts', 'ca/3000-3260-1331-3125')}}?sort=new&rel=2018-11-specialprice" title="座墊包{{$tail}}" target="_blank">
                        <img src="{{assetRemote('image/collection/specialprice/ca/9.jpg') }}">
                    </a>
                </li>
            </ul>
        </div>
    </section>
</div>
@if(isset($products) and isset($categories))
    
    <div class="block">
        <section>
            <div class="recommend">
                <h4>推薦商品</h4>
                @foreach($categories as $category)
                    @if(count($products[$category]))
                        @php
                            switch ($category)
                            {
                                case '1002':
                                    $ca_name = "全段排氣管";
                                    break;
                                case '1003':
                                    $ca_name = "排氣管尾段";
                                    break;
                                case '1121':
                                    $ca_name = "後貨架";
                                    break;
                                case '1109':
                                    $ca_name = "頭罩、擋風鏡";
                                    break;
                                case '1101':
                                    $ca_name = "坐墊相關零件";
                                    break;
                                case '3021':
                                    $ca_name = "夾克・防摔衣";
                                    break;
                                case '3128':
                                    $ca_name = "後背包";
                                    break;
                                case '3135':
                                    $ca_name = "後行李箱";
                                    break;
                                case '3125':
                                    $ca_name = "座墊包";
                                    break;
                            }
                        @endphp
                    <div class="row box-content"> 
                        <div class="title-main-box-clear"><h2>{{$ca_name}}</h2></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                            <ul class="owl-carouse-advertisement owl-carousel-4">
                                @php                                    
                                    if(count($products[$category]) <= 20){
                                        $count = count($products[$category]);
                                    }else{
                                        $count = 20;
                                    }
                                    $product_rands = $products[$category]->random($count);
                                    $key = 0;
                                @endphp
                                @foreach($product_rands as $key => $product_rand)
                                        <li>
                                            <div class="image-box text-center">
                                                <div class="helper-box">
                                                    <a href="{{URL::route('product-detail', $product_rand->url_rewrite)}}?rel=2018-03-2018SS" target="_blank">
                                                        <img src="{{ $product_rand->image }}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="size-10rem">
                                                <a href="{{URL::route('product-detail', $product_rand->url_rewrite)}}?rel=2018-03-2018SS" target="_blank">
                                                    {{$product_rand->manufacturer_name}}<br>{{$product_rand->product_name}}
                                                </a>
                                            </div>
                                        </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </section>
    </div>
@endif
<div id="pagetop">
    <a href="javascript:void(0);">PAGE TOP</a>
    <i class="fa fa-angle-top" aria-hidden="true"></i>
</div>

<!-- <div class="block">
    <section>
        <div class="showall">
            <a href="" title="" target="_blank">查看全部</a>
        </div>
    </section>
</div> -->
<script src="{{assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript">
    // スライダー
    jQuery(document).ready(function(){
        if(jQuery(".bxslider li").length > 1){
            var slideNum = jQuery('.bxslider li').size();
            var slider = jQuery('.bxslider').bxSlider({
                auto: true,
                autoHover: true,
                slideMargin: 20,
                nextText: '>',
                prevText: '<',
                onSliderLoad:function(currentIndex){
                    jQuery('.bxslider li').removeClass('active');
                    jQuery('.bxslider li:nth-child(3n-1)').addClass('active');
                },
                onSlideBefore: function($slideElement, oldIndex, newIndex){
                    var new_i = newIndex%3 - 1;
                    var nth = (new_i < 0) ? '3n-1' : '3n'+new_i;
                    jQuery('.bxslider li').removeClass('active');
                    jQuery('.bxslider li:nth-child('+nth+')').addClass('active');
                }
            });
        }else{
            jQuery(".bxslider li").addClass("active");
        }

        $('.bx-controls a').click(function(){
            slider.stopAuto();
            slider.startAuto();
        });
    });

    $(document).ready(function(){
        jQuery(function(){
            var topBtn = jQuery('#pagetop');
            topBtn.hide();
            jQuery(window).scroll(function () {
                if (jQuery(this).scrollTop() > 100) {
                    topBtn.fadeIn();
                } else {
                    topBtn.fadeOut();
                }
            });
            //スクロールしてトップ
            topBtn.click(function () {
                jQuery('body,html').animate({
                    scrollTop: 0
                }, 500);
                return false;
            });

            var agent = navigator.userAgent;
            if (agent.indexOf('iPhone') > 0 ||
                (agent.indexOf('Android') > 0 && agent.indexOf('Mobile') > 0)) {
                jQuery("a").attr("target","_top");
            }

        });

    });
</script>
