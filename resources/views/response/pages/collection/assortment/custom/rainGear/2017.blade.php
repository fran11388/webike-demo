
	<link type="text/css" rel="stylesheet" href="https://www.webike.tw/plugin/lightGallery/src/css/lightgallery.css" />
<style>
	.container .other-link {
		border: 1px solid;
    	padding: 15px;
    	border-radius: 5px;
    	color: #416090;
    	width:216px;
    	height:62px;
	}
	.container .other-link:hover {
		background: #416090;
	    color: #fff;
	    text-decoration: none
	}
	.container .closer-media {
		display:none;
	}
	.container ul {
		list-style-type:none;
	}
	.banner-container {

	}
	.link-container ul li a{
		min-height:61.7px;
		background-size: 100% 100% !important;
		margin-left:auto;
		margin-right:auto;
		display:block;
	}
	.import-container ul li a{
		min-height:123.7px;
		background-size: 100% 100% !important;
		margin-left:auto;
		margin-right:auto;
		display:block;
	}
	.import-container .page-selector-import {
		background:url({{assetRemote('image/collection/rainGear/2017/japan.png')}});
	}
	.import-container .page-selector-domestic {
		background:url({{assetRemote('image/collection/rainGear/2017/taiwan.png')}});		
	}
	.import-container .page-selector-import:hover,.page-selector-import:focus {
		background:url({{assetRemote('image/collection/rainGear/2017/japan2.png')}});
	}
	.import-container .page-selector-domestic:hover,.page-selector-domestic:focus {
		background:url({{assetRemote('image/collection/rainGear/2017/taiwan2.png')}});		
	}
	.link-container .lowprice{
		background:url({{assetRemote('image/collection/rainGear/2017/lowprice2.png')}});
	}
	.link-container .highperformance{
		background:url({{assetRemote('image/collection/rainGear/2017/highperformance2.png')}});
	}
	.link-container .unique{
		background:url({{assetRemote('image/collection/rainGear/2017/unique2.png')}});
	}
	.link-container .inner{
		background:url({{assetRemote('image/collection/rainGear/2017/inner2.png')}});
	}
	.link-container .maintenance{
		background:url({{assetRemote('image/collection/rainGear/2017/maintenance2.png')}});
	}
	.link-container .lowprice-main .lowprice:hover,.link-container .lowprice-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/lowprice1.png')}});
	}
	.link-container .highperformance-main .highperformance:hover,.link-container .highperformance-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/highperformance1.png')}});
	}
	.link-container .unique-main .unique:hover,.link-container .unique-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/unique1.png')}});
	}
	.link-container .inner-main .inner:hover,.link-container .inner-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/inner1.png')}});
	}
	.link-container .maintenance-main .maintenance:hover,.link-container .maintenance-main .active{
		background:url({{assetRemote('image/collection/rainGear/2017/maintenance1.png')}});
	}
	.description-container {
		border: 2px dotted #368fc7;
		color: #368fc7;
		padding:10px;
		text-align: center;
	}
	.product-container ul li .color ul li div {
		width: 18px;
	    height: 18px;
	    border: 1px solid #ccc;
	    padding: 1px;
	    box-sizing: content-box;
	}
	.product-container {
		border-bottom: 2px solid #eee;
		padding:20px 0px;
	}
	.product-container ul li .color ul li{
		float:left;
		margin-right:5px;
	}
	.product-container ul li .size ul li{
		float:left;
		margin-right:5px;
	}
	.product-container .select img{
		display:block;
		max-height: 292px;
		min-height:292px;
		margin-right: auto;
		margin-left:auto;
	}
	.product-container .small-photo{
		display:block;
		margin-right:auto;
		margin-left:auto;
		font-size:0px;
		height:142px;
	}
	.product-container .small-photo img{
		max-height:127px;
	}
	.container .blocknone {
		display:none;
	}
	.product-container .photos .main-photo .aniimated-thumbnials a{
		border:1px solid #ddd;
		display:none;
	}
	.product-container .photos .main-photo .aniimated-thumbnials .photo {
		margin-right:10px;
	}
	.product-container .photos .main-photo .select {
		display:block !important;
	}
	.product-container .photos .main-photo .small-select {
		display:block !important;
	}
	.information .button {
		width:140px;
		height:42px;
		background-size:100% 100%;
		background: url({{assetRemote('image/collection/rainGear/2017/button.png')}}) no-repeat;
		display:block;
	}
	.information .button:hover {
		background: url({{assetRemote('image/collection/rainGear/2017/button_hover.png')}}) no-repeat;
	}
	.import-container .media {
		display:none;
	}
	@media (max-width: 767px) {
		.product-container .closer {
			display:none;
		}
		.product-container .closer-media {
			display:block;
		}
		.product-container .small-photo {
			max-height:230px !important;
			min-height:auto !important;
			height:auto;
		}
		.product-container .small-photo img{
			max-height:200px !important;
			min-height:auto !important;
			margin-right:auto;
			margin-left:auto;
		}
		.link-container li {
			margin-bottom:10px;
		}
		.product-container .select{
			margin:right:0px;
			margin-bottom:10px;
		}
		.product-container .select img{
			max-height: 230px;
			min-height: auto;
		}
		.product-container .button {
			margin-right:auto;
			margin-left:auto;
		}
		.import-container .media {
			display:block;
		}
		.import-container ul li a {
			min-height: auto;
		}
		.import-container .pc {
			display:none;
		}
	}
	.media .active {
		background-color: red !important;
	}
	.import-container .pc .page-selector-import.active{
		background:url({{assetRemote('image/collection/rainGear/2017/japan2.png')}});
	}
	.import-container .pc .page-selector-domestic.active{
		background:url({{assetRemote('image/collection/rainGear/2017/taiwan2.png')}});
	}
	.container .domestic-page{
		display: none;
	}
</style>
	<div class="banner-container block">
		<img src="{{assetRemote('image/collection/rainGear/rainbanner.jpg')}}" title="{!! $assortment->name !!}" alt="{!! $assortment->name !!}">
	</div>
	<div class="import-container block">
		<ul class="clearfix row pc">
			@foreach($page_types as $type_key => $page_type )
				<li class="col-md-6 col-sm-6 col-xs-12"><a href="javascript:void(0)" class="page-selector page-selector-{!! $page_type !!} {!! $page_type !!}-link {{$type_key == 0 ? 'active' : ''}}" data-page-type="{!! $page_type !!}"></a></li>
			@endforeach
		</ul>
		<ul class="clearfix row block media">
			@foreach($page_types as $type_key => $page_type )
				<li class="col-md-6 col-sm-6 col-xs-6"><a href="javascript:void(0)" class="btn btn-warning page-selector {!! $page_type !!}-link {{$type_key == 0 ? 'active' : ''}}" data-page-type="{!! $page_type !!}">{!! $page_type !!}</a></li>
			@endforeach
		</ul>*
	</div>
	@php 
		$num = 1;
	@endphp	
	@foreach($page_types as $type_key => $page_type )
		@php
			$products = $data->$page_type;
		@endphp
		<div class="type-page-links link-container block {{$page_type . '-links'}}">
			<ul class="clearfix row">
				<li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12 lowprice-main" id="menu1" onclick="slipTo1('.{!! $page_type !!}-page')"><a href="javascript:void(0)" class="lowprice {{$type_key == 0 ? 'active' : ''}}"></a></li>
				<li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12 highperformance-main" id="menu2" onclick="slipTo1('.{!! $page_type !!}-page')"><a href="javascript:void(0)" class="highperformance"></a></li>
				<li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12 unique-main" id="menu3" onclick="slipTo1('.{!! $page_type !!}-page')"><a href="javascript:void(0)" class="unique"></a></li>
				<li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12 inner-main" id="menu4" onclick="slipTo1('.{!! $page_type !!}-page')"><a href="javascript:void(0)" class="inner"></a></li>
				<li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12 maintenance-main" id="menu5" onclick="slipTo1('.{!! $page_type !!}-page')"><a href="javascript:void(0)" class="maintenance"></a></li>
			</ul>
		</div>
		<div class="type-pages {{$page_type.'-page' }}">
			@foreach($products as $page => $products_info)
				<div class="{{($num == 1 || $num == 6)? '' : 'blocknone' }} menu{{$num}} pages">
					<div class="description-container">
						<span class="size-10rem">{{$description[$page]}}</span>
					</div>
					@foreach($products_info as $product)
						<div class="product-container block wear_list">
							<ul>
								<li class="clearfix">  
									<div class="closer col-md-5 col-sm-5 col-xs-12">
										<div class="photos box">
											<ul class="clearfix">
												<li class=" main-photo">
													<div class="aniimated-thumbnials clearfix">
														@foreach($product->main_images as $key => $image)
															@if($key == 0)
																<a href="{{$image}}" class="select {{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo row text-center">
															@else
																<a href="{{$image}}" class="{{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo row text-center">
															@endif
																<img src="{{$image}}" title="2017RainGear-{{$product->name}}">
															</a>
														@endforeach
														@foreach($product->other_images as $other_image)
															<a href="{{$other_image}}" class="col-md-4 col-sm-4 col-xs-12 small-select small-photo box text-center">
																<img src="{{$other_image}}" title="2017RainGear-{{$product->name}}">
																<span class="helper"></span>
															</a>
														@endforeach
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class=" information col-md-7 col-sm-7 col-xs-12">
										<div class="name box">
											<a href="{{$product->link}}" target="_blank">
												<span class="size-10rem">{{$product->full_name}}</span>
											</a>
										</div>
										<div class="price box">
											<span class="font-color-red size-10rem">販賣價格: NT$ {{round($product->prices[0]->price)}}</span>
										</div>
										@if($product->colors)
											<div class="color box">
												<ul class="clearfix">
													@foreach($product->colors as $color)
														<li>
															<div style="background-color: {{$color}}"></div>
														</li>
													@endforeach
												</ul>
											</div>
										@endif
										@if($product->getGroupSelectsAndOptions())
											<div class="size box">
												<ul class="clearfix">
													@php 
														$size_groups = $product->getGroupSelectsAndOptions();
													@endphp
													@foreach($size_groups as $size_group)
														@foreach($size_group->options as $size)
															@php 
																$str_size = str_replace('尺寸：','',$size->name);
															@endphp
															@if($str_size == 'S')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/s.png')}}" title="2017RainGear-{{$product->name.'-尺寸 S'}}">
																</li>
															@elseif($str_size == 'M')														
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/m.png')}}" title="2017RainGear-{{$product->name.'-尺寸 M'}}">
																</li>
															@elseif($str_size == 'L')														
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/L.png')}}" title="2017RainGear-{{$product->name.'-尺寸 L'}}">
																</li>
															@elseif($str_size == 'XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/XL.png')}}" title="2017RainGear-{{$product->name.'-尺寸 XL'}}">
																</li>
															@elseif($str_size == '2XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/2XL.png')}}" title="2017RainGear-{{$product->name.'-尺寸 2XL'}}">
																</li>
															@elseif($str_size == '3XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/3XL.png')}}" title="2017RainGear-{{$product->name.'-尺寸 3XL'}}">
																</li>
															@elseif($str_size == '4XL')
																<li>
																	<img src="{{assetRemote('image/collection/rainGear/2017/4XL.png')}}" title="2017RainGear-{{$product->name.'-尺寸 4XL'}}">
																</li>
															@endif
														@endforeach
													@endforeach
												</ul>
											</div>
										@endif
										<div class="photos box closer closer-media col-md-5 col-sm-5 col-xs-12">
											<ul class="clearfix">
												<li class=" main-photo">
													<div class="aniimated-thumbnials clearfix">
														@foreach($product->main_images as $key => $image)
															@if($key == 0)
																<a href="{{$image}}" class="select {{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo text-center">
															@else
																<a href="{{$image}}" class="{{count($product->other_images) == 0 ? 'col-md-12 col-sm-12' : 'col-md-8 col-sm-8'}} col-xs-12 photo text-center">
															@endif
																<img src="{{$image}}" title="2017RainGear-{{$product->name}}">
															</a>
														@endforeach
														@foreach($product->other_images as $other_image)
															<a href="{{$other_image}}" class="col-md-4 col-sm-4 col-xs-12 small-select small-photo box text-center">
																<img src="{{$other_image}}" title="2017RainGear-{{$product->name}}"><span class="helper"></span>
															</a>
														@endforeach
													</div>
												</li>
											</ul>
										</div>
										<div class="text box">
											@if($product->description)
												<span>
													@if($product->description != strip_tags($product->description))
														{!! $product->description !!}
													@else
														{!! nl2br($product->description) !!}
													@endif
												</span>
											@else
												<span>
													@php
														$product->noTranslateButton = true;
														$description_html = view('response.pages.product-detail.partials.info-detail-description', ['product' => $product])->render();
														echo strip_tags($description_html, '<br><br/><br />');
													@endphp
												</span>
											@endif
										</div>

										<div class="block">
											<a href="{{$product->link}}" class="button" target="_blank"></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					@endforeach
				</div>
				@php
				 $num++;
				@endphp
			@endforeach
			<div class="text-center width-full block">
				<a class="other-link btn btn-default" href="{{\URL::route('collection-type-detail',['type' => 'category','url_rewrite' => 'RainGoods2017'])}}">雨天用品特輯頁面 »</a>
			</div>
		</div>
	@endforeach


	<script src="{!! assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe-ui-default.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/elevatezoom-master/jquery.elevateZoom-3.0.8.min.js') !!}"></script>


{{--     <script src="{!! assetRemote('plugin/lightslider/src/js/lightslider.js') !!}"></script> --}}
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lightgallery.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lg-zoom.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lg-thumbnail.min.js') !!}"></script>
    <script src="{!! assetRemote('plugin/lightGallery/src/js/lg-thumbnail.min.js?time=') !!}"></script>

	<script>
		$('.aniimated-thumbnials').lightGallery({
		    thumbnail:true
		});
	</script>
	<script>
		$('.import-container .media li').click(function(){
			$('.import-container ul li a').removeClass('active');
			$(this).children('a').addClass('active');
		});
		function slipTo1(element){
			if (window.matchMedia('(max-width: 767px)').matches) {
		        var y = parseInt($(element).offset().top) - 80;
		        $('html,body').animate({scrollTop: y}, 400);
		    }
	    }
		$('.page-selector').click(function(){
			$(this).addClass('active');
			$(this).parent().siblings().children('.page-selector').removeClass('active');
			console.log($(this));
			var pageType = $(this).attr('data-page-type');
			$('.' + pageType + ' .lowprice').addClass('active');
			$('.menu6').removeClass('blocknone');
			$('.type-pages').hide();
			$('.type-pages').find('a').removeClass('active');
			$('.type-pages').find('li:first-child').find('a').addClass('active');
			$('.type-page-links').hide();
			$('.type-page-links').find('a').removeClass('active');
			$('.type-page-links').find('li:first-child').find('a').addClass('active');
			$('.' + pageType + '-page').show();
			$('.' + pageType + '-links').show();
			$('.import').hide();
			console.log(pageType);
		});
		if (window.matchMedia('(max-width: 767px)').matches) {
        	$(".wear_list ul li .information .color ul li").click(function(){
				var num = $(this).index();
				$(this).closest('.information').find(".closer").find('.select').removeClass("select");
				$(this).closest('.information').find(".closer").find('.aniimated-thumbnials').children().eq(num).addClass("select");
			});	
	    } else {
	        $(".wear_list ul li .information .color ul li").hover(function(){
				var num = $(this).index();
				$(this).closest('.information').siblings(".closer").find('.select').removeClass("select");
				$(this).closest('.information').siblings(".closer").find('.aniimated-thumbnials').children().eq(num).addClass("select");
			});	
	    }
		

		$(".link-container li").click(function(){
			$('.link-container li a').removeClass('active');
			$(this).children('a').addClass('active');
			var id = $(this).attr('id');
			$('.pages').each(function(){
				if(!$(this).hasClass('blocknone')){
					$(this).addClass('blocknone');
				}
			});
			var name = "." + id;
			$(name).removeClass('blocknone');
		});
	</script>
