<style>
    .search-container {
        padding:10px;
        background-color:#FDE841;
    }
    .search-container .description{
        padding:10px;
        background-color: #FDE841;
        border: 3px #CCC solid;
        padding: 10px;
        border-radius: 5px;
        background-color: #fff;
        color:#000;
    }
    .search-container .search .search-text input{
        padding:8px;
    }
    .search-container .search .search-title {
        line-height:40px;
    }
    .search-container h1 {
        font-size:1.2rem !important;
    }
    .search-container .search .search-button a{
        width:200px;
    }
    .data-container table .battery {
        height:188px !important;
        overflow: auto;
    }
    .data-container table .battery li{
        line-height:24px;
    }
    .data-container table .model {
        height:188px !important;
    }
    .data-container table .model img{
        max-height:161px;
    }
    .data-container table th,.data-container table td{
        border: 3px #000 solid !important;
    }
    .data-container table th{
        background: #ececec;
    }
    .data-container table .product-link {
        display: inline-block;
        vertical-align: middle;
    }
    .link-container .link-opacity{
        opacity: 0.7;
    }
    .link-container .link-opacity:hover{
        opacity: 1;
    }
</style>
<div class="search-container block content-last-box">
    <h1 class="box font-bold link1 size-10rem AGM">【MOTOBATT 電池規格】AGM 閥控式強效電池</h1>
    <h1 class="box font-bold link2 size-10rem hidden GEL">【MOTOBATT 電池規格】GEL 膠體電池</h1>
    <h1 class="box font-bold link3 size-10rem hidden PRO">【MOTOBATT 電池規格】PRO Lithium 賽事級鋰鐵電池</h1>
    <div class="description box">
        <span class="link1 size-10rem">以優異的AGM製造技術，將更少量的硫酸電解液包覆在特殊玻璃纖維隔膜中，因此產品不僅更加環保，也同時解決了所有的機車震動傾倒問題，產品出廠即完全密封且電力飽滿，使用無需任何保養，且電池放置方式也更有彈性，是真正專為重型機車所量身訂製的專用優質電池。</span>
        <span class="link2 size-10rem hidden">針對日常交通使用的一般重型機車所研發，膠體電池(以下簡稱 GEL電池)最早是由德國政府研究開發，主要是作為軍事用途，因為軍用車輛(如 坦克車、吉普車等) 在戰場上不能因為電池中彈後外殼破裂，電池液洩漏導致車輛無法行動而被殲滅，所以德國軍方開發了以電解凝膠為原料的GEL膠體電池，即使電池外殼中彈破裂也不會失去功能，時至今日凝膠電解質材料的進步與普及已經可以做為商業用途，主要提供給高安全性要求的環境使用，如 飛機、大型電腦機房等等，而GEL電池的 高耐熱、使用壽命超長、高度安全、充放電快速、低自放電率的特性也特別適合機車使用。</span>
        <span class="link3 size-10rem hidden">專為高性能街跑車所開發設計，現行的跑車多為四缸高轉速引擎，試想當每分鐘轉速高達上萬轉時(RPM)，四顆火星塞需要多大的電流來擊發電弧，目前您車上所使用的一般鉛酸電池，電力供給並不足以讓油氣100%完全爆炸燃燒，所以會有馬力輸出不夠線性與油氣燃燒不完全的問題，當一顆擁有強大放電能力的電池，能瞬間提供足夠的電力讓每一顆火星塞擊發出完美的電弧時，您才能感受瞬間拉轉的強大動力，還有那拳拳到肉的排氣聲浪！MOTOBATT歷時兩年研發出此系列 PRO Lithium鋰鐵電池，除了在原料上嚴格選用AAA級鋰電芯，還開發出了高效耐熱的控制電路板，有效保護與平衡電池的輸出入電流，讓這顆電池比市場其他品牌足足提升了150%的電力容量表現，在充電的效率上更是驚人的在騎乘20~30分鐘後即能完全充飽電池電力，再加上絕對輕量化的設計以及MOTOBATT全球專利的四端子功能，從內到外十足為賽事而生，是您追求高性能的不二首選。</span>
    </div>
    <div class="search clearfix">
        <div class="col-md-2 col-sm-2 col-xs-12 text-center search-title">
            <span class="size-10rem font-bold">原廠電池型號搜尋</span>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12 search-text box">
            <input class="width-full search-input" type="text" placeholder="請搜尋原廠電池型號...">
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 search-button text-center">
            <a class="btn btn-info button">搜尋</a>
        </div>
    </div>
</div>
<div class="data-container table-responsive">
    <table class="width-full table">
        <thead>
        <tr class="text-center">
            <th class="text-center">電池型號</th>
            <th class="text-center">原廠電池型號</th>
            <th class="text-center">電池規格說明</th>
            <th class="text-center">商品連結</th>
        </tr>
        </thead>
        @php
            $num = 1;
            $title = ['AGM' => '【MOTOBATT】閥控式強效級機車啟動電池-','GEL' => '【MOTOBATT】GEL 膠體密封長效型機車啟動電池-','PRO' => '【MOTOBATT】鋰鐵強效賽事級機車啟動電池-' ];
        @endphp
        @foreach($data as $link => $values)
            <tbody class="link{{$num}} {{ $num == 1 ? '' : 'hidden' }}">
                @foreach($values as $sku => $value)
                    <tr>
                        <td class="text-center">
                            <div class="model">
                                <h2 class="size-10rem box">{{ $value->model }}</h2>
                                <a href="{{ $value->link ? $value->link : \URL::route('product-detail',$sku) }}" title="{{ $title[$link].$value->model.' - 「Webike-摩托百貨」' }}" target="_blank">
                                    <img src="{{ $value->image }}" alt="{{ $title[$link].$value->model.' - 「Webike-摩托百貨」' }}">
                                </a>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="battery">
                                @if(count($value))
                                    <ul>
                                        @foreach($value->batteries as $battery)
                                            <li class="{{ $battery }}">
                                                <span class="battnum">{{ $battery }}<br></span>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">{!! str_replace('\n',"<br>",$value->regular) !!}</td>
                        <td class="text-center">
                            <div class="battery">
                                <div class="product-link">
                                    <a class="btn btn-warning" href="{{ $value->link ? $value->link : \URL::route('product-detail',$sku) }}" title="{{ $title[$link].$value->model.' - 「Webike-摩托百貨」' }}" target="_blank">商品頁面</a>
                                </div>
                                <span class="helper"></span>
                            </div>
                        </td>
                    </tr>
                @endforeach
                @php
                    $num++;
                @endphp
            </tbody>
        @endforeach
    </table>
</div>
<script>
    $('.link-container').children().click(function () {
        $('.link-container')
        $('.data-container table tr').removeClass('hidden');
        $('.data-container table tr li span.battnum').removeClass('font-color-red');
        $('.link-container div a').removeClass('active');
        $(this).children().addClass('active');

        $('.data-container table tbody').addClass('hidden');
        $('.search-container .description span').addClass('hidden');
        $('.search-container h1').addClass('hidden');

        if($(this).children().hasClass('link1')){
            $('.link1').removeClass('hidden');
        }else if($(this).children().hasClass('link2')){
            $('.link2').removeClass('hidden');
        }else{
            $('.link3').removeClass('hidden');
        }

        $('.link-container').children().each(function () {
            if(!$(this).children().hasClass('active')){
                $(this).children().addClass('link-opacity');
            }else{
                $(this).children().removeClass('link-opacity');
            }
        });
    });

    $('.search-button .button').click(function () {
        var value = $('.search-input').val().toUpperCase();
        if( value == null || value.length <= 0 ){
            alert('請輸入搜尋關鍵字');
        }else{
            $('.battery').closest('tr').addClass('hidden'); 
            $('.battery ul li span.battnum').each(function () {
                $(this).removeClass('font-color-red');
                var battnum = $(this).text();
                if(battnum.indexOf(value) != -1){                    
                    $(this).closest('tr').removeClass('hidden');
                    $(this).addClass('font-color-red');
                }else{
                    $(this).closest('tr').addClass('hidden');
                }

                if($(this).closest('ul').find('.battnum').hasClass("font-color-red")){
                    $(this).closest('tr').removeClass('hidden');
                }
            });


        }

    });
</script>