<div>
    <ul class="ul-left-title">
        <li class="box-page-group">
            <div class="title-box-page-group">
                <h3>流行特輯</h3>
            </div>
            @if(isset($current_type))
                <div class="center-box">
                    <a href="{{$base_url}}" class="border-radius-2 btn base-btn-gray btn-full">回流行特輯</a>
                </div>
            @endif
            @foreach($types as $type)
                <div class="center-box">
                    <a class="a-sub3-l" href="{!! $type->link ? $type->link : URL::route('collection-type', $type->url_rewrite) !!}">
                        <figure class="zoom-image thumb-img">
                            <img src="{{$type->image}}" alt="{{$type->name . $tail}}">
                        </figure>
                    </a>
                </div>
            @endforeach
        </li>
    </ul>
</div>
