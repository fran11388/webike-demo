@extends('response.layouts.1column')
@section('style')
<style>
	.breadcrumb-mobile {
		background-color: #1c2029;
	}
	.breadcrumb-mobile .breadcrumb-product-detail {
		max-width: 1200px;
		margin-right:auto;
		margin-left:auto;
	}
	.breadcrumb-mobile .breadcrumb-product-detail .breadcrumb-item span{
		color:white !important;
	}
	.breadcrumb-mobile .breadcrumb-product-detail .breadcrumb-item span:hover {
		color:#e61e25 !important;
	}
	.breadcrumb-mobile .breadcrumb-product-detail .active span:hover {
		color:white !important;
	}
	.contents-all-page .container {
		max-width:100% !important;
		padding:0px;
	}
	.breadcrumb-mobile .breadcrumb-product-detail {
		padding:8px 15px 8px 0px;
	}
	.all-content{
		background-color: #1c2029;
	}
	.suzuka-container{
		max-width: 1200px;
		margin-right:auto;
		margin-left:auto;
		background-color: white;
	}
	.link-container {
		background-color: #e5e5e5;
		padding-bottom:20px;
		padding-top:20px;
	}
	.link-container ul li div{
    	min-height:43.67px;
		background-size: 100% 100% !important;
		margin-left:auto;
		margin-right:auto;
	}
	@media(max-width:767px){
		.link-container ul li {
			float:none;
			margin-right:auto;
			margin-left:auto;
			margin-bottom:10px;
		}
		.breadcrumb-mobile {
			padding-left: 10px;
			margin-right:0px;
		}
	}
	.link-container .about{
		background: url({{assetRemote('image/suzuka/2017/about.png')}}) no-repeat;
	}
	.link-container .active{
		background:url({{assetRemote('image/suzuka/2017/active.png')}}) no-repeat;
	}
	.link-container .ticket{
		background:url({{assetRemote('image/suzuka/2017/ticket.png')}}) no-repeat;
	}
	.link-container .free{
		background:url({{assetRemote('image/suzuka/2017/free.png')}}) no-repeat;
	}
	.link-container .curcuit{
		background:url({{assetRemote('image/suzuka/2017/curcuit.png')}}) no-repeat;
	}
	.link-container .about:hover{
		background: url({{assetRemote('image/suzuka/2017/about_hover.png')}}) no-repeat;	
	}
	.link-container .active:hover{
		background:url({{assetRemote('image/suzuka/2017/active_hover.png')}}) no-repeat;	
	}
	.link-container .ticket:hover{
		background:url({{assetRemote('image/suzuka/2017/ticket_hover.png')}}) no-repeat;	
	}
	.link-container .free:hover{
		background:url({{assetRemote('image/suzuka/2017/free_hover.png')}}) no-repeat;	
	}
	.link-container .curcuit:hover{
		background:url({{assetRemote('image/suzuka/2017/curcuit_hover.png')}}) no-repeat;	
	}
	.line-event {
		background:url(https://img.webike.net/jp_statics/gfs/nossl_css/images/suzuka-eight-hours/bg-line.jpg) top center;
		margin: 10px 0;
		height: 16px;
	}
	.suzuka-padding {
		padding:0px 20px !important;
	}
	.suzuka-padding-text {
		padding:10px;
	}
	.ct-title-news {
		background:url(https://img.webike.net/jp_statics/gfs/nossl_css/images/suzuka-eight-hours/bg-titile.jpg) top center no-repeat;
		height:55px;
		background-size: contain;
	}
	.ct-title-news h2 {
		padding-top: 9px;
		display: inline-block;
    	background-color: white;
	}
	.suzuka-container li {
		list-style-type: none;
	}
	.suzuka-container .title {
		padding: 5px 0;
	}
	.border-bottom-red {
		border-bottom: 3px solid #e61e25;
	}
	.suzuka-container .detail-img {
		float:right;
		margin-left:10px;
	}
	.suzuka-container .introduce-left {
		width:49%;
		float:left;
		margin-right:2%;
	}
	.suzuka-container .introduce-right {
		width:49%;
		float:left;
	}
	.detail-container .detail-img li {
		margin-bottom:10px;
	} 
	.gallery-contain img {
		position:relative;
		height: 281.66px;
	}
	.gallery-contain .description {
		overflow:auto;
	}
	.gallery-style2 {
		padding-left:5px;
		padding-right:5px;
	}
	.gallery-container-style2 .row{
		margin-right:-5px;
		margin-left:-5px;
	}

	.detail-container .introduce{
		background-color: #ececec;
	}
	.detail-container .left {
		height:260px;
	}
	.detail-container .introduce-left .detail-img,.introduce-right .detail-img {
		padding-left:0px;
	} 
	
	.ticket-container .ticket {
		background-color: #eaeaea;
		margin-right:auto;
		margin-left:auto;
		float:right;
		margin-top:9%;
	}
	.ticket-container .ticket a:hover {
		opacity: 0.7;
	}
	@media (max-width: 500px) {
		.suzuka-container .introduce-left {
			width:100% !important;
			margin-right:0px;
			margin-bottom:10px;
			float:none;
		}
		.suzuka-container .introduce-right {
			width:100% !important;
			float:none;
		}
		.detail-container .left {
			height:auto;
		}
		.suzuka-container .detail-img {
			float:none;
			margin-left:0px;
		}
	}
	.suzuka-container .visit {
		padding-bottom:23px !important;
	}
	.suzuka-container .visit a:hover {
		opacity: 0.7;
	}
	.suzuka-container .offer {
		margin-top:62px;
	}
	#curcuit {
		margin-bottom:40px !important;
	}
</style>
@stop
@section('middle')
	<div class="all-content">
		<div class="suzuka-container">
			<div class="banner-container">
				<img src="{{assetRemote('image\suzuka\2018\suzukahead370.jpg')}}" alt="2018鈴鹿8耐" title="2018鈴鹿8耐">
			</div>
			<div class="link-container clearfix">
				<div class="col-md-1 col-sm-1 visible-md visible-lg visible-sm"></div>
				<ul class="ul-menu-event col-md-10 col-sm-10 col-xs-12 clearfix">                         		
					<li class="text-center col-lg-3 col-md-3 col-sm-3 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#about')" title="2018鈴鹿8耐-關於8耐">
							<div class="about"></div>
						</a>                         
					</li>                         
					<li class="text-center col-lg-3 col-md-3 col-sm-3 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#active')" title="2018鈴鹿8耐-周邊活動">
							<div class="active" title="2018鈴鹿8耐-周邊活動"></div>
						</a>                         
					</li>                                            
					<li class="text-center col-lg-3 col-md-3 col-sm-3 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#free')" title="2018鈴鹿8耐-Nic Wang自由行遊記">
							<div class="free" title="2018鈴鹿8耐-Nic Wang自由行遊記"></div>
						</a>                         
					</li>
					<li class="text-center col-lg-3 col-md-3 col-sm-3 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#curcuit')" title="2018鈴鹿8耐-鈴鹿賽道">
							<div class="curcuit" title="2018鈴鹿8耐-鈴鹿賽道"></div>
						</a>                         
					</li>                      
				</ul>
				<div class="col-md-1 col-sm-1 visible-md visible-lg visible-sm"></div>
			</div>
			<div class="news-container width-full content-last-block block suzuka-padding">
				<div class="news-banner width-full text-center">
					<img src="{{assetRemote('image\suzuka\2017\img-thelatest.png')}}" alt="2018鈴鹿8耐-最新賽事新聞" title="2018鈴鹿8耐-最新賽事新聞">        
					<div class="line-event width-full"></div>
				</div>
				<div class="title-news width-full">        
					<div class="ct-title-news width-full text-center">            
						<h2 class="font-bold">Webike moto news</h2>        
					</div>    
				</div>
				@include('response.pages.collection.suzuka8hours.news')
			</div>
			
			<div class="detail-container suzuka-padding clearfix block" id="about">
				<h2 class="title font-bold box border-bottom-red">關於 鈴鹿8耐</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-01.jpg')}}" alt="2018鈴鹿8耐-介紹" title="2018鈴鹿8耐-介紹">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-02.jpg')}}" alt="2018鈴鹿8耐-介紹" title="2018鈴鹿8耐-介紹">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-03.jpg')}}" alt="2018鈴鹿8耐-介紹" title="2018鈴鹿8耐-介紹">
							</figure>
						</li>
					</ul>
				</div>
				<div class="text">
					<h3 class="block font-bold">41年經典賽事</h3>
					<span class="size-10rem">2018 41屆鈴鹿8耐 為本次世界耐力錦標賽WEC最終戰，今年依舊引起世界各地賽車迷的注意。<br><br>第一屆鈴鹿8耐在1978年舉行，這是日本在全球舞台上展露頭角的一年，例如沖繩島交通法規的變化、成田國際機場正式啟用。這41年來日本創締造許多為人樂道的歷史事蹟。<br><br>鈴鹿8耐第一屆獲勝車隊是吉村YOSHIMURA。之後的40年，他們從未缺席任何一場鈴鹿8耐。奠定了吉村與鈴鹿8耐歷史上密不可分的關係，各家車廠間的競爭隨之展開。鈴鹿8耐現在是HONDA，YAMAHA，SUZUKI，KAWASAKI等日本4大廠的必爭之地。贏得鈴鹿8耐的車廠，除了贏得榮耀，最重要的是贏得車迷對車廠的支持與信任。<br><br>Wayne Gardner、Wes Cooley、Mike Baldwin、Graeme Crosby、David Aldana、HervéMoineau等是廣為人知鈴鹿8耐的重要歷史人物。其中Wayne Gardner不得不另外介紹，Mr. Gardner多次在鈴鹿8耐獲勝，並在1987年贏得WGP500世界冠軍。鈴鹿8耐的聲望與知名度也隨著Mr. Gardner水漲船高。<br><br>隨著Wayne Gardner的崛起，許多世界摩托車錦標賽的車手開始參加鈴鹿8耐。如Kenny Roberts、Tadahiko Taira、Eddie Lawson、Wayne Rainey、Kevin Schwantz、Mick Doohan和Valentino Rossi…等等，這些車手的加入，往往成為一整年車迷們茶餘飯後的焦點話題。<br><br></span>
					<h3 class="block font-bold">鈴鹿8耐 讓鈴鹿賽車場進入全新的領域</h3>
					<span class="size-10rem">世界頂級車手將鈴鹿8耐轉變為“衝刺耐力賽”，贏得世界耐力錦標賽的關鍵是穩定駕駛與速度，這是耐久賽不變的道理，但為了贏得鈴鹿8耐，車手們不僅僅要有穩定的油門，而是以最接近極限的方式”穩定”騎乘。<br><br>世界頂尖車手從以前到現在總能創造出許多驚人的紀錄。 2015年獲勝者YAMAHA FACTORY RACING TEAM在前年延攬了MotoGP車手Pol Espargaró、Bradley Smith與日本首席車手 中須賀克行 加入，創造一個夢幻的團隊。且MotoGP車手Pol Espargaró騎乘YZF-R1廠車在排位賽跑出相當驚人2：06.258單圈成績。<br><br>MuSashi RT HARC-PRO。（HONDA）2016年找了前MotoGP冠軍，Casey Stoner 與已故車手 Nicky Hayden。Yoshimura Suzuki Shell Advance (SUZUKI)和團隊KAGAYAMA（SUZUKI）帶回了老將Kevin Schwantz。Leon Haslam則是加入Team GREEN (Kawasaki)，並獲得第二名，是場上唯一能對YAMAHA場對造成威脅的車隊。<br><br>今年2018 41屆“鈴鹿8耐”是日本賽車史上重要的里程碑，也是各車廠、車手們競爭的最佳舞台。</span>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block">
				<h2 class="title font-bold box">第41屆 鈴鹿8耐 觀賽重點</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-04.jpg')}}" alt="2018鈴鹿8耐-觀賽重點" title="2018鈴鹿8耐-觀賽重點">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-05.jpg')}}" alt="2018鈴鹿8耐-觀賽重點" title="2018鈴鹿8耐-觀賽重點">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-06.jpg')}}" alt="2018鈴鹿8耐-觀賽重點" title="2018鈴鹿8耐-觀賽重點">
							</figure>
						</li>
					</ul>
				</div>
				<div class="text">
					<h3 class="font-bold">最強冠軍候補就是YAMAHA FACTORY RACING TEAM</h3><br>
					<span class="size-10rem">
						終於，在本週末就要迎接被暱稱為「8耐」的鈴鹿8小時耐力賽的到來，雖然從昨天(周四)就已經開始進行本屆賽程，但各車隊想必直到最後一刻為止都還在忙著最終準備及調整吧！<br><br>
						今年最熱門的冠軍候補果然就是將目標放在4連霸的YAMAHA FACTORY RACING TEAM了吧！以目前聲勢高漲的中須賀克行為主，加上在世界超級摩托車錦標賽(WSBK）狀況絕佳的Alex Lowes以及Michael van der Mark等，YAMAHA FACTORY RACING TEAM可說是集結了最頂尖車手的一流隊伍。<br><br>
						中須賀克行在之前接受採訪時就非常有自信的表示「我們現在的狀況極佳，因此不管發生什麼狀況，只要有我們3個人在就不會有問題！」在比賽前早早地展露出作為衛冕冠軍的從容姿態。<br><br>
					</span>
				</div>
				<div class="text">
					<h3 class="font-bold">WSBK王者也加入了KAWASAKI，優勝目標近在咫尺</h3><br>
					<span class="size-10rem">
						能和衛冕冠軍YAMAHA對抗的應該就非Kawasaki Team GREEN莫屬了，Kawasaki Team GREEN在2016年與2017年都獲得第2名，今年更是徵召了在WSBK奪下3連霸、正朝向4連霸之路邁進的Jonathan Rea（以下以Johnny表示）。<br><br>
						剛結束WSBK賽事的Johnny馬不停蹄的在7月10日～12日參加公開測試，原本他是預定要從第二次才開始參加練習，但是在他抵達賽道一個小時後舉行的第一次練習中便已經可以看到他的身影，而且還馬上跑出絕佳成績，甚至在第3天練習時還跑進2秒06秒，順勢寫下車隊的最佳成績，讓人感覺到他對這次贏得冠軍志在必得的樣子。<br><br>
						雖然多少有受到時差影響，但身為世界冠軍的Johnny努力調整時差，默默地表現出專業車手該有的素質。而令人在意的是在公開測試中，Johnny對參賽車的設定做了變動，因此Leon Haslam及渡邊一馬在比賽中平均會騎乘多久的時間，應該都會影響到比賽戰略；究竟KAWASAKI是否能在暌違25年後再度拿下冠軍王座呢？<br><br>
					</span>
				</div>
				<div class="text">
					<h3 class="font-bold">因為Leon Camier受傷，HRC陣前換將</h3><br>
					<span class="size-10rem">
						睽違10年再度重返鈴鹿8耐的HRC，以「Red Bull Honda with日本郵局」的隊名，派出高橋巧、中上貴晶，再加上代替傷兵的Patrick Jacobsen代表參賽；身為全日本JSB1000級別的王牌選手，並參與參賽車研發的高橋巧完全不在意周圍吵鬧的聲音，全心全意地投入在自己的工作上。<br><br>
						而身為MotoGP現役車手的中上貴晶也表示將在今年一雪去年摔車的恥辱，目標就是拿下鈴鹿8耐的冠軍!中上貴晶還提到他去年是在Moto2級別征戰，今年則是提升到GP級別，所以在一開始騎乘鈴鹿8耐參賽車時感覺速度特別慢，還以為是參賽車壞掉；從這件事中可以看出MotoGP的參賽車絕對是集頂尖技術與性能於一身的特別車款。<br><br>
						雖然因為Leon Camier受傷所以緊急徵召Patrick Jacobsen加入，但是在事前測試中，Patrick Jacobsen就成功跑出HARC-PRO車隊的最佳成績，因此在正賽時應該會按照他對CBR1000RRW的駕馭程度決定起用方式。<br><br>
					</span>
				</div>
				<div class="text">
					<h3 class="font-bold">Kevin Schwantz出任Team KAGAYAMA</h3><br>
					<span class="size-10rem">
						YOSHIMURA SUZUKI MOTUL RACING派出了津田拓也與Sylvain Guintoli選手，第3位選手雖然在BSB的Bradley Ray及全日本JSB1000級別的渡邊一樹之間猶豫，但最後還是確定由Bradley Ray代表參賽。<br><br>
						此外，渡邊一樹則是和車隊代表生形秀之以及Tommy Bridewell一起組成S-pulse Dream Racing IAI參加比賽；在測試時渡邊一樹同時試乘了兩支隊伍的參賽車，如果正式比賽時也能展現出驚人速度的話，應該就可以拿下不錯的成績。<br><br>
						Team KAGAYAMA U.S.A.除了找來Kevin Schwantz擔任總教練外，車手部分則邀請到了Joe Roberts、浦本修充與加賀山就臣等3人，目標當然就是要登上頒獎台。<br><br>
						KYB MORIWAKI MOTUL RACING的Dan Linfoot在21日的比賽中受傷，所以或許會和去年一樣，正賽將由高橋裕紀與清成龍一的2人組合挑戰也說不定；而由秋吉耕祐率領的au・TELURU MotoUP Racing，雖然因為長島哲太在MotoGP荷蘭站時受傷，再加上Isaac Vinales在正式比賽時只有雨中的參賽經驗等不確定因素，但是由於秋吉耕祐本身的表現極為穩定，因此萬一前幾名種子選手出狀況的話，應該也有希望能夠站上頒獎台。<br><br>
						Team Sup Dream Honda的山口辰也、岩戶亮介、作本輝介以及Honda Suzuka Racing Team的日浦大治朗、龜井雄大、安田毅史，則有機會競爭中段的排名成績。<br><br>
						競爭前幾名的大概就是YAMAHA FACTORY RACING TEAM、Kawasaki Team GREEN、Red Bull Honda with日本郵局以及MuSASHi RT HARC-PRO等這幾支車隊，而Honda感覺應該會從本屆開始奮起直追；此外，離世界耐力錦標賽冠軍只差一步之遙的F.C.C. TSR Honda France也值得密切關注。<br><br>
					</span>
				</div>
				<div class="text">
					<h3 class="font-bold">SST級別賽事也備受矚目</h3><br>
					<span class="size-10rem">
						除了EWC級別外，參賽車狀況接近一般市售車款的SST級別也非常值得一看。大家相當熟悉的青木宣篤將代表「MotoMap SUPPLY」騎乘SUZUKI的GSX-R1000參加SST級別。<br><br>
						在其他參賽選手方面，今野由寬、Josh Waters雖然是去年的老面孔，但是卻從EWC級別轉戰SST級別。由於SST級別不能像EWC級別一樣進行快拆輪胎的改裝，因此在維修區的作業時間就會變得比較長，然而去年贏得冠軍的ITO RACING卻能夠將輪胎更換次數控制在3次並留下209圈的驚人紀錄，所以才能以SST參賽車在綜合排名內拿下第15名，這可說是一項驚人壯舉。<br><br>
						由於今年ITO RACING放棄報名，因此MotoMap SUPPLY自然也就成為這次的冠軍候補車隊，與之抗衡的應該就是採用BMW S1000RR的TONE RT SYNCEDGE 4413、Team Hanshin Riding School以及ARMY･GIRL TEAM MF & Kawasaki等KAWASAKI陣營的車隊，還有騎乘YAMAHA YZF-R1的NCXX RACING &ZENKOUKAI吧！<br><br>
						由於今年的天氣相當炎熱，因此應該會對輪胎造成極大負擔，所以決勝關鍵可能就會變成待在維修區時間的長短，所以SST級別的冠軍爭奪戰也相當值得關注喔！<br><br>
					</span>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block">
				<h2 class="title font-bold box">鈴鹿4耐 未來之星比賽</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-07.jpg')}}" alt="鈴鹿4耐-未來之星比賽">
							</figure>
						</li>
					</ul>
				</div>
				<div class="text">
					<span class="size-10rem">鈴鹿4耐在鈴鹿8耐決賽前一天舉行。這將是日本與世界各地業餘與新人車手的一場競賽。在這場比賽中，兩名騎手在4小時內輪流騎乘一台賽車。比賽車輛規定是4行程600cc，ST組規則，並使用普利司通輪胎，在比賽中不能更換輪胎。這些規定使得一些人認為比鈴鹿8耐困難度更高。如果要贏得鈴鹿4耐，保護輪胎成為重要的關鍵。<br><br>亞洲賽車錦標賽(ARRC)在馬來西亞，泰國，印尼，印度和鈴鹿舉辦，亞洲車手近年來不斷的成長。去年鈴鹿4耐獲獎者是來自印尼的Irfan Ardiansyah和Rheza Danica。第三輪亞洲賽車錦標賽將於6月2日至4日在鈴鹿賽道舉行。許多年輕的日本和其他亞洲車手將參加這個比賽，被視為今年鈴鹿4耐的前哨站。</span>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block" id="active">
				<div class="width-full clearfix">
					<h2 class="title font-bold box border-bottom-red">周邊活動</h2>
					<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
						<ul class="clearfix row">
							<li class="col-md-12 col-sm-12 col-xs-12">
								<figure class="zoom-image">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-08.jpg')}}" alt="2018鈴鹿8耐-周邊活動" title="2018鈴鹿8耐-周邊活動">
								</figure>
							</li>
						</ul>
					</div>
					<div class="text">
						<span class="size-10rem">鈴鹿8耐的重頭戲之一正是夜間賽程的「Night Run」，把點亮暗夜的螢光棒發給V1、V2席的觀賽者，幫在8小時的嚴酷賽事中奮戰的選手們加油打氣。<br><br>夜間觀賽期間，持有螢光棒的觀賽者能夠自由變換螢光棒的顏色，為自己心儀的車隊加油，當接近賽事尾聲時，觀賽席將會出現變化，也將會為整個會場營造出炫幻般的景象。</span>

						{{-- <h3 class="block font-bold">Soaking wet & FMX performance, BIKE! LIVE! SPLASH!.</h3>
						<span>The World's top FMX performances will be combined with a cooling-of service. You can watch dynamic bike performances while being splashed with cool and refreshing water.</span> --}}
					</div>
				</div>
			
				<div class="gallery-container-style2 clearfix">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
							<div class="gallery-contain box">
								<div class="width-full text-center box">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-09.jpg')}}" alt="2018鈴鹿8耐-各車廠 最新車款展示與試乘" title="2018鈴鹿8耐-各車廠 最新車款展示與試乘">
								</div>
								<h3 class="box font-bold">各車廠 最新車款展示與試乘</h3>
								<div class="description width-full"><span class="size-10rem">從250cc到1000cc，日、歐、美各大車廠不只展示最新車輛並提供試乘活動給來自世界各地的車迷朋友。</span></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
							<div class="gallery-contain box">
								<div class="width-full text-center box">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-10.jpg')}}" alt="2018鈴鹿8耐-“騎士村莊”周邊商品販售" title="2018鈴鹿8耐-“騎士村莊”周邊商品販售">
								</div>
								<h3 class="box font-bold">“騎士村莊”周邊商品販售</h3>
								<div class="description width-full"><span class="size-10rem">不管是車上用的，身上穿的，”騎士村莊”裡都找的到，若沒有騎車的車迷也不用擔心，各品牌周邊精品也有販售。</span></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
							<div class="gallery-contain box">
								<div class="width-full text-center box">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-11.jpg')}}" alt="2018鈴鹿8耐-摩托車體驗專區" title="2018鈴鹿8耐-摩托車體驗專區">
								</div>
								<h3 class="box font-bold">摩托車體驗專區</h3>
								<div class="description width-full"><span class="size-10rem">還沒有駕照嗎?但又想體驗摩托車騎乘與換檔樂趣，那一定要來體驗專區，歡迎全家大小一同參加體驗。</span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Circuit-container suzuka-padding clearfix block" id="free">
				<h2 class="title font-bold box border-bottom-red">自由行遊記</h2>
				<div class="clearfix btn-gap-bottom">
					<div class="btn-gap-bottom">
						<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
							<ul class="clearfix row">
								<li class="col-md-12 col-sm-12 col-xs-12 nic">
									<figure class="zoom-image">
										<img src="{{assetRemote('image\suzuka\2018\mmm.jpg')}}" alt="2018鈴鹿8耐-Nic Wang自由行遊記" title="2018鈴鹿8耐-Nic Wang自由行遊記">
									</figure>
								</li>
							</ul>
						</div>
						<h3 class="block font-bold">馬克媽媽檔案</h3>
						<span class="size-10rem">馬克媽媽，來自台灣的人氣部落客。以產後快速塑身、回復姣好身材打開知名度，並活耀於部落格的人氣部落客。看不出年紀的她，其實已經是四個小孩的媽了！這次就讓馬克媽媽以親子的角度，帶大家暢遊鈴鹿賽車場、與體驗MOTOPIA的各項兒童遊樂設施!</span>
					</div>
					<div class="btn-gap-bottom travels">
						<h3 class="font-bold">2018 馬克媽媽 遊記</h3>
						<a href="https://www.suzukacircuit.jp/tw/featured/markmama/round01/" target="_blank" title="日本鈴鹿賽車場2018年全新版 - 小馬克的必玩推薦 No.1"><span class="size-10rem">日本鈴鹿賽車場2018年全新版 - 小馬克的必玩推薦 No.1</span></a><br>
						<a href="https://www.suzukacircuit.jp/tw/featured/markmama/round02/" target="_blank" title="日本鈴鹿賽車場2018年全新版 - 小馬克的必玩推薦 No.2"><span class="size-10rem">日本鈴鹿賽車場2018年全新版 - 小馬克的必玩推薦 No.2</span></a><br>
						<a href="https://www.suzukacircuit.jp/tw/featured/markmama/round03/" target="_blank" title="日本鈴鹿賽車場2018年全新版 - 小馬克的必玩推薦 No.3"><span class="size-10rem">日本鈴鹿賽車場2018年全新版 - 小馬克的必玩推薦 No.3</span></a><br>
						<a href="https://www.suzukacircuit.jp/tw/featured/markmama/round04/" target="_blank" title="日本鈴鹿賽車場2018年全新版 - 小馬克的私心推薦"><span class="size-10rem">日本鈴鹿賽車場2018年全新版 - 小馬克的私心推薦</span></a><br>
						<a href="https://www.suzukacircuit.jp/tw/featured/markmama/round05/" target="_blank" title="日本鈴鹿賽車場2018年全新版 - 賽車場的飯店 & 餐廳"><span class="size-10rem">日本鈴鹿賽車場2018年全新版 - 賽車場的飯店 & 餐廳</span></a><br>
						<a href="https://www.suzukacircuit.jp/tw/featured/markmama/round06/" target="_blank" title="日本鈴鹿賽車場2018年全新版 - 賽車場的交通 & 資訊"><span class="size-10rem">日本鈴鹿賽車場2018年全新版 - 賽車場的交通 & 資訊</span></a><br>
					</div>
				</div>
			</div>

			<div class="Circuit-container suzuka-padding clearfix block">
				<div class="clearfix btn-gap-bottom">
					<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
						<ul class="clearfix row">
							<li class="col-md-12 col-sm-12 col-xs-12 nic">
								<figure class="zoom-image">
									<img src="{{assetRemote('image\suzuka\2017\img_nic.png')}}" alt="2018鈴鹿8耐-Nic Wang自由行遊記" title="2018鈴鹿8耐-Nic Wang自由行遊記">
								</figure>
							</li>
						</ul>
					</div>
					<h3 class="block font-bold">Nic Wang檔案</h3>
					<span class="size-10rem">Nic是馬來西亞頂尖的重型機車部落客之一。在他接觸重型機車之前，其實早與汽車業有很深的淵源。他原本是街頭賽車手，後來加入Toyo Tires Drift Malaysia車隊。從賽車場退休後，他意外發掘對重型機車的熱情，一切就從他為了每日通勤而買了一輛重型機車開始。如今，Nic是最傑出的重型機車部落客之一。他的部落格是MinistryOfSuperbike.com，追蹤人數在短短一年半的時間內就從40人增加到45,000人。<br><br>讓Nic異軍突起的重點，在於他經營部落格的方式。MinistryOfSuperbike.com部落格與其說是技術取向，其實更聚焦在生活風格。Nic相信網路上已經有許多技術相關的網站了，然而重型機車的生活風格卻是小眾市場。<br><br>Nic對重型機車最初的熱情來自Kawasaki 150 RR，如今他的愛車是Kawasaki Ninja ZX-6R 636。與許多騎士不同的是，他並不喜歡和大型車隊一起上路，也不曾參與任何長途騎乘。他把對重型機車的熱情投注在提升技巧與速度上，因此他的週日例行作業就是到雲頂高原的山路練習騎乘技術。此外，他還會定期在雪邦國際賽道（Sepang International Circuit）的本地賽道體驗活動鍛鍊技術。如今，Nic的部落格引起許多知名廠商的注意，在本文報導的同時，MinistryOfSuperbike.com已和普利司通、Shark Helmets等許多大廠進行合作。</span>
				</div>
				
				<div class="travels">
					<h3 class="font-bold">2017 Nic Wang 遊記</h3>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round01/index.html" target="_blank" title="2018鈴鹿8耐-準備工作" nofollow><span class="size-10rem">ROUND1: Suzuka 8hours Adventure - Part 1（準備工作）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round02/index.html" target="_blank" title="2018鈴鹿8耐-買票囉" nofollow><span class="size-10rem">ROUND2: Suzuka 8hours Adventure - Part 2 （買票囉）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round03/index.html" target="_blank" title="2018鈴鹿8耐-行前記事" nofollow><span class="size-10rem">ROUND3: Suzuka 8hours Adventure - Part 3（行前記事）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round04/index.html" target="_blank" title="2018鈴鹿8耐-大阪一日遊" nofollow><span class="size-10rem">ROUND4: Suzuka 8hours Adventure - Part 4（大阪一日遊）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round05/index.html" target="_blank" title="2018鈴鹿8耐-從大阪到鈴鹿" nofollow><span class="size-10rem">ROUND5: Suzuka 8hours Adventure - Part 5（從大阪到鈴鹿）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round06/index.html" target="_blank" title="2018鈴鹿8耐-自由練習 & 預選" nofollow><span class="size-10rem">ROUND6: Suzuka 8hours Adventure - Part 6（自由練習 & 預選）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round07/index.html" target="_blank" title="2018鈴鹿8耐-Paddock & 展覽" nofollow><span class="size-10rem">ROUND7: Suzuka 8hours Adventure - Part 7（Paddock & 展覽）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round08/index.html" target="_blank" title="2018鈴鹿8耐-競速電影院 & Motopia" nofollow><span class="size-10rem">ROUND8: Suzuka 8hours Adventure - Part 8（競速電影院 & Motopia）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round09/index.html" target="_blank" title="2018鈴鹿8耐-比賽日" nofollow><span class="size-10rem">ROUND9: Suzuka 8hours Adventure - Part 9（比賽日）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round10/index.html" target="_blank" title="2018鈴鹿8耐-餐飲專區@鈴鹿" nofollow><span class="size-10rem">ROUND10: Suzuka 8hours Adventure - Part 10（餐飲專區@鈴鹿）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round11/index.html" target="_blank" title="2018鈴鹿8耐-道頓堀" nofollow><span class="size-10rem">ROUND11: Suzuka 8hours Adventure - Part 11（道頓堀）</span></a><br>
					<a href="https://www.suzukacircuit.jp/tw/featured/8hours_adventure/round12/index.html" target="_blank" title="2018鈴鹿8耐-耐久賽的難忘體驗" nofollow><span class="size-10rem">ROUND12: Suzuka 8hours Adventure - Final（耐久賽的難忘體驗）</span></a>
				</div>
			</div>
			<div class="Circuit-container suzuka-padding clearfix block bl" id="curcuit">
				<h2 class="title font-bold box border-bottom-red">鈴鹿賽道介紹</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-13.jpg')}}">
							</figure>
						</li>
					</ul>
				</div>
				<span class="size-10rem">鈴鹿賽車場設立於1962年，為日本最初的正規競速賽車場，開設後便一直是日本賽車運動的中心地。<br><br>從1987年起開辦的F1日本大獎賽，到舉辦超過30次的鈴鹿8小時耐久賽，每項賽事都超越了賽車運動的框架，成為定期大型活動。<br><br>鈴鹿賽車場在舉辦競賽的同時，也致力於振興賽車運動文化。以培育世界級賽車手、騎士為目的所設立的賽車學校，便是其中一環。<br><br>遊樂園MOTOPIA中設有多項年幼孩童也能自己操控的車輛，目的除了在於開拓「未來的粉絲層」外，同時也是基於讓汽車文化在日本扎根之願景。<br><br>此外，本賽車場以「款待」的精神與「機動性度假村」的構思為基礎，傾力於交通安全教育，並力圖完善飯店、餐廳設施，以長宿型休閒度假村的定位，提供優質環境與服務，積極開創更加符合時代潮流的新興休閒事業。<br><br>鈴鹿賽車場以競速為中心，提供充滿魅力的賽車運動、娛樂活動的舞台，吸引全球遊客造訪，從小孩到大人皆可盡歡。撼動人心的「感動」，就在這裡。</span>
				
			</div>
			<div class="clearfix visit">
				<div class="col-md-2 col-sm-2"></div>
				<ul class="ul-menu-event col-md-8 col-sm-8 col-xs-12 clearfix text-center">                         		
					<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                             
						<a href="http://www.suzukacircuit.jp/tw/" target="_blank" nofollow>                                 
							<img src="{{assetRemote('image\suzuka\2017\about-1.png')}}">
						</a>                         
					</li>                         
					<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                             
						<a href="http://www.suzukacircuit.jp/" target="_blank" nofollow>                                 
							<img src="{{assetRemote('image\suzuka\2017\about-2.png')}}">                 
						</a>                         
					</li>                         
					<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                             
						<a href="http://www.suzukacircuit.jp/en/" target="_blank" nofollow>                                 
							<img src="{{assetRemote('image\suzuka\2017\about-3.png')}}">                 
						</a>                         
					</li>                                            
				</ul>
				<div class="col-md-2 col-sm-2"></div>
				<br>
				<div class="clearfix width-full text-center offer">
					<span class="size-10rem col-md-12 col-sm-12 col-xs-12">◎「Webike」為本屆賽事官方授權之宣傳單位，宣傳素材由株式會社Mobilityland提供，請勿節錄或轉載。</span>
				</div>
			</div>
		</div>
	</div>
@stop
@section('script')
<script>
	function slipTo1(element){
        var y = parseInt($(element).offset().top) - 80;
        $('html,body').animate({scrollTop: y}, 400);
    }
    $('.link-shopping').removeClass('navbar-active');
</script>
@stop