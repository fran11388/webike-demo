@extends('response.layouts.1column')
@section('style')
<style>
	.breadcrumb-mobile {
		background-color: #1c2029;
	}
	.breadcrumb-mobile .breadcrumb-product-detail {
		max-width: 1200px;
		margin-right:auto;
		margin-left:auto;
	}
	.breadcrumb-mobile .breadcrumb-product-detail .breadcrumb-item span{
		color:white !important;
	}
	.breadcrumb-mobile .breadcrumb-product-detail .breadcrumb-item span:hover {
		color:#e61e25 !important;
	}
	.breadcrumb-mobile .breadcrumb-product-detail .active span:hover {
		color:white !important;
	}
	.contents-all-page .container {
		max-width:100% !important;
		padding:0px;
	}
	.breadcrumb-mobile .breadcrumb-product-detail {
		padding:8px 15px 8px 0px;
	}
	.all-content{
		background-color: #1c2029;
	}
	.suzuka-container{
		max-width: 1200px;
		margin-right:auto;
		margin-left:auto;
		background-color: white;
	}
	.link-container {
		background-color: #e5e5e5;
		padding-bottom:20px;
		padding-top:20px;
	}
	.link-container ul li div{
    	min-height:43.67px;
		background-size: 100% 100% !important;
		margin-left:auto;
		margin-right:auto;
	}
	@media(max-width:767px){
		.link-container ul li {
			float:none;
			margin-right:auto;
			margin-left:auto;
			margin-bottom:10px;
		}
		.breadcrumb-mobile {
			padding-left: 10px;
			margin-right:0px;
		}
	}
	.link-container .about{
		background: url({{assetRemote('image/suzuka/2017/about.png')}}) no-repeat;
	}
	.link-container .active{
		background:url({{assetRemote('image/suzuka/2017/active.png')}}) no-repeat;
	}
	.link-container .ticket{
		background:url({{assetRemote('image/suzuka/2017/ticket.png')}}) no-repeat;
	}
	.link-container .free{
		background:url({{assetRemote('image/suzuka/2017/free.png')}}) no-repeat;
	}
	.link-container .curcuit{
		background:url({{assetRemote('image/suzuka/2017/curcuit.png')}}) no-repeat;
	}
	.link-container .about:hover{
		background: url({{assetRemote('image/suzuka/2017/about_hover.png')}}) no-repeat;	
	}
	.link-container .active:hover{
		background:url({{assetRemote('image/suzuka/2017/active_hover.png')}}) no-repeat;	
	}
	.link-container .ticket:hover{
		background:url({{assetRemote('image/suzuka/2017/ticket_hover.png')}}) no-repeat;	
	}
	.link-container .free:hover{
		background:url({{assetRemote('image/suzuka/2017/free_hover.png')}}) no-repeat;	
	}
	.link-container .curcuit:hover{
		background:url({{assetRemote('image/suzuka/2017/curcuit_hover.png')}}) no-repeat;	
	}
	.line-event {
		background:url(https://img.webike.net/jp_statics/gfs/nossl_css/images/suzuka-eight-hours/bg-line.jpg) top center;
		margin: 10px 0;
		height: 16px;
	}
	.suzuka-padding {
		padding:0px 20px !important;
	}
	.suzuka-padding-text {
		padding:10px;
	}
	.ct-title-news {
		background:url(https://img.webike.net/jp_statics/gfs/nossl_css/images/suzuka-eight-hours/bg-titile.jpg) top center no-repeat;
		height:55px;
		background-size: contain;
	}
	.ct-title-news h2 {
		padding-top: 9px;
		display: inline-block;
    	background-color: white;
	}
	.suzuka-container li {
		list-style-type: none;
	}
	.suzuka-container .title {
		padding: 5px 0;
	}
	.border-bottom-red {
		border-bottom: 3px solid #e61e25;
	}
	.suzuka-container .detail-img {
		float:right;
		margin-left:10px;
	}
	.suzuka-container .introduce-left {
		width:49%;
		float:left;
		margin-right:2%;
	}
	.suzuka-container .introduce-right {
		width:49%;
		float:left;
	}
	.detail-container .detail-img li {
		margin-bottom:10px;
	} 
	.gallery-contain img {
		position:relative;
		height: 281.66px;
	}
	.gallery-contain .description {
		overflow:auto;
	}
	.gallery-style2 {
		padding-left:5px;
		padding-right:5px;
	}
	.gallery-container-style2 .row{
		margin-right:-5px;
		margin-left:-5px;
	}

	.detail-container .introduce{
		background-color: #ececec;
	}
	.detail-container .left {
		height:260px;
	}
	.detail-container .introduce-left .detail-img,.introduce-right .detail-img {
		padding-left:0px;
	} 
	
	.ticket-container .ticket {
		background-color: #eaeaea;
		margin-right:auto;
		margin-left:auto;
		float:right;
		margin-top:9%;
	}
	.ticket-container .ticket a:hover {
		opacity: 0.7;
	}
	@media (max-width: 500px) {
		.suzuka-container .introduce-left {
			width:100% !important;
			margin-right:0px;
			margin-bottom:10px;
			float:none;
		}
		.suzuka-container .introduce-right {
			width:100% !important;
			float:none;
		}
		.detail-container .left {
			height:auto;
		}
		.suzuka-container .detail-img {
			float:none;
			margin-left:0px;
		}
	}
	.suzuka-container .visit {
		padding-bottom:23px !important;
	}
	.suzuka-container .visit a:hover {
		opacity: 0.7;
	}
	.suzuka-container .offer {
		margin-top:62px;
	}
	#curcuit {
		margin-bottom:40px !important;
	}
</style>
@stop
@section('middle')
	<div class="all-content">
		<div class="suzuka-container">
			<div class="banner-container">
				<img src="{{assetRemote('image\suzuka\2017\banner.jpg')}}" alt="2017鈴鹿8耐" title="2017鈴鹿8耐">
			</div>
			<div class="link-container clearfix">
				<div class="col-md-1 col-sm-1 visible-md visible-lg visible-sm"></div>
				<ul class="ul-menu-event col-md-10 col-sm-10 col-xs-12 clearfix">                         		
					<li class="text-center col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#about')" title="2017鈴鹿8耐-關於8耐">
							<div class="about"></div>
						</a>                         
					</li>                         
					<li class="text-center col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#active')" title="2017鈴鹿8耐-周邊活動">
							<div class="active" title="2017鈴鹿8耐-周邊活動"></div>
						</a>                         
					</li>                         
					<li class="text-center col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#ticket')" title="2017鈴鹿8耐-門票販售">
							<div class="ticket" title="2017鈴鹿8耐-門票販售"></div>
						</a>                         
					</li>                         
					<li class="text-center col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#free')" title="2017鈴鹿8耐-Nic Wang自由行遊記">
							<div class="free" title="2017鈴鹿8耐-Nic Wang自由行遊記"></div>
						</a>                         
					</li>
					<li class="text-center col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-7">                             
						<a href="javascript:void(0)" onclick="slipTo1('#curcuit')" title="2017鈴鹿8耐-鈴鹿賽道">
							<div class="curcuit" title="2017鈴鹿8耐-鈴鹿賽道"></div>
						</a>                         
					</li>                      
				</ul>
				<div class="col-md-1 col-sm-1 visible-md visible-lg visible-sm"></div>
			</div>
			<div class="news-container width-full content-last-block block suzuka-padding">
				<div class="news-banner width-full text-center">
					<img src="{{assetRemote('image\suzuka\2017\img-thelatest.png')}}" alt="2017鈴鹿8耐-最新賽事新聞" title="2017鈴鹿8耐-最新賽事新聞">        
					<div class="line-event width-full"></div>
				</div>
				<div class="title-news width-full">        
					<div class="ct-title-news width-full text-center">            
						<h2 class="font-bold">Webike moto news</h2>        
					</div>    
				</div>
				@include('response.pages.collection.suzuka8hours.news')
			</div>
			
			<div class="detail-container suzuka-padding clearfix block" id="about">
				<h2 class="title font-bold box border-bottom-red">關於 鈴鹿8耐</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-01.jpg')}}" alt="2017鈴鹿8耐-介紹" title="2017鈴鹿8耐-介紹">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-02.jpg')}}" alt="2017鈴鹿8耐-介紹" title="2017鈴鹿8耐-介紹">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-03.jpg')}}" alt="2017鈴鹿8耐-介紹" title="2017鈴鹿8耐-介紹">
							</figure>
						</li>
					</ul>
				</div>
				<div class="text">
					<h3 class="block font-bold">40年經典賽事</h3>
					<span class="size-10rem">2017 40屆鈴鹿8耐 首次成為世界耐力錦標賽WEC最終戰，今年絕對比以往更能引起世界各地賽車迷的注意。<br><br>第一屆鈴鹿8耐在1978年舉行，這是日本在全球舞台上展露頭角的一年，例如沖繩島交通法規的變化、成田國際機場正式啟用。這40年來日本創締造許多為人樂道的歷史事蹟。<br><br>鈴鹿8耐第一屆獲勝車隊是吉村YOSHIMURA。之後的39年，他們從未缺席任何一場鈴鹿8耐。奠定了吉村與鈴鹿8耐歷史上密不可分的關係，各家車廠間的競爭隨之展開。鈴鹿8耐現在是HONDA，YAMAHA，SUZUKI，KAWASAKI等日本4大廠的必爭之地。贏得鈴鹿8耐的車廠，除了贏得榮耀，最重要的是贏得車迷對車廠的支持與信任。<br><br>Wayne Gardner、Wes Cooley、Mike Baldwin、Graeme Crosby、David Aldana、HervéMoineau等是廣為人知鈴鹿8耐的重要歷史人物。其中Wayne Gardner不得不另外介紹，Mr. Gardner多次在鈴鹿8耐獲勝，並在1987年贏得WGP500世界冠軍。鈴鹿8耐的聲望與知名度也隨著Mr. Gardner水漲船高。<br><br>隨著Wayne Gardner的崛起，許多世界摩托車錦標賽的車手開始參加鈴鹿8耐。如Kenny Roberts、Tadahiko Taira、Eddie Lawson、Wayne Rainey、Kevin Schwantz、Mick Doohan和Valentino Rossi…等等，這些車手的加入，往往成為一整年車迷們茶餘飯後的焦點話題。<br><br></span>
					<h3 class="block font-bold">鈴鹿8耐 讓鈴鹿賽車場進入全新的領域</h3>
					<span class="size-10rem">世界頂級車手將鈴鹿8耐轉變為“衝刺耐力賽”，贏得世界耐力錦標賽的關鍵是穩定駕駛與速度，這是耐久賽不變的道理，但為了贏得鈴鹿8耐，車手們不僅僅要有穩定的油門，而是以最接近極限的方式”穩定”騎乘。<br><br>世界頂尖車手從以前到現在總能創造出許多驚人的紀錄。 2015年獲勝者YAMAHA FACTORY RACING TEAM在去年延攬了MotoGP車手Pol Espargaró、Bradley Smith與日本首席車手 中須賀克行 加入，創造一個夢幻的團隊。且MotoGP車手Pol Espargaró騎乘YZF-R1廠車在排位賽跑出相當驚人2：06.258單圈成績。<br><br>MuSashi RT HARC-PRO。（HONDA）2016年找了前MotoGP冠軍，Casey Stoner 與已故車手 Nicky Hayden。Yoshimura Suzuki Shell Advance (SUZUKI)和團隊KAGAYAMA（SUZUKI）帶回了老將Kevin Schwantz。Leon Haslam則是加入Team GREEN (Kawasaki)，並獲得第二名，是場上唯一能對YAMAHA場對造成威脅的車隊。<br><br>今年2017 40屆“鈴鹿8耐”是日本賽車史上重要的里程碑，也是各車廠、車手們競爭的最佳舞台。</span>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block">
				<h2 class="title font-bold box">第40屆 鈴鹿8耐 觀賽重點</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-04.jpg')}}" alt="2017鈴鹿8耐-觀賽重點" title="2017鈴鹿8耐-觀賽重點">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-05.jpg')}}" alt="2017鈴鹿8耐-觀賽重點" title="2017鈴鹿8耐-觀賽重點">
							</figure>
						</li>
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-06.jpg')}}" alt="2017鈴鹿8耐-觀賽重點" title="2017鈴鹿8耐-觀賽重點">
							</figure>
						</li>
					</ul>
				</div>
				<div class="text">
					<span class="size-10rem">今年的鈴鹿8耐將成為世界耐力錦標賽的最終戰，同時也是第一次在日本舉辦的最終戰。<br><br>換句話說，這次的大會將成為決定世界耐力錦標賽總排名的最後一戰，所以除了世界耐力錦標賽全程參賽車隊以外，對於只參加鈴鹿8耐的車隊而言，想必激起了比往年更大的動力與熱情。<br><br>世界耐力錦標賽中，F.C.C. TSR Honda與Webike TRICK STAR兩車隊全程參賽，在2016-2017年系列賽的開幕戰Bol d’or 24小時（法國）耐力賽中Webike TRICK STAR與F.C.C. TSR Honda分別拿下了第3名和第5名的好成績，因此在今年鈴鹿8耐期待日本車隊拿下首座冠軍獎盃的呼聲也不斷地高漲中。<br><br>Bol d’or 24小時耐久賽獲勝的S.E.R.T（SUZUKI ENDURANCE RACING TEAM）、SRC Kawasaki、GMT94 YAMAHA、YART Yamaha Official EWC Team等常勝車隊相信也將會參加40屆鈴鹿8耐，尤其是過去曾經拿下15座冠軍獎盃的S.E.R.T從2015年起達成V2蟬聯兩年冠軍，今年將首度挑戰鈴鹿8耐。<br><br>從2015年起連續獲勝的YAMAHA FACTORY RACING TEAM雖然還沒有公布參賽選手陣容，不過日前已經表示將會派出兩車隊參加鈴鹿8耐，車隊王牌-中須賀克行選手如果在鈴鹿8耐達成3連勝的話，將會成為繼1993-1995年Aaron Slight選手之後的第2人。<br><br>投入新款參賽車的MuSASHi RT HARC-PRO.車隊，到目前為止曾經與Casey Stoner、Nicky Hayden等MotoGP前冠軍選手合作，今年則確認將起用擁有3次獲勝經驗的高橋巧選手與被稱為「距離MotoGP最近的日本選手」Moto2選手—中上貴晶，另一名參賽選手的動向則值得關注。<br><br>另外，名門車隊MORIWAKI MOTUL RACING復活參戰，並且起用了高橋裕紀選手與清成龍一選手，兩位選手都擁有豐富的賽事經驗，其中清成選手擁有與宇川徹同樣的鈴鹿8耐最多的5勝紀錄。<br><br>此外，參賽車GSX-R1000全面改款的SUZUKI車隊群中，將以Yoshimura SUZUKI MOTUL Racing與由選手兼教練的加賀山就臣領軍的Team KAGAYAMA為參賽主力車隊，兩車隊都還沒公布參賽選手陣容，不過在以往的鈴鹿8耐中，兩車隊都招攬擁有超強戰力的選手參賽，所以今年同樣值得關注SUZUKI將招攬哪些選手參加鈴鹿8耐。<br><br>唯一能夠與去年鈴鹿8耐獲勝的YAMAHA FACTORY RACING TEAM在同一圈速上較勁的Kawasaki Team GREEN，參加全日本JSB1000級別賽事的選手大換血，因此今年的”Coca Cola”鈴鹿8小時耐力賽想必會是一場全新的挑戰。<br><br>另外，去年的鈴鹿8耐舉辦了暌違已久的8小時完全乾地賽事，並且賽程中沒有前導車介入，在這樣的情況下獲勝的YAMAHA FACTORY RACING TEAM創下了218圈的紀錄，也因此在今年的鈴鹿8耐，奪下冠軍的指標圈數就成了218圈。※最高圈紀錄是由Cabin Honda車隊加藤大治郎與Colin Edwards在2002年創下的219圈。</span>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block">
				<h2 class="title font-bold box">鈴鹿4耐 未來之星比賽</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-07.jpg')}}" alt="鈴鹿4耐-未來之星比賽">
							</figure>
						</li>
					</ul>
				</div>
				<div class="text">
					<span class="size-10rem">鈴鹿4耐在鈴鹿8耐決賽前一天舉行。這將是日本與世界各地業餘與新人車手的一場競賽。在這場比賽中，兩名騎手在4小時內輪流騎乘一台賽車。比賽車輛規定是4行程600cc，ST組規則，並使用普利司通輪胎，在比賽中不能更換輪胎。這些規定使得一些人認為比鈴鹿8耐困難度更高。如果要贏得鈴鹿4耐，保護輪胎成為重要的關鍵。<br><br>亞洲賽車錦標賽(ARRC)在馬來西亞，泰國，印尼，印度和鈴鹿舉辦，亞洲車手近年來不斷的成長。去年鈴鹿4耐獲獎者是來自印尼的Irfan Ardiansyah和Rheza Danica。第三輪亞洲賽車錦標賽將於6月2日至4日在鈴鹿賽道舉行。許多年輕的日本和其他亞洲車手將參加這個比賽，被視為今年鈴鹿4耐的前哨站。</span>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block" id="active">
				<div class="width-full clearfix">
					<h2 class="title font-bold box border-bottom-red">周邊活動</h2>
					<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
						<ul class="clearfix row">
							<li class="col-md-12 col-sm-12 col-xs-12">
								<figure class="zoom-image">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-08.jpg')}}" alt="2017鈴鹿8耐-周邊活動" title="2017鈴鹿8耐-周邊活動">
								</figure>
							</li>
						</ul>
					</div>
					<div class="text">
						<span class="size-10rem">鈴鹿8耐的重頭戲之一正是夜間賽程的「Night Run」，把點亮暗夜的螢光棒發給V1、V2席的觀賽者，幫在8小時的嚴酷賽事中奮戰的選手們加油打氣。<br><br>夜間觀賽期間，持有螢光棒的觀賽者能夠自由變換螢光棒的顏色，為自己心儀的車隊加油，當接近賽事尾聲時，觀賽席將會出現變化，也將會為整個會場營造出炫幻般的景象。</span>

						{{-- <h3 class="block font-bold">Soaking wet & FMX performance, BIKE! LIVE! SPLASH!.</h3>
						<span>The World's top FMX performances will be combined with a cooling-of service. You can watch dynamic bike performances while being splashed with cool and refreshing water.</span> --}}
					</div>
				</div>
			
				<div class="gallery-container-style2 clearfix">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
							<div class="gallery-contain box">
								<div class="width-full text-center box">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-09.jpg')}}" alt="2017鈴鹿8耐-各車廠 最新車款展示與試乘" title="2017鈴鹿8耐-各車廠 最新車款展示與試乘">
								</div>
								<h3 class="box font-bold">各車廠 最新車款展示與試乘</h3>
								<div class="description width-full"><span class="size-10rem">從250cc到1000cc，日、歐、美各大車廠不只展示最新車輛並提供試乘活動給來自世界各地的車迷朋友。</span></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
							<div class="gallery-contain box">
								<div class="width-full text-center box">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-10.jpg')}}" alt="2017鈴鹿8耐-“騎士村莊”周邊商品販售" title="2017鈴鹿8耐-“騎士村莊”周邊商品販售">
								</div>
								<h3 class="box font-bold">“騎士村莊”周邊商品販售</h3>
								<div class="description width-full"><span class="size-10rem">不管是車上用的，身上穿的，”騎士村莊”裡都找的到，若沒有騎車的車迷也不用擔心，各品牌周邊精品也有販售。</span></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 gallery-style2">
							<div class="gallery-contain box">
								<div class="width-full text-center box">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-11.jpg')}}" alt="2017鈴鹿8耐-摩托車體驗專區" title="2017鈴鹿8耐-摩托車體驗專區">
								</div>
								<h3 class="box font-bold">摩托車體驗專區</h3>
								<div class="description width-full"><span class="size-10rem">還沒有駕照嗎?但又想體驗摩托車騎乘與換檔樂趣，那一定要來體驗專區，歡迎全家大小一同參加體驗。</span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="detail-container suzuka-padding clearfix block">
				<h2 class="title font-bold box">第1屆鈴鹿8耐冠軍選手 W.Cooley確定擔任活動嘉賓</h2>
				<div class="clearfix introduce introduce-left left suzuka-padding-text">
					<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
						<ul class="clearfix row">
							<li class="col-md-12 col-sm-12 col-xs-12">
								<figure class="zoom-image">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-14.jpg')}}" alt="2017鈴鹿8耐-W.Cooley擔任活動嘉賓" title="2017鈴鹿8耐-W.Cooley擔任活動嘉賓">
								</figure>
							</li>
						</ul>
					</div>
					<div class="text">
						<h3 class="block font-bold">1978年第一屆鈴鹿8耐的冠軍選手 W.Cooley確定獲邀參加第40屆“Coca Cola”鈴鹿8小時耐力賽。</h3>
						<div class="row">
							<span class="size-10rem col-md-8 col-sm-8 col-xs-12">技壓當時世界聞名的Honda參賽選手群，精彩地奪下第1屆冠軍獎盃的W.Cooley（當時所屬車隊：Yoshimura Japan）將擔任活動嘉賓，將重現當時的賽後訪談與示範騎乘演出等，另外也將邀請多位歷年優勝選手一起出任活動嘉賓。</span>
						</div>
					</div>
				</div>
				<div class="clearfix introduce introduce-right suzuka-padding-text">
					<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
						<ul class="clearfix row">
							<li class="col-md-12 col-sm-12 col-xs-12">
								<figure class="zoom-image">
									<img src="{{assetRemote('image\suzuka\2017\img-tab-news-15.jpg')}}" alt="2017鈴鹿8耐-W.Cooley擔任活動嘉賓" title="2017鈴鹿8耐-W.Cooley擔任活動嘉賓">
								</figure>
							</li>
						</ul>
					</div>
					<div class="text">
						<h3 class="block font-bold">【Wes・Cooley】</h3>
						<div class="row">
							<span class="size-10rem col-md-8 col-sm-8 col-xs-12">1956年生，來自美國・加州，於1978年的鈴鹿8耐第1屆大會上以Yoshimura吉村車隊的參賽選手之姿與Mike Baldwin一同參戰，並且成為值得紀念的第一屆鈴鹿8耐冠軍，1980年與Graeme Crosby組隊參戰再度拿下鈴鹿8耐冠軍。<br><br>除此之外，Cooley也在1979年與1980年的AMA超級摩托車錦標賽中拿下冠軍，活躍於當時的賽車界，並於2004年進入AMA摩托車名人堂。</span>
						</div>
					</div>
				</div>
			</div>
			<div class="ticket-container suzuka-padding clearfix block" id="ticket">
				<h2 class="title font-bold box border-bottom-red">門票販售</h2>
				<div class="detail-img col-md-6 col-sm-6 col-xs-12 clearfix row">
					<ul class="clearfix">
						<li class="col-md-12 col-sm-12 col-xs-12 ticket clearfix suzuka-padding-text">
							<figure class="zoom-image">
								<a href="{{URL::route('product-detail',['url_rewrite' => 't00065108'])}}" title="2017鈴鹿8耐-門票販售" target="_blank">
									<img src="{{assetRemote('image\suzuka\2017\img-banner-ticket2.png')}}" alt="2017鈴鹿8耐-門票販售" title="2017鈴鹿8耐-門票販售">
								</a>
							</figure>
						</li>
					</ul>
				</div>
				<div class="box clearfix col-md-6 col-sm-6 col-xs-12">
					<span class="size-10rem">票券種類：預售觀戰券 (成人)【自由席】</span><br>
					<span class="size-10rem">入場區域：C、D、E、G、I、R、S 區</span><br>
					<span class="size-10rem">※ 若座位為指定席，還需要額外購買指定席位票券</span><br>
					<span class="size-10rem">※ 請隨時留意SUZUKA官方網站上，所釋出的指定席座位及票券種類。</span><br>
					<span class="size-10rem">※ 成人觀戰券已包含Motopia 一日通行證</span><br>
					<span class="size-10rem">※ 成人觀戰券已包含Motopia 一日通行證</span><br>
					<span class="size-10rem">※ 含遊樂園區“Motopia”通行證</span><br>
					<span class="size-10rem">賽事日期：7月27日(四)~7月30日(日)套票...<a href="{{URL::route('product-detail',['url_rewrite' => 't00065108'])}}" title="2017鈴鹿8耐-門票販售" target="_blank">了解更多</a></span><br>

				</div>
			</div>
			<div class="Circuit-container suzuka-padding clearfix block" id="free">
				<h2 class="title font-bold box border-bottom-red">自由行遊記</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12 nic">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img_nic.png')}}" alt="2017鈴鹿8耐-Nic Wang自由行遊記" title="2017鈴鹿8耐-Nic Wang自由行遊記">
							</figure>
						</li>
					</ul>
				</div>
				<h3 class="block font-bold">Nic Wang檔案</h3>
				<span class="size-10rem">Nic是馬來西亞頂尖的重型機車部落客之一。在他接觸重型機車之前，其實早與汽車業有很深的淵源。他原本是街頭賽車手，後來加入Toyo Tires Drift Malaysia車隊。從賽車場退休後，他意外發掘對重型機車的熱情，一切就從他為了每日通勤而買了一輛重型機車開始。如今，Nic是最傑出的重型機車部落客之一。他的部落格是MinistryOfSuperbike.com，追蹤人數在短短一年半的時間內就從40人增加到45,000人。<br><br>讓Nic異軍突起的重點，在於他經營部落格的方式。MinistryOfSuperbike.com部落格與其說是技術取向，其實更聚焦在生活風格。Nic相信網路上已經有許多技術相關的網站了，然而重型機車的生活風格卻是小眾市場。<br><br>Nic對重型機車最初的熱情來自Kawasaki 150 RR，如今他的愛車是Kawasaki Ninja ZX-6R 636。與許多騎士不同的是，他並不喜歡和大型車隊一起上路，也不曾參與任何長途騎乘。他把對重型機車的熱情投注在提升技巧與速度上，因此他的週日例行作業就是到雲頂高原的山路練習騎乘技術。此外，他還會定期在雪邦國際賽道（Sepang International Circuit）的本地賽道體驗活動鍛鍊技術。如今，Nic的部落格引起許多知名廠商的注意，在本文報導的同時，MinistryOfSuperbike.com已和普利司通、Shark Helmets等許多大廠進行合作。</span>
				
			</div>
			<div class="Circuit-container suzuka-padding clearfix block">
				<h2 class="title font-bold box">NIC 8耐觀賽遊記</h2>
				<a href="http://www.suzukacircuit.jp/tw/featured/round01/index.html" target="_blank" title="2017鈴鹿8耐-準備工作" nofollow><span class="size-10rem">ROUND1: Suzuka 8hours Adventure - Part 1（準備工作）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round02/index.html" target="_blank" title="2017鈴鹿8耐-買票囉" nofollow><span class="size-10rem">ROUND2: Suzuka 8hours Adventure - Part 2 （買票囉）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round03/index.html" target="_blank" title="2017鈴鹿8耐-行前記事" nofollow><span class="size-10rem">ROUND3: Suzuka 8hours Adventure - Part 3（行前記事）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round04/index.html" target="_blank" title="2017鈴鹿8耐-大阪一日遊" nofollow><span class="size-10rem">ROUND4: Suzuka 8hours Adventure - Part 4（大阪一日遊）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round05/index.html" target="_blank" title="2017鈴鹿8耐-從大阪到鈴鹿" nofollow><span class="size-10rem">ROUND5: Suzuka 8hours Adventure - Part 5（從大阪到鈴鹿）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round06/index.html" target="_blank" title="2017鈴鹿8耐-自由練習 & 預選" nofollow><span class="size-10rem">ROUND6: Suzuka 8hours Adventure - Part 6（自由練習 & 預選）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round07/index.html" target="_blank" title="2017鈴鹿8耐-Paddock & 展覽" nofollow><span class="size-10rem">ROUND7: Suzuka 8hours Adventure - Part 7（Paddock & 展覽）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round08/index.html" target="_blank" title="2017鈴鹿8耐-競速電影院 & Motopia" nofollow><span class="size-10rem">ROUND8: Suzuka 8hours Adventure - Part 8（競速電影院 & Motopia）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round09/index.html" target="_blank" title="2017鈴鹿8耐-比賽日" nofollow><span class="size-10rem">ROUND9: Suzuka 8hours Adventure - Part 9（比賽日）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round10/index.html" target="_blank" title="2017鈴鹿8耐-餐飲專區@鈴鹿" nofollow><span class="size-10rem">ROUND10: Suzuka 8hours Adventure - Part 10（餐飲專區@鈴鹿）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round11/index.html" target="_blank" title="2017鈴鹿8耐-道頓堀" nofollow><span class="size-10rem">ROUND11: Suzuka 8hours Adventure - Part 11（道頓堀）</span></a><br>
				<a href="http://www.suzukacircuit.jp/tw/featured/round12/index.html" target="_blank" title="2017鈴鹿8耐-耐久賽的難忘體驗" nofollow><span class="size-10rem">ROUND12: Suzuka 8hours Adventure - Final（耐久賽的難忘體驗）</span></a>
			</div>
			<div class="Circuit-container suzuka-padding clearfix block bl" id="curcuit">
				<h2 class="title font-bold box border-bottom-red">鈴鹿賽道介紹</h2>
				<div class="detail-img col-md-4 col-sm-4 col-xs-12 clearfix row">
					<ul class="clearfix row">
						<li class="col-md-12 col-sm-12 col-xs-12">
							<figure class="zoom-image">
								<img src="{{assetRemote('image\suzuka\2017\img-tab-news-13.jpg')}}">
							</figure>
						</li>
					</ul>
				</div>
				<span class="size-10rem">鈴鹿賽車場設立於1962年，為日本最初的正規競速賽車場，開設後便一直是日本賽車運動的中心地。<br><br>從1987年起開辦的F1日本大獎賽，到舉辦超過30次的鈴鹿8小時耐久賽，每項賽事都超越了賽車運動的框架，成為定期大型活動。<br><br>鈴鹿賽車場在舉辦競賽的同時，也致力於振興賽車運動文化。以培育世界級賽車手、騎士為目的所設立的賽車學校，便是其中一環。<br><br>遊樂園MOTOPIA中設有多項年幼孩童也能自己操控的車輛，目的除了在於開拓「未來的粉絲層」外，同時也是基於讓汽車文化在日本扎根之願景。<br><br>此外，本賽車場以「款待」的精神與「機動性度假村」的構思為基礎，傾力於交通安全教育，並力圖完善飯店、餐廳設施，以長宿型休閒度假村的定位，提供優質環境與服務，積極開創更加符合時代潮流的新興休閒事業。<br><br>鈴鹿賽車場以競速為中心，提供充滿魅力的賽車運動、娛樂活動的舞台，吸引全球遊客造訪，從小孩到大人皆可盡歡。撼動人心的「感動」，就在這裡。</span>
				
			</div>
			<div class="clearfix visit">
				<div class="col-md-2 col-sm-2"></div>
				<ul class="ul-menu-event col-md-8 col-sm-8 col-xs-12 clearfix text-center">                         		
					<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                             
						<a href="http://www.suzukacircuit.jp/tw/" target="_blank" nofollow>                                 
							<img src="{{assetRemote('image\suzuka\2017\about-1.png')}}">
						</a>                         
					</li>                         
					<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                             
						<a href="http://www.suzukacircuit.jp/" target="_blank" nofollow>                                 
							<img src="{{assetRemote('image\suzuka\2017\about-2.png')}}">                 
						</a>                         
					</li>                         
					<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12">                             
						<a href="http://www.suzukacircuit.jp/en/" target="_blank" nofollow>                                 
							<img src="{{assetRemote('image\suzuka\2017\about-3.png')}}">                 
						</a>                         
					</li>                                            
				</ul>
				<div class="col-md-2 col-sm-2"></div>
				<br>
				<div class="clearfix width-full text-center offer">
					<span class="size-10rem col-md-12 col-sm-12 col-xs-12">◎「Webike」為本屆賽事官方授權之宣傳單位，宣傳素材由株式會社Mobilityland提供，請勿節錄或轉載。</span>
				</div>
			</div>
		</div>
	</div>
@stop
@section('script')
<script>
	function slipTo1(element){
        var y = parseInt($(element).offset().top) - 80;
        $('html,body').animate({scrollTop: y}, 400);
    }
    $('.link-shopping').removeClass('navbar-active');
</script>
@stop