<style>
	.news-container a {
		color:black;
	}
	.news-container a:hover{
		opacity: 0.7;
		color:black;
	}
	.news-container .other {
		position:relative;
	}
	.news-container .other .readMore {
		position:absolute;
		bottom:0px;
		right:0px;
	}
	.news-container .other img{
		max-height:128px;
	}
	
</style>
<div class="news box clearfix block">
	<div class="main">
		<div class="col-md-5 col-sm-5 col-xs-12">
			<div class="news-img box">
				<a href="{{$suzukaLink[0]}}" alt="{{$suzukaNewTitles[0]}}">
					<img src="{{$suzukaNewsimgs[0]}}" alt="{{$suzukaNewTitles[0]}}" title="{{$suzukaNewTitles[0]}}">
				</a>
			</div>
			<div class="news-information">
				<a href="{{$suzukaLink[0]}}">
					<h3 class="font-bold">{{$suzukaNewTitles[0]}}</h3>
				</a>
				<span>{!! $suzukaNewsContents[0]!!}</span>
			</div>
		</div>
	</div>
	<div class="other">
		<div class="col-md-7 col-sm-7 col-xs-12">
			@foreach($suzukaNewTitles as $key => $suzukaNewTitle)
				@if($key != 0)
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="news-img box">
							<a href="{{$suzukaLink[$key]}}" title="{{$suzukaNewTitle}}">
								<img src="{{$suzukaNewsimgs[$key]}}" alt="{{$suzukaNewTitle}}" title="{{$suzukaNewTitle}}">
							</a>
						</div>
						<a href="{{$suzukaLink[$key]}}">
							<h3 class="font-bold">{{$suzukaNewTitle}}</h3>
						</a>
					</div>
				@endif
			@endforeach
			<a class="readMore" href="https://www.webike.tw/bikenews/tag/suzuka8hours/"><span class="font-color-red"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span><span class="font-color-blue">閱讀更多</span></a>
		</div>
	</div>
</div>