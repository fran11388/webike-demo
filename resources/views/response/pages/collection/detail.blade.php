@extends($layout)
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/collection.css') !!}">
    <style>
        .box-fix-column .ct-right-below .row{
            margin-left:0px !important;
            margin-right:0px !important;
        }
    </style>
@stop
@foreach($blocks as $section => $blocks)
    @section($section)
        @foreach($blocks as $block)
            {!! $block !!}
        @endforeach
    @stop
@endforeach
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>

    <script type="text/javascript">
        var baseUrl= '{{ request()->url() }}';
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }
        $(".select-option-redirect").change(function(){
            window.location = $(this).val();
        });

        function setListMode( value ){
            var d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }
        $("a.a-sub2 > span").click(function(){
            window.location =  $(this).parent().attr('data-href');
            return 0;
        });

        $(".select-motor-manufacturer").change(function(){
            var value = $(this).find('option:selected').val();
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = $(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(".select-motor-displacement").change(function(){
            var value = $(this).find('option:selected').val();
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                        $(".select-motor-manufacturer").find('option:selected').val() +
                        "&displacement=" +
                        value;

                $.get( url , function( data ) {
                    var $target = $(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(".select-motor-model,.select-motor-mybike").change(function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ modifyGetParameters(['mt' => '%s']) }}", value );
            }
        });
    </script>
@stop