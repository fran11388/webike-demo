@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" href="{{assetRemote('css/pages/collection/ngkStock/ngkStock.css?'.'time=')}}">
@stop
@section('middle')
    <div class="ngkStock-container">
        <div class="link-container row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center block">
                <img src="{{assetRemote('image/collection/ngkStock/banner.png')}}" title="NGK火星塞全品項販賣中{{ $tail }}" alt="NGK火星塞全品項販賣中{{ $tail }}" >
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <img class="width-full standard" src="{{assetRemote('image/collection/ngkStock/ngk_normal.png')}}" title="NGK標準型火星塞全品項販賣中{{ $tail }}" >
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <a href="{{URL::route('collection-ngkStock',['type'=>'ir'])}}" title="銥合金火星塞{{ $tail }}">
                    <img class="width-full opacity" src="{{assetRemote('image/collection/ngkStock/ngk_ir.png')}}" title="NGK銥合金火星塞全品項販賣中{{ $tail }}" >
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <a href="{{URL::route('collection-ngkStock',['type'=>'race'])}}" title="競賽型火星塞{{ $tail }}">
                    <img class="width-full opacity" src="{{assetRemote('image/collection/ngkStock/ngk_race.png')}}" title="NGK競賽型火星塞全品項販賣中{{ $tail }}" >
                </a>
            </div>
        </div>
        <div class="description-container text-center block">
            <span class="font-bold size-10rem">「Webike-摩托百貨」NGK標準型火星塞全品項販賣中</span><br>
            <span class="font-bold size-10rem">※當廠商缺貨時，商品交期有可能會再延長，詳細請見各品項商品詳細頁的即時庫存回報。</span>
        </div>
        <div class="title block">
            <h1 class="font-bold">標準型火星塞</h1>
        </div>
        <div class="table">
            <table style="width:1200px">
                <tr>
                    <th style="width:7%"></th>
                    <th style="width:37.2%"><span class="size-10rem">B(14mm)</span></th>
                    <th style="width:27.9%"><span class="size-10rem">C(10mm)</span></th>
                    <th style="width:18.6%"><span class="size-10rem">D(12mm)</span></th>
                    <th style="width:9.3%"><span class="size-10rem">E(8mm)</span></th>
                </tr>
                @foreach($ngkStockDatas as $stockType => $datas)
                    @if($stockType == '熱價7' ||$stockType == '熱價9')
                        <tr>
                            <th style="width:7%"></th>
                            <th style="width:37.2%"><span class="size-10rem">B(14mm)</span></th>
                            <th style="width:27.9%"><span class="size-10rem">C(10mm)</span></th>
                            <th style="width:18.6%"><span class="size-10rem">D(12mm)</span></th>
                            <th style="width:9.3%"><span class="size-10rem">E(8mm)</span></th>
                        </tr>
                    @endif
                    <tr class="text-center">
                        <td class="data-title"><span class="size-10rem">{{ $stockType }}</span></td>
                        @foreach($datas as $key => $data)
                            @php
                                switch ($key) {
                                    case '0':
                                        $size = '3';
                                        break;
                                    case '1':
                                        $size = '4';
                                        break;
                                    case '2':
                                        $size = '6';
                                        break;
                                   case '3':
                                        $size = '12';
                                        break;
                                }
                            @endphp
                            <td class="data">
                                <ul class="clearfix">
                                    @foreach($data as  $sku)
                                        @php
                                            $product = $ngkProducts->get($sku);
                                        @endphp
                                        @if($product)
                                            <a href="{{URL::route('product-detail',['url_rewrite'=>$sku])}}" target="_blank" title="{{$product->name.$tail}}">
                                                <li class="item col-md-{{ $size }} col-sm-{{ $size }} col-xs-{{ $size }}">
                                                    <div class="ngkProduct">
                                                        <div class="box">
                                                            <span class="font-bold size-085rem ">{{ $product->model_number }}</span>
                                                        </div>
                                                        <img src="{{assetRemote('image/collection/ngkStock/normal.png')}}">
                                                    </div>
                                                    <span class="helper"></span>
                                                </li>
                                            </a>
                                        @else
                                            <li class="item col-md-{{ $size }} col-sm-{{ $size }} col-xs-{{ $size }}"></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="link-container row">
            <div class="col-md-4 col-sm-4 hidden-xs text-center block"></div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <a class="footer-button" href="{{URL::route('parts',['section'=>'/br/497?q=標準'])}}" title="標準型火星塞{{ $tail }}" target="_blank">
                    <img class="width-full" src="{{assetRemote('image/collection/ngkStock/NGKbutton_standard.png')}}" title="NGK標準型火星塞全品項販賣中{{ $tail }}" >
                </a>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs text-center block"></div>
        </div>
    </div>
@stop