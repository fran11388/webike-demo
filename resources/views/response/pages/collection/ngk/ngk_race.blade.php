@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" href="{{assetRemote('css/pages/collection/ngkStock/ngkStock.css?'.'time=')}}">
@stop
@section('middle')
    <div class="ngkStock-container">
        <div class="link-container row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center block">
                <img src="{{assetRemote('image/collection/ngkStock/banner.png')}}" title="NGK火星塞全品項販賣中{{ $tail }}" alt="NGK火星塞全品項販賣中{{ $tail }}" >
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <a href="{{URL::route('collection-ngkStock',['type'=>'normal'])}}" title="標準型火星塞{{ $tail }}">
                    <img class="width-full opacity" src="{{assetRemote('image/collection/ngkStock/ngk_normal.png')}}" title="NGK標準型火星塞全品項販賣中{{ $tail }}" >
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <a href="{{URL::route('collection-ngkStock',['type'=>'ir'])}}" title="銥合金火星塞{{ $tail }}">
                    <img class="width-full opacity" src="{{assetRemote('image/collection/ngkStock/ngk_ir.png')}}" title="NGK銥合金火星塞全品項販賣中{{ $tail }}" >
                </a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <img class="width-full race" src="{{assetRemote('image/collection/ngkStock/ngk_race.png')}}" title="NGK競賽型火星塞全品項販賣中{{ $tail }}" >
            </div>
        </div>
        <div class="description-container text-center block">
            <span class="font-bold size-10rem">「Webike-摩托百貨」NGK競賽型火星塞全品項販賣中</span><br>
            <span class="font-bold size-10rem">※當廠商缺貨時，商品交期有可能會再延長，詳細請見各品項商品詳細頁的即時庫存回報。</span>
        </div>
        <div class="title block" style="background: #E70010;color:#fff;">
            <h1 class="font-bold">競賽型火星塞</h1>
        </div>
        <div class="table">
            <table style="width:1200px">
                <tr>
                    <th style="width:7%"></th>
                    <th style="width:37.2%"><span class="size-10rem">B(14mm)</span></th>
                    <th style="width:27.9%"><span class="size-10rem">C(10mm)</span></th>
                    <th style="width:18.6%"><span class="size-10rem">D(12mm)</span></th>
                    <th style="width:9.3%"><span class="size-10rem">E(8mm)</span></th>
                </tr>
                @foreach($ngkStockDatas as $stockType => $datas)
                    <tr class="text-center">
                        @if($stockType == '熱價10.5')
                            <td class="data-title"><span class="size-10rem">熱價10.5</span></td>
                        @elseif($stockType == '熱價9.5')
                            <td class="data-title"><span class="size-10rem">熱價9.5</span></td>
                        @else
                            <td class="data-title"><span class="size-10rem">{{ $stockType }}</span></td>
                        @endif
                        @foreach($datas as $key => $data)
                            @php
                                switch ($key) {
                                    case '0':
                                        $size = '3';
                                        break;
                                    case '1':
                                        $size = '4';
                                        break;
                                    case '2':
                                        $size = '6';
                                        break;
                                   case '3':
                                        $size = '12';
                                        break;
                                }
                            @endphp
                            <td class="data">
                                <ul class="clearfix">
                                    @foreach($data as  $sku)
                                        @php
                                            $product = $ngkProducts->get($sku);
                                        @endphp
                                        @if($product)
                                            <a href="{{URL::route('product-detail',['url_rewrite'=>$sku])}}" target="_blank" title="{{$product->name.$tail}}">
                                                <li class="item col-md-{{ $size }} col-sm-{{ $size }} col-xs-{{ $size }}">
                                                    <div class="ngkProduct">
                                                        <div class="box">
                                                            <span class="font-bold size-085rem ">{{ $product->model_number }}</span>
                                                        </div>
                                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                                    </div>
                                                    <span class="helper"></span>
                                                </li>
                                            </a>
                                        @else
                                            <li class="item col-md-{{ $size }} col-sm-{{ $size }} col-xs-{{ $size }}"></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="title block">
            <h1 class="font-bold">特殊火星塞</h1>
        </div>
        <div class="table">
            <table style="width:1200px">
                <tr>
                    <th style="width:7%"></th>
                    <th><span class="size-10rem">螺紋直徑(依廠商資訊)</span></th>
                </tr>
                <tr class="text-center">
                    <td><span class="size-10rem">熱價9</span></td>
                    <td class="data">
                        <ul class="clearfix special text-center">
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'1957802'])}}" target="_blank" title="{{$ngkProducts->get('1957802')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('1957802')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="text-center">
                    <td><span class="size-10rem">熱價10.5</span></td>
                    <td class="data">
                        <ul class="clearfix special text-center">
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'1953385'])}}" target="_blank" title="{{$ngkProducts->get('1953385')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('1953385')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'1957793'])}}" target="_blank" title="{{$ngkProducts->get('1957793')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('1957793')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'1957796'])}}" target="_blank" title="{{$ngkProducts->get('1957796')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('1957796')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'1957801'])}}" target="_blank" title="{{$ngkProducts->get('1957801')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('1957801')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'1957804'])}}" target="_blank" title="{{$ngkProducts->get('1957804')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('1957804')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <a href="{{URL::route('product-detail',['url_rewrite'=>'21683376'])}}" target="_blank" title="{{$ngkProducts->get('21683376')->name.$tail}}">
                                <li>
                                    <div class="ngkProduct">
                                        <div class="box">
                                            <span class="font-bold size-085rem">{{ $ngkProducts->get('21683376')->model_number }}</span>
                                        </div>
                                        <img src="{{assetRemote('image/collection/ngkStock/race.png')}}">
                                    </div>
                                    <span class="helper"></span>
                                </li>
                            </a>
                            <li></li><li></li><li></li><li></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="link-container row">
            <div class="col-md-4 col-sm-4 hidden-xs text-center block"></div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center block">
                <a class="footer-button" href="{{URL::route('parts',['section'=>'/br/497?q=競賽'])}}" title="競技型火星塞{{ $tail }}" target="_blank">
                    <img class="width-full" src="{{assetRemote('image/collection/ngkStock/NGKbutton_race.png')}}" title="NGK競技型火星塞全品項販賣中{{ $tail }}" >
                </a>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs text-center block"></div>
        </div>
    </div>
@stop