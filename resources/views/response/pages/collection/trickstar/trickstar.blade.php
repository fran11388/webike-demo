@extends('response.layouts.1column')
@section('style')

<style>
.trickstar-content h1{
	margin: 20px 0;
	font-weight: bold;
	font-size: 1.6rem;
}

.trickstar-content p{
	font-size: 1rem;
	line-height: 1.6;
}

.trickstar-content h2{
	border-bottom: 1px solid #aaa;
	margin: 50px 0 10px 0;
	padding: 5px;
	line-height: 1;
	font-size: 1.4rem
}

.trickstar-content .image-box img{
	margin: 10px 0;
}

.trickstar-content .image-box .float{
	float: left;
	height: 400px;
	margin-bottom: 50px; 
}

.trickstar-content .image-box figure{
	width: 50%;
	padding: 0 10px;
}

.trickstar-content .text-red{
	color: red;
}

.trickstar-content .text-purple{
	color: purple;
}

.trickstar-content li{
	list-style: none;
	font-size: 1rem;
}

.trickstar-content li i{
	font-size: 1.4rem;
    padding: 5px;
}

.trickstar-content .figure-caption{
	color: #1E252E;
	font-size: 1rem;
}

h3 a{
	font-size: 1rem;
}

.list{
	display: inline-block;
	padding: 20px;
	background: #EEE;
	border-radius: 5px;
	position: absolute;
	top: 31%;
	right: 20%;
}

.fix{
	position: fixed;
	top: 10%;
	right: 20%;
}

@media all and (max-width: 768px) {
	.list{
		display: inline-block;
		position: initial;
	}
}

.list2{
	display: inline-block;
	padding: 20px;
	background: #EEE;
	border-radius: 5px;
	margin: 20px 0;
}

.list2 a{
  display: block;
  padding: 3px 0;
  font-size: 1.2rem;
}

.list2 h3{
  padding: 5px 0;
  font-size: 1.2rem;
}

.list a{
  display: block;
  padding: 3px 0;
  font-size: 1.2rem;
}
  
.list h3{
  padding: 5px 0;
  font-size: 1.2rem;
}

.hight{
  height: 300px !important;
}

.hight2{
  height: 300px !important;
  margin-bottom: 10px !important;
}

.image-box{
	margin: 20px 0;
}

.p-box{
	margin: 20px 0;
}

.vedio-box{
	margin: 20px 0; 
}

.trickstar-warp{
	width: 800px;
}

.hight3{
	height: 300px !important;
}

.px-2{
	padding: 0 10px;
}

</style>


@stop
@section('middle')
	<div class="all-content">
	<div class="list">
		<h3>目錄</h3>
		<a href="#one">1.TRICK STAR Racing</a>
		<a href="#two">2.IKAZUCHI排氣管</a>
		<a href="#three">3.引擎保護外蓋</a>
		<a href="#four">4.競賽手套（短版&長版）</a>
		<a href="#five">5.把手拉桿護弓</a>
		<a href="#six">6.鈦合金散熱水管</a>
		<a href="#seven">7.車身保護滑塊</a>
	</div>
	<div class="trickstar-warp">
		<div class="trickstar-content">

			<h1>這次要為大家介紹TRICK STAR的商品，競賽零件果然看起來非常帥氣！</h1>
			
			<p>
				以EWC世界耐力錦標賽為主參加各大摩托車賽事的「TRICK STAR」，以「製作摩托車騎士真正想要的東西」為信念，並活用在比賽中高水準的戰鬥經驗，為大家提供站在摩托車騎士立場思考所製作的商品及各項服務。
			</p>

			

			<h2 id="one">TRICK STAR Racing</h2>

			<div class="image-box">
				<img class="px-2" src="{{assetRemote('image/collection/trickstar/zip/TRICK1.jpg')}}" alt="">

				<figure class="figure float hight2">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK2.jpg')}}" class="figure-img img-fluid rounded row" alt="...">
					<figcaption class="figure-caption">EVA RT初號機Webike TRICK STAR</figcaption>
				</figure>

				<figure class="figure float hight2">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK3.jpg')}}" class="figure-img img-fluid rounded alt="...">
					<figcaption class="figure-caption">Webike Tati Team TRICK STAR</figcaption>
				</figure>
				<div class="fixclear"></div>	
			</div>
			<p>
				成立於2001年的TRICK STAR主要以「世界耐力錦標賽（EWC）」為主，並參加「亞洲摩托車錦標賽（ARRC）」、「全日本摩托車錦標賽（MFJ SUPERBIKE）」等各式各樣的摩托車賽事。
				<br>
				TRICK STAR在去年(2018)8月宣布將全程參加2018-19 EWC世界耐力錦標賽之後，於首戰的Bol d'Or 24小時耐力賽就拿下<span class="text-red">SST級別第二名及綜合第八名的成績！</span>
				<br>
				此外，在鈴鹿賽道舉辦的「鈴鹿8小時耐力賽」中則以<span class="text-purple">「EVA塗裝」</span>為特色的「EVA RT初號機Webike TRICK STAR」參賽。
				<br>
				TRICK STAR以「不侷限於勝利的比賽」、「令人憧憬的帥氣比賽」、「連結新世代的比賽」為概念，持續挑戰各大摩托車賽事，並不斷向世人傳遞摩托車的魅力所在。 
				<br>
				而TRICK STAR Racing的代表鶴田竜二是前廠隊車手，他從1984年就開始參加摩托車錦標賽，更在2009年的鈴鹿8小時耐力賽中代表車隊拿下亞軍，留下無數輝煌的參賽經歷。
				<br>
				這個由真正車手親自參與零件研發的品牌，活用比一般人更加豐富的摩托車賽事經驗，把「正統競賽規格」的信念致力於所有的零件製造之中。
			</p>
			<h2 id="two">IKAZUCHI排氣管</h2>
			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK4.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">TRICK STAR Racing實際在比賽中使用的排氣管</figcaption>
				</figure>
				<div class="fixclear"></div>
			</div>
			<p>
				由TRICK STAR推出的排氣管的特徵是六角形，這個形狀是透過流體力學分析所設計出來，在追求能承受比賽時間最長24小時的耐用性及性能後，最終確立為這個形狀。
				<br>
				藉由採用這個形狀，讓排氣管能在適當排出壓力的同時提高廢氣的流動速度，並且成功達到在「高轉速範圍有著壓倒性的延伸感」以及在「低中轉速範圍確保厚實的扭力輸出」。
				<br>
				除了推出車款專用的排氣管外，也有販售<a href="https://www.webike.tw/parts?q=IKAZUCHI%20Universal">通用型排氣管</a>。
			</p>

			<div class="p-box">
				<p>【IKAZUCHI排氣管的實績】</p>

				<ul>
					<li><i>●</i>Bol d'Or 24小時耐久賽（2016年）季軍，參賽車ZX-10R還創下最高時速343km/h的紀錄！比起其他競賽對手還要更快10km/h。</li>
					<li><i>●</i>在日本自動車研究所（JARI，Japan Automobile Research Institute）所實行的KAWASAKI H2R最高時速測試（車手：鶴田竜二）中，創下385km/h的紀錄。</li>
				</ul>		
			</div>

			<div class="image-box">

				<figure class="figure float hight3">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK5.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">插口直徑是通用性極高的Φ60.5mm</figcaption>
				</figure>

				<figure class="figure float hight3">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK6.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">同時推出長版（475mm）與短版（410mm）</figcaption>
				</figure>
				<figure class="figure float hight3">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK7.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">六角形開口意外地很大。 </figcaption>
				</figure>
				<figure class="figure float hight3">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK8.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">左右兩側都有TRICK STAR標誌</figcaption>
				</figure>
				<div class="fixclear"></div>	
			</div>


			<p>
				此外，透過這個和管樂器相似的排氣管形狀，讓車輛在全轉速範圍都能響起魄力十足的排氣音浪。
				<br>
				有興趣的朋友們請參考影片介紹！在這支影片中可以聽到
				<br>
				KAWASAKI H2（slip-on滑套型）、KAWASAKI Z900RS（全段排氣管）的「IKAZUCHI排氣音浪」。
				<br>
				其他像是「煞車拉桿護弓」、「引擎用護蓋」等，由TRICK STAR推出給車輛細節使用的各種升級零件也非常值得注意。
			</p>

			<div class="vedio-box">
				<h3>
					<a href="https://www.youtube.com/watch?v=O8MJ-iMEElU">
						<i>▼</i>採用TRICK STAR製零件的H2 & Z900RS！展示IKAZUCHI排氣管的排氣音浪及自創零件的【Webike TV】
					</a>
				</h3>
				<div class="vedio">
					<iframe width="75%" height="300" src="https://www.youtube.com/embed/O8MJ-iMEElU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>


				<a href="https://www.webike.tw/parts/br/785?q=IKAZUCHI+%E6%8E%92%E6%B0%A3%E7%AE%A1">
					<p>
						<i>●</i>IKAZUCHI 全段/尾段排氣管商品頁面
					</p>
				</a>
				<a href="https://www.webike.tw/parts?q=IKAZUCHI%20Universal">
					<p>
					<i>●</i>IKAZUCHI 通用型排氣管尾段商品頁面
					</p>
				</a>


			<h2 id="three">引擎保護外蓋</h2>
			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK9.jpg')}}" alt="">
				</figure>
			</div>

			<p>
				引擎保護外蓋能在發生摔車意外時，保護引擎不受損傷的第二層保護蓋，特色是採用鋁合金切削加工，透過切削加工的方式，成功兼顧「強度」與「輕量化」的優點。
				<br>
				原本切削加工的製品都會留下切割過的痕跡，但是引擎保護外蓋將這些痕跡全部消去並加上黑色防蝕處理，呈現出和原廠零件不相上下的極高品質及兼具成熟氣質的典雅質感。
			</p>

			
			<div class="image-box">
				<p><i>▼</i>保留切割痕跡的例子，將切削風格作為吸引人的特色，藉此呈現出改裝感。</p>
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK10.jpg')}}" alt="">
				</figure>
			</div>
			
			<div class="image-box">
				<p><i>▼</i>去除掉引擎保護外蓋上的切割痕跡，在呈現出一體感的同時也表現出高級感。</p>
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK11.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>	
			</div>
			<a href="https://www.webike.tw/parts/ca/1000-1180-1316-1120?q=TRICK+STAR++%E5%BC%95%E6%93%8E">
				<p><i>●</i>引擎保護外蓋套件商品頁面</p>
			</a>

			<h2 id="four">競賽手套（短版&長版）</h2>
			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK12.jpg')}}" alt="">
				</figure>
				<div class="fixclear"></div>	
			</div>

			<p>
				因為選用柔軟的皮革製作而成，所以從買回來剛開始戴就不會有壓迫感，讓人能輕鬆操控油門與煞車。
				<br>
				這次TRICAKSTAR推出的競賽手套有分長短兩款，每一款都能同時應付長途旅行與賽道騎乘等多種用途。
			</p>

			
			<div class="image-box">
				<p><i>▼</i>左邊是短版、右邊是長版競賽手套</p>
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK13.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>	
			</div>
			
			<p>
				<a href="https://www.webike.tw/parts/ca/3000?q=TRICK+STAR+%E7%AB%B6%E8%B3%BD%E7%9F%AD%E6%89%8B%E5%A5%97">【競賽短版防摔手套】</a>
			</p>
			
			<div class="image-box">
				
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK14.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption"><i>●</i>尺寸：S/M/L/LL</figcaption>
					<figcaption class="figure-caption"><i>●</i>顏色：白色、黑色、綠色、紅色、藍色</figcaption>
				</figure>
				<figure class="figure float">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK15.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">止滑部分的圖案是TRICK STAR Racing標誌中的「星星圖案」</figcaption>
				</figure>
				<figure class="figure float">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK16.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">食指部分能夠操控智慧型手機等觸控螢幕</figcaption>
				</figure>
				<div class="fixclear"></div>	
			</div>

			
			<div class="image-box">
				<p><i>▼</i>推出的手套顏色從左邊開始分別是「紅色、綠色、藍色、黑色、白色」</p>
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK17.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>	
			</div>

			<p><a href="https://www.webike.tw/parts/ca/3000-3020-3060-3061?q=TRICK+STAR+%E6%89%8B%E5%A5%97">【競賽長版防摔手套】</a></p>

			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK18.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption"><i>●</i>尺寸：S/M/L/LL</figcaption>
					<figcaption class="figure-caption"><i>●</i>顏色：白色、黑色、綠色、紅色、藍色</figcaption>
				</figure>

				<figure class="figure float">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK19.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">手掌部分使用記憶海綿</figcaption>
				</figure>
				<figure class="figure float">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK20.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">食指部分一樣能夠操控智慧型手機等觸控式螢幕</figcaption>
				</figure>
				<div class="fixclear"></div>
			</div>

			

			<div class="image-box">
				<p><i>▼</i>推出顏色從左邊開始分別是「紅色、綠色、藍色、黑色、白色」</p>
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK21.jpg')}}" class="figure-img img-fluid rounded" alt="...">

				</figure>
				<div class="fixclear"></div>
			</div>
			<a href="https://www.webike.tw/parts/ca/3000-3020-3060-3061?q=TRICK+STAR+%E6%89%8B%E5%A5%97">
				<p><i>●</i>競賽手套</p>
			</a>
			<a href="https://www.webike.tw/parts/ca/3000?q=TRICK+STAR+%E7%AB%B6%E8%B3%BD%E7%9F%AD%E6%89%8B%E5%A5%97">
				<p><i>●</i>競賽手套(短版)</p>
			</a>




			<h2 id="five">把手拉桿護弓</h2>
			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK22.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>
			</div>

			<p>
				這款把手拉桿護弓是TRICK STAR Racing實際使用於比賽時間最長24小時的世界耐力錦標賽中的商品，所以耐用性值得信賴！
				<br>
				TRICK STAR Racing實際在「EWC世界耐力錦標賽  利曼24小時耐力賽」中安裝在ZX-10R上，證明了這項產品能承受比賽中的碰撞及耐力賽事的嚴苛環境。
				<br>
				材質選用重視耐用性的鋁合金切削加工，同時也考慮到可能會干擾到拉桿，因此護弓部分能夠調整15mm的寬度。
			</p>


			
			<div class="image-box">
				<p><i>▼</i>實際在EWC世界耐力錦標賽的利曼24小時耐力賽中使用</p>
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK23.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<figure class="figure float">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK24.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">採用無段調整，可調整範圍多達15mm。</figcaption>
				</figure>
				
				<figure class="figure float">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK25.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">尾端的「TRICK STAR」字母設計成競賽風格</figcaption>
				</figure>
				<div class="fixclear"></div>
			</div>

			<a href="https://www.webike.tw/parts?q=TRICK%20STAR%20%E8%AD%B7%E5%BC%93">
				<p><i>●</i>把手拉桿護弓套件商品頁面</p>	
			</a>

			<h2 id="six">鈦合金散熱水管</h2>

			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK26.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>
			</div>

			<p>
				使用熱傳導率極高的鈦合金為材質，藉此發揮出出色的冷卻效果！更加入彩鈦加工處理，因此在提升冷卻效果的同時還能當作裝飾零件。
				<br>
				這次我們特別借來示範的車款是搭載了機械增壓的Ninja H2，雖然鈦合金散熱水管實際上和機械增壓的功能毫無關係，但是從原廠橡膠水管換成金屬材質，更能加強搭載機械增壓的氛圍。
			</p>
			<div class="image-box">
				<figure class="figure float hight2">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK27.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<figure class="figure float hight2">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK28.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>
			</div>
			<a href="https://www.webike.tw/parts?q=TRICK%20STAR%20%E6%B0%B4%E7%AE%A1"><p><i>●</i>鈦合金散熱水管套件商品頁面</p></a>


			<h2 id="seven">車身保護滑塊</h2>


			<div class="image-box">
				<figure class="figure">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK29.jpg')}}" class="figure-img img-fluid rounded" alt="...">
				</figure>
				<div class="fixclear"></div>
			</div>
			<p>
				車身保護滑塊可以讓摔車時的衝擊力道透過滑行來減弱，除了能在意外發生時發揮保護效果外還能當作車身造型，所以為了讓損傷能降到最低並讓愛車有帥氣的外觀，加裝保護滑塊是不能省的升級花費！
				<br>
				保護滑塊部分使用塑鋼製造，並加入TRICK STAR Racing的標誌，形狀則以經典的圓錐型為首，同時還推出具流線型的TYPE-E、三角形的TYPE-D等多種款式。
				<br>
				此外，和一般車架保護滑塊完全不同的這個形狀，絕對是有考慮到流體力學而製作的商品！
				<br>
				不管是不是能夠實際體驗到流體力學的效果，但是在保護車身的同時考慮到流體力學，讓這個商品能使人感受到「正統競賽規格」的感覺。
			</p>

			<div class="image-box">

				<figure class="figure float hight">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK30.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">TYPE-E</figcaption>
				</figure>
				
				<figure class="figure float hight">
					<img src="{{assetRemote('image/collection/trickstar/zip/TRICK31.jpg')}}" class="figure-img img-fluid rounded" alt="...">
					<figcaption class="figure-caption">TYPE-D</figcaption>
				</figure>
				<div class="fixclear"></div>
			</div>
			<a href="https://www.webike.tw/parts/ca/1000-1059-1117?q=TRICK+STAR+TYPE-E">
				<p><i>●</i>車身防倒球 (TYPE-E)商品頁面</p>
			</a>
			<a href="https://www.webike.tw/parts/ca/1000-1059-1117?q=TRICK+STAR+TYPE-D">
				<p><i>●</i>車身防倒球 (TYPE-D)商品頁面</p>
			</a>


			<div class="list2">
				<a href="https://www.webike.tw/parts/br/785?q=IKAZUCHI+%E6%8E%92%E6%B0%A3%E7%AE%A1">IKAZUCHI 全段/尾段排氣管商品頁面</a>
				<a href="https://www.webike.tw/parts?q=IKAZUCHI%20Universal">IKAZUCHI 通用型排氣管尾段商品頁面</a>
				<a href="https://www.webike.tw/parts/ca/1000-1180-1316-1120?q=TRICK+STAR++%E5%BC%95%E6%93%8E">引擎保護外蓋套件商品頁面</a>
				<a href="https://www.webike.tw/parts/ca/3000?q=TRICK+STAR+%E7%AB%B6%E8%B3%BD%E7%9F%AD%E6%89%8B%E5%A5%97">競賽短版防摔手套商品頁面</a>
				<a href="https://www.webike.tw/parts/ca/3000-3020-3060-3061?q=TRICK+STAR+%E6%89%8B%E5%A5%97">競賽防摔手套商品頁面</a>
				<a href="https://www.webike.tw/parts?q=TRICK%20STAR%20%E7%AB%B6%E8%B3%BD%E6%89%8B%E5%A5%97">競賽防摔手套商品頁面</a>
				<a href="https://www.webike.tw/parts/ca/3000?q=TRICK+STAR+%E7%AB%B6%E8%B3%BD%E7%9F%AD%E6%89%8B%E5%A5%97">競賽短版防摔手套商品頁面</a>
				<a href="https://www.webike.tw/parts?q=TRICK%20STAR%20%E8%AD%B7%E5%BC%93">把手拉桿護弓套件商品頁面</a>
				<a href="https://www.webike.tw/parts?q=TRICK%20STAR%20%E6%B0%B4%E7%AE%A1">鈦合金散熱水管套件商品頁面</a>
				<a href="https://www.webike.tw/parts/ca/1000-1059-1117?q=TRICK+STAR+TYPE-E">車身防倒球 (TYPE-E)商品頁面</a>
				<a href="https://www.webike.tw/parts/ca/1000-1059-1117?q=TRICK+STAR+TYPE-D">車身防倒球 (TYPE-D)商品頁面</a>
			</div>


	</div>
	</div>
		
	</div>
@stop
@section('script')

<script>
	// var list = documaent.querySelector(".list");

jQuery(document).ready(function($) {

    $(window).scroll(function(){

    	// console.log($(window).scrollTop());
    	

    	var top = $('.list').offset().top - $('#mainNav').height();

    	console.log(top);
    	console.log($('.list').offset().top);

    	if($(window).scrollTop() > 200 ){

    		$('.list').addClass('fix');
    	}else {
    		$('.list').removeClass('fix');
    	}
    		
    	
	})

});

</script>
@stop