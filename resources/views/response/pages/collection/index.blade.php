@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/collection.css') !!}">
    <style>
        .form-group .title-container {
            margin-left:15px;
        }
    </style>
@stop
@section('left')
    @include('response.pages.collection.partials.types-menu')
@stop
@section('right')
    <div class="row form-group">
        <div class="block clearfix title-container">
            <div class="title-main-box">
                <h1>流行特輯</h1>
            </div>
        </div>
    @php
        $count = 0;
    @endphp
    @foreach($collection as $key => $assortment)
        @if($count != 0 and $count % 2 == 0)
            </div>
            <div class="row form-group">
        @endif
        <div class="col-xs-12 col-sm-6 col-md-6">
            <a href="{!! $assortment->link !!}" title="{{$assortment->name . $tail}}" {{$assortment->_blank == 1 ? 'target=_blank' : ''}}>
                <figure class="zoom-image thumb-img">
                    <img src="{!! $assortment->banner !!}" alt="{{$assortment->meta_title . $tail}}">
                </figure>
            </a>
            <span class="dotted-text3">{{$assortment->meta_description}}</span>
        </div>
        @php
            $count++;
        @endphp
    @endforeach
    </div>
    <div class="text-center">
        {!! $collection !!}
    </div>

@stop
@section('script')
@stop