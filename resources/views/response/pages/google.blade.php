<!doctype html>
<html>
<head>
    @include ( 'response.layouts.partials.head' )
</head>
<body>

@include ( 'response.layouts.partials.header.index' )
        <!-- Content -->
<section id="contents top-shopping contents-all-page" class="ct-main top-shopping contents-all-page">
    <ul class="menu-left-mobile col-xs-12">
        @include ( 'response.common.hotlinks-top' )
    </ul>
    <div class="container container-top-page">
        <div class="breadcrumb-mobile">
            <ol class="breadcrumb breadcrumb-product-detail">
                {!! $breadcrumbs_html !!}
            </ol>
        </div>
        <div>
            <script>
                (function() {
                    var cx = '006775458616325244567:qeruhud4weg';
                    var gcse = document.createElement('script');
                    gcse.type = 'text/javascript';
                    gcse.async = true;
                    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(gcse, s);
                })();
            </script>
            <gcse:searchresults-only cr="countryTW" resultsUrl="{{route('google')}}"></gcse:searchresults-only>
        </div>
    </div>
</section>
<!--End Content -->
@include ( 'response.layouts.partials.footer.index' )
@include ( 'response.layouts.partials.script' )

</body>
</html>