@extends('response.layouts.1column')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{assetRemote('/css/pages/intructions.css')}}">
	<style>
		@media (max-width: 767px){
			#A .img ul li{
				width:100% !important;
				margin-bottom:10px !important;
			}
		}
		@media (min-width: 768px){
			#A .img ul li{
				width:50% !important;
				margin-bottom:10px !important;
			}
		}
		@media (min-width: 992px){
			#A .img ul li{
				width:25% !important;
			}
		}

		#A .img ul li img {
			width:240px;
			height:180px;
			border: 1px solid #dcdee3;
		}
		#A span {
			font-size:1rem;
		}
		#A .btn-danger {
			width:200px;
			height:40px;
		}
		#A .container-intruction-detail .ul-membership li {
			margin-bottom:0px;
		}
		.info-table {
			padding:0px !important;
		}
		.info-table li {
			margin-bottom:0px !important;
			padding:10px;
			border:1px solid black;
		}
		.history ul {
			list-style-type:none;
		}
		.history ul .text {
			min-height:154px;
			max-height:154px;
			overflow: auto;
		}
		.box-container-intruction .mainbanner {
			border:0px !important;
			margin:0px !important;
			padding:0px !important;
		}
		.box-container-intruction .banner-container {
			margin:0px !important;
		}

	</style>
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li class="banner-container">
                    <div class="title text-center mainbanner">
                        <img src="{{assetRemote('image/ucrr/mainbanner.png')}}" alt="UCRR 大專校際道路機車賽 - 「Webike-摩托百貨」">
                    </div>
                </li>
            </ul>
        </div>
    </div>
	<div class="box-container-intruction" id="A">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>什麼是UCRR?</h2></div>
                	<span>
                		大專校際道路機車賽(University Campus of Road Racing)是由一群機研社出身、同時熱衷於摩托車賽事的大學生舉辦。於2012年開始籌畫，並在隔年(2013)三月於龍潭LTNS舉辦了第一屆的UCRR。
                	</span><br><br>
                	<span>
                		出生於機研社的我們，深知學生參與賽車的辛苦，為了使更多熱愛機車賽事的朋友能夠親身參與其中，我們致力推動參賽成本的降低，體現在UCRR中的是選手村住宿優惠、免費載車服務、校際統規車比賽、高限制的改裝規範、連身皮衣與人身部品優惠；也嘗試增添參賽的樂趣性，如大場地高圈數的比賽、將排位與決賽分為兩天舉行。
                	</span><br><br>
                	<span>
                		一場運動賽事不只需要裁判與選手，每一位車手都需要掌聲與觀眾，這是我們所堅信的。因此成功的賽事除了遠道而來的選手團隊，更需要結合在地的人氣與地氣。
                	</span><br><br>
                	<span>
                		UCRR在賽事中規劃了豐富的周邊活動與表演，並在其中努力結合在地元素，包括鄰近大學社團演出、小吃美食餐車、賽前的當地宣傳、選手之夜及餐會觀眾皆能共襄盛舉。
                	</span><br><br>
                	<span>
                		幾次賽事舉辦的經驗讓我們了解到，一個宗旨與願景不能只是口號、一個夢想若不能踏實經營，靠著廠商幾次的同情贊助也終究無以支撐，唯有努力提升品牌價值與實質回饋，UCRR的初衷才得以永續耕耘。
                	</span><br><br><br>
                    <div class="text-center img">
                    	<ul class="clearfix">
                    		<li class="col-xs-12 col-sm-6 col-md-3">
                        		<img src="{{assetRemote('image/ucrr/event_1.png')}}" alt="UCRR 大專校際道路機車賽 - 「Webike-摩托百貨」">
                        	</li>
                        	<li class="col-xs-12 col-sm-6 col-md-3">
                        		<img src="{{assetRemote('image/ucrr/event_2.jpg')}}" alt="UCRR 大專校際道路機車賽 - 「Webike-摩托百貨」">
                        	</li>
                        	<li class="col-xs-12 col-sm-6 col-md-3">
                        		<img src="{{assetRemote('image/ucrr/event_3.png')}}" alt="UCRR 大專校際道路機車賽 - 「Webike-摩托百貨」">
                        	</li>
                        	<li class="col-xs-12 col-sm-6 col-md-3">
                        		<img src="{{assetRemote('image/ucrr/event_4.png')}}" alt="UCRR 大專校際道路機車賽 - 「Webike-摩托百貨」">
                        	</li>
                        </ul>
                    </div><br>
	            </li>
	        </ul>
	        <h3 class="font-bold">UCRR歷年集錦</h3><br>
	        <div class="history">
		        <ul class="clearfix">
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>2014</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
					                <li>
					                	<span>參賽學校：61</span><br>
					                </li>
					                <li>
					                	<span>參賽人數：144</span><br>
					                </li>
					                <li>
					                	<span>參賽車輛數：206</span><br>
						            </li>
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/2014.png')}}" alt="2014 UCRR - 「Webike-摩托百貨」">
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>2015</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
					                <li>
					                	<span>參賽學校：74</span><br>
					                </li>
					                <li>
					                	<span>參賽人數：182</span><br>
					                </li>
					                <li>
					                	<span>參賽車輛數：240</span><br>
						            </li>
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/2015.png')}}" alt="2015 UCRR - 「Webike-摩托百貨」">
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>2016</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
					                <li>
					                	<span>參賽學校：90</span><br>
					                </li>
					                <li>
					                	<span>參賽人數：272</span><br>
					                </li>
					                <li>
					                	<span>參賽車輛數：454</span><br>
						            </li>
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/2016.png')}}" alt="2016 UCRR - 「Webike-摩托百貨」">
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
				</ul>
			</div>
        </div>
    </div>
    <div class="box-container-intruction" id="A">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>2017 UCRR賽制介紹</h2></div>
                </li>
            </ul>
        	<div class="history">
		        <ul class="clearfix">
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>賽制</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/rule_1.png')}}" alt="UCRR賽制介紹 - 「Webike-摩托百貨」">
						            </li>
						            <li class="text">
						            	<span>今年爭先賽採排位－預賽－決賽制，靜態起跑。各組排位15分鐘，預賽六圈依排名決定決賽出發順序，決賽圈數為10圈。</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>計時賽</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/rule_2.png')}}" alt="UCRR賽制介紹 計時賽 - 「Webike-摩托百貨」">
						            </li>
						            <li class="text">
						            	<span>為讓更多學弟妹可以加入賽車運動，本屆除原本的TimeAttack最速單圈賽（樂趣體驗組）外，新開兩組計時賽組別，DiffAVG最小秒差賽（進階推廣組）與Gymkhana金卡那（空地王者組），裝備、車輛與技術要求比起爭先賽降低許多，外加上獨立頒獎！</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>選手資格</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/rule_3.png')}}" alt="UCRR賽制 選手資格 - 「Webike-摩托百貨」">
						            </li>
						            <li class="text">
						            	<span>現役大專生，或休學一年內者。且於計時賽外任何公開比賽得過前三名者或持有國內任何賽會Ｂ級選手以上者，不得報名Ｄ組。持有國內Ｂ級選手證並於2016年國內任何Ｂ級比賽得過前三名者及曾經持有國內Ａ級選手者不得報名Ｃ組及Ｄ組。</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
				</ul>
			</div>
			<div class="history">
		        <ul class="clearfix">
			        <li class="col-xs-12 col-sm-6 col-md-6">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>2017 夏季賽極限站</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/speedway_1.png')}}" alt="UCRR 夏季賽極限站 - 「Webike-摩托百貨」">
						            </li>
						            <li>
						            	<span>報名方式：符合資格者皆可報名，請填寫<a class="target" href="https://ucrr.net/我要報名?utm_source=webike&amp;utm_medium=button&amp;utm_campaign=webike" target="_blank" nofollow="">報名表</a>報名表</a><br>
						            	報名截止：報名截止日: 2017/05/14<br>
						            	賽時時間：2017/5/27-28<br>
										活動地址：325桃園市龍潭區高楊北路452巷48號<br>
										夏季限定 - 校際爭霸卡丁車賽
						            	</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-6">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>2017 冬季賽台糖站</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/speedway_2.png')}}" alt="UCRR 夏季賽極限站 - 「Webike-摩托百貨」">
						            </li>
						            <li>
						            	<span>報名方式：符合資格者皆可報名，請填寫<a class="target" href="https://ucrr.net/我要報名?utm_source=webike&amp;utm_medium=button&amp;utm_campaign=webike" target="_blank" nofollow="">報名表</a>報名表</a><br>
						            	報名截止：<br>
						            	賽事時間：2017/12/16-17<br>
										活動地址：825高雄市橋頭區橋南路688-1號<br>
										冬季限定 - 全車種4H耐久賽
						            	</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="box-container-intruction" id="A">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>UCRR參賽福利</h2></div>
                </li>
            </ul>
        	<div class="history">
		        <ul class="clearfix">
		        	<li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>R.S.T.Riding Academy 選手培訓計畫</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/benefit_5.png')}}" alt="UCRR R.S.T.Riding Academy 選手培訓計畫 - 「Webike-摩托百貨」">
						            </li>
						            <li class="text">
						            	<span>凡報名17'夏季賽的選手在比賽前都可以免費使用RS Riding Academy的維修空間與機具喔!</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>免費載車服務</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/benefit_2.png')}}" alt="UCRR 免費載車服務 - 「Webike-摩托百貨」">
						            </li>
						            <li class="text">
						            	<span>載一臺賠一臺的免費載車服務，本屆大會依然會繼續載(燒錢)下去，為了就是深根基層賽事，給懷抱賽車夢的各位一個發揮的舞台!</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="title-main-box">
					            <h2>公共意外險</h2>
					        </div>
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/benefit_6.png')}}" alt="公共意外險 - 「Webike-摩托百貨」">
						            </li>
						            <li class="text">
						            	<span>參加UCRR賽事將會替選手投保公共意外險</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="box-container-intruction" id="A">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>Webike挺UCRR</h2></div>
                </li>
            </ul>
        	<div class="history">
		        <ul class="clearfix">
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/site_1.png')}}" alt="攤位活動 - 「Webike-摩托百貨」">
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/site_2.png')}}" alt="賽車展示 - 「Webike-摩托百貨」">
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
			        <li class="col-xs-12 col-sm-6 col-md-4">
				        <div class="box-container-intruction">
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li class="text-center">
						            	<img src="{{assetRemote('image/ucrr/site_3.png')}}" alt="現場徵才 - 「Webike-摩托百貨」">
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
					<li class="col-xs-12 col-sm-12 col-md-12">
				        <div class="box-container-intruction">
					        <div class="container-intruction-detail info-table">
						        <ul class="ul-membership">
						            <li>
						            	<span>「Webike」贊助UCRR大專盃賽車活動，我們藉此拋磚引玉，希望能營造一個健康且正規的賽車環境；未來我們也會持續支持類似的賽事活動，讓更多年輕朋友一同體驗賽車的美好。</span>
						            </li>
						        </ul>
						    </div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
@stop