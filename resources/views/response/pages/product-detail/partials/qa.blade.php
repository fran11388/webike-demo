@if(count($answers))
<div id="customer-qa" class="customer-qa">
    <div class="title-main-box title-detail-info">
        <h2 id="customer-q-a" class="title-content-detail-info">客戶問與答</h2>
    </div>
    <!--
    <div class="title-main2">
        <div class="ct-title-main2">
            <span class="text-ct-title-main2">Get specific details about this product from customrs who own it</span>
            <div class="ct-title-main2-right">

                <label for="">Sort by:</label>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Number of answers
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)">Number of answers 1</a></li>
                        <li><a href="javascript:void(0)">Number of answers 2</a></li>
                        <li><a href="javascript:void(0)">Number of answers 3</a></li>
                    </ul>
                </div>

                <a class="btn btn-asking" href="" target="_blank">商品諮詢</a>
            </div>
        </div>
    </div>
    -->
    <ul class="ul-ct-customer-qa">
        @php
            $qa_num = 0;
        @endphp
        @foreach($answers as $key => $answer)
            @php
                $question = $answer->question;
                $qa_num++;
            @endphp
            <li class="li-ct-customer-qa">
                <!--
                <div class="top-ct-customer-qa clearfix">
                    <div class="top-ct-customer-qa-left question-title">
                        <i class="fa fa-question-circle size-12rem"></i>
                        <a class="size-10rem" href="javascript:void(0)"> {{$qa_num . '.' . $question->content}}</a>
                    </div>

                    <div class="top-ct-customer-qa-right">
                        <a href="javascript:void(0)">
                            <i class="fa fa-thumbs-up"></i> 有幫助 (0)
                        </a>
                    </div>

                </div>
                -->
                <!-- Customer Q & A Show -->
                <div class="customer-qa-show {!! $qa_num > 3 ? 'hide' : '' !!} box">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="ct-customer-qa-show">
                                <h3 class="title"><i class="fa fa-comment-o size-12rem font-color-blue"></i> {!! $qa_num . '.' . $question->content !!}</h3>
                                <div class="ct-customer-qa-show-detail"><i class="fa fa-check size-12rem font-color-green"></i> {!! echoWithLinkTag($answer->content) !!}</div>
                                <div class="like-comment clearfix">
                                    <div class="pull-right">
                                        <a class="btn btn-asking answer-praise" href="javascript:void(0)" data-value="{!! $answer->id !!}">
                                            <i class="fa fa-thumbs-up"></i> 有幫助 <span class="count"></span>
                                        </a>
                                    </div>
                                    <div class="btn-label pull-right">此則問與答是否有幫助到您？ </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
        @if(count($answers) > 3)
            <li>
                <a id="customer-qa-show-all" class="btn-detail-info" href="javascript:void(0)">查看全部<i class="fa fa-caret-right" aria-hidden="true"></i></a>
            </li>
        @endif
    </ul>
    <!--
    <ul class="pagination pagination-sm">
        <li><a href="javascript:void(0)"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
        <li><a href="javascript:void(0)">2</a></li>
        <li><a href="javascript:void(0)">3</a></li>
        <li><a href="javascript:void(0)">4</a></li>
        <li><a class="active" href="javascript:void(0)">5</a></li>
        <li><a href="javascript:void(0)">6</a></li>
        <li><a href="javascript:void(0)">7</a></li>
        <li><a href="javascript:void(0)"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
    </ul>
    <div class="btn-see-all-pagination"><span>1-5 of 109 Total reviews</span><a href="javascript:void(0)">See all <i class="fa fa-caret-right" aria-hidden="true"></i></a></div>
    -->
</div>
@endif