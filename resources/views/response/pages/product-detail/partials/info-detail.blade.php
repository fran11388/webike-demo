<div class="ct-product-info-detail col-xs-12 col-sm-12 col-md-12">
    <div class="row">
        <ul class="menu-info">
            <li>
                <a href="javascript:void(0)" onclick="slipTo('#anchor_detail_information')">
                    <h2>商品詳細敘述</h2>
                </a>
            </li>
            @if (count($product->fitModels))
                <li>
                    <a href="javascript:void(0)" onclick="slipTo('#anchor_fit_model')">
                        <h2>適用車型</h2>
                    </a>
                </li>
            @endif
            <li>
                <a href="javascript:void(0)" onclick="slipTo('#anchor_customer_review')">
                    <h2>商品評論</h2>
                </a>
            </li>
            @if(count($ranking_sales_products) or count($ranking_popularity_products)  )
                <li>
                    <a href="javascript:void(0)" onclick="slipTo('#product_ranking')">
                        <h2>商品排行</h2>
                    </a>
                </li>
            @endif
            @if(count($answers))
            <li>
                <a href="javascript:void(0)" onclick="slipTo('#customer-qa')">
                    <h2>客戶問與答</h2>
                </a>
            </li>
            @endif
            <li>
                <a href="javascript:void(0)" onclick="slipTo('#anchor_recommendation')">
                    <h2>相關推薦商品</h2>
                </a>
            </li>
        </ul>
    </div>


    <div class="top-info-detail">
        <div class="right-box-detail">
            {{--  Explore Related product --}}
            @if(count($populars))
                <div class="title-main-box">
                    <h2>{{$last_category ? $last_category->name : '同分類'}} 銷售排行</h2>
                </div>
                <div class="explore-product">
                    <ul class="ct-explore-product">
                        @foreach ($populars as $popular)
                            <li>
                                @include('response.common.product.i',['product'=>$popular])
                            </li>
                        @endforeach
                    </ul>
                    @if($last_category)
                        <div class="hidden-xs">
                            <a class="btn-customer-review-read-more" href="{!! forceGetSummeryUrl(['category' => $last_category]) !!}">更多 {{$last_category->name}} <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    @endif
                </div>
            @endif
            {{--  Explore Related product --}}
            {{--<ul class="banner-advertisement-right">--}}
                {{--<li>--}}
                    {{--@include('response.common.dfp.ad-300x250')--}}
                {{--</li>--}}
            {{--</ul>--}}
        </div>

        <div class="left-box-detail">
            <!-- box detail info -->
            <div class="box-detail-info">
                <div class="title-main-box title-detail-info">
                    <h2 class="title-content-detail-info" id="anchor_detail_information">商品詳細敘述</h2>
                </div>
                @if( $product->type == Everglory\Constants\ProductType::OUTLET and $outlet = $product->outlet )
                    <div class="container-box">
                        <h3 class="font-bold">【Outlet商品敘述】</h3>
                        <div class="font-color-red font-bold size-10rem">
                            @if($outlet->description)
                                商品狀況：{{ $outlet->description }}<br>
                            @else
                                商品狀況：庫存新品<br>
                            @endif

                            @if(count($product->options))
                                @foreach ($product->options as &$option)
                                    {{ $option->label->name.'：'.$option->name }}<br/>
                                @endforeach
                            @endif
                        </div>
                        <hr>
                    </div>
                @endif
                @if($extraDescriptions = $product->getExtraDescription())
                    @foreach($extraDescriptions as $extraDescription)
                        {!! $extraDescription !!}
                    @endforeach
                    <br>
                @endif

                @include('response.pages.product-detail.partials.info-detail-description')

                @if( $product->type == Everglory\Constants\ProductType::OUTLET )
                    <div class="container-box">
                        <hr>
                        <h3 class="font-bold">【outlet補充說明】</h3>
                        『OUTLET出清商品』為現貨出清商品不提供信用卡分期付款作為結帳。如果您訂單中有選購『OUTLET出清商品』此筆訂單將無法使用信用卡分期付款方式作為結帳，請改用信用卡一次付清或是貨到付款、轉帳匯款等方式結帳。<br>
                        <br>
                        部分『OUTLET出清商品』有商品瑕疵、或是包裝破損、缺件等情況都會以照片和文字敘述說明，可以接受商品狀況再進行訂購。如果對於商品狀況有不清楚的部分，請您在商品諮詢中提出詢問，我們將為您回答。<br>
                        <br>
                        部分『OUTLET出清商品』廠商已經不再生產，也無法提供後續零配件、耗材補充訂購、以及保固等服務。<br>
                        <br>
                        訂購『OUTLET出清商品』如商品本身有瑕疵或是包裝破損缺件無法為您辦理[不良品更換良品]服務。<br>
                        <br>
                        會員在「Webike-摩托百貨」訂購『OUTLET出清商品』 依規定按照結帳金額開立統一發票。<br>
                        <br>
                        『OUTLET出清商品』數量有限，售完為止，如果結帳之後因該項商品已經售完沒有庫存，客服人員將通知您並且取消該項商品。<br>
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                        <a class="btn btn-asking btn-full" href="{{URL::route('customer-service-proposal-create', 'service') . '?skus=' . $product->url_rewrite}}" target="_blank"><i class="fa fa-comments-o size-12rem"></i> 商品諮詢</a>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                        <a class="btn btn-asking btn-full" href="{{URL::route('mitumori') . '?skus=' . $product->url_rewrite}}" target="_blank"><i class="fa fa-comments-o size-12rem"></i> 未登錄商品諮詢</a>
                    </div>
                    <div class="col-xs-12 col-sm-1 col-md-2 col-lg-2">
                    </div>
                </div>
            </div>
            <!-- box fix modol -->

            @if (count($product->fitModels))
                <div class="box-detail-info">

                    <div class="title-main-box title-detail-info">
                        <h2 class="title-content-detail-info" id="anchor_fit_model">適用車型</h2>
                        <!-- <div class="ct-title-main2-right">
                            <a class="btn btn-asking licenses-info" href="javascript:void(0)">什麼是機號?</a>
                        </div> -->
                    </div>
                    <div class="container-box">
                        <ul class="ul-fix-model">
                            @foreach ($product->fitModels->groupBy('maker') as $models)
                                <?php
                                    $first_model = $models->first();
                                    if($first_model->motorManufacturerByName){
                                        $image = $first_model->motorManufacturerByName->image;
                                    }elseif($first_model->style == 'Universal'){
                                        $image = assetRemote('image/productdetail/motor/universal.png');
                                    }elseif($first_model->maker == 'Other' || $first_model->maker == 'Others') {
                                        $image = assetRemote('image/productdetail/motor/other.png');
                                    }else{
                                        $image = '';
                                    }
                                ?>
                                <li class="li-fix-model">
                                    <div class="ct-fix-model">
                                        <div class="ct-fix-model-left">
                                            <div class="brand-logo">
                                                <div class="img-thumbnail thumbnail-logo-brand zoom-image" href="javascript:void(0)">
                                                    <img src="{{ $image }}" alt="{!! $first_model->maker !!}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="ct-fix-model-right">
                                            <ul>
                                                @foreach ($models as $key => $model)
                                                    <li>
                                                        {{ $model->model . ' ' . $model->style }}
                                                    </li>
                                                    @if($loop->iteration == 5 )
                                                        </ul>
                                                        <ul class="show-more-fix-model" style="display: none;">
                                                    @endif
                                                @endforeach
                                            </ul>
                                            @if(count($models) > 5)
                                                <div class="btn-see-all-pagination btn-see-all-pagination-fix-model">
                                                    <a href="javascript:void(0)">顯示全部 <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @if (count($product->motors))
        <div class="box-detail-info">

            <div class="title-main-box title-detail-info">
                <h2 class="title-content-detail-info" id="anchor_fit_model">適用車型改裝及用品</h2>
                <!-- <div class="ct-title-main2-right">
                    <a class="btn btn-asking licenses-info" href="javascript:void(0)">什麼是機號?</a>
                </div> -->
            </div>
            <div class="container-box">
                <ul class="ul-fix-model">
                    @foreach ($product->motors->groupBy('manufacturer_id') as $motors)
                        <?php
                        $first_motor = $motors->first();
                        if($first_motor->manufacturer){
                            $image = $first_motor->manufacturer->image;
                        }
                        ?>
                        <li class="li-fix-model">
                            <div class="motor-link-block">
                                <div class="ct-fix-model-right">
                                    <ul>
                                        <li class="motor-block-title">
                                            <span class="font-bold">{{ $first_motor->manufacturer->name }}</span>
                                        </li>
                                        @foreach ($motors as $key => $motor)
                                            <li class="motor-block">
                                                <a href="{{ \URL::route('summary',['section' => '/mt/'.$motor->url_rewrite]) }}" alt="{{ $motor->name }}" target="_blank">{{ $motor->name }}</a>
                                            </li>
                                            @if($loop->iteration == 5 )
                                    </ul>
                                    <ul class="show-more-fix-model" style="display: none;">
                                        @endif
                                        @endforeach
                                    </ul>
                                    @if(count($motors) > 5)
                                        <div class="btn-see-all-pagination btn-see-all-pagination-fix-model">
                                            <a href="javascript:void(0)">顯示全部 <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
    @endif

    <!-- 商品評論 -->
    <div class="customer-review">
        <div class="title-main-box title-detail-info">
            <h2 class="title-content-detail-info with-tag" id="anchor_customer_review">商品評論</h2>
            <div class="tag">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                        <span id="review-select-text" data-selected-option-value="created_at">時間排序</span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)" class="review-sort-option" data-option-value="created_at">時間排序</a></li>
                        <li><a href="javascript:void(0)" class="review-sort-option" data-option-value="ranking">評價排序</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="customer-review-total">
            <?php $reviews_group = $reviews_info->groupBy('ranking'); ?>
            <ul class="row col-xs-block">
                <li class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                    <div class="ct-review-detail">
                        <h1>{{ number_format($reviews_info->avg('ranking'),1)  }}</h1>
                        <span class="icon-star star-{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT)  }}"></span>
                        <p>{{ count($reviews_info) }} 商品評論</p>
                    </div>
                </li>
                <li class="col-xs-12 col-sm-4 col-md-5 col-lg-3">
                    <div class="ct-totaldetail ">
                        @if(count($reviews_info))
                            <table class="rating-star-table">
                                <tr>
                                    <?php
                                    $_count = (isset($reviews_group[5]) ? count($reviews_group[5]) : 0);
                                    $_percentage = 0;
                                    if (count($reviews_info)) {
                                        $_percentage = round($_count * 100 / count($reviews_info));
                                    }
                                    ?>
                                    <td><p class="rating-star-name">5</p></td>
                                    <td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <p class="rating-star-name"></p>非常好
                                    </td>
                                    <td>
                                        <p class="rating-bar-bg">
                                            <span class="rating-bar-with" style="width:{{ $_percentage }}%;"></span>
                                        </p>
                                    </td>
                                    <td>
                                        <span class="rating-star-number">{{ $_count }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <?php
                                    $_count = (isset($reviews_group[4]) ? count($reviews_group[4]) : 0);
                                    $_percentage = 0;
                                    if (count($reviews_info)) {
                                        $_percentage = round($_count * 100 / count($reviews_info));
                                    }
                                    ?>
                                    <td><p class="rating-star-name">4</p></td>
                                    <td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i>
                                    </td>
                                    <td><p class="rating-star-name"></p>優良</td>
                                    <td><p class="rating-bar-bg"><span class="rating-bar-with"
                                                                       style="width:{{ $_percentage }}%;"></span></p></td>
                                    <td><span class="rating-star-number">{{ $_count }}</span></td>
                                </tr>
                                <tr>
                                    <?php
                                    $_count = (isset($reviews_group[3]) ? count($reviews_group[3]) : 0);
                                    $_percentage = 0;
                                    if (count($reviews_info)) {
                                        $_percentage = round($_count * 100 / count($reviews_info));
                                    }
                                    ?>
                                    <td><p class="rating-star-name">3</p></td>
                                    <td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i>
                                    </td>
                                    <td><p class="rating-star-name"></p>普通</td>
                                    <td><p class="rating-bar-bg"><span class="rating-bar-with"
                                                                       style="width:{{ $_percentage }}%;"></span></p></td>
                                    <td><span class="rating-star-number">{{ $_count }}</span></td>
                                </tr>
                                <tr>
                                    <?php
                                    $_count = (isset($reviews_group[2]) ? count($reviews_group[2]) : 0);
                                    $_percentage = 0;
                                    if (count($reviews_info)) {
                                        $_percentage = round($_count * 100 / count($reviews_info));
                                    }
                                    ?>
                                    <td><p class="rating-star-name">2</p></td>
                                    <td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i>
                                    </td>
                                    <td><p class="rating-star-name"></p>不佳</td>
                                    <td><p class="rating-bar-bg"><span class="rating-bar-with"
                                                                       style="width:{{ $_percentage }}%;"></span></p></td>
                                    <td><span class="rating-star-number">{{ $_count }}</span></td>
                                </tr>
                                <tr>
                                    <?php
                                    $_count = (isset($reviews_group[1]) ? count($reviews_group[1]) : 0);
                                    $_percentage = 0;
                                    if (count($reviews_info)) {
                                        $_percentage = round($_count * 100 / count($reviews_info));
                                    }
                                    ?>
                                    <td><p class="rating-star-name">1</p></td>
                                    <td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i>
                                    </td>
                                    <td><p class="rating-star-name"></p>糟糕</td>
                                    <td><p class="rating-bar-bg"><span class="rating-bar-with"
                                                                       style="width:{{ $_percentage }}%;"></span></p></td>
                                    <td><span class="rating-star-number">{{ $_count }}</span></td>
                                </tr>
                            </table>
                        @else
                            <span>此商品沒有商品評論。</span>
                        @endif
                    </div>
                </li>
                <li class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    <a class="btn btn-warning btn-full" href="{{route('review-create',$product->sku )}}" target="_blank">撰寫評論 <i class="fa fa-edit size-12rem"></i></a>
                </li>
            </ul>
        </div>
        @if(count($reviews_info))
            <ul class="ul-content-comment-detail" id="review-list">
                @include('response.pages.product-detail.partials.review')
            </ul>
            <ul class="pagination pagination-sm" id="review-pagination">
                <li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
                <li><a class="active" href="#">1</a></li>
                @for($i = 2; $i <= ceil(count($reviews_info)/$review_per_page) ; $i++)
                    <li><a href="#">{{$i}}</a></li>
                @endfor
                <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
            </ul>
            <div class="btn-see-all-pagination">
                <span>{{count($reviews_info)}} 則 評論</span>
                <a href="{{  route('review-search',['ca'=> null , 'sku'=>$product->url_rewrite]) }}">
                    查看全部
                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                </a>
            </div>
        @endif
    </div>
    <!-- end 商品評論 -->
    <!-- customer-q&a -->
    @include('response.pages.product-detail.partials.qa')
    <!--end customer-q&a -->
</div>