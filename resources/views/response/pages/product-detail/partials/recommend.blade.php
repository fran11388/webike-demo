<div class="ct-below-shopping col-xs-12 col-sm-12 col-md-12 col-lg-12" id="anchor_recommendation">
    @if(count($recommends))
    <div class="row box-content custom-parts">
        <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>推薦商品</h2>
        </div>
        <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="owl-carousel3">
                @foreach ($recommends as $product)
                    @include('response.common.product.c')
                @endforeach
            </div>
        </div>
    </div>
    @endif
    @if($last_category)
        <div class="hidden-lg hidden-md hidden-sm">
            <a class="btn btn-warning btn-full" href="{!! forceGetSummeryUrl(['category' => $last_category]) !!}">更多 {{$last_category->name}} <i class="fa fa-caret-right" aria-hidden="true"></i></a>
        </div>
    @endif
</div>

