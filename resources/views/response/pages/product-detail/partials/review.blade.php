@foreach ($reviews_info->forPage($reviews_page,$review_per_page) as $review)
    <li class="content-comment-detail row">
        <div class="ct-comment-detail-right col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="thumb-product-comment">
                <a class="thumbnail-all" href="{{route('review-show',$review->id)}}">
                    <img src="{!! $review->photo_key ? '//img.webike.tw/review/'.$review->photo_key : $review->product_thumbnail !!}"
                         alt="{!! $review->product_name !!} {!!  $review->product_manufacturer !!}"/>
                </a>
            </div>
            <div class="ct-comment-detail">
                <h2>
                    <a href="{{route('review-show',$review->id)}}">{{ $review->title }}</a>
                </h2>
                <p class="date-comment">{{ date("Y/m/d", strtotime($review->created_at)) }}</p>
                <div class="group-star">
                    <span class="icon-star star-0{{$review->ranking}}"></span>
                    <div class="by-name-user">by
                        <span>{{ $review->nick_name }}</span>
                    </div>
                </div>
                            <span class="text-ct-comment">
                            {{ $review->content }}
                        </span>
                {{--
                                            <div class="like-comment col-xs-12">
                                                <strong>Was this review helpful? </strong>
                                                <div class="btn-like">
                                                    <a href="">
                                                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                        Yes
                                                    </a>
                                                    <span>(150)</span>
                                                </div>
                                                <div class="btn-like">
                                                    <a href="">
                                                        <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                        No
                                                    </a>
                                                    <span>(150)</span>
                                                </div>
                                            </div>
                --}}
            </div>
        </div>
    </li>
@endforeach
