<!-- fix-Model -->
@if ($product->fitModels and count($product->fitModels))
    <div class="fix-Model content-last-block">
        <div class="title-model">
            <h3>適用車型:</h3>
        </div>
        @foreach ($product->fitModels->groupBy('maker')->take(3) as $models)
            <?php
                $first_model = $models->first();
                if($first_model->motorManufacturerByName){
                    $image = $first_model->motorManufacturerByName->image;
                }elseif($first_model->style == 'Universal'){
                    $image = assetRemote('image/productdetail/motor/universal.png');
                }elseif($first_model->maker == 'Other' || $first_model->maker == 'Others') {
                    $image = assetRemote('image/productdetail/motor/other.png');
                }else{
                    $image = '';
                }
            ?>
            <div class="img-thumbnail-model">
                <a class="img-thumbnail thumbnail-fix-model" href="#">
                    <img src="{{ $image }}" alt="{!! $first_model->maker !!}">
                </a>
            </div>

            <span>
                        @foreach ($models->take(3) as $model)
                    {{ $model->model . ' ' . $model->style }} <br>
                @endforeach

                    </span>
            <div style="clear:both;"></div>

        @endforeach

        <a href="javascript:void(0)" class="btn-detail-info" onclick="slipTo('#anchor_fit_model')">
            查看更多
            <i class="fa fa-caret-right" aria-hidden="true"></i>
        </a>
    </div>
@endif
<!-- fix-Model -->