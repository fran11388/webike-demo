<hr>
<div class="select-info">
    <form method="post" action="{{ route('cart-update') }}" id="acb">
    <ul class="product-data-table">
        <li class="product-table-content">
            @if( $product->group_code )
                <?php
                $selects = $product->getGroupSelectsAndOptions();
                ?>
                @foreach ($selects as $select )
                    <ul class="product-table-row col-xs-block row">
                        <li class="col-md-3 col-sm-4 col-xs-12">
                            <cite class="font-color-red">選擇商品形式：</cite>
                        </li>
                        <li class="col-md-9 col-sm-8 col-xs-12 loading-nail">
                            @include('response.common.loading.xs')
                            <div id="product-options" class="dropdown">
                                <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown">
                                    <span class="product-select-text" data-selected-option-index="">{{ $select->name }}</span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach ($select->options as $option)
                                        <li>
                                            <a href="javascript:void(0)" class="product-select-option"
                                               data-product-sku="{{ isset($option->product_sku) ? $option->product_sku : '' }}"
                                               data-option-value="{{ $option->name }}"
                                               data-option-index="{{ $loop->index }}">
                                                {{ $option->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    </ul>
                @endforeach
            @endif

            {{-- <ul class="product-table-row col-xs-block row">
                <li class="col-md-3 col-sm-4 col-xs-12">
                    <cite>商品編號：</cite>
                </li>
                <li class="col-md-9 col-sm-8 col-xs-12">
                    <div id="model-number">{{ $product->model_number }}</div>
                </li>
            </ul> --}}
        </li>
    </ul>
    @if(in_array($product->type,[Everglory\Constants\ProductType::NORMAL,Everglory\Constants\ProductType::OUTLET]))
        <div class="clearfix block" id="estimate_tr">
            <div class="estimate_request">
                @if($product->p_country == 'J')
                    <div class="goldenweek-position-pc-sd">
                        @include('response.pages.product-detail.partials.goldenweek-tag')
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($product->group_code)
                            <p class="delivery-box delivery-message">請先選擇商品形式。</p>
                        @else
                            <p class="delivery-box delivery-message">查詢中...</p>
                        @endif
                    </div>
                </div>
                <label class="estimate_request tip content-last-box hide">★加入購物車下方有"預先確認交期"功能，若有需要您可以在購買前先行查詢交期狀態。</label>
            </div>
        </div>
    @endif
    <div class="clearfix select-address {{$customer_address ? 'active' : ''}} hide">
        <div class="select-city">
            <div>
                <select class="select2 select-country  btn-label" >
                    <option value="">請選擇縣市</option>
                    @foreach ($shippingCountry as $country)
                        <option value="{{$country->id}}" {{($customer_address and ($customer_address->county == $country->name)) ? 'selected="selected"': ' '}}>{{$country->name}}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <select class="select2 select-area btn-label " >
                    <option value="">請選擇地區</option>
                    @if(isset($country_area))
                        @foreach ($country_area as $area)
                            <option value="{{$area->shipping_day}}" {{($customer_address and ($customer_address->district == $area->area)) ? 'selected="selected"': ' '}}>{{$area->area}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="address-masage">
            <span class="vertical-middle" style="height:38px;"></span>
            <span class="masage-text"> << 您可在此選擇居住地區，以確認收到商品包裹之日期</span>
        </div>
    </div>
    @include('response.pages.product-detail.partials.product-relieved-shopping')
    <hr style="margin: 20px 0">
    @if( $product->checkPointDiscount($current_customer) )
        <div class="point_discount">
            <div class="title clearfix">
                @if(activeValidate('2018-04-01 00:00:00','2018-05-01'))
                    <span class="font-bold size-12rem">4月專屬，POINT點數現折活動!</span>
                @else
                    <span class="font-bold size-12rem"><span class="hidden-xs">愛用國貨、</span>POINT點數現折活動!</span>
                @endif
            </div>
            <div class="info clearfix">
                <p class="font-bold font-color-red col-sm-block col-xs-block size-10rem">購買立即折抵{!! number_format($product->getFinalPoint($current_customer),0) !!}元</p>
                <a class="btn btn-info col-sm-block col-xs-block" href="{{URL::route('benefit-sale-pointinfo')}}" title="點數現折商品活動說明 - 「Webike-摩托百貨」" target="_blank">活動說明
                </a>
            </div>
        </div>
    @endif
    <div class="group-button-detail">
        <input type="hidden" name="sku" id="sku" value="{{ $product->group_code ? '' : $product->sku }}"/>
        <div class="form-group-button">
            <div class="col-xs-12 col-sm-4 col-md-4 determine-number flex-box">
                <label class="btn-label label-large text-nowrap">數量</label>
                <ul class="btn btn-large">
                    <li {{ $product->type == \Everglory\Constants\ProductType::GROUPBUY ? '' : 'onclick=minusQty()' }}><a href="javascript:void(0)" class="count-button minusQty">-</a></li>
                    <li><input type="number" name="qty" id="qty" value="1" min="1" readonly></li>
                    <li {{ $product->type == \Everglory\Constants\ProductType::GROUPBUY ? '' : 'onclick=addQty()' }}><a href="javascript:void(0)" class="count-button addQty" >+</a></li>
                    <li style="height:100%"></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 btn-addcart">
                <a id="add_to_cart" class="btn btn-large bg-color-red size-10rem buy_btn submit-disabled loading-stock disabled" href="javascript:void(0)"><i class="fa fa-shopping-cart size-12rem"></i> 加入購物車</a>
            </div>
            @if($product->type == \Everglory\Constants\ProductType::NORMAL)
                <div class="col-xs-6 col-sm-4 col-md-4 btn-addtolist">
                    <a id="add_to_wishlist" class="btn size-10rem base-btn-gray buy_btn submit-disabled loading-stock" href="javascript:void(0)">加入待購清單</a>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 btn-purchase">
                    <a id="groupbuy-product" class="btn size-10rem base-btn-gray buy_btn submit-disabled loading-stock" href="javascript:void(0)" ><span class="hidden-xs hidden-sm">購買</span>10件以上優惠</a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <a class="btn btn-asking btn-full estimate_request disabled" href="javascript:void(0)" title="您可以在購買前查詢庫存交期。" onclick="submitForm(this,'{{ route('customer-estimate-update') }}')"><i class="fa fa-envelope hidden-xs hidden-sm"></i> 預先確認交期</a>
                </div>
            @elseif($product->type == \Everglory\Constants\ProductType::OUTLET)
                <div class="col-xs-6 col-sm-6 col-md-6 btn-addtolist">
                    <a id="add_to_wishlist" class="btn size-10rem base-btn-gray buy_btn submit-disabled loading-stock" href="javascript:void(0)">加入待購清單</a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <a class="btn btn-asking btn-full estimate_request disabled" href="javascript:void(0)" title="您可以在購買前查詢庫存交期。" onclick="submitForm(this,'{{ route('customer-estimate-update') }}')"><i class="fa fa-envelope hidden-xs hidden-sm"></i> 預先確認交期</a>
                </div>
            @elseif(in_array($product->type,[\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED]))
                <div class="col-xs-6 col-sm-6 col-md-6 btn-addtolist">
                    <a id="add_to_wishlist" class="btn size-10rem base-btn-gray buy_btn submit-disabled loading-stock" href="javascript:void(0)">加入待購清單</a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 btn-purchase">
                    <a id="groupbuy-product" class="btn size-10rem base-btn-gray buy_btn submit-disabled loading-stock" href="javascript:void(0)" ><span class="hidden-xs hidden-sm">購買</span>10件以上優惠</a>
                </div>
            @else
                <div class="col-xs-12 col-sm-12 col-md-12 btn-addtolist">
                    <a id="add_to_wishlist" class="btn size-10rem base-btn-gray buy_btn submit-disabled loading-stock" href="javascript:void(0)">加入待購清單</a>
                </div>
            @endif

            <div class="fixclear"></div>
        </div>
    @if(count($product->getInstallments($current_customer)))
        <div class="hide" id="installment-table">
            <h3>
                @if($current_customer and $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE)
                    此項商品提供經銷商分期服務(支持13大發卡銀行，請參閱"<a href="{!! route( 'service-dealer-installment-info' ) !!}" title="經銷商信用卡分期服務{!! $tail !!}" target="_blank">經銷商信用卡分期服務</a>")。
                @else
                    信用卡分期(支持13大發卡銀行，請參閱"<a href="{!! route('customer-rule', 'member_rule_2') !!}#E_b" title="信用卡分期說明{!! $tail !!}" target="_blank">信用卡分期說明</a>")。
                @endif
            </h3>
            @include('response.common.installments-table')
        </div>
    @endif
    

</div>
</form>
</div>