<div class="ct-below-shopping col-xs-12 col-sm-12 col-md-12 col-lg-12" id="anchor_also_buy">
    @if(count($alsoBuyProducts))
        <div class="row box-content custom-parts">
            <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>其他人也購買了</h2>
            </div>
            <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="owl-carousel3">
                    @foreach ($alsoBuyProducts as $product)
                        @include('response.common.product.c')
                    @endforeach
                </div>
            </div>
        </div>
    @endif
</div>

