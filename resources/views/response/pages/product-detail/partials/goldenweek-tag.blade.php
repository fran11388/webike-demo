@if(activeValidate('2019-04-01','2019-05-01'))
<style>

	.goldenweek-position-mb-sd{
		position: relative;
	    height: 40px;
	    width: 100%;
	}

	.goldenweek-position-pc-sd{
		position: relative;
	    height: 40px;
	    width: 100%;
	}

	.goldenweek-position-genuineparts{
		position: relative;
	    height: 40px;
	    margin: 60px 0 0 0;
	    width: 60%;
	}

	.goldenweek-position{
    	position: relative;
    	height: 30px;
    	width: 80%;
	}

	.estimate_request{
		position: relative;
		margin-top: 10px; 
	}

	.goldenweek-tag{
		position: absolute;
		width: 210px;
		top: -10px;
		right: 5px;
		margin: 0 !important;
	}

	.goldenweek-content{
		/*border: solid 1px black;*/
		background: #f8b739;
		border-radius: 50px;
		height: 30px;
		width: 210px;
		position: absolute;
		/*position: relative;*/
		top: 0px;
		right: 0px;
		padding: 7px;
		z-index: 3;
		overflow: hidden;
		text-decoration: none !important;
	}

	.goldenweek-content p{
		text-align: center;
		z-index: 20;
	}

	.goldenweek-content:after {
		background: #fff;
		content: "";
		height: 130px;
		left: -75px;
		opacity: .8;
		position: absolute;
		top: -50px;
		-webkit-transform: rotate(35deg);
		transform: rotate(35deg);
		transition: all 550ms cubic-bezier(0.19, 1, 0.22, 1);
		width: 50px;
		z-index: -10;
	}

	.goldenweek-content.open:after{
		left: 120%;
		transition: all 550ms cubic-bezier(0.19, 1, 0.22, 1);
	}

	.goldenweek-content:hover:after {
		left: 120%;
		transition: all 550ms cubic-bezier(0.19, 1, 0.22, 1);
	}

	.goldenweek-content p:hover: {
		color: red;
	}

	.goldenweek-arrow{
		width: 0;
		height: 0;
		border-style: solid;
		border-width: 15px 15px 0 15px;
		border-color: #f8b739 transparent transparent transparent;
		position: absolute;
		top: 30px;
		right: 42%;
		z-index: 3;
		margin: 0 !important;
	}

</style>
<div class="goldenweek-tag">
	<a href="javascript:void(0)" class="goldenweek-content" >
		<p>黃金周即將到來，早買早享受!!</p>
	</a>
	<div class="goldenweek-arrow"></div>
</div>

<script type="text/javascript">

	$('.goldenweek-content').click(function(){
		swal({
			html: '<div>'+
			'<br>'+
			'<div style="text-align: left; font-size: 1.2rem; text-align: justify;">'+
				'<span>'+
					'2019 年日本黃金周連續假期即將到來，4/27(六) ~ 5/7(二)廠商將會有7~10天的假期。想在黃金週連假前收到商品的會員，趕快手刀下單吧！在4/24(三)前購買有標示「Webike在庫」商品，就可在4/29(一)收到商品！<br>'+
				'</span>'+
			'</div>'+
		'</div>',
		width: 300,
		})	
	});

	
</script>

@endif