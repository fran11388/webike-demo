<div class="module-title">
    <div class="brand-logo">
        <a class="img-thumbnail thumbnail-logo-brand" href="{{ URL::route('summary', 'br/' . $product->manufacturer->url_rewrite) }}" title="{{$product->manufacturer->name . $tail}}">
            <img src="{{ $product->manufacturer->image }}" alt="{{$product->manufacturer->name . $tail}}"/>
        </a>
    </div>
    <div class="product-info">
        <h2>
            <span class="name-brand">
                <a href="{{ URL::route('summary', 'br/' . $product->manufacturer->url_rewrite) }}" title="{{$product->manufacturer->name . $tail}}">
                    {{ $product->manufacturer->name }}
                </a>
            </span>
        </h2>
        <h1>{{ $product->name }}</h1>
        <ul class="product-table-row2 col-xs-block">
            <li class="product-table-row2-number">
                <cite>商品編號：</cite>
            </li>
            <li class="product-table-row2-number-title">
                <div id="model-number">{{ $product->model_number }}</div>
            </li>
        </ul>
        <ul class="info-brand">
{{--             <li>
                <span class="name-brand">
                    品牌:
                    <a href="{{ URL::route('summary', 'br/' . $product->manufacturer->url_rewrite) }}" title="{{$product->manufacturer->name . $tail}}">
                        {{ $product->manufacturer->name }}
                    </a>
                </span>
            </li> --}}
            <li>
                <span class="icon-star star-{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT)}}"></span>
            </li>
            <li>
                <a class="review" href="{{ URL::route('review-search', '') }}?sku={{$product->url_rewrite}}">共{{ count($reviews_info) }}則評論</a>
            </li>
            <!--
            <li>
                <a class="qa" href="#">問與答</a>
            </li>
            -->
            <li>
                <div class="tags clearfix" id="tag_area">
            </li>
            
        </ul>
    </div>
</div>