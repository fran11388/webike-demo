
<div class="ct-product-deatil col-xs-12 col-sm-12 col-md-12">
    <div class="ct-product-deatil-left">
        <ul id="imageGallery">
            @if($images = $product->images and count($images))
                @foreach ($images as $image)
                    <li class="img-thumbnail thumbnail-img-product img-zoom-in" data-thumb="{{ cdnTransform($image->thumbnail) }}" data-src="{{ cdnTransform($image->image) }}">
                        <img src="{{ cdnTransform($image->image) }}" alt="{{$product->full_name . $tail}}" title="{{$product->full_name . $tail}}" />
                    </li>
                @endforeach
            @else
                <li class="img-thumbnail thumbnail-img-product">
                    <img src="{{cdnTransform(NO_IMAGE)}}" alt="{{$product->full_name . $tail}}" title="{{$product->full_name . $tail}}"/>
                </li>
            @endif
        </ul>
    </div>
    <div class="ct-product-deatil-right">
        @include('response.pages.product-detail.partials.info')
        <h1 class="price-product-deatil">
            @if($product->type == Everglory\Constants\ProductType::OUTLET)
                出清價：
            @elseif($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]))
                經銷價：
            @else
                會員價：
            @endif
            <strong>{!! formatPrice( 'final-price' , $product->getFinalPrice($current_customer)) !!}</strong>
            @if($product->getDifferencePrice($current_customer))
                <span class="note">(上月會員價{!! formatPrice( 'final-price' , $product->getDifferencePrice($current_customer)) !!}，現省{!! formatPrice( 'final-price' , $product->getDifferencePrice($current_customer) - $product->getFinalPrice($current_customer) ) !!})</span>
            @endif
        </h1>
        @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]) and !($product->type == Everglory\Constants\ProductType::OUTLET))
            <div class="btn-gap-top">
                <span>
                    會員價：
                    <span class="hidden-lg hidden-md hidden-sm"><br></span>
                    <s>{!! formatPrice( 'normal-price' , $product->getNormalPrice()) !!}</s>
                </span>
            </div>
        @endif
        <div class="ct-price-old row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span class="font-color-blue">
                    @if($product->checkPointDiscount($current_customer))
                        <span>點數現折</span>：
                        <span class="hidden-lg hidden-md hidden-sm"><br></span>
                        <span id="final-point">{!! number_format($product->getFinalPoint($current_customer),0) !!}</span>元
                    @else
                        <span class="hidden-xs">回饋</span>點數：
                        <span class="hidden-lg hidden-md hidden-sm"><br></span>
                        <span id="final-point">{!! number_format($product->getFinalPoint($current_customer),0) !!}</span>點
                    @endif

                </span>
            </div>
<!--             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <span>
                    定價：
                    <span class="hidden-lg hidden-md hidden-sm"><br></span>
                    <s>{!! formatPrice( 'base-price' , $product->getPrice()) !!}</s>
                </span>
            </div> -->
        </div>
        <div class="tags clearfix box" id="tag_area">
   </div>


        @include('response.pages.product-detail.partials.select-info')

        @include('response.pages.product-detail.partials.fix-model')




        <!-- products points -->
        <?php

        $summary = '';
        if($product->productDescription and
            $product->productDescription->status_id <> 4 and //4 >> no API data
            $product->productDescription->summary_trans_flg >= 8 // > 8 >> translated
        ){
            $summary = $product->productDescription->summary;
        }
        $summary = str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $summary);

        //警語
        $summary = $warning_text . $summary;

        ?>
        @if ($summary)
            <div class="fix-Model content-last-block">
                <div class="title-model">
                    <h3>商品特點:</h3>
                </div>
                <span>
                    @if($extraDescriptions = $product->getExtraDescription())
                        @foreach($extraDescriptions as $extraDescription)
                            {!! $extraDescription !!}
                        @endforeach
                        <br>
                    @endif
                    {!!  nl2br($summary) !!}
                </span>
                <a href="javascript:void(0)" class="btn-detail-info" onclick="slipTo('#anchor_detail_information')">
                    查看更多
                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                </a>
            </div>
        @endif
    <!-- products points -->

    </div>
</div>