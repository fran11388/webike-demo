@extends('response.layouts.1column')
@section('style')
    <!-- <link rel="stylesheet" type="text/css"  href="{!! assetRemote('plugin/jquery-bxslider/jquery.bxslider.min.css') !!}"> -->
    <link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.12/jquery.bxslider.min.css">
    <!-- <link rel="stylesheet" type="text/css"  href="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe.css') !!}"> -->
    <link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.min.css">
    <!-- <link rel="stylesheet" type="text/css"  href="{!! assetRemote('plugin/PhotoSwipe-4.1.2/default-skin/default-skin.css') !!}"> -->
    <link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.min.css">
    <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/product-relieved-shopping.css') !!}">
    <!-- <link type="text/css" rel="stylesheet" href="{!! asset('plugin/lightslider/src/css/lightslider.css') !!}" /> -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css"/>
    <!-- <link type="text/css" rel="stylesheet" href="{!! asset('plugin/lightGallery/src/css/lightgallery.css') !!}" /> -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css"/>

    <style type="text/css">
        .goog-te-gadget-link{
            display: none;
        }
        #main-trans-btn{
            margin-left: 10px;
        }
        .loading-nail{
            position: relative;
        }
        .loading-box{
            position: absolute;
            top: 0;
            right: 0;
            z-index: 9999;
            padding:0 3% 0 0;
            display: none;
        }
        .dropdown-toggle.has-error{
            border-color: #e61e25;
            font-weight: bold;
        }
        .ct-product-deatil .point_discount {
            margin:0px 0px 20px;
        }
        .ct-product-deatil .point_discount .title {
            padding:7px 15px;
            background-color:#e61e25;
        }
        .ct-product-deatil .point_discount .info{
            width:100%;
            border:1px solid red;
            padding:10px;
        }
        .ct-product-deatil .point_discount p{
            float: left;
            letter-spacing: 2px;
            line-height: 34px;
        }
        .ct-product-deatil .point_discount span {
            color:white;
        }
        .ct-product-deatil .point_discount .btn-info{
            float: right;
        }
        .bx-wrapper{
            margin-bottom: 10px;
        }
        .bx-wrapper .bx-viewport{
            height: auto !important;
            margin: 0 auto;

        }
        #estimate_tr .delivery-message{
            padding: 5px 10px;
            line-height: 28px;
        }
        .ct-product-deatil span.note{
            font-size: .85rem !important;
            color: #333;
        }
        .ct-product-deatil span.note span{
            font-size: .85rem !important;
            color: #333;
        }
        #installment-table ul.history-info-table > li > ul > li[class^="col-"]{
            margin: 0;
            font-size: .85rem;
            min-height: 30px;
        }
        #installment-table ul.history-info-table li.history-table-title ul{
            background-color: #acacac;
        }
        #installment-table ul.history-info-table li.history-table-content ul:nth-child(2n){
            background-color: #efefef;
        }
        .motor-link-block {
            background: #f5f5f5;
            margin: 0;
            padding: 5px;
            width: 100%;
            overflow: hidden;
            float: left;
        }
        .motor-block{
            border:0px !important;
            padding:5px 0px 0px 0px !important;
        }
        .motor-block-title {
            border-bottom: 1px solid #ccc !important;
            padding:5px 0px !important;
        }
        .select-info .select-address div{
            float: left;
        }
        .select-info .select-address .address-masage{
            margin-left: 30px;
            font-size: 1rem;            
        }
        .select-address .select-city {
            width: 35%;
        }
        .select-address .select-city div{
            width:50%;
        }
        .select-address .select-city div span.select2-container{
            width:100%!important;
        }
        table{
            border:1px solid #777;
        }
        table > tr,td {
            border:1px solid #777;
        }
        /*
.bx-wrapper .bx-viewport ul.bxslider{
    display: flex;
}
.bx-wrapper .bx-viewport ul.bxslider li{
    display: flex;
    justify-content: center;
    flex-direction: column;
}
        */
        @media (max-width: 781px) {
            .ct-product-deatil .point_discount p{
                float: none;
                letter-spacing: 2px;
                padding-top:10px;
                font-size:1rem !important;
                margin-left: auto;
                margin-right: auto;
                margin-bottom:10px;
                text-align:center;
            }
            .ct-product-deatil .point_discount .btn-info{
                float: none;
                margin-left: auto;
                margin-right: auto;
            }

            iframe {
                width:100%;
                height:auto;
            }
        }
        @media (max-width: 781px) {
            .bx-custom-pager .owl-prev, .bx-custom-pager .owl-next
        }
        .estimate_time {
            color: #999999;
            font-weight: normal;
        }
    </style>
@stop
@section('script')
    <!-- <script async src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe-ui-default.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe-ui-default.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/elevatezoom-master/jquery.elevateZoom-3.0.8.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/elevatezoom/3.0.8/jquery.elevatezoom.min.js"></script>

    <!-- <script src="{!! assetRemote('plugin/lightslider/src/js/lightslider.js') !!}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <!-- <script src="{!! assetRemote('plugin/lightGallery/src/js/lightgallery.min.js') !!}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/lightGallery/src/js/lg-zoom.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/lg-zoom/1.1.0/lg-zoom.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/lightGallery/src/js/lg-thumbnail.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/lg-thumbnail/1.1.0/lg-thumbnail.min.js"></script>
    <script src="{!! assetRemote('js/basic/select2-size-reset.js') !!}"></script>


    <script src="//translate.google.com/translate_a/element.js?cb=googleSectionalElementInit&ug=section&hl=zh-TW"></script>
    <script src="{!! assetRemote('plugin/google/translator.js') !!}"></script>

    @include('response.pages.product-detail.partials.page-scripts')

    <script type="text/javascript">
        /*
        _paq.push(['setEcommerceView', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$category_names) !!}"],{!! round($product->getFinalPrice($current_customer)) !!}]);
        */
    </script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Product",
        "name": "{{$product->name}}",
        "image": "{{$product->getThumbnail()}}",
        "url": "{{URL::route('product-detail', $product->url_rewrite)}}",
        "description": "
            @if( $product->type == Everglory\Constants\ProductType::OUTLET and $outlet = $product->outlet )
                @if($outlet->description)
                    商品狀況：{{ strip_tags($outlet->description) }}。
                @else
                    商品狀況：庫存新品。
                @endif
                @if(count($product->options))
                    @foreach ($product->options as &$option)
                        {{ $option->label->name.'：'.$option->name }}
                    @endforeach
                @endif
            @endif
            @if($product->productDescription)
                {{ str_replace('/catalogue/', 'http://www.webike.net/catalogue/', strip_tags($product->productDescription->summary)) }}
                {{ str_replace('/catalogue/', 'http://www.webike.net/catalogue/', strip_tags($product->productDescription->sentence)) }}
            @endif
        ",
        "mpn": "{{$product->model_number}}",
        "brand": {
            "@type": "Thing",
            "name": "{{$product->manufacturer->name}}"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT) }}",
            @if(count($reviews_info))
                "bestRating": "{{ str_pad(round($reviews_info->max('ranking')), 2,"0" , STR_PAD_LEFT) }}",
                "worstRating": "{{ str_pad(round($reviews_info->min('ranking')), 2,"0" , STR_PAD_LEFT) }}",
            @else
                "bestRating": "0",
                "worstRating": "0",
            @endif
            "reviewCount": "{{ count($reviews_info) }}"

        },
        "offers": {
            "@type": "Offer",
            "priceCurrency": "TWD",
            "price": "{!! $product->getNormalPrice() !!}",
            "priceValidUntil": "{!! date('Y-m-d') !!}",
            "itemCondition": "http://schema.org/UsedCondition",
            "availability": "http://schema.org/InStock",
            "seller": {
                "@type": "Organization",
                "name": "「Webike-摩托百貨」"
            }
        }
    }
    </script>
@stop
@section('middle')

    <div class="product-detail col-xs-12 col-sm-12 col-md-12">
        
        {{-- @include('response.pages.product-detail.partials.info') --}}

        @include('response.pages.product-detail.partials.detail')
        <!-- content info detail -->
        @include('response.pages.product-detail.partials.info-detail')
        <!--end content info detail -->

        {{--@include('response.pages.product-detail.partials.customer-also-view')--}}
        @include('response.pages.product-detail.partials.ranking')

        <div class="also-buy" style="display: none"></div>
        
        @include('response.pages.product-detail.partials.recommend')

        @include('response.common.ad.banner-small')

        <!-- Customer view -->
        <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('response.common.loading.md')
        </div>
        <!-- Customer view end-->

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-advertisement">
            @include('response.common.dfp.ad-728x90')
        </div>
    </div>
    @include('response.common.photoswipe-default')
@stop

