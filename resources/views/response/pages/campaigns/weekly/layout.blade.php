@extends('response.layouts.1column-2019weekly')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/benefit/sale/2019-weekly-promotion.css') }}">
<link rel="stylesheet" href="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.css') }}">
<style type="text/css">
.promotion-container .page-block .page-block-title{
	font-size: 34px;
	text-align: center;
	background: rgb( {!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!});/*區塊title 背景，同主題色*/
	color: {!! $main_color_text !!};/*區塊title 文字顏色，同主題文字色*/
	margin: 0 auto;
	padding: 20px 0;
}
.promotion-container .page-block .page-block-product{
	width: 100%;
	margin: 0 auto;
	padding: 20px 0;
	border: 1px solid;
    background: #fff;
}
.promotion-container .promotion-btn{
    font-weight: bold;
    padding: 15px 35px;
    border-radius: 50px;
    background-color: {!! $anchor_background_color !!};/*按鈕背景顏色，同錨點*/
    color: #000;
    display: block;
    margin: 30px auto 15px;
    width: 450px;
    font-size: 22px;
    text-decoration: none;
    text-align: center;
    animation-duration: 0.3s;
}
.promotion-container .promotion-btn:hover{
	background-color: {!! $anchor_background_hover_color !!};/*按鈕背景顏色，同錨點hover*/
}
.promotion-container .product-banner:hover{
    opacity: 0.7;
}
.main-background-color{
	background: rgba( {!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!}, 0.6);/*有透明度的主題色*/
	padding: 10px;
}
.page-block .product_area .list-barrier{
	background: rgb( {!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!});/*分類及車型文字背景，同主題色*/
	color: {!! $main_color_text !!};/*分類及車型文字顏色，同主題文字色*/
	width: 100%;
	height: 100%;
	text-align: center;
}

</style>
@stop
@section('middle')
	{!! $item_block !!}
@stop
@section('script')
<script type="text/javascript">
	function slipTo1(element){
        var y = parseInt($(element).offset().top) - 68;
        $('html,body').animate({scrollTop: y}, 400);
    }
</script>
<script type="text/javascript">
	$(window).scroll(function(){
        if ($(this).scrollTop() > 900) {
          $('.promotion-container .links-fixed').show();
        } else {
          $('.promotion-container .links-fixed').hide();
        }
    });
</script>
<script type="text/javascript">
	$('.page-block .block-product .product_area .list-page a').mouseenter(function(){
		$(this).find('div.list-barrier').removeClass('hidden');
		$(this).find('.list-img').addClass('hidden');
	});
	$('.page-block .block-product .product_area .list-page a').mouseleave(function(){
		$(this).find('div.list-barrier').addClass('hidden');
		$(this).find('.list-img').removeClass('hidden');
	});
</script>
<script type="text/javascript">
	$('.owl-carouse-product-loop').addClass('owl-carousel').owlCarousel({
		autoplayTimeout:1500,
		autoplayHoverPause:true,
		loop:true,
		margin:10,
		nav:false,
	    slideBy : 1,
	    center: true,
	    autoplay: true,
		autoPlaySpeed: 500,
		dots: true,
		autoplayHoverPause: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	});
	$(document).ready(function(){
		$('.product-list .loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
	});

	$(".page-block .product-list").mouseenter(function(){
	  $(this).find('.loop-photo').removeClass('hidden');
	  $(this).find('.loop-photo .owl-carouse-product-loop').trigger('play.owl.autoplay',[1500]);
	  $(this).addClass('active');
	  setTimeout(function(){ 
	  		$('.page-block .product-list.active .owl-carouse-product-loop li').removeClass('hidden');
	  }, 500);
	});

	$(".page-block .product-list").mouseleave(function(){
	  	$(this).find('.loop-photo').addClass('hidden');
	  	$(this).removeClass('active');
	  	$(this).find('.loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
	});
</script>
<script type="text/javascript">
	
	$(document).on('change', ".select-motor-manufacturer", function(){
        var value = $(this).find('option:selected').val();
        var _this = $(this);
        if (value){
            var url = path + "/api/motor/displacements?manufacturer=" + value;
            $.get( url , function( data ) {
                var $target = _this.closest('.ct-table-cell').find(".select-motor-displacement");
                $target.find("option:not(:first)").remove().end();
                for (var i = 0 ; i < data.length ; i++){
                    $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                }
            });
        }
    });

    $(document).on('change', ".select-motor-displacement", function(){
        var value = $(this).find('option:selected').val();
        var _this = $(this);
        if (value){
            var url = path + "/api/motor/model?manufacturer=" +
                _this.closest('.ct-table-cell').find(".select-motor-manufacturer").find('option:selected').val() +
                "&displacement=" +
                value;

            $.get( url , function( data ) {
                var $target = _this.closest('.ct-table-cell').find(".select-motor-model");
                $target.find("option:not(:first)").remove().end();
                for (var i = 0 ; i < data.length ; i++){
                    $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                }
            });
        }
    });

    $(document).on('change', ".select-motor-model", function(){
        var value = $(this).find('option:selected').val();
        $('.btn-my-bike span').text('MyBike商品搜尋');
        if ((value) && (value != "車型")){
            link = '{{route('parts')}}' + '/mt/' + value;
        	$('.motor .page-block-product .promotion-btn').removeClass('btn-disable').attr('href',link);
        }else{
        	$('.motor .page-block-product .promotion-btn').addClass('btn-disable').attr('href','javascript:void(0)');
        }
    });

    $(document).on('click', ".select-motor-mybike", function(){
        var value = $(this).attr('name');
        motor_name = $(this).text();
        if (value){
        	$('.btn-my-bike span').text(motor_name);
        	link = '{{route('parts')}}' + '/mt/' + value;
        	$('.motor .page-block-product .promotion-btn').removeClass('btn-disable').attr('href',link);
        }else{
        	$('.motor .page-block-product .promotion-btn').addClass('btn-disable').attr('href','javascript:void(0)');
        }
    });
</script>
@stop