<style type="text/css">

.banner-area{
	position: relative;
}
.banner-container .banner-block.theme .theme-block{
	color: {!! $main_color_text !!}; /*主題文字顏色*/
	position: absolute;
	padding: 10px 10px 20px;
	z-index: 1;
	bottom: 0;
	width: 47%;
}
.owl-carouse-main-banner .owl-dots{
    text-align: center;
    background: {!! $anchor_background_color !!}; /* dost區塊背景色，同錨點原始背景色*/
}
.owl-carouse-main-banner .owl-dots .owl-dot{
    display:inline-block;
    margin-left: 10px;
}
.owl-carouse-main-banner .owl-dots .owl-dot span{
    background: #fff;
    border: 1px solid #fff;
    width: 10px;
    height: 10px;
    display: block;
    border-radius: 999em;
    cursor: pointer;
}
.owl-carouse-main-banner .owl-dots .owl-dot.active span{
    background: #797979;
    border: 1px solid #797979;
}
.theme-block h2 i.fa{
	background: {!! $anchor_background_color !!}; /*箭頭按鈕背景色，同錨點原始背景色*/
    padding: 5px;
    border-radius: 999em;
    cursor: pointer;
}
.main-banner .banner-container .banner-area .theme-block h2 span{
	font-size: 4rem;
	letter-spacing: 5px;
}
.theme-text-hidden {
	visibility: hidden;
}
.owl-carouse-main-banner .animated { 
  animation-duration: 3500ms;
  animation-fill-mode: both; }
</style>
<div class="main-banner">
	<div class=" banner-container clearfix">
		<div class="banner-block theme">
		    <div class="banner-area theme-area">
		    	<ul class="owl-carouse-main-banner">
		    		@foreach($images as $img)
			    		<li class="width-max">
				            <img class="image-cover" src="{!! $img !!}">
			    		</li>
		    		@endforeach
		    	</ul>
		        <div class="theme-block main-background-color theme-text-hidden">
	        		<h2 class="btn-gap-bottom">
		        		<span class=" vertical-middle">{!! $promotion_title !!}</span>
		        		<i class="fa fa-chevron-down" aria-hidden="true"></i>
		        	</h2>
		        	<span class="theme-text size-12rem theme-text-hidden">{{$promotion_introduce }}
					</span>
		        </div>
		    </div>
		</div>
	</div>
</div>
	<script type="text/javascript">

		$(document).ready(function(){
			$('.main-banner .banner-container .theme-block ').removeClass('theme-text-hidden');
			var theme_block_height = $('.main-banner .banner-container .theme-block ').height() + 10;
			$('.banner-container .banner-block .theme-block').css('bottom','-' + theme_block_height + 'px');
			setTimeout(function(){
				showThemeTitle();
			},500);
			setTimeout(function(){
				showTheme();
			},2000);
		});

		$('.main-banner .banner-container .theme-block i').on('click',function(){
			
			if(($(this).hasClass('fa-chevron-up') && !($('.main-banner .banner-container .theme-block ').hasClass('open')))){
					showTheme();
			}else{
				$('.main-banner .banner-container .theme-block i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
				$('.main-banner .banner-container .theme-block .main-theme').removeClass('hidden');

				showThemeTitle();

				setTimeout(function(){
					$('.main-banner .banner-container .theme-block  span.theme-text').addClass('theme-text-hidden');
				},550);
			}			
		});

		function showThemeTitle(){
			var theme_text_height = $('.main-banner .banner-container .theme-block  span.theme-text').height() + 3;
			var height = '-' + theme_text_height + 'px';
			$('.banner-container .banner-block .theme-block').animate({bottom: height},500);
			$('.main-banner .banner-container .theme-block ').removeClass('open');
		}

		function showTheme(){
			$('.banner-container .theme-block').animate({bottom: "0px"},1000);
			$('.main-banner .banner-container .theme-block i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
			$('.main-banner .banner-container .theme-block  span.theme-text').removeClass('theme-text-hidden');
			$('.main-banner .banner-container .theme-block ').addClass('open');
		}

		$('.owl-carouse-main-banner').addClass('owl-carousel').owlCarousel({
			animateOut: 'fadeOut',
			animateIn: 'flipInX',
			items:1,
			center: true,
		    autoplay: true,
		    // autoplay: false,
			autoPlaySpeed: 1000,
			loop:true,
			mouseDrag:false,
			dots: true,
			smartSpeed:500
		});
	</script>