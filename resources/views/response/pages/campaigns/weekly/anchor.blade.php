<style type="text/css">
	.promotion-container .links-bar{
		z-index: 15;
		max-width: 1280px;
		width: 100%;
		margin: 0 auto;
		background-size: contain;
	}
	.promotion-container .links-fixed{
		position: fixed!important;
		top: 0;
		width: 100%;
		background: none!important;
		z-index: 15;
		display: none;
	}
	.promotion-container .links{
		z-index: 15;
		position: relative;
		background: #fff;
	}
	.promotion-container .links-bar ul li:last-child{
		border-right: none;
	}
	.promotion-container .links-bar ul li{
		width: 25%;
		background-color: {!! $anchor_background_color !!};/*錨點背景顏色*/
		border-right: 2px solid  rgb( {!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!});/*錨點邊線顏色，同主題色*/
		box-sizing: border-box;
		text-align: center;
		float: left;
		font-size: 1.25rem;
		padding: 20px;
	}
	.promotion-container .links-bar ul li a{
		color: {!! $anchor_color !!};/*錨點文字顏色*/
	}
	.promotion-container .links-bar ul li:hover{
		background-color: {!! $anchor_background_hover_color !!};/*錨點hover背景顏色*/
	    cursor: pointer;
	}
</style>
<div class="links">
	<div class="links-bar">
		<div>
			<ul class="clearfix">
				@foreach($block_names as $key => $anchor_name)
					<li onclick="slipTo1('{!! '#'.$anchor_name !!}')">
						<a href="javascript:void(0)">
							{{$name[$key]}}
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>
<div class="links-fixed">
	<div class="links-bar">
		<div>
			<ul class="clearfix">
				@foreach($block_names as $key => $anchor_name)
					<li onclick="slipTo1('{!! '#'.$anchor_name !!}')">
						<a href="javascript:void(0)">
							{{$name[$key]}}
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>