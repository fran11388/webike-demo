<style type="text/css">
@foreach($tag_colors as $tag_sort => $tag_color)
	.motor .list-page:nth-child({!! $tag_sort+1 !!}) .list-barrier .text-tag{
		font-size: 1.25rem;
		width: 95px;
		margin: 0 auto;
		background: {!! $tag_color !!};
	}
@endforeach
</style>
<div class="promotion-page-block">
	<div class="page-block motor" id="MotorDouble">
		<h2 class="page-block-title">{!! $title !!}</h2>
		<div class="page-block-product">
			<div class="block-product">
				<div class="product_area">
					<ul class="clearfix motor-banner">
						@foreach($url_rewrites as $motor_sort => $url_rewrite)
							<li class="list-page">
								<a href="{{ route('parts','mt/'.$url_rewrite).'?'.$rel_parameter}}" target="_blank" class="zoom-image">
									<div class="list-barrier hidden">
										<div class="vertical-middle">
											<span class="size-19rem">{{$names[$motor_sort]}}</span>
											@if($week_active)
												<div class="text-center">
													<span class="label text-tag">{{$tag_text[$motor_sort]}}</span>
												</div>
											@endif
										</div>
										<span class="vertical-middle list-height">
										</span>
									</div>
									<div class="list-img">
										<img src="{{$images[$motor_sort]}}" class="motor-img">
									</div>
								</a>	
							</li>
						@endforeach
					</ul>
				</div>
				<div class="product_area">
					<div class="ct-table-cell">
			            <ul class="ul-cell-dropdown-list container">
			                <li class="li-cell-dropdown-item">
			                    <div class=" select-box">
			                        <select class="select2 select-motor-manufacturer">
			                            <option>請選擇廠牌</option>
			                            @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
			                                <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
			                            @endforeach
			                        </select>
			                    </div>
			                </li>
			                <li class="li-cell-dropdown-item">
			                    <div class=" select-box">
			                        <select class="select2 select-motor-displacement">
			                            <option>cc數</option>
			                        </select>
			                    </div>
			                </li>
			                <li class="li-cell-dropdown-item">
			                    <div class=" select-box">
			                        <select class="select2 select-motor-model">
			                            <option>車型</option>
			                        </select>
			                    </div>
			                </li>
			                @include('response.common.list.filter-mybike')
			            </ul>
			        </div>
			        <div class="text-center">
			        	<a class="promotion-btn btn-disable" href="javascript:void(0)"  target="_blank">車型搜尋</a>
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>