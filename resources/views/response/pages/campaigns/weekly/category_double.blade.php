<style type="text/css">
@foreach($tag_colors as $tag_sort => $tag_color)
	.category .list-page:nth-child({!! $tag_sort+1 !!}) .list-barrier .text-tag{
		font-size: 1.25rem;
		width: 95px;
		margin: 0 auto;
		background: {!! $tag_color !!};
	}
@endforeach
</style>
<div class="promotion-page-block">
	<div class="page-block category" id="CategoryDouble">
		<h2 class="page-block-title">{!! $title !!}</h2>
		<div class="page-block-product">
			<div class="block-product">
				<div class="product_area">
					<ul class="clearfix">
						@foreach($url_rewrites as $category_sort => $url_rewrite)
							<li class="list-page">
								<a href="{{ route('parts','ca/'.$url_rewrite).'?'.$rel_parameter}}" target="_blank" class="zoom-image">
									<div class="list-barrier hidden">
										<div class="vertical-middle">
											<span class="size-19rem">{{$names[$category_sort]}}</span>
											@if($week_active)
												<span class="text-tag label">{{$tag_text[$category_sort]}}</span>
											@endif
										</div>
										<span class="vertical-middle list-height">
										</span>
									</div>
									<div class="list-height list-img">
										<img class="image-cover" src="{!! $images[$category_sort] !!}">
									</div>
								</a>	
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>