<div class="promotion-page-block">
	<div class="page-block week_sale" id="BrandDiscount">
		<h2 class="page-block-title">{!! $title !!}</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix">
					@foreach($brandDiscount_datas as $discount_brands_url_rewrite => $discount_data)
						<li class="product-list li-list" >
							<div>
								<a href="{{route('parts', 'br/'.$discount_brands_url_rewrite).'?'.$rel_parameter}}" target="_blank">
									<div class="product-img">
										<img src="{{$discount_data['brand_image']}}">
									</div>
								</a>
								<div class="product-text text-center">
									<span>最大{{$week_active ? number_format($discount_data['offer']*100): '9'}}折優惠</span>
								</div>
								<div class="product-tag">
									@foreach($discount_data['ca_name'] as $discount_ca_key => $discount_categorie)
										@php
											$path = explode('-',$discount_categorie->url_path);
											$tag_class = 'label-primary';
											switch ($path['0']) {
												case '3000':
													$tag_class = 'label-primary';
												break;
												case '1000':
													$tag_class = 'label-danger';
												break;
												case '8000':
													$tag_class = 'label-warning';
												break;
												case '4000':
													$tag_class = 'label-warning';
												break;
												default:
													$tag_class = 'label-primary';
												break;
											}
										@endphp
										<span class="dotted-text tag label {{$tag_class}} {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$discount_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$discount_categorie->name}}</span>
									@endforeach
								</div>
							</div>
							<div class="loop-photo hidden">
								<h2>{{$discount_data['brand_name']}}</h2>
								<ul class="owl-carouse-product-loop">
									@if($discount_data['product_image'])
										@foreach($discount_data['product_image'] as $discount_image_key => $discount_product_image)
											<li class="text-center {{$discount_image_key == 0 ? ' ': 'hidden'}}">
												<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
											</li>
										@endforeach
									@else
										<li class="text-center">
											<img src="{{NO_IMAGE}}">
										</li>
									@endif
								</ul>
							</div>
						</li>
					@endforeach 
				</ul>
			</div>
		</div>
	</div>
</div>