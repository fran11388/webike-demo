<style type="text/css">
	.search{
	    padding: 15px 0px;
		background-color: rgb({!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!}); /*搜尋背景，同主題色*/
		position: relative;
		z-index: 10;
	}
	.search .search-text{
		background: #fff;
		border-radius: 25px 0 0 25px;
		border: none;
		width: auto;
		padding: 14px;
		overflow: hidden;
	}
	.search .search-text input{
		border: 0;
		width: 100%;
	    font-family: FontAwesome, 'メイリオ';
	}
	.search .search-btn{
		float: right;
	}
	.search .search-btn input{
		font-family: FontAwesome, 'メイリオ';
		background: #910000;
		font-weight: bold;
		color: #fff;
		border: 0;
		padding: 15px;
		border-radius: 0 25px 25px 0;
		cursor: pointer;
	}
</style>
<div id="search" class="search">
	<div class="block-container">
		<div class="search-btn">
			<input type="submit" value="">			
		</div>
		<div class="search-text">
			<input id="search_bar" type="text" name="search" autocomplete="off" placeholder="&#xF002; 請輸入關鍵字(54萬項商品、1840家廠牌)">
		</div>
	</div>
</div>
 <script src="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>

        $(function() {
            $( "#search_bar" ).autocomplete({
                minLength: 1,
                source: solr_source,
                focus: function( event, ui ) {
                    $('#search_bar').val();
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'header');
                    return false;
                },
                change: function(event, ui) {

                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }
                
                return bag
                // .append( '<a href="http://localhost/test">' + item.icon + "<p>" + item.label + "</p></a>" )
                    .append( '<a  href="javascript:void(0)" class="search-keyword">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
        });

        
        var current_suggest_connection = null;
        function solr_source(request, response){
            var params = {q: request.term};

            current_suggest_connection = $.ajax({
                url: "{{ route('api-suggest')}}",
                method:'GET',
                data : params,
                dataType: "json",
                beforeSend: function( xhr ) {
                    if(current_suggest_connection){
                        current_suggest_connection.abort();
                    }
                },
                success: function(data) {
                    response(data);
                }
            });
        }
       $(document).on('click', ".ui-menu li.ui-menu-item", function(){
            var keyword = $(this).find('span.font-normal').text();
            $('.search-text #search_bar').val(keyword);
        });

        $('#search .search-btn input').click(function(){
        	keyword = $('.search-text #search_bar').val();
        	if(keyword){
        		window.location = '{{route('parts')}}' + '?q=' + keyword;
        	}
        });
    </script>