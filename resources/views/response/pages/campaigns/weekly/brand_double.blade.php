<div class="promotion-page-block">
	<div class="page-block double" id="BrandDouble">
		<h2 class="page-block-title">{!! $title !!}</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix">
					 @foreach($brandDouble_datas as $double_brand_url_rewrite => $double_brand)
						<li class="product-list li-list">
							<a href="{{route('parts', 'br/'.$double_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
								<div class="product-img">
									<img src="{{$double_brand['brand_image']}}">
								</div>
								<div class="product-text text-center">
									<span>點數{{$week_active ? $double_brand['offer'] .'倍':'加倍'}}回饋</span>
								</div>
							</a>
							<div class="product-tag">
								@foreach($double_brand['ca_name'] as $double_ca_key => $double_categorie)
									@php
										$path = explode('-',$double_categorie->url_path);
										$tag_class = 'label-primary';
										switch ($path['0']) {
											case '3000':
												$tag_class = 'label-primary';
											break;
											case '1000':
												$tag_class = 'label-danger';
											break;
											case '8000':
												$tag_class = 'label-warning';
											break;
											case '4000':
												$tag_class = 'label-warning';
											break;
											default:
												$tag_class = 'label-primary';
											break;
										}
									@endphp
									<span class="dotted-text tag label {{$tag_class}} {{$double_ca_key == 2 ? 'hidden': ' ' }}">{{$double_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$double_categorie->name}}</span>
								@endforeach
							</div>
							<div class="loop-photo hidden">
								<h2>{{$double_brand['brand_name']}}</h2>
								<ul class="owl-carouse-product-loop">
									@if($double_brand['product_image'])
										@foreach($double_brand['product_image'] as $double_image_key => $double_product_image)
											<li class="text-center {{$double_image_key == 0 ? ' ': 'hidden'}}">
												<img src="{{ $double_product_image ? $double_product_image : NO_IMAGE}}">
											</li>
										@endforeach
									@else
										<li class="text-center}}">
											<img src="{{NO_IMAGE}}">
										</li>
									@endif
								</ul>
							</div>
						</li>
					@endforeach 
				</ul>
			</div>
		</div>
	</div>
</div>