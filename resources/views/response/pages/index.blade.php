<!doctype html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include ( 'response.layouts.partials.head' )
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/home/index.css') }}">
</head>
<body>
<div class="all-page">
    @include ( 'response.layouts.partials.header.index' )
    <!-- Content -->
    <section id="contents" class="content-top-page homepage">
        @include ( 'response.layouts.partials.header.bannerbar' )
        @include ( 'response.layouts.partials.header.promotion-tag' )
        <ul class="menu-left-mobile col-xs-12">
            @include ( 'response.common.hotlinks-top' )
        </ul>
        <div class="container container-top-page">
            <div class="box-content-brand-top-page">
                <div class="ct-left-search-list col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-xs no-padding">
                    @if(\Ecommerce\Core\Agent::isNotPhone())
                        <div class="row">
                            @include('response.pages.partials.popular-motor')
                        </div>
                        <div class="row">
                            <div class="box col-xs-12 col-sm-12 col-md-12 motogp">
                                <h3 class="title-box text-center">2019 MotoGP車手排行榜</h3>
                                <div class="content-box col-xs-12 col-sm-12 col-md-12">
                                    <ul class="Ranking-motogp">
                                        @foreach($gp_racers as $key => $gp_racer)
                                            <li class="clearfix">
                                                <a href="{{route('benefit-event-motogprider-2019', $gp_racer->racer->number)}}" title="{{$gp_racer->racer->nickname}}" target="_blank" rel="nofollow">
                                                    <label class="bg-color-red text-center">{{$key + 1}}</label>
                                                    <span class="dotted-text">{{'#' . $gp_racer->racer->number . ' ' . $gp_racer->racer->nickname . ' (' . number_format($gp_racer->total_score) . ')'}}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <h5 class="content-last-box size-08rem box">
                                         @if($active_station)
                                            <a href="{{URL::route('benefit-event-motogp-2019')}}" title="{{date('Y年') . 'MotoGP預測總冠軍：第' . $active_station->name . '站' . $active_station->name . '活動進行中'}}">
                                                <span class="font-color-normal">預測總冠軍有獎活動：</span><br>
                                                <span class="font-color-red font-bold">>> {{$active_station->name}}活動進行中</span>
                                            </a>
                                        @else 
                                            <a href="{{URL::route('benefit-event-motogp-2019')}}" title="{{date('Y年') . 'MotoGP預測總冠軍'}}">
                                                <span class="font-color-normal">預測總冠軍有獎活動</span>
                                                @if(isset($gp_racers[0]) and $champ = $gp_racers[0])
                                                    <br>
                                                    <span class="font-color-red font-bold">>> 年度總冠軍 {{'#' . $champ->racer->number . ' ' . $champ->racer->nickname}}</span>
                                                @endif
                                            </a>
                                         @endif 
                                    </h5>
                                </div>
                            </div>
                        </div>
                        @if(!$current_customer)
                            <div class="row box">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="customer-dashboard box-content">
                                        <div class="title white">
                                            <img src="{!! assetRemote('image/shopping/img-title-01.jpg') !!}" alt="加入會員{{$tail}}"/>
                                        </div>
                                        <div class="content">
                                            <span>進口重機、機車改裝用品情報，新車中古車資訊，全球摩托車新聞滿載!!會員專屬優惠及好康、獨家電子報、生日禮券等...眾多服務，歡迎體驗。</span>
                                            <div class="text-center content-last-box">
                                                ~Webike滿足您的摩托人生!
                                            </div>
                                            <div class="text-center content-last-box">
                                                <a class="btn bg-color-red" href="{{URL::route('customer-account-create')}}" title="加入會員{{$tail}}">立即加入會員</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-page-group">
                                    <div class="title-box-page-group">
                                        <h3>摩托百貨</h3>
                                    </div>
                                    <div class="ct-box-page-group">
                                        <ul class="ul-menu-ct-box-page-group">
                                            <li>
                                                <a href="{{route('motor')}}" title="車型索引{{$tail}}">
                                                    <span>車型索引</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('summary', ['ca/1000'])}}" title="改裝零件{{$tail}}">
                                                    <span>改裝零件</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('summary', ['ca/3000'])}}" title="騎士用品{{$tail}}">
                                                    <span>騎士用品</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('genuineparts')}}" title="正廠零件{{$tail}}">
                                                    <span>正廠零件</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('outlet')}}" title="Outlet{{$tail}}">
                                                    <span>Outlet</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('brand')}}" title="精選品牌{{$tail}}">
                                                    <span>精選品牌</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('dealer')}}" title="經銷據點{{$tail}}">
                                                    <span>經銷據點</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-page-group">
                                    <div class="title-box-page-group">
                                        <h3>摩托車市</h3>
                                    </div>
                                    <div class="ct-box-page-group">
                                        <ul class="ul-menu-ct-box-page-group">
                                            <li>
                                                <a href="{{MOTOMARKET . '/search/imp'}}" title="進口新車．中古車{{\Everglory\Constants\Website::MOTOMARKET}}">
                                                    <span>進口新車．中古車</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{MOTOMARKET . '/search/dom'}}" title="國產新車．中古車{{\Everglory\Constants\Website::MOTOMARKET}}">
                                                    <span>國產新車．中古車</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{MOTOMARKET . '/search/personal'}}" title="個人自售{{\Everglory\Constants\Website::MOTOMARKET}}">
                                                    <span>個人自售</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{MOTOMARKET . '/shop'}}" title="販賣車商{{\Everglory\Constants\Website::MOTOMARKET}}">
                                                    <span>販賣車商</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{MOTOMARKET . '/customer/product/create'}}" title="免費刊登{{\Everglory\Constants\Website::MOTOMARKET}}">
                                                    <span>免費刊登</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-page-group">
                                    <div class="title-box-page-group">
                                        <h3>摩托新聞</h3>
                                    </div>
                                    <div class="ct-box-page-group">
                                        <ul class="ul-menu-ct-box-page-group">
                                            <li>
                                                <a href="{{BIKENEWS . '/category/newmodel/'}}" title="新車新聞 - 「{{\Everglory\Constants\Website::BIKENEWS}}」">
                                                    <span>新車新聞</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{BIKENEWS . '/category/newmodel-imp/'}}" title="試乘報告 - 「{{\Everglory\Constants\Website::BIKENEWS}}」">
                                                    <span>試乘報告</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{BIKENEWS . '/category/racing/'}}" title="賽事消息 - 「{{\Everglory\Constants\Website::BIKENEWS}}」">
                                                    <span>賽事消息</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{BIKENEWS . '/category/column/'}}" title="名家專欄 - 「{{\Everglory\Constants\Website::BIKENEWS}}」">
                                                    <span>名家專欄</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{BIKENEWS . '/category/industry/'}}" title="業界情報 - 「{{\Everglory\Constants\Website::BIKENEWS}}」">
                                                    <span>業界情報</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-page-group">
                                    <div class="title-box-page-group">
                                        <h3>推薦連結</h3>
                                    </div>
                                    <div class="ct-box-page-group">
                                        <div class="row box">
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <a href="{{ MOTOCHANNEL }}" title="{!! \Everglory\Constants\Website::MOTOCHANNEL !!}">
                                                    <figure class="zoom-image">
                                                        <img src="{!! assetRemote('image/WebikeChannel_200X61.png') !!}" alt="{!! \Everglory\Constants\Website::MOTOCHANNEL !!}">
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                @if(isset($advertiser))
                                                    @for($i = 1 ; $i <= 5 ; $i++)
                                                        <div class="box">
                                                            <figure class="zoom-image">
                                                                    {!! $advertiser->call(21) !!}
                                                            </figure>
                                                        </div>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                        {{--<div class="row">--}}
                                            {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                                                {{--@if($active_station)--}}
                                                    {{--<a href="{{route('benefit-event-motogp-2017')}}" title="{{date('Y年') . 'MotoGP預測總冠軍：第' . $active_station->name . '站' . $active_station->name . '活動進行中'}}">--}}
                                                        {{--<figure class="zoom-image">--}}
                                                            {{--<img src="https://img.webike.tw/assets/images/shared/bannerforNews.png" alt="{{date('Y年') . 'MotoGP預測總冠軍：第' . $active_station->name . '站' . $active_station->name . '活動進行中'}}">--}}
                                                        {{--</figure>--}}
                                                    {{--</a>--}}
                                                {{--@else--}}
                                                    {{--<a href="{{route('benefit-event-motogp-2017')}}" title="{{date('Y年') . 'MotoGP預測總冠軍'}}">--}}
                                                        {{--<figure class="zoom-image">--}}
                                                            {{--<img src="https://img.webike.tw/assets/images/shared/bannerforNews.png" alt="{{date('Y年') . 'MotoGP預測總冠軍'}}">--}}
                                                        {{--</figure>--}}
                                                    {{--</a>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-page-group">
                                    <div class="title-box-page-group">
                                        <h3>最新品牌</h3>
                                    </div>
                                    <div class="ct-box-page-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                @if(isset($advertiser))
                                                    @for($i = 1 ; $i <= 5 ; $i++)
                                                        <div class="box">
                                                            <figure class="zoom-image">
                                                                {!! $advertiser->call(25) !!}
                                                            </figure>
                                                        </div>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.9&appId=791319474279962";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>
                                <div class="fb-page" data-href="https://www.facebook.com/WebikeTaiwan/" data-small-header="false" data-width="200" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/WebikeTaiwan/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/WebikeTaiwan/">Webike台灣</a></blockquote></div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <div class="clearfix">
                        <!-- news -->
                        <div id="index_featured_post">
                            <div id="main_slider" class="flexslider clearfix">
                                <ul class="slides clearfix">
                                    @php
                                        $post_images = [];
                                    @endphp
                                    @foreach($posts as $key => $post)
                                        @php
                                            $post_thumb_img = ThumborBuildWithCdn($post->thumbnail,650,380);
                                            $post_images[$key] = $post_thumb_img;
                                        @endphp
                                        <li>
                                            <a href="{{$post->guid}}" title="{{$post->post_title . $tail}}">
                                                <img src="{{ $post_thumb_img }}" alt="{{$post->post_title . $tail}}">
                                                <!-- <img src="{{ str_replace('http:','',str_replace('.jpg','-650x380.jpg',$post->thumbnail)) }}" alt="{{$post->post_title . $tail}}"> -->
                                                {{--<img src="{{ str_replace('http:','',$post->thumbnail) }}" alt="{{$post->post_title . $tail}}">--}}
                                                <span class="helper"></span>
                                            </a>
                                            <a class="flex-caption" href="{{$post->guid}}" title="{{$post->post_title . $tail}}">{{$post->post_title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="flexslider-controls">
                                <ol class="flex-control-nav">
                                    @foreach($posts as $key => $post)
                                    <li>
                                        <a href="{{$post->guid}}" title="{{$post->post_title . $tail}}">
                                            <div class="image">
                                                <img src="{{ isset($post_images[$key]) ? $post_images[$key] : ''}}" alt="{{$post->post_title . $tail}}">
                                            </div>
                                            <div class="info">
                                                <p class="date">{{date('Y/m/d', strtotime($post->post_date))}}</p>
                                            <!-- category name
                                                <p class="category">
                                                </p>
                                            -->
                                                <h4 class="title dotted-text2">{{$post->post_title}}</h4>
                                            </div>
                                        </a>
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix block">
                        <a class="btn-customer-review-read-more" href="{!! BIKENEWS !!}" target="_blank">>> 更多新聞</a>
                    </div>
                    <!--
                    <div class="clearfix block blue-box">
                        <a href="http://www.webike.tw/bikenews/2017/04/24/%E3%80%90%E7%B3%BB%E7%B5%B1%E5%85%AC%E5%91%8A%E3%80%91%E5%9B%A0hinet-apcn2%E6%B5%B7%E7%BA%9C%E9%9A%9C%E7%A4%99%EF%BC%8C%E9%83%A8%E5%88%86%E9%80%A3%E7%B7%9A%E5%93%81%E8%B3%AA%E6%9C%83%E5%8F%97%E5%88%B0/" title="【系統公告】因HiNet APCN2海纜障礙，部分連線品質會受到影響" target="_blank">
                            <h2>
                                【系統公告】因HiNet APCN2海纜障礙，部分連線品質會受到影響。
                            </h2>
                        </a>
                    </div>
                    -->
                    <div class="clearfix box-content">
                        <div class="clearfix">
                            <div class="title-main-box-clear">
                                <h2>
                                    <span>會員好康　</span>
                                    <a href="{!! URL::route('benefit') !!}" title="{!! '會員好康' . $tail !!}">>> 更多好康</a>
                                </h2>
                            </div>
                        </div>
                        @include('response.common.ad.banner-small')
                    </div>

{{--                    @include('response.pages.home.category')--}}

                    <div class="box-content">
                        <div class="clearfix">
                            <div class="title-main-box-clear">
                                <h2>
                                    <span>摩托百貨商品分類　</span>
                                </h2>
                            </div>
                        </div>
                        <div class="row categories-box">
                            <!-- <div class="owl-carousel-responsive"> -->
                            <div>
                                @php
                                    $last_category = $main_categories->pull(count($main_categories) - 1);
                                @endphp
                                @foreach($main_categories as $main_category)
                                    <div class="col-xs-6 col-sm-2-4 col-md-2-4 col-lg-2-4 box">
                                        @php
                                            $link = route('summary', ['ca/' . $main_category->mptt->url_path]);
                                            if($main_category->url_rewrite == 9000){
                                                $link = route('genuineparts');
                                            }
                                        @endphp
                                        <a class="content" href="{!! $link !!}" title="{{$main_category->name . $tail}}">
                                            <img src="{{assetRemote('image/category/index/' . $main_category->url_rewrite . '.png')}}" alt="{{$main_category->name . $tail}}">
                                            <span class="name">
                                                <span class="text">{{$main_category->name}}</span>
                                                <span class="helper"></span>
                                            </span>
                                        </a>
                                    </div>
                                @endforeach
                                @if($last_category)
                                    <div class="hidden-xs">
                                        <div class="col-xs-12 col-sm-2-4 col-md-2-4 col-lg-2-4 box">
                                        @php
                                            $link = route('summary', ['ca/' . $last_category->mptt->url_path]);
                                            if($last_category->url_rewrite == 9000){
                                                $link = route('genuineparts');
                                            }
                                        @endphp
                                            <a class="content" href="{!! $link !!}" title="{{$last_category->name . $tail}}">
                                                <img src="{{assetRemote('image/category/index/' . $last_category->url_rewrite . '.png')}}" alt="{{$last_category->name . $tail}}">
                                                <span class="name">
                                                    <span class="text">{{$last_category->name}}</span>
                                                    <span class="helper"></span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    {{--<div class="box-content hidden-sm hidden-md hidden-lg">--}}
                        {{--<div class="clearfix">--}}
                            {{--<div class="title-main-box-clear">--}}
                                {{--<h2>--}}
                                    {{--<span>正廠零件查詢與購買　</span>--}}
                                {{--</h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box">--}}
                                {{--<a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::IMPORT['url_code']]) !!}" title="進口正廠零件 - 「Webike-摩托百貨」">--}}
                                    {{--<img src="https://img.webike.tw/shopping/image/oempart/banner-{!! \Everglory\Constants\CountryGroup::IMPORT['url_code'] !!}.png">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">--}}
                                {{--<a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::DOMESTIC['url_code']]) !!}" title="國產正廠零件 - 「Webike-摩托百貨」">--}}
                                    {{--<img src="https://img.webike.tw/shopping/image/oempart/banner-{!! \Everglory\Constants\CountryGroup::DOMESTIC['url_code'] !!}.png">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="box-content">
                        <div class="clearfix">
                            <div class="title-main-box-clear">
                                <h2>
                                    <span>全球摩托車資料庫　</span>
                                    <a href="{!! URL::route('motor') !!}" title="{!! '車型索引' . $tail !!}">>> 更多廠牌車型</a>
                                </h2>
                            </div>
                        </div>
                        <!-- motors -->
                        <div class="motor-select-box">
                            <form action="" method="GET" id="motor_search_form">
                                <input type="hidden" name="rel" value="homepage">
                                <ul class="box clearfix no-list-style" id="bike-first-row">
                                    <li class="col-xs-6 col-sm-3 col-md-4 box">
                                        <select class="select2 select-motor-manufacturer">
                                            <option value="">請選擇廠牌</option>
                                            @foreach ($motor_manufacturers as $_manufacturer)
                                                <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                            @endforeach
                                        </select>
                                    </li>
                                    <li class="col-xs-6 col-sm-4 col-md-4 box">
                                        <select class="select2 select-motor-model" disabled>
                                            <option value="">車型</option>
                                        </select>
                                    </li>
                                    <li class="col-xs-12 col-sm-3 col-md-2 box">
                                        <input class=" btn btn-primary btn-full" type="submit" name="" value="搜尋">
                                    </li>
                                </ul>
                            </form>
                        </div>
                        @include('response.common.plugin.suggest')
                        <div class="clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="motor-manufacturer-box">
                                    <div class="row">
                                        @foreach($main_manufacturers as $key => $main_manufacturer)
                                            @if($key == 4)
                                                @break;
                                            @endif
                                            <div class="col-xs-6 col-sm-3 col-md-3">
                                                <div class="box-content manufacturer-logo">
                                                    <a class="logo-link" href="javascript:void(0)" title="{{$main_manufacturer->name . $tail}}">
                                                        {!! lazyImage(assetRemote('image/motor-manufacturer/logo-index/' . $main_manufacturer->url_rewrite . '.png'),$main_manufacturer->name . $tail) !!}
                                                        <span class="helper"></span>
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="manufacturer-logo-list">
                                                        @foreach(\Everglory\Constants\MotorDisplacementLevel::DISPLACEMENT_LEVELS as $displacement_level)
                                                            @php
                                                                $variable_name = 'DISPLACEMENT_' . $displacement_level;
                                                            @endphp
                                                            <li>
                                                                <a href="{{route('motor-manufacturer', [$main_manufacturer->url_rewrite, $displacement_level])}}" title="{{$main_manufacturer->name . \Everglory\Constants\MotorDisplacementLevel::getConst($variable_name) . $tail}}">
                                                                    {{\Everglory\Constants\MotorDisplacementLevel::getConst($variable_name)}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            @php
                                                $main_manufacturers->shift();
                                            @endphp
                                        @endforeach
                                    </div>
                                    <div class="row owl-carousel-responsive">
                                        @foreach($main_manufacturers as $key => $main_manufacturer)
                                            <div class="col-xs-12 col-sm-12 col-md-2-4 col-lg-2-4 item-product-grid">
                                                <div class="box-content manufacturer-logo">
                                                    <a class="logo-link" href="{{route('motor-manufacturer', [$main_manufacturer->url_rewrite, '00'])}}" title="{{$main_manufacturer->name . $tail}}">
                                                        {!! lazyImage(assetRemote('image/motor-manufacturer/logo-index/' . $main_manufacturer->url_rewrite . '.png'),$main_manufacturer->name . $tail) !!}
                                                        <span class="helper"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        @include('response.pages.summary.partials.reviews', ['title_box' => 'title-main-box-clear'])
                    </div>
                    <div class="box-content">
                        <div class="motor-product-box">
                            <div class="clearfix">
                                <div class="title-main-box-clear">
                                    <h2>
                                        <span>新車．中古車情報　</span>
                                        <a href="{!! MOTOMARKET !!}" title="{!! \Everglory\Constants\Website::MOTOMARKET . $tail !!}" target="_blank">>> 更多車輛情報</a>
                                    </h2>
                                </div>
                            </div>
                            <ul class="row owl-carousel-responsive">
                                @foreach($new_motors as $motor_num => $new_motor)
                                    <li class="col-xs-12 col-sm-12 col-md-2-4 col-lg-2-4 item-product-grid">
                                        @include('response.common.product.j',['motor' => $new_motor, 'motor_num' => $motor_num])
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="clearfix">
                            <div class="title-main-box-clear">
                                <h2>
                                    <span>流行特輯　</span>
                                    <a href="{!! URL::route('collection') !!}" title="{!! '流行特輯' . $tail !!}">>> 更多特輯</a>
                                </h2>
                            </div>
                        </div>

                        <div class="row-advertisement owl-carouse-advertisement">
                            @foreach($assortments as $assortment)
                                <div class="item">
                                    <a class="ct-row-advertisement col-xs-12 col-sm-12 col-md-12 col-lg-12" href="{{$assortment->link}}">
                                        {!! lazyImage($assortment->banner,$assortment->meta_title . $tail) !!}
                                    </a>
                                </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="box-content">
                        <div class="box-ct-new-lineup box-content custom-parts">
                            <div class="clearfix">
                                <div class="title-main-box-clear">
                                    <h2>
                                        <span>新上架品牌及商品　</span>
                                        <a href="{!! URL::route('parts', ['']) !!}?sort=new" title="{!! '新上架品牌及商品' . $tail !!}">>> 查看更多</a>
                                    </h2>
                                </div>
                            </div>
                            <div class="ct-new-custom-part-on-sale-top col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="ul-ct-famous-brand owl-carousel-responsive" data-owl-items="3">
                                    @foreach ($new_product_brands as $key => $node)
                                        <li class="col-xs-12 col-sm-12 col-md-2-4 col-lg-2-4 item-product-grid">
                                            <a href="javascript:void(0)" onclick="getProduct(this, '{{$node->url_rewrite}}',true)" title="">
                                                {!! lazyImage(str_replace('http:','',$node->image),$node->name . $tail) !!}
                                            </a>
                                        </li>
                                        {{-- @if($loop->iteration == 30)
                                            @break
                                        @endif --}}
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            @include('response.common.loading.md')
                            <div class="row">
                                <div id="brand_products" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <div id="pagetop">
        <a href="javascript:void(0);">PAGE TOP</a>
        <i class="fa fa-angle-top" aria-hidden="true"></i>
    </div>
    <!--End Content -->
    @include ( 'response.layouts.partials.footer.index' )
</div>
@include ( 'response.layouts.partials.script' )
<script type="text/javascript" src="{{ assetRemote('plugin/FlexSlider-master/jquery.flexslider-min.js') }}"></script>
<script type="text/javascript">


    $(document).on("change", ".select-motor-manufacturer", function() {
        manufacturer = $(this).val();
        motor = $(this).closest('form').find('.select-motor-model');
        motor.attr('disabled',false).select2('destroy');
        motor.select2({
            width:"100%",
            minimumInputLength: 1,
            language: {
                inputTooShort: function(args) {
                    // args.minimum is the minimum required length
                    // args.input is the user-typed text
                    return "請輸入至少"+ args.minimum + "個字元";
                },
//                errorLoading: function() {
//                    return "發生錯誤";
//                },
                noResults: function() {
                    return "找不到結果";
                },
                searching: function() {
                    return "搜尋中...";
                }
            },
            ajax: {
                url: "{{route('api-motor-manufacturer-model')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        motor_name: params.term, // search term
                        manufacturer: manufacturer
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj.key, text: obj.name };
                        })
                    };
                }
            }
        });
    });

    $('#motor_search_form').submit(function () {
       if(!$(this).attr('action')){
           swal(
               '未選擇車型!',
               '請先選擇車型再進行搜尋!',
               'error'
           );
           return false;
       }
    });
    $(document).on("change", ".select-motor-model", function() {
        motor = $(this).closest('form').find('.select-motor-model');
        $(this).closest('form').attr('action',"{{route('summary','mt')}}/" + motor.val());
    });

    function getProduct(_this, url_rewrite ,firstGetProduct){
        $('.ul-ct-famous-brand li img').removeClass('active');
        $(_this).find('img').addClass('active');
        $('#brand_products').empty();
        $('.loading-box').show();
        if(firstGetProduct){
            slipTo('#brand_products');
        }
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "{{ route('home-newProduct') }}",
            type:"POST",
            data: {'_token': token,'url_rewrite': url_rewrite ,'firstGetProduct' : firstGetProduct},
            success:function(result){
                result = JSON.parse(result);
                $('.loading-box').hide();
                $('#brand_products').html(result.html);
                slider = $('.owl-carousel-history').addClass('owl-carousel').owlCarousel({
                    loop:false,
                    nav:true,
                    margin:5,
                    slideBy : 6,
                    URLhashListener:true,
                    startPosition: 'URLHash',
                    responsive:{
                        0:{
                            items:2
                        },
                        600:{
                            items:3
                        },
                        1000:{
                            items:6
                        }
                    }
                }).find("img").unveil().trigger("unveil");

                if(result.productCount > 7){
                    $('#brand_products .owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
                    $('#brand_products .owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
                }else{

                    $('#brand_products .owl-carousel').find('.owl-prev').remove();
                    $('#brand_products .owl-carousel').find('.owl-next').remove();
                    
                }




            

            },error:function(){
                console.log("ajax fail");
//                alert("error!!!!");
            }
        }); //end of ajax
    }

    @if($new_product_brands->first())
        getProduct('.ul-ct-famous-brand li:first-child a', '{{$new_product_brands->first()->url_rewrite}}',false);
    @endif

    jQuery('.flexslider').flexslider({
        slideshowSpeed: 4000,
        directionNav: false,
        manualControls: ".flex-control-nav li",
        // animation: "slide",
        pasneOnHover: true,
        start: function(){
            jQuery("#main_slider ul").css("display","block");
        }

    });

</script>
</body>
</html>