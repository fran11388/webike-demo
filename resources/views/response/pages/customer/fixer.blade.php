@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')

    <div class="title-main-box">
        <h2>故障排除</h2>
    </div>
    <div class="box-info-oem box-info-compalate">
        若您無法正常登入，您可以請參考下列方式進行故障排除<br>
        1.點選<a class="btn btn-danger" target="_blank" href="{{route('customer-fixer-refresh')}}" title="故障排除 - 「Webike-摩托百貨」">故障排除</a>，由系統進行錯誤排除後，再嘗試登入。<br><br>
        2.線上<a class="btn btn-primary" target="_blank" href="{{route( 'customer-service-proposal-create', 'system' )}}" title="系統及網頁問題回報 - 「Webike-摩托百貨」">填寫表單</a>回報，描述您遇到的問題，工作人員將盡快為您修復。<br><br>
        3.至<a class="btn btn-danger" href="{{route('customer-forgot-password')}}" title="忘記密碼 - 「Webike-摩托百貨」">忘記密碼</a>頁面中，重置您的密碼。<br><br>
    </div>

@stop
@section('script')

@stop