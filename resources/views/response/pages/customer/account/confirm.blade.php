@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>基本資料管理</h2>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('customer-account-confirm') }}">
        <div class="box-items-group-customer-account">
            <div class="box-customer-account">
                <h2>修改資料請先輸入現在的密碼</h2>
            </div>
            <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">帳號 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="steps step-3 txt-customer-account btn-label" type="text" name="email" value="{{ $current_customer->email }}" readonly>
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">輸入密碼 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="steps step-3 txt-customer-account btn-label" type="password" name="password" value="">
                    </td>
                </tr>
            </table>
        </div>
        <div class="row block">
            <div class="hidden-xs col-sm-3 col-md-4"></div>
            <div class="text-center col-xs-12 col-sm-6 col-md-4"> <input class=" btn btn-danger btn-full" type="submit" name="" value="進入修改頁面"></div>
            <div class="hidden-xs col-sm-3 col-md-4"></div>
        </div>
    </form>

@stop
@section('script')

@stop
