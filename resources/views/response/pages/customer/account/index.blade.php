@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>基本資料管理</h2>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="block" method="POST" action="{{ route('customer-account-update') }}">
        @include('response.pages.customer.account.partials.edit')
        <div class="row">
            <div class="hidden-xs col-sm-3 col-md-4"></div>
            <div class="text-center col-xs-12 col-sm-6 col-md-4"> <input class="btn btn-danger btn-full" type="submit" name="" value="確 認 修 改"></div>
            <div class="hidden-xs col-sm-3 col-md-4"></div>
        </div>
    </form>

@stop
@section('script')
    <script type="text/javascript" src="{!! assetRemote('plugin/jquery.twzipcode.min.js') !!}"></script>
    <script type="text/javascript">
        $('#twzipcode').twzipcode({
            readonly:true,
            'countyName': 'address[county]',
            'districtName': 'address[district]',
            'zipcodeName': 'address[zipcode]'
        });

    </script>
    <script type="text/javascript" src="{!! assetRemote('plugin/birthday.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $.ms_DatePicker({
                YearSelector: ".sel_year",
                MonthSelector: ".sel_month",
                DaySelector: ".sel_day"
            });
            $.ms_DatePicker();
        });
    </script>
    <script type="text/javascript" src="{!! assetRemote('js/pages/customer/account.js') !!}"></script>
@stop
