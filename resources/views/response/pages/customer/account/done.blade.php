@extends('response.layouts.1column')
@section('middle')
    <style type="text/css">
        .box-info-account-title{
            width: 610px;
            margin-left: auto;
            margin-right: auto;
        }
        .box-info-account-title img{
            width: 100%;
        }
        .box-info-account-title-text{
            font-size: 1.5rem;
            font-weight: bold;
        }
        .box-info-compalate {
            width: 610px;
            margin-left: auto;
            margin-right: auto;
            float: none;
            border-color: #b8dce8;
        }
        .box-info-oem{
            padding: 10px;
        }
        @media (max-width: 767px){
            .box-info-account-title{
                width: 100%;
            }
            .box-info-compalate {
                width: 100%;
            }
            .box-info-account-title-text {
                font-size: 1rem;
            }
        }
    </style>
    <div class="ct-oem-part-right box-fix-auto">
        @if(\Route::currentRouteName() == 'customer-account-create-done')
            @include('response.pages.customer.account.partials.done.create')
        @elseif(\Route::currentRouteName() == 'customer-account-complete-done')
            @include('response.pages.customer.account.partials.done.complete')
        @endif
    </div>
@stop
@section('script')
    <script type="text/javascript">

        @if(session()->has('pending_route'))
            $( document ).ready(function(){
                setTimeout(function () {
                    //history.go(-2);
                    window.location = "{{ route(session()->get('pending_route')) }}"
                }, 5000);
            });
        @elseif(session()->has('previous_url'))
            $( document ).ready(function(){
                setTimeout(function () {
                    //history.go(-2);
                    window.location = "{{ session()->get('previous_url') }}"
                }, 5000);
            });
        @else
            $( document ).ready(function(){
                setTimeout(function () {
                    //history.go(-2);
                    window.location = "{{ route('shopping')}}"
                }, 20000);
            });
        @endif
    </script>
@stop