@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')

    <div class="title-main-box">
        <h2>基本資料管理</h2>
    </div>
    @if(session()->has('account-edit-success'))
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">會員資料修改成功！</h1>
        </div>
        <div class="center-box">
            <a class="btn btn-default" href="{!! $base_url !!}">回會員中心</a>
        </div>
    @else
        @include('response.pages.customer.account.partials.edit')
        <div class="row block">
            <div class="hidden-xs col-sm-3 col-md-4"></div>
            <div class="text-center col-xs-12 col-sm-6 col-md-4"> <a href="{{ route('customer-account-confirm') }}"  class=" btn btn-danger btn-full">修 改 會 員 資 料</a></div>
            <div class="hidden-xs col-sm-3 col-md-4"></div>
        </div>
    @endif

@stop
@section('script')
    <script type="text/javascript" src="{!! assetRemote('plugin/jquery.twzipcode.min.js') !!}"></script>
    <script type="text/javascript">
        $('#twzipcode').twzipcode({
            readonly:true,
            'countyName': 'address[county]',
            'districtName': 'address[district]',
            'zipcodeName': 'address[zipcode]'
        });

    </script>
    <script type="text/javascript" src="{!! assetRemote('plugin/birthday.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $.ms_DatePicker({
                YearSelector: ".sel_year",
                MonthSelector: ".sel_month",
                DaySelector: ".sel_day"
            });
            $.ms_DatePicker();
        });
    </script>
    <script type="text/javascript" src="{!! assetRemote('js/pages/customer/account.js') !!}"></script>
    <script type="text/javascript">
        $.each( $('table.table-page-customer-account input'), function(){
            $(this).prop('disabled','true');
        });
        $.each( $('table.table-page-customer-account select'), function(){
            $(this).prop('disabled','true');
        });
    </script>
@stop
