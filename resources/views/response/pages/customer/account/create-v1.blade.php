@extends('response.layouts.1column-login')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/login.css') !!}">
@stop
@section('middle')
    <div class="ct-container-login-page box-fix-column">
        <div class="form-login-right box-fix-with">
            <div class="container-login-title">
                <img src="{!! assetRemote('image/customer/account/Webikesignup_logo.png') !!}">
            </div>
            <form method="POST" action="{{ route('customer-account-create') }}">
                {{ csrf_field() }}
                <h1 class="title-create-account">方式1.Facebook 註冊</h1>
                <div class="form-group">
                    <button type="submit" class="btn btn-facebook btn-full btn-signin" onclick="location.href='{{ route('login-facebook') }}';ga('send', 'event', 'account', 'create', 'facebook');">Facebook 註冊</button>
                </div>
                <div class="text-left">
                    <span>
                        - 透過Facebook的API取得您的email資訊，
                    </span>
                    - 請先 <a href="https://www.facebook.com/">登入您的Facebook</a> ，了解更多請閱讀 <a href="{{getBikeNewsUrl('/2016/07/27/%E3%80%90%E6%9C%8D%E5%8B%99%E6%9B%B4%E6%96%B0%E3%80%91facebook%E6%9C%83%E5%93%A1%E5%8A%9F%E8%83%BD%E6%9B%B4%E6%96%B0/')}}">詳細
                        說明</a>。
                </div>
                <h1 class="title-create-account">方式2.快速註冊</h1>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="list-style-type: none;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <label>
                    <input required="" type="text" class="form-control" name="email" placeholder="請輸入E-Mail" >
                </label>
                <label>
                    <input required="" type="password" class="form-control" name="password" placeholder="密碼不能低於6個字元">
                </label>
                <label>
                    <input required="" type="password" class="form-control" name="password_confirm" placeholder="確認密碼">
                </label>
                <div class="form-group">
                    <button type="submit" class=" btn btn-danger btn-fast-registration" onclick="ga('send', 'event', 'account', 'create', 'normal');">E-Mail驗證</button>
                </div>
                <hr>
                <div class="form-group">
                    <button type="button" class="btn btn-default btn-fast-registration" onclick="location.href='{{ route('login') }}';">返回會員登入</button>
                </div>
            </form>
        </div>
    </div>
@stop
@section('script')
@stop