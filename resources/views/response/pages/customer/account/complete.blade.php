@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>完整註冊</h2>
    </div>
    @include('response.common.message.error')
    <form method="POST" action="{{ route('customer-account-update') }}">
        @include('response.pages.customer.account.partials.edit')
        <div class=" text-center col-xs-12 col-sm-12 col-md-12"> <input class=" btn btn-primary border-radius-2 btn-danger btn-confirm-the-changes" type="submit" name="" value="確 認 修 改"></div>
    </form>


@stop
@section('script')
    <script type="text/javascript" src="{!! assetRemote('plugin/jquery.twzipcode.min.js') !!}"></script>
    <script type="text/javascript">
        $('#twzipcode').twzipcode({
            readonly:true,
            'countyName': 'address[county]',
            'districtName': 'address[district]',
            'zipcodeName': 'address[zipcode]'
        });

    </script>
    <script type="text/javascript" src="{!! assetRemote('plugin/birthday.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $.ms_DatePicker({
                YearSelector: ".sel_year",
                MonthSelector: ".sel_month",
                DaySelector: ".sel_day"
            });
            $.ms_DatePicker();
        });
    </script>
    <script type="text/javascript" src="{!! assetRemote('js/pages/customer/account.js') !!}"></script>
@stop
