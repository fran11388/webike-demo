<!--
<div class="title-main-box">
    <h2>歡迎您成為「Webike」會員，您已經完成[快速註冊]</h2>
</div>
-->
<div class="box-info-account-title text-center">
    <img src="{{ assetRemote('image/customer/account/pop-ups-benefit.jpg') }}">
    <div class="box-info-account-title-text">
        您已經完成「快速註冊」，繼續完成<a href="{!! route('customer-account-complete') !!}" title="完整註冊{!! $tail !!}">「完整註冊」</a>。
    </div>
</div>
<div class="box-info-oem box-info-compalate">

    會員福利與服務：<br>
    1.<a href="{{ route('shopping') }}" title="「Webike-摩托百貨」">「Webike-摩托百貨」</a>100元體驗折價券，您可於<a href="{{ route('cart') }}" title="我的購物車 - 「Webike-摩托百貨」">購物車</a>結帳時選擇使用<br>
    2.「Webike」電子報(每週五發送)<br>
    3.首次登錄<a href="{{ route('benefit-event-mybike') }}" title="MyBike">「MyBike」</a>即可獲得100元現金點數。<br>
    4.<a href="{{ route('genuineparts') }}" title="正廠零件查詢系統 - 「Webike-摩托百貨」">正廠零件查詢系統</a><br>
    5.<a href="{{ route('mitumori') }}" title="未登錄商品查詢系統 - 「Webike-摩托百貨」">未登錄商品查詢系統</a><br>
    6.<a href="{{ route('groupbuy') }}" title="團購商品查詢系統 - 「Webike-摩托百貨」">團購商品查詢系統</a><br>
    7.<a href="{{ MOTOMARKET }}" title="「Webike-摩托車市」">「Webike-摩托車市」刊登服務</a><br>
    <br>
    注意事項：<br>
    1.<a href="{{ route('shopping') }}" title="「Webike-摩托百貨」">「摩托百貨」</a>與<a href="http://www.webike.tw/motomarket/" title="「Webike-摩托車市」">「摩托車市」</a>的登入帳號與密碼通用，請您務必寄下您的註冊資料。<br>
    2.目前[電子報]預設為開通狀態，您可以至"<a href="{{ route('customer') }}" title="會員中心 - 「Webike-摩托百貨」">會員中心</a>">"<a href="{{ route('customer-account')}}" title="基本資料設定 - 「Webike-摩托百貨」">基本資料設定</a>"進行修改；<br>或是當您收到電子報後，於下方連結按下「取消訂閱」即可。<br>
    3.相關本站的規定與說明，請至<a href="{{ route('customer') }}" title="會員中心 - 「Webike-摩托百貨」">「會員中心」</a>。<br>
</div>
<div class="center-box">
    @if( session()->has('pending_route'))
        ◎系統將於5秒後自動回到註冊前，不想等待請按下方連結：
        <a class="btn btn-default" href="{{ route(session()->get('pending_route')) }}">回到註冊前頁面</a>
    @elseif( session()->has('previous_url'))
        ◎系統將於5秒後自動回到註冊前，不想等待請按下方連結：
        <a class="btn btn-default" href="{{ session()->get('previous_url') }}">回到註冊前頁面</a>
    @else
        ◎系統將於20秒後自動回到首頁，不想等待請按下方連結：
        <a class="btn btn-default" href="{!! $home_url !!}">回首頁</a>
    @endif

</div>