<div class="title-main-box">
    <h2>「Webike台灣」感謝您，您已經完成【完整註冊】</h2>
</div>
{{--<div class="center-box">--}}
{{--<h1 class="number-text-genuuepart-complate">您的查詢編號為：{!! session('code') !!}</h1>--}}
{{--</div>--}}
<div class="box-info-oem box-info-compalate">

    您享有的服務及功能：<br>
    1.「Webike台灣」電子報(每周五發行)。<br>
    2.<a target="_blank" href="{{ route('genuineparts')}}" title="HONDA、YAMAHA、SUZUKI、KAWASAKI、HARLEY-DAVIDSON、DUCATI正廠機車零件查詢、報價系統-in webike台灣">正廠零件查詢系統</a>。<br>
    3.<a target="_blank" href="{{ route('mitumori')}}" title="未登錄機車零件、騎士用品、安全帽、配件報價、查詢系統-in webike台灣">未登錄商品查詢系統</a>。<br>
    4.<a target="_blank" href="{{ route('groupbuy')}}" title="團購正廠零件、改裝零件、騎士用品、安全帽、配件、各式機車零件查詢、報價系統-in webike台灣">團購商品報價系統</a>。<br>

        5.
        <a target="_blank" href="javascript:void(0)" title="各式機車改裝零件、保養維修工具、custom part-in webike台灣">改裝零件</a>、
        <a target="_blank" href="javascript:void(0)" title="各式騎士用品、安全帽、配件、周邊商品-in webike台灣">騎士用品</a>、
        <a target="_blank" href="{{ route('genuineparts')}}" title="HONDA、YAMAHA、SUZUKI、KAWASAKI、HARLEY-DAVIDSON、DUCATI正廠機車零件查詢、報價系統-in webike台灣">正廠零件</a>
        …等購物功能。
    <br>
    6.購物滿額免運費。	<br>
    7.
        <a target="_blank" href="javascript:void(0)" title="各式機車改裝零件、保養維修工具、騎士用品商品評論 in webike台灣">商品評論</a>及討論功能。
    <br>
    8.<a target="_blank" href="javascript:void(0)" title="機車改裝零件、騎士用品，商品回饋查詢 in webike台灣">投稿評論點數大方送</a>。	<br>
    9.$200元生日禮券。<br>
    10.愛車特惠活動通知。<br>
</div>
<div class="center-box">
    @if( session()->has('back') )
        ◎系統將於5秒後自動轉向完整註冊前頁面，或是選擇前往以下的服務：
        <a class="btn btn-default" href="{{ session()->get('back') }}">回到註冊前頁面</a>
    @else
        ◎系統將於20秒後自動回到首頁，不想等待請按下方連結：
        <a class="btn btn-default" href="{!! $home_url !!}">回首頁</a>
    @endif

</div>