
    <div class="box-items-group-customer-account">
        <div class="box-customer-account">
            <h2>會員基本資料</h2>
        </div>
        <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
            @if(\Route::currentRouteName() != 'customer-account-complete')
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">帳號 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="btn-label steps step-3 txt-customer-account" type="text" name="email" value="{{ $current_customer->email }}" readonly>
                    </td>
                </tr>
            @endif
            @if(\Route::currentRouteName() == 'customer-account')
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">輸入新密碼 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="btn-label steps step-3 txt-customer-account" type="password" name="password" value="">
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">確認新密碼 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="btn-label steps step-3 txt-customer-account" type="password" name="password_confirm" value="">
                    </td>
                </tr>
            @endif
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">暱稱 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="nickname" value="{{ old('nickname') ? old('nickname') : $current_customer->nickname }}">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">姓名 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="realname" value="{{ old('realname') ? old('realname') : $current_customer->realname }}">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">性別 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    @if(old('gender'))
                        <label class="radio-inline"><input type="radio" name="gender" value="1" {{ old('gender') == '1' ? 'checked' : ''  }}>男</label>
                        <label class="radio-inline"><input type="radio" name="gender" value="2" {{ old('gender') == '2' ? 'checked' : ''  }}>女</label>
                    @else
                        <label class="radio-inline"><input type="radio" name="gender" value="1" {{ $current_customer->gender == '1' ? 'checked' : ''  }}>男</label>
                        <label class="radio-inline"><input type="radio" name="gender" value="2" {{ $current_customer->gender == '2' ? 'checked' : ''  }}>女</label>
                    @endif

                </td>
            </tr>

            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">生日 {!! (Route::currentRouteName() == 'customer-account-complete') ? '<span class="font-color-red">※必填</span>' : ''  !!}</td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <ul class="from-owned-vehicles">
                        <li class="col-xs-3 col-sm-3 col-md-3"> <span class="text-day">西元 年  月 日</span> </li>
                        @if(Route::currentRouteName() == 'customer-account')
                            <li class="col-xs-2 col-sm-2 col-md-2">
                                <div class="select2-container select2-container--default" style="width:100%">
                                    <select rel="{{  old('birthday-year') ? old('birthday-year') : ($current_customer->birthday ? date("Y", strtotime($current_customer->birthday)) : '' ) }}" class="select2-selection select2-selection--single sel_year" style="width:100%" disabled>
                                    </select>
                                </div>
                            </li>
                            <li class="col-xs-2 col-sm-2 col-md-2">
                                <div class="select2-container select2-container--default" style="width:100%">
                                    <select rel="{{  old('birthday-month') ? old('birthday-month') : ($current_customer->birthday ? date("m", strtotime($current_customer->birthday)) : '' ) }}" class="select2-selection select2-selection--single sel_month" style="width:100%" disabled>
                                    </select>
                                </div>
                            </li>
                            <li class="col-xs-2 col-sm-2 col-md-2">
                                <div class="select2-container select2-container--default" style="width:100%">
                                    <select rel="{{  old('birthday-day') ? old('birthday-day') : ($current_customer->birthday ? date("d", strtotime($current_customer->birthday)) : '' ) }}" class="select2-selection select2-selection--single sel_day" style="width:100%" disabled>
                                    </select>
                                </div>
                            </li>
                        @else
                            <li class="col-xs-2 col-sm-2 col-md-2">
                                <div class="select2-container select2-container--default" style="width:100%">
                                    <select name="birthday-year" rel="{{  old('birthday-year') ? old('birthday-year') : ($current_customer->birthday ? date("Y", strtotime($current_customer->birthday)) : '' ) }}" class="select2-selection select2-selection--single sel_year" style="width:100%">
                                    </select>
                                </div>
                            </li>
                            <li class="col-xs-2 col-sm-2 col-md-2">
                                <div class="select2-container select2-container--default" style="width:100%">
                                    <select name="birthday-month" rel="{{  old('birthday-month') ? old('birthday-month') : ($current_customer->birthday ? date("m", strtotime($current_customer->birthday)) : '' ) }}" class="select2-selection select2-selection--single sel_month" style="width:100%">
                                    </select>
                                </div>
                            </li>
                            <li class="col-xs-2 col-sm-2 col-md-2">
                                <div class="select2-container select2-container--default" style="width:100%">
                                    <select name="birthday-day" rel="{{  old('birthday-day') ? old('birthday-day') : ($current_customer->birthday ? date("d", strtotime($current_customer->birthday)) : '' ) }}" class="select2-selection select2-selection--single sel_day" style="width:100%">
                                    </select>
                                </div>
                            </li>
                        @endif

                    </ul>
                </td>
            </tr>
            <tr>
                @include('response.pages.customer.mybike.partials.form')
            </tr>
            <tr>
                @include('response.pages.customer.newsletter.partials.form')
            </tr>
        </table>
    </div>
    <div class="box-items-group-customer-account">
        <div class="box-customer-account">
            <h2>商品配送資料</h2>
        </div>
        <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">收件人姓名 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="address[firstname]" value="{{ old('address.firstname') ? old('address.firstname') : $address->firstname }}">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">公司名稱</td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="address[company]" value="{{ old('address.company') ? old('address.company') : $address->company }}" placeholder="公司名稱">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">統一編號</td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="address[vat_number]" value="{{ old('address.vat_number') ? old('address.vat_number') : $address->vat_number }}" placeholder="統一編號">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">行動電話 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="address[mobile]" value="{{ old('address.mobile') ? old('address.mobile') : $address->mobile }}" placeholder="">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">市內電話 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <input class="btn-label steps step-3 txt-customer-account" type="text" name="address[phone]" value="{{ old('address.phone') ? old('address.phone') : $address->phone }}" placeholder="">
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 col-sm-3 col-md-3">配送地址 <span class="font-color-red">※必填</span></td>
                <td class="col-xs-9 col-sm-9 col-md-9">
                    <ul class="form-group from-owned-vehicles">
                        <div id="twzipcode">
                            <li class="col-xs-4 col-sm-4 col-md-4">
                                <div data-name="address[county]" data-role="county" data-style="btn-label width-full"></div>
                            </li>
                            <li class="col-xs-4 col-sm-4 col-md-4">
                                <div data-name="address[district]" data-role="district" data-style="btn-label width-full"></div>
                            </li>
                            <li class="col-xs-4 col-sm-4 col-md-4">
                                <div data-name="address[zipcode]" data-role="zipcode" data-value="{{ old('address.zipcode') ? old('address.zipcode') : $address->zipcode }}" data-style="btn-label width-full"></div>
                            </li>
                        </div>
                    </ul>
                    <input class="btn-label width-full steps step-3 txt-customer-account-end" type="text" name="address[address]" value="{{ old('address.address') ? old('address.address') : $address->address }}" placeholder="">
                </td>
            </tr>
        </table>
    </div>
