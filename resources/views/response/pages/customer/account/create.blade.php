@extends('response.layouts.1column-simplify')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/login.css') !!}">
@stop
@section('page-title')
@stop
@section('middle')
    <div class="ct-container-login-page box-fix-column">
        <div class="form-login-right form-login-right-create box-fix-with ">
            <span>請選擇註冊方式：</span>
            <h1 class="title-create-account">方式1.Facebook 註冊</h1>
            <form method="POST" action="{{ route('customer-account-create') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="button" class="btn btn-default btn-primary border-radius-2 btn-login-with-face" onclick="location.href='{{ route('login-facebook') }}';">Facebook 註冊</button>
                </div>
                        <span>
                            - 透過Facebook的API取得您的email資訊，
                        </span>
                - 請先 <a href="https://www.facebook.com/">登入您的Facebook</a> ，了解更多請閱讀 <a href="{{getBikeNewsUrl('2016/07/27/%E3%80%90%E6%9C%8D%E5%8B%99%E6%9B%B4%E6%96%B0%E3%80%91facebook%E6%9C%83%E5%93%A1%E5%8A%9F%E8%83%BD%E6%9B%B4%E6%96%B0/')}}">詳細
                    說明</a>。
                <h1 class="title-create-account">方式2.快速註冊</h1>

                <label>
                    <input required="" type="text" class="form-control" name="email" placeholder="請輸入E-Mail" >
                </label>
                <label>
                    <input required="" type="password" class="form-control" name="password" placeholder="密碼不能低於6個字元">
                </label>
                <label>
                    <input required="" type="password" class="form-control" name="password_confirm" placeholder="確認密碼">
                </label>
                <div class="form-group">
                    <button type="submit" class=" btn btn-danger border-radius-2 bnt-fast-registration">10秒快速註冊</button>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="list-style-type: none;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </form>
        </div>
        <div class="form-image-left box-fix-auto visible-md visible-lg">
            <img src="{!! $background_image !!}" alt="會員登入{{$tail}}">
        </div>
    </div>
@stop
@section('script')
@stop