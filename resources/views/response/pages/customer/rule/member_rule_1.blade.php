@extends('response.layouts.2columns')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
    <style>
        .buyimg a{
            position:absolute;
        }
        .buyimg a:hover {
            opacity: 0.5;
            cursor:pointer;
            background: white;
        }
        .buyimg .step1 {
            top: 1%;
            width: 47%;
            height: 19%;
            left: 1%;
        }
        .buyimg .step2 {
            top: 11%;
            width: 48%;
            left: 51%;
            height: 18%;
        }
        .buyimg .step3 {
            top: 22%;
            width: 47%;
            height: 19%;
            left: 1%;
        }
        .buyimg .step4 {
            top: 31%;
            width: 48%;
            left: 51%;
            height: 18%;
        }
        .buyimg .step5 {
            top: 42%;
            width: 47%;
            height: 19%;
            left: 1%;
        }
        .buyimg .step6 {
            top: 51%;
            width: 48%;
            left: 51%;
            height: 18%;
        }
        .buyimg .step7 {
            top: 61%;
            width: 47%;
            height: 19%;
            left: 1%;
        }
        .buyimg .step8 {
            top: 71%;
            width: 48%;
            left: 51%;
            height: 18%;
        }
        .buyimg .step9 {
            top: 81%;
            width: 47%;
            height: 19%;
            left: 1%;
        }
    </style>
@stop
@section('left')
	@include('response.pages.customer.partials.menu')
@stop
@section('right')
	<div class="box-container-intruction">
        <div class="title-main-box">
            <h1>會員條約-會員條約說明</h1>
        </div>
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                	<div class="title buyimg" style="position:relative;">
                        @if(hasChangedLogistic())
                            <img src="//img.webike.tw/assets/images/service/instructions_new.jpg">
                        @else
                            <img src="//img.webike.tw/assets/images/service/instructions.jpg">
                        @endif
                         <a class="step1" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#A'])}}"></a>
                         <a class="step2" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#B'])}}"></a>
                         <a class="step3" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#E'])}}"></a>
                         <a class="step4" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#D'])}}"></a>
                         <a class="step5" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#E_c'])}}"></a>
                         <a class="step6" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#F'])}}"></a>
                         <a class="step7" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#G'])}}"></a>
                         <a class="step8" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#H'])}}"></a>
                         <a class="step9" href="{{URL::route('customer-rule',['rule_code'=>'member_rule_2#I'])}}"></a>
                    </div>
	            </li>
	        </ul>
		</div>
	</div>
@stop
