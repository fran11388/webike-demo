@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{assetRemote('/css/pages/intructions.css')}}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="title-main-box">
                <ul class="row">
                    <li class="col-md-12 col-sm-12 col-xs-12 text-left">
                        <h1>購物及服務說明</h1>
                    </li>
                </ul>
            </li>
            <li class="history-table-content history-genuine-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-4 col-sm-12 col-xs-12"><a href="#A">如何挑選商品</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#B">加入購物車</a></li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><a href="#B_a">加價購</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#C">進行結帳</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#E">付款方式說明</a></li>
                </ul>
            </li>
            <li class="history-table-content history-genuine-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-4 col-sm-12 col-xs-12"><a href="#D">確認訂單與交期</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#F">商品備料</a></li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><a href="#G">DHL快遞進口</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#E_c">付款通知(匯款)</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#H">大嘴鳥宅配到府</a></li>
                </ul>
            </li>
            <li class="history-table-content history-genuine-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-4 col-sm-12 col-xs-12"><a href="#I">商品評論說明與規定</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#J">發票開立及規定</a></li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><a href="#K">退款說明</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#M">退換貨說明</a></li>
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="#L">折價券使用說明</a></li>
                </ul>
            </li>
            <li class="history-table-content history-genuine-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-3 col-sm-12 col-xs-12"><a href="{{URL::route('customer-rule',['rule_code'=>'pointinfo'])}}" target="_blank">回饋點數使用說明</a></li>
                </ul>
            </li>
        </ul>
    </div><br>
    <div class="box-container-intruction" id="A">
        <div class="title-main-box">
            <h2>如何挑選商品</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="title"><h2>型錄方式選擇商品：「Webike摩托百貨」目前有38萬項以上的商品，1300多個知名品牌，如何找到您想要的商品？</h2></div>
                    <span>
                        我們提供六大主題館，您可以依需求與喜好，進到各個分類，慢慢挑、慢慢逛。<br>
                        1.開始購物時請參照網頁上方的索引欄，有<a href="{{URL::route('motor')}}">「車型索引」</a>、<a href="{{URL::route('summary',['section'=>'ca/1000'])}}">「改裝零件」</a>、<a href="{{URL::route('summary',['section'=>'ca/3000'])}}">「騎士用品」</a>、<a href="{{URL::route('genuineparts')}}">「正廠零件」</a>、<a href="{{URL::route('outlet')}}">「OUTLET」</a>、<a href="{{URL::route('summary',['section'=>'br'])}}">「精選品牌」</a>、等按鈕，會分別進入各主題的購物首頁。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/1.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                       2.<a href="{{URL::route('motor')}}">「車型索引」</a>-來自台灣、日本、美國、德國、義大利、奧地利、英國、瑞典、西班牙、印度、等國家的車廠品牌，並以排氣量來做分類，快速找到您愛車的專屬頁面，更快速找到您愛車專用的零件，也可以在您愛車的車頁面中查看維修資訊，DIY不求人。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/2.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        3.<a href="{{URL::route('summary',['section'=>'ca/1000'])}}">「改裝零件」</a>- 依據全車各部位做分類，引擎、排氣系統、轉向系統、傳動系統、底盤、煞車系統、電系零件、車身骨架、外觀零件、保養維修工具、維護用品耗材、輪胎，依據您的需求來找到適合您愛車的零件，提供各分類的最新商品、促銷商品、商品評論等資訊。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/3.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        4.<a href="{{URL::route('summary',['section'=>'ca/3000'])}}">「騎士用品」</a>- 依據各種用途的部品做分類，安全帽、騎士服裝配件、鞋類、機車相關精品、行李箱、旅行用品、電子機器類、賽車風格、越野滑胎風格，依據您的需求來挑選最適合您的騎士用品，提供當季最新新品、促銷商品、商品評論等資訊。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/4.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        5.<a href="{{URL::route('summary',['section'=>'br'])}}">「精選品牌」</a>- 約有1400多個國內外知名品牌，如您已經有喜愛的特定品牌，直接從此分類挑選適合您的商品，各品牌頁面也有針對品牌的詳細說明以及對應商品分類等資訊。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/5.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        6.<a href="{{URL::route('genuineparts')}}">「正廠零件」</a>-日本 HONDA、 YAMAHA、 SUZUKI、 KAWASAKI、 美國 HARLEY-DAVIDSON、 德國 BMW、 英國 TRIUMPH、 義大利 DUCATI、 APRILIA、 PIAGGIO、 VESPA、 HUSQVARNA、 MOTOGUZZI、 GILERA、 奧地利 KTM、 泰國 HONDA、 YAMAHA、 十七個進口廠牌。<br>
                        光陽KYMCO、 三陽SYM、 台灣山葉YAMAHA、 台鈴SUZUKI、 宏佳騰AEON、 PGO摩特動力、 哈特佛HARTFORD、 七大國產廠牌，輸入零件料號，免費查詢報價及交期。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/6.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        <a>利用搜尋找尋商品</a>：我已經知道想要找什麼商品，如何搜尋比較快?<br>
                        我們提供人性化的搜尋功能，您可以利用「MyBike」、「關鍵字」、「商品分類」、「品牌」以及「車型搜尋」，快速找到您到的商品。<br>
                        <h3 class="font-bold">功能1，MyBike快速搜尋：</h3><br>
                        登錄「Mybike」之後，我們會紀錄您車子的資料，您之後搜尋商品時可以很輕鬆的搭配「Mybike」功能快速的找到對應您愛車的商品。<br><br>

                        <h3 class="font-bold">功能2，快速搜尋：</h3><br>
                        在每一頁上方索引欄位的下面有一個商品搜尋欄位，輸入您想找的關鍵字，在按「商品搜尋」即可<br>
                        範例：我要找SHOEI X-Eleven的內襯，此時關鍵字就會分為三組""SHOEI""，""X-Eleven""，""內襯""，我們只要將這三組關鍵字以空格隔開，並輸入搜尋欄送出查詢即可。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/7.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        <h3 class="font-bold">功能3，品牌搜尋：</h3><br>
                        當您只有一組關鍵字時，可利用上方的品牌選擇再更精簡搜尋結果，讓您更便利的找到您要的商品。<br>
                        範例：跟上方相同，我一樣要找SHOEI <br>X-Eleven的內襯，此時我先輸入關鍵字搜尋""內襯""，此時搜尋結果出現了300多項各品牌的內襯相關商品
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/8.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        雖然搜尋""內襯""我一樣可以找的到我要的商品，但我就必須要一頁一頁的從300多項商品中挑出我要的，此時我們只要在上方的品牌選擇的部分選擇"SHOEI"，系統就會將SHOEI相關的內襯列出，再依據對應的帽型去挑選內襯即可。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('image/customer/rule/2/9.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        <h3 class="font-bold">功能4，車型搜尋：</h3><br>
                        當您只有一組關鍵字時，可利用上方的車型選擇再更精簡搜尋結果，讓您更便利的找到您要的商品。<br>
                        範例：我要找CB1100的後扶手，此時我一樣可以先搜尋""後扶手""，會搜尋出各品牌對應各種車型使用的後扶手商品，約有200多項。<br>

                        此時我們可以在上方的車型選擇欄位選擇""HONDA""，""1000cc以上""，""CB1100""，再重新送出搜尋，此時會出現對應CB1100車型可以使用的後扶手商品，這樣就會精確的搜尋出您需要的商品。<br>
                        <a>商品詳細頁面</a>：我們提供以下資訊做為您的選購參考。<br>
                        1.商品圖片：您可以左右滑動，瀏覽更多的商品相關圖片。<br>
                        2.品牌名稱：若您想看此品牌更多的對應商品，可以直接點選此連結。<br>
                        3.商品名稱及分類：您可以知道此商品的商品名稱，以及對應的分類位置。<br>
                        4.商品售價：商品定價以及會員價。<br>
                        5.商品編號及對應車型、年份：原廠提供的商品編號，以及此商品可以適用的車型年份規格。<br>
                        6.商品交期：若有庫存的話，我們會顯示庫存量並且告知您大約的交期。<br>
                        7.商品交期查詢：若無庫存的話，您可以使用商品交期查詢功能來進行交期的詢問，約2~3個工作天之內我們會回覆給您。<br>
                        8.購買數量：預設為1，您可以直接變更數量。<br>
                        9.選擇尺寸：依商品不同會有需要選擇尺寸、顏色、年式...等相關規格出現。<br>
                        10.加入購物車及加入代購清單：您要購買請直接加入購物車；若您還在考慮，可以加入代購清單
                        11.商品說明：我們提供原廠的敘述說明，並直接利用"google翻譯"翻譯成中文，若您想要看原文敘述，請點選"顯示原文"即可。若您對商品有任何疑問，請點<a href="{{URL::route('customer-service-proposal')}}">「聯絡我們」</a>我們客服部門會盡速答覆您的問題；若您有安裝或技術上的問題，請點選<a href="{{URL::route('dealer')}}">「Webike-經銷商」</a>，您可以選擇就近的經銷商直接與他們洽詢。<br>
                        12.<a href="{{URL::route('groupbuy')}}">團購商品</a>：如您欲購買的數量超過10個，您可以使用團購商品查詢系統查詢報價，我們將會給您額外的折扣。<br>
                        13.<a href="{{URL::route('review')}}">會員評論</a>：依據其他車友分享的購買經驗，商品圖、使用經驗、注意事項等...做為您參考的依據。<br>
                        14.商品諮詢：若您對商品有任何的疑問，您可以直接點選商品諮詢，我們的服務人員收到您的問題之後會盡快回覆給您。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/10.png')}}">
                    </div><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="B">
        <div class="title-main-box">
            <h2>加入購物車</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="title"><h2>加入購物車：計算您購買的商品金額及運費，若您有我們的折價券或是現金點數，也在可以在此頁使用來折抵消費金額。</h2></div>
                    <span>
                        1.繼續購物：若您還想選購其他商品，請按「繼續購物」回到上商品頁。<br>
                        2.商品數量：變更買數量，EX：想要變更成2個，請在「1」的地方輸入「2」之後按下「修改數量」按鈕。<span class="font-color-red">※如果沒有按下「修改數量」按鈕，系統不會做變更，請注意。</span><br>
                        3.刪除：如需刪除這項商品，請點擊刪除鈕。<br>
                        4.使用折價券：您可以利用下拉選單選擇您擁有的折價券，再按下「輸入」即可享有優惠。<br>
                        5.現金點數折抵：您若擁有"現金點數"可以直接輸入您想折抵的金額（1點可折抵1元），再按下輸入後即可折抵商品金額。<br>
                        6.結帳：確認顯示的內容無誤之後，請點擊頁面內的「結帳」按鈕以前往「結帳頁面」，確認付款方式及配送容。<br>
                        7.加價購：我們提供了3種不同級距的加價購商品，共20項商品以最優惠的價格提供給您。<br>
                        購物車右方為「最近瀏覽的商品」，如有需要可立即加入購物車。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/11_old.png')}}">
                    </div><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="B_a">
        <div class="title-main-box">
            <h2>加價購</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        <a>什麼是「商品加價購功能」?</a><br>
                        商品加價購是提供會員在購物車結帳頁面時，在頁面下方即可以看到「Webike-摩托百貨」嚴選的優質商品，您可以在這邊選購到最低的優惠價格，加買越多越划算!在加價購專區我們也將會定期更新，</span><span class="font-color-red">加價購商品最低「1元」起</span><span>，數量有限，售完為止。<br>
                        <a>如何使用「商品加價購功能」?</a><br>
                        1.於「Webike-摩托百貨」挑選您喜愛的商品後，並點選「加入<a href="{{URL::route('cart')}}">購物車</a>」
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/12.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        2.在購物車結帳頁面下方，即會出現可以加價購買的商品清單。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/13_old.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        3.商品加價購商品區域說明：<br>
                        商品加價購商品共區分為3區，分別為：<br>
                        購物車金額滿3000 元加價購專區<br>
                        購物車金額滿1000元加價購專區<br>
                        購物車金額滿500元加價購專區<br>
                        <span class="font-color-red">※當 購物車 中的商品金額達到加價購金額條件時，「加入購物車」按鈕即會亮起，此時即可將加價購商品加入購物車（如下圖所示）。</span>
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/14.png')}}">
                    </div><br>
                    <span>
                        <a>「商品加價購功能」注意事項</a><br>
                        商品加價購可一次將多種商品加入<a href="{{URL::route('cart')}}">購物車</a><br>
                        單一品項商品的數量為「1個」，單一品項僅能加入購物車「1次」
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="C">
        <div class="title-main-box">
            <h2>進行結帳</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        <a>結帳</a>：最終訂單確認頁面，請仔細核對您的收件資料以及購物商品。<br>
                        1.選擇付款方式：貨到付款（二萬元以下金額）、信用卡、信用卡分期付款、銀行轉帳。<br>
                        2.本公司所有販賣商品均開立發票，若有需要統一編號及公司抬頭，請在「發票」欄位輸入。<br>
                        3.商品配送地址為您當初申請會員時進行完整註冊所輸入的資料，若需要變更，請至「會員中心」->「基本資料管理（帳號/購物）」進行修改，在這邊您也可以選擇「配送時間」及「配送備註」。<br>
                        4.為保障您的權益，「購買同意書」請仔細閱讀「交貨期說明」與「購買條約」，同意後請打勾。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/34_old.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        5.若有其他需要提醒我們的地方，請在「備註事項」輸入您的訊息。<br>
                        6.如需修改購物商品，請點擊藍色按鈕「回購物車修改」。<br>
                        7.請再次確認「*欄位」是否有輸入或同意，最後按下「送出訂單」按鈕，即完成訂購。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/36.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        <a>訂購完成</a><br>
                        1.送出訂單後，系統會產生一組「訂單編號」；之後的訂單若有任何問題，我們將依據「訂單編號」替您服務。<br>
                        2.在訂單成立同時，我們會發送一封「新訂單通知信」，裡面包含您的訂單詳細內容，您可以經由信件內的連結查詢您的訂單資訊。<br>
                        3.若您10分鐘內沒有收到郵件，請檢查您的信箱，信件是否被分類到”垃圾郵件”，如果沒有請您來信order@webike.tw，或是至"聯絡我們"中發訊息詢問，我們隨即幫您解決此問題。<br>
                        4.同一時我們已經幫您確認商品交期，作業須1~2個工作天，交期確認後我們會發送”交期與付款方式通知信”到您的信箱；若超過3個工作尚未收到，請您來信order@webike.tw，或是至"聯絡我們"中發訊息詢問，我們隨即幫您處理。<br>
                        5.若有其問題與建議，請來信service@webike.tw，或是至"聯絡我們"中發訊息詢問。<br>
                        <span class="font-color-red">※如在10分鐘以內沒有收到郵件，請先檢查是否通知信被分到”垃圾郵件”；若依然沒有收到，請來信order@webike.tw，或是使用"聯絡我們"的站內客服系統，並附上訂單編號及會員註冊信箱，隨即幫您補發通知信。</span>
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/17.png')}}">
                    </div><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="E">
        <div class="title-main-box">
            <h2>付款方式說明</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        <a id="E_a">A.貨到付款：</a><br>
                        「Webike-摩托百貨」與台灣宅配通合作提供貨到付款服務，進口商品也可以貨到付款這是全台首創，只要消費金額在兩萬元台幣以下皆可使用。<br>
                        <a>A-1付款流程：</a><br>
                        （1）在結帳頁面選擇”貨到付款”，貨到付款手續費系統會自動計算，詳細手續費請參照以下說明。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/18_old.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （2）按下”送出訂單”，即完成交易，同一時間系統會發送「新訂單通知信」供您確認。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/19.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        <a>A-2貨到付款相關說明：</a><br>
                        （1）由於進口商品的關係，兩萬元以下有支援貨到付款，若超過金額請選擇其他付款方式。<br>
                        （2）台灣宅配通貨到付款手續費：
                    </span><br><br>
                    <div class="history-table-container">
                        <ul class="history-info-table">
                            <li class="history-table-title visible-md visible-lg">
                                <ul>
                                    <li class="col-md-12 col-sm-4 col-xs-4">
                                        <span class="size-12rem">收取現金 (金額 / 單筆)</span>
                                    </li>
                                </ul>
                            </li>
                            <li class="history-table-content history-genuine-table-content">
                                <ul class="col-sm-block col-xs-block clearfix">
                                    <li class="col-md-4 col-sm-12 col-xs-12"><span>1 元 ~ 2,000 元</span></li>
                                    <li class="col-md-2 col-sm-12 col-xs-12"><span>30元</span></li>
                                </ul>
                                <ul class="col-sm-block col-xs-block clearfix">
                                    <li class="col-md-4 col-sm-12 col-xs-12"><span>2,001 元 ~ 5,000 元</span></li>
                                    <li class="col-md-2 col-sm-12 col-xs-12"><span>60元</span></li>
                                </ul>
                                <ul class="col-sm-block col-xs-block clearfix">
                                    <li class="col-md-4 col-sm-12 col-xs-12"><span>5,001 元 ~ 10,000 元</span></li>
                                    <li class="col-md-2 col-sm-12 col-xs-12"><span>90元</span></li>
                                </ul>
                                <ul class="col-sm-block col-xs-block clearfix">
                                    <li class="col-md-4 col-sm-12 col-xs-12"><span>10,001 元 ~210,000 元</span></li>
                                    <li class="col-md-2 col-sm-12 col-xs-12"><span>120元</span></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （3）貨到付款送達不進行簽收付款者，本公司會電話通知請盡速完成簽收動作，若惡意不完成交易者，隨即取消該會員資格，並委請律師進行後續處理。<br>
                        （4）若訂單有部分商品因庫存缺貨或是停止販售，我們會寄送「交期通知信」告知您交期狀態，並且會站內發送通知，請您協助我們處理修改訂單。<br><br>


                        <a id="E_b">B.信用卡：</a><br>
                        本公司採用玉山銀行網路信用卡系統，操作簡單、安全，可以接受的卡種有：VISA、MasterCard以及JCB，刷卡消費不會收取任何手續費，請多加利用。<br>
                        <a id="E_b_1">B-1信用卡付款流程：</a><br>
                        （1）在結帳頁面選擇"信用卡"<br>
                        （2）按下"送出訂單"
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/20_old.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （3）會跳出玉山銀行信用卡交易系統，請輸入信用卡資料，再按下”確定送出”。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/18.jpg')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        備註：<br>
                        信用卡卡號：請輸入您的信用卡卡號。<br>
                        卡片有效日期：請輸入您的卡片有效日期。<br>
                        卡片背面簽名欄末3碼：請輸入您的卡片背面簽名欄末3碼。<br>
                        驗證碼：驗證碼無大小寫限制。<br><br>


                        （4）信用卡交易成功，請按下”完成”結束。<br>

                    </span><br><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/19.png">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span class="font-color-red">
                        如信用卡交易成功之後，請確認有無收到訂單通知信，如尚未收到相關信件，請來信order@webike.tw，或是使用"聯絡我們"的站內客服系統，我們將會協助您處理後續事項。</span><br>
                    <span>
                        （5）信用卡交易失敗，請確認輸入資料是否正確或選擇其他付款方式，按下”回到結帳頁”重新操作。<br>
                    </span><br><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/20.png">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        <a id="E_b_2">B-2信用卡分期付款使用說明：</a><br>
                        本公司採用聯合信用卡以及玉山銀行兩種分期付款系統，有3期、6期0利率以及12期三種選擇，滿足條件即可使用分期優惠。<br><br>
                        分期條件：
                        1.結帳金額滿3千元，可享3期0利率分期。<br>
                        2.結帳金額6千元以上，可選擇3期、6期0利率以及12期分期付款。<br></span>
                    <span class="font-color-red">※結帳未滿3千元僅提供”信用卡一次付清”。</span><br><br>

                    <span class="font-color-red">※團購商品與Outlet商品不提供"分期付款服務"。</span><br><br>
                    <span>
                        目前支援3期、6期0利率銀行：中信銀行、國泰世華、玉山銀行、台新銀行、花旗銀行、永豐銀行、聯邦銀行、遠東銀行、新光銀行、澳盛銀行、大眾銀行、台北富邦、第一銀行 等…共13家發卡銀行。<br><br>

                        支援12期銀行:中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、新光銀行、澳盛銀行、玉山銀行 等…共11家發卡銀行。<br><br>

                        （1）在結帳頁面時，選擇"信用卡分期"
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/21.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （2）依據您結帳的金額，3000元以上至5999元會顯示”3期”0利率選項、6000元以上擇會顯示”3期”、”6期”0利率以及"12期"選項，再依據您的需求去選擇您要的分期方式；同時系統會計算出您首期以及其他期繳款的金額。※目前支援的銀行共有13間，請依據支援的發卡銀行去做選擇，如沒有該銀行發行的卡片，將無法使用分期的功能，還請您多多見諒。 </span>
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/22.png')}}">
                    </div><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/23.png')}}">
                    </div><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/12new.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （3）送出訂單之後，將會跳出聯合信用卡分期系統or玉山信用卡分期系統的頁面，請填入您的信用卡卡號、有效期限、卡片背面後三碼等信用卡資訊，再點選”確認付款”。
                    </span><br><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/24.jpg">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        確認付款之後，會跳出該信用卡銀行的確認頁面，為了確認此筆刷卡訂單為您本人使用，該銀行會發簡訊寄送OTP服務密碼至您的手機，請填入服務密碼之後按”確定”送出即可。
                    </span><br><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/25.jpg">
                    </div><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/26.jpg">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （4）確認刷卡資料-信用卡授權訊息
                    </span><br><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/27.png">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （5）訂購完成後，您可以至”訂單動態及歷史履歷”查看您的訂單狀態。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/24.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        同時系統也會發送”信用卡付款成功通知信”至您的Email，郵件中也會有您的刷卡內容、分期期數、付款金額等資訊。
                    </span><br><br>
                    <div class="text-center">
                        <img src="//img.webike.tw/assets/images/customer/rule/2/29.jpg">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span class="font-color-red">
                        如信用卡交易成功之後，請確認有無收到訂單通知信，若沒有收到請您檢查您的”垃圾信件”或請來信order@webike.tw我們將為您處理。<br><br></span>
                    <span>
                        <a id="E_b_3">B-3信用卡付款相關說明：</a><br>
                        （1）個資法規定我們無法取得或保存消費者的信用卡資料，交易安全由玉山銀行及聯合信用卡系統負責，請顧客放心。<br>
                        （2）本公司不會以任何方式要求會員提供信用卡的相關資料，若有類似的情況，請會員拒絕告知相關資訊，並盡速與我們確認。<br>
                        （3）若會員未使用信用卡購買本公司產品，發生盜刷的情況，請第一時間與您的銀行進行止付，再通知我們進行相關處置。<br>
                        （4）若訂單有部分商品因庫存缺貨或是停止販售，我們會寄送「交期通知信」告知您交期狀態，請您協助我們處理修改訂單。<br>
                        （5）信用卡一次付清可以”下修”刷卡金額；若是取消整筆交易，我們會全額退還給您不會收取任何費用。<br>
                        （6）若</span><span class="font-color-red">使用信用卡分期付款方式，刷卡金額則無法修改，如有遇到缺貨或是交期過長的問題，我們會通知您，並且取消整筆交易，信用卡授權金額也會退還</span><span>；請您重新選購後再進行結帳。<br><br>

                        <a id="E_c">C.匯款轉帳：</a><br>
                        <a>C-1付款流程：</a><br>
                        （1）請在結帳頁面選擇”銀行轉帳”，並按下”送出訂單”
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/25_old.png')}}">
                    </div><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                    <span>
                        （2）送出訂單後您會收到「新訂單通知信」，此時您還不需要付款，我們會先向供貨廠商確認庫存狀態與交期。
                    </span><br><br>
                    <div class="text-center">
                        <img src="{{assetRemote('/image/customer/rule/2/26.png')}}">
                    </div><br><br>
                    <span>
                        （3）約1-2工作天您會收到「交期與付款通知信」，屆時請您依據信件中的金額及匯款帳號進行付款。<br><br>

                        <a>C-2銀行匯款相關說明：</a><br>
                        （1）請依據「交期與付款通知信」中的匯款資訊進行匯款，您也可以至"訂單動態及歷史履歷中"查看匯款資訊。<br>
                        （2）由於庫存隨時變動，請於三個工作天內完成匯款，確保交期不會延誤。<br>
                        （3）若超過匯款期限未完成匯，款該帳號會自動關閉無法再進行匯款，該筆訂單也會自動取消，系統將記錄成為「個人因素取消訂單」，累計兩次「個人因素取消訂單」，我們依據會員條約規定取消您的會員資格。<br>
                        （4）請注意，使用ATM轉帳匯款金額超過銀行單日匯款上限無法轉帳時，需要臨櫃填寫匯款單匯款（跨行轉帳手續費由匯款人負擔）。<br>
                        （5）當您匯款完成，我們確認款項後會發送「付款成功通知信」至您的e-mail信箱，我們會隨即進行「商品備料」作業。<br>
                        （6）訂單交貨期以您匯款完成後的時間開始計算。<br>
                        （7）若完成匯款的訂單，因為庫存缺貨或是停止販售必須取消該項商品訂購時，我們會將該項商品的款項退還至您指定的帳戶，詳細退款的說明請參閱[退款說明]。
                    </span>
                </li>
            </ul>
        </div>
        <div class="box-container-intruction" id="D">
            <div class="title-main-box">
                <h2>確認訂單與交期</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            交期確認中：在我們接到您的訂單後，需有1~2個工作天進行「交期與商品庫存的確認」(如遇到例假日則順延)。<br>
                            關於交期：<br>
                            單一商品交期：本公司定期進口各國各式摩托車相關商品，一般交期約4~6個工作天，(以交期通知的交期為準)，由台灣廠商進貨商品一般交期為2~3個工作天。<br>
                            複數商品交期：若您一次購買複數商品，我們以等待最長時間的商品為統一交期；若部分商品交期過長，客服人員會以使用線上訊息或是電子郵件通知會員，會員可以選擇繼續等待或要求修改「訂單內容」取消該項商品。<br>
                            交期確認之後我們會回覆「交期」與「匯款資訊」供您確認，確認無誤後請您進行匯款。信用卡付款、貨到付款在商品交期正常的狀況下，我們會直接為您向廠商訂購商品，並且回覆「交期通知信」向您告知已經完成對供貨廠商訂購作業。<br></span><span class="font-color-red">
                            備註：如遇交貨期過長的情形，我們提供給您修改訂單的服務。您可以選擇取消交期過長的商品，我們會修改訂單並重新計算金額，請您依據修改後的「訂單內容」進行匯款。信用卡付款會為您調整信用卡刷卡金額。</span><span><br>
                            「交期與付款通知信」、「交期通知信」、「訂單狀態」中標示的交期為預估交期，是依照下訂商品「有庫存」的情況下，從對供貨廠商訂購完成到您收到商品的大約時間。
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="F">
            <div class="title-main-box">
                <h2>商品備料</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            商品備料：Webike物流中心負責備料作業，若您購買複數商品，我們會等候您所有商品到齊後，隨即幫您辦理「DHL國際快遞進口作業」。<br>
                            交期延誤情形：如遇日本、台灣例假日、廠商缺貨或海關查驗…等情況，交期可能超過10天甚至於更長。若有這種情況，我們會發送「交期延誤通知信」通知您， 並告知新的交期。<br>
                            由於本公司以及各供應商的庫存隨時變動，可能會有商品缺貨、停產或是發生特殊情況無法出貨…等情況，很抱歉我們將取消這類商品的訂單，再重新修改訂單；若您已匯款我們會將無法交貨商品的費用退還給您，不便之處請多多包涵。退款說明請參閱[關於退款]
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="G">
            <div class="title-main-box">
                <h2>DHL國際快遞進口作業</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                           DHL國際快遞進口作業<br>
                            本公司採用DHL國際快遞，每周1至周5，每天進口，並投保貨物險，提供會員最快速、最安全的購物服務。<br>

                            正廠零件訂購我們無法判斷尺寸及重量(如引擎總成、整流罩、車殼…等)，購買前請務必思考清楚；尺寸重量限制：材積=長+(寬+高)X2 < 300公分，重量-30kg以下，若超過寄送限制必須取消此項零件訂購。<br>

                            如果訂購商品中，有違反航空運輸限制之商品，例如油類、化學品、清潔劑、高壓氣瓶、鋰電池、罐裝液態物品……等商品或是配件。無法運輸時必須取消該項商品訂購。<br>

                            本公司進口商品皆合法申請報關，依規定繳納進口貨物稅以及營業稅，會員於「Webike摩托百貨」 購物網站所購買的商品，均已包含稅金及相關手續費用，不會額外收取其他費用。<br>

                            每批進口商品因報關程序海關拆箱檢驗，所以商品包裝盒可能會有拆開或是輕微損傷的情況。我們無法保證商品包裝盒的狀況，包裝盒破損無法認定為商品瑕疵。<br>
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="H">
            <div class="title-main-box">
                <h2>台灣國內配送</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            「Webike-摩托百貨」的商品統一由「台灣宅配通」配送<br>
                            運費說明<br>
                            購物金額合計2000元以上，免運費。<br>
                            購物金額合計未滿2000元，運費一律100元。<br>
                            ※大型商品的運費將另外計算 (材積：長+寬+高大於150公分)。<br>
                            ※外島的運費不論購物金額多寡，一律250元。<br>
                            ※外島地區無法提供貨到付款服務。<br>
                            配送地點為偏遠地區及外島的情況:<br>
                            台灣本島配送時間為一個工作天，若配送地點為外島或偏遠地區則商品抵達的時間需增加一天(偏遠地區以台灣宅配通所公布之地區為準)。<br>
                            配送地區限制:<br>
                            以下區域恕不提供宅配通貨件配送服務：<br>
                            ．澎湖地區：望安鄉、七美鄉、虎井島、 桶盤島、大倉嶼、員貝嶼、鳥嶼、花嶼、吉貝嶼。<br>
                            ．金門地區：烏坵、烈嶼、大膽、二膽。<br>
                            ．馬祖地區(連江縣)：南竿鄉、北竿鄉、莒光鄉、東引鄉。<br>
                            ．其它：蘭嶼、綠島、小琉球等外島及郵政信箱。<br>
                            配送日期時間的服務<br>
                            除了配送路線因災害事故等遭到封鎖或收件地址為重災區與其他狀況，導致宅配通人員無法將物品送達之外，宅配服務365天全年無休。<br>
                            配送状況<br>
                            在商品發送後，我們會發送「商品發送完成通知信」，並在郵件內附上您的配送單號碼，同時也會發送訊息通知在您登入網站時提醒您商品配送狀態。<br>
                            配送狀況查詢：台灣宅配通配送狀況查詢<br>
                            無人收件再次配送<br>
                            宅配通的宅配人員會留一張送達通知單在您的地址信箱裡，上面載有投遞日期、時間及營業所聯絡電話，您可依此電話與營業所人員聯絡，約定下次配送時間；若未接獲您的來電，宅配人員將於隔天再次配送，因此請您在收到「商品發送完成通知信」之後應密切注意收件地址收件狀況以免發生無人收件情形影響您收件的時間。(台灣宅配通據點查詢)<br>
                            沒有收到商品的情況<br>
                            收到「商品發送完成通知信」之後，經過2～3日商品還是沒有收到的情況，請向宅配服務窗口詢問，也可以在您登入網站時至會員中心 > 聯絡我們 > 問題提問 > 訂單及配送問題 提出查詢。或寄送電子郵件至 support@webike.tw查詢。<br>
                            備註：因下列理由商品被退回至本公司時、我們將向您索取寄送往返的運費。<br>
                            ●初次配送後經過7日後仍然因您個人因素而無法完成交貨。<br>
                            ●您拒絕收取貨件。
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="I">
            <div class="title-main-box">
                <h2 >商品評論</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            <a>給予評論</a>：當您收到商品使用過之後，歡迎在該商品評論區發表您的使用心得以及對該商品的評論，若經採用後我們會回饋「現金點數」給您。<br>
                            <a>評論方式</a>：<br>
                            1.您可以在 會員中心>我的帳號>訂購履歷>查看訂單 <br>中查詢您所訂購過的商品，點選商品進入商品頁面。<br>
                            2.在該項商品頁面下方的點選「撰寫商品評論拿免費點數」，即可以開始撰寫商品的評論。<br>
                            3.請在[您的感想]撰寫您使用過該項商品的心得，並且上傳您的使用照片。<br>
                            4.當您完成商品評論之後 ，請點選[送出]系統會自動將您的評論回傳到本公司服務中心。<br>
                            5.我們於七個工作天內審核您的評論，您的評論經採用之後會刊登於該商品的「商品評論」中，提供有興趣的車友作為參考。<br>
                            6.評論一經採用之後，系統會將50點匯入您的點數帳戶中作為回饋，您可以使用該點數作為購物折抵。詳情請參閱 ﹝<a href="{{URL::route('customer-rule',['rule_code'=>'pointinfo'])}}">點數回饋使用說明</a>﹞<br>
                            <a>評論審核</a>：<br>
                            1.我們誠摯歡迎您留下評論，但本站對於會員所撰寫的評論保有審核以及決定刊登的權利；若您的內容有包含針對性、攻擊性、不雅的字眼，本站不予刊登。<br>
                            2.撰寫字數限制為100個字元以上，有照片為佳。<br>
                            3.審核標準會依據您提供的資訊多寡與內容豐富性進行給點；若是您的生動內容有超過100個字元，並附有照片，我們會給予50點點數，若您沒有提供照片僅有豐富的文字內容我們只會給予20點點數。<br>
                            </span><span class=".font-color-red">（※正廠零件撰寫請務必上傳照片，如正廠零件沒有上傳照片我們將不會通過審核，還請見諒）。點數給予的審核權由本站決定，而我們不定期也會舉辦點數加倍活動，屆時以活動公告為準。</span><span><br>
                            4.您所撰寫的評論為您個人使用的心得，屬於個人言論自由，不代表本公司立場，審核人員無權也不會修改您的評論內容。<br>
                            5.您所撰寫的內容可能經由他人轉載而出現在其他網站平台、討論區，因著作權歸屬為您而非本站，本站無權限制，因此您留下的評論一經本站刊登之後即為公開資訊，在此特別聲明。<br>
                            6.您上傳的照片視同您同意本站進行有限度的使用，如：站內連結、宣傳素材、facebook分享…等相關活動。<br>
                            7.您提供的商品評論及照片，原則上我們不會進行移除，若您需要移除請隨時告知我們。
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="J">
            <div class="title-main-box">
                <h2>發票開立及規定</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            1.本公司合法進口各國各式摩托車相關商品，購物網站上所有商 品價格均包含進口貨物稅以及營業稅，會員所訂購的商品依法開立統一發票。<br>
                            2.訂購商品以每一筆交易訂單為單位，開立一份二聯或三聯式統一發票(另附商品明細)，與訂購商品一同寄出。(如果需要開立三聯式統一發票請務必於系統結帳時輸入貴公司統一編號及抬頭)。<br>
                            3.我們無法在同一筆訂單重複開立或是分別開立2張以上的統一發票。<br>
                            4. 當您收到發票時請妥善保管，如商品需要退貨或是收到的商品有問題必須更換良品時務必將發票連同商品一起寄回，才能進行銷帳以及退款作業。如果沒有將發票寄回，我們無法確認該商品是經由 「Webike-摩托百貨」 購物網站購買，該商品無法進行退換貨作業，我們會將該商品寄回給您並且需由您付擔運費。<br>
                            5.發票遺失我們無法受理退換貨作業。<a href="#M">﹝退換貨說明﹞</a><br>
                            6.依照財政部現行統一發票使用辦法第九條第一項第四款規定，營業人銷售貨物或勞務與持用簽帳卡（信用卡）之買受人者，除開立二聯式收銀機統一發票者外，應於發票備註欄載明簽帳卡（信用卡）卡號末四碼，以維護消費者權益。
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="K">
            <div class="title-main-box">
                <h2>退款說明</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            退款原因：如發生下列情況我們會將您所付的款項退回給您<br>
                            1.已經確認訂購的商品並且完成信用卡付款或匯款手續，因缺貨或是停止販售，必須取消該項商品。<br>
                            2.收到商品之後因為個人因素必須退貨，於收到商品7日內提出辦理退貨的情況。<br>
                            3.收到商品之後因為個人因素必須更換商品規格(尺寸、顏色、款式....等)，於收到商品7日內提出辦理更換，但供貨廠商已經停止供應欲更換的商品規格時。<br>
                            4.收到的商品為不良品需要辦理更換良品，但供貨廠商已經停止供應相同型式規格的商品時。<br>
                            退款作業說明：<br>
                            1.因廠商缺貨或是停止販售的商品在重新修改訂單後。我們會計算退回的款項，將費用退還給您，其餘有庫存的商品仍然依照正常訂購作業訂購。<br>
                            2.信用卡修改金額或退款：玉山銀行規定只能下修金額，不會超過原本刷卡金額；若是取消整筆交易，我們會全額退還給您不會收取任何費用。<br>
                            3.匯款退款時，請告訴我們銀行名稱與代碼、分行名稱與分行代碼及帳戶號碼與戶名，款項會退到您所指定的帳戶。(※退貨退款時將由退款金額中扣除銀行轉帳手續費NT$30圓)
                        </span><br>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="L">
            <div class="title-main-box">
                <h2>折價券說明</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            何謂折價券?<br>
                            1.此折價券為本公司發行的電子折價券，可以在「Webike-摩托百貨」購物網站使用，折抵購物金額。<br>
                            2.折價券的獲得方式有很多種，主要為新會員註冊體驗折價券、會員生日購物禮券以及各不定期舉辦的活動贈送的折價券等等...皆可以獲得折價券。<br>
                            3.折價券皆有使用期限，在獲得折價券的同時還請注意使用期限，逾期將無法使用。<br><br>
                            折價券顯示<br>
                        </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/31.png')}}">
                        </div>
                    </li>
                    <li>
                        <span>
                            當您登入網站時，紅色會員名稱欄位會顯示您目前所擁有的折價券張數。<br><br>

                            折價券使用方式<br>
                        </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/30_old.png')}}">
                        </div><br><br>
                        <span>
                            1.輸入折價券號碼：請進入「購物車」頁面下方，在「折價券」的欄位裡，有您獲得的折價券，您可選擇下拉式選單，選擇您要使用的折價券，按下「輸入」按鈕。<br><br>
                        </span>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/29_old.png')}}">
                        </div><br><br>
                        <span>
                            2.折價折抵成功：在右下方優惠折抵會顯示折抵金額，則表示輸入成功，並且有成功折抵消費金額。<br>
                            3.每人每次單筆訂單限折抵一張折價券。<br>
                            4.折價券可以與點數一同使用。<br>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="L">
            <div class="title-main-box">
                <h2>訂單取消說明</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            當您結帳完成產生訂單後，在訂單初期的流程，我們會先進行交期確認，再交期確認時期，我們尚未向廠商訂購下單，若您有任何需要取消訂單的需求(如選錯付款方式、選錯商品、選錯數量等....因素)，您可以透過以下方式來自行申請取消訂單。<br><br>
                            1.在會員中心→正在進行中的訂單中可以查看到您剛下訂的訂單狀態，在下圖中您可以看到訂單共有四個流程，當訂單狀態還在「交期確認中」的時候，您可以點選下方的"取消訂單"的連結來進行訂單的取消。<br>
                        </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/50_old.png')}}">
                        </div>
                        <span>
                           2.此時您可以看到要取消的訂單的訂單內容，點選"確認送出"。<br>
                        </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/51_old.png')}}">
                        </div>
                        <span>
                           送出後，您會收到訂單取消申請的編號，請注意，這部分僅為提出申請而已，您的訂單在這時候尚未完成取消。<br>
                        </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/52.png')}}">
                        </div>
                        <span>
                           3.我們在收到您取消訂單的請求後，會盡快幫您進行審核，當確認此筆訂單可以取消時，我們會直接將該筆訂單取消，您也可以在"訂單動態及歷史履歷"中查看目前的狀態。<br>
                           假使您是使用信用卡付款，我們也會一併將您的信用卡資訊做刷退的動作。<br>
                           若您對於此有任何的疑慮，也歡迎隨時使用"聯絡我們"與我們聯繫，感謝。<br>
                        </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/53.png')}}">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="M">
            <div class="title-main-box">
                <h2>退換貨說明</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                            「Webike-摩托百貨」基於商品性質或廠商的要求，關於退換貨我們必須在顧客事前聯絡時確認狀況。依照內容的不同可能會發生不相同的退換貨標準或是有無法退換貨的情況。
                            此外，在「Webike-摩托百貨」所購入的商品，要辦理退換貨時分為 [<a href="#ee">商品退貨</a>] 和 [<a href="#ef">商品換貨</a>]兩種情況，這兩種情況的退換貨條件和流程會有所不同，請您在進行退換貨前確認以下內容。


                            <br><h3 class="font-bold">壹、說明:</h3><br>
                            1.凡是「Webike台灣」會員在「Webike-摩托百貨」所購買商品，可依照消費者保護法第十九條規定，享有商品貨到日起，七天猶豫期權益。但猶豫期並非試用期，您所退回的商品必須是全新狀態(不得有刮傷、破損、受潮)，並且保持完整包裝的完整性，意即必須恢復至您收到商品時的原始狀態（包含商品本體、配件、原廠包裝、保護袋、保麗龍、保證書、隨附文件、贈品….等）。 否則將影響您的退貨權益，也可能依照損毀程度扣除回復原狀之必要費用。<br><br>
                            2.</span><span class="font-color-red">正廠零件訂購並非於「Webike-摩托百貨」商品頁面所刊登之商品，是接受會員委託依照零件料號，向各正廠供應商訂購之摩托車零配件，不是屬於完整的消費性商品。除零件本身有功能異常或是配件缺少，本公司會協助會員向正廠供應商進行更換，除此之外正廠零件下訂後無法取消或辦理退貨，即"委託購買"將不適用於消費者保護法第十九條之規定。</span><span>會員在訂購前，須以專業基礎做為訂購考量，應該事先與相關技術從業商家或是專業技術人員協助確認您所查詢訂購的零件料號是否正確後再購買。以避免會員個人缺乏對技術方面理解而造成錯誤購買，如果無法確認時請您向「Webike-摩托百貨」各地經銷商協助確認或是代為訂購。<br><br>
                            3.由於透過「Webike-摩托百貨」所訂購商品多數由國外進口，所以退換貨流程繁                    雜。從收件當日到換貨、退款完成，整個作業須經過許多步驟。由本公司與各廠商聯繫辦理鑑定與寄送退回。<br><br>
                            4.各廠商之處理程序和時程不相同，退貨退款所需相關業務辦理最長約２5－３０個工作天。更換尺寸、顏色、規格的換貨作業所需相關業務辦理約5-7個工作天。會員辦理退貨前須了解以上狀況，對於本公司處理的時程與方法不得有異議。<br><br>
                            5.更換不同尺寸、顏色、規格商品因價差所產生費用必須另外向您收取，退換貨所產生的運費、手續費以及相關所需費用由本公司負擔。<br><br>
                            6.本公司完成退貨作業會將退貨款項退至會員於退貨申請表填寫指定的帳戶，客戶服務單位另發送訊息外與寄發e-mail通知，完成退貨程序。(※退貨退款時將由退款金額中扣除銀行轉帳手續費)<br><br>
                            <br><h3 class="font-bold">貳、無法退換貨情況:</h3><br>
                            除《通訊交易解除權合理例外情事適用準則》之規定外，若退貨換時有下列情形，亦將影響您的退換貨權利或需負擔毀壞之費用：<br>
                            1.透過本公司系統對廠商訂購[客製化訂製商品]和[未登錄商品]除商品本身瑕疵、運送過程中故障損壞或是與訂製內容不相符之外無法辦理退換貨。<br>
                            2.使用團購系統訂購的商品為批發販售，無法辦理退換貨。<br>
                            3.結帳時加購當期加價購商品無法單獨辦理退貨。<br>
                            4.發票遺失或是損毀無法證明由本公司購買的商品。<br>
                            5.商品包裝毀損、封條移除、吊牌拆除、貼膠移除或標籤拆除等情形。<br>
                            6.商品頁面中已經標示不可退貨或不可取消的商品<br>
                            7.個人衛生商品(內衣褲)及衣飾類(如衣服、褲子、襪子、圍巾)等經剪標或下水，或商品有不可回復原狀之髒污或磨損痕跡。<br>
                            8.各廠牌摩托車正廠零件。<br>
                            9.影音光碟及圖書商品 (如包含電腦軟體、書籍、錄音帶、錄影帶、CD、VCD、DVD等)屬智慧財產權之商品，除商品本身有瑕疵外，一經拆封恕，不接受退貨。<br>
                            10.不經由退換貨流程填寫退換貨申請與聯絡就直接寄回本公司的商品<br>
                            11.其他逾越檢查之必要或可歸責於您之事由，致商品有價值降低或毀損、損失或其他變更者。<br><br>
                            <br><h3 class="font-bold">叁、備註:</h3><br>
                            1.更換商品規格停產或限定商品等、無法取得進行更換時該項商品必須辦理退貨、退款。(※退款時將由退款金額中扣除轉帳手續費)<br>
                            2.於商品收回流程中如有破損、遺失等等情況本公司不負擔一切責任，收回前請確認包裝完整。<br>
                            <span　class="font-color-red">請注意※若無事先填寫退換貨申請資料提出申請就將商品寄回，我們會再將商品寄送退還給您，不做退換貨的處理，運費也將由您自行負擔。</span　class="font-color-red"><br><br>
                        <br><h3 class="font-bold" id="ee">肆、商品退貨:</h3><br>
                        [商品退貨流程]<br><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/32.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">1如需申請辦理商品退貨作業請於收到商品7日內至　 <a href="{{URL::route('shopping')}}">摩托百貨></a><a href="{{URL::route('customer')}}">會員中心></a><a href="{{URL::route('customer-history-order')}}">訂單動態及歷史履歷</a> 點選<a>[查看訂單]</a>檢視訂單內容。(7日退貨期限起算日為-物流業者送達會員收件地址簽收時算起)超過時間無法申請辦理。</span><br><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/33_old.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">2.確認訂單內容商品為欲退貨商品點選[退換貨申請]。</span><br><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/34.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                            3. 退換貨選擇<br>
                            　(1)選擇 [退貨]。<br>
                            　(2)選擇退貨商品與數量。<br>
                            　(3)點選下一步填寫退貨申請表。<br>
                            </span><br><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/35.png')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                4.填寫退貨申請表<br>
                                (1)確認收件人資訊，如果需要更改可以點選修改更新資料，標示※符號為必填欄位務必填寫完整。<br>
                                (２)選擇退貨原因。<br>
                                (３)選擇商品狀態，並拍照上傳。（照片上傳容量限制為５ＭＢ）<br>
                                (４)選擇取件時間。（請確認取件時間為可交付宅配人員回收的時段取件相關事項可以填寫於下方備註欄以便
                                　宅配人員確認）<br>
                                (５)填寫退款資訊。（退款資訊為退貨退款之重要資料，請務必確實填寫）<br>
                                (６)確認並勾選退貨準備事項。（退貨重要項目請確實核對並勾選）。<br>
                                (７)點選確認送出。<br>
                            </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/36.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                ５.退貨申請完成<br>我們在收到您提出的退貨申請後，將會為您處理後續退貨作業，您可以在退換貨履歷中　查詢退貨狀態。<br>
                        請注意※<br>
                        1若無事先填寫退換貨申請資料提出申請就將商品寄回，我們會再將商品寄送退還給您，不做退換貨的處理，運費也將由您自行負擔。<br>
                        2如果出貨時打包寄送紙箱已遺失，請另使用其他紙箱包覆於商品原廠包裝之外，切勿直接於商品原廠包裝（彩盒）上黏貼紙張或書寫文字。若商品原廠包裝(彩盒)損毀或遺失將無法辦理退貨。<br><br>
                            </span><br><br>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                            <img src="{{assetRemote('image/customer/rule/2/55.jpg')}}"><br><br>
                            <span>
                                    [切勿直接於商品原廠包裝（彩盒）上黏貼紙張或書寫文字]
                                </span>
                            <br><br>
                        </div>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                            <img src="{{assetRemote('image/customer/rule/2/56.jpg')}}"><br><br>
                            <span>
                                    [切勿直接於商品原廠包裝（彩盒）上黏貼膠帶]
                                </span>
                            <br><br>
                        </div>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                            <img src="{{assetRemote('image/customer/rule/2/57.jpg')}}"><br><br>
                            <span>
                                    [請使用紙箱將退回商品妥善打包，以保持退回商品完整性]
                                </span>
                            <br><br>
                        </div>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                        </div>

                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/37.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                ６.退貨狀態查詢<br>
　                               點選退換貨申請單號碼即可檢視該筆退貨商品的處理狀態。<br>
                            </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/38.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                ７.退貨狀態顯示<br>
                            　您所申請的退貨狀態都會顯示於狀態列，退貨過程當中如果有任何相關問題都可以在退換貨問題資訊中提出詢問，客服專員將會回答您退貨的問題。<br>
                            </span><span>
                            請注意商品收件時間，將出貨明細、發票與商品打包完整交由宅配人員取回，並保留宅配取貨單據至退貨流程結束。 (如果沒有將發票與出貨明細收回，我們無法確認該商品為本公司所販售)。若因為以上因素無法進行退貨作業，我們必須將商品寄回給您，並且需由您付擔運費。<br></span><br>
                        <h3 class="font-bold" id="ef">伍、商品換貨:</h3><br>
                        <span>
                            我們期望客戶所收到的每項商品都是完整合用的的，但萬一送達的商品有下列情形發生我們提供商品更換的服務。<br>
                            1.收到的商品規格不符合您的需求，需要更換尺寸、顏色、樣式…等需求。<br>
                            2.收到錯誤的商品與商品頁面所顯示的規格形式不同，需要更換正確商品。<br>
                            3.收到的商品有瑕疵、故障品，需要更換良品。<br>
                            請在收到商品的7天內提出申請。我們會盡速為您交換良品並且運費由本公司負擔。如遇良品無法取得的情況，則在收到您的商品後，為您辦理退貨、退款的處理。原則上，關於商品到達後超過7日的商品，不再為顧客做更換良品的處理。請在商品到達後盡早做商品確認。回收更換商品寄送的運費由本公司負擔。<br><br>
                            [商品換貨流程]<br>
                            </span><br>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/39.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                1如需申請辦理商品換貨作業請於收到商品7日內至　 摩托百貨>會員中心>訂單動態及歷史履歷 點選[查看訂單]檢視訂單內容。(7日退貨期限起算日為-物流業者送達會員收件地址簽收時算起)超過時間無法申請辦理。<br>
                            </span>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/40.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                2.確認訂單內容商品為欲退貨商品點選[退換貨申請]。<br>
                            </span>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/41.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                3. 退換貨選擇<br>
                                　(1)選擇 [換貨]。<br>
                                　(2)選擇換貨商品與數量。<br>
                                　(3)點選下一步填寫退貨申請表。<br><br>
                            </span>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/42.png')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                4.填寫換貨申請表<br>
                                (1)確認收件人資訊，如果需要更改可以點選修改更新資料，標示※符號為必填欄位務必填寫完整。<br>
                                (2)選擇換貨原因。<br>
                                (3)複製要更換商品網頁貼上<br>
                                (4)填寫欲更換商品的規格(尺寸、顏色、款式……)<br>
                                (5)如有任何需求可於下方備註說明欄填寫說明<br>
                                (6) (7) 選擇商品狀態，並拍照上傳。（照片上傳容量限制為５ＭＢ）<br>
                                (8)選擇取件時間。（請確認取件時間為可交付宅配人員回收的時段取件相關事項可以填寫於下方備註欄以便
                                　宅配人員確認）<br>
                                (9)確認並勾選退貨準備事項。（退貨重要項目請確實核對並勾選）。<br>
                                (10)點選確認送出。<br>
                            </span>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/43.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                ５.換貨申請完成<br>我們在收到您提出的換貨申請後，將會為您處理後續換貨作業，您可以在退換貨履歷中　查詢退貨狀態。<br>
                                請注意※<br>
                                1若無事先填寫退換貨申請資料提出申請就將商品寄回，我們會再將商品寄送退還給您，不做退換貨的處理，運費也將由您自行負擔。<br>
                                2如果出貨時打包寄送紙箱已遺失，請另使用其他紙箱包覆於商品原廠包裝之外，切勿直接於商品原廠包裝（彩盒）上黏貼紙張或書寫文字。若商品原廠包裝(彩盒)損毀或遺失將無法辦理退貨。
                                <br><br>
                            </span>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                            <img src="{{assetRemote('image/customer/rule/2/55.jpg')}}"><br><br>
                            <span>
                                    [切勿直接於商品原廠包裝（彩盒）上黏貼紙張或書寫文字]
                                </span>
                            <br><br>
                        </div>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                            <img src="{{assetRemote('image/customer/rule/2/56.jpg')}}"><br><br>
                            <span>
                                    [切勿直接於商品原廠包裝（彩盒）上黏貼膠帶]
                                </span>
                            <br><br>
                        </div>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                            <img src="{{assetRemote('image/customer/rule/2/57.jpg')}}"><br><br>
                            <span>
                                    [請使用紙箱將退回商品妥善打包，以保持退回商品完整性]
                                </span>
                            <br><br>
                        </div>
                        <div class="text-center col-md-6 col-sm-12 col-xs-12">
                        </div>

                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/44.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                ６.退貨狀態查詢<br>
　                               點選退換貨申請單號碼即可檢視該筆換貨商品的處理狀態。<br><br>
                            </span>
                        <div class="text-center">
                            <img src="{{assetRemote('image/customer/rule/2/45.jpg')}}">
                        </div><br><br>
                        <span class="font-color-red">
                                ７.退貨狀態顯示<br>
                                　您所申請的退貨狀態都會顯示於狀態列，退貨過程當中如果有任何相關問題都可以在退換貨問題資訊中提出詢問，客服專員將會回答您退貨的問題。<br>
                            </span>
                        <span>
                                請注意商品收件時間，將出貨明細、發票與商品打包完整交由宅配人員取回，並保留宅配取貨單據至退貨流程結束。 (如果沒有將發票與出貨明細收回，我們無法確認該商品為本公司所販售)。若因為以上因素無法進行換貨作業，我們必須將商品寄回給您，並且需由您付擔運費。
                            </span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script>
        $('#apply').click(function(){
            var href = $('#apply1').attr('href');
            window.open(href);
        });
    </script>
@stop
