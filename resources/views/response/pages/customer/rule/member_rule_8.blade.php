@extends('response.layouts.2columns')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
@stop
@section('left')
	@include('response.pages.customer.partials.menu')
@stop
@section('right')
	<div class="box-container-intruction">
        <div class="title-main-box">
            <h1>會員條約-隱私權保護政策</h1>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                	<span>
                		本公司非常重視您的個人隱私。從您身上取得的個人資料將遵從以下隱私權保護對策管理。隱私權保護對策是用來說明弊公司在資料使用上皆遵從個人資料保護法，請您安心使用弊公司的服務。
                	</span>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>個人資料的使用</h2></div>
                	<span>
                		您在本公司網站「Webike台灣」所輸入的住址、姓名、E-mail信箱等等個人資料會使用在本公司網站賣場上販賣物品的發送或交貨期等聯絡、提供評價等其他服務與商品資訊的E-mail發送等服務上。另外，會員登錄時所填的性別與生日將以不特定對象的形式用於改善服務的統計上。
                	</span><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>個人資料的共用</h2></div>
                	<span>
                		您的個人資料在本公司提供服務上有其必要性時、本公司會將所需部分的資料交給本公司夥伴。例如：委託運輸公司運送貨物時，就必須將您的住址、姓名、電話號碼交給運輸公司。您的資料本公司除了用於提供服務之外，不會用於其他目的。
                	</span><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>個人資料的公開</h2></div>
                	<span>
                		本公司不會將您的個人資料對第三者作公開、販售、借出等等動作。但是，如遇以下情況則會進行公開。<br>
                		1.已事先取得您的同意時。<br>
					 	2.將經過統計與分析所得到的非特定型態的資料公開或交付商業合作夥伴等等第三方的情況。<br>
					 	3.為了達成個人資料的使用目的，交給與本公司簽訂保密條約的業務委託方與商業合作夥情況。<br>
					 	4.法令或政府機關有要求的情況。<br>
					 	5.人的生命、身體、財產等等有急迫危險，有緊急的必要性時。<br>
                	</span><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>關於個人資料、會員登錄資料的查詢、修改、消除</h2></div>
                	<span>
                		您的會員登錄資料可由此（<a href="{{URL::route('customer-account')}}" title="webike會員登錄資料頁面">會員登錄資料頁面</a>）進行查詢、修改，想要消除會員登錄（退會）請來信至service@webike.tw。
                	</span>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>關於Cookie</h2></div>
                	<span>
                		當您在本公司網站使用賣場購物車或會員服務等等時，我們將使用Cookie技術來記錄您的資訊。<br>
						Cookie是一種當您訪問本公司網站時，用來識別您的電腦的一種小檔案。由伺服器發送存取在您的電腦裡。我們使用Cookie來充實服務，以期望您能夠更便利地使用。<br>
						您也可以拒絕Cookie存取，請確認您使用的瀏覽器設定。如果拒絕Cookie存取，將無法使用本網站賣場提供的購物車等等基本功能。這種情況，本公司廣告客戶的Cookie使用遵照廣告客戶自身的隱私權保護對策，廣告客戶或其他公司無法參閱本公司網站的Cookie。
                	</span><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>關於安全性憑證（SSL）</h2></div>
                	<span>
                		本公司網站在您的個人資料面內加入了SSL安全性憑證。這是一種將檔案暗號化的傳輸方式。透過使用SSL您的個人情報或信用卡號會被轉變為暗號並傳送到伺服器，如此一來便能防止監聽或竄改情報，讓個人資料可以安全傳輸。
                	</span><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>關於Access log</h2></div>
                	<span>
                		本公司網站會將進入網站的各位以Access log形式記錄起來。Access log會記錄機器的IP位址、瀏覽器種類、網域名、進入時刻等等資料。Access log將會被活用在公司統計分析上，但與個人情報無連接，並且不會被利用在上述目的之外的用途。
                	</span><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>隱私權保護對策的修改</h2></div>
                	<span>
                		本公司可能會對隱私權保護對策的內容進行修改。如有重要變更，會在官方網站進行公布。
                	</span>
	            </li>
	        </ul>
		</div>
	</div>
@stop
