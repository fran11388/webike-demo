<td class="col-xs-3 col-sm-3 col-md-3">擁有的車輛</td>
<td class="col-xs-9 col-sm-9 col-md-9" id="field_mybike">
    <ul class="form-group from-owned-vehicles clearfix ready-detect" id="bike-first-row">
        <li class="col-xs-3 col-sm-3 col-md-3">
            <select class="select2 select-motor-manufacturer">
                <option value="">請選擇廠牌</option>
                @foreach ($motor_manufacturers as $_manufacturer)
                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                @endforeach
            </select>
        </li>
        <li class="col-xs-3 col-sm-3 col-md-3">
            <select class="select2 select-motor-displacement" disabled>
                <option>cc數</option>
            </select>
        </li>
        <li class="col-xs-3 col-sm-3 col-md-3">
            <select name="my-bike[]" class="select2 select-motor-model" disabled>
                <option value="">車型</option>
            </select>
        </li>
        <li class="col-xs-3 col-sm-3 col-md-3">
            <input class=" btn btn-danger border-radius-2 btn-remove-vehicle" type="button" value="移除">
        </li>
    </ul>
    @foreach($motors as $motor)
        <ul class="form-group from-owned-vehicles clearfix">
            <li class="col-xs-3 col-sm-3 col-md-3">
                <select class="select2 select-motor-manufacturer btn-label" readonly>
                    <option>{{ $motor->manufacturer->url_rewrite }}</option>
                </select>
            </li>
            <li class="col-xs-3 col-sm-3 col-md-3">
                <select class="select2 select-motor-displacement btn-label" readonly>
                    <option selected>{{ $motor->displacement }}</option>
                </select>
            </li>
            <li class="col-xs-3 col-sm-3 col-md-3">
                <select name="my-bike[]" class="select2 select-motor-model btn-label" readonly>
                    <option value="{{ $motor->url_rewrite }}" selected>{{ $motor->name }}</option>
                </select>
            </li>
            <li class="col-xs-3 col-sm-3 col-md-3">
                <input class=" btn btn-danger btn-remove-vehicle" type="button" value="移除">
            </li>
        </ul>
    @endforeach
    <ul class="form-group from-owned-vehicles" id="bike-last-row">
        <li class="col-xs-3 col-sm-3 col-md-3">
            <input class=" btn btn-primary btn-add-new-vehicle" type="button" name="" value="新增車輛">
        </li>
    </ul>
</td>