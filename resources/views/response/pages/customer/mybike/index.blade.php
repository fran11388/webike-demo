@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>Mybike管理</h2>
    </div>
    @include('response.common.message.error')
    @include('response.common.message.success')
    <form method="POST" action="{{ route('customer-mybike') }}">
        <div class="box-items-group-customer-account">
            <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
                <tr>
                    @include('response.pages.customer.mybike.partials.form')
                </tr>
            </table>

        </div>
        <div class="row">
            <div class="hidden-xs col-sm-3 col-md-4"></div>
            <div class="text-center col-xs-12 col-sm-6 col-md-4"> <input class=" btn btn-danger btn-full btn-confirm-the-changes" type="submit" name="" value="確 認 修 改"></div>
            <div class="hidden-xs col-sm-3 col-md-4"></div>
        </div>
    </form>
@stop
@section('script')
    <script type="text/javascript" src="{!! assetRemote('js/pages/customer/account.js') !!}"></script>
@stop
