@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>購買商品撰寫評論</h2>
    </div>
    <div class="history-transparent-box">
        <h3>歡迎分享您的使用心得，簡單三步驟即可獲得回饋點數! (<a href="{{route('review-step')}}" target="_balnk">使用說明</a>)</h3>
    </div>
    <form action="">
        <div class="history-table-container">
            @if($collection->count())
                <ul class="history-info-table">
                    <li class="history-table-title">
                        <ul>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">購買時間</span></li>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">品牌名稱</span></li>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">品名</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">是否撰寫</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">撰寫評論</span></li>
                        </ul>
                    </li>
                    <li class="history-table-content history-genuine-table-content">
                        @foreach($collection as $item)
                            <ul>
                                <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{!! $item->created_at !!}</span></li>
                                <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"><a href="{!! route('summary','br/'.$item->source->manufacturer_url_rewrite) !!}" target="_blank">{!! $item->manufacturer_name !!}</a></span></li>
                                <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"><a href="{!! route('product-detail',$item->url_rewrite) !!}" target="_blank">{!! $item->product_name !!}</a></span></li>
                                <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{!! count($item->source->reviews) > 0 ? 'O' : 'X' !!}</span></li>
                                <li class="col-md-2 col-sm-2 col-xs-2">
                                    <span class="font-16px">
                                        @if(count($item->source->reviews) > 0)
                                            已撰寫
                                        @else   
                                            <a class="blue-text" href="{!! route('review-create',$item->url_rewrite) !!}">撰寫評論</a>
                                        @endif
                                    </span>
                                </li>
                            </ul>
                        @endforeach
                    </li>
                </ul>
                <div class="row ct-pagenav">
                    {!! $pager !!}
                </div>
            @else
                <div class="container">
                    <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
                </div>
            @endif
        </div>
        <div class="center-box">
            <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
        </div>
    </form>

@stop
