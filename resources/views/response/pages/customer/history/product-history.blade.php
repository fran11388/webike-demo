@extends('response.layouts.2columns')
@section('style')
    {{--     <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
        <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}"> --}}
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/product-history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <?php
    $role_id = Auth::user()->role->id;
    $price_string = '';
    if (in_array($role_id, [\Everglory\Constants\CustomerRole::GENERAL])) $price_string = '會員價';
    if (in_array($role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF])) $price_string = '經銷價';
    ?>

    <div class="title-main-box">
        <h2>購入商品履歷</h2>
    </div>
    <div class="history-transparent-box">
        <h3>您好，{{ $current_customer->realname }}，以下是購入商品履歷，商品詳細資訊請直接點擊該商品。這裡提供購買習慣分析功能。</h3>
    </div>

    <form method="get" action="{{route('customer-history-purchased')}}">
        <div class="product-history-select">
            <select name="m_id" id="" class="w-select-down select2">
                <option value="">品牌</option>
                {{-- 以下為購買過商品的品牌才顯示 --}}
                @foreach($manufacturers as $manufacturer)
                    <option value="{{$manufacturer->id}}"
                            @php
                                $m_id = request()->get('m_id');
                            @endphp
                            @if($manufacturer->id==$m_id)
                            selected
                            @endif
                    >{{$manufacturer->name}}</option>
                @endforeach

            </select>

            <select name="c_identifier" id="" class="w-select-down select2">
                <option value="">商品分類</option>

                @foreach($first_layer_categories as $url_rewrite=>$name)
                    <option value="{{$url_rewrite}}"
                            @php
                                $c_identifier=request()->get('c_identifier');
                            @endphp
                            @if($url_rewrite == $c_identifier)
                            selected
                            @endif
                    >{{$name}}</option>
                @endforeach
            </select>

            <input name="keyword" type="search" placeholder="搜索" class="w-search"
                   value="{{request()->get('keyword')}}">

            <button type="submit" class="w-botton-search"></button>
            <button type="button" class="w-botton-b w-botton-clean"
                    onclick="window.location.replace('{{route('customer-history-purchased')}}')">清除搜索條件
            </button>

            <div class="fixclear"></div>
        </div>


        <div class="product-history-sort">
            <div class="product-history-view">
                <h2>顯示方式</h2>
                <form>
                    <div class="product-history-view-t">
                        <input id="product-history-view-t" type="radio" name="display" value="time"
                               onclick="chooseDisplay(this)"
                        >
                        <span>時間</span>
                    </div>
                    <div class="product-history-view-p">
                        <input id="product-history-view-p" type="radio" name="display" value="product"
                               onclick="chooseDisplay(this)"
                        >
                        <span>商品</span>
                    </div>

                </form>
            </div>
            <p>共:<span>{{$collection->count()}}</span>項商品</p>

            <select name="sort" id="" class="w-sort-down" onchange="submit()">
                <option class="soflow-hidden" disabled="" selected="" hidden="" value="">商品排序</option>
                @foreach($sortingWays as $key=>$value)
                    <option value="{{$key}}"
                            @php
                                $sort = request()->get('sort');
                            @endphp
                            @if($key==$sort)
                            selected
                            @endif
                    >{{$value}}</option>
                @endforeach
            </select>
            <div class="fixclear"></div>
        </div>
    </form>

    <div class="product-history-content">
        <ul class="product-history-list-p">
            <li class="product-history-list-title">
                <ul>
                    <li>商品</li>
                    <li>商品資訊</li>
                    <li>購買習慣</li>
                    {{-- <li>備註</li> --}}
                    <li></li>
                    <div class="fixclear"></div>
                </ul>
            </li>
            {{--顯示方式-商品--}}
            @foreach($collection as $item)
                <li class="product-history-list-project">
                    <ul class="product-history-list-project-t">
                        <li>
                            <a href="{{ route('product-detail',$item->sku) }}" target="_blank">
                                <img src="{{ $item->productAccessor ? $item->productAccessor->getImage() : NO_IMAGE }}"
                                     alt="">
                            </a>
                        </li>
                        <li>
                            <p>
                                品名：<span
                                        class="dotted-text1">{{$item->details->where('attribute','product_name')->first()->value}}</span>
                            </p>
                            <p>商品編號：<span class="dotted-text1">{{$item->model_number}}</span></p>
                            <p>
                                品牌：<span
                                        class="dotted-text1">{{$item->details->where('attribute','manufacturer_name')->first()->value}}</span>
                            </p>
                            <p>目前{{$price_string}}：<span class="dotted-text1">
                                    @if($item->productAccessor)
                                        @if(in_array($item->product_type,[\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY]))
                                            NT$ --
                                        @else
                                            NT$ {{number_format($item->productAccessor->getFinalPrice($item->customer))}}
                                        @endif
                                    @else
                                        商品已下架
                                    @endif
                                </span></p>

                            <?php
                            $product_option_string = $item->details->where('attribute', 'product_options')->first()->value;
                            $options = \Ecommerce\Service\HistoryService::parseProductOptions($product_option_string);
                            ?>
                            @foreach($options as $question=>$answer)
                                <p class="product-option-{{$item->sku}}">{{$answer}}</p>
                                {{--<p>{{$question}}：<span>{{$answer}}</span></p>--}}
                            @endforeach

                            <div>
                                {{-- <i class="w-botton-b" style="display: none">有庫存</i> --}}

                                <i class="w-botton-b stock_tag_{{$item->sku}}" id="" style="display: none">有庫存</i>
                                <a href="{{route('review-create',$item->sku)}}" target="_blank"
                                   class="w-botton-y">撰寫評論</a>
                                <span>評論即可獲得點數回饋</span>
                            </div>
                        </li>
                        <li>
                            <p>購買次數：<span>{{$item->buy_times}}</span></p>
                            <p>購買總數：<span>{{$item->buy_count}}</span></p>
                            <p>購買頻率：<span>
                                    @if($item->buy_fq==0)
                                        --
                                    @else
                                        {{number_format($item->buy_fq)}}
                                    @endif
                                    天</span></p>
                        </li>
                        <li>
                            @if($item->product_type == 0 and $item->productAccessor)
                                <button class="w-botton-r execute-btn" data-product_type="{{$item->product_type}}"
                                        data-sku="{{$item->sku}}">加入購物車
                                </button>
                            @elseif($item->product_type == 1 and isset($manufacture_suppliers[$item->manufacturer_id]))
                                <button class="w-botton-b execute-btn" data-product_type="{{$item->product_type}}"
                                        data-model_number="{{$item->model_number}}"
                                        data-manufacturer_name="{{$item->manufacturer->name}}"
                                        data-supplier_id="{{$manufacture_suppliers[$item->manufacturer_id]}}"
                                        data-note="{{$item->note}}">一鍵查詢
                                </button>
                            @endif
                        </li>
                        <div class="fixclear"></div>
                    </ul>
                    <ul class="product-history-list-project-c">
                        <li>
                            <h3>訂單日期</h3>
                            <h3>價格</h3>
                            <h3>訂單編號</h3>
                            <h3>購買數量</h3>
                            <h3>備註</h3>
                            <div class="fixclear"></div>
                        </li>

                        {{--                        @if($sku_grouped_order_items->get($item->sku)!==null)--}}
                        @foreach($sku_grouped_order_items->get($item->sku) as $order_item)
                            <li>
                                <span>{{$order_item->order->created_at->toDateString()}}</span>
                                <span>NT$ {{number_format($order_item->price)}} </span>
                                <span><a href="{{route('customer-history-order-detail',$order_item->order->increment_id)}}">{{$order_item->order->increment_id}}</a></span>
                                <span>{{$order_item->quantity}}</span>
                                <span>
                                        <a href="javascript:void(0)">
                                            <i class="fas fa-pen" data-item-id="{{$order_item->id}}"></i>
                                        </a>
                                    </span>
                                <div class="fixclear"></div>
                            </li>
                        @endforeach
                        {{--@endif--}}

                        <div class="fixclear"></div>
                    </ul>
                </li>
            @endforeach

        </ul>
        <ul class="product-history-list-t">
            <li class="product-history-list-title">
                <ul>
                    <li>購買時間</li>
                    <li>購買價格</li>
                    <li>品牌</li>
                    <li>品名</li>
                    <li>備註</li>
                    <li></li>
                </ul>
                <div class="fixclear"></div>
            </li>

            {{--顯示方式-時間--}}
            @foreach($collection as $item)
                @foreach($sku_grouped_order_items->get($item->sku) as $order_item)
                    <li class="product-history-list-project">
                        <ul class="product-history-list-project-t" id="item_{{$order_item->sku}}">
                            <li>{{$order_item->order->created_at->toDateString()}}</li>
                            <?php
                            //                            if($order_item->price==8359) dd($order_item);
                            ?>
                            <li>NT$ {{number_format($order_item->price)}}</li>
                            <li>{{$order_item->details->where('attribute','manufacturer_name')->first()->value}}</li>
                            <li>{{$order_item->details->where('attribute','product_name')->first()->value}}</li>

                            <li>
                                <a href="javascript:void(0)">
                                    <i class="fas fa-pen dotted-text1" data-item-id="{{$order_item->id}}"></i>
                                </a>
                            </li>
                            <li>
                                @if($order_item->product_type == 0 and $order_item->productAccessor)
                                    <button class="w-botton-r execute-btn"
                                            data-product_type="{{$order_item->product_type}}"
                                            data-sku="{{$order_item->sku}}">加入購物車
                                    </button>
                                @elseif($order_item->product_type == 1 and isset($manufacture_suppliers[$order_item->manufacturer_id]))
                                    <button class="w-botton-b execute-btn"
                                            data-product_type="{{$order_item->product_type}}"
                                            data-model_number="{{$order_item->model_number}}"
                                            data-manufacturer_name="{{$order_item->manufacturer->name}}"
                                            data-supplier_id="{{$manufacture_suppliers[$order_item->manufacturer_id]}}"
                                            data-note="{{$order_item->note}}">一鍵查詢
                                    </button>
                                @endif
                            </li>
                            <div class="fixclear"></div>
                        </ul>
                        <ul class="product-history-list-project-c">
                            <li>
                                <a href="{{ route('product-detail',$order_item->sku) }}" target="_blank">
                                    <img src="{{ $order_item->productAccessor ? $order_item->productAccessor->getImage() : NO_IMAGE }}"
                                         alt="">
                                </a>
                            </li>
                            <li>
                                <h3>目前商品狀況</h3>
                                <p>商品編號：<span class="dotted-text1">{{$order_item->model_number}}</span></p>
                                <p>目前{{$price_string}}：<span>
                            @if($order_item->productAccessor)
                                            @if(in_array($order_item->product_type,[\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY]))
                                                NT$ --
                                            @else
                                                NT$ {{number_format($order_item->productAccessor->getFinalPrice($order_item->customer))}}
                                            @endif


                                        @else
                                            商品已下架
                                        @endif
                                </span></p>

                                <div>
                                    <i class="w-botton-b stock_tag_{{$order_item->sku}}" id=""
                                       style="display: none">有庫存</i>
                                    {{--<button class="w-botton-y">撰寫評論</button>--}}
                                    <a href="{{route('review-create',$order_item->sku)}}" target="_blank"
                                       class="w-botton-y">撰寫評論</a>
                                    <span>評論即可獲得點數回饋</span>
                                </div>

                            </li>
                            <li>
                                <h3>購入歷史</h3>
                                <p>訂單日期：<span>{{$order_item->order->created_at->toDateString()}}</span></p>
                                <p>單價：<span>NT$ {{number_format($order_item->price)}}</span></p>
                                <p>
                                    訂單編號：<a href="{{route('customer-history-order-detail',$order_item->order->increment_id)}}"><span>{{$order_item->order->increment_id}}</span></a>
                                </p>
                                <p>購買數量：<span>{{$order_item->quantity}}</span></p>
                                <?php
                                $product_option_string = $order_item->details->where('attribute', 'product_options')->first()->value;
                                $options = \Ecommerce\Service\HistoryService::parseProductOptions($product_option_string);
                                ?>
                                @foreach($options as $question=>$answer)
                                    <p class="">{{$answer}}</p>
                                    {{--<p>{{$question}}：<span>{{$answer}}</span></p>--}}
                                @endforeach
                            </li>
                            <li>
                                <h3>習慣分析</h3>
                                <p>購買次數：<span>{{$item->buy_times}}</span></p>
                                <p>購買總數：<span>{{$item->buy_count}}</span></p>
                                <p>購買頻率：<span>
                                    @if($item->buy_fq==0)
                                            --
                                        @else
                                            {{number_format($item->buy_fq)}}
                                        @endif

                                        天</span></p>

                                {{--<p>上次購買日：<span>{{$item->order->created_at->toDateString()}}</span></p>--}}

                            </li>
                            <div class="fixclear"></div>
                        </ul>

                    </li>
                @endforeach


            @endforeach

        </ul>
    </div>

    @include ( 'response.pages.customer.history.product-history-remarks' )
    @include ( 'response.pages.customer.history.product-history-number' )
    <div class="text-center">
        {!! $collection->appends(request()->except('page'))->links() !!}
    </div>

@stop
@section('script')
    <script>
        jQuery(document).ready(function ($) {
            $('.product-history-list-project-t').click(function (event) {
                /* Act on the event */
                sku = $(this)[0].id.split('_')[1];
                {{--$.ajax({--}}
                {{--url: '{{route('api-stock')}}',--}}
                {{--method: 'GET',--}}
                {{--data: {--}}
                {{--sku: sku,--}}
                {{--},--}}
                {{--success: function (result) {--}}
                {{--result = JSON.parse(result);--}}
                {{--if (result[sku].stock > 0) {--}}
                {{--$('#stock_tag_' + sku).css('display', 'block');--}}
                {{--}--}}
                {{--}--}}
                {{--});--}}

                event.stopPropagation();
                $(this).parent().find('.product-history-list-project-c').stop().slideToggle(250);
            });

            $.ajax({
                url: '{{route('api-stock')}}',
                method: 'GET',
                data: {
                    sku: "{{implode(',',$collection->pluck('sku')->all())}}",
                },
                success: function (result) {
                    result = JSON.parse(result);
                    for (let [key, value] of Object.entries(result)) {
                        if (value.stock > 0) {
                            $('.stock_tag_' + key).css('display', 'block');
                        }
                        // console.log(key, value);
                    }
                }
            });
        });

        // 選擇枝用參數
        var sku_original = "";

        $('#execute').click(function () {
            $(this).addClass('disabled').attr('disabled', 'disabled');
            var windowElement = $(this).closest('.product-history-number');
            var product_type = $(windowElement).find('input[name="product_type"]').val();
            if (product_type == 0) {
                var qty = $(windowElement).find('#amount').val();
                var sku = $(windowElement).find('input[name="sku"]').val();
                var select_judje = $(".select-type select").find("option:selected");
                var select_judje_key = true;

                // 判斷選擇枝 有沒有被選過
                select_judje.each(function(){
                    
                    if($(this).val() == "original"){
                        $("#execute").attr('disabled', false);
                        select_judje_key = false;

                        alert("您還有規格沒選取");
                        return false;
                    }
                })

                console.log(qty)

                if(select_judje_key == true){

                    var option_name = "";
                    var option_name_first = $(".select-type select").find("option:selected").html();
                    var option_mi = $(".select-type select").find("option:selected").attr("mi_str");
                    var option_gc = $(".select-type select").find("option:selected").attr("gc_str");

                    if($(".select-type select").find("option:selected").size() > 1){
                        $(".select-type select").find("option:selected").eq(1).each(function(){
                            option_name = option_name_first + "|" + $(this).html();
                            // console.log($(this).html())
                        })
                    }else{
                        option_name = option_name_first;
                        // console.log(option_name_first);
                    }
                    $.ajax({
                            url: '{{route('product-detail-info')}}',
                            data:{
                                sku: sku_original,
                                na: option_name,
                                gc: option_gc,
                                mi: option_mi,
                            },
                            method: 'GET',
                            success: function (result) {
                                // console.log(result.sku);
                                // return result.sku;

                                addCart(result.sku, qty);
                            },
                            error: function () {
                            }
                        });
                }



            } else if (product_type == 1) {
                var manufacturer = $(windowElement).find('input[name="manufacturer"]').val();
                var divide = $(windowElement).find('input[name="divide"]').val();
                var model_numbers = [];
                var qty = [];
                var note = [];
                model_numbers[0] = $(windowElement).find('input[name="model_number"]').val();
                note[0] = $(windowElement).find('input[name="note"]').val();
                qty[0] = $(windowElement).find('#amount').val();
                doEstimate(model_numbers, qty, note, manufacturer, divide);
            } else {
                unlock();
                swal('error!', 'selected action not exist.', 'error');
            }

        });

            

        $(document).ready(function () {

            $(".execute-btn").click(function (event) {
                var windowElement = $('.product-history-number');
                var product_type = $(this).attr('data-product_type');
                if (product_type == 0) {
                    $(windowElement).find('input').val('1');
                    $(windowElement).find('.genuine-section').hide();
                    $(windowElement).find('input[name="sku"]').val($(this).attr('data-sku'));
                    $(windowElement).find('input[name="product_type"]').val(product_type);

                    //選擇枝1.拿到原始商品sku
                    sku_original = $(this).attr('data-sku');

                    $(".select-type").removeAttr("hidden");

                    var _token = $('meta[name="csrf-token"]').attr("content");
                    $.ajax({
                        url: '{{route('api-product-options')}}',
                        method: 'GET',
                        data: {
                            sku: sku_original,
                            _token: _token
                        },
                        success: function (result) {
                            $("#execute").attr('disabled', false);
                            var selectType = $('.product-history-number .select-type'); 
                            var selectAdd = "";

                            for(i=0 ; i<result.length ; i++){
                                // console.log(result[i].options[i].name);
                                // console.log(result[i].options.length);
                                // console.log(Object.keys(result[i].options).length)
                                var optionsAdd = "",
                                    gc_str = "",
                                    mi_str = "";

                                for(j=0 ; j < Object.keys(result[i].options).length ; j++){

                                    var str2 = '<option value="selected" mi_str="'+ result[i].options[j].manufacturer_id +'"' + 'gc_str="'+ result[i].options[j].group_code + '">' + result[i].options[j].name + '</option>';

                                    optionsAdd = optionsAdd + str2;
                                }

                                // console.log(optionsAdd);
                                var str = '<select class="w-select-down"><option value="original" disabled selected hidden>' + result[i].name + '</option>' + optionsAdd + '</select>'

                                selectAdd = selectAdd + str;
                            }

                            selectType.html(selectAdd);
                        },
                        error: function () {
                        }
                    });

                } else if (product_type == 1) {
                    $(windowElement).find('input').val('1');
                    $(windowElement).find('.genuine-section').show();
                    $(windowElement).find('input[name="model_number"]').val($(this).attr('data-model_number'));
                    $(windowElement).find('input[name="product_type"]').val(product_type);
                    $(windowElement).find('input[name="note"]').val($(this).attr('data-note'));
                    $(windowElement).find('input[name="manufacturer"]').val($(this).attr('data-manufacturer_name'));
                    if ($(this).attr('data-supplier_id') == 1) {
                        $(windowElement).find('input[name="divide"]').val("{{\Everglory\Constants\CountryGroup::IMPORT['url_code']}}");
                    } else {
                        $(windowElement).find('input[name="divide"]').val("{{\Everglory\Constants\CountryGroup::DOMESTIC['url_code']}}");
                    }

                    //正廠零件沒有選擇枝
                    $(".select-type").attr("hidden",true);
                }
                event.stopPropagation();//停止事件冒泡
                $(".product-history-number-background").toggle();


            });



            


            $(".product-history-remarks-title i").click(function (event) {
                event.stopPropagation();
                $(".product-history-remarks-background").toggle();
            });
            $(".product-history-number-title i").click(function (event) {
                event.stopPropagation();
                $(".product-history-number-background").toggle();
            });
            $(".product-history-search-title i").click(function (event) {
                event.stopPropagation();
                $(".product-history-search-background").toggle();
            });

            var productSelectId = document.getElementById('product-history-view-p'),
                timeSelectId = document.getElementById('product-history-view-t'),
                productView = document.querySelector('.product-history-list-p'),
                timeView = document.querySelector('.product-history-list-t');

            let display = getCookie('display');
            if (display == 'time') {
                timeView.style.display = 'block';
                productView.style.display = 'none';
                timeSelectId.setAttribute('checked', 'checked');
            } else if (display == 'product') {
                productView.style.display = 'block';
                timeView.style.display = 'none';
                productSelectId.setAttribute('checked', 'checked');
            }
            else {
                timeView.style.display = 'block';
                productView.style.display = 'none';
                timeSelectId.setAttribute('checked', 'checked');
            }

        });

        function unlock() {
            $('#execute').removeClass('disabled').removeAttr('disabled', 'disabled');
        }

        function addCart(sku, qty) {
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('cart-update')}}',
                data: {
                    sku: sku,
                    qty: qty,
                    _token: _token
                },
                success: function (result) {
                    unlock();
                    $(".product-history-number-background").toggle();
                    swal('已加入購物車。', '成功', 'success')
                },
                error: function () {
                    unlock();
                }
            });
        }

        function doEstimate(model_numbers, quantity, note, manufacturer, divide) {
            if (model_numbers.length > 0 && quantity.length > 0) {
                swal({
                    title: '您確定要送出嗎?',
                    text: "點選「確認送出」後將送出查詢。",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '送出查詢',
                    cancelButtonText: '取消',
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#777',
                }).then(function () {
                    var step = false;
                    $.ajax({
                        url: "{!! route('realTime-genuineparts-estimate') !!}",
                        data: {
                            _token: $('meta[name=csrf-token]').prop('content'),
                            api_usage_name: manufacturer,
                            step: step,
                            divide: divide,
                            syouhin_code: model_numbers,
                            qty: quantity,
                            note: note
                        },
                        type: 'POST',
                        dataType: 'json',
                        timeout: 200000,
                        success: function (result) {
                            unlock();
                            if (result.success) {
                                link = "{{ \URL::route('genuineparts-estimate-redirect-estimate-done') }}" + "?code=" + result.code;
                                $('.realtime-genuineparts').attr('href', link).show(1000);
                                $('.realtime-genuineparts-text').show();
                                setTimeout(function () {
                                    window.open(link);
                                }, 100000);

                                if (result.code) {
                                    stepOther(result);
                                }
                            } else {
                                if (result.type == 'not_login') {
                                    window.location.href = "{{ \URL::route('login') }}";
                                } else {
                                    alert(result.message);
                                }
                            }

                        },
                        error: function (xhr, ajaxOption, thrownError) {
                            unlock();
                            window.open("{{ \URL::route('customer-history-genuineparts') }}");
                        }
                    });

                    swal({
                        title: '正廠零件查詢中',
                        html: "<span>請稍後約10秒鐘<span><span class='realtime-genuineparts-text' style='display:none;'>，若不想等待請按 <a href='' class='realtime-genuineparts' style='text-decoration: underline'>\"查詢完畢再通知\"</a></span>",
                        allowOutsideClick: false,
                        onOpen: function () {
                            swal.showLoading()
                        }

                    }).then(
                        function () {
                        },
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                                console.log('I was closed by the timer')
                            }
                            unlock();
                        }
                    );

                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        unlock();
                        return false;
                    }
                })
            }
        }

        function stepOther(result) {
            var estimate_code = result.code;
            var divide = result.divide;
            var curl_para = result.curl_para;
            var step = true;
            $.ajax({
                url: "{!! route('realTime-genuineparts-estimate') !!}",
                data: {
                    _token: $('meta[name=csrf-token]').prop('content'),
                    estimate_code: estimate_code,
                    step: step,
                    divide: divide,
                    curl_para: curl_para
                },
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    unlock();
                    if (result.real_time) {
                        window.open("{{request()->getSchemeAndHttpHost()}}" + '/customer/history/genuineparts/detail/' + result.code);
                    } else {
                        window.open("{{ \URL::route('genuineparts-estimate-redirect-estimate-done') }}" + "?code=" + result.code);
                    }
                },
                error: function (xhr, ajaxOption, thrownError) {
                    unlock();
                }
            });
        }

        function chooseDisplay(elmnt) {
            var productSelectId = document.getElementById('product-history-view-p'),
                timeSelectId = document.getElementById('product-history-view-t'),
                productView = document.querySelector('.product-history-list-p'),
                timeView = document.querySelector('.product-history-list-t');

            setCookie('display', elmnt.value, 30);
            if (elmnt.value == 'time') {
                timeView.style.display = 'block';
                productView.style.display = 'none';
            }
            if (elmnt.value == 'product') {
                productView.style.display = 'block';
                timeView.style.display = 'none';
            }

        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        //  getOrderItems('172918,172919')
        function getOrderItems(order_item_ids) {
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('rest-order-items')}}',
                method: 'GET',
                data: {
                    ids: order_item_ids,
                    _token: _token
                },
                success: function (result) {
                    return result;
                },
                error: function () {

                }
            });
        }


        //  putOrderItems(172918,'備註內容...')
        function putOrderItems(order_item_id, remark) {
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('rest-order-items')}}',
                method: 'PUT',
                data: {
                    id: order_item_id,
                    remark: remark,
                    _token: _token
                },
                success: function (result) {
                    // console.log(result)
                },
                error: function () {

                }
            });
        }

        function getProductOptions(sku){
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('api-product-options')}}',
                method: 'GET',
                data: {
                    sku: sku,
                    _token: _token
                },
                success: function (result) {
                    console.log(result)
                },
                error: function () {

                }
            });
        }



        jQuery(document).ready(function ($) {

            var AllI = $(".product-history-list-project-t li a i");
            var totalID1 = $(".product-history-list-project-t li a i").eq(0).attr('data-item-id');
            var totalID = totalID1;

            // 商品顯示
            var v2_AllI = $(".product-history-list-project-c li span a i");
            var v2_totalID1 = $(".product-history-list-project-c li span a i").eq(0).attr('data-item-id');
            var v2_totalID = v2_totalID1;



            for( i=1 ; i<AllI.length ; i++ ){
                
                var ID = AllI.eq(i).attr('data-item-id');
                totalID =  totalID + "," + ID;

                var v2_ID = v2_AllI.eq(i).attr('data-item-id');
                v2_totalID = v2_totalID + "," + v2_ID;
            }



            //1.畫面進入時 載入所有備註並顯示

            var _token = $('meta[name="csrf-token"]').attr("content");

            $.ajax({
                url: '{{route('rest-order-items')}}',
                method: 'GET',
                data: {
                    ids: totalID,
                    _token: _token
                },
                success: function (result) {

                    for( i=0 ; i<AllI.length ; i++ ){
                        var AllI_ID = AllI.eq(i).attr('data-item-id');

                            if(result[AllI_ID].remark == null || result[AllI_ID].remark == ""){
                                
                            }else{
                                AllI.eq(i).html(result[AllI_ID].remark );
                                AllI.eq(i).removeClass('fa-pen');
                            }
                    }


                    for( i=0 ; i<v2_AllI.length ; i++ ){
                        var v2_AllI_ID = v2_AllI.eq(i).attr('data-item-id');

                            if(result[v2_AllI_ID].remark == null || result[v2_AllI_ID].remark == ""){
                                
                            }else{
                                v2_AllI.eq(i).html(result[v2_AllI_ID].remark );
                                v2_AllI.eq(i).removeClass('fa-pen');
                            }
                    }

                    },
                error: function () {
                    }
            });


            //2.點擊備註按鈕 

            AllI.click(function(event){
                
                var id = $(this).attr('data-item-id');
                var remark_Window_view = $(".product-history-remarks").find('textarea');

                //挑出視窗動作
                 event.stopPropagation();
                $(".product-history-remarks-background").toggle();

                //標記共同視窗 暫存屬性
                $(".product-history-remarks").attr("variableId",id);

                //共同視窗 寫入內容
                
                var _token = $('meta[name="csrf-token"]').attr("content");
                $.ajax({
                    url: '{{route('rest-order-items')}}',
                    method: 'GET',
                    data: {
                        ids: id,
                        _token: _token
                    },
                    success: function (result) {
                        remark_Window_view.val(result[id].remark);
                    },
                    error: function () {
                    }
                });
            });


            //2.點擊備註按鈕 V2
            v2_AllI.click(function(event){
                var v2_id = $(this).attr('data-item-id');
                var v2_remark_Window_view = $(".product-history-remarks").find('textarea');

                // 挑出視窗動作
                 event.stopPropagation();
                $(".product-history-remarks-background").toggle();

                //標記共同視窗 暫存屬性
                $(".product-history-remarks").attr("variableId",v2_id);

                //共同視窗 寫入內容
                
                var _token = $('meta[name="csrf-token"]').attr("content");
                $.ajax({
                    url: '{{route('rest-order-items')}}',
                    method: 'GET',
                    data: {
                        ids: v2_id,
                        _token: _token
                    },
                    success: function (result) {
                        v2_remark_Window_view.val(result[v2_id].remark);
                    },
                    error: function () {

                    }
                });

            });


            //儲存 備註內容並更新畫面顯示

            $('.product-history-remarks-title button').click(function (event) {

                var id2 = $(this).parent().parent().attr("variableId");
                var text = $(this).parent().parent().find('textarea').val();

                var remark_view = $(".product-history-list-project-t li a i[data-item-id = '" + id2 + "']");

                var v2_remark_view = $(".product-history-list-project-c li span a i[data-item-id = '" + id2 + "']");

                // console.log(removeAllSpace(text));


                //視窗內有無內容
                if(text != "" && removeAllSpace(text) != ""){

                    //存入資料庫
                    putOrderItems(id2,text);

                    //關閉視窗
                    $(".product-history-remarks-background").hide();
                    remark_view.removeClass('fa-pen');
                    v2_remark_view.removeClass('fa-pen');

                    remark_view.html(text);
                    v2_remark_view.html(text);

                    
                }else if(text=="" || removeAllSpace(text) == ""){

                    //存入資料庫
                    putOrderItems(id2,"");

                    $(".product-history-remarks-background").hide();
                    remark_view.addClass('fa-pen');
                    v2_remark_view.addClass('fa-pen');

                    remark_view.html(text);
                    v2_remark_view.html(text);
                }
            });


            // 去除所有空格
            function removeAllSpace(str) {
                return str.replace(/\s+/g, "");
            }
            

        });


    </script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
@stop

