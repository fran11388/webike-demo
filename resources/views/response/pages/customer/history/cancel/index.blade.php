@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <style type="text/css">
        .history-page-state a span.action{
            position: absolute;
            bottom: -25px;
            color: #0069bf;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>取消訂單</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>訂單編號：{{ $document->code }}</h1>
            <hr class="visible-xs">
        </div>
    </div>
    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="history-table-title">
                <ul>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">訂購日期</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">付款方式</span></li>
                    <li class="col-md-6 col-sm-6 col-xs-6"><span class="font-16px">收件資訊</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">訂單狀態</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                <ul>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{{ $document->created_at }}</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{{ $document->payment  }}</span></li>
                    <li class="col-md-6 col-sm-6 col-xs-6">
                        <span class="font-16px">收件人: {{ $document->customer_name }}</span>
                        <span class="font-16px">地址:{{ $document->address }}</span>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{!! $document->state !!}</span></li>
                </ul>
            </li>
        </ul>
    </div>
    <form method="POST" action="{{route('customer-history-order-detail-cancel',$document->code)}}">
        <div class="history-table-container">
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-title">
                    <ul>
                        <li class="col-md-1 col-sm-1 col-xs-1"></li>
                        <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">商品名稱</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">單價</span></li>
                        <li class="col-md-1 col-sm-1 col-xs-1"><span class="font-16px">數量</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">小計</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">商品詳細</span></li>
                    </ul>
                </li>
                <li class="history-table-content history-order-detail-table-content">
                    @foreach($collection as $key => $item)
                        <ul>
                            <li class="col-md-1 col-sm-1 col-xs-1"><span class="font-16px">{{$key + 1}}</span></li>
                            <li class="col-md-4 col-sm-4 col-xs-4"><span>{{$item->product_name}}</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{{$item->price}}</span></li>
                            <li class="col-md-1 col-sm-1 col-xs-1"><span class="font-16px">{{$item->quantity}}</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{{$item->total}}</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><a href="{{URL::route('product-detail', $item->url_rewrite)}}" target="_blank"><span class="font-16px">商品詳細</span></a></li>
                        </ul>
                    @endforeach
                    <ul class="history-table-content-resum-block">
                        <li class="col-md-7 col-sm-7 col-xs-7"><span class="font-16px">訂單備註：{{$document->comment}}</span></li>
                        <li class="col-md-5 col-sm-5 col-xs-5">
                            <ul>
                                <li>
                                    <span class="title">使用點數:</span>
                                    <span class="value">{{ $document->rewardpoints_amount }}</span>
                                </li>
                                <li>
                                    <span class="title">商品金額合計:</span>
                                    <span class="value">{{ $document->grand_total }}</span>
                                </li>
                                <li>
                                    <span class="title">運費:</span>
                                    <span class="value">{{ $document->shipping_amount }}</span>
                                </li>
                                <li>
                                    <span class="title">貨到付款手續費:</span>
                                    <span class="value">{{ $document->payment_amount }}</span>
                                </li>
                                <li>
                                    <span class="title">折扣:</span>
                                    <span class="value">{{ $document->coupon_amount }}</span>
                                </li>
                                <li class="history-content-final-resum">
                                    <span class="title">總計:</span>
                                    <span class="value history-content-final-price">{{ $document->total }}</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <span class="text-red-color">取消訂單說明：</span>
        <div class="box-items-group-show">
            <div class="ct-more-box-items-group-show">
                1. 目前訂單取消僅支援"整筆訂單取消"，若要繼續購物請您再次操作，造成不便請多多包涵。<br>
                2. 信用卡一次付清與信用卡分期付款會於次個工作日退還授權金額。若以請款之信用卡款項，我們將以匯款方式退回現金。<br>
            </div>
        </div>
        <div class="center-box">
            <button type="submit" class="btn btn-danger border-radius-2 btn-send-genuinepart-finish">確認送出</button>
        </div>
    </form>
@stop
@section('script')
@stop