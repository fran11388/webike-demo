@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <style type="text/css">
        .history-page-state a span.action{
            position: absolute;
            bottom: -25px;
            color: #0069bf;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="ct-oem-part-right box-fix-auto">
        <div class="title-main-box">
            <h2>訂單取消申請完成</h2>
        </div>
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">訂單編號：{!! session('code') !!}</h1>
        </div>
        <div class="box-info-oem box-info-compalate">
            您的訂單已經取消成功！<br>
            若您有後續的問題可以由<a href="{{URL::route('customer-history-order')}}" title="訂單履歷{{$tail}}">訂單履歷</a>進行查詢。
        </div>
        <div class="center-box">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{{URL::route('customer-history-order')}}">回訂單履歷</a>
        </div>
    </div>

@stop
@section('script')
@stop


