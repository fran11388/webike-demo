@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/cartpagedetail.css') !!}">
    <style type="text/css">
        .history-page-state a span.action{
            position: absolute;
            bottom: -25px;
            color: #0069bf;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>{{$type_name}}申請</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>訂單編號：{{ $document->code }}</h2>
            <hr class="visible-xs">
        </div>
    </div>
    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="history-table-title visible-md visible-lg">
                <ul>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂購日期</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">付款方式</span></li>
                    <li class="col-md-6 col-sm-6 col-xs-6"><span class="size-10rem">收件資訊</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂單狀態</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->created_at }}</span></li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->payment  }}</span></li>
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <span class="size-10rem">收件人: {{ $document->customer_name }}</span>
                        <span class="size-10rem">地址:{{ $document->address }}</span>
                    </li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->state !!}</span></li>
                </ul>
            </li>
        </ul>
    </div>
    <form id="main_form" method="POST" action="{{route('customer-history-order-detail-return-create',$document->code)}}" enctype="multipart/form-data">
        <input type="hidden" name="orders[]" value="{{$document->code}}">
        <input type="hidden" name="type" value="{{$type}}">
        <div class="box-items-group-customer-account">
            <div class="box-customer-account">
                <h2>{{$type_name}}申請表</h2>
            </div>
            <table class="table table-bordered col-xs-12 col-sm-12 col-md-12">
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2" rowspan="2">會員基本資料</td>
                    <td class="col-xs-2 col-sm-2 col-md-2">會員姓名</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        {{ $current_customer->nickname }}
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">聯繫email</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        {{ $current_customer->email }}
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2" rowspan="4">取貨地址</td>
                    <td class="col-xs-2 col-sm-2 col-md-2">收件人姓名<span class="font-color-red">※必填</span></td>
                    <td class="col-xs-6 col-sm-6 col-md-6">
                        <input class="steps step-3 form-control" type="text" name="address[firstname]" placeholder="請輸入...." value="{{ $address->firstname }}" readonly>
                    </td>
                    <td class="col-xs-2 col-sm-2 col-md-2"><input class="btn btn-primary border-radius2 btn-address-modify col-md-12" type="button" value="修改"></td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">取貨地址<span class="font-color-red">※必填</span></td>
                    <td class="col-xs-6 col-sm-6 col-md-6">
                        <div id="twzipcode" class="form-inline form-group">
                            <div class="form-group">
                                <div data-name="address[county]" data-role="county"></div>
                            </div>
                            <div class="form-group">
                                <div data-name="address[district]" data-role="district"></div>
                            </div>
                            <div class="form-group">
                                <div data-name="address[zipcode]" data-role="zipcode" data-value="{{ $address->zipcode }}"></div>
                            </div>
                        </div>
                        <input class="steps step-3 form-control" type="text" name="address[address]" value="{{ $address->address }}" placeholder="" readonly>
                    </td>
                    <td class="col-xs-2 col-sm-2 col-md-2"><input class="btn btn-primary border-radius2 btn-address-modify col-md-12" type="button" value="修改"></td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">行動電話<span class="font-color-red">※必填</span></td>
                    <td class="col-xs-6 col-sm-6 col-md-6"> <input class="steps step-3 form-control" type="text" name="address[mobile]" placeholder="請輸入...." value="{{$address->mobile}}" readonly></td>
                    <td class="col-xs-2 col-sm-2 col-md-2"><input class="btn btn-primary border-radius2 btn-address-modify col-md-12" type="button" value="修改"></td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">市內電話<span class="font-color-red">※必填</span></td>
                    <td class="col-xs-6 col-sm-6 col-md-6">
                        <input class="steps step-3 form-control" type="text" name="address[phone]" placeholder="請輸入...." value="{{$address->phone}}" readonly>
                    </td>
                    <td class="col-xs-2 col-sm-2 col-md-2"><input class="btn btn-primary border-radius2 btn-address-modify col-md-12" type="button" value="修改"></td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2" rowspan="{{count($collection)}}">商品{{$type_name}}<span class="font-color-red">※必填</span></td>
                    @foreach($collection as $key => $item)
                        @if($key != 0 )
                            </tr>
                            <tr>
                        @endif
                        <td class="col-xs-2 col-sm-2 col-md-2">
                            {{$type_name}}商品{{$key + 1}}
                            <input type="hidden" name="items[]" value="{{$item->source->id}}">
                            <input type="hidden" name="quantity[{{$item->source->id}}]" value="{{$select_item[$item->source->id]}}">
                        </td>

                        <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                            <strong>【{{ $item->manufacturer_name }}】{{$item->product_name}}</strong><span>{{$item->price}} X {{$select_item[$item->source->id]}} = {{$item->currency . round($item->source->price * $select_item[$item->source->id])}}</span>
                            <br/>
                            @if($item->source->product_type == \Everglory\Constants\ProductType::ADDITIONAL and $type == 'return_all')
                                (訂單商品全數退貨，加價購商品必須一併退還)
                                <span>{{$type_name}}原因：</span><span class="font-color-red">※必填</span>
                                <select class="form-control" name="reason[{{$item->source->id}}]">
                                    <?php
                                    $reason = $reasons->first(function ($e) {
                                        return $e->id == 4;
                                    });
                                    ?>
                                    <option value="{{$reason->id}}">{{$reason->name}}</option>
                                </select>
                            @else
                                <span>{{$type_name}}原因：<span class="font-color-red">※必填</span></span>
                                <div>
                                    <select class="form-control" name="reason[{{$item->source->id}}]">
                                        <option value="">請選擇{{$type_name}}原因</option>
                                        @foreach($reasons as $reason)
                                            <option value="{{$reason->id}}">{{$reason->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br/><br/>

                                @if($type == 'exchange')
                                    {{$type_name}}商品網址：<span class="font-color-red">※必填</span>
                                    <div>
                                        <input class="steps step-3 form-control" type="text" name="url[{{$item->source->id}}]" placeholder="請輸入網址...(不得高於原商品金額)" value="">
                                    </div>
                                    {{$type_name}}規格：<input class="steps step-3 form-control" type="text" name="spec[{{$item->source->id}}]" placeholder="請輸入規格..." value="">
                                    <br/>
                                @endif
                                <div>
                                    <textarea class="form-control" rows="6" name="note[{{$item->source->id}}]" placeholder="請輸入補充說明..."  maxlength="500"></textarea>
                                </div>
                                <br/>
                                目前商品狀態：<span class="font-color-red">※必填</span>

                                    <div class="form-inline situation_radio">
                                        @foreach($situations as $situation)
                                            <div class="form-group">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="situation[{{$item->source->id}}]" value="{{$situation->id}}">
                                                        <span>{{$situation->name}}</span>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                <br/>
                                <div>
                                    <div class="box-fix-with left form-group">
                                        <figure class="ct-img-left thumb-box-border thumb-img">
                                            <img src="{{assetRemote('image/bg-btn-choose-file.jpg')}}" alt="" width="250">
                                            <input type="hidden" name="image[{{$item->source->id}}]" value="">
                                            <a class="btn btn-primary border-radius-2 btn-choose-file visible-sm visible-md visible-lg imageUploadHref" onclick="uploadClick()">請自行拍攝商品當下狀況照片並上傳</a>
                                        </figure>
                                        <a class="btn btn-primary border-radius-2 btn-choose-file visible-xs imageUploadHref" onclick="uploadClick()">請自行拍攝商品當下狀況照片並上傳</a>
                                    </div>
                                </div>
                            @endif
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2" rowspan="2">取貨</td>
                    <td class="col-xs-2 col-sm-2 col-md-2">取件時間<span class="font-color-red">※必填</span></td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="address[shipping_time]" value="morning">
                                        <span>早上</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="address[shipping_time]" value="noon">
                                        <span>下午</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="address[shipping_time]" value="night">
                                        <span>晚上</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="address[shipping_time]" value="anytime" checked>
                                        <span>不指定</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">取件備註</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <textarea class="form-control" rows="6" name="address[shipping_comment]" placeholder="請輸入取件備註..." maxlength="500"></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2" rowspan="4">退款作業</td>
                    <td class="col-xs-2 col-sm-2 col-md-2">退款銀行代碼</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <input class="steps step-3 form-control required" type="number" name="account_bank" placeholder="請輸入...." value="">
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">退款分行代碼</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <input class="steps step-3 form-control required" type="number" name="account_branch" placeholder="請輸入...." value="">
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">退款戶名</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <input class="steps step-3 form-control required" type="text" name="account_name" placeholder="請輸入...." value="">
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">退款帳號</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <input class="steps step-3 form-control required" type="number" name="account_number" placeholder="請輸入...." value="">
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">退款注意事項</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        填寫完畢後請將該申請書與商品和出貨明細交由宅配人員收回。我們在收到會員退換貨申請書與商品後會有專員為您處理退換貨作業(7日退貨期限起算日為物流業者寄送抵達會員收件地址完成收件手續當時算起)
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2" rowspan="5">退貨準備事項<span class="font-color-red">※必填</span></td>
                    <td class="col-xs-10 col-sm-10 col-md-10" colspan="3">
                        請同意以下準備作業：
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">檢查商品：</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="checkbox" name="rule[1]">
                                        <span>請確認商品、吊牌、包裝等相關配件無缺件</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
{{--                 <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">準備單據：</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="checkbox" name="rule[2]">
                                        <span>發票及商品明細需連同退貨商品一同回收</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr> --}}
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">裝箱打包：</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="checkbox" name="rule[3]">
                                        <span>產品包裝也屬於商品一部分為避免破損，請利用原始寄送外箱或自行另加外箱打包</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">等待回收：</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="2">
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="checkbox" name="rule[4]">
                                        <span>請勿直接寄回商品，我們會派宅配免費到府回收</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="col-xs-2 col-sm-2 col-md-2">客戶補充說明：</td>
                    <td class="col-xs-8 col-sm-8 col-md-8" colspan="3">
                        <textarea class="form-control" rows="6" name="comment" placeholder="請輸入補充說明..."></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <h3 class="font-color-red center-box">※以上打勾視同您同意退貨規定，若與事實不符我們將無法受理，商品也會再次退回，並向您收取物流作業費用。</h3>
        <div class="center-box">
            <a href="{{route('customer-history-order-detail-return',$document->code)}}" class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-left">回上頁修改</a>
            <button type="submit" class="btn btn-danger border-radius-2 btn-send-genuinepart-finish btn-send-genuinepart-finish-right" id="form_submit">確認送出</button>
        </div>
    </form>
    <form style="position: absolute;left:-1000px;top:0px;" id="hidden-form" target="upload_target"
          action="{{ route('customer-history-order-detail-return-create-image',$document->code) }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="item_id" value="">
        <input id="choose-file" type="file" size="40" name="photo" accept="image/*" style="width:260px;margin:0">
    </form>
    <iframe id="upload_target" name="upload_target" src="#" style="display:none; width:0;height:0;border:0px solid #fff;"></iframe>

@stop
@section('script')
    <script src="{!! assetRemote('js/pages/history/history.js') !!}"></script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script type="text/javascript" src="{!! assetRemote('plugin/jquery.twzipcode.min.js') !!}"></script>

    <script type="text/javascript">
        function uploadClick(){
            $('#choose-file').click();
        }

        $('#twzipcode').twzipcode({
            readonly:true,
            'countyName': 'address[county]',
            'districtName': 'address[district]',
            'zipcodeName': 'address[zipcode]'
        });
        twzipcodeCompare = [
                'address[county]',
                'address[district]',
                'address[zipcode]',
            ];
        $.each(twzipcodeCompare, function(key, item){
            console.log('[name="' + item + '"]');
           $('[name="' + item + '"]').addClass('form-control');
        });
    </script>
    <script>

        $type_name = "{{$type_name}}";
        $('.history-page-state a span.action').click(function(){
            window.open($(this).attr('data-content'));
        });

        $.each( $('#twzipcode select'), function(){
            $(this).prop('disabled',true);
        });

        $(document).on("click", ".btn-address-modify", function () {
            btn = $(this);
            btn.toggleClass('btn-primary');
            btn.toggleClass('btn-danger');
            target_td = btn.closest('td').prev('td');
            if(btn.hasClass('btn-danger')){
                btn.val('確認');
                target_td.find('input').prop('readonly', false).focus();
                target_td.find('select').prop('disabled', false);
            }else{
                btn.val('修改');
                target_td.find('input').prop('readonly', true);
                target_td.find('select').prop('disabled', true);
            }
        });

        function addErrorMessage(element,slipToElements) {
            element.parent().addClass('has-error');
            slipToElements.e.push(element);
            if(element.attr('type') == 'checkbox'){
                element.parent().after( '<span class="font-color-red error"><b>※此欄位必填</b></span>' );
            }else{
                element.after( '<span class="font-color-red error"><b>※此欄位必填</b></span>' );
            }

            slipToElements.stop = true;
        }

        $('#main_form').submit(function(){
            var slipToElements = {e:[],stop : false};

            $('.font-color-red.error').remove();
            $('.has-error').removeClass('has-error');
//
            //address modifying
            if($('.btn-address-modify.btn-danger').length){
                swal(
                        '取貨地址錯誤!',
                        '請先完成修改!',
                        'error'
                );
                return false;
            }

            //reason and note
            $.each( $('select[name^=reason]') , function(){
                if(!$(this).val()){
                    addErrorMessage($(this),slipToElements);
                }else if($(this).val() == 9 && !$(this).closest('td').find('textarea[name^="note"]').val()){
                    addErrorMessage($(this).closest('td').find('textarea[name^="note"]'),slipToElements);
                }
            });

            //address product url
            $.each( $('input[name^=address],input[name^=url]') , function(){
                if(!$(this).val()){
                    addErrorMessage($(this),slipToElements);
                }
            });

            //situation and image
            $.each( $('div.situation_radio') , function(){
                if($(this).find('input[name^="situation"]:checked').length == 0){
                    addErrorMessage($(this),slipToElements);
                }else if(!$(this).closest('td').find('input[name^="image"]').val()){
                    addErrorMessage($(this).closest('td').find('input[name^="image"]').closest('div'),slipToElements);
                }
            });
            //confirm
            $.each( $('input[name^=rule]') , function(){
                if(!$(this).is(":checked")){
                    addErrorMessage($(this),slipToElements);
                }
            });



            if(slipToElements.stop){
                slipTo(slipToElements.e[0]);
                return false;
            }

            if(confirm('確認要提出申請？')){
                $.each( $('#twzipcode select'), function(){
                    $(this).prop('disabled',false);
                });
                $('#form_submit').hide();
            }

        });


        $('.imageUploadHref').click(function(){
            //set item_id to the image form
            item_id = $(this).closest('td').prev('td').find('input[name^="items"]').val();
            $('input[name="item_id"]').val(item_id);
        });

        $('input[type="file"]').change(function () {
            //submit form when choice file
            $('#form_submit').prop('disabled', true).css('cursor', 'no-drop');
            $('#hidden-form').submit();
        });
        function stopUpload(result) {
            //image upload result

            $('#form_submit').prop('disabled', false).css('cursor', 'pointer');
            $('#hidden-form').find('input[type="file"]').val('');
            image_input = $('input[name="image['+ result.item_id +']"]');
            if (result.success) {
                image_input.val(result.key);
                image_input.prev('img').attr('src', "{{assetPathTranslator('assets/photos/return/',config('app.env'))}}" +'/'+ result.key);
            } else {
                image_input.val('');
                image_input.prev('img').attr('src', "{{assetPathTranslator('image/bg-btn-choose-file.jpg',config('app.env'))}}");
                alert(result.message);
                if (result.reload) {
                    window.location.reload();
                    return false;
                }
            }
            return true;
        }
    </script>
@stop