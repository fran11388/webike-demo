@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/cartpagedetail.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>退換貨申請</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>訂單編號：{{ $document->code }}</h1>
            <hr class="visible-xs">
        </div>
    </div>

    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="history-table-title visible-md visible-lg">
                <ul>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂購日期</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">付款方式</span></li>
                    <li class="col-md-6 col-sm-6 col-xs-6"><span class="size-10rem">收件資訊</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂單狀態</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->created_at }}</span></li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->payment  }}</span></li>
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <span class="size-10rem">收件人: {{ $document->customer_name }}</span>
                        <span class="size-10rem">地址:{{ $document->address }}</span>
                    </li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->state !!}</span></li>
                </ul>
            </li>
        </ul>
    </div>

    <form method="POST" action="{{ route('customer-history-order-detail-return',$document->code) }}" id="rex_form">
        <div class="form-group">

            @include('response.common.message.error')

            <label class="control-label">
                <h3>1.選擇退貨或換貨:</h3>
            </label>
            <div class="controls box-fix-auto col-md-12 col-sm-12 col-xs-12">
                <select class="form-control" name="type">
                    <option value="">請選擇退貨或換貨</option>
                    @foreach($document->return_types as $return_type)
                        <option value="{{$return_type->value}}">{{$return_type->name}}</option>

                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <h3>2.挑選商品及數量:</h3>
        </div>
        <div class="history-table-container">
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-5 col-sm-5 col-xs-5"><span class="size-10rem">商品</span></li>
                        <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">購買金額與數量</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">是否退換貨</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">選擇退貨數量</span></li>
                    </ul>
                </li>
                <li class="history-table-content history-order-detail-table-content">
                    @foreach($collection as $key => $item)
                        <?php $num = $key + 1 ?>
                        <ul class="col-sm-block col-xs-block clearfix">
                            <li class="col-md-5 col-sm-12 col-xs-12 history-multi-content-estimate">
                                <div class="history-content-estimate-block">
                                    <div class="history-content-img box-fix-with">
                                        <a class="zoom-image" href="javascript:void(0)">
                                            <img src="{{$item->thumbnail}}" alt="{{'【' . $item->manufacturer_name . '】' . $item->product_name}}">
                                        </a>
                                    </div>
                                    <div class="history-content-estimate col-md-10">
                                        <a href="javascript:void(0)"><h3>{{ $item->manufacturer_name . '：' . $item->product_name}}</h3></a>
                                        <span>{{$item->model_number}}</span>
                                        <span>{{$item->options}}</span>
                                    </div>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-12 col-xs-12"><span>{{$item->price}} X {{$item->quantity}}件</span></li>
                            <li class="col-md-2 col-sm-12 col-xs-12">
                                <span class="size-10rem">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="validate[{{ $num }}]" value="{{$item->source->id}}" {{ old('validate.'.$num) ? 'checked' : '' }} data-type="{{$item->source->product_type}}">
                                        </label>
                                    </div>
                                </span>
                            </li>
                            <li class="col-md-2 col-sm-12 col-xs-12">
                                <!-- quantity plugin -->
                                <div class="form-group-options">
                                    <div id="1" class="input-group input-group-option quantity-wrapper">
                                        <div class="input-group-addon input-group-addon-remove quantity-remove btn">
                                            <i class="glyphicon glyphicon-minus"></i>
                                        </div>
                                        <input class="form-control quantity-count disabled btn-label" disabled type="text" name="quantity[{{$num}}]" data-max="{{$item->quantity}}" value="{{ old('quantity.'.$num) ? old('quantity.'.$num) : '1' }}">
                                        <div class="input-group-addon input-group-addon-remove quantity-add btn">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </div>
                                    </div>
                                </div>
                                <!-- quantity plugin -->
                            </li>
                        </ul>
                    @endforeach
                </li>
            </ul>
        </div>
        <div class="history-transparent-box center-box">
            <a href="{{route('customer-history-order-detail',$document->code)}}" class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-left">回上一頁</a>
            <button class="btn btn-default  border-radius-2 btn-send-the-query base-btn-gray" type="submit">下一步填寫表單</button>
        </div>
    </form>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/history/history.js') !!}"></script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script>
        function getReturnType() {
            type = $('select[name=type]').val();
            if(!type){
                // slipTo($('select[name=type]') );
                swal(
                    '資訊不足',
                    '請先選擇退貨或換貨',
                    'error'
                );
                $('select[name=type]').closest('div').addClass('has-error');
            }
            return type;
        }

        function checkAdditional() {
            checkit = true;
            checked_box = $('input[name^="validate"][data-type!="4"]:checked');
            if($('input[name^="validate"][data-type!="4"]').length == checked_box.length){
                $.each( checked_box  , function(){
                    qty = $(this).closest('li').next('li').find('.quantity-count');
                    if(qty.val() != qty.data('max')){
                        checkit = false;
                    }
                });
            }else{
                checkit = false;
            }
            return checkit;
        }

        //Choice type script
        $('select[name=type]').change(function(){
            if($(this).val() == 'return_all'){
                selectAllItems();
            }else{
                $('input[name^="validate"]').prop('checked',false).prop('disabled',false);
                $.each( $('input[name^="quantity"]') , function(){
                    $(this).val(1).prop('disabled',true);
                });
            }
            $('select[name=type]').closest('div').removeClass('has-error');
        });


        //checkbox click
        $('input[name^=validate]').click(function(){

            checkbox = $(this);
            type = getReturnType();
            if(!type){
                checkbox.prop('checked',false);
            }
            if(checkbox.prop('checked')){
                if(type == 'return' && checkAdditional() && $('select[name="type"]  option[value="return_all"]').length == 1){
                    swal({
                        title: '您選擇了訂單內所有商品',
                        text: "將類型自動改為整張訂單退貨",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: '取消',
                        confirmButtonText: '確認',
                        allowOutsideClick: false
                    }).then(function () {
                        $('select[name=type]').val('return_all').change();
                    }, function (dismiss) {
                        if (dismiss === 'cancel') {
                            checkbox.prop('checked',false);
                        }
                    });
                }else{
                    checkbox.closest('ul').find('.quantity-count').removeAttr('disabled').removeClass('disabled');
                }
            }else{
                checkbox.closest('ul').find('.quantity-count').prop('disabled', true).addClass('disabled');
            }
        });

        $('.quantity-count').change(function(){
            type = getReturnType();
            input = $(this);
            value = input.val();
            var max = input.attr('data-max');

            if(type == 'return' && checkAdditional() && $('select[name="type"]  option[value="return_all"]').length == 1){
                swal({
                    title: '您選擇了訂單內所有商品',
                    text: "將類型自動改為整張訂單退貨",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '取消',
                    confirmButtonText: '確認',
                    allowOutsideClick: false
                }).then(function () {
                    $('select[name=type]').val('return_all').change();
                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        input.val(value - 1);
                    }
                });
            }

            if(isNaN(value)){
                input.val(1);
                swal(
                    '數量輸入錯誤',
                    '僅可填入數量',
                    'error'
                )
            }else if(value <= 0){
                input.val(1);
                swal(
                    '數量輸入錯誤',
                    '數量不可低於1',
                    'error'
                )
            }else if(value > max){
                input.val(max);
                swal(
                    '數量輸入錯誤',
                    '超過最大數量',
                    'error'
                )
            }

        });

        controlQuantity();
        function selectAllItems() {
            $.each( $('input[name^="validate"]') , function(){
                $(this).prop('checked',true).prop('disabled',true);
            });
            $.each( $('input[name^="quantity"]') , function(){
                $(this).val($(this).data('max')).prop('disabled',true);
            });

        }







        $('#rex_form').submit(function(){
            $(this).find('button[type="submit"]').hide();
            if(!$('select[name="type"]').val()){
                swal(
                        '申請資訊不足',
                        '請選擇退貨或換貨',
                        'error'
                );
                $(this).find('button[type="submit"]').show();
                $('select[name=type]').closest('div').addClass('has-error');
                return false;
            }
            if($('input[name^="validate"]:checked').length == 0){
                swal(
                        '申請資訊不足',
                        '請選擇欲退換貨之商品',
                        'error'
                );
                $(this).find('button[type="submit"]').show();
                return false;
            }
            $('input[name^="quantity"]').prop('disabled',false);
            $('input[name^="validate"]').prop('disabled',false);

        });
    </script>
@stop