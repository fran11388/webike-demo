@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>團購商品查詢履歷</h2>
    </div>
    <div class="history-transparent-box">
        <h3>本查詢系統會保留7天報價資料，一旦過了期限內容便會消失，請您再次查詢。</h3>
        <h3>團購商品的價格會依據正廠售價及匯率變動，因此不同時間點查詢的價格可能會有變化。</h3>
    </div>
    <form action="">
        <div class="history-table-container">
            @if($collection->count())
                <ul class="history-info-table">
                    <li class="history-table-title visible-md visible-lg">
                        <ul>
                            <li class="col-md-4 col-sm-4 col-xs-4"><span class="size-10rem">查詢編號</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">查詢日期</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">回覆日期</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">報價有效期限</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">查看報價</span></li>
                        </ul>
                    </li>
                    <li class="history-table-content history-genuine-table-content">
                        @foreach($collection as $item)
                            <ul class="col-sm-block col-xs-block clearfix">
                                <li class="col-md-4 col-sm-12 col-xs-12"><a href="{!! $item->link !!}"><span class="size-10rem blue-text">{!! $item->code !!}</span></a></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->created_at !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->response_at !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->expiration !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12">
                                    <span class="size-10rem blue-text">
                                        <a href="{!! $item->link !!}">查看報價</a>
                                        @if(!$item->has_read)
                                            {!! $item->read_tip !!}
                                        @endif
                                    </span>
                                </li>
                            </ul>
                        @endforeach
                    </li>
                </ul>
                <div class="row ct-pagenav">
                    {!! $pager !!}
                </div>
            @else
                <div class="container">
                    <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
                </div>
            @endif
        </div>
        <div class="history-transparent-box history-genuine-submit-block">
            <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
        </div>
    </form>
@stop
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
@stop