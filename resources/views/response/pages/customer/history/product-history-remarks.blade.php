<div class="product-history-remarks-background">
    <div class="product-history-remarks">
        <div class="product-history-remarks-title">
            <a href="javascript:void(0)"><i class="fas fa-times"></i></a>
            <span>備註</span>
            <button class="w-botton">儲存</button>
        </div>
        <div class="product-history-remarks-content">
            <textarea onkeydown="this.value=this.value.substr(0,19)" placeholder="自定20字內備註"></textarea>
        </div>
    </div>
</div>