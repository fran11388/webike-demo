@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    @if( $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE)
        <div class="title-main-box">
            <h2>經銷商升級狀態一覽</h2>
        </div>
        <form action="">
            <div class="history-table-container">
                <ul class="history-info-table">
                    <li class="history-table-title visible-md visible-lg">
                        <ul>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">會員等級</span></li>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">本月回饋點數</span></li>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">本月累積升級金額</span></li>
                            @if(is_null($current_customer->getNextLevelAmount()))
                                <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">下月等級</span></li>
                            @else
                                <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">距離下個等級尚需</span></li>
                            @endif

                        </ul>
                    </li>
                    <li class="history-table-content history-genuine-table-content">
                        <ul class="col-sm-block col-xs-block clearfix">
                            <li class="col-md-3 col-sm-12 col-xs-12 no-margin level-card {!! $current_customer->getCurrentLevelCssClass() !!}">
                                <h3>{{ $current_customer->getCurrentLevelName() }}</h3>
                            </li>
                            <li class="col-md-3 col-sm-12 col-xs-12"><span class="size-12rem font-color-red">{{$current_customer->getCurrentLevelRate()}}倍</span></li>
                            <li class="col-md-3 col-sm-12 col-xs-12"><span class="size-10rem">NT${{ number_format($current_customer->getCurrentAmount()) }}</span></li>
                            @if(is_null($current_customer->getNextLevelAmount()))
                                <li class="col-md-3 col-sm-12 col-xs-12 no-margin level-card {!! \Ecommerce\Repository\CustomerLevelRepository::LEVEL[3]['css_class'] !!}">
                                    <h3>{!! \Ecommerce\Repository\CustomerLevelRepository::LEVEL[3]['display_name'] !!}</h3>
                                    <span class="size-10rem font-color-red">回饋點數{!! \Ecommerce\Repository\CustomerLevelRepository::LEVEL[3]['rate'] * 100 !!}倍</span>
                                </li>
                            @else
                                <li class="col-md-3 col-sm-12 col-xs-12"><span class="size-10rem">NT${{ number_format($current_customer->getNextLevelAmount()) }}</span></li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
        </form>
    @endif



    <div class="title-main-box">
        <h2>點數獲得及使用履歷</h2>
    </div>
    <div class="history-transparent-box">
        <h3>您好，{{ $current_customer->realname }}，以下是您的點數獲得及使用履歷，請把握使用期限多加利用。</h3>
    </div>
    <form action="">
        <div class="history-table-container">
            @if($collection->count())
                <ul class="history-info-table">
                    <li class="history-table-title visible-md visible-lg">
                        <ul>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">訂單編號</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">日期</span></li>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">狀態</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">增加點數</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">使用點數</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">使用期限</span></li>
                        </ul>
                    </li>
                    <li class="history-table-content history-genuine-table-content">
                        @foreach($collection as $item)
                            <ul class="col-sm-block col-xs-block clearfix">
                                <li class="col-md-3 col-sm-12 col-xs-12">{!! $item->link_html !!}</li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $item->date }}</span></li>
                                <li class="col-md-3 col-sm-12 col-xs-12"><span class="size-10rem">{!!  $item->description  !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $item->points_current }}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12">{!! $item->points_spend_html !!}</li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $item->end_date }}</span></li>
                            </ul>
                        @endforeach
                    </li>
                </ul>
                <div class="row ct-pagenav">
                    {!! $pager !!}
                </div>
            @else
                <div class="container">
                    <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
                </div>
            @endif
        </div>
        <div class="center-box">
            <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
        </div>
    </form>

@stop
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
@stop