@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
     <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/genuineparts-relieved-shopping.css') !!}">
    <style>
        li.estimate-text {
            margin:15px 0px !important;
        }
        ul.history-table-adjust li.history-table-content ul.extra-link li{
            margin-top: 0 !important;
        }
        ul.history-table-adjust li.history-table-content ul.extra-link {
            display:none;
        }
        ul.history-table-adjust li.history-table-content ul.caution-content {
            display:none;
        }
        ul.history-table-adjust li.history-table-content .group-row:nth-child(2n) ul{
            background: #eee;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>正廠零件查詢 {!! $document->code !!}</h2>
    </div>
    <div class="history-transparent-box">
        <h3>本查詢系統會保留15天報價資料，一旦過了期限內容便會消失，請您再次查詢。</h3>
        <h3>正廠零件的價格會依據正廠售價及匯率變動，因此不同時間點查詢的價格可能會有變化。</h3>
    </div>
    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="history-table-title visible-md visible-lg">
                <ul>
                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="size-10rem">查詢編號</span></li>
                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="size-10rem">報價日期</span></li>
                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="size-10rem">報價有效期限</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->code !!}</span></li>
                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->created_at !!}</span></li>
                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->expiration !!}</span></li>
                </ul>
            </li>
        </ul>
    </div>
    @include('response.pages.genuineparts.partials.genuineparts-relieved-shopping')
    <form action="{{route('cart-update')}}" method="POST">
        <div class="history-transparent-box">
            <label><input class="switch" type="checkbox"> 全選</label>
            <h3 class="text-red-color">&#149 請勾選想要購買的商品，並按下"加入購物車"進行結帳動作；若要修改商品數量可由購物車頁面修改。</h3>
        </div>
        <div class="history-table-container">
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-1 col-sm-1 col-xs-1"></li>
                        <li class="col-md-5 col-sm-5 col-xs-5"><span class="size-10rem">零件料號</span></li>
                        <li class="col-md-4 col-sm-4 col-xs-4"><span class="size-10rem">預估交期</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">備註</span></li>
                    </ul>
                </li>
                <li class="history-table-content">
                    @foreach($collection as $key => $item)
                        <div class="group-row">
                            <ul class="col-sm-block col-xs-block clearfix">
                                <li class="col-md-1 col-sm-1 col-xs-1 visible-md visible-lg"><span class="size-10rem">{!! $key + 1 !!}</span></li>
                                <li class="col-md-5 col-sm-12 col-xs-12">
                                    <span class="size-10rem"></span>
                                    <div class="history-table-multi-content-block">
                                        <label>
                                            <input class="follower" type="checkbox" {!! $item->can_sell ? 'checked' : 'disabled' !!} name="sku[{{$item->url_rewrite}}]" value="{{$item->url_rewrite}}">
                                            <input type="hidden" name="qty[{{$item->url_rewrite}}]" value="{{$item->quantity}}">
                                            <input type="hidden" name="cache[{{$item->url_rewrite}}]" value="{{$item->cache_id}}">
                                            @if($item->link)
                                                <a href="{!! $item->link !!}" target="_blank"> {!! $item->name !!} <strong>({!! $item->quantity !!}個)</strong></a>
                                            @else
                                                {!! $item->name !!} <strong>({!! $item->quantity !!}個)</strong>
                                            @endif
                                        </label>
                                        <br>
                                        {!! $item->price_html !!}
                                    </div>
                                </li>
                                <li class="col-md-4 col-sm-12 col-xs-12 estimate-text "><span class="size-10rem">{!! $item->delivery !!}</span></li>
                                @if($item->outletProduct)
                                    <li class="col-md-2 col-sm-12 col-xs-12"><div style="line-height:46px;"><span class="size-10rem">{!! $item->note ? $item->note : '-' !!}</span></div><div style='padding-top: 15px;border-top: 1px solid #dcdee3;margin-top: 15px;'><a href="{{ \URL::route('product-detail', $item->outletProduct->url_rewrite) }}" target="_blank" class="size-085rem">商品頁面</a></div></li>
                                @else
                                    <li class="col-md-2 col-sm-12 col-xs-12 "><span class="size-10rem">{!! $item->note !!}</span></li>
                                @endif
                            </ul>
                            <ul class="col-sm-block col-xs-block clearfix extra-link" id="{{ $item->model_number }}">
                                <li class="col-md-1 col-sm-1 col-xs-1 visible-md visible-lg"></li>
                                <li class="col-md-5 col-sm-12 col-xs-12 model_number"></li>
                                <li class="col-md-4 col-sm-12 col-xs-12 delivery"></li>
                                <li class="col-md-2 col-sm-12 col-xs-12 note"></li>
                            </ul>
                            <ul class="col-sm-block col-xs-block clearfix caution-content extra-link">
                                <li class="col-md-1 col-sm-1 col-xs-1 visible-md visible-lg"></li>
                                <li class="col-md-11 col-sm-11 col-xs-11 caution"><span class="font-color-red" style="padding-top: 10px; border-top: 1px #CCC solid;"></span></li>
                            </ul>
                        </div>
                    @endforeach
                </li>
            </ul>
        </div>
        @if(in_array(true, $collection->pluck('can_sell')->toArray()))
            <div class="history-transparent-box">
                <button class="btn btn-danger border-radius-2 btn-send-the-query history-btn-submit">加入購物車</button>
            </div>
        @endif
    </form>
    <div class="history-gray-box">
        ※ 已選取的商品才會被送入購物車。<br>
        <h3 class="text-red-color">※ 預估交期是指"單項商品"下訂後的預估到貨時間，不包含國定假日或廠商休假。若購買複數商品以交期最長的作為統一交貨時間，確切整筆訂單交貨時間以"訂購確認通知書"中的交期為準。</h3>
        ※ 數量的變更、消除請在放入購物車後執行 <br>
        ※ 如果下訂的商品數量比報價查詢時還多，則可能發生庫存不足的情況。<br>
        ※ 因廠商的庫存狀況變化非常迅速，偶爾會有下訂後才變成缺貨的情況。 如遇此情況我們將發送「Webike 訂單相關問題 TWXXXXXXXX 」，屆時請您協助我們處理您的訂單。<br>
        ※ 以下特殊狀態無法訂購："廢盤"該項商品廠商停產、"交期未定"目前廠商庫存不足，暫時無法訂購、"不明"表示輸入的料號不正確
    </div>
@stop

@section('script')
    <script src="{!! assetRemote('js/pages/history/history.js') !!}"></script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script>
        @if($model_numbers)
            $(document).ready(function(){
{{--                var model_numbers = JSON.parse("{{ $model_numbers }}");--}}
                var model_numbers = "{!! $model_numbers !!}";
                $.ajax({
                    url: "{!! route('customer-history-genuineparts-detail-match') !!}",
                    data: {_token: $('meta[name=csrf-token]').prop('content'),model_numbers:model_numbers,manufacturer_id:"{!! $collection->first()->manufacturer_id !!}"},
                    type: 'POST',
                    dataType: 'json',
                    success: function(result){
                        console.log(result);
                        if(result) {
                            var block = "";
                            var group_row;
                            var caution = "※「正廠零件頁面」與「一般商品頁面」供應商不同，購買前請確認\"售價\"與\"交期\"。";
                            $.each(result, function (model_number) {
                                $.each(this, function (title, val) {
                                    $('#' + model_number).find('.' + title).append(val);
                                    $('#' + model_number).show();
                                });
                                group_row = $('#' + model_number).parents('.group-row');
                                group_row.find('.caution-content .caution span').text(caution);
                                group_row.find('.caution-content').show();
                            });
                        }
                    },
                    error: function(xhr, ajaxOption, thrownError){

                    }
                });
            });
        @endif
    </script>
    {{--<script>--}}
        {{--$('.history-btn-submit').click(function(){--}}
            {{--var items = $(this).closest('form').find('input[name^=item]');--}}
            {{--addToCart(items, $(this));--}}
        {{--});--}}
    {{--</script>--}}
@stop
