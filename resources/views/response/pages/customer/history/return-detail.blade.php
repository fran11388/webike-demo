@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <style type="text/css">
        .history-page-state a span.action{
            position: absolute;
            bottom: -25px;
            color: #0069bf;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>{{$document->type_name}}訂單編號：{!! $document->code !!}</h2>
    </div>
    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="history-table-title visible-md visible-lg">
                <ul>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂購日期</span></li>
                    <li class="col-md-6 col-sm-6 col-xs-6"><span class="size-10rem">收件資訊</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂單狀態</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->created_at }}</span></li>
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <span class="size-10rem">
                            收件人: {{ $document->customer_name }}<br>
                            地址:{{ $document->address }}
                        </span>
                    </li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->state !!}</span></li>
                </ul>
            </li>
        </ul>
    </div>
    <div id="myNavbar" class="menu-brand-top-page">
        <div class="ct-menu-brand-top-page2">
            <ul class="ul-menu-brand-top-page ul-menu-motor-top-page">
                <li>
                    <a href="#return_status">{{$document->type_name}}狀態</a>
                </li>
                <li>
                    <a href="#return_detail">{{$document->type_name}}商品明細</a>
                </li>
                <li>
                    <a href="{{$contact_link}}" target="_blank">{{$document->type_name}}問題資訊</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="form-group">
        <h3 id="return_status">目前{{$document->type_name}}狀態</h3>
        <hr class="visible-xs">
    </div>
    <div class="container breadcrumb-container">
        <div class="checkout-page-state return-state history-page-state">
            @foreach($process->process as $status)
                <a href="javascript:void(0)" class="{{$status['active'] ? '' : 'active-state'}}">
                    <h3>{{$status['name']}}</h3>
                    {!! $status['text'] ? '<span>'.$status['text'].'</span>' : '' !!}
                    @if($status['function'])
                        <span class="action" data-content="{{$status['function_link']}}">{{$status['function']}}</span>
                    @endif
                </a>
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <h3 id="return_detail">{{$document->type_name}}商品明細</h3>
        <hr class="visible-xs">
    </div>
    <form action="">
        <div class="history-table-container">
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-5 col-sm-5 col-xs-5"><span class="size-10rem">商品</span></li>
                        <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">{{$document->type_name}}金額</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">處理方式</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">備註狀態</span></li>
                    </ul>
                </li>
                <li class="history-table-content history-order-detail-table-content">
                    @foreach($collection as $key => $item)
                    <ul class="col-sm-block col-xs-block clearfix">
                        <li class="col-md-5 col-sm-12 col-xs-12 history-multi-content-estimate">
                            <div class="history-content-estimate-block">
                                <div class="history-content-img box-fix-with">
                                    <a class="zoom-image" href="javascript:void(0)">
                                        <img src="{{$item->thumbnail}}" alt="{{'【' . $item->manufacturer_name . '】' . $item->product_name}}">
                                    </a>
                                </div>
                                <div class="history-content-estimate col-md-10 col-sm-12 col-xs-12">
                                    <a href="javascript:void(0)"><h3>{{ $item->manufacturer_name . '：' . $item->product_name}}</h3></a>
                                    <span>{{$item->model_number}}</span>
                                    <span>{{$item->options}}</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-12 col-xs-12"><span>{{$item->price}} X {{$item->quantity}}件 = {{$item->total}}</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{$document->type_name}}</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{$document->state}}</span></li>
                    </ul>
                    @endforeach
                </li>
            </ul>
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-content history-order-detail-table-content">
                    <ul class="history-table-content-resum-block">
                        <li class="col-md-7 col-sm-12 col-xs-12"><span class="size-10rem">{{$document->type_name}}備註：{{$document->comment}}</span></li>
                        <li class="col-md-5 col-sm-12 col-xs-12">
                            <ul>
                                <li>
                                    <span class="title">總計:</span>
                                    <span class="value history-content-final-price">{{ $document->total }}</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="history-transparent-box">
            <a class="btn btn-default btn-send-the-query base-btn-gray" href="{{URL::route('customer-history-return')}}">回上一頁</a>
        </div>
    </form>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/history/history.js') !!}"></script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script>
        $('.history-page-state a span.action').click(function(){
            window.open($(this).attr('data-content'));
        });
    </script>
@stop