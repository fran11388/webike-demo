@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <style type="text/css">
        .history-page-state a span.action{
            position: absolute;
            bottom: -20px;
            color: #0069bf;
        }
        .history-page-state a span.action.second{
            position: absolute;
            bottom: -35px;
            color: #0069bf;
        }
        .history-page-state a span.action.third{
            position: absolute;
            bottom: -50px;
            color: #0069bf;
        }
        @media(max-width:900px ){
            .form-group{
                padding-top: 45px;
            }
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>訂單編號：{!! $document->code !!}</h2>
    </div>
    <div class="history-table-container">
        <ul class="history-info-table">
            <li class="history-table-title visible-md visible-lg">
                <ul>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂購日期</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">付款方式</span></li>
                    <li class="col-md-6 col-sm-6 col-xs-6"><span class="size-10rem">收件資訊</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂單狀態</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                <ul class="col-sm-block col-xs-block clearfix">
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->created_at }}</span></li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{ $document->payment  }}</span></li>
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <span class="size-10rem">收件人: {{ $document->customer_name }}<br>地址:{{ $document->address }}</span>
                    </li>
                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $document->state !!}{!! $document->state_note ? '<span>' . $document->state_note . '</span>' : '' !!}</span></li>
                </ul>
            </li>
        </ul>
    </div>

    @include('response.common.message.error')

    <div id="myNavbar" class="menu-brand-top-page">
        <div class="ct-menu-brand-top-page2">
                <ul class="ul-menu-brand-top-page ul-menu-motor-top-page">
                    <li>
                        <a href="#order_status">訂單狀態</a>
                    </li>
                    <li>
                        <a href="#order_detail">訂單明細</a>
                    </li>
                    @if($process->can_cancel)
                        <li>
                            <a href="{{ route('customer-history-order-detail-cancel',$document->code) }}">訂單取消</a>
                        </li>
                    @endif
                    @if($process->can_return)
                        <li>
                                <a href="{{ route('customer-history-order-detail-return',$document->code) }}">退換貨申請</a>
                        </li>
                    @endif
                    <?php /*
                    <li>
                        <a href="#product-review">下載訂單明細</a>
                    </li>
                    <li>
                        <a href="#product-review">下載報價單</a>
                    </li>
                    */ ?>
                    <li>
                        <a href="{{ route('customer-service-proposal-create',['ticket_type'=>'order','orders'=>$document->code]) }}">訂單問題詢問</a>
                    </li>
                    @if(in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
                        <li>
                            <a href={{URL::route('customer-history-order-quotes',['code'=> $document->code])}}>訂購明細表</a>
                        </li>
                    @endif
                </ul>
        </div>
    </div>
    <div class="form-group">
        <h3 id="order_status">目前訂單狀態</h3>
    </div>
    <div class="container breadcrumb-container">
        <div class="checkout-page-state history-page-state">
            @foreach($process->process as $status)
                <a href="javascript:void(0)" class="{{$status['active'] ? '' : 'active-state'}}">
                    <h3>{{$status['name']}}</h3>
                    {!! $status['text'] ? '<span>'.$status['text'].'</span>' : '' !!}
                    @if($status['function'])
                        <span class="action" data-content="{{$status['function_link']}}" data-blank="{{$status['function_blank']}}">{{$status['function']}}</span>
                    @endif
                    @if(isset($status['invoice_number']))
                        @if($status['random_number'])
                            <span class="action {{$status['function']? 'second' : ''}}" data-content="https://einvoice.ecpay.com.tw/SearchInvoice/Invoice" data-blank="true">發票號碼：{{$status['invoice_number']}}</span>
                            <span class="action {{$status['function']? 'third' : 'second'}}" data-content="https://einvoice.ecpay.com.tw/SearchInvoice/Invoice" data-blank="true">隨機碼：{{$status['random_number']}}</span>
                        @endif
                    @endif

                </a>
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <h3 id="order_detail">訂單明細</h3>
        <hr class="visible-xs">
    </div>
    <form action="">
        <div class="history-table-container">
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-1 col-sm-1 col-xs-1"></li>
                        <li class="col-md-4 col-sm-4 col-xs-4"><span class="size-10rem">商品名稱</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">單價</span></li>
                        <li class="col-md-1 col-sm-1 col-xs-1"><span class="size-10rem">數量</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">小計</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">商品詳細</span></li>
                    </ul>
                </li>
                <li class="history-table-content history-order-detail-table-content">
                    @foreach($collection as $key => $item)
                    <ul class="col-sm-block col-xs-block clearfix">
                        <li class="col-md-1 col-sm-6 col-xs-6 visible-md visible-lg"><span class="size-10rem">{{$key + 1}}</span></li>
                        <li class="col-md-4 col-sm-12 col-xs-12">
                            <p class="number-text-genuuepart-complate"><span class="size-10rem">{{$item->product_name}}</span></p>
                            <p class="font-color-normal">
                            @if($item->options !='')
                                {!!"&nbsp".$item->options!!}
                            @endif
                            </p>
                            
                        </li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{$item->price}}</span></li>
                        <li class="col-md-1 col-sm-12 col-xs-12"><span class="size-10rem">{{$item->quantity}}</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{$item->total}}</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><a href="{{$item->link}}" target="_blank"><span class="size-10rem">商品詳細</span></a></li>
                    </ul>
                    @endforeach
                </li>
            </ul>
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-content history-order-detail-table-content">
                    <ul class="history-table-content-resum-block col-sm-block col-xs-block">
                        <li class="col-md-7 col-sm-12 col-xs-12">
                            <ul>
                                <li>
                                    <span class="size-10rem">訂單備註：{{$document->comment}}</span>
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-5 col-sm-12 col-xs-12">
                            <ul>
                                <li>
                                    <span class="title">使用點數:</span>
                                    <span class="value">-{{ $document->rewardpoints_amount }}</span>
                                </li>
                                <li>
                                    <span class="title">商品金額合計:</span>
                                    <span class="value">{{ $document->grand_total }}</span>
                                </li>
                                <li>
                                    <span class="title">運費:</span>
                                    <span class="value">{{ $document->shipping_amount }}</span>
                                </li>
                                <li>
                                    <span class="title">{{$document->fee_name}}:</span>
                                    <span class="value">{{ $document->payment_amount }}</span>
                                </li>
                                @if(activeValidate('2017-12-20 00:00:00','2017-12-25') and $current_customer and !in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::WHOLESALE]))
                                    <li>
                                        <span class="title">滿額折抵:</span>
                                        <span class="value">-NT${{ $document->full_quota_discount }}</span>
                                    </li>
                                @endif
                                <li>
                                    <span class="title">折扣:</span>
                                    <span class="value">-{{ $document->coupon_amount }}</span>
                                </li>
                                <li class="history-content-final-resum">
                                    <span class="title">總計:</span>
                                    <span class="value history-content-final-price">{{ $document->total }}</span>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            @if( $document->virtual_account )
                <ul class="history-info-table history-table-adjust">
                    <li class="history-table-content history-order-detail-table-content">
                        <ul class="history-table-content-resum-block col-sm-block col-xs-block">
                            <li class="col-md-12 col-sm-12 col-xs-12">
                                @if( $document->virtual_active )
                                    <ul>
                                        <li>
                                        <span>您本次購買的商品總金額為<span class="blue_text">新台幣 {{ $document->total }} 元</span>。</span>
                                        <span><span class="blue_text">請您在 {{ $document->virtual_deadline }}前匯款至以下帳戶：</span></span>
                                        <span>匯款銀行： 玉山銀行新莊分行</span>
                                        <span><B>銀行代碼： 808</B></span>
                                        <span><B>銀行帳號(共13碼)： {{$document->virtual_account}}</B></span>
                                        <span>戶 名：榮芳興業有限公司</span>
                                        <span>(轉帳手續費由匯款人負擔，使用ATM轉帳如超過當日匯款上限必須臨櫃填寫匯款單。)</span>
                                        <span></span>
                                        <span class="text-red-color">請注意:</span>
                                        <span class="text-red-color">1.此帳號為本筆訂單專屬之匯款帳號，若您下次購物，請不要匯款至此帳號。</span>
                                        <span class="text-red-color">2.請您確認您的匯款金額，若金額不同將無法匯款成功。</span>
                                        <span class="text-red-color">3.若超過匯款期限，此帳號將會自動關閉，訂單也會取消，您必須再重新操作一次結帳流程。</span>
                                        <span class="text-red-color">4.若您有其他問題與建議請"<a href="{{ route('customer-service-proposal-create',['ticket_type'=>'order','orders'=>$document->code]) }}">線上諮詢</a>"。</span>
                                        </li>
                                    </ul>
                                @elseif($document->virtual_finish)
                                    <span>※您已經匯款完成。</span>
                                @else
                                    <span class="text-red-color">※已超過匯款時間。</span>
                                    <span class="text-red-color">若您需要重新取得匯款帳號，請洽"<a href="{{ route('customer-service-proposal-create',['ticket_type'=>'order','orders'=>$document->code]) }}">線上諮詢</a>"。</span>
                                @endif
                            </li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
        <div class="history-transparent-box">
            <a class="btn btn-default border-radius-2 btn-send-the-query base-btn-gray" href="{{URL::route('customer-history-order')}}">回上一頁</a>
        </div>
    </form>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/history/history.js') !!}"></script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script>
        $('.history-page-state a span.action').click(function(){
            if($(this).attr('data-blank')){
                window.open($(this).attr('data-content'));
            }else{
                window.location = $(this).attr('data-content');
            }
        });
    </script>
@stop