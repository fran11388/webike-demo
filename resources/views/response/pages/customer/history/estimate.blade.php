@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>商品交期查詢履歷</h2>
    </div>
    <div class="history-transparent-box history-box-border">
        <h2>以下為商品查詢交期注意事項：</h2><br>
        <h3><i class="fa fa-check" aria-hidden="true"></i>此清單中的商品為"待查詢的商品明細"，請務必按下右下方"送出查詢"才會動作。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>商品交期資訊中，交期已回覆交期的計算為商品下訂後開始計算，以工作天為單位不包含假日；若是匯款轉帳，以匯款完成日開始計算。會在預估交期欄位中顯示，可以按"加入購物車"進行結帳；若交期還在查詢或售完，則無法加入購物車。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>交期查詢資訊回覆後僅 <span>保留7天</span>，超過7天後會自動清除紀錄，若您有需要請再次查詢。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>以下的交期資訊為該項商品目前的狀態，實際交期以結帳後我們寄送的"訂單交期通知"為準</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>庫存與交期狀態隨時變動，有可能發生下訂後商品售完之情況，若遇到此情況Webike客服人員會主動與您聯繫，請您協助我們處理訂單。</h3>
    </div>
    <form class="text-center" action="{{route('cart-update')}}" method="POST">
        <h2>商品交期資訊</h2>
        <hr>
            @if($collection->count())
                @foreach($collection as $estimate)
                    <div class="history-transparent-box" id="{{$estimate->code}}">
                        <div class="row">
                            <h3 class="col-md-5 col-sm-12 col-xs-12">查詢編號：{{$estimate->code}}</h3>
                            <h3 class="col-md-7 col-sm-12 col-xs-12">查詢日期：{{$estimate->created_at}}</h3>
                        </div>
                    </div>
                    <div class="history-table-container one-for-all">
                        <ul class="history-info-table history-table-adjust">
                            <li class="history-table-title visible-md visible-lg">
                                <ul>
                                    <li class="col-md-1 col-sm-1 col-xs-1">
                                        <label class="size-10rem"><input class="switch" type="checkbox">全選</label>
                                    </li>
                                    <li class="col-md-6 col-sm-6 col-xs-6"><span class="size-10rem">零件料號</span></li>
                                    <li class="col-md-1 col-sm-1 col-xs-1"><span class="size-10rem">查詢數量</span></li>
                                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">報價有效日期</span></li>
                                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">預估交期</span></li>
                                </ul>
                            </li>
                            <li class="history-table-content history-genuine-table-content">
                                @foreach($estimate->items as $item)
                                    <ul class="col-sm-block col-xs-block clearfix">
                                        <li class="col-md-1 col-sm-12 col-xs-12">
                                            <div>
                                                <input class="follower" type="checkbox" {!! $item->can_sell ? 'checked' : 'disabled' !!} name="sku[{{$item->url_rewrite}}]" value="{{$item->url_rewrite}}">
                                                <input type="hidden" name="qty[{{$item->url_rewrite}}]" value="{{$item->quantity}}">
                                                <input type="hidden" name="cache[{{$item->url_rewrite}}]" value="{{$item->cache_id}}">
                                            </div>
                                        </li>
                                        <li class="col-md-6 col-sm-12 col-xs-12 history-multi-content-estimate">
                                            <div class="history-content-estimate-block">
                                                <div class="history-content-img box-fix-with">
                                                    <a class="zoom-image" href="{!! $item->link !!}">
                                                        <img src="{{$item->thumbnail}}" alt="{{$item->name}}">
                                                    </a>
                                                </div>
                                                <div class="history-content-estimate col-md-9">
                                                    <a href="{!! $item->link !!}" title="{{$item->name}}">
                                                        <h2>{{$item->name}}</h2>
                                                    </a>
                                                    商品編號：{{$item->model_number}}<br><br>
                                                    @if(count($item->options))
                                                        <div class="box">
                                                            @foreach($item->options as $option)
                                                                {{$option}}<br>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                    @if(count($item->models))
                                                        <div class="box">
                                                        @foreach($item->models as $model)
                                                            對應車型：{{str_limit($model,'200','...')}}<br>
                                                        @endforeach
                                                        </div>
                                                    @endif
                                                    {{str_limit($item->summary,'200','...')}}
                                                </div>
                                            </div>
                                        </li>
                                        <li class="col-md-1 col-sm-12 col-xs-12"><span class="size-10rem">{{$item->quantity}}件</span></li>
                                        <li class="col-md-2 col-sm-12 col-xs-12">
                                            <span class="size-10rem">{{$estimate->expiration}} </span>
                                        </li>
                                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{{$item->delivery}}</span></li>
                                    </ul>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                @endforeach
                <div class="row ct-pagenav">
                    {!! $pager !!}
                </div>
            @else
                <div class="container">
                    <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
                </div>
            @endif

        <div class="history-transparent-box">
            <a class="btn btn-default pull-left" href="{{$base_url}}">回會員中心首頁</a>
            <button class="btn btn-danger pull-right">加入購物車</button>
        </div>
    </form>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/history/history.js') !!}"></script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    {{--<script>--}}
        {{--$('.history-btn-submit').click(function(){--}}
            {{--var items = $(this).closest('form').find('input[name^=item]');--}}
            {{--addToCart(items, $(this));--}}
        {{--});--}}
    {{--</script>--}}
@stop
