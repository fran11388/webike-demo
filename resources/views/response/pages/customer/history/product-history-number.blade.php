<div class="product-history-number-background">
    <div class="product-history-number">
        <div class="product-history-number-title">

            <a href="javascript:void(0)"><i class="fas fa-times"></i></a>
            <span>數量</span>
            
            <input type="hidden" name="sku" value="">
            <input type="hidden" name="model_number" value="">
            <input type="hidden" name="product_type" value="">
            <input type="hidden" name="divide" value="">
            <input type="hidden" name="manufacturer" value="">
        </div>
        <div class="product-history-number-content" style="background: #fff;">

            <div class="select-type" hidden>
            </div>

            <div class="number-check">
                <input type="number" min="0" id="amount" class="productNumber" onkeydown="this.value=this.value.substr(0,3)" value="1">
                <button class="w-botton-b plus">+</button>
                <button class="w-botton-b reduce">-</button>
            </div>
            
            <div class="genuine-section">
                <div style="border-top: 1px #ccc solid;padding: 4px 8px;">正廠零件備註：</div>
                <input name="note" placeholder="備註內容" value="" style="width: 100%;padding: 8px;border: 0;">
            </div>
            <div class="product-history-number-content-check">
                <button class="w-botton" id="execute">加入</button>
            </div>
        </div>
    </div>
</div>

<script>
    var plus = document.querySelector('.plus'),
        reduce = document.querySelector('.reduce'),
        amount = document.querySelector('.productNumber');

    plus.onclick = function(){
      amount.value++;
      amount.textcontent = amount.value;
    }
    reduce.onclick = function(){
      if(amount.value > 1){
        amount.value--;
        amount.textcontent = amount.value;
      }
    }


</script>