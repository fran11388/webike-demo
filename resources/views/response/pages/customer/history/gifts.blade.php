@extends('response.layouts.2columns')
@section ('style')
	<link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="ct-oem-part-right box-fix-auto gifts-container">
	    <div class="title-main-box">
	    	<h2>贈品兌換</h2>
	    </div>
	    <div class="history-transparent-box history-box-border manual">
	        <h3>※贈品兌換功能：Webike摩托百貨的會員，參加相關活動就有機會獲得免費的贈品。 </h3>
	        <h3>※贈品加入購物車：下方列表為已經獲得的贈品，只要在您想要兌換的贈品按下"加入購物車"，即可在購物車頁面看到。</h3>
	        <h3>※購物兌換：贈品在未加入購買商品時是無法結帳的，另外所有贈品都有兌換的期限，請於時限內使用完畢。</h3>
	        <h3>※贈品異常處理：若贈品兌換時發生庫存不足的狀況，本公司會於貨到後再補寄給您；其他異常的狀態，客服人員會主動聯繫協助您處理。</h3>
	    </div>
	    <div class="history-table-container">
	        <ul class="history-info-table history-table-adjust">
	            <li class="history-table-title visible-md visible-lg">
	                <ul>
	                    <li class="col-md-5 col-sm-7 col-xs-7"><span>贈品</span></li>
	                    <li class="col-md-3 col-sm-1 col-xs-1"><span>兌換期限</span></li>
	                    <li class="col-md-2 col-sm-1 col-xs-1"><span>數量</span></li>
	                    <li class="col-md-2 col-sm-1 col-xs-1"><span>操作</span></li>
	                </ul>
	            </li>


	            @php
	            	$gift_cart = $customerGifts->filter(function($gift){
	            		if($gift->status == 2){
	            			return $gift;
	            		}
	            	});
	            @endphp

	            <li class="history-table-content">
				@if(count($customerGifts))
					@foreach($customerGifts as $gift)
						@php
							$product = $products[$gift->gift->orig_product_id];
						@endphp
						@if($gift->status == '1' or $gift->status == '2')
							<ul class="col-sm-block col-xs-block clearfix">
								<li class="col-md-5 col-sm-12 col-xs-12 history-multi-content-estimate">
									<div class="history-content-estimate-block">
										<div class="history-content-img box-fix-with">
											<a class="zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" target="_blank">
												 <img src="{{ $product->getThumbnail() }}" alt="">
											</a>
										</div>
										<div class="clearfix">
											<div class="history-content-estimate col-md-10 col-sm-12 col-xs-12">
												<a href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}">
													<h2>
														{{ $product->manufacturer->name }} ： {{ $product->name }}
													</h2>
												</a>
													<span class="size-10rem word-break width-full">
														商品編號:{!! $product->model_number !!}</br>
														@if($options = $product->getSelectsAndOptions() and $options->count())
															規格：
																@foreach($options as $option)
																	{{ $option->label }}：{{ $option->option }}
																@endforeach
															<br/>
														@endif
												   		@if($fit_models = $product->getFitModels() and $fit_models->count())
															<div class="toggle-box">
																<div class="toggle-button  text-right">
																	<a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊...</a>
																</div>
																<div class="toggle-content hidden">
																	@foreach($fit_models as $maker => $models)
																			{{$maker}}：
																		@foreach($models as $key => $model)
																			@if($key != 0)
																					 /
																			@endif
																				{{$model->model . ' ' . $model->style}}
																			@if($loop->iteration == 3)
																					...
																					@break
																			@endif
																		@endforeach
																			@if($loop->iteration == 3)
																				...
																				@break
																			@endif
																	@endforeach
																		<br/>
																</div>
															</div>
														@endif
													</span>
												</div>
											</div>
										</div>
									</li>
									<li class="col-md-3 col-sm-12 col-xs-12">
										<span class="size-10rem text-center">{{ $gift->gift->expired_date }}</span>
									</li>

									<li class="col-md-2 col-sm-12 col-xs-12">
										<span class="size-10rem">1件</span>
									</li>
									<li class="col-md-2 col-sm-12 col-xs-12">
										@if(count($gift_cart) and $gift->status=='1')
											<button type='submit' class='btn btn-danger btn-full' disabled>加入購物車兌換</button>
										@elseif($gift->status == '1')
											<form class="box" method="post" action="{{ URL::route('customer-history-gifts')}}">
												<button type='submit' name="action" value="join_cart" class='btn btn-danger btn-full'>加入購物車兌換</button>
												<input type="hidden" name="gift" value="{{$gift->id}}">
											</form>
										@elseif($gift->status=='2')
											<form class="box" method="post" action="{{ URL::route('customer-history-gifts')}}">
												<button type='submit' name="action" value="remove_cart" class='btn btn-defult btn-full'>取消加入購物車</button>
												<input type="hidden" name="gift" value="{{$gift->id}}">
											</form>
										@endif
									</li>
								</li>
							</ul>
						@endif
					@endforeach
				@endif
	        </li>
	    </div>	  
	</div>
@stop
@section('script')
    <script type="text/javascript">
        function toggleCartInfo(_this){
            var target = $(_this);
            target.closest('.toggle-box').find('.toggle-content').toggleClass('hidden');
        }
    </script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
@stop