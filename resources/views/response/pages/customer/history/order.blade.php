@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>{{$title_text}}</h2>
    </div>
    <div class="history-transparent-box">
        <h3>您好，{{ $current_customer->realname }}，以下是{{$title_text}}，各筆訂單的詳細資訊請點選"查看訂單"。</h3>
    </div>
    <form action="">
        <div class="history-table-container">
            @if($collection->count())
                <ul class="history-info-table">
                    <li class="history-table-title visible-md visible-lg">
                        <ul>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">訂單編號</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂購日期</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂單金額</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">商品數量</span></li>
                            <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">訂單狀態</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">訂單明細</span></li>
                        </ul>
                    </li>
                    <li class="history-table-content history-genuine-table-content">
                        @foreach($collection as $item)
                            <ul class="col-sm-block col-xs-block clearfix">
                                <li class="col-md-3 col-sm-12 col-xs-12"><a href="{!! $item->link !!}"><span class="size-10rem blue-text">{!! $item->code !!}</span></a></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->created_at !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->total !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->quantity !!}</span></li>
                                <li class="col-md-3 col-sm-12 col-xs-12"><span class="size-10rem">{!! $item->state !!}</span></li>
                                <li class="col-md-2 col-sm-12 col-xs-12">
                                    <span class="size-10rem blue-text">
                                        <a href="{!! $item->link !!}">查看訂單</a>
                                        {!! $item->read_tip !!}
                                    </span>
                                </li>
                            </ul>
                        @endforeach
                    </li>
                </ul>
                <div class="row ct-pagenav">
                    {!! $pager !!}
                </div>
            @else
                <div class="container">
                    <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
                </div>
            @endif
        </div>
        <div class="center-box">
            <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
        </div>
    </form>

@stop
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
@stop
