@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <style type="text/css">
        @media (max-width: 992px){
            table td[class*=col-], table th[class*=col-]{
                float: left;
            }
        }
        #faq{
            float: left;
            width:100%;
            padding: 0 20px;
        }
        #faq .faq-block{
            border: 1px solid #000
        }
        #faq .faq-block .faq-block-title{
            border-bottom: 1px solid #000;
            padding: 10px;
            background: #000;
            color: #fff;
        }
        #faq .faq-block .faq-text{
            padding: 10px 20px 20px 10px;
        }
        #faq .question-block .question-title{
            background: #000;
            color: #fff;
            border-bottom: 1px solid #000;
            padding: 10px;
        }
        #faq .question-block textarea{
            width: 100%;
        }
        #faq .show-question-block{
            color: #7d7979;
            padding-left: 10px;
        }
        .ct-customer-center-page #faq .history-table-container ul li{
            float: none;
            padding: 0;
        }
        .ct-customer-center-page #faq .history-table-container ul li span{
            text-align: center;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>聯絡我們</h2>
    </div>
    <div class="row ct-customer-center-page">
        @include('response.pages.customer.contact.partials.navbar')
        <form method="POST" action="{!! URL::route('customer-service-proposal-create', $type->url_rewrite) !!}">
            <div class="tab-ct-customer-center-page">
                <div class="tap-ct-create-question">
                    <h2 class="title-tap-ct-create-question">問題提問</h2>
                    <div  class="tab-ct-customer-center-page-detail tab-ct-customer-center-question">
                        <table class="table-page-customer-question col-xs-12 col-sm-12 col-md-12">
                            <tr>
                                <td class="col-md-2 col-sm-4 col-xs-4">會員姓名：</td>
                                <td class="col-md-10 col-sm-8 col-xs-8">
                                    {{$current_customer->realname}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 col-sm-4 col-xs-4">Email：</td>
                                <td class="col-md-10 col-sm-8 col-xs-8">
                                    {{$current_customer->email}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 col-sm-4 col-xs-4">問題類型：</td>
                                <td class="col-md-10 col-sm-8 col-xs-8">
                                    {{$type->name}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 col-sm-6 col-xs-12 hidden-sm hidden-xs">問題分類：</td>
                                <td class="col-md-10 col-sm-6 col-xs-12 step-selects-group-1">
                                    <h5 class="visible-sm visible-xs">問題分類：</h5>
                                    <ul class="form-group from-owned-vehicles">
                                        <li class="col-xs-6 col-sm-6 col-md-4">
                                            <select name="family" class="select2 step-selects-station-1">
                                                <option class="default" disabled selected>請選擇問題類型</option>
                                            </select>
                                        </li>
                                        <li class="col-xs-6 col-sm-6 col-md-4">
                                            <select name="category_id" class="select2 step-selects-station-2">
                                                <option class="default" value="" selected>請選擇問題分類</option>
                                            </select>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @if($type->order_flg)
                                <tr>
                                    <td class="col-md-2 col-sm-4 col-xs-4">相關訂單：</td>
                                    <td class="col-md-10 col-sm-8 col-xs-8">
                                        <ul class="form-group from-owned-vehicles">
                                            <li class="col-xs-12 col-sm-6 col-md-4">
                                                <select class="select2 orders" name="orders">
                                                    <option class="default" disabled selected>請選擇欲詢問訂單</option>
                                                    @foreach($orders as $order)
                                                        <option value="{{$order->increment_id}}" data="{{$order->id}}">{{$order->increment_id}}</option>
                                                    @endforeach
                                                </select>
                                            </li>
                                            <li class="col-xs-12 col-sm-6 col-md-2">
                                                <a class="btn btn-asking pull-left" href="javascript:void(0)" onclick="getOrderSelect();">訂單編號查詢</a>
                                            </li>
                                        </ul>
                                    </td>
                                </td>
                            @endif
                            @foreach($relations['html'] as $relation)
                                {!! $relation !!}
                            @endforeach
                            <!-- <tr>
                                <td class="col-md-2 col-sm-4 col-xs-4 visible-lg visible-md">問題敘述 : </td>
                                <td class="col-md-10 col-sm-12 col-xs-12">
                                    <h5 class="visible-sm visible-xs">問題敘述：</h5>
                                    <textarea class="checkout-option-large-width" name="content" rows="6" placeholder="字數上限為800字..."></textarea>
                                </td>
                            </tr> -->
                        </table>

                    </div>
                    @include('response.pages.customer.contact.partials.faq')
                </div>
            </div>
        </form>
    </div>
    <div class="center-box">
        <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
    </div>
@stop
@section('script')
    <script type="application/javascript">
        var route_name = "{{Route::currentRouteName()}}";
    </script>
    <script src="{{assetRemote('js/pages/customer/contact.js')}}"></script>
    <script type="application/javascript">
        var types_layer = {!! $types_layer !!};
        @if(session()->has('msg'))
            swal(
                'Oops...',
                '請選擇問題分類！',
                'error'
            );
        @endif

        function putInStepSelects(target, jsonObject){
            $.each(jsonObject, function(key, optionValue){
                if($.type(optionValue) == 'object'){
                    $(target).append($('<option value="' + key + '">' + key + '</option>'));
                }else{
                    $(target).append($('<option value="' + optionValue + '">' + key + '</option>'));
                }
            });
        }
        putInStepSelects('.step-selects-group-1 .step-selects-station-1', types_layer["{{$type->name}}"]);
        function stepSelects(groupClass, thisStation, jsonObject){
            var station_font = 'step-selects-station-';
            var className = $(groupClass).find(thisStation).prop('class');
            var stepSort = className.substr(className.indexOf(station_font) + station_font.length, className.length);
            var next = $(groupClass).find('select.' + station_font + (parseInt(stepSort) + 1));
            $(next).addClass('disabled').prop('disabled', true);
            $(thisStation).on('change', function(){
                var value = $(this).val();
                $(next).find('option').not('.default').remove();
                if($(next).length && Object.keys(jsonObject[value]).length > 0){
                    next.removeClass('disabled').removeAttr('disabled');
                    $.each(jsonObject[value], function(key, optionValue){
                        $(next).append($('<option value="' + optionValue + '">' + key + '</option>'));
                    });
                }
            });
        }
        stepSelects('.step-selects-group-1', '.step-selects-station-1', types_layer["{{$type->name}}"]);

        $('form').submit(function(){
            var btn = $('.btn-send-the-query');
            btn.addClass('disabled').prop('disabled');
            var category_id = $('select[name="category_id"]');
            if(!category_id.val()){
                swal(
                        'Oops...',
                        '請選擇問題分類！',
                        'error'
                );
                btn.removeClass('disabled').removeAttr('disabled');
                return false;
            }
            var orders = $('select[name="orders"]');
            if(orders.length && !orders.val()){
                swal(
                        'Oops...',
                        '請選擇欲詢問之訂單！',
                        'error'
                );
                btn.removeClass('disabled').removeAttr('disabled');
                return false;
            }
            var content = $('textarea[name="content"]');
            var content_text = content.val().trim();
            if(content_text.length && content_text.length > 500) {
                swal(
                        'Oops...',
                        '輸入字數不可大於500字！',
                        'error'
                );
                btn.removeClass('disabled').removeAttr('disabled');
                return false;
            }else if(!content_text.length){
                swal(
                        'Oops...',
                        '尚未輸入回覆內容！',
                        'error'
                );
                btn.removeClass('disabled').removeAttr('disabled');
                return false;
            }
        });

        var orderSelect;
        function getOrderSelect()
        {
            if(orderSelect != null){
                orderSelect.close();
            }

            var url = "{{ route('customer-history-order-preview') }}";
            orderSelect = window.open(url,'_blank','width=1000,height=800,top=2%,left=2%,resizable=no,toolbar=no, menubar=no');
        }

        function setOrderSelect(order_increment_id)
        {
            $('select[name="orders"]').val(order_increment_id).trigger("change");
        }

        function setFamily(value)
        {
            $('select[name="family"]').val(value).trigger("change");
        }

        function setCategory(value)
        {
            $('select[name="category_id"]').val(value).trigger("change");
        }

        $(document).on('change', ".step-selects-group-1 .step-selects-station-1", function(){
            resetFaq();
        });

        $(document).on('click', "#faq .show-question-block .show-question", function(){
            $("#faq .question-block").removeClass('hidden');
        });

        function resetFaq(){
            $('#faq').replaceWith("<div id='faq'></div>");
        }

        // problem on change
        $(document).on('change', ".step-selects-group-1 .step-selects-station-2,.from-owned-vehicles .orders", function(){
            var value = $('.step-selects-group-1 .step-selects-station-2').find('option:selected').val();
            var token = $('meta[name=csrf-token]').attr('content');
            var order = $('.orders').find('option:selected').attr('data');
            resetFaq();
            if(value){
                var data={'_token':token,'category_id' : value,'order' : order};
                var current_url=window.location.href;
                var url=new URL(current_url);
                url.searchParams.forEach(function(item,index,array){
                    data[index]=item;
                })
                $.ajax({
                    url: "{!! route('customer-service-proposal-getfaq') !!}",
                    data:data,
                    type:"POST",
                    dataType:'JSON',
                    success:function(result){
                       $('#faq').replaceWith(result.html);
                    },
                    error:function(xhr,ajaxOptions,thrownError,textStatus){

                    }
                });
            }else{
                swal("請選擇問題分類");
            }
        });
    </script>
    @foreach($relations['javascript'] as $relation)
        {!! $relation !!}
    @endforeach
@stop
