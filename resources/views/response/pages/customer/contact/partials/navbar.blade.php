<ul class="nav nav-tabs btn-tabs-customer-info">
    <li><a name="customer-service-message" href="{{URL::route('customer-service-message')}}">新訊息通知<span class="tips ticket"></span></a></li>
    <li><a name="customer-service-history" href="{{URL::route('customer-service-history')}}">問題紀錄</a></li>
    <li><a name="customer-service-proposal" href="{{URL::route('customer-service-proposal')}}">問題提問</a></li>
    <li><a name="customer-service-info" href="{{URL::route('customer-service-information')}}">客服資訊</a></li>
</ul>