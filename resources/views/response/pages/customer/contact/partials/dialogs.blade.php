@foreach($replies as $replie)
    <tr class="dialog">
        <td class="col-xs-12 col-sm-12 col-md-2 visible-md visible-lg">
            @if($replie->replyer->type == 'service')
                客服人員:<br>
            @else
                {{$replie->replyer->name}}:<br>
            @endif
            {{$replie->timestamp}} <br>
            @if($replie->replyer->type != 'service' and $replie->read)
                (已受理)
            @endif
        </td>
        <td class="col-xs-12 col-sm-12 col-md-9">
            <span class="visible-sm visible-xs">
                @if($replie->replyer->type == 'service')
                    客服人員:<br>
                @else
                    {{$replie->replyer->name}}:<br>
                @endif
                {{$replie->timestamp}} <br>
                @if($replie->replyer->type != 'service' and $replie->read)
                    (已受理)
                @endif
            </span>
            <span class="text-message-histories {{$replie->replyer->type == 'service' ? 'question' : 'answer'}}">
                {!! nl2br(echoWithLinkTag($replie->content)) !!}
            </span>
        </td>
    </tr>
@endforeach
