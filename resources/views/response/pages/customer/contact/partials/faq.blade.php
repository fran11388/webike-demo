<div id="faq">
	@if(isset($answer))
		<div class="faq-block">
			<div class="faq-block-title">系統即時訊息回覆</div>
			<div class="faq-text clearfix">
				<span>{!! $answer['faq_text'] !!}</span>
			</div>
			@if($answer['default'] && !isset($answer['choose']))
				<div class="show-question-block">※ 如以上仍無法解答您的問題，或是有任何需求，請<a class="show-question" href="javascript:void(0)">按此</a>與客服人員聯絡。</div>
			@endif
		</div>
		<div class="question-block content-last-block {{$answer['default'] ? 'hidden' : ''}}">
			<div class="clearfix">
			    	<div class="visible-lg visible-md question-title">問題敘述 </div>
			        <h5 class="visible-sm visible-xs">問題敘述：</h5>
			        <textarea class="checkout-option-large-width" name="content" rows="6" placeholder="字數上限為800字..."></textarea>
		    	</div>
			<div><button class="btn btn-danger border-radius-2 btn-send-the-query">送出</button></div>
		</div>
    @endif
</div>