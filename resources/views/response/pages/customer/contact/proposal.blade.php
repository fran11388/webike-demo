@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h1>線上客服諮詢</h1>
    </div>
    <div class="row ct-customer-center-page">
        @include('response.pages.customer.contact.partials.navbar')
        <div class="tab-ct-customer-center-page">
            <div class="tap-ct-create-question">
                @foreach($departments as $department)
                <h1>{{$department->frontend_name}}</h1>
                <div  class="row tab-ct-customer-center-page-detail tab-ct-customer-center-question tab-ct-customer-center-question-catrgories">
                    <ul class="ct-box-question-categories">
                        @foreach($department->types as $key => $type)
                            @if($key%3 == 0 and $key != 0)
                                </ul>
                                <ul class="ct-box-question-categories">
                            @endif
                            <li class="col-xs-12 col-sm-6 col-md-4">
                                <div class="ct-box-question-categories-text">
                                    <div class="titlle-box-question-categories">
                                        <h3><a href="{{URL::route('customer-service-proposal-create', $type->url_rewrite)}}">{{$type->name}}</a></h3>
                                    </div>
                                    <span>{!! $type->description !!}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="center-box">
        <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
    </div>
@stop
@section('script')
    <script>
        $(document).ready(function(){
            confirmActiveUseName('.nav-tabs li a', '.nav-tabs li', "{{Route::currentRouteName()}}");
        });
    </script>
@stop
