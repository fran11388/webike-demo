@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h1>線上客服諮詢</h1>
    </div>
    <div class="row ct-customer-center-page">
        @include('response.pages.customer.contact.partials.navbar')
    </div>
    @if($types->count())
        <div class="center-box row">
            <div class="col-md-offset-9 col-md-3 col-sm-12 col-xs-12">
                <form method="GET">
                    <select class="select2" name="type_url_rewrite">
                        <option class="default" value="" {{!request()->input('type_url_rewrite') ? 'selected' : ''}}>篩選問題分類</option>
                        @foreach($types as $type)
                            <option value="{{$type->url_rewrite}}" {{$type->url_rewrite == request()->input('type_url_rewrite') ? 'selected' : ''}}>{{$type->name}}</option>
                        @endforeach
                    </select>
                </form>
            </div>
        </div>
    @endif
    <div class="history-table-container">
    @if($collection->count())
            <ul class="history-info-table">
                <li class="history-table-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span>訊息時間</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span>問題類型</span></li>
                        <li class="col-md-4 col-sm-4 col-xs-4"><span>最新訊息內容</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span>備註</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span>問題編號</span></li>
                    </ul>
                </li>
                <li class="history-table-content history-genuine-table-content">
                    @foreach($collection as $item)
                        <ul class="col-sm-block col-xs-block clearfix">
                            <li class="col-md-2 col-sm-12 col-xs-12">
                                <span class=" blue-text">{!! $item->created_at !!}</span>
                            </li>
                            <li class="col-md-2 col-sm-12 col-xs-12"><span>{{$item->type}}</span></li>
                            <li class="col-md-4 col-sm-12 col-xs-12">
                                <span class="history-genuine-table-content-blue">
                                    <a href="{{$item->link}}">{{$item->latest_reply}}</a>
                                </span>
                            </li>
                            <li class="col-md-2 col-sm-12 col-xs-12">
                                <span>{!! $item->note !!}</span>
                            </li>
                            <li class="col-md-2 col-sm-12 col-xs-12">
                                <a href="{{$item->link}}" title="{{$item->code . $tail}}">
                                    <span class="font-16px blue-text history-genuine-table-content-blue">{{$item->code}} </span>
                                </a>
                            </li>
                        </ul>
                    @endforeach
                </li>
            </ul>
            <div class="row ct-pagenav">
                {!! $pager !!}
            </div>
        @else
            <div class="container">
                <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
            </div>
        @endif
    </div>
    <div class="center-box">
        <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
    </div>
@stop
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script type="application/javascript">
        var route_name = "{{Route::currentRouteName()}}";
    </script>
    <script src="{{assetRemote('js/pages/customer/contact.js')}}"></script>
    <script type="application/javascript">
        $('select[name="type_url_rewrite"]').on('change', function(){
             $(this).closest('form').submit();
        });
    </script>
@stop
