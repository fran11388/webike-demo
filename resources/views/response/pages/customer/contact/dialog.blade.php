@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
    <style type="text/css">

        @media (max-width: 992px){
            table td[class*=col-], table th[class*=col-]{
                float: left;
            }
        }
        .reply-area{
            display: none;
        }
    </style>
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>聯絡我們</h2>
    </div>
    <div class="row ct-customer-center-page">
        @include('response.pages.customer.contact.partials.navbar')

        <div class="tab-ct-customer-center-page">
            <div class="tab-ct-customer-center-page-detail tab-ct-customer-center-question">
                <h2 class="title-tap-ct-create-question">問題詳細</h2>
                <table class="table-page-customer-question col-md-6 col-sm-12 col-xs-12">
                    <tr>
                        <td class="col-md-4 ol-sm-6 col-xs-6">開啟時間:</td>
                        <td class="col-md-8 ol-sm-6 col-xs-6">
                            {{$ticket->created_at}}
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4 ol-sm-6 col-xs-6">問題類型:</td>
                        <td class="col-md-8 ol-sm-6 col-xs-6">
                            {{$ticket->category}}
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4 ol-sm-6 col-xs-6">問題編號:</td>
                        <td class="col-md-8 ol-sm-6 col-xs-6">
                            {{$ticket->increment_id}}
                        </td>
                    </tr>
                </table>
                <table class="table-page-customer-question col-xs-12 col-sm-12 col-md-6">
                    <tr>
                        <td class="col-md-4 ol-sm-6 col-xs-6">備註:</td>
                        <td class="col-md-8 ol-sm-6 col-xs-6">
                            @foreach($relations as $relation)
                                {!! $relation !!}<br>
                            @endforeach
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="tab-ct-customer-center-page">
            <div class="tab-ct-customer-center-page-detail tab-ct-customer-center-question">
                <h2 class="title-tap-ct-create-question">訊息內容與回覆</h2>
                <table class="table-page-customer-question col-xs-12 col-sm-12 col-md-12 dialogs-area">
                    <tr>
                        <td class="col-xs-12 col-sm-12 col-md-2"></td>
                        <td class="col-xs-12 col-sm-12 col-md-9">
                            <button id="start-reply" class="btn btn-danger border-radius-2 btn-send-the-query2">再回覆</button>
                        </td>
                    </tr>
                    <tr class="reply-area">
                        <td class="col-xs-12 col-sm-12 col-md-2">請輸入您的回覆:</td>
                        <td class="col-xs-12 col-sm-12 col-md-9">
                            <div class="form-group">
                                <textarea name="content" class="form-control checkout-option-large-width" rows="4"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr class="reply-area">
                        <td class="col-xs-12 col-sm-12 col-md-2"></td>
                        <td class="col-xs-12 col-sm-12 col-md-9">
                            <button id="send-replay" class="btn btn-danger border-radius-2 border-radius-2 btn-send-the-query2 btn-send-the-query4">送出回覆</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="center-box">
        <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
    </div>
@stop
@section('script')
    <script>
        var route_name = "{{Route::currentRouteName()}}";
    </script>
    <script src="{{assetRemote('js/pages/customer/contact.js')}}"></script>
    <script>
        function getDialogs(){
            $('.dialogs-area tr.dialog').remove();
            $.ajax({
                url: "{!! URL::route('customer-service-render-dialogs', $ticket->url_rewrite) !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content')},
                type:"POST",
                dataType:'html',

                success: function(result){
                    $('.dialogs-area').prepend(result);
                },

                error:function(xhr, ajaxOptions, thrownError){
                    swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                    )
                }
            });
        }

        $(document).ready(function(){
            getDialogs();
        });

        $('#start-reply').click(function(){
            $('.reply-area').toggle();
            if($('.reply-area').css('display') == 'none'){
                $(this).removeClass('base-btn-gray').addClass('btn-danger');
            }else{
                $(this).addClass('base-btn-gray').removeClass('btn-danger');
            }
        });

        function sendReply(){
            var area = $('#send-replay');
            area.addClass('send-replay');
            var content = $('textarea[name="content"]');
            var content_text = content.val().trim();
            console.log(content_text.length);
            content.closest('.form-group').removeClass('has-error');
            if(content_text.length && content_text.length <= 500) {
                $.ajax({
                    url: "{!! URL::route('customer-service-reply') !!}",
                    data: {
                        '_token': $('meta[name=csrf-token]').prop('content'),
                        'content': content_text,
                        'increment_id': "{!! $ticket->increment_id !!}"
                    },
                    type: "POST",
                    dataType: 'html',

                    success: function (result) {
                        area.removeClass('disabled').removeAttr('disabled');
                        content.val('');
                        getDialogs();
                    },

                    error: function (xhr, ajaxOptions, thrownError) {
                        area.removeClass('disabled').removeAttr('disabled');
                        swal(
                                'Oops...',
                                'Something went wrong!',
                                'error'
                        );
                    }
                });
            }else if(!content_text.length){
                area.removeClass('disabled').removeAttr('disabled');
                content.closest('.form-group').addClass('has-error');
                swal(
                        'Oops...',
                        '尚未輸入回覆內容!',
                        'error'
                );
                slipTo('textarea[name=content]');
            }else{
                area.removeClass('disabled').removeAttr('disabled');
                content.closest('.form-group').addClass('has-error');
                swal(
                        'Oops...',
                        '輸入字數不可大於500字!',
                        'error'
                );
                slipTo('textarea[name=content]');
            }
        }

        $('#send-replay').on('click', function(){
            if($(this).prop('class').indexOf('disabled') >= 0){
                return false;
            }
            $(this).addClass('disabled').prop('disabled', true);
            sendReply();
        });

        //to post has read
        $.ajax({
            url: "{!! URL::route('customer-service-read') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content'), 'increment_id': "{!! $ticket->increment_id !!}"},
            type:"POST",
            dataType:'html',

            success: function(result){
//                console.log('success');
            },

            error:function(xhr, ajaxOptions, thrownError){
                swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                )
            }
        });

    </script>
@stop
