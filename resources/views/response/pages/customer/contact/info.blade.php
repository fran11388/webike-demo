@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h1>線上客服諮詢</h1>
    </div>
    <div class="row ct-customer-center-page">
        @include('response.pages.customer.contact.partials.navbar')
        <div class="tab-content tab-ct-customer-center-page">
            <div id="menu3" class="tab-ct-customer-center-page-detail tab-pane fade in active">
                <div class="tab-ct-customer-center-page-detail-left col-xs-12 col-sm-6 col-md-16">
                    <h1 class="title">「Webike摩托百貨」客服資訊  </h1>
                    <ul class="tab-ct-bottom-customer-center-page">
                        <li>營運公司：榮芳興業有限公司(<a href="http://www.everglory.asia/" title="榮芳興業有限公司" target="_blank" nofollow>公司概要</a>)</li>
                        <li>地址：{!! OFFICE_ADDRESS !!}</li>
                        <li>客戶服務時間：周一至周五 9:30-17:30(國定例假日除外)</li>
                    </ul>
                </div>
                <div class="tab-ct-customer-center-page-detail-left col-xs-12 col-sm-6 col-md-16">
                    <h1 class="title"> 線上問題諮詢</h1>
                    <ul class="tab-ct-bottom-customer-center-page">
                        <li>
                            <a target="_blank" href="{{route('customer-service-proposal-create',['ticket_type' => 'order'])}}">訂單及配送問題 </a>
                        </li>
                        <li>
                            <a target="_blank" href="{{route('customer-service-proposal-create',['ticket_type' => 'service'])}}">網站操作及商品諮詢</a>
                        </li>
                        <li>
                            <a target="_blank" href="{{route('customer-service-proposal-create',['ticket_type' => 'support'])}}">售後服務相關問題</a>
                        </li>
                        <li>
                            <a target="_blank" href="{{route('customer-service-proposal-create',['ticket_type' => 'biz'])}}">經銷夥伴招募</a>
                        </li>
                        <li>
                            <a target="_blank" href="{{route('customer-service-proposal-create',['ticket_type' => 'supply'])}}">供應商合作洽談</a>
                        </li>
                        <li>
                            ※為提供顧客最快速與專業的服務，請依據您的問題類型提問。
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="center-box">
        <a class="btn btn-default" href="{!! $base_url !!}">回會員中心首頁</a>
    </div>
@stop
@section('script')
    <script>
        $(document).ready(function(){
            confirmActiveUseName('.nav-tabs li a', '.nav-tabs li', "{{Route::currentRouteName()}}");
        });
    </script>
@stop
