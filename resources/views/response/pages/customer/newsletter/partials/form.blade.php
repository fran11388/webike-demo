<td class="col-xs-3 col-sm-3 col-md-3">電子報訂閱</td>
<td class="col-xs-9 col-sm-9 col-md-9">
    @if(!is_null(old('newsletter')))
        <label class="radio-inline"><input type="radio" value="1" name="newsletter" {{ old('newsletter') == '1' ? 'checked' : ''   }}>是</label>
        <label class="radio-inline"><input type="radio" value="0" name="newsletter" {{ old('newsletter') != '1' ? 'checked' : ''   }}>否</label>
    @else
        <label class="radio-inline"><input type="radio" value="1" name="newsletter" {{ $current_customer->newsletter == '1' ? 'checked' : ''   }}>是</label>
        <label class="radio-inline"><input type="radio" value="0" name="newsletter" {{ $current_customer->newsletter != '1' ? 'checked' : ''   }}>否</label>
    @endif
</td>