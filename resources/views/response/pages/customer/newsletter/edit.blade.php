@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>訂閱電子報</h2>
    </div>
    @include('response.common.message.error')
    @include('response.common.message.success')
    <form method="POST" action="{{ route('customer-newsletter') }}">
        <div class="box-items-group-customer-account">
            <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
                <tr>
                    @include('response.pages.customer.newsletter.partials.form')
                </tr>
            </table>

        </div>
        <div class=" text-center col-xs-12 col-sm-12 col-md-12"> <input class=" btn btn-primary border-radius-2 btn-danger btn-confirm-the-changes" type="submit" name="" value="確 認 修 改"></div>
    </form>
@stop
@section('script')
    <script type="text/javascript" src="{!! assetRemote('js/pages/customer/account.js') !!}"></script>
@stop
