@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="ct-oem-part-right box-fix-auto">
        <div class="title-main-box">
            <h2>感謝您的查詢!!</h2>
        </div>
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">您的查詢編號為：{!! session('code') !!}</h1>
        </div>
        <div class="box-info-oem box-info-compalate">
            請注意：<br>
            1.我們同一時間也寄一封查詢明細至您的e-mail信箱，供您確認。<br>
            2.一般商品交期查詢<B>1~2</B>個工作天，查詢結果會回復至您的e-mail信箱，或您也可以進入"
            <a href="{{route('customer')}}" title="會員中心{!! $tail !!}">會員中心</a>"→"
            <a href="{{route('customer-history-estimate')}}" title="商品交期查詢履歷{!! $tail !!}">商品交期查詢履歷</a>"中查看。<br>
            3.您如果超過3天內沒有收到報價回復，請來信service@webike.tw，我們會隨即幫您確認。<br>
            4.商品交期資訊再回覆後僅保持7天時間，時間超過後會自動刪除，若有需要請再次查詢。<br>
        </div>
        <div class="center-box">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{!! $home_url !!}">回首頁</a>
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{{route('customer-history-estimate')}}">商品交期查詢履歷</a>
        </div>
    </div>
@stop
@section('script')
@stop