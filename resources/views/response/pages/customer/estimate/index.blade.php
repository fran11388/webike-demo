@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>商品交期待查清單</h2>
    </div>
    <div class="history-transparent-box history-box-border">
        <h3><i class="fa fa-check" aria-hidden="true"></i>●操作方法請詳閱：商品交期查詢操作說明</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>●此清單中的商品為"待查詢的商品明細"，請務必按下右下方"送出查詢"才會動作。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>●單項商品查詢數量預設為"1"，若有複數商品請於下方數量欄位自行修改。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>●送出查詢後此清單項目會清空，同一時間您查詢的項目會移動至"商品交期查詢履歷"，查詢的進度也於該頁進行顯示。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>●此項服務完全免費，只要「Webike-摩托百貨」有上架之商品皆可查詢。​​​​​​​</h3>
    </div>



    <div class="history-table-container">
        @if(count($carts))
            <ul class="history-info-table history-table-adjust">
                <li class="history-table-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-7 col-sm-7 col-xs-7"><span class="size-10rem">商品</span></li>
                        <li class="col-md-3 col-sm-3 col-xs-3"><span class="size-10rem">數量</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">管理</span></li>
                    </ul>
                </li>
                <li class="history-table-content">
                    @foreach($carts as $cart)
                        <?php $product = $cart->product; ?>
                        <ul class="col-sm-block col-xs-block clearfix">
                            <li class="col-md-7 col-sm-12 col-xs-12 history-multi-content-estimate">
                                <div class="history-content-estimate-block">
                                    <div class="history-content-img box-fix-with">
                                        <a class="zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}">
                                            <img src="{{ $product->getThumbnail() }}" alt="">
                                        </a>
                                    </div>
                                    <div class="history-content-estimate col-md-10">
                                        <h2>{{ $product->getManufacturerName() }} ： {{ $product->name }}</h2>
                                        <span>商品編號：{{ $product->model_number }}</span><br/>
                                        @if($options = $product->getSelectsAndOptions() and $options->count())
                                            <span>規格：
                                                @foreach($options as $option)
                                                    {{ $option->label }}：{{ $option->option }}
                                                @endforeach
                                        </span><br/>
                                        @endif
                                        @if($fit_models = $product->getFitModels() and $fit_models->count())
                                            <span>對應車型：
                                                @foreach($fit_models as $maker => $models)
                                                    {{$maker}}：
                                                    @foreach($models as $key => $model)
                                                        @if($key != 0)
                                                            /
                                                        @endif
                                                        {{$model->model . ' ' . $model->style}}
                                                        @if($loop->iteration == 3)
                                                            ...
                                                            @break
                                                        @endif
                                                    @endforeach
                                                    @if($loop->iteration == 3)
                                                        ...
                                                        @break
                                                    @endif
                                                @endforeach
                                        </span><br/>
                                        @endif
                                        @if($product->getProductDescription())
                                            <span>要點：{{str_limit($product->getProductDescription()->summary,'200','...')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </li>
                            <li class="col-md-3 col-sm-13 col-xs-13 history-content-cart-option">
                                <form method="POST" action="{{ route('customer-estimate-update') }}">
                                    <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                                    <span><input type="text" name="qty" value="{{ $cart->quantity }}"></span>
                                    <span><button class="btn btn-default history-btn-default">修改數量</button></span>
                                </form>
                            </li>
                            <li class="col-md-2 col-sm-12 col-xs-12">
                                <form method="POST" action="{{ route('customer-estimate-remove') }}">
                                    <input name="code" type="hidden" value="{{ $cart->protect_code }}">
                                    <span><button class="btn btn-default history-btn-default">刪除</button></span>
                                </form>
                            </li>
                        </ul>
                    @endforeach
                </li>
            </ul>
        @else
            <div class="container">
                <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
            </div>
        @endif

    </div>
    <div class="history-transparent-box">
        <button class="btn btn-default base-btn-gray border-radius-2 btn-send-the-query" onclick="history.back();">回上一頁</button>
        @if(count($carts))
            <form method="POST" action="{{ route('customer-estimate') }}">
                <button class="btn btn-danger border-radius-2 btn-send-the-query history-btn-submit">送出查詢</button>
            </form>
        @endif
    </div>



@stop
@section('script')
@stop