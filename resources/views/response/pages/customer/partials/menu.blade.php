<link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>訂單相關</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            @if(isset($order_ing_count))
            <li class="{{$order_ing_count ? 'active' : ''}}{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-order-ing') }}" title="正在進行中的訂單{{$tail}}"><span>正在進行中的訂單</span><span>{{$order_ing_count ? '(' . $order_ing_count . ')' : ''}}</span></a>
            </li>
            @endif
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-order') }}" title="訂單動態及歷史履歷{{$tail}}"><span>訂單動態及歷史履歷</span><span class="tips order"></span></a>
            </li>

                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ route('customer-history-purchased') }}" title="My Style?{{$tail}}"><span>購入商品履歷</span><span class="tips order"></span></a>
                </li>

            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('cart') }}"><span>我的購物車</span><span class="icon-count cart"></span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-wishlist') }}"><span>待購清單</span><span class="icon-count wishlist"></span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-return') }}" title="退換貨申請履歷{{ $tail }}"><span>退換貨申請履歷</span><span class="tips"></span></a>
            </li>
        </ul>
    </div>
</div>

<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>商品查詢</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-genuineparts') }}" title="正廠零件查詢履歷{{ $tail }}"><span>正廠零件查詢履歷</span><span class="tips estimate-genuine"></span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-estimate') }}"><span>商品交期待查清單</span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-estimate') }}"><span>商品交期查詢履歷</span><span class="tips estimate-general"></span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-mitumori') }}" title="未登錄商品查詢履歷{{ $tail }}"><span>未登錄商品查詢履歷</span><span class="tips mitumori"></span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-groupbuy') }}" title="團購商品查詢履歷{{ $tail }}"><span>團購商品查詢履歷</span><span class="tips groupbuy"></span></a>
            </li>
        </ul>
    </div>
</div>

<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>線上客服諮詢</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{URL::route('customer-service-message')}}"><span>新訊息通知</span><span class="tips ticket"></span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{URL::route('customer-service-history')}}"><span>問題紀錄</span></a>
            </li>
            <li>
                <a href="{{URL::route('customer-service-proposal')}}"><span>問題提問</span></a>
            </li>
            <li>
                <a href="{{URL::route('customer-service-information')}}"><span>客服資訊</span></a>
            </li>
        </ul>
    </div>
</div>

<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>我的帳號</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li class="{{$current_customer ? '' : 'disabled'}}">
                @if(!$current_customer or $current_customer->role_id != \Everglory\Constants\CustomerRole::REGISTERED)
                    <a href="{{ route('customer-account') }}"><span>基本資料管理(帳號/購物)</span></a>
                @else
                    <a href="{{ route('customer-account-complete') }}"><span>完整註冊</span></a>
                @endif

            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-mybike') }}"><span>MyBike登錄</span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-points') }}"><span>點數獲得及使用履歷</span></a>
            </li>
            {{--@if(false)--}}
                {{--<li class="{{$current_customer ? '' : 'disabled'}}">--}}
                    {{--<a href="{{ route('customer-history-gifts') }}"><span>贈品兌換</span><span class="icon-count gift"></span></a>--}}
                {{--</li>--}}
            {{--@endif--}}
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-history-review') }}"><span>購買商品撰寫評論</span></a>
            </li>
            <li class="{{$current_customer ? '' : 'disabled'}}">
                <a href="{{ route('customer-newsletter') }}"><span>訂閱電子報</span></a>
            </li>
        </ul>
    </div>
</div>

<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>購物及服務說明</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li>
                <a href="{{route('customer-rule','member_rule_2').'#A'}}"><span>購買商品</span></a>
            </li>
            <li>
                <a href="{{route('customer-rule','member_rule_2').'#G'}}"><span>運送方式</span></a>
            </li>
            <li>
                <a href="{{route('customer-rule','member_rule_2').'#J'}}"><span>發票開立及規定</span></a>
            </li>
            <li>
                <a href="{{route('customer-rule','member_rule_2').'#M'}}"><span>訂單取消與退換貨</span></a>
            </li>
            <li>
                <a href="{{route('customer-rule','member_rule_3')}}"><span>會員條約</span></a>
            </li>
            <li>
                <a href="{{route('customer-rule','member_rule_8')}}"><span>隱私權保護政策</span></a>
            </li>
        </ul>
    </div>
</div>
<!--
<div class="box-page-group ct-customor-center">
    <div class="ct-box-page-group">
        <ul class="ul-banner-ct-box-page-group">
            <li class="btn-customor-center">
                <a href="{{ MOTOMARKET }}"><figure class="zoom-image"><img src="{!! assetRemote('image/btn-customor-center.jpg') !!}" alt="banner"></figure></a>
            </li>
        </ul>
    </div>
</div>
-->