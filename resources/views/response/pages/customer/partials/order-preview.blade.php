<!DOCTYPE html>
<html>
    <head>
        <title>訂單列表</title>
        @include('response.layouts.partials.head')
        <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
        <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
        <style type="text/css">
            .windows{
                padding:4%;
                max-height: 480px;
                overflow-y: scroll;
            }
            @media screen and (max-width: 767px) {
                .windows{
                    padding:2%;
                }
            }
            .history-info-table .history-table-content ul{
                cursor: pointer;
            }
        </style>
    </head>
    <body>
    <div class="title-main-box">
        <h2>訂單一覽</h2>
    </div>
    <div class="windows history-table-container">
        @if($collection->count())
            <ul class="history-info-table form-group">
                <li class="history-table-title">
                    <ul>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">選擇</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">訂單編號</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">訂購日期</span></li>
                        <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">訂單金額</span></li>
                        <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">購買項目</span></li>
                    </ul>
                </li>
                <li class="history-table-content history-genuine-table-content">
                    @foreach($collection as $order)
                        <ul>
                            <li class="col-md-2 col-sm-2 col-xs-2">
                                <div class="radio">
                                    <label>
                                        <input class="form-control" type="radio" name="increment_id" value="{{$order->code}}">
                                    </label>
                                </div>
                            </li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><a href="{!! $order->link !!}" target="_blank"><span class="font-16px blue-text">{!! $order->code !!}</span></a></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{!! $order->created_at !!}</span></li>
                            <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">{!! $order->total !!}</span></li>
                            <li class="col-md-4 col-sm-4 col-xs-4">
                                <div class="text-left">
                                    @foreach($order->items as $item)
                                        {{$item->name . ($item->options ? '(' . $item->options . ')' : '')}}<br>
                                        數量：{{$item->quantity}}<br>
                                        單價：{{$item->price}}<br>
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                    @endforeach
                </li>
            </ul>
        @else
            <div class="container">
                <h2 class="center-box text-second-menu">目前沒有查詢資料</h2>
            </div>
        @endif
    </div>
    <div class="container text-center">
        <a class="btn btn-danger" href="javascript:void(0)" onclick="finish();">選擇此訂單</a>
    </div>
    @include('response.layouts.partials.script')
    <script type="text/javascript">
        $('.history-info-table .history-table-content ul').click(function(){
            $(this).find('input').get(0).click();
        });

        function finish(){
            if( $('input[name=increment_id]:checked').length > 0 ){
                var increment_id = $('input[name=increment_id]:checked').val();
                //alert('回傳');
                window.opener.setOrderSelect(increment_id);
                $(window).unbind("beforeunload");
                window.close();
            }else{
                alert('請選擇訂單。');
                return false;
            }
        }
    </script>
    </body>
</html>


