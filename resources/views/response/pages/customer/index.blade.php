@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')

    <div class="title-main-box">
        <h1>會員好康</h1>
    </div>
    <div class="row ct-customer-center-page">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_01.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">日本、美國、義大利、泰國、台灣…超過1300個國際知名品牌，一次購足；不僅如此，每周皆有新品牌陸續加入產品線。</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_02.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">超過38萬項「改裝零件」及「騎士用品」，還有獨家「正廠零件」供應系統，提供您最全方面的產品需求。</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_03.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">定期推出會員專屬優惠活動及OUTLET特賣，不定期也會舉辦各式抽獎及現金點數集點活動。周周有驚喜!天天有好康!</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_04.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">會員終身免費，一個帳號即可同時使用【摩托百貨】與【摩托車市】的服務；「Webike」工程團隊也會持續開發更便利的新功能，滿足您的需求。</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            @if(hasChangedLogistic())
                <img src="{!! assetRemote('image/banner/10_Customer_Center_05_new.png') !!}" alt="image name">
                <span class="dotted-text3 force-limit">「新竹物流」配送到府。訂單金額滿3000可刷卡3期0利率，滿6000可6期0利率、12期低利分期。</span>
            @else
                <img src="{!! assetRemote('image/banner/10_Customer_Center_05.png') !!}" alt="image name">
                <span class="dotted-text3 force-limit">凡購物滿2千元，台灣本島免運費，「台灣宅配通」配送到府。信用卡滿3000元分期0利率。</span>
            @endif
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_06.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">超過220家「Webike經銷商」遍布全台，提供顧客商品諮詢、產品訂購、安裝維修…等服務。</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_07.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">採用「DHL」航空快遞每日進口，每周一至五急速發貨，並投保貨物險，安全有保障!</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_08.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">「Webike」編輯部為您帶來最新業界頭條、新車試乘報告、國內外賽事報導、機車相關商品及服務訊息每日更新，還有車界知名作家專欄請不要錯過。</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <img src="{!! assetRemote('image/banner/10_Customer_Center_09.png') !!}" alt="image name">
            <span class="dotted-text3 force-limit">本公司為中華民國台灣合法設立，所有商品走合法報關管道，並且均開立統一發票，提供給您最有保障的消費權益。</span>
        </div>
    </div>

@stop
@section('script')
    <script type="application/javascript">
        getDispCount();
    </script>
@stop