@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>待購清單</h2>
    </div>
    <div class="history-transparent-box history-box-border">
        <h3><i class="fa fa-check" aria-hidden="true"></i>※交貨期以同一訂單內商品的最晚交貨期為準。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>※請注意部分商品的交貨期較長，另外廠商的庫存情況隨時都有變化，您所選購的商品也可能因為廠商缺貨而需要更長的等候時間。甚至因為商品在您下訂之後剛好售罄而停產，如遇此情形，請恕我們必須取消您對該項商品的訂購。</h3>
        <h3><i class="fa fa-check" aria-hidden="true"></i>※該筆訂單的金額將重新計算。並退還差額。如因此有造成您的不便，敬請多加包涵。</h3>
    </div>
    <div class="history-table-container">
        <ul class="history-info-table history-table-adjust">
            <li class="history-table-title visible-md visible-lg">
                <ul>
                    <li class="col-md-5 col-sm-7 col-xs-7"><span>商品</span></li>
                    <li class="col-md-2 col-sm-1 col-xs-1"><span>單價</span></li>
                    <li class="col-md-1 col-sm-1 col-xs-1"><span>數量</span></li>
                    <li class="col-md-2 col-sm-1 col-xs-1"><span>小計</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span>管理</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                @foreach($carts as $cart)
                    <?php $product = $cart->product; ?>
                    <ul class="col-sm-block col-xs-block clearfix">
                        <li class="col-md-5 col-sm-12 col-xs-12 history-multi-content-estimate">
                            <div class="history-content-estimate-block">
                                <div class="history-content-img box-fix-with">
                                    <a class="zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}">
                                        <img src="{{ $product->getThumbnail() }}" alt="">
                                    </a>
                                </div>
                                <div class="history-content-estimate col-md-10">
                                    <a href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}">
                                        <h2>{{ $product->getManufacturerName() }} ： {{ $product->name }}</h2>
                                    </a>
                                    <span class="size-10rem word-break width-full">
                                        商品編號：{{ $product->model_number }}<br/>
                                    @if($options = $product->getSelectsAndOptions() and $options->count())
                                        規格：
                                            @foreach($options as $option)
                                                {{ $option->label }}：{{ $option->option }}
                                            @endforeach
                                        <br/>
                                    @endif
                                    <div class="toggle-box">
                                        <div class="toggle-button  text-right">
                                            <a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊...</a>
                                        </div>
                                        <div class="toggle-content hidden">
                                            @if($fit_models = $product->getFitModels() and $fit_models->count())
                                                對應車型：
                                                    @foreach($fit_models as $maker => $models)
                                                        {{$maker}}：
                                                        @foreach($models as $key => $model)
                                                            @if($key != 0)
                                                                 /
                                                            @endif
                                                            {{$model->model . ' ' . $model->style}}
                                                            @if($loop->iteration == 3)
                                                                ...
                                                                @break
                                                            @endif
                                                        @endforeach
                                                        @if($loop->iteration == 3)
                                                            ...
                                                            @break
                                                        @endif
                                                    @endforeach
                                                <br/>
                                            @endif
                                            @if($productDescription = $product->getproductDescription())
                                                要點：{!! str_limit($productDescription->summary,'200','...') !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-2 col-sm-12 col-xs-12">
                            <span class="size-10rem text-center">NT${{ number_format($product->getFinalPrice($current_customer)) }}</span>
                        </li>
                        <li class="col-md-1 col-sm-12 col-xs-12">
                            <span class="size-10rem">{{ $cart->quantity }}件</span>
                        </li>
                        <li class="col-md-2 col-sm-12 col-xs-12">
                            <span class="size-10rem text-center">NT${{ number_format($product->getFinalPrice($current_customer) * $cart->quantity) }}</span>
                        </li>
                        <li class="col-md-2 col-sm-12 col-xs-12">
                            <form class="box" method="POST" action="{{ route('customer-wishlist-move') }}">
                                <input name="qty" type="hidden" value="{{ $cart->quantity }}">
                                <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                                <input name="code" type="hidden" value="{{ $cart->protect_code }}">
                                <button type="submit" value="update" class="btn btn-danger btn-full">加入購物車</button>
                            </form>
                            <form method="POST" action="{{ route('customer-wishlist-remove') }}">
                                <input name="code" type="hidden" value="{{ $cart->protect_code }}">
                                <button type="submit" class="btn btn-default history-btn-default btn-full">刪除商品</button>
                            </form>
                        </li>
                    </ul>
                @endforeach
            </li>
        </ul>
    </div>
    <div class="history-transparent-box">
        <button class="btn btn-default base-btn-gray border-radius-2 btn-send-the-query" onclick="history.back();">回上一頁</button>
    </div>


@stop
@section('script')
    <script type="text/javascript">
        function toggleCartInfo(_this){
            var target = $(_this);
            target.closest('.toggle-box').find('.toggle-content').toggleClass('hidden');
        }
    </script>
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
@stop