@extends('response.layouts.2columns')
@section('left')
    <div class="mitumori-left-part">
        <div class="box-page-group">
            <div class="title-box-page-group">
                <h3>未登錄商品</h3>
            </div>
            <div class="ct-box-page-group">
                <ul class="ul-menu-ct-box-page-group">
                    <li>
                        <a href="{{URL::route('customer-history-mitumori')}}" title="未登錄商品查詢履歷{{$tail}}"><span>未登錄商品查詢履歷</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('right')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/mitumori.css') !!}">
    <div class="mitumori-right-part">
        <div class="title-main-box">
            <h1>未登錄商品查詢購買系統</h1>
        </div>
        <a class="banner-advertisement" href="javascript:void(0)"><img src="{{ assetRemote('image/banner/banner-mitumori.png') }}" alt="banner"></a>
        <div class="box-info-oem col-xs-12 col-sm-12 col-md-12">
            此查詢功能提供「Webike摩托百貨」網站上未登錄之商品，查詢報價及交期。<br>
            查詢注意事項：<br>
            1.我們大約需要一至三個工作天，請耐心等候回復。<br>
            2.報價均為新台幣含稅、含運報價，報價有效期限7天(到期後查詢會自動取消，請再次查詢)。<br>
            3.如您能提供該正確的商品編號、商品網址…等資訊，將加快我們查詢的時間。<br>
            4.報價回覆後請到：會員中心>>未登錄商品查詢履歷裡面查看商品資訊<br>
            5.未登錄之商品為「Webike摩托百貨」代為購買，一但下訂後無法取消訂單，因此下訂購買前請審慎考慮!!<br>
            <br>
            其他注意事項：<br>
            1.正廠零件請利用"<a href="{{route('genuineparts')}}" title="正廠零件查詢系統{{$tail}}" target="_blank">正廠零件查詢系統</a>"查詢（HRC、競技零件、選配配件、工具除外）。<br>
            2.當商品有年份、車身顏色…等區分時，我們會再請您提供車身編號、引擎號碼或是車輛的色碼編號，國產車型可查看您的"<a href="{{route('motor-licenses-info')}}" target="_blank">行照中顯示的機號</a>"。<br>
            3.關於未登錄商品查詢相關問題，請至<a href="{{route('customer-service-proposal-create', 'service')}}" title="未登錄商品問題提問{{$tail}}" target="_blank">未登錄商品問題提問</a>，或詢問<a href="{{DEALER_URL}}" title="Webike-實體經銷商{{$tail}}" target="_blank">Webike-實體經銷商</a>。<br>
        </div>
        <div class="box-items-group pages-action active" id="page-1">
            <form class="form-group mitumori-form simple-testing" method="POST" action="{!! URL::route('mitumori-estimate') !!}">
                <div class="form-block">
                    @for($i=1;$i<=3;$i++)
                        <div class="row mitumori-form-block-item st-row">
                            <div class="mitumori-number">
                                <h1>{!! $i !!}</h1>
                            </div>
                            <ul class="ul-mitumori-form-item-row">
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12 font-color-red">商品品牌</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="st-require-text width-full" type="text" name="syouhin_maker[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12 font-color-red">商品名稱</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="st-require-text width-full" type="text" name="syouhin_name[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12">商品編號（建議）</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="width-full" type="text" name="syouhin_code[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12">形式(顏色/尺寸)</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="width-full" type="text" name="syouhin_type[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12">車輛製造商</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="width-full" type="text" name="syouhin_taiou_maker[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12">車型/出廠年份</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="width-full" type="text" name="syouhin_taiou_syasyu[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-4 col-md-4 col-sm-4 col-xs-12 font-color-red">數量(個)</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><input class="st-require-number width-full" type="text" name="syouhin_kosuu[]"></div>
                                    </div>
                                </li>
                                <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <label class="col-lg-2 col-md-2 col-sm-4 col-xs-12">備註說明(請貼商品網址或其他資訊)</label>
                                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
                                            <textarea class="width-full" type="text" rows="3" name="syouhin_bikou[]"></textarea>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endfor
                </div>
                <div class="row submit-form">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-default border-radius-2 base-btn-gray btn-submit-form-mitumori" onclick="addRow();">追加輸入欄</button>
                        <button type="button" class="btn btn-danger border-radius-2 btn-submit-form-mitumori" onclick="goNextStep();">送出查詢</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="pages-action col-xs-12 col-sm-12 col-md-12" id="page-2">
            <ul class="table-main-info">
                <li class="table-main-title visible-md visible-lg">
                    <ul>
                        <li class="col-md-1 col-sm-12 col-xs-12"><span class="size-10rem">&nbsp;</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">商品品牌</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">商品名稱</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">商品編號</span></li>
                        <li class="col-md-1 col-sm-12 col-xs-12"><span class="size-10rem">形式</span></li>
                        <li class="col-md-2 col-sm-12 col-xs-12"><span class="size-10rem">車輛製造商</span></li>
                        <li class="col-md-1 col-sm-12 col-xs-12"><span class="size-10rem">年分</span></li>
                        <li class="col-md-1 col-sm-12 col-xs-12"><span class="size-10rem">數量</span></li>
                    </ul>
                </li>
                <li class="table-main-content" id="mitumori-preview-content">
                </li>
            </ul>
            <div class="center-box">
                <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish" onclick="goPrevPage();">返回修正</a>
                <a class="btn btn-danger border-radius-2 btn-send-genuinepart-finish" onclick="doSubmit();">確認送出</a>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script type="text/javascript" src="{!! assetRemote('js/pages/mitumori/mitumori.js') !!}"></script>
    <script type="text/javascript">
        function goNextStep(){
            var result = simpleTesting();
            if(result.success){
                var confirm_table = '';
                var row_count = 0;
                $(mitumori_preview_content).html('');
                $(mitumori_form + ' .mitumori-form-block-item').each(function(ul_key, ul){
                    var tds = '';
                    var width_num = 2;
                    var width_sm_num = 12;
                    var null_count = 0;
                    var has_count = false;
                    $(ul).find('input').each(function(input_key, input){
                        if(!has_count){
                            tds += '<li class="col-md-1 col-sm-12 col-xs-12"><h3>' + (row_count + 1) + '</h3></li>';
                            has_count = true;
                        }
                        if(!$(input).val()){
                            null_count++;
                        }
                        if($(input).attr('name').indexOf('syouhin_taiou_syasyu') >= 0){
                            width_num = 1;
                            width_sm_num = 12;
                        }else if($(input).attr('name').indexOf('syouhin_kosuu') >= 0){
                            width_num = 1;
                            width_sm_num = 12;
                        }else if($(input).attr('name').indexOf('syouhin_taiou_maker') >= 0){
                            width_num = 2;
                            width_sm_num = 12;
                        }else if($(input).attr('name').indexOf('syouhin_bikou') >= 0){
                            return true;
                        }
                        tds += '<li class="col-md-' + width_num + ' col-sm-' + width_sm_num + ' col-xs-' + width_sm_num + '"><span>' + $(input).val() + '</span></li>';
                    });
                    if(tds.length > 0 && null_count != $(ul).find('input').length ){
                        row_count++;
                        confirm_table += '<ul class="table-main-item col-sm-block col-xs-block clearfix">' + tds + '</ul>';
                    }
                });
                $(mitumori_preview_content).append(confirm_table);
                reSizeTableResponsive();
                goNextPage();
            }else {
                swal({
                    title: '錯誤',
                    html: "<h2>紅色項目為必填，且商品數量不可為0。<br>請再次確認</h2>",
                    type: 'error',
                }).then(function () {
                    slipTo(result.error_row);
                });
            }
        }

        function doSubmit(){
            var result = simpleTesting();
            if(result.success){
                swal({
                    title: '您確定要送出嗎?',
                    text: "點選「確認送出」後將送出查詢。",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '確認送出',
                    cancelButtonText: '取消',
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#777',
                }).then(function () {
                    $(mitumori_form).submit();
                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        return false;
                    }
                });
            }else{
                swal({
                    title: '錯誤',
                    html: "<h2>紅色項目為必填，且數量不可為0。<br>請再次確認</h2>",
                    type: 'error',
                }).then(function () {
                    goPrevPage();
                });
            }
        }
    </script>
@stop
