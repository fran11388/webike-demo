@extends('mobile.layouts.mobile-amp')
@section('style')
@stop
@section('middle')
<amp-selector id="mySelector" layout="container" >
  <div option select>Option One</div>
  <div option>Option Two</div>
  <div option>Option Three</div>
</amp-selector>
@stop
@section('script')
<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
@stop