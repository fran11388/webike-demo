@extends('response.layouts.2columns')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
    <style>
        .agree input:hover,label:hover{
            cursor: pointer;
        }
        .form {
            position:relative;
        }
        .join-content .form .cover{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #CCC;
            opacity: 0.5;
        }
    </style>
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>導覽</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join-form') }}#申請表格"><span>申請表格</span><span class="tips order"></span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join-form') }}#注意事項"><span>注意事項</span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join-form') }}#經銷商規約"><span>經銷商規約</span></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
    <div class="box-container-intruction" id="經銷商規約">
        <div class="title-main-box">
            <h2>申請前請詳細閱讀經銷商會員條約說明</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                    經銷會員條約說明<br>
                    第1條 本條約定義範圍<br>
                    1.本條約由榮芳興業有限公司(以下稱為甲方)針對「Webike-摩托百貨」之經銷會員(以下稱為乙方)所制定在「Webike-摩托百貨」網站使用上的條款及規範。<br>
                    2.甲方可更改或更新條約內容，恕不另外通知乙方，乙方有任何意見可隨時來信提出建議。<br>
                    ※Email：biz@webike.tw<br><br>

                    第2條 會員帳號管理責任<br>
                    1.一旦申請帳號完成之後，乙方需要自行管理會員帳號及密碼，如有帳號無法登入或是忘記密碼等情形，乙方可以在第一時間來信尋求甲方協助。<br>
                    2.乙方不得有轉讓或是轉賣帳號的情形，若經發現帳號非為乙方本人或是乙方員工使用，甲方有權停權乙方帳號。<br>
                    3.如乙方發現帳號密碼非本人使用，有被盜用的情形或疑慮，請隨時通知甲方協助處理。<br>
                    4.乙方不得將甲方網站上提供給乙方專用技術文件外流。<br><br>

                    第3條 購物相關規範<br>
                    @if(activeShippingFree())
                        1.乙方在購物前有責任先完全了解甲方的購物規範(免運費上限、付款方式等...)，如乙方對於商品下訂前有任何疑慮，請先向甲方諮詢。<br>
                    @else
                        1.乙方在購物前有責任先完全了解甲方的購物規範、付款方式等...，如乙方對於商品下訂前有任何疑慮，請先向甲方諮詢。<br>
                    @endif
                    2.如乙方購買商品有瑕疵、破損、缺件等情形...甲方將無條件幫乙方辦理退換貨之服務。<br>
                    3.乙方不得無條件提出退貨要求。<br>
                    4.乙方不得在購買後無理由拒絕取件(貨到付款)。<br><br>

                    第4條 退會及違反規定相關事項<br>
                    1.乙方如需退會，可以隨時與甲方聯絡辦理相關事宜。<br>
                    2.乙方退會時，本公司得要求該會員全數償還債務(如果有)。<br>
                    3.一經退會手續後，即不能使用本服務。退會的同時登錄資料與點數、折價券也會一併消失。<br>
                    4.乙方如違反上訴規定超過2次，甲方將有權強制取消乙方帳號權限，且終身不得再次申請，即不能享有與本服務相關的所有權利、特別優惠.....等服務。另外，隨著會員的退出，對本公司也不再具有請求權。
                    </span>
                </li>
                <li>
                    <div class="agree">
                        <input id="agree" name="agree" type="checkbox">
                        <label for="agree" class="font-color-red size-10rem font-bold">※請詳閱經銷會員條約，同意後再填寫下方申請表格。</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="注意事項">
        <div class="title-main-box">
            <h2>注意事項</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        1.申請文件：營利事業登記證及名片。<br>
                        2.營利事業登記證或名片影本以照片形式EMAIL回傳：biz@webike.tw， 我們收到資料後會盡速與您聯繫，謝謝。
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="申請表格">
        <div class="title-main-box">
            <h2>申請表格</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership join-content">
                <li class="form">
                    @if( \Auth::check() and \Auth::user()->role_id == 1 )
                        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSePgrrZ9daawmMj2TQO79EjJl-SDVN3akc4NUroJ7mcbVfwmw/viewform?entry.1359139938={{\Auth::user()->email}}" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                    @elseif( \Auth::check() and \Auth::user()->role_id > 1 )
                        <?php 
                            $address = \Auth::user()->address->filter(function($address){
                                return $address->is_default;
                            })->first();
                        ?>
                        @if( $address )
                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSePgrrZ9daawmMj2TQO79EjJl-SDVN3akc4NUroJ7mcbVfwmw/viewform?entry.1100434559&entry.82935622={{\Auth::user()->realname}}&entry.2006302817={{\Auth::user()->nickname}}&entry.2097300346={{$address->mobile}}&entry.1603447888={{$address->county.$address->district.$address->address}}&entry.1320722965&entry.460141575={{$address->phone}}&entry.343372104&entry.1238118797&entry.1359139938={{\Auth::user()->email}}" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                        @else
                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSePgrrZ9daawmMj2TQO79EjJl-SDVN3akc4NUroJ7mcbVfwmw/viewform?entry.1100434559&entry.82935622={{\Auth::user()->realname}}&entry.2006302817={{\Auth::user()->nickname}}&entry.2097300346=&entry.1687528031=&entry.1320722965&entry.997705937=&entry.343372104&entry.1238118797&entry.1359139938={{\Auth::user()->email}}" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                        @endif
                    @else
                        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSePgrrZ9daawmMj2TQO79EjJl-SDVN3akc4NUroJ7mcbVfwmw/viewform?embedded=true" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                    @endif
                    <div class="cover" onclick="goRules();"></div>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $('#agree').click(function(){
            if($(this).prop('checked')){
                $('.join-content .form .cover').hide();
                goForm();
            }else{
                $('.join-content .form .cover').show();
            }
        });
        function goRules(){
            var y = $('#經銷商規約').offset().top;
            $('html,body').animate({scrollTop: y}, 400);
        }

        function goForm(){
            var y = $('#申請表格').offset().top;
            $('html,body').animate({scrollTop: y}, 400);
        }
    </script>
@stop
