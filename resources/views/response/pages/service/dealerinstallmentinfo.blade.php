@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{assetRemote('css/pages/intructions.css')}}">
    <style>
        .btn-info{
            width:200px;
            height:40px;
        }
        .dealergrade span{
            font-size:1rem;
        }
        .dealergrade .bannerimg {
            border:0px !important;
            padding:0px;
        }
        .bannercontain {
            margin-bottom: 0px;
        }
        .dealergradelink:hover {
            cursor:pointer;
        }
    </style>
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>導覽</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ \URL::route('customer') }}" target="_blank"><span class="dealergradelink">經銷商會員中心</span><span class="tips order"></span></a>
                </li>
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ \URL::route('service-dealer-grade-info') }}" target="_blank"><span class="dealergradelink">經銷商分級制度</span></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
    <div class="dealergrade">
        <div class="box-container-intruction bannercontain">
            <div class="container-intruction-detail bannerimg">
                <ul class="ul-membership text-center">
                    <li>
                        <div class="text-center"><img src="{{assetRemote('image/service/installment/banner.png')}}"></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="關於經銷商分期付款服務">
            <div class="title-main-box">
                <h2>關於經銷商分期付款服務</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                    <span>
                        1.各位經銷商會員們，「Webike摩托百貨」消費的最後結帳金額超過3千元，即可享有信用卡分3期、6期與12期的分期付款服務。<br><br>
                        2.分期付款利率計算，3期=結帳金額+2.5%；6期=結帳金額+3.5%；12期=結帳金額+6.5%。<br><br>
                        3.a.支援3期、6期銀行：中信銀行、國泰世華、玉山銀行、台新銀行、花旗銀行、永豐銀行、聯邦銀行、遠東銀行、新光銀行、澳盛銀行、大眾銀行、台北富邦、第一銀行 等…共13家發卡銀行。<br><br>
                        &nbsp&nbsp&nbspb.支援12期銀行:中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、新光銀行、澳盛銀行、玉山銀行 等…共11家發卡銀行。<br><br>
                        4.使用信用卡分期付款不會影響點數或折價券的使用與獲得權益。<br><br>
                        5.如您使用信用卡分期付款，同時在一筆訂單裡有多件商品，但其中有一件商品有售完或交期較久的狀況，我們無法替您直接取消這項商品，而讓其它商品繼續訂購，因為聯合信用卡中心的信用卡分期付款無法退刷部分金額，只能請您先取消整張訂單並整筆退刷，再重新訂購您需要的商品。<br><br>
                        6.不建議使用＂簽帳金融卡＂進行刷卡，因為後續如遇到退款問題，退款的處理時間將會相當漫長。<br><br>
                        7.發票的部份最後會以您的結帳總金額（包含分期利率）來開立。
                    </span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-container-intruction" id="使用說明">
            <div class="title-main-box">
                <h2>使用說明</h2>
            </div>
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li>
                        <span>
                        分期付款的資訊將會出現在下列頁面，商品頁面、購物車以及結帳頁面。<br><br>
                        1.商品頁面（該件商品的分期金額試算表）<br>
                        此金額僅供您試算參考使用，並不代表是最後的結帳分期金額。<br><br>
                        </span>
                        <div class="text-center"><img src="{{assetRemote('image/service/installment/1.png')}}"></div><br><br>
                        <span>
                        2.購物車（訂單的分期金額試算表）<br>
                        您可以將多件商品一起加入購物車後再進行結帳，結帳前您可以將點數與折價券進行套用，套用之後的合計金額將會成為您使用分期付款的本金，除本金之外，我們會依照分期的期數在收取利率費用，3期=結帳金額+2.5%；6期=結帳金額+3.5%；12期=結帳金額+6.5%，下方的試算表已經是包含利率費用的金額。如您需要使用分期付款的服務，請在此頁面點選結帳，前往下個頁面操作。<br><br>
                        <div class="text-center"><img src="{{assetRemote('image/service/installment/2.png')}}"></div><br><br>
                        3.結帳頁面<br>
                        在您選擇付款方式為＂經銷商信用卡分期＂並選擇相應的期數之後，即會出現訂單的分期金額試算表。您確認金額正確後，即可依照正常的訂購流程送出訂單。<br><br>
                        <div class="text-center"><img src="{{assetRemote('image/service/installment/3.png')}}"></div><br><br>
                        </span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="box-container-intruction" id="經銷商相關問題諮詢">
            <div class="title-main-box">
                <h2>經銷商相關問題諮詢</h2>
            </div>
            <div class="box-container-intruction" id="立刻加入">
                <div class="container-intruction-detail">
                    <ul class="ul-membership">
                        {{--<li class="text-center">--}}
                            {{--<a class="btn btn-info" href="{{URL::route('service-dealer-join-form')}}" target="_blank">立即加入經銷商</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<div class="text-center"><a href="{{URL::route('service-dealer-join-form')}}" target="_blank"><img src="//img.webike.tw/assets/images/service/dealer/8.png"></a></div>--}}
                        {{--</li>--}}
                        <li>
                            @if($hsaNewOffice)
                                <div class="text-center"><img src="//img.webike.tw/assets/images/service/dealer/8_1.png"></div>
                            @else
                                <div class="text-center"><img src="//img.webike.tw/assets/images/service/dealer/8.png"></div>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script>
        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 100;
            $('html,body').animate({scrollTop: y}, 400);
        }
    </script>
@stop
