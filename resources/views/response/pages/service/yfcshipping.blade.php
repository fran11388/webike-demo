@extends('response.layouts.1column')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/1.jpg"></div>
	            </li>
	        </ul>
		</div>
	</div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>Webike橫濱物流中心(YFC)介紹</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                       Webike是以電子商務為核心的摩托車商品供應商。要如何提供客戶最好的購物環境?除了前台的網站與服務之外，後勤的物流系統也是非常重要的環節，以下就帶大家到「Webike橫濱物流中心」認識一下吧!<br>
                        「Webike橫濱物流中心」簡稱YFC，位於神奈川縣橫濱市瀨谷區，因應出貨量的成長，於2013年4月移轉至此；此中心採用最現代化的技術與管理工具，主要負責”進貨管理”、”統籌倉儲”以及”訂單出貨”…等工作。
                    </span><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>管理系統介紹</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        Webike擁有13年物流管理的經驗，自行開發"即時物流管理系統"，可以有效提升作業效率且降低人工作業的失誤。<br>
                        我們利用了小筆電搭配藍芽條碼機管理所有商品，商品資訊都會連線至”即時物流管理系統”，不論是上架、庫存、出貨的動態即時更新，並可以確保不會出錯。
                    </span><br><br>
                    <div class="title"><h2>利用電腦與條碼機，即時連線掌握所有貨況</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/2.jpg"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>倉儲介紹</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        目前YFC每天都有數萬項的商品上下架，包含了零組件與騎士用品。為達成有效率及安全的庫存管理，我們依據商品種類、尺寸分門別類進行庫存；以衣服為例除了吊掛的方式保管，還備有除濕機防潮，在輪胎、油品類也有專門櫃位保存，這都是為了確保讓客戶不會收到毀損或不良的商品。
                    </span><br>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="title"><h2>服飾吊掛架</h2></div>
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/4.jpg"></div><br>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="title"><h2>安全帽櫃位</h2></div>
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/5.jpg"></div><br>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="title"><h2>大型零件櫃位</h2></div>
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/6.jpg"></div><br>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="title"><h2>小型零件櫃位</h2></div>
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/7.jpg"></div><br>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="title"><h2>油品櫃位</h2></div>
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/8.jpg"></div><br>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="title"><h2>輪胎櫃位</h2></div>
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/9.jpg"></div><br>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>YFC充實的一天</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        您想知道Webike如何準備您的商品?您想知道YFC工作人員的工作流程?下面介紹帶您一窺究竟。
                    </span><br>
                    <div class="title"><h2>1.每日朝會：由主管說明今天任務及注意事項。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/10.jpg"></div><br>
                    <div class="title"><h2>2.商品入庫檢查：核對商品名稱、數量並檢查是否有瑕疵品。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/11.jpg"></div><br>
                    <div class="title"><h2>3.上架作業：商品分門別類進行上架。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/12.jpg"></div><br>
                    <div class="title"><h2>4.準備出貨：掃描訂單，讀取出貨商品資訊。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/13.jpg"></div><br>
                    <div class="title"><h2>5.揀選商品：找到正確商品後，再利用條碼機進行確認。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/15.jpg"></div><br>
                    <div class="title"><h2>6.集中商品：每筆訂單挑選完成後，必須集中避免與其他訂單混淆。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/16.jpg"></div><br>
                    <div class="title"><h2>7.訂單再次確認：由專門人員再次確認商品明細及數量。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/17.jpg"></div><br>
                    <div class="title"><h2>8.打包作業：專門的打包工作台進行商品打包。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/18.jpg"></div><br>
                    <div class="title"><h2>9.準備出貨：所有打包完成的訂單利用籠車集中，等待出貨。</h2></div>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/19.jpg"></div><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>YFC最專業、用心的服務團隊</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        「Webike」的目標是提供符合愛車人士期待的服務，並且以騎士的同理心不斷提升更好的品質。<br>
                        YFC全體工作夥伴會不斷的努力，盡全力滿足客戶的需求，當客戶快速、安心的收到商品是我們最大的欣慰與動力!!
                    </span><br>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/20.jpg"></div><br>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/yfcshipping/21.png"></div><br>
                </li><br>
                <li class="text-center">
                    <a class="btn btn-danger" href="{{URL::route('customer-account-create')}}">馬上加入會員</a>
                </li>
            </ul>
        </div>
    </div>
@stop
