@extends('response.layouts.2columns')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>導覽</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join') }}#好康介紹"><span>好康介紹</span><span class="tips order"></span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join') }}#申請資格"><span>申請資格</span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join') }}#擔當介紹"><span>擔當介紹</span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-dealer-join') }}#加入經銷"><span>加入經銷</span><span class="tips"></span></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/dealer/1.png"></div>
	            </li>
	        </ul>
		</div>
	</div>
    <div class="box-container-intruction" id="好康介紹">
        <div class="title-main-box">
            <h2>WeBike經銷會員8大優勢</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center col-md-4 col-sm-12 col-xs-12"><a target="_blank" href="{{URL::route('genuineparts')}}" title="6大正廠零件，快速報價，訂購便利。 - 「Webike-摩托百貨」經銷商大募集"><img src="//img.webike.tw/assets/images/service/dealer/2.png" alt="6大正廠零件，快速報價，訂購便利。 - 「Webike-摩托百貨」經銷商大募集"></a></div>
                    <div class="text-center col-md-4 col-sm-12 col-xs-12"><a target="_blank" href="{{URL::route('summary',['section'=>'ca/1000'])}}" title="6大正廠零件，快速報價，訂購便利。 - 「Webike-摩托百貨」經銷商大募集"><img src="//img.webike.tw/assets/images/service/dealer/3.png" alt="26萬項改裝部品。對應車型，每周更新。 - 「Webike-摩托百貨」經銷商大募集"></a></div>
                    <div class="text-center col-md-4 col-sm-12 col-xs-12"><a target="_blank" href="{{URL::route('summary',['section'=>'ca/3000'])}}" title="6大正廠零件，快速報價，訂購便利。 - 「Webike-摩托百貨」經銷商大募集"><img src="//img.webike.tw/assets/images/service/dealer/4.png" alt="4萬項騎士用品。最新流行、款式眾多 - 「Webike-摩托百貨」經銷商大募集"></a></div>
                </li>
                <li>
                    <div class="text-center col-md-12 col-sm-12 col-xs-12"><img src="//img.webike.tw/assets/images/service/dealer/5.png"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="申請資格">
        <div class="title-main-box">
            <h2>摩托車相關產業，都歡迎加入！</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                       凡是摩托車相關產業，都歡迎加入：<br>
                        車輛維修：進口重機維修、三陽、光陽、山葉、台鈴、宏佳騰...等機車維修車行<br>
                        機車商品販賣：人身部品專賣店、改裝用品店、機車材料行、潮流服飾店<br>
                        車輛販賣業：重機專賣店、車輛代理商、二手車販賣店<br>
                        機車服務業：駕訓班、車輛租賃、機車美容、烤漆服務<br>
                        商品供應商：輪胎、油品、工具、電池以及機車相關零組件供應商<br>
                        如您有任何問題，歡迎您直接與我們聯繫，將會有專員替您服務，感謝。
                    </span><br><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="擔當介紹">
        <div class="title-main-box">
            <h2>加入經銷商簡單三步驟</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/dealer/6.png"></div><br>
                    <span>
                        在本頁最下方按下"立即加入經銷商"按鈕後，填寫基本資料。<br>
                        表單填寫完成後，請傳真證明文件：營業登記及名片。<br>
                        審核通過後，我們會寄確認通知信，即完成申請。
                    </span><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="加入經銷">
        <div class="title-main-box">
            <h2>經銷業務店長-蘇店長</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center col-md-6 col-sm-12 col-xs-12"><img src="//img.webike.tw/assets/images/service/dealer/7.png"></div>
                    <span>
                        各位經銷商大家好，小弟擔任「Webike-摩托百貨」經銷商業務已經邁入第四個年頭，這段時間感謝業界先進的指導與鞭策，讓我們的服務更加地提升。<br><br>

                        我們是以”摩托車產業”為出發點全新型態的供應商，目的是將全球各地的摩托車商品及文化引進台灣，也將台灣優質的商品推廣到全世界。再次希望前輩們不吝給予服務的機會，大家互相切磋與交流，共同為台灣摩托車產業貢獻心力!
                    </span><br><br>
                </li>
                <li id="我有興趣想要刊登">
                    <div class="text-center"><a href="{{ route('service-dealer-join-form') }}" class="btn btn-info" target="_blank">立即加入經銷商</a></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="加入經銷">
        <div class="title-main-box">
            <h2>經銷商分級制度</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center">
                        <a href='{{ route('service-dealer-grade-info') }}' target="_blank"><img src="{{assetRemote('image/service/dealergradeinfo/banner-min.png')}}">
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
@stop
