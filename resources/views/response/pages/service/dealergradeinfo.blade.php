@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{assetRemote('css/pages/intructions.css')}}">
    <style>
        .btn-info{
            width:200px;
            height:40px;
        }
        .dealergrade span{
            font-size:1rem;
        }
        .dealergrade .bannerimg {
            border:0px !important;
            padding:0px;
        }
        .bannercontain {
            margin-bottom: 0px;
        }
        .dealergradelink:hover {
            cursor:pointer;
        }
    </style>
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>導覽</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a onclick="slipTo1('#什麼是升級制度')"><span class="dealergradelink">什麼是升級制度?</span><span class="tips order"></span></a>
                </li>
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a onclick="slipTo1('#升級辦法')"><span class="dealergradelink">升級辦法</span></a>
                </li>
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a onclick="slipTo1('#優惠回饋說明')"><span class="dealergradelink">優惠回饋說明</span><span class="tips"></span></a>
                </li>
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a onclick="slipTo1('#結算方式')"><span class="dealergradelink">結算方式</span><span class="tips order"></span></a>
                </li>
                <li href="" class="{{$current_customer ? '' : 'disabled'}}">
                    <a onclick="slipTo1('#立刻加入')"><span class="dealergradelink">立刻加入</span><span class="tips order"></span></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
<div class="dealergrade">
    <div class="box-container-intruction bannercontain">
        <div class="container-intruction-detail bannerimg">
            <ul class="ul-membership text-center">
                <li>
                    <div class="text-center"><img src="{{assetRemote('image/service/dealergradeinfo/banner.png')}}"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="什麼是升級制度">
        <div class="title-main-box">
            <h2>什麼是經銷商升級制度?</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        經銷商升級制度是依照經銷商消費購買的狀況進行分類，針對每月訂單狀況提供不同的優惠服務。<br><br>
                        目前經銷商升級制度共分為三種會員等級：黃金會員、白銀會員、標準會員，共有不同的回饋點數機制。<br><br>
                        會員等級會依據每月消費狀況進行調整，結算方式如下方說明。<br>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="升級辦法">
        <div class="title-main-box">
            <h2>升級辦法</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="{{assetRemote('image/service/dealergradeinfo/Snap2.png')}}"></div><br><br>
                     <span>
                        申請加入經銷商後，即享有一般經銷商權利外，每個月10號，我們將根據您在統計期間內完成簽收的實際訂單金額進行會員等級的調整，並給予不同分級的優惠回饋。
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="優惠回饋說明">
        <div class="title-main-box">
            <h2>優惠回饋說明</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <!--<div class="row container box-motogpevent-top">
                        <table class="table table-bordered table-motogp-detail">
                            <thead class="table-moto-boder-none">
                                <tr>
                                    <th class="text-center">會員等級</th>
                                    <th class="text-center">回饋說明</th>
                                    <th class="text-center">回饋點數</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">黃金會員</td>
                                    <td class="text-center">當月訂單總金額6萬台幣以上</td>
                                    <td class="text-center">2%</td>
                                </tr>
                                <tr class="active">
                                    <td class="text-center">白銀會員</td>
                                    <td class="text-center">當月訂單總金額3萬台幣以上到6萬台幣以下</td>
                                    <td class="text-center">1.5%</td>
                                </tr>
                                <tr>
                                    <td class="text-center">標準會員</td>
                                    <td class="text-center">當月訂單總金額低於3萬台幣</td>
                                    <td class="text-center">1%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>-->
                    <div class="text-center"><img src="https://img.webike.tw/imageUpload/15478804281547880428960.jpg"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="結算方式">
        <div class="title-main-box">
            <h2>結算方式</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="{{assetRemote('image/service/dealergradeinfo/Snap3.png')}}"></div><br><br>
                     <span>
                        詳細說明<br>
                        ▪月結算日：每個月的1號(結算的是上個月的金額)<br>
                        ▪統計期間：每個月的1號到當月的最後一天(28、29、30、或31號)<br>
                        ▪等級公佈與更新日：每月10號<br>
                        ▪等級有效期間：當月10號到次月的9號(統計以及等級持續期間的關係如圖紅線)<br><br>
                        ※在統計期間內，經銷會員所完成簽收的訂單金額(不包含退貨、退貨處理中訂單)。假如在本月下訂的訂單無法在本月完成出貨，而是在下個月完成簽收，訂單金額將會在下個月累計。
                    </span>
                </li>
            </ul>
        </div>
        <div class="box-container-intruction" id="立刻加入">
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li class="text-center">
                        <a class="btn btn-info" href="{{URL::route('service-dealer-join-form')}}" target="_blank">立即加入經銷商</a>
                    </li>
                    <li>
                        @if($hsaNewOffice)
                            <div class="text-center"><a href="{{ route('service-dealer-join-form') }}" target="_blank"><img src="//img.webike.tw/assets/images/service/dealer/8_1.png"></a></div>
                        @else
                            <div class="text-center"><a href="{{ route('service-dealer-join-form') }}" target="_blank"><img src="//img.webike.tw/assets/images/service/dealer/8.png"></a></div>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    <script>
        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 100;
            $('html,body').animate({scrollTop: y}, 400);
        }
    </script>
@stop
