@extends('response.layouts.2columns')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>導覽</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join-form') }}#申請表格"><span>申請表格</span><span class="tips order"></span></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
    <div class="box-container-intruction" id="申請表格">
        <div class="title-main-box">
            <h2>申請表格</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    @if( \Auth::check() and \Auth::user()->role_id == 1 )
                        <iframe src="https://docs.google.com/a/everglory.asia/forms/d/1q36FJxnFcu76oqWZFY0jqjCUalmwmb-D585xQjJ5k8E/viewform?entry.149596388=&entry.653091453={{\Auth::user()->realname}}&entry.489083080=&entry.1276523199={{\Auth::user()->realname}}&entry.904036965={{\Auth::user()->email}}&entry.1410798112&entry.1374974104&entry.155994411&entry.457218848=" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                    @elseif( \Auth::check() and \Auth::user()->role_id > 1 )
                        <?php 
                            $address = \Auth::user()->address->filter(function($address){
                                return $address->is_default;
                            })->first();
                        ?>
                        @if( $address )
                            <iframe src="https://docs.google.com/a/everglory.asia/forms/d/1q36FJxnFcu76oqWZFY0jqjCUalmwmb-D585xQjJ5k8E/viewform?entry.149596388=&entry.653091453={{\Auth::user()->realname}}&entry.489083080={{$address->phone}}&entry.1276523199={{\Auth::user()->realname}}&entry.904036965={{\Auth::user()->email}}&entry.1410798112&entry.1374974104&entry.155994411&entry.457218848=" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                        @else
                            <iframe src="https://docs.google.com/a/everglory.asia/forms/d/1q36FJxnFcu76oqWZFY0jqjCUalmwmb-D585xQjJ5k8E/viewform?entry.149596388=&entry.653091453={{\Auth::user()->realname}}&entry.489083080=&entry.1276523199={{\Auth::user()->realname}}&entry.904036965={{\Auth::user()->email}}&entry.1410798112&entry.1374974104&entry.155994411&entry.457218848=" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                        @endif
                    @else
                        <iframe src="https://docs.google.com/a/everglory.asia/forms/d/1q36FJxnFcu76oqWZFY0jqjCUalmwmb-D585xQjJ5k8E/viewform?embedded=true" width="100%" height="700" frameborder="2" marginheight="10" marginwidth="0">載入中…</iframe>
                    @endif
                </li>
            </ul>
        </div>
    </div>
@stop
