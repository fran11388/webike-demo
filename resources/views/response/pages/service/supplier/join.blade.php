@extends('response.layouts.2columns')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>導覽</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join') }}#品牌強力募集"><span>品牌強力募集</span><span class="tips order"></span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join') }}#行銷、銷售首選"><span>行銷、銷售首選</span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join') }}#刊登費用Free"><span>刊登費用Free</span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join') }}#簡單完成網路銷售"><span>簡單完成網路銷售</span><span class="tips"></span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join') }}#產品開發負責人"><span>產品開發負責人</span><span class="tips order"></span></a>
                </li>
                <li class="{{$current_customer ? '' : 'disabled'}}">
                    <a href="{{ URL::route('service-supplier-join') }}#我有興趣想要刊登"><span>我有興趣想要刊登</span><span class="tips order"></span></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/1.png"></div>
	            </li>
	        </ul>
		</div>
	</div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>我們正在尋找</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/2.png"></div>
                    <span>
                       您的公司是否有製造、代理或銷售摩托車相關的商品? 除了目前的銷售體系外，還想要增加銷售的通路?<br>
                        不論您是經銷代理商、產品製造商、品牌開發商、摩托車相關批發業、自有品牌設計師，我們都期待能夠與您配合。
                    </span><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="行銷、銷售首選">
        <div class="title-main-box">
            <h2>為什麼要來刊登?</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/3.png"></div>
                    <span>
                        我們擁有上萬名的會員，且皆為對摩托車相關資訊有相當興趣的會員，您的商品能夠更加精準的提供給適合您的客群瀏覽，我們也有多元化的行銷管道，Facebook粉絲版、廣告、網頁各Banner區塊的曝光搭配活動及特輯製作，能夠更加提升您的商品的曝光及推廣，24小時營業的摩托百貨，會員們更能夠在任何時間、任何地點上來瀏覽及購物。
                    </span><br><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="刊登費用Free">
        <div class="title-main-box">
            <h2>我們提供</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/4.png"></div>
                    <span>
                        我們幫您免去多數線上購物平台按件、按月的收費方式，不論商品數量，上架通通免費，還能擁有自己品牌頁面，更能夠協助您進行各種多樣化的行銷活動。
                    </span><br><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="簡單完成網路銷售">
        <div class="title-main-box">
            <h2>並且還能幫您</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/5.png"></div>
                    <span>
                        上架流程對您來說相當簡單，您僅須備妥商品資料提供給我們，後續上架的動作皆由我們負責，我們會有專人為您處理客戶服務問題，客戶也可以在我們平台使用我們的付款流程，您也只需要將商品統一寄送至我們的倉庫，配貨上會更輕鬆。<br>
                        如您定期有新品上市，也僅需要再補充資料給我們，我們立即也可以幫您將最新商品更新上去，輕鬆簡單的就能完成網路銷售!
                    </span><br><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction" id="產品開發負責人">
        <div class="title-main-box">
            <h2>產品開發負責人-楊專員</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center col-md-6 col-sm-12 col-xs-12"><img src="//img.webike.tw/assets/images/service/supplier/join/6.png"></div>
                    <span>
                        Webike 將電子商務的技術結合對摩托車的熱情， 在機車的核心中看見技術與文化的傳承，我們相信透過產品本質，能讓熱愛機車的朋友真切的感受到對於廠商在研發、品牌思維的用心，  這絕對不是冷冰冰的機械運轉，而是帶有溫度的精神延續。希望能與摩托車產業一同成長，一同看見世界，被世界看見。
                    </span><br><br>
                </li>
                <li id="我有興趣想要刊登">
                    <div class="text-center"><a href="{{ route('service-supplier-join-form') }}" class="btn btn-info">我有興趣想要刊登</a></div>
                </li>
                <li>
                    @if($hsaNewOffice)
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/7_1.png"></div>
                    @else
                        <div class="text-center"><img src="//img.webike.tw/assets/images/service/supplier/join/7.png"></div>
                    @endif
                </li>
            </ul>
        </div>
    </div>
@stop
