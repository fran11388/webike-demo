<div class="box col-xs-12 col-sm-12 col-md-12 populz">
    <h3 class="title-box text-center">熱門車型排行 <i class="fa fa-question-circle" aria-hidden="true" title="近一個月熱門車型"></i></h3>

    <div class="content-box col-xs-12 col-sm-12 col-md-12">
        <ul class="populz-zar">
            @foreach($popular_motors as $popular_motor)
                <li>
                    <a class="size-08rem" title="{{$popular_motor->name}}" class="a-link-blue-color  dotted-text1" href="{{route('summary',['section'=>'mt/'.$popular_motor->url_rewrite])}}">
                        <span>{{$loop->iteration}}</span>{{$popular_motor->name}}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>