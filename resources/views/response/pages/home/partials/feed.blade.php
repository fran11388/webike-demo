@if(!is_null($feed))
    <div class="post ct-below-post">
        <div class="entry-header thumbnail-below-post col-xs-4 col-sm-4 col-md-4 ">
            <div class="entry-thumbnail"><a class="zoom-image rounded-img" href="{!! $feed->url !!}?rel={{base64_encode('homepage_v1')}}" target="_blank">
                    <img class="img-responsive rounded-img" src="{!! $feed->image_url !!}" alt="">
                </a></div>
        </div>
        <!--
        <div class="entry-meta-2">
            <ul class="list-inline meta-ct">
                <li class="comments"><a href="#"><i class="fa fa-comment-o size-12rem"></i> <span class="comments-show">Comment</span> </a></li>
                <li class="share"><i class="fa fa-share-alt icon-share size-12rem"></i>
                    <ul class="share-show">
                        <li><a href="#"><span>Facebook</span><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><span>Twitter</span><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
        -->
        <div class="col-xs-8 col-sm-8 col-md-8 post-content content-below-post">
            @php
                $color_class = [
                    '摩托百貨' => 'font-color-red',
                    '摩托車市' => 'font-color-orange',
                    '摩托新聞' => 'font-color-green',
                ]
            @endphp
            <span class="post-tag dotted-text1 width-full hidden-xs {{isset($color_class[$feed->data_category]) ? $color_class[$feed->data_category] : 'text-primary'}}">{!! $feed->data_category . '：' . $feed->data_type !!}</span>
            <a class="title-content-below-post dotted-text2 width-full" href="{!! starts_with($feed->url,'http') ? $feed->url : env('APP_URLMODIFY_PATH','') . $feed->url  !!}?rel={{base64_encode('homepage_v1')}}" target="_blank">
                <h2 class="entry-title a-link-blue-color"> {!! str_limit($feed->title, 80, '...') !!} </h2>
            </a>
            <span class="post-tag dotted-text1 width-full hidden-lg hidden-md hidden-sm {{isset($color_class[$feed->data_category]) ? $color_class[$feed->data_category] : 'text-primary'}}">{!! $feed->data_category . '：' . $feed->data_type !!}</span>
            <span class="post-content">{!! str_limit($feed->description, 180, '...') !!}</span>
        </div>
    </div>
    <!--/post-->
@endif