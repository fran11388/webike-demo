<style>
    .box-content .category-container {
        padding:10px;
        margin-bottom:-20px;
    }
    @media (min-width : 768px) {
        .box-content .category-container .contain {
            width: 11.11%;
        }
    }
</style>
<div class="box-content">
    <div class="clearfix">
        <div class="title-main-box-clear">
            <h2>
                <span>摩托百貨商品分類　</span>
            </h2>
        </div>
    </div>
    <div class="category-container clearfix owl-carousel-responsive " data-owl-items="3">
        @foreach($display_categories as $title => $category)
            <div class="contain col-xs-12 col-sm-12  block text-center item-product-grid">
                <div class="category-image box">
                    <a href="{{ $category == 'genuineparts' ? \URl::route('genuineparts') : \URL::route('summary',['section' => 'ca/'.$category]) }}" title="{{ $title.$tail }}">
                        <img src="{{ assetRemote('image/category/index-new/'.$category.'.jpg') }}" alt="{{ $title.$tail }}">
                    </a>
                </div>
                <span class="size-10rem font-bold">{{ $title }}</span>
            </div>
        @endforeach
    </div>
</div>
<script>
    $(function() {
        $('.category-container .category-image img')
            .mouseover(function() {
                var image = $(this).attr('src');
                var image_hover = image.replace(".jpg", "hover.jpg");
                $(this).attr("src", image_hover);
            })
            .mouseout(function() {
                var image_hover = $(this).attr('src');
                var image = image_hover.replace("hover.jpg", ".jpg");
                $(this).attr("src", image);
            });
    });
</script>