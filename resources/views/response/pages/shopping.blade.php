@extends('response.layouts.1column')
@section('style')
    <style type="text/css">
        .blue-box{
            border:3px #005fb3 solid;
            padding:10px;
            text-align: center;
        }
    </style>
@stop
@section('middle')

    @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))
        @include('response.common.ad.banner-slideshow',['ad_id'=>11,'lazy_load'=>false])
    @else
        @include('response.common.ad.banner-slideshow',['ad_id'=>1,'lazy_load'=>false])
    @endif


    <div class="ct-left-shopping col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <!--
            <div class="row box-content blue-box">
                <a href="http://www.webike.tw/bikenews/2017/04/24/%E3%80%90%E7%B3%BB%E7%B5%B1%E5%85%AC%E5%91%8A%E3%80%91%E5%9B%A0hinet-apcn2%E6%B5%B7%E7%BA%9C%E9%9A%9C%E7%A4%99%EF%BC%8C%E9%83%A8%E5%88%86%E9%80%A3%E7%B7%9A%E5%93%81%E8%B3%AA%E6%9C%83%E5%8F%97%E5%88%B0/" title="【系統公告】因HiNet APCN2海纜障礙，部分連線品質會受到影響" target="_blank">
                    <h2>
                        【系統公告】因HiNet APCN2海纜障礙，部分連線品質會受到影響。
                    </h2>
                </a>
            </div>
        -->
        <!-- collection -->
        <div class="row box-content winter-collerction">
            <div class="title-winter-collerction col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <h2>
                    <span>2019 春夏 Collection</span>
                    <a href="{!! URL::route('collection-type-detail',['type'=>'category','url_rewrite'=>'2019spring']) !!}" title="{!! '2019 秋冬 Collection' . $tail !!}">>> 更多新品</a>
                </h2>
            </div>

            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 ct-winter-collerction">
                <ul class="product-list owl-carousel-ajax" data-owl-ajax="{{json_encode($riding_gear->params)}}" data-owl-ajax-load="false">
                    @foreach($riding_gear->collection as $product)
                        <li data-sku="{{$product->sku}}">
                            @include('response.common.product.a')
                        </li>
                    @endforeach
                </ul>

            </div>
        </div>


        <!-- outlet -->


        <div class="row box-content winter-collerction">
            <div class="title-winter-collerction col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <img class=" visible-md visible-lg" src="{!! assetRemote('image/shopping/img-outlet.jpg') !!}" width="120" height="73" alt=""/>
                <h2>
                    <span>OUTLET STORE OPEN!!</span>
                    <a href="{!! URL::route('outlet') !!}" title="{!! 'OUTLET' . $tail !!}">>> 每周更新</a>
                </h2>
            </div>

            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 ct-outlet-store-open">
                <ul class="product-list owl-carousel-ajax" data-owl-ajax="{{json_encode($outlet->params)}}" data-owl-ajax-load="false">
                    @foreach($outlet->collection as $product)
                        <li data-sku="{{$product->sku}}">
                            @include('response.common.product.b')
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="lazy-render custom-parts"></div>
        </div>

        <div class="row box-content">
            <div class="title-main-box-clear">
                <h2>
                    <span>會員好康　</span>
                </h2>
            </div>
            @include('response.common.ad.banner-small')
        </div>


        <div class="row box-content">
            <div class="title-main-box-clear">
                <h2>
                    <span>推薦品牌　</span>
                </h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                @if(isset($advertiser))
                    {!! $advertiser->call(28) !!}
                @endif
            </div>
        </div>

        @foreach($categories as $url_path => $category)
            @include('response.pages.shopping.partials.product.normal')
        @endforeach

{{--        @include('response.pages.shopping.partials.product.book')--}}

        <div class="row box-content">
            <div class="title-main-box-clear">
                <h2>
                    <span>流行特輯　</span>
                    <a href="{!! URL::route('collection') !!}" title="{!! '流行特輯' . $tail !!}">>> 查看全部</a>
                </h2>
            </div>
            <div class="owl-carouse-advertisement">
                @foreach($assortments as $assortment)
                    <div class="item">
                        <a class="ct-row-advertisement col-xs-12 col-sm-12 col-md-12 col-lg-12" href="{{$assortment->link}}">
                            <img class="" src="{{cdnTransform($assortment->banner)}}" alt="{{$assortment->meta_title . $tail}}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <!-- Customer view -->
        <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('response.common.loading.md')
        </div>
        <!-- Customer view end-->


        <div class="row row-advertisement">
            @include('response.common.dfp.ad-728x90')
        </div>
    </div>
    <div id="m-menu" class="hidden-xs">
        <div>
            <div class="ct-right-shopping col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <div class="row box-content customer-dashboard">
                    @if(Auth::check())
                        <div class="title">{{$current_customer->nickname}} 您好！</div>
                        @if( $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE )
                            <div class="gap"></div>
                            <div class="level-card {!! $current_customer->getCurrentLevelCssClass() !!}">
                                <label>{{ $current_customer->getCurrentLevelName() }} 本月回饋{{$current_customer->getCurrentLevelRate()}}倍</label>
                                @if(!is_null($current_customer->getNextLevelAmount()))
                                    <p>距離下個級距 NT${{$current_customer->getNextLevelAmount()}}</p>
                                @endif
                            </div>

                        @endif
                        <div class="content">
                            <div class="main-info">
                                <ul>
                                    <li>
                                        現金點數：<span class="count">{{$current_customer->getCurrentPoints()}}</span>點
                                        <a href="{{URL::route('customer-history-points')}}" title="點數獲得及使用履歷{{$tail}}">
                                            <i class="glyphicon glyphicon-search size-08rem"></i>
                                        </a>
                                    </li>
                                    <li>
                                        折價券：<span class="count">{{ count($current_customer->getCurrentCoupons()) }}</span>張
                                        <a href="{{URL::route('cart')}}" title="購物車{{$tail}}">
                                            <i class="glyphicon glyphicon-search size-08rem"></i>
                                        </a>
                                    </li>
                                    @if( $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE )
                                        <li>
                                            <a href="{{ route('service-dealer-grade-info') }}" title="經銷分級詳細說明{{$tail}}" target="_blank">詳細說明</a>
                                        </li>
                                        <li>
                                            <hr style="margin: 2px 0;">
                                        </li>
                                        <li>
                                            <a href="{{ route('cart') }}" title="我的購物車{{$tail}}">我的購物車<span class="icon-count cart"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer-wishlist') }}" title="待購清單{{$tail}}">待購清單<span class="icon-count wishlist"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer-history-order') }}" title="訂單動態及歷史履歷{{$tail}}">訂單動態及歷史履歷<span class="tips order"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer-history-genuineparts') }}" title="正廠零件查詢履歷{{$tail}}">正廠零件查詢履歷<span class="tips estimate-genuine"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer-history-estimate') }}" title="商品交期查詢履歷{{$tail}}">商品交期查詢履歷<span class="tips estimate-general"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer-history-mitumori') }}" title="未登錄商品查詢履歷{{$tail}}">未登錄商品查詢履歷<span class="tips mitumori"></span></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('customer-history-groupbuy') }}" title="團購商品查詢履歷{{$tail}}">團購商品查詢履歷<span class="tips groupbuy"></span></a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                            @if(isset($disps) and count($disps))
                                <div class="disp-list">
                                    <ul>
                                        @foreach($disps as $disp)
                                            <li><a href="{{$disp->link}}" title="{{$disp->name . $tail}}">{{$disp->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    @else
                        <div class="title white">
                            <img src="{!! assetRemote('image/shopping/img-title-01.jpg') !!}" alt="加入會員{{$tail}}"/>
                        </div>
                        <div class="content">
                            <span>進口重機、機車改裝用品情報，新車中古車資訊，全球摩托車新聞滿載!!會員專屬優惠及好康、獨家電子報、生日禮券等...眾多服務，歡迎體驗。</span>
                            <div class="text-center content-last-box">
                                ~Webike滿足您的摩托人生!
                            </div>
                            <div class="text-center content-last-box">
                                <a class="btn bg-color-red" href="{{URL::route('customer-account-create')}}" title="加入會員{{$tail}}">立即加入會員</a>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row box-content">
                    <div class="title-box-page-group">
                        <h3>購買正廠零件</h3>
                    </div>
                    <ul class="box-content-brand">
                        @foreach($suppliers as $supplier)
                            <?php
                            $divide = 'import';
                            if($supplier->motor_manufacturer and $supplier->motor_manufacturer->country_code == 'TW'){
                                $divide = 'domestic';
                            }
                            ?>
                            <li>
                                <a class="zoom-image" href="{{route('genuineparts-divide',[$divide,$supplier->api_usage_name])}}">
                                    {!! lazyImage( assetRemote('image/oempart/'.$supplier->api_usage_name.'.png') , $supplier->api_usage_name .'正廠零件' ) !!}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="row box-content">
                    <a href="{{route('mitumori')}}" target="_blank" title="未登錄商品報價及交期查詢！{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/banner-mitumori-small.png') }}" alt="未登錄商品報價及交期查詢！{{$tail}}">
                        </figure>
                    </a>
                </div>

                <div class="row box-content">
                    <a href="{{route('groupbuy')}}" target="_blank" title="團購系統-所有商品皆可開團！{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/banner-groupbuy-small.png') }}" alt="團購系統-所有商品皆可開團！{{$tail}}">
                        </figure>
                    </a>
                </div>
                @if($customer and ($customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE or $customer->role_id == \Everglory\Constants\CustomerRole::STAFF))
                    <div class="row box-content">
                        <a href="{{route('service-dealer-installment-info')}}" target="_blank" title="經銷商分期付款服務{{$tail}}">
                            <figure class="zoom-image">
                                <img src="{{ assetRemote('image/banner/installment.png') }}" alt="經銷商分期付款服務{{$tail}}">
                            </figure>
                        </a>
                    </div>
                @endif
                <div class="row box-content">
                    <a href="{{route('customer-service-proposal')}}" target="_blank" title="線上服務諮詢{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/service200X80.png') }}" alt="線上服務諮詢{{$tail}}">
                        </figure>
                    </a>
                </div>
                <div class="row">
                    <div class="box-page-group">
                        <div class="title-box-page-group">
                            <h3>車型索引</h3>
                        </div>
                        <div class="ct-box-page-group">
                            <ul class="ul-menu-ct-box-page-group">
                                @php
                                    $motor_manufacturers = \Ecommerce\Repository\MotorRepository::selectManufacturersByUrlRewrite(\Everglory\Constants\ViewDefine::MAIN_MOTOR_MANUFACTURERS);
                                @endphp
                                @foreach($motor_manufacturers as $motor_manufacturer)
                                    <li>
                                        <a href="{{route('motor-manufacturer', [$motor_manufacturer->url_rewrite, '00'])}}" title="{{$motor_manufacturer->name . $tail}}">
                                            <span>{{$motor_manufacturer->name}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row box-content">
                    <a href="{{route('customer-rule', ['member_rule_2'])}}#E" target="_blank" title="付款方式說明(信用卡/分期0利率/匯款/貨到付款){{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/payment200X80.png') }}" alt="付款方式說明(信用卡/分期0利率/匯款/貨到付款){{$tail}}">
                        </figure>
                    </a>
                </div>

                <div class="row box-content">
                    <a href="{{route('customer-rule', ['member_rule_2'])}}#G" target="_blank" title="DHL 挑戰業界最速 每日快遞進口{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/banner-18.png') }}" alt="DHL 挑戰業界最速 每日快遞進口{{$tail}}">
                        </figure>
                    </a>
                </div>

                <div class="row box-content">
                    <a href="{{route('service-yfcshipping')}}" target="_blank" title="YFC橫濱物流中心介紹{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/Logistic200X80.png') }}" alt="YFC橫濱物流中心介紹{{$tail}}">
                        </figure>
                    </a>
                </div>

                <div class="row box-content">
                <!-- <img class="" src="{!! assetRemote('image/shopping/banner-left-01.jpg') !!}" alt=""/> -->
                    <div class="box-notification">
                        <h2 class="font-bold box">Webike 8大特點</h2>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>100%正品保證</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>DHL進口全台最速</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>上千個國際品牌超過{{round(\Ecommerce\Repository\ProductRepository::getMainProductCount() / 10000)}}萬項商品</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>零件、用品、耗材一次購足</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>多元付款方式</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>依法開立發票</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>友善退換貨服務</span>
                        </div>
                        <div class="inline-rule clearfix">
                            <img class="tag" src="//img.webike.tw/shopping/image/V-tag.png">
                            <span>會員專屬回饋</span>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-asking btn-full" href="{{route('customer-rule','member_rule_2')}}" title="更多購物說明{{$tail}}">更多購物說明</a>
                        </div>
                    </div>
                </div>

                <div class="row box-content">
                    <a href="{{route('service-dealer-join')}}" target="_blank" title="經銷商募集{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/dealerAddBanner.png') }}" alt="經銷商募集{{$tail}}">
                        </figure>
                    </a>
                </div>
                <div class="row box-content">
                    <a href="{{route('service-supplier-join')}}" target="_blank" title="品牌大募集{{$tail}}">
                        <figure class="zoom-image">
                            <img src="{{ assetRemote('image/banner/manufacturerAdd.png') }}" alt="品牌大募集{{$tail}}">
                        </figure>
                    </a>
                </div>

                @if(isset($advertiser))
                    <div class="row box-content">
                        <div class="title-box-page-group">
                            <h3>本月主打</h3>
                        </div>
                        <div class="ct-box-page-group">
                            <?php
                                $display_num = 3;
                                if($current_customer){
                                    $display_num = 5;
                                }
                            ?>
                            @for($i = 1 ; $i <= $display_num ; $i++)
                                <div class="box">
                                    {!! $advertiser->call(25) !!}
                                </div>
                            @endfor
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@stop
@section('script')
    <script src="{{ assetRemote('js/pages/owl.carousel-ajax.js') }}"></script>
    <script src="{!! assetRemote('plugin/slick/slick.js') !!}"></script>
    <script type="text/javascript">

    </script>
@stop