@extends('response.layouts.2columns')
@section('left')
    <div class="mitumori-left-part">
        <div class="box-page-group">
            <div class="title-box-page-group">
                <h3>團購商品</h3>
            </div>
            <div class="ct-box-page-group">
                <ul class="ul-menu-ct-box-page-group">
                    <li>
                        <a href="{{URL::route('customer-history-groupbuy')}}"><span>團購商品查詢履歷</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('right')
    <div class="ct-oem-part-right box-fix-auto">
        <div class="title-main-box">
            <h1>團購商品查詢完成</h1>
        </div>
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">您的查詢編號為：{!! session('code') !!}</h1>
        </div>
        <div class="box-info-oem box-info-compalate">
            請注意：<br>
            1.我們同一時間也寄一封查詢明細至您的e-mail信箱，供您確認。<br>
            2.一般團購商品查詢約<B>1~3</B>個工作天，查詢結果會回復至您的e-mail信箱，或您也可以進入"
            <a href="{{URL::route('customer')}}" title="會員中心{!! $tail !!}">會員中心</a>"→"
            <a href="{{URL::route('customer-history-groupbuy')}}" title="團購商品查詢履歷{!! $tail !!}">團購商品查詢履歷</a>"中查看。<br>
            3.您如果超過3天內沒有收到報價回復，請來信service@webike.tw，我們會隨即幫您確認。<br>
            4.若有相關技術問題，請洽各地區 <a href="{{URL::route('dealer')}}" title="Webike台灣-實體經銷商{!! $tail !!}">「Webike台灣-經銷商」</a>，其他問題請「<a href="{{URL::route('customer-service-proposal')}}" title="聯絡我們{!! $tail !!}">聯絡我們</a>」<br>
        </div>
        <div class="center-box">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{!! $home_url !!}">回首頁</a>
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{!! $base_url !!}">回團購商品</a>
        </div>
    </div>
@stop
@section('script')
@stop