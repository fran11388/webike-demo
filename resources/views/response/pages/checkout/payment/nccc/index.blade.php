<?php
$price = $quote->subtotal;
$MerchantID = '1300853683';
$TerminalID = '70513877';
$POST_URL = 'https://nccnet-ec.nccc.com.tw/merchant/HPPRequest';
$MAC_KEY = '9fa191200b702aafa991f54adc30aa5e04b76deff5d49c43a9c105a7cf1305f1';

if( env('CHECKOUT_DEBUG', FALSE) ){
    $MerchantID = '6600800020';
    $TerminalID = '70502756';
    $POST_URL = 'https://nccnet-ectest.nccc.com.tw/merchant/HPPRequest';
    $MAC_KEY = 'ffc5eea6ff6c873a976730b36e225c5a916861a4658c8288f26d1abf3e360494';
}

$t = microtime(true);
$micro = sprintf("%06d", ($t - floor($t)) * 1000000);
$d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
//$OrderID = 'NCCC-' . $d->format("YmdHisu");
$OrderID = 'NCCC-' . $d->format("YmdHisu");
$TransAmt = $price;
//TransMode=交易種類 0一般 1分期 2紅利折抵
$TransMode = '1';
$Install = $quote->install;;
$NotifyURL=\Request::root() . '/checkout/payment/verify';


$cc = new \Everglory\Models\Sales\Creditcard();
$cc->ONO = $OrderID;
$cc->TA = $TransAmt;
$cc->remote_addr = $_SERVER['REMOTE_ADDR'];
if(\Auth::check()){
    $cc->customer_id =  $quote->customer_id;
}
$cc->quote_id = $quote->id;
$cc->save();


//Remember to add to this array for new parameter
$Signature_array = [
    'MerchantID' => $MerchantID,
    'TerminalID' => $TerminalID,
    'OrderID' => $OrderID,
    'TransAmt' => $TransAmt,
    'TransMode' => $TransMode,
    'Install' => $Install,
    'NotifyURL' => $NotifyURL,
    'CSS_URL' => '',
    'BankNo' => '',
    'TEMPLATE' => '',
    'TravelLocCode' => '',
    'TravelStartDate' => '',
    'TravelEndDate' => '',
];

$Signature = hash('sha256', urldecode(http_build_query($Signature_array)) . '&' . hash('sha256',$MAC_KEY));
?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/tr/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="Mon, 22 Jul 2002 11:12:01 GMT">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=no">
    </head>
    <body onload="document.HPP.submit();">
        <form name="HPP" method="post" target="HPPFrame"
              ACTION="<?php echo $POST_URL ?>">

            <input type="hidden" name="MerchantID" value='<?php echo $MerchantID ?>' />
            <input type="hidden" name="TerminalID" value='<?php echo $TerminalID ?>' />
            <input type="hidden" name="OrderID" value='<?php echo $OrderID ?>'/>
            <input type="hidden" name="TransAmt" value='<?php echo $TransAmt ?>' />
            <input type="hidden" name="TransMode" value='<?php echo $TransMode ?>' />
            <input type="hidden" name="Install" value='<?php echo $Install ?>' />
            <input type="hidden" name="NotifyURL" value='<?php echo $NotifyURL ?>' />
            <input type="hidden" name="Signature" value='<?php echo $Signature ?>' />
            <!-- 版型修改用
            <input type="hidden" name="CSS_URL" value='https://www.webike.tw/assets/css/mobile/nccc.css' />
            <input type="hidden" name="TEMPLATE" value='BOTH_SIMPLE' />
            -->
            <iframe name="HPPFrame" id="mainFrame" height="500" width="700" Frameborder="0"></iframe>

        </form>
    </body>
</html>