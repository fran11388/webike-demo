<?php

$price = $quote->subtotal;
$MID = '8080059471';
$CID = '';


$t = microtime(true);
$micro = sprintf("%06d", ($t - floor($t)) * 1000000);
$d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
//$ONO = $d->format("YmdHisu");
$ONO = $d->format("16mdHisu");
$TA = $price;
$U = \Request::root() . '/checkout/payment/verify';
$KEY = 'KM9QOEDZOJX9G9K85P4AT3XMGZ9K36RB';
$IC = '';
$action = 'https://acq.esunbank.com.tw/acq_online/online/sale42.htm';

if (env('CHECKOUT_DEBUG', FALSE)) {
//    $MID = '8089010942';
//    $KEY = 'YYNJ094PSCQWKJI2UMYTRADE5RYZRTGH';
//    $action = 'https://acqtest.esunbank.com.tw/acq_online/online/sale42.htm';
    $MID = '8089000016';
    $KEY = 'WEGSC0Q7BAJGTQYL8BV8KRQRZXH6VK0B';
    $action = 'https://acqtest.esunbank.com.tw/ACQTrans/esuncard/txnf0160';
}

if ($quote->payment_method == \Everglory\Constants\Payment::CREDIT_CARD) {
    $TID = 'EC000001';
    $M = $MID . "&" . $CID . "&" . $TID . "&" . $ONO . "&" . $TA . "&" . $U . "&" . $KEY;
} else {
    $TID = 'EC000002';
    if ($quote->install == 3)
        $IC = "0100103";
    if ($quote->install == 6)
        $IC = "0100106";
    if ($quote->install == 12)
        $IC = "0100112";
    $M = $MID . "&" . $CID . "&" . $TID . "&" . $ONO . "&" . $TA . "&" . $U . "&" . $IC . "&N&" . $KEY;
}



$M = md5($M);

$cc = new \Everglory\Models\Sales\Creditcard();
$cc->ONO = $ONO;
$cc->TA = $TA;
$cc->remote_addr = $_SERVER['REMOTE_ADDR'];
if (\Auth::check()) {
    $cc->customer_id = $quote->customer_id;
}
$cc->quote_id = $quote->id;
$cc->save();

?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/tr/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Mon, 22 Jul 2002 11:12:01 GMT">
</head>
<body onload="document.form.submit();">
<form name="form" method="post" action="{{ $action }}" style="">
    <table>
        <tr>
            <td>
                <input type="hidden" name="MID" value='<?php echo $MID ?>' placeholder="特店編號"/>
            <td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="CID" value='<?php echo $CID ?>' placeholder="子特店編號"/>
            <td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="TID" value='<?php echo $TID ?>' placeholder="終端機代號"/>
            <td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="ONO" value='<?php echo $ONO ?>' placeholder="訂單編號"/>
            <td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="TA" value='<?php echo $TA ?>' placeholder="消費金額"/>
            <td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="U" value='<?php echo $U ?>' placeholder="URL"/>
            <td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="M" value='<?php echo $M ?>' placeholder="押碼"/>
            <td>
        </tr>
        @if ($IC)
            <tr>
                <td>
                    <input type="hidden" name="IC" value='<?php echo $IC ?>'/>
                <td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="BPF" value='N'/>
                <td>
            </tr>
        @endif
        @if (env('CHECKOUT_DEBUG', FALSE))
            <tr>
                <td>
                    <input type="hidden" name="TYP" value='05'/>
                <td>
            </tr>
        @endif
    </table>

</form>
</body>
<script type="text/javascript">
    //document.form.submit();
</script>
</html>