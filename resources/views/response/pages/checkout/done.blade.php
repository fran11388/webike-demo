@extends('response.layouts.1column')
@section('middle')
    <div class="ct-oem-part-right box-fix-auto">
        <div class="title-main-box">
            <h2>感謝您的購買!!</h2>
        </div>
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">您的訂單編號為：{!! $order_increment_id !!}</h1>
        </div>
        <div class="box-info-oem box-info-compalate">
            請注意：<br>
            1.我們已發送一封”新訂單通知信”，裡面包含您的訂單詳細內容，您可以經由信件內的連結查詢您的訂單資訊。<br>
            2.若您10分鐘內沒有收到郵件，請檢查您的信箱，信件是否被分類到”垃圾郵件”，如果沒有請您來信order@webike.tw，我們隨即幫您補發。<br>
            3.同一時間我們已經幫您確認商品交期，作業須1~2個工作天，交期確認後我們會發送”交期與付款方式通知信”到您的信箱；若超過3個工作尚未收到，請您來信order@webike.tw，我們隨即幫您處理。<br>
            4.若有其問題與建議，請來信service@webike.tw <br>
        </div>
        <div class="center-box">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{!! $home_url !!}">回首頁</a>
        </div>
    </div>
@stop
@section('script')
    @php
        $trackData = session('trackData');
    @endphp
    <script type="text/javascript">
        // Assuming multiple items are purchased
        fbq('track', 'Purchase', {
            content_ids: [{!! implode(', ', $trackData->item_ids) !!}],
            content_type: 'product',
            value: {!! $trackData->revenue !!},
            currency: 'TWD'
        });
    </script>
    <!-- End Facebook Pixel Purchase Code -->

    <script type="text/javascript">
        var dimensionValue = '{!! $current_customer->role->name !!}';
        ga('require', 'ecommerce');
        ga('ecommerce:addTransaction', {'id': '{!! $trackData->id !!}',
            'affiliation': 'Webike',
            'revenue': '{!! $trackData->revenue !!}',
            'shipping': '{!! $trackData->shipping !!}',
            'tax': {!! $trackData->tax !!},
            'currency': '{!! $trackData->currency !!}',
            'dimension6': dimensionValue //customer-role
        });
        @foreach( $trackData->items as $item )
            ga('ecommerce:addItem', {'id': '{!! $trackData->id !!}',
                'name': '{!! $item->name !!}',
                'sku': '{!! $item->sku !!}',
                'price': '{!! $item->price !!}',
                'quantity': '{!! $item->quantity !!}',
                'category': '{!!  $item->category !!}',
                'currency': '{!! $item->currency !!}' });
            /*
            _paq.push(['addEcommerceItem',
                "{!! $item->sku !!}", // (required) SKU: Product unique identifier
                "{!! $item->name !!}", // (optional) Product name
                ["{!! implode('","',$item->categories) !!}"], // (optional) Product category. You can also specify an array of up to 5 categories eg. ["Books", "New releases", "Biography"]
                {!! round($item->price) !!}, // (recommended) Product price
                {!! $item->quantity !!} // (optional, default to 1) Product quantity
            ]);
            */
        @endforeach
        /*
        _paq.push(['trackEcommerceOrder',
            "{!! $trackData->increment_id !!}", // (required) Unique Order ID
            {!! round($trackData->revenue) !!}, // (required) Order Revenue grand total (includes tax, shipping, and subtracted discount)
            {!! round($trackData->subtotal) !!}, // (optional) Order sub total (excludes shipping)
            {!! $trackData->tax !!}, // (optional) Tax amount
            {!! round($trackData->shipping) !!}, // (optional) Shipping amount
            {!! round($trackData->discount) !!} // (optional) Discount offered (set to false for unspecified parameter)
        ]);
        */
        ga('ecommerce:send');
    </script>
    <!-- Google Code for &#20132;&#26131;&#23436;&#25104; Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = {!! $trackData->conversion_id !!};
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "{!! $trackData->conversion_label!!}";
        var google_conversion_value = {!! $trackData->revenue !!};
        var google_conversion_currency = "{!! $trackData->currency !!}";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript"
            src="//www.googleadservices.com/pagead/conversion.js">
        </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/{!! $trackData->conversion_id !!}/?value={!! $trackData->revenue !!}&currency_code={!! $trackData->currency !!}&label={!! $trackData->conversion_label!!}&guid=ON&script=0"/>
        </div>
    </noscript>
@stop