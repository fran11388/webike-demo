<?php
$celebration = new \Ecommerce\Core\Celebration();
?>
<ul class="table-cart-info container">
    <li class="table-cart-title visible-lg visible-md visible-sm">
        <ul>
            <li class="col-md-1 col-sm-1 col-xs-1"><h2>NO</h2></li>
            <li class="col-md-5 col-sm-5 col-xs-6"><h2>商品</h2></li>
            <li class="col-md-2 col-sm-2 col-xs-2"><h2>單價</h2></li>
            <li class="col-md-2 col-sm-2 col-xs-2"><h2>數量</h2></li>
            <li class="col-md-2 col-sm-2 col-xs-2"><h2>小計</h2></li>
        </ul>
    </li>
    <li class="table-cart-content">
        @foreach ($carts as $cart)
            @include('response.pages.checkout.partials.list-item')
        @endforeach
        @foreach ($gifts as $product)
            @include('response.pages.checkout.partials.list-gift')
        @endforeach
    </li>
    <li class="table-cart-resum">
        <ul>
            <li class="col-md-8 col-sm-12 col-xs-12 table-cart-resum-left">
                <h3>
                    折扣備註：<br>
                    @if($coupon = $service->getUsageCoupon())
                        已使用{{$coupon->name}}。<br>
                    @endif
                    @if($points = $service->getUsagePoints())
                        已使用{{$points}}點點數。<br>
                    @endif
                    @if ($celebration->getDiscount())
                        {{ $celebration->getTitleInCheckout() }}
                    @endif
                </h3>
            </li>
            <li class="col-md-4 col-sm-12 col-xs-12 table-cart-resum-right">
                <ul>
                    <li>
                        <div class="resum-title"><h3>運費:</h3></div>
                        <div class="resum-value"><h3>NT${{ number_format($service->getFee()) }}</h3>
                        </div>
                    </li>
                    <?php $method = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('cash-on-delivery'); ?>
                    @if ($method->isAvailable())
                        <li style="display:none;" class="show-in-cd">
                            <div class="resum-title">
                                <h3>貨到付款手續費：</h3>
                            </div>
                            <div class="resum-value">
                                <h3>
                                    NT${{ number_format($method->getFee()) }}
                                </h3>
                            </div>
                        </li>
                    @endif
                    <li>
                        <div class="resum-title"><h3>商品金額合計：</h3></div>
                        <div class="resum-value"><h3>
                                NT${{ number_format($service->getBaseTotal()) }}</h3></div>
                    </li>

                    @if ($celebration->getDiscount())
                        <li>
                            <div class="resum-title"><h3>滿額折抵：</h3></div>
                            <div class="resum-value"><h3>
                                    NT${{ number_format($celebration->getDiscount()) }}</h3></div>
                        </li>
                    @endif
                    <li>
                        <div class="resum-title"><h3>折抵金額合計：</h3></div>
                        <div class="resum-value"><h3>
                                NT${{ number_format($service->getDiscountTotal() + $celebration->getDiscount() ) }}</h3></div>
                    </li>
                    <li class="resum-final-value">
                        <div class="resum-title"><h3 class="layaway-origin">實際結帳金額：</h3><h3 class="layaway-override" style="display: none;">原始結帳總金額</h3></div>
                        <div class="resum-value">
                            <h3>
                                NT$<span class="hide-in-cd size-10rem">{{ number_format($service->getFee() + $service->getSubtotal() - $celebration->getDiscount() ) }}</span>
                                <span class="show-in-cd" style="display:none;">{{ number_format($service->getFee() + $service->getSubtotal() + $method->getFee() - $celebration->getDiscount()) }}</span>
                            </h3>
                        </div>
                    </li>
                    <li class="layaway-total-price-area hide">
                        <div class="resum-title"><h3 class="font-bold font-color-red"><span class="hidden-xs size-10rem">信用卡</span><span class="layaway-aliquot size-10rem"></span>期分期<span class="hidden-xs size-10rem">結帳</span>金額：</h3></div>
                        <div class="resum-value"><h3 class="font-bold font-color-red">NT$<span class="layaway-total-price size-10rem"></span></h3></div>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>