<div class="checkout-options container">
    <div class="title-main-box checkout-options-title" id="title_payment">
        <h2>付 款 方 式</h2>
    </div>
    <div class="checkout-options-content box-fix-column">
        <div class="options-content-title box-fix-with"><h2>請選擇付款方式</h2></div>
        <div class="options-content-main checkout-option-style1 box-fix-auto">
            <ul>
                <li class="option-style1-title"><h3><span>※</span> 請選擇您的付款方式：</h3></li>
                <?php $method = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('cash-on-delivery'); ?>
                @if ($method->isAvailable())
                    <li class="form-group">
                        <label class="radio-inline">
                            <input type="radio" name="payment-method" data-install="0" value="cash-on-delivery">
                            貨到付款
                        </label>
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_a' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(手續費及說明)</a>
                    </li>
                @endif
                <li class="form-group">
                    <label class="radio-inline">
                        <input type="radio" name="payment-method" data-install="0" value="bank-transfer" required>
                        匯款轉帳
                    </label>
                    <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_c' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(流程及說明)</a>
                </li>
                @php
                    $multiples = \Ecommerce\Shopping\Payment\CoreInstallment::decideMultiples($current_customer->role_id);
                    $esan_disabled = ['text' => '', 'flg' => false, 'layaway_flg' => false];
                    if(activeValidate('2018-09-06 04:00:00', '2018-09-06 06:00:00')){
                        $esan_disabled['flg'] = true;
                        $esan_disabled['layaway_flg'] = true;
                        $esan_disabled['text'] = '※系統商安全性維護中，請選擇其他付款方式。維護時間為2018-09-06 04:00~06:00。';
                    }else if($service->includeGroupbuyItem()){
                        $esan_disabled['layaway_flg'] = true;
                        $esan_disabled['text'] = '※選購"團購商品"無法使用信用卡分期付款。';
                    }else if($service->includeOutletItem()){
                        $esan_disabled['layaway_flg'] = true;
                        $esan_disabled['text'] = '※選購"OUTLET出清商品"無法使用信用卡分期付款。';
                    }
                    $nccc_disabled = ['text' => '', 'flg' => false, 'layaway_flg' => false];
                    if(activeValidate('2018-06-25 02:00:00', '2018-06-25 04:30:00')){
                        $nccc_disabled['flg'] = true;
                        $nccc_disabled['layaway_flg'] = true;
                        $nccc_disabled['text'] = '※系統商安全性維護中，請選擇其他付款方式。<br>維護時間為2018-06-25 02:00~04:30。';
                    }else if($service->includeGroupbuyItem()){
                        $nccc_disabled['layaway_flg'] = true;
                        $nccc_disabled['text'] = '※選購"團購商品"無法使用信用卡分期付款。';
                    }else if($service->includeOutletItem()){
                        $nccc_disabled['layaway_flg'] = true;
                        $nccc_disabled['text'] = '※選購"OUTLET出清商品"無法使用信用卡分期付款。';
                    }
                @endphp
                <li class="form-group">
                    <label class="radio-inline">
                        @if($esan_disabled['flg'])
                            <input type="radio" name="payment-method" data-install="0" value="credit-card" disabled>
                        @else
                            <input type="radio" name="payment-method" data-install="0" value="credit-card" required>
                        @endif
                        信用卡一次付清
                    </label>
                    <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(流程及說明)</a>
                </li>
                @php
                    $has_layaway_payment = \Ecommerce\Shopping\Payment\PaymentManager::hasLayawayPayment();
                    $layaway_multiple_text = '0利率';
                    $layaway_info_text = '分期服務說明';
                    $layaway_info_url = route( 'customer-rule', 'member_rule_2'.'#E_b_2' );
                    $layaway_front = '';
                    $layaway_tag = '';
                    if($current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE){
                        $layaway_multiple_text = '';
                        $layaway_info_text = '經銷商分期服務說明';
                        $layaway_info_url = route( 'service-dealer-installment-info' );
                        $layaway_front = '經銷商';
                        $layaway_tag = '<img src="' . assetRemote('image/new_right.gif') . '">';
                    }
                @endphp
                @if($esan_disabled['layaway_flg'])
                    <li class="form-group">
                        <div class="fake-point"></div>
                        <label class="radio-inline">
                            <input type="radio" name="payment-method" data-install="0" value="credit-card-layaway" disabled>
                            {!! $layaway_front !!}信用卡<span class="installment_title_text" style="color:#333333"></span>期分期付款
                        </label>
                        <a href="{!! $layaway_info_url !!}" style="display: inline-block;vertical-align: middle;" target="_blank">({!! $layaway_info_text !!})</a>
                        {!! $layaway_tag !!}
                    </li>
                    <li class="form-group">
                        <span class="font-color-red">{{$esan_disabled['text']}}</span>
                    </li>
                @elseif($has_layaway_payment and !$esan_disabled['layaway_flg'])
                    <li class="form-group">
                        <div class="fake-point"></div>
                        <label class="radio-inline">
                            <input type="radio" name="payment-method" data-install="0" value="credit-card-layaway" required>
                            <span class="hidden-xs" style="color:#333">{!! $layaway_front !!}</span>信用卡<span class="installment_title_text hidden-xs" style="color:#333"></span><span class="hidden-xs" style="color:#333">期</span>分期付款
                        </label>
                        <a href="{!! $layaway_info_url !!}" style="display: inline-block;vertical-align: middle;" target="_blank">({!! $layaway_info_text !!})</a>
                        {!! $layaway_tag !!}
                    </li>
                @endif
            </ul>
        </div>
    </div>
    @if($has_layaway_payment)
        <div class="checkout-options-content box-fix-column layaway-payment" style="display: none;">
            <div class="options-content-title box-fix-with"><h2>{!! $layaway_front !!}信用卡{!! $layaway_multiple_text !!}分期</h2></div>
            <div class="options-content-main checkout-option-style1 box-fix-auto clearfix">
                <hr>
                <ul>
                    <li><h3>聯合信用卡中心分期付款</h3></li>
                    <?php
                    $ncccMethod = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('credit-card-layaway-nccc');
                    ?>
                    @if($nccc_disabled['layaway_flg'])
                        <li class="form-group">
                            <span class="font-color-red">{!! $nccc_disabled['text'] !!}</span>
                        </li>
                        @if ($ncccMethod->isAvailable())
                            <li class="installment_text">
                                <label>
                                    <input type="radio" name="payment-method" value="credit-card-layaway-nccc" data-install="3" disabled>
                                    3期{!! (isset($multiples[3]) and $multiples[3] > 1) ? '' : '0利率' !!}信用卡分期
                                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                    @endif
                                </label>
                            </li>
                        @endif
                        <?php $ncccMethod->setInstall(6) ?>
                        @if ($ncccMethod->isAvailable())
                            <li class="installment_text">
                                <label>
                                    <input type="radio" name="payment-method" value="credit-card-layaway-nccc" data-install="6" disabled>
                                    6期{!! (isset($multiples[6]) and $multiples[6] > 1) ? '' : '0利率' !!}信用卡分期
                                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                    @endif
                                </label>
                            </li>
                        @endif
                        <?php $ncccMethod->setInstall(12) ?>
                        @if ($ncccMethod->isAvailable())
                            <li class="installment_text">
                                <label>
                                    <input type="radio" name="payment-method" value="credit-card-layaway-nccc" data-install="12" disabled>
                                    12期{!! (isset($multiples[12]) and $multiples[12] > 1) ? '' : '0利率' !!}信用卡分期
                                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                    @endif
                                </label>
                            </li>
                        @endif
                    @else
                        @if ($ncccMethod->isAvailable())
                            <li class="installment_text">
                                <label>
                                    <input type="radio" name="payment-method" value="credit-card-layaway-nccc" data-install="3" required>
                                    3期{!! (isset($multiples[3]) and $multiples[3] > 1) ? '' : '0利率' !!}信用卡分期
                                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                    @endif
                                </label>
                            </li>
                        @endif
                        <?php $ncccMethod->setInstall(6) ?>
                        @if ($ncccMethod->isAvailable())
                            <li class="installment_text">
                                <label>
                                    <input type="radio" name="payment-method" value="credit-card-layaway-nccc" data-install="6" required>
                                    6期{!! (isset($multiples[6]) and $multiples[6] > 1) ? '' : '0利率' !!}信用卡分期
                                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                    @endif
                                </label>
                            </li>
                        @endif
                        <?php $ncccMethod->setInstall(12) ?>
                        @if ($ncccMethod->isAvailable())
                            <li class="installment_text">
                                <label>
                                    <input type="radio" name="payment-method" value="credit-card-layaway-nccc" data-install="12" required>
                                    12期{!! (isset($multiples[12]) and $multiples[12] > 1) ? '' : '0利率' !!}信用卡分期
                                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                    @endif
                                </label>
                            </li>
                        @endif
                    @endif
                    <li class="layaway-aliquot-text-3"><label>◎支援：中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、遠東銀行、新光銀行、澳盛銀行、大眾銀行、台北富邦、第一銀行；共十二家發卡銀行。</label></li>
                    <li class="layaway-aliquot-text-6"><label>◎支援：中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、遠東銀行、新光銀行、澳盛銀行、大眾銀行、台北富邦、第一銀行；共十二家發卡銀行。</label></li>
                    <li class="layaway-aliquot-text-12"><label>◎支援：中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、新光銀行、澳盛銀行、台北富邦、第一銀行；共十家發卡銀行。</label></li>
                    <li></li>
                </ul>
                <ul>
                    <li><h3>玉山銀行分期付款</h3></li>
                    <?php $esunMethod = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('credit-card-layaway-esun'); ?>
                    @if ($esunMethod->isAvailable())
                        <li>
                            <label>
                                <input type="radio" name="payment-method" value="credit-card-layaway-esun" data-install="3"
                                       required>
                                3期{!! (isset($multiples[3]) and $multiples[3] > 1) ? '' : '0利率' !!}信用卡分期
                                @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                    <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                @endif
                            </label>
                        </li>
                    @endif
                    <?php $esunMethod->setInstall(6) ?>
                    @if ($esunMethod->isAvailable())
                        <li>
                            <label>
                                <input type="radio" name="payment-method" value="credit-card-layaway-esun" data-install="6"
                                       required>

                                6期{!! (isset($multiples[6]) and $multiples[6] > 1) ? '' : '0利率' !!}信用卡分期
                                @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                    <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                @endif
                            </label>
                        </li>
                    @endif
                    <?php $esunMethod->setInstall(12) ?>
                    @if ($esunMethod->isAvailable())
                        <li>
                            <label>
                                <input type="radio" name="payment-method" value="credit-card-layaway-esun" data-install="12"
                                       required>

                                12期{!! (isset($multiples[12]) and $multiples[12] > 1) ? '' : '0利率' !!}信用卡分期
                                @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                                    <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                                @endif
                            </label>
                        </li>
                    @endif
                    <li><label>◎僅支援"玉山銀行"信用卡<br></label></li>
                </ul>
                <ul class="layaway-payment-pre-area">
                    <li class="option-style1-title layaway-payment-pre" style="display: none;">
                        <hr>
                        <h3 class="layaway-total-price-area hide">信用卡<span class="layaway-aliquot"></span>期分期結帳金額：<span class="layaway-total-price"></span> 元。<span class="layaway-multiple"></span></h3>
                        <h3>分期費用：首期 <span class="first"></span> 元，其他期 <span class="other"></span> 元。</h3>
                    </li>
                </ul>
            </div>
        </div>
    @endif
</div>