<ul class="table-cart-item">
    <li class="col-md-1 col-sm-1 col-xs-1 visible-lg visible-md visible-sm">
        <h3>{{ $loop->iteration }}</h3>
    </li>
    <li class="col-md-5 col-sm-5 col-xs-12">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-5">
                <figure class="thumbnail zoom-image">
                    <img src="{{$product->getThumbnail()}}" alt="{{$product->full_name}}">
                </figure>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-7">
                <h2 class="checkout-product-name">
                    贈品：{{ formatProductFullName($product->name, $product->manufacturer->name) }}
                </h2>
                <div class="visible-xs content-last-box">
                    <div class="row">
                        <div class="col-xs-6 font-color-red">
                            贈品 NT$0
                        </div>
                        <div class="col-xs-6 text-right">
                            數量：1
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <li class="col-md-2 col-sm-2 hidden-xs">
        <h3>NT$0</h3>
    </li>
    <li class="col-md-2 col-sm-2 hidden-xs">
        <h3>1</h3>
    </li>
    <li class="col-md-2 col-sm-2 hidden-xs">
        <h3>NT$0</h3>
    </li>
</ul>