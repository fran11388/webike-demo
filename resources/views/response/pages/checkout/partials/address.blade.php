<div class="checkout-options container">
    <div class="title-main-box checkout-options-title">
        <h2>配送地址</h2>
    </div>
    <div class="checkout-options-content box-fix-column">
        <div class="options-content-title box-fix-with"><h2>配送地址</h2></div>
        <div class="options-content-main checkout-option-style2 box-fix-auto">
            <ul>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <div class="inline">
                        <label for="txt1"><span>※</span>收件人姓名：</label>
                        <input type="text" name="name" value="{{ $current_customer->address->firstname }}" required>
                    </div>
                </li>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <div class="inline">
                        <div id="twzipcode" class="tip_brfore form-inline" >
                            <label for="txt1"><span>※</span>配送地址：</label>
                            <div class="form-group">
                                <div data-name="county" data-role="county"></div>
                            </div>
                            <div class="form-group">
                                <div data-name="district" data-role="district"></div>
                            </div>
                            <div class="form-group">
                                <div data-name="zipcode" data-role="zipcode" data-value="{{ $current_customer->address->zipcode }}"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <div class="inline">
                        <label for="txt1"><span>　</span>　　　　　</label>
                        <input type="text" name="street" value="{{ $current_customer->address->address }}" required>
                    </div>
                </li>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <div class="inline">
                        <label for="txt1"><span>※</span>行動電話：</label>
                        <input type="text" name="mobile" value="{{ $current_customer->address->mobile }}" required>
                    </div>
                </li>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <div class="inline">
                        <label for="txt1"><span>※</span>室內電話：</label>
                        <input type="text" name="phone" value="{{ $current_customer->address->phone }}" placeholder="02-99887766" required>
                    </div>
                </li>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <div class="inline">
                        <label><span>※</span>配送時間：</label>
                        <label class="radio-inline">
                            <input name="shipping-time" value="morning" type="radio"> 上午
                        </label>
                        <label class="radio-inline">
                            <input name="shipping-time" value="noon" type="radio"> 下午
                        </label>
                        <?php /*
                        <label class="radio-inline">
                            <input name="shipping-time" value="night" type="radio"> 晚上
                        </label>
                        */ ?>
                        <label class="radio-inline">
                            <input name="shipping-time" value="anytime" type="radio" checked> 不指定
                        </label>
                    </div>
                </li>
                <li class="form-group checkout-option-txt checkout-option-txt2 box-fix-column">
                    <label>配送備註：</label>
                    <textarea class="checkout-option-large-width" name="shipping_comment" rows="4"></textarea>
                </li>
            </ul>
        </div>
    </div>
</div>