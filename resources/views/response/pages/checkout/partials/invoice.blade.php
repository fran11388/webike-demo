<style>
    .ecpay-container {
        border-left: 1px solid #dcdee3;
        background-color: #fff;
        padding-top: 5px;
        padding-bottom: 15px;
    }
    .ecpay-container{
        padding: 15px;
    }
    .ecpay-container .tab-pane {
        border: 1px solid #ddd;
        border-top: 0px;
        padding: 10px;
    }
    .ecpay-container div.active{
        background-color: #eee !important;
    }
    .ecpay-container li.active a {
        background-color: #eee !important;
        border-bottom:0px;
    }
    .ecpay-container .ecpay-agreement-container .ecpay-agreement{
        background-color:rgb(235, 235, 228);
        padding:5px;
        border: 1px solid #dcdee3;
    }
    .ecpay-container label{
        line-height: 25px;
    }
    .padding-none {
        padding:0px;
    }
    .ecpay-container .ecpay-content-container .personal-invoice-content .title{
        width:18%;
    }
    .ecpay-container .ecpay-content-container .personal-invoice-content .content{
        width:81%;
    }
    /*.ecpay-container .ecpay-content-container .recover-email {*/
        /*background-color: #005fb3 !important;*/
        /*height: 34px !important;*/
        /*margin: 0 10px;*/
    /*}*/
    .ecpay-container .ecpay-content-container .email {
        margin: 0 10px;
    }
    .padding-right-10 {
        padding-right:10px;
    }
    @media(max-width:600px){
        .ecpay-container .ecpay-content-container .title,.ecpay-container .ecpay-content-container .content,.checkout-options .checkout-options-content .ecpay-container ul li{
            width:100% !important;
        }
        .ecpay-container .ecpay-content-container .email {
            margin:10px 0px;
        }
        /*.ecpay-container .ecpay-content-container .recover-email {*/
            /*margin: 0;*/
        /*}*/
        /*.ecpay-container .ecpay-content-container .personal-invoice-content .recover-email {*/
            /*margin: 10px 0;*/
        /*}*/
        .ecpay-container .form-group {
            margin-bottom: 10px !important;
        }
        .padding-right-10 {
            padding-right:0px;
        }
        .ecpay-container li.active a {
            margin-right:0px !important;
        }
    }
    .ecpay-container .ecpay-content-container input {
        padding:0px 5px;
    }
    .ecpay-container .ecpay-content-container .invoice-email-text {
        color: #e61e25;
        font-weight: bold;
    }
    .ecpay-container .ecpay-content-container .donat_code,.ecpay-container .ecpay-content-container .donat_code:active,.ecpay-container .ecpay-content-container .donat_code:focus {
        outline: none !important;
        border: 0px !important;
        -moz-outline-style: none;
    }
    .ecpay-container .ecpay-content-container .vat-number-check-ok {
        display:inline-block;
        vertical-align: middle;
    }



</style>
<div id="invoice" class="checkout-options container">
    <div class="title-main-box checkout-options-title">
        <h2>發票資料</h2>
    </div>
    <div class="checkout-options-content box-fix-column">
        <div class="options-content-title box-fix-with"><h2>選擇發票類型</h2></div>
        @if(hasChangedInvoice())
            <div class="ecpay-container">
                <input type="hidden" name="final_email">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="button" href="#personal-invoice-content">
                            <label class="radio-inline">
                                <input type="radio" name="vat-require" class="personal-invoice-content" value="0" checked>
                                個人電子發票(兩聯式)
                            </label>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="button" href="#company-invoice-content">
                            <label class="radio-inline">
                                <input type="radio" name="vat-require" class="company-invoice-content" value="1">
                                公司報帳用電子發票(三聯式)
                            </label>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="button" href="#donat-invoice-content">
                            <label class="radio-inline">
                                <input type="radio" name="vat-require" class="donat-invoice-content" value="2">
                                捐贈發票
                            </label>
                        </a>
                    </li>
                </ul>

                <div class="tab-content box ecpay-content-container">
                    <div id="personal-invoice-content" class="tab-pane fade in active personal-invoice-content personal-invoice-block">
                        <div class="box form-inline">
                            <div class="title padding-none form-group">
                                <h3 class="size-10rem font-bold">綠界科技載具</h3>
                            </div>
                            <div class="content padding-none form-group">
                                <h3 class="size-10rem">(電子發票開立通知將透過EMAIL發送)</h3>
                            </div>
                        </div>
                        <div class="form-inline">
                            <div class="title  padding-none form-group">
                                <h3>請輸入電子信箱地址:</h3>
                            </div>
                            <div class="content  padding-none form-group">
                                <input class="col-xs-full form-control email" type="email" value="{{ $current_customer->email }}" name="email" id="email" required>
                                {{--<button class="col-xs-full form-control email btn btn-warning recover-email" type="button" value="{{ $current_customer->email }}">預設信箱</button>--}}
                                <span class="col-xs-full padding-none invoice-email-text">※預設為會員帳號所註冊之信箱，若有需要可以直接修改</span>
                            </div>
                        </div>
                    </div>
                    <div id="company-invoice-content" class="tab-pane fade company-invoice-content company-invoice-block">
                        <div class="box form-inline width-full">
                            <div class="box form-group">
                                <div class="title form-group padding-right-10">
                                    <h3>請輸入公司名稱：</h3>
                                </div>
                                <div class="content form-group padding-right-10">
                                    <input class="form-control" type="text" id="company" name="company" placeholder="" value="{{$current_customer->address->company}}">
                                </div>
                            </div>
                            <div class="box form-group">
                                <div class="title form-group padding-right-10">
                                    <h3>請輸入統一編號：</h3>
                                </div>
                                <div class="content form-group padding-right-10">
                                    <input class="form-control disabled" type="text" id="vat_number" name="vat_number" placeholder="" value="{{$current_customer->address->vat_number}}">
                                    <div id='vat_number_massage' class='check form-group'>
                                        <span class="font-color-red hidden vat_number_count_error ">※統一編號字數錯誤</span>
                                        <span class="font-color-red hidden vat_number_error">※統一編號錯誤</span>
                                        <span class="helper"></span>
                                        <i class='fa fa-check-circle hidden vat-number-check-ok'></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-inline">
                                <div class="padding-right-10 form-group">
                                    <h3>請輸入電子信箱地址:</h3>
                                </div>
                                <div class="content  padding-none form-group">
                                    <input class="col-xs-full form-control email" type="email" value="{{ $current_customer->email }}" name="email" id="email">
                                </div>
                                {{--<div class="content  padding-none form-group">--}}
                                    {{--<button class="col-xs-full form-control email btn btn-warning recover-email" type="button" value="{{ $current_customer->email }}">預設信箱</button>--}}
                                {{--</div>--}}
                                <div class="content  padding-none form-group">
                                    <span class="col-xs-full padding-none invoice-email-text">※預設為會員帳號所註冊之信箱，若有需要可以直接修改</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="donat-invoice-content" class="tab-pane fade donat-invoice-content donat-invoice-block">
                        <div class="box width-full">
                            <div class="form-inline">
                                <div class="padding-right-10 form-group col-xs-full clearfix">
                                    <label class="radio-inline">
                                        <input type="radio" name="donat_code" class=" donat_code" value="25885">
                                        伊甸基金會
                                    </label>
                                </div>
                                <div class="padding-right-10 form-group col-xs-full clearfix">
                                    <label class="radio-inline">
                                        <input type="radio" name="donat_code" class="donat_code" value="531">
                                        董氏基金會
                                    </label>
                                </div>
                                <div class="padding-right-10 form-group col-xs-full clearfix">
                                    <label class="radio-inline">
                                        <input type="radio" name="donat_code" class="donat_code" value="919">
                                        創世基金會
                                    </label>
                                </div>
                                <div class="padding-right-10 form-group col-xs-full clearfix">
                                    <label class="radio-inline">
                                        <input type="radio" name="donat_code" class="donat_code" value="621">
                                        漸凍人協會
                                    </label>
                                </div>
                                <div class="padding-right-10 form-group col-xs-full clearfix">
                                    <label class="radio-inline">
                                        <input type="radio" name="donat_code" class="donat_code" value="8585">
                                        家扶基金會
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="width-full">
                            <div class="form-inline">
                                <div class="padding-right-10 form-group col-xs-full">
                                    <h3>請輸入電子信箱地址:</h3>
                                </div>
                                <div class="content  padding-none form-group col-xs-full">
                                    <input class="col-xs-full form-control email" type="email" value="{{ $current_customer->email }}" name="email" id="email">
                                </div>
                                {{--<div class="content  padding-none form-group col-xs-full">--}}
                                    {{--<button class="col-xs-full form-control email btn btn-warning recover-email" type="button" value="{{ $current_customer->email }}">預設信箱</button>--}}
                                {{--</div>--}}
                                <div class="content  padding-none form-group col-xs-full">
                                    <span class="col-xs-full padding-none invoice-email-text">※預設為會員帳號所註冊之信箱，若有需要可以直接修改</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ecpay-agreement-container">
                    <div class="ecpay-agreement box">
                        <div class="box clearfix">
                            <label class="checkbox-inline">
                                <input class="agreement-rule" type="checkbox" name="agreement-3" required="">
                                同意<a href="https://www.ecpay.com.tw/" target="_blank">綠界科技</a>在電子發票相關業務上使用會員個人資料。 (<a href="{{ \URL::route('customer-rule','member_rule_8') }}" target="_blank">會員條約-隱私權保護政策</a>)
                            </label>
                        </div>
                        <div class="clearfix">
                            <label class="checkbox-inline">
                                <input class="agreement-rule" type="checkbox" name="agreement-4" required="">
                                若有退換貨需求時，我同意由榮芳興業有限公司代為處理電子發票以及銷貨退回證明單，以加速退貨退款作業。
                            </label>
                        </div>
                    </div>
                    <div>
                        <p class="box">在訂單包裹出貨後，我們會透過<a href="https://www.ecpay.com.tw/" target="_blank">綠界科技</a>將您訂單發票的電子檔透過電子郵件或是手機簡訊，發送至您在會員帳號所登錄的電子信箱或是手機號碼。</p>
                        <p>依統一發票使用辦法規定：個人(二聯式)發票一經開立，無法更改或改開公司戶(三聯式)發票。 (<a href="https://www.einvoice.nat.gov.tw/index" target="_blank">財政部電子發票流程說明</a>)</p>
                    </div>
                </div>
            </div>
        @else
            <div class="options-content-main checkout-option-style2 box-fix-auto">
                <ul>
                    <li>
                        <label class="radio-inline">
                            <input type="radio" name="vat-require" value="0" checked>
                            不需要統一編號
                        </label>
                    </li>
                    <li>
                        <label class="radio-inline">
                            <input type="radio" name="vat-require" value="1" id="vat_true">
                            需要統一編號
                        </label>
                    </li>
                    <li class="form-group checkout-option-txt box-fix-column">
                        <label class="control-label box-fix-with"><span class="font-color-red">※本公司開立3三聯式統一發票</span></label>
                    </li>
                    <li class="form-group checkout-option-txt box-fix-column">
                        <div class="inline ">
                            <label for="txt1">統一編號：</label>
                            <input class="form-control disabled" type="text" id="vat_number" name="vat_number" placeholder="" value="{{$current_customer->address->vat_number}}" disabled>
                            <div id='vat_number_massage' class='check'>
                                <span class="font-color-red hidden vat_number_count_error ">※統一編號字數錯誤</span>
                                <span class="font-color-red hidden vat_number_error">※統一編號錯誤，請輸入正確的統一編號</span>
                                <i class='fa fa-check-circle hidden'></i>
                            </div>
                        </div>
                    </li>
                    <li class="form-group checkout-option-txt box-fix-column">
                        <div class="inline">
                            <label for="txt2">公司抬頭：</label>
                            <input class="form-control disabled" type="text" id="company" name="company" placeholder="" value="{{$current_customer->address->company}}" disabled>
                        </div>
                    </li>
                </ul>
            </div>
        @endif
    </div>
</div>
<script>
    @if(hasChangedInvoice())
//        $('.ecpay-container .ecpay-content-container .recover-email').click(function(){
//            $('.ecpay-container .ecpay-content-container .email').val($(this).val());
//        });

        $('.ecpay-container ul li').click(function(){
            $('.ecpay-container .ecpay-content-container input').prop('required',false);
            $('.ecpay-container ul li input').prop('checked',false);

            var input = $(this).find('input');
            input.prop('checked',true);

            var tab_class = input.attr('class');

            $('.ecpay-container .ecpay-content-container .' + tab_class).find('.email').prop('required',true);

        });

        $("#vat_number").change(function(){
            var vat_number = $(this).val();
            checkVat_number(vat_number);

        });

        function checkVat_number(vat_number ){
            $("#vat_number_massage span").addClass('hidden');
            $("#vat_number_massage i").addClass('hidden');
            var check_number = ['1','2','1','2','1','2','4','1'];
            if(vat_number.length != 8 ){
                $("#vat_number_massage span.vat_number_count_error").removeClass('hidden');
                return false;
            }
            var number = vat_number.split("");
            var sum_multiple_number = 0;

            for(var i=0;i<=7;i++){
                var multiple_number = number[i] * check_number[i];
                if(multiple_number > 9){
                    var string_multiple_number = multiple_number.toString() ;
                    var multiple_number_ten = parseInt(string_multiple_number.substr(0,1));
                    var multiple_number_one = parseInt(string_multiple_number.substr(1,1));
                    multiple_number = multiple_number_ten + multiple_number_one;
                }
                sum_multiple_number = multiple_number + sum_multiple_number;
            }

            if(sum_multiple_number % 10 == 0 || (number[6] == 7 && ((sum_multiple_number - (10 - 1)) % 10 == 0 || (sum_multiple_number - 10) % 10 == 0))){
                $("#vat_number_massage i").removeClass('hidden');
                return true;
            }else{
                $("#vat_number_massage span.vat_number_error").removeClass('hidden');
                return false;
            }
        }

    @else
        $("#vat_number").change(function(){
            var vat_number = $(this).val();
            checkVat_number(vat_number);

        });

        function checkVat_number(vat_number ){
            $("#vat_number_massage span").addClass('hidden');
            $("#vat_number_massage i").addClass('hidden');
            var check_number = ['1','2','1','2','1','2','4','1'];
            if(vat_number.length != 8 ){
                $("#vat_number_massage span.vat_number_count_error").removeClass('hidden');
                return false;
            }
            var number = vat_number.split("");
            var sum_multiple_number = 0;

            for(var i=0;i<=7;i++){
                var multiple_number = number[i] * check_number[i];
                if(multiple_number > 9){
                    var string_multiple_number = multiple_number.toString() ;
                    var multiple_number_ten = parseInt(string_multiple_number.substr(0,1));
                    var multiple_number_one = parseInt(string_multiple_number.substr(1,1));
                    multiple_number = multiple_number_ten + multiple_number_one;
                }
                sum_multiple_number = multiple_number + sum_multiple_number;
            }

            if(sum_multiple_number % 10 == 0 || (number[6] == 7 && ((sum_multiple_number - (10 - 1)) % 10 == 0 || (sum_multiple_number - 10) % 10 == 0))){
                $("#vat_number_massage i").removeClass('hidden');
                return true;
            }else{
                $("#vat_number_massage span.vat_number_error").removeClass('hidden');
                return false;
            }
        }
    @endif
</script>