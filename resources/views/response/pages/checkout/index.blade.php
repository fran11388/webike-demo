@extends('response.layouts.1column-simplify')
@section('style')
<link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/checkout.css') !!}">
<style type="text/css">
    .checkout-options ul li{
        position: relative;
    }
    .checkout-options ul li .fake-point{
        position: absolute;
        top: 18px;
        left: 19.3px;
        background: #666;
        width: 7px;
        height: 7px;
        border-radius: 3.5px;
        z-index: 999;
        display: none;
    }
    div.checkout-options div.checkout-options-content.layaway-payment div.options-content-main ul{
        width: 50%;
        float: left;
    }
    div.checkout-options div.checkout-options-content.layaway-payment div.options-content-main ul li{
        float: none;
        width: 100%;
    }
    div.checkout-options div.checkout-options-content.layaway-payment div.options-content-main ul.layaway-payment-pre-area{
        width: 100%;
    }
    .layaway-payment .option-style1-title{
        border-right: none !important;
    }
    #invoice .inline div.check{
        margin-left: 5px;
        display: inline-block;
        vertical-align: middle;
        line-height: initial;
    }
    #invoice #vat_number_massage i {
        font-size:25px; 
        color:#42b72a;
    }
</style>
@stop
@section('page-title')
    <!-- <h2></h2> -->
@stop
@section('middle')
    <!-- Content -->
    @if($current_customer and $current_customer->role_id == \Everglory\Constants\CustomerRole::STAFF)
        <div class="hidden-xs staff-icon">
            Staff
        </div>
    @endif
    <form method="post" action="{{route('checkout-save')}}" id="checkout_form">
        {{ csrf_field() }}
        @include('response.common.message.error')
        <input type="hidden" name="protect-code" value="{{ $protect_code }}"/>
        <input type="hidden" name="install" value="0"/>
        <section id="contents top-shopping contents-all-page" class="ct-main checkout-ct-main row">
            <div class="container">

                @include('response.pages.checkout.partials.prices')

                @include('response.pages.checkout.partials.payment')

                @include('response.pages.checkout.partials.invoice')

                <div class="row block hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-12 form-group">
                        <label class="checkbox-inline font-bold">
                            <input id="agreement-rule-clicker" type="checkbox"> ※同意購買說明(<a href="#title_agreement">查看詳細</a>)
                        </label>
                    </div>
                </div>

                <div class="row block hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-12">
                        <button class="btn btn-danger btn-large btn-full submit checkout_submit">送出訂單</button>
                    </div>
                </div>

                @include('response.pages.checkout.partials.address')

                @include('response.pages.checkout.partials.agreements')

                <div class="checkout-options block hidden-lg hidden-md hidden-sm">
                    <div class="title-main-box checkout-options-title" id="title_agreement">
                        <h2>商品列表</h2>
                    </div>
                    @include('response.pages.checkout.partials.prices')
                </div>

                <h3 class="checkout-form-notice text-center box">◎請確認※欄位是否有填寫或勾選。</h3>

                <div class="row block">
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                        <a href="{{route('cart')}}" title="購物車{{$tail}}" class="btn btn-default btn-full">
                            <span class="hidden-xs">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                回購物車修改
                            </span>
                            <span class="hidden-lg hidden-md hidden-sm">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                <i class="glyphicon glyphicon-shopping-cart size-10rem"></i>
                            </span>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-4 hidden-xs"></div>
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-8">
                        <button class="btn btn-danger btn-full submit checkout_submit">送出訂單</button>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <!-- /.content -->

@stop

@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/checkout/checkout.js') !!}"></script>
    <script src="{!! assetRemote('plugin/jquery.twzipcode.min.js') !!}"></script>
    <script type="text/javascript">


        function disablePayment( value ){
            var outside = ["連江縣", "金門縣", "南海諸島", "澎湖縣"];
            var payway = $('input[value=cash-on-delivery]');
            if( $.inArray(value, outside) !== -1 ){
                payway.prop('disabled', 'disabled');
                payway.removeAttr('checked');
            }else{
                payway.removeAttr('disabled');
            }
        }

        $(document).ready(function(){
            var value = $('select[name=county]').find('option:selected').val();
            disablePayment( value );

            var installment_text = '';
            var text = '';
            $('.options-content-main ul .installment_text').each(function(index,value){
                if(index != 0){
                    text = '、';
                }
                installment_text = installment_text + text + $(this).children('label').text().trim().match(/\d+/);
            });
            $('.installment_title_text').text(installment_text);

        });

        $(document).on('change', $('select[name=county]'), function(){
            var value = $(this).find('option:selected').val();
            disablePayment( value );
        });

        $('#agreement-rule-clicker').click(function(){
            if($(this).prop('checked')){
                $('input.agreement-rule').prop('checked', true);
            }else{
                $('input.agreement-rule').removeAttr('checked');
            }
        });

        $("input[name=vat-require]").change(function(){
            var relations = ['#vat_number', '#company'];
            $("#vat_number_massage span").addClass('hidden');
            $("#vat_number_massage i").addClass('hidden');
            @if(!hasChangedInvoice())
                $.each(relations, function(key, relation) {
                    $(relation).addClass('disabled').prop('disabled', true);
                });
            @endif
            if($(this).val() == 1){
                $.each(relations, function(key, relation){
                    $(relation).removeClass('disabled').removeAttr('disabled');
                });
                var vat_number = $('#vat_number').val();
                if(vat_number){
                    checkVat_number(vat_number);
                }
            }
        });


        $("input[name=payment-method]").change(function(){
            if ($(this).val() == 'cash-on-delivery'){
                $(".show-in-cd").show();
                $(".hide-in-cd").hide();
            } else {
                $(".show-in-cd").hide();
                $(".hide-in-cd").show();
            }
            var parent = $('.layaway-payment');
            $('.layaway-total-price-area').addClass('hide');
            $('.layaway-override').hide();
            $('.layaway-origin').show();
            parent.find('.layaway-payment-pre').hide();
            if($(this).val().indexOf('layaway') >= 0){
                $('[class^=layaway-aliquot-text-]').hide();
                $('[class=layaway-aliquot-text-3]').show();
                var total = {{$service->getFee() + $service->getSubtotal()}};
//                var aliquot = $(this).closest('label').text().trim().substr(0, 1);
                var aliquot = $(this).closest('label').text().trim().match(/\d+/);
                var sub_total = "{{ $service->getSubtotal() }}";
                var multiples = JSON.parse('{!! json_encode(\Ecommerce\Shopping\Payment\CoreInstallment::decideMultiples($current_customer->role_id)) !!}');
                @php
                    $celebration = new \Ecommerce\Core\Celebration();
                    $celebration_discount = $celebration->getDiscount();
                @endphp
                var promo_discount = 0;

                total = Math.round({!! ($service->getSubtotal() - $celebration_discount) !!} * multiples[aliquot]) + {!! $service->getFee() !!};

                var pre_layaway = Math.floor(total / aliquot);
                parent.find('span.first').html((total - (pre_layaway * (aliquot - 1))).toLocaleString(
                        undefined, // use a string like 'en-US' to override browser locale
                        {}
                ));
                parent.find('span.other').html(pre_layaway.toLocaleString(
                        undefined, // use a string like 'en-US' to override browser locale
                        {}
                ));
                if($(this).val().indexOf('layaway-') >= 0){
                    parent.find('.layaway-payment-pre').show();
                    $('.layaway-aliquot').text(aliquot);
                    if($(this).val() == 'credit-card-layaway-nccc' || $(this).val() == 'credit-card-layaway-esun'){
                        $('[class^=layaway-aliquot-text-]').hide();
                        $('.layaway-aliquot-text-' + aliquot).show();
                    }
                    if((multiples[aliquot] - 1) > 0){
                        $('.layaway-override').show();
                        $('.layaway-origin').hide();
                        var origin_total = "{{number_format($service->getBaseTotal())}}";
                        var total_discount = "{{ number_format($service->getDiscountTotal() + $celebration->getDiscount() ) }}";
                        $('.layaway-multiple').text('(' + origin_total.toLocaleString(
                                undefined, // use a string like 'en-US' to override browser locale
                                {}

                            ) + ' - '  + total_discount + ')' + ' x ' + '(1 + ' + (((multiples[aliquot] - 1) * 100).toFixed(1)) + '%)' + {!! $service->getFee() ? '" + " + ' . $service->getFee() : "''" !!});

                        $('.layaway-total-price').text(total.toLocaleString(
                            undefined, // use a string like 'en-US' to override browser locale
                            {}
                        ));
                        $('.layaway-total-price-area').removeClass('hide');
                    }

                }
                $(this).closest('li').find('.fake-point').show();
                $(".layaway-payment").show();
            }else{
                parent.find('.layaway-payment-pre').hide();
                $(this).closest('div').find('.fake-point').hide();
                $(".layaway-payment").hide();
            }
        });

        $('#twzipcode').twzipcode({
            readonly:true,
            'css': ['form-control select2', 'form-control select2', 'form-control'],
        });

        $('#checkout_form').submit(function(){
            payment_method = $('input[name="payment-method"]:checked').val();

            if(!payment_method || payment_method =='credit-card-layaway'){
//                alert('請選擇付款方式!');
                swal(
                    '付款方式錯誤',
                    '請選擇一種付款方式!',
                    'error'
                ).then(function (result) {
                    slipTo('#title_payment');
                });

                return false;
            }
            @if(hasChangedInvoice())

                if($('input[name^="agreement"]:checked').length != 4){
    //                alert('請選擇付款方式!');
                    swal(
                        '購買同意書未確認',
                        '請確認所有購買同意書!',
                        'error'
                    ).then(function (result) {
                        slipTo('#title_agreement');
                    });

                    return false;
                }

                var tab_class = $('input[name=vat-require]:checked').attr('class');
                var email = $('.ecpay-container .ecpay-content-container .' + tab_class).find('.email').val();
                if(!email){
                    swal(
                        '電子信箱尚未輸入',
                        '請輸入寄送電子信箱',
                        'error'
                    ).then(function (result) {
                        if($(window).width() > 500){
                            slipTo('#title_payment');
                        }else{
                            slipTo('#invoice');
                        }
                    });
                    return false;
                }else{
                    $('input[name=final_email]').val(email);
                }


                if($('input[name=vat-require]:checked').val() == 2){
                    var donat_code = $('input[name=donat_code]:checked').val();
                    if(!donat_code){
                        swal(
                            '捐贈代碼尚未選擇',
                            '請選擇捐贈代碼',
                            'error'
                        ).then(function (result) {
                            if($(window).width() > 500){
                                slipTo('#title_payment');
                            }else{
                                slipTo('#invoice');
                            }
                        });
                        return false;
                    }
                }

                if($('input[name=vat-require]:checked').val() == 1){
                    var vat_number = $('#vat_number').val();
                    var vat_number_result =  checkVat_number(vat_number);
                    if(!vat_number){
                        swal(
                            '統一編號未輸入',
                            '請輸入統一編號',
                            'error'
                        ).then(function (result) {
                            if($(window).width() > 500){
                                slipTo('#title_payment');
                            }else{
                                slipTo('#invoice');
                            }
                            $("#vat_number_massage span.vat_number_count_error").removeClass('hidden');
                        });
                        return false;
                    }else if(!vat_number_result){
                        swal(
                            '輸入錯誤',
                            '請輸入正確的統一編號',
                            'error'
                        ).then(function (result) {
                            if($(window).width() > 500){
                                slipTo('#title_payment');
                            }else{
                                slipTo('#invoice');
                            }
                        });

                        return false;
                    }
                }

                var vat_require = $('input[name=vat-require]:checked').val();
                if(vat_require == 0){
                    $('.company-invoice-block input').val('');
                    $('.donat-invoice-block input').val('');
                }else if(vat_require == 1){
                    $('.personal-invoice-block input').val('');
                    $('.donat-invoice-block input').val('');
                }else if(vat_require == 2){
                    $('.personal-invoice-block input').val('');
                    $('.company-invoice-block input').val('');
                }
            @else
                if($('input[name^="agreement"]:checked').length != 2){
    //                alert('請選擇付款方式!');
                    swal(
                        '購買同意書未確認',
                        '請確認所有購買同意書!',
                        'error'
                    ).then(function (result) {
                        slipTo('#title_agreement');
                    });

                    return false;
                }
                if(!$('#vat_number').hasClass('disabled')){
                    var vat_number = $('#vat_number').val();
                    var vat_number_result =  checkVat_number(vat_number);
                    if(!vat_number){
                        swal(
                            '統一編號未輸入',
                            '請輸入統一編號',
                            'error'
                        ).then(function (result) {
                            if($(window).width() > 500){
                                slipTo('#title_payment');
                            }else{
                                slipTo('#invoice');
                            }
                            $("#vat_number_massage span.vat_number_count_error").removeClass('hidden');
                        });
                        return false;
                    }else if(!vat_number_result){
                        swal(
                            '輸入錯誤',
                            '請輸入正確的統一編號',
                            'error'
                        ).then(function (result) {
                            if($(window).width() > 500){
                                slipTo('#title_payment');
                            }else{
                                slipTo('#invoice');
                            }
                        });

                        return false;
                    }
                }
            @endif

            $('.checkout_submit').attr('disabled',true);

            $("input[name=install]").val( $("input[name=payment-method]:checked").attr('data-install'));
        });

    </script>
    <script src="{!! assetRemote('js/basic/select2-size-reset.js') !!}"></script>
@stop
