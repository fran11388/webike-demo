@extends('response.layouts.2columns')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
@stop
@section('left')
	@include('response.pages.genuineparts.partials.all-type')
@stop
@section('right')
  <div class="mitumori-right-part">
      <div class="title-main-box">
          <h1>專有名詞對照表</h1>
      </div>
  </div>
	<table class="table table-bordered" cellspacing="0" cellpadding="0">
      <tbody>
      	<tr>
          <td bgcolor="#7EBCFA" colspan="5" class="text-center">引擎部位</td>
        </tr>
        <tr>
          <td bgcolor="#7EBCFA">　</td>
          <td bgcolor="#7EBCFA">日文名稱</td>
          <td bgcolor="#7EBCFA">英文名稱</td>
          <td bgcolor="#7EBCFA">其他英文說法</td>
          <td bgcolor="#7EBCFA">中文名稱</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">1</td>          
          <td bgcolor="#FFFFFF">シリンダヘツド</td>
          <td bgcolor="#FFFFFF">CYLINDER&nbsp;HEAD</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">汽缸頭</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>2</td>
          <td>シリンダ</td>
          <td>CYLINDER&nbsp;</td>
          <td>　</td>
          <td>汽缸</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">3</td>
          <td bgcolor="#FFFFFF">バルブ</td>
          <td bgcolor="#FFFFFF">VALVE&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">汽門</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>4</td>
          <td>カムシヤフト．チエーン</td>
          <td>CAMSHAFT．CHAIN&nbsp;</td>
          <td>　</td>
          <td>凸輪軸．內鍊條</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">5</td>
          <td bgcolor="#FFFFFF">クランクシヤフト．ピストン</td>
          <td bgcolor="#FFFFFF">CRANKSHAFT．PISTON&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">區軸.活塞</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>6</td>
          <td>ウオ－タポンプ</td>
          <td>WATER PUMP&nbsp;</td>
          <td>　</td>
          <td>水幫浦</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">7</td>
          <td bgcolor="#FFFFFF">ラジエタ．ホ－ス</td>
          <td bgcolor="#FFFFFF">RADIATOR．HOSE&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">水箱．冷卻水管</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>8</td>
          <td>オイルポンプ</td>
          <td>OIL PUMP&nbsp;</td>
          <td>　</td>
          <td>機油幫浦</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">9</td>
          <td bgcolor="#FFFFFF">オイルクリ－ナ</td>
          <td bgcolor="#FFFFFF">OIL CLEANER</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">機油濾清器</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>10</td>
          <td>オイルパン</td>
          <td>OIL PAN</td>
          <td>　</td>
          <td>油底殼</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">11</td>
          <td bgcolor="#FFFFFF">インテ－ク</td>
          <td bgcolor="#FFFFFF">INTAKE</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">進氣系統</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>12</td>
          <td>キヤブレタ</td>
          <td>CARBURETOR&nbsp;</td>
          <td>　</td>
          <td>化油器</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">13</td>
          <td bgcolor="#FFFFFF">スロツトルボデー</td>
          <td bgcolor="#FFFFFF">THROTTLE BODY</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">節氣門(噴射)</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>14</td>
          <td>エアーインダクションシステム</td>
          <td>AIR INDUCTION    SYSTEM&nbsp;</td>
          <td>　</td>
          <td>空氣導流系統</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">15</td>
          <td bgcolor="#FFFFFF">イグニッションシステム</td>
          <td bgcolor="#FFFFFF">IGNITION SYSTEM</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">點火系統</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>16</td>
          <td>エキゾ－スト</td>
          <td>EXHAUST&nbsp;</td>
          <td>MUFFLER</td>
          <td>排氣系統</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">17</td>
          <td bgcolor="#FFFFFF">&nbsp;バランサ</td>
          <td bgcolor="#FFFFFF">BALANCER</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">平衡器</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>18</td>
          <td>クランクケ－ス</td>
          <td>CRANKCASE&nbsp;</td>
          <td>　</td>
          <td>區軸箱</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">19</td>
          <td bgcolor="#FFFFFF">クランクケ－スカバ－　</td>
          <td bgcolor="#FFFFFF">CRANKCASE COVER</td>
          <td bgcolor="#FFFFFF">ENGINE COVER</td>
          <td bgcolor="#FFFFFF">曲軸箱外蓋</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>20</td>
          <td>クラツチ</td>
          <td>CLUTCH&nbsp;</td>
          <td>　</td>
          <td>離合器</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">21</td>
          <td bgcolor="#FFFFFF">ゼネレ－タ</td>
          <td bgcolor="#FFFFFF">GENERATOR&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">發電系統</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>22</td>
          <td>スタ－テイングモ－タ</td>
          <td>STARTING MOTOR&nbsp;</td>
          <td>　</td>
          <td>啟動馬達</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">23</td>
          <td bgcolor="#FFFFFF">スタ－タ</td>
          <td bgcolor="#FFFFFF">STARTER&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">啟動機構(腳踩)</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>24</td>
          <td>トランスミツシヨン</td>
          <td>TRANSMISSION&nbsp;</td>
          <td>　</td>
          <td>變速箱機構</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">25</td>
          <td bgcolor="#FFFFFF">シフトカム．フオ－ク</td>
          <td bgcolor="#FFFFFF">SHIFT CAM． FORK&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">變速撥叉</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>26</td>
          <td>シフトシヤフト</td>
          <td>SHIFT SHAFT&nbsp;</td>
          <td>CHANGE PEDAL</td>
          <td>打檔桿機構</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">27</td>
          <td bgcolor="#FFFFFF">オイルタンク</td>
          <td bgcolor="#FFFFFF">OIL TANK</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">機油箱(二行程)</td>
        </tr>
      </tbody></table><br><br>
	  
      <table class="table table-bordered" cellspacing="0" cellpadding="0">
      	<tr>
          <td bgcolor="#7EBCFA" colspan="5" class="text-center">車身部位</td>
      	</tr>
        <tbody><tr>
          <td bgcolor="#7EBCFA">　</td>
          <td bgcolor="#7EBCFA">日文名稱</td>
          <td bgcolor="#7EBCFA">英文名稱</td>
          <td bgcolor="#7EBCFA">其他英文說法</td>
          <td bgcolor="#7EBCFA">中文名稱</td>
        </tr>
        <tr>
          <td bgcolor="#CCCCCC">1</td>
          <td bgcolor="#CCCCCC">フレ－ム</td>
          <td bgcolor="#CCCCCC">FRAME&nbsp;</td>
          <td bgcolor="#CCCCCC">　</td>
          <td bgcolor="#CCCCCC">車台骨架</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">2</td>
          <td bgcolor="#FFFFFF">エンジンマウント</td>
          <td bgcolor="#FFFFFF">ENGINE MOUNT</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">引擎吊架</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>3</td>
          <td>フレームフィッティング</td>
          <td>FRAME&nbsp; FITTINGS</td>
          <td>　</td>
          <td>車台固定組件</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">4</td>
          <td bgcolor="#FFFFFF">フエンダ</td>
          <td bgcolor="#FFFFFF">FENDER&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">擋泥板．土除</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>5</td>
          <td>サイドカバ－</td>
          <td>SIDE COVER</td>
          <td>　</td>
          <td>車身側蓋</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">6</td>
          <td bgcolor="#FFFFFF">リヤア－ム</td>
          <td bgcolor="#FFFFFF">REAL ARM</td>
          <td bgcolor="#FFFFFF">SWINGARM</td>
          <td bgcolor="#FFFFFF">後搖臂</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>7</td>
          <td>リヤサスペンシヨン</td>
          <td>REAR SUSPENSION&nbsp;</td>
          <td>SHOCK ABSORBER</td>
          <td>後避震系統</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">8</td>
          <td bgcolor="#FFFFFF">ハンドルパイプ．トツプブリツジ</td>
          <td bgcolor="#FFFFFF">HANDLE PIPE．TOP    BRIDGE&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">把手．上三角台</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>9</td>
          <td>ステアリング</td>
          <td>STEERING&nbsp;</td>
          <td>　</td>
          <td>轉向系統</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">10</td>
          <td bgcolor="#FFFFFF">フロントフオ－ク</td>
          <td bgcolor="#FFFFFF">FRONT FORK&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">前叉</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>11</td>
          <td>フユエルタンク</td>
          <td>FUEL TANK&nbsp;</td>
          <td>　</td>
          <td>油箱</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">12</td>
          <td bgcolor="#FFFFFF">シ－ト</td>
          <td bgcolor="#FFFFFF">SEAT&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">座墊</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>13</td>
          <td>タイヤ</td>
          <td>TIRES</td>
          <td>　</td>
          <td>輪胎</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">14</td>
          <td bgcolor="#FFFFFF">フロントホイ－ル</td>
          <td bgcolor="#FFFFFF">FRONT WHEEL&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">前輪框</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>15</td>
          <td>フロントブレ－キキヤリパ</td>
          <td>FRONT BRAKE    CALIPER&nbsp;</td>
          <td>　</td>
          <td>前煞車卡鉗(分泵)</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">16</td>
          <td bgcolor="#FFFFFF">リヤ    ホイ－ル</td>
          <td bgcolor="#FFFFFF">REAR WHEEL&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">後輪框</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>17</td>
          <td>リヤ    ブレ－キキヤリパ</td>
          <td>REAR BRAKE    CALIPER&nbsp;</td>
          <td>　</td>
          <td>後煞車卡鉗(分泵)</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">18</td>
          <td bgcolor="#FFFFFF">ステアリングハンドル．ケ－ブル</td>
          <td bgcolor="#FFFFFF">STEERING    HANDLE．CABLE&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">龍頭把手．離合器線.油門線…</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>19</td>
          <td>フロントマスタシリンダ</td>
          <td>FRONT MASTER    CYLINDER&nbsp;</td>
          <td>　</td>
          <td>前剎車主缸(總泵)</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">20</td>
          <td bgcolor="#FFFFFF">ぺダル</td>
          <td bgcolor="#FFFFFF">PEDAL</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">打檔桿．煞車踏桿</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>21</td>
          <td>フ－トレスト</td>
          <td>FOOTREST&nbsp;</td>
          <td>STEP</td>
          <td>腳踏板</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">22</td>
          <td bgcolor="#FFFFFF">スタンド</td>
          <td bgcolor="#FFFFFF">STAND</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">駐車架</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>23</td>
          <td>リヤマスタシリンダ</td>
          <td>REAR MASTER    CYLINDER&nbsp;</td>
          <td>　</td>
          <td>後煞車主缸(總泵)</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">25</td>
          <td bgcolor="#FFFFFF">フラツシヤライト</td>
          <td bgcolor="#FFFFFF">FLASHER LIGHT&nbsp;</td>
          <td bgcolor="#FFFFFF">WINKER</td>
          <td bgcolor="#FFFFFF">方向燈</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>26</td>
          <td bgcolor="#CCCCCC">メ－タ</td>
          <td>METER&nbsp;</td>
          <td>　</td>
          <td>儀表</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">27</td>
          <td bgcolor="#FFFFFF">ヘツドライト</td>
          <td bgcolor="#FFFFFF">HEATLIGHT</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">頭燈</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>28</td>
          <td>テ－ルライト</td>
          <td>TAILLIGHT</td>
          <td>　</td>
          <td>尾燈</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">29</td>
          <td bgcolor="#FFFFFF">ハンドルスイツチ．レバ－</td>
          <td bgcolor="#FFFFFF">HANDLE SWITCH．    LEVER&nbsp;</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">把手開關．離合器拉桿.剎車拉桿</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>30</td>
          <td>エレクトリカル</td>
          <td>ELECTRICAL</td>
          <td>　</td>
          <td>電系</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">31</td>
          <td bgcolor="#FFFFFF">ワイヤーハーネス</td>
          <td bgcolor="#FFFFFF">WIRE HARNESS</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">線組．線束</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>32</td>
          <td>バツテリー</td>
          <td>BATTERY</td>
          <td>　</td>
          <td>電池</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">33</td>
          <td bgcolor="#FFFFFF">ラベル</td>
          <td bgcolor="#FFFFFF">LABLE</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">警告標籤</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>34</td>
          <td>グラフイツク．エンブレム</td>
          <td>GRAPHIC．EMBLEM</td>
          <td>MARK．DECALS</td>
          <td>外觀貼紙</td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF">35</td>
          <td bgcolor="#FFFFFF">オーナーズツール</td>
          <td bgcolor="#FFFFFF">OWNER'S TOOLS</td>
          <td bgcolor="#FFFFFF">　</td>
          <td bgcolor="#FFFFFF">隨車工具</td>
        </tr>
        <tr bgcolor="#CCCCCC">
          <td>36</td>
          <td>カウリング　</td>
          <td>COWLING</td>
          <td>　</td>
          <td>整流罩</td>
        </tr>
      </tbody></table><br><br>

      <table cellspacing="0" cellpadding="0" border="1" align="center" class="table table-bordered">
        <tbody><tr>
          <td nowrap="" bgcolor="#7EBCFA" colspan="10"><p align="center">片假名50音</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#CCCCCC" colspan="10"><p align="center">清音</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ア</p></td>
          <td nowrap=""><p align="center">カ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">サ</p></td>
          <td nowrap=""><p align="center">タ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ナ</p></td>
          <td nowrap=""><p align="center">ハ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">マ</p></td>
          <td nowrap=""><p align="center">ヤ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ラ</p></td>
          <td nowrap=""><p align="center">ワ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">a</p></td>
          <td nowrap=""><p align="center">ka</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">sa</p></td>
          <td nowrap=""><p align="center">ta</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">na</p></td>
          <td nowrap=""><p align="center">ha</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ma</p></td>
          <td nowrap=""><p align="center">ya</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ra</p></td>
          <td nowrap=""><p align="center">wa</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">イ</p></td>
          <td nowrap=""><p align="center">キ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">シ</p></td>
          <td nowrap=""><p align="center">チ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ニ</p></td>
          <td nowrap=""><p align="center">ヒ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ミ</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">リ</p></td>
          <td nowrap=""><p align="center">　</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">i</p></td>
          <td nowrap=""><p align="center">ki</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">shi</p></td>
          <td nowrap=""><p align="center">chi</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ni</p></td>
          <td nowrap=""><p align="center">hi</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">mi</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ri</p></td>
          <td nowrap=""><p align="center">　</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ウ</p></td>
          <td nowrap=""><p align="center">ク</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ス</p></td>
          <td nowrap=""><p align="center">ツ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ヌ</p></td>
          <td nowrap=""><p align="center">フ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ム</p></td>
          <td nowrap=""><p align="center">ユ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ル</p></td>
          <td nowrap=""><p align="center">ン</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">u</p></td>
          <td nowrap=""><p align="center">ku</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">su</p></td>
          <td nowrap=""><p align="center">tsu</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">nu</p></td>
          <td nowrap=""><p align="center">fu</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">mu</p></td>
          <td nowrap=""><p align="center">yu</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ru</p></td>
          <td nowrap=""><p align="center">n</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">エ</p></td>
          <td nowrap=""><p align="center">ケ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">セ</p></td>
          <td nowrap=""><p align="center">テ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ネ</p></td>
          <td nowrap=""><p align="center">ヘ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">メ</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">レ</p></td>
          <td nowrap=""><p align="center">　</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">e</p></td>
          <td nowrap=""><p align="center">ke</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">se</p></td>
          <td nowrap=""><p align="center">te</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ne</p></td>
          <td nowrap=""><p align="center">he</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">me</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">re</p></td>
          <td nowrap=""><p align="center">　</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">オ</p></td>
          <td nowrap=""><p align="center">コ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ソ</p></td>
          <td nowrap=""><p align="center">ト</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ノ</p></td>
          <td nowrap=""><p align="center">ホ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">モ</p></td>
          <td nowrap=""><p align="center">ヨ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ロ</p></td>
          <td nowrap=""><p align="center">ヲ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">o</p></td>
          <td nowrap=""><p align="center">ko</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">so</p></td>
          <td nowrap=""><p align="center">to</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">no</p></td>
          <td nowrap=""><p align="center">ho</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">mo</p></td>
          <td nowrap=""><p align="center">yo</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ro</p></td>
          <td nowrap=""><p align="center">o</p></td>
        </tr>
        <tr>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#CCCCCC" colspan="9"><p align="center">濁音</p></td>
          <td nowrap="" bgcolor="#CCCCCC"><p align="center">半濁音</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ガ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ザ</p></td>
          <td nowrap=""><p align="center">ダ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">バ</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">パ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">GA</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ZA</p></td>
          <td nowrap=""><p align="center">DA</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">BA</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">PA</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ギ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ジ</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">ビ</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ピ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">GI</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ZI</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">BI</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">PI</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">グ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ズ</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">ブ</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">プ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">GU</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ZU</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">BU</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">PU</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ゲ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ゼ</p></td>
          <td nowrap=""><p align="center">デ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">ベ</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ペ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">GE</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ZE</p></td>
          <td nowrap=""><p align="center">DE</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">BE</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">PE</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ゴ</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ゾ</p></td>
          <td nowrap=""><p align="center">ド</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">ボ</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">ポ</p></td>
        </tr>
        <tr>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">GO</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">ZO</p></td>
          <td nowrap=""><p align="center">DO</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center"><a href="#EBEBEB">BO</a></p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">　</p></td>
          <td nowrap="" bgcolor="#EBEBEB"><p align="center">　</p></td>
          <td nowrap=""><p align="center">PO</p></td>
        </tr>
      </tbody>
    </table>
    <div class="text-center"><a class="btn btn-default" href="{{URL::route('genuineparts')}}">回正廠零件首頁</a></div><br>
@stop