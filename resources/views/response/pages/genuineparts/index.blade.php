@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/product-relieved-shopping.css') !!}">
    <?php
        if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE])){
            $zones = [
                '8' => 18,
            ];
        }else{
            $zones = [
                '8' => 8,
            ];
        }
    ?>
    <style>
        .title-main-box .realtime-tag img{
            width:auto;
            margin-top:0px;
        }
        .no-haeley{
            margin: 10px 0px;
        }
        .banner-advertisement{
            float: none;
        }
        .haeley-text{
            background-color: red;
            color: #FFF;
            padding: 0px 5px;
        }
        .haeley-text span{
            font-weight: bold;
            font-size: 1rem;
        }
        a.limit img{
            width: 100%
        }
    </style>
@stop

@section('left')
    <div class="box-page-group box-page-group-top">
        <div class="ct-box-page-group">
            <ul class="ul-button-ct-box-page-group">
                @foreach($import_manufacturers as $name => $collection)
                    @if(isset($collection['group']))
                        <li>
                            <a href="javascript:void(0)">{!! $name !!}正廠零件報價查詢 <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                            <ul class="improt-menu-left">
                                @foreach($collection['group'] as $genuine_manufacturer)
                                    <li><a href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"><span>{!! $genuine_manufacturer->display_name !!}</span></a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    @include('response.pages.genuineparts.partials.link')
    @include('response.pages.genuineparts.partials.function-banner')


    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>正廠零件功能與說明</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-banner-ct-box-page-group">
                @for($i = 1 ; $i <= 6 ; $i++)
                    <li>
                        <div>
                            {!! $advertiser->call($zones[8]) !!}
                        </div>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
@stop
@section('right')
    <div class="title-main-box">
        <h1>Webike正廠零件查詢購買系統</h1>
    </div>
    <a class="banner-advertisement" href="{{route('genuineparts-step')}}"><img src="{!! assetRemote('image/oempart/banner02.jpg') !!}" alt="banner"></a>
    @if(activeValidate('2019-03-29','2019-05-03') and $bar_obvious)
        <div class="no-haeley"> 
            <img src="{{ assetRemote('image/benefit/big-promotion/2019/newyear/1901_genuine_370.jpg') }}" alt="banner">
        </div>
    @endif
    <div class="btn-gap-top btn-gap-bottom">
        <iframe width="1000" height="520" src="https://www.youtube.com/embed/2IXSPsEESzI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="goldenweek-position-genuineparts">
        @include('response.pages.product-detail.partials.goldenweek-tag')
    </div>
    <div class="box-info-oem">

        【Webike正廠零件查詢購買系統】購買進口，國產原廠機車零件不求人，24小時線上免費報價，簡單又快速!<br>
        目前系統支援：<br>
        @include ( 'response.pages.genuineparts.partials.domestic-country' )
        @include ( 'response.pages.genuineparts.partials.import-country' )
        <br>
        @include ( 'response.pages.genuineparts.partials.attention' )
    </div>
    @include('response.pages.genuineparts.partials.genuineparts-relieved-shopping-img')
    @foreach($sect_manufacturers as $name => $collection)
        @if(isset($collection['group']))
            <div class="box-items-group">
                <div class="title-main-box">
                    @foreach($collection['icons'] as $icon)
                        <img src="{!! assetRemote($icon) !!}" alt="icon flag"/>
                    @endforeach
                    <h2 style="{{ $name == '日本' ? 'float:left;margin-right:10px;' : '' }}">{!! $name !!}廠牌正廠零件</h2>
                        @if($name == '日本')
                            <span class="realtime-tag"><img src="{{ assetRemote('image/label/real_time.gif') }}" alt="正廠零件即時回覆"></span>
                        @endif

                </div>
                <div class="ct-box-items-group">
                    <ul class="ul-ct-box-items-group">
                        @foreach($collection['group'] as $genuine_manufacturer)
                            <li><a style="display: table-cell;" class="logo-brand logo-harley" href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"><img src="{!! assetRemote('image/oempart/'.$genuine_manufacturer->api_usage_name.'.png') !!}" alt="logo brand"/></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    @endforeach
@stop
@section('script')
@stop