@extends('response.layouts.1column')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="title text-center">
                        <h2 class="size-15rem">正廠零件 Parts List閱覽</h2>
                    </div>
                    <div class="text-center">
	                    <span>
	                        以下為日本四大廠零件料號查詢方式，請各位經銷商自行查詢您所需要的零件料號。<br>
	※本專頁為「Webike經銷商」零件料號查詢用。請注意，不能公開，若有不當使用行為，隨即取消經銷商資格。<br>
	※本公司不提供零件料號代查服務，也不會檢查料號是否正確，請多多包涵。<br>
	※請注意零件手冊中所列的價格並非真實售價，售價一律以本公司”零件查詢結果”為準。<br>
	                    </span>
	                </div>
                </li>
                <li>
                   	<div class="title"></div>
                    <div class="text-center col-md-6 col-sm-12 col-xs-12" style="border-right:1px solid #dcdee3;">
                    	<img src="//img.webike.net/sys_images/smpl0/honda_logo.gif" alt="HONDA" ><br><br>
	                    <span>
	                        <a href="{{URL::route('genuineparts-manual-list',['manufacturer' => 'honda','displacement' =>'50-125'])}}">日規車系零件手冊</a><br>
							<a href="http://www.ronayers.com/oemparts/c/honda/parts" target="_blank">美規車系零件網站</a><br>
							<a href="https://www.bike-parts-honda.com/" target="_blank">歐規車系零件網站</a><br>
	                    </span>
	                </div>
	               
                    <div class="text-center col-md-6 col-sm-12 col-xs-12">
                    	<img src="//img.webike.net/sys_images/smpl0/yamaha_logo.gif" alt="YAMAHA" ><br><br>
	                    <span>
	                        <a href="https://www.yamaha-motor.co.jp/parts-search/pc/" target="_blank">日規車系零件手冊</a><br>
							<a href="https://www.bike-parts-yam.com/" target="_blank">美規車系零件網站</a><br>
							<a href="https://www.bike-parts-yam.com/" target="_blank">歐規車系零件網站</a><br>
	                    </span>
	                </div>
                </li>
                <li>
                    <div class="title"></div>
                    <div class="text-center col-md-6 col-sm-12 col-xs-12" style="border-right:1px solid #dcdee3;">
                    	<img src="//img.webike.net/sys_images/smpl0/suzuki_logo.gif" alt="SUZUKI" ><br><br>
	                    <span>
	                        {{-- <a href="http://www.webike.tw/genuineparts/manual/suzuki/50-125">日規車系零件手冊</a><br> --}}
							<a href="http://www.ronayers.com/oemparts/c/suzuki/parts" target="_blank">美規車系零件網站</a><br>
							<a href="https://www.bike-parts-suz.com/" target="_blank">歐規車系零件網站</a><br>
	                    </span>
	                </div>
                    <div class="text-center col-md-6 col-sm-12 col-xs-12">
                    	<img src="//img.webike.net/sys_images/smpl0/kawasaki_logo.gif" alt="KAWASAKI" ><br><br>
	                    <span>
	                        <a href="http://www.kawasaki-motors.com/for_users/partscatalog/kmj/html/PCSearch.html" target="_blank">日規車系零件手冊</a><br>
							<a href="https://www.kawasaki.com/parts" target="_blank">美規車系零件網站</a><br>
							<a href="https://www.bike-parts-kawa.com/" target="_blank">歐規車系零件網站</a><br>
	                    </span>
	                </div>
                </li>
                <li class="text-center">
                	<div class="title"></div>
                	<a class="btn btn-default">回正廠零件</a>
                </li>
            </ul>
        </div>
    </div>
@stop