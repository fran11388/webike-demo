<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>正廠手冊書籍購買</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li><a href="{{route('parts',['br/300/'])}}?q=零件手冊" target="_blank">HONDA零件手冊</a></li>
            <li><a href="{{route('parts',['br/847/'])}}?q=零件手冊" target="_blank">YAMAHA零件手冊</a></li>
            <li><a href="{{route('parts',['br/740/'])}}?q=零件手冊" target="_blank">SUZUKI零件手冊</a></li>
            <li><a href="{{route('parts',['br/345/'])}}?q=零件手冊" target="_blank">KAWASAKI零件手冊</a></li>
            <li><a href="{{route('parts',['br/300/'])}}?q=維修手冊" target="_blank">HONDA維修手冊</a></li>
            <li><a href="{{route('parts',['br/847/'])}}?q=維修手冊" target="_blank">YAMAHA維修手冊</a></li>
            <li><a href="{{route('parts',['br/740/'])}}?q=維修手冊" target="_blank">SUZUKI維修手冊</a></li>
            <li><a href="{{route('parts',['br/345/'])}}?q=維修手冊" target="_blank">KAWASAKI維修手冊</a></li>
            <li><a href="{{route('parts',['br/2076/'])}}?q=維修手冊" target="_blank">HAYNES維修手冊</a></li>
            <li><a href="{{route('parts',['br/1988/'])}}?q=維修手冊" target="_blank">CLYMER維修手冊</a></li>
        </ul>
    </div>
</div>
<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>正廠精品配件</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li><a href="{{route('summary',['br/300'])}}" target="_blank">HONDA正廠零件精品配件</a></li>
            <li><a href="{{route('summary',['br/847'])}}" target="_blank">YAMAHA正廠零件精品配件</a></li>
            <li><a href="{{route('summary',['br/740'])}}" target="_blank">SUZUKI正廠零件精品配件</a></li>
            <li><a href="{{route('summary',['br/345'])}}" target="_blank">KAWASAKI正廠零件精品配件</a></li>
            <li><a href="{{route('summary',['br/284'])}}" target="_blank">HARLEY-DAVIDSON正廠零件精品配件</a></li>
            <li><a href="{{route('summary',['br/2125'])}}" target="_blank">BMW正廠零件精品配件</a></li>
        </ul>
    </div>
</div>
<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>零件料號查詢</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li>
                <a href="javascript:void(0)"><h2>HONDA</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="http://www.ronayers.com/oemparts/c/honda_motorcycle/parts" title="HONDA美規料號查詢" target="_blank" nofollow="">美規料號查詢</a></li>
                    <li><a href="https://www.bike-parts-honda.com/" title="HONDA歐規料號查詢" target="_blank" nofollow="">歐規料號查詢</a></li>
                    <li><a href="https://www.hondamotopub.com/HMJ/" title="HONDA日規料號查詢" target="_blank" nofollow="">日規料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>YAMAHA</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.shopyamaha.com/parts-catalog/lines/street-mcy" title="YAMAHA美規料號查詢" target="_blank" nofollow="">美規料號查詢</a></li>
                    <li><a href="https://www.bike-parts-yam.com/" title="YAMAHA歐規料號查詢" target="_blank" nofollow="">歐規料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>SUZUKI</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="http://www.ronayers.com/oemparts/#/c/suzuki_motorcycle/parts" title="SUZUKI美規料號查詢" target="_blank" nofollow="">美規料號查詢</a></li>
                    <li><a href="https://www.bike-parts-suz.com/" title="SUZUKI歐規料號查詢" target="_blank" nofollow="">歐規料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>KAWASAKI</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.kawasaki.com/ownercenter" title="KAWASAKI美規料號查詢" target="_blank" nofollow="">美規料號查詢</a></li>
                    <li><a href="https://www.bike-parts-kawa.com/" title="KAWASAKI歐規料號查詢" target="_blank" nofollow="">歐規料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>HARLEY</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="http://www.jerseyh-d.com/Components.aspx" title="HARLEY-DAVIDSON料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>BMW</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="http://www.realoem.com/bmw/select.do?vin=&amp;kind=M&amp;arch=0" title="BMW料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>DUCATI</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://amsducati.com/ducati-oem/" title="DUCATI料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>KTM</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.fowlersparts.co.uk/browser/manufacturer/ktm" title="KTM料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>

            <li>
                <a href="javascript:void(0)"><h2>TRIUMPH</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.fowlersparts.co.uk/" title="TRIUMPH料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>APRILIA</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.fowlersparts.co.uk/browser/manufacturer/aprilia" title="APRILIA料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>PIAGGIO</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.fowlersparts.co.uk/browser/manufacturer/piaggio" title="PIAGGIO料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>VESPA</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/1_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA引擎零件" target="_blank" nofollow="">引擎零件</a></li>
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/86_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA懸吊與煞車" target="_blank" nofollow="">懸吊與煞車</a></li>
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/112_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA板金車台零件及徽章" target="_blank" nofollow="">板金車台零件及徽章</a></li>
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/200_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA橡膠零件及里程表" target="_blank" nofollow="">橡膠零件及里程表</a></li>
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/274_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA坐墊零件" target="_blank" nofollow="">坐墊零件</a></li>
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/296_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA電系零件" target="_blank" nofollow="">電系零件</a></li>
                    <li><a href="https://img.webike.net/sys_images/genuine/parts_list/PIAGGIO/374_VINT_VESP_it-IT_VINT_VESP_it-IT.pdf" title="VESPA周邊配件" target="_blank" nofollow="">周邊配件</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>HUSQVARNA</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="http://www.husqyparts.com/" title="HUSQVARNA料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>MOTOGUZZI</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.motointernational.com/parts-catalogs-moto-guzzi" title="MOTOGUZZI料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)"><h2>GILERA</h2></a>
                <ul class="sub-menu-box-group">
                    <li><a href="https://www.fowlersparts.co.uk/browser/manufacturer/gilera" title="GILERA料號查詢" target="_blank" nofollow="">料號查詢</a></li>
                </ul>
            </li>
            <!-- <li class="label">DERBI</li>
            <li><a href="https://www.fowlersparts.co.uk/browser/manufacturer/derbi" title="DERBI料號查詢" target="_blank" nofollow>料號查詢</a></li>
            <li><a href="https://www.oemmotorparts.com/oem2.asp?M=Derbi" title="DERBI料號查詢" target="_blank" nofollow>料號查詢</a></li> -->
        </ul>
    </div>
</div>
<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>車主使用手冊</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li><a href="http://www.honda.co.jp/ownersmanual/HondaMotor/motor/" title="HONDA車主使用手冊" target="_blank" nofollow=""><span>HONDA車主使用手冊</span></a></li>
            <li><a href="http://www.yamaha-motor.co.jp/mc/owner-support/manual/index.html" title="YAMAHA車主使用手冊" target="_blank" nofollow=""><span>YAMAHA車主使用手冊</span></a></li>
            <li><a href="http://www.ducati.com/services/maintenance/index.do" title="DUCATI車主使用手冊" target="_blank"><span>DUCATI車主使用手冊</span></a></li>
        </ul>
    </div>
</div>