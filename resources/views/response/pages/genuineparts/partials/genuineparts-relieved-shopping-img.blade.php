<style>
	.relieved-shopping {
		margin-bottom: 20px;
		border: none !important;
	}
	.relieved-shopping-authentic{
		margin-right: 0 !important;
	}
	.relieved-shopping-rex{
		border-right: none !important;
		border-left: none !important;
	}
	
	@media(max-width:760px){
		.relieved-shopping-authentic{
			width: 48.5%;
		}
		.relieved-shopping-authentic{
			margin-right: 0;
			margin: 0 0%;
		} 
		.relieved-shopping-rex{
			width: 48.5%;
		}
		.relieved-shopping-rex{
			margin: 0 0%;
		}
		.relieved-shopping{
			margin-bottom: 0px;
		}
		.relieved-shopping li{
			line-height: 0px !important;
		}
	}
</style>

<div class="relieved-shopping">
	<ul class="clearfix">
		<li class="col-md-6 col-sm-2 col-xs-2 relieved-shopping-authentic size-10rem font-bold">
			<a href="javascript:void(0)" class="relieved-shopping-authentic-view">
				<img src="{{ assetRemote('image/banner/Authentic-safeguard.jpg') }}" alt="">
			</a>
		</li>
		<li class="col-md-6 col-sm-2 col-xs-2  relieved-shopping-rex size-10rem font-bold">
			<a href="javascript:void(0)" class="relieved-shopping-rex-view">
				<img src="{{ assetRemote('image/banner/Return-safeguard.jpg') }}" alt="">
			</a>
		</li>
	</ul>
</div>
<script type="text/javascript">

	$('.relieved-shopping-authentic-view').click(function(){
		swal({
			html: '<div>'+
			'<div>'+
				'<div style="color:#ffffff;background: #9fc5e8;vertical-align: middle;display: inline-block;padding: 5px;"> 正品<br>保證</div>'+
				'<div style="vertical-align: middle;display: inline-block;padding-left:10px">'+
					'<div>Webike安心服務</div>'+
					'<div>正品保證商品</div>'+
				'</div>'+
			'</div>'+
			'<br>'+
			'<div style="text-align: left;">'+
				'<span>'+
					'正品保證之商品，若有被查獲疑似為仿冒品，並經過相關訴訟，最後交由法院判決如實者，將會對購買此商品的顧客進行當初購買價格的10倍賠償。'+
				'</span>'+
			'</div>'+
		'</div>',
		width: 300,
		})	
	});

	$('.relieved-shopping-rex-view').click(function(){
		swal({
			html: '<div>'+
			'<div>'+
				'<div style="color:#ffffff;background: #93c47d;vertical-align: middle;display: inline-block;padding: 5px;"> 退換<br>保證</div>'+
				'<div style="vertical-align: middle;display: inline-block;padding-left:10px">'+
					'<div>Webike安心服務</div>'+
					'<div>退換貨保證商品</div>'+
				'</div>'+
			'</div>'+
			'<br>'+
			'<div style="text-align: left;">'+
				'<span>'+
					'退換貨保證之商品，在您收到商品包裹當天起算，我們提供優於法令規定的10天鑑賞期，包含改裝零件、騎士用品以及原廠零件都可以申請免費退換貨服務，此服務將不會另外再收取任何運費與手續費。'+
				'</span>'+
			'</div>'+
		'</div>',
		width: 300,
		})	
	});

</script>