@if(isset($country_manufacturers['台灣']['group']))
    <?php $domestic_count = 0; ?>
    台灣
    @foreach($country_manufacturers['台灣']['group'] as $genuine_manufacturer)
        <a href="{!! URL::route('genuineparts-divide', [$country_manufacturers['台灣']['url_code'], $genuine_manufacturer->api_usage_name]) !!}"> {!! $genuine_manufacturer->display_name !!} </a>、
        <?php $domestic_count++; ?>
    @endforeach
    {!! $domestic_count !!}大國產廠牌。<br>
@endif