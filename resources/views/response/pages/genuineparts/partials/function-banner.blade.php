<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>正廠零件相關功能</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-banner-ct-box-page-group">
            <li>
                <a href="{{route('genuineparts-step')}}"><img src="{!! assetRemote('image/oempart/banner01.jpg') !!}" alt="正廠零件操作說明"></a>
            </li>
        </ul>
        @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]))
            <ul class="ul-banner-ct-box-page-group">
                <li>
                    <a href="{{route('genuineparts-manual')}}"><img src="//img.webike.tw/assets/images/user_upload/billboard/2016-03-18RandomjTjBS.png" alt="經銷商零件手冊閱覽"></a>
                </li>
            </ul>
        @endif
    </div>
</div>