<?php unset($country_manufacturers['台灣']) ?>
<?php $import_count = 0; ?>
@foreach($country_manufacturers as $name => $collection)
    @if(isset($collection['group']))
        {!! $name !!}
        @foreach($collection['group'] as $genuine_manufacturer)
            <a href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"> {!! $genuine_manufacturer->display_name !!} </a>、
            <?php
            $import_count++;
            ?>
        @endforeach
    @endif
@endforeach
@if($import_count)
    {!! $import_count !!}大國進口廠牌。<br>
@endif