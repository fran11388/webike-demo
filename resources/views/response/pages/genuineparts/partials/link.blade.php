<div class="box-page-group">
    <div class="title-box-page-group">
        <h3>正廠零件功能與說明</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group">
            <li>
                <a href="{{route('customer-history-genuineparts')}}"><span>正廠零件報價履歷</span></a>
            </li>
            <li>
                <a href="{{route('genuineparts-step')}}"><span>正廠零件系統使用說明</span></a>
            </li>
            <li>
                <a href="{{route('customer-service-proposal-create','service') . '?category_id=83' }}" target="_blank"><span>正廠零件系統提問</span></a>
            </li>
            <li>
                <a href="{{route('genuineparts-trans')}}"><span>中英日摩托車專有名詞對照表</span></a>
            </li>
        </ul>
    </div>
</div>