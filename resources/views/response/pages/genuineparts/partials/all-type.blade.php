<div class="box-page-group box-page-group-top">
    <div class="ct-box-page-group">
        <ul class="ul-button-ct-box-page-group">
            @foreach($import_manufacturers as $name => $collection)
                <li>
                    <a href="javascript:void(0)">{!! $name !!}正廠零件報價查詢 <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                    <ul class="improt-menu-left">
                        @foreach($collection['group'] as $genuine_manufacturer)
                            <li><a href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"><span>{!! $genuine_manufacturer->display_name !!}</span></a></li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>
@include('response.pages.genuineparts.partials.link')
<div class="box-page-group visible-sm visible-md visible-lg">
    <div class="title-box-page-group">
        <h3>正廠零件功能與說明</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-banner-ct-box-page-group">
            <li>
                <a href="javascript:void(0)"><img src="{!! assetRemote('image/oempart/banner01.jpg') !!}" alt="banner"></a>
            </li>
        </ul>
    </div>
</div>


@include('response.pages.genuineparts.partials.manual-import' )