注意事項：<br>
1.本系統查詢報價完全免費，使用前請先"<a href="{{route('login')}}">登入會員</a>"；若您還沒加入會員，請點選"<a href="{{route('customer-account-create')}}">快速註冊</a>"。<br>
2.此系統僅限定使用「正廠零件料號」進行查詢，零件料號須由會員自行透過原廠零件手冊進行確認。<br>
3.若有技術性問題或訂購安裝服務，請洽全台各地「<a href="{{route('dealer')}}" target="_blank">Webike經銷商</a>」。<br>
4.其他關於系統相關問題，請至 <a href="{{route('customer-service-proposal-create','service')}}" target="_blank">網站操作及商品諮詢</a> 提問。<br>