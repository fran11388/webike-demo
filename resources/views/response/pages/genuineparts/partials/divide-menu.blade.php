<div class="box-page-group box-page-group-top">
    <div class="ct-box-page-group">
        <ul class="ul-button-ct-box-page-group">
            @foreach($import_manufacturers as $name => $collection)
                @if(isset($collection['group']))
                    <li>
                        <a href="javascript:void(0)">{!! $name !!}正廠零件報價查詢 <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                        <ul class="improt-menu-left">
                            @foreach($collection['group'] as $genuine_manufacturer)
                                <li><a href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"><span>{!! $genuine_manufacturer->display_name !!}</span></a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
@include('response.pages.genuineparts.partials.link')

@include('response.pages.genuineparts.partials.function-banner')


@include('response.pages.genuineparts.partials.manual-'. $divide)
