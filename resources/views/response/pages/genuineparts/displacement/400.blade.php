@extends('response.layouts.1column')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/history.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="title text-center">
                        <h2 class="size-15rem">HONDA 日規零件手冊</h2>
                    </div>
                    <div class="text-center">
                    	<span class="font-color-red">
                    		<br>※重要訊息※<br>
                    	</span>
                    	<span>
                    		零件手冊閱覽需輸入ID及PASS，請點選型錄後輸入：<br><br>
                    	</span>
                    	<span class="size-12rem font-bold">
                    		■■ 零件手冊閲覧用ID・PASS ■■<br>
                    		ID（使用者名稱）： partslist<br>
                    		PASS（密碼）： buhin7777<br>
                    	</span>
                    	<span class="font-color-red">
                    		<br>★閱覽用帳號密碼不定時更新，只提供「Webike經銷商」閱覽用，若作其他用途或不當散播，隨即取消該經銷商資格★<br><br>
                    	</span>
                    	<span>
                    		<a target="_blank" href="http://www.adobe.co.jp/products/acrobat/readstep2.html"><img src="//img.webike.net/sys_images/smpl1/pdf_bnr.gif" width="88" border="0" height="31" alt="Adobe(R) Reader(R)"></a>以下型綠為Adobe(R) Reader(R) PDF形式，請經銷商先行安裝程式，謝謝。<br><br>
                    	</span>
	                    <span class="size-12rem font-bold">
	                        <a href="{{URL::route('genuineparts-manual')}}">回手冊總覽</a><br><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'50'])}}?part=1">50cc - Part1</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'50'])}}?part=2">50cc - Part2</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'50-125'])}}">50 - 125cc</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'126-400'])}}">126 - 400cc</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'400'])}}">400cc 以上</a><br><br>
							<div class="text-center">
                                <strong>
                                    <font class="size-12rem">
                                        <a href="#600cc">600cc</a>
                                    </font> 
                                </strong> / 
                                <a href="#650cc">
                                    <strong>
                                        <font class="size-12rem">650cc</font>
                                    </strong>
                                </a> / 
                                <a href="#700cc">
                                    <strong>
                                        <font class="size-12rem">700cc</font>
                                    </strong>
                                </a> / 
                                <a href="#750cc">
                                    <strong>
                                        <font class="size-12rem">750cc</font>
                                    </strong>
                                </a> / 
                                <a href="#800cc">
                                    <strong>
                                        <font class="size-12rem">800cc</font>
                                    </strong>
                                </a> /
                                <a href="#954cc">
                                    <strong>
                                        <font class="size-12rem">954cc</font>
                                    </strong>
                                </a> /
                                <a href="#1000cc">
                                    <strong>
                                        <font class="size-12rem">1000cc</font>
                                    </strong>
                                </a> /
                                <a href="#1100cc">
                                    <strong>
                                        <font class="size-12rem">1100cc</font>
                                    </strong>
                                </a> /
                                <a href="#1300cc">
                                    <strong>
                                        <font class="size-12rem">1300cc</font>
                                    </strong>
                                </a> /
                                <a href="#1500cc">
                                    <strong>
                                        <font class="size-12rem">1500cc</font>
                                    </strong>
                                </a> /
                                <strong>
                                    <font class="size-12rem">
                                        <a href="#1800cc">1800cc</a>
                                    </font>
                                </strong>
                                <br>
                            </div>
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>

<!--CBR600Fここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="600cc">600cc</a></font><a name="600cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CBR600F</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　CBR600F</font><br></td><td><font color="310B00">PC25</font></td>
    <td><font color="310B00">PC25-140</font></td>
    <td><a href="https://img.webike.net/plsm/honda/007.pdf"><strong>CBR600FV<br>CBR600FV-II</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">PC35</font></td><td><font color="310B00">PC35-100</font></td>
  <td><a href="https://img.webike.net/plsm/honda/008.pdf"><strong>CBR600FX</strong></a></td>
 </tr>
 </tbody></table>
<!--CBR600Fここまで-->

<br>

<!--CBR600RRここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CBR600RR</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　CBR600RR</font><br></td><td><font color="310B00">PC37</font></td>
    <td><font color="310B00">PC37-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda_10.pdf"><strong>CBR600RR3</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">PC40</font></td><td><font color="310B00">PC40-100</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda_11.pdf"><strong>CBR600RR7</strong></a></td>
 </tr>
 </tbody></table>
<!--CBR600RRここまで-->

<br>

<!--HORNET600ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■HORNET600【ホーネット】</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　HORNET</font></td><td rowspan="3"><font color="310B00">PC34</font></td>
    <td><font color="310B00">PC34-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda600-01.pdf"><strong>CB600FW<br>CB600FW-II</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">PC34-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda600-01.pdf"><strong>CB600FX<br>CB600FX-II</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　HORNET-S</font></td><td><font color="310B00">PC34-150</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda600-01.pdf"><strong>CB600F2Y</strong></a></td>
 </tr>
 </tbody></table>
<!--HORNET600ここまで-->

<br>

<!--SILVER WING600ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■SILVER WING600 【シルバーウィング】</font></b></td>
  </tr>
  <tr>
    <td rowspan="7"><font color="310B00">　　SILVER WING<br><br>　　SILVER WING600 ABS<br><br>　　SILVER WING GT600<br><br>
    　　SILVER WING GT600 ABS</font><br></td><td rowspan="6"><font color="310B00">PF01</font></td>
    <td><font color="310B00">PF01-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda400-07.pdf"><strong>FJS600_1</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">PF01-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda400-07.pdf"><strong>FJS600_2</strong></a></td>
 </tr>
 <tr><td><font color="310B00">PF01-120</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda400-07.pdf"><strong>FJS600A3<br>FJS600D3</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">PF01-140</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_silverwing400_silverwing600.pdf"><strong>FJS600A5<br>FJS600D5</strong></a></td>
 </tr>
 <tr><td><font color="310B00">PF01-150</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_silverwing400_silverwing600.pdf"><strong>FJS600A7<br>FJS600D7</strong></a></td>
 </tr>
 
 <tr><td><font color="310B00">PF01-160</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_silverwing400_silverwing600.pdf"><strong>FJS600A8<br>FJS600D8</strong></a></td>
 </tr>
 <tr><td><font color="310B00">PF02</font></td>
  <td><font color="310B00">PF02-100</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_silverwing400_silverwing600.pdf"><strong>FJS600A9<br>FJS600D9</strong></a></td>
 </tr>
 </tbody></table>
<!--SILVER WING600ここまで-->

<br>

<!--TRANSALP600ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■XLV600 TRANSALP 【トランザルプ】</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　トランザルプ600V</font></td></tr><tr><td><font color="310B00">PD06</font></td><td><font color="310B00">PD06-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda400-06.pdf"><strong>XL600VH</strong></a></td>
 </tr>
</tbody></table>
<!--TRANSALP600ここまで-->

<br><br>



<!--BROS650ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="650cc">650cc</a></font><a name="650cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■BROS650 【ブロス】</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　BROS</font></td></tr><tr><td><font color="310B00">RC31</font></td><td><font color="310B00">RC31-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/002.pdf"><strong>NT650J</strong></a></td>
 </tr>
</tbody></table>
<!--BROS650ここまで-->

<br><br>

<!--NC700Xここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="700cc">700cc</a></font><a name="700cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■NC700X</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　NC700X</font></td></tr><tr><td rowspan="6"><font color="310B00">RC63</font></td><td rowspan="6"><font color="310B00">RC63-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/20130101_nc700x_nc700x-abs.pdf"><strong>NC700XC</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　NC700X ＜ABS＞</font></td><td><a href="https://img.webike.net/plsm/honda/20130101_nc700x_nc700x-abs.pdf"><strong>NC700XAC</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　NC700X DualClutch Transmission ＜ABS＞</font></td><td><a href="https://img.webike.net/plsm/honda/20130101_nc700x_nc700x-abs.pdf"><strong>NC700XDC</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　NC700X TypeLD</font></td><td><a href="https://img.webike.net/plsm/honda/20130101_nc700x_nc700x-abs.pdf"><strong>NC700XLC</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　NC700X TypeLD ＜ABS＞</font></td><td><a href="https://img.webike.net/plsm/honda/20130101_nc700x_nc700x-abs.pdf"><strong>NC700XALC</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　NC700X TypeLD DualClutch<br>　　Transmission ＜ABS＞</font></td><td><a href="https://img.webike.net/plsm/honda/20130101_nc700x_nc700x-abs.pdf"><strong>NC700XDLC</strong></a></td>
 </tr>
</tbody></table>
<!--NC700Xここまで-->

<br><br>
<!--CB750ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="750cc">750cc</a></font><a name="750cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CB750</font></b></td>
 </tr>
 <tr>   
  <td rowspan="7"><font color="310B00">　　CB750<br>　　ナイトホーク750</font><br></td><td rowspan="5"><font color="310B00">RC42</font></td>
  <td><font color="310B00">RC42-100</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_20.pdf"><strong>CB750FIIN</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">RC42-110</font></td>
 <td><a href="https://img.webike.net/plsm/honda/201001_20.pdf"><strong>CB750FIIT</strong></a></td>
  </tr>
  <tr>
 <td><font color="310B00">RC42-125</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_20.pdf"><strong>CB750FII1</strong></a></td>
 </tr>
 <tr>
  <td><font color="310B00">RC42-160</font></td>
   <td><a href="https://img.webike.net/plsm/honda/cb750_20120518.pdf"><strong>CB750F7-J<br>CB750F7-2J</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">RC42-170</font></td>
 <td><a href="https://img.webike.net/plsm/honda/cb750_20120518.pdf"><strong>CB750F8-J<br>CB750F8-2J</strong></a></td>
 </tr>
<tr>
 <td><font color="310B00">RC39</font></td>
  <td><font color="310B00">RC39-100</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_20.pdf"><strong>NAS750M</strong></a></td>
 </tr>
 </tbody></table>
<!--CB750ここまで-->

<br>

<!--CB750Fここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CB750F</font></b></td>
  </tr>
  <tr>
    <td rowspan="5"><font color="310B00">　　CB750F<br>　　CB750FB BOL D’OR2<br>　　CB750F INTEGRA</font></td></tr><tr><td rowspan="4"><font color="310B00">RC04</font></td>
    <td><font color="310B00">RC04-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda750-01.pdf"><strong>CB750FZ</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">RC04-101</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda750-01.pdf"><strong>CB750FA</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">RC04-102</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda750-01.pdf"><strong>CB750FB<br>CB750FBB</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">RC04-103</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda750-01.pdf"><strong>CB750FC<br>CB750F2C</strong></a></td>
 </tr>
</tbody></table>
<!--CB750Fここまで-->

<br>

<!--MAGNA750ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■MAGNA750 【マグナ】</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　MAGNA</font></td></tr><tr><td><font color="310B00">RC43</font></td><td><font color="310B00">RC43-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/017.pdf"><strong>VF750CR</strong></a></td>
 </tr>
</tbody></table>
<!--MAGNA750ここまで-->

<br>

<!--SHADOW750ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■SAHDOW750 【シャドウ】</font></b></td>
  </tr>
  <tr>
    <td rowspan="10"><font color="310B00">　　SAHDOW750<br><br>　　SHADOW750＜ABS＞<br><br>　　SHADOW Phantom750<br><br>　　VT750S</font></td><td rowspan="2"><font color="310B00">RC44</font></td>
    <td><font color="310B00">RC44-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/023.pdf"><strong>NV750C2V</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">RC44-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/023.pdf"><strong>NV750C2W</strong></a></td>
 </tr>
 <tr>
 <td rowspan="5"><font color="310B00">RC50</font></td>
  <td><font color="310B00">RC50-100<br>RC50-101<br>RC50-102</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_22.pdf"><strong>VT750C4<br>VT750CA4</strong></a></td>
 </tr>
 <tr>
  <td><font color="310B00">RC50-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_22.pdf"><strong>VT750C5<br>VT750CA5</strong></a></td>
 </tr>
   <tr>
  <td><font color="310B00">RC50-120</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_22.pdf"><strong>VT750C6</strong></a></td>
 </tr>
 <tr><td><font color="310B00">RC50-140</font></td>
  <td><a href="https://img.webike.net/plsm/honda/201001_23.pdf"><strong>VT750C8<br>VT750CA8</strong></a></td>
 </tr>
  <tr><td><font color="310B00">RC50-170</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_shadow750.pdf"><strong>VT750CAC</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">RC53</font></td>
  <td><font color="310B00">C53-120</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_shadow750.pdf"><strong>VT750C2BC</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">RC56</font></td>
  <td><font color="310B00">RC56-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_shadow750.pdf"><strong>VT750CSC</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">RC58</font></td>
  <td><font color="310B00">RC58-120</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_shadow750.pdf"><strong>VT750SC</strong></a></td>
 </tr>
 <tr>
    <td rowspan="1"><font color="310B00">　　SHADOW SLASHER750</font></td><td><font color="310B00">RC48</font></td><td><font color="310B00">RC48-100<br>RC48-105</font></td>
    <td><a href="https://img.webike.net/plsm/honda/024.pdf"><strong>NV750DC1</strong></a></td>
 </tr>
 </tbody></table>
<!--SHADOW750ここまで-->

<br>

<!--VFR750Fここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■VFR750F</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　VFR750F</font></td></tr><tr><td><font color="310B00">RC36</font></td><td><font color="310B00">RC36-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda_08.pdf"><strong>VFR750FL</strong></a></td>
 </tr>
</tbody></table>
<!--VFR750Fここまで-->

<br><br>

<!--VFR800ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="800cc">800cc</a></font><a name="800cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■VFR800</font></b></td>
  </tr>
  <tr>
    <td rowspan="6"><font color="310B00">　　VFR<br>　　VFR ＜ABS＞<br>　　VFR ＜ABS＞・SPECIAL</font></td></tr><tr><td rowspan="5"><font color="310B00">RC46</font></td>
    <td><font color="310B00">RC46-115</font></td>
    <td><a href="https://img.webike.net/plsm/honda/vfr_vfrabs_absspecial_20120509.pdf"><strong>VFR800_2</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">RC46-130</font></td>
    <td><a href="https://img.webike.net/plsm/honda/vfr_vfrabs_absspecial_20120509.pdf"><strong>VFR800_4</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">RC46-140</font></td>
    <td><a href="https://img.webike.net/plsm/honda/vfr_vfrabs_absspecial_20120509.pdf"><strong>VFR800_5</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">RC46-150</font></td>
    <td><a href="https://img.webike.net/plsm/honda/vfr_vfrabs_absspecial_20120509.pdf"><strong>VFR800A6</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">RC46-160</font></td>
    <td><a href="https://img.webike.net/plsm/honda/vfr_vfrabs_absspecial_20120509.pdf"><strong>VFR800A7</strong></a></td>
 </tr>
</tbody></table>
<!--VFR800ここまで-->

<br><br>

<!--CBR954RRここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="954cc">954cc</a></font><a name="954cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CBR954RR</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　CBR954RR</font></td>
    </tr><tr><td><font color="310B00">SC50</font></td><td><font color="310B00">SC50-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_4.pdf"><strong>CBR900RR2</strong></a></td>
 </tr>
</tbody></table>
<!--CBR954RRここまで-->

<br><br>
<!--CBR1000RRここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
<thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="1000cc">1000cc</a></font><a name="1000cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CBR1000RR</font></b></td>
  </tr>
  <tr>
    <td rowspan="3"><font color="310B00">　　CBR1000RR</font></td>
    </tr><tr><td><font color="310B00">SC57</font></td><td><font color="310B00">SC57-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda_05.pdf"><strong>CBR1000RR4</strong></a></td>
 </tr>
 <tr><td><font color="310B00">SC59</font></td><td><font color="310B00">SC59-101</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_5.pdf"><strong>CBR1000RR8-J<br>CBR1000RR8-2J</strong></a></td>
 </tr>
</tbody></table>
<!--CBR1000RRここまで-->

<br>

<!--ファイヤーストームここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■FireStorm 【ファイヤーストーム】</font></b></td>
  </tr>
  <tr>
    <td rowspan="5"><font color="310B00">　　FireStorm</font></td></tr><tr><td rowspan="4"><font color="310B00">SC36</font></td>
    <td><font color="310B00">SC36-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_14.pdf"><strong>VTR1000FV</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">SC36-110</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_14.pdf"><strong>VTR1000FX</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">SC36-120</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_14.pdf"><strong>VTR1000F1</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">SC36-130</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_14.pdf"><strong>VTR1000F2</strong></a></td>
 </tr>
</tbody></table>
<!--ファイヤーストームここまで-->

<br><br>

<!--CB1100ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
<thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="1100cc">1100cc</a></font><a name="1100cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CB1100</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　CB1100<br>　　CB1100 ＜ABS＞<br>　　CB1100 ＜TypeI＞<br>　　CB1100 ＜TypeII＞
    <br>　　CB1100 ＜TypeI＞ ABS<br>　　CB1100 ＜TypeII＞ ABS<br>　　CB1100 BLACK STYLE<br>　　CB1100 BLACK STYLE ＜ABS＞</font></td><td rowspan="3"><font color="310B00">SC65</font></td>
    <td><font color="310B00">SC65-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/20130101_cb1100type1_cb1100type2.pdf"><strong>CB1100A-3J<br>CB1100A-4J<br>CB1100AA-3J<br>CB1100AA-4J</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">SC65-120</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_cb1100type1_cb1100type2.pdf"><strong>CB1100C-J<br>CB1100C-5J<br>CB1100AC-J<br>CB1100AC-5J</strong></a></td>
 </tr>
 <tr><td rowspan="1"><font color="310B00">　　CB1100 ＜スペシャルエディション＞ ABS<br>　　CB1100 ＜無限エディション＞</font></td><td><font color="310B00">SC65-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_cb1100type1_cb1100type2.pdf"><strong>CB1100AB-3J<br>CB1100AB-YA</strong></a></td>
 </tr>
 </tbody></table>
<!--CB1100ここまで-->

<br><br>
<!--CB1300SFここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
    <thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="1300cc">1300cc</a></font><a name="1300cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■CB1300SF</font></b></td>
  </tr>
  <tr>
    <td rowspan="5"><font color="310B00">　　CB1300 SUPER FOUR<br><br>　　CB1300 SUPER FOUR ＜ABS＞<br><br>　　CB1300 SUPER FOUR ＜ABS＞<br>　　Special Edition<br><br>　　CB1300 SUPER BOL D’OR<br><br>　　CB1300 SUPER BOL D’OR ＜ABS＞</font></td>
    </tr><tr><td rowspan="1"><font color="310B00">SC40</font></td><td><font color="310B00">SC40-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_3.pdf"><strong>CB1300FW<br>CB1300FW-II<br>CB1300FW-III<br>CB1300FW-IV</strong></a></td>
 </tr>
    <tr><td rowspan="3"><font color="310B00">SC54</font></td><td><font color="310B00">SC54-100</font></td>
   <td><a href="https://img.webike.net/plsm/honda/honda_06.pdf"><strong>CB1300F3-J<br>CB1300F3-2J<br>CB1300F1_3</strong></a></td>
 </tr>
 <tr><td><font color="310B00">SC54-150<br>SC54-151</font></td>
    <td><a href="https://img.webike.net/plsm/honda/cb1300superfour_abs_special_20120518.pdf"><strong>CB1300_8-J・J/A<br>CB1300_8-2J・2J/A<br>CB1300S8-J・J/A
    <br>CB1300S8-3J・3J/A<br>CB1300A8-J・J/A<br>CB1300A8-2J・2J/A<br>CB1300A8-5J・5J/A<br>CB1300SA8-J・J/A<br>CB1300SA8-3J・3J/A</strong></a></td>
 </tr>
 <tr><td><font color="310B00">SC54-160</font></td>
    <td><a href="https://img.webike.net/plsm/honda/cb1300superfour_abs_special_20120518.pdf"><strong>CB1300_9-J<br>CB1300_9-2J<br>CB1300S9-J
    <br>CB1300S9-3J<br>CB1300A9-J<br>CB1300A9-2J<br>CB1300SA9-J<br>CB1300SA9-3J</strong></a></td>
 </tr>
</tbody></table>
<!--CB1300SFここまで-->

<br>

<!--VT1300CRここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■VT1300CR／VT1300CS</font></b></td>
  </tr>
  <tr>
    <td rowspan="2"><font color="310B00">　　VT1300CR<br>　　VT1300CR ＜ABS＞</font></td><td rowspan="2"><font color="310B00">SC66</font></td>
    <td><font color="310B00">SC66-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/20130101_vt1300cr_vt1300cr-abs.pdf"><strong>VT1300CRA<br>VT1300CRAA</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">SC66-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/20130101_vt1300cr_vt1300cr-abs.pdf"><strong>VT1300CRB<br>VT1300CRAB</strong></a></td>
 </tr>
<tr>
 <td rowspan="1"><font color="310B00">　　VT1300CS<br>　　VT1300CS ＜ABS＞</font></td>
 <td><font color="310B00">SC67</font></td>
    <td><font color="310B00">SC67-110</font></td>
    <td><a href="https://img.webike.net/plsm/honda/20130101_vt1300cr_vt1300cr-abs.pdf"><strong>VT1300CSB<br>VT1300CSAB</strong></a></td>
 </tr>
 </tbody></table>
<!--VT1300CRここまで-->

<br>

<!--X4ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■X4</font></b></td>
  </tr>
  <tr>
    <td rowspan="5"><font color="310B00">　　X4</font></td></tr><tr><td rowspan="4"><font color="310B00">SC38</font></td>
    <td><font color="310B00">SC38-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda1300-01.pdf"><strong>CB1300DCV</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">SC38-110</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda1300-01.pdf"><strong>CB1300DCW</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">SC38-120</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda1300-01.pdf"><strong>CB1300DCX</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">SC38-121</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda1300-01.pdf"><strong>CB1300DCY</strong></a></td>
 </tr>
</tbody></table>
<!--X4ここまで-->

<br><br>
<!--VALKYRIEここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
<thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="1500cc">1500cc</a></font><a name="1500cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■VALKYRIE 【ワルキューレ】</font></b></td>
  </tr>
  <tr>
    <td rowspan="4"><font color="310B00">　　VALKYRIE<br>　　VALKYRIE Tourer</font></td></tr><tr><td rowspan="3"><font color="310B00">SC34</font></td>
    <td><font color="310B00">SC34-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_27.pdf"><strong>GL1500CV<br>GL1500CW</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">SC34-101</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_27.pdf"><strong>GL1500CX<br>GL1500CTX</strong></a></td>
 </tr>
  <tr>
 <td><font color="310B00">SC34-102</font></td>
    <td><a href="https://img.webike.net/plsm/honda/201001_27.pdf"><strong>GL1500CTY</strong></a></td>
 </tr>
</tbody></table>
<!--VALKYRIEここまで-->

<br><br>

<!--GL1800ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
<thead>
        <tr><td colspan="4"><span style="font-weight:bold;"><b><font color="310B00"><a name="1800cc">1800cc</a></font><a name="1800cc"><span></span></a></b></span></td></tr>
        <tr>
            <th>車名</th>
            <th>型式</th>
            <th>車体番号</th>
            <th></th>
        </tr>
    </thead>
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■GL1800</font></b></td>
  </tr>
  <tr>
    <td rowspan="4"><font color="310B00">　　GOLD WING</font></td><td rowspan="5"><font color="310B00">SC47</font></td>
    <td><font color="310B00">SC47-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda_04.pdf"><strong>GL1800A2</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">SC47-110</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda_04.pdf"><strong>GL1800A3</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">SC47-131</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda_04.pdf"><strong>GL1800A5</strong></a></td>
 </tr>
  <tr>
  <td><font color="310B00">SC47-141</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda_04.pdf"><strong>GL1800_6</strong></a></td>
 </tr><tr>
 <td rowspan="1"><font color="310B00">　　GOLD WING・USパッケージ</font></td><td><font color="310B00">SC47-120</font></td>
  <td><a href="https://img.webike.net/plsm/honda/honda_04.pdf"><strong>GL1800_4</strong></a></td>
 </tr>
 </tbody></table>
<!--GL1800ここまで-->

<br>

<!--VTX1800ここから-->
<table class="table table-bordered"  border="1" cellspacing="0" cellpadding="3" bordercolorlight="#C0C0C0" bordercolordark="#C0C0C0">
  <tbody><tr bgcolor="FFE3E3">
    <td colspan="4"><b><font color="310B00">■VTX1800</font></b></td>
  </tr>
  <tr>
    <td rowspan="3"><font color="310B00">　　VTX</font></td></tr><tr><td rowspan="2"><font color="310B00">SC46</font></td>
    <td><font color="310B00">SC46-100</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda_03.pdf"><strong>VTX1800C2</strong></a></td>
 </tr>
 <tr>
 <td><font color="310B00">SC46-110</font></td>
    <td><a href="https://img.webike.net/plsm/honda/honda_03.pdf"><strong>VTX1800C3</strong></a></td>
 </tr>
</tbody></table>
<!--VTX1800ここまで-->
@stop
