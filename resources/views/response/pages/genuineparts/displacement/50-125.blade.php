@extends('response.layouts.1column')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/history.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="title text-center">
                        <h2 class="size-15rem">HONDA 日規零件手冊</h2>
                    </div>
                    <div class="text-center">
                    	<span class="font-color-red">
                    		<br>※重要訊息※<br>
                    	</span>
                    	<span>
                    		零件手冊閱覽需輸入ID及PASS，請點選型錄後輸入：<br><br>
                    	</span>
                    	<span class="size-12rem font-bold">
                    		■■ 零件手冊閲覧用ID・PASS ■■<br>
                    		ID（使用者名稱）： partslist<br>
                    		PASS（密碼）： buhin7777<br>
                    	</span>
                    	<span class="font-color-red">
                    		<br>★閱覽用帳號密碼不定時更新，只提供「Webike經銷商」閱覽用，若作其他用途或不當散播，隨即取消該經銷商資格★<br><br>
                    	</span>
                    	<span>
                    		<a target="_blank" href="http://www.adobe.co.jp/products/acrobat/readstep2.html"><img src="//img.webike.net/sys_images/smpl1/pdf_bnr.gif" width="88" border="0" height="31" alt="Adobe(R) Reader(R)"></a>以下型綠為Adobe(R) Reader(R) PDF形式，請經銷商先行安裝程式，謝謝。<br><br>
                    	</span>
	                    <span class="size-12rem font-bold">
	                        <a href="{{URL::route('genuineparts-manual')}}">回手冊總覽</a><br><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'50'])}}?part=1">50cc - Part1</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'50'])}}?part=2">50cc - Part2</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'50-125'])}}">50 - 125cc</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'126-400'])}}">126 - 400cc</a><br>
							<a href="{{URL::route('genuineparts-manual-list',['manufacturer'=>'honda','displacement'=>'400'])}}">400cc 以上</a><br><br>
							<div class="text-center">
								<strong>
									<font class="size-12rem">
										<a href="#70cc">70cc</a>
									</font>	
								</strong> / 
								<a href="#80cc">
									<strong>
										<font class="size-12rem">80cc</font>
									</strong>
								</a> / 
								<a href="#90cc">
									<strong>
										<font class="size-12rem">90cc</font>
									</strong>
								</a> / 
								<a href="#100cc">
									<strong>
										<font class="size-12rem">100cc</font>
									</strong>
								</a> / 
								<a href="#110cc">
									<strong>
										<font class="size-12rem">110cc</font>
									</strong>
								</a> / 
								<strong>
									<font class="size-12rem">
										<a href="#125cc">125cc</a>
									</font>
								</strong>
								<br>
          					</div>
	                    </span>
	                </div>
                </li>
            </ul>
           	<div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-12 col-sm-4 col-xs-4" style="border-bottom:1px solid white" id="70cc">
		                    	<span class="font-16px">70cc</span>
							</li>
						</ul>
					</li>
		            <li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">車名</span></li>
		                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">型式</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">車体番号</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"></span></li>
		                </ul>
		            </li>
		            <li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CUB70 【カブ】</span></li>
						</ul>
					</li>
		            <li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ70 スーパーカスタム<br>
		　　スーパーカブ70 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">C70</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">C70-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C70CMN／C70DN</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ70 スーパーカスタム<br>
		　　スーパーカブ70 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">C70</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">C70-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C70CMP／C70DP</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ70 スーパーカスタム<br>
		　　スーパーカブ70 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">C70</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">C70-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C70CMS／C70DS</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ70 スーパーカスタム<br>
		　　スーパーカブ70 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">C70</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">C70-140</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C70CMV／C70DV</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ70 カスタム<br>スーパーカブ70 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">C70</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">C70-150</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-01.pdf"><strong>C70CMX／C70DX</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-12 col-sm-4 col-xs-4" style="border-bottom:1px solid white" id="80cc">
		                    	<span class="font-16px">80cc</span>
							</li>
						</ul>
					</li>
		            <li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">車名</span></li>
		                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">型式</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">車体番号</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"></span></li>
		                </ul>
		            </li>
		            <li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CR80</span></li>
						</ul>
					</li>
		            <li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CR80<br>CR80R2</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HE04</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HE04-260</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda80-01.pdf"><strong>CR80RY<br>CR80RBY</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CR80</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CRM80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HD12</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HD12-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_11.pdf"><strong>CRM80R</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■MTX80R</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">MTX80R</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HD08</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HD08-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_19.pdf"><strong>MTX80RFD</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■NSR80</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">NSR80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC06-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/018.pdf"><strong>NSR80J-II</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">NSR80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC06-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/018.pdf"><strong>NSR80K</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">NSR80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC06-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/018.pdf"><strong>NSR80L</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">NSR80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC06-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/018.pdf"><strong>NSR80N</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■スペイシー80</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF02-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_7.pdf"><strong>CH80MSC-1／-2</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY80</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF03-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_7.pdf"><strong>CH80MDF</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-12 col-sm-4 col-xs-4" style="border-bottom:1px solid white" id="90cc">
		                    	<span class="font-16px">90cc</span>
							</li>
						</ul>
					</li>
		            <li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">車名</span></li>
		                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">型式</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">車体番号</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"></span></li>
		                </ul>
		            </li>
		            <li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■Cabina90 【キャビーナ】<br>■Broad90　【ブロード】</span></li>
						</ul>
					</li>
		            <li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">Cbina90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF06-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda50-04.pdf"><strong>SCX90R</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">Broad90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF06-300</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda50-04.pdf"><strong>SCX90S-III</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		   	<div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CD90</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90A</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90H</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90H</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90N</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-140</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90P</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-150</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90S</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA03-170</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-01.pdf"><strong>CD90W</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CUB90【カブ】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 スーパーカスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-180</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C90CMN／C90DN</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 スーパーカスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-190</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C90CMP／C90DP</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 スーパーカスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-200</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C90CMS／C90DS</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 スーパーカスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-210</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-02.pdf"><strong>C90CMV／C90DV</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 カスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-220</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-01.pdf"><strong>C90CMX／C90DX</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 カスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-250</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-01.pdf"><strong>C90CM1／C90D1</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ90 カスタム<br>スーパーカブ90 デラックス</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA02-260</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda70-01.pdf"><strong>C90CM2／C90D2</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■Joker90 【ジョーカー】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">Joker90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF09</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF09-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda50-14.pdf"><strong>SRX90T</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">Joker90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF09</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF09-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda50-14.pdf"><strong>SRX90W</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■LEAD90 【リード90】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">リード90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF05</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF05-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-02.pdf"><strong>NH90MJ</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">リード90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF05</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF05-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-02.pdf"><strong>NH90MN</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">リード90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF05</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF05-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-02.pdf"><strong>NH90MP-J</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">リード90</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HF05</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HF05-128</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda90-02.pdf"><strong>NH90MP-YA</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-12 col-sm-4 col-xs-4" style="border-bottom:1px solid white" id="100cc">
		                    	<span class="font-16px">100cc</span>
							</li>
						</ul>
					</li>
		            <li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">車名</span></li>
		                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">型式</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">車体番号</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"></span></li>
		                </ul>
		            </li>
		            <li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■APE100 【エイプ】</span></li>
						</ul>
					</li>
		            <li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">エイプ・100<br>デラックス<br>スペシャル</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC07</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC07-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda200906-01.pdf"><strong>XZ100_6-J／-2J</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">エイプ・100<br>デラックス<br>スペシャル</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC07</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC07-140</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda200906-01.pdf"><strong>XZ100_7-J／-2J</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">エイプ・100<br>デラックス<br>スペシャル</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC07</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC07-150</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda200906-01.pdf"><strong>XZ100_8-J／-2J／-3J</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">エイプ・100<br>デラックス<br>スペシャル</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC07</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC07-160</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda200906-01.pdf"><strong>XZ100_9-J</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">エイプ・100 Type D</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HC13</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HC13-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda200906-01.pdf"> <strong>XZ100_9-2J</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CUB100 【カブ】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">カブ100EX</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA05</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA05-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda100-02.pdf"><strong>C100CMJ</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">カブ100EX</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA05</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA05-000</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda100-02.pdf"><strong>C100CMK</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ100</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">HA06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">HA06-000</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda100-02.pdf"> <strong>C100CMP</strong><br></a></span><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda100-02.pdf"><strong>C100CMS</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■SPACY100 【スペイシー】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY100</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF13</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF13-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda_25.pdf"><strong>SCR100WH3</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY100</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF13</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF13-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda_25.pdf"><strong>SCR100WH6</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY100</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF13</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF13-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda_25.pdf"><strong>SCR100WH7</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■LEAD100 【リード】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">リード<br>リード100</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF06-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda100-01.pdf"><strong>NH100W</strong></a></span><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda100-01.pdf"><strong>NH100X</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-12 col-sm-4 col-xs-4" style="border-bottom:1px solid white" id="110cc">
		                    	<span class="font-16px">110cc</span>
							</li>
						</ul>
					</li>
		            <li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">車名</span></li>
		                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">型式</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">車体番号</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"></span></li>
		                </ul>
		            </li>
		            <li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CUB110 【カブ】</span></li>
						</ul>
					</li>
		            <li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ110pro</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JA07</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JA07-300</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/super_cub110_pro_20120509.pdf"><strong>C110BN9</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">スーパーカブ110</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JA10</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JA10-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/20130101_supercub110.pdf"> <strong>NBC110C</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■LEAD110 【リード】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">LEAD<br>LEADスペシャルカラー<br>LEAD　EX</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF19</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF19-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/lead_lead_special_color_ex_20120509.pdf"><strong>NHX110WH8 </strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">LEAD<br>LEADスペシャルカラー<br>LEAD　EX</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF19</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF19-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/lead_lead_special_color_ex_20120509.pdf"><strong>NHX110WH9</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">LEAD<br>LEADスペシャルカラー<br>LEAD　EX</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF19</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF19-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/lead_lead_special_color_ex_20120509.pdf"><strong>NHX110WHA</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■BENLY110 【ベンリィ】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">ベンリィ110<br>ベンリィ110プロ</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JA09</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JA09-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/20130101_benly110_benly110pro.pdf"><strong>MW1101WHC</strong></a></span><span class="font-16px"><a href="https://img.webike.net/plsm/honda/20130101_benly110_benly110pro.pdf"><strong>MW1102WHC</strong></a></span><span class="font-16px"><a href="https://img.webike.net/plsm/honda/20130101_benly110_benly110pro.pdf"><strong>MW110WHC-J</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
			<div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■DiO110 【ディオ】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">DiO110</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF31</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF31-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/20130101_dio110.pdf"><strong>NSC110WHB</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-12 col-sm-4 col-xs-4" style="border-bottom:1px solid white" id="125cc">
		                    	<span class="font-16px">125cc</span>
							</li>
						</ul>
					</li>
		            <li class="history-table-title visible-md visible-lg">
		                <ul>
		                    <li class="col-md-4 col-sm-4 col-xs-4"><span class="font-16px">車名</span></li>
		                    <li class="col-md-2 col-sm-2 col-xs-2"><span class="font-16px">型式</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px">車体番号</span></li>
		                    <li class="col-md-3 col-sm-3 col-xs-3"><span class="font-16px"></span></li>
		                </ul>
		            </li>
		            <li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CB125T</span></li>
						</ul>
					</li>
		            <li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CB125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC06-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/004.pdf"><strong>CD125TF</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CB125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC06</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC06-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/004.pdf"><strong>CB125TJ</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CBX125C</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CB125C</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC12</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC12-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_6.pdf"><strong>CBX125CE</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CB125C</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC12</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC12-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_6.pdf"><strong>CBX125CH</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CBX125F</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CB125F</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC11</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC11-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda_22.pdf"><strong>CBX125F<br>CBX125FE<br></strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CB125C</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC11</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC11-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda_22.pdf"><strong>CBX125FH</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CD125T</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">CD125T</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">CD125T-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-01.pdf"><strong>CD125TF</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">CD125T</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">CD125T-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-01.pdf"><strong>CD125TJ</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">CD125T</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">CD125T-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-01.pdf"><strong>CD125TN</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">CD125T</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">CD125T-140</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-01.pdf"><strong>CD125TP</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">CD125T</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">CD125T-150</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-01.pdf"><strong>CD125TS</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CD125T</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JA03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JA03-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-01.pdf"><strong>CD125T1</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■CR125R</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">CR125R</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JE01</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JE01-192</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_10.pdf"><strong>CR125RW</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■MBX125FD</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">MBX125FD</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JC10</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JC10-192</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_17.pdf"><strong>MBX125FD</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■NX125</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">NX125</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JD09</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JD09-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/nx125_20120518.pdf"><strong>NX125J</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■SPACY125 【スペイシー】</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF02-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-03.pdf"><strong>CH125CD</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF02</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF02-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-03.pdf"><strong>CH125CF</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF03-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-03.pdf"><strong>CH125H</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF03-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-03.pdf"><strong>CH125N</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF03-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-03.pdf"><strong>CH125P</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF03</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF03-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-03.pdf"><strong>CH125R</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF04</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF04-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-02.pdf"><strong>CHA125S</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF04</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF04-110</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-02.pdf"><strong>CHA125W</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF04</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF04-120</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-02.pdf"><strong>CHA1251</strong></a></span></li>
		                </ul>
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">SPACY125 <br> SPACY125ストライカー</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF04</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF04-130</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/honda125-02.pdf"><strong>CHA1253</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■PCX</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">PCX</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JF28</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JF28-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/pcx_20120509.pdf"><strong>WW125EX2A</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
		    <div class="history-table-container">
		        <ul class="history-info-table">
		        	<li class="history-table-content history-genuine-table-content" style="border-bottom:1px dotted #9fa7b5;">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-12 col-sm-12 col-xs-12"><span class="font-16px blue-text">■XLR125R</span></li>
						</ul>
					</li>
		        	<li class="history-table-content history-genuine-table-content">
		                <ul class="col-sm-block col-xs-block clearfix">
		                    <li class="col-md-4 col-sm-12 col-xs-12"><span class="font-16px blue-text">XLR125R</span></li>
		                    <li class="col-md-2 col-sm-12 col-xs-12"><span class="font-16px">JD16</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px">JD16-100</span></li>
		                    <li class="col-md-3 col-sm-12 col-xs-12"><span class="font-16px"><a href="https://img.webike.net/plsm/honda/201001_29.pdf"><strong>XLR125RP</strong></a></span></li>
		                </ul>
		            </li>
		        </ul>
		    </div>
        </div>
    </div>
@stop
@section('script')
	<script type="text/javascript" src="{{asset('js/pages/history/table-response.js')}}"></script>
@stop
