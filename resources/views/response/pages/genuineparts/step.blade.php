@extends('response.layouts.2columns')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/history.css')}}">
@stop
@section('left')
	<div class="box-page-group box-page-group-top">
        <div class="ct-box-page-group">
            <ul class="ul-button-ct-box-page-group">
                @foreach($import_manufacturers as $name => $collection)
                    <li>
                        <a href="javascript:void(0)">{!! $name !!}正廠零件報價查詢 <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                        <ul class="improt-menu-left">
                            @foreach($collection['group'] as $genuine_manufacturer)
                                <li><a href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"><span>{!! $genuine_manufacturer->display_name !!}</span></a></li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @include('response.pages.genuineparts.partials.link')
    @include('response.pages.genuineparts.partials.function-banner')


    <div class="box-page-group visible-sm visible-md visible-lg">
        <div class="title-box-page-group">
            <h3>正廠零件功能與說明</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-banner-ct-box-page-group">
                <li>
                    <a href="javascript:void(0)"><img src="{!! asset('image/oempart/banner04.png') !!}" alt="banner"></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{!! asset('image/oempart/banner05.png') !!}" alt="banner"></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{!! asset('image/oempart/banner06.png') !!}" alt="banner"></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{!! asset('image/oempart/banner07.png') !!}" alt="banner"></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{!! asset('image/oempart/banner08.png') !!}" alt="banner"></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{!! asset('image/oempart/banner09.png') !!}" alt="banner"></a>
                </li>
            </ul>
        </div>
    </div>
@stop
@section('right')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="title text-center">
                        <img src="{{assetRemote('image/genuineparts/step/mainbanner.png')}}" alt="正廠零件查詢購買系統操作說明「Webike-摩托百貨」" title="正廠零件查詢購買系統操作說明「Webike-摩托百貨」">
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>什麼是正廠零件?</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        正廠零件為「車輛製造商製造生產之零配件」<br>
						每台摩托車都是由數千至數萬個零配件組合而成，在龐大的零件數量之下，各零件的命名也不盡相同，消費者要如何才能夠正確的選購真正想要的零件呢?<br>
						因此各摩托車原廠皆會推出對應該車型的「零件手冊」，在零件手冊上您可以找到該車型各細部零件的「料號」，我們只要依據零件手冊上面標示的料號來進行訂購，就能避免訂購錯誤的情形。<br>
                    </span>
                    <span class="text-red-color">
                    	※料號：料號依據各車廠定義不同，也將會有不同的編碼型式，通常都會是"數字"與"英文"搭配標點符號來組成。
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>目前可以購買那些品牌?</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        我們目前系統支援：<br><br>
                        進口廠牌<br>
    					@include ( 'response.pages.genuineparts.partials.import-country' )<br>
    					國產廠牌<br>
    					@include ( 'response.pages.genuineparts.partials.domestic-country' )
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>Webike正廠零件查詢購買系統操作流程</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <div class="text-center">
                    	<img src="https://img.webike.tw/assets/images/genuineparts/step/Snap2.png" alt="正廠零件購物流程" title="正廠零件購物流程「Webike-摩托百貨」">
	                </div>
                    <span>
                        選擇您要查詢的廠牌，並且填入您要查詢的料號之後，送出查詢。<br>
                        依據不同廠牌及國內外廠商的不同，因此回覆報價的時間也不盡相同，最快5分鐘就會回覆報價以及交期。<br>
                        確定報價及交期之後，您就可以開始訂購，下訂之後進口零件最快4個工作天到貨，國產零件最快1個工作天到貨。<br>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>進口正廠零件查詢操作步驟</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                	<span>
                		1.零件手冊<br>
                		請先準備一本零件手冊，可以選擇在我們網站<a href="{{URL::route('summary',['section'=>'parts/ca/3000-1210-t004'])}}" title="購買零件手冊「Webike-摩托百貨」" target="_blank">購買零件手冊</a>，或是參考我們所提供的歐美零件查詢網站。
                	</span>
                    <div class="text-center">
                    	<img src="https://img.webike.tw/assets/images/genuineparts/step/import-1.png" alt="零件手冊「Webike-摩托百貨」" title="零件手冊「Webike-摩托百貨」">
	                </div>
                    <span>
                        2.零件料號<br>
                        選好零件部位之後，在依照分解圖所示的編號去尋找您需要的零件料號。 （如圖所示，1號零件所對應的零件料號即為17210-MFL-000）
                    </span>
                    <div class="text-center">
						<img src="https://img.webike.tw/assets/images/genuineparts/step/import-2.png" alt="零件料號「Webike-摩托百貨」" title="零件料號「Webike-摩托百貨」">
                    </div>
                    <span>
                    	3.選擇廠牌<br>
                    	在<a href="{{URL::route('genuineparts')}}" title="正廠零件「Webike-摩托百貨」" target="_blank">正廠零件首頁</a>，請您選擇您愛車的品牌，目前我們提供的進口廠牌有日本 HONDA、 YAMAHA、 SUZUKI、 KAWASAKI、 美國 HARLEY-DAVIDSON、 德國 BMW、 英國TRIUMPH、 義大利 DUCATI、 APRILIA、 PIAGGIO、 VESPA、 HUSQVARNA、 MOTOGUZZI、 GILERA、 奧地利 KTM、 泰國 HONDA、 YAMAHA、 十七個進口廠牌。<br>
                    	4.進口正廠零件料號及數量、備註輸入<br><br>
                    </span>
                    <div class="text-center">
                    	<img src="{{assetRemote('image/genuineparts/step/import-3.png')}}" alt="選擇正廠零件廠牌「Webike-摩托百貨」" title="選擇正廠零件廠牌「Webike-摩托百貨」">
                    </div>
                    <span>
                    	<br>請輸入<a href="{{URL::route('summary',['section'=>'parts/ca/3000-1210-t004'])}}" title="購買零件手冊「Webike-摩托百貨」" target="_blank">進口正廠零件</a>的”料號”（統一為英數大寫半形）及所需的數量，您也可以在備註欄輸入中文名稱，以利日後您要核對商品時有個備註可以依據。若您不知道料號，請洽「<a href="{{URL::route('dealer')}}" title="Webike經銷商「Webike-摩托百貨」" target="_blank">Webike經銷商</a>或購買正廠零件手冊。<br>
                    </span>
                    <span class="text-red-color">
                    	※再輸入料號的欄位請勿輸入空格、中文字、以及非原廠料號內提供的符號，以避免查詢錯誤的情形發生。<br>
                    	※若您有不同廠牌的零件需要查詢，請分開查詢，同一品牌僅接受該品牌的料號，若輸入非該品牌的"零件料號"，查詢結果將會不明。<br>
                    	※進口正廠零件會有所限制，材積過大、危險品（電瓶、鋰電池（超過3個）、異體、高壓氣體、氣瓶...等），需商檢品（輪胎），以上幾類無法進口，假使您有購買到此類相關商品，我們將會通知您並協助取消此筆訂單。<br><br>
                    </span>
                    <div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-4.png')}}" alt="輸入零件料號及數量「Webike-摩托百貨」" title="輸入零件料號及數量「Webike-摩托百貨」">
                    </div><br>
                    <span>
                    	輸入完零件料號及數量之後，再按下送出查詢即可，送出前請再次確認是否為您要查詢的品牌。
                    </span><br>
                    <div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-5.png')}}" alt="確認查詢的正廠零件品牌「Webike-摩托百貨」" title="確認查詢的正廠零件品牌「Webike-摩托百貨」">
                    </div><br>
                    <span>
                    	5.查看報價及交期<br>
                    	送出查詢後，您會得到一串查詢編號，您可以至頁面左方的「<a href="{{URL::route('customer-history-genuineparts')}}" title="查詢履歷「Webike-摩托百貨」" target="_blank">查詢履歷</a>」確認您的查詢履歷及查詢結果。
                    </span><br>
                    <div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-6.png')}}" alt="查看正廠零件報價及交期「Webike-摩托百貨」" title="查看正廠零件報價及交期「Webike-摩托百貨」">
                    </div><br>
                    <span>
                    	查詢時間約為5分鐘至1~2個工作天左右不等，查詢結果完成後，我們會回覆email至您的信箱，同時在畫面左上角我們也會發一封通知訊息給您，您也可以至<a>正廠零件查詢購買系統 > 查詢履歷</a> 查看報價及交期。
                    </span><br>
                    <div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-7.png')}}" alt="查看正廠零件報價及交期「Webike-摩托百貨」" title="查看正廠零件報價及交期「Webike-摩托百貨」">
                    </div><br>
                    <span>
                    	在頁面中我們會提供您查詢料號的"報價"及"交期"，報價皆包含國際運費與關稅及發票稅金，交期為預估的工作天時間（不包含假日），確認價格及交期您可以接受之後，選擇加入購物車即可開始訂購。<br>
                    </span>
                    <span class="text-red-color">
                    	※請注意，每次報價之後僅會保留14日，一旦超過時間報價內容便會消失，屆時還請您再次查詢。<br>
                    	※進口正廠零件的價格會依據原廠售價及匯率變動，因此不同時間點查詢的價格可能會有變化。<br>
                    </span><br>
                    <div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-8.png')}}" alt="開始訂購正廠零件「Webike-摩托百貨」" title="開始訂購正廠零件「Webike-摩托百貨」">
                    </div><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>國產正廠零件查詢操作步驟</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
					<span>
					1.零件手冊<br>
					請先準備零件手冊，我們有提供相關推薦連結，或是您可以至google搜尋或是國內各大論壇查詢。<br>
					</span>
					<br>
					<div class="text-center">
						<img src="https://img.webike.tw/assets/images/genuineparts/step/import-9.png" alt="零件手冊「Webike-摩托百貨」" title="零件手冊「Webike-摩托百貨」">
					</div>
					<br>
					<span>
					2.零件料號<br>
					選好零件部位之後，在依照爆炸圖所示的編號去尋找您需要的零件料號。 （如圖所示，左拉桿所對應的零件料號即為53178-GAK-901）<br>
					</span>
					<br>
					<div class="text-center">
						<img src="https://img.webike.tw/assets/images/genuineparts/step/import-10.png" alt="零件料號「Webike-摩托百貨」" title="零件料號「Webike-摩托百貨」">
					</div>
					<br>
					<span>
					3.選擇廠牌<br>
					在正廠零件首頁，請您選擇您愛車的品牌，目前我們提供國產廠牌光陽KYMCO、三陽SYM、台灣山葉YAMAHA、台鈴SUZUKI、宏佳騰AEON、PGO摩特動力、哈特佛HARTFORD，七家國產廠牌。<br>
					</span>
					<br>
					<div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-11.png')}}" alt="選擇正廠零件廠牌「Webike-摩托百貨」" title="選擇正廠零件廠牌「Webike-摩托百貨」">
					</div>
					<br>
					<span>
					4.國產正廠料號及數量、備註輸入<br>
					請輸入國產正廠零件的”料號”（統一為英數大寫半形）及所需的數量，您也可以在備註欄輸入中文名稱，以利日後您要核對商品時有個備註可以依據。若您不知道料號，請洽「<a href="{{URL::route('dealer')}}" title="Webike經銷商「Webike-摩托百貨」" target="_blank">Webike經銷商</a>」或購買正廠零件手冊。
					</span>
					<br>
					<span class="text-red-color">
					※再輸入料號的欄位請勿輸入空格、中文字、以及非原廠料號內提供的符號，以避免查詢錯誤的情形發生。<br>
					※若您有不同廠牌的零件需要查詢，請分開查詢，同一品牌僅接受該品牌的料號，若輸入非該品牌的"零件料號"，查詢結果將會不明。
					</span>
					<br>
					<div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-15.jpg')}}" alt="輸入零件料號及數量「Webike-摩托百貨」" title="輸入零件料號及數量「Webike-摩托百貨」">
					</div>
					<br>
					<span>
					輸入完零件料號及數量之後，再按下送出查詢即可，送出前請再次確認是否為您要查詢的品牌。<br>
					</span>
					<br>
					<div class="text-center">
					<img src="{{assetRemote('image/genuineparts/step/import-17.jpg')}}" alt="確認查詢的正廠零件品牌「Webike-摩托百貨」" title="確認查詢的正廠零件品牌「Webike-摩托百貨」">
					</div>
					<br>
					<span>
					5.查看報價及交期<br>
					送出查詢後，您會得到一串查詢編號，您可以至頁面左方的「<a href="{{URL::route('customer-history-genuineparts')}}" title="查詢履歷「Webike-摩托百貨」" target="_blank">查詢履歷</a>」確認您的查詢履歷及查詢結果。
					</span>
					<br>
					<div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-16.jpg')}}" alt="查看正廠零件報價及交期「Webike-摩托百貨」" title="查看正廠零件報價及交期「Webike-摩托百貨」">
					</div>
					<br>
					<span>
					查詢時間約為一小時至1~2個工作天左右不等，查詢結果完成後，我們會回覆email至您的信箱，同時在畫面左上角我們也會發一封通知訊息給您，您也可以至<a>正廠零件查詢購買系統 > 查詢履歷</a> 查看報價及交期。
					</span>
					<br>
					<div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-18.jpg')}}" alt="查看正廠零件報價及交期「Webike-摩托百貨」" title="查看正廠零件報價及交期「Webike-摩托百貨」">
					</div>
					<span>
					在頁面中我們會提供您查詢料號的"報價"及"交期"，報價皆發票稅金，交期為預估的工作天時間（不包含假日），確認價格及交期您可以接受，選擇加入購物車之後即可開始訂購。
					</span>
					<br>
					<span class="text-red-color">
					※請注意，每次報價之後僅會保留7日，一旦超過時間報價內容便會消失，屆時還請您再次查詢。<br>
					</span>
					<br>
					<div class="text-center">
					<img src="{{assetRemote('image/genuineparts/step/import-19.jpg')}}" alt="開始訂購正廠零件「Webike-摩托百貨」" title="開始訂購正廠零件「Webike-摩托百貨」">
					</div>
                </li>
            </ul>
        </div>
        <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>購買流程</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
					<span>
					當您查詢完正廠零件報價之後，除了（交期未定、停產、完售、不明）等商品之外，其餘的正廠零件商品皆可在頁面中勾選，並且加入<a href="{{URL::route('cart')}}" title="購物車「Webike-摩托百貨」" target="_blank">購物車</a>，除了正廠零件之外，您同時也可以購買"<a href="{{URL::route('cart')}}#additional" title="購物車「Webike-摩托百貨」" target="_blank">加價購</a>"之商品或是其餘<a href="{{URL::route('summary',['section'=>'ca/3000'])}}" title="騎士用品「Webike-摩托百貨」" target="_blank">騎士用品</a>與<a href="{{URL::route('summary',['section'=>'ca/1000'])}}" title="騎士用品「Webike-摩托百貨」" target="_blank">改裝零件</a>，全部加入購物車之後，再進行結帳即可。<br>
                    @if(activeShippingFree())
                        全館購物滿$3000元免運費，未滿3000元國內宅配運費$90元，您可以選擇貨到付款（會再收取手續費）、信用卡結帳、銀行匯款等三種付款方式。
                    @else
					    不限訂單金額、數量，運費統一90元，您可以選擇貨到付款（會在收取手續費）、信用卡結帳、銀行匯款等三種付款方式。<br>
                    @endif
					在結帳時，您也可以使用您累積的點數或是您收到的任何折價券來折抵消費金額。<br>
					</span>
					<div class="text-center">
						<img src="{{assetRemote('image/genuineparts/step/import-20.jpg')}}" alt="訂購正廠零件或是騎士用品與改裝零件「Webike-摩托百貨」" title="訂購正廠零件或是騎士用品與改裝零件「Webike-摩托百貨」">
					</div>
				</li>
			</ul>
		</div>
    </div>
    <div class="text-center">
    	<a class="btn btn-default" href="{{URL::route('genuineparts')}}" title="正廠零件「Webike-摩托百貨」" target="_blank">返回正廠零件首頁</a>
    </div>
@stop
@section('script')
@stop