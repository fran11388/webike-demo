@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="/css/pages/webikewanted.css">
    <link rel="stylesheet" type="text/css" href="/css/pages/guess.css">
    <style>
        #choose div:hover {
            background-color:white;
            opacity:0.2;
            cursor:pointer;
        }
        .baseBtn_yellow{
            width: 300px;
            background: linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
            background-color: #ffec64;
            border-radius: 6px;
            border: 1px solid #ffaa22;
            display: inline-block;
            color: #0d0d0d;
            font-family: arial;
            text-decoration: none;
            text-shadow: 0px 1px 0px #ffee66;
            line-height: 38px;
            text-align: center;
            font-size: 15px;
            font-weight: bold;
        }
        .baseBtn_yellow:hover {
            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffab23), color-stop(1, #ffec64));
            background:-moz-linear-gradient(top, #ffab23 5%, #ffec64 100%);
            background:-webkit-linear-gradient(top, #ffab23 5%, #ffec64 100%);
            background:-o-linear-gradient(top, #ffab23 5%, #ffec64 100%);
            background:-ms-linear-gradient(top, #ffab23 5%, #ffec64 100%);
            background:linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffab23', endColorstr='#ffec64',GradientType=0);
            background-color:#ffab23;
            color:#0d0d0d;
        }
        .baseBtn_yellow:active {
            position:relative;
            top:1px;
        }
        .red_text{
            color:red;
            font-weight: bold;
        }
        @media(max-width:768px) {
            .calendar-wanted {
                width:25% !important;
            }

            .switch a {
                padding:10px;
            }
        }
        @media(max-width:532px) {
            .calendar-wanted {
                width:50% !important;
            }
        }

        .btn-calendar-wanted a {
            min-width: auto !important;
        }
    </style>
@stop
@section('middle')
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="banner-wanted-top form-group">
                <img src="{!! assetRemote('image/benefit/big-promotion/2019/spring/1904_Wanted.jpg') !!}" alt="Name banner">
            </div>
            <div class="description col-xs-12 col-sm-12 col-md-12 form-group">
                <table class="table borderless">
                    <tbody>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【活動期間】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【活動期間】</p><span class="dotted-text3.force-limit">2019年3月29日(五)~5月02日(四)的活動期間中，每日皆有任務題目，您可以參考<a href="javascript:void(0)" onclick="slipTo1('#calendar')">※下方的日程表</a>。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【任務內容】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【任務內容】</p><span class="dotted-text3.force-limit">您可以在下方的<a href="javascript:void(0)" onclick="slipTo1('#question')">【今日任務】</a>區域中看到一張圖片，圖片為每日的任務題目，並且有數字1~30組成的區塊，請在圖片中找到任務目標，並點選該區塊，送出答案即可。※您可以選擇ON或OFF來開啟或關閉區塊的虛線及數字顯示。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【任務時間】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【任務時間】</p><span class="dotted-text3.force-limit">每日00：00至23：59皆可參與當日任務。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【任務解答】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【任務解答】</p><span class="dotted-text3.force-limit">在每日00：00變換任務之後，您可在下方的日程表裡看到前一日任務的正確答案。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【點數發放】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【點數發放】</p><span class="dotted-text3.force-limit">每次任務答題完成，如解答正確，我們會直接將點數發放至您的帳號內，您可以至<a href="{{route('customer-history-points')}}" target="_blank" title="點數獲得及使用履歷 - 「Webike-摩托百貨」">點數獲得及使用履歷</a>裡查看。</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-wanted-page form-group">
                <ul class="ul-article row col-xs-12 col-sm-12 col-md-12">
                    <li class="li-article col-xs-12 col-sm-7 col-md-7">
                        <h1><img src="/image/event/wantedinfo00.png"></h1>
                        <span>在活動期間將每日更新題目，請在任務圖片中找到目標，右圖為該活動的目標，目標將會藏匿在下面的任務圖片中，請找出他。</span>
                    </li>
                    <li class="col-xs-12 col-sm-5 col-md-5 text-center">
                        <img src="/image/event/wantedinfo01.png" class="bg">
                    </li>
                </ul>
                <div class="switch form-group">
                    <a href="javascript:void(0)">
                        OFF
                    </a>
                    <a class="select" href="javascript:void(0)">
                        ON
                    </a>
                </div>
                <div class="example form-group">
                    <div class="question col-xs-12 col-sm-12 col-md-12">
                        <img src="/image/event/wantedinfo02.png" alt="name">
                        <img class="border" src="/image/event/36_cellnum.png" alt="name">
                    </div>
                </div>
                <ul class="ul-article col-xs-12 col-sm-12 col-md-12 form-group">
                    <li class="li-article content-2 col-xs-12 col-sm-7 col-md-7">
                        <h1><img src="/image/event/wantedinfo08_1.png"></h1>
                        <span class="top5">在活動期間將每日更新題目，請在任務圖片中找到目標，右圖為該活動的目標，目標將會藏匿在下面的任務圖片中，請找出他。</span>
                        <img class="img-btn-img-click" src="/image/event/wantedinfo08_2.png">
                    </li>
                    <li class="col-xs-12 col-sm-5 col-md-5 text-center">
                        <img src="/image/event/wantedinfo08_3.png" class="bg">
                    </li>
                </ul>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row"><img class="img-full" src="{{assetRemote('image/event/webikequiz-3.jpg')}}" alt=""></div>
                </div>
            </div>
            <div id="question" class="box-wanted-page form-group">
                <ul class="ul-article row col-xs-12 col-sm-12 col-md-12">
                    <li class="li-article col-xs-12 col-sm-7 col-md-7">
                        <h1><img src="/image/event/wantedinfo09.png"></h1>
                        @if($question)
                            @if( $question->product_id and $question->product->id )
                                <div class="ct-article">
                                    <span>
                                    <h5>
                                        <a href="{{URL::route('product-detail', $question->product->url_rewrite)}}" alt="{{$question->product->name.$tail}}" target="_blank">【{{$question->product->manufacturer->name}}】{{$question->product->name}}</a>
                                    </h5>
                                        {{$question->description}}
                                    </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="row text-center">
                                        <a class="btn btn-warning border-radius-2" href="{{URL::route('product-detail', $question->product->url_rewrite)}}" target="_blank" alt="{{$question->product->name.$tail}}">查看此商品</a>
                                    </div>
                                </div>
                            @else
                                <div class="ct-article">
                                    <a href="javascript:void(0)">在活動期間將每日更新題目，請在任務圖片中找到目標</a>
                                    <span>
                                        <h5>【{{$question->genuine_maker}}】{{$question->genuine_name}}</h5>
                                        {{$question->description}}
                                    </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="row text-center">
                                        <a class="btn btn-warning border-radius-2" href="{{URL::route('genuineparts')}}" target="_blank" alt="{{$question->genuine_maker.$tail}}">{{$question->genuine_maker}}正廠零件</a>
                                    </div>
                                </div>

                            @endif
                        @else
                            <div class="switch form-group">
                                <a class="off" href="javascript:void(0)">
                                    OFF
                                </a>
                                <a class="on select" href="javascript:void(0)">
                                    ON
                                </a>
                            </div>
                        @endif

                    </li>
                    <li class="col-xs-12 col-sm-5 col-md-5 text-center" style="position:relative">
                        <img src="/image/event/wanted_simple_bg.png" class="bg">
                        @if($question)
                            <img src="{{$question->after_img}}" class="bg" style="position: absolute;left: 26%;top: 10px;width:69%">
                        @endif
                    </li>
                </ul>
                <div class="example form-group">
                    @if($question)
                        <div class="switch form-group">
                            <a class="off" href="javascript:void(0)">
                                OFF
                            </a>
                            <a class="on select" href="javascript:void(0)">
                                ON
                            </a>
                        </div>
                    @endif
                    <label style="float:none;overflow: initial;" >
                        請選取今日目標的區域，並按下送出答案即可。
                    </label>
                    {{-- <div class="question col-xs-12 col-sm-12 col-md-12" style="position: relative">
                        <img src="/image/event/2016-07-15_befor.png" alt="name">
                        <img class="36_cell border" src="/image/event/36_cellnum.png" alt="name"> --}}
                    @if($question)
                        {{-- <img class="colorbox-13968" src="{{$question->after_img}}"> --}}
                        <div style="position:relative;">
                            @if( $customer_guess )
                                <img class="colorbox-13968" src="{{$question->ans_img}}" style="z-index:0;width: 100%;">
                            @else
                                <img class="colorbox-13968" src="{{$question->before_img}}" style="z-index:0;width: 100%;">
                            @endif
                            <img class="36_cell" src="/image/event/36_cellnum.png" alt="" style="position:absolute;top:0;z-index:1;width: 100%;left: 0;">
                            <div id="choose" style="position:absolute;width:100%;height:100%;top:0px;z-index:9999">
                                <?php $x = array(0, 16.666, 33.333, 49.998, 66.664, 83.33); ?>
                                <?php $y = array(0, 20, 40, 60, 80); ?>
                                <?php $value = 0; ?>
                                @foreach ($y as $y_value)
                                    @foreach ($x as $x_value)
                                        <?php $value++; ?>
                                        <div //id="{{$value}}" value="{{$value}}" style="left:{{$x_value}}%; top:{{$y_value}}%;position:absolute;width:16.666%;height: 20.1%;z-index:999;cursor:pointer;"></div>
                            @endforeach
                            @endforeach
                        </div>
                </div>
                @else
                @endif
                </li>
                </ul>
                {{-- </div> --}}
            </div>
            @if(Auth::check() == 'true')
                @if( isset($question) )
                    <div class="col-xs-12 col-sm-12 col-md-12" id="result">
                        <div class="row text-center btn-calendar-wanted thumb-box-border">
                            <div class="row">
                                @if( $customer_guess )
                                    @if($customer_mission)
                                        <?php $customer_answers =  explode( ',', $customer_guess->answer ); ?>
                                        <p class="red_text" style="margin-bottom: 10px;">※恭喜您答對了！您的答案為{{$customer_answers[0]}}。<br>恭喜您獲得10點現金點數。您可以至
                                            <a href="{{route('customer-history-points')}}" target="_blank" title="點數獲得及使用履歷 - 「Webike-摩托百貨」">點數獲得及使用履歷</a> 確認您獲得的現金點數。</p>
                                    @else
                                        <?php
                                        $question_answers = explode(',', $question->answer);
                                        $customer_answers =  explode( ',', $customer_guess->answer );
                                        ?>
                                        <p class="red_text">※答錯囉！您的答案為【{{$customer_answers[0]}}】。正確答案為【{{$question_answers[0]}}】！
                                        </p>
                                    @endif
                                @else
                                    <form id="answer" method="post" style="text-align: center" action={{URL::route('benefit-event-webikeguess-post',array('type_id'=>2))}}>
                                        <input name="ans_date" type="text" value="'{{date('Y-m-d')}}'" style="display:none;">
                                        <input class="btn btn-danger" type="submit" value="送出答案">
                                        <input id="customer_ans" name="customer_ans" type="text" value="" hidden="true">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xs-12 col-sm-12 col-md-12" id="result">
                        <div class="row text-center btn-calendar-wanted thumb-box-border">
                            <p class="red_text">※本日尚無題目。</p>
                        </div>
                    </div>
                @endif
            @else
                <div class="col-xs-12 col-sm-12 col-md-12" id="result">
                    <div class="row text-center btn-calendar-wanted thumb-box-border">
                        <a class="btn btn-default s border-radius-2" href="{{ \URL::route('login') }}" >會員登入</a>
                        <a class="btn btn-danger border-radius-2" href="{{ \URL::route('customer-account-create') }}">加入會員</a>
                        <label class="font-color-red">※參加活動，請先登入會員。</label>
                    </div>
                </div>
            @endif
        </div>
        <div id="calendar" class="box-wanted-page form-group">
            <h1 class="form-group">
                <img src="/image/event/wantedinfo10.png" alt="Name">
            </h1>
            <div class="box-calendar col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <ul class="title hidden-xs">
                        <li><span>一</span></li>
                        <li><span>二</span></li>
                        <li><span>三</span></li>
                        <li><span>四</span></li>
                        <li><span>五</span></li>
                        <li><span>六</span></li>
                        <li><span>日</span></li>
                    </ul>
                    <ul class="ct-calendar">
                        @foreach($guesses as $key =>$guess)
                            @if( $key == 0 and $guess->date > $calender_start )
                                <?php
                                $front_day = strtotime( current(current($guesses))->date ) - strtotime($calender_start);
                                $front_day = $front_day/86400;
                                //dd($calender_start);
                                ?>
                                @for ($i=0 ; $i < $front_day ; $i++)
                                    <li class="calendar-wanted">
                                        <div class="ct-box-calendar" style="height: 158px;">
                                            <div class="date">{{ date('m/d', strtotime( "+".$i." day", strtotime($calender_start)) ) }}</div>
                                        </div>
                                    </li>
                                @endfor
                            @endif
                            @if($guess->date < $today)
                                <li class="calendar-wanted">
                                    <div class="ct-box-calendar" style="height: 158px;">
                                        <div class="date">{{ date('m/d', strtotime($guess->date) ) }}</div>
                                        <a id="ansblock" class="popup" href="javascript:void(0)" rel="leanModal">
                                            <div class="cover answer">
                                                <img src="/image/benefit/event/WebikeGuess/answer.png">
                                            </div>
                                            <img src={{$guess->before_img}} class="thum" style="height: 74%; width:100%">
                                            <div class="content" style="display:none">
                                                <?php
                                                $guess_answers = explode(',', $guess->answer);
                                                ?>
                                                <img class="after colorbox-13968" src={{$guess->after_img}}>
                                                <img class="ans_img colorbox-13968" src={{$guess->ans_img}}>
                                                <h1 class="queanswer font-color-red form-group col-xs-12 col-sm-12 col-md-12">
                                                    @foreach ($guess_answers as $guess_answer)
                                                        正確答案為【{{$guess_answer}}】！
                                                    @endforeach

                                                </h1>
                                                <?php $product = $guess->product; ?>
                                                @if( $guess->product_id and $product )
                                                    <span class="producturl">{{URL::route('product-detail', $product->url_rewrite)}}</span>
                                                    <h2>【{{$product->manufacturer->name}}】{{$product->name}}</h2>
                                                    <span class="col-xs-12 col-sm-12 col-md-12">
                                                        商品編號:{{$product->model_number}} <br><br>
                                                        {{$guess->description}}
                                                    </span>
                                                    <span class="href">{{URL::route('product-detail', $product->url_rewrite)}}</span>
                                                    <span class="hreftext">查看此商品</span>
                                                @else
                                                    <span class="producturl">{{URL::route('genuineparts')}}</span>
                                                    <h2>【{{$guess->genuine_maker}}】{{$guess->genuine_name}}</h2>
                                                    <span class="col-xs-12 col-sm-12 col-md-12">
                                                        {{$guess->description}}
                                                    </span>
                                                    <span class="href">{{URL::route('genuineparts')}}</span>
                                                    <span class="hreftext">{{$guess->genuine_maker}}正廠零件</span>
                                                @endif
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            @elseif( $guess->date == $today )
                                <li class="calendar-wanted">
                                    <div class="ct-box-calendar" style="height: 158px;">
                                        <div class="date" >{{ date('m/d', strtotime($guess->date) ) }}</div>
                                        <a class="today" href="javascript:void(0)" >
                                            <div class="cover answer">
                                                <img src="{{$photo_path1}}start_icon.gif" alt="" style="margin-top: 120px;">
                                            </div>
                                            <img src={{$guess->before_img}} class="thum" style="height: 74%;width:100%">
                                        </a>
                                    </div>
                                </li>
                            @else
                                <li class="calendar-wanted">
                                    <div class="ct-box-calendar" style="height: 158px;">
                                        <div class="date" >{{ date('m/d', strtotime($guess->date) ) }}</div>
                                        <img src="{{$photo_path1}}coming_soon.png" style="background-size: 100% 100%;position: absolute;margin-top: 24px;height: 74%;width:100%" class="thum">
                                        <img src={{$guess->before_img}} class="thum" style="margin-bottom:20px;height: 74%;width:100%">
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="answers">
        <div id="ans" class="answer">
            <div class="box-quiz-page box-fix-column" style="position:relative;relative">
                <div class="close" style="position:absolute;right:10px;top:10px;"><img src="/image/benefit/event/WebikeGuess/20130628_webiquiz_modal_close.png" alt=""></div>
                <div class="box" style="margin-top:40px">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="example form-group">
                            @if($question)
                                {{-- <img class="colorbox-13968" src="{{$question->after_img}}"> --}}
                                <div style="position:relative;">
                                    <img class="ans_img colorbox-13968" src="{{$question->ans_img}}" style="z-index:0;width: 100%;">
                                    <img class="36_cell" src="/image/event/36_cellnum.png" alt="" style="position:absolute;top:0;z-index:1;width: 100%;">
                                </div>
                                <div class="switch form-group clearfix">
                                    <div class="col-md-6 col-sm-6 col-xs-6 ">
                                        <a class="off " style="float:none" href="javascript:void(0)">
                                            OFF
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 ">
                                        <a class="on select " style="float:none" href="javascript:void(0)">
                                            ON
                                        </a>
                                    </div>
                                </div>
                            @else
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="box-ct-quiz-right">
                            <div style="position:relative;">
                                <img src="/image/event/wanted_simple_bg.png">
                                @if($question)
                                    <img class="after" src="{{$question->after_img}}"  style="position: absolute;left: 23%;top: 10px; width: 69%;">
                                @endif
                            </div>
                            <h1 class="queanswer font-color-red form-group col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size: 25px;"></h1>
                            <div class="form-group container-quiz-right col-xs-12 col-sm-12 col-md-12">
                                <a class="producturl form-group col-xs-12 col-sm-12 col-md-12" href=""><h2></h2></a>
                                <span class="col-xs-12 col-sm-12 col-md-12"></span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <a class="btn btn-warning style-button-strong border-radius-2" href=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('script')
    <script>
        var alert_msg = "{!! \Session::get("msg") !!}";
        if( alert_msg != null && alert_msg.length > 0 ){
            alert( alert_msg );
        }
        $('.off').click(function(){
            $(this).closest('.example').find('.36_cell').css({'display':'none'});
        });
        $('.on').click(function(){
            $(this).closest('.example').find('.36_cell').css({'display':'block'});
        });
        $('.today').click(function(){
            slipTo1('#question');
        });
        $('.ct-box-calendar #ansblock').click(function(){
            var answers = $('#answers');
            var ansimg = $(this).find('.content').find('.ans_img').attr('src');
            answers.find('.ans_img').attr('src',ansimg);
            var after = $(this).find('.content').find('.after').attr('src');
            answers.find('.after').attr('src',after);
            var answer = $(this).find('.content').find('.queanswer').text();
            answers.find('h1').text(answer);
            var producturl = $(this).find('.content').find('.producturl').text();
            answers.find('.producturl').attr('href',producturl);
            var producttext = $(this).find('.content').find('h2').text();
            answers.find('h2').text(producttext);
            var productcontent = $(this).find('.content').find('span.col-xs-12').html();
            answers.find('span.col-xs-12').html(productcontent);
            var href = $(this).find('.content').find('span.href').text();
            answers.find('a.btn').attr('href',href);
            var hreftext = $(this).find('.content').find('span.hreftext').text();
            answers.find('a.btn').text(hreftext);
        });

        {{--var pulldown = {{$finish}};--}}
//        if( pulldown == true ){
//            var y = $('#question').offset().top - 160;
//            $('html,body').animate({scrollTop: y},800);
//        }
        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 80;
            $('html,body').animate({scrollTop: y}, 400);
        }

    </script>
    @if(Auth::check())
        @if(!$customer_guess)
            <script>
                $('#choose div').click(function(){
                    $('#choose div').css({'opacity':0});
                    $('#choose div').removeClass('active');
                    $(this).css({'opacity': 0.6,'background-color':'white'});
                    $(this).addClass('active');
                });
            </script>
        @endif
        <script>
            $('form').click(function(e){
                var b = 0;
                $("#choose div").each(function(index){
                    var hasActive = $(this).hasClass('active');
                    if(hasActive){
                        var useranswer = $(this).attr('id');
                        $('#answer').children('#customer_ans').attr('value',useranswer);
                        b = true;
                        return b;
                    }
                });
                if(b == 0){
                    $(this).find('p').remove();
                    $(this).append('<p class="red_text">請先選擇答案。</p>');
                    e.preventDefault();
                }else if(b == true){
                    $('#answer').children('.btn-danger').addClass('disabled');
                    $('#answer').children('.btn-danger').prop('disabled',true);
                    $(this).submit();
                }
                return b;
            });
        </script>
        @if(Session::get("finish") == 'true')
            <script>
                slipTo1('#result');
            </script>
        @else
            <script>
                $(document).ready(function(){
                    $('meta[name="viewport"]').attr('content', 'width=device-width, initial-scale=1, user-scalable:no');
                    slipTo1('#question');
                });
            </script>
        @endif
    @endif
@stop