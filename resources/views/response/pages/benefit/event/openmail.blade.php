@extends('response.layouts.1column')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{asset('/css/pages/intructions.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
	        <ul class="ul-membership text-center">
                <li>
                    <div><img src="https://img.webike.tw/assets/images/benefit/event/openmail/1.1.png" alt="週週開信拿現金點數"></div>
	            </li>
	        </ul>
            <ul class="ul-membership">
                <li>
                    <div class="title"><h2>會員好康新登場-週週開信拿現金點數</h2></div>
                    <span>
                        快速簡單三步驟，點數輕鬆入袋!
                    </span><br><br>
                </li>
            </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>STEP1.  開啟每週的電子報，並點擊”現金點數領取”</h2></div>
                    <div class="text-center"><img src="https://img.webike.tw/assets/images/benefit/event/openmail/1.png" alt="STEP1 開啟每周的電子報，並點擊”現金點數領取”"></div>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>STEP2.  連結到Webike台灣站，確認有無出現點數領取確認頁面</h2></div>
                    <div><img src="https://img.webike.tw/assets/images/benefit/event/openmail/2.png" alt="STEP2連結到Webike台灣站，確認有無出現點數領取確認頁面  開啟您的註冊信箱"></div><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>STEP3.  至”點數獲得履歷”檢查是否有獲得點數</h2></div>
                    <div class="text-center"><a href="{{URL::route('customer-history-points')}}"><img src="https://img.webike.tw/assets/images/benefit/event/openmail/3.png" alt="STEP3至”點數獲得履歷”檢查是否有獲得點數  開啟您的註冊信箱"></a></div><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>※注意事項</h2></div>
                	<span>
                		請檢查是否有收到每周五的電子報。
                	</span><br><br>
                    <div class="text-center"><img src="https://img.webike.tw/assets/images/benefit/event/openmail/4.png" alt="開啟您的註冊信箱，請檢察是否有收到每周五的電子報。"></div><br>
                    <span>
                        如未收到，請檢查您的電子信箱垃圾郵件區，檢查電子報是否在裡面，如果在內，請將它設定為”非垃圾郵件”， 以確保您的領取點數的權益。 如果收件夾與垃圾郵件區皆沒有收到電子報，請來信 service@webike.tw，我們將為您處理相關事項。
                    </span><br>
                    <div class="text-center"><img src="https://img.webike.tw/assets/images/benefit/event/openmail/5.png" alt="如未收到請檢察您的信箱垃圾郵件區，檢查電子報是否在裡面"></div><br>
	            </li>
	        </ul><br><br>
            <ul class="ul-membership">
                <li>
                    <div class="col-md-3 col-sm-12 col-xs-12"></div>
                    <div><a class="btn btn-default col-md-2 col-sm-12 col-xs-12" href="{{URL::route('benefit')}}">回會員好康首頁</a></div>
                    <div class="col-md-2 col-sm-12 col-xs-12"></div>
                    <div><a class="btn btn-danger col-md-2 col-sm-12 col-xs-12" href="{{URL::route('customer-account-complete-done')}}">訂閱電子報</a></div>
                    <div class="col-md-3 col-sm-12 col-xs-12"></div>
                </li>
            </ul>
		</div>
	</div>
@stop
