@extends('response.layouts.1column')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{assetRemote('css/pages/intructions.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership text-center">
                <li>
                    <div><img src="https://img.webike.tw/assets/images/benefit/event/mybike/mybike.png" alt="週週開信拿現金點數"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>【新機能登場】”「MyBike」登錄您的愛車”活動介紹</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        <h2>什麼是「<a href="{{URL::route('customer-mybike')}}" >Mybike</a>」？</h2><br>
                        為了提供給您更好的購物體驗，在本次「Webike2.0」改版我們更新了這個功能，目的為了讓您能更容易的找到您愛車對應的商品。<br>
                        在您登錄「Mybike」之後，我們會紀錄您車子的資料，您之後搜尋商品時可以很輕鬆的搭配「Mybike」功能快速的找到對應您愛車的商品，並且日後如有車型對應折扣活動或是相關新聞我們也會email通知您。<br>
                        即日起，只要您首次登錄「MyBike」，即可獲得$100點點數，如原先就有登錄過的會員，直接至「MyBike」頁面重新登錄，並按下確認修改，也可以獲得點數。<br>
                        </span><span class="font-color-red">※「MyBike」功能可登錄多台車，但點數部分只會領取一次$100點。</span><span><br>
                        凡「Webike台灣」會員皆可參加活動，歡迎踴躍登錄!
                    </span><br><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>「My Bike」登錄及使用教學</h2>
        </div>
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                    <div class="title"><h2>1.如何進入「MyBike」登錄頁面</h2></div><br>
                    <span>
                        在登錄之前，要先請您「<a href="{{URL::route('login')}}" >登入會員帳號</a>」，登入之後您在首頁右邊的會員中心區塊，即可看到紅色標示的「<a href="{{URL::route('customer-mybike')}}" >MyBike登錄</a>」字樣，點選之後即可連結至「MyBike」登錄頁面。
                    </span><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/1.png')}}" alt="STEP1 開啟每周的電子報，並點擊”現金點數領取”"></div>
	            </li>
                <li>
                    <div class="title"><h2>2.開始進行「MyBike」登錄</h2></div><br>
                    <span>
                        進入到「<a href="{{URL::route('customer-mybike')}}" >My Bike」登錄頁面</a>之後，您可以將您擁有的車輛依據廠牌、CC數、型號等依序選擇完畢之後，點選「確認修改」即可。
                    </span><br><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/2.png')}}" alt="STEP1 開啟每周的電子報，並點擊”現金點數領取”"></div><br><br>
                    <span>
                        點選「確認修改」後，您將會獲得$100點點數，您可以至「<a href="{{URL::route('customer-history-points')}}" >點數及歷史履歷</a>」查看。
                    </span><br><br>
                    <div class="text-center"><img src="https://img.webike.tw/assets/images/benefit/event/mybike/3.png" alt="會員中心"></div><br><br>
                    <span>
                        如您有多台車，您也可以再回到<a href="{{URL::route('customer-mybike')}}" >「MyBike」登錄頁面</a>，點選「新增車輛」在繼續進行多台車輛的登錄。
                    </span><br><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/4.png')}}" alt="Mybike登錄"></div><br><br>
                </li>
                <li>
                    <div class="title"><h2><a href="{{URL::route('customer-mybike')}}" >3.開始使用「MyBike」功能</a></h2></div><br>
                    <span>
                        我們將會紀錄您登錄「MyBike」的車輛資訊，您可以利用功能來找到對應該車型的商品。<br><br>
                        a.您在搜尋欄位右側可以找到「MyBike商品搜尋」的功能，點選後可看到您所有登錄過的車輛，點選後會連結到該車輛對應的車型商品頁面，您可在裡面看到車輛評價介紹、最新商品、詳細商品分類、商品評論分享、對應品牌等相關商品資訊。
                    </span><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/5.png')}}" alt="Mybike商品搜尋"></div><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/6.png')}}" alt="Mybike商品搜尋"></div>
                    <span>
                        b.當您在搜尋相關改裝商品時，您可以搭配「MyBike」功能，輕鬆幫助您篩選出對應您愛車的相關改裝商品。<br>
                        EX.當您要找「風鏡」商品，搜尋出來的結果為6709項風鏡相關商品，此時我們可以在車型選擇右邊的「MyBike選擇」功能，選擇我們想要找的對應車輛(假設為GSX-R600)。
                    </span><br><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/7.png')}}" alt="STEP1 開啟每周的電子報，並點擊”現金點數領取”"></div><br>
                    <span>
                        選擇完後將會篩選出剩下98件對應GSX-R600使用的風鏡相關商品，讓您輕鬆的找出您想要的商品。
                    </span><br><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/event/mybike/8.png')}}" alt="STEP1 開啟每周的電子報，並點擊”現金點數領取”"></div><br>
                </li>
	        </ul>
		</div>
        <div class="box-container-intruction">
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li class="text-center">
                        <a class="btn btn-danger" href="{{URL::route('customer-mybike')}}" >立即登錄「MyBike」</a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
@stop
