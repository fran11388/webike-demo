@extends('response.layouts.1column')
@section('style')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="/css/pages/webikewanted.css">
	<link rel="stylesheet" type="text/css" href="/css/pages/guess.css">
	<link rel="stylesheet" type="text/css" href="/css/pages/loading_1.css">
	<link rel="stylesheet" type="text/css" href="/css/pages/jquery.jqpuzzle.css">
	<style type="text/css">
		.box-quiz-page h1{
			float:none;
		}
		.borderless tr td:first-child {
			width:113px;
		}
		.jqPuzzle { 
		    background-color: #fff; 
		    /*border: 5px outset #FF0000; */
		    border:5px outset #333;
		    padding: 20px; 
		    padding-bottom: 0px; 
		    margin:0 auto;
		    width:100% !important;
		    height:598px !important;
		} 
		@media screen and (max-width: 767px) {
			.jqPuzzle { 
				height:auto !important;
				padding: 0;
			}
		}
		.jqPuzzle .jqp-wrapper { 
		    padding: 0px; 
		    /*margin:0 auto 10px !important;*/
		    /*border: 2px inset #FF0000; */
		    margin-right:auto !important;
		    margin-left:auto !important;
		    border: 2px inset #333; 
		    background-color: #442222; 
		} 
		.jqPuzzle .jqp-piece { 
		    border: 2px outset #333333; 
		    color: #FFFFFF; 
		    font-size: 30px; 
		    line-height: 30px; 
		    text-align: right; 
		    font-family: Georgia, "Times New Roman", Times, serif; 
		} 
		.jqPuzzle a {
			display:none !important; 
		}
		.jqPuzzle .jqp-controls a{
			display:block !important;
			background: #aaa;
			color:#fff;
		}
		.jqPuzzle .jqp-controls a:hover{
			background: #eee;
			color:#333;
		}
		.jqPuzzle .jqp-controls a.jqp-toggle{
			background: #eee;
			color:#333;
		} 
		.photo img {
			margin-top: 60px;
		}
		.messages {
			/*width: 870px;*/
		    margin: 0 auto;
		    background: #333;
		    color: #fff;
		    padding: 10px;
		    font-size: 14px;
		    font-weight: bold;
		    line-height: 1.4;
		}
		.introduce {
			/*width: 870px;*/
		    margin: 0 auto;
		    padding: 5px;
		    font-size: 14px;
		    font-weight: normal
		}
		.introduce span {
			display:block;
			margin-top: 5px;
			font-size:14px;
			letter-spacing: 1px;
			line-height: 1.4;
		}
		.introduce h5 {
			margin-top: 10px;
			font-size: 16px;
		    font-weight: bold;
		    line-height: 1.2;
		}
		.introduce .btn-ui {
			text-align:right;
		}
		.introduce .btn {
			background: #d90114 !important;
		    color: #fff !important;
		    display: inline-block;
		    font-size: 17px;
		    letter-spacing: 1px;
		    /*width: 180px;*/
		    padding: 0 10px;
		    /*height: 40px;*/
		    /*line-height: 40px;*/
		    vertical-align: middle;
		    border: none;
		    border-radius: 3px;
		}
		.introduce a:hover{
			opacity: 0.6;
		}
		.floatingCirclesG{
			display: none;
			position: absolute;
		    top: 25%;
		    right: 38%;
		}
		li {
			list-style-type:none;
		}
		#ans .ansImage {
			width:50%;
			margin-bottom:10px;
		}
		#ans .ansText {
			width:50%;
			margin-bottom:10px;
		}
		@media(max-width: 992px){
			#ans .ansImage {
				width:100%;
				margin-bottom:10px;
			}
			#ans .ansText {
				width:100%;
				margin-bottom:10px;
			}
			#ans .obvious {
				height:834px !important;
				width:100% !important;
			}
			#ans .ansImage .photo img{
				position:static;
				margin-top:10px;
			}
		}
		@media(max-width: 767px){
			.box-quiz-page-info-top .mission {
				margin-bottom:10px;
			}
			.missionDate {
				width:50% !important;
			}	
		}
		@media(max-width: 500px){
			.missionDate {
				width:100% !important;
			}	
			#ans .obvious {
				height:750px !important;
				width:100% !important;
			}
			#ans .ansImage .photo img{
				position:static;
				margin-top:10px;
			}
			#ans .ansImage {
				width:100%;
				margin-bottom:10px;
			}
			#ans .ansImage .photo {
				height:auto;
			}
			#ans .anxText {
				padding-left:0px;
				overflow: initial !important;
			}
			#todayquiz .jqPuzzle {
				width:100% !important;
			}
			.jqPuzzle .jqp-wrapper{
				width:100% !important;
			}
			.jqPuzzle .jqp-wrapper div:first-child{
				width:100% !important;
			}
			#question .messages {
				margin-top:30px;
			}
		}
		@media(max-width: 400px){
			#question .messages {
				margin-top:60px;
			}
		}
		
		
	</style>
@stop
@section('middle')
	<div class="quiz-page col-xs-12 col-sm-12 col-md-12">
        <div class="banner-top-quiz text-center">
            <img src="{{assetRemote('image/benefit/big-promotion/2019/newyear/999_370.jpg')}}" alt="">
        </div>
        <div class="box-quiz-page">
            <div class="box-quiz-page-info-top">
                <ul class="clearfix row">
					<li class="clearfix box">
						<div class="col-md-2 col-sm-2 col-xs-12 mission">
							<span class="size-10rem">【活動期間】</span>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<span class="size-10rem"> 2018年12月28日(五)~2019年1月31日(四)的活動期間中，每日皆有任務題目，您可以參考<a href="#calender">※下方的日程表</a>。</span>
						</div>
					</li>
					<li class="clearfix box">
						<div class="col-md-2 col-sm-2 col-xs-12 mission">
							<span class="size-10rem">【任務內容】</span>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<span class="size-10rem">您可以在下方的<a href="#question">【今日任務】</a>區域中看到每日的任務題目，任務圖片將會被分割成3X3的區塊，其中有8個區塊有圖片以及一個空的區塊，點擊空區塊旁的圖片將可移動，請在5分鐘之內將正確圖片拼出來吧!</span>
						</div>
					</li>
					<li class="clearfix box">
						<div class="col-md-2 col-sm-2 col-xs-12 mission">
							<span class="size-10rem">【任務時間】</span>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<span class="size-10rem">每日00：00至23：59皆可參與當日任務。</span>
						</div>
					</li>
					<li class="clearfix box">
						<div class="col-md-2 col-sm-2 col-xs-12 mission">
							<span class="size-10rem">【任務解答】</span>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<span class="size-10rem">在每日00：00變換任務之後，您可在下方的日程表裡看到前一日任務的正確答案。</span>
						</div>
					</li>
					<li class="clearfix box">
						<div class="col-md-2 col-sm-2 col-xs-12 mission">
							<span class="size-10rem">【點數發放】</span>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							<span class="size-10rem">每次任務答題完成，如解答正確，我們會直接將點數發放至您的帳號內，您可以至<a href="https://www.webike.tw/customer/history/points" target="_blank" title="點數獲得及使用履歷 - 「Webike-摩托百貨」">點數獲得及使用履歷</a>裡查看。</span>
						</div>
					</li>
				</ul>
            </div>
        </div>
	    <div class="box-quiz-page">
            <h1 class="form-group">「滑滑九宮格」活動說明</h1>
            <span class="form-group size-10rem">
                您將可以在每日任務中看到一個拆散成9個區塊的商品圖，其中最一開始右下角的區塊將是空的，點擊與空的區塊相連的圖片，可以將該圖片移動至原本空的位置，藉此將完整的商品圖片拼出來。 只要您在5分鐘內完成任務，將可立即獲得現金點數，每日皆有任務，連續35天完成，最大將可獲得$550點現金點數。<br><br>

			<font class="font-color-red size-10rem">※在任務下方，您可以選擇「顯示原圖」，點選之後將會有任務完成的原圖給您參考，您也可以點選「顯示編號」，圖片中將會有編號順序讓您參考。</font>
            </span>
            <img class="form-group" src="{{assetRemote('image/event/img-banner-quiz2.jpg')}}" alt="「滑滑九宮格」活動說明">
            <span class="form-group size-12rem">「滑滑九宮格」活動說明</span>
            <img src="{{assetRemote('image/event/webikequiz-3.jpg')}}" alt="">
        </div>
        <div class="box-quiz-page" id="question">
            <h1 class="form-group">每日任務</h1>
            @if( isset($question) and $question )
            	{{-- <img src="{{$question->before_img}}" class="target" id="portrait"> --}}
            	
				{{-- <button type="button" id="try">try</button> --}}
				<div id="todayquiz" style="position: relative;">
				<img src="{{$question->before_img}}" class="target">
				@if(Auth::check())
						<div class="floatingCirclesG">
		                    <div class="f_circleG" id="frotateG_01"></div>
		                    <div class="f_circleG" id="frotateG_02"></div>
		                    <div class="f_circleG" id="frotateG_03"></div>
		                    <div class="f_circleG" id="frotateG_04"></div>
		                    <div class="f_circleG" id="frotateG_05"></div>
		                    <div class="f_circleG" id="frotateG_06"></div>
		                    <div class="f_circleG" id="frotateG_07"></div>
		                    <div class="f_circleG" id="frotateG_08"></div>
		                </div>
	                </div>
					<div class="messages">
						<p>
							點選任一拼圖已開始遊戲，點選"顯示原圖"可以看到拼圖原圖，並將計時暫停。<br>
							@if( $customer_guess )
								<br>
								<font color="#ff7777">※您已經作答過本日的題目。{{$customer_guess->answer}}</font><br>
								前往<a href="http://www.webike.tw/customer/history/points" target="_blank" title="點數獲得及使用履歷{{$tail}}">點數獲得及使用履歷</a> 確認您獲得的現金點數。<br>
								<br>

							@endif
						</p>
					</div>
				@else
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 ct-btn-guess">
                        <div class="row" style="padding: 10px;border: 1px solid #cdcdcd;background-color: #ebebeb;">
                            <div class="col-xs-6 col-sm-6 col-md-6 text-right box">
                                <a class="btn btn-warning border-radius-2" href="{{URL::route('login')}}" alt="{{'會員登入'.$tail}}">會員登入</a>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 text-left box">
                                <a class="btn btn-danger border-radius-2" href="{{URL::route('customer-account-create')}}" alt="{{'加入會員'.$tail}}">加入會員</a>
                            </div>
                            <div class="full-width text-center">
                            	<span class="text-ct-detail font-color-red">※參加活動，請先登入會員。</span>
                            </div>
                        </div>
                    </div>
				@endif
				</div>
				<div class="introduce">
					@if( $question->product_id and $question->product->id )
						<h5>
							<a href="{{URL::route('product-detail', $question->product->url_rewrite)}}" alt="{{$question->product->name.$tail}}" target="_blank">【{{$question->product->manufacturer->name}}】{{$question->product->name}}</a>
						</h5>
						<span>
                            商品料號:{{$question->product->model_number}}
                        </span>
                        <span>
                        	{{$question->description}}
                        </span>
                        <div class="btn-ui align-right">
							<a class="btn btn-default" href="{{URL::route('product-detail', $question->product->url_rewrite)}}" title="{{$question->product->name.$tail}}" target="_blank">商品頁面</a>
						</div>
                    @else
                    	<h5>【{{$question->genuine_maker}}】{{$question->genuine_name}}</h5>
                    	<br>
                        <span>
                        	{{$question->description}}
                        </span>
					@endif
				</div>
			@else
				<ul class="messages width-full" >
	                <li><span>※本日尚無題目。</span></li>
	            </ul>
            @endif

            
        </div>
        <div class="box-quiz-page" id="calender">
            <h1 class="form-group">每日任務解答</h1>
            <div class="box-calendar col-xs-12 col-sm-12 col-md-12">
                <div class="row clearfix">
                    <ul class="title hidden-xs">
                        <li><span>一</span></li>
                        <li><span>二</span></li>
                        <li><span>三</span></li>
                        <li><span>四</span></li>
                        <li><span>五</span></li>
                        <li><span>六</span></li>
                        <li><span>日</span></li>
                    </ul>
                    <ul class="ct-calendar">
                    	@foreach ($guesses as $key => $guess)
							@if( $key == 0 and $guess->date > $calender_start )
								<?php
								$front_day = strtotime( $guess->date ) - strtotime($calender_start); 
								$front_day = $front_day/86400;
								?>
								@for ($i=0 ; $i < $front_day ; $i++)
									<li class="missionDate hidden-xs">
										<div class="ct-box-calendar" style="max-height: 145px;">
											<div class="date">{{ date('m/d', strtotime( "+".$i." day", strtotime($calender_start)) ) }}</div>
										</div>
									</li>
								@endfor
							@endif
							@if($guess->date < $today)
								<li class="missionDate">
                        			<div class="ct-box-calendar">
										<div class="date">{{ date('m/d', strtotime( $guess->date))}}</div>
										<a class="link" href="javascript:void(0)" rel="leanModal">
		                                    <div class="cover answer">
		                                    	<img src="/image/event/answer.png">
		                                    </div>
		                                    <img src="{{$guess->before_img}}" class="thum">
		                                	<div class="content" style="display:none">
												@if( $guess->product_id and $guess->product->id )
													<span class="name">{{$guess->product->name}}</span>
													<span class="producturl">{{URL::route('product-detail', $guess->product->url_rewrite)}}</span>
													<h2>【{{$guess->product->manufacturer->name}}】{{$guess->product->name}}</h2>
													<span class="description">
				                                        商品編號:{{$guess->product->model_number}} <br><br>
				                                        {{$guess->description}}
				                                    </span>
				                                    <span class="href">{{URL::route('product-detail', $guess->product->url_rewrite)}}</span>
				                                    <span class="hreftext">查看此商品</span>
				                                @else
				                                	<span class="name">{{$guess->genuine_maker}}正廠零件</span>
				                                	<span class="producturl">{{URL::route('genuineparts')}}</span>
				                                	<h2>【{{$guess->genuine_maker}}】{{$guess->genuine_name}}</h2>
				                                	<span class="description">
				                                        {{$guess->description}}
				                                    </span>
				                                    <span class="href">{{URL::route('genuineparts')}}</span>
				                                    <span class="hreftext">{{$guess->genuine_maker}}正廠零件</span>
												@endif
		                                	</div>
		                                </a>
                            		</div>
								</li>
							@elseif($today == $guess->date)
								<li class="missionDate">
                        			<div class="ct-box-calendar">
										<div class="date">{{ date('m/d', strtotime( $guess->date))}}</div>
										<a id="today" rel="leanModal">
		                                    <div class="cover answer">
		                                    	<img src="{{$photo_path1}}start_icon.gif" alt="" style="margin-top: 114px;">
		                                    </div>
		                                    	<img src="{{$guess->before_img}}" class="thum">
		                                </a>
                            		</div>
								</li>
							@else
								<li class="missionDate">
                        			<div class="ct-box-calendar">
										<div class="date">{{ date('m/d', strtotime( $guess->date))}}</div>
										<a rel="leanModal">
		                                    <div class="cover answer">
		                                    	<img src="{{$photo_path1}}coming_soon.png"  alt="" >
		                                    </div>
		                                    	<img src="{{$guess->before_img}}" class="thum">
		                                </a>
                            		</div>
								</li>
							@endif
                        @endforeach
                        </li>
                    </ul>
                </div>
            </div>
            <div id="answers">
                <div id="ans" class="answer">
                    <div class="box-quiz-page box-fix-column obvious" style="width:53%;height:483px;float:none;margin-right:auto;margin-left:auto">
                    	<div class="close visible-sm visible-xs"><img src="{{assetRemote('image/benefit/event/WebikeGuess/20130628_webiquiz_modal_close.png')}}" alt=""></div>
                        <div class="box-fix-with box-quiz-left ansImage">
                            <ul class="ul-guess-img">
                                <li class="photo text-center box">
                                    <img src="img/event/9_cellnum.png" alt="">
                                </li>
                            </ul>
                        </div>
                        <div class="box-fix-auto box-quiz-right anxText">
                            <div class="close hidden-sm hidden-xs"><img src="{{assetRemote('image/benefit/event/WebikeGuess/20130628_webiquiz_modal_close.png')}}" alt=""></div>
                            <div class="box-ct-quiz-right">
                                <div class="form-group container-quiz-right col-xs-12 col-sm-12 col-md-12" style="height:368px">
                                    <a class="form-group col-xs-12 col-sm-12 col-md-12" href="" target="_blank" alt=""><h2>【YOSHIMURA】R-11 cyclone 競賽型鈦合金排氣管尾段</h2></a>
                                    <span class="description col-xs-12 col-sm-12 col-md-12">
                                        商品編號:150-519-8F50 <br><br>
                                        最高性能的追求，SUZUKI公升及跑車對應的競賽用鈦合金全段排氣管。YOSHIMURA所推出的「R-11」頂級排氣管，已經在WSBK以及鈴鹿8小時耐力賽進行實戰投入，並且獲得巨大的成果。它所提供的性能高於原廠排氣管，馬力峰值就增加了14.9ps之譜(YOSHIMURA社計測値)，整段排氣管均由鈦合金製造，「R-11」重量比原廠排氣管減輕了約49%。
                                    </span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <a class="btn btn-warning style-button-strong border-radius-2" href="javascript:void(0)" target="_blank" alt="">查看此商品</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop
@section('script')
<script type="text/javascript" src="https://img.webike.net/js/jquery/jquery.unveil.js"></script>
<script type="text/javascript" src="{{assetRemote('js/pages/webikequiz/jquery.jqpuzzle.min.js')}}"></script>
<script>
	$(document).ready(function(){
	    var y = $('#question').offset().top - 100;
	    $('html,body').animate({scrollTop: y},800);
	});
	$('#today').click(function(){
		slipTo('#todayquiz');
	});
	$("#try").click(function(){
		var msgArea = $('.messages');
		var y = msgArea.offset().top - 500;
		$('html,body').animate({scrollTop: y}, 400);
		$('.messages').css({'background':'white','border':'4px solid red'});
		$('.messages').html('<font color="#ff7777" style="font-size:18px;">※您已成功作答此次問題，</font><br><font color="black" style="font-size:18px;">恭喜您獲得10點現金點數。</font><br><font color="black" style="font-size:18px;">您可以至</font><a href="http://www.webike.tw/customer/history/points" target="_blank" title="點數獲得及使用履歷{{$tail}}" style="font-size:18px;">點數獲得及使用履歷</a> <font color="black">確認您獲得的現金點數。</font><br>');
		// $('.floatingCirclesG').show();
		// var token = $('meta[name="csrf-token"]').attr('content');
		// var answer = "{{base64_encode('true')}}";
  //       $.ajax({
  //           url: "{{URL::Route('benefit-event-webikequiz')}}",
  //           type:"POST",
  //           data: {'_token': token,'aa':'aa','answer': answer},
  //           dataType:'JSON',
  //           success:function(result){
  //           	$.each(result.messages, function(key, message){
  //           		var html = $('.messages').append(message);
  //           	});
  //           	$('.floatingCirclesG').hide();
  //               alert(result.messages);
  //           },error:function(){ 
  //               alert("error!!!!");
  //           }
  //       }); //end of ajax
    });

	$(".link").click(function(){
		$("#answers").css({'display':'block'});
		var name = $(this).find('.content').children('.name').text();
		var tail = ' - 「Webike-摩托百貨」';
		var product = name + tail;
		var pic = $(this).find('.thum').attr('src');
		$("#answers").find('.photo').children('img').attr('src',pic);
		var title = $(this).find('.content').children('h2').text();
		$('#answers').find('h2').text(title);
		var producturl = $(this).find('.content').children('.producturl').text(); 
		$('#answers').find('.form-group a').attr({'href':producturl,'alt':product});
		var description = $(this).find('.content').children('.description').html();
		$('#answers').find('.description').html(description);
		var href = $(this).find('.content').children('.href').text();
		$('#answers').find('.btn').attr({'href':href,'alt':product});
		var hreftext = $(this).find('.content').children('.hreftext').text();
		$('#answers').find('.btn').text(hreftext);
	});
	
	
</script>
@if( isset($question) and $question )
<script type="text/javascript">
	// define your own texts 
    var myTexts = { 
        secondsLabel: '秒' ,
        movesLabel: '次移動' ,
        shuffleLabel:'亂數拼圖', 
	    toggleOriginalLabel:'顯示原圖',
	    toggleNumbersLabel:'顯示編號',
	    confirmShuffleMessage:'你確定要刷新Quiz?',
    }; 

    var mySettings = { 
        rows: 3, 
        cols: 3, 
        language: 'en', 
        shuffle: true,
        numbers: false,
        control: { 
            shufflePieces: false,    // display 'Shuffle' button [true|false] 
	        confirmShuffle: true,   // ask before shuffling [true|false] 
	        toggleOriginal: true,   // display 'Original' button [true|false] 
	        toggleNumbers: true,    // display 'Numbers' button [true|false] 
	        counter: true,          // display moves counter [true|false] 
	        timer: true,            // display timer (seconds) [true|false] 
	        pauseTimer: true        // pause timer if 'Original' button is activated 
	                                // [true|false]
        }, 
        animation: { 
            shuffleRounds: 1, 
            slidingSpeed: 100, 
            shuffleSpeed: 200 
        },
        success: { 
        	callback: function(results) {
        		$('.floatingCirclesG').show();
        		if(results.seconds <= 300){
            		var result = "{{base64_encode('true')}}";
            	}else{
            		var result = "{{base64_encode('false')}}";
            	}
                var token = $('meta[name="csrf-token"]').attr('content');
		        $.ajax({
		            url: "{{URL::Route('benefit-event-webikequiz')}}",
		            type:"POST",
		            data: {'_token': token,'result': result,'answer':'移動' + results.moves + '次，共花費' + results.seconds + '秒。','ans_date': "{{date('Y-m-d')}}" },
		            dataType:'JSON',
		            success:function(result){

		            	if(result.msg != ''){
		            		alert(result.msg);
		            		$('.floatingCirclesG').hide();
		            		result = false;
							return;
		            	}

		            	$('.messages').css({'background':'white','border':'4px solid red'});
		            	$.each(result.messages, function(key, message){
		            		var html = $('.messages').html(message);
		            	});
		            	$('.floatingCirclesG').hide();
		            	slipTo('.messages');
						result = true;
						return;
		            },error:function(){ 
		                alert("error!!!!");
		            }
		        }); //end of ajax
		        return result;
            }
        } 
    };
    </script>
    @if(Auth::check())
    	<script type="text/javascript" src="{{assetRemote('js/pages/webikequiz/jquery-save-browser.js')}}"></script>
		<script>
			var hasGuess = {{$customer_guess ? 'true' : 'false'}};
			
	        // define your own settings 
	        if(hasGuess){
	        	mySettings['shuffle'] = false;
	        	mySettings['control']['shufflePieces'] = true;
	        	mySettings['success'] = {};
	        }
			$('#question img').jqPuzzle(mySettings, myTexts);
		</script>
	@else
		<script>
			$('#question img').jqPuzzle(mySettings, myTexts);
		</script>
	@endif
	<script type="text/javascript">
		if($(window).width() <= 767){
			setTimeout(
					function()
					{
						var frame = $('.jqp-wrapper');
						$(frame).find('.jqp-piece').css({
							'background-size': $(frame).width() + 'px ' + $(frame).height() + 'px',
						});
						$(frame).find('div:first-child').addClass('test').css('background-size', $(frame).width() + 'px ' + $(frame).height() + 'px');
					}, 2000);
		}

	</script>
@endif
@stop