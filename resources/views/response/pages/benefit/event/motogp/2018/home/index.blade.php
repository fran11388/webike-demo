@extends('response.layouts.1column')
@section('style')
    <style>
        .menu-left-mobile{
            display:none;
        }
        .padding0{
            padding: 0px;
        }
        .btn-clear {
            background-color: #00c400;
            color: #fff;
            border:0px;
        }
        .btn-clear:hover {
            color: #fff;
            opacity: 0.9;
        }
        /*.margin-bottom40 {*/
            /*margin-top: 40px;*/
        /*}*/
        .top-shopping .container{
            max-width: none !important;
            padding: 0px;
        }
        

        .motogp-container {
            background-color: #e9e9e9;
            padding-bottom:100px;
        }


        .motogp-container .out-width{
            max-width: 1230px !important;
            margin: 0 auto;
        }
        .motogp-container .gp-title{
            margin-bottom:30px;
        }
        .motogp-container .gp-title h2{
            font-size:1.5rem !important
        }

        .motogp-container .game-choose .racer{
            position:relative;
        }
        .motogp-container .game-choose .racer .racer-img{
            position: absolute;
            width: 106%;
            top: -6%;
            right: -9%;
            z-index: 10;
        }
        .motogp-container .gp-choose{
            position: relative;
            margin-top: -45px;
        }
        .motogp-container .gp-choose .gp-choose-center{
            margin-bottom: 15px;
        }
        .motogp-container .gp-choose .gp-choose-left{
            position:absolute;
            top: 8%;
            right:71%;
        }
        .motogp-container .gp-choose .gp-choose-right{
            position:absolute;
            top: 11%;
            left: 71%;
        }
        .motogp-container .gp-banner{
            background: url({{assetRemote('image/benefit/event/motogp/2018/gpbanner8.jpg')}});
            color:#FFF;
            padding-top:40px;
            padding-bottom:20px;
        }
        .motogp-container .gp-race-title{
            margin-bottom: 30px;
        }
        .motogp-container .gp-race-title h2{
            margin-bottom: 10px;
        }
        .motogp-container .gp-banner .game-choose .col-md-1{
            width:11%;
            padding: 0px;
            margin: 0px 0px 10px 10px;
        }
        .motogp-container .gp-btn .bg-color-red{
            border: none;
        }
        .motogp-container .gp-banner .gp-choose .gp-link span{
            color:#fff;
            text-decoration:underline;
        }
        .motogp-container .gp-banner .gp-choose .gp-link div:nth-child(2){
            padding-right: 0px;
        }
        .motogp-container .container .container-buttom-border{
            border-bottom: 4px solid #dddddd;
            padding: 30px 0px;
        }
        .motogp-container .container h2{
            font-weight:bold;
        }
        .motogp-container .container .station-container{
            padding: 30px 0px;
        }
        .motogp-container .container .substation.station-2{
            margin-top: 10px;
        }
        .motogp-container .container .substation table{
            background-color: white;
            border-radius:4px;
        }
        .motogp-container .container .substation table tr td{
            border: 1px solid #aaaaaa40;
        }
        .motogp-container .container .substation .station-text td:nth-child(1){
            width: 80px!important;
        }
        .motogp-container .container .substation .station-text td{
            width: 108px;
            height: 50px;
        }
        .motogp-container  .container .prediction{
            padding-left: 25%;
            padding-right: 25%;
        }
        .motogp-container  .container .prediction .prediction-podium{
            position: relative;
            display: inline-block;
            margin-left: 10%;
            width: 53%;
        }
        .motogp-container  .container .prediction .prediction-podium .prediction-podium-left{
            position: absolute;
            top: 11%;
            right:69%;
        }
        .motogp-container  .container .prediction .prediction-podium .prediction-podium-right{
            position: absolute;
            top: 15%;
            left: 69%;
        }
        .motogp-container .container .top .ridertop{
            background-color: white;
            padding: 0px;
            padding-top:10px;
        }
        .motogp-container .container .top .teamtop{
            margin-left: 5px;
            background-color: white;
            padding: 10px 30px;
        }
        .motogp-container .container .top .teamtop .teamtop-team > div{
            height: 65px;
        }
        .motogp-container .container .top .teamtop .teamtop-team .team-images{
            max-height:100%;
        }
        .motogp-container .container .top .teamtop h2 {
            margin-bottom:16px;
        }
        .motogp-container .container .top .ridertop .ridertop-rider{
            margin-top: 10px ;
            border-top: 1px solid #ddd;
        }
        .motogp-container .container .top .ridertop .ridertop-rider li {
            list-style: none;
            padding-left:0px;
            padding-right:0px;
        }
        .motogp-container .container .top .ridertop .ridertop-rider li .rank-content{
            list-style: none;
            padding: 10px 15px;
            height: 77.94px;
            border-bottom: 1px solid #ddd;
        }
        .motogp-container .container .top .ridertop .ridertop-rider li .rank-content > div{
            height:51.94px;
            font-size:0px;
        }
        .motogp-container .vertical-align-middle{
            display:inline-block;
            vertical-align: middle;
            max-width:100%;
        }
        .motogp-container .container .top .ridertop .ridertop-rider li:nth-child(2n){
            border-left: 1px solid #ddd;
        }
        .motogp-container .container .top .ridertop .ridertop-rider  .nationality p{
            margin-bottom: 10px;
        }
        .motogp-container .container .top .ridertop .ridertop-rider .integraltotal{
            padding-top: 20px;
        }
        .top-ranking .ridertop-rider .rank-number{
            padding:0px;
        }
        .motogp-container .container .rider .rider-date{
            background-color: white;
            margin-top: 10px;
        }
        .motogp-container .container .rider .rider-date .owl-carousel-gp-racer-data {
            padding: 30px 0px;
        }
        .speedwaies-container .owl-carousel-gp-racer-data {
            padding-left:10px !important;
        }
        .motogp-container .container .rider .rider-date ul li{
            list-style: none;
            /*float: left;*/
            /*margin: 0px 10px;*/
        }
        /*.motogp-container .container .rider .rider-date .rider-nationality{*/
            /*float: left;*/
        /*}*/
        #pchistory table th {
            width: 9.09%;
        }
        #pchistory table th,td {
            height:55px;
            vertical-align:middle !important;
        }
        #pchistory .not-join{
            color: #aaaaaa;
        }
        .prediction-ranking{
            display: inline-block;
            vertical-align: bottom;
            width: 36%;
        }
        .news-container .main-news {
            font-size:0px;
        }
        .motogp-container .container .top .teamtop .teamtop-team .team-img-block {
            position:relative;
            height:65px;
        }
        .motogp-container .container .top .teamtop .teamtop-team .team-img .team-rank-img{
            position:absolute;
            /*right:10%;*/
            /*width:89%;*/
            z-index:10;
            /*top:0%;*/
        }
        .speedwaies-container .racer-next,.speedwaies-container .racer-return{
            height:337px !important;
        }
        .owl-carousel-gp-racer-data li .speedway-name{
            margin: 2px 0px;
        }

        .teamtop .owl-prev {
            top: -14%;
            left: -9.5%;
            background-color: #f5f5f5;
            box-shadow: none;
            -webkit-box-shadow: none;
            border-top-right-radius: 2px;
            border-bottom-right-radius: 2px;
            opacity: 1;
            padding: 13px 10px;
            cursor: pointer; 
        }
        .teamtop .owl-next {
            right: -9.6%;
            top:-14%;
            background-color: #f5f5f5;
            box-shadow: none;
            -webkit-box-shadow: none;
            border-top-left-radius: 2px;
            border-bottom-left-radius: 2px;
            opacity: 1;
            padding: 13px 10px;
            cursor: pointer; 
            
        }
        
        .teamtop .owl-prev:hover i{

            color: #333
        }

        .teamtop .owl-next:hover i{

            color: #333
        }

        .main-news-image img{

            object-fit: cover;
        }

        .news-image img{

            object-fit: cover;
        }
    
        @media(max-width:1111px){
            .motogp-container .container .prediction{
                padding-left:20%;
                padding-right:20%;
            }

            .teamtop .owl-prev {
            top: -14%;
            left: -11%;
            }
            .teamtop .owl-next {
            right: -11%;
            top:-14%;
            
            }
        }
        @media(max-width:800px){
            .motogp-container .container .prediction{
                padding-left:25%;
                padding-right:25%;
            }
            .prediction-ranking{
                width:100%;
                margin:0 auto !important;
            }
            .motogp-container .container .prediction .prediction-podium{
                width:100%;
                margin:0 auto !important;
            }
        }
        @media(max-width:768px){
            .game-choose {
                overflow-x: auto;
                padding-top: 10px;
            }
            .motogp-container .container .prediction{
                padding-left:30%;
                padding-right:25%;
                margin-top:20px;
            }
            .motogp-container .game-choose .racer .racer-img{
                position: absolute;
                width: 55%;
                top: -6%;
                right: 19%;
                z-index: 10;
            }

        }

        @media(max-width:625px){
            .motogp-container .container .prediction{
                padding-left:15%;
                padding-right:15%;
            }
        }
        @media(max-width:550px){
            .motogp-container {
                padding-bottom: 50px !important;
            }
            .out-width .overflow-menu-ui .overflow-menu.list-menu{
                position:relative;
                padding-right:25px;
                height:42px;
                overflow-y:hidden;
            }
            .out-width .overflow-menu-ui .list-menu.active{
                overflow-y: inherit;
            }
            .out-width .overflow-menu-ui .overflow-menu.list-menu .list-row{
                width: 25px;
                position: absolute;
                right: 0%;
                top: 0%;
                height: 42px;
                text-align: center;
            }
            .xs-width-full{
                width:100%;
            }
            .motogp-container .container .top .teamtop .teamtop-team .team-img-block {
                height:110px;
            }
            .news-container .main-news{
                height:auto !important
            }
            .motogp-container .container .top .teamtop .teamtop-team{
                text-align:center;
            }
            .motogp-container .container .top .teamtop .teamtop-team .team-img {
                height:125px;
            }
            .motogp-container .container .top .teamtop .teamtop-team .team-title{
                height:30px;
            }
            .motogp-container .container .top .teamtop .teamtop-team .team-score{
                height:15px;
            }
            .motogp-container .container .top .ridertop{
                margin-bottom:20px;
            }
            .motogp-container .container .top-ranking .team-container{
                padding:0px;
            }
            .motogp-container .container .top-ranking .teamtop{
                margin-left:0px;
            }
            .motogp-container .gp-choose{
                padding:20px 40px;
                margin-top:auto;
            }
            .motogp-container .game-choose .racer .racer-img{
                position: absolute;
                width: 82%;
                top: -6%;
                right: 5%;
                z-index: 10;
            }
            .motogp-container .gp-race-title{
                margin-bottom:20px;
            }
            .news-container .main-news-image img {
                max-width:100% !important;
            }
            #mobilehistory{
                width: 100%;
                overflow-y: auto;
                white-space: nowrap;
            }
            #mobilehistory table {
                margin-bottom:0px !important;
            }
            .out-width{
                padding-left:20px;
                padding-right:20px;
            }
            .speedwaies-container .racer-next,.speedwaies-container .racer-return{
                height:353px !important;
            }
            .motogp-container .gp-banner {
                padding-bottom: 0px;
            }
            .race-table table {
                margin-bottom:0px !important;
            }

            .teamtop .owl-prev {
            top: -5.3%;
            left: -9.6%;
            }
            .teamtop .owl-next {
            right: -9.6%;
            top:-5.3%;     
            }
        }
        @media(max-width:400px){
            .motogp-container .game-choose .racer .racer-img{
                position: absolute;
                width: 77%;
                top: -6%;
                right: 7%;
                z-index: 10;
            }

            .teamtop .owl-prev {
            top: -5.3%;
            left: -11%;
            }
            .teamtop .owl-next {
            right: -11%;
            top:-5.3%;     
            }


        }
        @media(max-width:321px){
            .motogp-container .container .prediction{
                padding-left:10%;
                padding-right:5%;
            }
            .game-choose .col-xs-3 {
                width:33.333333%;
            }
            .motogp-container .game-choose .racer .racer-img{
                position: absolute;
                width: 82%;
                top: -6%;
                right: 5%;
                z-index: 10;
            }

            .teamtop .owl-prev {
            top: -5.4%;
            left: -13.6%;
            }
            .teamtop .owl-next {
            right: -13.6%;
            top:-5.4%;     
            }
        }

        .race_date td {
            height:40px;
        }
        /*.news-container .main-news-image img{*/
            /*max-height:398.66px;*/
            /*max-width:650px;*/
        /*}*/
        .news-container a {
            color: #333;
        }
        .news-container a:hover img{
            color: #333;
            opacity: 0.7;
        }
        .news-container h2,h3 {
            max-height:22px;
            overflow:hidden;
            text-overflow:ellipsis;
            white-space:nowrap;
        }
        .owl-carousel-gp-racer-data.owl-carousel {
            width:83.33333333%;
            margin:0 auto;
            float:left;
        }
        .container-buttom-border .racer-return,.container-buttom-border .racer-next {
            background-color:#f5f5f5;
            height:216px;
            font-size:0px;
        }
        .rider-date .owl-carousel-gp-racer-data .racer_detail {
            padding:0 10%;
        }
        .rider-date .owl-carousel-gp-racer-data .racer_detail .rider-nationality{
            float:left;
        }
        .rider-date .owl-carousel-gp-racer-data .racer_detail .rider-facebook{
            float:right
        }

        .rider-date .owl-carousel-gp-racer-data {
            padding-left:10px;
        }
        .owl-carousel-gp-score-rank .team-score {
            font-size:0px;
        }

        .race-table {
            width: 100%;
            overflow-x: auto;
            white-space: nowrap;
            background-color: white;
        }

        .motogp-container .container .top .teamtop .teamtop-team .team-img {
            font-size:0px;
        }
        .team-container .teamtop .owl-nav .owl-prev {
            display:none;
        }
        .motogp-container a {
            color: #333333;
        }
        .motogp-container .btn {
            color: #fff !important;
        }
        ol.breadcrumb-product-detail{
            margin: -1px auto;
            margin-bottom: -40px;
            max-width:1230px;

        }
        ol.breadcrumb-product-detail li a,ol.breadcrumb-product-detail .active span {
            color:white;
        }
        ol.breadcrumb-product-detail li a:hover {
            color:#e61e25;
        }
        .owl-stage-outer {
            list-style-type: none;
        }
        #race_date table.race_date a{
            color: #005fb3;
        }
        @if(!$is_phone)
            .img-block:hover .origin-link-img{
                display:none;
            }
            .img-block:hover .more-link-img{
                display:inline !important;
            }
        @endif
        .selection-search {
            background: white;
        }
        @media(min-width:768px){
            .bannerbar {
                display:none;
            }
            .contents-all-page  {
                margin-top: 150px;
            }
        }
        .bannerbar-phone {
            display:none !important;
        }
    </style>
@stop
@section('middle')
    <div class="motogp-container clearfix">
        @include('response.pages.benefit.event.motogp.2018.home.partials.customer_choose')
        @include('response.pages.benefit.event.motogp.2018.home.partials.list')
        <div class="clearfix container" id="motogp-content">
            <div>
                @include('response.pages.benefit.event.motogp.2018.home.partials.reward-record')
                @include('response.pages.benefit.event.motogp.2018.home.partials.racer-score-rank')
                @include('response.pages.benefit.event.motogp.2018.home.partials.racer-data')
                @include('response.pages.benefit.event.motogp.2018.home.partials.speedwaies')
                @include('response.pages.benefit.event.motogp.2018.home.partials.race_date')
                @include('response.pages.benefit.event.motogp.2018.home.partials.news-info')
            </div>
        </div>
    </div>
    @if($is_phone)
        @include('response.pages.benefit.event.motogp.2018.home.partials.mobile-choice')
    @endif
@stop
@section('script')
    <script>
        $('.game-ok').click(function(e){
            var check = true;
            $('.game-ok input').attr('disabled', true);
            $('.choose-racer').each(function(){
                if($(this).val() == ''){
                    check = false;
                    swal("{{ !isset($customer) ? '請登入會員' : '請選擇車手'}}");
                }
            });
            if(check){
                $('#choice').submit();
            }else{
                e.preventDefault();
                $('.game-ok input').attr('disabled', false);
            }
        });
        @if(session()->has('danger'))
            swal("{{ session()->get('danger') }}");
        @elseif(request()->get('finish'))
            swal({
                type: 'success',
                title: '投票完成',
                showConfirmButton: false,
                timer: 3000
            });
        @endif
        
        $('.racer-next').click(function(){
            $(this).closest('.rider').find('.rider-date .owl-carousel-gp-racer-data .owl-nav .owl-next').click();
        });
        $('.racer-return').click(function(){
            $(this).closest('.rider').find('.rider-date .owl-carousel-gp-racer-data .owl-nav .owl-prev').click();
        });
        {{--@if($is_phone)--}}
            {{--$(window).scroll(function(){--}}
                {{--if($(this).scrollTop() > parseInt($('#submit-button').offset().top) && $('.motogp-container .game-choose .racer').hasClass('number1')){--}}
                    {{--$('#mobile-choice').css("cssText",'display:block !important');--}}
                {{--}else{--}}
                    {{--$('#mobile-choice').css("cssText",'display:none !important');--}}
                {{--}--}}
            {{--});--}}
        {{--@endif--}}
         $(document).ready(function(){
            if($(window).width() <= 321){
                $('.game-choose').css('height',$('.gp-gmae .game-choose .racer').height()*3+10);
            }else if($(window).width() <= 550){
                $('.game-choose').css('height',$('.gp-gmae .game-choose .racer').height()*4+10);
            }else if($(window).width() < 769){
                $('.col-md-1').removeClass('col-md-1');
                $('.game-choose').css('height',$('.gp-gmae .game-choose .racer').height()*4+10);
            }
        });
        {{--$(window).scroll(function(){--}}
            {{--@if($is_phone)--}}
                {{--if($(this).scrollTop() > parseInt($('#submit-button').offset().top)){--}}
                    {{--$('#mobile-choice').css("cssText",'display:block !important');--}}
                {{--}else{--}}
                    {{--$('#mobile-choice').css("cssText",'display:none !important');--}}
                {{--}--}}
            {{--@endif--}}
        {{--});--}}
        @if(\Auth::check() and !isset($customer_choices->one[$now_speedway->id]))
            $('.motogp-container .game-choose .racer').click(function(){
                var active = $(this).hasClass('active');
                var number1_img = $('#number1').attr('src');
                var number2_img = $('#number2').attr('src');
                var number3_img = $('#number3').attr('src');
                var choose_number1 = $('.motogp-container .game-choose .racer').hasClass('number1');
                var choose_number2 = $('.motogp-container .game-choose .racer').hasClass('number2');
                var choose_number3 = $('.motogp-container .game-choose .racer').hasClass('number3');
                var number1 = $('.motogp-container .game-choose .number1');
                var number2 = $('.motogp-container .game-choose .number2');
                var number3 = $('.motogp-container .game-choose .number3');
                var _this = $(this);
                if(!choose_number3){
                    _this.find('.before_img').css("cssText",'display:none !important');
                    _this.find('.after_img').css("cssText",'display:inline !important');
                }

                if(choose_number1 && choose_number2 && !choose_number3 && !active){
                    _this.find('.racer-img img').attr('src',number3_img);
                    _this.find('.racer-img').show();
                    _this.addClass('number3').addClass('active');
                    $('#choose_number_three').val(_this.attr('id'));
//                    $('.cursor-pointer').find('.lock_img').css("cssText",'display:inline !important');
                    $('.cursor-pointer').each(function(){
                        if(!$(this).closest('.racer').hasClass('active')){
                            $(this).find('.before_img').css("cssText",'display:none !important');
                            $(this).find('.lock_img').css("cssText",'display:inline !important');
                            $(this).closest('.racer').css('padding-bottom','22px').find('p').hide();
                        }
                    });
                    $('.mobile-number3-img').css("cssText",'display:block !important');
                }else if(choose_number1 && !choose_number3 && !active){
                    _this.find('.racer-img img').attr('src',number2_img);
                    _this.find('.racer-img').show();
                    _this.addClass('number2').addClass('active');
                    $('#choose_number_two').val(_this.attr('id'));
                    $('.mobile-number2-img').css("cssText",'display:block !important');
                }else if(!choose_number3 && !active){
                    _this.find('.racer-img img').attr('src',number1_img);
                    _this.find('.racer-img').show();
                    _this.addClass('number1').addClass('active');
                    $('#choose_number_one').val(_this.attr('id'));
                    $('.mobile-number1-img').css("cssText",'display:block !important');
                    $('#mobile-choice').css("cssText",'display:block !important');
                }
                var game_choose = $('.motogp-container .game-choose');

                $('.gp-gmae .gp-choose-center img').attr('src',game_choose.find('.number1 .cursor-pointer .after_img').attr('src'));
                $('.gp-gmae .gp-choose-left img').attr('src',game_choose.find('.number2 .cursor-pointer .after_img').attr('src'));
                $('.gp-gmae .gp-choose-right img').attr('src',game_choose.find('.number3 .cursor-pointer .after_img').attr('src'));
                $('.mobile-choice .mobile-number1 .mobile-number1-racer').attr('src',game_choose.find('.number1 .cursor-pointer .after_img').attr('src'));
                $('.mobile-choice .mobile-number2 .mobile-number2-racer').attr('src',game_choose.find('.number2 .cursor-pointer .after_img').attr('src'));
                $('.mobile-choice .mobile-number3 .mobile-number3-racer').attr('src',game_choose.find('.number3 .cursor-pointer .after_img').attr('src'));
            });

            $('.clear-all').click(function(){
                $('#mobile-choice').css("cssText",'display:none !important');
                clearAll();
            });

            function clearAll()
            {
                $('.cursor-pointer').each(function(){
                    $(this).find('.before_img').css("cssText",'display:inline !important');
                    $(this).find('.lock_img').css("cssText",'display:none !important');
                    $(this).closest('.racer').css('padding-bottom','0px').find('p').show();
                });
                var number1 = $('.motogp-container .game-choose .number1');
                var number2 = $('.motogp-container .game-choose .number2');
                var number3 = $('.motogp-container .game-choose .number3');
                $('#choose_number_one').val('');
                $('#choose_number_two').val('');
                $('#choose_number_three').val('');
                $('.gp-gmae .gp-choose-left img').attr('src','');
                $('.gp-gmae .gp-choose-right img').attr('src','');
                $('.gp-gmae .gp-choose-center img').attr('src',"{{ assetRemote('image/benefit/event/motogp/2018/choose/default.png') }}");
                $('.mobile-choice .mobile-number1 .mobile-number1-racer').attr('src','');
                $('.mobile-choice .mobile-number2 .mobile-number2-racer').attr('src','');
                $('.mobile-choice .mobile-number3 .mobile-number3-racer').attr('src','');
                $('.mobile-number1-img').css("cssText",'display:none !important');
                $('.mobile-number2-img').css("cssText",'display:none !important');
                $('.mobile-number3-img').css("cssText",'display:none !important');
                $('.mobile-choice').css("cssText",'display:none !important');
                number1.find('.after_img').css("cssText",'display:none !important');
                number1.find('.before_img').css("cssText",'display:inline !important');
                number2.find('.after_img').css("cssText",'display:none !important');
                number2.find('.before_img').css("cssText",'display:inline !important');
                number3.find('.after_img').css("cssText",'display:none !important');
                number3.find('.before_img').css("cssText",'display:inline !important');
                number1.removeClass('active').removeClass('number1').find('.racer-img img').attr('src','');
                number2.removeClass('active').removeClass('number2').find('.racer-img img').attr('src','');
                number3.removeClass('active').removeClass('number3').find('.racer-img img').attr('src','');
            }

            $('.game-computer').click(function(){
                clearAll();
                $('.gp-link').hide();
                $('.submit-content .gp-btn').hide();
                $('.submit-content .random-ing').css("cssText",'display:block !important');

                $.ajax({
                    url: "{!! route('benefit-event-motogp-2018-rate-choose') !!}",
                    data: {_token: $('meta[name=csrf-token]').prop('content')},
                    type: 'POST',
                    dataType: 'json',
                    success: function(result){
                        $('.gp-link').show();
                        $('.submit-content .gp-btn').css("cssText",'display:block !important');
                        $('.submit-content .random-ing').hide();
                        $('.game-choose .active').find('.before_img').css("cssText",'display:inline !important');
                        $('.game-choose .active').find('.after_img').css("cssText",'display:none !important');
                        $('.motogp-container .game-choose .number1').removeClass('number1').removeClass('active').find('.racer-img').hide();
                        $('.motogp-container .game-choose .number2').removeClass('number2').removeClass('active').find('.racer-img').hide();
                        $('.motogp-container .game-choose .number3').removeClass('number3').removeClass('active').find('.racer-img').hide();
                        $('.mobile-number1-img').css("cssText",'display:block !important');
                        $('.mobile-number2-img').css("cssText",'display:block !important');
                        $('.mobile-number3-img').css("cssText",'display:block !important');
                        $.each(result,function(key,val){
                            var id = val.id;
                            $('#' + id).find('.before_img').css("cssText",'display:none !important');
                            $('#' + id).find('.after_img').css("cssText",'display:inline !important');
                            $('#' + id).find('.racer-img').show();
                            if(key == 0){
                                $('.gp-gmae .gp-choose-center img').attr('src',val.img);
                                $('.mobile-choice .mobile-number1 .mobile-number1-racer').attr('src',val.img);
                                $('#' + id).addClass('number1').addClass('active').find('.racer-img img').attr('src',$('#number1').attr('src'));
                                $('#choose_number_one').val(id);
                            }else if(key == 1){
                                $('.gp-gmae .gp-choose-left img').attr('src',val.img);
                                $('.mobile-choice .mobile-number2 .mobile-number2-racer').attr('src',val.img);
                                $('#' + id).addClass('number2').addClass('active').find('.racer-img img').attr('src',$('#number2').attr('src'));
                                $('#choose_number_two').val(id);
                            }else{
                                $('.gp-gmae .gp-choose-right img').attr('src',val.img);
                                $('.mobile-choice .mobile-number3 .mobile-number3-racer').attr('src',val.img);
                                $('#' + id).addClass('number3').addClass('active').find('.racer-img img').attr('src',$('#number3').attr('src'));
                                $('#choose_number_three').val(id);
                            }
                        });
                        $('#mobile-choice').css("cssText",'display:block !important');
                        $('.cursor-pointer').each(function(){
                            if(!$(this).closest('.racer').hasClass('active')){
                                $(this).find('.before_img').css("cssText",'display:none !important');
                                $(this).find('.lock_img').css("cssText",'display:inline !important');
                                $(this).closest('.racer').css('padding-bottom','22px').find('p').hide();
                            }
                        });
                    },
                    error: function(xhr, ajaxOption, thrownError){
                    }
                });
            });

        @endif
        $('.ridertop .owl-carousel-gp-score-rank').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            margin:5,
            slideBy : 4,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
        $('.teamtop .owl-carousel-gp-score-rank').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            margin:5,
            slideBy : 4,
            mouseDrag : false,
            touchDrag : false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
        $('.owl-carousel-gp-racer-data').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            margin:5,
            slideBy : 4,
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:4
                },
                1000:{
                    items:8
                }
            }
        });
        $('.owl-carousel').find('.owl-prev').html('<div class=""><i class="glyphicon glyphicon-triangle-left size-08rem" aria-hidden="true"></i></i></div>');
        $('.owl-carousel').find('.owl-next').html('<div class=""><i class="glyphicon glyphicon-triangle-right size-08rem" aria-hidden="true"></i></i></div>');
        $('.owl-carousel2').find('.owl-prev').addClass('btn-owl-prev');
        $('.owl-carousel2').find('.owl-next').addClass('btn-owl-next');
        $('.rider-date .owl-nav').hide();

        function slipTo1(element){
            var y = parseInt($(element).offset().top) - $('#mainNav .container-header').height() - $('.motogp-menu').height() -5;
            $('html,body').animate({scrollTop: y}, 400);
        }
        $('.team-container .teamtop .owl-nav .owl-prev').click(function(){
            $('.teamtop h2').text('TOP5 車隊積分排行');
            $('.team-container .teamtop .owl-nav .owl-next').show();
            $('.team-container .teamtop .owl-nav .owl-prev').hide();
        });
        $('.team-container .teamtop .owl-nav .owl-next').click(function(){
            $('.teamtop h2').text('TOP5 車廠積分排行');
            $('.team-container .teamtop .owl-nav .owl-prev').show();
            $('.team-container .teamtop .owl-nav .owl-next').hide();
        });
    </script>
@stop