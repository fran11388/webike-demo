{{--<div class="station-container container-buttom-border">--}}
    {{--<div class="substation station-1">--}}
        {{--<table>--}}

@if(isset($customer))
    <div class="gp-box-content clearfix col-xs-block col-sm-block container-buttom-border" style="position:relative;" id="reward-record">
        <div class="out-width">
            <div class="text-center gp-title">
                <h2>2018 MotoGP 預測履歷</h2>
            </div>
            <div class="rankcontainer">
                <div class="info-table">
                    <div class="clearfix hidden-xs" id="pchistory">
                        <table class="table table-bordered text-center" style="background-color: white;margin-bottom:20px">
                            <?php
                            $ranks = array('title','第一名','第二名','第三名');
                            ?>
                            @foreach($ranks as $rank)
                                <tr>
                                    @if($rank == 'title')
                                        @for($i = 0; $i <= 10 ; $i++)
                                            @if($i==0)
                                                <th> </th>
                                            @else
                                                <th class="text-center font-bold">第{{$i}}站</th>
                                            @endif
                                        @endfor
                                    @else
                                        @for($i = 0; $i <= 10 ; $i++)
                                            @if($i==0)
                                                <td class="font-bold">{{$rank}}</td>
                                            @elseif($rank == '第一名' && array_key_exists($i, $speedway_choices->one))
                                                @if(isset($speedway_real_choices->one[$i][0]) )
                                                    @if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
                                                        <?php $times[25] += 1; ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 25 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->one[$i][0]))
                                                    <td>{{ $speedway_choices->one[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @elseif($rank == '第二名' && array_key_exists($i, $speedway_choices->two))
                                                @if(isset($speedway_real_choices->two[$i][0]) )
                                                    @if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
                                                        <?php $times[20] += 1; ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 20 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->two[$i][0]))
                                                    <td>{{ $speedway_choices->two[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @elseif($rank == '第三名' && array_key_exists($i, $speedway_choices->three))
                                                @if(isset($speedway_real_choices->three[$i][0]) )
                                                    @if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
                                                        <?php $times[16] += 1; ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 16 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->three[$i][0]))
                                                    <td>{{ $speedway_choices->three[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @else
                                                <td class="not-join">未參加</td>
                                            @endif
                                        @endfor
                                    @endif
                                </tr>
                            @endforeach
                        </table>
                        <table class="table table-bordered text-center" style="background-color: white">
                            @foreach($ranks as $rank)
                                <tr>
                                    @if($rank == 'title')
                                        @for($i = 10; $i <= 19 ; $i++)
                                            @if($i==10)
                                                <th> </th>
                                            @else
                                                <th class="text-center font-bold">第{{$i}}站</th>
                                            @endif
                                        @endfor
                                        <th class="text-center font-bold">累計答對數</th>
                                    @else
                                        @for($i = 10; $i <= 19 ; $i++)
                                            @if($i==10)
                                                <td class="font-bold">{{$rank}}</td>
                                            @elseif($rank == '第一名' && array_key_exists($i, $speedway_choices->one))
                                                @if($i == 12 && $speedway_choices->one[$i][0])
                                                    <td>賽事取消</td>
                                                @elseif(isset($speedway_real_choices->one[$i][0]))
                                                    @if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
                                                        <?php $times[25] += 1; ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 25 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->one[$i][0]))
                                                    <td>{{ $speedway_choices->one[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @elseif($rank == '第二名' && array_key_exists($i, $speedway_choices->two))
                                                @if($i == 12 && $speedway_choices->two[$i][0])
                                                    <td>賽事取消</td>
                                                @elseif(isset($speedway_real_choices->two[$i][0]) )
                                                    @if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
                                                        <?php $times[20] += 1; ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 20 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->two[$i][0]))
                                                    <td>{{ $speedway_choices->two[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @elseif($rank == '第三名' && array_key_exists($i, $speedway_choices->three))
                                                @if($i == 12 && $speedway_choices->three[$i][0] )
                                                    <td>賽事取消</td>
                                                @elseif(isset($speedway_real_choices->three[$i][0]) )
                                                    @if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
                                                        <?php $times[16] += 1; ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 16 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->three[$i][0]))
                                                    <td>{{ $speedway_choices->three[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @else
                                                <td class="not-join">未參加</td>
                                            @endif
                                        @endfor
                                        @if($rank == '第一名')
                                            <td>{{$times[25]}}</td>
                                        @elseif($rank == '第二名')
                                            <td>{{$times[20]}}</td>
                                        @else
                                            <td>{{$times[16]}}</td>
                                        @endif
                                    @endif
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="clearfix hidden-sm hidden-md visible-xs block" id="mobilehistory" style="background-color: white">
                        <table class="table table-bordered text-center" >
                            <?php
                            $ranks = array('title','第一名','第二名','第三名');
                            $times = array(25 => 0, 20 => 0, 16 => 0);
                            $joins = 0;
                            $points = 0;
                            ?>
                            @foreach($ranks as $rank)
                                <tr>
                                    @if($rank == 'title')
                                        @for($i = 0; $i <= 19 ; $i++)
                                            @if($i==0)
                                                <th> </th>
                                            @else
                                                <th class="text-center">第{{$i}}站</th>
                                            @endif
                                        @endfor
                                        <th class="text-center">答對</th>
                                    @else
                                        @for($i = 0; $i <= 19 ; $i++)
                                            @if($i==0)
                                                <td>{{$rank}}</td>
                                            @elseif($rank == '第一名' && array_key_exists($i, $speedway_choices->one))
                                                @if($i == 12 && $speedway_choices->one[$i][0])
                                                    <td>賽事取消</td>
                                                @elseif(isset($speedway_real_choices->one[$i][0]) )
                                                    <?php $joins++; ?>
                                                    {{--{{$speedway_real_choices->one[$i][0]}}<br>--}}
                                                    {{--{{$speedway_choices->one[$i][0]}}<br>--}}
                                                    {{--{{$speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0]}}<br>--}}
                                                    @if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
                                                        <?php
                                                        $times[25] += 1;
                                                        $points += 25;
                                                        ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 25 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->one[$i][0]))
                                                    <?php $joins++; ?>
                                                    <td>{{ $speedway_choices->one[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @elseif($rank == '第二名' && array_key_exists($i, $speedway_choices->two))
                                                @if($i == 12 && $speedway_choices->two[$i][0])
                                                    <td>賽事取消</td>
                                                @elseif(isset($speedway_real_choices->two[$i][0]) )
                                                    @if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
                                                        <?php
                                                        $times[20] += 1;
                                                        $points += 20;
                                                        ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 20 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->two[$i][0]))
                                                    <td>{{ $speedway_choices->two[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @elseif($rank == '第三名' && array_key_exists($i, $speedway_choices->three))
                                                @if($i == 12 && $speedway_choices->three[$i][0])
                                                    <td>賽事取消</td>
                                                @elseif(isset($speedway_real_choices->three[$i][0]) )
                                                    @if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
                                                        <?php
                                                        $times[16] += 1;
                                                        $points += 16;
                                                        ?>
                                                        <td class="text-center font-color-red"><span>恭喜答對</span><br><span>獲得 16 點</span></td>
                                                    @else
                                                        <td>X</td>
                                                    @endif
                                                @elseif(isset($speedway_choices->three[$i][0]))
                                                    <td>{{ $speedway_choices->three[$i][0] }}</td>
                                                @else
                                                    <td class="not-join">未參加</td>
                                                @endif
                                            @else
                                                <td class="not-join">未參加</td>
                                            @endif
                                        @endfor
                                        @if($rank == '第一名')
                                            <td>{{$times[25]}}</td>
                                        @elseif($rank == '第二名')
                                            <td>{{$times[20]}}</td>
                                        @else
                                            <td>{{$times[16]}}</td>
                                        @endif
                                    @endif
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="text-right">
                        <p class="font-color-red size-10rem font-bold">參加站數：{{ $joins }} 站</p>
                        <p class="font-color-red size-10rem font-bold">目前獲得點數：{{ $points_current }} 點</p>
                    </div>
                </div>
            </div>
            @if(count($customer_choose_racers_score))
                <div class="clearfix prediction">
                    <div class="prediction-ranking">
                        <div class="box">
                            <h2 class="text-left">您目前積分預測的排名</h2>
                            @foreach($customer_choose_racers_score as $racer)
                                @php
                                    $racers_score['number1'] = $customer_choose_racers_score[0];
                                    $racers_score['number2'] = $customer_choose_racers_score[1];
                                    $racers_score['number3'] = $customer_choose_racers_score[2];
                                @endphp
                                <div class="row text-left">
                                    <span class="size-10rem col-md-9 col-sm-12 col-xs-9">{{ $racer->racer->number }} {{ $racer->racer->name }}</span>
                                    <span class="size-10rem col-md-3 col-sm-12 col-xs-3">{{ $racer->score }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="prediction-podium">
                        <div class="prediction-podium-left">
                            <img  src="{{assetRemote('image/benefit/event/motogp/2018/choose/after').'/'.$racers_score['number2']->racer->number.'.png'}}" alt="{{ '2018 MotoGP 【'.$racers_score['number2']->racer->number.'】 '.$racers_score['number2']->racer->name }}">
                        </div>
                        <div class="prediction-podium-center text-center box">
                            <img  src="{{assetRemote('image/benefit/event/motogp/2018/choose/after').'/'.$racers_score['number1']->racer->number.'.png'}}" alt="{{ '2018 MotoGP 【'.$racers_score['number1']->racer->number.'】 '.$racers_score['number1']->racer->name }}">
                        </div>
                        <div class="prediction-podium-right">
                            <img  src="{{assetRemote('image/benefit/event/motogp/2018/choose/after').'/'.$racers_score['number3']->racer->number.'.png'}}" alt="{{ '2018 MotoGP 【'.$racers_score['number3']->racer->number.'】 '.$racers_score['number3']->racer->name }}">
                        </div>
                        <div class="text-center">
                            <img src="{{assetRemote('image/benefit/event/motogp/2018/goldpodium.png')}}">
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endif