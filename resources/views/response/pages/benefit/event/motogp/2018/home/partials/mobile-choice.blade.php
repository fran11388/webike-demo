<style>
    .mobile-choice{
        background-color:rgba(255, 255, 255, 0.9);
        position:fixed;
        bottom:0px;
        height:85px;
        z-index:999;
        font-size:0px;
    }
    .mobile-choice .mobile-submit{
        height:65px;
        padding:5px;
    }
    .mobile-choice .relative{
        position:relative;
        width:25%;
        padding:0 15px;
    }
    .mobile-choice .mobile-rank {
        position:absolute;
        right:3%;
        bottom:2%;
        width:82%;
    }
    .vertical-align-middle{
        display:inline-block;
        vertical-align: middle;
    }
    /*.mobile-choice .mobile-submit {*/
        /*clear: both;*/
    /*}*/
</style>
<div class="mobile-choice clearfix width-full hidden" id="mobile-choice">
    <span class="helper"></span>
    <div class="mobile-number1 relative vertical-align-middle">
        <img class="hidden mobile-number1-img mobile-rank" src="{{ assetRemote('image/benefit/event/motogp/2018/choose/number1.png') }}">
        <img class="mobile-number1-racer mobile-racer">
    </div>
    <div class="mobile-number2 relative vertical-align-middle">
        <img class="hidden mobile-number2-img mobile-rank" src="{{ assetRemote('image/benefit/event/motogp/2018/choose/number2.png') }}">
        <img class="mobile-number2-racer mobile-racer">
    </div>
    <div class="mobile-number3 relative vertical-align-middle">
        <img class="hidden mobile-number3-img mobile-rank" src="{{ assetRemote('image/benefit/event/motogp/2018/choose/number3.png') }}">
        <img class="mobile-number3-racer mobile-racer">
    </div>
    <div class="mobile-submit game-ok text-center vertical-align-middle">
        <span class="helper"></span>
        <a class="btn btn-danger border-radius-2 vertical-align-middle" href="javascript:void(0)">
            送出
        </a>
    </div>
</div>