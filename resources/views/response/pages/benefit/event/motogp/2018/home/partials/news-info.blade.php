<style>
    .news-container .news-image img{
        width:270px;
        height: 141.33px;
    }
    .news-container .news-image{
        width:270px;
        height: 141.33px;
    }
    #news-info {
        border-bottom: 0px;
    }
    .main-news-image img {
        width: 100%;
        height: 100%;
    }
    .main-news div {
        height: 375.33px;
    }
    @media(max-width:550px){
        .main-news div {
            height: 100%;
        }
    }
    @media(max-width:330px){
      #news-info .out-width .news-link div{
        float: none!important;
        text-align: center!important;
        width: auto;
      }
      #news-info .out-width .news-link .motogp-news{
        margin-top: 10px; 
      }
      #news-info .out-width .news-link .motogp-news a{
        width: 61.5%;
      }
    }

</style>
@if(count($stationNews))
    <div class="rider container-buttom-border" id="news-info">
        <div class="out-width">
            <div class="text-center block gp-title">
                <h2>2018 MotoGP 最新消息</h2>
            </div>
            <div class="news-container clear row block">
                <div class="col-md-6 col-sm-6 col-md-12 main-news">
                    <span class="helper"></span>
                    <a href="{{ $stationNews[0] ? $stationNews[0]->guid : '' }}">
                        <div class="main-news-image box vertical-align-middle">
                            <div class="box  text-center">
                                <img src="{{ $stationNewsimgs ? $stationNewsimgs[0] : '' }}">
                            </div>
                            <h2 class="font-bold text-center title box">{{ $stationNews[0]->post_title }}</h2>
                            <span>{!! $stationNewsContents ? $stationNewsContents[0] : '' !!}</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 col-md-12 clearfix news">
                    @foreach($stationNews as $key => $stationNew)
                        @if($key != 0)
                            <div class="col-md-6 col-sm-6 col-md-12 box">
                                <a href="{{ $stationNew->guid }}">
                                    <div class="news-image box text-center">
                                        <img src="{{ $stationNewsimgs ? $stationNewsimgs[$key] : '' }}">
                                    </div>
                                </a>
                                <div class="news-content">
                                    <a href="{{ $stationNew->guid }}">
                                        <h3 class="font-bold box title">{{ $stationNew->post_title }}</h3>
                                    </a>
                                    <span>{!! $stationNewsContents ? $stationNewsContents[$key] : '' !!}</span>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="text-center clearfix news-link">
                <div class="col-sm-6 col-md-6 col-xs-6 text-right ">
                    <a class="btn btn-warning" href="https://www.webike.tw/bikenews/category/racing/" target="_blank">更多 MotoGP 新聞</a>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-6 text-left motogp-news">
                    <a class="btn btn-info" href="{{ \URL::route('benefit') }}" target="_blank">更多會員好康</a>
                </div>
            </div>
        </div>
    </div>
@endif