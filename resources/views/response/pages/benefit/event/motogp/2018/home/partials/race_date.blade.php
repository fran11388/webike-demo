<div class="rider container-buttom-border" id="race_date">
    <div class="out-width">
        <div class="text-center gp-title">
            <h2>2018 MotoGP 賽程表</h2>
        </div>
        <div class="race-table">
            <table class="table table-bordered race_date" style="background-color: white;margin-bottom:0px">
                <thead>
                <tr>
                    <th class="text-center font-bold">站次</th>
                    <th class="text-center font-bold">正賽時間</th>
                    <th class="text-center font-bold">比賽站名</th>
                    <th class="text-center font-bold">比賽賽道</th>
                </tr>
                </thead>
                <tbody>
                @foreach($speedwaies as $key => $speedway)
                    @php
                        $key++;
                    @endphp
                    <tr>
                        <td class="text-center">第 {{ $key }} 站</td>
                        <td class="text-center">{{ date('m 月 d 日',strtotime(date($speedway->raced_at_local))) }}</td>
                        <td class="text-center"><a href="{{ \URL::route('benefit-event-motogp-2018detail',['station' => $speedway->station_name]) }}" target="_blank">{{ $speedway->station_name  }}</a></td>
                        <td>{{ $speedway->speedway_name.' ('.$speedway->speedway_name_en.')' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
