@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{assetRemote('css/benefit/event/motogpevent.css')}}">
	<style>
		.short-description{
			border: 4px solid #dcdee3;
    		margin: 20px 0px;
		}
		#selfranking ul {
			margin-bottom:10px;
		}
		#selfranking table{
			margin-top: 0px;
		    float: left;
		    width: 10%;
		    background: white;
		}
		#mobilehistory{
			display:none;
		}
		#pchistory table{
	        width:100%;
	    }
	    form .drop-site-activity-forecast .box-fix-column {
			padding:37px 10px !important;
		}
		@media screen and (max-width: 767px) {
		    #pchistory{
		        display:none;
		    }
		    #mobilehistory{
				display:block;
				width:100%;
				white-space:nowrap;
				overflow-x: auto;
		    }
		    .container-moto-gp-event .ul-ct-site-activity-forecast {
				display:block;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-left {
				padding-right: 0px;
				margin-bottom: 14px;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .drop-site-activity-forecast .box-fix-column{
				float:none;
				width:100%;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .note {
				position:static;
				text-align:center;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .note span{
				float:none;
				display:block;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .note a{
				float:none;
			}
			.ct-item-ct-your-scoreboard-left .right{
				padding:0px;
			}
			.ul-ct-your-scoreboard .item-ct-your-scoreboard-right .ul-table-info-moto-gp .border-radius-2 {
				margin-top:10px !important;
				float:none !important;
			}
			.ul-ct-your-scoreboard .item-ct-your-scoreboard-right .table-info-moto-gp{
				text-align:center;
			}
			.ul-ct-your-scoreboard .item-ct-your-scoreboard-right li{
				margin-top: 10px;
			}
			#chart {
				overflow-x: auto
			}
			#makerBarChart {
				overflow-x: auto
			}
			#teamBarChart {
				overflow-x: auto
			}
		}
		.raceafter {
		    border: 1px solid black;
		    padding: 11px;
		}
		.topthree span{
			white-space: initial;
			padding: 4px 12px;
		    width: 10px;
		    line-height: 16px;
		    color: #000;
		    font-weight: bold;
		    display: block;
		    font-size: 12px;
		    white-space: initial;
		    padding: 4px 24px 4px 13px;
		    margin-bottom: 20px;
		}
		.topthree label{
			margin-top: 10px;
		}
		.topthree {
			margin-top:20px;
		}
		.topthree .num1{
			background: #FFAA00;
		}
		.topthree .num2{
			background: #E6E6E6;
		}
		.topthree .num3{
			background: #F26946;
		}
		.select2-results .select2-results__option {
			font-size:5px;
		}
		iframe html body ._li{
			display:none !important;
		}
		.uiScaledImageContainer {
			width:100% !important;
		}

        .right-side-box{
            padding-right: 5%;
        }
        .left-side-box{
            padding-left: 5%;
        }
        .gp-box{
            margin-top: 40px;
            border: 2px #777 solid;
        }
        .gp-col{
            display: flex;
            justify-content: center;
            flex-direction: column;
            padding: 3% 2%;
        }
        .gp-box-content{
            display: flex;
            background: white;
        }
        .gp-box-content h3{
            font-weight: bold;
            font-size: 1.2rem;
        }
        .gp-box-content ul li{
            text-decoration: none;
            list-style: none;
        }
        .gp-box-title h2{
            position: relative;
        }
        .gp-box-title h2 span{
            color: #fff;
            position: absolute;
            left: 2%;
            line-height: 30px;
            font-size: 1.2rem;
            font-weight: bold;
        }
        .gp-box-title h2 img{
            width:25%;
        }
        .gp-box-border{
            border:1px #ccc solid;
        }
        .box-green{
            background: rgba(0, 176, 0, 0.8);
        }
        .box-green1{
            background: rgba(0, 176, 0, 0.8);
        }
        .box-white{
            background: #fff;
        }
        .btn-winner {
			border: 2px solid #e61e25;
			color: #e61e25;
			width: 100%;
        }
        .btn-winner:hover {
        	background-color: #e61e25;
            color: #fff;
        }
        .gp-col .box-white .result{
        	float:none;
        }
        .gp-col .right-side-box .box-white li {
        	display:block;
        	clear:both;
        	margin:2%;
        }
        .gp-col .right-side-box .box-white .border-radius-2{
			text-align: center;
			padding: 5px 10px;
			width: 40%;
			border: 2px solid #333333;
			font-weight: bold;
			text-transform: uppercase;
			min-height: 35px;
			float:left;
        }
        .gp-col .right-side-box .box-white .num-percent {
        	width: 20%;
		    line-height: 35px;
		    float: left;
		    text-align: center;
        }
        .gp-col .right-side-box .box-white .percent {
        	margin: 12px 0;
		    width: 40%;
		    height: 10px;
		    background-color: #efefef;
		    float: left;
        }
        .gp-col .right-side-box .box-white .ct-percent {
		    height: 10px;
		    background-color: #ff6600;
		    float: left;
    	}
    	.box-light-blue{
            background: rgba(0, 155, 238, 0.8);
        }
        .box-light-blue1{
            background: rgba(0, 155, 238, 0.8);
        }
        .box-light-red{
        	background:rgba(225, 17, 17 ,1);
        }
        .box-light-red1{
        	background:rgba(225, 17, 17 ,1);
        }
        .box-light-red2{
			background:rgba(255, 120, 0, 0.87);
        }
        .box-white{
            background: #fff;
        }
        .nail-box{
            position: relative;
        }
        .fly-box{
            position: absolute;
            width:450px;
        }
        .box-tall{
            height:100%;
        }
        .nail-left{
            left:10%;
        }
        .nail-right {
            right:10%;
        }
        .nail-top {
            top: 8.5%;
        }
        /*.nail-bottom {
            bottom: 8.5%;
        }*/
        .gp-box .line {
        	height:5px;
        	float:none;
        }
        .box-white .title-site-activity-forecast-right{
        	font-size: 1.2rem;
		    padding: 5px;
		    width: 100%;
		    background-color: #0066c0;
		    color: #fff;
		    text-align: center;
        }
        .box-white .note{
		    width: 100%;
		    padding: 10px;
		    float: left;
		    text-align:center;
		    background-color: #efefef;
		    bottom: 0;
        }
		.nail-box .info {
			float:right;
		}
		.gp-box-content .info-table {
			position: absolute;
		    width: 100%;
		    z-index: 2;
		}
		#myNavbar li {
			padding:0;
			height:100%;
			min-width:0;
		}
		#myNavbar li a{
			height:100%;
			padding-top:9px;
		}
		#myNavbar li span{
			width:100%;
			font-weight: bold;
		}
		#myNavbar ul {
			height:40px;
		}
		.game .title-site-activity-forecast-right{
			font-weight: bold;
			background: rgba(225, 17, 17 ,1);
		}
		.game .ct-site-activity-forecast {
			padding:2%;
		}
		.row span {
			font-size: 1rem;
		}
		.note1 {
			border: 1px solid #ddd;
			width:99.9%;
			background: white;
			padding:2%;
		}
		.info-table table{
			margin-bottom:0px;
		}
		.allteam li span {
			padding:0px !important;
			margin-bottom:0px !important;
		}
		.allteam li {
			padding:7px 0px;
		}
		.allteamresult li{
			padding:0px;
		}
		.row {
			margin-bottom:40px;
		}
		#myNavbar li span {
			font-size:1.2rem;
		}
	</style>
@stop
@section('middle')
	<?php
		$photo_home = 'http://img.webike.tw/assets/images/motogp/2017/';
	?>
	<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <img src="{{assetRemote('image/benefit/event/motogp/banner1.png')}}" alt="">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div id="myNavbar" class="menu-brand-top-page">
                <div class="ct-menu-brand-top-page2" style="margin:0px 0px 40px 0px">
                    <ul class="ul-menu-brand-top-page ul-menu-motor-top-page" style="width: 100% !important;background: #202732;display: inline-block;padding: 0px;">
                    	@if(count($top10_score) > 0)
							<?php
							$sizeb = 1;
							$sizea =2;
							?>
						@else
							<?php
							$sizeb = 2;
							$sizea =2;
							?>
                    	@endif
                    	<li class="col-md-{{$sizeb}} col-sm-12 col-xs-12"></li>
                        <li class="col-md-{{$sizea}} col-sm-12 col-xs-12">
                        	<span>
                        		<a href="javascript:void(0)" title="本站活動預測 - 「Webike-摩托百貨」" onclick="slipTo1('#site-prediction')">本站活動預測</a>
                        	</span>
                        </li>
                        @if(count($top10_score) > 0)
	                        <li class="col-md-{{$sizea}} col-sm-12 col-xs-12">
	                        	<span>
	                            	<a href="javascript:void(0)" title="本站活動預測 - 「Webike-摩托百貨」" onclick="slipTo1('#points-ranking')">目前積分排行</a>
	                            </span>
	                        </li>
                        @endif
                        <li class="col-md-{{$sizea}} col-sm-12 col-xs-12">
                        	<span>
                            	<a href="javascript:void(0)" title="本站活動預測 - 「Webike-摩托百貨」" onclick="slipTo1('#driver-information')">車手資訊成績</a>
                            </span>
                        </li>
                        <li class="col-md-{{$sizea}} col-sm-12 col-xs-12">
                        	<span>
                            	<a href="javascript:void(0)" title="本站活動預測 - 「Webike-摩托百貨」" onclick="slipTo1('#schedule')">賽程表</a>
                            </span>
                        </li>
                        <li class="col-md-{{$sizea}} col-sm-12 col-xs-12">
                        	<span>
                            	<a href="javascript:void(0)" title="本站活動預測 - 「Webike-摩托百貨」" onclick="slipTo1('#event-discuss')">活動討論區</a>
                            </span>
                        </li>
						<li class="col-md-{{$sizeb}} col-sm-12 col-xs-12"></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        	<a href="{{URL::route('benefit-event-motogp-2017info')}}" title="2017MotoGP活動頁面說明" target="_blank">
            	<img src="{{assetRemote('image/benefit/event/motogp/banner2.png')}}" alt="">
            </a>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="site-prediction">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span style="float:left;padding:0 23px;position:static;background:black">本站活動預測</span>
                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-red1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                	<div class="col-md-6 col-sm-6 col-xs-12 nail-box no-padding addict">
                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col box-tall box-light-red">
                            <div class="fly-box nail-left nail-bottom" style="left:25%">
                                <div>
									@if(count($now_speedway) != 0)
										<a href="{{URL::route('benefit-event-motogp-2017detail', $now_speedway->station_name)}}" title="{{$now_speedway->station_name.$tail}}" target="_blank">
											<div style="position:absolute;width:40%;height:99.8%;padding:5%;top:0px" class="box-light-blue visible-md visible-lg">
												<img src="{{assetRemote('image/benefit/event/motogp/speedway/'.$now_speedway->name.'.jpg')}}" alt="【2017{{$now_speedway->name}}】台灣正賽時間{{$now_speedway->raced_at_local.$tail}}"><br>
												@if($now_speedway->active == 2)
													<ul style="color:white" class="allteam allteamresult">
								                		<li style="padding:4px 0px">
								            				<h3>{{$now_speedway->name}}</h3>
								            			</li>
								            			<li>
									                    	<span style="color:white;font-weight:bold;">
									                    		比賽結果:
									                    	</span>
								                        </li>
								                    	@if(is_null($now_speedway->numberOne) && is_null($now_speedway->numberTwo) && is_null($now_speedway->numberThree))
								                    		<li>
							                        			<span style="color:white;font-weight:bold">
								                    				第1名：
								                    			</span>
							                        		</li>
							                        		<li>
							                        			<span style="color:white;font-weight:bold">
										                    		第2名：
										                    	</span>
							                        		</li>
							                        		<li>
							                        			<span style="color:white;font-weight:bold">
										                   			第3名：
										                   		</span>
							                        		</li>
										                @else
										                	<li>
							                        			<span style="color:white;font-weight:bold">
								                    				第1名：
								                    			</span>
							                        		</li>
							                        		<li>
							                        			<span style="color:white;">
							                        				{{$now_speedway->numberOne['name']}}
							                        			</span>
							                       			</li>
							                        		<li>
							                        			<span style="color:white;font-weight:bold">
										                    		第2名：2017-03-27 00:00:00
										                    	</span>
							                        		</li>
							                        		<li>
							                        			<span style="color:white;">
							                        				{{$now_speedway->numberTwo['name']}}
							                        			</span>
							                       			</li>
							                        		<li>
							                        			<span style="color:white;font-weight:bold">
										                   			第3名：
										                   		</span>
							                        		</li>
							                        		<li>
							                        			<span style="color:white;">
							                        				{{$now_speedway->numberThree['name']}}
							                        			</span>
							                       			</li>
									                   	@endif
										            </ul>
								                @else
								                	<ul style="color:white" class="allteam">
								                		<li>
								                			<h3>{{$now_speedway->name}}</h3>
								                		</li>
														<li>
															<span style="color:white;font-weight:bold;">
																正賽當地時間：
															</span>
														</li>
														<li>
															<span style="color:white;">
																{{date('m/d h:i A',strtotime($now_speedway->raced_at))}}
															</span>
														</li>
														<li>
															<span style="color:white;font-weight:bold;">
																正賽台灣時間：
															</span>
														</li>
														<li>
															<span style="color:white;">
																{{date('m/d h:i A',strtotime($now_speedway->raced_at_local))}}
															</span>
														</li>
													</span>
												@endif
											</div>
						                    <img src="{{assetRemote('image/benefit/event/motogp/speedway/banner/'.$now_speedway->name.'.jpg')}}" alt="【2017{{$now_speedway->name}}】台灣正賽時間{{$now_speedway->raced_at_local.$tail}}">
						                    {{-- <span>{{$now_speedway->station_name}}賽事概要</span> --}}
						                </a>
					                @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col" style="padding:3% 4% 3% 0">
                        <div class="box-white gp-box-border game">
                        	{{\Session::get('danger')}}
				            <?php
								$now = date('Y-m-d H:i:s');
								// $now = '2017-04-03 00:00:01';
								$nums = array('冠軍', '亞軍', '季軍');
							 ?>
							@if(count($choices) != 0)
				            	<div class="title-site-activity-forecast-right border-radius-2" style="background:rgba(225, 17, 17 ,1)">
									<h2>您所選的前三名</h2>
								</div>
								@foreach($choices as $key => $choice)
									<?php 
										$racer = $choice->racer;
									?>
									<div class="topthree col-xs-12 col-sm-12 col-md-12">
										<div class="col-xs-12 col-sm-3 col-md-3"></div>
										<div class="col-xs-12 col-sm-2 col-md-2 text-right">
											<span class="num{{$key + 1}}">{{$nums[$key]}}</span>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-4">
											<label title="【{{$racer->number}}】{{ $racer->name }}">【{{$racer->number}}】{{ $racer->name }}</label>
										</div>
										<div class="col-xs-12 col-sm-3 col-md-3"></div>
									</div>
								@endforeach
							@else
								@if(count($now_speedway) != 0 && strtotime($now) > strtotime($now_speedway->closested_at))
									<div class="title-site-activity-forecast-right border-radius-2">
										<h2>已過本次活動時間</h2>
									</div>
									<div class="topthree col-xs-12 col-sm-12 col-md-12 text-center">
										<div class="links">
											<span style="display:initial;">
												您尚未參加預測<br>下次活動時間，詳見各站<a href="#calendar" title="活動時間表{{$tail}}">活動時間表</a><br>
											</span><br>
										</div>
									</div>
								@elseif( !isset($customer) )
									<div class="title-site-activity-forecast-right border-radius-2">
										<h4>請先登入，並完成"完整註冊"</h4>
									</div>
									<div class="link-area col-xs-12 col-sm-12 col-md-12">
										<div class="links small text-center" style="margin-top:25px;margin-bottom:25px">
											<a class="btn btn-warning" style="width:200px;height:40px;padding-top: 10px" href="{{URL::route('login')}}" title="登入會員{{$tail}}">登入會員</a>
											<a class="btn btn-danger" style="width:200px;height:40px;padding-top: 10px" href="{{URL::route('customer-account-create')}}" title="免費加入會員{{$tail}}">免費加入會員</a>
										</div>
									</div>
								@elseif( isset($customer) and $customer->role_id == 1 )
									<div class="title-site-activity-forecast-right border-radius-2">
										<h4>參加本活動需完成"完整註冊"</h4>
									</div>
									<div class="link-area col-xs-12 col-sm-12 col-md-12">
										<div class="links" style="margin-top:25px">
											<a class="btn btn-danger" href="{{URL::route('customer-account-complete')}}" title="完整註冊{{$tail}}">完整註冊</a>
										</div>
									</div>
								@elseif(count($now_speedway) != 0)
					                <div class="title-site-activity-forecast-right border-radius-2">
					                    第{{$now_speedway->id}}站 {{$now_speedway->station_name}} 比賽預測活動
					                </div>
					                <div class="ct-site-activity-forecast">
						                <form method="post">
						                	<ul class="ul-ct-site-activity-forecast">
							                	<li class="ct-site-activity-forecast-right" style="margin-left: auto;margin-right:auto">
									                <ul class="drop-site-activity-forecast">
									                    <li class="box-fix-column " style="float:none;width:250px;padding:24px 10px !important">
									                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="champion">冠軍</span></div>
									                        <div class="box-fix-auto drop-site-activity-forecast-right" style="width:73%">
									                            <select class="select2" name="my_choice[]">
									                                <option value="">預測分站第1名</option>
									                                @foreach($racers as $racer)
									                                	<option value="{{$racer->id}}">【{{$racer->number}}】{{ $racer->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
									                    </li>
									                    <li class="box-fix-column " style="float:none;width:250px;padding:24px 10px !important">
									                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="runner-up">亞軍</span></div>
									                        <div class="box-fix-auto drop-site-activity-forecast-right" style="width:73%">
									                            <select class="select2" name="my_choice[]">
									                                <option value="">預測分站第2名</option>
									                                @foreach($racers as $racer)
									                                	<option value="{{$racer->id}}">【{{$racer->number}}】{{ $racer->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
									                    </li>
									                    <li class="box-fix-column " style="float:none;width:250px;padding:24px 10px !important">
									                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="third">季軍</span></div>
									                        <div class="box-fix-auto drop-site-activity-forecast-right" style="width:73%">
									                            <select class="select2" name="my_choice[]">
									                                <option value="">預測分站第3名</option>
									                                @foreach($racers as $racer)
									                                	<option value="{{$racer->id}}">【{{$racer->number}}】{{ $racer->name }}</option>
									                                @endforeach
									                            </select>
									                        </div>
									                    </li>
									                </ul>
									            </li>
											</ul>
											<div class="text-center">
												<input class="btn btn-default btn-warning border-radius-2 btn-moto-gp-member" style="width:200px;height:40px" type="submit" value="送出答案" id="answer">
											</div>
						                </form>
						            </div>
					            @endif
				        	@endif
				        	@if(count($now_speedway) != 0)
				        		<div class="note" style="">
				        			<span class="pull-right">本站活動截止時間：{{date('m/d h:i A',strtotime($now_speedway->closested_at . '- 1 second'))}}止</span>
				        			<span class="pull-left">
				        				<a href="https://sports.bwin.com/en/sports/40/betting/motorbikes#sportId=40" title="歐洲bwin網站冠軍預測參考" target="_blank" nofollow>歐洲bwin網站冠軍預測參考</a>
				        			</span>
				        		</div>
				        		<?php /*if(count($choices) != 0){ echo 'position:static;'.'margin-top:55px;';}*/?>
				        	@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!isset($customer))
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="gp-box">
	                <div class="gp-box-title">
	                    <h2 class="clearfix">
	                        <span style="float:left;padding:0 23px;position:static;background:black">您的積分排行</span>
	                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
	                    </h2>
	                </div>
	                <div class=" col-md-12 col-sm-12 col-xs-12 line box-green1"></div>
	                <div class="gp-box-content clearfix col-xs-block col-sm-block">
	                    <div class="col-md-3 col-sm-6 col-xs-12 gp-col text-center">
							<div><img src="{{assetRemote('image/moto-gp/icon-champion.jpg')}}" alt="name image"><h2>目前總冠軍預測狀況</h2></div>
	                    </div>
	                    <div class="col-md-6 col-sm-12 col-xs-12 gp-col gp-col box-green">
	                        <div class="right-side-box">
	                        	<div class="col-md-12 col-sm-12 col-xs-12 gp-col">
			                        <div class="right-side-box">
			                            <div class="box-white gp-box-border">
			                            	@if($other_champigns and count($other_champigns))
				                                <ul class="clearfix">
				                                	<h3>其他會員預測的冠軍支持度</h3>
				                                	<?php $total_champion = 0 ?>
													@foreach($other_champigns as $other_chamign)
														<?php 
														 	$fk_champion = $other_chamign->champion* 11;
														 	$total_champion += $fk_champion ;
														 ?>
								                        <li class="clearfix">
								                            <span class="border-radius-2" style="text-transform:none">{{$other_chamign->racer_number}} {{$other_chamign->racer_name}}</span>
								                            <div class="num-percent"></div>
								                            <div class="percent">
								                                <div class="ct-percent" style="width:0%;display:none">{{$fk_champion}}</div>
								                            </div>
								                        </li>
								                    @endforeach
				                                </ul>
				                            @endif
			                            </div>
			                        </div>
			                    </div>
	                        </div>
	                    </div>
	                    <div class="col-md-3 col-sm-6 col-xs-12">
                    	</div>
	                </div>
	            </div>
			</div>
        @endif
        @if(isset($customer))
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="gp-box">
	                <div class="gp-box-title">
	                    <h2 class="clearfix">
	                        <span style="float:left;padding:0 23px;position:static;background:black">您的積分排行</span>
	                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
	                    </h2>
	                </div>
	                <div class=" col-md-12 col-sm-12 col-xs-12 line box-green1"></div>
	                <div class="gp-box-content clearfix col-xs-block col-sm-block">
	                    <div class="col-md-3 col-sm-6 col-xs-12 gp-col text-center">
	                    	@if(isset($customer))
			                    <div><img src="{{assetRemote('image/moto-gp/icon-champion.jpg')}}" alt="name image"><h2 style="font-weight: bold">您心目中的總冠軍</h2></div>
			                    <?php 
									$first_score = 0;
								?>
								@foreach($my_champigns as $my_champign)
									@if($my_champign->scores >= $first_score or $first_score == 0)
										<a class="btn btn-winner">{{$my_champign->racer_number}} {{$my_champign->racer_name}}</a>
									@else
										<?php continue; ?>
									@endif
									<?php $first_score = $my_champign->scores; ?>
								@endforeach
							@else
								<div><img src="{{assetRemote('image/moto-gp/icon-champion.jpg')}}" alt="name image"><h2>您心目中的總冠軍</h2></div>
							@endif
	                    </div>
	                    @if(isset($customer))
	                    	<div class="col-md-3 col-sm-6 col-xs-12 gp-col box-green">
	                        	<div class="box-white gp-box-border text-left">
									<div class="result col-xs-12 col-sm-12 col-md-12" style="padding: 20px 20px 20px 21%;">
										<span>
					                        您的預測結果 <br>
					                        參加次數：{{isset($stations) ? $stations : 0}}站 <br>
					                        猜對第一名：{{isset($choice_result[25]) ? $choice_result[25] : 0}}次<br>
					                        猜對第二名：{{isset($choice_result[20]) ? $choice_result[20] : 0}}次<br>
					                        猜對第三名：{{isset($choice_result[16]) ? $choice_result[16] : 0}}次<br>
					                        目前獲得點數：{{$point ? $point->points_current : 0}}點<br>
					                        <a href="{{URL::route('customer-history-points')}}">點數獲得履歷</a>
				                        </span>
				                    </div>
				                </div>
	                    	</div>
	                    @endif
	                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col">
	                        <div class="right-side-box">
	                            <h3>其他會員預測的冠軍支持度</h3>
	                            <div class="box-white gp-box-border">
	                            	@if($other_champigns and count($other_champigns))
		                                <ul class="clearfix">
		                                	<?php $total_champion = 0 ?>
											@foreach($other_champigns as $other_chamign)
												<?php 
												 	$fk_champion = $other_chamign->champion* 11;
												 	$total_champion += $fk_champion ;
												 ?>
						                        <li class="clearfix">
						                            <span class="border-radius-2" style="text-transform:none">{{$other_chamign->racer_number}} {{$other_chamign->racer_name}}</span>
						                            <div class="num-percent"></div>
						                            <div class="percent">
						                                <div class="ct-percent" style="width:0%;display:none">{{$fk_champion}}</div>
						                            </div>
						                        </li>
						                    @endforeach
		                                </ul>
		                            @endif
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                @if(isset($customer))
			            <div class="gp-box-content clearfix col-xs-block col-sm-block" style="position:relative;">
			            	<div class="rankcontainer" style="<?php if(isset($customer)){echo 'height:422px';}?>">
			                	<div class="info-table" style="width:96%;top: 0%;padding:2%;margin:2%;background:white">
			            		@if( isset($speedway_choices) && isset($speedway_real_choices))
			                		<h3>您的預測歷史紀錄</h3>
			                		<div class="clearfix" id="pchistory">
										<table class="table table-bordered text-center" style="background-color: white;margin-bottom:20px">
											<?php 
												$rank = array('title','第1名','第2名','第3名');
											?>
											@foreach($rank as $a)
												<tr>
													@if($a == 'title')
														@for($i = 0; $i <= 9 ; $i++)
															@if($i==0)
																<th> </th>
															@else
																<th colspan="2" class="text-center">第{{$i}}站</th>
															@endif
														@endfor
													@else
														@for($i = 0; $i <= 9 ; $i++)
															@if($i==0)
																<td>{{$a}}</td>
															@elseif($a == '第1名' && array_key_exists($i, $speedway_choices->one))
																<td>{{$speedway_choices->one[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
																		<?php $times[25] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第2名' && array_key_exists($i, $speedway_choices->two))
																<td>{{$speedway_choices->two[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
																		<?php $times[20] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第3名' && array_key_exists($i, $speedway_choices->three))
																<td>{{$speedway_choices->three[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
																		<?php $times[16] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@else
																<td>-</td>
																<td>-</td>
															@endif
														@endfor
													@endif
												</tr>
											@endforeach
										</table>
										<table class="table table-bordered text-center" style="background-color: white">
											@foreach($rank as $a)
												<tr>
													@if($a == 'title')
														@for($i = 10; $i <= 18 ; $i++)
															@if($i==0)
																<th> </th>
															@else
																<th colspan="2" class="text-center">第{{$i}}站</th>
															@endif
														@endfor
														<th class="text-center">答對</th>
													@else
														@for($i = 10; $i <= 18 ; $i++)
															@if($i==0)
																<td>{{$a}}</td>
															@elseif($a == '第1名' && array_key_exists($i, $speedway_choices->one))
																<td>{{$speedway_choices->one[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
																		<?php $times[25] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第2名' && array_key_exists($i, $speedway_choices->two))
																<td>{{$speedway_choices->two[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
																		<?php $times[20] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第3名' && array_key_exists($i, $speedway_choices->three))
																<td>{{$speedway_choices->three[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
																		<?php $times[16] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@else
																<td>-</td>
																<td>-</td>
															@endif
														@endfor
														@if($a == '第1名')
															<td>{{$times[25]}}</td>
														@elseif($a == '第2名')
															<td>{{$times[20]}}</td>
														@else
															<td>{{$times[16]}}</td>
														@endif
													@endif
												</tr>
											@endforeach
										</table>
									</div>
									<div class="clearfix" id="mobilehistory" style="background-color: white">
										<table class="table table-bordered text-center" >
											<?php 
												$rank = array('title','第1名','第2名','第3名');
												$times = array(25 => 0, 20 => 0, 16 => 0);
											?>
											@foreach($rank as $a)
												<tr>
													@if($a == 'title')
														@for($i = 0; $i <= 18 ; $i++)
															@if($i==0)
																<th> </th>
															@else
																<th colspan="2" class="text-center">第{{$i}}站</th>
															@endif
														@endfor
														<th class="text-center">答對</th>
													@else
														@for($i = 0; $i <= 18 ; $i++)
															@if($i==0)
																<td>{{$a}}</td>
															@elseif($a == '第1名' && array_key_exists($i, $speedway_choices->one))
																<td>{{$speedway_choices->one[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
																		<?php $times[25] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第2名' && array_key_exists($i, $speedway_choices->two))
																<td>{{$speedway_choices->two[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
																		<?php $times[20] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第3名' && array_key_exists($i, $speedway_choices->three))
																<td>{{$speedway_choices->three[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
																		<?php $times[16] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@else
																<td>-</td>
																<td>-</td>
															@endif
														@endfor
														@if($a == '第1名')
															<td>{{$times[25]}}</td>
														@elseif($a == '第2名')
															<td>{{$times[20]}}</td>
														@else
															<td>{{$times[16]}}</td>
														@endif
													@endif
												</tr>
											@endforeach
										</table>
									</div>
								@endif
							</div>
	                	</div>
	                    <div class="col-md-3 col-sm-6 col-xs-12 gp-col text-center">
	                    </div>
	                    <div class="col-md-3 col-sm-6 col-xs-12 gp-col box-green"></div>
	                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col">
	                        <div class="right-side-box"></div>
	                    </div>
	                </div>
	            @endif
	            </div>
	        </div>
       	@endif
        @if(count($top10_score) > 0)
	        <div class="col-md-12 col-sm-12 col-xs-12" id="points-ranking">
	            <div class="gp-box totalrank" style="position: relative;">
	                <div class="gp-box-title">
	                    <h2 class="clearfix">
	                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 積分排行</span>
	                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
	                    </h2>
	                </div>
	                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
	                <div class="gp-box-content clearfix col-xs-block col-sm-block">
	                	<div class="rankcontainer" style="height:422px">
		                	<div class="info-table" style="width:100%;top: 10%;width:100%">
							    <div class="container-moto-gp-event ct-your-scoreboard">
							        <div class="lineChart col-xs-12 col-sm-12 col-md-12">
							            <h3>車隊積分排行榜</h3>
							            <div id="chart" class="ct-lineChart text-center"></div>
							        </div>
							    </div>
		                	</div>
	                	</div>
	                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
	                    <div class="col-md-6 col-sm-6 col-xs-12 nail-box no-padding">
	                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col box-tall box-light-blue"></div>
	                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
	                    </div>
	                </div>
	                <div class="gp-box-content clearfix col-xs-block col-sm-block">
	                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col">
	                    	<h3>車隊積分排行榜</h3>
	                        <div class="box-white gp-box-border">
	                        	<div id="teamBarChart" class="ct-half-groph"></div>
	                        </div>
	                    </div>
	                    <div class="col-md-6 col-sm-6 col-xs-12 nail-box no-padding">
	                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col box-tall box-light-blue">
	                            <div class="fly-box nail-left nail-bottom">
	                            	<h3>車廠積分排行榜</h3>
	                                <div class="box-white gp-box-border">
										<div id="makerBarChart" class="ct-half-groph"></div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
	                    </div>
	                </div>
	            </div>
	        </div>
        @endif
        <div class="col-md-12 col-sm-12 col-xs-12" id="driver-information">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 車手資訊及成績</span>
                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    <div class="col-md-12 col-sm-12 col-xs-12 nail-box no-padding">
                    	<div style="overflow-x: auto;width:100%;left: 4%;top: 2%;width:92%" class="info-table info-table-container">
					        <table class="table table-bordered table-motogp-driver-information">
					            <thead>
					            <tr>
					                <th class="text-center">車號</th>
					                <th class="text-center">國籍</th>
					                <th class="text-center">車手姓名</th>
					                <th class="text-center">車隊名稱</th>
					                <th class="text-center">車輛<br>廠牌</th>
					                <th class="text-center">生涯<br>頒獎台</th>
					                <th class="text-center">生涯<br>總冠軍</th>
					                <th class="text-center">2016<br>年度排名</th>
					                <th class="text-center">2017<br>累計積分</th>
					                <th class="text-center">2017<br>目前排名</th>
					            </tr>
					            </thead>
					            <tbody>
					            @foreach( $racers as $key => $racer )
						            <tr>
						                <td class="text-center">{{$racer->number}}</td>
						                <td class="text-center"><img src="{{$photo_home}}country/{{$racer->country}}.png" alt="2017MotoGP【{{$racer->number}}】{{$racer->name}}國籍"></td>
						                <td><a href="{{$racer->site}}" title="2017MotoGP【{{$racer->number}}】{{$racer->name}}" target="_blank" nofollow>{{$racer->name}}</a></td>
						                <td><a href="{{$racer->team_site}}" title="2017MotoGP{{$racer->team_name}}" target="_blank" nofollow>{{$racer->team_name}}</a></td>
						                <td class="text-center">{{$racer->motor_manufacturer_name}}</td>
						                <td class="text-center">{{$racer->podium_quantity}}</td>
						                <td class="text-center">{{$racer->champion_quantity}}</td>
						                <td class="text-center">{{$racer->champion_lastyear}}</td>
						                <td class="text-center">{{$racer->total_scores ? $racer->total_scores : 0}}</td>
						                <td class="text-center">{{$racer->ranking ? $racer->ranking : '-'}}</td>
						            </tr>
					            @endforeach
					            </tbody>
					        </table>
					         <div class="note1" style="overflow-y: auto">
					         	<span>
						            註1:生涯頒獎台包含MotoGP&GP500最高級距離賽事累積成績，由左至右依序為冠軍、亞軍、季軍次數。<br>
						            註2:總冠軍為MotoGP&GP500最高級距離賽事總和。<br>
						            資料來源：MotoGP <br>
					           	</span>
					        </div>
						</div>		
                        <div class="col-md-3 col-sm-3 col-xs-12 gp-col box-tall box-light-blue info">
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12 gp-col contestant-info">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="schedule">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 賽程表</span>
                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-red2"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col">
                        <div class="box-white gp-box-border" style="padding:1%">
                        	<h1 class="font-color-red" style="font-weight: bold">※此時間為官方提供時間僅供參考，若臨時有異動將會再行更新。</h1>
					        <ul class="promo-calendar-page">
					        	<?php
					        		$color = array('box-light-blue','box-green','box-light-red2','box-light-red2','box-light-blue','box-green','box-green','box-light-red2','box-light-blue','box-light-blue','box-green','box-light-red2','box-light-red2','box-light-blue','box-green','box-green','box-light-red2','box-light-blue');
					        	?>
					        	@foreach ($speedwaies as $key => $speedway)
					        		<?php $winerGroup = $speedway->winerGroup; ?>
						            <li class="col-xs-12 col-sm-4 col-md-4">
						            	<a href="{{URL::route('benefit-event-motogp-2017detail', $speedway->station_name)}}" title="{{$speedway->station_name.$tail}}" target="_blank" style="box-shadow:none;-webkit-box-shadow:none">
							            	<div style="position:absolute;width:40%;height:95.3%;padding:3%;top:2.5%" class="{{$color[$key]}}">
							            		<img src="{{assetRemote('image/benefit/event/motogp/speedway/'.$speedway->name.'.jpg')}}" alt="【2017{{$speedway->name}}】台灣正賽時間{{$speedway->raced_at_local.$tail}}"><br>
							            		@if($speedway->active == 2)
							            			<ul style="color:white" class="allteam allteamresult">
								                		<li style="padding:4px 0px">
								            				<h3>{{$speedway->name}}</h3>
								            			</li>
								            			<li>
									                    	<span style="color:white;font-weight:bold;">
									                    		比賽結果:
									                    	</span>
									                    </li>
								                    	@foreach( $winerGroup as $key => $winer )
									                        <li>
									                        	<span style="color:white;font-weight:bold">
									                        		第{{$key}}名：
									                        	</span>
									                        </li>
									                        <li>
									                        	<span style="color:white;">
									                        		{{$winer ? $winer->name : ''}}
									                        	</span>
									                       	</li>
									                    @endforeach
									                </ul>
								                @else
								                	<ul style="color:white" class="allteam">
								                		<li>
															<h3>{{$speedway->name}}</h3>
														</li>
														<li>
															<span style="color:white;font-weight:bold;">		
																正賽當地時間：
															</span>
														</li>
														<li>
															<span style="color:white;">
																{{date('m/d h:i A',strtotime($speedway->raced_at))}}
															</span>
														</li>
														<li>
															<span style="color:white;font-weight:bold;">		
																正賽台灣時間：
															</span>
														</li>
														<li>
															<span style="color:white;">
																{{date('m/d h:i A',strtotime($speedway->raced_at_local))}}
															</span>
														</li>
								                	</ul>
								                @endif
								            </div>
						                    <img src="{{assetRemote('image/benefit/event/motogp/speedway/banner/'.$speedway->name.'.jpg')}}" alt="【2017{{$speedway->name}}】台灣正賽時間{{$speedway->raced_at_local.$tail}}">
						                    {{-- <span>{{$now_speedway->station_name}}賽事概要</span> --}}
						                </a>
						            </li>
						        @endforeach
					        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="event-discuss">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 活動討論區</span>
                    	<img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col">
                        <div class="box-white gp-box-border">
                        	{{-- <div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.8";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-post" data-href="https://www.facebook.com/WebikeTaiwan/posts/480050898785203" data-width="auto" data-show-text="true"><blockquote cite="https://www.facebook.com/WebikeTaiwan/posts/480050898785203" class="fb-xfbml-parse-ignore"><p>&#x3010;&#x7de8;&#x8f2f;&#x9577;&#x5c08;&#x6b04;&#x3011;&#x76f4;&#x7dda;&#x738b;&#x8005;&#x7684;&#x6b9e;&#x843d;! YAMAHA &#x300c;VMAX&#x300d;&#x6b77;&#x53f2;&#x56de;&#x9867;

	&#x7814;&#x767c;&#x51fa;VMAX&#x7684;&#x7814;&#x767c;&#x5718;&#x968a;&#x8981;&#x8868;&#x660e;&#x7684;&#x5c31;&#x662f;&#x300c;&#x76e1;&#x60c5;&#x5730;&#x904b;&#x7528;VMAX&#x4f86;&#x8c50;&#x5bcc;&#x4eba;&#x751f;&#x300d;&#xff0c; &#x6b63;&#x662f;&#x5411;&#x4e16;&#x9593;&#x5c55;&#x73fe;&#x6469;&#x6258;&#x8eca;&#x6b3e;&#x5b58;&#x5728;&#x610f;&#x7fa9;&#x7684;&#x540d;&#x8a00;&#x3002;

	&#x8a73;&#x7d30;&#x65b0;&#x805e;&#xff1a;http://www.webike.tw/bikenews/?p=14807</p>由 <a href="https://www.facebook.com/WebikeTaiwan/">Webike台灣</a>貼上了&nbsp;<a href="https://www.facebook.com/WebikeTaiwan/posts/480050898785203">2017年3月5日</a></blockquote></div> --}}

							<div id="fb-root"></div>
							<script>
							(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.5";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));
							document.write('<div class="fb-like" data-href="http://'+document.location.hostname+document.location.pathname+'" data-layout="button" data-action="like" data-show-faces="false" data-share="true" style="float:right;"></div>');
							</script>
							

							<div id="fb-root"></div>
							<script>
							(function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (d.getElementById(id)) return;
								js = d.createElement(s); js.id = id;
								js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1";
								fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));

							document.write('<div class="fb-comments" data-href="http://'+document.location.hostname+document.location.pathname+'" data-num-posts="3" data-width="100%" data-order-by="reverse_time"></div>');
							</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

@if(isset($total_champion))
	<script type="text/javascript">
		var total_champion = "{{$total_champion}}";
	</script>
@endif
<script>
	$(window).resize(function(){
        $('.fly-box').each(function(){
            console.log($(this).closest('.nail-box').width());
            $(this).width($(this).closest('.nail-box').width() * 0.90);
        });
        var height = $('.info-table-container').height() + 85;
		$('.contestant-info').closest('.nail-box').height(height);
		// var height = $('.discussfb').height() + 50;
		// $('.discuss').height(height);
    });

    function slipTo1(element){

	    var y = parseInt($(element).offset().top) - 140;
	    $('html,body').animate({scrollTop: y}, 400);
	}

	var first_view = true;
	$(document).scroll(function(){
        if( first_view && $(this).scrollTop() + $(window).height() >= $(".ct-percent:last").offset().top ){
        	first_view = false;
        	peoples = 0;
			$('.ct-percent').each(function(key,element){
				peoples += parseInt($(element).text());
				percentage = Math.round(parseInt($(element).text()) / parseInt(total_champion) * 100 ) + '%';
				// $(element).text('').animate({display: block});
				$(element).text('').animate({width: percentage}, 1500);
				$(element).css('display','block');
				$(element).closest('li').find('.num-percent').html(percentage);
			});
			var h3 = $('.ct-percent').closest('.right-side-box').find('h3');
			// alert();
			h3.text(h3.text() + '(共 '+ peoples +' 人)');
		}
	});

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var options = {
        	titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
	        lineWidth: 1,
	        // colors: ['#2864D1', '#D10501'],
	        chartArea: {
	            left: 80,
	            right: 25,
	            top: 50
	        },
	        width: 1161,
            height: 300,
	        pointSize: 6,
	        hAxis: {
	            baseline: 0,
	            slantedText: true,
		        slantedTextAngle:30,
	        	title: '賽次'
	        },
	        vAxis: {
	            viewWindow: {
	                min: 0
	            },
	            baseline: 0,
	            title: '累積積分'
	        },
	        legend: {
	            position: 'top'
	        },
	        tooltip: {
	        	isHtml: true,
	            textStyle: {
	                fontSize: 13
	            }
	        },
	        focusTarget: 'category',
	        'areaOpacity': 0.1,
	        annotations: {
				textStyle: {
					fontName: 'Times-Roman',
					fontSize: 13,
					bold: true,
					italic: true,
				}
			},
	    };

        var data = new google.visualization.DataTable();
	    data.addColumn('string', '站別');
	    
        @foreach($top10 as $racer)
        	data.addColumn('number', '【{{$racer->number}}】{{$racer->nickname}}');
        @endforeach

	    data.addRows([
	    	<?php 
				$scores = array();
				$last_top10_str = '';
		    	for($i = 1 ; $i <= 18 ; $i++){
	    			foreach($top10 as $key => $racer){
	    				if( !isset($scores[$key]) ){
	    					$scores[$key] = 0;
	    				}
	    				$top10_score->filter(function($racer_score) use($racer, $i, $key, &$scores){
	    					if($racer_score->speedway_id == $i and $racer_score->racer_id == $racer->id){
    							$scores[$key] += $racer_score->score;
	    					}
	    				});
	    			}
	    			$top10_str = implode(',', $scores);
	    			if($top10_str !== $last_top10_str){
	    				$last_top10_str = $top10_str;
	    				echo '["第'.($i).'站", '.$top10_str.'],';
	    			}
		    	}
	    	?>
        ]);

	    var formatter = new google.visualization.PatternFormat('{1}');
	    formatter.format(data, [0, 1], 1);
	    formatter.format(data, [0, 2], 2);

	    var chart = new google.visualization.LineChart(document.getElementById('chart'));
	    chart.draw(data, options);

	    var data = new google.visualization.DataTable();
	    data.addColumn('string', '車隊');
	    data.addColumn('number', '積分');
	    data.addRows([
    			@foreach( $teamData as $team )
    				['{{$team->team_name}}', {{$team->sum}}],
    			@endforeach
    		]);
    	var view = new google.visualization.DataView(data);
    	var options = {
				titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
				chartArea: {
		            left: 120,
		            right: 25,
		            top: 25,
		            height: 220
		        },
				width: 470,
				height: 300,
				bar: {groupWidth: "60%"},
				legend: { position: "none" },
				vAxis: {
		            viewWindow: {
		                min: 0
		            },
		            baseline: 0,
		            title: ''
		        },
		        hAxis: {
		        	title: '',
		        	slantedText: true,
		            slantedTextAngle:30
		        }
	    };
	    var teamBarChart = new google.visualization.BarChart(document.getElementById("teamBarChart"));
	    teamBarChart.draw(view, options);

	    var data = new google.visualization.DataTable();
	    data.addColumn('string', '車廠');
	    data.addColumn('number', '積分');
	    data.addRows([
    			@foreach( $makerData as $maker )
    				['{{$maker->motor_manufacturer_name}}', {{$maker->sum}}],
    			@endforeach
    		]);
    	var view = new google.visualization.DataView(data);
    	var options = {
				titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
				chartArea: {
		            left: 100,
		            right: 25,
		            top: 25,
		            height: 220
		        },
				width: 470,
				height: 300,
				bar: {groupWidth: "60%"},
				legend: {
		            position: 'none'
		        },
		        vAxis: {
		            viewWindow: {
		                min: 0
		            },
		            baseline: 0,
		            title: ''
		        },
		        hAxis: {
		        	title: '',
		        	slantedText: true,
		            slantedTextAngle:30
		        }
	    	};
	    var makerBarChart = new google.visualization.BarChart(document.getElementById("makerBarChart"));
	    makerBarChart.draw(view, options);
    }

	function checkingRacer(){
	var select_racer = [];
		$('.select2 option:selected').each(function(key,element){
			if($(element).val()){
				select_racer.push($(element).val());
				console.log(select_racer);
			}
		});
		$('.select2 option').attr('disabled',false);
		$('.select2 option').each(function(key,element){
			if($.inArray( $(element).val(), select_racer ) >= 0 && !$(element).is(':selected')){
				$(element).attr('disabled',true);
				$(".select2").select2();
			}
		});
	}
	checkingRacer();
	$('.select2').change(function(){
		checkingRacer();
	});

	$('#answer').click(function(){
		var validate_fail = false;
		$('.error').remove();
		$('.select2 option:selected').each(function(key,element){
			if(!$(element).val()){
				$(element).closest('.box-fix-column .drop-site-activity-forecast-right').after('<div><span class="error" style="color:red;">請選擇車手!</span></div>');
				validate_fail = true;
			}
		});
		if(validate_fail){
			return false;
		}

		$(this).prop('disabled', true).css('cursor', 'no-drop');
		$(this).closest('form').submit();
		
		// _fbq.push(['track', 'MotoGP2017']);
		// _gaq.push(['_trackEvent','link', 'click', 'MotoGP2017']);
	});

	var height = $('.info-table-container').height() + 85;
	$('.contestant-info').closest('.nail-box').height(height);

	var height = $('.game').height() + 85;
	$('.addict').height(height);
	$('.addict').css('min-height', '470px');

</script>

@stop
