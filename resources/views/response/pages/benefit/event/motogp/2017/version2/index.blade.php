@extends('response.layouts.1column')
@section('style')
    <style type="text/css">
        .right-side-box{
            padding-right: 5%;
        }
        .left-side-box{
            padding-left: 5%;
        }
        .gp-box{
            margin-top: 20px;
            border: 2px #777 solid;
        }
        .gp-col{
            display: flex;
            justify-content: center;
            flex-direction: column;
            padding: 3% 2%;
        }
        .gp-box-content{
            display: flex;
            background: #e7e7e7;
        }
        .gp-box-content h3{
            font-weight: bold;
            font-size: 1.2rem;
        }
        .gp-box-content ul li{
            text-decoration: none;
            list-style: none;
        }
        .gp-box-title h2{
            position: relative;
        }
        .gp-box-title h2 span{
            color: #fff;
            position: absolute;
            left: 2%;
            line-height: 30px;
            font-size: 1.2rem;
            font-weight: bold;
        }
        .gp-box-title h2 img{
            width:25%;
        }
        .gp-box-border{
            border:1px #ccc solid;
        }
        .box-green{
            background: rgba(0, 189, 3, 0.6);
        }
        .box-light-blue{
            background: rgba(3, 79, 137, 0.6);
        }
        .box-white{
            background: #fff;
        }
        .nail-box{
            position: relative;
        }
        .fly-box{
            position: absolute;
            width:450px;
        }
        .box-tall{
            height:100%;
        }
        .nail-left{
            left:10%;
        }
        .nail-right {
            right:10%;
        }
        .nail-top {
            top: 8.5%;
        }
        .nail-bottom {
            bottom: 8.5%;
        }
    </style>
@stop

@section('middle')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <img src="{{assetRemote('image/moto-gp/2017/banner1.png')}}" alt="">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div id="myNavbar" class="menu-brand-top-page">
                <div class="ct-menu-brand-top-page2">
                    <ul class="ul-menu-brand-top-page ul-menu-motor-top-page">
                        <li>
                            <a href="" title="本站活動預測{{$tail}}">本站活動預測</a>
                        </li>
                        <li>
                            <a href="" title="本站活動預測{{$tail}}">本站活動預測</a>
                        </li>
                        <li>
                            <a href="" title="本站活動預測{{$tail}}">本站活動預測</a>
                        </li>
                        <li>
                            <a href="" title="本站活動預測{{$tail}}">本站活動預測</a>
                        </li>
                        <li>
                            <a href="" title="本站活動預測{{$tail}}">本站活動預測</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <img src="{{assetRemote('image/moto-gp/2017/banner2.png')}}" alt="">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2>
                        <img src="{{assetRemote('image/moto-gp/2017/title-bg.png')}}" alt="">
                        <span>本站活動預測</span>
                    </h2>
                </div>
                <div class="gp-box-content clearfix">
                    <div class="col-md-4 col-sm-6 col-xs-12 gp-col">
                        <h3>您心目中的總冠軍</h3>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 gp-col box-green">
                        <div class="box-white gp-box-border" style="height: 300px;">

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col">
                        <div class="right-side-box">
                            <h3>其他會員預測的冠軍支持度(共9900人)</h3>
                            <div class="box-white gp-box-border">
                                <ul>
                                    <li>數值A</li>
                                    <li>數值B</li>
                                    <li>數值C</li>
                                    <li>數值D</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="event-discuss">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2>
                        <img src="{{assetRemote('image/moto-gp/2017/title-bg.png')}}" alt="">
                        <span>賽道紀錄</span>
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col">
                        <div class="box-white gp-box-border">
                            @if($thisSpeedway->pole_rider_2017)
                                <table class="table table-bordered table-motogp-detail">
                                    <thead class="table-moto-boder-none">
                                    <tr>
                                        <th class="text-center"></th>
                                        <th class="text-center">車手姓名</th>
                                        <th class="text-center">成績</th>
                                        <th class="text-center">時速</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">桿位</td>
                                        <td class="text-center">{{$thisSpeedway->pole_rider_2017}}</td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->pole_record_2017);
                                        ?>
                                        <td class="text-center">{{$values[0]}}</td>
                                        <td class="text-center">{{$values[1]}}</td>
                                    </tr>
                                    <tr class="active">
                                        <td class="text-center">冠軍</td>
                                        <td class="text-center">{{$thisSpeedway->first_2017}}</td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->first_record_2017);
                                        ?>
                                        <td class="text-center">{{$values[0]}}</td>
                                        <td class="text-center">{{$values[1]}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">亞軍</td>
                                        <td class="text-center">{{$thisSpeedway->second_2017}}</td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->second_diff_2017);
                                        ?>
                                        <td class="text-center">{{$values[0]}}</td>
                                        <td class="text-center">{{$values[1]}}</td>
                                    </tr>
                                    <tr class="active">
                                        <td class="text-center">季軍</td>
                                        <td class="text-center">{{$thisSpeedway->third_2017}}</td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->third_diff_2017);
                                        ?>
                                        <td class="text-center">{{$values[0]}}</td>
                                        <td class="text-center">{{$values[1]}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(window).resize(function(){
            $('.fly-box').each(function(){
                console.log($(this).closest('.nail-box').width());
                $(this).width($(this).closest('.nail-box').width() * 0.90);
            });
        });
    </script>
@stop