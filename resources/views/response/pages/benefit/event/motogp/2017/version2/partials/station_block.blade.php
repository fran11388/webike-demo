<ul class="promo-calendar-page">
@foreach ($speedwaies as $key => $speedway)
    <?php $winerGroup = $speedway->winerGroup; ?>
    <li class="col-xs-12 col-sm-4 col-md-4">
        <a href="{{URL::route('benefit-event-motogp-2017detail', $speedway->station_name)}}" title="2017 MotoGP {{$speedway->station_name.$tail}}" target="_blank" style="box-shadow:none;-webkit-box-shadow:none">
            <div style="position:absolute;width:40%;height:95.3%;padding:3%;top:2.5%" class="{{$color[$key]}}">
                <img src="{{assetRemote('image/benefit/event/motogp/speedway/'.$speedway->name.'.jpg')}}" alt="【2017 MotoGP {{$speedway->name}}】台灣正賽時間{{$speedway->raced_at_local.$tail}}"><br>
                @if($speedway->active == 2)
                    <ul class="allteam allteamresult">
                        <li>
                            <h3>{{$speedway->name}}</h3>
                        </li>
                        <li>
                            <span>
                                比賽結果:
                            </span>
                        </li>
                        @foreach( $winerGroup as $key => $winer )
                            <li>
                                <span>
                                    第{{$key}}名：
                                </span>
                            </li>
                            <li>
                                <span>
                                    {{$winer ? $winer->name : ''}}
                                </span>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <ul class="allteam">
                        <li>
                            <h3>{{$speedway->name}}</h3>
                        </li>
                        <li>
                            <span class="visible-lg visible-md">
                                正賽當地時間：
                            </span>
                            <span class="visible-sm visible-xs">
                                當地時間<br>
                            </span>
                        </li>
                        <li>
                            <span class="visible-lg visible-md">
                                {{date('m/d h:i A',strtotime($speedway->raced_at))}}
                            </span>
                            <span class="visible-sm visible-xs">
                                {{date('m/d H:i',strtotime($speedway->raced_at))}}
                            </span>
                        </li>
                        <li>
                            <span class="visible-lg visible-md">
                                正賽台灣時間：
                            </span>
                            <span class="visible-sm visible-xs">
                                台灣時間<br>
                            </span>
                        </li>
                        <li>
                            <span class="visible-lg visible-md">
                                {{date('m/d h:i A',strtotime($speedway->raced_at_local))}}
                            </span>
                            <span class="visible-sm visible-xs">
                                {{date('m/d H:i',strtotime($speedway->raced_at_local))}}
                            </span>
                        </li>
                    </ul>
                @endif
            </div>
            <img src="{{assetRemote('image/benefit/event/motogp/speedway/banner/'.$speedway->name.'.jpg')}}" alt="【2017 MotoGP {{$speedway->name}}】台灣正賽時間{{$speedway->raced_at_local.$tail}}">
            {{-- <span>{{$now_speedway->station_name}}賽事概要</span> --}}
        </a>
    </li>
@endforeach
</ul>