@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{assetRemote('css/benefit/event/motogpevent.css')}}">s
    <style>
        .motogpinfo {
            width:100%;
            border:1px solid black;
            margin-bottom:30px;
        }
        .ct-activitiesa-award img {
            width:100%;
        }
        .ct-activitiesa-award li span {
            height: 150px;
            overflow: auto;
        }
        .ct-activitiesa-award li h2{
            height: 85px;
            margin: 15px 0 10px;
            padding: 5px 0;
        }
        @media screen and (max-width: 767px) {
            .ct-activitiesa-award li {
                float:none;
                width:100%;
                clear:both;
                margin-left:0px !important;
            }
            .ct-activitiesa-award span {
                word-break: break-all;
                min-height:0;
            }
            .container-moto-gp-event .ul-ct-guessing-method li{
                width:100%;
                display:block;
                margin-top:10px;
            }
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left{
            width:100%;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left span{
            width:90%;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:after{
            content:none;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:before{
            width:10%;
        }
        .returnbtn .returnbtnhref{
            display:block;
        }
        .returnbtn .returnbtnhref:hover {
            opacity: 0.7;
            display:block;
            color:white;
        }

        .right-side-box{
            padding-right: 5%;
        }
        .left-side-box{
            padding-left: 5%;
        }
        .gp-box{
            margin-top: 40px;
            border: 2px #777 solid;
        }
        .gp-col{
            display: flex;
            justify-content: center;
            flex-direction: column;
            padding: 2%;
        }
        .gp-box-content{
            display: flex;
            background: #e7e7e7;
        }
        .gp-box-content h3{
            font-weight: bold;
            font-size: 1.2rem;
        }
        .gp-box-content ul li{
            padding: 15px;
            text-decoration: none;
            list-style: none;
        }
        .gp-box-title {
            height:40px;
        }
        .gp-box-title h2{
            font-weight: bold;
        }
        .gp-box-title h2 span{
            float: left;
            color: #fff;
            line-height: 30px;
            font-size: 1.2rem;
            font-weight: bold;
            background: #000;
            height:40px !important;
            padding-top:5px !important;
        }
        .gp-box-title h2 img{
            width: 99px;
            height: 30px;
            float: left;
            width:99px;
            height: 40px !important;
        }
        .gp-box-border{
            border:1px #ccc solid;
        }
        .box-green{
            background: rgba(0, 189, 3, 0.6);
        }
        .box-green1{
            background: rgba(0, 189, 3, 1);
        }
        .box-white{
            background: #fff;
        }
        .btn-winner {
            border: 2px solid #e61e25;
            color: #e61e25;
            width: 100%;
        }
        .btn-winner:hover {
            background-color: #e61e25;
            color: #fff;
        }
        .gp-col .box-white .result{
            float:none;
        }
        .gp-col .right-side-box .box-white li {
            display:block;
            clear:both;
            margin:2%;
        }
        .gp-col .right-side-box .box-white .border-radius-2{
            text-align: center;
            padding: 5px 10px;
            width: 40%;
            border: 2px solid #333333;
            font-weight: bold;
            text-transform: uppercase;
            min-height: 35px;
            float:left;
        }
        .gp-col .right-side-box .box-white .num-percent {
            width: 20%;
            line-height: 35px;
            float: left;
            text-align: center;
        }
        .gp-col .right-side-box .box-white .percent {
            margin: 12px 0;
            width: 40%;
            height: 10px;
            background-color: #efefef;
            float: left;
        }
        .gp-col .right-side-box .box-white .ct-percent {
            height: 10px;
            background-color: #ff6600;
            float: left;
        }
        .box-light-blue{
            background: rgba(3, 79, 137, 0.6);
        }
        .box-light-blue1{
            background: rgba(3, 79, 137, 1);
        }
        .box-light-red{
            background:rgba(227, 17, 16 ,0.6);
        }
        .box-light-red1{
            background:rgba(227, 17, 16 ,1);
        }
        .box-white{
            background: #fff;
        }
        .nail-box{
            position: relative;
        }
        .fly-box{
            position: absolute;
            width:450px;
        }
        .box-tall{
            height:100%;
        }
        .nail-left{
            left:10%;
        }
        .nail-right {
            right:10%;
        }
        .nail-top {
            top: 8.5%;
        }
        /*.nail-bottom {
            bottom: 8.5%;
        }*/
        .gp-box .line {
            height:5px;
            float:none;
        }
        .box-white .title-site-activity-forecast-right{
            font-size: 1.2rem;
            padding: 5px;
            width: 100%;
            background-color: #0066c0;
            color: #fff;
            text-align: center;
        }
        .box-white .note{
            width: 100%;
            padding: 5px;
            float: left;
            text-align:center;
            background-color: #efefef;
            bottom: 0;
        }
        .nail-box .info {
            float:right;
        }
        .gp-box-content .info-table {
            position: absolute;
            width: 100%;
            z-index: 2;
        }
        .row span {
            font-size: 1rem;
        }
        .row {
            margin-bottom:40px;
        }
        .btn-bottom {
            margin-bottom:40px;
        }
        .btn-bottom .btn-danger{
            width:200px !important;
            height:40px !important;
            padding-top:9px;
        }
        .ul-ct-guessing-method{
            font-size: 0;
        }
        .ct-activitiesa-award li span{
            display: block;
            float:none;
        }
        .box-about-the-track-right{
            padding: 1%;
        }

        @media(max-width: 992px){
            .ul-ct-guessing-method li{
                width:33.3333%;
            }
            .ul-ct-guessing-method li:nth-child(2), .ul-ct-guessing-method li:nth-child(4){
                display:none;
            }
        }
        @media(max-width: 767px){
            .box-about-the-track-right{
                padding: 2%;
            }
            .ct-activitiesa-award li{
                width:100%;
                margin-bottom: 20px;
            }
            .ct-activitiesa-award li:nth-of-type(2){
                margin: 0 0 20px 0;
            }
            ul.ct-activitiesa-award li span{
                border: 1px #ddd solid;
                padding:5px;
            }
        }
    </style>
@stop
@section('middle')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <img src="{{assetRemote('image/benefit/event/motogp/info/banner3.png')}}" alt="2017 MotoGP 冠軍大預測">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="gp-box thisspeedwayinfo">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span class="col-lg-3 col-md-4 col-sm-12 col-xs-12">2017 MotoGP 冠軍大預測</span>
                        <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP 冠軍大預測">
                    </h2>
                </div>
                <div class="gp-box-content clearfix" style="background: rgba(67,83,168,1);">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col" style="padding:2%">
                        <div class="box-white gp-box-border">
                            <div class="box-about-the-track-right col-xs-12 col-sm-12 col-md-12">
                                <div class="container-moto-gp-event2">
                        		<span>
							        【活動說明】預測2017MotoGP各分站前三名，猜對即可贏得免費點數，並有機會贏得K-3 SV Top Tartaruga 賽車安全帽、Webike套裝紀念貼紙及300元禮券。<br>
							        【活動時間】自2017/03/15日起開放答題，每站都可參加，詳見<a href="{{URL::route('benefit-event-motogp-2017').'#schedule'}}">各站活動時間表</a>。<br>
							        【活動資格】凡是「Webike-摩托百貨」完整會員皆可參加，若還沒註冊的會員，請按此 <a href="{{URL::route('customer-account-create')}}"> 免費加入</a>。<br>
							        【活動解答】MotoGP各站結束後隔日，我們會公布每站正確答案，結果以MotoGP官方結果為準。<br>
							        【活動規範】每個會員每站僅限答題一次，提交答案後無法修改，請小心回答。得獎者我們會以電話進行會員身分資料確認，若查證冒名者將會取消資格，並依據<a href="{{URL::route('customer-rule',['rule_code'=>'member_rule_3'])}}">會員規約</a>刪除會員帳號<br>
							    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="gp-box thisspeedwayinfo">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span class="col-lg-3 col-md-3 col-sm-12 col-xs-12">2017 MotoGP 活動獎勵</span>
                        <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP 活動獎勵">
                    </h2>
                </div>
                <div class="gp-box-content clearfix" style="background: yellow;">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col" style="padding:2%">
                        <div class="box-white gp-box-border">
                            <div class="box-about-the-track-right col-xs-12 col-sm-12 col-md-12">
                                <div class="container-moto-gp-event2">
                                    <ul class="ct-activitiesa-award">
                                        <li>
                                            <figure><img src="{{assetRemote('image/benefit/event/motogp/info/s1.jpg')}}" alt="2017 MotoGP 活動獎勵"></figure>

                                            <h2>猜對分站前三名現金點數免費拿</h2>
                                            <span>1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。 <br>
												2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。 <br>
												3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱現金點數說明 <br>
												4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在點數獲得及使用履歷中查詢。
												                    1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。 <br>
												2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。 <br>
												3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱<a href="http://www.webike.tw/customer/rule/pointinfo" title="現金點數說明 - 「Webike-摩托百貨」" target="_blank">現金點數說明</a> <br>
												4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在<a href="http://www.webike.tw/customer/history/points" title="點數獲得及使用履歷 - 「Webike-摩托百貨」" target="_blank">點數獲得及使用履歷</a>。
							                </span>
                                        </li>
                                        <li>
                                            <figure><img src="{{assetRemote('image/benefit/event/motogp/info/s2.jpg')}}" alt="2017 MotoGP 活動獎勵"></figure>

                                            <h2>分站冠軍全部答對AGV烏龜帽直接帶回家</h2>
                                            <span>1.只要您答對2017 MotoGP各分站冠軍，AGV K-3 SV Top Tartaruga 賽車安全帽直接帶回家，名額不限。<br>
							2.贈品規格<a href="http://www.webike.tw/sd/t00039080" title="【AGV】K-3 SV Top Tartaruga 全罩式安全帽 - 「Webike-摩托百貨」" target="_blank">AGV K-3 SV Top Tartaruga 全罩式安全帽/SIZE:L</a><br>
							3.答對名單會在第18站瓦倫西亞比賽結束後次日公布在此活動頁面。<br>
							</span>
                                        </li>
                                        <li>
                                            <figure><img src="{{assetRemote('image/benefit/event/motogp/info/s3.jpg')}}" alt="2017 MotoGP 活動獎勵"></figure>

                                            <h2>預測年度冠軍贈送300元禮券再抽AGV烏龜帽及Webike 紀念貼紙</h2>
                                            <span>1.連續18站參加活動，系統會自動累計總冠軍分數，若您預測的年度總冠軍與2017MotoGP總冠軍相同，即可獲得「Webike-摩托百貨」300元現金禮券，並可參加AGV K-3 SV Top Tartaruga安全帽、Webike 紀念貼紙抽獎。<br>
							2.答對者抽出<a href="http://www.webike.tw/sd/t00039080" title="【AGV】K-3 SV Top Tartaruga 全罩式安全帽 - 「Webike-摩托百貨」" target="_blank">AGV K-3 SV Top Tartaruga 全罩式安全帽/SIZE:L</a>一名、Webike紀念貼紙五名。<br>
							3.獲獎名單預定會在第18站瓦倫西亞比賽結束後次日公布在此活動頁面。若有提前封王的情況，我們將以封王該站為標準提前宣布獎名單。
							※為了公平起見，此好康僅限每站皆有參加的會員為主。</span>
                                        </li>
                                    </ul>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="gp-box thisspeedwayinfo">
                <div class="gp-box-title">
                    <h2 class="clearfix">
                        <span class="col-lg-3 col-md-4 col-sm-12 col-xs-12">2017 MotoGP 活動方式</span>
                        <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP 活動方式" >
                    </h2>
                </div>
                <div class="gp-box-content clearfix" style="background: rgba(67,83,168,1);">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col" style="padding:2%">
                        <div class="box-white gp-box-border">
                            <div class="box-about-the-track-right col-xs-12 col-sm-12 col-md-12">
                                <div class="container-moto-gp-event ct-guessing-method">
                                    <ul class="ul-ct-guessing-method">
                                        <li><img src="{{assetRemote('image/moto-gp/img-step-01.png')}}" alt="2017 MotoGP 活動方式"></li>
                                        <li><i class="fa fa-chevron-right visible-md visible-lg" aria-hidden="true"></i></li>
                                        <li><img src="{{assetRemote('image/moto-gp/img-step-02.png')}}" alt="2017 MotoGP 活動方式"></li>
                                        <li><i class="fa fa-chevron-right visible-md visible-lg" aria-hidden="true"></i></li>
                                        <li><img src="{{assetRemote('image/moto-gp/img-step-03.png')}}" alt="2017 MotoGP 活動方式"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center btn-bottom">
        <a class="btn btn-danger" href="{{URL::route('benefit-event-motogp-2017')}}">MotoGP預測總冠軍首頁</a>
    </div>
@stop