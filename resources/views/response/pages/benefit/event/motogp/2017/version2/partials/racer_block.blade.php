@foreach( $racers as $key => $racer )
    <tr>
        <td class="text-center">{{$racer->number}}</td>
        <td class="text-center"><img src="{{$photo_home}}country/{{$racer->country}}.png" alt="2017MotoGP【{{$racer->number}}】{{$racer->name}}國籍"></td>
        <td><a href="{{$racer->site}}" title="2017MotoGP【{{$racer->number}}】{{$racer->name}}" target="_blank" nofollow>{{$racer->name}}</a></td>
        <td><a href="{{$racer->team_site}}" title="2017MotoGP{{$racer->team_name}}" target="_blank" nofollow>{{$racer->team_name}}</a></td>
        <td class="text-center">{{$racer->motor_manufacturer_name}}</td>
        <td class="text-center">{{$racer->podium_quantity}}</td>
        <td class="text-center">{{$racer->champion_quantity}}</td>
        <td class="text-center">{{$racer->champion_lastyear}}</td>
        <td class="text-center">{{$racer->total_scores ? $racer->total_scores : 0}}</td>
        <td class="text-center">{{$racer->ranking ? $racer->ranking : '-'}}</td>
    </tr>
@endforeach