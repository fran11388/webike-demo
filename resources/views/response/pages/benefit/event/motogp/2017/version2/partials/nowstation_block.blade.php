<a href="{{URL::route('benefit-event-motogp-2017detail', $now_speedway->station_name)}}" title="{{$now_speedway->station_name.$tail}}" target="_blank">
    <div class="box-light-blue fly-box-cover">
        <img src="{{assetRemote('image/benefit/event/motogp/speedway/'.$now_speedway->name.'.jpg')}}" alt="【2017{{$now_speedway->name}}】台灣正賽時間{{$now_speedway->raced_at_local.$tail}}"><br>
        @if($now_speedway->active == 2)
            <ul class="allteam allteamresult">
                <li>
                    <h3>{{$now_speedway->name}}</h3>
                </li>
                <li>
                    <span>
                        比賽結果:
                    </span>
                </li>
                @if(is_null($now_speedway->numberOne) && is_null($now_speedway->numberTwo) && is_null($now_speedway->numberThree))
                    <li>
                        <span>
                            第1名：
                        </span>
                    </li>
                    <li>
                        <span>
                            第2名：
                        </span>
                    </li>
                    <li>
                        <span>
                            第3名：
                        </span>
                    </li>
                @else
                    <li>
                        <span>
                            第1名：
                        </span>
                    </li>
                    <li>
                        <span>
                            {{$now_speedway->numberOne['name']}}
                        </span>
                    </li>
                    <li>
                        <span>
                            第2名：
                        </span>
                    </li>
                    <li>
                        <span>
                            {{$now_speedway->numberTwo['name']}}
                        </span>
                    </li>
                    <li>
                        <span>
                            第3名：
                        </span>
                    </li>
                    <li>
                        <span>
                            {{$now_speedway->numberThree['name']}}
                        </span>
                    </li>
                @endif
            </ul>
        @else
            <ul class="allteam">
                <li>
                    <h3>{{$now_speedway->name}}</h3>
                </li>
                <li>
                    <span class="visible-lg visible-md">
                        正賽當地時間：
                    </span>
                    <span class="visible-sm visible-xs">
                        當地時間<br>
                    </span>
                </li>
                <li>
                    <span class="visible-lg visible-md">
                        {{date('m/d h:i A',strtotime($now_speedway->raced_at))}}
                    </span>
                    <span class="visible-sm visible-xs">
                        {{date('m/d H:i',strtotime($now_speedway->raced_at))}}
                    </span>
                </li>
                <li>
                    <span class="visible-lg visible-md">
                        正賽台灣時間：
                    </span>
                    <span class="visible-sm visible-xs">
                        台灣時間<br>
                    </span>
                </li>
                <li>
                    <span class="visible-lg visible-md">
                        {{date('m/d h:i A',strtotime($now_speedway->raced_at_local))}}
                    </span>
                    <span class="visible-sm visible-xs">
                        {{date('m/d H:i',strtotime($now_speedway->raced_at_local))}}
                    </span>
                </li>
        @endif
    </div>
    <img src="{{assetRemote('image/benefit/event/motogp/speedway/banner/'.$now_speedway->name.'.jpg')}}" alt="【2017{{$now_speedway->name}}】台灣正賽時間{{$now_speedway->raced_at_local.$tail}}">
    {{-- <span>{{$now_speedway->station_name}}賽事概要</span> --}}
</a>