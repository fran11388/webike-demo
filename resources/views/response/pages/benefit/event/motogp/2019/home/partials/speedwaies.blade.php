<div class="rider container-buttom-border speedwaies-container" id="speedwaies">
    <div class="out-width">
        <div class="text-center gp-title">
            <h2>2019 MotoGP 賽道資訊</h2>
        </div>
        <div class="rider-date clearfix">
            <div class="racer-return col-md-1 col-sm-1  col-xs-1 text-center cursor-pointer padding0">
                <span class="helper"></span>
                <div class="vertical-align-middle">
                    <i class="glyphicon glyphicon-triangle-left size-12rem" aria-hidden="true"></i>
                </div>
            </div>
            <ul class="clearfix owl-carousel-gp-racer-data">
                @foreach($speedwaies as $key => $speedway)
                    <li>
                        <div>
                            <a href="{{ \URL::route('benefit-event-motogp-2019detail',['station' => $speedway->station_name]) }}" title="{{ $speedway->station_name. '賽道資訊' }}" class="img-block" target="_blank">
                                <img src="{{assetRemote('image/benefit/event/motogp/2019/rider-speedway/speedway').'/'.($key+1).'.png'}}" alt="2019 MotoGP　{{ $speedway->station_name }}" class="origin-link-img">
                                @if(!$is_phone)
                                    <img src="{{assetRemote('image/benefit/event/motogp/2019/rider-speedway/speedway-hover').'/'.($key+1).'.png'}}" alt="2019 MotoGP　{{ $speedway->station_name }} -更多資訊" class="hidden more-link-img" >
                                @endif
                            </a>
                            <div class="clearfix">
                                <div class=" col-md-4 col-sm-4 col-xs-4 text-right padding0">
                                    <img src="{{assetRemote('image/benefit/event/motogp/2019/rider-speedway/country').'/'.($key+1).'.png'}}">
                                </div>
                                <div class=" col-md-8 col-sm-8 col-xs-8 text-center padding0">
                                    <span class="font-bold">{{ $speedway->station_name }}</span>
                                </div>

                            </div>
                            <div class="text-center speedway-name">
                                <span class="font-bold">{{ $speedway->speedway_name }}</span>
                            </div>
                            @if(count($speedway->numberOne))
                                <div>
                                    <p>第一名</p>
                                    <p>{{ $speedway->numberOne ? $speedway->numberOne->name : '' }}</p>
                                </div>
                                <div>
                                    <p>第二名</p>
                                    <p>{{ $speedway->numberTwo ? $speedway->numberTwo->name : '' }}</p>
                                </div>
                                <div>
                                    <p>第三名</p>
                                    <p>{{ $speedway->numberThree ? $speedway->numberThree->name : '' }}</p>
                                </div>
                            @else
                                <div>
                                    <p class="size-08rem">台灣排位時間</p>
                                    <p class="size-08rem text-center">{{ date('m/d H:i',strtotime($speedway->rank_at_local)) }}</p>
                                </div>
                                <div class="box">
                                    <p class="size-08rem">台灣正賽時間</p>
                                    <p class="text-center size-08rem">{{ date('m/d H:i',strtotime($speedway->raced_at_local)) }}</p>
                                </div>
                                @if(strtotime(date('y-m-d H:i:s')) < strtotime($speedway->closested_at) and $speedway->active == 1)
                                    <div class="text-center font-color-red">
                                        <p class="size-08rem font-bold">投票進行中</p>
                                    </div>
                                @endif
                                @if($is_phone)
                                    <div class="text-center" style="margin-top:10px;">
                                        <a href="{{ \URL::route('benefit-event-motogp-2019detail',['station' => $speedway->station_name]) }}">
                                            <span class="font-bold">更多資訊</span>
                                        </a>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="racer-next col-md-1 col-sm-1  col-xs-1 text-center cursor-pointer padding0">
                <span class="helper"></span>
                <div class="vertical-align-middle">
                    <i class="glyphicon glyphicon-triangle-right size-12rem" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>