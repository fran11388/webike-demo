<div class="top clearfix container-buttom-border" id="racer-score-rank">
    <div class="out-width">
        <div class="text-center gp-title">
            <h2>2019 MotoGP 積分排行</h2>
        </div>
        <div class="top-ranking clearfix">
            @if(count($top10))
                <div class="ridertop col-xs-12 col-sm-8 col-md-8">
                    <h2 class="text-center">TOP10 車手積分排行</h2>
                    <div class="ridertop-rider">
                        <ul class="clearfix {{ $is_phone ? 'owl-carousel-gp-score-rank' : ''}}">
                            @php
                                $num = 1;
                            @endphp
                            @foreach($top10 as $key => $racer)
                                @if(in_array($num,[1,6]))
                                    <li class="clearfix col-xs-12 col-sm-4 col-md-6">
                                @endif
                                <div class="clearfix rank-content">
                                    <div class="col-xs-2 col-sm-2 col-md-2 rank-number text-center">
                                        <span class="helper"></span>
                                        <img class="vertical-align-middle" src="{{assetRemote('image/benefit/event/motogp/2019/integral/rankingnum/').'/'.$num.'.png'}}">
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        <span class="helper"></span>
                                        <a href="{{route('benefit-event-motogprider-2019', $racer->number)}}" target="_blank" title="{{ $racer->number. '  '.$racer->name.' 車手資訊' }}" target="_blank">
                                            <img class="vertical-align-middle" src="{{assetRemote('image/benefit/event/motogp/2019/integral/ridernum').'/'.$racer->number.'.png'}}" alt="{{ '2019 MotoGP 【'.$racer->number.'】 '.$racer->name }}">
                                        </a>
                                    </div>
                                    <div class="col-xs-5 col-sm-5 col-md-5 padding0">
                                        <span class="helper"></span>
                                        <div class="vertical-align-middle">
                                            <p class="font-bold"><a href="{{route('benefit-event-motogprider-2019', $racer->number)}}" target="_blank" title="{{ $racer->number. '  '.$racer->name.' 車手資訊' }}">{{ $racer->name }}</a></p>
                                            <img src="{{assetRemote('image/benefit/event/motogp/2019/integral/nationality').'/'.$racer->number.'.png'}}" alt="{{ '2019 MotoGP 【'.$racer->number.'】 '.$racer->name.'國籍' }}">
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 padding0">
                                        <span class="helper"></span>
                                        <p class="vertical-align-middle font-bold">{{ $racer->total_scores ? number_format($racer->total_scores) : 0 }}</p>
                                    </div>
                                </div>
                                @if(in_array($num,[5,10]))
                                    </li>
                                @endif
                                @php
                                    $num++;
                                @endphp
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if(count($teamData))
                <div class="col-xs-12 col-sm-4 col-md-4 team-container">
                    <div class="teamtop">
                        <h2 class="text-center">TOP5 車隊積分排行</h2>
                        <ul class="owl-carousel-gp-score-rank">
                            @php
                                $rank_imgs = [assetRemote('image/benefit/event/motogp/2019/integral/team-outborder/numberOne.png'),assetRemote('image/benefit/event/motogp/2019/integral/team-outborder/numberTwo.png'),assetRemote('image/benefit/event/motogp/2019/integral/team-outborder/numberThree.png')]
                            @endphp
                            <li>
                                @foreach($teamData as $key => $team)
                                    <div class="teamtop-team clearfix box">
                                        <div class="col-xs-12 col-sm-3 col-md-3 padding0 team-img">
                                            <span class="helper"></span>
                                            <div class="vertical-align-middle team-img-block">
                                                <img class="team-rank-img" src="{{ array_key_exists($key,$rank_imgs) ? $rank_imgs[$key] : assetRemote('image/benefit/event/motogp/2019/integral/team-outborder/other.png') }}">
                                                <img class="team-images" src="{{ $team->team_img }}" alt="{{ '2019 MotoGP-'.$team->team_name.'-車隊積分' }}">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8 padding0 team-title">
                                            <span class="helper"></span>
                                            <span class="vertical-align-middle font-bold">{{ $team->team_name }}</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-1 col-md-1 padding0 team-score">
                                            <span class="helper"></span>
                                            <span class="vertical-align-middle font-bold size-08rem">{{ $team->sum ? number_format($team->sum) : 0 }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </li>
                            <li>
                                @foreach($makerData as $key=> $maker)
                                    <div class="teamtop-team clearfix box">
                                        <div class="col-xs-12 col-sm-3 col-md-3 padding0 team-img">
                                            <span class="helper"></span>
                                            <div class="vertical-align-middle team-img-block">
                                                <img class="team-rank-img" src="{{ array_key_exists($key,$rank_imgs) ? $rank_imgs[$key] : assetRemote('image/benefit/event/motogp/2019/integral/team-outborder/other.png') }}">
                                                <img class="team-images" src="{{ assetRemote('image/benefit/event/motogp/2019/integral/maker'.'/'.$maker->motor_manufacturer_name.'.png') }}" alt="{{ '2019 MotoGP-'.$maker->motor_manufacturer_name.'-車廠積分' }}">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8 padding0 team-title">
                                            <span class="helper"></span>
                                            <span class="vertical-align-middle font-bold">{{ $maker->motor_manufacturer_name }}</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-1 col-md-1 padding0 team-score">
                                            <span class="helper"></span>
                                            <span class="vertical-align-middle font-bold size-08rem">{{ $maker->sum ? number_format($maker->sum) : 0 }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>