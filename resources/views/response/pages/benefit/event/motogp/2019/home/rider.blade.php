@extends('response.layouts.1column')
@section('style')
<style>
   .motogprider-container .rider-banner-block .phone-img{
        background-color: #636260;
        text-align: center;
    }
    ol.breadcrumb-product-detail{
    	margin: -1px auto!important;
    }
	.motogprider-container{
		margin-top: -15px;
		background-color: #e9e9e9;
	}
	.motogprider-container .motogp-menu {
		width: 100%;
		border-top: 1px solid #d0d0d0;
		border-bottom: 1px solid #d0d0d0;
		background-color: white;
	}
	.motogprider-container .motogp-menu .overflow-menu li h3{
		color: #636260;
		font-weight: bold;
	}
	.motogprider-container .out-width {
		max-width: 1230px !important;
		margin: 0 auto;
	}
	.motogprider-container .container-buttom-border{
		border-bottom: 4px solid #dddddd;
		padding: 30px 0px;
	}
	.motogprider-container .gp-title {
		margin-bottom: 30px;
	}
	.motogprider-container .gp-rider-name{
		margin-bottom: 10px;
	}
	.motogprider-container .gp-title h2 {
		font-weight: bold;
	}
	.motogprider-container .motogp-menu .container li:hover, .motogprider-container .motogp-menu .container li.active {
		padding-bottom: 0px;
		border-bottom: 5px solid red;
		background-color: white;
	}
	.motogp-menu .overflow-menu-ui ul li {
		padding: 5px;
	}
	.motogprider-container .motogp-menu .container li .list-menu {
		padding: 5px 13px;
		color: #2a313c;
		float: left;
		transition: all 0s ease !important;
	}
	.motogprider-container .motogp-menu .container li:hover a, .motogprider-container .motogp-menu .container li.active a {
		color: red;
	}
	.top-shopping .container{
		max-width: none !important;
		padding: 0px;
	}
	.motogprider-container .motogp-menu .container{
		max-width: 1230px !important;
	}
	.motogprider-container h2{

	}
	.motogprider-container .rider-data{
		height: 420px;
		overflow-y: hidden;
	}
	.motogprider-container .rider-data table{
		margin-left: auto;
		margin-right: auto;
		table-layout: fixed;
		word-break: break-all;
		background: #fff;
	}
	.motogprider-container .rider-data table tr td,
	.motogprider-container .rider-data table tr th{
		text-align: center;
		overflow: hidden;
	}
	.motogprider-container .rider-data .rider-data-table .table-data-title div{
		font-size: 1rem;
		font-weight: bold;
	}
	.motogprider-container .rider-data .inforamtion table tr td div,
	.motogprider-container .rider-data .inforamtion table tr th div{
		width: 150px;
		height: 30px;
		line-height: 30px;
	}
	.motogprider-container .rider-data .history table tr td div,
	.motogprider-container .rider-data .history table tr th div{
		width: 70px;
		height: 30px;
		line-height: 30px;
	}
	.motogprider-container .rider-data .current table tr td div,
	.motogprider-container .rider-data .data-area.current table tr th div{
		width: 110px;
		height: 20px;
		line-height: 20px;
	}
	.motogprider-container .rider-data-group{
		margin-top: 20px;
	}
	.motogprider-container .rider-data-group .slick-dots{
		top: -30px;
	}
	.motogprider-container	.pager-link-block{
		display: inline-block;
	}
	.motogprider-container .pager-link-block span{
	    text-indent: -9999px;
	    display: block;
	    width: 10px;
	    height: 10px;
	    margin: 0 5px;
	    outline: 0;
	    border-radius: 5px;
	    background: #1ca28a;
	    cursor: pointer;
	    opacity: 0.5;
	}
	.motogprider-container .slick-prev, .motogprider-container .slick-next{
		z-index: 99;
	}
	.motogprider-container .slick-prev{
		left:5%;
	}
	.motogprider-container .slick-next{
		right:5%;
	}
	.chart-box{
		width: 100%;
		overflow-x: auto;
		overflow-y: hidden;
	}
	.chart-cover{
		width: 1000px;
		margin: 0 auto;
	}
	@media (max-width: 767px){
		.motogprider-container .rider-data{
			height: auto;
		}
		.motogprider-container .rider-data-area{
			padding: 3%;
		}
		/*.motogprider-container .rider-data .rider-data-table{*/
			/*width: 100%;*/
		/*}*/
	}
	.motogprider-container .motogp-menu.fixed{
		position: fixed;
		top: 69px;
		z-index: 9999;
		background-color: #efefef;
	}
	.motogp-menu .overflow-menu li:hover .link-top, .motogp-menu .history-gp-link:hover .list-menu h3,.motogp-menu .overflow-menu li.active .list-menu h3{
		color:red;
	}
	.rider-banner-brec {
		position: absolute;
		top: 0px;
		width: 100%;
		height: 100%;
	}
	.rider-banner-block{
		position: relative;
		margin-top: 10px;
	}
	.rider-banner-block .breadcrumb-mobile {
		max-width: 1230px;
		margin-left: auto;
		margin-right:auto;
	}
	.rider-banner-block .breadcrumb-mobile li a ,.rider-banner-block .breadcrumb-mobile li.active span{
		color:white;
	}
	.rider-banner-block .breadcrumb-mobile li .list-menu:hover{
		color:red;
	}
	.container-buttom-border {
		border-bottom:0px !important;
	}
	.return-home {
		padding-bottom:100px;
	}

	@media(max-width:550px){
		.xs-width-full{
			width:100%;
		}

		.motogprider-container .container-buttom-border{
			padding:0px 0px 30px 0px;
		}
		.motogprider-container .motogp-menu .out-width {
			height:42px;
		}
		.return-home {
			padding-bottom:50px;
		}
		.xs-width-full{
			width:100%;
		}
		.overflow-menu-ui .overflow-menu.list-menu{
			position:relative;
			padding-right:25px;
			height:42px;
			overflow-y:hidden;
		}
		.overflow-menu-ui .list-menu.active{
			overflow-y: inherit;
		}
		.overflow-menu-ui .overflow-menu.list-menu .list-row{
			width: 25px;
			position: absolute;
			right: 0%;
			top: 0%;
			height: 42px;
			text-align: center;
		}
	}
	#rider-ranking {
		padding:0 10px;
	}
   @media(min-width:768px){
	   .bannerbar {
		   display:none;
	   }
	   .motogprider-container {
		   margin-top:145px;
	   }
   }
   .bannerbar-phone {
	   display:none !important;
   }
	select {
		background:white;
	}



   .motogp-menu {
	   width: 100%;
	   border-top: 1px solid #d0d0d0;
	   border-bottom: 1px solid #d0d0d0;
	   background-color:white;
   }
   .motogp-menu .container li:hover .list-menu,.motogp-menu .container li.active .list-menu{
	   color: red;
   }
   .motogp-menu .container li:hover,.motogp-menu .container li.active{
	   padding-bottom:0px;
	   border-bottom:  5px solid red;
	   background-color: white;
   }
   .btn-asking{
	   border: 0px;
   }
   .motogp-menu .container li .list-menu{
	   padding: 5px 13px;
	   color: #2a313c;
	   float: left;
	   transition: all 0s ease !important;
   }
   .motogp-menu .overflow-menu-ui ul li{
	   padding:5px;
   }
   .motogp-menu .list-history-row{
	   height:42px;
	   position:absolute;
	   right:11%;
	   margin-top:-6px;
	   cursor:pointer;
   }
   .motogp-menu .history-race:hover{
	   position:relative;
   }
.motogp-menu .history-race:hover ul{
	   display:block !important;
	   position:absolute;
	   top:42px;
	   z-index:20;
	   left:0px;
	   width: 190px;
	   height: 238px;
	   overflow: auto;
   }
   .motogp-menu .history-race:hover ul li{
	   display:block;
	   list-style-type: none;
	   border: 1px solid #ddd;
	   cursor:pointer;
	   background-color: white !important;
   }
   .motogp-menu .history-race:hover ul li:hover{
	   background-color:#d5d5d5 !important;
	   border-bottom: 1px solid #ddd !important;
	   padding-bottom:5px !important;
   }
   .motogp-menu .overflow-menu li h3{
	   color: #636260;
	   font-weight:bold;
   }
   .motogp-menu .overflow-menu li.active .list-menu h3{
	   color:red;
   }
   .motogp-menu .history-race{
	   position:relative;
	   padding-right:30px !important;
   }
   .motogp-menu .history-race .history-gp-link{
        padding-left: 15px;
    }
   .motogp-menu .history-race .history-gp-link{
        padding-left: 15px;
    }
   .motogprider-container .all-rider h2{
   		font-size: 1.2rem;
   }
</style>
@stop
@section('middle')
<div class="motogprider-container">
	<div class="rider-banner-block">
		@if($is_phone)
			<div class="phone-img">
				<img src="{{assetRemote('image/benefit/event/motogp/2019/phone-rider-banner/' . $racerInfos->racer->number . '.jpg')}}" alt="2019 MotoGP {{'【' . $racerInfos->racer->number . '】' . $racerInfos->racer->name}}">
			</div>
		@else
			<div>
				<img src="{{assetRemote('image/benefit/event/motogp/2019/rider-banner/' . $racerInfos->racer->number . '.jpg')}}" alt="2019 MotoGP {{'【' . $racerInfos->racer->number . '】' . $racerInfos->racer->name}}">
			</div>
		@endif
		<div class="rider-banner-brec"></div>
	</div>
	<div class="motogp-menu clearfix">
	    <div class="container out-width">
	        <div class="overflow-menu-ui">
	            <ul class="overflow-menu list-menu">
	                <li>
	                    <a href="{{route('benefit-event-motogp-2019')}}" title="2019 MotoGP" class="list-menu">
	                        <h3 class="link-top">2019 MotoGP 首頁</h3>
	                    </a>
	                </li>
	                <li class="active" data-element="pagetop">
	                    <a class="anchor list-menu" href="javascript:void(0)" >
	                        <h3 class="link-top">車手簡歷</h3>
	                    </a>
	                </li>
	                <li class="anchor" data-element="slick-slide00">
	                    <a class="list-menu" href="javascript:void(0)" >
	                        <h3 class="link-top">生涯概要</h3>
	                    </a>
	                </li>
	                <li class="anchor" data-element="slick-slide01">
	                    <a class="list-menu" href="javascript:void(0)" >
	                        <h3 class="link-top">歷年成績</h3>
	                    </a>
	                </li>
	                <li class="xs-width-full anchor" data-element="slick-slide02">
	                    <a class="list-menu" href="javascript:void(0)" >
	                        <h3 class="link-top">目前成績</h3>
	                    </a>
	                </li>
	                <li class="xs-width-full anchor" data-element="rider-ranking" >
	                    <a class="list-menu" href="javascript:void(0)" >
	                        <h3 class="link-top">車手積分排行榜</h3>
	                    </a>
	                </li>
					<li class="xs-width-full visible-xs hidden-sm hidden-md anchor" data-element="more-rider">
						<a class="list-menu" href="javascript:void(0)" >
							<h3 class="link-top">車手一覽</h3>
						</a>
					</li>
					<li onclick="slipTo1('#')" class="xs-width-full history-race hidden-xs">
						<a href="javascript:void(0)" title="2019 MotoGP 冠軍預測活動.{{ $tail }}" class="history-link menu-link list-menu" target="_blank">
							<h3 class="link-top">車手一覽</h3>
						</a>
						<div class="list-history-row">
							<span class="helper"></span>
							<img src="{{ assetRemote('image/benefit/event/motogp/2019/left-row.png') }}" class="vertical-align-middle">
						</div>
						@if(!$is_phone)
							<ul class="hidden">
								@foreach($racers as $racer)
									<a href="{{route('benefit-event-motogprider-2019', $racer->number)}}" target="_blank" title="{{ $racer->number. '  '.$racer->name.' 車手資訊' }}" class="img-block">
										<li class="clearfix history-gp-link text-center">
											<h3 class="text-left">{{ $racer->number . ' ' . $racer->name }}</h3>
										</li>
									</a>
								@endforeach
							</ul>
						@endif
					</li>
					<div class="list-row visible-xs">
						<span class="helper"></span>
						<img src="{{ assetRemote('image/benefit/event/motogp/2019/left-row.png') }}" class="vertical-align-middle">
					</div>
	            </ul>
	        </div>
	    </div>
	</div>
	<div id="motogp-content">
		<div class="container-buttom-border" id="rider-container">
			<div class="rider-data-area block">
				@php
					$dataStyle = [
						'生涯概要' => 'inforamtion',
						'歷年成績' => 'history',
						'目前成績' => 'current',
					];
					$count = 0;
				@endphp
				<div class="text-center font-bold gp-rider-name">
					<h1>{{$racerInfos->racer->name}}</h1>
				</div>
				<div class="text-center gp-title hidden-xs">
					<h2>生涯概要</h2>
				</div>
				<div class="rider-data-group">
					@foreach($racerInfos->infoData as $type => $data)
						<div class="rider-data text-center {{ "slick-slide0".$count }}">
							<h2 class="hidden-lg hidden-md hidden-sm font-bold box">
								{{$type}}
							</h2>
							<div class="{{$dataStyle[$type]}} table-responsive">
								<table class="rider-data-table table-bordered">
									<thead>
									<tr class="hidden">
										<th class="table-data-title text-left" colspan="{{count($data[0])}}"><div>{{$type}}</div></th>
									</tr>
									<tr>
										@foreach($data[0] as $column)
											<th><div>{{$column}}</div></th>
										@endforeach
									</tr>
									</thead>
									<tbody>
									<?php unset($data[0]); ?>
									@if($type == '目前成績' and !count($data))
										@foreach(\Ecommerce\Repository\MotoGp\Gp2019\MotogpRepository::getspeedwaies() as $speedway)
											<tr>
												<td><div>{{$speedway->id}}</div></td>
												<td><div>{{date('m月d日', strtotime($speedway->raced_at_local))}}</div></td>
												<td><div>{{$speedway->name}}</div></td>
												<td><div> - </div></td>
												<td><div> - </div></td>
												<td><div> - </div></td>
											</tr>
										@endforeach
									@else
										@foreach($data as $rowData)
											<tr>
												@foreach($rowData as $value)
													<td><div>{{$value}}</div></td>
												@endforeach
											</tr>
										@endforeach
									@endif
									</tbody>
								</table>
							</div>
						</div>
						@php
							$count++;
						@endphp
					@endforeach
				</div>
			</div>
			<div id="rider-ranking" class="rider-ranking">
				<div class="text-center gp-title all-rider">
					<h2>2019 MotoGP 車手積分排行榜</h2>
				</div>
				<div class="chart-box">
					<div class="chart-cover">
						<div id="chart" class="ct-lineChart text-center"></div>
					</div>
				</div>
			</div>
			<div id="more-rider" class="more-rider rider-ranking clearfix visible-xs hidden-sm hidden-md" style="margin-top:30px;padding:0 15px;">
				<div class="text-center gp-title all-rider">
					<h2>車手一覽</h2>
				</div>
				<div class="">
					<select class="select2" onchange="location = this.value;">
						<option value="">車手一覽</option>
						@foreach($racers as $racer)
							<option value="{{route('benefit-event-motogprider-2019', $racer->number)}}">{{ $racer->number . ' ' . $racer->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<div class="width-full text-center return-home">
			<a href="{{ \URL::route('benefit-event-motogp-2019') }}" class="btn btn-warning">回活動首頁</a>
		</div>
	</div>
</div>
@stop
@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    $('.motogp-menu .overflow-menu li').click(function(){
        $('.motogp-menu .overflow-menu li').removeClass('active');
        $(this).addClass('active');
        $('.out-width .overflow-menu-ui .overflow-menu.list-menu').removeClass('active');
    });

	$(window).scroll(function(){
		if($(window).width() > 550){
			if ($(this).scrollTop() > (parseInt($('#motogp-content').offset().top) - $('#mainNav .container-header').height() - $('.motogp-menu').height())) {
				$('#mainNav .menu-shopping .container').css("cssText",'display:none !important');
				$('.motogp-menu').addClass('fixed');
				if($('#mainNav').hasClass('nav-down')){
					$('.motogp-menu.fixed').css('top','108px');
				}else{
					$('.motogp-menu.fixed').css('top','69px');
				}
			}else if($(this).scrollTop() < $('#motogp-content').height()){
				$('#mainNav .menu-shopping .container').css("cssText",'display:block !important');
				$('.motogp-menu').removeClass('fixed');
			}
		}
	});
    $('.breadcrumb-mobile').hide().clone().appendTo('.rider-banner-brec').show();

	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {

		var options = {
			titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
			lineWidth: 1,
			// colors: ['#2864D1', '#D10501'],
			chartArea: {
				left: 80,
				right: 25,
				top: 50
			},
			width: 1000,
			height: 300,
			pointSize: 6,
			hAxis: {
				baseline: 0,
				slantedText: true,
				slantedTextAngle:30,
				title: '賽次'
			},
			vAxis: {
				viewWindow: {
					min: 0
				},
				baseline: 0,
				title: '累積積分'
			},
			legend: {
				position: 'top'
			},
			tooltip: {
				isHtml: true,
				textStyle: {
					fontSize: 13
				}
			},
			focusTarget: 'category',
			'areaOpacity': 0.1,
			annotations: {
				textStyle: {
					fontName: 'Times-Roman',
					fontSize: 13,
					bold: true,
					italic: true,
				}
			},
		};

		var data = new google.visualization.DataTable();
		data.addColumn('string', '站別');

		@foreach($top10 as $racer)
            data.addColumn('number', '【{{$racer->number}}】{{$racer->name}}');
		@endforeach

        data.addRows([
			<?php
			$scores = array();
			$last_top10_str = '';
			for($i = 1 ; $i <= 19 ; $i++){
				foreach($top10 as $key => $racer){
					if( !isset($scores[$key]) ){
						$scores[$key] = 0;
					}
					$top10_score->filter(function($racer_score) use($racer, $i, $key, &$scores){
						if($racer_score->speedway_id == $i and $racer_score->racer_id == $racer->id){
							$scores[$key] += number_format($racer_score->score);
						}
					});
				}
				$top10_str = implode(',', $scores);
				if($top10_str !== $last_top10_str){
					$last_top10_str = $top10_str;
					echo '["第'.($i).'站", '.$top10_str.'],';
				}
			}
			?>
		]);

		var formatter = new google.visualization.PatternFormat('{1}');
		formatter.format(data, [0, 1], 1);
		formatter.format(data, [0, 2], 2);

		var chart = new google.visualization.LineChart(document.getElementById('chart'));
		chart.draw(data, options);

	}
</script>
<script>
	$(document).ready(function(){
		$('.overflow-menu .anchor').click(function(){
			var id = $(this).attr('data-element');
			if($(window).width() > 550){
                var y = parseInt($('.' + id).offset().top) - $('#mainNav .container-header').height() - $('.motogp-menu').height() - 100;
			}else{
                var y = parseInt($('.' + id).offset().top) - $('#mainNav .navbar-header').height();
			}

			if(id != 'pagetop'){
				$('html,body').animate({scrollTop: y}, 400);
			}
			$('#' + id).click();
		});
        $('.motogp-menu .overflow-menu li').click(function(){
            $('.motogp-menu .overflow-menu li').removeClass('active');
            if(!$(this).hasClass('active')){
                $(this).addClass('active');
            }
        });

		$('.rider-data-group').slick({
			dots: true,
			responsive: [
				{
					breakpoint: 767,
					settings: "unslick"
				}
			]
		});

		$('.rider-data-group button').click(function(){
			resetHeight();
		});
		$('.rider-data-group .slick-dots').click(function(){
			resetHeight();
		});

		function resetHeight(){
			var active = $('.rider-data-group .slick-active');
			var title = active.find('.table-data-title').text();
			$('.rider-data-area h2').text(title);
			var slick_height = active.find('table').height() + 5;
			$('.rider-data-group').height(slick_height);
			active.height(slick_height);
		}

//		$('.owl-carousel-promo').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>')
//		$('.owl-carousel-promo').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>')
	});
    $('.list-row').click(function(){
        if(!$('.out-width .overflow-menu-ui .overflow-menu.list-menu').hasClass('active')){
            $('.out-width .overflow-menu-ui .overflow-menu.list-menu').addClass('active');
        }else{
            $('.out-width .overflow-menu-ui .overflow-menu.list-menu').removeClass('active');
        }
    });
</script>
@stop