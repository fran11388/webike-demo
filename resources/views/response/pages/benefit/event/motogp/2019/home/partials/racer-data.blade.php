<div class="rider container-buttom-border" id="racer-data">
    <div class="out-width">
        <div class="text-center gp-title">
            <h2>2019 MotoGP 車手資訊及成績</h2>
        </div>
        <div class="rider-date clearfix rider-data-content">
            @if(count($racers))
                <div class="racer-return col-md-1 col-sm-1  col-xs-1 text-center cursor-pointer padding0">
                    <span class="helper"></span>
                    <div class="vertical-align-middle">
                        <i class="glyphicon glyphicon-triangle-left size-12rem" aria-hidden="true"></i>
                    </div>
                </div>
                <ul class="clearfix owl-carousel-gp-racer-data col-md-10">
                    @foreach($racers as $racer)
                        <li>
                            <div class="text-center">
                                <a href="{{route('benefit-event-motogprider-2019', $racer->number)}}" target="_blank" title="{{ 
                                    $racer->number. '  '.$racer->name.' 車手資訊' }}" class="img-block">
                                    <img src="{{assetRemote('image/benefit/event/motogp/2019/rider-speedway/ridernum').'/'.$racer->number.'.png'}}" alt="{{ '2019 MotoGP 【'.$racer->number.'】 '.$racer->name }}" class="origin-link-img">
                                    @if(!$is_phone)
                                        <img src="{{assetRemote('image/benefit/event/motogp/2019/rider-speedway/ridernum-hover').'/'.$racer->number.'.png'}}" alt="{{ '2019 MotoGP 【'.$racer->number.'】 '.$racer->name.'-更多資訊' }}" class="hidden more-link-img">
                                    @endif
                                </a>
                                <p>
                                    <a href="{{route('benefit-event-motogprider-2019', $racer->number)}}" target="_blank" title="{{
                                        $racer->number. '  '.$racer->name.' 車手資訊' }}">{{ $racer->name }}</a>
                                </p>
                                <div class="clearfix racer_detail">
                                    <div class="rider-nationality">
                                        <img src="{{assetRemote('image/benefit/event/motogp/2019/integral/nationality/').'/'.$racer->number.'.png'}}" alt="{{ '2019 MotoGP '.$racer->number.' '.$racer->name.'國籍' }}">
                                    </div>
                                    <a href="{{ $racer->site }}" title="{{ $racer->number.' '.$racer->name.'-facebook' }}" target="_blank" rel="nofollow">
                                        <div class="rider-facebook">
                                            <img src="{{assetRemote('image/benefit/event/motogp/2019/rider-speedway/	Facebook.png')}}" alt="{{ '2019 MotoGP '.$racer->name.'-facebook' }}">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="racer-next col-md-1 col-sm-1  col-xs-1 text-center cursor-pointer padding0">
                    <span class="helper"></span>
                    <div class="vertical-align-middle">
                        <i class="glyphicon glyphicon-triangle-right size-12rem" aria-hidden="true"></i>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>