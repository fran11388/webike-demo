@extends('response.layouts.1column')
@section('style')
<style>
    .container .breadcrumb-mobile{
        margin-bottom: -40px;
    }
    ol.breadcrumb-product-detail li a, ol.breadcrumb-product-detail .active span{
        color: white;
    }
    ol.breadcrumb-product-detail li a:hover{
        color: #e61e25;
    }
    .info-banner .breadcrumb-mobile{
        margin-bottom: 0px;
    }
     .motogp-info .banner-img .phone-banner{
        position: relative;
        height: 300px;
        background:url("{{assetRemote('image/benefit/event/motogp/2019/BANNER.jpg')}}");
        background-size: 150% 100%;

    }
    .motogp-info .banner-img .phone-banner .phone-text{
        width: 100%;
        color: white;
        position: absolute;
        top: 43%;
    }
    .motogp-info .banner-img .phone-banner .phone-text span{
        font-size: 1.5rem;
    }
    .banner-img .info-banner .breadcrumb-mobile ol.breadcrumb-product-detail{
        margin: 1px auto;
        max-width: 1230px;
    }
	.top-shopping .container{
        max-width: none !important;
        padding: 0px;
   	}
   	.container.out-width{
		max-width: 1230px !important;
   	}
    .motogp-info .banner-img{
        position: relative;
    }
    .motogp-info .info{
    	width: 1230px;
    	margin-left: auto;
    	margin-right: auto;
    }
    .info-container{
    	background-color: #e9e9e9
    }
    .motogp-info .info h2{
    	text-align: center;
    	font-weight: bold;

    }
	.motogp-info .info ul{
		list-style: none;
	}
	.info  .pager-link-block{
        display: inline-block;
    }
    .info  .pager-link-block span{
        text-indent: -9999px;
        display: block;
        width: 10px;
        height: 10px;
        margin: 0 5px;
        outline: 0;
        border-radius: 5px;
        background: #000;
        cursor:pointer;
        opacity: 0.5;
    }

   	.info  .pager-link-block.active span{
        background: #000;
        opacity: 1;
    }
    .info .pager-link-block{
        display: inline-block;
    }
   .info-container .info-depiction{
	    margin-top: 30px;
   		background-color: white;
    	padding: 20px 30px;
   	}
   	.info-container .info-depiction p{
   		margin-top: 5px;
   		font-size: 1rem; 
   	}
   	.info-container .info-depiction p:nth-child(1){
   		margin-top: 0px; 
   	}
   .motogp-info	.info  .title {
   		margin-bottom: 30px;
   		margin-top: 30px;
        
   	}
    .motogp-info    .info  .title h2{
        font-size: 1.5rem;
    }
   .motogp-info	.info .info-play  {
   		margin-top: 30px;
             
   	}
    .motogp-info .info .info-play h2{
        font-size: 1.5rem;
    }
   .info-container .info .owl-carousel-motogp  .owl-stage .owl-item img{
		width: 100%;
   	}
    .info-container .info .owl-carousel-motogp .owl-next{
        top: 50%
    }
    .info-container .info .owl-carousel-motogp .owl-prev{
        top: 50%
    }
   	.info-container  .motogp-award .motogp-award-depiction ul li{
   		list-style: none;
		float: left;
		width: 30%;
   	}
   	.info-container  .motogp-award .motogp-award-depiction ul li img{
   		width: 100%;
   	}
   	.info-container  .motogp-award .motogp-award-depiction ul li:nth-child(2){
   		margin: 0px 5%;
   	}
   	.info-container  .motogp-award .motogp-award-depiction ul li h2{
		margin: 10px 0px;
   	}
	.info-container  .motogp-award .motogp-award-depiction ul li span.red-color{
		color: red;
	}
	.info-container  .motogp-award .motogp-award-depiction .depiction{
		height: 150px;
		overflow: auto;
		font-size: 1rem;
	}
	.info-container  .btn-bottom{
		margin: 30px 0px 100px 0px; 
	}
	.info-container  .btn-bottom a{
		width: 200px;
		font-size: 1rem; 
	}
	@media(max-width: 1250px) {
		.motogp-info .info{
			width: auto;
		}
	}
	@media(max-width: 500px){
		.info-container .motogp-award .motogp-award-depiction ul li{
			width:100%;
			margin: 0%!important;
		}
		.info-container .motogp-award .motogp-award-depiction .depiction{
			height: 140px;
			font-size: 1.2rem;
		}

	}
   @media(min-width:768px){
       .bannerbar {
           display:none;
       }
       .motogp-info {
           margin-top:153px;
       }
   }
   .bannerbar-phone {
       display:none !important;
   }
</style>
@stop
@section('middle')
<div class="motogp-info clearfix">
    <div class="banner-img">
        @if($is_phone)
            <div class="phone-banner" >
                
                <div class="phone-text text-center">
                    <div><span class="font-bold">活動說明與獎項</span></div>
                </div>
            </div>
        @else
            <img src="{{assetRemote('image/benefit/event/motogp/2019/info/banner.jpg')}}">
        @endif
        <div style="position: absolute;top: 0px;width: 100%;height: 100%;">
            <div class="info-banner">

            </div>
        </div>
    </div>
    @include('response.pages.benefit.event.motogp.2019.home.partials.list')
    <div id="motogp-content">
        <div class="info-container clearfix" id="reward-record">
            <div class="info">
                <div>
                    <div class="info-play ">
                        <h2>如何參加活動</h2>
                    </div>
                    <div>
                        @php
                            $titles = ["選擇前三名","確認名次是否相符","送出完成預測","電腦選號功能","選錯該怎麼辦","如何獲得點數","安慰獎","站站參加拿點數","如何獲得萬元購物金","萬元購物金直接帶回家","等你來預測"];
                        @endphp
                        <ul >
                            <li>
                                <div class="title-block text-center font-bold">
                                    <div class="title-name">
                                        @foreach($titles as $title)
                                            <h2 class="url_path_title" {{$loop->iteration != 1 ? 'style=display:none;' : ''}}>{{$title}}</h2>
                                        @endforeach
                                    </div>
                                </div>
                                <div id="carousel-dots" class="text-center"></div>
                                <div class="owl-carousel-motogp">
                                    @foreach($titles as $key => $title)
                                        <img src="{{assetRemote('image/benefit/event/motogp/2019/info/info-'.$key.'.jpg')}}">
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=" info info-depiction">
                <p>【活動內容】2019 MotoGP開跑！預測分站前三名，答對即可獲得免費點數，並有機會贏得最終大獎1萬元現金點數及300元禮券。</p>
                <p>【活動辦法】自2019/03/01日起，凡是會員即可開始預測開幕站的名次，活動時間至最終站結束(共19站)，詳見
                    <a  href="{{URL:: route('benefit-event-motogp-2019').'#race_date'}}" >2019 MotoGP 賽程表。</a></p>
                <p>【解答公布】每分站結束後，次日會公布正確答案，答對即可獲得對應點數，比賽結果以MotoGP官方結果為準。</p>
                <p>【遊戲規範】會員每站僅限答題一次，提交答案後無法修改，請小心回答。</p>
                <p>【最終大獎】得獎者我們會以電話進行會員身分資料確認，若查證冒名者將會取消資格，另行抽出新得獎者，並依據會員規約刪除會員帳號。</p>
                <p>【獎項期限】2019 MotoGP活動贈送之所有點數與折價卷，皆從獲得日起為期一年之效期，請會員在期限內使用。</p>
            </div>
            <div class="info motogp-award">
                <div class="title">
                    <h2>活動獎勵與說明</h2>
                </div>
                <div class="motogp-award-depiction">
                    <ul class="clearfix">
                        <li>
                            <figure>
                                <img src="{{assetRemote('image/benefit/event/motogp/2019/info/icon1-01.png')}}">
                            </figure>
                            <h2>獎項一</h2>
                            <div class="depiction">
                            <span>
                                1.於各分站活動期間選擇您心目中的前三名，若與比賽結果相同，即可贏得現金點數。<br>
                                2.答對分站名次之對應點數，第一名25點、第二名20點、第三名16點，全部19站皆答對最多可獲得1159點。<br>
                                3.只要您有參加活動，如果前三名皆預測錯誤，就可獲得安慰獎10點。<br>
                                4.詳細獲得的點數，<a href="{{URL:: route('benefit-event-motogp-2019').'#reward-record'}}" target="_blank">可查看2019 預測總冠軍</a>或者從<a href="{{URL::route('customer-history-points')}}" target="_blank">點數獲得及使用履歷</a>中查詢。
                            </span>
                            </div>
                        </li>
                        <li>
                            <figure>
                                <img src="{{assetRemote('image/benefit/event/motogp/2019/info/icon2-01.png')}}">
                            </figure>
                            <h2>獎項二</h2>
                            <div class="depiction">
                            <span>
                                1.只要您答對2019 MotoGP各分站冠軍，
                            </span>
                                <span class="red-color">1萬元現金點數</span>
                                <span>直接帶回家，名額不限。<br>
                                2.答對名單會在最終站比賽結束後次日公布。
                             </span>
                            </div>
                        </li>
                        <li>
                            <figure>
                                <img src="{{assetRemote('image/benefit/event/motogp/2019/info/icon3-01.png')}}">
                            </figure>
                            <h2>獎項三</h2>
                            <div class="depiction">
                                <span>
                                    1.連續19站參加活動，系統會自動累計總冠軍分數。 <br>
                                    2.若您預測的年度總冠軍與2019 MotoGP總冠軍相同，即可獲得300元現金折價卷。<br>
                                    3.從每站皆有參加且年度冠軍答對者中，我們將會抽出1萬元現金點數一名。<br>
                                    4.獲獎名單預定會在最終站比賽結束後次日公布。
                                </span>
                           </div>
                        </li>
                     </ul>
               </div>
           </div>
            <div class="text-center btn-bottom">
                <a  class="btn btn-warning " href="{{URL:: route('benefit-event-motogp-2019')}}">回活動首頁</a>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    <script>
        $('.info-banner').append($('.breadcrumb-mobile').clone());

        $('.owl-carousel-motogp').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            dotsContainer: '#carousel-dots',
            dotClass: 'pager-link-block',
            margin:0,
            slideBy : 5,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        }).on('translate.owl.carousel', function(event) {
            _this = $(this);
            index = event.relatedTarget.current() + 1;
            orig_index = index;


            var tempFix = index - (event.relatedTarget.clones().length / 2);
            if (tempFix > 0) {
                index = tempFix;
            } else {
                index = _this.prev('.title-block').find('.title-name h2').length;
            }

            title = _this.closest('li').find('.title-block .title-name h2:nth-child(' + index + ')');

            $('.url_path_title:visible').fadeOut(500, function () {
                title.fadeIn(500);
            });
        });

        $('.owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
        $('.owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
</script>
@stop