<style type="text/css">
    .motogp-tv .tv{
        background-color: white;
        width: 49%;
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .motogp-tv .tv h2{
        margin-bottom:  20px;
    }
    .motogp-tv .tv-left{
        margin-right: 2%;
    }
    .motogp-tv .tv .tv-show{
        margin-top: 20px;
    }
    .motogp-tv .gp-title{
        text-align: center;
        margin: 30px 0px;
        font-weight: bold;
    }
    .motogp-tv a{
        color: #005fb3;
    }
    @media(max-width:550px){
    .motogp-tv .tv-left{
        margin-left: 3%;
        margin-right: 2%;
    }
    .motogp-tv .tv{
        width: 46%;
    }
}
</style>
<div id="motogp-tv" class="clearfix motogp-tv out-width">
    <div class="gp-title">
        <h2>2019 MotoGP 電視轉播</h2>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-5 tv tv-left">
        <h2>MotoGP {{$thisSpeedway->station_name}} 排位賽轉播時間</h2>
       @if($thisSpeedway->ranking_view_datetime)
            @php
                $rank_blade = parseBladeCode($thisSpeedway->ranking_view_datetime,['ranking_view' => $thisSpeedway->ranking_view,'ranking_station' => $thisSpeedway->station_name])
            @endphp
            {!! $rank_blade !!}
        @else
            尚未公布
        @endif
    </div>

    <div class="col-xs-4 col-sm-4 col-md-5 tv">
        <h2>MotoGP {{$thisSpeedway->station_name}} 正賽轉播時間</h2>
        @if($thisSpeedway->formal_view_datetime)
            @php
                $formal_blade =  parseBladeCode($thisSpeedway->formal_view_datetime,['formal_view' => $thisSpeedway->formal_view,'formal_station' => $thisSpeedway->station_name])
            @endphp
            {!! $formal_blade !!}
        @else
            尚未公布
        @endif
    </div>
</div>