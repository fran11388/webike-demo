<style>
    .motogp-menu .out-width{
        max-width: 1230px !important;
        margin: 0 auto;
    }
    .out-width .overflow-menu-ui{
        height:42px !important;
    }
    .motogp-menu {
        width: 100%;
        border-top: 1px solid #d0d0d0;
        border-bottom: 1px solid #d0d0d0;
        background-color:white;
    }
    .motogp-menu .container li:hover a,.motogp-menu .container li.active .menu-link{
        color: red;
    }
    .motogp-menu .container li:hover,.motogp-menu .container li.active{
        padding-bottom:0px;
        border-bottom:  5px solid red;
        background-color: white;
    }
    .btn-asking{
        border: 0px;
    }
    .motogp-menu .container li .menu-link{
        padding: 5px 13px;
        color: #2a313c;
        float: left;
        transition: all 0s ease !important;
    }
    .motogp-menu .overflow-menu-ui ul li{
        padding:5px;
    }
    .motogp-menu .list-history-row{
        height:42px;
        position:absolute;
        right:11%;
        margin-top:-6px;
        cursor:pointer;
    }
    .motogp-menu .history-race{
        position:relative;
        padding-right:30px !important;
    }
    .motogp-menu .history-race:hover{
        position:relative;
    }
    .motogp-menu .history-race:hover ul{
        display:block !important;
        position:absolute;
        top:42px;
        z-index:20;
        left: 0px;
        width:125px;
        height:238px;
        overflow:auto;
    }
    .motogp-menu .history-race:hover ul li{
        display:block;
        list-style-type: none;
        border: 1px solid #ddd;
        cursor:pointer;
        background-color: white !important;
    }
    .motogp-menu .history-race:hover ul li:hover{
        background-color:#d5d5d5 !important;
        border-bottom: 1px solid #ddd !important;
        padding-bottom:5px !important;
    }
    .motogp-menu .overflow-menu li h3{
        color: #636260;
        font-weight:bold;
    }
    .motogp-menu .overflow-menu li:hover .link-top,.motogp-menu .history-gp-link:hover .menu-link h3{
        color:red !important;
    }
    .motogp-menu .overflow-menu li.active .menu-link h3{
        color:red;
    }
    .motogp-menu .history-gp{
        display:none !important;
    }
    .motogp-menu {
        width: 100%;
        border-top: 1px solid #d0d0d0;
        border-bottom: 1px solid #d0d0d0;
    }
    .motogp-menu.fixed{
        position: fixed;
        top: 69px;
        z-index: 1000;
        background-color: #efefef;
    }
    @media(max-width:550px){
        .xs-width-full{
            width:100%;
        }
        .out-width .overflow-menu-ui .overflow-menu.list-menu{
            position:relative;
            padding-right:25px;
            height:42px;
            overflow-y:hidden;
        }
        .out-width .overflow-menu-ui .list-menu.active{
            overflow-y: inherit;
        }
        .out-width .overflow-menu-ui .overflow-menu.list-menu .list-row{
            width: 25px;
            position: absolute;
            right: 0%;
            top: 0%;
            height: 42px;
            text-align: center;
        }
        .motogp-menu.fixed{
            position: fixed;
            top: 55px;
            z-index: 1000;
            background-color: #efefef;
        }
    }

    .motogp-menu .list-history-row{
        height:42px;
        position:absolute;
        right:11%;
        margin-top:-6px;
        cursor:pointer;
    }
    /*.motogp-menu .history-race{*/
    /*position:relative;*/
    /*padding-right:30px !important;*/
    /*}*/
    .motogp-menu .history-race:hover{
        position:relative;
    }
    .motogp-menu .history-race:hover ul{
        display:block !important;
        position:absolute;
        top:42px;
        z-index:20;
        left:0px;
    }
    .motogp-menu .history-race:hover ul li{
        display:block;
        list-style-type: none;
        border: 1px solid #ddd;
        cursor:pointer;
        background-color: white !important;
    }
    .motogp-menu .history-race:hover ul li:hover{
        background-color:#d5d5d5 !important;
        border-bottom: 1px solid #ddd !important;
        padding-bottom:5px !important;
    }
    .motogp-menu .overflow-menu li h3{
        color: #636260;
        font-weight:bold;
    }
    .motogp-menu .overflow-menu li.active .menu-link h3{
        color:red;
    }
    .motogp-menu .history-gp{
        display:none !important;
    }
    .motogp-menu .overflow-menu li:hover .link-top,.motogp-menu .history-gp-link:hover a h3{
        color:red !important;
    }
    .motogp-menu .history-race .history-gp-link{
        padding-left: 15px;
    }
</style>
<div class="motogp-menu clearfix">
    <div class="container out-width">
        <div class="overflow-menu-ui ">
            <ul class="overflow-menu list-menu">
                <li >
                    <a href="{{URL:: route('benefit-event-motogp-2019')}}" class="menu-link">
                        <h3 class="link-top">2019 MotoGP 首頁</h3>
                    </a>
                </li>
                <li class="active" onclick="slipTo1('#motogp-speedway')">
                    <a class="menu-link" href="javascript:void(0)">
                        <h3 class="link-top">賽道介紹</h3>
                    </a>
                </li>
                <li onclick="slipTo1('#history')">
                    <a class="menu-link" href="javascript:void(0)">
                        <h3 class="link-top">歷史成績</h3>
                    </a>
                </li>
                <li onclick="slipTo1('#motogp-tv')" class="xs-width-full">
                    <a class="menu-link" href="javascript:void(0)">
                        <h3 class="link-top">電視轉播</h3>
                    </a>
                </li>
                <li onclick="slipTo1('#motogp-news')" class="xs-width-full">
                    <a class="menu-link" href="javascript:void(0)">
                        <h3 class="link-top">賽後報導</h3>
                    </a>
                </li>
                <li onclick="slipTo1('#more-speedway')" class="xs-width-full visible-xs hidden-sm hidden-md">
                    <a class="menu-link" href="javascript:void(0)">
                        <h3 class="link-top">賽道一覽</h3>
                    </a>
                </li>
                <li onclick="slipTo1('#')" class="xs-width-full history-race hidden-xs">
                    <a href="javascript:void(0)" title="2019 MotoGP 冠軍預測活動.{{ $tail }}" class="history-link menu-link" target="_blank">
                        <h3 class="link-top">賽道一覽</h3>
                    </a>
                    <div class="list-history-row">
                        <span class="helper"></span>
                        <img src="{{ assetRemote('image/benefit/event/motogp/2018/left-row.png') }}" class="vertical-align-middle">
                    </div>
                    @if(!$is_phone)
                        <ul class="hidden">
                            @foreach($speedwaies as $speedway)
                                <a href="{{ \URL::route('benefit-event-motogp-2019detail',['station' => $speedway->station_name]) }}" title="{{ $speedway->name. '賽道資訊' }}" class="img-block" target="_blank">
                                    <li class="clearfix history-gp-link text-center">
                                        <h3 class="text-left  ">{{ $speedway->station_name }}</h3>
                                    </li>
                                </a>
                            @endforeach
                        </ul>
                    @endif
                </li>
                {{--@if($is_phone)--}}
                    {{--@foreach($speedwaies as $speedway)--}}
                        {{--<li class="clearfix xs-width-full history-race">--}}
                            {{--<a href="{{ \URL::route('benefit-event-motogp-2019detail',['station' => $speedway->station_name]) }}" title="{{ $speedway->name. '賽道資訊' }}" class="img-block" target="_blank">--}}
                                {{--<h3>{{ $speedway->station_name }}</h3>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                {{--@endif--}}
                <div class="list-row visible-xs">
                    <span class="helper"></span>
                    <img src="{{ assetRemote('image/benefit/event/motogp/2018/left-row.png') }}" class="vertical-align-middle">
                </div>
            </ul>
        </div>
    </div>
</div>
<script>
    @if(!$is_phone)
        $(window).scroll(function(){
            if ($(this).scrollTop() > 600) {
                $('.motogp-menu').addClass('fixed');
            } else {
                $('.motogp-menu').removeClass('fixed');
            }
        });
    @endif
    $('.motogp-menu .overflow-menu li').click(function(){
        $('.motogp-menu .overflow-menu li').removeClass('active');
        $(this).addClass('active');
    });
    $('.list-row').click(function(){
        if(!$('.out-width .overflow-menu-ui .overflow-menu.list-menu').hasClass('active')){
            $('.out-width .overflow-menu-ui .overflow-menu.list-menu').addClass('active');
        }else{
            $('.out-width .overflow-menu-ui .overflow-menu.list-menu').removeClass('active');
        }
    });
    $('.history-race').click(function(){
        if($('.history-gp').css('display') == 'block'){
            $('.history-gp').css("cssText",'display:none !important');
        }else{
            $('.history-gp').css("cssText",'display:block !important');
        }
    });
</script>