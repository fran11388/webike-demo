<div class="gp-banner clearfix customer_choose" id="customer_choose">
    <form method="post" action="{{ \URL::route('benefit-event-motogp-2019') }}" id="choice">
        <div class="out-width">
            @if($now_speedway)
                <div class="gp-race-title text-center">
                    @if($is_phone)
                        <h1 class="font-bold">2019 MotoGP 冠軍預測 </h1>
                        <h1 class="font-bold"> 萬元購物金送給你</h1>
                    @else
                        <h1 class="font-bold">2019 MotoGP 冠軍預測 萬元購物金送給你</h1>
                    @endif
                    <h1>{{ $now_speedway->station_name }} 台灣正賽時間：{{ date('m/d',strtotime($now_speedway->raced_at_local)) }}</h1>
                    <h3>投票截止日期：{{ date('m/d H:i ',strtotime($now_speedway->closested_at . '- 1 second')) }}</h3>
                </div>
            @endif
            <div class="clearfix gp-gmae clearfix row">
                @if(count($racers))
                    <div class="clearfix  game-choose col-xs-12 col-sm-12 col-md-9">
                        <img src = "{{ assetRemote('image/benefit/event/motogp/2019/choose/number1.png') }}" id="number1" class="hidden">
                        <img src = "{{ assetRemote('image/benefit/event/motogp/2019/choose/number2.png') }}" id="number2" class="hidden">
                        <img src = "{{ assetRemote('image/benefit/event/motogp/2019/choose/number3.png') }}" id="number3" class="hidden">
                        <input type="hidden" value="" id="choose_number_one" class="choose-racer" name="my_choice[]">
                        <input type="hidden" value="" id="choose_number_two" class="choose-racer" name="my_choice[]">
                        <input type="hidden" value="" id="choose_number_three" class="choose-racer" name="my_choice[]">
                        @foreach($racers as $racer)
                            <div class="col-xs-3 col-sm-3 {{ $is_phone ? '' : 'col-md-1'}} text-center racer" id="{{ $racer->id }}">
                                @if(isset($customer))
                                    @if($customer_choices and $now_speedway and isset($customer_choices->one[$now_speedway->id]))
                                        @php
                                            if($number_one = collect($customer_choices->one[$now_speedway->id]) and $number_one->first() == $racer->number){
                                                $choose = true;
                                                $border_img = assetRemote('image/benefit/event/motogp/2019/choose/number1.png');
                                            }elseif($number_two = collect($customer_choices->two[$now_speedway->id]) and $number_two->first() == $racer->number){
                                                $choose = true;
                                                $border_img = assetRemote('image/benefit/event/motogp/2019/choose/number2.png');
                                            }elseif($number_three = collect($customer_choices->three[$now_speedway->id]) and $number_three->first() == $racer->number){
                                                $choose = true;
                                                $border_img = assetRemote('image/benefit/event/motogp/2019/choose/number3.png');
                                            }else{
                                                $choose = false;
                                            }
                                        @endphp
                                        <div class="racer-img {{ $choose ? '' : 'hidden' }}">
                                            <img src="{{ $choose ? $border_img : '' }}">
                                        </div>
                                    @else
                                        <div class="racer-img {{ 'racer_'.$racer->number }}">
                                            <img src="">
                                        </div>
                                    @endif
                                    <div class="{{ isset($customer_choices->one[$now_speedway->id]) ? '' : 'cursor-pointer' }}">
                                        @if($customer_choices and isset($customer_choices->one[$now_speedway->id]))
                                            <img src="{{assetRemote('image/benefit/event/motogp/2019/choose').($choose ? '/after' : '/lock').'/'.$racer->number.'.png'}}" class="before_img" alt="{{ '2019 MotoGP 【'.$racer->number.'】'.$racer->name }}">
                                        @else
                                            <img src="{{assetRemote('image/benefit/event/motogp/2019/choose').($choose ? '/after' : '/before').'/'.$racer->number.'.png'}}" class="before_img" alt="{{ '2019 MotoGP 【'.$racer->number.'】'.$racer->name }}">
                                            <img src="{{assetRemote('image/benefit/event/motogp/2019/choose/after').'/'.$racer->number.'.png'}}" class="hidden after_img" alt="{{ '2019 MotoGP 【'.$racer->number.'】'.$racer->name }}">
                                            <img src="{{assetRemote('image/benefit/event/motogp/2019/choose/lock.png')}}" class="hidden lock_img" alt="{{ '2019 MotoGP 【'.$racer->number.'】'.$racer->name }}">
                                        @endif
                                    </div>
                                @else
                                    <div>
                                        <img src="{{assetRemote('image/benefit/event/motogp/2019/choose').($choose ? '/after' : '/lock').'/'.$racer->number.'.png'}}" class="before_img" alt="{{ '2019 MotoGP 【'.$racer->number.'】'.$racer->name }}">
                                    </div>
                                @endif
                                <p class="size-10rem">{{ $racer->nickname }}</p>
                            </div>
                        @endforeach
                    </div>
                @endif
                <div class="gp-choose col-xs-12 col-sm-12 col-md-3">
                    <div class="gp-choose-content">
                        @if($customer_choices and $now_speedway and isset($customer_choices->one[$now_speedway->id]))
                            @php
                                $customer_finish = true;
                            @endphp
                        @endif
                        @if(!$is_phone)
                            <div class="gp-choose-left">
                                <img  src="{{ $customer_finish ? assetRemote('image/benefit/event/motogp/2019/choose/after').'/'.collect($customer_choices->two[$now_speedway->id])->first().'.png' : '' }}">
                            </div>
                            <div class="gp-choose-center text-center">
                                <img  src="{{ $customer_finish ? assetRemote('image/benefit/event/motogp/2019/choose/after').'/'.collect($customer_choices->one[$now_speedway->id])->first().'.png' : assetRemote('image/benefit/event/motogp/2019/choose/default.png')}}">
                            </div>
                            <div class="gp-choose-right">
                                <img  src="{{ $customer_finish ? assetRemote('image/benefit/event/motogp/2019/choose/after').'/'.collect($customer_choices->three[$now_speedway->id])->first().'.png' : ''}}">
                            </div>
                            <div class="text-center box">
                                <img src="{{assetRemote('image/benefit/event/motogp/2019/podium.png')}}">
                            </div>
                        @endif
                        {{--@if(!$customer_finish)--}}
                        @if(isset($customer))
                            <div class="submit-content">
                                <div class="gp-btn clearfix text-center row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 game-computer box" style="padding-right:5px;">
                                        <input type="button" class="width-full btn btn-warning" value="電腦選號" {{ ($customer_finish || !isset($customer)) ? 'disabled' : '' }}>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 clear-all box" style="padding-left:5px;">
                                        <input type="button" class="width-full btn btn-clear border-radius-2" value="清除全部" {{ ($customer_finish || !isset($customer)) ? 'disabled' : '' }}>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12  game-ok box" id="submit-button">
                                        <input type="button" class="width-full btn btn-danger border-radius-2" value="{{ $customer_finish ? '本站已投票' : '確認送出'}}" {{ ($customer_finish || !isset($customer)) ? 'disabled' : '' }}>
                                    </div>
                                </div>
                                <div class="random-ing text-center hidden">
                                    <h2>電腦選號中···</h2>
                                </div>
                            </div>
                        @else
                            <div class="submit-content">
                                <div class="gp-btn clearfix text-center row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 game-computer box" style="padding-right:5px;">
                                        <a class="width-full btn btn-warning" href="{{URL::route('login')}}" alt="{{'會員登入'.$tail}}">會員登入</a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 clear-all box" style="padding-left:5px;">
                                        <a class="btn btn-danger border-radius-2 width-full" href="{{URL::route('customer-account-create')}}" alt="{{'加入會員'.$tail}}">加入會員</a>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12  game-ok box">
                                        <a type="button" class="width-full btn btn-clear border-radius-2" disabled>登入即可參與遊戲</a>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="clearfix gp-link">
                            <div class="col-xs-12 col-sm-12 col-md-6 text-center">
                                <a href="{{ \URL:: route('benefit-event-motogp-2019info') }}" target="_blank">
                                    <span>詳細活動說明>>></span>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 text-center">
                                <div>
                                    <a href="https://www.sportslottery.com.tw/sports-betting#m/all/s-646/c/t-8964/462/2" target="_blank" rel="nofollow">
                                        <span>台灣運彩網站參考></span>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://sports.bwin.com/en/sports/40/betting/motorbikes#eventId=&leagueIds=6942&marketGroupId=&page=0&sportId=40" target="_blank" rel="nofollow">
                                        <span>歐洲 bwin 網站參考></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>