<style>
    .out-width{
        max-width: 1230px !important;
        margin: 0 auto;
    }
    .out-width .overflow-menu-ui{
        height:42px !important;
    }
    .motogp-menu {
        width: 100%;
        border-top: 1px solid #d0d0d0;
        border-bottom: 1px solid #d0d0d0;
        background-color:white;
    }
    .motogp-menu .container li:hover a,.motogp-menu .container li.active a{
        color: red;
    }
    .motogp-menu .container li:hover,.motogp-menu .container li.active{
        padding-bottom:0px;
        border-bottom:  5px solid red;
        background-color: white;
    }
    .btn-asking{
        border: 0px;
    }
    .motogp-menu .container li a{
        padding: 5px 13px;
        color: #2a313c;
        float: left;
        transition: all 0s ease !important;
    }
    .motogp-menu .overflow-menu-ui ul li{
        padding:5px;
    }
    .motogp-menu .list-history-row{
        height:42px;
        position:absolute;
        right:0%;
        margin-top:-6px;
        cursor:pointer;
    }
    .motogp-menu .history-race{
        position:relative;
    }
    .motogp-menu .history-race:hover ul{
        display:block !important;
        position:absolute;
        top:42px;
        z-index:20;
        left:0px;
    }
    .motogp-menu .history-race:hover ul li{
        display:block;
        list-style-type: none;
        border: 1px solid #ddd;
        cursor:pointer;
        background-color: white !important;
    }
    .motogp-menu .history-race:hover ul li:hover{
        background-color:#d5d5d5 !important;
        border-bottom: 1px solid #ddd !important;
        padding-bottom:5px !important;
    }
    .motogp-menu .overflow-menu li h3{
        color: #636260;
        font-weight:bold;
    }
    .motogp-menu .overflow-menu li.active h3{
        color:red;
    }
    .motogp-menu .history-gp{
        display:none !important;
    }
    .motogp-menu .overflow-menu li:hover .link-top,.motogp-menu .history-gp-link:hover a h3{
        color:red !important;
    }
    /*.motogp-menu .history-race .history-link{*/
        /*padding-right: 0px !important;*/
    /*}*/
    .motogp-menu {
        width: 100%;
        border-top: 1px solid #d0d0d0;
        border-bottom: 1px solid #d0d0d0;
    }
    .motogp-menu.fixed{
        position: fixed;
        top: 69px;
        z-index: 1031;
        background-color: #efefef;
    }
    @media(max-width:550px){
        .xs-width-full{
            width:100%;
        }
        .out-width .overflow-menu-ui .overflow-menu.list-menu{
            position:relative;
            padding-right:25px;
            height:42px;
            overflow-y:hidden;
        }
        .out-width .overflow-menu-ui .list-menu.active{
            overflow-y: inherit;
        }
        .out-width .overflow-menu-ui .overflow-menu.list-menu .list-row{
            width: 25px;
            position: absolute;
            right: 0%;
            top: 0%;
            height: 42px;
            text-align: center;
        }
        .motogp-menu .container .overflow-menu-ui ul li .href-link{
            width: 100%;
        }

    }

</style>
@php
    if(\Request::route()->getName() == 'benefit-event-motogp-2019'){
        $link = true;
    }else{
        $link = false;
    }
@endphp
<div class="motogp-menu clearfix">
    <div class="container out-width">
        <div class="overflow-menu-ui ">
            <ul class="overflow-menu list-menu">
                <li class="{{ $link ? "active" : ' ' }}" onclick="{{ $link ? "slipTo1('#customer_choose')" : ' ' }}">
                    <a title="2019 MotoGP 冠軍預測活動.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#customer_choose'}}">
                        <h3 class="link-top">2019 MotoGP 冠軍大預測</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#motogp-tv')" : ' ' }}">
                    <a title="電視轉播.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#motogp-tv'}}">
                        <h3 class="link-top">電視轉播</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#reward-record')" : ' ' }}">
                    <a title="預測履歷.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#reward-record'}}">
                        <h3 class="link-top">預測履歷</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#racer-score-rank')" : ' ' }}" class="xs-width-full">
                    <a title="積分排行.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#racer-score-rank'}}">
                        <h3 class="link-top">積分排行</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#racer-data')" : ' ' }}" class="xs-width-full">
                    <a title="車手資訊及成績.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#racer-data'}}">
                        <h3 class="link-top">車手資訊及成績</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#speedwaies')" : ' ' }}" class="xs-width-full">
                    <a title="賽道資訊.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#peedwaies'}}">
                        <h3 class="link-top">賽道資訊</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#race_date')" : ' ' }}" class="xs-width-full">
                    <a title="賽程表.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#race_date'}}">
                        <h3 class="link-top">賽程表</h3>
                    </a>
                </li>
                <li onclick="{{ $link ? "slipTo1('#news-info')" : ' ' }}" class="xs-width-full">
                    <a title="最新消息.{{ $tail }}" href="{{ $link ? 'javascript:void(0)' : URL:: route('benefit-event-motogp-2019').'#news-info'}}">
                        <h3 class="link-top">最新消息</h3>
                    </a>
                </li>
                <li  onclick="slipTo1('#')" class=" xs-width-full {{ $link ? " " : "active" }}">
                    <a title="活動說明.{{ $tail }}" href="{{ $link ? URL:: route('benefit-event-motogp-2019info') : 'javascript:void(0)'}}" class="href-link" target="_blank">
                        <h3 class="link-top">活動說明</h3>
                    </a>
                </li>
                <li  onclick="slipTo1('#')" class=" xs-width-full history-race">
                    <a href="javascript:void(0)" title="2019 MotoGP 冠軍預測活動.{{ $tail }}" class="history-link href-link" target="_blank">
                        <h3 class="link-top">歷屆賽事</h3>
                    </a>
                    @if(!$is_phone)
                    <div class="list-history-row">
                        <span class="helper"></span>
                        <img src="{{ assetRemote('image/benefit/event/motogp/2019/left-row.png') }}" class="vertical-align-middle">
                    </div>
                        <ul class="hidden">
                            <li class="clearfix history-gp-link">
                                <a href="{{ \URL::route('benefit-event-motogp-2018') }}" target="_blank" title="2019 MotoGP 冠軍預測活動.{{ $tail }}">
                                    <h3>2018 MotoGP</h3>
                                </a>
                            </li>
                            <li class="clearfix history-gp-link">
                                <a href="{{ \URL::route('benefit-event-motogp-2017') }}" target="_blank" title="2019 MotoGP 冠軍預測活動.{{ $tail }}">
                                    <h3>2017 MotoGP</h3>
                                </a>
                            </li>
                        </ul>
                    @endif
                </li>
                @if($is_phone)
                    <li class="clearfix xs-width-full history-race">
                        <a href="{{ \URL::route('benefit-event-motogp-2018') }}" title="2019 MotoGP 冠軍預測活動.{{ $tail }}">
                            <h3>  - 2018 MotoGP</h3>
                        </a>
                    </li>
                    <li class="clearfix xs-width-full history-race">
                        <a href="{{ \URL::route('benefit-event-motogp-2017') }}" title="2019 MotoGP 冠軍預測活動.{{ $tail }}">
                            <h3>  - 2017 MotoGP</h3>
                        </a>
                    </li>
                @endif
                <div class="list-row visible-xs">
                    <span class="helper"></span>
                    <img src="{{ assetRemote('image/benefit/event/motogp/2019/left-row.png') }}" class="vertical-align-middle">
                </div>
            </ul>
        </div>
    </div>
</div>
<script>
    $(window).scroll(function(){
        if($(window).width() > 550){
            if ($(this).scrollTop() > (parseInt($('#motogp-content').offset().top) - $('#mainNav .container-header').height() - $('.motogp-menu').height())) {
                $('#mainNav .menu-shopping .container').css("cssText",'display:none !important');
                $('.motogp-menu').addClass('fixed');
                if($('#mainNav').hasClass('nav-down')){
                    $('.motogp-menu.fixed').css('top','108px');
                }else{
                    $('.motogp-menu.fixed').css('top','69px');
                }
            }else if($(this).scrollTop() < $('#motogp-content').height()){
                $('#mainNav .menu-shopping .container').css("cssText",'display:block !important');
                $('.motogp-menu').removeClass('fixed');
            }
        }
    });
    $('.motogp-menu .overflow-menu li').click(function(){
        $('.motogp-menu .overflow-menu li').removeClass('active');
        $(this).addClass('active');
        $('.out-width .overflow-menu-ui .overflow-menu.list-menu').removeClass('active');
    });
    $('.list-row').click(function(){
        if(!$('.out-width .overflow-menu-ui .overflow-menu.list-menu').hasClass('active')){
            $('.out-width .overflow-menu-ui .overflow-menu.list-menu').addClass('active');
        }else{
            $('.out-width .overflow-menu-ui .overflow-menu.list-menu').removeClass('active');
        }
    });
    $('.history-race').click(function(){
        if($('.history-gp').css('display') == 'block'){
            $('.history-gp').css("cssText",'display:none !important');
        }else{
            $('.history-gp').css("cssText",'display:block !important');
        }
    });

</script>

