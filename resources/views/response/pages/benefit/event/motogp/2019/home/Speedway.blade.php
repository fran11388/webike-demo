@extends('response.layouts.1column')
@section('style')
    <style>
        .container .breadcrumb-mobile{
            margin-bottom: -40px;
        }
        .station-banner .breadcrumb-mobile{
            margin-bottom: 0px;
        }
        .station-img .station-banner-block{
             position: absolute;top: 0px;width: 100%;height: 100%;
        }
        .motogp-speedway .station-img .phone-img{
            background-color: #636260;
            padding: 30px 10% 30px 10%;
            text-align: center;
        }
        .station-img .station-banner .breadcrumb-mobile ol.breadcrumb-product-detail{
            margin: 0 auto;
            max-width: 1230px;
        }
        ol.breadcrumb-product-detail li a, ol.breadcrumb-product-detail .active span{
            color: white;
        }
        ol.breadcrumb-product-detail li a:hover{
            color: #e61e25;
        }
        .top-shopping .container{
            max-width: none !important;
            padding: 0px;
        }
        .motogp-speedway .station-img{
            position: relative;
        }
        .motogp-speedway .speedway-container{
            background-color: #e9e9e9
        }
        .motogp-speedway .speedway-container .speedway{
            max-width: 1230px;
            margin-left: auto;
            margin-right: auto;
        }

        .motogp-speedway .speedway-container .speedway .speedway-history{
            margin-top: 20px;
            width: 100%;
            overflow-y: auto;
            white-space: nowrap;

        }
        .motogp-speedway .speedway-container .speedway .speedway-history table{
            background-color: white;
            width: 100%;
        }
        .motogp-speedway .speedway-container .speedway .speedway-history table tr td{
            height:40px;
            width:355px;
            border-top:  1px solid #e9e9e9;
            border-bottom: 1px solid #e9e9e9;
            border-left: 1px solid;
            text-align: center;
            padding: 10px;
        }
        .motogp-speedway .speedway-container .speedway .speedway-history table tr td:nth-child(1){
            width:95px;
            border-left: 1px solid #e9e9e9;
        }
        .motogp-speedway .speedway-container .speedway .speedway-history table tr th h2 {
            margin: 10px 0px;
            font-weight: bold;
        }
        .motogp-speedway .speedway-container .speedway .speedway-history table tr th{
            font-size: 1rem;
            font-weight: bold;
            padding:5px;
        }
        .motogp-speedway .speedway-container .speedway .title{
            text-align: center;
            margin: 30px 0px;
            font-weight: bold;
        }
        .motogp-speedway .speedway-container .speedway .title h2{
            font-size: 1.2rem;
        }
        .motogp-speedway .speedway-container .speedway .station-name{
            margin-top: 30px;
            margin-bottom: 10px;
        }
        .motogp-speedway .speedway-container .speedway .motogp-news{
            background-color: white;
            padding: 30px 30px 30px 0px;
            width: 100%;
        }
        .motogp-speedway .speedway-container .speedway .motogp-news .news-photo{
            padding: 0px 30px;
        }
        .motogp-speedway .speedway-container  .motogp-home-btn{
            margin:30px 0px 100px 0px;
        }
        @media(min-width:768px){
            .bannerbar {
                display:none;
            }
            .motogp-speedway {
                margin-top:154px;
            }
        }
        .bannerbar-phone {
            display:none !important;
        }
        @media(max-width:550px){
            .motogp-speedway .speedway-container .speedway .motogp-news .news-photo{
                padding: 0px;
            }
            .motogp-speedway .speedway-container .speedway .motogp-news{
                width: 100%;
                padding-right: 0px;
            }
            .motogp-speedway .speedway-container .speedway .motogp-news .news-text{
                overflow: hidden;
                text-overflow: ellipsis;
                word-break: break-all;
                word-wrap: break-word;
                display: -webkit-box;
                -webkit-box-orient: vertical;
                -webkit-line-clamp: 2;
                max-height: 200%;
            }
        }
        select {
            background:white;
        }
    </style>
@stop
@section('middle')

    <?php
    function recordExchange($string)
    {
        $values = $string;
        if(strpos($values, ',') !== false){
            $values = explode(',', $values);
        }else{
            $values = array('-', '-');
        }
        return $values;
    }
    ?>
    <div id="motogp-speedway" class="motogp-speedway clearfix">
        <div class="station-img">
            @if($is_phone)
            <div class="phone-img">
                <img src="{{assetRemote('image/benefit/event/motogp/2018/phone-speedway/'.$thisSpeedway->id.'.png')}}" alt="2019 MotoGP {{$thisSpeedway->station_name}}">
            </div>
        @else
            <div>
                <img src="{{assetRemote('image/benefit/event/motogp/2018/speedway/'.$thisSpeedway->id.'.jpg')}}" alt="2019 MotoGP {{$thisSpeedway->station_name}}">
            </div>
        @endif
            <div class="station-banner-block">
                <div class="station-banner"></div>
            </div>
        </div>
        @include('response.pages.benefit.event.motogp.2019.home.partials.speedway-list')
            <div id="motogp-content">
                <div class="speedway-container">
                    <div class="speedway clearfix">
                        <div id="history">
                            <div class="text-center font-bold station-name">
                                <h1>2019 MotoGP {{$thisSpeedway->station_name}}</h1>
                            </div>
                            <div class="text-center font-bold">
                                <h2>歷史成績</h2>
                            </div>
                            <div class="speedway-history">
                                <table>
                                    <tr>
                                        <th colspan="4" class="text-center">2019 MotoGP{{$thisSpeedway->station_name}}成績</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><span>車手姓名</span></td>
                                        <td><span>成績</span></td>
                                        <td><span>時速</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>桿位</span></td>
                                        <td><span>{{$thisSpeedway->pole_rider_2019}}</span></td>
                                        <?php

                                        $values = recordExchange($thisSpeedway->pole_record_2019);
                                        ?>
                                        <td><span>{{$values[0]}}</span></td>
                                        <td><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>冠軍</span></td>
                                        <td><span>{{$thisSpeedway->first_2019}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->first_record_2019);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>亞軍</span></td>
                                        <td><span>{{$thisSpeedway->second_2019}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->second_diff_2019);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>季軍</span></td>
                                        <td><span>{{$thisSpeedway->third_2019}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->third_diff_2019);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="speedway-history">
                                <table>
                                    <tr>
                                        <th colspan="4" class="text-center">2018 MotoGP{{$thisSpeedway->station_name}}成績</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><span>車手姓名</span></td>
                                        <td><span>成績</span></td>
                                        <td><span>時速</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>桿位</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->pole_rider_2018}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->pole_record_2018);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td><span>冠軍</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->first_2018}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->first_record_2018);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>亞軍</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->second_2018}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->second_diff_2018);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>季軍</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->third_2018}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->third_diff_2018);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="speedway-history">
                                <table>
                                    <tr>
                                        <th colspan="4" class="text-center">
                                            {{$thisSpeedway->station_name}}賽道紀錄
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            紀錄保持人
                                        </td>
                                        <td>
                                            紀錄成績
                                        </td>
                                        <td>
                                            時速
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>最快單圈</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->max_singleturn_racer}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->max_singleturn);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>正賽紀錄</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->max_record_racer}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->max_record);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>最快排位</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->max_qualifying_racer}}</span></td>
                                        <?php
                                        $values = recordExchange($thisSpeedway->max_qualifying);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>最快時速</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->max_speed_racer}}</span></td>
                                        <td class="text-center"><span>-</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->max_speed}}</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        @include('response.pages.benefit.event.motogp.2019.home.partials.motogp_tv')
                        @if($stationNew)
                            <div id="motogp-news">
                                <div class="title">
                                    <h2>2019 MotoGP 賽後報導</h2>
                                </div>
                                <div class="clearfix motogp-news">
                                    <div class="col-xs-12 col-sm-4 col-md-4 news-photo">
                                        @foreach($stationNewsimg as $key => $newsImage)
                                            @if($key == 0)
                                                <a class="zoom-image rounded-img" href="{{$stationNew->guid}}" target="_blank">
                                                    <img class="img-responsive rounded-img box" src="{{$newsImage}}" alt="2019 MotoGP {{$thisSpeedway->name}} 賽後報導">
                                                </a>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="col-xs-12 col-sm-8 col-md-8 post-content content-below-post">
                                        <a class="title-content-below-post" href="{{$stationNew->guid}}" target="_blank">
                                            <h2 class="entry-title a-link-blue-color"> {{$stationNew->post_title}} </h2>
                                        </a>
                                        <span class="news-text">
                                        {!!$stationNewsContent!!}
                                    </span>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div id="more-speedway" class="clearfix visible-xs hidden-sm hidden-md" style="margin: 0px 10px;">
                            <div class="title">
                                <h2>賽道一覽</h2>
                            </div>
                            <div class="width-full">
                                <select class="select2" onchange="location = this.value;">
                                    <option value="">賽道一覽</option>
                                    @foreach($speedwaies as $speedway)
                                        <option value="{{ \URL::route('benefit-event-motogp-2019detail',['station' => $speedway->station_name]) }}">{{ $speedway->station_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="text-center motogp-home-btn">
                            <a class="btn btn-warning  border-radius-2" href="{{ \URL::route('benefit-event-motogp-2019') }}">
                                回活動首頁
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
@section('script')
    <script >
        $(window).scroll(function(){
            if($(window).width() > 550){
                if ($(this).scrollTop() > (parseInt($('#motogp-content').offset().top) - $('#mainNav .container-header').height() - $('.motogp-menu').height())) {
                    $('#mainNav .menu-shopping .container').css("cssText",'display:none !important');
                    $('.motogp-menu').addClass('fixed');
                    if($('#mainNav').hasClass('nav-down')){
                        $('.motogp-menu.fixed').css('top','108px');
                    }else{
                        $('.motogp-menu.fixed').css('top','69px');
                    }
                }else if($(this).scrollTop() < $('#motogp-content').height()){
                    $('#mainNav .menu-shopping .container').css("cssText",'display:block !important');
                    $('.motogp-menu').removeClass('fixed');
                }
            }
        });
        function slipTo1(element){
            if($(window).width() > 550) {
                var y = parseInt($(element).offset().top) - $('#mainNav .container-header').height() - $('.motogp-menu').height() - 20;
                $('html,body').animate({scrollTop: y}, 400);
            }else{
                var y = parseInt($(element).offset().top) - $('#mainNav .container-header').height();
                $('html,body').animate({scrollTop: y}, 400);
            }
        }

        $('.station-banner').append($('.breadcrumb-mobile').clone());

    </script>
@stop