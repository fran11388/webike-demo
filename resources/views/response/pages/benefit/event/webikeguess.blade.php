@extends('response.layouts.1column')
@section('style')
<link rel="stylesheet" type="text/css" href="{!! assetRemote('/css/pages/webikewanted-rewrite.css?date=20170406-00') !!}">
<link rel="stylesheet" type="text/css" href="{!! assetRemote('/css/pages/guess.css?date=20170331-01') !!}">
<style>
..box-quiz-page .photo .choices div{
	cursor: pointer;
}
.answer-selected{
	opacity: 0.6 !important;
	background: #ffffff !important;
}
.webike2018-guess-img img{
    min-width: 100%;
}
/*
@media (min-width: 768px) {
	height:158px !important;
}
*/

</style>
@stop
@section('middle')
	<?php
		$today = date("Y-m-d");
		if(isset($question)){
			$product = $question->product;
		}
		if(!isset($finish)){
			$finish = 'false';
		}
	?>
	<div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <img class="img-full" src="{!! assetRemote('image/benefit/event/WebikeGuess/2018/webikeguess750.gif') !!}" alt="">
        </div>
    </div>
    <div class="guess-page col-xs-12 col-sm-12 col-md-12">
        <div class="box-guess-page">
            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <div class="row webike2018-guess-img">
                    <img src="{!! assetRemote('image/benefit/event/WebikeGuess/2018/guess2.jpg') !!}" alt="Name">
                </div>
            </div>
            <h1 class="title form-group font-bold size-12rem">注意事項</h1>
            <div class="box-guess-page-info-top">
                <table class="table borderless">
                    <tbody>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【活動期間】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【活動期間】</p><span class="dotted-text3.force-limit">2018年10月1日(一)~10月31日(三)的活動期間中，每日皆有任務題目，您可以參考<a href="#calender">※下方的日程表</a>。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【任務內容】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【任務內容】</p><span class="dotted-text3.force-limit">您可以在下方的<a href="#question">【今日任務】</a>區域中看到一張圖片，圖片為每日的任務題目，並且有數字1~9組成的區塊，請在圖片中找到任務目標，並點選該區塊，送出答案即可。※您可以選擇ON或OFF來開啟或關閉區塊的虛線及數字顯示。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【任務時間】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【任務時間】</p><span class="dotted-text3.force-limit">每日00：00至23：59皆可參與當日任務。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【任務解答】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【任務解答】</p><span class="dotted-text3.force-limit">在每日00：00變換任務之後，您可在下方的日程表裡看到前一日任務的正確答案。</span></td>
                    </tr>
                    <tr>
                        <td class="visible-md visible-lg"><span class="dotted-text3.force-limit">【點數發放】</span></td>
                        <td><p class="size-10rem visible-sm visible-xs">【點數發放】</p><span class="dotted-text3.force-limit">每次任務答題完成，如解答正確，我們會直接將點數發放至您的帳號內，您可以至<a href="{{route('customer-history-points')}}" target="_blank" title="點數獲得及使用履歷 - 「Webike-摩托百貨」">點數獲得及使用履歷</a>裡查看。</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h1 ><img src="{!! assetRemote('image/benefit/event/WebikeGuess/webikeguess_tit01.png') !!}" alt="" id="question"></h1>
        <div class="box-quiz-page box-fix-column" >
            <div class="col-lg-5 col-md-6 col-sm-7 col-xs-12 gamecontain">
                <ul class="ul-guess-img">
                    <li class="title"><span>按下Start按鈕，選取圖型變化的區塊，按下送出進行作答。</span></li>
                    <li class="photo">
                    	@if($question)
                        	<img class="after colorbox-13968 image_now" src="{{$question->after_img}}">
							<img class="before colorbox-13968 image_answer" src="{{$question->before_img}}">
						@endif
						<div class="choices" id="choices">
							<?php $x = array(0, 33.33, 66.66); ?>
							<?php $y = array(0, 33.33, 66.66); ?>
							<?php $value = 0; ?>
							@foreach ($y as $y_value)
								@foreach ($x as $x_value)
									<?php $value++; ?>
									<div id="{{$value}}" value="{{$value}}" style="left:{{$x_value}}%; top:{{$y_value}}%;"></div>
								@endforeach
							@endforeach
						</div>
						<div class="mask"></div>
						<img class="9cell" src="{!! assetRemote('image/benefit/event/WebikeGuess/9_cellnum.png') !!}" alt="">
						<span class="helper"></span>
                        
                    </li>
                    <li class="player">
                        <ul class="ul-player col-xs-block">
                            <li class="col-xs-4 col-sm-2 col-md-2 play">
                                <a class="btn-play" href="javascript:void(0)"><i class="fa fa-play" aria-hidden="true"></i><span>Start!</span></a>
                            </li>
							<li class="col-xs-8 col-sm-3 col-md-3 hidden-lg hidden-md hidden-sm">
								{{-- <div class="onoffswitch visible-xs visible-md">
                                    <div class="onoffswitch-inner">
                                        <div class="onswitch">
                                            <span>ON</span>
                                        </div>
                                        <div class="offswitch">
                                            <span>OFF</span>
                                        </div>
                                    </div>
                                </div> --}}
								<div class="onoffswitch">
									<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch2" checked>
									<label class="onoffswitch-label" for="myonoffswitch2">
                                        <span class="onoffswitch-inner">
                                    		<span class="onswitch">ON<span class="onoffswitch-switch" style="position:absolute;z-index:2;right:0px"></span></span>
                                    		<span class="offswitch">OFF<span class="onoffswitch-switch"  style="display:none"></span></span>
                                        </span>
									</label>
								</div>
							</li>
                            <li class="timer col-xs-12 col-sm-7 col-md-7">
                                <div>
                                    <p style="width: 0%"></p>
                                </div>
                            </li>
                            <li class="col-xs-12 col-sm-3 col-md-3 hidden-xs">
                            	{{-- <div class="onoffswitch visible-xs visible-md">
                            		<div class="onoffswitch-inner">
                            			<div class="onswitch">
                            				<span>ON</span>
                            			</div>
                            			<div class="offswitch">
                            				<span>OFF</span>
                            			</div>
                            		</div>
                            	</div> --}}
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch2" checked>
                                    <label class="onoffswitch-label" for="myonoffswitch2">
                                        <span class="onoffswitch-inner">
                                    		<span class="onswitch">ON<span class="onoffswitch-switch" style="position:absolute;z-index:2;right:0px"></span></span>
                                    		<span class="offswitch">OFF<span class="onoffswitch-switch"  style="display:none"></span></span>
                                        </span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </li>
                    @if(Auth::check() == 'true')
                        @if( isset($question) )
							<li class="btn-guess clearfix">
	                            <div class="col-xs-12 col-sm-12 col-md-12 ct-btn-guess">
	                                <div class="row">
	                                	@if( $customer_guess )
	                                		@if($customer_mission)
	                                			<?php $customer_answers =  explode( ',', $customer_guess->answer ); ?>
	                                			<p class="red_text" style="margin-bottom: 10px;"><span>※恭喜您答對了！您的答案為{{$customer_answers[0]}}。<br>恭喜您獲得10點現金點數。您可以至
												<a href="{{route('customer-history-points')}}" target="_blank" title="點數獲得及使用履歷 - 「Webike-摩托百貨」">點數獲得及使用履歷</a> 確認您獲得的現金點數。</span></p>
											@else
												<?php
											        $question_answers = explode(',', $question->answer);
											        $customer_answers =  explode( ',', $customer_guess->answer );
									        	?>
									        	<p class="red_text"><span>※您已經回答過今日的題目！您的答案為【{{$customer_answers[0]}}】。正確答案為【{{$question_answers[0]}}】！
									        	</span></p>
		                                    @endif
		                                @else
		                                    <form id="answer" method="post" style="text-align: center" action={{URL::route('benefit-event-webikeguess-post',array('type_id'=>1))}}>
		                                    	<input name="ans_date" type="text" value="{{date('Y-m-d')}}" style="display:none;">
		                                    	<input class="btn btn-danger" style="width: 50%;" type="submit" value="送出答案">
		                                    	<input id="customer_ans" name="customer_ans" type="text" value="" hidden="true">
		                                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		                                    </form>
		                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
										@endif
	                                </div>
	                            </div>
	                        </li>
                        @endif
                    @else
	                    <li class="btn-guess">
                            <div class="col-xs-12 col-sm-12 col-md-12 ct-btn-guess">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <a class="btn btn-warning" href="{{route('login')}}" title="會員登入{{$tail}}">會員登入</a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <a class="btn btn-danger" href="{{route('customer-account-create')}}" title="加入會員{{$tail}}">加入會員</a>
                                    </div>
                                    <span class="text-ct-detail font-color-red">※參加活動，請先登入會員。</span>
                                </div>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-5 col-xs-12 productinfo">
                <div class="box-ct-quiz-right">
                	@if( isset($question)  )
                    	<?php $product = $question->product; ?>
                    	@if( $question->product_id and $product )
		                    <div class="form-group container-quiz-right col-xs-12 col-sm-12 col-md-12">
		                        <a class="form-group col-xs-12 col-sm-12 col-md-12" href="{{URL::route('product-detail', $product->url_rewrite)}}" target="_blank"><h2>【{{$product->manufacturer->name}}】{{$product->name}}</h2></a>
		                        <span class="col-xs-12 col-sm-12 col-md-12">商品編號:{{$product->model_number}} <br><br> {{$question->description}}</span>
		                    </div>
		                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		                        <a class="btn btn-warning style-button-strong border-radius-2" href="{{URL::route('product-detail', $product->url_rewrite)}}" target="_blank">查看此商品</a>
		                    </div>
		                @else
		                	<div class="form-group container-quiz-right col-xs-12 col-sm-12 col-md-12">
		                        <a class="form-group col-xs-12 col-sm-12 col-md-12" href="{{URL::route('genuineparts')}}"><h2>【{{$question->genuine_maker}}】{{$question->genuine_name}}</h2></a>
		                        <span class="col-xs-12 col-sm-12 col-md-12">{{$question->description}}</span>
		                    </div>
		                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		                        <a class="btn btn-warning style-button-strong border-radius-2" href="{{URL::route('genuineparts')}}" target="_blank">HONDA正廠零件</a>
		                    </div>
		                @endif
	                @endif
                </div>
            </div>
       </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <h1 class="form-group"><img src="{!! assetRemote('image/benefit/event/WebikeGuess/webikeguess_tit02.png') !!}" alt=""></h1>
            <div class="box-calendar col-xs-12 col-sm-12 col-md-12" id="calender">
                <div class="row">
                    <ul class="title visible-sm visible-md visible-lg">
                        <li><span>一</span></li>
                        <li><span>二</span></li>
                        <li><span>三</span></li>
                        <li><span>四</span></li>
                        <li><span>五</span></li>
                        <li><span>六</span></li>
                        <li><span>日</span></li>
                    </ul>
                    <ul class="ct-calendar">
						@foreach($guesses as $key =>$guess)
							@if( $key == 0 and $guess->date > $calender_start )
								<?php
								$front_day = strtotime( current(current($guesses))->date ) - strtotime($calender_start); 
								$front_day = $front_day/86400;
								//dd($calender_start);
								?>
								@for ($i=0 ; $i < $front_day ; $i++)
									<li>
										<div class="ct-box-calendar">
											<div class="date">{{ date('m/d', strtotime( "+".$i." day", strtotime($calender_start)) ) }}</div>
										</div>
									</li>
								@endfor
							@endif
							@if($guess->date < $today)
	                            <li>
	                                <div class="ct-box-calendar">
	                                    <div class="date">{{ date('m/d', strtotime($guess->date) ) }}</div>
	                                    <a class="popup" href="javascript:void(0)" rel="leanModal">
	                                        <div class="cover answer">
	                                            <img src="{!! assetRemote('image/benefit/event/WebikeGuess/answer.png') !!}" alt="">
	                                        </div>
	                                        <img src={{$guess->before_img}} class="thum">
	                                        <div class="content" style="display:none">
	                                        	<?php 
													$guess_answers = explode(',', $guess->answer);
												?>
												<img class="image_now after colorbox-13968" src={{$guess->after_img}}>
												<img class="image_answer before colorbox-13968" src={{$guess->before_img}}>
												<h1 class="row font-color-red form-group col-xs-12 col-sm-12 col-md-12">正確答案為
												@foreach ($guess_answers as $guess_answer)
													【{{$guess_answer}}】
												@endforeach
												！
												</h1>
												<?php $product = $guess->product; ?>
												@if( $guess->product_id and $product )
													<span class="producturl">{{URL::route('product-detail', $product->url_rewrite)}}</span>
													<h2>【{{$product->manufacturer->name}}】{{$product->name}}</h2>
				                                    <span class="col-xs-12 col-sm-12 col-md-12">
				                                        商品編號:{{$product->model_number}} <br><br>
				                                        {{$guess->description}}
				                                    </span>
				                                    <span class="href">{{URL::route('product-detail', $product->url_rewrite)}}</span>
				                                    <span class="hreftext">查看此商品</span>
				                                @else
				                                	<span class="producturl">{{URL::route('genuineparts')}}</span>
													<h2>【{{$guess->genuine_maker}}】{{$guess->genuine_name}}</h2>
				                                    <span class="col-xs-12 col-sm-12 col-md-12">
				                                        {{$guess->description}}
				                                    </span>
				                                    <span class="href">{{URL::route('genuineparts')}}</span>
				                                    <span class="hreftext">{{$guess->genuine_maker}}正廠零件</span>
				                                @endif
	                                        </div>
	                                    </a>
	                                </div>
		                        </li>
		                    @elseif( $guess->date == $today )
								<li>
									<div class="ct-box-calendar">
										<div class="date" >{{ date('m/d', strtotime($guess->date) ) }}</div>
										<a href="javascript:void(0)" onclick="slipTo1('#question')">
	                                        <div class="cover answer">
	                                            <img src="{{$photo_path1}}start_icon.gif" alt="" style="margin-top: 77%;">
	                                        </div>
	                                        <img src={{$guess->before_img}} class="thum">
	                                        <div class="content" style="display:none">
												<img class="image_now after colorbox-13968" src={{$guess->after_img}}>
												<img class="image_answer before colorbox-13968" src={{$guess->before_img}}>
	                                        </div>
	                                    </a>
									</div>
								</li>
							@else
								<li>
									<div class="ct-box-calendar">
										<div class="date" >{{ date('m/d', strtotime($guess->date) ) }}</div>
                                        <img src="{{$photo_path1}}coming_soon.png" style="background-size: 100% 100%;position: absolute; left:0;margin-top: 24px;" class="thum">
                                        <img src={{$guess->before_img}} class="thum" style="margin-bottom:20px">
                                        <div class="content" style="display:none">
											<img class="image_now after colorbox-13968" src={{$guess->after_img}}>
											<img class="image_answer before colorbox-13968" src={{$guess->before_img}}>
                                        </div>
									</div>
								</li>
		                    @endif
	                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!--Box show detail-->
        <div id="answers">
            <div id="ans" class="answer">
                <div class="box-quiz-page box-fix-column">
                    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 gamecontain">
                        <ul class="ul-guess-img clearfix">
                            <li class="title clearfix"><span>按下Start按鈕，選取圖型變化的區塊，按下送出進行作答。</span></li>
                            <li class="photo">
                            	<img class="image_now after colorbox-13968" src=''>
								<img class="image_answer before colorbox-13968" src=''>
								<div class="mask">
								</div>
								<img class="9cell" src="{!! assetRemote('image/benefit/event/WebikeGuess/9_cellnum.png') !!}" alt="">
                            </li>
                            <li class="player">
                                <ul class="ul-player">
                                    <li class="col-xs-2 col-sm-2 col-md-2 play">
                                        <a class="btn-play" href="javascript:void(0)"><i class="fa fa-play" aria-hidden="true"></i><span>Start!</span></a>
                                    </li>
                                    <li class="timer col-xs-7 col-sm-7 col-md-7">
                                        <div>
                                            <p style="width: 0%"></p>
                                        </div>
                                    </li>
                                    <li class="col-xs-3 col-sm-3 col-md-3">
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked="">
                                            <label class="onoffswitch-label" for="myonoffswitch">
                                                <span class="onoffswitch-inner">
                                                	<span class="onswitch">ON<span class="onoffswitch-switch" style="position:absolute;z-index:2"></span></span>
                                        			<span class="offswitch">OFF<span class="onoffswitch-switch" style="display:none"></span></span>
                                                </span>
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12 productinfo">
                        <div class="close"><img src="{!! assetRemote('image/benefit/event/WebikeGuess/20130628_webiquiz_modal_close.png') !!}" alt=""></div>
                        <div class="box-ct-quiz-right">
                            <h1 class="row font-color-red form-group col-xs-12 col-sm-12 col-md-12"></h1>
                            <button class="baseBtn_red btn btn-info hidden-sm hidden-xs" style="display: block;float: none;font-size: 18px;padding: 5px;">題目解答</button>
                            <div class="form-group container-quiz-right col-xs-12 col-sm-12 col-md-12">
                                <a class="form-group col-xs-12 col-sm-12 col-md-12" href=""><h2></h2></a>
                                <span class="col-xs-12 col-sm-12 col-md-12"></span>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <a class="btn btn-warning style-button-strong border-radius-2" target="_blank" href=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End box save detail -->
    </div>
	
@stop
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://img.webike.net/js/jquery/jquery.unveil.js"></script>
<script type="text/javascript" src="https://img.webike.net/sys_images/saimg/jquery.searcher.js"></script>
<script type="text/javascript" src="https://img.webike.net/js/jquery/purl.js"></script>
<script type="text/javascript">

<!--
var randnum = Math.floor( Math.random() * 1000 );
document.write('<script type="text/javascript" src="https://img.webike.net/sys_images/ca169/20150320_webiquiz.json?' + randnum + '"><\/script>');
document.write('<script type="text/javascript" src="https://img.webike.net/sys_images/ca169/20150320_webiquiz_answer.json?' + randnum + '"><\/script>');
-->
</script>
<script type="text/javascript" src="https://img.webike.net/sys_images/dscom/leanModal.js"></script>

<script type="text/javascript">
/*
$(window).resize(function(){
 	var height = $('.image_now').height();
 	var width = $('.image_now').width();

 	$('.image_now').closest('.photo').find('.choices').height(height);
 	$('.image_now').closest('.photo').find('.choices').width(width);
 	$('.image_now').closest('.gamecontain').find('.player').width(width);
 	$('.image_now').closest('.gamecontain').find('.btn-guess').width(width);
//          //  $('.contestant-info').closest('.nail-box').height(height);
    // var height = $('.discussfb').height() + 50;
    // $('.discuss').height(height);
});

$(document).ready(function(){
	var height = $('.image_now').height();
 	$('.image_now').closest('.photo').height(height);
 	$('.image_now').closest('.mask').height(height);
});
*/
var alert_msg = '{{\Session::get("msg")}}';
if( alert_msg != null && alert_msg.length > 0 ){
	alert( alert_msg );
}

jQuery(function () {

/*---------------------------------------------
カレンダーと当日問題表示
---------------------------------------------*/ 
	var html1 = "";
	var html2 = "";
	var count1 = 0;
	var count2 = 0;
	
	var url = jQuery.url();
	var date = url.param("date");

	if(date){
		var today = date;
	}else{
		var today = String(2015062218);
	}
});
jQuery(function () {
	jQuery('a[rel*=leanModal]').leanModal({
		top:50,
		overlay :0.5,
		closeButton: ".close",
		modalparent: "#answers"
	});
	
});
jQuery(function () {
	var setImg = '.photo';
	var speedTime = 15000;
	var delayTime = 1000;

//	jQuery(document).on('click','form',function(e)
	jQuery('form#answer').submit(function(){
	    if(!$('#customer_ans').attr('value').length){
	    	$(this).find('p').remove();
	    	$(this).append('<p class="red_text">請先選擇答案。</p>');
			return false;
//			e.preventDefault();
	    }else{
	    	$('#answer .baseBtn_yellow').prop('disabled', true);
			return true;
	    }

	});

//	jQuery(document).on('click','.choices div',function(){
	$('.choices div').click(function(){
		jQuery('.box-quiz-page .photo .choices div').removeClass('answer-selected');
		$('#customer_ans').attr('value', $(this).attr('id'));
//		jQuery(this).parent().children('div').css({'opacity':'0.4','background':'none'});
		jQuery(this).addClass('answer-selected');
	});
	//通常再生ボタンクリック
	jQuery(document).on('click','.player .play', function(){
		
		//告知時のみ使用
		jQuery('.player .ans').removeClass('ans').addClass('ansing');
		var timer = jQuery(this).closest('.ul-player').children('.timer');
		var playing = jQuery(this).closest('.ul-player').children('.playing').attr('class');
		var photo = jQuery(this).closest('.ul-guess-img').children('.photo');
		//
		jQuery(this).removeClass('play').addClass('playing');
		jQuery(timer).children('div').css({'background-color':'white'}); //遊戲開始規0
		// jQuery(photo).children('.message').css({display:'none',opacity:'0'});
		jQuery(photo).children('img').css({opacity:100});
		jQuery(timer).children().children('p').css({'width':'0','background':'#ffba00'}); //遊戲開始規0
		jQuery(photo).children('.mask').css({'opacity':100,'z-index':2}).delay(delayTime).fadeTo(0, 0); //九宮格圖片在上面 
		jQuery(photo).children('img:nth-child(2)').delay(delayTime).fadeTo(speedTime, 0); //跑遊戲時間
		jQuery(timer).children().children('p').delay(delayTime).animate({width:'100%'},speedTime,'',function(){
			jQuery('.player .playing').removeClass('playing').addClass('play');
			jQuery(photo).children('.message').css({display:'block',opacity:'100'});
			jQuery(photo).children('.mask').css({'opacity':0,'z-index':0});
			jQuery('.player .ansing').removeClass('ansing').addClass('ans');
		}); 
	});

	jQuery(document).on('click','.popup',function(){
		var after = jQuery(this).children('.content').children('.after').attr('src');
		var before = jQuery(this).children('.content').children('.before').attr('src');
		var answer = jQuery(this).children('.content').children('.row').text();
		var producturl = jQuery(this).children('.content').find('span.producturl').text();
		var product = jQuery(this).children('.content').find('h2').text();
		var description = jQuery(this).children('.content').find('span.col-xs-12').html();
		var href = jQuery(this).children('.content').find('.href').text();
		var hreftext = jQuery(this).children('.content').find('.hreftext').text();
		
		
		jQuery('#ans').find('.photo').children('.after').attr('src',after);
		jQuery('#ans').find('.photo').children('.before').attr('src',before);
		jQuery('#ans').find('.box-ct-quiz-right').children('h1').text(answer);
		jQuery('#ans').find('h2').parent('a').attr('href',producturl);
		jQuery('#ans').find('h2').text(product);
		jQuery('#ans').find('.box-ct-quiz-right').find('span').html(description);
		jQuery('#ans').find('.btn.btn-warning').attr('href',href);
		jQuery('#ans').find('.btn.btn-warning').text(hreftext);

	});

	// 	//解答の解答ボタンクリック
		jQuery(document).on('click','.box-ct-quiz-right .baseBtn_red', function(){
			var answer = jQuery('#ans').children().children().children().children().children().children('.play');
			jQuery(answer).prev().removeClass('play').addClass('playing');
			jQuery(answer).removeClass('ans').addClass('ansing');
			jQuery(answer).closest('.ul-guess-img').children('.photo').children('.message').css({display:'none',opacity:'0'});
			jQuery(answer).closest('.ul-guess-img').children('.photo').children('img').css({opacity:100});
			jQuery(answer).next('.timer').children('div').css("background","#e61e25");
			jQuery(answer).next('.timer').children('div').children('p').css("background","#e61e25");
			jQuery(answer).next('.timer').children().children('p').css({'width':'0'});
			jQuery(answer).closest('.ul-guess-img').children('.photo').children('.mask').css({'opacity':100,'z-index':2}).fadeTo(0, 0); 
			jQuery(answer).closest('.ul-guess-img').children('.photo').children('img:nth-child(2)').show(0).delay(1000).hide(0).delay(1000).show(0).delay(1000).hide(0).delay(1000).show(0).delay(1000).hide(0).delay(1000).show(0);
			jQuery(answer).closest('.ul-guess-img').children('.photo').children('img:nth-child(1)').show(0);

			jQuery(answer).next('.timer').children().children('p').delay(delayTime).animate({width:'100%'},6000,'',function(){
					jQuery(answer).stop();
					jQuery(answer).removeClass('playing').addClass('play');
					// jQuery('.player .ansing').removeClass().addClass('ans');
			});   
		});

	function offSwitch(target){
		jQuery(target).closest('.ul-guess-img').children('.photo').children('.9cell').css('display','none');
	}
	function onSwitch(target){
		jQuery(target).closest('.ul-guess-img').children('.photo').children('.9cell').css('display','block');
	}

	// jQuery(document).on('click','.onoffswitch-switch', function(){
	// 	var c = jQuery(this).parent().prop('class');
	// 	// $(this).css('display','block');
	// 	$(this).css('display','none');
		
	// 	if(c == 'offswitch'){
	// 	// 	alert(c);
	// 		// jQuery(this).closest('.ul-guess-img').children('.photo').children('.9cell').css('display','none');
	// 	// 	// offSwitch(this);
	// 		// $(this).closest('.onoffswitch-inner').find('.onswitch .onoffswitch-switch').css('display','block');
	// 	}
	// 	else if(c == 'onswitch'){

	// 	// 	onSwitch(this);
	// 		// $(this).closest('.onoffswitch-inner').find('.offswitch .onoffswitch-switch').css('display','block');
	// 	}
		
	// });
//    $('.onswitch').click(function(){
//        alert('123');
//        $('.onswitch').css('display','none !important');
//        $('.offswitch').css('display','block !important');
//    });
//    $('.offswitch').click(function(){
//        $('.offswitch').css('display'.'none !important');
//        $('.onswitch').css('display','block !important');
//    });
	 jQuery(document).on('click','.onoffswitch-inner .offswitch', function(){
	 	$(this).css('display','none');
	 	$(this).parent('.onoffswitch-inner').find('.onswitch').css({'display':'block'});
	 	onSwitch(this);
	 	// $(this).closest('.onoffswitch-inner').find('.onswitch .onoffswitch-switch').css('display','block');
	 });
	 jQuery(document).on('click','.onoffswitch-inner .onswitch', function(){
	 	$(this).css('display','none');
	 	$(this).parent('.onoffswitch-inner').find('.offswitch').css({'display':'block'});
	 	offSwitch(this);
	 });

});


</script>
<script type="text/javascript">
	var pulldown = {{$finish}}; 
	if( pulldown == true ){
	    var y = $('#question').offset().top - 160;
	    $('html,body').animate({scrollTop: y},800);
	}
	function slipTo1(element){
	    var y = parseInt($(element).offset().top) - 120;
	    $('html,body').animate({scrollTop: y}, 400);
	}
</script>
@stop
