@extends('response.layouts.2columns')
@section('style')
    <style>
        ul {
            list-style-type:none;
        }
        .box-items-group li {
            margin:20px;
        }
        .benefit_div img{
            width: 100%;
            height: auto;
        }
        .benefit_text {
            display: none;
        }
        .recom a {
            border: 1px solid #e0e6f0;
        }
        .benefit-title {
            margin-bottom:0px;
        }
        .benefit_div a{
            margin-bottom: 10px;
            display: block;
        }
        .benefit_div .benefit_text{
            text-align: left;
            font-size: 1rem;
        }
    </style>
    <?php
    if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE])){
        $zones = [
            '3' => 13,
            '4' => 14,
            '9' => 19,
            '10' => 20,
        ];
    }else{
        $zones = [
            '3' => 3,
            '4' => 4,
            '9' => 9,
            '10' => 10,
        ];
    }
    ?>
@stop
@section('left')
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>免費好康活動</h3>
        </div>
        <div id="list_benefit" class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group">

            </ul>
        </div>
    </div>
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>熱門推薦</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-banner-ct-box-page-group">
                @for($i = 1 ; $i <= 10 ; $i++)
                    <li>
                        <div class="benefit_div recom">
                            {!! $advertiser->callTest($zones[9],'',false) !!}
                        </div>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
    <div class="box-page-group">
        <div class="title-box-page-group">
            <h3>服務專區</h3>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-banner-ct-box-page-group">
                @for($i = 1 ; $i <= 10 ; $i++)
                    <li>
                        <div class="benefit_div">
                            {!! $advertiser->callTest($zones[10],'',false) !!}
                        </div>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
@stop
@section('right')
    <div class="box-items-group">
        <div class="title-main-box benefit-title">
            <h1>會員好康</h1>
        </div>
        <?php date_default_timezone_set('Asia/Taipei'); ?>
        @if( strtotime(date('Y-m-d H:i:s')) >= strtotime(date('2017-05-29')) and strtotime(date('Y-m-d H:i:s')) < strtotime(date('2017-06-03')))
            <div class="ct-box-items-group list_source">
                <ul class="ul-ct-box-items-group-benefit">
                    <li>
                        <a href="{{ route('cart') }}?rel=2017-06-bannerbar-forth-anniversary" target="_blank">
                            <img src="{{assetRemote('image/banner/4anniversary.png')}}">
                        </a>
                    </li>
                    <li>
                        <p class="size-10rem">感謝台灣車友4年來的支持與指教，5/29-6/2全館商品不限金額免運費，歡迎共襄盛舉。</p><br>
                        <p class="size-10rem">活動期間： 2017/05/29(一)~06/02(五)</p>
                        <p class="size-10rem">活動內容： 凡是周年慶活動期間在「Webike-摩托百貨」購物的會員可享有購物免運費優惠。</p>
                        <p class="size-10rem">免運費折價券：在5/29(一)12：00 AM，將會發放二張折價券，每張可折抵購物運費乙次，使用期限只到06/02(五)。</p>
                        <p class="size-10rem">※每張訂單僅可使用一張折價券，折價券可以與現金點數合併使用；折價券僅可折抵商品金額，貨到付款手續費無法抵用。</p>
                    </li>
                </ul>
            </div>
        @endif
    </div>
    <div class="box-items-group">
        <div class="title-main-box-2">
            <h2>★本月特價促銷★</h2>
        </div>

        <div class="ct-box-items-group text-center list_source" id="sales">
            <ul class="ul-ct-box-items-group-benefit">
                @for($i = 1 ; $i <= 10 ; $i++)
                    <li id="banner-24-model">
                        <div class="benefit_div">
                            {!! $advertiser->callTest($zones[3],'',false) !!}
                        </div>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
    <div class="box-items-group">
        <div class="title-main-box-2">
            <h2>★免費好康活動★</h2>
        </div>
        <div class="ct-box-items-group text-center list_source" id="benefit">
            <ul class="ul-ct-box-items-group-benefit">
                @for($i = 1 ; $i <= 10 ; $i++)
                    <li id="banner-24-model">
                        <div class="benefit_div">
                            {!! $advertiser->callTest($zones[4],'',false) !!}
                        </div>
                    </li>
                @endfor
            </ul>
        </div>
    </div>
    @if(!$current_customer)
        <div class="row block">
            <div class="col-md-4 hidden-sm hidden-xs"></div>
            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                <a class="btn btn-warning btn-full" href="{{route('customer-account-create')}}" title="加入會員{{$tail}}">加入會員</a>
            </div>
            <div class="col-md-4 hidden-sm hidden-xs"></div>
        </div>
    @endif
@stop

@section('script')
    <script type='text/javascript'>
        $( "div[id^=beacon] img" ).load(function() {
            benefit_div = $(this).parents('div.benefit_div');
            image = benefit_div.find('img:first');
            area_id = image.parents('div.list_source').attr('id');
            link  = "javascript:void(0)";
            parent = image.parent();
            if(parent.is('a')){
                link = parent.attr('href');
            }
            if(image.attr("alt")){
                $('#list_'+area_id).find('ul').append('<li> <a href="'+link+'"><span>'+image.attr("alt") +'</span></a></li>');
            }

            //adjust image
//            benefit_div.find('img:first').removeAttr('width').removeAttr('height');
            //display text
            $('.benefit_text').show();
        });
    </script>
@stop