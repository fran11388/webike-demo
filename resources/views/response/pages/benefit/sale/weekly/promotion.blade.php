@extends('response.layouts.1column-2019weekly')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/benefit/sale/2019-weekly-promotion.css') }}">
<link rel="stylesheet" href="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.css') }}">
<style type="text/css">
.promotion-container .page-block .page-block-title{
	font-size: 34px;
	text-align: center;
	background: rgb( 5, 48, 125);/*區塊title 背景，同主題色*/
	color: #ffffff;/*區塊title 文字顏色，同主題文字色*/
	margin: 0 auto;
	padding: 20px 0;
}
.promotion-container .page-block .page-block-product{
	width: 100%;
	margin: 0 auto;
	padding: 20px 0;
	border: 1px solid;
    background: #fff;
}
.promotion-container .promotion-btn{
    font-weight: bold;
    padding: 15px 35px;
    border-radius: 50px;
    background-color: #cfe2f3;/*按鈕背景顏色，同錨點*/
    color: #000;
    display: block;
    margin: 30px auto 15px;
    width: 450px;
    font-size: 22px;
    text-decoration: none;
    text-align: center;
    animation-duration: 0.3s;
}
.promotion-container .promotion-btn:hover{
	background-color: #e4ecf3;/*按鈕背景顏色，同錨點hover*/
}
.promotion-container .product-banner:hover{
    opacity: 0.7;
}
.main-background-color{
	background: rgba( 5, 48, 125, 0.6);/*有透明度的主題色*/
	padding: 10px;
}
.page-block .product_area .list-barrier{
	background: rgb( 5, 48, 125);/*分類及車型文字背景，同主題色*/
	color: #fff;
	width: 100%;
	height: 100%;
	text-align: center;
}
.page-block .text-tag{
	background: #ff0000;
    font-size: 1.25rem;
    width: 95px;
    margin: 0 auto;
}
</style>
@stop
@section('middle')
@include('response.pages.benefit.sale.weekly.otherblock.main-banner')
@include('response.pages.benefit.sale.weekly.otherblock.search-bar')
<div class="promotion-page-block">
	<div class="promotion-block">
		@include('response.pages.benefit.sale.weekly.otherblock.links-bar')
		@include('response.pages.benefit.sale.weekly.otherblock.weekly-sale')
		@include('response.pages.benefit.sale.weekly.otherblock.double-point')
		@include('response.pages.benefit.sale.weekly.otherblock.category')
		@include('response.pages.benefit.sale.weekly.otherblock.motor')
		@include('response.pages.benefit.sale.weekly.otherblock.collection')
	</div>
</div>
@stop
@section('script')
     <script src="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>

        $(function() {
            $( "#search_bar" ).autocomplete({
                minLength: 1,
                source: solr_source,
                focus: function( event, ui ) {
                    $('#search_bar').val();
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'header');
                    return false;
                },
                change: function(event, ui) {

                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }
                
                return bag
                // .append( '<a href="http://localhost/test">' + item.icon + "<p>" + item.label + "</p></a>" )
                    .append( '<a  href="javascript:void(0)" class="search-keyword">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
        });

        
        var current_suggest_connection = null;
        function solr_source(request, response){
            var params = {q: request.term};

            current_suggest_connection = $.ajax({
                url: "{{ route('api-suggest')}}",
                method:'GET',
                data : params,
                dataType: "json",
                beforeSend: function( xhr ) {
                    if(current_suggest_connection){
                        current_suggest_connection.abort();
                    }
                },
                success: function(data) {
                    response(data);
                }
            });
        }
       $(document).on('click', ".ui-menu li.ui-menu-item", function(){
            var keyword = $(this).find('span.font-normal').text();
            $('.search-text #search_bar').val(keyword);
        });

        $('#search .search-btn input').click(function(){
        	keyword = $('.search-text #search_bar').val();
        	if(keyword){
        		window.location = '{{route('parts')}}' + '?q=' + keyword;
        	}
        });
    </script>
    <script type="text/javascript">
		$('.owl-carouse-product-loop').addClass('owl-carousel').owlCarousel({
			autoplayTimeout:1500,
			autoplayHoverPause:true,
			loop:true,
			margin:10,
			nav:false,
		    slideBy : 1,
		    center: true,
		    autoplay: true,
			autoPlaySpeed: 500,
			dots: true,
			autoplayHoverPause: false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});
		$(document).ready(function(){
			$('.product-list .loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
		});

		$(".page-block .product-list").mouseenter(function(){
		  $(this).find('.loop-photo').removeClass('hidden');
		  $(this).find('.loop-photo .owl-carouse-product-loop').trigger('play.owl.autoplay',[1500]);
		  $(this).addClass('active');
		  setTimeout(function(){ 
		  		$('.page-block .product-list.active .owl-carouse-product-loop li').removeClass('hidden');
		  }, 500);
		});

		$(".page-block .product-list").mouseleave(function(){
		  	$(this).find('.loop-photo').addClass('hidden');
		  	$(this).removeClass('active');
		  	$(this).find('.loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
		});
	</script>
		<script type="text/javascript">
		
		$(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.ct-table-cell').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-model", function(){
            var value = $(this).find('option:selected').val();
            if ((value) && (value != "車型")){
                link = '{{route('parts')}}' + '/mt/' + value;
            	$('#motor .page-block-product .promotion-btn').removeClass('btn-disable').attr('href',link);
            }else{
            	$('#motor .page-block-product .promotion-btn').addClass('btn-disable').attr('href','javascript:void(0)');
            }
        });

        $(document).on('click', ".select-motor-mybike", function(){
            var value = $(this).attr('name');
            motor_name = $(this).text();
            if (value){
            	$('.btn-my-bike span').text(motor_name);
            	link = '{{route('parts')}}' + '/mt/' + value;
            	$('#motor .page-block-product .promotion-btn').removeClass('btn-disable').attr('href',link);
            }else{
            	$('#motor .page-block-product .promotion-btn').addClass('btn-disable').attr('href','javascript:void(0)');
            }
        });

        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 50;
            $('html,body').animate({scrollTop: y}, 400);
        }
	</script>
	<script type="text/javascript">

		$(document).ready(function(){
			$('.main-banner .banner-container .theme-block ').removeClass('theme-text-hidden');
			var theme_block_height = $('.main-banner .banner-container .theme-block ').height() + 10;
			$('.banner-container .banner-block .theme-block').css('bottom','-' + theme_block_height + 'px');
			setTimeout(function(){
				showThemeTitle();
			},500);
			setTimeout(function(){
				showTheme();
			},2000);
		});

		$('.main-banner .banner-container .theme-block i').on('click',function(){
			
			if(($(this).hasClass('fa-chevron-up') && !($('.main-banner .banner-container .theme-block ').hasClass('open')))){
					showTheme();
			}else{
				$('.main-banner .banner-container .theme-block i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
				$('.main-banner .banner-container .theme-block .main-theme').removeClass('hidden');

				showThemeTitle();

				setTimeout(function(){
					$('.main-banner .banner-container .theme-block  span.theme-text').addClass('theme-text-hidden');
				},550);
			}			
		});

		function showThemeTitle(){
			var theme_text_height = $('.main-banner .banner-container .theme-block  span.theme-text').height() + 3;
			var height = '-' + theme_text_height + 'px';
			$('.banner-container .banner-block .theme-block').animate({bottom: height},500);
			$('.main-banner .banner-container .theme-block ').removeClass('open');
		}

		function showTheme(){
			$('.banner-container .theme-block').animate({bottom: "0px"},1000);
			$('.main-banner .banner-container .theme-block i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
			$('.main-banner .banner-container .theme-block  span.theme-text').removeClass('theme-text-hidden');
			$('.main-banner .banner-container .theme-block ').addClass('open');
		}

		$('.owl-carouse-main-banner').addClass('owl-carousel').owlCarousel({
			animateOut: 'fadeOut',
			animateIn: 'flipInX',
			items:1,
			center: true,
		    autoplay: true,
		    // autoplay: false,
			autoPlaySpeed: 1000,
			loop:true,
			mouseDrag:false,
			dots: true,
			smartSpeed:500
		});
	</script>
	<script type="text/javascript">
		$('.page-block .block-product .product_area .list-page').mouseenter(function(){
			$(this).find('div.list-barrier').removeClass('hidden');
			$(this).find('img.image-cover').addClass('hidden');
		});
		$('.page-block .block-product .product_area .list-page').mouseleave(function(){
			$(this).find('div.list-barrier').addClass('hidden');
			$(this).find('img.image-cover').removeClass('hidden');
		});
	</script>
@stop