<div class="page-block" id="motor">
	<h2 class="page-block-title">當週加倍車型</h2>
	<div class="page-block-product">
		<div class="block-product">
			<div class="product_area">
				@php
					$motors = [
						'CBR1000RR' => ['image' => 'https://img.webike.tw/imageUpload/15472017151547201715761.jpg','link' => '303'],
						'YZF-R1' => ['image' => 'https://img.webike.tw/imageUpload/15472016621547201662395.jpg','link' => '926'],
						'GSX-R1000' => ['image' => 'https://img.webike.tw/imageUpload/15472016461547201646981.jpg','link' => '672'],
						'ZX-10R' => ['image' => 'https://img.webike.tw/imageUpload/15472016331547201633174.jpg','link' => '465'],
						'YZF-R6' => ['image' => 'https://img.webike.tw/imageUpload/15472016751547201675863.jpg','link' => '896'],
						'CBR600RR' => ['image' => 'https://img.webike.tw/imageUpload/15472017021547201702466.jpg','link' => '256']
					];
				@endphp
				<ul class="clearfix motor-banner">
					@foreach($motors as $motor_name => $motor)
						<li class="list-page">
							<a href="{{ route('summary','mt/'.$motor['link'].'?'.$rel_parameter)}}" target="_blank" class="zoom-image">
								<div class="list-barrier hidden">
									<div class="vertical-middle">
										<span class="size-19rem">{{$motor_name}}</span>
										<div class="text-center">
											<span class="label text-tag">點數2倍</span>
										</div>
									</div>
									<span class="vertical-middle list-height">
									</span>
								</div>
								<img src="{{$motor['image']}}" class="motor-img">
							</a>	
						</li>
					@endforeach
				</ul>
			</div>
			<div class="product_area">
				<div class="ct-table-cell">
		            <ul class="ul-cell-dropdown-list container">
		                <li class="li-cell-dropdown-item">
		                    <div class=" select-box">
		                        <select class="select2 select-motor-manufacturer">
		                            <option>請選擇廠牌</option>
		                            @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
		                                <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
		                            @endforeach
		                        </select>
		                    </div>
		                </li>
		                <li class="li-cell-dropdown-item">
		                    <div class=" select-box">
		                        <select class="select2 select-motor-displacement">
		                            <option>cc數</option>
		                        </select>
		                    </div>
		                </li>
		                <li class="li-cell-dropdown-item">
		                    <div class=" select-box">
		                        <select class="select2 select-motor-model">
		                            <option>車型</option>
		                        </select>
		                    </div>
		                </li>
		                @include('response.common.list.filter-mybike')
		            </ul>
		        </div>
		        <div class="text-center">
		        	<a class="promotion-btn btn-disable" href="javascript:void(0)"  target="_blank">車型搜尋</a>
		        </div>
			</div>
		</div>
	</div>
</div>