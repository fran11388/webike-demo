<style type="text/css">
	.search{
	    padding: 15px 0px;
		background-color: #05307d; /*搜尋背景，同主題色*/
		position: relative;
		z-index: 10;
	}
	.search .search-text{
		background: #fff;
		border-radius: 25px 0 0 25px;
		border: none;
		width: auto;
		padding: 14px;
		overflow: hidden;
	}
	.search .search-text input{
		border: 0;
		width: 100%;
	    font-family: FontAwesome, 'メイリオ';
	}
	.search .search-btn{
		float: right;
	}
	.search .search-btn input{
		font-family: FontAwesome, 'メイリオ';
		background: #910000;
		font-weight: bold;
		color: #fff;
		border: 0;
		padding: 15px;
		border-radius: 0 25px 25px 0;
		cursor: pointer;
	}
</style>
<div id="search" class="search">
	<div class="block-container">
		<div class="search-btn">
			<input type="submit" value="">			
		</div>
		<div class="search-text">
			<input id="search_bar" type="text" name="search" autocomplete="off" placeholder="&#xF002; 請輸入關鍵字(54萬項商品、1840家廠牌)">
		</div>
	</div>
</div>