<style type="text/css">

.banner-area{
	position: relative;
}
.banner-container .banner-block.theme .theme-block{
	color: #ffffff; /*主題文字顏色*/
	position: absolute;
	padding: 10px 10px 20px;
	z-index: 1;
	bottom: 0;
	width: 47%;
    text-shadow: 2px 2px 2px #000000;
}
.owl-carouse-main-banner .owl-dots{
    text-align: center;
    background: #cfe2f3; /* dost區塊背景色，同錨點原始背景色*/
}
.owl-carouse-main-banner .owl-dots .owl-dot{
    display:inline-block;
    margin-left: 10px;
}
.owl-carouse-main-banner .owl-dots .owl-dot span{
    background: #fff;
    border: 1px solid #fff;
    width: 10px;
    height: 10px;
    display: block;
    border-radius: 999em;
    cursor: pointer;
}
.owl-carouse-main-banner .owl-dots .owl-dot.active span{
    background: #797979;
    border: 1px solid #797979;
}
.theme-block h2 i.fa{
	background: #cfe2f3; /*箭頭按鈕背景色，同錨點原始背景色*/
    padding: 5px;
    border-radius: 999em;
    cursor: pointer;
}
.main-banner .banner-container .banner-area .theme-block h2 span{
	font-size: 4rem;
	letter-spacing: 5px;
}
.theme-text-hidden {
	visibility: hidden;
}
.owl-carouse-main-banner .animated { 
  animation-duration: 3500ms;
  animation-fill-mode: both; }
</style>
<div class="main-banner">
	<div class=" banner-container clearfix">
		<div class="banner-block theme">
		    <div class="banner-area theme-area">
		    	<ul class="owl-carouse-main-banner">
		    		@php
		    			$banner = ['https://img.webike.tw/imageUpload/15471980151547198015831.jpg','http://img.webike.tw/assets/images/weekly/cbr.png','https://img.webike.tw/imageUpload/15471980981547198098767.jpg','https://img.webike.tw/imageUpload/15471981131547198113858.jpg','http://img.webike.tw/assets/images/weekly/v4.jpg'];
		    		@endphp
		    		@foreach($banner as $img)
			    		<li class="width-max">
				            <img class="image-cover" src="{!! $img !!}">
			    		</li>
		    		@endforeach
		    	</ul>
		        <div class="theme-block main-background-color theme-text-hidden">
	        		<h2 class="btn-gap-bottom">
		        		<span class=" vertical-middle">仿賽主題週</span>
		        		<i class="fa fa-chevron-down" aria-hidden="true"></i>
		        	</h2>
		        	<span class="theme-text size-12rem theme-text-hidden">如賽車般的外觀，低趴的姿勢，跨上車就進入戰鬥狀態。仿賽車顧名思義，為在封閉場地比賽的賽車的市售版本，通常也是各家工藝技術的集大成。製造商取自在賽事中獲得的經驗.，並將配備下放至一般市售車種，成為戰鬥氣息滿點的仿賽車。本週我們為您準備了仿賽車的相關品牌以及分類，一起享受這個熱血沸騰的氣息吧！
					</span>
		        </div>
		    </div>
		</div>
	</div>
</div>