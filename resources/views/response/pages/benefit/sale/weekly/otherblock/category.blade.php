<div class="page-block" id="category">
	<h2 class="page-block-title">仿賽熱門分類</h2>
	<div class="page-block-product">
		<div class="block-product">
			<div class="product_area">
				@php
					$categories=[
					'賽車手套' => ['image' => 'https://img.webike.tw/imageUpload/15471988331547198833730.jpg','link' => '3000-3020-3060-3061'],
					'全罩式安全帽' => ['image' => 'https://img.webike.tw/imageUpload/15477967421547796742173.jpg','link' => '3000-3001-3002'],
					'排氣管尾段' => ['image' => 'https://img.webike.tw/imageUpload/15471989541547198954569.jpg','link' => '1000-1001-1003'],
					'頭罩、擋風鏡' => ['image' => 'https://img.webike.tw/imageUpload/15471988641547198864559.JPG','link' => '1000-1110-1109']
					];
				@endphp
				<ul class="clearfix">
					@foreach($categories as $category_name => $category)
						<li class="col-md-3 col-sm-3 col-xs-6 list-page">
							<a href="{{ route('summary','ca/'.$category['link'].'?'.$rel_parameter )}}" target="_blank" class="zoom-image">
								<div class="list-barrier hidden">
									<div class="vertical-middle">
										<span class="size-19rem">{{$category_name}}</span>
										<span class="text-tag label">點數2倍</span>
									</div>
									<span class="vertical-middle list-height">
									</span>
								</div>
								<div class="list-height">
									<img class="image-cover " src="{{$category['image']}}">
								</div>
							</a>	
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>