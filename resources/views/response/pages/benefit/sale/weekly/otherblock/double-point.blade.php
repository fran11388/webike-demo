<div class="page-block" id="double-point">
		<h2 class="page-block-title">當週加倍品牌</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix">
					{{-- @foreach($discount_brands as $discount_brand_url_rewrite => $discount_brand)
						<li class="product-list li-list">
							<a href="{{route('summary', 'br/'.$discount_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
								<div class="product-img">
									<img src="{{$discount_brand['brand_image']}}">
								</div>
								<div class="product-text text-center">
									<span>點數2倍現折</span>
								</div>
							</a>
							<div class="product-tag">
								@foreach($discount_brand['ca_name'] as $discount_ca_key => $discount_categorie)
									@php
										$path = explode('-',$discount_categorie->url_path);
										$tag_class = 'label-primary';
										switch ($path['0']) {
											case '3000':
												$tag_class = 'label-primary';
											break;
											case '1000':
												$tag_class = 'label-danger';
											break;
											case '8000':
												$tag_class = 'label-warning';
											break;
											case '4000':
												$tag_class = 'label-warning';
											break;
											default:
												$tag_class = 'label-primary';
											break;
										}
									@endphp
									<span class="dotted-text tag label {{$tag_class}} {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$discount_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$discount_categorie->name}}</span>
								@endforeach
							</div>
							<div class="loop-photo hidden">
								<h2>{{$discount_brand['brand_name']}}</h2>
								<ul class="owl-carouse-product-loop">
									@foreach($discount_brand['product_image'] as $discount_image_key => $discount_product_image)
										<li class="text-center {{$discount_image_key == 0 ? ' ': 'hidden'}}">
											<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
										</li>
									@endforeach
								</ul>
							</div>
						</li>
					@endforeach --}}
					@for($i=1;$i<=5;$i++)
						<li class="product-list li-list" >
							<div>
								<a href="https://www.webike.tw/br/635" target="_blank">
									<div class="product-img">
										<img src="https://img.webike.net/sys_images/brand/brand_635.gif">
									</div>
								</a>
								<div class="product-text text-center">
									<span>最大88折優惠</span>
								</div>
								<div class="product-tag">
									<span class="dotted-text tag label label-primary">安全帽</span>
									<span class="dotted-text tag label label-primary">騎士服裝</span>
								</div>
							</div>
							<div class="loop-photo hidden">
								<h2>RS TAICHI</h2>
								<ul class="owl-carouse-product-loop">
									<li class="text-center ">
										<img src="https://img.webike.net/catalogue/10053/trv037_s.jpg">
									</li>
									<li class="text-center ">
										<img src="https://img.webike.net/catalogue/10053/trv037_s.jpg">
									</li>
									<li class="text-center ">
										<img src="https://img.webike.net/catalogue/10053/trv037_s.jpg">
									</li>	
								</ul>
							</div>
						</li>
					@endfor
				</ul>
			</div>
		</div>
	</div>