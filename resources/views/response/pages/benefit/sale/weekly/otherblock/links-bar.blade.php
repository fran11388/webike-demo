<style type="text/css">
		.promotion-container .links-bar{
		margin: 0 auto;
		position: sticky;
		top: 0;
		z-index: 10;
		max-width: 100%;
		min-width: 100%;
	}
	.promotion-container .links-bar ul li:last-child{
		border-right: none;
	}
	.promotion-container .links-bar ul li{
		width: 25%;
		background-color: #cfe2f3;/*錨點背景顏色*/
		border-right: 2px solid #05307d;/*錨點邊線顏色，同主題色*/
		box-sizing: border-box;
		text-align: center;
		float: left;
		font-size: 1.25rem;
		padding: 20px;
	}
	.promotion-container .links-bar ul li a{
		color: #000000;/*錨點文字顏色*/
	}
	.promotion-container .links-bar ul li:hover{
		background-color: #e4ecf3;/*錨點hover背景顏色*/
	    cursor: pointer;
	}
</style>
<div class="page-block">
	<div class="links-bar">
		<div>
			<ul class="clearfix">
				<li onclick="slipTo1('#week_sale')">
					<a href="javascript:void(0)">
						熱血風格品牌
					</a>
				</li>
				<li onclick="slipTo1('#double-point')">
					<a href="javascript:void(0)">
						當週加倍品牌
					</a>
				</li>
				<li onclick="slipTo1('#category')">
					<a href="javascript:void(0)">
						仿賽熱門分類
					</a>
				</li>
				<li onclick="slipTo1('#motor')">
					<a href="javascript:void(0)">
						當週加倍車型
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>