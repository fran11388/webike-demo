<div class="page-block" id="week_sale">
		<h2 class="page-block-title">熱血風格品牌</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix">

					{{-- @foreach($sale_brands as $sale_brand_url_rewrite => $sale_brand)
						<li class="product-list li-list" >
							<div>
								<a href="{{route('summary', 'br/'.$sale_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
									<div class="product-img">
										<img src="{{$sale_brand['brand_image']}}">
									</div>
								</a>
								<div class="product-text text-center">
									<span>最大88折優惠</span>
								</div>
								<div class="product-tag">
									@foreach($sale_brand['ca_name'] as $sale_ca_key => $sale_categorie)
										@php
											$path = explode('-',$sale_categorie->url_path);
											$tag_class = 'label-primary';
											switch ($path['0']) {
												case '3000':
													$tag_class = 'label-primary';
												break;
												case '1000':
													$tag_class = 'label-danger';
												break;
												case '8000':
													$tag_class = 'label-warning';
												break;
												case '4000':
													$tag_class = 'label-warning';
												break;
												default:
													$tag_class = 'label-primary';
												break;
											}
										@endphp
										<span class="dotted-text tag label {{$tag_class}} {{$sale_ca_key == 2 ? 'hidden': ' ' }}">{{$sale_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$sale_categorie->name}}</span>
									@endforeach
								</div>
							</div>
							<div class="loop-photo hidden">
								<h2>{{$sale_brand['brand_name']}}</h2>
								<ul class="owl-carouse-product-loop">
									@foreach($sale_brand['product_image'] as $sale_image_key => $sale_product_image)
										<li class="text-center {{$sale_image_key == 0 ? ' ': 'hidden'}}">
											<img src="{{ $sale_product_image ? $sale_product_image : NO_IMAGE}}">
										</li>
									@endforeach
								</ul>
							</div>
						</li>
					@endforeach --}}
					@for($i=1;$i<=5;$i++)
						<li class="product-list li-list">
							<div>
								<a href="https://www.webike.tw/br/635" target="_blank">
									<div class="product-img">
										<img src="https://img.webike.net/sys_images/brand/brand_635.gif">
									</div>
								</a>
								<div class="product-text text-center">
									<span>最大88折優惠</span>
								</div>
								<div class="product-tag">
									<span class="dotted-text tag label label-primary">皮革夾克・防摔衣</span>
									<span class="dotted-text tag label label-primary">賽車手套</span>
								</div>
							</div>
							<div class="loop-photo hidden">
								<h2>RS TAICHI</h2>
								<ul class="owl-carouse-product-loop">
									<li class="text-center ">
										<img src="https://img.webike.net/catalogue/10053/trv037_s.jpg">
									</li>
									<li class="text-center ">
										<img src="https://img.webike.net/catalogue/10053/trv037_s.jpg">
									</li>
									<li class="text-center ">
										<img src="https://img.webike.net/catalogue/10053/trv037_s.jpg">
									</li>	
								</ul>
							</div>
						</li>
					@endfor
				</ul>
			</div>
		</div>
	</div>