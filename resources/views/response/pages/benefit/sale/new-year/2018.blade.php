@extends('response.layouts.1column')
@section('style')
<style>
	.bannerbar{
		margin-top: 95px !important;
	}
	.top-shopping .container{
		max-width: none !important;
	}
	.contents-all-page{
		background: url({{assetRemote('image/benefit/sale/pc2018newyear/background.png')}});
	}
	ol.breadcrumb-product-detail{
		display: none;
	}
	.new-year .heade-banner img{
		width: 100%;
	}
	.new-year .heade-banner{
		margin-left: 5%;
		margin-right: 5%;
		padding-left: 10%;
		padding-right: 10%;
	}

	.new-year .game img{
		width: 100%;
	}
	.new-year .game{
		margin-top: 40px;
		margin-left: 9%;
		margin-right: 3%;
	}
	.new-year .game-photo{
		float: left;
		width: 30%;
	}
	.new-year .game_center{
		width: 40%;
		float: left;
		position: relative;
		margin-left: -3%;
		margin-right: -3%
	}
	.new-year .game-play{
		width:68%;
		position: absolute;
		left: 16%;
		top: 25.6%;
	}
	.new-year .game-play ul{
		list-style-type: none;
	    
	}
	.new-year .activity{
		float: left;
		width:25%;
		padding-left: 4px;
		padding-bottom: 4px;

	}
	.new-year .award-title img{
		width: 70%;
	}
	.new-year .award-title{
		width:33%;
		margin-left: auto;
		margin-right: auto;
		margin-top: 20px;
		margin-bottom: 40px;
	}
	.new-year .list{
		width: 70%;
		margin: 0 auto;
	}
	.new-year .pc-list{
		position: relative;
	}
	.new-year .day-list li{
		padding: 0px;
		list-style: none;
		width: 22% !important;
	}

	.new-year .day-list li:nth-child(1){
		margin-left: 0px;
	}
	.new-year .pc-list .pc-award .award-vertical{
		-webkit-writing-mode: vertical-rl;
		writing-mode: vertical-rl
	}
	.new-year .pc-award{
		position: absolute;
		left: 45%;
		top: 28%;
		color: #000;
		letter-spacing:2px;
	}
	.new-year .pc-award .award-num{
		margin-left: -10%;
	}
	.new-year .description-text{
        float: left;
	}
	.new-year .description-text p:nth-child(2){
		margin-top: 10px;
	}
	.new-year .despiction li:nth-child(1){
		margin-top: 0px;
	}
	.new-year .despiction li{
		margin-top: 10px;
		font-size: 1rem !important;
	}

	.new-year .despiction{
		padding: 20px;
		background-color: #FFF;
		margin-top: 40px;
		margin-left: 15%;
		margin-right: 15%;

	}
	.new-year .new-year-btn{
	    margin-top: 40px;
    	margin-bottom: 100px;
    	text-align: center;
	}
	.new-year .new-year-btn a{
    	min-width: 180px;
    	font-size: 1.2rem;
	}
	.phactive{
		/*border: 3px solid !important;*/
		outline: solid;
		outline-width: 3px; 
	}

	.new-year .not_login {
        width: 31%;
        margin-right: auto;
        margin-left: auto;
        padding: 20px;
        background-color: white;
        border: 3px solid #ccc;
        text-align: center;
    }
    .new-year .not_login .btn-warning{
    	margin-right: 20px; 
    }
	.new-year .pc-award-list{
		margin-right: -3%;
		margin-left: -3%;
	}
	.new-year .ph-remind p{
		color:#e61e25;
	}
	.new-year .ph-remind{
		margin-top: 20px
	}
	.new-year .ph-award {
		margin-top: 10px;
	}
	.new-year .day-list a{
		color:#000 !important;
	}
	.new-year .day-list a:hover{
		color:#fff !important;
	}
	@media(max-width:1680px){		
		.new-year .pc-award{
			left:44%;
		}
	}
	@media(max-width:1480px){
		.new-year .pc-award{
			left:43%;
		}
	}
	@media(max-width:1380px){
		.new-year .pc-award{
			left:43%;
		}
	}
	@media(max-width:1280px){
		.new-year .pc-award{
			left:41%;
		}
	}
    @media(max-width:990px){
		
		.new-year .heade-banner{
			margin:0px;
			padding: 0px;
		}
		.new-year .game_center{
			width:100%;
		}

		.ph-ok-btn{
			margin-top: 20px;
		}
		.new-year .new-year-btn{
		    margin-top: 20px;
		}
		.new-year .award-title{
			width: 100%;
		}
		.new-year .day-list li{
			width: 33% !important;
		}
		.ph-list{
			height:315px;
		}
		.new-year .list{
			width: 100%;
		}
		.ph-list img {
			margin-bottom: -15%;
		}
		.award-vertical{
			-webkit-writing-mode: vertical-rl;
			writing-mode: vertical-rl;
			margin: 0px auto;
			padding-right: 2%;
		}
		.new-year .pc-award{
			position: absolute;
			top: 54%;
		}
		.new-year .game{
			margin-top: 20px;
		}
		.new-year .award-title{
			margin-top: 20px;
			margin-bottom: 20px;
		}
		.new-year .despiction{
			margin-top: 20px;
		}
		.new-year .not_login{
			margin-top: 20px;
		}
		.new-year .award-title img{
		width: 100%;
		}
		.new-year .pc-award-list{
			margin: 0px;
		}
	}
	@media(max-width:800px){
		.ph-list{
			height: 350px;
		}
	}
	 @media(max-width:768px){
       .new-year .not_login{
            width:100% !important;
        }
        .bannerbar{
        	margin-top: 55px !important;
        }
      .new-year  .description-text p{
      	font-size: 0.85rem;
      }
    }
    @media(max-width:500px){
		.ph-list{
			height:350px;
		}
		.new-year .game{
			margin-top: 20px;
			margin-left: 0px;
			margin-right: 0px;
		}
		.new-year .game_center{
			margin-left: 0px;
			margin-right: 0px;
		}

    }
	@if(isset($customer_not_reward_data) and count($customer_not_reward_data))
		.game-play ul.hidden-xs li .active_hover{
			outline: solid;
			outline-width: 3px;
		}
	@endif
		
</style>
@stop
@section('middle')

<div class="new-year">
	<div class="heade-banner">
        <img  class="hidden-xs hidden-sm" src="{{assetRemote('image/benefit/sale/pc2018newyear/Headbanner.png')}}">
        <img  class="hidden-lg hidden-md" src="{{assetRemote('image/benefit/sale/ph2018newyear/phone_headbanner.png')}}" >
	</div>
	@if(isset($customer_not_reward_data) and count($customer_not_reward_data))
		<div class=" ph-remind hidden-md hidden-lg text-center">
			<p class="font-bold size-12rem">請點選以下任一紅包</p>
		</div>
	@endif
	<div class="game clearfix ">
		<div class="game-photo   hidden-xs hidden-sm">
	        <img src="{{assetRemote('image/benefit/sale/pc2018newyear/game_left.png')}}">
		</div>
		<div class="game_center" id="game_center">
	        <img src="{{assetRemote('image/benefit/sale/pc2018newyear/game_center.png')}}">
			<div class="game-play clearfix ">
				<ul class="clearfix hidden-xs hidden-sm">
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,1)" onmouseleave="mouseleave(this,1)" onclick="pcstartgame(this,1)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_1.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,2)" onmouseleave="mouseleave(this,2)" onclick="pcstartgame(this,2)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_2.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,3)" onmouseleave="mouseleave(this,3)" onclick="pcstartgame(this,3)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_3.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,4)" onmouseleave="mouseleave(this,4)" onclick="pcstartgame(this,4)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_4.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,5)" onmouseleave="mouseleave(this,5)" onclick="pcstartgame(this,5)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_5.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,6)" onmouseleave="mouseleave(this,6)" onclick="pcstartgame(this,6)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_6.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,7)" onmouseleave="mouseleave(this,7)" onclick="pcstartgame(this,7)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_7.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onmouseenter="mouseenter(this,8)" onmouseleave="mouseleave(this,8)" onclick="pcstartgame(this,8)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_8.jpg')}}">
						</div>
					</li>
				</ul>
				<ul class="clearfix hidden-lg hidden-md">
					<li class="activity">
						<div>
							<img  onclick="phstartgame(this,1)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_1.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img  onclick="phstartgame(this,2)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_2.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img  onclick="phstartgame(this,3)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_3.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img  onclick="phstartgame(this,4)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_4.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img  onclick="phstartgame(this,5)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_5.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onclick="phstartgame(this,6)"  src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_6.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onclick="phstartgame(this,7)"  src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_7.jpg')}}">
						</div>
					</li>
					<li class="activity">
						<div>
							<img onclick="phstartgame(this,8)" src="{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_8.jpg')}}">
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="game-photo  hidden-xs hidden-sm">
	        <img src="{{assetRemote('image/benefit/sale/pc2018newyear/game_right.png')}}">
		</div>
	</div>
	@if($customer and $activity_open)
		<div class="hidden-lg hidden-md text-center ph-ok-btn">
	        <button type="submit" onclick="phgame()" class="btn bg-color-red style-button-strong border-radius-2" value="" >
	            確認
	        </button>
		</div>
	@elseif(!$activity_open)
	@else
        <div class="not_login">
            <a class="btn btn-warning" href="{{ \URL::route('login') }}">會員登入</a>
            <a class="btn btn-danger" href="{{ \URL::route('customer-account-create') }}">加入會員</a>
        </div>
    @endif
	<div class="award-title text-center">
            <img class="hidden-xs hidden-sm" src="{{assetRemote('image/benefit/sale/pc2018newyear/awardtitle.png')}}">
            <img class="hidden-lg hidden-md" src="{{assetRemote('image/benefit/sale/ph2018newyear/awardtitle.png')}}">
	</div>
	<div class="list" id="result-container">
		<ul class="clearfix day-list">
		@if($days)
			@foreach($days as $key => $day)
						@php
							$customer_reward_flg = false;
							$pc_image = '/N_';
						@endphp
				@if($customer_gifts_data)
					@if(array_key_exists($day, $customer_gifts_data))
						@php
							$gifts_link = strpos($customer_gifts_data[$day],'折價卷')? "cart":"customer-history-gifts";
							$customer_reward_flg = true;
							$pc_image = '/Y_';
						@endphp
					@endif
				@endif
				 <li class="pc-award-list  col-xs-4 col-sm-4 col-md-2 {{$day}}">
					<div class="pc-list hidden-xs hidden-sm">
						<span class="hidden no-reward">{{ assetRemote('image/benefit/sale/pc2018newyear/Y_').$key .'.png'}}</span>
			            <img  src="{{assetRemote('image/benefit/sale/pc2018newyear').$pc_image. $key .'.png'}}">
			            @if($customer_reward_flg)
							<div class="pc-award {{isset($customer_gifts_data[$day])? '' : 'hidden'}}">
				            	@php
				            		$gifts_strs = explode("-",$customer_gifts_data[$day]);
				            	@endphp
			            		<a href="{{ \URL::route("$gifts_link") }}" target="_blank" target="_blank">
					            	@foreach($gifts_strs as $gifts_strs)
					            		@if(is_numeric($gifts_strs))
											<p class="size-12rem font-bold award-num">{{$gifts_strs}}</p>
										@else
											<p class="size-12rem font-bold award-vertical reawrd-text ">{{$gifts_strs}}</p>
										@endif
									@endforeach
								</a>
							</div>
						@endif
						<div class="pc-award  hidden">
							@if(isset($reward))
				            	@php
				            	$gifts_link = strpos($reward->name,'折價卷')? "cart":"customer-history-gifts";
				            		$gifts_names = explode("-",$reward->name);
				            	@endphp
				            	<a href="{{ \URL::route("$gifts_link") }}" target="_blank">
					            	@foreach($gifts_names as $gifts_name)
						            		@if(is_numeric($gifts_name))
												<p class="size-12rem font-bold award-num">{{$gifts_name}}</p>
											@else
												<p class="size-12rem font-bold award-vertical reawrd-text ">{{$gifts_name}}</p>
											@endif
									@endforeach
								</a>
							@endif
						</div>
					</div>
					<div class="ph-list hidden-lg hidden-md text-center ">
						<img  src="{{assetRemote('image/benefit/sale/ph2018newyear/phone_icon_'. $key .'.png')}}">
			            @if($customer_reward_flg)
							<div class="ph-award {{isset($customer_gifts_data[$day])? '' : 'hidden'}}">
				            	@php
				            		$gifts_strs = explode("-",$customer_gifts_data[$day]);
				            	@endphp
				            	<a href="{{ \URL::route("$gifts_link") }}" target="_blank">
					            	@foreach($gifts_strs as $gifts_strs)
						            		@if(is_numeric($gifts_strs))
												<p class="size-12rem font-bold award-num">{{$gifts_strs}}</p>
											@else
												<p class="size-12rem font-bold award-vertical reawrd-text ">{{$gifts_strs}}</p>
											@endif
									@endforeach
								</a>
							</div>
						@endif
						<div class="ph-award  hidden">
							@if(isset($reward))
				            	@php
				            		$gifts_names = explode("-",$reward->name);
				            	@endphp
				            	<a href="{{ \URL::route("$gifts_link") }}" target="_blank">
					            	@foreach($gifts_names as $gifts_name)
						            		@if(is_numeric($gifts_name))
												<p class="size-12rem font-bold award-num">{{$gifts_name}}</p>
											@else
												<p class="size-12rem font-bold award-vertical reawrd-text ">{{$gifts_name}}</p>
											@endif
									@endforeach
								</a>
							@endif
						</div>
					</div>
				</li>
			@endforeach
			@endif
		</ul>
	</div>
	<div class="despiction">
            <ul class="ul-ct-brand-selection ">
                <li class="clearfix">
                    <div class="size-10rem description-text text-center">【活動內容】</div>
                    <div class="size-10rem description-text"><span>咚咚隆咚鏘~Webike祝各位會員新年快樂！從除夕到初五天天抽紅包，通通有獎！最大獎發發發紅包送給你！</span></div>
                </li>
                <li class="clearfix">
                    <div class="size-10rem description-text text-center">【活動辦法】</div>
                    <div class="size-10rem description-text"><span>活動期間 2/15(除夕) ~ 2/20(初五) 凡註冊的會員每日都可以抽獎一次，共有6次機會。</span></div>
                </li>
                <li class="clearfix">
                    <div class="size-10rem description-text text-center">【獎項說明】</div>
                    <div class="size-10rem description-text">
                        <p class="size-10rem">「現金折價卷」您可以至 <a href = "{{ route('cart') }}" target="_blank">購物車</a> 折價卷欄位確認您獲得的折價卷，此折價卷適用全館所有商品，但僅限一張訂單使用。</p>
                        <p class="size-10rem"> 「贈品獎」您可以至 <a href ="{{ route('customer-history-gifts') }}" target="_blank"> 贈品兌換 </a>確認您獲得的獎品，兌換時只要將獎品按下加入購物車的按鈕，即可於下次購物時隨貨附贈。</p>
                    </div>
                </li>
                <li class="clearfix">
                	<div class="size-10rem description-text text-center">【活動期限】</div>
                	<div class="size-10rem description-text"><span>本次活動的現金折價卷與贈品兌換期限至2018年03月31日止。</span></div>
                </li>
            </ul>
        </div>

	<div class="text-center new-year-btn">
        <a class="btn btn-warning  border-radius-2" href="{{ \URL::route('benefit') }}" target="_blank">
            查看更多優惠
        </a>
	</div>
</div>
@stop
@section('script')
<script type="text/javascript">
	// $('.bannerbar').css("margin-top","145px");
	// alert(123);

	@if(!$customer)
		swal(
			'登入即可參與抽獎活動',
			'',
			'info'
		);
	@endif

	@if(isset($reward))
		reward = "{{$reward->id}}";
	@else
		reward = 0;
	@endif
	var locate = null;
	@if($location)
		locate = "{{ $location - 1 }}";
		gamesrc = "{{assetRemote('image/benefit/sale/pc2018newyear/visited/award_')}}";
	  	imagesrc = gamesrc+reward+".jpg";
		$(document).ready(function(){
			$('.game-play ul.hidden-xs li').each(function(key,val){
				if(key == locate){
					$(this).find('img').attr('src',imagesrc);
				}
			});
			$('.game-play ul.hidden-md li').each(function(key,val){
				if(key == locate){
					$(this).find('img').attr('src',imagesrc);
				}
			});

			$('.game-play ul.hidden-xs li img').addClass("active");
			$('.game-play ul.hidden-md li img').addClass("finish");
			$('div.hidden-md button').addClass('hidden');
			$('div.hidden-md button').prop('disabled', true);
		});
	@endif
  function mouseenter(image,num) {
  	@if($customer and $activity_open)
	  	var active = true;
	  	if($('.game-play ul.hidden-xs li img').hasClass('active')){
	  		active = false;
	  	}
	  	$(image).addClass('active_hover');
	  	 img = "{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_')}}";
	  	if(image.src === img+num+".jpg" && active)
	  	{
		  	src = "{{assetRemote('image/benefit/sale/pc2018newyear/hover/re_hover_')}}";
		    image.src = src+num+".gif";
		}
	@endif
  }

  function mouseleave(image,num) {
  	@if($customer)
		var active = true;
		$('.game-play ul.hidden-xs li img').removeClass('active_hover');
	  	if($('.game-play ul.hidden-xs li img').hasClass('active')){
	  		active = false;
	  	}
	  	img = "{{assetRemote('image/benefit/sale/pc2018newyear/hover/re_hover_')}}";
	  	if(image.src == img+num+".gif" && active)
	  	{
	  		src = "{{assetRemote('image/benefit/sale/pc2018newyear/link/re_link_')}}";
	        image.src = src+num+".jpg";
	    }
    @endif
  }

  function pcstartgame(image,num){
  	@if($customer and $activity_open)
	  	var active = true;
	  	if($('.game-play ul.hidden-xs li img').hasClass('active')){
	  		active = false;
	  	}
		photoSrc = "{{assetRemote('image/benefit/sale/pc2018newyear/visited/award_')}}";
	  	 gamePhoto = photoSrc+reward+".jpg";


	  	if(image.src != gamePhoto && active)
  	  	{
        	 $(image).addClass("active");
	  	   	src = "{{assetRemote('image/benefit/sale/pc2018newyear/active/award_')}}";
	        image.src = src+reward+"/re_active_"+num+".gif";

		  	 $.ajax({
		        url: "{!! route('benefit-sale-double11-createGift') !!}",
		        data: {_token: $('meta[name=csrf-token]').prop('content'),_num:num},
		        type: 'POST',
		        dataType: 'json',
		        success: function(result){
		        },
		        error: function(xhr, ajaxOption, thrownError){

		        }
		    });
	        setTimeout(function changePhoto(){
	        	image.src = gamePhoto;
	        	 $(image).addClass("active");
                var y = $('#result-container').offset().top - 180;
                $('html,body').animate({scrollTop: y},800);
	        },4000);
	        locate = num -1 ;
			$('.game-play ul.hidden-md li').each(function(key,val){
				if(key == locate){
					$(this).find('img').attr('src',gamePhoto);
					$(this).find('img').addClass('finish');
					$('div.hidden-md button').prop('disabled', true);
				}
			});
    	}



    	var Today = "{{date('Y-m-d')}}";
    	var reward_photo = $(".list ul.day-list li." +Today+ " span").text();
    	$(".list ul.day-list li." +Today+ " img").attr('src', reward_photo);
    	$(".list ul.day-list li." +Today+ " .pc-list div.pc-award").removeClass("hidden");
	@endif

  }

  function phstartgame(image,num){
  	@if($customer and $activity_open)
		var finish = true;
		if($('.game-play ul.hidden-md li img').hasClass('finish')){
			finish = false;
		}
		if(finish){
			if($('.game-play ul.hidden-md li img').hasClass('phactive')){
				$('.game-play ul.hidden-md li img').removeClass("phactive");
			}
			$(image).addClass("phactive");
			$('div.hidden-md button').attr('value',num);
		}
	@endif
  }
	@if(isset($customer_not_reward_data) and count($customer_not_reward_data) and count($customer_not_reward_data->first()) and $customer_not_reward_data->first()->status == 0)
		$('#game_center .game-play li.activity').click(function(){
			var y = $('#game_center').offset().top - 100;
			$('html,body').animate({scrollTop: y},800);
		});
	@endif

  function phgame(){
	@if($customer and $activity_open)
		if($('.game-play ul.hidden-md li img').hasClass('phactive')){
		$('.game-play ul.hidden-md li img.phactive').addClass("finish");
		var num = $('div.hidden-md button').attr('value');
		src = "{{assetRemote('image/benefit/sale/pc2018newyear/active/award_')}}";
		image = src+reward+"/re_active_"+num+".gif";
		$('.game-play ul.hidden-md li img.phactive').attr('src',image);

		 $.ajax({
			url: "{!! route('benefit-sale-double11-createGift') !!}",
			data: {_token: $('meta[name=csrf-token]').prop('content'),_num:num},
			type: 'POST',
			dataType: 'json',
			success: function(result){
			},
			error: function(xhr, ajaxOption, thrownError){
			}
		});
		 photoSrc = "{{assetRemote('image/benefit/sale/pc2018newyear/visited/award_')}}";
		 gamePhoto = photoSrc+reward+".jpg";
		setTimeout(function changePhoto(){
			$('.game-play ul.hidden-md li img.phactive').attr('src',gamePhoto);
			var y = $('#result-container').offset().top - 180;
			$('html,body').animate({scrollTop: y},800);
		},4200);


		$('div.hidden-md button').prop('disabled', true);
		$('div.hidden-md button').addClass('hidden');
		locate = num-1 ;
		gamesrc = "{{assetRemote('image/benefit/sale/pc2018newyear/visited/award_')}}";
		imagesrc = gamesrc+reward+".jpg";
		$('.game-play ul.hidden-xs li').each(function(key,val){
			if(key == locate){
				$(this).find('img').attr('src',imagesrc);
				$(this).find('img').addClass('active');
			}
		});
		var Today = "{{date('Y-m-d')}}";
		$(".list ul.day-list li." +Today+ " .ph-list .ph-award").removeClass("hidden");

		}
	@endif
  }

</script>
@stop