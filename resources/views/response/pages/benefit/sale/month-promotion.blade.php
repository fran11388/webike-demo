@extends('response.layouts.1column')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{assetRemote('css/pages/webikewanted.css')}}">
	<style>
	#myNavbar li a {
		background-color: {{$promotion->category_button_backgroundcolor}};
	}
	#myNavbar li a:hover{
		background-color:{{$promotion->category_button_hover_color}} !important;
	}
	#myNavbar li a{
		border: 1px solid {{$promotion->category_button_border_color}}
	}
	#myNavbar li {
		padding: 5px 7px;
	}
	.ct-sale-page .title {
		background-color: {{$promotion->main_color}}
	}
	.ct-sale-page .title h2{
		color: {{$promotion->font_color}}
	}
	.content-box-sale .product-sale .li-product-sale:hover{
		border:1px solid {{$promotion->brand_border_color}};
	}
	.content-box-sale .product-sale .sale{
		height:256.54px;
	}
	.content-box-sale .product-sale .group{
		height:204.54px
	}
	#myNavbar {
		width:100%
	}
	.content-box-sale .product-sale {
		margin:10px 0;
	}
	.product-sale li {
		list-style-type:none;
	}
	.month-category{
		margin-right:auto;
		margin-left:auto;
	}
	.sale-page .form-group {
		margin-bottom:20px;
	}
	.category_link {
		color: {{$promotion->category_button_text_color}} !important; 
	}
	.category_link:hover {
		color: {{$promotion->link_hover_font_color}} !important; 
	}
	.photo-box li img {
		max-height:94px;
	}
	.fatherday {
		margin-top:20px;
		margin-bottom:80px;
	}
	@if($promotion->brand_title_color)
		.brand-title {
			background-color: {{ $promotion->brand_title_color }} !important;
		}
	@endif

	@if($promotion->brand_font_color)
		.brand-title {
			color: {{ $promotion->brand_font_color }} !important;
		}
	@endif
	/*.content-box-sale .product-sale #group:nth-child(3n){
		margin-left:-1px;
	}
	.content-box-sale .product-sale #group:nth-child(n){
		margin-bottom:-1px;
	}
	.content-box-sale .product-sale #group:nth-child(3n-1){
		margin-left:-1px;
	}
	.content-box-sale .product-sale .sale{
		border-right:0px;
		margin-bottom:-1px;
	}
	.content-box-sale .product-sale .sale:nth-child(4n){
		border-right:1px solid #b7b7b7;
	}*/
	/*.content-box-sale .product-sale .li-product-sale:nth-child(n){
		border-left:0px;
	}*/
	</style>
@stop
@section('middle')
	<div class="sale-page block">
        <div class="form-group text-center">
            <img class=" form-group" src="{{$promotion->banner}}" alt="{{$promotion->name.$tail}}">
        </div>
        <div id="myNavbar" class="row ct-menu-sale form-group month-category">
            <ul class="text-center">
            	@foreach($types as $info)
            		<li class="col-xs-6 col-sm-4 col-md-2-4 col-lg-2-4">
            			<a onclick="slipTo1('#item{{$info->id}}')" class="category_link"  href="javascript:void(0)" title="{{$info->name.$tail}}">{{$info->name}}</a>
            		</li>
            	@endforeach
            </ul>
        </div>
    	@php
    		$num = 0;
    	@endphp
    	@foreach($promotion->items->groupBy('type_id') as $promo_data)
    		@foreach($promo_data as $info)
				@if($num != $info->type->id)
					@if($num != 0)
							</ul>
							</div>
						</div>
					@endif
					<div id="item{{$num+1}}" class="ct-sale-page form-group">
			            <div class="title col-xs-12 col-sm-12 col-md-12">
			                <h2>{{$info->type->name}}</h2>
			            </div>
			            <div class="content-box-sale">
			            	<ul class="product-sale">
		        @endif
		        @if($info->type->template == 'single')
	                <li class="sale li-product-sale col-xs-12 col-sm-6 col-md-3">
	                    <ul class="box-fix-column content text-center">
	                        <li>
	                            <a href="{{URL::route('parts', ['section' => 'ca/' . $info->url_rewrite])}}?rel={!! $promotion->url_rewrite . '-monthly-promotion' !!}" title="{{$info->name.$tail}}" target="_blank">
	                                <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
	                                	@php
	                                		$url_rewrite = explode('-', $info->url_rewrite);
											$thisImg = 'https://img.webike.net/sys_images/category/bm_'.end($url_rewrite).'.gif';
											if($info->image){
												$thisImg = $info->image;
											}
										@endphp
	                                    <img src="{{$thisImg}}" alt="{{$info->name.$tail}}">
	                                </figure>
	                                <label class="dotted-text1 brand-title">{{$info->name}}</label>
	                       			<span class=" dotted-text1 font-color-red text-center">{{$info->note}}</span>
	                            </a>
	                        </li>
	                    </ul>
	                </li>
	            @else
	            	<li id="" class="group li-product-sale col-xs-12 col-sm-6 col-md-4">
	            		@if($info->type_id == 5)
	            			<a href="{{URL::route('summary', '/mt/' . $info->url_rewrite)}}?rel={!! $promotion->url_rewrite . '-monthly-promotion' !!}" title="{{$info->name.$tail}}" target="_blank">
						@elseif($info->type_id == 1)
							<a href="{{URL::route('summary', '/br/' . $info->url_rewrite)}}?rel={!! $promotion->url_rewrite . '-monthly-promotion' !!}" title="{{$info->name.$tail}}" target="_blank">
	            		@else
							<a href="{{URL::route('parts', ['section' => 'br/'. $info->url_rewrite])}}?rel={!! $promotion->url_rewrite . '-monthly-promotion' !!}" title="{{$info->name.$tail}}" target="_blank">
	                   	@endif
	                        <div class="photo-box">
	                            <ul>
	                                <li class="col-xs-7 col-sm-7 col-md-7"><figure class="zoom-image"><img src="{{$info->image}}" alt="{{$info->name.$tail}}"></figure></li>
	                                <li class="col-xs-5 col-sm-5 col-md-5"><figure class="zoom-image thumb-box-border"><img src="{{$info->sub_image}}" alt=""></figure></li>
	                            </ul>
	                        </div>
	                        <label class="dotted-text1 brand-title">{{$info->name}}</label>
	                        <span class=" dotted-text1 font-color-red text-center">{{$info->note}}</span>
	                    </a>
	                </li>

	            @endif
	            @php
	        		$num = $info->type_id;
	        	@endphp
        	@endforeach
        @endforeach
						</ul>
					</div>
				</div>
		{{--<div class="width-full text-center fatherday">--}}
			{{--<a href="{{ \URL::route('benefit-event-fatherday',['year' => 2017]) }}" target="_blank" title="2017 88節免運費活動 - 「Webike-摩托百貨」">--}}
				{{--<img src="{{ assetRemote('image/benefit/sale/fatherday/banner.png')}}" alt="88節免運費活動{{$tail}}">--}}
			{{--</a>--}}
		{{--</div>--}}
@stop
@section('script')
	<script>
        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 70;
            $('html,body').animate({scrollTop: y}, 400);
        }
	</script>
@stop