@extends('response.layouts.1column')
@section('style')
<style>
	body{ 
		position: initial !important; 
	}
	.owl-prev{
		color: #ff7a0b;
		top: 40%;
		left: 6%;
	}
	.owl-prev:hover{
		background-color: #ff7a0b;
	}
	.owl-next{
		color: #ff7a0b;
		top: 40%;
		right: 6%;
	}
	.owl-next:hover{
		background-color: #ff7a0b;
	}
	.breadcrumb-mobile .breadcrumb-product-detail{
		padding: 0px;
	}
	section .container{
		padding: 0px;
		max-width: 100% !important;
	}
	.anniversary-5th .background-orange{
	    background-color: #ff7a0b;
	}
	.anniversary-5th .anniversary-5th-banner{
		position: relative;
	}
	.anniversary-5th .anniversary-5th-banner .down{
	    position: absolute;
		top: 81%;
    	left: 49%;
	}
	.anniversary-5th .anniversary-5th-share ul{
		margin-top: 20px;
	}
	.anniversary-5th .anniversary-5th-share .down{
		margin-top: 20px;
	}
	.anniversary-5th .anniversary-5th-share span{
		color: #fff;
	}
	.anniversary-5th .anniversary-5th-share .phone-title{
		color: #fff;
		display: block;
		
	}
	.anniversary-5th .anniversary-5th-share .phone-title .phone-title-text{
		text-align: center;
		font-size: 1.75rem;
		font-weight: bold;
	}
	.anniversary-5th .phone-title .title-text{
		text-align: center;
		display: block;
		font-size: 1.75rem;
		font-weight: bold;
	}
	.anniversary-5th .phone-title .text{
		display: block;
		font-size: 1rem;
		padding: 5px 8%;
	}
	.anniversary-5th .anniversary-5th-share .year{
		margin-top: 10px;
	}
	.anniversary-5th .title{
		padding: 30px 0px;
	}
	.anniversary-5th-award ul.pc li{
		list-style: none;
		float: left;
		width: 20%;
	}
	.anniversary-5th .anniversary-5th-award ul li div:nth-child(2){
		margin: 15px 0px;
	}
	.anniversary-5th ul.pc{
		max-width: 1230px;
	    padding-right: 100px;
	    padding-left: 100px;
	    margin-right: auto;
	    margin-left: auto;
	}

	.anniversary-5th .anniversary-5th-award .phone-title span{
		color: #ff7a0b;
		display: block;

	}
	.anniversary-5th .anniversary-5th-other ul {
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.anniversary-5th .anniversary-5th-other ul li div:nth-child(2){
		margin: 10px 0px;
	}
	.anniversary-5th ul li{
		list-style: none;
	}
	.anniversary-5th-award .add-car .limit-pc{
		float: left;
		height: 38px;
		margin-left: 5px;
	}
	.anniversary-5th-award .add-car .limit-pc span{
		line-height: 38px;
	}
	.anniversary-5th-award .add-car .add-car-btn-pc{
		text-align: right;
		float: left;
		width: 53%;
	}

	
	.anniversary-5th .anniversary-5th-award .btn-disable{
		background: #ccc;
		border-color: #ccc;
		color: #666;
	}
	.font-color-blue{
		color: #005fb3;
	}
	li figure.zoom-image img:hover{
       	-webkit-transform: scale(1.2);
	    -moz-transform: scale(1.2);
	    -ms-transform: scale(1.2);
	    -o-transform: scale(1.2);
	    transform: scale(1.2);
	}
	.gift .choose .display-block{
		display: block!important;
	}
	.gift .choose .display-none{
		display: none!important;
	}
	.anniversary-5th-award .not_login .btn-warning{
		margin-right: 15px;
	}
	button[disabled]{
		opacity:1!important;
	}
	.anniversary-5th .anniversary-5th-award .add-car button{
			width: 150px;
	}
	@media(max-width:500px){
		.anniversary-5th-award .choose{
			padding: 5%;
		}
		.anniversary-5th-award .add-car .add-car-btn{
			position: relative;
		}
		.anniversary-5th-award .add-car .add-car-btn .loading{
			position: absolute;
		    top: 6%;
		    left: 66%;
		}

		.anniversary-5th .anniversary-5th-other img{
			width: 100%;
		}
		.anniversary-5th-other .title span{
			color: #fff;
			font-size: 1.75rem;
		}
		.anniversary-5th .anniversary-5th-other ul{
			padding-left: 10%;
			padding-right: 10%;
		}
		.anniversary-5th .anniversary-5th-other .title{
			padding: 10px 0px;
		}
		.anniversary-5th .anniversary-5th-other ul li:nth-child(2){
			margin:  10px 0px;
		}
		.anniversary-5th .anniversary-5th-award .add-car{
			padding: 15px 0px 30px 0px;
		}
		.anniversary-5th .anniversary-5th-award ul li,.anniversary-5th .anniversary-5th-share ul li{
			padding: 0 15%;
		}
		.anniversary-5th-award .add-car .limit{
			margin-top: 10px;
		}
		.anniversary-5th .anniversary-5th-banner .down{
			left: 46%;
		}
		.anniversary-5th .anniversary-5th-award .owl-carousel .owl-stage{
			padding-left:140px!important;
			padding-right: 140px!important;
		}
		.anniversary-5th .anniversary-5th-award .owl-carousel .owl-stage .active.owl-item{
			margin-left:-140px!important;
			margin-right: -140px!important;
		}
	}
	@media(max-width:400px){
		.owl-prev{
			top: 30%;
		}
		.owl-next{
			top: 30%;
		}
		.anniversary-5th .anniversary-5th-award .owl-carousel .owl-stage{
			padding-left:130px!important;
			padding-right: 130px!important;
		}
		.anniversary-5th .anniversary-5th-award .owl-carousel .owl-stage .active.owl-item{
			margin-left:-130px!important;
			margin-right: -130px!important;
		}
	}
	@media(max-width:350px){
		.anniversary-5th .anniversary-5th-share .phone-title .phone-title-text{
			display: block;
		}
		.anniversary-5th .anniversary-5th-award .owl-carousel .owl-stage{
			padding-left:120px!important;
			padding-right: 120px!important;
		}
		.anniversary-5th .anniversary-5th-award .owl-carousel .owl-stage .active.owl-item{
			margin-left:-120px!important;
			margin-right: -120px!important;
		}
	}
</style>
@stop
@section('middle')
<div class="anniversary-5th">
	<div class="anniversary-5th-banner">
		@if($is_phone)
			<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/ph/banner.jpg')}}">
		@else
			<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/banner.jpg')}}">
		@endif
		<div class="down {{$is_phone? 'ph': 'pc'}}">
			<a href="javascript:void(0)">
				<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/down.png')}}">
			</a>
		</div>
	</div>
	<div class="anniversary-5th-share background-orange title ">
		@if($is_phone)
			<div class="phone-title">
				<div class="text-center">
					<span class="phone-title-text">與您分享</span>
					<span class="phone-title-text">我們值得紀念的時刻</span>
				</div>
				<span class="text text-center">從2013年開站以來，始終秉持著「愛車、愛人及這片土地」的精神，往後我們將繼續提供更好的服務與功能給大家！</span>
			</div>
			<?php
				$share_years = [ '2013', '2014', '2015', '2016', '2017', '2018']
			 ?>
			<ul class="owl-carousel-4">
				@foreach($share_years as $share_year)
					<li>
						<img class="banner" src="{{assetRemote('image/benefit/sale/anniversary/5-th/ph/img'.$share_year.'.png')}}">
						<div class="text-center font-bold year">
							<span class="size-10rem">{{$share_year}}</span>
						</div>
					</li>
				@endforeach
			</ul>
		@else
			<img class="banner" src="{{assetRemote('image/benefit/sale/anniversary/5-th/2_c.png')}}">
		@endif
		<div class="down text-center {{$is_phone? 'ph': 'pc'}}">
			<a href="javascript:void(0)">
				<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/down.png')}}">
			</a>
		</div>
	</div>
	<div class="anniversary-5th-award">
		@if($is_phone)
			<div class="phone-title title">
				<span class="title-text">感謝您多年來的支持</span>
				<span class="title-text">我們有好禮送給您！</span>
				<span class="text text-center">凡會員於6/1~6/8期間進入本站，皆可參加好禮五選一活動，每位會員期間內只可領取且兌換一次，兌換期限至6/30止！</span>
			</div> 
			@php
				$fold = 'ph';
				$gifts = $gifts->reverse();
				$gifts->all();
			@endphp
		@else
			<div class="text-center title">
				<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/3_txt.png')}}">
			</div>
			@php
				$fold = 'pc';
			@endphp
		@endif
		@php
			$count = array( '250','100','150','200');
		@endphp
		<ul class="{{$is_phone? 'owl-carousel-4 ph' : 'clearfix pc'}}">
			@foreach($gifts as $key => $gift)
					@if( $gift->type != 2)
						<li>
							<div class="text-center choose">
								<img  class="img-photo"  src="{{assetRemote('image/benefit/sale/anniversary/5-th/'.$fold.'/award/award'.$gift->id.'.png')}}">
								<img  class=" hidden img-activity" src="{{assetRemote('image/benefit/sale/anniversary/5-th/'.$fold.'/award/award'.$gift->id.'_1.png')}}">
								<span class="hidden">{{$gift->id}}</span>
							</div>
							<div class="text-center">
								<a class="product_name" target="_blank" href="{{route('product-detail',$gift->productUrl)}}" >{{$gift->name}}</a>
							</div>
							<div class="text-center gift_remain">
									<span class="font-color-red font-bold size-10rem remain">{{$count[$key]}}份 已送完</span>
							</div>
						</li>
					@else
						<li class="gift">
							<div class="text-center choose">
								<img  class="img-activity" src="{{assetRemote('image/benefit/sale/anniversary/5-th/'.$fold.'/award/award'.$gift->id.'_1.png')}}">
								<span class="hidden">{{$gift->id}}</span>
							</div>							
							<div class="text-center">
								<span class="font-color-blue product_name">{{$gift->name}}</span>
							</div>
							@php
								$remain = str_replace("-","",$gift->remain)+1000;
							@endphp
							<div class="text-center gift_remain remain-choose">
									<span class="font-color-red font-bold size-10rem active-ct-tap-country">已送出{{$remain}}份</span>
									<span class="font-color-red font-bold size-10rem remain {{$customer_choose? 'hidden': ''}}">請點選「立即領取」</span>
							</div>
						</li>
					@endif
			@endforeach
			@if(!$is_phone)
				<div class="loading hidden">
					@include('response.common.loading.md')
				</div>
			@endif
		</ul>
		<div class="text-center add-car title ">
			<div class="clearfix">
				@if($customer_no_login)
			        <div class="not_login text-center">
			            <a class="btn btn-warning" href="{{ \URL::route('login') }}">會員登入</a>
			            <a class="btn btn-danger" href="{{ \URL::route('customer-account-create') }}">加入會員</a>
			        </div>
				@elseif($customer_choose)
					<div class=" {{$is_phone ? 'add-car-btn' : 'add-car-btn-pc'}}">
						<button class="btn btn-disable finish " href="javascript:void(0)" disabled="disabled" >好禮已領取</button>
					</div>
					<div class="{{$is_phone ? 'limit' : 'limit-pc'}}">
						<span>(兌換請至<a href="{{route('cart')}}" target="_blank">購物車</a>)</span>
					</div>
				@else
					<div class=" {{$is_phone ? 'add-car-btn' : 'add-car-btn-pc'}}">
						<button class="btn btn-danger" href="javascript:void(0)">立即領取</button>
						@if($is_phone)
							<div class="loading hidden">
								@include('response.common.loading.xs')
							</div>
						@endif
					</div>
					<div class="{{$is_phone ? 'limit' : 'limit-pc'}}">
						<span>(數量有限送完為止)</span>
					</div>
				@endif
			</div>
		</div>
	</div>
	<div class="anniversary-5th-other">
		<div class="background-orange text-center title">
			@if($is_phone)
				<span>還有其他好康推薦給您</span>
			@else
				<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/4_title.png')}}">
			@endif
		</div>
		<div>
			<ul class="clearfix {{$is_phone ? '' :'pc'}}">
				<li class="col-xs-12 col-sm-4 col-md-4">
					<div class="text-center">
						<h2> 6月精選 SALE</h2>
					</div>
					<div class="text-center">
						<a href="{{route('benefit-sale-month-promotion','2018-06')}}" target="_blank" >
							<figure class="zoom-image">
								<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/june345.jpg')}}">
							</figure>
						</a>
					</div>
					<div>
						<span>
							精選商品大彙整、熱門車型改裝品點數5倍回饋，這個月份給您最Hot的選擇！                                               
						</span>
					</div>
				</li>
				<li class="col-xs-12 col-sm-4 col-md-4">
					<div class="text-center"> 
						<h2> OUTLET</h2> 
					</div>
					<div class="text-center">
						<a href="{{route('outlet')}}" target="_blank" >
							<figure class="zoom-image">
								<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/OUTLET345.jpg')}}">
							</figure>
						</a>
					</div>
					<div>
						<span>
							定期上架各大品牌出清商品，賣不掉就再降，快來尋找專屬於你的寶物。
						</span>
					</div>
				</li>
				<li class="col-xs-12 col-sm-4 col-md-4">
					<div class="text-center">
						<h2>加價購</h2>
					</div>
					<div class="text-center">
						<a href="{{route('cart')}}" target="_blank" >
							<figure class="zoom-image">
								<img src="{{assetRemote('image/benefit/sale/anniversary/5-th/add345.jpg')}}">
							</figure>
						</a>
					</div>
					<div>
						<span>
							本月專屬，不限品項訂單滿3千元，YAMALUBE 齒輪油 GII-130 加購價只要1元！
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>
	 <div class="text-center title">
          <a class="btn btn-info" href="{{ \URL::route('benefit') }}" target="_blank">更多會員好康</a>
     </div>
</div>
@stop
@section('script')
<script type="text/javascript">
	/*$('.anniversary-5th-award ul.pc li .choose').hover(function(){
		if(!$('.anniversary-5th-award .add-car div button').hasClass('finish')){
			$(this).find('img.img-photo').addClass('hidden');
			$(this).find('img.img-activity').removeClass('hidden');
		}
	},function(){
		$(this).find('img.img-photo').removeClass('hidden');
		$(this).find('img.img-activity').addClass('hidden');
	});*/


	$('.anniversary-5th-banner .down.pc img').click(function(){
		var y = $('.anniversary-5th-share').offset().top -175 ;
		$('html,body').animate({scrollTop: y},800);
	});

	$('.anniversary-5th-share .down.pc img').click(function(){
		var y =	$('.anniversary-5th-award').offset().top -150;
			$('html,body').animate({scrollTop: y},800);
	});
	
	$('.anniversary-5th-banner .down.ph img').click(function(){
		var y = $('.anniversary-5th-share').offset().top -40 ;
		$('html,body').animate({scrollTop: y},800);
	});

	$('.anniversary-5th-share .down.ph img').click(function(){
		var y =	$('.anniversary-5th-award').offset().top -40;
			$('html,body').animate({scrollTop: y},800);
	});

    $(document).on('click touchstart','.anniversary-5th-award ul.ph .owl-item.active li .choose', function(){
		var target = typeof target !== 'undefined' ? target : '.anniversary-5th-award ul .owl-item.active li .choose';
		var target_element = $(target);
		var btn = $('.anniversary-5th-award .add-car div button');
        if (!btn.hasClass('finish')) {
        	var remain = $(this).closest('li').find('.gift_remain span.remain').text();
        	if(remain != "請點選「立即領取」" ){
				swal('好禮已領完');
				
    		}else{

				$(this).find('img.img-photo').addClass('display-none');
				$(this).find('img.img-activity').addClass('display-block');
		        var this_active = $('.anniversary-5th-award ul li.gift');
		        this_active.removeClass('gift');

		        $(target).closest('li').addClass('gift');
		        $('.anniversary-5th-award .add-car div button').removeClass('btn-disable');
		        $('.anniversary-5th-award .add-car div button').addClass('btn-danger');
		        $('.anniversary-5th-award .add-car div button').text('立即領取');
		        $('.anniversary-5th-award .add-car div button').removeAttr('disabled');

	    	}
    	}
	});

    $('.anniversary-5th-award ul.pc li .choose').click(function(){

        if(!$('.anniversary-5th-award .add-car div button').hasClass('finish')){
        	var remain = $(this).closest('li').find('.gift_remain span.remain').text();
        	
        	if(remain != "請點選「立即領取」" ){
					swal('好禮已領完');
				
    		}else{
				$(this).find('img.img-photo').addClass('display-none');
				$(this).find('img.img-activity').addClass('display-block');
		        var this_active = $('.anniversary-5th-award ul li.gift');
		        this_active.removeClass('gift');
		        $(this).closest('li').addClass('gift');
		        $('.anniversary-5th-award .add-car div button').removeClass('btn-disable');
		        $('.anniversary-5th-award .add-car div button').addClass('btn-danger');
		        $('.anniversary-5th-award .add-car div button').text('立即領取');
		        $('.anniversary-5th-award .add-car div button').removeAttr('disabled');

    		}

        }
    });

	$('.anniversary-5th-award .add-car button').click(function(){
		var gift_id = $('.anniversary-5th-award ul li.gift .choose span').text();
		var gift_name = $('.anniversary-5th-award ul li.gift  .product_name').text();
		var gift_photo =$('.anniversary-5th-award ul li.gift .choose img.img-activity').attr('src'); 
		swal({
			  title: '選擇的好禮',
			  text: gift_name,
			  showCancelButton: true,
			    imageUrl: gift_photo,
				imageWidth: 200,
				imageHeight: 250,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: '確定',
			  cancelButtonText: '取消'
			}).then((result) => {
					$('.anniversary-5th-award .loading').removeClass('hidden');
				    $('.anniversary-5th-award .add-car div button').attr('disabled',true);
				    $('.anniversary-5th-award .add-car div button').text('好禮已領取');
		            $('.anniversary-5th-award .add-car div button').removeClass('btn-danger');
    				$('.anniversary-5th-award .add-car div button').addClass('btn-disable');
    				$('.anniversary-5th-award .add-car div button').addClass('finish');
    				$('.anniversary-5th-award .gift .remain-choose .remain').addClass('hidden');
				 $.ajax({
					url: "{!! route('benefit-sale-addGift') !!}",
					data: {'_token': $('meta[name=csrf-token]').prop('content'), 'gift':gift_id},
					type: 'POST',
					dataType: 'json',
					success: function(result){
						$.each( result.result, function(key, val){
							if(key == "true"){
								swal({
								      title: '完成',
								      html: '已加入<a href ={{route("cart")}} target="_blank">購物車</a>',
								      type: 'success',
								      confirmButtonClass: 'confirm-ok',
								      allowOutsideClick: false
								    });
							}else{
								swal({
								      title: '錯誤',
								      text: val,
								      type: 'error'
								    });
								if(val == "好禮已領完"){
									location.reload();
								}
							}
						});
					 	$('.anniversary-5th-award .loading').addClass('hidden');   
					},
					error: function(xhr, ajaxOption, thrownError){
					}
				});
			})
			.catch(swal.noop);
		});

		$(document).on('click','.confirm-ok',function(){
			location.reload();
		});

</script>
@stop