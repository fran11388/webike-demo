<div id="product_brand" class="row box-content winter-collerction">
	<div class="ct-winter-collerction">
        <ul class="product-list owl-carousel-6 owl-carouse-brand-product  owl-loaded owl-drag ">
    		@foreach($riderProducts as $key => $riderProduct)
				<li>
					<div class="product-grid">
						<a href="{{route('product-detail',['url_rewrite' => $riderProduct->sku]).'?'.$rel_parameter}}" target="_blank">
							<figure class="zoom-image">
								@php
									$img = isset($riderProduct->images[0])? $riderProduct->images[0]->image : NO_IMAGE;
								@endphp
								<img src="{{ $img }}">
							</figure> </a>
						<a href="{{route('product-detail',['url_rewrite' => $riderProduct->sku]).'?'.$rel_parameter}}" target="_blank">
							<span class="name product-name dotted-text2 force-limit">{{$riderProduct->name}}</span>
						</a>
						<span class="name dotted-text1 force-limit">{{$riderProduct->manufacturer->name}} </span>
						<p class="box text-center size-10rem">會員價 NT$ {{ number_format($riderProduct->final_price) }}</p>
						<p class="final-price box text-center ">回饋點數 {{ number_format($riderProduct->final_point) }} 點</p>
					</div>
				</li>
	    	@endforeach		
        </ul>
	</div>
	<div class="text-center country-discount-all-btn">
		<a href="https://www.webike.tw/collection/brand/point-five?rel=2018-07-2018Summer&sort=visits&limit=40&order=asc&br=635" class=" btn btn-warning" target="_blank">查看全部</a>
	</div>
</div>	
