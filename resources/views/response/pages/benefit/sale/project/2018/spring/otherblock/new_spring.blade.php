
<div id="new_product" class="ct-sale-page">
	<div class="title  text-center">
		<h2 class="font-bold size-15rem">春夏新品 點數回饋</h2>
	</div>
	<div class="content-box-sale">
		<ul class="product-sale">
			<li class="li-product-sale">
				<a href="{{route('collection-type-detail', ['type' => 'category' , 'url_rewrite' => '2018SS']).'?'.$rel_parameter}}" class="clearfix" target="_blank">
					<div class="photo-box">
						<ul class="clearfix">
							<li>
								<figure class="zoom-image">
									<img class="hidden-xs" src="{{assetRemote('image/benefit/big-promotion/2018/spring/ssbanner/2018ss370.jpg')}}">
									<img class="hidden-md hidden-lg" src="{{assetRemote('image/benefit/big-promotion/2018/spring/ssbanner/2018SS345.jpg')}}">
								</figure>
							</li>
						</ul>
					</div>
				</a>
				<div class="description">
					<span>「春夏新品」─ 2018 春夏騎士用品特輯，最新的都在這裡！這個春夏！依舊引領風騷走在潮流尖端！
					</span>
				</div>
			</li>
		</ul>
	</div>
	<div class="content-box-sale ss-search-bar">
		<div class="col-md-4">
			<select class=" br ss-search-bar-br">
				<option>請選擇品牌</option>
				<option value="635">RS TAICHI</option>
				<option value="2162">HONDA RIDING GEAR</option>
				<option value="1418">BATES</option>
				<option value="1375">POWERAGE</option>
				<option value="211">ELF</option>
			</select>
		</div>
		<div class="col-md-4">
			<select class="  ss-search-bar-ca ca ">
				<option>請選擇分類</option>
			</select>
		</div>
		<div class="col-md-4 ss-searchbtn-bar">
			<button id="ss-searchbtn" class="btn btn-warning searchbtn">
				搜尋
			</button>
		</div>
	</div>
	<div class="summer-block">
		@include('response.pages.benefit.sale.project.2018.spring.otherblock.new_spring_product')
	</div>
</div>
<script>
	$('#new_product .loading-box').hide();
	var token = $('meta[name=csrf-token]').attr('content');
    $(document).on('change', ".ss-search-bar-br", function(){
        var brand = $(this).find('option:selected').val();
        var _this = $(this);
        if (brand){
            $.ajax({
	        		url: "{!! route('benefit-sale-2018-promotion-getCategory') !!}",
	        		data:{'_token':token ,'brand' : brand},
	        		type:"POST",
        			dataType:'JSON',
        			success:function(result){
        				var category = result;
		                var $target = _this.closest('.ss-search-bar').find(".ss-search-bar-ca");
		                $target.find("option:not(:first)").remove().end();
		                $.each(result,function(i,val){
		                    $target.append($("<option></option>").attr("value",i).text(val));

		            	});
        		},
        			error:function(xhr,ajaxOptions,thrownError,textStatus){
        				
        		}
        	});
            
        }
    });
	

	$("#ss-searchbtn").click(function(){
		var categories = $(".ss-search-bar .ca").children(":selected").attr("value");
		var brand = $(".ss-search-bar .br").children(":selected").attr("value");
		if( categories && brand){
			$('.summer-block').html();
	       	$('#new_product .loading-box').show();
	       	$('#ss-newproduct-list').hide();
	    	$.ajax({
        		url: "{!! route('benefit-sale-2018-promotion-getItem') !!}",
        		data:{'_token':token ,'ca' : categories , 'br' : brand},
        		type:"POST",
        		dataType:'JSON',
        		success:function(result){
					$('.summer-block').html(result.html);
        			$('#new_product .loading-box').hide();
        			$('#ss-newproduct-list').show();
        			var link = "{{ route('summary',['section' => '/ca']).'/' }}" + categories + "/br/"+ brand;
        			$('#new_product .ss-newproduct-all-btn a').attr('href',link);
        			$('.owl-carousel-6').addClass('owl-carousel').owlCarousel({
						loop:false,
						nav:true,
						margin:5,
					    slideBy : 6,
						responsive:{
							0:{
								items:2
							},
							600:{
								items:3
							},
							1000:{
								items:6
							}
						}
					});

					$('.owl-carousel:not(.custom)').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
					$('.owl-carousel:not(.custom)').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
					$('.owl-carousel2:not(.custom)').find('.owl-prev').addClass('btn-owl-prev');
					$('.owl-carousel2:not(.custom)').find('.owl-next').addClass('btn-owl-next');
        		},
        		error:function(xhr,ajaxOptions,thrownError,textStatus){
        		}
        	});
		}else{
			alert('請選擇品牌及分類');
		}
	});
</script>
