<div id="special_price_product" class="row box-content winter-collerction">
	<div class="ct-winter-collerction">
        <ul class="product-list owl-carousel-6 owl-carouse-brand-product  owl-loaded owl-drag ">
    		@foreach($specialProducts as $key => $specialProduct)
				<li>
					<div class="product-grid">
						<a href="{{route('product-detail',['url_rewrite' => $specialProduct->sku]).'?'.$rel_parameter}}" target="_blank">
							<figure class="zoom-image">
								@php
									$img = isset($specialProduct->images[0])? $specialProduct->images[0]->image : NO_IMAGE;
								@endphp
								<img src="{{ $img }}">
							</figure> 
						</a>
						<a href="{{route('product-detail',['url_rewrite' => $specialProduct->sku]).'?'.$rel_parameter}}" target="_blank">
							<span class="name product-name dotted-text2 force-limit">{{$specialProduct->name}}</span>
						</a>
						<span class="name dotted-text1 force-limit">{{$specialProduct->manufacturer->name}} </span>
						<p class="box text-center size-10rem">會員價 NT$ {{ number_format($specialProduct->final_price) }}</p>
						<p class="final-price box text-center ">回饋點數 {{ number_format($specialProduct->final_point) }} 點</p>
					</div>
				</li>
	    	@endforeach		
        </ul>
	</div>
	<div class="text-center country-discount-all-btn">
		<a href="{{route('parts','br/541')}}" class=" btn btn-warning" target="_blank">查看全部</a>
	</div>
</div>	
