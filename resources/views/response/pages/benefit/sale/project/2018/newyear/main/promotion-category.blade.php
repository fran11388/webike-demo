@extends('response.layouts.1column-bigpromo')
@section('style')
<style>
	@media (min-width: 768px) {
		.navbar-nav {
			width:100%;
		}
		.navbar-nav li {
			width:9%;
			text-align: center;
			font-size:1rem;
		}
		.promotion-title {
			background: url({{assetRemote('image/benefit/big-promotion/2018_banner_head.png')}}) repeat-x #fc37a2;
			padding-top:100px;
		}
	}
	.webike-logo {
		width:100%;
	}
	.container-header {
		background-color: #ffffff;
		min-height:61px;
	}
	#mainNav {
		background-color: #ffffff;
		min-height:initial;
		position:initial;
		margin-bottom:0px;
		border:0px;
	}
	.col1-logo {
		width:271px;
		height:41px;
		margin:10px;
	}
	.big-promotion .promotion-container {
		
		max-width:none;
		width:100% !important;
		padding:0 0 40px 0 !important;
	}
	.promotion-title a:hover {
		opacity: 0.8;
	}
	.ct-sale-page {
		width: 100%;
		float: left; 
	}
	.ct-sale-page .title {
		padding: 15px 20px;
		background-color: black; 
	}
    .ct-sale-page .title h2 {
		color: #fff; 
	}

.ct-menu-sale {
  float: left; }
  .ct-menu-sale ul {
    padding: 0 3px;
    list-style-type: none; }
    .ct-menu-sale ul li {
      float: left;
      padding: 5px 12px; }
      .ct-menu-sale ul li a {
        color: #333333;
        font-size: 1rem;
        padding: 10px;
        border: 1px solid #e61e25;
        width: 100%;
        text-align: center;
        float: left; }
        .ct-menu-sale ul li a:hover {
          background-color: #e61e25;
          color: #fff; }

/*.content-box-sale {
  width: 100%;
  float: left; }*/
  .content-box-sale .product-sale {
    width: 100%;
    float: left;
    list-style-type: none; }
    .content-box-sale .product-sale .li-product-sale {
      padding: 20px 15px;
      border: 1px solid #b7b7b7;
      float: left; }
      .content-box-sale .product-sale .li-product-sale .photo-box {
        width: 100%;
        float: left; }
        .content-box-sale .product-sale .li-product-sale .photo-box li {
          list-style-type: none;
          padding: 0 10px; }
          .content-box-sale .product-sale .li-product-sale .photo-box li figure img {
            -webkit-transform: scale(1.02);
            -moz-transform: scale(1.02);
            -ms-transform: scale(1.02);
            -o-transform: scale(1.02);
            transform: scale(1.02); }
            .content-box-sale .product-sale .li-product-sale .photo-box li figure img:hover {
              -webkit-transform: scale(1.2);
              -moz-transform: scale(1.2);
              -ms-transform: scale(1.2);
              -o-transform: scale(1.2);
              transform: scale(1.2); }
        .content-box-sale .product-sale .li-product-sale .photo-box li:first-child {
          /*border-right: 1px dotted #b7b7b7;*/
          padding-left: 0;
          width: 66.3%;
          float: left; }
        .content-box-sale .product-sale .li-product-sale .photo-box li:last-child {
          padding-right: 0;
          width: 33.7%;
          float: left; }
      .content-box-sale .product-sale .li-product-sale:hover {
        border: 1px solid #e61e25; }
    .content-box-sale .product-sale label {
      margin: 10px 0;
      padding: 2px 5px;
      font-size: 1rem;
      font-weight: bold;
      width: 100%;
      color: #fff;
      float: left;
      background-color: #696969;
      text-align: center;
      height: 28px; }
    .content-box-sale .product-sale span {
      width: 100%;
      font-size: 1.2rem;
      float: left; }
    .section {
    	background:white;
    	border-radius: 5px;
    	margin-top:30px;
    }
    .form-group {
    	margin-bottom:0px;
    }
    span {
    	float:none !important;
    	font-size:1rem !important;
    }
    .description {
    	margin-top:10px;
    }
    .activity .photo-box li {
    	width:100% !important;
    }
    .link-home {
    	margin-top:0px !important;
    	max-width:1903px !important;
    	background-color: #1ca28a !important;
    }
    .block-link {
    	list-style-type:none;
    	border:1px solid white;
    	float:left;
	    margin: 0 2% 10px 0;
	    width: 23.5%;
    }
    @media (min-width: 501px) {
	    .block-link:nth-child(4n) {
			margin: 0 0 10px 0;
	    }
	}
    .block-link:hover{
		background-color: white;
		cursor: pointer;
    }
    .block-link a{
    	display:block;
    	width:100%;
		color:white;
		border:0px ;
		padding:10px;
    }
    .block-link a:hover{
		color:black ;
    }
    @media (max-width: 768px) {
	    .category {
	    	width:50% !important;
	    }
	}
    @media (max-width: 566px) {
	    .block-link {
	    	list-style-type:none;
	    	border:1px solid white;
	    	float:left;
		    margin: 0 0 20px 0;
		    width: 100%;
	    }
	    .category {
	    	width:100% !important;
	    }
	}
    .category .photo-box li {
    	max-height:108px;
    }
    .category .zoom-image {
    	height:108px;
    	display:block;
    }
    .manufacturer{
    	display:block;
    	margin-bottom:10px;
    	    padding-top: 10px;
    	clear:left;
    }
	.helper {
		width:auto !important;
		height:100% !important;
	}
	.category figure.thumb-box-border {
		border:0px;
	}
	/*.product-sale .li-product-sale:nth-child(3n){
		border-right:0px !important;
	}*/
	.main-container .link-container {
		display:none;
	}
	.link-home .link-container{
		padding-right: 15px;
		padding-left: 15px;
	}
	.promotion-container .categories-container{
		margin-bottom: 100px;
	}
	
</style>
@stop
@section('middle')
{{-- <div class="promotion-title"></div> --}}
@include('response.pages.benefit.sale.project.2018.otherblock.main-banner')
{{-- @include('response.pages.benefit.sale.project.2018.otherblock.searchbar') --}}
@include('response.pages.benefit.sale.project.2018.otherblock.main-category-link')
	<div class="clearfix text-center link-home">
		<div class="link-container">
	        <ul class="clearfix">
	        	@foreach($categories as $key => $items)
	        		@if($items[0]->mptt->name == "旅行攜物網・綑綁帶・掛鈎")
		            	<li class="block-link size-12rem"><a onclick="slipTo1('#{{$items[0]->mptt->url_rewrite}}')">旅行攜物網・綁帶</a></li>
		            @else
		            	<li class="block-link size-12rem"><a onclick="slipTo1('#{{$items[0]->mptt->url_rewrite}}')">{{$items[0]->mptt->name}}</a></li>
		            @endif
		        @endforeach
	        </ul>
	    </div>
	</div>
<div class="container categories-container">

	@foreach($categories as $key => $items)
		<div class="section clearfix">
			<div id="item1" class="ct-sale-page form-group">
	            <div class="title col-xs-12 col-sm-12 col-md-12 text-center" id="{{$items[0]->mptt->url_rewrite}}">
	                <h2 class="font-bold size-15rem">{{$items[0]->mptt->name}}</h2>
	            </div>
	            <div class="content-box-sale">
	                <ul class="product-sale">
	                	@foreach($items as $category)
							@php
								$br_rewrite = $category['br_rewrite'];
                                $ca_rewrite = $category['ca_rewrite'];
                                $discount = $discounts->filter(function($discount) use($br_rewrite, $ca_rewrite){
                                    return ($discount->category_url_rewrite == $ca_rewrite and $discount->manufacturer_url_rewrite == $br_rewrite);
                                })->first();
							@endphp
		                    <li class="li-product-sale col-xs-12 col-sm-6 col-md-4 category text-center">
		                        <a href="{{URL::route('parts',['section' => 'ca/'.$category->mptt->url_path.'/br/'.$category['br_rewrite']]).'?'.$rel_parameter}}" target="_blank">
		                            <div class="photo-box">
		                                <ul class="clearfix">
		                                    <li class="col-xs-7 col-sm-7 col-md-7">
		                                    	<div class="zoom-container">
		                                    		<figure class="zoom-image"><img src="{{$category['br_image']}}" alt=""><span class="helper"></span></figure>
		                                    	</div>
		                                    </li>
		                                    <li class="col-xs-5 col-sm-5 col-md-5">
		                                    	<div class="zoom-container">
		                                    		<figure class="zoom-image thumb-box-border"><img src="{{$category['ca_image']}}" alt=""><span class="helper"></span></figure>
		                                    	</div>
		                                    </li>
		                                </ul>
		                            </div>
		                            <span class="manufacturer">{{$category->manufacturer->name}}</span>
									@if($discount)
										@if(strpos($category['br_rewrite'], 't') !== false)
											@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
												<span class=" dotted-text1 font-color-red text-center">點數{{number_format($discount->d_point)}}倍</span>
											@else
												<span class=" dotted-text1 font-color-red text-center">點數{{number_format($discount->c_point)}}倍</span>
											@endif
										@else
											@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
												<span class=" dotted-text1 font-color-red text-center">最大{{number_format($discount->discountd)}}%off</span>
											@else
												<span class=" dotted-text1 font-color-red text-center">最大{{number_format($discount->discountc)}}%off</span>
											@endif
										@endif
									@else
										<span class=" dotted-text1 font-color-red text-center">大特價</span>
									@endif
		                        </a>
		                    </li>
		                @endforeach
	                </ul>
	            </div>
	        </div>
	    </div>
	@endforeach
    @include('response.pages.benefit.sale.project.2018.otherblock.otherpromotions')
</div>
@include('response.layouts.partials.footer.shopping')
@stop
@section('script')
<script>
	function slipTo1(element){
	    var y = parseInt($(element).offset().top) - 180;
	    $('html,body').animate({scrollTop: y}, 400);
	}
	$(document).ready(function(){
		var get = null;
		@if(\Request::input('ca'))
			get = '#' + "{{\Request::input('ca')}}";
		@endif
		if(get){
			var y = parseInt($(get).offset().top) - 180;
	    	$('html,body').animate({scrollTop: y}, 400);
		}
	});
</script>
@stop