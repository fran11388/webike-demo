<style>
	.otherPromotions .photo-box li{
		width:100% !important;
	}
		.othertitle{
		margin-bottom: 10px;
	}
		.otherlink{
		margin-top: 10px;
		height: 70px;
		overflow: auto;
	}
</style>
<?php
	$otherPromotions = array(
		'本月好康活動'=>array(
//			'正廠零件95折優惠'=>array(
//				'img'=>assetRemote('image/benefit/big-promotion/genuine345.png'),
//				'description'=>'凡在SPRING SALE期間內購買正廠零件，全品項95折優惠!
//
//日本 HONDA、 YAMAHA、 SUZUKI、 KAWASAKI、 美國 HARLEY-DAVIDSON、 德國 BMW、 英國 TRIUMPH、 義大利 DUCATI、 APRILIA、 PIAGGIO、 VESPA、 HUSQVARNA、 MOTOGUZZI、 GILERA、 奧地利 KTM、 泰國 HONDA、 YAMAHA、 十七個進口廠牌。
//
// 光陽KYMCO、 三陽SYM、 台灣山葉YAMAHA、 台鈴SUZUKI、 宏佳騰AEON、 PGO摩特動力、 哈特佛HARTFORD、七大國產廠牌。	',
// 				'url'=>URL::route('genuineparts')
//			),
            // 'WANTED 每日搜索任務'=>array(
            //     'description'=>'完成每日任務，即可獲得10元現金點數，31天全數答對，再送200點，最大510元點數回饋。',
            //     'img'=>assetRemote('image/benefit/big-promotion/Wanted345.png'),
            //     'url'=>URL::route('benefit-event-webikewanted')
            // ),
			'周年慶品牌現折'=>array(
				'img'=>assetRemote('image/benefit/big-promotion/2018/fall/banner/count_discount270.jpg'),
				'description'=>'周年慶品牌現折─多項優質品牌，上千項點數優惠商品，回饋的現金點數立即現折。',
				'url'=>'https://www.webike.tw/collection/brand/point-discount?rel=2018-10-2018fall&sort=visits&limit=40&order=asc'
			),
			'2019秋冬新品 點數5倍'=>array(
				'img'=>assetRemote('image/benefit/big-promotion/2018/fall/banner/345x270.jpg'),
				'description'=>'「2019秋冬精選」─各大品牌換季上架，趁著天涼好個秋，備齊騎士用品！',
				'url'=>'https://www.webike.tw/collection/brand/point-five?rel=2018-10-2018fall&sort=visits&limit=40&order=asc'
			),
            'GUESS 每日任務'=>array(
                'description'=>'「Webike Guess」─答對即可獲得「10元現金點數」最高可獲得510元現金點數！',
                'img'=>assetRemote('image/benefit/big-promotion/2018/activity/webikeguesslong345.jpg'),
                'url'=>URL::route('benefit-event-webikeguess-get')
            )
		),
		'其他好康'=>array(
            // '預測總冠軍'=>array(
            //     'description'=>'預測2017MotoGP各分站前三名，猜對即可贏得免費點數，並有機會贏得K-3 SV Top Tartaruga 賽車安全帽、Webike套裝紀念貼紙及300元禮券',
            //     'img'=>assetRemote('image/benefit/big-promotion/gpbanner1.png'),
            //     'url'=>URL::route('benefit-event-motogp-2017')
            // ),
            'OUTLET'=>array(
                'description'=>'「Outlet」定期上架各大品牌出清商品，賣不掉就再降，快來尋找專屬於你的寶物。',
                'img'=>assetRemote('image/benefit/big-promotion/2018/fall/banner/outlet345.jpg'),
                'url'=>URL::route('outlet')
            ),
            'MyBike'=>array(
                'img'=>assetRemote('image/benefit/big-promotion/2018/otherpages/mybike345.jpg'),
                'description'=>'只要您首次登錄「MyBike」，即可獲得$100元點數',
                'url'=>URL::route('benefit-event-mybike')
            ),
            '本月加價購'=>array(
			'img'=>assetRemote('image/benefit/big-promotion/2018/fall/banner/10addprice345.jpg'),
			'description'=>'「加價購」─本月專屬加價購，訂單滿額商品讓你銅板帶回家！',
			'url'=>URL::route('cart')
            )
		)
	);
?>
@foreach($otherPromotions as $title => $promotionInfo)
	<div class="section clearfix">
		<div id="item1" class="ct-sale-page form-group clearfix otherPromotions">
		    <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
		        <h2 class="font-bold size-15rem">{{$title}}</h2>
		    </div>
		    <div class="content-box-sale">
		        <ul class="product-sale">
		        	@foreach($promotionInfo as $promotionName => $info)
			            <li class="li-product-sale col-xs-12 col-sm-12 col-md-4">
			                <div class="text-center font-bold othertitle">
			                	<h2>{{$promotionName}}</h2>
			                </div>
			                @if($promotionName == "銷售排行")
			                <a href="{{$promotionInfo[$promotionName]['url'].'?sort=year&'.$rel_parameter}}" class="clearfix" target="_blank">
		                	@else
			                <a href="{{$promotionInfo[$promotionName]['url'].'?'.$rel_parameter}}" class="clearfix" target="_blank">
		                	@endif
			                    <div class="photo-box">
			                        <ul class="clearfix">
			                            <li class="col-xs-12 col-sm-12 col-md-12 text-center"><figure class="zoom-image col-sm-block col-xs-block"><img src="{{$promotionInfo[$promotionName]['img']}}" alt=""></figure></li>
			                        </ul>
			                    </div>
			                </a>
			                <div class="otherlink">
			                	<span class="">{{$promotionInfo[$promotionName]['description']}}</span>
			                </div>
			            </li>
			        @endforeach
		        </ul>
		    </div>
		</div>
	</div>
@endforeach