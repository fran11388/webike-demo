<div id="product_brand" class="row box-content winter-collerction">
	<div class="ct-winter-collerction">
        <ul class="product-list owl-carousel-6 owl-carouse-brand-product  owl-loaded owl-drag ">
    		@foreach($products as $key => $product)
				<li>
					<div class="product-grid">
						<a href="{{route('product-detail',['url_rewrite' => $product->sku]).'?'.$rel_parameter}}" target="_blank">
							<figure class="zoom-image">
								@php
									$img = isset($product->images['0'])? $product->images['0']->thumbnail: NO_IMAGE;
								@endphp
								<img src="{{ $img }}">
							</figure> </a>
						<a href="{{route('product-detail',['url_rewrite' => $product->sku]).'?'.$rel_parameter}}" target="_blank">
							<span class="name product-name dotted-text2 force-limit">{{$product->name}}</span>
						</a>
						<span class="name dotted-text1 force-limit">{{$product->manufacturer->name}} </span>
						<?php
							$final_price = $product->getFinalPrice($current_customer);
							$final_point = $product->getFinalPoint($current_customer);
						?>
						<p class="box text-center size-10rem"><s>會員價 NT$ {{ number_format($final_price) }}</s></p>
						<p class="final-price box text-center ">現折價 NT$ {{ number_format($final_price-$final_point) }}</p>
					</div>
				</li>
	    	@endforeach		
        </ul>
	</div>
	<div class="text-center country-discount-all-btn">
		<a href="https://www.webike.tw/collection/brand/point-discount?rel=2018-07-2018Summer&sort=visits&limit=40&order=asc&br=t10" class=" btn btn-warning" target="_blank">查看全部</a>
	</div>
</div>	
