<style>
	.collection .category{
	 	margin: 30px 0px 100px 0px;	 	
	}

	.owl-stage-outer{
	 	overflow: none;
	}
	#category span{
	 	font-size: 1.2rem !important;
	}
	.category-img{
	 	margin-top: 10px;
	 	margin-bottom: 10px;
	}
	.product-grid a figure{
	 	width:100%;
	 	height: 100%;
	 	vertical-align: middle;
	    text-align: center;
	    position: relative;
	    display: block;
	    font-size: 0;
	    margin: 0 auto;
	}
	.category .category-title{
	 	width:100%;
	}
 	.category-text{
	margin-top: 10px;
	height: 55px;
	overflow: auto;
	}
		@media (min-width: 768px) {
		.collection .li-product-sale {
	    	margin-left:5%;
	    	margin-bottom:40px;
	    }
	}
	    .collection {
    	background-color: white;
    }
    .collection .collectiontext {
    	margin-top:40px;
    	margin-bottom:-20px;
    }
    .collection .btn {
    	width:200px;
    	height:40px;
    	padding-top:10px;
    }
    .allbtn {
    	margin-top:30px;
    }
    .allbtn ul{
    	list-style-type: none;
    }
    .collection span.dotted-text1{
    	display:block;
    	margin-bottom:10px;
    }
    .imgcontainer{
    	width:100% !important;
    }
	.dotted-text3.force-limit{
		height: 70px !important;
	}
	@media (max-width: 768px) {
	    .collection .content-box-sale {
	    	margin-bottom:0px;
	    }
	    .moreCollection {
			margin:20px 0 10px 0;
		}
		.loginbar li{
			margin:10px 0px;
			padding:0px;
		}
		.allbtn {
			margin-top:0px;
		}
	}
	.collection .categorya {
		margin:30px 0px;
	}
	.collection .collection-bottom{
		margin: 0px 0px 100px 0px;
	}
	.collection .product-grid .dotted-text1 {
		height:21.5px !important;
	}
	#category .box-content{
		border: 1px solid #1ca28a;
	}
</style>
<div class="collection clearfix " id="collection">
	<div id="category" class="ct-sale-page form-group clearfix {{ $current_customer ? 'category':'categorya' }}">
	    <div class="title width-full text-center">
	        <h2 class="font-bold size-15rem">特輯情報</h2>
	    </div>
		<div class="box-content winter-collerction">
			<div class=" ct-winter-collerction ">
		        <ul class="product-list owl-carouse-advertisement-loop ">
					@foreach($collections as $key => $collection)
						<li>
							
							{{-- {{dd($collection)}} --}}
							<div class="product-grid">
								<a href="{{$collection->link}}?rel=2018-01-2018Spring" title="{{$collection->name.$tail}}" target="_blank" class="clearfix">
								<span class="  name product-name dotted-text1 force-limit text-center category-title">{{$collection->name}}</span>
								</a>
								<div class="category-img">
								<a href="{{$collection->link}}?rel=2018-01-2018Spring" title="{{$collection->name.$tail}}" target="_blank" class="clearfix">
										<ul class="clearfix">
											<li class="col-xs-12 col-sm-12 col-md-12 imgcontainer">
												<figure class="zoom-image">
													<img src="{!! $collection->banner !!}" alt="{{$collection->name.$tail}}">
												</figure>
											</li>
										</ul>
								</a>
								</div>
								<div class="  category-text text-left">
									<span class=" box ">{{$collection->meta_description}}</span>
								</div>
							</div>
						</li>
					@endforeach
		        </ul>
			</div>
		</div>
	</div>
	@if(!$current_customer)
		<div class="allbtn col-xs-12 col-sm-12 col-md-12 collection-bottom">
			<ul class="clearfix loginbar">
				<li class="col-xs-12 col-sm-4 col-md-4 text-center">
					<a class="btn btn-danger" href="{{URL::route('customer-account-create')}}?rel=2018-01-2018Spring" target="_blank">新會員募集</a>
				</li>
				<li class="col-xs-12 col-sm-4 col-md-4 text-center">
					<a class=" btn btn-default btn-warning border-radius-2 btn-signin" href="{{URL::route('login')}}?rel=2018-01-2018Spring" target="_blank">登入</a>
				</li>
				<li class="col-xs-12 col-sm-4 col-md-4 text-center">
					<a class="btn btn-default btn-primary border-radius-2 btn-login-with-face btn-full" href="{{URL::route('login-facebook')}}?rel=2018-01-2018Spring" target="_blank">Facebook 登入</a>
				</li>
			</ul>
		</div>
	@endif 
</div>
<script>
	$('.owl-carouse-advertisement-loop').addClass('owl-carousel').owlCarousel({
	autoplay:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	loop:true,
	margin:10,
	nav:true,
    slideBy : 3,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});

</script>