
	<div class="fatherday-productguide-product-container" id="father-choose-product" >
		<ul class="father-productguide-product owl-carousel-product">
			@foreach ($products as $product)
				<li>
					<div class="product-grid">
						<a href="{{route('product-detail',['url_rewrite' => $product->sku])}}" target="_blank">
							@php
								$img = isset($product->images['0'])? $product->images['0']->thumbnail : NO_IMAGE;
							@endphp
							<figure class="zoom-image">
								<img src="{{$img}}">
							</figure>
						</a>
						<a href="{{route('product-detail',['url_rewrite' => $product->sku])}}" target="_blank">
							<span class="name product-name dotted-text2 force-limit">{{$product->name}}</span>
						</a>
						<span class="name dotted-text1 force-limit">{{$product->manufacturer->name}}</span>		
						<p class="final-price box text-center">會員價 NT$ {{ number_format($product->final_price) }}</p>						
					</div>
				</li>
			@endforeach
		</ul>
		<div class="dad-button-block">
			<div class="text-center">
					<a href="https://www.webike.tw/collection/brand/fatherday-dad?sort=visits&limit=40&order=asc&ca=1000-1001" class="btn dad-button" target="_blank">更多酷爸必須品</a>
			</div>
		</div>
		<div class="kid-button-block hidden">
			<div class="text-center">
				<a href="https://www.webike.tw/collection/brand/fatherday-kid?sort=visits&limit=40&order=asc&ca=3000-3001" class="btn kid-button" target="_blank">展現孝心挑禮物</a>
			</div>
		</div>
	</div>
