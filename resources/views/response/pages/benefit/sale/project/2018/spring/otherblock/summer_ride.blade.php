<style>
	#summer_ride .box-content{
		margin-bottom: 0px;
	    padding: 0;
	    background-color: #fff;
	    border: 1px solid #c7b299;
	    display: block;
	    width:100%;
	    margin-left: auto;
	    margin-right: auto;

	}
	#summer_ride .content-box-sale{
		float: none;
	}
	#summer_ride{
		float: none;
		margin-top: 30px;
	}

	#summer_ride .product-grid span{
		width: 100%;
	}

	#summer_ride .brand_url_rewrite img{
	    
		height:71px;
	}
	#summer_ride .brand_url_rewrite:hover img{
	    opacity: 1;
		height:71px;
	    transform: scale(1.3);
        
	}
	#summer_ride .brand_url_rewrite:hover{
		cursor: pointer;
	}
	#summer_ride #summer_brand .brand_url_rewrite{
		opacity:0.5;
	}

	#summer_ride #summer_brand .owl-stage .center .brand_url_rewrite{
		opacity:1;
	}
	#summer_ride .loading-box {
		display:none;
	}
	#summer_ride #product_brand .country-discount-all-btn{
		margin-bottom: 20px;
	}
	#summer_ride #product_brand .country-discount-all-btn .btn-warning{
		background-color: #b92229;
	}
	#summer_ride #summer_discount_brand .product-grid a figure{
		height: inherit;
	}
	@media(max-width: 550px){
		#summer_ride .brand_url_rewrite img{
				height: inherit;
			}
		#summer_ride #summer_discount_brand #summer_brand .product-grid .col-md-7 .zoom-container .zoom-image{
			display: block;
		}
	}
</style>
@php
	$summer_brands = [
		array('id'=>'635',
			'name' => 'RS TAICHI',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_635.gif',
					'img'=>'https://img.webike.net/catalogue/images/52764/RST633WH01_01.jpg',
				     'discount'=>'點數5倍回饋'),
		array('id'=>'289',
			'name' => 'HenlyBegins',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_289.gif',
					'img'=>'https://img.webike.net/catalogue/images/41966/96900_01.jpg',
				     'discount'=>'點數5倍回饋'),
		array('id'=>'1206',
			'name' => 'FIVE	',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1206.gif',
					'img'=>'https://img.webike.net/catalogue/images/52901/wfx_max_wp_black_2019_face.jpg',
				     'discount'=>'點數5倍回饋'),
		array('id'=>'1375',
			'name' => 'POWERAGE',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1375.gif',
					'img'=>'https://img.webike.net/catalogue/18463/PJ478_BK.jpg',
				     'discount'=>'點數5倍回饋'),
		array('id'=>'2162',
			'name' => 'HONDA RIDING GEAR',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_2162.gif',
					'img'=>'https://img.webike.net/catalogue/images/53402/0SYESY37K_01.jpg',
				     'discount'=>'點數5倍回饋'),
		];
@endphp
<div id="summer_ride" class="ct-sale-page form-group clearfix">
    <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
        <h2 class="font-bold size-15rem">2019秋冬新品 點數5倍</h2>
    </div>
    <div class="content-box-sale clearfix">
        <ul class="product-sale">
            <li class="li-product-sale col-xs-12 col-sm-12 col-md-12">
                <a href="https://www.webike.tw/collection/brand/point-five?rel=2018-10-2018fall&sort=visits&limit=40&order=asc" class="clearfix" target="_blank">
                    <div class="photo-box">
                        <ul class="clearfix">
                            <li class="col-xs-12 col-sm-12 col-md-12">
                            	<figure class="zoom-image">
                            		<img class="hidden-xs" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/fall370.jpg')}}" alt="2019秋冬新品 點數5倍">
                            		<img class="hidden-md hidden-lg" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/345x270.jpg')}}" alt="2019秋冬新品 點數5倍">
                            	</figure>
                            </li>
                        </ul>
                    </div>
                </a>
                <div class="description">
                	<span class="">	
                		「2019秋冬精選」─各大品牌換季上架，趁著天涼好個秋，備齊騎士用品！
                	</span>
                </div>
            </li>
        </ul>
    </div>
    <div id="summer_discount_brand" class="row box-content winter-collerction">
		<div class="ct-winter-collerction">
	        <ul id="summer_brand" class="product-list owl-carouse-advertisement-test  owl-loaded owl-drag ">
	    		@foreach($summer_brands as $title => $summer_brand)
	       			<li id="{{ $summer_brand['id'] }}" class="brand_url_rewrite">
                   		<div class="product-grid">
                   			<ul class="clearfix">
                   				<a href="{{route('summary', 'br/'.$summer_brand['id'])}}" target="_blank">
	                   				<li class="col-xs-12 col-sm-7 col-md-7">
	                   					<div class="zoom-container">
			                   				<figure class="zoom-image">
			                   					<img src="{{$summer_brand['brandimg']}}">
			                   					<span class="helper"></span>
			                   				</figure>
		                   				</div>
							    	</li> 
							    	<li class="col-xs-5 col-sm-5 col-md-5 hidden-xs">
							    		<div class="zoom-container">
			                   				<figure class="zoom-image">
			                   					<img src="{{$summer_brand['img']}}" >
			                   					<span class="helper"></span>
			                   				</figure>
		                   				</div> 
							    	</li>
						    	</a>
		    				</ul>
			    		</div>
			    		<div class="product-grid">
					        <span class="manufacturer">{{$summer_brand['name']}}</span>
				    
			    			<span class="final-price box text-center">{{$summer_brand['discount']}}</span>
		    			</div>
		    		</li>
		    	@endforeach		
	        </ul>
		</div>
	</div>
	@include('response.common.loading.md')
	@include('response.pages.benefit.sale.project.2018.spring.otherblock.summer_ride_product')
</div>
<script>
	$('.owl-carouse-advertisement-test').addClass('owl-carousel').owlCarousel({
	center:true,
 	loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    touchDrag:false,
    slideBy : 1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});

	$('#summer_discount_brand #summer_brand .owl-nav').click(function(){
		var brand_url_rewrite = $('#summer_ride #summer_brand .center .brand_url_rewrite').attr('id');
		// alert(brand_url_rewrite);
	       	$('#summer_ride #product_brand').hide();
	       	$('#summer_ride .loading-box').show();
    	 	var token = $('meta[name=csrf-token]').attr('content');
    	 	// var brand_url_rewrite = $('#summer_ride #summer_brand .brand_url_rewrite').attr('id');

        	$.ajax({
        		url: "{!! route('benefit-getProductByBrandAndCategory') !!}",
        		data:{'_token':token ,'brand_url_rewrite' : brand_url_rewrite},
        		type:"POST",
        		dataType:'JSON',
        		success:function(result){
        			$('#summer_ride .loading-box').hide();
        			$('#summer_ride #product_brand').replaceWith(result.html);
    				slider = $('#summer_ride .owl-carouse-brand-product').addClass('owl-carousel').owlCarousel({
	                    loop:false,
	                    nav:true,
	                    margin:5,
	                    slideBy : 6,
	                    URLhashListener:true,
	                    startPosition: 'URLHash',
	                    responsive:{
	                        0:{
	                            items:2
	                        },
	                        600:{
	                            items:3
	                        },
	                        1000:{
	                            items:6
	                        }
	                    }
	                }).find("img").unveil().trigger("unveil");
        			$('#summer_ride .owl-carouse-brand-product.owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
                    $('#summer_ride .owl-carouse-brand-product.owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
                	var summer_ride_url = "https://www.webike.tw/collection/brand/point-five?rel=2018-07-2018Summer&sort=visits&limit=40&order=asc&br=" + brand_url_rewrite;
	                $('#summer_ride #product_brand .country-discount-all-btn a').attr('href',summer_ride_url);
        		},

        		error:function(xhr,ajaxOptions,thrownError,textStatus){
        			
        		}
        	});
        });

		
		
</script>