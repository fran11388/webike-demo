<div class="ss-newproduct content-box-sale">
	 	@include('response.common.loading.md')
		<div id="ss-newproduct-list">
			<ul class=" {{ $is_phone ? 'owl-carousel-6 owl-carouse-brand-product' : ' '}}">
				@foreach($new_products as $key => $product)
					@php
						$name = $new_products[$key]->name;
						$sku = $new_products[$key]->sku;
						$image = $new_products[$key]->getImage();
						$price = number_format($new_products[$key]->getPrice());
						$brand = $new_products[$key]->getManufacturerName();
						$point = $new_products[$key]->getPoint();
					@endphp
					<li>
						<div class="product-grid"> 
							<a href="{!! route('product-detail',$sku)!!}" target="_blank">
								<figure>
									<img src="{{$image}}">	
								</figure>
							</a>
							<a href="{!! route('product-detail',$sku)!!}" target="_blank">
								<span class="dotted-text2 name dotted-text2 force-limit">{{$name}}</span>
							</a>
							<div>
								<span class="name dotted-text1 force-limit">{{$brand}}</span>
							</div>
							<div class="text-center box">
								<span class="font-color-red font-bold">NT${{$price}}</span>
							</div>
							<div class="text-center">
								<span class="font-color-red font-bold">回饋點數:{{$point}}點</span>
							</div>
						</div>
					</li>
				@endforeach
			</ul>
			<div class="text-center ss-newproduct-all-btn">
				<a href="{{route('collection-type-detail', ['type' => 'category' , 'url_rewrite' => '2018SS']).'?'.$rel_parameter}}" class=" btn btn-warning" target="_blank">查看全部</a>
			</div>
		</div>
	</div>