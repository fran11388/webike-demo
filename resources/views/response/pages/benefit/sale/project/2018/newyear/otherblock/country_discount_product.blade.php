<div id="product_brand" class="row box-content winter-collerction">
	<div class="ct-winter-collerction">
        <ul class="product-list owl-carousel-6 owl-carouse-brand-product  owl-loaded owl-drag ">
    		@foreach($products as $key => $product)
				<li>
					<div class="product-grid">
						<a href="{{route('product-detail',['url_rewrite' => $product->sku])}}" target="_blank">
							<figure class="zoom-image">
								@php
								$img = isset($product->images['0']->thumbnail)? $product->images['0']->thumbnail: 'NO_IMAGE';

								@endphp
								<img src="{{ $img }}">
							</figure> </a>
						<a href="{{route('product-detail',['url_rewrite' => $product->sku])}}" target="_blank">
							<span class="name product-name dotted-text2 force-limit">{{$product->name}}</span>
						</a>
						<span class="name dotted-text1 force-limit">{{$product->manufacturer->name}} </span>
						<?php
							$final_price = $product->getFinalPrice($current_customer);
							$final_point = $product->getFinalPoint($current_customer);
							$discount_text = '';
							$discount = round($final_price / $product->price ,2) * 100;
							if($discount < 90){
								$discount_text = '('. $discount . '%)';
							}
						?>
						<p class="final-price box ">NT$ {{ number_format($final_price) }}{{$discount_text}}</p>
					</div>
				</li>
	    	@endforeach		
        </ul>
	</div>
</div>	
