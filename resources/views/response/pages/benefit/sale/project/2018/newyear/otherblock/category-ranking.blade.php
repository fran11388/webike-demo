<style>
    #discount_week .category-block{
        padding:10px;
    }
    #discount_week #item1 .title-section > li {
        padding-left:7.5px;
        padding-right:7.5px;
    }
    #discount_week #item1 ul.title-section{
        border:0px;
    }
    #discount_week .title-section li{
        background-color:white !important;
    }
    #discount_week li .title{
        background: #8ed1c5 !important;
    }
    #discount_week li h2{
        color:black !important;
    }
    #discount_week li.active .title{
        background-color:#1ca28a !important;
    }
    #discount_week li.active .title h2{
        color:white !important;
    }


    #discount_week .ct-sale-page .row {
        margin-right:-7.5px !important;
        margin-left:-7.5px !important;
    }
    #discount_week .content-box-sale{
        border: 1px solid #1ca28a;
    }

    #discount_week .content-box-sale ul li{
        border:0px;
        padding:15px;
    }
    #discount_week .content-box-sale ul li:nth-child(1),#discount_week .content-box-sale ul li:nth-child(2),#discount_week .content-box-sale ul li:nth-child(3),#discount_week .content-box-sale ul li:nth-child(4){
        padding-top:0px;
    }



    #discount_week .category_choose:hover{
        background: #1ca28a !important;
    }


    #discount_week .content-box-sale{
        position: relative;
    }
    #discount_week .category_choose_left {
        position: absolute;
        top: 36%;
        left: 0;
        padding: 80px 20px;
        background-color: rgba(28,162,138,.3);
        -webkit-box-shadow: 3px 1px 3px 0px rgba(0, 0, 0, 0.2);
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
        cursor:pointer;
        z-index:100;
    }
    #discount_week .fa {
        font-size:2.2rem;
        opacity:1;
    }
    #discount_week .category_choose_left:hover,#discount_week .category_choose_right:hover {
        background-color: #1ca28a!important;
    }
    #discount_week .category_choose_left:hover .fa-chevron-left,#discount_week .category_choose_right:hover .fa-chevron-right{
        color:#fff!important;
    }
   
    #discount_week .category_choose_right {
        position: absolute;
        top: 36%;
        right: 0;
        padding: 80px 20px;
        background-color: rgba(28,162,138,.3);
        -webkit-box-shadow: 3px 1px 3px 0px rgba(0, 0, 0, 0.2);
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
        cursor:pointer;
        z-index:100;
    }
    #discount_week .pager-link-block{
        display: inline-block;
    }
    #discount_week .pager-link-block span{
        text-indent: -9999px;
        display: block;
        width: 10px;
        height: 10px;
        margin: 0 5px;
        outline: 0;
        border-radius: 5px;
        background: #1ca28a;
        cursor:pointer;
        opacity: 0.5;
    }

    #discount_week .pager-link-block.active span{
        background: #1ca28a;
        opacity: 1;
    }
    #discount_week .category-title{
        float:left;
    }
    #discount_week .title-section .category-title a {
        padding:0px !important;
        margin:0px !important;
    }

    #discount_week .ranking-product{
        list-style-type: none;
    }
     #discount_week .ranking-product-block .manufacturer-block{
         height:22px;
     }
    #discount_week .ranking-product .photo-box .zoom-image {
        height:190px;
        display:block;
        font-size:0px;
    }
    #discount_week .ranking-product .photo-box .zoom-container img{
        max-height:291px;
        display:inline-block;
        vertical-align: middle;
    }
    @media(max-width:450px){
        #discount_week .ranking-product .photo-box .zoom-image {
            height:200px;
        }
        #discount_week .ranking-product .photo-box .zoom-container img {
            max-height:200px;
        }
    }
    #discount_week .ranking-product .ranking-product-name{
        height:45px !important;
    }
    #discount_week .ranking-product-name span{
        width:100%;
        font-size:1rem;
    }
    #discount_week  .manufacturer-block span{
        font-size: 0.85rem!important;
    }
    #discount_week .ranking-product  .ranking-price{
        font-size: 1rem;
    }
    #discount_week .ranking-btn{
        color: #fff;
        background-color: #1ca28a;
        border-color: #1ca28a;
        margin-bottom: 10px;
    }
</style>

<div class="discount-container" id="discount_week">
    <div class="section clearfix">
        <div id="item1" class="ct-sale-page form-group clearfix">
            <ul class="title-section clearfix row nav nav-tabs">
                @foreach($ranking_categories as $key => $category)
                    <li class="clearfix col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12 category-title {{$loop->iteration == 1 ? 'active' : ''}} " data-root="{{$key}}" data-sub="{{json_encode($category['sub']->pluck('name', 'url_path')->toArray())}}">
                        <a data-toggle="tab" href="#{{$key}}">
                            <div class=" {{$loop->iteration != 1 ? 'category_choose' : 'active'}} title  text-center width-full" style="padding:4px;">
                                <h2 class="font-bold size-12rem">{{ $category['title'] }}</h2>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="content-box-sale tab-content">
                @foreach($ranking_categories as $key => $categories)
                    <div id="{{$key}}" class="tab-pane fade {{$loop->iteration == 1 ? 'in active' : ''}}">
                        <div id="carousel-custom-nav_{{$key}}">
                        </div>

                        {{--<div class="category_choose_left"><div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div></div>--}}
                        {{--<div class="category_choose_right"><div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div></div>--}}
                        <div class="category-block text-center font-bold">
                            <div class="category-name">
                                @foreach($categories['sub'] as $category)
                                    <h2 class="url_path_title" data-url_path="{{$category->url_path}}" {{$loop->iteration != 1 ? 'style=display:none;' : ''}}>{{$category->name}}</h2>
                                @endforeach
                            </div>
                            <div id="carousel-custom-dots_{{$key}}"></div>
                        </div>
                        <div id="owl-carousel-ranking_{{$key}}">
                            @foreach($categories['sub'] as $category)
                                <ul class="product-sale" data-url_path="{{$category->url_path}}">
                                    <div class="loading_div">
                                        @include('response.common.loading.md')
                                    </div>
                                </ul>
                            @endforeach
                        </div>
                        <div class="category-btn-block">
                            <div class="category-btn text-center">
                                @foreach($categories['sub'] as $category)
                                     <a class=" url_route btn ranking-btn" href="{{URL::route('ranking',['section' => 'ca/'.$category->url_path]).'?sort=year&rel=2018-01-2018Spring'}}"  data-url_path="{{$category->url_path}}" {{$loop->iteration != 1 ? 'style=display:none;' : ''}} target="_blank">
                                            查看全部
                                        </a>
                                @endforeach
                            </div>
                        </div>  
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script>
    @foreach($ranking_categories as $key => $category)
        $('#owl-carousel-ranking_{{$key}}').addClass('owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            margin:1,
            slideBy : 1,
            dotsContainer: '#carousel-custom-dots_{{$key}}',
            dotClass: 'pager-link-block',
            navContainer:'#carousel-custom-nav_{{$key}}',
            navClass : ['category_choose_left','category_choose_right'],
            navSpeed:1000,
            dotsSpeed:1000,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        }).on('translate.owl.carousel', function(event) {

            //Title change
            _this = $(this);
            index = event.relatedTarget.current() + 1;
            orig_index = index;

            var tempFix = index - (event.relatedTarget.clones().length / 2);
            if(tempFix > 0) {
                index = tempFix;
            }else{
                index = _this.prev('.category-block').find('.category-name h2').length;
            }

            title = _this.prev('.category-block').find('.category-name h2:nth-child('+ index +')');

            $('.url_path_title:visible').fadeOut( 500, function() {
                title.fadeIn(500)
            });


            btn = _this.next('.category-btn-block').find('.category-btn a:nth-child('+ index +')');

            $('.url_route:visible').fadeOut( 500, function() {
                btn.fadeIn(500)
            });
            
            //Loading ajax
            item = _this.find('.owl-item:nth-child('+ orig_index +')');
            ajax_loading_ranking_content(item,"{{$key}}" ,title.data('url_path'));
        });


        $('#carousel-custom-nav_{{$key}}').find('.category_choose_left').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
        $('#carousel-custom-nav_{{$key}}').find('.category_choose_right').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
    @endforeach
    item = $('#owl-carousel-ranking_1000 .owl-item.active');
    ajax_loading_ranking_content(item,'1000','1000-1001');
    var currentRequest = null;
    function ajax_loading_ranking_content(item,root,value){

        check = item.find('div.loading_div');
        if(check.length) {
            if(currentRequest != null) {
                currentRequest.abort();
            }
            currentRequest = $.ajax({
                url: "{{route('benefit-sale-2018-promotion-specialDiscount-ranking')}}",
                type: "POST",
                data: {root: root, value: value},
                success: function (result) {
                    result = JSON.parse(result);
                    check.replaceWith(result.html);
                }, error: function () {
                    console.log("ajax fail");
                }
            });
        }
    }

    $( document ).ready(function() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target) // activated tab
            parent = target.closest('li');
            root = parent.data('root');
            sub = parent.data('sub');

            sub_url_path = Object.keys(sub)[0];
            if(Object.keys(sub)[0] == 215){
                sub_url_path = 6371;
            }
            item = $('#owl-carousel-ranking_'+root).find('.owl-item').first();

            ajax_loading_ranking_content(item,root,sub_url_path);
        });
    });

</script>
