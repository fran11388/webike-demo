@extends('response.layouts.1column-bigpromo')
@section('style')
	<style>
		@media (min-width: 768px) {
			.navbar-nav {
				width:100% !important;
			}
			.navbar-nav li {
				width:9%;
				text-align: center;
				font-size:1rem;
			}
			.promotion-title {
				background: url({{assetRemote('image/benefit/big-promotion/2018_banner_head.png')}}) repeat-x #fc37a2;
				padding-top:100px;
			}
		}
		.rider {
			background: url({{assetRemote('image/benefit/big-promotion/rider.png')}}) no-repeat;
			min-height: 110px;
			background-size: 87%;
			margin-left:auto;
			margin-right:auto;
		}
		.rider:hover {
			background: url({{assetRemote('image/benefit/big-promotion/rider.png')}}) no-repeat;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/celebration/big-promotion/home.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/benefit/sale/2018-spring.css') }}">
@stop
@section('middle')
{{-- <div class="promotion-title"></div> --}}
@include('response.pages.benefit.sale.project.2018.spring.otherblock.main-banner')

@include('response.pages.benefit.sale.project.2018.spring.otherblock.main-category-link')
<div class="container ">
	@if($type == '2017-spring')
		<div id="recommend" class="section clearfix">
			<div id="item1" class="ct-sale-page form-group" style="border: 1px solid #b7b7b7;">
				<div class="title col-xs-12 col-sm-12 col-md-12 text-center">
					<h2>每日推薦商品</h2>
				</div>
				<div class="content-box-sale">
					<ul class="product-sale">
						@foreach($recommends as $recommend_product)
							<li class="item item-product-grid thumb-box-border col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12   ">
								<a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
									<figure class="zoom-image">
										<img src="{{$recommend_product->image}}" alt="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}">
									</figure>
								</a>
								<a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" class="title-product-item" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
									<span class="bold-text dotted-text2 force-limit">{{$recommend_product->name}}</span>
									<span class="normal-text dotted-text1">{{$recommend_product->manufacturer_name}}</span>
								</a>
								@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
									<label class="price font-bold">NT${{number_format($recommend_product->price,0, '.' ,',')}} (最大{{number_format($recommend_product->d_price)}}%off)</label>
								@else
									<label class="price font-bold">NT${{number_format($recommend_product->price,0, '.' ,',')}}(<?php if($recommend_product->point_type == 0){echo ' 最大'.number_format($recommend_product->c_price).'%off)';}else{ echo number_format($recommend_product->c_point).'倍點數現折)';}?></label>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	@endif
	@if($type == '2017-summer')
		@if(count($recommends))
			<div id="recommend" class="section clearfix">
				<div id="item1" class="ct-sale-page form-group">
					<div class="title col-xs-12 col-sm-12 col-md-12 text-center">
						<h2>每日推薦商品</h2>
					</div>
		{{-- 				<div class="row box-content">
					    <div class="title-main-box-clear">
					        <h2>
					            <span>{!! $category->name !!}推薦商品　</span>
					            <a href="{!! URL::route('summary', 'ca/' . $category->mptt->url_path) !!}" title="{!! $category->name . '全部商品' !!}">>> 查看全部</a>
					        </h2>
					    </div> --}}
				    <div class="box-content">
				        <div class="ct-riding-gear col-xs-12 col-sm-12 col-md-12 col-lg-12">
				            <ul class="product-list owl-carousel-promo">
				                @foreach($recommends[0] as $recommend_product)
				                <li>
				                    <a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank" class="recommendImage">
										<figure class="zoom-image recommendZoom">
											<img src="{{$recommend_product->image}}" alt="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}">
											<span class="helper"></span>
										</figure>
									</a>
									<a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" class="title-product-item" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
										<span class="bold-text dotted-text2 force-limit box">{{$recommend_product->name}}</span>
									</a>
									@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
										<span class="size-10rem">{{$recommend_product->manufacturer_name}}</span>
										<label class="price font-bold box width-full">NT${{number_format($recommend_product->d_money,0, '.' ,',')}}(最大{{number_format($recommend_product->d_price)}}%off)</label>
									@else
										<span class="size-10rem">{{$recommend_product->manufacturer_name}}</span>
										<label class="price font-bold box width-full">NT${{number_format($recommend_product->c_money,0, '.' ,',')}}(<?php if($recommend_product->point_type == 0){echo '最大'.number_format($recommend_product->c_price).'%off)';}else{ echo number_format($recommend_product->c_point).'倍點數現折)';}?></label>
									@endif
									@if(count($recommend_product->motor))
										<span class="size-10rem">{{$recommend_product->motor}}</span>
									@endif
				                </li>
				                @endforeach
				            </ul>
				        </div>
					</div>
				</div>
			</div>
		@endif
	@endif
	{{--@foreach($categories as $category_title => $info)--}}
		{{--<div class="section clearfix categories">--}}
			{{--<div id="item1" class="ct-sale-page form-group">--}}
		        {{--<div class="title col-xs-12 col-sm-12 col-md-12 text-center">--}}
		            {{--<h2 class="font-bold size-15rem">{{$category_title}}</h2>--}}
		        {{--</div>--}}
		        {{--<div class="content-box-sale">--}}
		            {{--<ul class="product-sale">--}}
		            	{{--@foreach($info as $title => $link)--}}
							{{--@php--}}
                                {{--$ca_rewrite = $link['url_rewrite'];--}}
                                {{--$discount = $categories_discount->filter(function($discount) use($ca_rewrite){--}}
                                    {{--return ($discount->url_rewrite == $ca_rewrite);--}}
                                {{--})->first();--}}
							{{--@endphp--}}
			                {{--<li class="li-product-sale col-xs-12 col-sm-3 col-md-3">--}}
			                	{{--<span class=" dotted-text1 font-bold size-12rem text-center">{{$title}}</span>--}}
			                    {{--<a href="{{$link['url']}}?ca={{$link['url_rewrite'].'&'.$rel_parameter}}" class="clearfix">--}}
			                        {{--<div class="photo-box">--}}
			                            {{--<ul class="clearfix">--}}
			                                {{--<li class="col-xs-12 col-sm-12 col-md-12 text-center"><figure class="zoom-image col-sm-block col-xs-block"><img src="{{$link['img']}}" alt=""></figure></li>--}}
			                            {{--</ul>--}}
			                        {{--</div>--}}
									{{--@if($discount)--}}
										{{--@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))--}}
											{{--<label class="dotted-text1 discount">{{'最大'.number_format($discount->discountd).'%off'}}</label>--}}
										{{--@else--}}
											{{--<label class="dotted-text1 discount">{{'最大'.number_format($discount->discountc).'%off'}}</label>--}}
										{{--@endif--}}
									{{--@else--}}
										{{--<label class="dotted-text1 discount">大特價</label>--}}
									{{--@endif--}}
			                        {{-- <span class=" dotted-text1 font-color-red text-center">點數加倍</span> --}}
			                    {{--</a>--}}
			                {{--</li>--}}
			            {{--@endforeach--}}
		            {{--</ul>--}}
		        {{--</div>--}}
		    {{--</div>--}}
	    {{--</div>--}}
	{{--@endforeach--}}

	{{--@foreach($categories as $key => $data)--}}
		{{--@if((strtotime($data[0]) <= strtotime($date) and strtotime($data[1]) >= strtotime($date)))--}}
			{{--<div class="width-full">--}}
				{{--<a href="{{\URL::route('parts',['section' => 'ca/'.$data[2]]).'?'.$rel_parameter}}" title="{{ $data[3] }}【點數5倍送】" target="_blank">--}}
					{{--<figure class="zoom-image">--}}
						{{--<img src="{{assetRemote('image\benefit\big-promotion\weeksale370.png')}}" alt="{{ $data[3] }}【點數5倍送】">--}}
					{{--</figure>--}}
				{{--</a>--}}
			{{--</div>--}}
		{{--@endif--}}
	{{--@endforeach--}}


	{{-- @include('response.pages.benefit.sale.project.2018.spring.otherblock.timelimit') --}}
	@include('response.pages.benefit.sale.project.2018.spring.otherblock.special_price')

	@include('response.pages.benefit.sale.project.2018.spring.otherblock.summer_ride')

	@include('response.pages.benefit.sale.project.2018.spring.otherblock.country_discount')

	{{-- @include('response.pages.benefit.sale.project.2018.spring.otherblock.new_spring') --}}

    @include('response.pages.benefit.sale.project.2018.spring.otherblock.activities')

	@include('response.pages.benefit.sale.project.2018.spring.otherblock.otherpages')

	@include('response.pages.benefit.sale.project.2018.spring.otherblock.promotion-category')

</div>
	@include('response.layouts.partials.footer.shopping')

@stop
@section('script')
	<script type="text/javascript" src="{{ assetRemote('plugin/FlexSlider-master/jquery.flexslider-min.js') }}"></script>
	<script>
		
		$(document).ready(function(){
			$('.owl-carousel-promo').addClass('owl-carousel').owlCarousel({
				loop:true,
				nav:true,
				margin:5,
				slideBy : 6,
				URLhashListener:true,
				startPosition: 'URLHash',
				responsive:{
					0:{
						items:2
					},
					600:{
						items:3
					},
					1000:{
						items:5
					}
				}
			});

			$('.owl-carousel-promo').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>')
			$('.owl-carousel-promo').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>')
		});
        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 185;
            $('html,body').animate({scrollTop: y}, 400);
        }

//		$('.discount_title').each(function(){
//		    var date = new Date();
//		    var discount_date = $(this).find('span').text().split('#');
//            var begin_date = new Date(discount_date[0]);
//            var end_date = new Date(discount_date[1]);
//            if(begin_date <= date && date <= end_date){
//                $(this).addClass('active');
//			}
//		});
	</script>
@stop