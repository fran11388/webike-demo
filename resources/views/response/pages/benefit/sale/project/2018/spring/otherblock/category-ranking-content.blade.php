@foreach($ranking_products as $product)
    <li class="ranking-product col-xs-12 col-sm-6 col-md-3">
        <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
            <div class="photo-box text-center box">
                <div class="zoom-container text-center">
                    <figure class="zoom-image">
                        <img src="{{\Thumbor\Url\Builder::construct(config('phumbor.server'), config('phumbor.secret'), unicodeConvertForSpace($product->getImage()))->fitIn(190, 190)->build() }}" alt="">
                        {{-- <img src="{{ $product->getImage() }}"> --}}
                        <span class="helper"></span></figure>
                </div>
            </div>
            <div class="ranking-product-name  dotted-text2">
                <span>{{$product->full_name}}</span>
            </div>
        </a>
        <div class="manufacturer-block dotted-text2">
            <span class="manufacturer box font-bold">{{ $product->manufacturer->name }}</span>
        </div>
        <?php
        $final_price = $product->getFinalPrice($current_customer);
        $final_point = $product->getFinalPoint($current_customer);
        $discount_text = '';
        $discount = round($final_price / $product->price ,2) * 100;
        if($discount < 90){
            $discount_text = '('. $discount . '%)';
        }
        ?>
        <span class=" dotted-text1 font-color-red ranking-price">NT$ {{ number_format($final_price) }}{{$discount_text}}</span>
    </li>
@endforeach