<style>
.main_categories li{
	padding:0px;
	display:inline-block;
	width: 16.6666%;
	float:left;
	border-bottom: 1px #c7b299 solid;
}
.main_categories li img{
	max-width:100%;
}
.main_categories a div {
	min-height: 90px;
	background-size: 97% !important;
	margin-left:auto;
	margin-right:auto;
}

.main_categories a div:hover {
	min-height: 90px;
	background-size: 97% !important;
	margin-left:auto;
	margin-right:auto;
}
.main_categories_fixed li{
	padding:0 0 5px 0;
	display:inline-block;
	width: 16.6%;
	float:left;
}
.main_categories_fixed li img{
	max-width:100%;
}
.main_categories_fixed div {
	min-height: 65px;
	background-size: 90% !important;
	margin-left:auto;
	margin-right:auto;
}
.main_categories_fixed div:hover {
	min-height: 65px;
	background-size: 90% !important;
	margin-left:auto;
	margin-right:auto;
}
.home {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_01.jpg')}}) no-repeat #b92229;
	background-position: center
}
.custom {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_02.jpg')}}) no-repeat #544741;
	background-position: center
}
.rider {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_03.jpg')}}) no-repeat #544741;
	background-position: center
}
.travel {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_04.jpg')}}) no-repeat #544741;
	background-position: center
}
.fixer {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_05.jpg')}}) no-repeat #544741;
	background-position: center
}
.genuine {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_06.jpg')}}) no-repeat #544741;
	background-position: center
}
.special {
	background: url({{assetRemote('image/benefit/big-promotion/special1.png')}}) no-repeat;
	background-position: center
}
.home1 {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_01-1.jpg')}}) no-repeat  #b92229;
	background-position: center
}
.home:hover,.home.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_01-1.jpg')}}) no-repeat  #b92229;
	background-position: center
}
.special:hover,.special.hover {
	background: url({{assetRemote('image/benefit/big-promotion/special2.png')}}) no-repeat #544741;
	background-position: center
}
.custom:hover,.custom.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_02-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.rider:hover,.rider.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_03-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.travel:hover,.travel.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_04-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.fixer:hover,.fixer.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_05-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.genuine:hover,.genuine.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/button_06-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.home_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_01.jpg')}}) no-repeat #b92229;
	background-position: center
}
.special_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/special_fixed.png')}}) no-repeat  #544741;
	background-position: center
}

.custom_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_02.jpg')}}) no-repeat #544741;
	background-position: center
}
.rider_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_03.jpg')}}) no-repeat #544741;
	background-position: center
}
.travel_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_04.jpg')}}) no-repeat #544741;
	background-position: center
}
.fixer_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_05.jpg')}}) no-repeat #544741;
	background-position: center
}
.genuine_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_06.jpg')}}) no-repeat #544741;
	background-position: center
}
.home_fixed1 {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_01-1.jpg')}}) no-repeat  #b92229;
	background-position: center
}
.home_fixed:hover,.home_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_01-1.jpg')}}) no-repeat  #b92229;
	background-position: center
}
.special_fixed:hover,.special_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/special_fixed1.png')}}) no-repeat #544741;
	background-position: center
}
.custom_fixed:hover,.custom_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_02-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.rider_fixed:hover,.rider_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_03-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.travel_fixed:hover,.travel_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_04-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.fixer_fixed:hover,.fixer_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_05-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.genuine_fixed:hover,.genuine_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/2018/fall/button/fixed/button_06-1.jpg')}}) no-repeat  #544741;
	background-position: center
}
.main-container {
	background-color: #c7b299;
}
.link-container {
	background-color: #c7b299;
    margin-left: auto;
    margin-right: auto;
    max-width: 1000px;
    margin-top: 10px;
}
@media(min-width:768px ){
	 .main-container-home .link-container .col-md-2{
	 	width: 20%;
	 }
}
@media (max-width: 992px) {
	.main_categories a div {
		min-height: 110px;
		background-size: 57%;
		margin-left:auto;
		margin-right:auto;
	}
	.main_categories a div:hover {
		min-height: 110px;
		background-size: 57%;
		margin-left:auto;
		margin-right:auto;
	}

	.main_categories li {
		width:16.6666%;
	}
	.main_categories_fixed li{
		width:16.6666%;
	}
}
@media (max-width: 770px) {
	.main_categories a div {
		min-height: 110px;
		background-size: 200px;
		width: 200px;
    	margin-left:auto;
    	margin-right:auto;
	}
	.main_categories a div:hover {
		min-height: 110px;
		background-size: 200px;
		margin-left:auto;
		margin-right:auto;
	}
	.fixed-container {

		display:none !important;
	}

	.main-container-home {
		background-color: #c7b299 !important;
	}
}
.main_categories_fixed {
    position: fixed;
    top: 61px;
    z-index: 2;
	padding-right:15px;
	padding-left:15px;
    max-width:1000px;
    width:100%;
    right: 0;
    left: 0;
    margin-right: auto;
    margin-left: auto;
}
.main_categories {
	margin-left:auto;
    margin-right:auto;
	max-width:1000px;
	padding:0 15px;
}
.fixed-container {
	display:none;
	width:100%;
	position: fixed;
    top: 61px;
    z-index: 100;
    min-height: 68px;
    border-bottom: 3px #9e9e9e87  solid;
	
}
.fixed-container .fixed-block{
	background-color: #c7b299;
	min-height: 65px;
}
.main-container .active {
	background-color: #b92229;
}
.mobile-active {
	background-color: #d4d4d4;
	border-color: #8c8c8c;
}
.main-container .mobile {
	display: block !important;
    padding: 10px !important;
    height:auto !important;
}
.main-container .select2-container .select2-selection--single {
    height: 40px;
}
.main-container .select2-container .select2-selection--single .select2-selection__rendered {
	line-height: 40px !important;
}
.main-container .mobile-link {
	position:relative;
	width:95%;
}
.main-container .mobile-link .loading-box{
	position:absolute;
	right:11px;
	top:0px;
	display:none;
}

.link-container .block-link{
	border: 1px solid #544741;
	margin-bottom: 10px;
}

</style>
<div class="main-container text-center {{\Request::route()->getName() == 'benefit-sale-2018-promotion' ? 'main-container-home' : ''}}">
	<div class="main_categories">
		<ul class="clearfix text-center">
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2018-promotion').'?'.$rel_parameter}}">
					<div class="home text-center">
					</div>
				</a>
			</li>
			{{--<li class="category-link text-center hidden-xs">--}}
				{{--<a href="{{URL::route('benefit-sale-2018-promotion-specialDiscount').'?'.$rel_parameter}}">--}}
					{{--<div class="special">--}}
					{{--</div>--}}
				{{--</a>--}}
			{{--</li>--}}
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2018-promotion-category',['category' => '1000']).'?'.$rel_parameter}}">
					<div class="custom">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2018-promotion-category',['category' => '3000']).'?'.$rel_parameter}}">
					<div class="rider text-center">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2018-promotion-category',['category' => '3260']).'?'.$rel_parameter}}">
					<div class="travel">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2018-promotion-domestic').'?'.$rel_parameter}}">
					<div class="fixer">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('genuineparts').'?'.$rel_parameter}}" target="_blank">
					<div class="genuine">
					</div>
				</a>
			</li>

			<li class="visible-xs btn-gap-right btn-gap-left content-last-block block mobile-link">
				
					<select class="select2 select-category" name="my_choice[]">
						<option value="{{URL::route('benefit-sale-2018-promotion').'?'.$rel_parameter}}">活動首頁</option>
	                    <option value="{{URL::route('benefit-sale-2018-promotion-category',['category' => '1000']).'?'.$rel_parameter}}">改裝零件</option>
	                    <option value="{{URL::route('benefit-sale-2018-promotion-category',['category' => '3000']).'?'.$rel_parameter}}">騎士用品</option>
	                    <option value="{{URL::route('benefit-sale-2018-promotion-category',['category' => '3260']).'?'.$rel_parameter}}">旅行用品</option>
	                    <option value="{{URL::route('benefit-sale-2018-promotion-domestic').'?'.$rel_parameter}}">保養維修及工具</option>
	                    <option value="{{URL::route('genuineparts').'?'.$rel_parameter}}">正廠零件5%off</option>
	                </select>
	                @include('response.common.loading.xs')
			</li>
		</ul>
	</div>
	<div class="link-container">
		<ul class="clearfix">
			{{-- <li class="col-sm-2 col-xs-6 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#time-limit')">每週分類點數現折</a></div></li> --}}
			<li class="col-sm-2 col-xs-6 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#summer_ride')">2019秋冬新品 點數5倍</a></div></li>
			<li class="col-sm-2 col-xs-6 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#country_discount')">周年慶品牌現折</a></div></li>
			<li class="col-sm-2 col-xs-6 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#game')">GUESS 每日任務</a></div></li>
			<li class="col-sm-2 col-xs-6 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#more_benefit')">更多會員好康</a></div></li>
			<li class="col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#category')">特輯情報</a></div></li>
		</ul>
	</div>
</div>

<div class="fixed-container">
	<div class="fixed-block">
		<div class="main_categories_fixed">
			<ul class="clearfix">
				<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
					<a href="{{URL::route('benefit-sale-2018-promotion').'?'.$rel_parameter}}">
						<div class="home_fixed text-center">
						</div>
					</a>
				</li>
				{{--<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">--}}
					{{--<a href="{{URL::route('benefit-sale-2018-promotion-specialDiscount').'?'.$rel_parameter}}">--}}
						{{--<div class="special_fixed text-center">--}}
						{{--</div>--}}
					{{--</a>--}}
				{{--</li>--}}
				<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
					<a href="{{URL::route('benefit-sale-2018-promotion-category',['category' => '1000']).'?'.$rel_parameter}}">
						<div class="custom_fixed">
						</div>
					</a>
				</li>
				<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
					<a href="{{URL::route('benefit-sale-2018-promotion-category',['category' => '3000']).'?'.$rel_parameter}}">
						<div class="rider_fixed text-center">
						</div>
					</a>
				</li>
				<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
					<a href="{{URL::route('benefit-sale-2018-promotion-category',['category' => '3260']).'?'.$rel_parameter}}">
						<div class="travel_fixed">
						</div>
					</a>
				</li>
				<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
					<a href="{{URL::route('benefit-sale-2018-promotion-domestic').'?'.$rel_parameter}}">
						<div class="fixer_fixed">
						</div>
					</a>
				</li>
				<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
					<a href="{{URL::route('genuineparts').'?'.$rel_parameter}}" target="_blank">
						<div class="genuine_fixed">
						</div>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<script>
    $(window).scroll(function(){

        if ($(this).scrollTop() > 570) {
          $('.fixed-container').show();
        } else {
          $('.fixed-container').hide();
        }
    });

    $(document).ready(function(){
    	var route = "{{\Request::route()->getName()}}";
    	if(route == 'benefit-sale-2018-promotion'){
    		$('.home').addClass('home1');
    		$('.home1').removeClass('home');
    		$('.home1').addClass('active');
    		$('.home_fixed').addClass('home_fixed1');
    		$('.home_fixed1').removeClass('home_fixed');
    		$('.home_fixed1').addClass('active');
    	}else if(route == 'benefit-sale-2018-promotion-category'){
    		var route = "{{\Request::segment(6)}}";
    		if(route == 3000){
    			$('.rider').toggleClass("hover");
    			$('.rider_fixed').toggleClass("hover");
    		}else if(route == 1000){
    			$('.custom').toggleClass("hover");
    			$('.custom_fixed').toggleClass("hover");
    		}else{
    			$('.travel').toggleClass("hover");
    			$('.travel_fixed').toggleClass("hover");
    		}
    	}else if(route == 'benefit-sale-2018-promotion-domestic'){
    		$('.fixer').toggleClass("hover");
    		$('.fixer_fixed').toggleClass("hover");
    	}else{
    		$('.special').toggleClass("hover");
    		$('.special_fixed').toggleClass("hover");
    	}

    	var url = window.location.href;
    	$('.select-category option').each(function(key,element){
    		if($(this).val() == url){
    			$(this).attr('selected', true);
    		}
	    });
    });

    $(document).on('change', ".main-container .select-category", function () {
        var value = $(this).find('option:selected').val();
        location.href = value;
        $('.main_categories .loading-box').show();
    });

</script>