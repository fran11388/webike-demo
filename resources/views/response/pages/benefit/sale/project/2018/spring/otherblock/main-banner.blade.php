<style>
	.banner-container{
		background-color: #C7B299
}
</style>
<div class="banner-container">
	<div class="container">
	    <div class="banner text-center">
	        <a href="{{\URL::route('benefit-sale-2018-promotion')}}">
	            <img class="hidden-xs" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/1200x370-01.jpg')}}">
	            <img class="hidden-md hidden-lg" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/1200x628.jpg')}}">
	        </a>
	    </div>
	</div>
</div>