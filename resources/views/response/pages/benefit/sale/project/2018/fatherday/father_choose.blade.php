@php
	$dad_categorys = [
		array('id'=>'4','name'=>'排氣系統','link'=>'1000-1001','img'=>'catagorys/1001.jpg'),
		array('id'=>'79','name'=>'後貨架','link'=>'1000-1110-1121','img'=>'catagorys/1121.jpg'),
		array('id'=>'45','name'=>'懸吊・避震器','link'=>'1000-1311-1080','img'=>'catagorys/1080.jpg'),
		array('id'=>'59','name'=>'坐墊相關零件','link'=>'1000-1110-1101','img'=>'catagorys/1101.jpg'),
		array('id'=>'1120','name'=>'腳踏後移套件','link'=>'1000-1311-1050-6340','img'=>'catagorys/6340.jpg'),
		array('id'=>'67','name'=>'頭罩・檔風鏡','link'=>'1000-1110-1109','img'=>'catagorys/1109.jpg'),
		array('id'=>'60','name'=>'後扶手・後靠背','link'=>'1000-1110-1102','img'=>'catagorys/1102.jpg')
	];
	$kid_catagorys = [
		array('id'=>'277','name'=>'安全帽','link'=>'3000-3001','img'=>'catagorys/3001.jpg'),
		array('id'=>'289','name'=>'夾克・防摔衣','link'=>'3000-3020-3021','img'=>'catagorys/3021.jpg'),
		array('id'=>'305','name'=>'騎士手套','link'=>'3000-3020-3060','img'=>'catagorys/3060.jpg'),
		array('id'=>'1545','name'=>'褲子・防摔褲','link'=>'3000-3020-3071','img'=>'catagorys/3070.jpg'),
		array('id'=>'321','name'=>'包包','link'=>'3000-3020-3123','img'=>'catagorys/3123.jpg'),
		array('id'=>'300','name'=>'T恤','link'=>'3000-3020-3034','img'=>'catagorys/3034.jpg'),
		array('id'=>'296','name'=>'帽子','link'=>'3000-3020-3028','img'=>'catagorys/3028.jpg'),
		array('id'=>'1581','name'=>'車身用包包','link'=>'3000-3260-1331','img'=>'catagorys/1331.jpg'),
		array('id'=>'1577','name'=>'摩托車相關精品','link'=>'3000-1327','img'=>'catagorys/1327.jpg')
	]
@endphp
<div class="fatherday-productguide-block" id="father-choose">
		<div class="fatherday-title">
			<h1>爸爸節選物指南</h1>
		</div>
		<div class="fatherday-productguide-img-container clearfix">
			<div  class="fatherday-productguide-img-dad zoom-image">
				<a href="#father-productguide"><img class="productguide-img-dad hidden-xs" src="{{ assetRemote('/image/promotion/2018fatherday/leftdad.jpg') }}"></a>
				<a href="#father-productguide-mobile"><img class="productguide-img-dad-mobile hidden-md hidden-lg" src="{{ assetRemote('/image/promotion/2018fatherday/1725x370_left.jpg') }}"></a>
			</div>
			<div class="fatherday-productguide-img-kid zoom-image">
				<a href="#father-productguide"><img class="productguide-img-kid hidden-xs" src="{{ assetRemote('/image/promotion/2018fatherday/rightkid.jpg') }}"></a>
				<a href="#father-productguide-mobile"><img class="productguide-img-kid-mobile hidden-md hidden-lg" src="{{ assetRemote('/image/promotion/2018fatherday/1725x370_right.jpg') }}"></a>
			</div>
		</div>
		<div class="fatherday-productguide-ca-container" id="ca-choose">
			<div class="fade-dad show">				
				<ul class="father-productguide-dad-ca owl-carousel-ca">
					@foreach ($dad_categorys as $dad_category)
						<li id="{{$dad_category['id']}}" class="ca-list ca-id" data-content="{{$dad_category['link']}}">
							<div class="ca-img">
								<a href="https://www.webike.tw/collection/brand/fatherday-dad?sort=visits&limit=40&order=asc&ca={{$dad_category['link']}}" target="_blank"><img class="father-productguide-dad-ca-img" src="{{ assetRemote('/image/promotion/2018fatherday/'.$dad_category['img']) }}"></a>
							</div>
							<div class="ca-title">
								<span class="size-12rem">{{$dad_category['name']}}</span>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
			<div class="fade-kid hidden">
				<ul class="father-productguide-kid-ca owl-carousel-ca ">
					@foreach ($kid_catagorys as $kid_catagory)
						<li id="{{$kid_catagory['id']}}" class="ca-list ca-id" data-content="{{$kid_catagory['link']}}">
							<div class="ca-img">
								<a href="https://www.webike.tw/collection/brand/fatherday-kid?sort=visits&limit=40&order=asc&ca={{$kid_catagory['link']}}" target="_blank"><img class="father-productguide-dad-ca-img" src="{{ assetRemote('/image/promotion/2018fatherday/'.$kid_catagory['img']) }}"></a>
							</div>
							<div class="ca-title">
								<span class="size-12rem">{{$kid_catagory['name']}}</span>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	@include('response.common.loading.md')		
	@include('response.pages.benefit.sale.project.2018.fatherday.father_choose_product')
</div>

		