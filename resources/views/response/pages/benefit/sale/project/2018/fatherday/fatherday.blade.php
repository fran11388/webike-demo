@extends('response.layouts.1column')
@section('style')
<style>
	
.fatherday-banner{

	border: 1px solid #036EB8;
	margin-bottom: 20px;
}

.fatherday-title{

	padding: 10px;
	background-color: #000;
	text-align: center;
}

.fatherday-block{

	border: 1px solid #036EB8;
	margin-bottom: 20px;
}

.fatherday-productguide-block{

	border: 1px solid #036EB8;
	margin-bottom: 20px;
}

.fatherday-title h1{

	color: #fff;
	font-weight: normal;
}

.fatherday-shoppingfree-img-container{

	padding: 20px;
	border-bottom: 1px solid #036EB8;
	background-color: #036EB8;
}

.fatherday-shoppingfree-content{

	padding: 20px;
}

.fatherday-shoppingfree-content-mobile{

	padding: 20px;
}

.fatherday-shoppingfree-content-mobile li{

	list-style: none;
	padding: 0px;
	margin: 0px;
	font-size: 1rem !important;
	line-height: 1.5rem;
	margin-bottom: 5px; 
}

.fatherday-shoppingfree-content p{

	font-size: 1rem !important;
	line-height: 1.5rem;
	margin-bottom: 5px; 
}

.fatherday-productguide-img-container{

	padding: 20px;
	background-color: #036EB8;
}

.fatherday-productguide-img-dad{

	padding: 0px;
	cursor: pointer;
}

.productguide-img-dad-mobile{

	opacity: 1 !important;
}

.fatherday-productguide-img-kid{

	padding: 0px;
	cursor: pointer;
}

.productguide-img-kid-mobile{

	opacity: 1 !important;
}


.fatherday-benefit-block{

	border: 1px solid #036EB8;
	margin-bottom: 100px;
}

.fatherday-title h1{

	color: #fff;
	font-weight: normal;
}

.fatherday-benefit-content{

	padding: 20px;
}

.fatherday-benefit{

	padding: 0px 25px;
}

.fatherday-benefit-txt{

	padding-top: 10px;
}


.fatherday-benefit-txt span{

	line-height: 1.5rem;
}

.fatherday-benefit-content-moblie{

	padding: 20px;
}

.fatherday-benefit-moblie{

	padding: 0px 0px 20px 0px;
}

.fatherday-benefit-txt-moblie{

	padding-top: 10px;

}

.fatherday-benefit-txt-moblie span{

	line-height: 1.5rem;
}

.fatherday-productguide-ca-container{

	padding: 20px 0px 20px 0px;
	border-bottom: 1px solid #036EB8;

}

.father-productguide-dad-ca{

	list-style: none;
	padding: 0px;
	margin: 0px;
}

.father-productguide-kid-ca{

	list-style: none;
	padding: 0px;
	margin: 0px;
}

.owl-stage .ca-list{

	opacity: 0.5;
}


.owl-stage .center .ca-list{

	opacity: 1 !important; 
}

.ca-img{

	width: 70%;	
	margin: 0px auto;

}

.ca-title{

	width: 70%;
	text-align: center;
	padding-top: 10px; 
	margin:0px auto;
}

.ca-title span{

	font-weight: bold;
}

.fatherday-productguide-product-container{
	
	padding: 20px 0px 20px 0px;

}

.father-productguide-product{

	list-style: none;
}

.owl-next{
	background-color: #036EB8;
    color: #fff;
}

.owl-prev{
	background-color: #036EB8;
    color: #fff;
}

.owl-next:hover{

	background-color: #036EB8;
}

.owl-prev:hover{

   	background-color: #036EB8;
}

.dad-button-block{

	margin: 20px 0px 0px 0px;
}

.kid-button-block{

	margin: 20px 0px 0px 0px;
}

.dad-button{

	background-color: #036EB8 !important;
	border: none !important;
	color: #fff !important;
}

.kid-button{

	background-color: #F7931E !important;
	border: none !important;
	color: #fff !important;
}

.blue-background{

	background-color: #036EB8 !important;
}

.orange-background{
		
	background-color: #e89130 !important;

}

.blue-border{

	border: 1px solid #036EB8 !important;
}

.orange-border{

	border: 1px solid #e89130 !important;

}

.blue-border-bottom{

	border-bottom: 1px solid #036EB8 !important;
}

.orange-border-bottom{

	border-bottom: 1px solid #e89130 !important;

}

.opacity08{

	.opacity: 0.8;
}




</style>
@stop
@section('middle')
<div class="father-container">
	<div class="fatherday-banner">
		<img class="hidden-xs" src="{{ assetRemote('/image/promotion/2018fatherday/banner370.jpg') }}">
		<img class="hidden-md hidden-lg" src="{{ assetRemote('/image/promotion/2018fatherday/1200x628.jpg') }}">
	</div>
	<div class="fatherday-block">
		<div class="fatherday-title">
			<h1>全館免運費</h1>
		</div>
		<div class="fatherday-shoppingfree-img-container">
			<div class="fatherday-shoppingfree-img zoom-image">
				<a href="{{ route('cart') }}" target="_blank"><img class="shoppingfree-img hidden-xs" src="{{ assetRemote('/image/promotion/2018fatherday/free370.jpg') }}"></a>
				<a href="{{ route('cart') }}" target="_blank"><img class="hidden-md hidden-lg" src="{{ assetRemote('/image/promotion/2018fatherday/345x270.jpg') }}"></a>
			</div>
		</div>
		<div class="fatherday-shoppingfree-content hidden-xs">
			<p>【活動內容】在一年一度專屬父親的節日，Webike 特別舉辦了免運費活動獻給所有會員，無論你要孝敬老爸，或是犒賞自己都非常划算！</p>
			<p>【活動辦法】活動期間發送給所有會員兩張免運費折價券，此折價卷適用全館所有商品，一張訂單僅能使用一張免運費折價券。</p>
			<p>【使用說明】登入後在購物車頁面中找到折價券的欄位，選擇<a href = "{{ route('cart') }}" target="_blank">「這次運費我們"父親" 」</a>並點選輸入，即可折抵運費金額。</p>
			<div id="father-productguide"></div>
			<p>【活動期限】2018/8/1 00:00 ~ 2018/8/8 23:59</p>
		</div>
		<ul class="fatherday-shoppingfree-content-mobile hidden-md hidden-lg">
			<li>【活動內容】<br>在一年一度專屬父親的節日，Webike 特別舉辦了免運費活動獻給所有會員，無論你要孝敬老爸，或是犒賞自己都非常划算！</li>
			<li>【活動辦法】<br>活動期間發送給所有會員兩張免運費折價券，此折價卷適用全館所有商品，一張訂單僅能使用一張免運費折價券。</li>
			<li>【使用說明】<br>登入後在購物車頁面中找到折價券的欄位，選擇<a href = "{{ route('cart') }}" target="_blank">「這次運費我們"父親" 」</a>並點選輸入，即可折抵運費金額。</li>
			<div id="father-productguide-mobile"></div>
			<li>【活動期限】<br>2018/8/1 00:00 ~ 2018/8/8 23:59</li>
		</ul>
	</div>

	@include('response.pages.benefit.sale.project.2018.fatherday.father_choose')


	<div class="fatherday-benefit-block">
		<div class="fatherday-title">
			<h1>會員好康</h1>
		</div>
		<div class="fatherday-benefit-content hidden-xs clearfix">
			<div class="fatherday-benefit col-md-4 col-sm-12 col-xs-12">
				<div class="fatherday-benefit-img zoom-image">
					<a href="{{ \URL::route('benefit-sale-month-promotion',['code' => '2018-08']) }}" target="_blank" ><img src="{{ assetRemote('/image/promotion/2018fatherday/augustsale345.jpg') }}"></a>
				</div>
				<div class="fatherday-benefit-txt">
					<span class="size-10rem">8月精選優惠，指定品牌、分類、車型，特價與點數5倍回饋優惠實施中！</span>
				</div>
			</div>
			<div class="fatherday-benefit col-md-4 col-sm-12 col-xs-12">
				<div class="fatherday-benefit-img zoom-image">
					<a  href="{{ \URL::route('outlet') }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018fatherday/outlet345.jpg') }}"></a>
				</div>
				<div class="fatherday-benefit-txt">
					<span class="size-10rem">「Outlet」定期上架各大品牌出清商品，賣不掉就再降，快來尋找專屬於你的寶物。</span>
				</div>
			</div>
			<div class="fatherday-benefit col-md-4 col-sm-12 col-xs-12">
				<div class="fatherday-benefit-img zoom-image">
					<a  href="{{ route('cart') }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018fatherday/add345.jpg') }}"></a>
				</div>
				<div class="fatherday-benefit-txt">
					<span class="size-10rem">「加價購」─本月專屬，不限品項訂單滿3千元，鋼索注油器(雙頭)加購價只要1元！</span>
				</div>
			</div>
		</div>
		<div class="fatherday-benefit-content-moblie hidden-md hidden-lg clearfix">
			<div class="fatherday-benefit-moblie col-md-4 col-sm-12 col-xs-12">
				<div class="fatherday-benefit-img-moblie zoom-image">
					<a href="{{ \URL::route('benefit-sale-month-promotion',['code' => '2018-08']) }}" target="_blank" ><img src="{{ assetRemote('/image/promotion/2018fatherday/augustsale345.jpg') }}"></a>
				</div>
				<div class="fatherday-benefit-txt-moblie">
					<span class="size-10rem">8月精選優惠，指定品牌、分類、車型，特價與點數5倍回饋優惠實施中！</span>
				</div>
			</div>
			<div class="fatherday-benefit-moblie col-md-4 col-sm-12 col-xs-12">
				<div class="fatherday-benefit-img-moblie zoom-image">
					<a  href="{{ \URL::route('outlet') }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018fatherday/outlet345.jpg') }}"></a>
				</div>
				<div class="fatherday-benefit-txt-moblie">
					<span class="size-10rem">「Outlet」定期上架各大品牌出清商品，賣不掉就再降，快來尋找專屬於你的寶物。</span>
				</div>
			</div>
			<div class="fatherday-benefit-moblie col-md-4 col-sm-12 col-xs-12">
				<div class="fatherday-benefit-img-moblie zoom-image">
					<a  href="{{ route('cart') }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018fatherday/add345.jpg') }}"></a>
				</div>
				<div class="fatherday-benefit-txt-moblie">
					<span class="size-10rem">「加價購」─本月專屬，不限品項訂單滿3千元，鋼索注油器(雙頭)加購價只要1元！</span>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')
	<script>
		$(document).on('change','#father-choose #ca-choose .show div.center .center',function(){
			console.log(123);
		});

		$('#father-choose .loading-box').hide();
		$('.shoppingfree-img')
		.mouseover(function(event) {
			$('.fatherday-shoppingfree-img').css("background-color","#fff");
		});

		$('.productguide-img-dad')
		.mouseover(function(event) {
			$('.fatherday-productguide-img-container').css("background-color","#036EB8");
			$('.fatherday-productguide-img-dad').css("background-color","#fff");
		})	
		.mousedown(function(event) {
			$('.fatherday-productguide-img-container').addClass('blue-background');
			$('.fatherday-productguide-img-container').removeClass('orange-background');
			$('.father-productguide-dad-ca').removeClass('hidden');
			$('.father-productguide-kid-ca').addClass('hidden');
			$('.dad-button-block').removeClass('hidden');
			$('.kid-button-block').addClass('hidden');
			$('.father-productguide-dad-ca').find('.owl-prev').addClass('blue-background');
			$('.father-productguide-dad-ca').find('.owl-next').addClass('blue-background');
			$('.father-productguide-dad-ca').find('.owl-prev:hover').addClass('blue-background');
			$('.father-productguide-dad-ca').find('.owl-next:hover').addClass('blue-background');
			$('.father-productguide-kid-ca').find('.owl-prev').removeClass('orange-background');
			$('.father-productguide-kid-ca').find('.owl-next').removeClass('orange-background');
			$('.father-productguide-kid-ca').find('.owl-prev:hover').removeClass('orange-background');
			$('.father-productguide-kid-ca').find('.owl-next:hover').removeClass('orange-background');
			$('.father-productguide-product').find('.owl-prev').addClass('blue-background');
			$('.father-productguide-product').find('.owl-next').addClass('blue-background');
			$('.father-productguide-product').find('.owl-prev:hover').addClass('blue-background');
			$('.father-productguide-product').find('.owl-next:hover').addClass('blue-background');
			$('.father-productguide-product').find('.owl-prev').removeClass('orange-background');
			$('.father-productguide-product').find('.owl-next').removeClass('orange-background');
			$('.father-productguide-product').find('.owl-prev:hover').removeClass('orange-background');
			$('.father-productguide-product').find('.owl-next:hover').removeClass('orange-background');
			$('.fatherday-productguide-block').addClass('blue-border');
			$('.fatherday-productguide-block').removeClass('orange-border');
			$('.fatherday-productguide-ca-container').addClass('blue-border-bottom');
			$('.fatherday-productguide-ca-container').removeClass('orange-border-bottom');
			$('.fade-dad').fadeTo("slow",1);
			$('.fade-kid').fadeTo("slow",0);
		})
		.mouseup(function(event) {
			if($('.fade-kid').hasClass('show')){
				$('.fatherday-productguide-img-container').addClass('blue-background');
				$('.fade-dad').removeClass('hidden');
				$('.fade-kid').addClass('hidden');
				$('.fade-dad').addClass('show');
				$('.fade-kid').removeClass('show');
				postAjax();
			}
		});

		$('.productguide-img-dad-mobile')
		.click(function(event) {
			if($('.fade-kid').hasClass('show')){
				$('.fatherday-productguide-img-container').addClass('blue-background');
				$('.fatherday-productguide-img-container').removeClass('orange-background');
				$('.father-productguide-dad-ca').removeClass('hidden');
				$('.father-productguide-kid-ca').addClass('hidden');
				$('.dad-button-block').removeClass('hidden');
				$('.kid-button-block').addClass('hidden');
				$('.father-productguide-dad-ca').find('.owl-prev').addClass('blue-background');
				$('.father-productguide-dad-ca').find('.owl-next').addClass('blue-background');
				$('.father-productguide-dad-ca').find('.owl-prev:hover').addClass('blue-background');
				$('.father-productguide-dad-ca').find('.owl-next:hover').addClass('blue-background');
				$('.father-productguide-kid-ca').find('.owl-prev').removeClass('orange-background');
				$('.father-productguide-kid-ca').find('.owl-next').removeClass('orange-background');
				$('.father-productguide-kid-ca').find('.owl-prev:hover').removeClass('orange-background');
				$('.father-productguide-kid-ca').find('.owl-next:hover').removeClass('orange-background');
				$('.father-productguide-product').find('.owl-prev').addClass('blue-background');
				$('.father-productguide-product').find('.owl-next').addClass('blue-background');
				$('.father-productguide-product').find('.owl-prev:hover').addClass('blue-background');
				$('.father-productguide-product').find('.owl-next:hover').addClass('blue-background');
				$('.father-productguide-product').find('.owl-prev').removeClass('orange-background');
				$('.father-productguide-product').find('.owl-next').removeClass('orange-background');
				$('.father-productguide-product').find('.owl-prev:hover').removeClass('orange-background');
				$('.father-productguide-product').find('.owl-next:hover').removeClass('orange-background');
				$('.fatherday-productguide-block').addClass('blue-border');
				$('.fatherday-productguide-block').removeClass('orange-border');
				$('.fatherday-productguide-ca-container').addClass('blue-border-bottom');
				$('.fatherday-productguide-ca-container').removeClass('orange-border-bottom');
				$('.fade-dad').removeClass('hidden');
				$('.fade-kid').addClass('hidden');
				$('.fade-dad').addClass('show');
				$('.fade-kid').removeClass('show');
				postAjax();
			}

		});
		
		$('.productguide-img-kid')
		.mouseover(function(event) {
			$('.fatherday-productguide-img-container').css("background-color","#e89130");
			$('.fatherday-productguide-img-kid').css("background-color","#fff");

		})

		.mousedown(function(event) {
			$('.fatherday-productguide-img-container').addClass('orange-background');
			$('.fatherday-productguide-img-container').removeClass('blue-background');
			$('.father-productguide-dad-ca').addClass('hidden');
			$('.father-productguide-kid-ca').removeClass('hidden');
			$('.dad-button-block').addClass('hidden');
			$('.kid-button-block').removeClass('hidden');
			$('.father-productguide-dad-ca').find('.owl-prev').removeClass('blue-background');
			$('.father-productguide-dad-ca').find('.owl-next').removeClass('blue-background');
			$('.father-productguide-dad-ca').find('.owl-prev:hover').removeClass('blue-background');
			$('.father-productguide-dad-ca').find('.owl-next:hover').removeClass('blue-background');
			$('.father-productguide-kid-ca').find('.owl-prev').addClass('orange-background');
			$('.father-productguide-kid-ca').find('.owl-next').addClass('orange-background');
			$('.father-productguide-kid-ca').find('.owl-prev:hover').addClass('orange-background');
			$('.father-productguide-kid-ca').find('.owl-next:hover').addClass('orange-background');
			$('.father-productguide-product').find('.owl-prev').removeClass('blue-background');
			$('.father-productguide-product').find('.owl-next').removeClass('blue-background');
			$('.father-productguide-product').find('.owl-prev:hover').removeClass('blue-background');
			$('.father-productguide-product').find('.owl-next:hover').removeClass('blue-background');
			$('.father-productguide-product').find('.owl-prev').addClass('orange-background');
			$('.father-productguide-product').find('.owl-next').addClass('orange-background');
			$('.father-productguide-product').find('.owl-prev:hover').addClass('orange-background');
			$('.father-productguide-product').find('.owl-next:hover').addClass('orange-background');
			$('.fatherday-productguide-block').removeClass('blue-border');
			$('.fatherday-productguide-block').addClass('orange-border');
			$('.fatherday-productguide-ca-container').removeClass('blue-border-bottom');
			$('.fatherday-productguide-ca-container').addClass('orange-border-bottom');
			$('.fade-dad').fadeTo("slow",0);
			$('.fade-kid').fadeTo("slow",1);
		})

		.mouseup(function(event) {
			if($('.fade-dad').hasClass('show')){
				$('.fatherday-productguide-img-container').addClass('orange-background');
				$('.fade-dad').addClass('hidden');
				$('.fade-kid').removeClass('hidden');
				$('.fade-dad').removeClass('show');
				$('.fade-kid').addClass('show');
				postAjax();
			}
			
		});

		$('.productguide-img-kid-mobile')
		.click(function(event) {
			if($('.fade-dad').hasClass('show')){
				$('.fatherday-productguide-img-container').addClass('orange-background');
				$('.fatherday-productguide-img-container').removeClass('blue-background');
				$('.father-productguide-dad-ca').addClass('hidden');
				$('.father-productguide-kid-ca').removeClass('hidden');
				$('.dad-button-block').addClass('hidden');
				$('.kid-button-block').removeClass('hidden');
				$('.father-productguide-dad-ca').find('.owl-prev').removeClass('blue-background');
				$('.father-productguide-dad-ca').find('.owl-next').removeClass('blue-background');
				$('.father-productguide-dad-ca').find('.owl-prev:hover').removeClass('blue-background');
				$('.father-productguide-dad-ca').find('.owl-next:hover').removeClass('blue-background');
				$('.father-productguide-kid-ca').find('.owl-prev').addClass('orange-background');
				$('.father-productguide-kid-ca').find('.owl-next').addClass('orange-background');
				$('.father-productguide-kid-ca').find('.owl-prev:hover').addClass('orange-background');
				$('.father-productguide-kid-ca').find('.owl-next:hover').addClass('orange-background');
				$('.father-productguide-product').find('.owl-prev').removeClass('blue-background');
				$('.father-productguide-product').find('.owl-next').removeClass('blue-background');
				$('.father-productguide-product').find('.owl-prev:hover').removeClass('blue-background');
				$('.father-productguide-product').find('.owl-next:hover').removeClass('blue-background');
				$('.father-productguide-product').find('.owl-prev').addClass('orange-background');
				$('.father-productguide-product').find('.owl-next').addClass('orange-background');
				$('.father-productguide-product').find('.owl-prev:hover').addClass('orange-background');
				$('.father-productguide-product').find('.owl-next:hover').addClass('orange-background');
				$('.fatherday-productguide-block').removeClass('blue-border');
				$('.fatherday-productguide-block').addClass('orange-border');
				$('.fatherday-productguide-ca-container').removeClass('blue-border-bottom');
				$('.fatherday-productguide-ca-container').addClass('orange-border-bottom');
				$('.fade-dad').addClass('hidden');
				$('.fade-kid').removeClass('hidden');
				$('.fade-dad').removeClass('show');
				$('.fade-kid').addClass('show');
				postAjax();
			}
		});


			$('.owl-carousel-ca').addClass('owl-carousel').owlCarousel({
				center:true,
			 	loop:true,
			    margin:10,
			    nav:true,
			    slideBy : 1,
			    mouseDrag:false,
			    touchDrag:false,
			    responsive:{
			        0:{
			            items:1 
			        },
			        600:{
			            items:1
			        },
			        1000:{
			            items:3
			        }
			    }
			});

			$('.owl-carousel-ca').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
			$('.owl-carousel-ca').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
			
			$('.owl-carousel-product').addClass('owl-carousel').owlCarousel({
				loop:true,
				nav:true,
				margin:5,
				slideBy : 7,
				URLhashListener:true,
				startPosition: 'URLHash',
				responsive:{
					0:{
						items:2
					},
					600:{
						items:3
					},
					1000:{
						items:7
					}
				}
			});

			$('.owl-carousel-product').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
			$('.owl-carousel-product').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
		
			$("#father-choose #ca-choose ").on('click','.show .owl-nav',function(){
				postAjax();
			});

			function postAjax(){
		       	$('#father-choose #father-choose-product').hide();
	       		$('#father-choose .loading-box').show();
				setTimeout(function(){
					var ca_id = $('#father-choose #ca-choose .show div.center .ca-id').attr('id');
					var ca_url_rewrite = $('#father-choose #ca-choose .show div.center .ca-id').attr('data-content');
					var token = $('meta[name=csrf-token]').attr('content');
		        	$.ajax({
		        		url: "{!! route('benefit-event-fatherday-getProduct') !!}",
		        		data:{'_token':token ,'category_id' : ca_id},
		        		type:"POST",
		        		dataType:'JSON',
		        		success:function(result){
		        			$('#father-choose #father-choose-product').replaceWith(result.html);
			                $('#father-choose .loading-box').hide();
		    				slider = $('#father-choose .owl-carousel-product').addClass('owl-carousel').owlCarousel({
			                    loop:true,
			                    nav:true,
			                    margin:5,
			                    slideBy : 7,
			                    URLhashListener:true,
			                    startPosition: 'URLHash',
			                    responsive:{
			                        0:{
			                            items:2
			                        },
			                        600:{
			                            items:3
			                        },
			                        1000:{
			                            items:7
			                        }
			                    }
			                }).find("img").unveil().trigger("unveil");
		        			$('#father-choose .owl-carousel-product.owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
		                    $('#father-choose .owl-carousel-product.owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
			                if($('#ca-choose .fade-kid').hasClass('show')){
		                		var kid_choose_url = "https://www.webike.tw/collection/brand/fatherday-kid?sort=visits&limit=40&order=asc&ca=" + ca_url_rewrite;
			                	$('#father-choose-product .dad-button-block').addClass('hidden');
			                	$('#father-choose-product  .kid-button-block').removeClass('hidden');
			                	$('#father-choose .owl-carousel-product.owl-carousel').find('.owl-prev').addClass('orange-background');
			                	$('#father-choose .owl-carousel-product.owl-carousel').find('.owl-next').addClass('orange-background');
			                	$('#father-choose #father-choose-product .kid-button-block a.kid-button').attr('href',kid_choose_url)
			                }else{
			                	var father_choose_url = "https://www.webike.tw/collection/brand/fatherday-dad?sort=visits&limit=40&order=asc&ca=" + ca_url_rewrite;
			                	$('#father-choose #father-choose-product .dad-button-block a.dad-button').attr('href',father_choose_url);
			                }
		        		},

		        		error:function(xhr,ajaxOptions,thrownError,textStatus){
		        			
		        		}
		        	});
				},1000);
			}


			

		
	</script>
@stop