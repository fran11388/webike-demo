<style>
    .search {
        padding: 0px;
    }
    .searchbar {
        background-color: #FFF;
        height:50px;
        padding:10px;
    }
    .searchbar .contain {
        width:auto;
        max-width:1000px;
        padding-top: 5px;
        margin-right:auto;
        margin-left:auto;
    }
    .searchbar .contain li {
        padding:0px;
        list-style-type:none;
    }
    .searchbar .contain .first li {
        padding-right: 10px;
        padding-left: 0px;
    }
    .searchbar .contain input {
        height:30px !important;
    }
    .searchbar .contain .searchtext {
        width:100%;
        background-color: #ffffff;
        border: 1px solid #cccccc; 
        padding: 9px;
    }
    .searchbar .contain .btn-warning {
        width:100%;
        padding: 0;
    }
    .searchbar .contain .btn-danger {
        border-radius: 5px;
        width:100%;
        height:30px;
    }
    .contain li select {
        width:100%;
        height:30px;
        text-align: center;
        text-align-last: center;
    }
    .select2-hidden-accessible {
        border: 0 !important;
        clip: rect(0 0 0 0) !important;
        height: 1px !important;
        margin: -1px !important;
        overflow: hidden !important;
        padding: 0 !important;
        position: absolute !important;
        width: 1px !important;
    }
    .fixed {
        position: fixed;
        top: 0%;
        width:100%!important;
        min-width: 100%!important;
        z-index: 200!important;
    }
    .my-bike{
        padding-right: 0px !important;
    }
    .my-bike a {
        background-color:white;
        height:30px;
    }
    .my-bike .btn-my-bike {
        height:30px;
    }
    .my-bike .btn-my-bike span{
        line-height:30px;
    }
    .my-bike .btn-my-bike p {
        height:28px;
    }
    .select2-container .select2-selection--single {
        height:30px;
    }
    .mybikelogin {
        background-color:white;
        width:110px;
        height:30px;
    }
    .show-my-bike-fixed{
        position: fixed;
        top:45px;
    }
    @media (max-width: 768px) {
        .searchbar {
            display:none !important;
        }
    }
    .select2-container .select2-selection--single .select2-selection__rendered {
        line-height:28px !important;
    }
    .select2-selection__arrow b{
        margin-top:-4px !important;
    }
    .searchbar span {
        font-size: 0.85rem !important;
    }
    .searchbar input,input::-webkit-input-placeholder  {
        font-size: 0.85rem !important;
    }
</style>
<div class="searchbar">
    <div class="contain">
        <ul class="clearfix">
            <li class="col-md-4 col-sm-5 col-xs-12 first">
                <form id="parts" action="{{URL::route('home').'/parts'}}" method="GET" target="_blank">
                    <input type="hidden" name="mt" disabled="">
                    <li class="col-md-9 col-sm-9 col-xs-12">
                        <input class="searchtext" type="text" name="q" placeholder="商品關鍵字搜尋..."> 
                    </li>
                    <li class="col-md-3 col-sm-3 col-xs-12">
                        <input class="btn btn-warning searchbtn" type="submit" value="搜尋"> 
                    </li>
                </form>
            </li>
            <li class="col-md-8 col-sm-7 col-xs-12 first motors">
                <ul class="clearfix">
                    <li class="col-md-3 col-sm-3 col-xs-12">
                        <div class="select-list-box">
                            <select class="select2 select-motor-manufacturer">
                                <option>請選擇廠牌</option>
                                @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </li>
                    <li class="col-md-3 col-sm-3 col-xs-12">
                        <div class="select-list-box">
                            <select class="select2 select-motor-displacement">
                                <option>cc數</option>
                            </select>
                        </div>
                    </li>
                    <li class="col-md-3 col-sm-3 col-xs-12">
                        <div class="select-list-box">
                            <select class="select2 select-motor-model">
                                <option>車型</option>
                            </select>
                        </div>
                    </li>
                    <li class="col-md-3 col-sm-3 col-xs-12">
                        <ul>
                            @if($current_customer)
                                @if(count($current_customer->motors))
                                    <li class="my-bike animated fadeInRight" style="padding-right:0px;">
                                        <a href="javascript:void(0)" class="btn-my-bike visible-sm visible-md visible-lg" style="width:100%;"> <span>MyBike<span class="hidden-xs hidden-sm" style="padding: 0;margin:0;">商品</span><span class="hidden-md hidden-sm" style="padding: 0;margin:0;">搜尋</span></span>
                                            <p style="padding:9px;line-height:16px"><i class="fa fa-chevron-down" aria-hidden="true"></i></p>
                                        </a>
                                        <a href="" class="btn-my-bike-tablet font-color-red visible-sm">
                                            <i class="fa fa-motorcycle size-15rem" aria-hidden="true"></i>
                                        </a>

                                        <ul class="show-my-bike">
                                            @foreach($current_customer->motors as $motor)
                                                <li>
                                                    <a href="{{ route('parts',[ 'mt/'.$motor->url_rewrite ]).'?'.$rel_parameter }}" target="_blank">
                                                        {{ $motor->name }}
                                                    </a>
                                                </li>
                                            @endforeach

                                            <li class="btn-add-more-bike">
                                                <a href="{{ route('customer-mybike').'?'.$rel_parameter }}" target="_blank">
                                                    <i class="fa fa-plus" aria-hidden="true"></i> 編輯MyBike
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                @else
                                    <li class="my-bike animated"><a href="{{ route('customer-mybike').'?'.$rel_parameter }}" class="btn-login font-color-red" target="_blank">登錄MyBike</a></li>
                                @endif
                            @else
                                <li><a href="{{ route('customer-mybike').'?'.$rel_parameter }}" class="mybikelogin btn-login font-color-red" target="_blank">登錄MyBike</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<script>
    $(window).scroll(function(){
        if ($(this).scrollTop() > 560) {
          $('.show-my-bike').addClass('show-my-bike-fixed');
        } else {
          $('.show-my-bike').removeClass('show-my-bike-fixed');
        }
    });
    $('.searchbtn').click(function(event){
        var value = $('.searchtext').val();
        if(value == ''){
            alert('您尚未輸入關鍵字');
            event.preventDefault();
        }
    });
    $(".select-motor-manufacturer").change(function () {
        var value = $(this).find('option:selected').val();
        if (value) {
            var url = path + "/api/motor/displacements?manufacturer=" + value;
            $.get(url, function (data) {
                var $target = $(".select-motor-displacement");
                $target.find("option:not(:first)").remove().end();
                for (var i = 0; i < data.length; i++) {
                    $target.append($("<option></option>").attr("value", data[i]).text(data[i]))
                }
            });
        }
    });

    $(".select-motor-displacement").change(function () {
        var value = $(this).find('option:selected').val();
        if (value) {
            var url = path + "/api/motor/model?manufacturer=" +
                    $(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

            $.get(url, function (data) {
                var $target = $(".select-motor-model");
                $target.find("option:not(:first)").remove().end();
                for (var i = 0; i < data.length; i++) {
                    $target.append($("<option></option>").attr("value", data[i]['key']).text(data[i]['name']))
                }
            });
        }
    });

    $(".select-motor-model,.select-motor-mybike").change(function () {
        var value = $(this).find('option:selected').val();
        if (value) {
            redirect('', value);
        }
    });

    function redirect(q, motor) {
        var params = {};
        // if (q)
        //     params.q = q;
        // else if (getParameterByName('q'))
        //     params.q = getParameterByName('q');
        if (motor)
            params.motor = motor;
        else if (getParameterByName('motor'))
            params.motor = getParameterByName('motor');


        var esc = encodeURIComponent;
        var query = Object.keys(params).map(k => esc(k) + '=' + esc(params[k])).join('&');
        if (query)
            query = "/" + query;
        window.open('http://www.webike.tw/mt' + "/" + motor + "{{'?'.$rel_parameter}}");
    }

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
</script>