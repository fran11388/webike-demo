<style>
#category-page .box-content{
		margin-bottom: 0px;
	    padding: 0;
	    background-color: #fff;
	    border: 1px solid #1ca28a;
	    display: block;
	    width:100%;
	    margin-left: auto;
	    margin-right: auto;

	}
	#category-page .content-box-sale{
		float: none;
	}
	#category-page{
		margin-top: 30px;
		float: none;
	}

	
	</style>
<div id="category-page" class="ct-sale-page form-group clearfix">
    <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
        <h2 class="font-bold size-15rem">2017年度熱銷排行</h2>
    </div>
    <div class="content-box-sale clearfix">
        <ul class="product-sale">
            <li class="li-product-sale col-xs-12 col-sm-12 col-md-12">
{{--                 <a href="{{URL::route('ranking').'?sort=year&'.$rel_parameter}}" class="clearfix" target="_blank"> --}}
                    <div class="photo-box">
                        <ul class="clearfix">
                            <li class="col-xs-12 col-sm-12 col-md-12"><img src="{{assetRemote('image/benefit/big-promotion/2018/activity/ranking.jpg')}}" alt=""></li>
                        </ul>
                    </div>
                {{-- </a> --}}

            </li>
        </ul>
    </div>    
</div>