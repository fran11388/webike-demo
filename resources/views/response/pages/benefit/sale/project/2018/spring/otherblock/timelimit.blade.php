<style>
	.pc-limit-img-end,.nostart-active{
		cursor: default!important;
	}
	#time-limit .time-limit-banner figure img:hover{
		transform: scale(1.2);
	}
	#time-limit .time-limit-banner figure{
		padding: 0px;
	}
</style>				
@php

$limits = array(
	'1' => array('start' => "2018-03-29",'end' => "2018-04-08" ,'url' => URL::route('parts', ['section' => 'ca/1000-1001'])),
	'2' => array('start' => "2018-04-08",'end' => "2018-04-15" ,'url' => URL::route('parts', ['section' => 'ca/3000-3260'])),
	'3' => array('start' => "2018-04-15",'end' => "2018-04-22" ,'url' => URL::route('parts', ['section' => 'ca/1000-1110'])),
	'4' => array('start' => "2018-04-22",'end' => "2018-05-01" ,'url' => URL::route('parts', ['section' => 'ca/3000-3001'])),
);
@endphp

<div  id="time-limit" class=" time-limit clearfix ">
	@foreach($limits as $key =>$limit)
		@if(activeValidate($limit['start'],$limit['end']))
			<div class="time-limit-banner">
				<a href="{{$limit['url'].'?'.$rel_parameter}}" target="_blank">
					<figure class="zoom-image">
						<img src="{{assetRemote('image/benefit/big-promotion/2018/spring/pc/banner/banner_'.$key.'.jpg')}}">
					</figure>
				</a>
			</div>
		@endif
	@endforeach
	@foreach($limits as $key =>$limit)
		@php
		 	$activity = false;
			if(activeValidate($limit['start'],$limit['end'])){
				$photo_page = "link";
				$active = "link";
				$activity = true;
			}elseif(date('Y-m-d') < $limit['start']){
				$photo_page = "link-nostart";
				$active = 'nostart';
				$activity = false;
			}else{
				$photo_page = "visited-end";
				$active = "end";
				$activity = false;
			}
			if($is_phone){
				$page = 'ph';
			}else{
				$page = 'pc';
			}
		@endphp
		<div class="col-md-3 col-xs-6">
			<a href="{{ $activity ? $limit['url'].'?'.$rel_parameter : 'javascript:void(0)'}}" target="_blank">
				<img src="{{assetRemote('image/benefit/big-promotion/2018/spring/'. $page .'/'.$photo_page.'/limit_'.$key.'.jpg')}}" class="{{$page}}-limit-img-{{$active}}">
				<span class="hidden key">{{$key}}</span>
			</a>
		</div>

	@endforeach
</div>
<script  type="text/javascript">
	$(".pc-limit-img-nostart").click(function(){
		if(!$(this).hasClass("nostart-active")){
			var photo = "{{assetRemote('image/benefit/big-promotion/2018/spring/pc/active-nostart/limit_')}}";
			var key = $(this).closest('div').find('span').text();
			var nostartphoto = photo+key+".gif";
			$(this).attr("src",nostartphoto);
			$(this).addClass("nostart-active");
		}
	});
</script>