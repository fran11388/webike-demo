<style>
	#country_discount .box-content{
		margin-bottom: 0px;
	    padding: 0;
	    background-color: #fff;
	    border: 1px solid #c7b299;
	    display: block;
	    width:100%;
	    margin-left: auto;
	    margin-right: auto;

	}

	#country_discount .content-box-sale{
		float: none;
	}
	#country_discount{
		float: none;
		margin-top: 30px;
	}

	#country_discount .product-grid span{
		width: 100%;
	}

	#country_discount .brand_url_rewrite img{
	    
		height:71px;
	}
	#country_discount .brand_url_rewrite:hover img{
	    opacity: 1;
		height:71px;
	    transform: scale(1.3);
        
	}
	#country_discount .brand_url_rewrite:hover{
		cursor: pointer;
	}
	#country_discount #brand .brand_url_rewrite{
		opacity:0.5;
	}

	#country_discount #brand .owl-stage .center .brand_url_rewrite{
		opacity:1;
	}
	#country_discount .loading-box {
		display:none;
	}
	#country_discount #product_brand .country-discount-all-btn{
		margin-bottom: 20px;
	}
	#country_discount #product_brand .country-discount-all-btn .btn-warning{
		background-color: #b92229;
	}
	#country_discount #discount_brand .product-grid a figure{
		height: inherit;
	}
	@media(max-width: 550px){
		#country_discount .brand_url_rewrite img{
				height: inherit;
			}
		#country_discount #discount_brand #brand .product-grid .col-md-7 .zoom-container .zoom-image{
			display: block;
		}
	}
</style>
@php
	$brands = [
		array('id'=>'t10',
			'name' => 'MOS',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t10.png',
					'img'=>'https://img.webike.tw/product/t10/t00060870.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'2030',
			'name' => 'KOSO',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_2030.gif',
					'img'=>'https://img.webike.tw/product/2030/t00075150.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2289',
			'name' => 'DOG HOUSE 惡搞手工廠',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2289.png',
					'img'=>'https://img.webike.tw/product/2379/t00077110.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'1916',
			'name' => 'NCY',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1916.gif',
					'img'=>'https://img.webike.tw/product/1916/675I03LD323BKD-1.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'1378',
			'name' => 'SHAD',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1378.gif',
					'img'=>'https://img.webike.tw/product/1378/t00059056.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t1994',
			'name' => 'WRRP',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1994.png',
					'img'=>'https://img.webike.tw/product/t1994/wrrp-023-GOL-2.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'501',
			'name' => 'NHRC',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_501.gif',
					'img'=>'https://img.webike.tw/product/501/t00071173-4.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t1857',
			'name' => '機怪',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1857.png',
					'img'=>'https://img.webike.tw/product/t1857/GD019-BLA-SIL.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t1914',
			'name' => 'EPIC',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1914.png',
					'img'=>'https://img.webike.tw/product/t1914/t00045060.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'1624',
			'name' => 'SIMOTA',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1624.gif',
					'img'=>'https://img.webike.tw/product/1624/t00081625.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2514',
			'name' => 'DK design 達卡設計',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2514.png',
					'img'=>'https://img.webike.tw/product/t2514/t00065154.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2222',
			'name' => '庫帆GarageSaiL',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2222.png',
					'img'=>'https://img.webike.tw/product/t2222/t00109648-2.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2758',
			'name' => 'WIZH 欣炫',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2758.png',
					'img'=>'https://img.webike.tw/product/t2758/t00064042-2.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2287',
			'name' => 'K&S',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2287.png',
					'img'=>'https://img.webike.tw/product/t2287/t00069196.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t3643',
			'name' => 'EXPED',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t3643.png',
					'img'=>'https://img.webike.tw/product/t3643/t00127997.png',
				     'discount'=>'點數8倍現折'),
		array('id'=>'t2223',
			'name' => 'BENKIA',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2223.png',
					'img'=>'https://img.webike.tw/product/t2223/t00046477.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2012',
			'name' => 'SOL',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2012.png',
					'img'=>'https://img.webike.tw/product/t2012/t00084180.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2570',
			'name' => 'AUGI',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2570.png',
					'img'=>'https://img.webike.tw/product/t2570/t00058455.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t1886',
			'name' => 'EXUSTAR',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1886.png',
					'img'=>'https://img.webike.tw/product/t1886/t00066333.jpg',
				     'discount'=>'點數8倍現折'),
		array('id'=>'t1961',
			'name' => 'ZEUS 瑞獅',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1961.png',
					'img'=>'https://img.webike.tw/product/t1961/ZS-1200E-C-M.png',
				     'discount'=>'點數8倍現折'),
		array('id'=>'t1940',
			'name' => 'M2R',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1940.png',
					'img'=>'https://img.webike.tw/product/t1940/t00050796.jpg',
				     'discount'=>'點數8倍現折'),
		array('id'=>'t1895',
			'name' => 'SPEED-R',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1895.png',
					'img'=>'https://img.webike.tw/product/t1895/SG-SR13-S.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2013',
			'name' => 'KYT',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2013.png',
					'img'=>'https://img.webike.tw/product/t2013/kyt-vendtta-tpy-m.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2010',
			'name' => 'THH',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2010.png',
					'img'=>'https://img.webike.tw/product/t2010/T-76A+mechanical_dragon-BLA-M.jpg',
				     'discount'=>'點數8倍現折'),
		array('id'=>'t2573',
			'name' => 'Biltwell',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2573.png',
					'img'=>'https://img.webike.tw/product/t2573/t00073036.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t1828',
			'name' => 'ASTONE',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t1828.png',
					'img'=>'https://img.webike.tw/product/t1828/t00071991.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2571',
			'name' => 'K-MAX',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2571.png',
					'img'=>'https://img.webike.tw/product/t2571/t00058891.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t2419',
			'name' => '英爵士',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t2419.png',
					'img'=>'https://img.webike.tw/product/2906/t00082600.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t755',
			'name' => 'FILO DESIGN',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t755.png',
					'img'=>'https://img.webike.tw/product/t755/AS001O.jpg',
				     'discount'=>'點數5倍現折'),
		array('id'=>'t3577',
			'name' => 'MOTOBOY',
					'brandimg'=>'https://img.webike.tw/assets/images/brands/promotion/brand_t3577.png',
					'img'=>'https://img.webike.tw/product/t3577/t00125371.jpg',
				     'discount'=>'點數5倍現折'),
		];
@endphp
<div id="country_discount" class="ct-sale-page form-group clearfix">
    <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
        <h2 class="font-bold size-15rem">周年慶品牌現折</h2>
    </div>
    <div class="content-box-sale clearfix">
        <ul class="product-sale">
            <li class="li-product-sale col-xs-12 col-sm-12 col-md-12">
                <a href="https://www.webike.tw/collection/brand/point-discount?rel=2018-10-2018fall&sort=visits&limit=40&order=asc" class="clearfix" target="_blank">
                    <div class="photo-box">
                        <ul class="clearfix">
                            <li class="col-xs-12 col-sm-12 col-md-12">
                            	<figure class="zoom-image">
                            		<img class="hidden-xs" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/count_discount370.jpg')}}" alt="周年慶品牌現折">
                            		<img class="hidden-md hidden-lg" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/count_discount270.jpg')}}" alt="周年慶品牌現折">
                            	</figure>
                            </li>
                        </ul>
                    </div>
                </a>
                <div class="description">
                	<span class="">	
    	            	周年慶品牌現折─多項優質品牌，上千項點數優惠商品，回饋的現金點數立即現折。
                	</span>
                </div>
            </li>
        </ul>
    </div>
    <div id="discount_brand" class="row box-content winter-collerction">
		<div class="ct-winter-collerction">
	        <ul id="brand" class="product-list owl-carouse-advertisement-test  owl-loaded owl-drag ">
	    		@foreach($brands as $title => $brand)
	       			<li id="{{ $brand['id'] }}" class="brand_url_rewrite">
                   		<div class="product-grid">
                   			<ul class="clearfix">
                   				<a href="{{route('summary', 'br/'.$brand['id'])}}" target="_blank">
	                   				<li class="col-xs-12 col-sm-7 col-md-7">
	                   					<div class="zoom-container">
			                   				<figure class="zoom-image">
			                   					<img src="{{$brand['brandimg']}}">
			                   					<span class="helper"></span>
			                   				</figure>
		                   				</div>
							    	</li> 
							    	<li class="col-xs-5 col-sm-5 col-md-5 hidden-xs">
							    		<div class="zoom-container">
			                   				<figure class="zoom-image">
			                   					<img src="{{$brand['img']}}" >
			                   					<span class="helper"></span>
			                   				</figure>
		                   				</div> 
							    	</li>
						    	</a>
		    				</ul>
			    		</div>
			    		<div class="product-grid">
					        <span class="manufacturer">{{$brand['name']}}</span>
				    
			    			<span class="final-price box text-center">{{$brand['discount']}}</span>
		    			</div>
		    		</li>
		    	@endforeach		
	        </ul>
		</div>
	</div>
	@include('response.common.loading.md')
	@include('response.pages.benefit.sale.project.2018.spring.otherblock.country_discount_product')
</div>
<script>
	$('.owl-carouse-advertisement-test').addClass('owl-carousel').owlCarousel({
	center:true,
 	loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    touchDrag:false,
    slideBy : 1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});

	$('#discount_brand #brand .owl-nav').click(function(){
		var brand_url_rewrite = $('#country_discount #brand .center .brand_url_rewrite').attr('id');
		// var brand_name = $('#country_discount #brand .center .brand_url_rewrite span.manufacturer').text();
		// alert(brand_url_rewrite);
	       	$('#country_discount #product_brand').hide();
	       	$('#country_discount .loading-box').show();
    	 	var token = $('meta[name=csrf-token]').attr('content');

        	$.ajax({
        		url: "{!! route('benefit-getProductByBrand') !!}",
        		data:{'_token':token ,'brand_url_rewrite' : brand_url_rewrite},
        		type:"POST",
        		dataType:'JSON',
        		success:function(result){
        			$('#country_discount .loading-box').hide();
        			$('#country_discount #product_brand').replaceWith(result.html);
    				slider = $('#country_discount .owl-carouse-brand-product').addClass('owl-carousel').owlCarousel({
	                    loop:false,
	                    nav:true,
	                    margin:5,
	                    slideBy : 6,
	                    URLhashListener:true,
	                    startPosition: 'URLHash',
	                    responsive:{
	                        0:{
	                            items:2
	                        },
	                        600:{
	                            items:3
	                        },
	                        1000:{
	                            items:6
	                        }
	                    }
	                }).find("img").unveil().trigger("unveil");
        			$('#country_discount .owl-carouse-brand-product.owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
                    $('#country_discount .owl-carouse-brand-product.owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
                	var country_discount_url = "https://www.webike.tw/collection/brand/point-discount?rel=2018-07-2018Summer&sort=visits&limit=40&order=asc&br=" + brand_url_rewrite;
	                $('#country_discount #product_brand .country-discount-all-btn a').attr('href',country_discount_url);
        		},

        		error:function(xhr,ajaxOptions,thrownError,textStatus){
        			
        		}
        	});
        });

		
		
</script>