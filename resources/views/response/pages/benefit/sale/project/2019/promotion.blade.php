@extends('response.layouts.1column-2019bigpromo')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/benefit/sale/2019-promotion.css') }}">
<link rel="stylesheet" href="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.css') }}">
<style type="text/css">

</style>
@stop
@section('middle')
@include('response.pages.benefit.sale.project.2019.otherblock.main-banner')
<div id="search" class="search">
	<div class="block-container">
		<div class="search-btn">
			<input type="submit" value="">			
		</div>
		<div class="search-text">
			<input id="search_bar" type="text" name="search" autocomplete="off" placeholder="&#xF002; 請輸入關鍵字(54萬項商品、1840家廠牌)">
		</div>
	</div>
</div>
<div class="promotion-container">
	<div class="links-bar">
		<div>
			<ul class="clearfix">
				<li onclick="slipTo1('#week_sale')">
					<a href="javascript:void(0)" style="color: #fff;" >
						週次限定SALE品牌
					</a>
				</li>
				<li onclick="slipTo1('#country_discount')">
					<a href="javascript:void(0)" style="color: #fff;" >
						SPRING SALE 點數現折
					</a>
				</li>
				<li onclick="slipTo1('#genuineparts')">
					<a href="javascript:void(0)" style="color: #fff;" >
						正廠零件全面95折
					</a>
				</li>
				<li onclick="slipTo1('#category')">
					<a href="javascript:void(0)" style="color: #fff;" >
						分類搜尋
					</a>
				</li>
				<li onclick="slipTo1('#motor')">
					<a href="javascript:void(0)" style="color: #fff;" >
						車型搜尋
					</a>
				</li>
				<li onclick="slipTo1('#top100')">
					<a href="javascript:void(0)" style="color: #fff;" >
						熱銷品牌
					</a>
				</li>
				<li onclick="slipTo1('#webike-game')">
					<a href="javascript:void(0)" style="color: #fff;" >
						每日任務拿點數
					</a>
				</li>
			</ul>
		</div>
	</div>
<!-- 	<div class="promotion-schedule text-right">
		<a href="{{route('benefit-sale-2019-promotion-preview')}}">
			<i class="fa fa-calendar"> 查看各週優惠</i>
		</a>
	</div> -->
	@include('response.pages.benefit.sale.project.2019.otherblock.preview')
	<div class="page-block" id="genuineparts">
		<h2 class="page-block-title">正廠零件全面95折</h2>
		<div class="page-block-product">
			<p class="text-center">日、歐、美、台正廠零件全面5%OFF</p>
			<div class="block-product">
				<div class="product-banner">
					<a href="{{route('genuineparts').'?'.$rel_parameter}}" target="_blank">
						<img src="{!! assetRemote('image/benefit/big-promotion/2019/newyear/1901_genuine_370.jpg') !!}">
					</a>
				</div>
				<div class="text-center">
					<a class="promotion-btn" href="{{route('genuineparts').'?'.$rel_parameter}}" target="_blank">正廠零件查詢頁面</a>
				</div>
			</div>
		</div>
	</div>
	@include('response.pages.benefit.sale.project.2019.otherblock.category')
	<div class="page-block motor" id="motor">
		<h2 class="page-block-title">車型搜尋</h2>
		<div class="page-block-product">
			<p class="text-center">登錄MY BIKE找東西更方便</p>
			<div>
				<div class="ct-table-cell">
	                <ul class="ul-cell-dropdown-list container">
	                    <li class="li-cell-dropdown-item">
	                        <div class=" select-box">
	                            <select class="select2 select-motor-manufacturer">
	                                <option>請選擇廠牌</option>
	                                @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
	                                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
	                                @endforeach
	                            </select>
	                        </div>
	                    </li>
	                    <li class="li-cell-dropdown-item">
	                        <div class=" select-box">
	                            <select class="select2 select-motor-displacement">
	                                <option>cc數</option>
	                            </select>
	                        </div>
	                    </li>
	                    <li class="li-cell-dropdown-item">
	                        <div class=" select-box">
	                            <select class="select2 select-motor-model">
	                                <option>車型</option>
	                            </select>
	                        </div>
	                    </li>
	                    @include('response.common.list.filter-mybike')
	                </ul>
	            </div>
	            <div class="text-center">
	            	<a class="promotion-btn btn-disable" href="javascript:void(0)"  target="_blank">車型搜尋</a>
	            </div>
			</div>
		</div>
	</div>
	@include('response.pages.benefit.sale.project.2019.otherblock.top-20')
	<div class="page-block" id="webike-game"> 
		<h2 class="page-block-title">每日任務拿點數</h2>
		<div class="page-block-product">
			<p class="text-center">每日挑戰，答對即可獲得10元點數</p>
			<div class="block-product">
				<div class="product-banner">
					<a href="{{route('benefit-event-webikewanted').'?'.$rel_parameter}}" target="_blank">
						<img src="{!! assetRemote('image/benefit/big-promotion/2019/spring/1904_Wanted.jpg') !!}">
					</a>
				</div>
				<div class="text-center">
					<a class="promotion-btn" href="{{route('benefit-event-webikewanted').'?'.$rel_parameter}}" target="_blank">立即前往</a>
				</div>
			</div>
		</div>
	</div>
	@include('response.pages.benefit.sale.project.2019.otherblock.collection')
</div>
@stop
@section('script')
	<script type="text/javascript">
		
		$(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.ct-table-cell').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-model", function(){
            var value = $(this).find('option:selected').val();
            if ((value) && (value != "車型")){
                link = '{{route('parts')}}' + '/mt/' + value;
            	$('#motor .page-block-product .promotion-btn').removeClass('btn-disable').attr('href',link);
            }else{
            	$('#motor .page-block-product .promotion-btn').addClass('btn-disable').attr('href','javascript:void(0)');
            }
        });

        $(document).on('click', ".select-motor-mybike", function(){
            var value = $(this).attr('name');
            motor_name = $(this).text();
            if (value){
            	$('.btn-my-bike span').text(motor_name);
            	link = '{{route('parts')}}' + '/mt/' + value;
            	$('#motor .page-block-product .promotion-btn').removeClass('btn-disable').attr('href',link);
            }else{
            	$('#motor .page-block-product .promotion-btn').addClass('btn-disable').attr('href','javascript:void(0)');
            }
        });

        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 50;
            $('html,body').animate({scrollTop: y}, 400);
        }
	</script>
	<script type="text/javascript">
		$('.owl-carouse-product-loop').addClass('owl-carousel').owlCarousel({
			autoplayTimeout:1500,
			autoplayHoverPause:true,
			loop:true,
			margin:10,
			nav:false,
		    slideBy : 1,
		    center: true,
		    autoplay: true,
			autoPlaySpeed: 500,
			dots: true,
			autoplayHoverPause: false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});
		$(document).ready(function(){
			$('.product-list .loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
		});

		$(".page-block .product-list").mouseenter(function(){
		  $(this).find('.loop-photo').removeClass('hidden');
		  $(this).find('.loop-photo .owl-carouse-product-loop').trigger('play.owl.autoplay',[1500]);
		  $(this).addClass('active');
		  setTimeout(function(){ 
		  		$('.page-block .product-list.active .owl-carouse-product-loop li').removeClass('hidden');
		  }, 500);
		});

		$(".page-block .product-list").mouseleave(function(){
		  	$(this).find('.loop-photo').addClass('hidden');
		  	$(this).removeClass('active');
		  	$(this).find('.loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
		});


		$('.page-block-product .schedule-list .schedule-list-product').click(function(){
			$('.page-block-product .schedule-list .schedule-list-product').removeClass('active');
			$(this).addClass('active');
			data = $(this).attr('data');
			$('.page-block-product .week-page.active').addClass('hidden').removeClass('active');
			$('.page-block-product .week-page.' + data).addClass('active').removeClass('hidden');
			$('.page-block-product .week-page .country_discount').attr('id','');
			$('.page-block-product .week-page.active .country_discount').attr('id','country_discount');
		});
	</script>
     <script src="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>

        $(function() {
            $( "#search_bar" ).autocomplete({
                minLength: 1,
                source: solr_source,
                focus: function( event, ui ) {
                    $('#search_bar').val();
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'header');
                    return false;
                },
                change: function(event, ui) {

                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }
                
                return bag
                // .append( '<a href="http://localhost/test">' + item.icon + "<p>" + item.label + "</p></a>" )
                    .append( '<a  href="javascript:void(0)" class="search-keyword">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
        });

        
        var current_suggest_connection = null;
        function solr_source(request, response){
            var params = {q: request.term};

            current_suggest_connection = $.ajax({
                url: "{{ route('api-suggest')}}",
                method:'GET',
                data : params,
                dataType: "json",
                beforeSend: function( xhr ) {
                    if(current_suggest_connection){
                        current_suggest_connection.abort();
                    }
                },
                success: function(data) {
                    response(data);
                }
            });
        }
       
        $(document).on('click', ".ui-menu li.ui-menu-item  a.search-keyword", function(){
            var keyword = $(this).find('span.font-normal').text();
            $('.search-text #search_bar').val(keyword);
        });

        $('#search .search-btn input').click(function(){
        	keyword = $('.search-text #search_bar').val();
        	if(keyword){
        		window.location = '{{route('parts')}}' + '?q=' + keyword;
        	}
        });



    </script>
@stop