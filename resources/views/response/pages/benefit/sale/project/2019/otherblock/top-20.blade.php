@php
	$top_brands = [
		'300' => ['name' => 'HONDA' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_300.gif'],
		'704' => ['name' => 'SP武川' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_704.gif'],
		'854' => ['name' => 'YOSHIMURA' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_854.gif'],
		'541' => ['name' => 'OVER' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_541.gif'],
		'177' => ['name' => 'DAYTONA' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_177.gif'],
		'847' => ['name' => 'YAMAHA' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_847.gif'],
		'357' => ['name' => 'KITACO' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_357.gif'],
		'666' => ['name' => 'SHOEI' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_666.gif'],
		'364' => ['name' => 'KOMINE' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_364.gif'],
		'635' => ['name' => 'RS TAICHI' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_635.gif'],
		'2162' => ['name' => 'HONDA RIDING GEAR' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_2162.gif'],
		'49' => ['name' => 'Arai' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_49.gif'],
		'983' => ['name' => 'TANAX motofizz' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_983.gif'],
		'527' => ['name' => 'OGK KABUTO' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_527.gif'],
		't10' => ['name' => 'MOS' , 'image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t10.png'],
		't2514' => ['name' => 'DK design 達卡設計' , 'image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2514.png'],
		't2289' => ['name' => 'DOG HOUSE 惡搞手工廠' , 'image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2289.png'],
		'30' => ['name' => 'AGV' , 'image' => 'https://img.webike.net/sys_images/brand2/brand_30.gif'],
		't1961' => ['name' => 'ZEUS 瑞獅' , 'image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1961.png'],
		't2223' => ['name' => 'BENKIA' , 'image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2223.png']
		];
@endphp
<div class="page-block" id="top100">
	<h2 class="page-block-title">熱銷品牌</h2>
	<div class="page-block-product">
		<p class="text-center">TOP100品牌等你探索</p>
		<div class="block-product">
			<ul class="clearfix ul-list">
				@foreach($top_brands as $brand_url => $top_brand)
					<li class="product-list">
						<a href="{{route('parts','br/'.$brand_url).'?'.$rel_parameter}}" target="_blank">
							<div class="product-img">
								<img src="{{$top_brand['image']}}">
							</div>
						</a>
						<div class="product-text text-center">
							<span class="dotted-text width-full">{{$top_brand['name']}}</span>
						</div>
					</li>
				@endforeach
			</ul>
		</div>
        <div class="text-center btn-gap-top clearfix">
			<a class="promotion-btn" href="{{route('benefit-sale-2019-promotion-brand')}}">查看全部TOP100品牌</a>
		</div>
	</div>
</div>