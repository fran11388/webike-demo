<style type="text/css">
		.collection{
		margin-top: 40px; 
	}
	.collection .page-block-title{
		position: unset!important;
	}
	.collection .page-block-product{
		overflow: auto!important;
		max-width: 100%!important;
		min-width: 100%!important;
		border-radius: 0px!important;
	}
	.collection .page-block-product .block-product{
		max-width: 1280px;
		min-width: 1000px;
		margin: 10px auto 0px;
	}
	.collection .collection-text .dotted-text{
		width: 100%;
	    color: #910000;
	}
</style>
<div class="page-block collection">
	<div class="page-block-product">
		<h2 class="page-block-title">推薦特輯</h2>
		<div class="block-product">
			<ul class="clearfix">
				@foreach($collections as $key => $collection)
					<li class="collection-list col-md-6 col-lg-6">
						<div class="collection-list-product">
							<a href="{{$collection->link.'?'.$rel_parameter}}" title="{{$collection->name.$tail}}" target="_blank" class="clearfix">
								<div class="collection-img">
									<figure class="zoom-image thumb-img">
										<img src="{!! $collection->banner !!}" alt="{{$collection->name.$tail}}">
									</figure>
								</div>
							</a>
							<div class="collection-text">
								<span class="dotted-text">{{$collection->meta_description}}</span>
							</div>
						</div>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>