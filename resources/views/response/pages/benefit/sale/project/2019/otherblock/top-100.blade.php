@php
	$top_brands = [
			'300' => ['name' => 'HONDA','image' => 'https://img.webike.net/sys_images/brand2/brand_300.gif'],
			'704' => ['name' => 'SP武川','image' => 'https://img.webike.net/sys_images/brand2/brand_704.gif'],
			'854' => ['name' => 'YOSHIMURA','image' => 'https://img.webike.net/sys_images/brand2/brand_854.gif'],
			'541' => ['name' => 'OVER','image' => 'https://img.webike.net/sys_images/brand2/brand_541.gif'],
			'177' => ['name' => 'DAYTONA','image' => 'https://img.webike.net/sys_images/brand2/brand_177.gif'],
			'847' => ['name' => 'YAMAHA','image' => 'https://img.webike.net/sys_images/brand2/brand_847.gif'],
			'357' => ['name' => 'KITACO','image' => 'https://img.webike.net/sys_images/brand2/brand_357.gif'],
			'304' => ['name' => 'HURRICANE','image' => 'https://img.webike.net/sys_images/brand2/brand_304.gif'],
			'354' => ['name' => 'KIJIMA','image' => 'https://img.webike.net/sys_images/brand2/brand_354.gif'],
			'625' => ['name' => 'RK','image' => 'https://img.webike.net/sys_images/brand2/brand_625.gif'],
			'529' => ['name' => 'OHLINS','image' => 'https://img.webike.net/sys_images/brand2/brand_529.gif'],
			'345' => ['name' => 'KAWASAKI','image' => 'http://img.webike.tw/assets/images/brands/promotion/brand_345.png'],
			'2095' => ['name' => 'KTM POWER PARTS','image' => 'https://img.webike.net/sys_images/brand2/brand_2095.gif'],
			'339' => ['name' => 'K-FACTORY','image' => 'https://img.webike.net/sys_images/brand2/brand_339.gif'],
			'72' => ['name' => 'BABYFACE','image' => 'https://img.webike.net/sys_images/brand2/brand_72.gif'],
			'575' => ['name' => 'POSH','image' => 'https://img.webike.net/sys_images/brand2/brand_575.gif'],
			'411' => ['name' => 'Magical Racing','image' => 'https://img.webike.net/sys_images/brand2/brand_411.gif'],
			'732' => ['name' => 'SUNSTAR','image' => 'https://img.webike.net/sys_images/brand2/brand_732.gif'],
			'249' => ['name' => 'G-Craft','image' => 'https://img.webike.net/sys_images/brand2/brand_249.gif'],
			'865' => ['name' => 'ZETA','image' => 'https://img.webike.net/sys_images/brand2/brand_865.gif'],
			'147' => ['name' => 'CHIC DESIGN','image' => 'https://img.webike.net/sys_images/brand2/brand_147.gif'],
			'937' => ['name' => '才谷屋','image' => 'https://img.webike.net/sys_images/brand2/brand_937.gif'],
			'721' => ['name' => 'STRIKER','image' => 'https://img.webike.net/sys_images/brand2/brand_721.gif'],
			'1963' => ['name' => 'IMPACT','image' => 'https://img.webike.net/sys_images/brand2/brand_1963.gif'],
			'644' => ['name' => 'r’s gear','image' => 'https://img.webike.net/sys_images/brand2/brand_644.gif'],
			'29' => ['name' => 'AGRAS','image' => 'https://img.webike.net/sys_images/brand2/brand_29.gif'],
			'10' => ['name' => 'A-TECH','image' => 'https://img.webike.net/sys_images/brand2/brand_10.gif'],
			'2097' => ['name' => 'ENDURANCE','image' => 'https://img.webike.net/sys_images/brand2/brand_2097.gif'],
			'1302' => ['name' => 'T-REV','image' => 'https://img.webike.net/sys_images/brand2/brand_1302.gif'],
			'35' => ['name' => 'AKRAPOVIC','image' => 'https://img.webike.net/sys_images/brand2/brand_35.gif'],
			'1253' => ['name' => 'Puig','image' => 'https://img.webike.net/sys_images/brand2/brand_1253.gif'],
			'290' => ['name' => 'HEPCO＆BECKER','image' => 'https://img.webike.net/sys_images/brand2/brand_290.gif'],
			'666' => ['name' => 'SHOEI','image' => 'https://img.webike.net/sys_images/brand2/brand_666.gif'],
			'364' => ['name' => 'KOMINE','image' => 'https://img.webike.net/sys_images/brand2/brand_364.gif'],
			'635' => ['name' => 'RS TAICHI','image' => 'https://img.webike.net/sys_images/brand2/brand_635.gif'],
			'2162' => ['name' => 'HONDA RIDING GEAR','image' => 'https://img.webike.net/sys_images/brand2/brand_2162.gif'],
			'49' => ['name' => 'Arai','image' => 'https://img.webike.net/sys_images/brand2/brand_49.gif'],
			'983' => ['name' => 'TANAX motofizz','image' => 'https://img.webike.net/sys_images/brand2/brand_983.gif'],
			'527' => ['name' => 'OGK KABUTO','image' => 'https://img.webike.net/sys_images/brand2/brand_527.gif'],
			'179' => ['name' => 'DEGNER','image' => 'https://img.webike.net/sys_images/brand2/brand_179.gif'],
			'632' => ['name' => 'ROUGH＆ROAD','image' => 'https://img.webike.net/sys_images/brand2/brand_632.gif'],
			'298' => ['name' => 'HJC','image' => 'https://img.webike.net/sys_images/brand2/brand_298.gif'],
			'289' => ['name' => 'HenlyBegins','image' => 'https://img.webike.net/sys_images/brand2/brand_289.gif'],
			'740' => ['name' => 'SUZUKI','image' => 'https://img.webike.net/sys_images/brand2/brand_740.gif'],
			'1776' => ['name' => 'PREMIER','image' => 'https://img.webike.net/sys_images/brand2/brand_1776.gif'],
			'284' => ['name' => 'HARLEY-DAVIDSON','image' => 'https://img.webike.net/sys_images/brand2/brand_284.gif'],
			'1859' => ['name' => 'DOPPELGANGER OUTDOOR','image' => 'https://img.webike.net/sys_images/brand2/brand_1859.gif'],
			'2123' => ['name' => 'SCORPION','image' => 'https://img.webike.net/sys_images/brand2/brand_2123.gif'],
			'2480' => ['name' => 'RST','image' => 'https://img.webike.net/sys_images/brand2/brand_2480.gif'],
			'40' => ['name' => 'alpinestars','image' => 'https://img.webike.net/sys_images/brand2/brand_40.gif'],
			'267' => ['name' => 'GOLDWIN','image' => 'https://img.webike.net/sys_images/brand2/brand_267.gif'],
			'1902' => ['name' => 'TSDESIGN','image' => 'https://img.webike.net/sys_images/brand2/brand_1902.gif'],
			'340' => ['name' => 'KADOYA','image' => 'https://img.webike.net/sys_images/brand2/brand_340.gif'],
			't10' => ['name' => 'MOS','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t10.png'],
			't2514' => ['name' => 'DK design 達卡設計','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2514.png'],
			't2289' => ['name' => 'DOG HOUSE 惡搞手工廠','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2289.png'],
			'2030' => ['name' => 'KOSO','image' => 'https://img.webike.net/sys_images/brand2/brand_2030.gif'],
			't2198' => ['name' => 'ST.LUN 聖崙','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2198.png'],
			't3227' => ['name' => 'MOTONE','image' => 'http://img.webike.tw/brand/brand_t3227_2.jpg'],
			'1624' => ['name' => 'SIMOTA','image' => 'https://img.webike.net/sys_images/brand2/brand_1624.gif'],
			't38' => ['name' => 'WUKAWA','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t38.png'],
			't1914' => ['name' => 'EPIC','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1914.png'],
			't2222' => ['name' => '庫帆GarageSaiL','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2222.png'],
			't2758' => ['name' => 'WIZH 欣炫','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2758.png'],
			'501' => ['name' => 'NHRC','image' => 'https://img.webike.net/sys_images/brand2/brand_501.gif'],
			't1994' => ['name' => 'WRRP','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1994.png'],
			'2072' => ['name' => 'DART','image' => 'https://img.webike.net/sys_images/brand2/brand_2072.gif'],
			'1916' => ['name' => 'NCY','image' => 'https://img.webike.net/sys_images/brand2/brand_1916.gif'],
			'260' => ['name' => 'GIVI','image' => 'https://img.webike.net/sys_images/brand2/brand_260.gif'],
			'30' => ['name' => 'AGV','image' => 'https://img.webike.net/sys_images/brand2/brand_30.gif'],
			't1961' => ['name' => 'ZEUS 瑞獅','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1961.png'],
			't2223' => ['name' => 'BENKIA','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2223.png'],
			't2570' => ['name' => 'AUGI','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2570.png'],
			't2012' => ['name' => 'SOL','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2012.png'],
			't1895' => ['name' => 'SPEED-R','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1895.png'],
			't1886' => ['name' => 'EXUSTAR','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1886.png'],
			't1828' => ['name' => 'ASTONE','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1828.png'],
			't2452' => ['name' => 'HIGHWAY 1','image' => 'http://img.webike.tw/brand/brand_t2452_2.jpg'],
			't2010' => ['name' => 'THH','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2010.png'],
			't1940' => ['name' => 'M2R','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1940.png'],
			't2476' => ['name' => 'Louis','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2476.png'],
			't2198' => ['name' => 'ST.LUN 聖崙','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t2198.png'],
			'1860' => ['name' => 'Webike Garage','image' => 'https://img.webike.net/sys_images/brand2/brand_1860.gif'],
			't1918' => ['name' => '良輝','image' => 'https://img.webike.tw/assets/images/brands/promotion/brand_t1918.png'],
			'1319' => ['name' => 'STOMPGRIP','image' => 'https://img.webike.net/sys_images/brand2/brand_1319.gif'],
			'541' => ['name' => 'OVER','image' => 'https://img.webike.net/sys_images/brand2/brand_541.gif'],
			'2901' => ['name' => 'LEON','image' => 'https://img.webike.net/sys_images/brand2/brand_2901.gif'],
			't1997' => ['name' => 'CODO','image' => 'http://img.webike.tw/brand/brand_t1997_2.jpg'],
			'2902' => ['name' => 'SRC','image' => 'https://img.webike.net/sys_images/brand2/brand_2902.gif'],
			'1850' => ['name' => 'ICON','image' => 'https://img.webike.net/sys_images/brand2/brand_1850.gif'],
			'2026' => ['name' => 'DAINESE','image' => 'https://img.webike.net/sys_images/brand2/brand_2026.gif'],
			'386' => ['name' => 'LEAD','image' => 'https://img.webike.net/sys_images/brand2/brand_386.gif'],
			'1930' => ['name' => 'KTM','image' => 'https://img.webike.net/sys_images/brand2/brand_1930.gif'],
			'697' => ['name' => 'SPIDI','image' => 'https://img.webike.net/sys_images/brand2/brand_697.gif'],
			'2494' => ['name' => 'BELL','image' => 'https://img.webike.net/sys_images/brand2/brand_2494.gif'],
			'2224' => ['name' => 'SPEED AND STRENGTH','image' => 'https://img.webike.net/sys_images/brand2/brand_2224.gif'],
			'368' => ['name' => 'KOWA','image' => 'https://img.webike.net/sys_images/brand2/brand_368.gif'],
			'673' => ['name' => 'SK11','image' => 'https://img.webike.net/sys_images/brand2/brand_673.gif'],
			'589' => ['name' => 'PROXXON','image' => 'https://img.webike.net/sys_images/brand2/brand_589.gif'],
			'372' => ['name' => 'KTC','image' => 'https://img.webike.net/sys_images/brand2/brand_372.gif']
		];
@endphp
<div class="page-block" id="top100">
	<h2 class="page-block-title">熱銷品牌</h2>
	<div class="page-block-product">
		<p class="text-center">TOP100品牌等你探索</p>
		<div class="block-product">
			<ul class="clearfix ul-list">
				@foreach($top_brands as $brand_url => $top_brand)
					<li class="product-list">
						<a href="{{route('parts','br/'.$brand_url).'?'.$rel_parameter}}" target="_blank">
							<div class="product-img">
								<img src="{{$top_brand['image']}}">
							</div>
						</a>
						<div class="product-text text-center ">
							<span class="dotted-text width-full">{{$top_brand['name']}}</span>
						</div>
					</li>
				@endforeach
			</ul>
		</div>
        <div class="text-center btn-gap-top clearfix">
			<a class="promotion-btn" href="{{route('benefit-sale-2019-promotion')}}">回主頁</a>
		</div>
	</div>
</div>