@extends('response.layouts.1column-2019bigpromo')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/benefit/sale/2019-promotion.css') }}">
@stop
@section('middle')
	@include('response.pages.benefit.sale.project.2019.otherblock.main-banner')
	@include('response.pages.benefit.sale.project.2019.otherblock.top-100')
	@include('response.pages.benefit.sale.project.2019.otherblock.collection')
@stop
@section('script')
@stop