	<div class="page-block">
		<h2 class="page-block-title" id="week_sale">週次限定SALE品牌</h2>
		<div class="page-block-product">
			<p class="text-center">3/29-5/2 專屬折扣品牌</p>
			<div class="schedule-list">
				<ul class="clearfix">
					@php
						$weeks = ['3/28-4/4','4/5-4/11','4/12-4/18','4/19-4/25','4/26-5/2'];
						$weeks_day = ['第一週','第二週','第三週','第四週','第五週'];
					@endphp
					@foreach($weeks as $key => $week)
						<li class="schedule-list-product {{$key == $week_key ? 'active': '' }}" data="week_{{$key}}">
							<a href="javascript:void(0)">
								<span>{{$week}}</span>
								<span>{{$weeks_day[$key]}}</span>
							</a>
						</li>
					@endforeach
				</ul>
			</div>
			@foreach($week_sale_brands as $page_key => $week_sale_brand)
				<div class="week-page {{$page_key == $week_key ? 'active': 'hidden'}} week_{{$page_key}}">
					<div class="block-product">
						<ul class="clearfix">
							@foreach($week_sale_brand as $week_sale_brand_url_rewrite => $sale_brand)
								<li class="product-list li-list">
									<a href="{{route('parts','br/'.$week_sale_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
										<div class="product-img">
											<img src="{{$sale_brand['brand_image']}}">
										</div>
									</a>
									<div class="product-text text-center">
										<span>週次限定優惠</span>
									</div>
									<div class="loop-photo hidden">
										<h2>{{$sale_brand['brand_name']}}</h2>
										<ul class="owl-carouse-product-loop">
											@foreach($sale_brand['product_image'] as $sale_image_key => $sale_product_image)
												<li class="text-center {{$sale_image_key == 0 ? ' ': 'hidden'}}">
													<img src="{{ $sale_product_image ? $sale_product_image : NO_IMAGE}}">
												</li>
											@endforeach
										</ul>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
					 <div class="block-product country_discount" id="{{$page_key == $week_key ? 'country_discount': ''}}">
					 	<h2 class="page-block-title2">SPRING SALE 點數現折</h2>
					 	<p class="row-advertisement">3/29-5/2 品牌點數加倍，直接折扣</p>
						<ul class="clearfix">
							@foreach($week_discount_brands[$page_key] as $week_discount_brands_url_rewite =>  $discount_brand)
								<li class="product-list li-list">
									<a href="{{route('parts','br/'.$week_discount_brands_url_rewite).'?'.$rel_parameter}}" target="_blank">
										<div class="product-img">
											<img src="{{$discount_brand['brand_image']}}">
										</div>
									</a>
									<div class="product-text text-center">
										<span>點數2倍現折</span>
									</div>
									<div class="loop-photo hidden">
										<h2>{{$discount_brand['brand_name']}}</h2>
										<ul class="owl-carouse-product-loop">
											@foreach($discount_brand['product_image'] as $discount_image_key => $discount_product_image)
												<li class="text-center {{$discount_image_key == 0 ? ' ': 'hidden'}}">
													<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
												</li>
											@endforeach
										</ul>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
				</div> 
			@endforeach
		</div>
	</div>