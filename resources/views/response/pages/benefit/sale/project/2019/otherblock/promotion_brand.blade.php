<div class="page-block" id="week_sale">
		<h2 class="page-block-title">本週嚴選 SALE品牌</h2>
		<div class="page-block-product">
			<p class="text-center">{{$start_day}}-{{$end_day}} 專屬折扣品牌</p>
			<div class="block-product">
				<ul class="clearfix">
					@foreach($sale_brands as $sale_brand_url_rewrite => $sale_brand)
						<li class="product-list li-list" >
							<div>
								<a href="{{route('summary', 'br/'.$sale_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
									<div class="product-img">
										<img src="{{$sale_brand['brand_image']}}">
									</div>
								</a>
								<div class="product-text text-center">
									<span>最大88折優惠</span>
								</div>
								<div class="product-tag">
									@foreach($sale_brand['ca_name'] as $sale_ca_key => $sale_categorie)
										@php
											$path = explode('-',$sale_categorie->url_path);
											$tag_class = 'label-primary';
											switch ($path['0']) {
												case '3000':
													$tag_class = 'label-primary';
												break;
												case '1000':
													$tag_class = 'label-danger';
												break;
												case '8000':
													$tag_class = 'label-warning';
												break;
												case '4000':
													$tag_class = 'label-warning';
												break;
												default:
													$tag_class = 'label-primary';
												break;
											}
										@endphp
										<span class="dotted-text tag label {{$tag_class}} {{$sale_ca_key == 2 ? 'hidden': ' ' }}">{{$sale_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$sale_categorie->name}}</span>
									@endforeach
								</div>
							</div>
							<div class="loop-photo hidden">
								<h2>{{$sale_brand['brand_name']}}</h2>
								<ul class="owl-carouse-product-loop">
									@foreach($sale_brand['product_image'] as $sale_image_key => $sale_product_image)
										<li class="text-center {{$sale_image_key == 0 ? ' ': 'hidden'}}">
											<img src="{{ $sale_product_image ? $sale_product_image : NO_IMAGE}}">
										</li>
									@endforeach
								</ul>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="page-block" id="country_discount">
		<h2 class="page-block-title">New Year Sale 點數現折</h2>
		<div class="page-block-product">
			<p class="text-center">{{$start_day}}-{{$end_day}} 點數加倍現折品牌</p>
			<div class="block-product">
				<ul class="clearfix">
					@foreach($discount_brands as $discount_brand_url_rewrite => $discount_brand)
						<li class="product-list li-list">
							<a href="{{route('summary', 'br/'.$discount_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
								<div class="product-img">
									<img src="{{$discount_brand['brand_image']}}">
								</div>
								<div class="product-text text-center">
									<span>點數2倍現折</span>
								</div>
							</a>
							<div class="product-tag">
								@foreach($discount_brand['ca_name'] as $discount_ca_key => $discount_categorie)
									@php
										$path = explode('-',$discount_categorie->url_path);
										$tag_class = 'label-primary';
										switch ($path['0']) {
											case '3000':
												$tag_class = 'label-primary';
											break;
											case '1000':
												$tag_class = 'label-danger';
											break;
											case '8000':
												$tag_class = 'label-warning';
											break;
											case '4000':
												$tag_class = 'label-warning';
											break;
											default:
												$tag_class = 'label-primary';
											break;
										}
									@endphp
									<span class="dotted-text tag label {{$tag_class}} {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$discount_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$discount_categorie->name}}</span>
								@endforeach
							</div>
							<div class="loop-photo hidden">
								<h2>{{$discount_brand['brand_name']}}</h2>
								<ul class="owl-carouse-product-loop">
									@foreach($discount_brand['product_image'] as $discount_image_key => $discount_product_image)
										<li class="text-center {{$discount_image_key == 0 ? ' ': 'hidden'}}">
											<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
										</li>
									@endforeach
								</ul>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div> 