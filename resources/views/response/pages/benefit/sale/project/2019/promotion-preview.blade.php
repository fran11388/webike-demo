@extends('response.layouts.1column-2019bigpromo')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/benefit/sale/2019-promotion.css') }}">
@stop
@section('middle')
	@include('response.pages.benefit.sale.project.2019.otherblock.main-banner')
	<div class="page-block">
		<h2 class="page-block-title">本週嚴選 SALE品牌</h2>
		<div class="page-block-product">
			<div class="schedule-list">
				<ul class="clearfix">
					@php
						$weeks = ['12/28-1/3','1/4-1/10','1/11-1/17','1/18-1/24','1/25-1/31'];
						$weeks_day = ['第一週','第二週','第三週','第四週','第五週'];
					@endphp
					@foreach($weeks as $key => $week)
						<li class="schedule-list-product {{$key == $week_key ? 'active': '' }}" data="week_{{$key}}">
							<a href="javascript:void(0)">
								<span>{{$week}}</span>
								<span>{{$weeks_day[$key]}}</span>
							</a>
						</li>
					@endforeach
				</ul>
			</div>
			@foreach($week_sale_brands as $page_key => $week_sale_brand)
				<div class="week-page {{$page_key == $week_key ? 'active': 'hidden'}} week_{{$page_key}}">
					<div class="block-product">
						<h3 class="product-title">優惠品牌</h3>
						<ul class="clearfix">
							@foreach($week_sale_brand as $week_sale_brand_url_rewrite => $sale_brand)
								<li class="product-list li-list">
									<a href="{{route('summary','br/'.$week_sale_brand_url_rewrite).'?'.$rel_parameter}}" target="_blank">
										<div class="product-img">
											<img src="{{$sale_brand['brand_image']}}">
										</div>
									</a>
									<div class="product-text text-center">
										<span>最大88折優惠</span>
									</div>
									<div class="product-tag">
										@foreach($sale_brand['ca_name'] as $sale_ca_key => $sale_categorie)
											@php
												$path = explode('-',$sale_categorie->url_path);
												$tag_class = 'label-primary';
												switch ($path['0']) {
													case '3000':
														$tag_class = 'label-primary';
													break;
													case '1000':
														$tag_class = 'label-danger';
													break;
													case '8000':
														$tag_class = 'label-warning';
													break;
													case '4000':
														$tag_class = 'label-warning';
													break;
													default:
														$tag_class = 'label-primary';
													break;
												}
											@endphp
											<span class="dotted-text tag label {{$tag_class}} {{$sale_ca_key == 2 ? 'hidden': ' ' }}">{{$sale_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$sale_categorie->name}}</span>
										@endforeach
									</div>
									<div class="loop-photo hidden">
										<h2>{{$sale_brand['brand_name']}}</h2>
										<ul class="owl-carouse-product-loop">
											@foreach($sale_brand['product_image'] as $sale_image_key => $sale_product_image)
												<li class="text-center {{$sale_image_key == 0 ? ' ': 'hidden'}}">
													<img src="{{ $sale_product_image ? $sale_product_image : NO_IMAGE}}">
												</li>
											@endforeach
										</ul>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
					 <div class="block-product">
						<h3 class="product-title">2倍點數現折品牌</h3>
						<ul class="clearfix">
							@foreach($week_discount_brands[$page_key] as $week_discount_brands_url_rewite =>  $discount_brand)
								<li class="product-list li-list">
									<a href="{{route('summary','br/'.$week_discount_brands_url_rewite).'?'.$rel_parameter}}" target="_blank">
										<div class="product-img">
											<img src="{{$discount_brand['brand_image']}}">
										</div>
									</a>
									<div class="product-text text-center">
										<span>點數2倍現折</span>
									</div>
									<div class="product-tag">
										@foreach($discount_brand['ca_name'] as $discount_ca_key => $discount_categorie)
											@php
												$path = explode('-',$discount_categorie->url_path);
												$tag_class = 'label-primary';
												switch ($path['0']) {
													case '3000':
														$tag_class = 'label-primary';
													break;
													case '1000':
														$tag_class = 'label-danger';
													break;
													case '8000':
														$tag_class = 'label-warning';
													break;
													case '4000':
														$tag_class = 'label-warning';
													break;
													default:
														$tag_class = 'label-primary';
													break;
												}
											@endphp
											<span class="dotted-text dotted-text tag label {{$tag_class}} {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$discount_categorie->name == "各種電子機器固定座・支架" ? "各種電子機器固定座" :$discount_categorie->name}}</span>
										@endforeach
									</div>
									<div class="loop-photo hidden">
										<h2>{{$discount_brand['brand_name']}}</h2>
										<ul class="owl-carouse-product-loop">
											@foreach($discount_brand['product_image'] as $discount_image_key => $discount_product_image)
												<li class="text-center {{$discount_image_key == 0 ? ' ': 'hidden'}}">
													<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
												</li>
											@endforeach
										</ul>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
				</div> 
			@endforeach
			<div class="text-center">
				<a class=" promotion-btn" href="{{route('benefit-sale-2019-promotion')}}">回主頁</a>
			</div>
		</div>
	</div>
	@include('response.pages.benefit.sale.project.2019.otherblock.collection')
@stop
@section('script')
	<script type="text/javascript">
		$(".page-block .product-list").mouseenter(function(){
		  $(this).find('.loop-photo').removeClass('hidden');
		  $(this).addClass('active');
		  setTimeout(function(){ 
		  		$('.page-block .product-list.active .owl-carouse-product-loop li').removeClass('hidden');
		  }, 500);
		});
		$(".page-block .product-list").mouseleave(function(){
		  	$(this).find('.loop-photo').addClass('hidden');
		  	$(this).removeClass('active');
		});
		$('.page-block-product .schedule-list .schedule-list-product').click(function(){
			$('.page-block-product .schedule-list .schedule-list-product').removeClass('active');
			$(this).addClass('active');
			data = $(this).attr('data');
			$('.page-block-product .week-page.active').addClass('hidden').removeClass('active');
			$('.page-block-product .week-page.' + data).addClass('active').removeClass('hidden');
		});
	</script>
	<script type="text/javascript">
		$('.owl-carouse-product-loop').addClass('owl-carousel').owlCarousel({
			autoplayTimeout:1000,
			autoplayHoverPause:true,
			loop:true,
			margin:10,
			nav:false,
		    slideBy : 1,
		    center: true,
		    autoplay: true,
			autoPlaySpeed: 500,
			dots: true,
			autoplayHoverPause: false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});
		$(document).ready(function(){
			$('.product-list .loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
		});

		$(".page-block .product-list").mouseenter(function(){
		  $(this).find('.loop-photo').removeClass('hidden');
		  $(this).find('.loop-photo .owl-carouse-product-loop').trigger('play.owl.autoplay',[1000]);
		  $(this).addClass('active');
		  setTimeout(function(){ 
		  		$('.page-block .product-list.active .owl-carouse-product-loop li').removeClass('hidden');
		  }, 500);
		});

		$(".page-block .product-list").mouseleave(function(){
		  	$(this).find('.loop-photo').addClass('hidden');
		  	$(this).removeClass('active');
		  	$(this).find('.loop-photo .owl-carouse-product-loop').trigger('stop.owl.autoplay');
		});
	</script>
@stop