@extends('response.layouts.1column-bigpromo')
@section('style')
<link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/brand.css') !!}">
<link href="{{assetRemote('css/pages/celebration/big-promotion/category.css')}}" rel="stylesheet" type="text/css">
<style>
	.big-promotion .promotion-container {
		background-color: #c81727;
	}
	#recommend {
		padding:0px ;
	}
	#recommend li{
		background-color: white !important;
	}
	#recommend .recommendImage {
		height:140px;
		display:block;
	}
	#recommend .title-product-item {
		display:block;
		padding:10px 0 0 0;
	}
	#recommend .recommendZoom {
		height: 186px;
	    vertical-align: middle;
	    text-align: center;
	    position: relative;
	    display: block;
	    font-size: 0;
	    margin: 0 auto;
	}
	.discountBanner {
		margin:40px 0px;
	}
	.discountImage figure{
		border:2px solid #ccc;
		height:130px !important;
		padding:20px !important;
	}
	.discount-container {
		padding:20px !important;
	}
	.item-title {
		display:block;
		margin-bottom:10px;
	}
	.box-content {
		margin-bottom:0px;
	}
</style>
@stop
@section('middle')
@include('response.pages.benefit.sale.project.2017.otherblock.main-banner')
@include('response.pages.benefit.sale.project.2017.otherblock.searchbar')
@include('response.pages.benefit.sale.project.2017.otherblock.main-category-link')
<div class="container">
	<div class="box-fix-auto">
		<div class="discountBanner text-center">
	        <a href="javascript:void(0)" onclick="slipTo2(A)">
	            <img src="{{assetRemote('image/benefit/big-promotion/specialdiscount.png')}}">
	        </a>
	    </div>
        <div class="brand-table-container">
            <ul class="brand-info-table">
                <li class="brand-table-content">
                    <ul>
                        <li class="table-content-title col-md-3 col-sm-3 col-xs-3">
                            <span>字首排列</span>
                        </li>
                        <li class=" table-content-list col-md-9 col-sm-9 col-xs-9">
                            <ul>
                            	<li><a href="{{modifyGetParameters(['char'=>''])}}">全部</a></li>
                                @foreach($manufacturers_keys as $manufacturers_key)
                                    <li><a href="javascript:void(0)" onclick="slipTo1({{$manufacturers_key}})">{{$manufacturers_key}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        @if(count($manufacturers))
    		@foreach($manufacturers as $char => $manufacturer)
    			@if(Request::get('char'))
	        		@php
						$manufacturer_str = Request::get('char'); 
						$manufacturer = $manufacturers[$manufacturer_str];
						$char = $manufacturer_str;
					@endphp
				@endif
				<div id="{{$char}}">
			        <div id="recommend" class="section clearfix">
						<div id="item1" class="ct-sale-page form-group">
							<div class="title width-full text-center">
								<h2>{{$char}}字首</h2>
							</div>
						    <div class="box-content">
						        <div class="ct-riding-gear width-full">
						            <ul class="product-list owl-carousel-promo clearfix row discount-container">
						                @foreach($manufacturer as $key => $node)
						                <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3 text-center discountImage">
						                    <a href="{{ route('parts',['section' => 'br/'.$node->url_rewrite.'?fl=special-price' ]) }}" title="{{ $node->name . $tail }}" class="recommendImage" target="_blank">
												<figure class="zoom-image recommendZoom">
													@php
														$img = 'https://img.webike.net/sys_images/brand2/brand_'.$node->url_rewrite.'.gif';
													@endphp
													{!! lazyImage( $img , $node->name . '('  . $node->count . ')' )  !!}
													<span class="helper"></span>
												</figure>
											</a>
											<div class="block">
												<a class="item-title dotted-text2 size-10rem" href="{{ route('parts',['section' => 'br/'.$node->url_rewrite.'?fl=special-price' ]) }}" title="{{ $node->name . $tail }}" target="_blank">{{ $node->name }}</a>
												<span class="font-color-red font-bold size-10rem">{{ $node->count }}個商品</span>
											</div>
						                </li>
						                @endforeach 
						            </ul>
						        </div>
							</div>
						</div>
					</div>
				</div>
				@if(Request::get('char'))
					@break
				@endif
			@endforeach
		@endif
    </div>
</div>
@stop
@section('script')
	<script>
		function slipTo1(element){
		    var y = parseInt($(element).offset().top) - 115;
		    $('html,body').animate({scrollTop: y}, 400);
		}
		function slipTo2(element){
		    var y = parseInt($(element).offset().top) - 170;
		    $('html,body').animate({scrollTop: y}, 400);
		}
	</script>
@stop