@extends('response.layouts.1column-bigpromo')
@section('style')
	<style>
		@media (min-width: 768px) {
			.navbar-nav {
				width:100% !important;
			}
			.navbar-nav li {
				width:9%;
				text-align: center;
				font-size:1rem;
			}
			.promotion-title {
				background: url({{assetRemote('image/benefit/big-promotion/2017_banner_head.png')}}) repeat-x #fc37a2;
				padding-top:100px;
			}
		}
		.rider {
			background: url({{assetRemote('image/benefit/big-promotion/rider.png')}}) no-repeat;
			min-height: 110px;
			background-size: 87%;
			margin-left:auto;
			margin-right:auto;
		}
		.rider:hover {
			background: url({{assetRemote('image/benefit/big-promotion/rider.png')}}) no-repeat;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/celebration/big-promotion/home.css') }}">
@stop
@section('middle')
{{-- <div class="promotion-title"></div> --}}
@include('response.pages.benefit.sale.project.2017.otherblock.main-banner')
@include('response.pages.benefit.sale.project.2017.otherblock.searchbar')
@include('response.pages.benefit.sale.project.2017.otherblock.main-category-link')
<div class="container">
	@if($type == '2017-spring')
		<div id="recommend" class="section clearfix">
			<div id="item1" class="ct-sale-page form-group" style="border: 1px solid #b7b7b7;">
				<div class="title col-xs-12 col-sm-12 col-md-12 text-center">
					<h2>每日推薦商品</h2>
				</div>
				<div class="content-box-sale">
					<ul class="product-sale">
						@foreach($recommends as $recommend_product)
							<li class="item item-product-grid thumb-box-border col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-12   ">
								<a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
									<figure class="zoom-image">
										<img src="{{$recommend_product->image}}" alt="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}">
									</figure>
								</a>
								<a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" class="title-product-item" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
									<span class="bold-text dotted-text2 force-limit">{{$recommend_product->name}}</span>
									<span class="normal-text dotted-text1">{{$recommend_product->manufacturer_name}}</span>
								</a>
								@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
									<label class="price font-bold">NT${{number_format($recommend_product->price,0, '.' ,',')}} (最大{{number_format($recommend_product->d_price)}}%off)</label>
								@else
									<label class="price font-bold">NT${{number_format($recommend_product->price,0, '.' ,',')}}(<?php if($recommend_product->point_type == 0){echo ' 最大'.number_format($recommend_product->c_price).'%off)';}else{ echo number_format($recommend_product->c_point).'倍點數現折)';}?></label>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	@endif
	@if($type == '2017-summer')
		@if(count($recommends))
			<div id="recommend" class="section clearfix">
				<div id="item1" class="ct-sale-page form-group">
					<div class="title col-xs-12 col-sm-12 col-md-12 text-center">
						<h2>每日推薦商品</h2>
					</div>
		{{-- 				<div class="row box-content">
					    <div class="title-main-box-clear">
					        <h2>
					            <span>{!! $category->name !!}推薦商品　</span>
					            <a href="{!! URL::route('summary', 'ca/' . $category->mptt->url_path) !!}" title="{!! $category->name . '全部商品' !!}">>> 查看全部</a>
					        </h2>
					    </div> --}}
				    <div class="box-content">
				        <div class="ct-riding-gear col-xs-12 col-sm-12 col-md-12 col-lg-12">
				            <ul class="product-list owl-carousel-promo">
				                @foreach($recommends[0] as $recommend_product)
				                <li>
				                    <a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank" class="recommendImage">
										<figure class="zoom-image recommendZoom">
											<img src="{{$recommend_product->image}}" alt="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}">
											<span class="helper"></span>
										</figure>
									</a>
									<a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" class="title-product-item" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
										<span class="bold-text dotted-text2 force-limit box">{{$recommend_product->name}}</span>
									</a>
									@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
										<span class="size-10rem">{{$recommend_product->manufacturer_name}}</span>
										<label class="price font-bold box width-full">NT${{number_format($recommend_product->d_money,0, '.' ,',')}}(最大{{number_format($recommend_product->d_price)}}%off)</label>
									@else
										<span class="size-10rem">{{$recommend_product->manufacturer_name}}</span>
										<label class="price font-bold box width-full">NT${{number_format($recommend_product->c_money,0, '.' ,',')}}(<?php if($recommend_product->point_type == 0){echo '最大'.number_format($recommend_product->c_price).'%off)';}else{ echo number_format($recommend_product->c_point).'倍點數現折)';}?></label>
									@endif
									@if(count($recommend_product->motor))
										<span class="size-10rem">{{$recommend_product->motor}}</span>
									@endif
				                </li>
				                @endforeach
				            </ul>
				        </div>
					</div>
				</div>
			</div>
                <?php
                /*
        <div id="recommend" class="section clearfix">
            <div id="item1" class="ct-sale-page form-group">
                <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
                    <h2>每日推薦商品</h2>
                </div>
                <div class="box-content">
                    <div class="ct-riding-gear col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="product-list owl-carousel-promo">
                            @foreach($recommends[1] as $recommend_product)
                            <li>
                                <a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank" class="recommendImage">
                                    <figure class="zoom-image recommendZoom">
                                        <img src="{{$recommend_product->image}}" alt="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}">
                                        <span class="helper"></span>
                                    </figure>
                                </a>
                                <a href="{{url::route('product-detail',['url_rewrite' => $recommend_product->sku]).'?'.$rel_parameter}}" class="title-product-item" title="【{{$recommend_product->manufacturer_name}}】{{$recommend_product->name}}" target="_blank">
                                    <span class="bold-text dotted-text2 force-limit box">{{$recommend_product->name}}</span>
                                </a>
                                @if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))
                                    <span class="size-10rem">{{$recommend_product->manufacturer_name}}</span>
                                    <label class="price font-bold width-full">NT${{number_format($recommend_product->d_money,0, '.' ,',')}}(最大{{number_format($recommend_product->d_price)}}%off)</label>
                                    <label class="price font-bold box width-full">上月價格</label>

                                @else
                                    <span class="size-10rem">{{$recommend_product->manufacturer_name}}</span>
                                    <label class="price font-bold width-full">NT${{number_format($recommend_product->c_money,0, '.' ,',')}}(<?php if($recommend_product->point_type == 0){echo '最大'.number_format($recommend_product->c_price).'%off)';}else{ echo number_format($recommend_product->c_point).'倍點數現折)';}?></label>
                                    <label class="size-10rem font-bold box width-full">上月價格</label>
                                @endif
                                @if(count($recommend_product->motor))
                                    <span class="size-10rem">{{$recommend_product->motor}}</span>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        */
			?>
		@endif
	@endif
	{{--@foreach($categories as $category_title => $info)--}}
		{{--<div class="section clearfix categories">--}}
			{{--<div id="item1" class="ct-sale-page form-group">--}}
		        {{--<div class="title col-xs-12 col-sm-12 col-md-12 text-center">--}}
		            {{--<h2 class="font-bold size-15rem">{{$category_title}}</h2>--}}
		        {{--</div>--}}
		        {{--<div class="content-box-sale">--}}
		            {{--<ul class="product-sale">--}}
		            	{{--@foreach($info as $title => $link)--}}
							{{--@php--}}
                                {{--$ca_rewrite = $link['url_rewrite'];--}}
                                {{--$discount = $categories_discount->filter(function($discount) use($ca_rewrite){--}}
                                    {{--return ($discount->url_rewrite == $ca_rewrite);--}}
                                {{--})->first();--}}
							{{--@endphp--}}
			                {{--<li class="li-product-sale col-xs-12 col-sm-3 col-md-3">--}}
			                	{{--<span class=" dotted-text1 font-bold size-12rem text-center">{{$title}}</span>--}}
			                    {{--<a href="{{$link['url']}}?ca={{$link['url_rewrite'].'&'.$rel_parameter}}" class="clearfix">--}}
			                        {{--<div class="photo-box">--}}
			                            {{--<ul class="clearfix">--}}
			                                {{--<li class="col-xs-12 col-sm-12 col-md-12 text-center"><figure class="zoom-image col-sm-block col-xs-block"><img src="{{$link['img']}}" alt=""></figure></li>--}}
			                            {{--</ul>--}}
			                        {{--</div>--}}
									{{--@if($discount)--}}
										{{--@if($current_customer and in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))--}}
											{{--<label class="dotted-text1 discount">{{'最大'.number_format($discount->discountd).'%off'}}</label>--}}
										{{--@else--}}
											{{--<label class="dotted-text1 discount">{{'最大'.number_format($discount->discountc).'%off'}}</label>--}}
										{{--@endif--}}
									{{--@else--}}
										{{--<label class="dotted-text1 discount">大特價</label>--}}
									{{--@endif--}}
			                        {{-- <span class=" dotted-text1 font-color-red text-center">點數加倍</span> --}}
			                    {{--</a>--}}
			                {{--</li>--}}
			            {{--@endforeach--}}
		            {{--</ul>--}}
		        {{--</div>--}}
		    {{--</div>--}}
	    {{--</div>--}}
	{{--@endforeach--}}
	<div class="link-container">
		<ul class="clearfix">
			<li class=" size-12rem col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#discount_week')">本周點數5倍送</a></div></li>
			<li class=" size-12rem col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#wanted')">玩遊戲拿點數</a></div></li>
			<li class=" size-12rem col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#country_discount')">愛用國貨點數現折</a></div></li>
			<li class=" size-12rem col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#outlet')">OUTLET現貨出清</a></div></li>
			<li class=" size-12rem col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#more_benefit')">更多會員好康</a></div></li>
			<li class=" size-12rem col-sm-2 col-xs-12 col-md-2"><div class="block-link text-center"><a onclick="slipTo1('#footer')">特輯情報</a></div></li>
		</ul>
	</div>
	@foreach($discount_date as $key => $data)
		@if((strtotime($data[0]) <= strtotime($date) and strtotime($data[1]) >= strtotime($date)))
			<div class="width-full">
				<a href="{{\URL::route('parts',['section' => 'ca/'.$data[2]]).'?'.$rel_parameter}}" title="{{ $data[3] }}【點數5倍送】" target="_blank">
					<figure class="zoom-image">
						<img src="{{assetRemote('image\benefit\big-promotion\weeksale370.png')}}" alt="{{ $data[3] }}【點數5倍送】">
					</figure>
				</a>
			</div>
		@endif
	@endforeach

	@if(count($discount_category))
		<div class="discount-container" id="discount_week">
			<div class="section clearfix">
				<div id="item1" class="ct-sale-page form-group clearfix">
					<div class="title-section clearfix ">
						@foreach($discount_date as $key => $data)
							<a href="{{ (strtotime($data[0]) <= strtotime($date) and strtotime($data[1]) >= strtotime($date)) ? \URL::route('parts',['section' => 'ca/'.$data[2]]).'?'.$rel_parameter : 'javascript:void(0)' }}" target="_blank" alt="{{ $data[3] }}【點數5倍送】">
								<div class="discount_title title col-xs-12 text-center {{ (strtotime($data[0]) <= strtotime($date) and strtotime($data[1]) >= strtotime($date)) ? 'active' : '' }}" style="{{ $key == 3 ? 'margin-right:0px' : '' }};padding:4px;">
									<h2 class="font-bold size-12rem">{{ $data[3] }} </h2>
								</div>
							</a>
						@endforeach
					</div>
					<div class="content-box-sale">
						<ul class="product-sale">
							@foreach($discount_category as $info)
								@php
									$br_image = $info->br_image;
									$ca_image = $info->ca_image;
									$br_rewrite = $info->br_rewrite;
									$ca_rewrite = $info->ca_rewrite;
								@endphp
								<li class="li-product-sale col-xs-12 col-sm-6 col-md-4 category text-center">
									<a href="{{URL::route('parts',['section' => 'ca/'.$ca_rewrite.'/br/'.$br_rewrite]).'?'.$rel_parameter}}" target="_blank">
										<div class="photo-box">
											<ul class="clearfix">
												<li class="col-xs-7 col-sm-7 col-md-7">
													<div class="zoom-container">
														<figure class="zoom-image"><img src="{{$br_image}}" alt=""><span class="helper"></span></figure>
													</div>
												</li>
												<li class="col-xs-5 col-sm-5 col-md-5">
													<div class="zoom-container">
														<figure class="zoom-image thumb-box-border"><img src="{{$ca_image}}" alt=""><span class="helper"></span></figure>
													</div>
												</li>
											</ul>
										</div>
										<span class="manufacturer box">{{$info->manufacturer->name}}</span>
										<span class=" dotted-text1 font-color-red text-center">點數5倍</span>
									</a>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	@endif

    @include('response.pages.benefit.sale.project.2017.otherblock.activities')

	@include('response.pages.benefit.sale.project.2017.otherblock.otherpages')

</div>
@stop
@section('script')
	<script type="text/javascript" src="{{ assetRemote('plugin/FlexSlider-master/jquery.flexslider-min.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('.owl-carousel-promo').addClass('owl-carousel').owlCarousel({
				loop:true,
				nav:true,
				margin:5,
				slideBy : 6,
				URLhashListener:true,
				startPosition: 'URLHash',
				responsive:{
					0:{
						items:2
					},
					600:{
						items:3
					},
					1000:{
						items:5
					}
				}
			});

			$('.owl-carousel-promo').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>')
			$('.owl-carousel-promo').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>')
		});
        function slipTo1(element){
            var y = parseInt($(element).offset().top) - 170;
            $('html,body').animate({scrollTop: y}, 400);
        }

//		$('.discount_title').each(function(){
//		    var date = new Date();
//		    var discount_date = $(this).find('span').text().split('#');
//            var begin_date = new Date(discount_date[0]);
//            var end_date = new Date(discount_date[1]);
//            if(begin_date <= date && date <= end_date){
//                $(this).addClass('active');
//			}
//		});
	</script>
@stop