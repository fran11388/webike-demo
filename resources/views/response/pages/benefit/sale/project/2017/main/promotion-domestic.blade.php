@extends('response.layouts.1column-bigpromo')
@section('style')
<link href="{{assetRemote('css/pages/celebration/big-promotion/category.css')}}" rel="stylesheet" type="text/css">
<style>
	@media (min-width: 768px) {
		.navbar-nav {
			width:100%;
		}
		.navbar-nav li {
			width:9%;
			text-align: center;
			font-size:1rem;
		}
		.promotion-title {
			background: url({{assetRemote('image/benefit/big-promotion/2017_banner_head.png')}}) repeat-x #fc37a2;
			padding-top:100px;
		}
	}
	.webike-logo {
		width:100%;
	}
	.container-header {
		background-color: #ffffff;
		min-height:61px;
	}
	#mainNav {
		background-color: #ffffff;
		min-height:initial;
		position:initial;
		margin-bottom:0px;
		border:0px;
	}
	.col1-logo {
		width:271px;
		height:41px;
		margin:10px;
	}
	.big-promotion .promotion-container {
		background-color: #fc37a2;
		max-width:none;
		width:100% !important;
		padding:0 0 40px 0 !important;
	}
	.promotion-title a:hover {
		opacity: 0.8;
	}
	.ct-sale-page {
		width: 100%;
		float: left; 
	}
	.ct-sale-page .title {
		padding: 10px 20px;
		background-color: black; 
	}
    .ct-sale-page .title h2 {
		color: #fff; 
	}
    .section {
    	background:white;
    	border-radius: 5px;
    	padding:20px;
    	margin-top:30px;
    }
    .form-group {
    	margin-bottom:0px;
    }
    span {
    	float:none !important;
    	font-size:1rem ;
    }
    .description {
    	margin-top:20px;
    }
    .footer {
    	background-color: white;
    }
    .footer .collectiontext {
    	margin-top:40px;
    	margin-bottom:-20px;
    }
    .footer .btn {
    	width:200px;
    	height:40px;
    	padding-top:10px;
    }
    .allbtn {
    	margin-top:40px;
    }
    .allbtn ul{
    	list-style-type: none;
    }
    .activity .photo-box li {
    	width:100% !important;
    }
    .link-container {
    	margin-top:40px;
    }
</style>
@stop
@section('middle')
{{-- <div class="promotion-title"></div> --}}
@include('response.pages.benefit.sale.project.2017.otherblock.main-banner')
@include('response.pages.benefit.sale.project.2017.otherblock.searchbar')
@include('response.pages.benefit.sale.project.2017.otherblock.main-category-link')
<div class="container">
	@include('response.pages.benefit.sale.project.2017.otherblock.maintain')
    @include('response.pages.benefit.sale.project.2017.otherblock.otherpromotions')
</div>
@stop
@section('script')
@stop