@extends('response.layouts.1column')
@section ('style')
<style>
    /*.container {*/
        /*width:1200px !important;*/
    /*}    */
    .zoom-image:hover img{
        -webkit-transform: scale(1.2);
        -moz-transform: scale(1.2);
        -ms-transform: scale(1.2);
        -o-transform: scale(1.2);
        transform: scale(1.2);
        opacity: 1;
    }
    .double11-container  {
        border:0px solid black;
        font-size: 1rem !important;
    }
    .common {
        padding: 10px;
        border:1px solid black;
        font-size: 1rem !important;
    }


    .banner-container{
        margin-top: 20px;
        border:1px solid black;

    }    

    .double11-game-container {
        margin-top: 40px;
        border:1px solid black;
    }
   
    .double11-game-title{
        background-color: black;
        color: #FFFFFF;
        font-weight: bold;
        text-align: center; 
        padding: 15px;
    }

    .game-play  {
        padding: 50px 25px !important; 
        border-right:0px solid black !important;
        border-left: 0px solid black !important;
        background-image: url("{{ assetRemote('image/promotion/2018double11/banner/background1.jpg')}}");
        background-repeat: no-repeat;
        background-size: 100% 100%;
    } 

    .double11-container  .gifts_ilst  {
        padding-left: 12%;
        float:left;
    }

    .game-play-gifts {
        width: 86%;
        margin-left: 20px;
    } 
    .game-play-start{
        clear: left;
        /*padding-top: 20px;*/
        text-align: center;

    }


    .game-play-gifts-li{
        list-style-type:none;
        padding: 10px;
    }

    .game-narrative{
        padding: 10px;
        font-size: 1rem !important;

    }

    .gooditem-activity
    {
        padding:25px;
        background-image: url("{{ assetRemote('image/promotion/2018double11/banner/background3.jpg')}}");
        background-size: 100% 100%;
    }

    .gooditem-center-left
    {
        padding: 15px 5px 15px 15px;

    }
    .gooditem-center-center
    {
        padding: 15px 10px 15px 10px;

    }
    .gooditem-center-right
    {
        padding: 15px 15px 15px 5px;

    } 

 
    .voucher-border
    {
        border: 1px solid black;
        border-left: 0px !important;
        border-right: 0px !important; 
    }
    /*.voucher-exchange
    {
        padding: 45px !important;
        background-image: url("{{ assetRemote('image/promotion/2018double11/banner/background2.jpg')}}");
        background-size: 100% 100%;
    }
    .voucher-exchange a:hover {
        opacity: 0.7;
    }*/

    .outlet{
        border:1px solid;
        padding: 10px;
        background-color: #FFFFFF;

    }

    .outlet-title{
        text-align: center;
        font-weight: bold;
        font-size: 1rem !important;

    }

    .outlet-center{
       padding-top: 10px;     


    }
    .outlet-center:hover{
        opacity: 0.7;
    }
    
    .outlet-narrative{
        margin-top: 10px;
        padding-top: 10px;   
        overflow: auto;
        height: 70px;
    }
    @media (max-width: 992px){
       .gooditem-center-left{
            padding: 15px 15px 7.5px 15px !important;
        }        
        .gooditem-center-center {
            padding: 7.5px 15px 7.5px 15px !important;
        }
        .gooditem-center-right{
            padding: 7.5px 15px 15px 15px !important;
        }
    }
    @media (max-width: 1000px){

        .outlet-center{
            display: block !important;
        }

    }

    .double11-container .plate
    {
        padding-left: 53px;
        padding-top: 5px;
    }
    .double11-container .wheel{
        position: relative;
        padding-left: 5%;
        width: 100%;
        /*float:left;*/
    }
    .double11-container .wheel_background
    {
        padding-left: 55px;
        padding-top: 5px;
    }
    .double11-container canvas {
        position: absolute;
    }
    .double11-container .pointer{
        position: absolute;
        z-index: 100;
        left: 69px;
    }
    .double11-container .game-play-start img {
        width:70%;
    }
    /*.game-play-start {
        margin-right:20px;
    }*/
    .double11-container .game-play-start .btn-warning{
        margin-right:20px;
    }
    .not_login {
        width: 50%;
        margin-right: auto;
        margin-left: auto;
        padding: 20px;
        background-color: white;
        border: 3px solid #ccc;
    }
    @media(max-width:768px){
        .not_login{
            width:100%;
        }
        .wheel .wheel-control #canvas{
            left: 10px;
            top: 10px;
        }
 
    }
    @media(max-width: 1024px){
        .wheel-control #canvas{
            left: 13px;
            top: 14px;
        }       
    }
    .game-narrative .description-text {
        float: left;
        margin-right: 15px;
    }
    .game-narrative .ul-ct-brand-selection {
        padding:5px 10px;
    }
    .game-narrative .ul-ct-brand-selection li {
        padding-bottom: 5px;
    }
    .game-narrative .ul-ct-brand-selection p {
        padding-bottom:5px;
    }
    .game-narrative .ul-ct-brand-selection li:nth-child(3),.game-narrative .ul-ct-brand-selection p:nth-child(3) {
        padding-bottom:0px;
    }

    .voucher-narrative .description-text {
        float: left;
        margin-right: 15px;
    }
    .voucher-narrative .ul-ct-brand-selection {
        padding:5px 10px;
    }
    .voucher-narrative .ul-ct-brand-selection li {
        padding-bottom: 5px;
    }
    .voucher-narrative .ul-ct-brand-selection p {
        padding-bottom:5px;
    }
    .voucher-narrative .ul-ct-brand-selection li:nth-child(3),.game-narrative .ul-ct-brand-selection p:nth-child(2) {
        padding-bottom:0px;
    }
    .banner-container {
        margin-top:0px;
    }
    .mobile {
        display:none;
    }
    .wheel-control{
        position:relative;
        width:94%;
    }
    .double11-container .pointer{
        width: 100%;
    }
    .wheel_background{
        width:100%;
    }
    #canvas {
        width: 94%;
        left: 17px;
        top: 17px;
    }
    .gifts_ilst{
        padding-left: 4% !important;
        width:50%;
    }

    .wheel_background{
        padding:0 !important;
    }
    .double11-container .pointer{
        left: 0px;
    }
    .gifts_ilst .game-play-gifts .list .day-list{
        display: none;
    }
    .gifts_ilst .game-play-gifts .reward_day{
        cursor: pointer;
    }
    .gifts_ilst .game-play-gifts .list .day-active.day-list{
        display: block !important;
        padding: 10px 0px;
    }
    .gifts_ilst .game-play-gifts ul{
        list-style: none;
    }
    .gifts_ilst .game-play-gifts .day-active{
        background: #ffe400d4;
    }
    .gifts_ilst .game-play-gifts .choose_date.day-active{
        border: 1px solid;
        border-top: none;
        border-bottom: 1px solid #ffe40000;
    }
    .gifts_ilst .game-play-gifts .choose_date.day-no-active{
        border: 1px solid;
        border-top: none;
        border-bottom: 1px solid #cccccc00;
    }
    .gifts_ilst .game-play-gifts .reward_date ul li{
        border-bottom: 1px solid;
        text-align: center;
        font-weight: bold;
        font-size: 1.25rem;
        padding: 5px;
    }
    .gifts_ilst .game-play-gifts .day-no-active{
        background: #ccccccd4;
    }
    .gifts_ilst .game-play-gifts .list ul li{
        padding: 10px;
        font-weight: bold;
    }
    .game-play-gifts .list ul li div{
        padding: 0px;
    }
    .gifts_ilst .game-play-gifts .list{
        border: 1px solid;
        border-top: none;
    }

    .gifts_ilst .game-play-gifts .day-reward-title{
        font-family: fantasy;
        background: #7a181c;
        color: #fff;
        padding: 10px;
        font-size: 1.25rem;
        border: 1px solid #000;
    }
    .game-play .game-play-block{
        float: left;
        width: 50%;
    }
    .gifts_ilst .game-play-gifts .list .gift{
        color: #fff;
        background: #7a181c;
    }
    @media(min-width: 900px)
    {
        .game-play-gifts .list ul li div{
            font-size: 1.1rem;
        }
    }

    @media(max-width:700px){
        .wheel{
            width:100%;
        }
        .desktop{
            display:none;
        }
        .mobile{
            display:block;
        }
        #spin_button{
            width:82%;
        }

        .gifts_ilst{
            margin-top: 10px;
            padding-left: 0px !important;
            width:100%;
        }
        .game-play-gifts{
            margin-left:auto;
            margin-right:auto;
        }
        .double11-container .wheel{
            width:100%;
        }
        .game-play-start {
            margin-right:0px !important;
            padding-bottom:10px !important;
        }
        .pointer {
            left:-1px;
        }

        .game-play .game-play-block{
            width: 100%;
        }
       #canvas{
            left: 8px !important;
            top: 9px !important;
        }
        .gifts_ilst .game-play-gifts .list ul li{
        padding: 10px 10px 0px;
        }
        .gifts_ilst .game-play-gifts .list .day-active.day-list{
            padding-top: 0px;
        }
        .game-play-gifts{
            width: 100%;
        }
        .double11-game-container{
            margin-top: 10px;
        }
        
    }
    @media(max-width:350px){
        #canvas{
            left: 7px;
            top: 7px;
        }
    }
    .other-link .gooditem-activity{
        padding-bottom:100px !important;
    }
    a.toggle{
        font-size: 1rem;
        position: relative;
        padding: 15px 30px 15px 15px;
        background: #eee;
        display: block;
        border: 1px solid #ccc;
        font-weight: bold;
    }
    .toggle_block a.toggle:after{
        font-weight: normal;
        font-size: 2rem;
        content: '\f107';
        top: 0;
        font-family: FontAwesome;
        position: absolute;
        right: 15px;
        color: #444;
    }
    .toggle_block a.toggle.open:after{
        content: '\f106';
    }
    .gifts_ilst .reward_date ul li:first-child{
        border-left: 1px solid;
    }
    .gifts_ilst .reward_date ul li:last-child{
        border-right: 1px solid;
    }
    .double11-game-container .voucher-exchange .zoom-image{
        padding: 0px;
    }
    .pagebutton{
        outline: 1px solid;
        padding: 0px;
    }
    .crowns{
        width: 10%;
    }

</style>
@endsection
@section ('middle')
<div class="double11-container">
    <div class="banner-container">
        <div>
            <img src="{{ assetRemote('image/promotion/2018double11/banner/sale_1200628.jpg') }}" alt="webike 1111購物節">
        </div>
        <div class="clearfix">
            <div class="col-md-6 col-xs-6 pagebutton">
                <a onclick="slipTo('#double11-game')">
                    <img src="{{ assetRemote('image/promotion/2018double11/button/pagebutton1.png') }}"  onmouseover="pageImg(this,2);" onmouseout="pageImg(this,1);">
                </a>
            </div>
            <div class="col-md-6 col-xs-6 pagebutton">
                <a onclick="slipTo('#double11-coupons')">
                    <img src="{{ assetRemote('image/promotion/2018double11/button/pagebutton3.png') }}"  onmouseover="pageImg(this,4);" onmouseout="pageImg(this,3);">
                </a>
            </div>
        </div>
    </div>
    <div class="double11-game-container" id="double11-game">
        <div class="double11-game-title "> 
            <h1>好康一  每日輪盤抽大獎</h1>
        </div>
        <div class="game-play  common clearfix" >
            <div class="game-play-block">
                <div  class="game-play-start">

                    @if($customer_finish)
                        <div class="not_login text-center">
                            <span class="size-10rem font-bold">今日已抽獎，獎品為<br>{{$reward->name}}。</span>
                        </div>
                    @elseif($customer)
                        <div class="desktop">
                            <img id="spin_button" class="spin_button"
                            onmouseover="btnOver(this);"                   
                            onmouseout="btnOut(this);"
                             src="{{ assetRemote('image/promotion/2018double11/button/button1.png') }}"  onClick="startSpin();">
                        </div>
                        <div class="mobile">
                            <img id="spin_button" class="spin_button " src="{{ assetRemote('image/promotion/2018double11/button/button1.png') }}" 
                            onmousedown="this.src='{!! assetRemote('image/promotion/2018double11/button/button3.png') !!}'"
                            onClick="startSpin();">
                        </div>
                    @else
                        <div class="not_login">
                            <a class="btn btn-warning" href="{{ \URL::route('login') }}">會員登入</a>
                            <a class="btn btn-danger" href="{{ \URL::route('customer-account-create') }}">加入會員</a>
                        </div>
                    @endif
                </div>
                <div class="wheel">
                    <div class="wheel-control">
                        <img class="pointer" src="{{ assetRemote('image/promotion/2018double11/wheel/s.png') }}">
                        <img class="wheel_background" src="{{ assetRemote('image/promotion/2018double11/wheel/wheel11.gif') }}">
                        <td width="421" height="564" class="the_wheel" align="center" valign="center">
                            <canvas id="canvas" width="420" height="420">
                                <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please try another.</p>
                            </canvas>
                        </td>
                    </div>
                </div>
            </div>
            <div class=" gifts_ilst">
                <div class="game-play-gifts">
                    <div class="text-center day-reward-title">每日樂透獎項</div>
                    @php
                        $reword_date = ["2018-11-08" => "11/8","2018-11-09" => "11/9", "2018-11-10" => "11/10","2018-11-11" => "11/11"];
                    @endphp
                    <div class="reward_date clearfix">
                        <ul>
                            @foreach($reword_date as $key => $reword_day)
                                <li class="col-md-3 col-xs-3 reward_day {{ date('Y-m-d') == $key ? 'day-active choose_date' : 'day-no-active no_choose_date' }}" id="{!! $key !!}">
                                    {!! $reword_day !!}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="list">
                        @foreach($reward_lists as $key => $reward_list)
                            <div class="{{$key}} day-list {{ date('Y-m-d') == $key ? 'day-active' : 'day-no-active' }}">
                                <ul>
                                    @foreach($reward_list as $list)
                                        <li class="{{ $list['sort'] }} clearfix">
                                            <div class="col-md-2 col-xs-2 reward-num-bolck">
                                                <span class="reward-num ">{{ $list['reward_num'] }}</span>
                                            </div>
                                            <div class="col-md-10 col-xs-10 text-center reward-name-bolck">
                                                <span class="reward-name">{{ $list['reward_name'] }}</span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div> 
                </div>                    
            </div>
        </div>
        <div class="toggle_block">
            <a href="javascript:void(0)" class="toggle" target="_top">每日輪盤抽大獎活動說明</a>
            <div class="game-narrative toggle_text" style="display: none;"> 
                <ul class="ul-ct-brand-selection">
                    <li class="clearfix">
                        <div class="size-10rem description-text text-center">【活動期間】</div>
                        <div class="size-10rem description-text"><span>2018年11月8日 (00:00) ~ 2018年11月11日 (23:59) ，凡註冊的會員每日都可以抽獎一次，共有4次機會。</span></div>
                    </li>
                    <li class="clearfix">
                        <div class="size-10rem description-text text-center">【活動方式】</div>
                        <div class="size-10rem description-text"><span>按下"START"輪盤即開始旋轉，停止旋轉後即可獲得對應的獎項。</span></div>
                    </li>
                    <li class="clearfix">
                        <div class="size-10rem description-text text-center">【獎項說明】</div>
                        <div class="size-10rem description-text">
                            <p class="size-10rem">A. "現金點數獎"，您可以至 <a href="{{ \URL::route('customer-history-points') }}" target="_blank">點數獲得及使用履歷</a> 確認您獲得的現金點數，並於購物車中進行折抵。</p>
                            <p class="size-10rem"> B. "贈品獎"，您可以至<a href="{{ \URL::route('customer-history-gifts') }}" target="_blank"> 贈品兌換 </a>確認您獲得的獎品，兌換時只要將獎品按下加入購物車的按鈕，即可於下次購物時隨貨附贈。</p>
                            <p class="size-10rem"> C. 本次活動的點數與贈品使用期限至2018年11月30日止。</p> 
                        </div>
                    </li>                         
                </ul>
            </div>
        </div>

    </div>
    <div class="double11-game-container" id="double11-coupons">
        <div class="double11-game-title "> 
            <h1>好康二  限時禮卷免費送</h1>
        </div>

        <div class="voucher-exchange voucher-border">
           <a class="zoom-image" href="{{ \URL::route('cart') }}" target="_blank"><img src="{{ assetRemote('image/promotion/2018double11/banner/coupon_628.gif') }}"></a>
        </div>
        <div class="toggle_block">
            <a href="javascript:void(0)" class="toggle" target="_top">限時禮卷免費送活動說明</a>
            <div class="voucher-narrative game-narrative toggle_text" style="display:none;"> 
                <ul class="ul-ct-brand-selection ">
                    <li class="clearfix">
                        <div class="size-10rem description-text text-center">【發送時間】</div>
                        <div class="size-10rem description-text"><span>2018年11月8日 (00:00) 發送給所有會員一張111元現金抵用禮券。</span></div>
                    </li>
                    <li class="clearfix">
                        <div class="size-10rem description-text text-center">【使用期限】</div>
                        <div class="size-10rem description-text"><span>2018年11月11日 (23:59) 截止。</span></div>
                    </li>
                    <li class="clearfix">
                        <div class="size-10rem description-text text-center">【使用說明】</div>
                        <div class="size-10rem description-text">
                            <p class="size-10rem">登入後在購物車頁面中間找到折價券的欄位，選擇「1111購物節限時折價券[NT111]」並點選輸入，即可折抵商品金額111元。</p>
                            <p class="size-10rem">※折價券為一次性使用，若商品金額小於折價金額則餘額不會退回，另外運費不可以折抵。</p>
                        </div>
                    </li>                         
                </ul>
            </div>
        </div>
    </div>
    @include('response.pages.benefit.sale.project.2017.1111promo.specailprice')
    <div class="double11-game-container other-link">
        <div >
            <div class="double11-game-title">
                <h1>其他好康</h1>
            </div>
            <div class="gooditem-activity  clearfix">
                <div class="col-md-4 col-sm-12 col-xs-12 gooditem-center-left">
                    <div class="outlet">
                        <div class="outlet-title"><h2>OUTLET</h2></div>
                        <div class="outlet-center text-center zoom-image"><a href="{{ \URL::route('outlet') }}" target="_blank"><img src="{{ assetRemote('image/promotion/2018double11/banner/outlet345.jpg') }}"></a>
                        </div>
                        <div class="outlet-narrative">
                            <span class="size-10rem">OUTLET出清商品，賣不掉就降價，慢了就買不到了。</span>
                        </div>
                     </div>
                 </div>
                <div class="col-md-4 col-sm-12 col-xs-12 gooditem-center-center">
                     <div class="outlet">
                        <div class="outlet-title"><h2>11月精選SALE</h2></div>
                        <div class="outlet-center text-center zoom-image"><a href="{{ \URL::route('benefit-sale-month-promotion',['code' => '2018-11']) }}" target="_blank"><img src="{{ assetRemote('image/promotion/2018double11/banner/promotion_345.jpg') }}"></a>
                        </div>
                        <div class="outlet-narrative"><span class="size-10rem">
                        11月精選品牌、車型現金點數5倍回饋，精選分類2倍點數回饋，撿便宜趁現在。</span>
                        </div>
                     </div>
                 </div>
                <div class="col-md-4 col-sm-12 col-xs-12 gooditem-center-right">
                     <div class="outlet">
                        <div class="outlet-title"><h2>加價購</h2></div>
                        <div class="outlet-center text-center zoom-image"><a href="{{ \URL::route('cart') }}" target="_blank"><img src="{{ assetRemote('image/promotion/2018double11/banner/addprice_345.jpg') }}"></a>
                        </div>
                        <div class="outlet-narrative"><span class="size-10rem">
                        11月銅板加價購，多樣商品讓你順手帶回家！</span>
                        </div>
                     </div>
                </div>
            </div>
            {{-- @php
                $fruits = array('apple','orange','banana');
            @endphp
            @foreach($fruits as $fruit)
                <div class="col-md-4 col-sm-12 col-xs-12 outlet">
                    <div class="outlet-title"><h1>{{ $fruit }}</h1></div>
                    <div><a href="https://www.webike.tw/outlet?rel=2018-10-2018Fall"><img src="https://img.webike.tw/shopping/image/benefit/big-promotion/OUTLETJULY345.png"></a></div>
                    <div class="outlet-narrative"><h2>「Webike Outlet」定期上架各大品牌商品，只要「賣不掉，就調降」售價直到您滿意，歡迎大家搶便宜!!</h2></div>
                </div>
            @endforeach --}}
        </div>
    </div>
    <div class="load-image">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/1.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/2.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/3.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/4.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/5.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/6.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/7.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/8.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/9.png') }}">
        <img src="{{ assetRemote('image/promotion/2018double11/wheel/10.png') }}">

    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/Winwheel.js')}}"></script>
    <script type="text/javascript" src="{{assetRemote('plugin/wheel/tweenmax.js')}}"></script>
    <script>
        // Create new wheel object specifying the parameters at creation time.


        var theWheel = new Winwheel({
            'numSegments'       : 10,                 // Specify number of segments.
            'outerRadius'       : 200,               // Set outer radius so wheel fits inside the background.
            'drawText'          : true,              // Code drawn text can be used with segment images.
            'textFontSize'      : 16,
            'textOrientation'   : 'curved',
            'textAlignment'     : 'inner',
            'textMargin'        : 90,
            'textFontFamily'    : 'monospace',
            'textStrokeStyle'   : 'black',
            'textLineWidth'     : 3,
            'textFillStyle'     : 'white',
            'drawMode'          : 'segmentImage',    // Must be segmentImage to draw wheel using one image per segemnt.
            'segments'          :                    // Define segments including image and text.
                [
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/1.png') }}',  'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/2.png') }}',   'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/3.png') }}',  'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/4.png') }}',  'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/5.png') }}', 'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/6.png') }}', 'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/7.png') }}',  'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/8.png') }}', 'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/9.png') }}',  'text' : ''},
                    {'image' : '{{ assetRemote('image/promotion/2018double11/wheel/10.png') }}', 'text' : ''}
                ],
            'animation' :           // Specify the animation to use.
                {
                    'type'     : 'spinToStop',
                    'duration' : 5,     // Duration in seconds.
                    'spins'    : 10,     // Number of complete spins.
                    'callbackFinished' : 'alertPrize()'
                }
        });

//        if($( window ).width() ){
//            function draw() {
//                var ctx = (a canvas context);
//                ctx.canvas.width  = window.innerWidth;
//                ctx.canvas.height = window.innerHeight;
//                //...drawing code...
//            }
//        }


        $(".load-image img").one("load", function() {
            theWheel.draw();
            $(".load-image").hide();
        }).each(function() {
            if(this.complete) $(this).load();
        });

        @if(!$customer)
            swal(
                '登入即可參與抽獎活動',
                '',
                'info'
            )
        @endif

        $('.reward_day').click(function(){
            date = $(this).attr('id');
            $('.gifts_ilst .game-play-gifts .list .day-list').removeClass('day-active');
            $('.gifts_ilst .game-play-gifts .list .day-list.' + date).addClass('day-active');
            $('.reward_day').removeClass('choose_date');
            $(this).addClass('choose_date');
        });

        $('.toggle').click(function(){
            if($(this).hasClass('open')){
                $(this).closest('div.toggle_block').find('.toggle_text').hide();
                $(this).removeClass('open');
            }else{
                $(this).closest('div.toggle_block').find('.toggle_text').show();
                $(this).addClass('open');
            }
        });

        function slipTo(element){
            var y = parseInt($(element).offset().top) - 65;
            $('html,body').animate({scrollTop: y}, 400);
        }
        function pageImg(_this,num){
            img = '{{ assetRemote('image/promotion/2018double11/button/pagebutton') }}' + num + '.png';
            $(_this).attr('src', img);
        } 
        // Vars used by the code in this page to do power controls.
        var wheelPower    = 0;
        var wheelSpinning = false;

        // -------------------------------------------------------
        // Function to handle the onClick on the power buttons.
        // -------------------------------------------------------
        function powerSelected(powerLevel)
        {
            // Ensure that power can't be changed while wheel is spinning.
            if (wheelSpinning == false)
            {
                // Reset all to grey incase this is not the first time the user has selected the power.
                document.getElementById('pw1').className = "";
                document.getElementById('pw2').className = "";
                document.getElementById('pw3').className = "";

                // Now light up all cells below-and-including the one selected by changing the class.
                if (powerLevel >= 1)
                {
                    document.getElementById('pw1').className = "pw1";
                }

                if (powerLevel >= 2)
                {
                    document.getElementById('pw2').className = "pw2";
                }

                if (powerLevel >= 3)
                {
                    document.getElementById('pw3').className = "pw3";
                }

                // Set wheelPower var used when spin button is clicked.
                wheelPower = powerLevel;

                // Light up the spin button by changing it's source image and adding a clickable class to it.
                $('.spin_button').attr('src',"{{ assetRemote('image/promotion/2018double11/button/button3.png') }}");
                document.getElementById('spin_button').className = "clickable";
            }
        }

        // -------------------------------------------------------
        // Click handler for spin button.
        // -------------------------------------------------------
        function startSpin()
        {

            // Ensure that spinning can't be clicked again while already running.
            if (wheelSpinning == false)
            {
                // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                // to rotate with the duration of the animation the quicker the wheel spins.
                if (wheelPower == 1)
                {
                    theWheel.animation.spins = 3;
                }
                else if (wheelPower == 2)
                {
                    theWheel.animation.spins = 8;
                }
                else if (wheelPower == 3)
                {
                    theWheel.animation.spins = 15;
                }

                // Disable the spin button so can't click again while wheel is spinning.
                $('.spin_button').attr('src',"{{ assetRemote('image/promotion/2018double11/button/button3.png') }}").addClass('active');
//                document.getElementById('spin_button').className = "";

                // Begin the spin animation by calling startAnimation on the wheel object.
                theWheel.startAnimation({{ $game_active_result }});
                @if($customer)
                    setTimeout(function() {
                        $('.gifts_ilst .game-play-gifts .list .{{date('Y-m-d')}} .{{$reward_result}}').addClass('gift');
                        $('.gifts_ilst .game-play-gifts .list .{{date('Y-m-d')}}  .{{$reward_result}} .reward-name-bolck span.reward-name').append("<img src='{{assetRemote('image/promotion/2018double11/wheel/crowns.png')}}' class='crowns'>");
                        swal({
                            type: 'success',
                            title: '恭喜您！',
                            @if($reward_point)
                                html: '恭喜您獲得{{$reward->name}}。<br>您可以至<a href="{{ \URL::route('cart') }}" target="_blank">購物車</a>進行確認。',
                            @else
                                html: '恭喜您獲得{{$reward->name}}。<br>您可以至<a href="{{ \URL::route('customer-history-gifts') }}" target="_blank">贈品兌換</a>進行確認。',
                            @endif
                            showConfirmButton: false,
                            timer: 5000
                        });
                    }, 6000);
                @endif



                // Set to true so that power can't be changed and spin button re-enabled during
                // the current animation. The user will have to reset before spinning again.
                wheelSpinning = true;


                $.ajax({
                    url: "{!! route('benefit-sale-double11-createGift') !!}",
                    data: {_token: $('meta[name=csrf-token]').prop('content')},
                    type: 'POST',
                    dataType: 'json',
                    success: function(result){
                    },
                    error: function(xhr, ajaxOption, thrownError){

                    }
                });

            }
        }

        // -------------------------------------------------------
        // Function for reset button.
        // -------------------------------------------------------
        function resetWheel()
        {
            theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
            theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
            theWheel.draw();                // Call draw to render changes to the wheel.

            document.getElementById('pw1').className = "";  // Remove all colours from the power level indicators.
            document.getElementById('pw2').className = "";
            document.getElementById('pw3').className = "";

            wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
        }

        // -------------------------------------------------------
        // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
        // -------------------------------------------------------
        function alertPrize()
        {
            // Get the segment indicated by the pointer on the wheel background which is at 0 degrees.
            var winningSegment = theWheel.getIndicatedSegment();

            // Do basic alert of the segment text. You would probably want to do something more interesting with this information.
//            alert(winningSegment.text + ' says Hi');
        }

        function btnOver(_this){
            if(!$(_this).hasClass('active')){
                $(_this).attr('src', '{{ assetRemote('image/promotion/2018double11/button/start_button.png') }}');
            }
        }

        function btnOut(_this){
            if(!$(_this).hasClass('active')){
                $(_this).attr('src', '{{ assetRemote('image/promotion/2018double11/button/button1.png') }}');
            }
        }
    </script>
@endsection

