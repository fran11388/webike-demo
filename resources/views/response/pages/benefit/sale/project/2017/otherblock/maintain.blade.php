<style>
@media (max-width: 768px) {
    .maintain .category-block {
        width:100% !important;
        margin:20px 0 0 0 !important;
    }
}
.maintain {
        width:100%;
}
.maintain .category-block {
    float:left;
    margin:20px 2% 0 0;
    width:32%;
    list-style-type:none;
}
.maintain .category-block:nth-child(1) {
    margin:0 2% 0 0 ;
}
.maintain .category-block:nth-child(2) {
    margin:0 2% 0 0 ;
}
.maintain .category-block:nth-child(3) {
    margin:0 2% 0 0 ;
}
.maintain .category-block:nth-child(3n) {
    margin-right:0;
}
.maintain .category-block .image {
    width:100%;
}
.maintain .category-block .image span{
    display:block;
    margin-bottom:10px;
}
.maintain .category-block .link {
    padding:10px;
    border-top: 3px #000 solid;
    background: rgba(0, 0, 0, .6);
    min-height:177px;
}
.maintain .category-block li {
    list-style-type:none;
}
.maintain .category-block li span {
    color:white;
}
.size-12rem {
    display:block;
}
.maintain .item {
    padding:0px;
}

.maintain a span {
    font-size:0.85rem !important;
}
</style>
<?php 
    $items = array(
        '機油' => [
                'link' => \URL::route('summary',['section' => 'ca/4000-4001']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-4000-4001.png',
                'contents' => [
                    '1000-4000-4001-4002' => '四行程機油',
                    '1000-4000-4001-4006' => '二行程機油',
                    '1000-4000-4001-4003' => '機油濾芯',
                    '1000-4000-4001-4004' => '添加劑',
                    '1000-4000-4001-1335' => '其他油脂類'
                ],
            ],
        '化學用品' => [
                'link' => \URL::route('summary',['section' => 'ca/4000-4020']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-4000-4020.png',
                'contents' => [
                    '1000-4000-4020-4021' => '潤滑劑',
                    '1000-4000-4020-4022' => '洗淨・脫脂劑',
                    '1000-4000-4020-4023' => '防鏽・除鏽劑',
                    '1000-4000-4020-4025' => '補修專用化學用品',
                    '1000-4000-4020-4040' => '塗裝相關用品',
                    '1000-4000-4020-4024' => '液態密封膠(墊片膠)',
                    // '1000-4000-4020-4034' => '接著劑',
                    '1000-4000-4020-4037' => '其他化學用品',
                    '1000-4000-4020-4035' => '擦拭布',
                    '1000-4000-4020-4038' => '洗車用品'
                ],
            ],
        '電池相關' => [
                'link' => \URL::route('summary',['section' => 'ca/4000-1139']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-4000-1139.png',
                'contents' => [
                    '1000-4000-1139-1145' => '電瓶',
                    '1000-4000-1139-4069' => '充電器'
                ],
            ],
        '點火相關' => [
                'link' => \URL::route('summary',['section' => 'ca/1000-1180-1140']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-1180-1140.png',
                'contents' => [
                    '1000-1180-1140-1142' => '火星塞',
                    '1000-1180-1140-1143' => '高壓矽導線',
                    '1000-1180-1140-1322' => '高壓線圈'

                ],
            ],
        '鍊條' => [
                'link' => \URL::route('parts',['section' => 'ca/1000-1150-1153']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-8000.png',
                'contents' => [
                    '1000-1150-1153' => '鍊條相關零件',
                    '1000-8000-4066' => '鍊條類相關工具'
                ],
            ],
        '齒盤' => [
                'link' => \URL::route('parts',['section' => 'ca/1000-1150-1152']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-1150.png',
                'contents' => [
                    '1000-1150-1152' => '齒盤'
                ],
            ],
        '煞車皮來令片' => [
                'link' => \URL::route('parts',['section' => 'ca/1000-1010-1011']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-1010.png',
                'contents' => [
                    '1000-1010-1011' => '煞車皮(來令片)'
                ],
            ],
        '輪胎' => [
                'link' => \URL::route('summary',['section' => 'ca/4000-5000']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-5000.png',
                'contents' => [
                    '1000-5000-4092' => '道路胎',
                    '1000-5000-4093' => '越野胎',
                    '1000-5000-4094' => '氣嘴・內胎',
                    '1000-5000-5010' => '其他輪胎相關用品',
                    '1000-8000-4065' => '輪胎類相關工具'
                ],
            ],
        '工具' => [
                'link' => \URL::route('summary',['section' => 'ca/8000']),
                'photo' => assetRemote('image/benefit/big-promotion').'/1000-8000.png',
                'contents' => [
                    '1000-8000-4061' => '摩托車用工具',
                    '1000-8000-4062' => '配線関連',
                    '1000-8000-4063' => '維修保養用小物品',
                    '1000-8000-4064' => '特殊工具',
                    '1000-8000-4067' => '計測相關工具',
                    '1000-8000-4070' => '化油器相關工具',
                    '1000-8000-8003' => '手工具',
                    '1000-8000-8028' => '配線用工具',
                    '1000-8000-8860' => '測量工具',
                    '1000-8000-8088' => '工具箱・收納袋',
                    '1000-8000-8010' => '套裝工具',
                    '1000-8000-8900' => '車庫工具',
                    '1000-8000-8124' => '摩托車用特殊工具',
                    '1000-8000-8999' => '其他工具'
                ],
            ],
    );
 ?>
<div class="section clearfix">
    <div class="maintain">
        <ul class="clearfix">
            @foreach ($items as $title => $item)
                <li class="category-block">
                    <div class="image text-center">
                        <span class="size-12rem font-bold">{{$title}}</span>
                        <a href="{{$item['link'].'?'.$rel_parameter}}" alt="{{$title.$tail}}" title="{{$title.$tail}}" target="_blank">
                            <img src="{{$item['photo']}}" alt="{{$title.$tail}}" title="{{$title.$tail}}">
                        </a>
                    </div>
                    <div class="link clearfix">
                        <div class="col-md-12 col-sm-12 col-xs-12 item">
                            <ul>
                                @foreach ($item['contents'] as $url_path => $content)
                                    <li class="col-md-6 col-sm-12 col-xs-12">
                                        <a href="{{URL::route('summary',['section' => '/ca/'.$url_path]).'?'.$rel_parameter}}" title="{{$content.$tail}}" target="_blank"><span>{{$content}}</span></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>