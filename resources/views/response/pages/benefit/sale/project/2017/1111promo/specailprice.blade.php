<style>
	#special_price .box-content{
		margin-bottom: 0px;
	    padding: 0;
	    background-color: #fff;
	    border: 1px solid #000;
	    display: block;
	    width:100%;
	    margin-left: auto;
	    margin-right: auto;

	}
	#special_price  ul{
		list-style-type: none;
	}
	#special_price .owl-prev{
		background-color: #7a181c;	
	}
	#special_price .owl-next{
		background-color: #7a181c;	
	}
	#special_price .content-box-sale{
		float: none;
	}
	#special_price{
		float: none;
		margin-top: 30px;
	}

	#special_price .product-grid span{
		width: 100%;
	}

	#special_price .brand_url_rewrite img{
	    
		height:71px;
	}
	#special_price .brand_url_rewrite:hover img{
	    opacity: 1;
		height:71px;
	    transform: scale(1.3);
        
	}
	#special_price .brand_url_rewrite:hover{
		cursor: pointer;
	}
	#special_price #special_price_brand .brand_url_rewrite{
		opacity:0.5;
	}
	#special_price .content-box-sale .li-product-sale .photo-box{
		float: none;
	}
	#special_price #special_price_brand .owl-stage .center .brand_url_rewrite{
		opacity:1;
	}
	#special_price .loading-box {
		display:none;
	}
	#special_price #special_price_product .country-discount-all-btn{
		margin-bottom: 20px;
	}
	#special_price #special_price_product .country-discount-all-btn .btn-warning{
		background-color: #b92229;
	}
	#special_price #special_price_brand .product-grid a figure{
		height: inherit;
	}
	@media(max-width: 550px){
		#special_price .brand_url_rewrite img{
				height: inherit;
			}
		#special_price #special_price_brand #special_price_brand .product-grid .col-md-7 .zoom-container .zoom-image{
			display: block;
		}
	}
</style>
@php
	$special_price_brands = [
		array('id'=>'541',
			'name' => 'OVER',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_541.gif',
					'img'=>'https://img.webike.net/catalogue/images/24415/YZFR25_5.jpg'),
		array('id'=>'854',
			'name' => 'YOSHIMURA',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_854.gif',
					'img'=>'https://img.webike.net/catalogue/images/48274/110-235-5E80B_04.jpg'),
		array('id'=>'49',
			'name' => 'Arai',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_49.gif',
					'img'=>'https://img.webike.net/catalogue/images/48437/RX-7X_NAKASUGA21_P.jpg'),
		array('id'=>'364',
			'name' => 'KOMINE',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_364.gif',
					'img'=>'https://img.webike.net/catalogue/images/53473/07-590_01.jpg'),
		array('id'=>'635',
			'name' => 'RS TAICHI',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_635.gif',
					'img'=>'https://img.webike.net/catalogue/images/52764/RSB278BR01_01.jpg'),
		array('id'=>'300',
			'name' => 'HONDA',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_300.gif',
					'img'=>'https://img.webike.net/catalogue/images/48637/08L70K0FJ00_01.jpg'),
		array('id'=>'177',
			'name' => 'DAYTONA',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_177.gif',
					'img'=>'https://img.webike.net/catalogue/19197/75462.jpg'),
		array('id'=>'411',
			'name' => 'Magical Racing',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_411.gif',
					'img'=>'https://img.webike.net/catalogue/images/43684/001-GSXS16-04A0_01.jpg'),
		array('id'=>'354',
			'name' => 'KIJIMA',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_354.gif',
					'img'=>'https://img.webike.net/catalogue/images/46462/210-251_06.jpg'),
		array('id'=>'2162',
			'name' => 'HONDA RIDING GEAR',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_2162.gif',
					'img'=>'https://img.webike.net/catalogue/images/54109/0SYEXY3B_01.jpg'),
		array('id'=>'983',
			'name' => 'TANAX motofizz',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_983.gif',
					'img'=>'https://img.webike.net/catalogue/images/35221/MFK-238_01.jpg'),
		array('id'=>'289',
			'name' => 'HenlyBegins',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_289.gif',
					'img'=>'https://img.webike.net/catalogue/images/45942/96909.jpg'),
		array('id'=>'179',
			'name' => 'DEGNER',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_179.gif',
					'img'=>'https://img.webike.net/catalogue/images/53530/NB-163_06.jpg'),
		array('id'=>'732',
			'name' => 'SUNSTAR',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_732.gif',
					'img'=>'https://img.webike.net/catalogue/10063/EKSTD520-1.jpg'),
		array('id'=>'186',
			'name' => 'DID',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_186.gif',
					'img'=>'https://img.webike.net/catalogue/10626/530vx_gold.jpg'),
		array('id'=>'1302',
			'name' => 'T-REV',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1302.gif',
					'img'=>'https://img.webike.net/catalogue/images/13355/4340_1.jpg'),
		array('id'=>'209',
			'name' => 'EFFEX',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_209.gif',
					'img'=>'https://img.webike.net/catalogue/images/6188/ebb311b_1.jpg'),
		array('id'=>'1293',
			'name' => 'XAM',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1293.gif',
					'img'=>'https://img.webike.net/catalogue/18118/xam_classic.jpg'),
		array('id'=>'1601',
			'name' => 'SSK',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1601.gif',
					'img'=>'https://img.webike.net/catalogue/images/24039/lv-s-sr.jpg'),
		array('id'=>'427',
			'name' => 'MDF',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_427.gif',
					'img'=>'https://img.webike.net/catalogue/images/11097/mirror-6M-SV-mozinasi.jpg'),
		array('id'=>'1635',
			'name' => 'MAVERICK',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_1635.gif',
					'img'=>'https://img.webike.net/catalogue/images/30996/ume_tsuya_plus3.jpg'),
		array('id'=>'23',
			'name' => 'AC PERFORMANCE LINE',
					'brandimg'=>'https://img.webike.net/sys_images/brand2/brand_23.gif',
					'img'=>'https://img.webike.net/catalogue/13482/23_bkgo_front.jpg'),
		];
@endphp
<div id="special_price" class="ct-sale-page form-group clearfix">
    <div class="double11-game-title text-center">
        <h2 class="font-bold size-15rem">Webike 1111購物節—價格改定專區</h2>
    </div>
    <div class="content-box-sale clearfix">
        <ul class="product-sale">
            <li class="li-product-sale">
                <div class="photo-box">
                    <ul class="clearfix">
                        <li class="">
                        	<a href="https://www.webike.tw/collection/category/specialprice" target="_blank">
	                        	<figure class="zoom-image">
		                    		<img class="hidden-xs" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/specialprice_370.jpg')}}" alt="價格再定">
		                    		<img class="hidden-md hidden-lg" src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/specialprice_345.jpg')}}" alt="價格再定">
	                    		</figure>
                    		</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <div id="special_price_brand" class="row box-content winter-collerction">
		<div class="ct-winter-collerction">
	        <ul id="special_price_brand" class="product-list owl-carouse-advertisement-test  owl-loaded owl-drag ">
	    		@foreach($special_price_brands as $title => $special_price_brand)
	       			<li id="{{ $special_price_brand['id'] }}" class="brand_url_rewrite">
                   		<div class="product-grid">
                   			<ul class="clearfix">
                   				<a href="{{route('parts', 'br/'.$special_price_brand['id'])}}" target="_blank">
	                   				<li class="col-xs-12 col-sm-7 col-md-7">
	                   					<div class="zoom-container">
			                   				<figure class="zoom-image">
			                   					<img src="{{$special_price_brand['brandimg']}}">
			                   					<span class="helper"></span>
			                   				</figure>
		                   				</div>
							    	</li> 
							    	<li class="col-xs-5 col-sm-5 col-md-5 hidden-xs">
							    		<div class="zoom-container">
			                   				<figure class="zoom-image">
			                   					<img src="{{$special_price_brand['img']}}" >
			                   					<span class="helper"></span>
			                   				</figure>
		                   				</div> 
							    	</li>
						    	</a>
		    				</ul>
			    		</div>
			    		<div class="product-grid">
					        <span class="manufacturer">{{$special_price_brand['name']}}</span>
		    			</div>
		    		</li>
		    	@endforeach		
	        </ul>
		</div>
	</div>
</div>
<script>
	$('.owl-carouse-advertisement-test').addClass('owl-carousel').owlCarousel({
	center:true,
 	loop:true,
    margin:10,
    nav:true,
    mouseDrag:false,
    touchDrag:false,
    slideBy : 1,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
</script>