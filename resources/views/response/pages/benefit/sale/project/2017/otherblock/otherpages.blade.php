<style>
	.otherpages .photo-box li{
		width:100% !important;
	}
</style>
<?php
$otherpages = array(
	'更多會員好康'=>array(
		'預測總冠軍'=>array(
			'description'=>'預測2017MotoGP各分站前三名，猜對即可贏得免費點數，並有機會贏得K-3 SV Top Tartaruga 賽車安全帽、Webike套裝紀念貼紙及300元禮券',
			'img'=>assetRemote('image/benefit/big-promotion/gpbanner1.png'),
			'url'=>URL::route('benefit-event-motogp-2017')
		),
		'MyBike'=>array(
			'img'=>assetRemote('image/benefit/big-promotion/newmybike1200.png'),
			'description'=>'只要您首次登錄「MyBike」，即可獲得$100元點數',
			'url'=>URL::route('benefit-event-mybike')
		),
		'本月加價購'=>array(
			'img'=>assetRemote('image/benefit/big-promotion/addprice345.png'),
			'description'=>'本月精選加價購,「WebikeGarage」伸縮漏斗只要$1元，共多達20項優惠加價商品全面更新!',
			'url'=>URL::route('cart')
		)
	)
);
?>
@foreach($otherpages as $title => $pageinfo)
	<div class="section clearfix" id="more_benefit">
		<div id="item1" class="ct-sale-page form-group clearfix otherpages">
		    <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
		        <h2 class="font-bold size-15rem">{{$title}}</h2>
		    </div>
		    <div class="content-box-sale">
		        <ul class="product-sale">
		        	@foreach($pageinfo as $pagename => $info)
			            <li class="li-product-sale col-xs-12 col-sm-12 col-md-4">
			                <div class="text-center font-bold othertitle">
			                	<h2>{{$pagename}}</h2>
			                </div>
			                <a href="{{$pageinfo[$pagename]['url'].'?'.$rel_parameter}}" class="clearfix" target="_blank">
			                    <div class="photo-box">
			                        <ul class="clearfix">
			                            <li class="col-xs-12 col-sm-12 col-md-12 text-center"><figure class="zoom-image col-sm-block col-xs-block"><img src="{{$pageinfo[$pagename]['img']}}" alt=""></figure></li>
			                        </ul>
			                    </div>
			                </a>
			                <div class="otherlink">
			                	<span class="">{{$pageinfo[$pagename]['description']}}</span>
			                </div>
			            </li>
			        @endforeach
		        </ul>
		    </div>
		</div>
	</div>
@endforeach