@extends('response.layouts.1column')
@section ('style')
<style>
	ul li{
		list-style-type: none;
	}
	.all-page{
		background-image: url({{ assetRemote('/image/promotion/2017Christmas/snowbackground.gif') }});
	}
	.breadcrumb-item a span{
		color: #fff;
	}
	.zoom-image:hover img{
        -webkit-transform: scale(1.2);
        -moz-transform: scale(1.2);
        -ms-transform: scale(1.2);
        -o-transform: scale(1.2);
        transform: scale(1.2);
        opacity: 1;
    }
    .clerafix{
    	overflow: auto;
		zoom: 1;
    }
	.Christmas-container{
		
		font-size: 1rem !important;
	}
	.Christmas-area{
		border:1px solid;
		margin-bottom: 20px;
		background-color: #fff;
	}
	.Christmas-area-benefit{
		border:1px solid;
		margin-bottom: 100px;
		background-color: #fff;
	}
	.Christmas-img{
		padding: 20px;
		border-bottom: 1px solid;
	}
  	.Christmas-title{
  		padding: 10px;
  		background-color: #b30306;
  		color: #fff;
  		text-align: center;
		font-weight: bold;
	}
	.Christmas-text .Christmas-text-title{
		padding: 0px;
		margin-top: 10px;
		list-style-type: none;
		font-size: 1rem !important;

	}
	.Christmas-text .Christmas-text-detall{
		margin-top: 10px;
		list-style-type: none;
		font-size: 1rem !important;
	}
	.Christmas-text .Christmas-text-money-title{
		list-style-type: none;
		font-size: 1rem !important;
	}
	.Christmas-text .Christmas-text-money{
		list-style-type: none;
		font-size: 1rem !important;
	}	
	.Christmas-text p:nth-child(1){
		margin-top: 0px;
	}
	.Christmas-text p{
		margin-top: 10px;
		font-size: 1rem !important;
	}
	.Christmas-text{
		padding: 20px ;
	}
	.Christmas-benefit{
		padding: 20px 20px 7px 20px;
		margin-right: -5px;
		margin-left: -5px;
	}
	.Christmas-benefit-activity{
		padding: 0px 5px 0px 5px;
		 
	}
	.Christmas-benefit-activity-text{
		padding: 10px 0px 0px 15px ;
		overflow: auto;
		height: 62px;
	}
	.Christmas-benefit-activity-img{
		text-align: center;
    	display: block;
    	width: 100%;
	}
	.Christmas-container .price_list li{
		padding:0px;
	}
	.Christmas-container .price_area{
		background: #e0d470;
	}
	.Christmas-container .category_area ul{
	    padding: 2% 0 0 2%;
	}
	.Christmas-container .category_area ul li{
	    padding: 0 2% 2% 0;
	}
	.Christmas-container .category_area ul li a img:hover{
		outline: 3px solid #e0d470;
	}
	.Christmas-container .price_area,.Christmas-container .price_area .zoom-image{
		padding: 0px;
	}
	@media (max-width: 767px){
		.Christmas-container .category_area{
			padding: 0px;
		}
	}
	@media(min-width:1156px){
		.Christmas-text .Christmas-text-title{
			padding: 0px;
			width: 9%;
			margin-top: 10px;
			list-style-type: none;
			font-size: 1rem !important;
			text-align: center;
		}
		.Christmas-text .Christmas-text-detall{
			width:89%;
			margin-top: 10px;
			padding: 0px;
			list-style-type: none;
			font-size: 1rem !important;
		}
		.Christmas-text .Christmas-text-money-title{
			padding: 0px;
			width: 9%;
			list-style-type: none;
			font-size: 1rem !important;
		}
		.Christmas-text .Christmas-text-money{
			padding: 0px;
			width:89%;
			list-style-type: none;
			font-size: 1rem !important;

		}
	}
	@media(max-width:1155px;)}{
		.Christmas-text .Christmas-text-title{
			padding: 0px;
			width: 11%;
			margin-top: 10px;
			list-style-type: none;
			font-size: 1rem !important;
		}
		.Christmas-text .Christmas-text-money-title{
			padding: 0px;
			width: 11%;
			list-style-type: none;
			font-size: 1rem !important;
		}

	}
	@media(max-width:768px){
        .Christmas-benefit-activity{
            width:55%;
            margin-left: 22.5%;
            margin-right: 22.5%;
        }
    	.Christmas-benefit{
			padding: 10px 10px 10px 10px;
		}
		.Christmas-benefit-activity-img{
			margin-left: 0px;
		}
	}
	@media(max-width:450px){
        .Christmas-benefit-activity{
            width:80%;
            margin-left: 10%;
            margin-right: 10%;
        }
    	.Christmas-benefit-activity-text{
			padding: 10px 0px 0px 0px!important;
			overflow: auto;
			height: 79px;
		}
	}

</style>
@endsection
@section ('middle')
<div class="Christmas-container">
	<div class="Christmas-area">
		
		<img class="hidden-xs" src="{{ assetRemote('/image/promotion/2018Christmas/1812_370.jpg')}}">
		<img class="hidden-md hidden-lg hidden-sm" src="{{ assetRemote('/image/promotion/2018Christmas/1812_628.jpg')}}">
	</div>
	<div class="Christmas-area">
		<div class="Christmas-title"><h1>全館免運費</h1></div>
		<div class="Christmas-img zoom-image">
			<a href="{{ route('cart') }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018Christmas/1812_bonus_370.jpg') }}"></a>
		</div>
		<div class="Christmas-text">
			<p>【活動內容】HO~HO~HO~Webike化身為耶誕老公公來囉～您挑選的那份耶誕禮物我們幫您免費送貨到府！</p>
			<p>【活動辦法】活動期間發送給所有會員一張免運費的折價券(90元折價券)，此折價卷適用全館所有商品，為一次性使用。</p>
			<p>【使用說明】登入後在購物車頁面中間找到折價券的欄位，選擇<a href="{{ route('cart') }}" target="_blank">「耶誕老公公送貨[NT90]」</a>並點選輸入，即可折抵運費90元。</p>
			<p>【使用期限】2018/12/24 (00:00) ~ 2018/12/25 (23:59)</p>
		</div>
	</div>
	<div class="Christmas-area">
		<div class="Christmas-title "><h1>預算選禮物</h1></div>
		<div class="Christmas-img price_area">
			@php
				$price_gifts = ['1-1000','1000-1999','2000-5000','5000-99999'];
			@endphp
			<ul class="clerafix price_list">
				@foreach($price_gifts as $key => $price_gift)
					<li class="col-md-3 col-sm-3 col-xs-6">
						<a href="{{ route('parts') . '?price=' . $price_gift }}" target="_blank" class="zoom-image"><img src="{{ assetRemote('/image/promotion/2018Christmas/price/'. $key .".jpg") }}"></a>	
					</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="Christmas-area">
		<div class="Christmas-title "><h1>分類挑禮物</h1></div>
		<div class="Christmas-img category_area">
				@php
					$ca_gifts = ['3000-3001','3000-3020-3021','3000-3020-3071','3000-3020-3123','3000-3260-1331-3135','3000-3260-1331-3125','1000-1001-1003','1000-1110-1109','1000-1110-1121'];
				@endphp
			<ul class="clerafix">
				@foreach($ca_gifts as $key => $ca_gift)
					<li class="col-md-4 col-sm-6 col-xs-6">
						<a href="{{ route('parts',['ca/'.$ca_gift])}}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018Christmas/category/'. $key .".jpg") }}"></a>	
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>
	<div class="Christmas-area-benefit">
		<div class="Christmas-title"><h1>其他好康主打</h1></div>
		<div class="row Christmas-benefit">
			<div class="Christmas-benefit-activity col-md-4 col-sm-12 col-xs-12  ">
				<div class="zoom-image Christmas-benefit-activity-img ">
					<a href="{{ \URL::route('outlet') }}" target="_blank" ><img src="{{ assetRemote('/image/promotion/2018Christmas/12OUTLET345.jpg') }}"></a>
				</div>
				<div class="Christmas-benefit-activity-text ">
					<span class=" size-10rem ">OUTLET出清商品，賣不掉就再降，快來尋找專屬於你的寶物。</span>
				</div>
			</div>
			<div class="Christmas-benefit-activity col-md-4 col-sm-12 col-xs-12 ">
				<div class="zoom-image Christmas-benefit-activity-img">
					<a  href="{{ \URL::route('benefit-sale-month-promotion',['code' => '2018-12']) }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018Christmas/1812_promo_345.jpg') }}"></a>
				</div>
				<div class="Christmas-benefit-activity-text ">
					<span class=" size-10rem ">叮叮噹叮叮噹～給您最貼心溫暖的選擇，精選商品大折扣、熱門車型改裝品點數5倍回饋。</span>
				</div>
			</div>
			<div class="Christmas-benefit-activity col-md-4 col-sm-12 col-xs-12 ">
				<div class="zoom-image Christmas-benefit-activity-img">
					<a  href="{{ \URL::route('collection-type-detail',['type' => 'category', 'url_rewrite' => '2019fall']) }}" target="_blank"><img src="{{ assetRemote('/image/promotion/2018Christmas/fall345.jpg') }}"></a>
				</div>
				<div class="Christmas-benefit-activity-text ">
					<span class=" size-10rem ">冷冷細雨的冬天，防風防寒、又暖又帥的全都在最新騎士用品特輯。</span>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection
@section('script')
<script>
	$('.owl-carouse-loop').addClass('owl-carousel').owlCarousel({
	autoplay:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	loop:true,
	margin:10,
	nav:true,
    slideBy : 3,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});

</script>
@endsection