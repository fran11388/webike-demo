<style>
.main_categories li{
	padding:0px;
	display:inline-block;
	width: 16.6%;
	float:left;
}
.main_categories li img{
	max-width:100%;
}
.main_categories a div {
	min-height: 110px;
	background-size: 97% !important;
	margin-left:auto;
	margin-right:auto;
}
.main_categories a div:hover {
	min-height: 110px;
	background-size: 97% !important;
	margin-left:auto;
	margin-right:auto;
}
.main_categories_fixed li{
	padding:0 0 5px 0;
	display:inline-block;
	width: 16.6%;
	float:left;
}
.main_categories_fixed li img{
	max-width:100%;
}
.main_categories_fixed div {
	min-height: 65px;
	background-size: 90% !important;
	margin-left:auto;
	margin-right:auto;
}
.main_categories_fixed div:hover {
	min-height: 65px;
	background-size: 90% !important;
	margin-left:auto;
	margin-right:auto;
}
.home {
	background: url({{assetRemote('image/benefit/big-promotion/summersale1.png')}}) no-repeat;
	background-position: center
}
.rider {
	background: url({{assetRemote('image/benefit/big-promotion/rider.png')}}) no-repeat;
	background-position: center
}
.custom {
	background: url({{assetRemote('image/benefit/big-promotion/custom.png')}}) no-repeat;
	background-position: center
}
.special {
	background: url({{assetRemote('image/benefit/big-promotion/special1.png')}}) no-repeat;
	background-position: center
}
.travel {
	background: url({{assetRemote('image/benefit/big-promotion/travel.png')}}) no-repeat;
	background-position: center
}
.fixer {
	background: url({{assetRemote('image/benefit/big-promotion/fixer.png')}}) no-repeat;
	background-position: center
}
.genuine {
	background: url({{assetRemote('image/benefit/big-promotion/genuine.png')}}) no-repeat;
	background-position: center
}
.home1 {
	background: url({{assetRemote('image/benefit/big-promotion/summersale2.png')}}) no-repeat #fc37a2;
	background-position: center
}
.home:hover,.home.hover {
	background: url({{assetRemote('image/benefit/big-promotion/summersale2.png')}}) no-repeat #fc37a2;
	background-position: center
}
.special:hover,.special.hover {
	background: url({{assetRemote('image/benefit/big-promotion/special2.png')}}) no-repeat #fc37a2;
	background-position: center
}
.rider:hover,.rider.hover {
	background: url({{assetRemote('image/benefit/big-promotion/rider1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.custom:hover,.custom.hover {
	background: url({{assetRemote('image/benefit/big-promotion/custom1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.travel:hover,.travel.hover {
	background: url({{assetRemote('image/benefit/big-promotion/travel1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.fixer:hover,.fixer.hover {
	background: url({{assetRemote('image/benefit/big-promotion/fixer1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.genuine:hover,.genuine.hover {
	background: url({{assetRemote('image/benefit/big-promotion/genuine1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.home_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/summersale1.png')}}) no-repeat white;
	background-position: center
}
.special_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/special_fixed.png')}}) no-repeat white;
	background-position: center
}

.rider_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/rider-fixed.png')}}) no-repeat white;
	background-position: center
}
.custom_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/custom-fixed.png')}}) no-repeat white;
	background-position: center
}
.travel_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/travel-fixed.png')}}) no-repeat white;
	background-position: center
}
.fixer_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/fixer-fixed.png')}}) no-repeat white;
	background-position: center
}
.genuine_fixed {
	background: url({{assetRemote('image/benefit/big-promotion/genuine-fixed.png')}}) no-repeat white;
	background-position: center
}
.home_fixed1 {
	background: url({{assetRemote('image/benefit/big-promotion/summersale2.png')}}) no-repeat #fc37a2;
	background-position: center
}
.home_fixed:hover,.home_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/summersale2.png')}}) no-repeat #fc37a2;
	background-position: center
}
.home_fixed:hover,.home_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/summersale2.png')}}) no-repeat #fc37a2;
	background-position: center
}
.special_fixed:hover,.special_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/special_fixed1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.rider_fixed:hover,.rider_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/rider-fixed1.png')}}) no-repeat #fc37a2;;
	background-position: center
}
.custom_fixed:hover,.custom_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/custom-fixed1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.travel_fixed:hover,.travel_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/travel-fixed1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.fixer_fixed:hover,.fixer_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/fixer-fixed1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.genuine_fixed:hover,.genuine_fixed.hover {
	background: url({{assetRemote('image/benefit/big-promotion/genuine-fixed1.png')}}) no-repeat #fc37a2;
	background-position: center
}
.main-container {
	background-color: white;
}
@media (max-width: 992px) {
	.main_categories a div {
		min-height: 110px;
		background-size: 57%;
		margin-left:auto;
		margin-right:auto;
	}
	.main_categories a div:hover {
		min-height: 110px;
		background-size: 57%;
		margin-left:auto;
		margin-right:auto;
	}

	.main_categories li {
		width:16.6%;
	}
	.main_categories_fixed li{
		width:16.6%;
	}
}
@media (max-width: 770px) {
	.main_categories a div {
		min-height: 110px;
		background-size: 200px;
		width: 200px;
    	margin-left:auto;
    	margin-right:auto;
	}
	.main_categories a div:hover {
		min-height: 110px;
		background-size: 200px;
		margin-left:auto;
		margin-right:auto;
	}
	.fixed-container {
		display:none !important;
	}

	.main-container-home {
		background-color: #fc37a2 !important;
	}
}
.main_categories_fixed {
    position: fixed;
    top: 50px;
    z-index: 2;
	padding-right:15px;
	padding-left:15px;
    max-width:1000px;
    width:100%;
    right: 0;
    left: 0;
    margin-right: auto;
    margin-left: auto;
}
.main_categories {
	margin-left:auto;
    margin-right:auto;
	max-width:1000px;
	padding:0 15px;
}
.fixed-container {
	display:none;
	width:100%;
	position: fixed;
    top: 50px;
    z-index: 2;
    min-height: 68px;
    border-bottom: 3px #777 solid;
	background-color: white;
}
.main-container .active {
	background-color: #fc37a2;
}
.mobile-active {
	background-color: #d4d4d4;
	border-color: #8c8c8c;
}
.main-container .mobile {
	display: block !important;
    padding: 10px !important;
    height:auto !important;
}
.main-container .select2-container .select2-selection--single {
    height: 40px;
}
.main-container .select2-container .select2-selection--single .select2-selection__rendered {
	line-height: 40px !important;
}
.main-container .mobile-link {
	position:relative;
	width:95%;
}
.main-container .mobile-link .loading-box{
	position:absolute;
	right:11px;
	top:0px;
	display:none;
}

.main-container .category-link {

}

</style>
<div class="main-container text-center {{\Request::route()->getName() == 'benefit-sale-2017-promotion' ? 'main-container-home' : ''}}">
	<div class="main_categories">
		<ul class="clearfix text-center">
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2017-promotion').'?'.$rel_parameter}}">
					<div class="home text-center">
					</div>
				</a>
			</li>
			{{--<li class="category-link text-center hidden-xs">--}}
				{{--<a href="{{URL::route('benefit-sale-2017-promotion-specialDiscount').'?'.$rel_parameter}}">--}}
					{{--<div class="special">--}}
					{{--</div>--}}
				{{--</a>--}}
			{{--</li>--}}
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '1000']).'?'.$rel_parameter}}">
					<div class="custom">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3000']).'?'.$rel_parameter}}">
					<div class="rider text-center">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3260']).'?'.$rel_parameter}}">
					<div class="travel">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('benefit-sale-2017-promotion-domestic').'?'.$rel_parameter}}">
					<div class="fixer">
					</div>
				</a>
			</li>
			<li class="category-link text-center hidden-xs">
				<a href="{{URL::route('genuineparts').'?'.$rel_parameter}}" target="_blank">
					<div class="genuine">
					</div>
				</a>
			</li>
			<li class="visible-xs btn-gap-right btn-gap-left content-last-block block mobile-link">
				@if(\Request::route()->getName() != 'benefit-sale-2017-promotion')
					<select class="select2 select-category" name="my_choice[]">
{{--						<option value="{{URL::route('benefit-sale-2017-promotion-specialDiscount').'?'.$rel_parameter}}">特別折扣</option>--}}
	                    <option value="{{URL::route('benefit-sale-2017-promotion-category',['category' => '1000']).'?'.$rel_parameter}}">改裝零件</option>
	                    <option value="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3000']).'?'.$rel_parameter}}">騎士用品</option>
	                    <option value="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3260']).'?'.$rel_parameter}}">旅行用品</option>
	                    <option value="{{URL::route('benefit-sale-2017-promotion-domestic').'?'.$rel_parameter}}">保養維修及工具</option>
	                    <option value="{{URL::route('genuineparts').'?'.$rel_parameter}}">正廠零件5%off</option>
	                </select>
	                @include('response.common.loading.xs')
	            @else
{{--	            	<a href="{{URL::route('benefit-sale-2017-promotion-specialDiscount').'?'.$rel_parameter}}" class="visible-xs btn btn-default btn-gap-right btn-gap-left box btn-gap-top mobile">特別折扣</a>--}}
	            	<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '1000']).'?'.$rel_parameter}}" class="visible-xs btn btn-default btn-gap-right btn-gap-left box btn-gap-top mobile">改裝零件</a>
					<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3000']).'?'.$rel_parameter}}" class="visible-xs btn btn-default btn-gap-right btn-gap-left box mobile">騎士用品</a>
					<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3260']).'?'.$rel_parameter}}" class="visible-xs btn btn-default btn-gap-right btn-gap-left box mobile">旅行用品</a>
					<a href="{{URL::route('benefit-sale-2017-promotion-domestic').'?'.$rel_parameter}}" class="visible-xs btn btn-default btn-gap-right btn-gap-left box mobile">保養維修及工具</a>
					<a href="{{URL::route('genuineparts').'?'.$rel_parameter}}" target="_blank" class="visible-xs btn btn-default box btn-gap-right btn-gap-left genuine-mobile mobile">正廠零件5%off</a>
	            @endif
			</li>
		</ul>
	</div>
</div>
<div class="fixed-container">
	<div class="main_categories_fixed">
		<ul class="clearfix">
			<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
				<a href="{{URL::route('benefit-sale-2017-promotion').'?'.$rel_parameter}}">
					<div class="home_fixed text-center">
					</div>
				</a>
			</li>
			{{--<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">--}}
				{{--<a href="{{URL::route('benefit-sale-2017-promotion-specialDiscount').'?'.$rel_parameter}}">--}}
					{{--<div class="special_fixed text-center">--}}
					{{--</div>--}}
				{{--</a>--}}
			{{--</li>--}}
			<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
				<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '1000']).'?'.$rel_parameter}}">
					<div class="custom_fixed">
					</div>
				</a>
			</li>
			<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
				<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3000']).'?'.$rel_parameter}}">
					<div class="rider_fixed text-center">
					</div>
				</a>
			</li>
			<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
				<a href="{{URL::route('benefit-sale-2017-promotion-category',['category' => '3260']).'?'.$rel_parameter}}">
					<div class="travel_fixed">
					</div>
				</a>
			</li>
			<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
				<a href="{{URL::route('benefit-sale-2017-promotion-domestic').'?'.$rel_parameter}}">
					<div class="fixer_fixed">
					</div>
				</a>
			</li>
			<li class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
				<a href="{{URL::route('genuineparts').'?'.$rel_parameter}}" target="_blank">
					<div class="genuine_fixed">
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>
<script>
    $(window).scroll(function(){

        if ($(this).scrollTop() > 570) {
          $('.fixed-container').show();
        } else {
          $('.fixed-container').hide();
        }
    });

    $(document).ready(function(){
    	var route = "{{\Request::route()->getName()}}";
    	if(route == 'benefit-sale-2017-promotion'){
    		$('.home').addClass('home1');
    		$('.home1').removeClass('home');
    		$('.home1').addClass('active');
    		$('.home_fixed').addClass('home_fixed1');
    		$('.home_fixed1').removeClass('home_fixed');
    		$('.home_fixed1').addClass('active');
    	}else if(route == 'benefit-sale-2017-promotion-category'){
    		var route = "{{\Request::segment(6)}}";
    		if(route == 3000){
    			$('.rider').toggleClass("hover");
    			$('.rider_fixed').toggleClass("hover");
    		}else if(route == 1000){
    			$('.custom').toggleClass("hover");
    			$('.custom_fixed').toggleClass("hover");
    		}else{
    			$('.travel').toggleClass("hover");
    			$('.travel_fixed').toggleClass("hover");
    		}
    	}else if(route == 'benefit-sale-2017-promotion-domestic'){
    		$('.fixer').toggleClass("hover");
    		$('.fixer_fixed').toggleClass("hover");
    	}else{
    		$('.special').toggleClass("hover");
    		$('.special_fixed').toggleClass("hover");
    	}

    	var url = window.location.href;
    	$('.select-category option').each(function(key,element){
    		if($(this).val() == url){
    			$(this).attr('selected', true);
    		}
	    });
    });

    $(document).on("select2:select", function (e) { 
    	$('.select-category option:selected').each(function(key,element){
    		location.href = $(this).val();
    		$('.loading-box').show();
	    });
    });
    
</script>