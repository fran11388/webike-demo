<?php
$activities = array(
	'WANTED 每日搜索任務'=>array(
			'description'=>'完成每日任務，即可獲得10元現金點數，31天全數答對，再送200點，最大510元點數回饋。',
			'img'=>assetRemote('image/benefit/big-promotion/Wanted.png'),
			'url'=>URL::route('benefit-event-webikewanted'),
			'id' => 'wanted'
			),
    '愛用國貨-點數現折'=>array(
        'description'=>'愛用國貨，精選多項優質品牌，上千項點數優惠商品，回饋的現金點數立即現折。',
        'img'=>assetRemote('image/benefit/big-promotion/pointnow.png'),
        'url'=>'https://www.webike.tw/collection/brand/Pointdiscount',
		'id' => 'country_discount'
    ),
    'OUTLET現貨出清'=>array(
        'description'=>'「Webike Outlet」定期上架各大品牌商品，只要「賣不掉，就調降」售價直到您滿意，歡迎大家搶便宜!!',
        'img'=>assetRemote('image/benefit/big-promotion/OUTLETJULY370.png'),
        'url'=>URL::route('outlet'),
		'id' => 'outlet'
    ),
//	'正廠零件95折優惠'=>array(
//			'description'=>'凡在SPRING SALE期間內購買正廠零件，全品項95折優惠!
//
//日本 HONDA、 YAMAHA、 SUZUKI、 KAWASAKI、 美國 HARLEY-DAVIDSON、 德國 BMW、 英國 TRIUMPH、 義大利 DUCATI、 APRILIA、 PIAGGIO、 VESPA、 HUSQVARNA、 MOTOGUZZI、 GILERA、 奧地利 KTM、 泰國 HONDA、 YAMAHA、 十七個進口廠牌。
//
//光陽KYMCO、 三陽SYM、 台灣山葉YAMAHA、 台鈴SUZUKI、 宏佳騰AEON、 PGO摩特動力、 哈特佛HARTFORD、七大國產廠牌。
//														',
//			'img'=>assetRemote('image/benefit/big-promotion/genuine370.png'),
//			'url'=>URL::route('genuineparts')
//			),


);
?>
@foreach($activities as $activeName => $activeInfo)
	<div class="section clearfix">
		<div id="{{ $activeInfo['id'] }}" class="ct-sale-page form-group clearfix">
		    <div class="title col-xs-12 col-sm-12 col-md-12 text-center">
		        <h2 class="font-bold size-15rem">{{$activeName}}</h2>
		    </div>
		    <div class="content-box-sale">
		        <ul class="product-sale">
		            <li class="li-product-sale col-xs-12 col-sm-12 col-md-12">
		                <a href="{{$activities[$activeName]['url']}}?rel=2017-10-2017Fall" class="clearfix" target="_blank">
		                    <div class="photo-box">
		                        <ul class="clearfix">
		                            <li class="col-xs-12 col-sm-12 col-md-12"><figure class="zoom-image"><img src="{{$activities[$activeName]['img']}}" alt=""></figure></li>
		                        </ul>
		                    </div>
		                </a>
		                <div class="description">
		                	<span class="">{{$activities[$activeName]['description']}}</span>
		                </div>
		            </li>
		        </ul>
		    </div>
		</div>
	</div>
@endforeach