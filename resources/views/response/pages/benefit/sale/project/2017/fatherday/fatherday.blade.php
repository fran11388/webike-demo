@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{assetRemote('css/pages/intructions.css')}}">
    <style>
        .btn-danger{
            width:200px;
            height:40px;
            padding:10px;
        }
        .container-intruction-detail .ul-membership span {
            font-size:1rem !important;
        }
        .more-benefit {
            border:0px !important;
            padding-bottom:0px !important;
        }
        .more-benefit ul {
            border:1px solid #dcdee3;
        }
        .more-benefit li {
            list-style-type: none;
            padding:0px;
        }
        .more-benefit li .width-full {
            padding:10px;
            border:1px solid #dcdee3;
        }
        .more-benefit li .text{
            height:45px !important;
            overflow: auto !important;
        }
        .ul-membership li {
            margin-bottom:0px !important;
        }
        .other-collection img{
            max-height:177px;
        }
        .endblock {
            margin-bottom:80px;
        }
        @media(max-width:767px){
            .endblock > div {
                text-align:center !important;
            }
        }
    </style>
@stop
@section('middle')
    <div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership text-center">
                <li>
                    <div><a href="{{ \URL::route('cart') }}" target="_blank" title="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            <img src="{{ assetRemote('image/benefit/sale/fatherday/banner.png')}}" alt="88節免運費活動{{$tail}}">
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>日本盆節假期，部分進口商品會有所延誤</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        8月9日 ~ 8月16日適逢日本盂蘭盆節連續假期，日本進口商品的交期及出貨將有所延誤，不便之處敬請見諒。<br>
                        若需要在此期間添購進口商品，請於8月4日之前訂購，或是選購有"庫存OK"標示的商品，也可以選購國產品牌商品，較不會耽誤您的寶貴時間。<br>
                        若於連續期間訂購的進口商品，最快將於2017年8月16日起陸續抵達台灣，我們將會在最短的時間為您進行配送作業。<br>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>88節免運費活動說明</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        「Webike」祝各位帥氣的老爸~佳節愉快!<br>
                        在8月3日~8月7日一連五天，全館購物不限金額免運費，在您的”<a href="{{ \URL::route('cart') }}" target="_blank" title="2017 88節免運費活動 - 「Webike-摩托百貨」">購物車</a>”折價券欄位中，將會有兩張$100元的折價券，使用期限至8月7日止。<br>
                        ※注意事項<br>
                        1.折價券每次購物僅能使用一張，使用前請先登入會員。<br>
                        2.本次活動折價券僅能折扣運費，不能與其他折價券合併使用。<br>
                        3.請注意，購物滿2000元立即享有免運費的優惠，無法在折扣運費。
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>折價券使用說明</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        1.登入會員後，您可以從網頁右上方的「<a href="{{ \URL::route('cart') }}" target="_blank">購物車</a>」按鈕進入到購物車頁面查看折價券，進到購物車頁面之後，您可以在購物車頁面中間找到折價券的欄位，上面會顯示您目前有幾張尚未使用的折價券，點擊後會出現下拉式選單，裡面可以選擇您可以使用的折價券。
                    </span><br><br>
                    <div class="text-center">
                        <a href="{{ \URL::route('cart') }}" target="_blank" title="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            <img src="{{ assetRemote('image/benefit/sale/fatherday/6.png') }}" alt="88節免運費活動{{$tail}}">
                        </a>
                    </div><br><br>
                    <span>
                        2.選擇「88節限時免運費[NT100]」，並點選輸入，右邊運費的欄位將會由原本的100元變成0元。
                    </span><br>
                    <span class="font-color-red">※請注意，使用期限為8/3(四)~8/7(一)。</span>
                    <br><br>
                    <div class="text-center">
                        <a href="{{ \URL::route('cart') }}" target="_blank" title="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            <img src="{{ assetRemote('image/benefit/sale/fatherday/7.png') }}" alt="88節免運費活動{{$tail}}">
                        </a>
                    </div><br><br><br>
                </li>
            </ul>
        </div>
    </div>
    <div class="width-full more text-center block">
        <a href="{{ \URL::route('cart') }}" target="_blank" title="2017 88節免運費活動 - 「Webike-摩托百貨」">
            <img src="{{ assetRemote('image/benefit/sale/fatherday/more.png') }}" alt="88節免運費活動{{$tail}}">
        </a>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>本月會員好康</h2>
        </div>
        <div class="container-intruction-detail more-benefit">
            <ul class="row">
                <li class="col-md-6 col-sm-6 col-xs-12">
                    <div class="width-full">
                        <a href="{{ \URL::route('benefit-sale-month-promotion',['code' => '2017-08']) }}" title="2017 88節免運費活動 - 「Webike-摩托百貨」" target="_blank">
                            <div class="image-box text-center">
                                <img src="{{ assetRemote('image/benefit/sale/fatherday/1.png') }}" alt="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            </div>
                        </a>
                    </div>
                    <div class="width-full">
                        <div class="text">
                             <span>
                                8月sale:8月精選品牌、分類、車型優惠實施中!時間有限，請把握期限內購買!
                            </span>
                        </div>
                    </div>
                </li>
                <li class="col-md-6 col-sm-6 col-xs-12">
                    <div class="width-full">
                        <a href="{{ \URL::route('outlet') }}" title="2017 88節免運費活動 - 「Webike-摩托百貨」" target="_blank">
                            <div class="image-box text-center">
                                <img src="{{ assetRemote('image/benefit/sale/fatherday/4.png') }}" alt="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            </div>
                        </a>
                    </div>
                    <div class="width-full">
                        <div class=" text">
                         <span>
                            OUTLET:每周更新，賣不掉就降價，猶豫就買不到了!
                        </span>
                        </div>
                    </div>
                </li>
                <li class="col-md-6 col-sm-6 col-xs-12">
                    <div class="width-full">
                        <a href="{{ \URL::route('cart') }}" title="2017 88節免運費活動 - 「Webike-摩托百貨」" target="_blank">
                            <div class="image-box text-center">
                                <img src="{{ assetRemote('image/benefit/sale/fatherday/2.png') }}" alt="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            </div>
                        </a>
                    </div>
                    <div class="width-full">
                        <div class="text">
                         <span>
                            加價購:本月不限品項，訂單金額滿3000，加購90度延長氣嘴只要9元!
                        </span>
                        </div>
                    </div>
                </li>
                <li class="col-md-6 col-sm-6 col-xs-12">
                    <div class="width-full">
                        <a href="{{ \URL::route('review') }}" title="2017 88節免運費活動 - 「Webike-摩托百貨」" target="_blank">
                            <div class="image-box text-center">
                                <img src="{{ assetRemote('image/benefit/sale/fatherday/3.png')}}" alt="2017 88節免運費活動 - 「Webike-摩托百貨」">
                            </div>
                        </a>
                    </div>
                    <div class="width-full">
                        <div class="text">
                         <span>
                            評論:本月撰寫風鏡評論，最大100元現金點數回饋!
                        </span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="width-full text-center block endblock clearfix">
        <div class="col-md-6 col-sm-6 col-xs-12 text-right">
            <a class="btn btn-info btn-basic-wide box" href="{{ \URL::route('benefit') }}" target="_blank">其他會員好康</a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 text-left">
            <a class="btn btn-info btn-basic-wide" href="{{ \URL::route('shopping') }}" target="_blank">首頁</a>
        </div>
    </div>
@stop
