@extends('response.layouts.1column')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{assetRemote('css/pages/intructions.css')}}">
    <style>
        .btn-danger{
            width:200px;
            height:40px;
            padding:10px;
        }
        .container-intruction-detail .ul-membership span {
            font-size:1rem !important;
        }
    </style>
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
            <ul class="ul-membership text-center">
                <li>
                    <div><img src="{{assetRemote('image/benefit/big-promotion/2018/fall/banner/count_discount370.jpg')}}" alt="愛用國貨點數現折說明{{$tail}}"></div>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>什麼是點數現折?</h2>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                    <span>
                        凡是在「Webike-摩托百貨」購物網站內購買商品，皆可獲得「點數」回饋，一般點數獲得需要等到訂單流程結束後才會將點數發送至您的帳號內，「點數現折」意思即為在您將商品加入購物車後，直接將目前商品的點數直接在購物車中馬上折抵，不會進行累積。
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-container-intruction">
        <div class="title-main-box">
            <h2>點數現折使用說明</h2>
        </div>
        <div class="container-intruction-detail">
	        <ul class="ul-membership">
                <li>
                    <span>
                        1.在「愛用國貨-點數現折」的頁面中的商品，全數都有點數現折優惠。
                    </span><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/sale/pointinfo/brand-Pointdiscoun1.png')}}" alt="愛用國貨點數現折說明{{$tail}}"></div>
	            </li>
                <li>
                    <span>
                        2.進入至商品頁之後，您可以在商品頁裡面看到有標示「愛用國貨，POINT點數現折活動」，欄位中間會顯示此商品可現折的回饋點數。
                    </span><br><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/sale/pointinfo/brand-Pointdiscoun2.png')}}" alt="愛用國貨點數現折說明{{$tail}}"></div><br><br>
                    <span>
                        3.將商品加入至購物車後，即可在販賣價格欄位中看到，獲得的現金點數已經直接扣掉商品金額，而最下方顯示的本次購物獲得的現金點數獲得”0”點。
                    </span><br><br>
                    <div class="text-center"><img src="{{assetRemote('image/benefit/sale/pointinfo/brand-Pointdiscoun3.png')}}" alt="愛用國貨點數現折說明{{$tail}}"></div><br><br>
                    <span>
                        4.  如果您手頭上有”折價券”與之前累積的”點數”，還是可以在購物車中合併使用。
                    </span><br>
                </li>
	        </ul>
		</div>
        <?php
        /*
        <div class="box-container-intruction">
            <div class="container-intruction-detail">
                <ul class="ul-membership">
                    <li class="text-center">
                        <a class="btn btn-danger" href="{{URL::route('benefit-sale-2017-spring')}}?ca=recommend" target="_blank">回到點數現折頁面</a>
                    </li>
                </ul>
            </div>
        </div>
        */
        ?>
	</div>
@stop
