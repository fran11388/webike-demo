@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/cartpagedetail.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/brand.css') }}">

    <style>
        .join-myBike {
            margin-right:10px !important;
        }
    </style>
    <?php
        if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE])){
            $zones = [
                '9' => 19,
                '10' => 20,
            ];
        }else{
            $zones = [
                '9' => 9,
                '10' => 10,
            ];
        }
    ?>
@stop
@foreach($motors as $motor)
    <?php 
        $manufacturer = $motor->manufacturer;
        $url_rewrite = $motor->url_rewrite;
        $faurl_rewrite = $motor->faurl_rewrite;
    ?>
@endforeach
    @section('left')
        <?php
        $import_manufacturers = array('HONDA','YAMAHA','SUZUKI','KAWASAKI');
        ?>
        @if(in_array($manufacturer,$import_manufacturers))
            <div class="box-page-group">
                <div class="title-box-page-group">
                    <h3>請選擇排氣量</h3>
                </div>
                <div class="ct-box-page-group">
                    <ul class="ul-menu-ct-box-page-group ul-menu-ct-box-page-group-displacement">
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,50]) }}"><span class="blue-0066c0">- 50cc</span></a>
                        </li>
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,125]) }}"><span class="blue-0066c0">51 - 125cc</span></a>
                        </li>
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,250]) }}"><span class="blue-0066c0">126 - 250cc</span></a>
                        </li>
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,400]) }}"><span class="blue-0066c0">251 - 400cc</span></a>
                        </li>
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,750]) }}"><span class="blue-0066c0">401 - 750cc</span></a>
                        </li>
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,1000]) }}"><span class="blue-0066c0">751 - 1000cc</span></a>
                        </li>
                        <li>
                            <a href="{{URL::route('motor-manufacturer',[$manufacturer,1001]) }}"><span class="blue-0066c0">1001cc -</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
        <div class="box-page-group blank-box-page-group">
            <div class="ct-box-page-group">
                <?php
                $other_manufacturers = array('HARLEY-DAVIDSON','BMW','TRIUMPH','DUCATI','APRILIA','PIAGGIO','VESPA','HUSQVARNA','MOTOGUZZI','GILERA','KTM','光陽','三陽','台灣山葉','宏佳騰','台鈴','PGO摩特動力','哈特佛','CPI');
                $topFive_manufacturers = array('HONDA','YAMAHA','SUZUKI','KAWASAKI','HARLEY-DAVIDSON');

                ?>
                @if(in_array($manufacturer,$topFive_manufacturers))
                    <ul class="ul-menu-ct-box-page-group ul-menu-ct-box-page-group-displacement">
                        <li>

                            <a href="{{URL::route('genuineparts-divide',['import',$faurl_rewrite])}}"><figure class="zoom-image"> <img src="{{assetRemote('/image/factory/'.$faurl_rewrite.'/gen.png')}}" alt="banner"></figure></a>
                        </li>
                        <li>
                            <a href="{{URL::route('summary',['ca/1000/br/'.$url_rewrite])}}"><figure class="zoom-image"> <img src="{{assetRemote('/image/factory/'.$faurl_rewrite.'/custom.png')}}" alt="banner"></figure></a>
                        </li>
                        <li>
                            <a href="{{URL::route('summary',['ca/3000/br/'.$url_rewrite])}}"><figure class="zoom-image"> <img src="{{assetRemote('/image/factory/'.$faurl_rewrite.'/rider.png')}}" alt="banner"></figure></a>
                        </li>
                    </ul>
                @elseif(in_array($manufacturer,$other_manufacturers))
                    <ul class="ul-menu-ct-box-page-group ul-menu-ct-box-page-group-displacement">
                        <li>
                            <a href="{{URL::route('genuineparts-divide',['import',$faurl_rewrite])}}"><figure class="zoom-image"> <img src="{{assetRemote('image/factory/'.$faurl_rewrite.'/gen.png/')}}" alt="banner"></figure></a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
        <div class="box-page-group">
            <div class="title-box-page-group">
                <h3>服務專區</h3>
            </div>
            <div class="ct-box-page-group brand-moto-disp-banner">
                <ul class="ul-banner-ct-box-page-group">
                    @for($i = 1 ; $i <= 5 ; $i++)
                        <li>
                            <div>
                                {!! $advertiser->call($zones[10]) !!}
                            </div>
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
        <div class="box-page-group">
            <div class="title-box-page-group">
                <h3>推薦連結</h3>
            </div>
            <div class="ct-box-page-group brand-moto-disp-banner">
                <ul class="ul-banner-ct-box-page-group">
                    @for($i = 1 ; $i <= 5 ; $i++)
                        <li>
                            <div>
                                {!! $advertiser->call($zones[9]) !!}
                            </div>
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
    @stop
    @section('right')
    <div class="page-right-part box-fix-with hidden-xs">
        <div class="box-page-group">
            <div class="title-box-page-group title-box-cart-page-detail">
                <h3>車市最新販售車輛</h3>
            </div>
            <div class="ct-box-page-group">
                <div class="motor-product-box">
                    <ul class="row owl-carousel-responsive">
                        @foreach($newmotors as $motor_num => $new_motor)
                            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item-product-grid">
                                @include('response.common.product.j',['motor' => $new_motor, 'motor_num' => $motor_num])
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-main-part box-fix-auto">
        <div class="title-main-box">
            <h1>{{$manufacturer.' 車型索引'}}</h1>
        </div>
        <div class="brand-moto-table">
            <div class="brand-moto-table-title">
                <div class="col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">排氣量</span></div>
                <div class="col-md-10 col-sm-10 col-xs-10"><span class="size-10rem">車型名稱</span></div>
            </div>
            <div class="brand-moto-table-content">
                <ul>
                    <?php $num = 1;?>
                    @foreach($motors as $motor)
                        @php
                            if($motor->displacement == 0){
                                $other_motor = $motor;
                            }
                        @endphp
                        @if($motor->displacement != 0)
                            <li>
                                @if($num != $motor->displacement)
                                    <div class="brand-moto-table-content-left col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">{{$motor->displacement.'cc'}}</span>
                                    </div>
                                @else
                                    <div class="brand-moto-table-content-left col-md-2 col-sm-2 col-xs-2"><span class="size-10rem"></span>
                                    </div>
                                @endif
                                <div class="brand-moto-table-content-right col-md-10 col-sm-10 col-xs-10">
                                    <h3 class="float-left"><a href="{{URL::route('summary',['mt/' . $motor->motorurl_rewrite]) }}">{{$motor->name}}</a></h3>
                                    <h3 class="float-right"><a href="{{URL::route('summary',['mt/' . $motor->motorurl_rewrite]) }}">詳細資訊</a></h3>
                                    <h3 class="float-right join-myBike hidden-xs hidden-sm"><a href="{{ route('customer-mybike-add',[$motor->motorurl_rewrite]) }}" target="_blank">加入MyBike</a></h3>
                                </div>
                            </li>
                            <?php $num = $motor->displacement; ?>
                        @endif
                    @endforeach
                    @if(isset($other_motor))
                        <li>
                            <div class="brand-moto-table-content-left col-md-2 col-sm-2 col-xs-2"><span class="size-10rem"></span></div>
                            <div class="brand-moto-table-content-right col-md-10 col-sm-10 col-xs-10">
                                <h3 class="float-left"><a href="{{URL::route('summary',['mt/' . $other_motor->motorurl_rewrite]) }}">{{$other_motor->name}}</a></h3>
                                <h3 class="float-right"><a href="{{URL::route('summary',['mt/' . $other_motor->motorurl_rewrite]) }}">詳細資訊</a></h3>
                                <h3 class="float-right join-myBike hidden-xs hidden-sm"><a href="{{ route('customer-mybike-add',[$other_motor->motorurl_rewrite]) }}" target="_blank">加入MyBike</a></h3>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @stop
