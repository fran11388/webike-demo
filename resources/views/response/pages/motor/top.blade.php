@extends('response.layouts.2columns-capped')
@section('style')
    <link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">
@stop
@section('top')
    @include('response.pages.motor.partials.navbar')
@stop
@section('left')
    @include('response.pages.motor.partials.tab-links')

    @include('response.pages.motor.partials.popular')

    @include('response.pages.motor.partials.displacement')

    @include('response.common.dfp.ad-160x600')
@stop
@section('right')
    <div class="form-group title-motor-video">
        <h1>{{$current_name}} 車型首頁</h1>
    </div>
    @if($catalogue)
        @php
            $floats = $catalogue->floats->pluck('attribute_float_display','attribute_code');
        @endphp
        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h2>{{$current_name}}基本規格</h2>
                </div>
            </div>
            <div class="row">
                <div class="box-fix-column motor-info">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <figure class="zoom-image thumb-img thumb-box-border">
                                <a href="javascript:void(0)">
                                    <img src="{{ $catalogue->model_image_url ? RCJ_IMAGE_URL .$catalogue->model_image_url . 'L_' . $catalogue->model_image_file: NO_IMAGE }}" alt="{{$current_name}}">
                                </a>
                            </figure>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8">
                            <table class="table table-bordered ">
                                <tbody>
                                <tr>
                                    <th class="active" width="80px">排氣量</th>
                                    <td>{{ $catalogue->model_displacement }}cc</td>
                                </tr>
                                <tr>
                                    <th class="active" width="80px">最高馬力</th>
                                    <td>{{ $floats->get(16,'-')}}</td>
                                </tr>
                                <tr>
                                    <th class="active" width="80px">車體重量</th>
                                    <td>{{ $floats->get(24,'-') }}(概算値)kg</td>
                                </tr>
                                <tr>
                                    <th class="active" width="80px">引擎型式</th>
                                    <td>{{ $catalogue->model_engin_data or '-' }}</td>
                                </tr>
                                <tr>
                                    <th class="active" width="80px">最大扭力</th>
                                    <td>{{ $floats->get(17,'-') }}</td>
                                </tr>
                                <tr>
                                    <th class="active" width="80px">油箱容量</th>
                                    <td>{{ $floats->get(20,'-') }}公升</td>
                                </tr>
                                @if($specification)
                                    <tr>
                                        <th class="active" width="80px">簡介</th>
                                        <td>{{ $specification->summary }}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-9"></div>
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <a class="btn btn-danger btn-full" href="{{URL::route('motor-service', $current_motor->url_rewrite)}}" title="{{$current_name}}詳細維修資訊">
                                        詳細維修資訊
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="box-fix-column motor-info">
                    <div class="box-fix-with left">
                        <figure class="zoom-image thumb-img thumb-box-border">
                            <a href="javascript:void(0)">
                                <img src="{{ NO_IMAGE }}" alt="{{$current_name}}">
                            </a>
                        </figure>
                    </div>
                    <div class="box-fix-auto right table-motor-info">
                        <table class="table table-bordered ">
                            <tbody>
                            <tr>
                                <th class="active">排氣量</th>
                                <td>{{$current_motor->displacement}}cc</td>
                            </tr>
                            <tr>
                                <th class="active">最高馬力</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th class="active">車輛重量</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th class="active">引擎型式</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th class="active">最大扭力</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th class="active">油箱容量</th>
                                <td>-公升</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @include('response.pages.motor.partials.news')


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-advertisement">
        @include('response.common.dfp.ad-728x90')
    </div>

    @include('response.pages.motor.partials.motomarket')


    @include('response.pages.motor.partials.quick-search')



    @include('response.pages.summary.partials.new-lineup')

    @include('response.pages.summary.partials.reviews')

    @include('response.pages.summary.partials.hot-brand')

    @include('response.common.ad.banner-small')
@stop
@section('script')
    <script src="{{assetRemote('js/pages/searchList/searchList.js')}}"></script>
    <script>
        $(document).on("click", "#motor_search", function () {
            q = $('#motor_search_text').val();
            window.location.href = "{{URL::route('parts', 'mt/' . $current_motor->url_rewrite)}}" + "?q=" + q;
        });
    </script>
@stop