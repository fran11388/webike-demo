<div class="row">
    <div class="col-md-3 col-sm-4 col-xs-12">
        <a href="javascript:void(0)">
            <figure class="zoom-image border-box thumb-img">
                <img src="{{$current_motor->image ? cdnTransform($current_motor->image_path . 'L_' . $current_motor->image) : cdnTransform(NO_IMAGE) }}" alt="{{$current_motor->name}}">
            </figure>
        </a>
    </div>

    <div class="col-md-9 col-sm-8 col-xs-12">

        <h1>
            {{ isset($current_name) ? $current_name : $summary_title }}
            @if($current_customer)
            　<a class="btn btn-default add-my-bikes-btn {!! (!$addMyBikesDisabled ? 'add-my-bikes-disabled hide' : '') !!}" href="javascript:void(0);" disabled><i class="glyphicon glyphicon-ok-sign"></i> 已加入MyBike清單</a>
            　<a class="btn btn-danger add-my-bikes-btn {!! ($addMyBikesDisabled ? 'add-my-bikes-not-disabled hide' : '') !!}" href="javascript:void(0);" onclick="addMyBike();"><i class="glyphicon glyphicon-plus-sign"></i> 加入MyBike清單</a>
            @endif
        </h1>

        <div class="motor-top row">
            <ul class="ul-motor-top col-xs-12 col-sm-12 col-md-12">
                <li class="col-xs-6 col-sm-3 col-md-3">
                    <div class="title">對應商品</div>
                    <a href="{{ modifySummaryUrl(null,true) }}"><span>{{$info->total ? $info->total . '件' : '--'}}</span></a>
                </li>
                <li class="col-xs-6 col-sm-3 col-md-3">
                    <div class="title">新商品</div>
                    <a href="{{ modifySummaryUrl(null,true) }}?sort=new&fn=1"><span>{{$info->new ? $info->new . '件' : '--'}}</span></a>
                </li>
                <li class="col-xs-6 col-sm-3 col-md-3">
                    <div class="title">維護耗材</div>
                    <a href="{{ modifySummaryUrl(['ca'=>'4000'],true) }}"><span>{{$info->maintenance ? $info->maintenance . '件' : '--'}}</span></a>
                </li>
                <li class="col-xs-6 col-sm-3 col-md-3">
                    <div class="title">商品評論</div>
                    <a href="{{modifyReviewSearchUrlSummary()}}"><span>{{$ranking_avg->counts ? $ranking_avg->counts . '則' : '--'}}</span></a>
                </li>
            </ul>
        </div>
    </div>
</div>
