<div class="col-xs-12 col-sm-12 col-md-12 form-group">
    <div class="row">
        <div class="title-main-box">
            <h2>{{$current_name}}零件用品快速搜尋</h2>
        </div>
        <div class="row">
            <div class="col-lg-10 col-md-9 col-sm-8 col-xs-8"><input id="motor_search_text" type="text" name="q" value="" class="btn btn-default btn-full" placeholder="請輸入商品關鍵字..."></div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4"><a id="motor_search" class="btn btn-danger btn-full" href="javascript:void(0)">搜尋</a></div>
        </div>
    </div>
</div>
