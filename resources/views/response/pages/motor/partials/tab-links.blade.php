<div class="box-page-group row box-page-group-category">
    <div class="title-box-page-group">
        <h3>車輛相關連結</h3>
    </div>
    <ul class="box-content-child menu-relate-motor">
        <li class="box-fix-column item-motor-video">
            <a href="{{URL::route('motor-top', $current_motor->url_rewrite)}}" title="車型首頁{{$tail}}"><i class="fa fa-circle" aria-hidden="true"></i>車型首頁</a>
        </li>
        <li class="box-fix-column item-motor-video">
            <a href="{{URL::route('motor-service', $current_motor->url_rewrite)}}" title="{{$current_motor->name}}規格總覽{{$tail}}"><i class="fa fa-circle" aria-hidden="true"></i>規格總覽</a>
        </li>
        <li class="box-fix-column item-motor-video">
            <a href="{{URL::route('summary', 'mt/' . $current_motor->url_rewrite)}}" title="改裝及用品{{$tail}}"><i class="fa fa-circle" aria-hidden="true"></i>改裝及用品</a>
        </li>
        @if($genuine_link)
            <li class="box-fix-column item-motor-video">
                <a href="{{$genuine_link}}" title="正廠零件{{$tail}}" target="_blank"><i class="fa fa-circle" aria-hidden="true"></i>正廠零件</a>
            </li>
        @endif
        <li class="box-fix-column item-motor-video">
            <a href="{{URL::route('motor-review', $current_motor->url_rewrite)}}" title="商品評論{{$tail}}"><i class="fa fa-circle" aria-hidden="true"></i>商品評論</a>
        </li>
        <li class="box-fix-column item-motor-video">
            <a href="{{URL::route('motor-video',  $current_motor->url_rewrite)}}" title="車友影片{{$tail}}"><i class="fa fa-circle" aria-hidden="true"></i>車友影片</a>
        </li>
        <li class="box-fix-column item-motor-video">
            <a href="http://www.webike.tw/motomarket/search/fulltext?q={{$current_motor->name}}" title="新車、中古車{{$tail}}" target="_blank"><i class="fa fa-circle" aria-hidden="true"></i>新車、中古車</a>
        </li>
    </ul>
</div>