@if(isset($modelInfo))
    <div class="title-star-review">
        <span class="icon-star-bigger {{is_numeric($modelInfo->mybike_self_rating_total) ? 'star-0' . round($modelInfo->mybike_self_rating_total / 20) : ''}}"></span>
        <h1>{{ $modelInfo->mybike_self_rating_total }}</h1>
        <span>車友綜合評價</span>
    </div>
    <table class="rating-star-table">
        <?php
        $names = [
                'mybike_self_rating_fuelcost' => '油耗',
                'mybike_self_rating_driving' => '馬力',
                'mybike_self_rating_design' => '外觀',
                'mybike_self_rating_mente' => '保養',
                'mybike_self_rating_carry' => '乘載',
                'mybike_self_rating_handring' => '操控',
        ];
        ?>
        @foreach($names as $key => $name)
                <tr>
                    <?php

                        $avg = ( (is_numeric($modelInfo->$key) ? $modelInfo->$key : 0) / 5 ) * 100;
                    ?>
                    <td><p class="rating-star-name"></p></td>
                    <td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i></td>
                    <td><p class="rating-star-name"></p>{{ $name }}</td>
                    <td><p class="rating-bar-bg"><span class="rating-bar-with" style="width:{{ $avg }}%;"></span></p>
                    </td>
                    <td><span class="rating-star-number">{{ $modelInfo->$key }}</span></td>
                </tr>
        @endforeach
    </table>
@endif