@if(count($current_motor))
    <?php
    $motor_manufacturer_url_rewrite = $current_motor->manufacturer->url_rewrite;
    $import_manufacturers = array('HONDA','YAMAHA','SUZUKI','KAWASAKI');
    ?>
    @if(in_array($motor_manufacturer_url_rewrite,$import_manufacturers))

        <div class="box-page-group row box-page-group-category">
            <div class="title-box-page-group">
                <h3>請選擇排氣量</h3>
            </div>
            <ul class="box-content-child menu-relate-motor">
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,50]) }}"><span class="blue-0066c0">- 50cc</span></a>
                </li>
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,125]) }}"><span class="blue-0066c0">51 - 125cc</span></a>
                </li>
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,250]) }}"><span class="blue-0066c0">126 - 250cc</span></a>
                </li>
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,400]) }}"><span class="blue-0066c0">251 - 400cc</span></a>
                </li>
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,750]) }}"><span class="blue-0066c0">401 - 750cc</span></a>
                </li>
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,1000]) }}"><span class="blue-0066c0">751 - 1000cc</span></a>
                </li>
                <li class="box-fix-column item-motor-video">
                    <a href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,1001]) }}"><span class="blue-0066c0">1001cc -</span></a>
                </li>
            </ul>
        </div>
    @endif
@endif