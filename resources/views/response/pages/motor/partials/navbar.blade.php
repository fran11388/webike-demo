<div class="box-content-brand-top-page">
    <div class="box-fix-column ct-brand-top-page">
        <div class="box-fix-with box-ct-brand-top-page-right">
            @include('response.pages.motor.partials.analysis')

        </div>
        <div class="box-fix-auto box-ct-brand-top-page-left">
            <div class="content-box-ct-brand-top-page">
                @include('response.pages.motor.partials.info')
            </div>
        </div>
    </div>
    <div id="myNavbar" class="menu-brand-top-page">
        <div class="ct-menu-brand-top-page2">
            <ul class="ul-menu-brand-top-page ul-menu-motor-top-page">
                <li>
                    <a href="{{URL::route('motor-top', $current_motor->url_rewrite)}}" title="車型首頁{{$tail}}">車型首頁</a>
                </li>
                <li>
                    <a href="{{URL::route('motor-service', $current_motor->url_rewrite)}}" title="{{$current_motor->name}}規格總覽{{$tail}}">規格總覽</a>
                </li>
                <li>
                    <a href="{{URL::route('summary', 'mt/' . $current_motor->url_rewrite)}}" title="改裝及用品{{$tail}}">改裝及用品</a>
                </li>
                @if($genuine_link)
                    <li>
                        <a href="{{$genuine_link}}" title="正廠零件{{$tail}}" target="_blank">正廠零件</a>
                    </li>
                @endif
                <li>
                    <a href="{{URL::route('motor-review', $current_motor->url_rewrite)}}" title="商品評論{{$tail}}">商品評論</a>
                </li>
                <li>
                    <a href="{{ route('motor-video', $current_motor->url_rewrite) }}" title="車友影片{{$tail}}">車友影片</a>
                </li>
                <li>
                    <a href="http://www.webike.tw/motomarket/search/fulltext?q={{$current_motor->name}}" title="新車、中古車{{$tail}}" target="_blank">新車、中古車</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<script src="{{assetRemote('js/pages/link-active.js')}}"></script>