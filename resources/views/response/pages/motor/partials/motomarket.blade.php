@if(isset($newmotors) and count($newmotors))
    <div class="box-ct-new-lineup box-content custom-parts">
        <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>
                <span>{{$current_name}}新車・中古車情報　</span>
                <a href="http://www.webike.tw/motomarket/brand/{{$newmotors->first()->manufacurer}}/motor/{{$newmotors->first()->motor_model_id}}" target="_blank">>>查看全部</a>
            </h2>
        </div>
        <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class=" owl-carousel-6">
                @foreach($newmotors as $motor_num => $new_motor)
                    <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item-product-grid">
                        @include('response.common.product.j',['motor' => $new_motor, 'motor_num' => $motor_num])
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif