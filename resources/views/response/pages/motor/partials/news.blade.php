@if(count($news))
    <div class="box-content box-motor-data form-group">
        <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>{{$current_name}}最新消息</h2>
            {{--<a href="{{BIKENEWS . '/tag/' . $current_motor->name }}">查看全部</a>--}}
        </div>
        <ul class="grid-motor-data-news">
            @foreach ($news as $key => $post)
                @php
                    $photo = \Everglory\Models\News\Meta::join('wp_posts', 'meta_value', '=', 'wp_posts.ID')
                    ->where( 'wp_postmeta.post_id', $post->ID )
                    ->where( 'wp_postmeta.meta_key', '_thumbnail_id' )
                    ->first();
                    $photo_path = 'http://www.webike.tw/bikenews/wp-content/themes/opinion_tcd018/img/common/no_image2.jpg';
                    if( $photo and $photo->guid ){
                        $photo_path = $photo->guid;
                    }

                    $description_data = \Everglory\Models\News\Meta::where( 'post_id', $post->ID )
                    ->where( 'meta_key', 'short_description' )
                    ->first();

                    if( $description_data ){
                        $short_description = $description_data->meta_value;
                    }
                @endphp
                <li>
                    <div class="col-xs-4 col-sm-4 col-md-4 left"><a class="zoom-image" href="{{ BIKENEWS.'/'.$post->post_name}} "><img src="{{$photo_path}}" alt="{{$post->post_title}} - 「Webike-摩托新聞」"></a></div>
                    <div class="col-xs-8 col-sm-8 col-md-8 right">
                        <p class="time form-group">{{$post->post_date}}</p>
                        <h2 class="form-group"><a href="{{ BIKENEWS.'/'.$post->post_name}} " class="title">{{$post->post_title}}</a></h2>
                    <span class="form-group">
                        @if(isset($short_description))
                            {!! $short_description !!}
                        @endif
                    </span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endif