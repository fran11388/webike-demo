<div class="box-page-group row box-page-group-category">
    <div class="title-box-page-group">
        <h3>同級距熱門車款</h3>
    </div>
    <ul class="box-content-child item-product-grid grid-item-motor-left">
        @foreach($popular_motors as $popular_motor)
            <li class="item item-product-grid">
                <a href="{{route('motor-top',$popular_motor->url_rewrite)}}">
                    <figure class="zoom-image thumb-box-border thumbnail-img-168">
                        <img src="{{ $popular_motor->image_path ? $popular_motor->image_path . 'L_' . $popular_motor->image : NO_IMAGE }}" alt="{{$popular_motor->name}}">
                        <span class="btn btn-primary btn-rank">Top {{$loop->iteration}}</span>
                    </figure>
                </a>
                <a href="{{route('motor-top',$popular_motor->url_rewrite)}}" class="title-product-item dotted-text2">{{$popular_motor->name}}</a>
            </li>
        @endforeach

    </ul>
</div>
