@if(count($series))
    <div class="box-page-group row box-page-group-category serious-container">
        <div class="title-box-page-group">
            <h3>車輛系列一覽</h3>
        </div>
        <ul class="box-content-child grid-item-motor-video">
            @foreach($series as $year => $series_motors)

                <li class="box-fix-column item-motor-video {{$catalogue->model_release_year == $year ? 'active' : '' }}">
                    @foreach($series_motors as $series_motor)
                        @if($loop->iteration == 1 )
                            <div class="box-fix-with left series-motor">
                                <a href="{{route('motor-service',[$current_motor->url_rewrite,$series_motor->model_catalogue_id])}}">
                                    <figure class="zoom-image thumb-box-border thumbnail-img-65 thumb-img">
                                        <img src="{!! current($current_series_images[$year]) ? current($current_series_images[$year]) : NO_IMAGE !!}" alt="{{$series_motor->model_hyouji}}">
                                    </figure>
                                </a>
                            </div>
                            <div class="box-fix-auto right series-motor">
                                <a href="{{route('motor-service',[$current_motor->url_rewrite,$series_motor->model_catalogue_id])}}">
                                    {{$current_motor->name . $series_motor->model_grade}}<br/>
                                    {{$series_motor->model_release_year}}年
                                </a>
                            </div>
                            <span class="helper"></span>
                            @if(!$loop->last)
                                <ul class="second-menu-service">
                                    <li>
                                        其他規格・類型
                                    </li>
                            @endif
                        @else
                                <li>
                                    <a href="{{route('motor-service',[$current_motor->url_rewrite,$series_motor->model_catalogue_id])}}"><i class="fa fa-square" aria-hidden="true"></i> {{ $series_motor->model_grade ? $series_motor->model_grade : $current_motor->name }} </a>
                                </li>
                            @if($loop->last)
                                </ul>
                            @endif
                        @endif
                    @endforeach
                </li>
            @endforeach
        </ul>
    </div>
@endif