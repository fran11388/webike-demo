@extends('response.layouts.2columns')
@section('style')
    <style type="text/css">
        .thumb-img-list-brand-logo{
            background: #fff;
        }
    </style>
@stop
@section('left')
    <div class="box-page-group"><img src="{{assetRemote('image/brands/img-step-brand-index.jpg')}}" alt=" images step"></div>
    <div class="box-page-group">
        <ul class="ul-ct-brand-index">
            <?php
            $factory = array('HONDA','YAMAHA','SUZUKI','KAWASAKI');
            ?>
            @foreach($manufacturers as $info)
                <?php
                if(in_array($info->name,$factory)){
                    $displacement = '50';
                }else{
                    $displacement = '00';
                }
                ?>
                <li class="box-fix-column">
                    <div class="item-product-grid item-img-brand-index box-fix-with"><a href="{{URL::route('motor-manufacturer',[$info->name, $displacement]) }}">
                            <figure class="thumb-box-border zoom-image"><img src=" {{$info->image}}" alt=""></figure>
                        </a></div>
                    <div class="info-brand-right box-fix-auto">
                        <a href="{{URL::route('motor-manufacturer',[$info->name, $displacement]) }}">{{$info['name']}}</a>
                        <span>車款數 : {{$info->count}}</span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@stop
@section('right')
    <div class="title-main-box">
        <h1>車型索引</h1>
    </div>
    <div class="ct-brand-selection">
        <h2 class="title-brand-selection">廠牌選擇</h2>
        <!-- ul.ul-ct-brand-selection>li*4>(div.item-product-grid>a>figure>img)+(ul.list-info-brand-selection>li*7>a) -->
        <ul class="ul-ct-brand-selection">
            <?php $num = 1;?>
            @foreach($manufacturers as $info)
                @if($num <= 4)
                    <li class="col-xs-12 col-sm-3 col-md-3">
                        <div class="item-product-grid"><a href="{{URL::route('motor-manufacturer',[$info->name,50]) }}">
                            <figure class="thumb-box-logo-brand"><img src="{{$info->image}}" alt=""></figure>
                        </a></div>
                        <ul class="list-info-brand-selection">
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,50]) }}">- 50cc</a></li>
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,125]) }}">51 -125cc</a></li>
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,250]) }}">126 - 250cc</a></li>
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,400]) }}">251 - 400cc</a></li>
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,750]) }}">401 - 750cc</a></li>
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,1000]) }}">751 -1000cc</a></li>
                            <li><a href="{{URL::route('motor-manufacturer',[$info->name,1001]) }}">1001cc -</a></li>
                        </ul>
                    </li>
                @else
                    @break
                @endif
                <?php $num++;?>
            @endforeach
        </ul>
    </div>
    <div class="ct-list-brand-logo">
        <ul class="ul-list-brand-logo">
            <?php $num = 1;?>
            @foreach($manufacturers as $info)
                @if($num > 4)
                    <li>
                        <div class="item-product-grid"><a href="{{URL::route('motor-manufacturer',[$info->name,'00']) }}">
                            <figure class="zoom-image thumb-img-list-brand-logo"><img src="{{$info->image}}" alt="{{$info->name}}"></figure>
                        </a></div>
                    </li>
                @endif
                <?php $num++;?>
            @endforeach
        </ul>
    </div>
    <div id="myNavbar" class="box-part-number col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="title-main-box">
            <h2>品牌國籍</h2>
        </div>
        <?php $num = 1;?>
        <ul  class="menu-ct-list-brand-index">
            @foreach($motors as $country => $information)
                <?php
                if($num < 10){
                    $idnum = '0'.$num;    
                }else{
                    $idnum ="$num";
                }
                ?>
                <li><a href="#menu-brand-{{$idnum}}"> {{$country}}</a></li>
                 <?php $num++; ?>
            @endforeach  
        </ul>
        <?php $num = 1;?>
        <div class="ct-brand-nationality">
            @foreach($motors as $country => $information)
                <?php
                    if($num < 10){
                        $idnum = '0'.$num;    
                    }else{
                        $idnum ="$num";
                    }
                ?>
                <div id="menu-brand-{{$idnum}}" class="item-brand-nationality">
                    <div class="title-item-brand-nationality">
                        <img src="{{assetRemote('/image/oempart/icon-flag'.$idnum.'.jpg')}}" alt="">
                        <h2>{{$country}}</h2>
                    </div>

                    <ul class="ct-item-brand-nationality">
                        @foreach($information as $manufacturer)
                            <?php $url_rewrite = $manufacturer->url_rewrite; ?>
                            <li class="col-xs-12 col-sm-6 col-md-3"><a href="{{URL::route('motor-manufacturer',[$url_rewrite,'00']) }}">{{$manufacturer->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <?php $num++; ?>
            @endforeach  
        </div>
    </div>
@stop
    <!--End Content -->
<!-- jQuery -->

