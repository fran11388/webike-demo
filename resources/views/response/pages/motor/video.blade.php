@extends('response.layouts.2columns-capped')
@section('style')

@stop
@section('top')
    @include('response.pages.motor.partials.navbar')
@stop
@section('left')
    @include('response.common.list.side-product-ranking')
@stop
@section('right')
    @include('response.pages.summary.partials.video')
@stop
@section('script')
    <script src="{{assetRemote('js/pages/searchList/searchList.js')}}"></script>

@stop