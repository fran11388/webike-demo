@extends('response.layouts.2columns-capped')
@section('style')
<link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">
    <style type="text/css">
        .ct-item-product-grid-category-customer-review li{
            width:25%;
        }
    </style>
@stop
@section('top')
    @include('response.pages.motor.partials.navbar')
@stop
@section('left')
    @include('response.pages.motor.partials.tab-links')

    @include('response.pages.motor.partials.displacement')
@stop
@section('right')
    @if (count($reviews))
        @include('response.pages.summary.partials.reviews')
    @else
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">此車型暫無商品評論</h1>
        </div>
    @endif
@stop
@section('script')
    <script src="{{assetRemote('js/pages/searchList/searchList.js')}}"></script>
            <script>
        @if($current_motor)
        function addMyBike()
        {
            $('.add-my-bikes-btn').addClass('hide');
            $('.add-my-bikes-disabled').removeClass('hide');
            $.ajax({
                type: 'GET',
                url: "{!! route('customer-mybike-add', $current_motor->url_rewrite) !!}",
                data: {},
                dataType: 'json',
                success: function(result){
                    if(result.success){
                        var ajax_html = '<span class="size-10rem">{!! $current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') !!} 已經加入MyBike清單。若要移除或新增車型，請至<a href="{!! route('customer-mybike') !!}" target="_blank">會員中心修正</a>。</span><br><br>';
                        if(result.messages.length){
                            ajax_html += result.messages.join('<br>');
                        }
                        swal({
                            title: "成功加入Mybike",
                            html:  '<div class="text-left">' + ajax_html + '</div>',
                            type: "success",
                            confirmButtonText: "確定"
                        });
                    }

                }
            });
        }
        @endif
    </script>

@stop