@extends('response.layouts.2columns-capped')
@section('style')

<link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">

<style>
    ul.box-content-child > li.active {
        border: #555 1px dotted;
        background: #B4DEF5;
    }
    .item-motor-video .series-motor {
        display:inline-block;
        vertical-align: middle;
        float:none !important;
    }
    .serious-container .grid-item-motor-video .item-motor-video {
        padding-top:4px;
    }
    @media (min-width : 768px) {
        .tab-ct-service-page .table tr th {
            width: 294px;
        }
    }
</style>
@stop
@section('top')
    @include('response.pages.motor.partials.navbar')
@stop
@section('left')
        <div class="hidden-lg hidden-md hidden-sm">
            @include('response.pages.motor.partials.tab-links')
        </div>

        @include('response.pages.motor.partials.service.series')

        <div class="hidden-xs">
            @include('response.pages.motor.partials.tab-links')
        </div>

        @include('response.pages.motor.partials.displacement')
        {{--<div class="box-page-group row box-page-group-category">--}}
            {{--<div class="title-box-page-group">--}}
                {{--<h3>商品銷售排行</h3>--}}
            {{--</div>--}}
            {{--<ul class="box-content-child menu-relate-motor">--}}
                {{--<li class="box-fix-column item-motor-video">--}}
                    {{--<a href="javascript:void(0)"><i class="fa fa-circle" aria-hidden="true"></i>燃料消費率</a>--}}
                {{--</li>--}}
                {{--<li class="box-fix-column item-motor-video">--}}
                    {{--<a href="javascript:void(0)"><i class="fa fa-circle" aria-hidden="true"></i>航続可能距離</a>--}}
                {{--</li>--}}
                {{--<li class="box-fix-column item-motor-video">--}}
                    {{--<a href="javascript:void(0)"><i class="fa fa-circle" aria-hidden="true"></i>燃料タンク容量</a>--}}
                {{--</li>--}}
                {{--<li class="box-fix-column item-motor-video">--}}
                    {{--<a href="javascript:void(0)"><i class="fa fa-circle" aria-hidden="true"></i>燃料消費率</a>--}}
                {{--</li>--}}
                {{--<li class="box-fix-column item-motor-video">--}}
                    {{--<a href="javascript:void(0)"><i class="fa fa-circle" aria-hidden="true"></i>航続可能距離</a>--}}
                {{--</li>--}}
                {{--<li class="box-fix-column item-motor-video">--}}
                    {{--<a href="javascript:void(0)"><i class="fa fa-circle" aria-hidden="true"></i>燃料タンク容量</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        @include('response.common.dfp.ad-160x600')
@stop
@section('right')
@if($catalogue)
        <div class="form-group title-motor-video">
            <h1>{{$current_name}}規格總覽</h1>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h2>{{$current_name}}車輛圖鑑</h2>
                </div>
            </div>
            <div class="row thumb-box-border">
                @if(count($current_images))
                    <div class="img-slider-for2">
                        @foreach($current_images as $current_image)
                            <a class="img-thumbnail thumbnail-img-product2" href="javascript:void(0)"><img src="{!! $current_image !!}" alt="{{$current_name}}"/></a>
                        @endforeach
                    </div>
                    <div class="ct-img-slider-nav2">
                        <div class="img-slider-nav2">
                            @foreach($current_images as $current_image)
                                <a class="img-thumbnail img-thumbnail-detail" href="javascript:void(0)"><img src="{!! $current_image !!}" alt="{{$current_name}}"/></a>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="img-slider-for2">
                        <img src="{{ NO_IMAGE }}" alt="{{$current_name}}"/>
                    </div>
                @endif
            </div>
        </div>
<?php
$groups = $catalogue->groups->pluck('attribute_group_display','attribute_code');
$strings = $catalogue->strings->pluck('attribute_string_values','attribute_code');
$flags = $catalogue->flags->pluck('attribute_flag_values','attribute_code');
$floats = $catalogue->floats->pluck('attribute_float_display','attribute_code');
$integers = $catalogue->integers->pluck('attribute_integer_display','attribute_code');
?>
        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h2>{{$current_name}}基本規格</h2>
                </div>
                <table class="table table-bordered ">
                    <tbody>
                    <tr>
                        <th class="active">廠牌</th>
                        <td>{{ $current_motor->manufacturer->name }}</td>
                        <th class="active">引擎形式</th>
                        <td>{{ $catalogue->model_engin_data }}</td>
                    </tr>
                    <tr>
                        <th class="active">車型名稱</th>
                        <td>{{$current_motor->name}}</td>
                        <th class="active">引擎啟動方式</th>
                        <td>{{ $groups->get(13, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">款式・種類</th>
                        <td>{{$catalogue->model_grade}}</td>
                        <th class="active">最高馬力</th>
                        <td>{{ $floats->get(16, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">動力方式</th>
                        <td>-</td>
                        <th class="active">最大扭力</th>
                        <td>{{ $floats->get(17, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">型式</th>
                        <td>{{ $catalogue->model_katashiki }}</td>
                        <th class="active">車體重量(乾燥重量)</th>
                        <td>{{ $floats->get(24, ' - ') }}(概算値)kg</td>
                    </tr>
                    <tr>
                        <th class="active">排氣量</th>
                        <td>{{ $catalogue->model_displacement }}cc</td>
                        <th class="active">車體重量(裝備重量)</th>
                        <td>{{ $floats->get(25, ' - ') }}kg</td>
                    </tr>
                    <tr>
                        <th class="active">開始銷售年分</th>
                        <td>{{ $catalogue->model_release_year }}年</td>
                        <th class="active">馬力重量比</th>
                        <td>[{{ $floats->get(24, ' - ') }} / {{ $floats->get(16, ' - ') }}] kg/PS</td>
                    </tr>
                    <tr>
                        <th class="active">能源消耗值</th>
                        <td>{{ $floats->get(14, ' - ') }}</td>
                        <th class="active">全長・全高・全寬</th>
                        <td>{{ $integers->get(28, ' - ') }}mm
                            × {{ $integers->get(26, ' - ') }}mm
                            × {{ $integers->get(27, ' - ') }}mm</td>
                    </tr>
                    <tr>
                        <th class="active">油箱容量</th>
                        <td>{{ $floats->get(20, ' - ') }}公升</td>
                        <th class="active">座高</th>
                        <td>{{ $integers->get(22, ' - ') }}mm</td>
                    </tr>
                    <tr>
                        <th class="active">行駛距離</th>
                        <td>[{{ $floats->get(14, ' - ') }} * {{ $floats->get(20, ' - ') }}]km(概算値)</td>
                        <th class="active">前輪尺寸</th>
                        <td>
                            {{ $strings->get(33, ' - ') }}
                        </td>
                    </tr>
                    <tr>
                        <th class="active">燃料給油方式</th>
                        <td>{{ $groups->get(9) ,' - '}}</td>
                        <th class="active">後輪尺寸</th>
                        <td>
                            {{ $strings->get(42, ' - ') }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h2>{{$current_name}}維修資訊</h2>
                </div>
                <table class="table table-bordered ">
                    <tbody>
                    <tr>
                        <th class="active">標準裝備火星塞</th>
                        <td>{{ $strings->get(69, ' - ') }}</td>
                        <th class="active">齒盤規格</th>
                        <td>傳動(前) > {{ $integers->get(62, ' - ') }}T<br/>傳動(後) > {{ $integers->get(63, ' - ') }}T</td>
                    </tr>
                    <tr>
                        <th class="active">火星塞使用數</th>
                        <td>{{ $integers->get(70, ' - ') }}個</td>
                        <th class="active">鍊條規格</th>
                        <td>{{ $groups->get(64, ' - ') }} / {{ $groups->get(65, ' - ') }}鍊目數</td>
                    </tr>
                    <tr>
                        <th class="active">火星塞間隙</th>
                        <td>{{ $strings->get(71, ' - ') }}</td>
                        <th class="active">電瓶型號</th>
                        <td>{{ $strings->get(83, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">引擎機油總量</th>
                        <td>{{ $floats->get(10, ' - ') }}公升</td>
                        <th class="active">大燈</th>
                        <td>{{ $strings->get(84, ' - ') }}<br/>燈型:{{ $strings->get(85, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">機油交換時</th>
                        <td>{{ $floats->get(11, ' - ') }}</td>
                        <th class="active">大燈備註</th>
                        <td>{{ $strings->get(84, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">機油濾心交換時</th>
                        <td>{{ $floats->get(12, ' - ') }}</td>
                        <th class="active">尾燈</th>
                        <td>{{ $strings->get(87, ' - ') }} / {{ $strings->get(88, ' - ') }}</td>
                    </tr>
                    <tr>
                        <th class="active">前方向燈規格</th>
                        <td>{{ $strings->get(92, ' - ') }}</td>
                        <th class="active">後方向燈</th>
                        <td>{{ $strings->get(95, ' - ') }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h2>{{$current_name}}其他詳細維修資訊</h2>
                </div>
                <ul class="nav nav-tabs tap-motor-service">
                    <li class="active"><a data-toggle="tab" href="#home">車體</a></li>
                    <li><a data-toggle="tab" href="#menu1">懸吊</a></li>
                    <li><a data-toggle="tab" href="#menu2">配備</a></li>
                    <li><a data-toggle="tab" href="#menu3">引擎</a></li>
                    <li><a data-toggle="tab" href="#menu4">其他</a></li>
                </ul>
                <div class="tab-content tab-ct-service-page">
                    <div id="home" class="tab-pane fade in active">
                        <table class="table table-bordered ">
                            <tbody>
                            <tr>
                                <th class="active">引擎形式</th>
                                <td>{{ $strings->get(51, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">缸徑</th>
                                <td>{{ $floats->get(98, ' - ') }}mm</td>
                            </tr>
                            <tr>
                                <th class="active">行程</th>
                                <td>{{ $floats->get(99, ' - ') }}mm</td>
                            </tr>
                            <tr>
                                <th class="active">壓縮比</th>
                                <td>{{ $floats->get(15, ' - ') }}:1</td>
                            </tr>
                            <tr>
                                <th class="active">點火方式</th>
                                <td>{{ $groups->get(68, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">引擎潤滑方式</th>
                                <td>{{ $strings->get(115,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">車架樣式</th>
                                <td>{{ $groups->get(100,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">最小迴轉半徑</th>
                                <td>2.{{ $floats->get(30, ' - ') }}m</td>
                            </tr>
                            <tr>
                                <th class="active">傾角</th>
                                <td>{{ $floats->get(113,' - ') }}m</td>
                            </tr>
                            <tr>
                                <th class="active">前叉角度</th>
                                <td>{{ $floats->get(114,' - ') }}mm</td>
                            </tr>
                            <tr>
                                <th class="active">最低高度</th>
                                <td>{{ $integers->get(29, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">軸距</th>
                                <td>{{ $floats->get(116,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">轉向角度（右）</th>
                                <td>2.{{ $floats->get(117,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">轉向角度（左）</th>
                                <td>{{ $floats->get(118,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">乘員</th>
                                <td>{{ $integers->get(119,' - ') }}人</td>
                            </tr>
                            <tr>
                                <th class="active">油箱容量</th>
                                <td>{{ $floats->get(21, ' - ') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- message_histories -->
                    <div id="menu1" class="tab-ct-customer-center-page-detail tab-ct-customer-center-question tab-pane fade">
                        <table class="table table-bordered ">
                            <tbody>
                            <tr>
                                <th class="active">前煞車系統</th>
                                <td>{{ $groups->get(72, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前煞車備註</th>
                                <td>{{ $strings->get(73, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後煞車系統</th>
                                <td>{{ $groups->get(74, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後煞車備註</th>
                                <td>{{ $strings->get(75, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前懸吊系統</th>
                                <td>{{ $strings->get(101,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前懸吊行程</th>
                                <td>{{ $integers->get(104,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前叉類型</th>
                                <td>{{ $groups->get(103,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前叉直徑</th>
                                <td>{{ $integers->get(105,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後懸吊系統</th>
                                <td>{{ $strings->get(102,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後避震器個數</th>
                                <td>{{ $integers->get(110,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後懸吊行程</th>
                                <td>{{ $integers->get(111,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後輪行程</th>
                                <td>{{ $integers->get(112,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">ABS</th>
                                <td>{{ $flags->get(31, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">傳動方式</th>
                                <td>{{ $groups->get(32, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">機油交換量（公升）</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th class="active">鍊條備註</th>
                                <td>{{ $strings->get(66, ' - ') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--End message_histories -->
                    <!-- create_question -->
                    <div id="menu2" class="tap-ct-create-question tab-pane fade">
                        <table class="table table-bordered ">
                            <tbody>
                            <tr>
                                <th class="active">坐墊下容量</th>
                                <td>{{ $floats->get(23, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">安全帽收納空間</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th class="active">後視鏡安裝螺絲孔直徑</th>
                                <td>{{ $integers->get(120,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後視鏡備註</th>
                                <td>{{ $strings->get(121,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">電子設備</th>
                                <td>{{ $strings->get(122,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">油表</th>
                                <td>{{ $flags->get(123,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">碼表</th>
                                <td>{{ $flags->get(124,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">雙黃警示燈</th>
                                <td>{{ $flags->get(125,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">防盜系統</th>
                                <td>{{ $flags->get(126,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">尾燈燈座形狀</th>
                                <td>{{ $strings->get(89, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">尾燈燈座形狀2</th>
                                <td>{{ $strings->get(90, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">尾燈備註</th>
                                <td>{{ $strings->get(91, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前方向燈燈泡形式</th>
                                <td>{{ $strings->get(93, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前方向燈燈座形狀</th>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End create_question -->
                    <div id="menu3" class="tab-ct-customer-center-page-detail tab-pane fade ">
                        <table class="table table-bordered ">
                            <tbody>
                            <tr>
                                <th class="active">離合器形式</th>
                                <td>{{ $groups->get(82, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">變速系統</th>
                                <td>{{ $groups->get(81, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">齒輪數</th>
                                <td>{{ $integers->get(76, ' - ') }}速</td>
                            </tr>
                            <tr>
                                <th class="active">齒輪比</th>
                                <td>{{ $strings->get(77, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">一次減速比</th>
                                <td>{{ $strings->get(78, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">二次減速比</th>
                                <td>{{ $strings->get(79, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">無段變速車變速比</th>
                                <td>{{ $strings->get(80, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">進氣頂桿間隙</th>
                                <td> - </td>
                            </tr>
                            <tr>
                                <th class="active">排氣頂桿間隙</th>
                                <td> - </td>
                            </tr>
                            <tr>
                                <th class="active">怠數轉速</th>
                                <td>{{ $strings->get(56, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">油針段數(固定位置)</th>
                                <td>{{ $strings->get(57, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">浮球液位</th>
                                <td>{{ $integers->get(58, ' - ') }}mm</td>
                            </tr>
                            <tr>
                                <th class="active">怠速螺絲</th>
                                <td>{{ $strings->get(59, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">噴油嘴形式</th>
                                <td>{{ $strings->get(60, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">噴油嘴編號</th>
                                <td>{{ $integers->get(61, ' - ') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End create_question -->
                    <div id="menu4" class="tab-ct-customer-center-page-detail tab-pane fade ">
                        <table class="table table-bordered ">
                            <tbody>
                            <tr>
                                <th class="active">離合器鋼索/油管長度</th>
                                <td>{{ $strings->get(129,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">阻風門鋼索長度</th>
                                <td>{{ $strings->get(130,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">碼錶鋼索長度</th>
                                <td>{{ $strings->get(131,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">油門線長度</th>
                                <td>{{ $strings->get(132,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">前制動鋼索/油管長度</th>
                                <td>{{ $strings->get(133,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">後制動鋼索/油管長度</th>
                                <td>{{ $strings->get(134,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">其他鋼索/油管</th>
                                <td>{{ $strings->get(135,' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">車架 號碼開始</th>
                                <td> - </td>
                            </tr>
                            <tr>
                                <th class="active">車架 號碼結束</th>
                                <td> - </td>
                            </tr>
                            <tr>
                                <th class="active">引擎編號（生產開始）</th>
                                <td>{{ $strings->get(52, ' - ') }}</td>
                            </tr>
                            <tr>
                                <th class="active">引擎編號（生產結束）</th>
                                <td>{{ $strings->get(53, ' - ') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        @include('response.pages.motor.partials.quick-search')

        @include('response.pages.summary.partials.subcategory')

        @include('response.common.ad.banner-small')

        @include('response.pages.motor.partials.motomarket')

    @else
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">暫無維修資訊</h1>
        </div>
    @endif
@stop
@section('script')
    <script src="{{assetRemote('js/pages/searchList/searchList.js')}}"></script>
    <script>
        $(document).on("click", "#motor_search", function () {
            q = $('#motor_search_text').val();
            window.location.href = "{{URL::route('parts', 'mt/' . $current_motor->url_rewrite)}}" + "?q=" + q;
        });
    </script>
@stop