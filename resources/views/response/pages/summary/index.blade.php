@extends('response.layouts.1column')
@section('style')
    <link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">
    
    <style>
         @media(max-width:768px){
             .select2-container {
                 width:100% !important;
                 margin-top:10px;
             }
         }
        .motor-search-container {
            padding: 0px 10px;
        }

         .motor-search-container li:first-child{
            margin-top:0px !important;
         }

         .motor-search-container select {
             padding:10px;
         }
    </style>
@endsection
@section('middle')
    @include('response.pages.summary.partials.top-info')
    <div class="box-content-brand-top-page">
        <div id="m-menu" class="hidden-xs">
            <div>

                <?php \Debugbar::startMeasure('render','content-tabs'); ?>

                <div class="hidden-lg hidden-md hidden-sm">
                    @include('response.pages.summary.partials.content-tabs')
                </div>
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','brand-list'); ?>
                <aside class="ct-left-search-list col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    <ul class="ul-left-title row ct-left-search-list-main-menu">
                        {{--@include('response.common.list.menu-mybike')--}}
                            
                        <!--Category-->
                        @if($is_not_phone)
                            {!! $category_list_view !!}
                        @endif
                        {{--@include('response.pages.summary.partials.category-list')--}}
                        <!--/.categorylist -->
                        @include('response.pages.summary.partials.brand-list')

                        {{--@include('response.pages.summary.partials.recommend')--}}

                        {{--@include('response.pages.summary.partials.collection')--}}
                        <?php \Debugbar::stopMeasure('render'); ?>
                        <?php \Debugbar::startMeasure('render','side-product-ranking'); ?>
                        @if(count($current_motor))
                            <?php
                            $motor_manufacturer_url_rewrite = $current_motor->manufacturer->url_rewrite;
                            $import_manufacturers = array('HONDA','YAMAHA','SUZUKI','KAWASAKI');
                            ?>
                            @if(in_array($motor_manufacturer_url_rewrite,$import_manufacturers))

                                <li class="li-left-title col-md-12 col-sm-12 col-xs-12">
                                    <div class="slide-down-btn">
                                        <a href="javascript:void(0)" class="a-left-title">
                                            <span>請選擇排氣量</span>
                                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <ul class="ul-sub1 row">
                                        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                                            <ul class="ul-sub2 row">
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,50]) }}"><span class="blue-0066c0">- 50cc</span></a>
                                                </li>
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,125]) }}"><span class="blue-0066c0">51 - 125cc</span></a>
                                                </li>
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,250]) }}"><span class="blue-0066c0">126 - 250cc</span></a>
                                                </li>
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,400]) }}"><span class="blue-0066c0">251 - 400cc</span></a>
                                                </li>
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,750]) }}"><span class="blue-0066c0">401 - 750cc</span></a>
                                                </li>
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,1000]) }}"><span class="blue-0066c0">751 - 1000cc</span></a>
                                                </li>
                                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 ">
                                                    <a class="a-sub2 " href="{{URL::route('motor-manufacturer',[$motor_manufacturer_url_rewrite,1001]) }}"><span class="blue-0066c0">1001cc -</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        @endif

                        <?php \Debugbar::stopMeasure('render'); ?>
                        <?php \Debugbar::startMeasure('render','ATS'); ?>
                        @if(isset($current_category))
                                @if(isset($advertiser))
                                    @if(false !== strpos($current_category->mptt->url_path , '1000') )
                                        <li class="li-left-title col-md-12 visible-lg visible-md visible-sm">
                                            <div class="slide-down-btn">
                                                <a href="javascript:void(0)" class="a-left-title">
                                                    <span>推薦連結</span>
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <ul class="ul-sub1 row ">
                                                @for($i = 1 ; $i <= 5 ; $i++)
                                                    <li class="li-sub1 li-sub1-style2 col-md-12" style="padding-bottom: 10px;">
                                                        <div class="ad_block">
                                                            {!! $advertiser->call(23) !!}
                                                        </div>
                                                    </li>
                                                @endfor
                                            </ul>
                                        </li>
                                    @elseif(false !== strpos($current_category->mptt->url_path , '3000') )
                                        <li class="li-left-title col-md-12 visible-lg visible-md visible-sm">
                                            <div class="slide-down-btn">
                                                <a href="javascript:void(0)" class="a-left-title">
                                                    <span>推薦連結</span>
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <ul class="ul-sub1 row ">
                                                @for($i = 1 ; $i <= 5 ; $i++)
                                                    <li class="li-sub1 li-sub1-style2 col-md-12" style="padding-bottom: 10px;">
                                                        <div class="ad_block">
                                                            {!! $advertiser->call(24) !!}
                                                        </div>
                                                    </li>
                                                @endfor
                                            </ul>
                                        </li>
                                    @endif
                                @endif
                        @endif
                    </ul>
                </aside>
            </div>
        </div>
        <?php \Debugbar::stopMeasure('render'); ?>
        <?php \Debugbar::startMeasure('render','manufacturer-info'); ?>
        <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
            @if($current_motor and $current_manufacturer)
                <div class="box-ct-new-lineup box-content custom-parts box-fix-column">
                    @include('response.pages.summary.partials.manufacturer-info')
                </div>
            @endif
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','quick-search'); ?>
            @if($current_manufacturer)
                @if(($current_manufacturer->url_rewrite == 26) or ($current_manufacturer->url_rewrite == 411))
                    @include('response.pages.summary.partials.brand-movie')
                @endif
            @endif
            <div class="box-content custom-parts">
                <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2>
                        <span>{{ $summary_title }} 商品快速搜尋(請輸入關鍵字)　</span>
                    </h2>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                    @php
                        $part_segments = ['mt' => null, 'ca' => null, 'br' => null];
                        if($current_motor){
                            $part_segments['mt'] = $current_motor->url_rewrite;
                        }
                        if($current_category){
                            $part_segments['ca'] = $current_category->mptt->url_path;
                        }
                        if($current_manufacturer){
                            $part_segments['br'] = $current_manufacturer->url_rewrite;
                        }
                    @endphp
                    <form action="{!! getCurrentExploreUrl($part_segments, null, ['parts' => true]) !!}">
                        <div class="box-fix-column box-search-motor">
                            <div class="box-fix-auto">
                                <div class="box-fix-with pull-right">
                                    <button type="submit" class="btn btn-danger">搜尋</button>
                                </div>
                                <div class="box-fix-auto">
                                    <input type="search" name="q" required class="btn btn-default col-md-12" placeholder="輸入商品關鍵字...">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','subcategory_view'); ?>
            {{--@include('response.pages.summary.partials.subcategory')--}}
            {!! $subcategory_view !!}
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','ATS-2'); ?>
            @if(isset($current_category) and !$current_manufacturer)
                @if(isset($advertiser))
                        @if(false !== strpos($current_category->mptt->url_path , '1000') )
                            <div class="box-content custom-parts">
                                <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <h2>
                                        <span>{{ $summary_title }} 本月主打　</span>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                                    {!! $advertiser->call(26) !!}
                                </div>
                            </div>
                        @elseif(false !== strpos($current_category->mptt->url_path , '3000') )
                            <div class="box-content custom-parts">
                                <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <h2>
                                        <span>{{ $summary_title }} 本月主打　</span>
                                    </h2>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                                    {!! $advertiser->call(27) !!}
                                </div>
                            </div>
                        @endif
                @endif
            @endif
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','popular-lineup'); ?>
                @include('response.pages.summary.partials.popular-product')
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','new-lineup'); ?>
            {{--@include('response.pages.summary.partials.new-lineup')--}}
                @include('response.pages.summary.partials.new-lineup-ajax')
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','reviews'); ?>
                @include('response.pages.summary.partials.reviews')
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','custom-parts'); ?>
                @if($is_not_phone)
                    @include('response.pages.summary.partials.custom-parts')
                @endif
                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','custom-parts-recommend'); ?>
            {{--@include('response.pages.summary.partials.custom-parts')--}}
            <div class="lazy-render custom-parts"></div>

            <div class="lazy-render motor-btn"></div>
            {{-- @include('response.pages.summary.partials.custom-parts-recommend') --}}
            <?php \Debugbar::stopMeasure('render'); ?>
            <?php \Debugbar::startMeasure('render','on-sales'); ?>
            @include('response.pages.summary.partials.on-sales')
            <?php \Debugbar::stopMeasure('render'); ?>
            <?php \Debugbar::startMeasure('render','ranking'); ?>
            @include('response.pages.summary.partials.ranking')
            <?php \Debugbar::stopMeasure('render'); ?>
            <?php \Debugbar::startMeasure('render','hot-brand'); ?>
            @include('response.pages.summary.partials.hot-brand')
            <?php \Debugbar::stopMeasure('render'); ?>
            <?php \Debugbar::startMeasure('render','banner-small'); ?>
            @include('response.common.ad.banner-small')
            <?php \Debugbar::stopMeasure('render'); ?>
            <?php \Debugbar::startMeasure('render','md'); ?>
            <!-- Customer view -->
                <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @include('response.common.loading.md')
                </div>
            <!-- Customer view end-->

                <?php \Debugbar::stopMeasure('render'); ?>
                <?php \Debugbar::startMeasure('render','ad'); ?>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-advertisement">
                @include('response.common.dfp.ad-728x90')
            </div>

                <?php \Debugbar::stopMeasure('render'); ?>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ assetRemote('js/pages/owl.carousel-ajax.js') }}"></script>
    <script defer src="{{assetRemote('js/pages/link-active.js')}}"></script>
    <script defer src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script type="text/javascript">
        $('.motor-model-show').click(function(){
            if($(this).prop('class').indexOf('active') >= 0){
                $(this).closest('.motor-group').find('.motor-model:nth-child(n+11)').addClass('hide');
                console.log($(this).closest('.motor-group').find('.motor-model:nth-child(n+11)'));
                $(this).removeClass('active').text('>>顯示全部');
            }else{
                $(this).closest('.motor-group').find('.motor-model:nth-child(n+11)').removeClass('hide');
                $(this).addClass('active').text('<<隱藏部分');
            }
        });
        $('.motor-group-show').click(function(){
            if($(this).prop('class').indexOf('active') >= 0){
                $(this).closest('.fit-motor-groups').find('.motor-group:nth-child(n+5)').addClass('hide');
                $(this).prop('href', "{!! getPartsUrl(['motor' => $current_motor, 'category' => $current_category, 'manufacturer' => $current_manufacturer]) !!}").prop('target', '_blank').text('其他車型商品搜尋');
            }else{
                $(this).closest('.fit-motor-groups').find('.motor-group:nth-child(n+5)').removeClass('hide');
                $(this).addClass('active').text('查看所有對應車型');

            }
        });
        $("a.a-sub2 > span").click(function(){
            window.location =  $(this).parent().attr('data-href');
            return 0;
        });

        $.ajax({
            type: 'POST',
            url: window.location.href + "/lazy",
            data: {type:'mybike'},
            success: function(result){
                result = JSON.parse(result);
                html = result.html;
                for(var key in html){
                    $('.lazy-render.'+key).replaceWith(html[key]);
                }
            }
        });

        @if($current_motor)
            function addMyBike()
            {
                $('.add-my-bikes-btn').addClass('hide');
                $('.add-my-bikes-disabled').removeClass('hide');
                $.ajax({
                    type: 'GET',
                    url: "{!! route('customer-mybike-add', $current_motor->url_rewrite) !!}",
                    data: {},
                    dataType: 'json',
                    success: function(result){
                        if(result.success){
                            var ajax_html = '<span class="size-10rem">{!! $current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') !!} 已經加入MyBike清單。若要移除或新增車型，請至<a href="{!! route('customer-mybike') !!}" target="_blank">會員中心修正</a>。</span><br><br>';
                            if(result.messages.length){
                                ajax_html += result.messages.join('<br>');
                            }
                            swal({
                                title: "成功加入Mybike",
                                html:  '<div class="text-left">' + ajax_html + '</div>',
                                type: "success",
                                confirmButtonText: "確定"
                            });
                        }

                    }
                });
            }
        @endif


    </script>
    <script src="{!! assetRemote('plugin/slick/slick.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>
    <script>
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }

        var baseUrl= '{{ request()->url() }}';
        $(document).on('change', '.select-option-redirect', function(){
            window.location = $(this).val();
        });

        function setListMode( value ){
            var d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }

        $("a.a-sub2 > span").click(function(){
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('ul').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('ul').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('ul').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on("change", ".select-motor-model", function() {
            motor = $(this).closest('form').find('.select-motor-model').val();
            url = '{{modifySummaryUrl(['mt'=> '' ])}}';
            $(this).closest('form').attr('action',"{{route('summary','mt')}}/" + motor + url);
        });

        $('#motor_search_form .btn').click(function () {
           if(!$('#motor_search_form').attr('action')){
               swal(
                   '未選擇車型!',
                   '請先選擇車型再進行搜尋!',
                   'error'
               );
               return false;
           }else{
               window.location = $(this).closest('form').attr('action');
           }
        });



        $(document).on('change', ".select-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['br' => '%s'])) }}", value );
            }
        });

    </script>
@stop


