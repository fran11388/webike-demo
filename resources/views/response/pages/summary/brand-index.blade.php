@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/brand.css') !!}">
    <style>
        .item-block {
            height: 124px;
        }
    </style>
@endsection
@section('left')
    <div class="box-page-group">
        <div class="ct-box-page-group">
            <ul class="ul-left-title">
                @include('response.pages.summary.partials.brand-category-list')
            </ul>
        </div>
    </div>
    <div class="ct-brand-left-part box-fix-with">
        <div class="box-page-group">
            <div class="title-box-page-group box">
                <h3>對應車型品牌搜尋</h3>
            </div>
            <div class="ct-box-page-group">
                <ul class="ul-menu-ct-box-page-group ul-menu-ct-box-page-group-select">

                    @include('response.common.list.menu-mybike')
                    <li>
                        <div class="select-list-box">
                            <select class="select2 select-motor-manufacturer">
                                <option>請選擇廠牌</option>
                                @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </li>
                    <li class="li-cell-dropdown-item">
                        <div class="select-list-box">
                            <select class="select2 select-motor-displacement">
                                <option>cc數</option>
                            </select>
                        </div>
                    </li>
                    <li class="li-cell-dropdown-item">
                        <div class="select-list-box">
                            <select class="select2 select-motor-model">
                                <option>車型</option>
                            </select>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="box-page-group">
            @include('response.pages.summary.partials.brand-hot-brand')
        </div>
    </div>
@stop
@section('right')
    <div class="box-fix-auto">
        <div class="title-main-box">
            <h1>精選品牌</h1>
        </div>
        <div class="brand-table-container">
            <ul class="brand-info-table">
                <li class="brand-table-content">
                    <ul>
                        <li class="table-content-title col-md-3 col-sm-3 col-xs-3">
                            <span>搜尋條件</span>
                        </li>
                        <li class="table-content-tag col-md-9 col-sm-9 col-xs-9">
                            <ul>
                                @foreach($filters as $key => $filter)
                                    @if(request()->has($key))
                                        <li class="table-content-list-autowidth">
                                            <div class="table-content-tag-item">
                                                <span>{{$filter['label']}}:{{ $filter['name'] }}</span>
                                                @php
                                                    $arg = [];
                                                    foreach($filters as $_key => $_filter){
                                                        if($_key == $key ){
                                                            $arg[$_key] = '';
                                                        }else{
                                                            $arg[$_key] = request()->input($_key);
                                                        }
                                                    }
                                                @endphp
                                                <a href="{{ modifyManufacturerSearchUrl($arg) }}">
                                                    <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class="table-content-title col-md-3 col-sm-3 col-xs-3">
                            <span>品牌國籍</span>
                        </li>
                        <li class="table-content-list col-md-9 col-sm-9 col-xs-9">
                            <ul>
                                <li><a href="{{modifyGetParameters(['country'=>''])}}">全部</a></li>
                                @foreach($manufacturer_countries as $country)
                                    <li><a href="{{modifyGetParameters(['char'=>'','country'=>$country->name])}}">{{$country->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                    <ul>
                        <li class="table-content-title col-md-3 col-sm-3 col-xs-3">
                            <span>字首排列</span>
                        </li>
                        <li class=" table-content-list col-md-9 col-sm-9 col-xs-9">
                            <ul>
                                <li><a href="{{modifyGetParameters(['char'=>''])}}">全部</a></li>
                                @foreach($manufacturers_keys as $manufacturers_key)
                                    <li><a href="{{modifyGetParameters(['char'=>$manufacturers_key])}}">{{$manufacturers_key}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        @foreach($manufacturers as $char => $manufacturer)
            <div class="table-brands">
                <div class="table-brands-title">
                    <h2>{{$char}}字首</h2>
                </div>
                <ul class="table-brands-list">
                    @foreach($manufacturer as $key => $node)
                    <li class="table-brands-item">
                        <div class="table-brands-logo">
                            <a href="{{ route('summary',['br/'.$node->url_rewrite ]) }}" title="{{ $node->name . $tail }}">
                                <figure class="zoom-image is-hover-border">
                                    @if($node->image)
                                        {!! lazyImage( $node->image , $node->name . '('  . $node->count . ')' )  !!}
                                    @else
                                        <img src="https://img.webike.net/garage_img/no_photo.jpg" alt="{{ $node->name }}" class="item-block">
                                    @endif
                                </figure>
                            </a>
                        </div>
                        <a class="item-title dotted-text2 force-limit" href="{{ route('summary',['br/'.$node->url_rewrite ]) }}" title="{{ $node->name . $tail }}">{{ $node->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/brand/brand.js') !!}"></script>
    <script src="{!! assetRemote('js/basic/searchList.js') !!}"></script>
    <script type="text/javascript">
        var baseUrl = '{{ request()->url() }}';
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }
        $(".select-option-redirect").change(function () {
            window.location = $(this).val();
        });

        $("a.a-sub2 > span").click(function () {
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if(_this.closest('.ct-table-cell').length >= 1){
                var area = _this.closest('.ct-table-cell');
            }else{
                var area = _this.closest('.ul-menu-ct-box-page-group');
            }

            if (value) {
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get(url, function (data) {
                    var $target = area.find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if(_this.closest('.ct-table-cell').length >= 1){
                var area = _this.closest('.ct-table-cell');
            }else{
                var area = _this.closest('.ul-menu-ct-box-page-group');
            }

            if (value) {
                var url = path + "/api/motor/model?manufacturer=" +
                        _this.closest('.ul-menu-ct-box-page-group').find(".select-motor-manufacturer").find('option:selected').val() +
                        "&displacement=" +
                        value;

                $.get(url, function (data) {
                    var $target = area.find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-model,.select-motor-mybike", function(){
            var value = $(this).find('option:selected').val();
            if (value) {
                redirect('', value);
            }
        });


        function redirect(q, motor) {
            var params = {};
            if (q)
                params.q = q;
            else if (getParameterByName('q'))
                params.q = getParameterByName('q');
            if (motor)
                params.motor = motor;
            else if (getParameterByName('motor'))
                params.motor = getParameterByName('motor');


            var esc = encodeURIComponent;
            var query = Object.keys(params).map(k => esc(k) + '=' + esc(params[k])).join('&');
            if (query)
                query = "?" + query;

            window.location = window.location.href.split('?')[0] + query;
        }

        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
@stop


