
@if($is_not_phone)
    @if($hasFitMotors)
        <div class="box-ct-new-lineup box-content custom-parts">
            <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>{{ $fit_motor_title . '對應車型' }}</h2>
                {{--<div class="ct-title-main2-right">--}}
                {{--<a class="btn btn-warning" href="javascript:void(0)" title="{{ $summary_title . ' 全部促銷商品' . $tail }}">查看全部</a>--}}
                {{--</div>--}}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-gap-bottom">
                    <h3 class="font-bold">選擇您的車型</h3>
                </div>
                <div class="motor-select-box">
                    <form action="" method="GET" id="motor_search_form">
                        <ul class="box clearfix no-list-style" id="bike-first-row">
                            <li class="col-xs-6 col-sm-3 col-md-3 box">
                                <select class="select2 select-motor-manufacturer">
                                    <option value="">請選擇廠牌</option>
                                    @foreach ($motor_manufacturers as $_manufacturer)
                                        <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li class="col-xs-6 col-sm-3 col-md-3 box">
                                <select class="select2 select-motor-displacement">
                                    <option>cc數</option>
                                </select>
                            </li>
                            <li class="col-xs-6 col-sm-3 col-md-3 box">
                                <select class="select2 select-motor-model">
                                    <option value="">車型</option>
                                </select>
                            </li>
                            <li class="col-xs-12 col-sm-3 col-md-2 box">
                                <input class=" btn btn-primary btn-full" type="button" name="" value="搜尋">
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            @if(count($fit_mybikes))
                <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h3 class="font-bold">{!! $fit_motor_title . '對應MyBike　<a href="' . route('customer-mybike') . '" target="_blank"><i class="glyphicon glyphicon-edit"></i>設定Mybike</a>' !!}</h3>
                    </div>
                    @foreach ($fit_mybikes as $motor_groups)
                        <div class="box-fix-column ct-item-product-grid-category-bard-page motor-group">
                            <h4 class="title font-bold"><a href="{{route('motor-manufacturer', [$motor_groups->first()->manufacturer_url_rewrite, '00'])}}" title="{{$motor_groups->first()->manufacturer . $tail}}" target="_blank">{{$motor_groups->first()->manufacturer}}</a></h4>
                            <div class="box-fix-auto">
                                <ul>
                                    @foreach ($motor_groups as $motor)
                                        <li class="col-xs-6 col-sm-3 col-md-2-4 col-lg-2-4 motor-model">
                                            <a class="dotted-text1" href="{{ modifySummaryUrl(['mt'=>$motor->url_rewrite]) }}" title="{{$motor->manufacturer . ' ' . $motor->name . $tail}}">{{$motor->name}} ({{ $motor->count }})</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
                <?php
                    /*<hr class="width-full">*/
                ?>
            @endif

            {{-- @if(count($fit_motors))
                <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if(count($fit_motors))
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3 class="font-bold">{{ $fit_motor_title . '對應其他車種' }}</h3>
                        </div>
                    @endif
                    <div class="fit-motor-groups">
                        @php
                            $motor_group_key = 0;
                        @endphp
                        @foreach ($fit_motors as $motor_groups)
                            <div class="box-fix-column ct-item-product-grid-category-bard-page motor-group {!! $motor_group_key >= 4 ? 'hide' : '' !!}">
                                <h4 class="title font-bold"><a href="{{route('motor-manufacturer', [$motor_groups->first()->manufacturer_url_rewrite, '00'])}}" title="{{$motor_groups->first()->manufacturer . $tail}}" target="_blank">{{$motor_groups->first()->manufacturer}}</a></h4> --}}
                                <?php /*
                            <div class="box-fix-with item item-product-grid ct-item-product-grid-category-bard-page-left"></div>
                            <div class="box-fix-auto ct-item-product-grid-category-bard-page-right"></div>
                            */ ?>
                                {{-- <div class="box-fix-auto">
                                    <ul>
                                        @php
                                            $motor_model_key = 0;
                                        @endphp
                                        @foreach ($motor_groups as $motor)
                                            @if($motor_model_key >= 15)
                                                @break
                                            @endif
                                            <li class="col-xs-6 col-sm-3 col-md-2-4 col-lg-2-4 motor-model {!! $motor_model_key >= 10 ? 'hide' : '' !!}">
                                                <a class="dotted-text1" href="{{ modifySummaryUrl(['mt'=>$motor->url_rewrite]) }}" title="{{$motor->manufacturer . ' ' . $motor->name . $tail}}">{{$motor->name}} ({{ $motor->count }})</a>
                                            </li>
                                            @php
                                                $motor_model_key++;
                                            @endphp
                                        @endforeach
                                    </ul>
                                </div>

                                @if(count($motor_groups) >= 10)
                                    <div class="clearfix">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <a class="btn-customer-review-read-more motor-model-show" href="javascript:void(0)">>>顯示更多</a>
                                        </div>
                                    </div>
                                @endif
                                <hr class="width-full box">
                            </div>
                            @php
                                $motor_group_key++;
                            @endphp
                        @endforeach
                        @if(count($fit_motors) > 4)
                            <div class="text-center">
                                <a class="btn btn-asking motor-group-show" href="javascript:void(0)">查看更多對應車型</a>
                            </div>
                        @endif
                    </div>
                </div>
            @endif --}}

        </div>
    @endif

@else
    <div class="box-ct-new-lineup box-content custom-parts fit-motors-container">
        <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>{{ $summary_title . '對應車型' }}</h2>
        </div>
        <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12 motor-search-container">
            <ul class="ul-sub2 row">
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box btn-gap-top">
                    <div class="slide-down-btn">
                        <div class="a-sub2">
                            <select class="select2 select-motor-manufacturer width-full">
                                <option>請選擇廠牌</option>
                                @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </li>
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                    <div class="slide-down-btn">
                        <div class="a-sub2">
                            <select class="select2 select-motor-displacement width-full">
                                <option>cc數</option>
                            </select>
                        </div>
                    </div>
                </li>
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                    <div class="slide-down-btn">
                        <div class="a-sub2">
                            <select class="select2 select-motor-model width-full">
                                <option>車型</option>
                            </select>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
@endif
