@if(count($fit_motor_products))
    <div class="box-content custom-parts">
        <div class="title-main-box title-detail-info col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>{{ $summary_title }} MyBike對應商品</h2>
        </div>
        <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="product-list owl-carousel-6">
                @foreach ($fit_motor_products as $product)
                    <li>
                        @include('response.common.product.c')
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
@endif
