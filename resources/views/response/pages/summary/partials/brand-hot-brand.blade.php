@if(count($hot_manufacturers))
    <div class="title-box-page-group">
        <h3>熱門品牌</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group ul-menu-ct-box-page-group-ranking">
            @foreach ($hot_manufacturers as $key => $node)
                <li>
                    <div class="btn btn-default btn-brand-ranking ranking-label"><label>Top {{$loop->iteration}}</label></div>
                    <div class="ranking-image"><a href="{{route('summary', ['br/'.$node->url_rewrite])}}"><figure class="zoom-image is-hover-border">{!! lazyImage( $node->image , $node->name . '('  . $node->count . ')' )  !!}</figure></a></div>
                    <label class="ranking-text-disc bold blue-0066c0">{{$node->name}}</label>
                    {{--<div class="ranking-rating"><span class="icon-star star-04"></span><label>共133則評論</label></div>--}}
                </li>
                @if($loop->iteration == 5)
                    @break
                @endif
            @endforeach
        </ul>
    </div>
@endif