<div id="new-product" class="box-ct-new-lineup box-content custom-parts">
    <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>
            <span>{{ $summary_title }} 新商品　</span>
            <a href="{{ modifySummaryUrl(null,true) }}?sort=new" title="{{ $summary_title . ' 全部新商品' . $tail }}">>> 查看全部</a>
        </h2>
    </div>
    <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @foreach ($new_products->chunk(50) as $chunks)
            <ul class="product-list owl-carousel-6">
                @foreach ($chunks as $product)
                <li>
                    @include('response.common.product.d')
                </li>
                @endforeach
            </ul>
        @endforeach


    </div>
</div>