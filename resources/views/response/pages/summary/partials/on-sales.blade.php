@if (count($on_sales_products))
<div id="sale-ranking" class="box-ct-new-lineup box-content custom-parts">
    <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>
            <span>{{ $summary_title }} 促銷商品　</span>
            <a href="{{ modifySummaryUrl(null,true) }}?sort=sales&order=asc" title="{{ $summary_title . ' 全部促銷商品' . $tail }}">>> 查看全部</a>
        </h2>
    </div>
    <div class="ct-new-custom-part-on-sale-top col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <?php $product = $on_sales_products->first() ?>
                @include('response.common.product.f')
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <ul class="product-list">
                    @foreach ($on_sales_products as $product)
                        @if (!$loop->first)
                            <li class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                @include('response.common.product.g')
                            </li>
                        @endif
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>
@endif