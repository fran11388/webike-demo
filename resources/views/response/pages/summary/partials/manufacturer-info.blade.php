<div class="col-md-3 col-sm-4 col-xs-12">
    <a href="javascript:void(0)">
        <figure class="zoom-image border-box">
            <img src="{{ cdnTransform(str_replace('http:','',$current_manufacturer->image)) }}" alt="{{ $current_manufacturer->name }}">
        </figure>
    </a>
</div>
<div class="col-md-9 col-sm-8 col-xs-12">
    <h1 class="clearfix box">
        {{ $current_manufacturer->name }}
        @if($current_manufacturer->website)
            <div class="ct-title-main2-right">
                <a class="btn btn-warning" href="{{ $current_manufacturer->website }}" title="{{ $current_manufacturer->name . '官方網站' . $tail }}" target="_blank">官方網站</a>
            </div>
        @endif
    </h1>
    <span>
        {!! $current_manufacturer->description !!}
    </span>
</div>