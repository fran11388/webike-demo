<style>
    .subCategory-container {

    }
    .subCategory-container .contain .category-link{
        border: 1px solid #acacac;
        padding:5px;
    }
    .subCategory-container .contain .category-link:hover .category-title span{
        text-decoration:underline;
    }
    .subCategory-container .contain .category-title {
        float:left;
        font-size:0px;
        padding:0px;
    }
    .subCategory-container .contain .category-title span{
        display: table-cell;
        vertical-align: middle;
        position: relative;
        height: 40px;
    }
        
    .subCategory-container .categories-item {
        height:182px;
        overflow:auto;
        border: 1px solid #dcdee3;
    }
    .subCategory-container .categories-item ul li a{
        padding:8px;
        display:block;
    }
    .subCategory-container .categories-item ul li a:hover{
        background-color: #dcdee3;
        color: #005fb3 !important; 
    }
    .subCategory-container .categories-item ul li a:hover span{
        background-color: #dcdee3;
        color: #005fb3 !important; 
        text-decoration:underline;
    }
    .subCategory-container  li {
        list-style-type:none;
    }
    .subCategory-container .zoom-image img {
        height:48px;
        display:block;
        margin:0 auto;
    }
    .subCategory-container .zoom-image {
        display:block;
        border:0px;
    }
    .subCategory-container .image-container {
        padding:0px;
        text-align:right;
    }
    .subCategory-container .contain .category-title span{
        display: table-cell;
        vertical-align: middle;
        position: relative;
        height: 48px;
        color:#333333;
    }
    .subCategory-container .contain .category-title figure {
        margin-right:10px;
        float:left;
    }
    .subCategory-container .helper {
        height:48px;
    }
    .subCategory-container .zoom-image:hover img {
        opacity:1;
    }
    .subCategory-container .fa-chevron-right:hover {
        color: #005fb3;
    }
    .subCategory-container .contain .link:hover {
        color:#337ab7;
    }
    .subCategory-container .noneborder {
        border:0px;
    }
    .subCategory-container .margin40 {
        margin-bottom:40px;
    }
    .magazine-container {
        padding:10px 0px 0px 0px !important;
        margin-bottom:-20px;
    }
    @media(max-width:767px){
       .subCategory-container .contain .category-title { 
            margin-top:0px;
        }
        .subCategory-container .contain .category-link{
            padding:5px;
        }
        .subCategory-container .contain .category-title span{
            height:52px;
        }
        .subCategory-container .helper {
            height:52px;
        }
        .subCategory-container .margin40 {
            margin-bottom:10px;
        }
        .magazine-container {
            padding:0px !important;
            margin-bottom:0px;
        }
    }
</style>


<div id="all-good" class="box-ct-new-lineup box-content custom-parts">
    <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>
            <span>{{ isset($current_name) ? $current_name : $summary_title }}  商品分類　</span>
            <a href="{{ modifySummaryUrl(null,true) }}" title="{{ (isset($current_name) ? $current_name : $summary_title) . ' 全部分類商品' . $tail }}">>> 查看全部</a>
        </h2>
    </div>
    <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12 magazine-container">
        @php
            $child_flg = false;
        @endphp
        @if (isset($current_category) and $current_category )
            <?php
            $url_path = explode('-', $current_category->mptt->url_path);
            $target = $tree->where('url_rewrite', $url_path[0])->first();
            if($target){
                for ($i = 1; $i < count($url_path); $i++) {
                    $target = $target->nodes->where('url_rewrite', $url_path[$i])->first();
                }
                $parent = $target;
                if($target){
                    $target = $target->nodes->reject(function ($node) {
                        return $node->count == 0;
                    });
                }else{
                    $target = collect();
                }
            }else{
                $target = collect();
            }

            ?>
        @else
            <?php  $target = $tree; ?>
        @endif
        <div class="subCategory-container">
            <div class="contain">
                <ul class="clearfix">
                    @php
                        $all = $target;
                        $target = $target->filter(function($category,$key){
                            return count($category->nodes) != 0;
                        });
                        foreach($all as $key => $node){
                            if(count($node->nodes) == 0){
                                $target = $target->push($node);
                            }
                        }

                        $num = 1; 
                        $subCategory_box_test = false;
                    @endphp
                    @foreach ($target as $key => $node)
                        @if ($node->count)
                            <?php
                            $expand = false;
                            if (isset($current_category) and $current_category)
                                $expand = ($node->id == $current_category->id) ? true : false;
                            elseif ($loop->first)
                                $expand = true;
                            ?>
                            <li class="col-md-3 col-sm-6 col-xs-12 box margin40">
                                <?php
                                $args = [];
                                $args['ca'] = $node->mptt->url_path;
                                if (isset($current_manufacturer)){
                                    $args['br'] = $current_manufacturer->url_rewrite;
                                }
                                ?>
                                <a href="{{modifySummaryUrl($args)}}" class="link {{ count($node->nodes) ? 'subnone' : ''}}">
                                    <div class="category-link clearfix">
                                        <div class="category-title col-md-11 col-sm-11 col-xs-11 clearfix">
                                            <figure class="zoom-image thumb-box-border">
                                                {!!  lazyImage( cdnTransform(getCategoryImage($node)) , $node->name ) !!}
                                            </figure>
                                            <span class="size-10rem font-bold">{{ $node->name }} <!-- ({{ $node->count }}) --></span>
                                        </div>
                                        <div class="image-container col-md-1 col-sm-1 col-xs-1 hidden-xs">
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                            <span class="helper"></span>
                                        </div>
                                        <div class="image-container col-md-1 col-sm-1 col-xs-1 visible-xs media-category">
                                            <i class="fa {{count($node->nodes) ? 'fa-chevron-down' : 'fa-chevron-right'}}" aria-hidden="true"></i>
                                            <span class="helper"></span>
                                        </div>
                                    </div>
                                </a>
                                @if(count($node->nodes))
                                    @php
                                        $child_flg = true;
                                        $subCategory_box_test = true;
                                    @endphp
                                    <div class="categories-item hidden-xs">
                                        <ul>
                                            @foreach ($node->nodes as $key=> $node)
                                                @if ($node->count)
                                                    <li>
                                                        <?php
                                                        $args = [];
                                                        $args['ca'] = $node->mptt->url_path;
                                                        if (isset($current_manufacturer) and $current_manufacturer){
                                                            $args['br'] = $current_manufacturer->url_rewrite;
                                                        }
                                                        ?>
                                                        <a href="{{modifySummaryUrl($args)}}" title="{!! $node->name !!}">
                                                            <span class="size-08rem">{{ $node->name }} ({{ $node->count }})</span>
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @else
                                    @if($subCategory_box_test == true)
                                        <div class="categories-item hidden-xs"></div>
                                    @endif
                                @endif
                                @php
                                    if($num == 4){
                                        $num = 0;
                                        $subCategory_box_test = false;
                                    }
                                    $num++;
                                @endphp
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        </ul>
    </div>
</div>

<script>
    $(document).ready(function(){
        @if(!$child_flg)
            $('.categories-item').remove();
        @endif

        if (window.matchMedia('(max-width: 767px)').matches) {
            $('.subCategory-container .contain .subnone').attr("href","javascript:void(0)");
        }
    });
    $('.category-link').click(function(){
        if (window.matchMedia('(max-width: 767px)').matches) {
            var link = $(this).parent().attr('href');
            if(link == "javascript:void(0)"){
                var menu = $(this).find('.media-category i').attr('class');
                if(menu == "fa fa-chevron-down"){
                    $(this).find('.media-category i').addClass('fa-chevron-up');
                    $(this).find('.media-category i').removeClass('fa-chevron-down');
                }else {
                    $(this).find('.media-category i').addClass('fa-chevron-down');
                    $(this).find('.media-category i').removeClass('fa-chevron-up');
                }
            
                var display = $(this).parent().siblings('.categories-item').css('display');
                if(display == 'block'){
                    $(this).parent().siblings('.categories-item').addClass('hidden-xs');
                }else{
                    $(this).parent().siblings('.categories-item').removeClass('hidden-xs');
                }
            }
        }
    });
</script>