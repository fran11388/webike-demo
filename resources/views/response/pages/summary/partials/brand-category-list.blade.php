<li class="li-left-title col-md-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title">
            <span>分類對應品牌</span>
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12">
            <ul class="ul-sub2 row">
            @if ($current_category and $current_category->depth > 1 )
                <?php
                $url_path = explode('-', $current_category->mptt->url_path);
                $target = $tree->where('url_rewrite', $url_path[0])->first();
                if($target){
                    for ($i = 1; $i < count($url_path) - 1; $i++) {
                        $target = $target->nodes->where('url_rewrite', $url_path[$i])->first();
                    }
                    $parent = $target;
                    if($target){
                        $target = $target->nodes->reject(function ($node) {
                            return $node->count == 0;
                        });
                    }else{
                        $target = collect();
                    }
                }else{
                    $target = collect();
                }

                ?>
            @else
                <?php  $target = $tree ?>
            @endif
            @foreach ($target as $key => $node)
                @if ($node->count)
                    @php
                        $expand = false;
                        if ($current_category)
                            $expand = ($node->id == $current_category->id) ? true : false;
                        elseif ($loop->first)
                            $expand = true;
                    @endphp
                @endif
                <li class="li-sub2 col-md-12">
                    <div class="slide-down-btn clicked">
                        <div class="a-sub2">
                            @if(count($node->nodes))
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            @endif
                            <a href="{{modifyCategoryListUrl(Route::currentRouteName(),$node->mptt->url_path)}}" title="{{ $node->name . $tail }}">
                                <span>{{ $node->name }}</span>
                                <span class="count">({{ $node->count }})</span>
                            </a>
                        </div>
                    </div>
                    <ul class="ul-sub3 row" style="display: none;">
                        @foreach ($node->nodes as $key=> $node)
                            @if ($node->count)
                                <li class="li-sub2 col-md-12" style="display: none;">
                                    <div class="slide-down-btn">
                                        <div class="a-sub2">
                                            <a href="{{modifyCategoryListUrl(Route::currentRouteName(),$node->mptt->url_path)}}" title="{{ $node->name . $tail }}">
                                                <span>{{ $node->name }}</span>
                                                <span class="count">({{ $node->count }})</span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
            @endforeach
            </ul>
        </li>
    </ul>
</li>