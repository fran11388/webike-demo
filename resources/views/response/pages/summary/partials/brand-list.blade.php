@if (!$current_manufacturer)
<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title">
            <span>商品品牌</span>
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                @php
                    $manufacturer_max_count = 5;
                @endphp
                @foreach ($manufacturer as $key => $node)
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 {{$key >= $manufacturer_max_count ? 'hide' : ''}}">
                        <a class="a-sub2 {!! ($current_manufacturer and ($current_manufacturer->url_rewrite == $node->url_rewrite)) ? 'active' : '' !!}" href="{{modifySummaryUrl(['br'=> $node->url_rewrite ])}}" title="{{$node->name . $tail}}">
                            <span>
                                {{-- <input class="checkbox-left-sidebar-li" type="checkbox"> --}}
                                {{ $node->name }}
                            </span>
                            <span class="count">({{ $node->count }})</span>
                        </a>
                    </li>
                    @if($loop->iteration == $manufacturer_max_count)
                        @break
                    @endif
                @endforeach
                @if(count($manufacturer) > $manufacturer_max_count)
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 toggle-partner-switch">
                        <a class="a-sub2 text-right" href="{{route('brand',request()->only(['category','motor']) )}}" title="{{$node->name . $tail}}">
                            <span class="font-color-blue">>> 查看全部</span>
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    </ul>
</li>
@endif