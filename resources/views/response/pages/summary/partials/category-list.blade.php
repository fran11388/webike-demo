<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title">
            <span>商品分類</span>
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                @if ($current_category)
                    <?php
                    $url_path = explode('-', $current_category->mptt->url_path);
                    $target = $tree->where('url_rewrite', $url_path[0])->first();

                    for ($i = 1; $i < count($url_path) - 1; $i++) {
                        $target = $target->nodes->where('url_rewrite', $url_path[$i])->first();
                    }

                    $parent = $target;
                    if($target){
                        $target = $target->nodes->reject(function ($node) {
                            return $node->count == 0;
                        });
                    }else{
                        $target = collect();
                    }

                    ?>
                @else
                    <?php  $target = $tree ?>
                @endif
                @foreach ($target as $key => $node)
                    @if ($node->count)
                        @php
                            $args = [];
                            $args['ca'] = $node->mptt->url_path;
                            if ($current_manufacturer){
                                $args['br'] = $current_manufacturer->url_rewrite;
                            }
                            $folder_opened = ($current_category and ($current_category->url_rewrite == $node->url_rewrite)) ? true : false;
                        @endphp
                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                            <div class="slide-down-btn {!! $folder_opened ? '' : 'clicked' !!}">
                                <div class="a-sub2 {!! $folder_opened ? 'active' : '' !!}">
                                    @if(count($node->nodes))
                                        <i class="fa fa-{!! $folder_opened ? 'minus' : 'plus' !!}" aria-hidden="true"></i>
                                    @endif
                                    <a href="{{modifySummaryUrl($args)}}" title="{{ $node->name . $tail }}">
                                        <span>{{ $node->name }}</span>
                                    </a>
                                </div>
                            </div>
                            <ul class="ul-sub2 row" {!! $folder_opened ? '' : 'style="display: none;"' !!}>
                                @foreach ($node->nodes as $key=> $node_child)
                                    @if ($node_child->count)
                                        <?php
                                        $args = [];
                                        $args['ca'] = $node_child->mptt->url_path;
                                        if ($current_manufacturer){
                                            $args['br'] = $current_manufacturer->url_rewrite;
                                        }
                                        ?>
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12" {!! $folder_opened ? '' : 'style="display: none;"' !!}>
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="{{modifySummaryUrl($args)}}">
                                                        &nbsp
                                                        <span>{{ $node_child->name }}</span>
                                                        &nbsp
                                                        <span class="count">({{ $node_child->count }})</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>
    </ul>
</li>