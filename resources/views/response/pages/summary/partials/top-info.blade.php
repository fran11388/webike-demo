@if($current_category and !$current_motor and !$current_manufacturer)
    <!-- slide -->
    <div class="clearfix box">
        <div class="title-main-box">
            <h1>{{ $summary_title }}</h1>
        </div>
    </div>
    <div class="block">
        @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))
            @if(false !== strpos($current_category->mptt->url_path , '1000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>16,'lazy_load'=>false])
            @elseif(false !== strpos($current_category->mptt->url_path , '3000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>17,'lazy_load'=>false])
            @elseif(false !== strpos($current_category->mptt->url_path , '8000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>30,'lazy_load'=>false])
            @elseif(false !== strpos($current_category->mptt->url_path , '4000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>29,'lazy_load'=>false])
            @endif
        @else
            @if(false !== strpos($current_category->mptt->url_path , '1000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>6,'lazy_load'=>false])
            @elseif(false !== strpos($current_category->mptt->url_path , '3000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>7,'lazy_load'=>false])
            @elseif(false !== strpos($current_category->mptt->url_path , '8000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>30,'lazy_load'=>false])
            @elseif(false !== strpos($current_category->mptt->url_path , '4000') )
                @include('response.common.ad.banner-slideshow',['ad_id'=>29,'lazy_load'=>false])
            @endif
        @endif
    </div>
@else
    <div class="box-content-brand-top-page">
        <div class="box-fix-column ct-brand-top-page">
            @if($current_motor)
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="content-box-ct-brand-top-page">
                            @include('response.pages.motor.partials.info')
                        </div>
                        <hr class="visible-sm visible-xs">
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        @include('response.pages.motor.partials.analysis')
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="content-box-ct-brand-top-page">
                            @include('response.pages.summary.partials.manufacturer-info')
                        </div>
                        <hr class="visible-sm visible-xs">
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        @include('response.pages.summary.partials.review-ranking-avg')
                    </div>
                </div>
            @endif
        </div>

        <div id="myNavbar" class="menu-brand-top-page hidden-xs">
            <div class="ct-menu-brand-top-page2">
                @include('response.pages.summary.partials.content-tabs')
            </div>
        </div>
    </div>
@endif
