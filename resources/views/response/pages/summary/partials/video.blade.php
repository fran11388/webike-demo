<style>
    .active1{
        background: #A10400;
        font-weight: bold;
        color: white !important;
        padding: 2px 5px;
        border-radius: 5px;
    }
    .active1:hover{
        text-decoration: none;
    }
    .row .title-main-box{
        font-size: 1rem;
    }

</style>
@foreach($motoVideoResult['title'] as $key => $result)
    @if($motoVideoResult['result'][$key] == 'true' and isset($motoVideoResult['data'][$key]) )
        <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h1 class="col-xs-12 col-sm-12 col-md-3">{{$motoVideoResult['title'][$key]}}</h1>
                    <ul class="menu-title-main-box" style="text-decoration: none;">
                        <li><span>排序:</span></li>
                        <li><a href="{{ URL::route('motor-video', array($current_motor->url_rewrite)) }}?sort=title" class="{{$motoVideoResult['sort'][$key] == 'title' ? 'active1' : ''}}">相關性</a></li>
                        <li><a href="{{ URL::route('motor-video', array($current_motor->url_rewrite)) }}?sort=viewCount" class="{{$motoVideoResult['sort'][$key] == 'viewCount' ? 'active1' : ''}}">人氣排序</a></li>
                        <li><a href="{{ URL::route('motor-video', array($current_motor->url_rewrite)) }}?sort=date" class="{{$motoVideoResult['sort'][$key] == 'date' ? 'active1' : ''}}">更新時間</a></li>
                    </ul>
                </div>
                <ul class="grid-item-video">
                    @foreach ( $motoVideoResult['data'][$key]['items'] as $item )
                        @if(isset($item['id']['videoId']))
                            <li class="col-xs-12 col-sm-4 col-md-4">
                                <div class="container-video">
                                    <figure class="zoom-image">
                                        <a href="https://www.youtube.com/watch?v={{$item['id']['videoId']}}" title="{{$item['snippet']['title']}}"  target=_blank;>
                                            <img src="{{ isset($item['snippet']['thumbnails']['medium']['url']) ? $item['snippet']['thumbnails']['medium']['url'] : '' }}" alt="{{$item['snippet']['title']}}">
                                            <span class="icon-video"><i class="fa fa-play" aria-hidden="true"></i></span>
                                        </a>
                                    </figure>
                                    <a class="title-product-item dotted-text2" href="https://www.youtube.com/watch?v={{$item['id']['videoId']}}" title="{{$item['snippet']['title']}}" target=_blank;>{{$item['snippet']['title']}}</a>
                                    <span class="dotted-text4">
                                    {{ isset($item['snippet']['description']) ? $item['snippet']['description'] : ''}}
                                    </span>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    @else
       <div class="col-xs-12 col-sm-12 col-md-12 form-group">
            <div class="row">
                <div class="title-main-box">
                    <h2>{{$motoVideoResult['title'][$key]}}</h2>
                </div>
                <div class="" style="font-size: 22px;text-align: center;margin:100px 0;font-weight: bold;">此車型暫無影片</div>
            </div>
        </div>
    @endif
@endforeach