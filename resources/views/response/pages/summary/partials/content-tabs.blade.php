@if(isset($current_motor) and $current_motor)
    <ul class="ul-menu-brand-top-page ul-menu-motor-top-page">
        <li>
            <a href="{{URL::route('motor-top', $motor->url_rewrite)}}" title="車型首頁{{$tail}}">車型首頁</a>
        </li>
        <li>
            <a href="{{URL::route('motor-service', $motor->url_rewrite)}}" title="{{$motor->name}}規格總覽{{$tail}}">規格總覽</a>
        </li>
        <li>
            <a href="{{URL::route('summary', 'mt/' . $motor->url_rewrite)}}" title="改裝及用品{{$tail}}">改裝及用品</a>
        </li>
        @if($genuine_link)
            <li>
                <a href="{{$genuine_link}}" title="正廠零件{{$tail}}" target="_blank">正廠零件</a>
            </li>
        @endif
        <li>
            <a href="{{URL::route('motor-review',  $motor->url_rewrite)}}" title="商品評論{{$tail}}">商品評論</a>
        </li>
        <li>
            <a href="{{URL::route('motor-video',  $motor->url_rewrite)}}" title="車友影片{{$tail}}">車友影片</a>
        </li>
        <li>
            <a href="http://www.webike.tw/motomarket/search/fulltext?q={{$motor->name}}" title="新車、中古車{{$tail}}" target="_blank">新車、中古車</a>
        </li>
    </ul>
@elseif(count($current_manufacturer))
    <ul class="ul-menu-brand-top-page">
        <li class="col-xs-6 col-sm-2 col-md-2">
            <a href="{{route('summary', 'br/' . $current_manufacturer->url_rewrite)}}">品牌首頁</a>
        </li>
        <li class="col-xs-6 col-sm-2 col-md-2">
            <a href="{{getPartsUrl(['manufacturer' => $current_manufacturer])}}?sort=new">最新商品</a>
        </li>
        <li class="col-xs-6 col-sm-2 col-md-2">
            <a href="{{getPartsUrl(['manufacturer' => $current_manufacturer])}}">所有商品</a>
        </li>
        <!--
        <li class="col-xs-6 col-sm-2 col-md-2">
            <a href="#sale-ranking">銷售排行</a>
        </li>
        -->
        <li class="col-xs-6 col-sm-2 col-md-2">
            <a href="{!! route('review-search') !!}?br={{$current_manufacturer->name}}">商品評論</a>
        </li>
        @if($manufacturer_info and $manufacturer_info->active)
            <li class="col-xs-6 col-sm-2 col-md-2">
                <a href="{!! route('brand-info', $current_manufacturer->url_rewrite) !!}">品牌介紹</a>
            </li>
        @endif
    </ul>
@endif