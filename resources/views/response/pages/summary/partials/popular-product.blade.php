<div id="new-product" class="box-ct-new-lineup box-content custom-parts">
    <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>
            <span>{{ $summary_title }} 熱門商品　</span>
            <a href="{{ modifySummaryUrl(null,true) }}" title="{{ $summary_title . ' 全部熱門商品' . $tail }}">>> 查看全部</a>
        </h2>
    </div>
    <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php $page = $popular_products->params->page; ?>
            @for($i=1;$i<=$page;$i++)
            <?php $popular_products->params->page = $i; ?>
            <ul class="product-list owl-carousel-ajax" data-owl-ajax="{{json_encode($popular_products->params)}}" data-owl-ajax-load="false">
                @foreach ($popular_products->collection[$i] as $product)
                    <li data-sku="{{$product->sku}}">
                        @include('response.common.product.d')
                    </li>
                @endforeach
            </ul>
            @endfor
    </div>
</div>