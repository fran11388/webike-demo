@if (!isset($current_manufacturer) or !$current_manufacturer)
    <div class="box-ct-new-lineup box-content custom-parts">
        <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>
                <span>{{ $summary_title }} 熱門品牌　</span>
                <a href="{{route('brand',request()->only(['category','motor']) )}}" title="{{ $summary_title . ' 全部熱門品牌' . $tail }}">>>查看全部</a>
            </h2>
        </div>
        <div class="ct-new-custom-part-on-sale-top col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="ul-ct-famous-brand owl-carousel-responsive" data-owl-items="3">
                @foreach ($hot_manufacturers as $key => $node)

                    <li>
                        <a href="{{modifySummaryUrl(['br'=> $node->url_rewrite ])}}">{!! lazyImage( $node->image , $node->name . '('  . $node->count . ')' )  !!}</a>
                    </li>
                    @if($loop->iteration == 40)
                        @break
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
@endif