<div>
    <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2>
            <span>{{$current_manufacturer->name}} MOVIE</span>
        </h2>
    </div>
    <div class="text-center">
        @if($current_manufacturer->url_rewrite == 26)
            <iframe width="900" height="315" src="https://www.youtube.com/embed/Y2RI7MSPiOQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @elseif($current_manufacturer->url_rewrite == 411)
            <iframe width="900" height="315" src="https://www.youtube.com/embed/wwYK-oxwvF4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @endif
    </div>
</div>