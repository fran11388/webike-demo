<style>
    .ct-right-below .ct-right-below-body .product-item ul.product-description{
        height:110px;
    }
    .brand-ranking-right-info {
        float:none !important;
        display:inline-block !important;
        vertical-align: middle;
        padding:5px;
        margin:0px !important;
    }
    @media(min-width:768px){
        .product-info-search-list {
            width:65% !important;
        }
        .product-state-search-list {
            width:35% !important;
        }
    }
    .rating {
        height:43px !important;
    }
</style>
@foreach($manufacturers as $manufacturer)
    <div class="ct-right-below clearfix ranking-container">
        <div class="product-grid-ranking text-center">
            <p class="lank lank-0{{$loop->iteration}}">{{$loop->iteration}}</p>
            <span class="helper"></span>
        </div>
        <div class="ranking-product-detail">
            <div class="ct-right-below-body">
                <div class="product-item product-item-style-2" data-sku="{{$manufacturer->url_rewrite}}">
                    <div class="product-img">
                        <a class="thumbnail thumbnail-pin search-list-img zoom-image"
                           href="{{ route('summary' , 'br/'. $manufacturer->url_rewrite ) }}" target="_blank">
                            {!! lazyImage( \Thumbor\Url\Builder::construct(config('phumbor.server'), config('phumbor.secret'), unicodeConvertForSpace(fixHttpText($manufacturer->image)))->fitIn(190, 190)->build() , $manufacturer->full_name ) !!}
                            <span class="helper"></span>
                        </a>
                    </div>
                    <div class="product-detail">
                        <div class="product-info-search-list">
                            <h3 class="product-name">
                                <a class="dotted-text3" href="{{ route('summary' , 'br/'. $manufacturer->url_rewrite ) }}" target="_blank">
                                    <h1>{{ $manufacturer->full_name }}</h1>
                                </a>
                            </h3>

                            <ul class="product-description">
                                {{$manufacturer->description}}
                            </ul>
                        </div>
                        <div class="product-state-search-list">
                            <div class="rating box">
                                <div class="container icon-star-container">
                                    @if($manufacturer->review_count > 0)
                                        <div class="title-star-review">
                                            <span class="icon-star-bigger star-{{ str_pad(round($manufacturer->review_score / $manufacturer->review_score_count), 2,'0' , STR_PAD_LEFT) }} icon-star-element star-fix brand-ranking-right-info"></span>
                                            <h1 class="brand-ranking-right-info">{{number_format($manufacturer->review_score / $manufacturer->review_score_count,1)}}</h1>
                                            <span class="brand-ranking-right-info">({{$manufacturer->review_count}} 商品評論)</span>
                                        </div>
                                    @else
                                        <span class="icon-star star-hide icon-star-element star-fix"></span>
                                    @endif
                                </div>
                            </div>
                            @if($manufacturer->custom_count)
                                @if($current_motor)
                                    <div class="block">
                                        <a href="{{ route('parts' , 'mt/'.$current_motor->url_rewrite .'/ca/1000/br/'. $manufacturer->url_rewrite ) }}" title="「{{$current_motor->name}}」 {{$manufacturer->full_name}} 改裝零件 商品一覽" target="_blank">
                                            <span>「{{$current_motor->name}}」<br/>改裝零件 : {{$manufacturer->custom_count}}項商品</span>
                                        </a>
                                    </div>
                                @else
                                    <div class="block">
                                        <a href="{{ route('summary' , 'ca/1000/br/'. $manufacturer->url_rewrite ) }}" title="{{$manufacturer->full_name}}" target="_blank">
                                            <span>改裝零件 : {{$manufacturer->custom_count}}項商品</span>
                                        </a>
                                    </div>
                                @endif

                            @endif
                            @if($manufacturer->riding_count)
                                @if($current_motor)
                                    <div class="block">
                                        <a href="{{ route('parts' , 'mt/'.$current_motor->url_rewrite .'/ca/3000/br/'. $manufacturer->url_rewrite ) }}" title="「{{$current_motor->name}}」 {{$manufacturer->full_name}} 騎士用品 商品一覽" target="_blank">
                                            <span>「{{$current_motor->name}}」<br/>騎士用品 : {{$manufacturer->riding_count}}項商品</span>
                                        </a>
                                    </div>
                                @else
                                    <div class="block">
                                        <a href="{{ route('summary' , 'ca/3000/br/'. $manufacturer->url_rewrite ) }}" title="{{$manufacturer->full_name}}" target="_blank">
                                            <span>騎士用品 : {{$manufacturer->riding_count}}項商品</span>
                                        </a>
                                    </div>
                                @endif
                            @endif
                            <div class="div-btn-check-detail container product-detail-button">
                                <a href="{{ route('summary' , 'br/'. $manufacturer->url_rewrite ) }}" title="{{$manufacturer->full_name}}" target="_blank">
                                    <span>品牌首頁</span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach