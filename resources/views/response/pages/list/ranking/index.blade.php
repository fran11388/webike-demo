@extends('response.layouts.2columns')
@section('style')
@stop
@section('left')
    <aside class="ranking-left-container ct-left-search-list ct-left-search-list-main-menu col-xs-12 col-sm-4 col-md-3 col-lg-3">

        <ul class="ul-left-title row">
            @if(in_array('motors', $interface))
                <li class="li-left-title col-md-12 col-sm-12 col-xs-12">
                    <div class="slide-down-btn">
                        <a href="javascript:void(0)" class="a-left-title">
                            <span>車輛搜尋</span>
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                        </a>
                    </div>
                    <ul class="ul-sub1 row">
                        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                            <ul class="ul-sub2 row">
                                @if($current_customer)
                                    <?php
                                    $customer_motor = $current_customer->motors;
                                    ?>
                                    @if(count($customer_motor))
                                        <li class="btn-my-bike-container li-sub2 col-md-12 col-sm-12 col-xs-12 box text-center btn-gap-top visible-sm visible-md visible-lg">
                                            <a href="javascript:void(0)" class="btn-my-bike visible-md visible-lg"> <span>MyBike商品搜尋</span>
                                                <p><i class="fa fa-chevron-down" aria-hidden="true"></i></p>
                                            </a>
                                            <a href="" class="btn-my-bike-tablet font-color-red visible-sm">
                                                <i class="fa fa-motorcycle size-15rem" aria-hidden="true"></i>
                                            </a>

                                            <ul class="show-my-bike">
                                                @foreach ($customer_motor as $my_motor)
                                                    <li>
                                                        <a class="select-motor-mybike" href="javascript:void(0)" name="{{ $my_motor->url_rewrite }}" title="{{ $my_motor->name . $tail }}">
                                                            {{ $my_motor->name }}
                                                        </a>
                                                    </li>
                                                @endforeach

                                                <li class="btn-add-more-bike">
                                                    <a href="{{ route('customer-mybike') }}" target="_blank">
                                                        <i class="fa fa-plus" aria-hidden="true"></i> 編輯MyBike
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <script type="text/javascript">
                                            @php
                                                $route_name = Route::currentRouteName();
                                            @endphp
                                            $('.select-motor-mybike').click(function(){
                                                var value = $(this).prop('name');
                                                console.log(value);
                                                if (value){
                                                    @if(strpos($route_name, 'review') === false)
                                                        window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['mt' => '%s'])) }}", value );
                                                            @else
                                                            @if(strpos($route_name, 'review') !== false)
                                                    var url = "{!! modifyGetParameters(['page' => '', 'mt'=> 'mtUrlRewrite'], route('review-search', '')) !!}";
                                                    @endif
                                                        window.location = url.replace('mtUrlRewrite', value);
                                                    @endif
                                                }
                                            });
                                        </script>
                                    @endif
                                @else
                                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box text-center visible-sm visible-md visible-lg"><a class="btn btn-danger btn-full" href="{{ route('customer-mybike') }}" class="btn-login font-color-red">登錄MyBike</a></li>
                                @endif
                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                                    <div class="slide-down-btn">
                                        <div class="a-sub2">
                                            <select class="select2 select-motor-manufacturer">
                                                <option>請選擇廠牌</option>
                                                @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                                    <div class="slide-down-btn">
                                        <div class="a-sub2">
                                            <select class="select2 select-motor-displacement">
                                                <option>cc數</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                                    <div class="slide-down-btn">
                                        <div class="a-sub2">
                                            <select class="select2 select-motor-model">
                                                <option>車型</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            @endif
            @include('response.pages.list.partials.category-list')
        </ul>

    </aside>
    @if(false)
        <div class="box-page-group">
            <div class="title-box-page-group">
                <h3>推薦品牌</h3>
            </div>
            <div class="ct-box-page-group">
                <ul class="ul-banner-ct-box-page-group">
                    @for($i = 1 ; $i <= 10 ; $i++)
                        <li>
                            <div class="benefit_div recom">
                                {!! $advertiser->call(35) !!}
                            </div>
                        </li>
                    @endfor
                </ul>
            </div>
        </div>
    @endif

    @include('response.common.list.view-history-vertical')
@stop

@section('right')
    <div class="ct-right-search-list ">
        <div class="clearfix">
            <div class="title-main-box">
                <h1>{{ $title . ' 推薦' . $list_name }}</h1>
            </div>
        </div>
        <div id="myNavbar" class="menu-brand-top-page">
            <div class="ct-menu-brand-top-page2 ranking-tabs">
                <ul class="ul-menu-brand-top-page ul-menu-motor-top-page clearfix">
                    <li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-2-4">
                        <a class="{{ ($type !== 'brands' and $type !== 'popularity') ? 'navbar-active' : '' }}" href="{{ getCurrentExploreUrl(['type'=>'' ]) }}" title="銷售商品排名{{$tail}}">銷售商品排名</a>
                    </li>
                    @if(!$current_manufacturer)
                        <li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-2-4">
                            <a class="{{$type == 'brands' ? 'navbar-active' : '' }}" href="{{ getCurrentExploreUrl(['type'=>'brands' ]) }}" title="銷售品牌排名{{$tail}}">銷售品牌排名</a>
                        </li>
                    @endif
                    <li class="col-lg-2-4 col-md-2-4 col-sm-2-4 col-xs-2-4">
                        <a class="{{$type == 'popularity' ? 'navbar-active' : '' }}" href="{{ getCurrentExploreUrl(['type'=>'popularity' ]) }}" title="商品注目度排名{{$tail}}">商品注目<span class="hidden-xs">度</span>排名</a>
                    </li>
                </ul>
            </div>
            <div class="ct-right-top ranking-filter box">
                <div class="ct-table">
                    <div class="ct-table-row">
                        <div class="ct-table-cell cell-title width-full search-container">
                            <ul class="ul-cell-dropdown-list container cover-bottom-gap-5">
                                <li class="li-cell-dropdown-item li-cell-auto-width">
                                    <span class="ct-table-cell-title">目前搜尋條件 : </span>
                                </li>
                                @if ( $current_category)
                                    <li class="li-cell-dropdown-item li-cell-auto-width">
                                        <div class="btn-label div-tag-item">
                                            <span>分類：{{ $current_category->name }}</span>
                                            <a href="{{ getCurrentExploreUrl([] , ['ca'=>'']) }}">
                                                <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </li>
                                @endif

                                @if ( $current_manufacturer)
                                    <li class="li-cell-dropdown-item li-cell-auto-width">
                                        <div class="btn-label div-tag-item">
                                            <span>品牌：{{ $current_manufacturer->name }}</span>
                                            <a href="{{ getCurrentExploreUrl([] , ['br'=>'']) }}">
                                                <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </li>
                                @endif

                                @if ( $current_motor)
                                    <li class="li-cell-dropdown-item li-cell-auto-width">
                                        <div class="btn-label div-tag-item">
                                            <span>車型：{{ $current_motor->name }}</span>
                                            <a href="{{ getCurrentExploreUrl([] , ['mt'=>'']) }}">
                                                <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="ct-table-row">
                        <div class="ct-table-cell cell-title width-full search-container">
                            {{--<span class="ct-table-cell-title">排序:</span>--}}
                            <ul class="ul-cell-dropdown-list container cover-bottom-gap-5 arrange-text" style="text-decoration: none;">
                                <li>排名區間:</li>
                                <li>【</li>
                                <li>@if($sort == 'month')近30天@else<a href="{{ getCurrentExploreUrl(['sort'=>'month','type'=>$type ]) }}">近30天</a>@endif</li>
                                <li>@if($sort !== 'month' and $sort !== 'year')近60天@else<a href="{{ getCurrentExploreUrl(['sort'=>'','type'=>$type ]) }}">近60天</a>@endif</li>
                                <li>@if($sort == 'year')近360天@else<a href="{{ getCurrentExploreUrl(['sort'=>'year','type'=>$type ]) }}">近360天</a>@endif</li>
                                <li>】</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($count)
            @if(request()->get('type') == 'brands')
                @include('response.pages.list.ranking.brand-block')
            @else
                @include('response.pages.list.ranking.sell-block')
            @endif
        @else
            @include('response.pages.list.ranking.no-result')
        @endif

    </div>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>
    <script>
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }

        var baseUrl= '{{ request()->url() }}';
        $(document).on('change', '.select-option-redirect', function(){
            window.location = $(this).val();
        });

        function setListMode( value ){
            var d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }

        $("a.a-sub2 > span").click(function(){
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('ul').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('ul').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('ul').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-model,.select-motor-mybike", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = "{{ \URL::route('ranking',['section' => 'mt/']) }}" + '/' + value ;
            }
        });

        $(document).on('change', ".select-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['br' => '%s'])) }}", value );
            }
        });
    </script>
@stop