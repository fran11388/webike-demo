@foreach($products as $product)
    <div class="ct-right-below clearfix ranking-container">
        <div class="product-grid-ranking text-center">
            <p class="lank lank-0{{$loop->iteration}}">{{$loop->iteration}}</p>
            <span class="helper"></span>
        </div>
        <div class="ranking-product-detail">
            <div class="ct-right-below-body">
                <div class="product-item product-item-style-2" data-sku="{{$product->url_rewrite}}">
                    <div class="product-img">
                        @if($product->_numFound > 1)
                            <a class="thumbnail thumbnail-pin search-list-img zoom-image"
                               href="{{ route('parts') }}?relation={{$product->relation_product_id}}" target="_blank">
                                @else
                                    <a class="thumbnail thumbnail-pin search-list-img zoom-image"
                                       href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                                        @endif
                                        {!! lazyImage( ThumborBuildWithCdn($product->getOrigImage(),190,190), $product->full_name) !!}
                                        <span class="helper"></span>
                                    </a>
                    </div>
                    <div class="product-detail">
                        <div class="product-info-search-list">
                            <h3 class="product-name">
                                <a class="dotted-text3" href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                                    {{ $product->name }}
                                </a>
                                <a class="dotted-text3" href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
                                    {{ $product->manufacturer->name }}
                                </a>
                            </h3>


                            <?php
                            $final_price = $product->getFinalPrice($current_customer);
                            $final_point = $product->getFinalPoint($current_customer);
                            $discount_text = '';
                            $discount = round($final_price / $product->price ,2) * 100;
                            if($discount < 90){
                                $discount_text = '('. $discount . '%)';
                            }
                            ?>
                            <label class="price">NT$ {{ number_format($final_price) }}{{$discount_text}}</label>
                            <div class="tags clearfix">
                                @foreach($product->getTags($current_customer) as $tagName => $tagClass)
                                    <span class="{!! $tagClass !!}">{{$tagName}}</span>
                                @endforeach
                            </div>

                        </div>
                        <div class="product-state-search-list">
                            <span class="helper"></span>
                            <div class="vertical-middle">
                                <div class="div-btn-check-detail container product-detail-button">
                                    <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" title="{{$product->full_name}}" target="_blank">
                                        <span>商品詳細</span>
                                    </a>
                                </div>
                                @php
                                    $review_info = $product->getReviewInfo();
                                @endphp

                                <div class="rating col-md-12 ranking-sell-info">
                                    <div class="container icon-star-container text-cetner">
                                        @if($review_info->count > 0)
                                            <span class="icon-star star-{{ str_pad(round($review_info->average), 2,'0' , STR_PAD_LEFT) }} icon-star-element star-fix"></span><span>({{$review_info->count}}件)</span>
                                            <div class="text-cetner">
                                                <a class="name icon-star-text icon-star-element btn-gap-top" href="{{route('review-search',null)}}?sku={{$product->url_rewrite}}">查看商品評論</a>
                                            </div>
                                        @else
                                            <span class="icon-star star-hide icon-star-element star-fix"></span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach