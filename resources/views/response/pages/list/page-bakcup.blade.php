@extends('response.layouts.1column')
@section('style')
    <style type="text/css">
        .ct-right-below-body .product-item .product-name{
            height: 4.5rem;
        }
        .cover-bottom-gap-5{
            margin-bottom: -5px;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box{
            position: absolute;
            display: block;
            background: #fff;
            z-index: 998;
            padding: 15px;
            float: none;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box [class*="col-"]{
            padding-left: 5px;
            padding-right: 5px;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .row{
            margin-left: -5px;
            margin-right: -5px;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left{
            overflow: hidden;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box{
            float:left;
            padding: 2% 2% 0 0;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box.thumbnail-col-2{
            width: 50%;
            height: 83.7px;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box.thumbnail-col-3{
            width: 33.3333%;
            height: 67.5px;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box.thumbnail-col-2:nth-child(2n){
            padding: 2% 0 0 0;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box.thumbnail-col-3:nth-child(3n){
            padding: 2% 0 0 0;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box .thumbnail{
            float:left;
            display: block;
            width:100%;
            height: 100%;
            white-space: nowrap;
            text-align: center;
        }
        .ct-right-below .ct-right-below-body .product-item.product-preview-box .preview-left .img-box .thumbnail img{
            vertical-align: middle;
            display: inline-block;
        }
    </style>
@stop
@section('middle')
    <div class="hidden-lg hidden-md hidden-sm row box">
        <div class="col-xs-6">
            <a class="btn btn-default btn-full" href="#m-menu">篩選/排序 <i class="glyphicon glyphicon-filter"></i></a>
        </div>
        <div class="col-xs-6">
            <div class="pull-right btn-label no-padding-horizontal">
                <a href="javascript:void(0)" onclick="setListMode('product-item-style-2')">
                    <i class="fa fa-th-list menu-icon size-19rem list {{ (isset($_COOKIE['listMode']) and $_COOKIE['listMode'] == 'product-item-style-2') ? 'menu-icon-active' : '' }}" aria-hidden="true"></i>
                </a>
                &nbsp
                <a href="javascript:void(0)" onclick="setListMode('')">
                    <i class="fa fa-th-large menu-icon size-19rem large {{ (!isset($_COOKIE['listMode']) or $_COOKIE['listMode'] != 'product-item-style-2') ? 'menu-icon-active' : '' }}" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <div id="m-menu" class="hidden-xs">
        <div>
            <div class="hidden-lg hidden-md hidden-sm">
                @include('response.pages.list.partials.right-filter')
            </div>
            <aside class="ct-left-search-list ct-left-search-list-main-menu col-xs-12 col-sm-4 col-md-3 col-lg-3">

                <ul class="ul-left-title row">
                    <!--Event-->
                    @include('response.pages.list.partials.event-list')
                    <!--Filter-->
                    @include('response.pages.list.partials.filter-list')
                    <!--Country-->
                    @include('response.pages.list.partials.country-list')
                    <!--Category-->
                    @include('response.pages.list.partials.category-list')
                    <!--/.category-->
                    <!--Brands-->
                    @include('response.pages.list.partials.brand-list')
                    <!--/.brands-->
                    <!--Price Range-->
                    @include('response.pages.list.partials.price-range-list')
                    <!--/.price range-->
                    @if($current_motor)
                        <li class="li-left-title col-md-12 col-sm-12 col-xs-12">
                            <div class="slide-down-btn">
                                <a href="javascript:void(0)" class="a-left-title"><span>{{$current_motor->name}}</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                            </div>
                            <ul class="ul-sub1 row">
                                <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                                    <ul class="ul-sub2 row">
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="{{URL::route('motor-top', $current_motor->url_rewrite)}}" title="車型首頁{{$tail}}"><span>車型首頁</span></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="{{URL::route('motor-service', $current_motor->url_rewrite)}}" title="{{$current_motor->name}}規格總覽{{$tail}}"><span>規格總覽</span></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="{{URL::route('summary', 'mt/' . $current_motor->url_rewrite)}}" title="改裝及用品{{$tail}}"><span>改裝及用品</span></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="{{URL::route('motor-review', 'mt/' . $current_motor->url_rewrite)}}" title="商品評論{{$tail}}"><span>商品評論</span></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="{{URL::route('motor-video',  $current_motor->url_rewrite)}}" title="車友影片{{$tail}}"><span>車友影片</span></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                            <div class="slide-down-btn">
                                                <div class="a-sub2">
                                                    <a href="http://www.webike.tw/motomarket/search/fulltext?q={{$current_motor->name}}" title="新車、中古車{{$tail}}" target="_blank"><span>新車、中古車</span></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <hr>
                        </li>
                    @endif
                    <!--/.country-->
                    <!--Color-->
                    {{--@include('response.pages.list.partials.color-list')--}}
                    <!--./color-->
                    <!--Size-->
                    {{--@include('response.pages.list.partials.size-list')--}}
                    <!--/.size-->
                    <!--Satisfaction-->
                    {{--@include('response.pages.list.partials.satisfaction-list')--}}
                    <!--/satisfaction-->
                    <!--新商品-->
                    {{--@include('response.pages.list.partials.new-lineup-list')--}}
                    <!--/.新商品-->
                    <li class="li-left-title hidden-lg hidden-md hidden-sm col-xs-12">
                        <div class="slide-down-btn">
                            <a href="javascript:void(0)" class="a-left-title" target="_top"><span>每頁顯示</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
                        </div>
                        <ul class="ul-sub1 row">
                            <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
                                <ul class="ul-sub2 row">
                                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                                        <div class="box">
                                            <div class="clearfix content-last-box">
                                                <div class="col-xs-3 btn-label">顯示</div>
                                                <div class="col-xs-9">
                                                    <select class="btn btn-full select-option-redirect">
                                                        <option value="{{ getCurrentExploreUrl(['limit'=>'']) }}" {{ $rows == 40 ? 'selected' : '' }}>
                                                            40
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['limit'=>'100']) }}" {{ $rows == 100 ? 'selected' : '' }}>
                                                            100
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix content-last-box">
                                                <div class="col-xs-3 btn-label">排序</div>
                                                <div class="col-xs-9">
                                                    <select class="sort btn btn-full select-option-redirect">
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'' , 'order'=>'']) }}" {{ $sort == '' ? 'selected' : '' }}>
                                                            人氣商品
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'new', 'order'=>'']) }}" {{ $sort == 'new' ? 'selected' : '' }}>
                                                            新上架排序
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'sales' , 'order' => 'asc']) }}" {{ ($sort == 'sales' && $direction == 'asc') ? 'selected' : '' }}>
                                                            促銷排序
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'price' , 'order' => 'asc']) }}" {{ ($sort == 'price' && $direction == 'asc') ? 'selected' : '' }}>
                                                            價格低排序
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'price', 'order'=>'']) }}" {{ $sort == 'price' && $direction == '' ? 'selected' : '' }}>
                                                            價格高排序
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'manufacturer', 'order'=>'']) }}" {{ $sort == 'manufacturer' ? 'selected' : '' }}>
                                                            品牌名稱
                                                        </option>
                                                        <option value="{{ getCurrentExploreUrl(['sort'=>'ranking', 'order'=>'']) }}" {{ $sort == 'ranking' ? 'selected' : '' }}>
                                                            評價排序
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="ct-left-banner row hidden-xs">
                    @include('response.common.dfp.ad-160x600')
                </div>
            </aside>
        </div>
    </div>
    <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
        <div class="clearfix box">
            <div class="title-main-box ">
                <h1>{!! (Route::currentRouteName() == 'outlet') ? 'Outlet' : '' !!}商品一覽</h1>
            </div>
        </div>
        <div class="container">
            @if(Route::currentRouteName() == 'outlet')
                <div class="box">
                    <img src="{!! assetRemote('image/banner/banner-webike-outlet.png') !!}">
                </div>
            @endif
            <div class="hidden-xs">
                @include('response.pages.list.partials.right-filter')
            </div>
            @include('response.pages.list.partials.right-product-list')
            @include('response.pages.list.partials.pager')
        </div>
    </div>
    {{--@include('response.pages.list.partials.customer-also-view')--}}

    @include('response.common.ad.banner-small')

    <!--Customers Who Viewed This Item Also Viewed -->
    <!-- Customer view -->
        <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('response.common.loading.md')
        </div>
    <!-- Customer view end-->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-advertisement">
        @include('response.common.dfp.ad-728x90')
    </div>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            if ($(window).width() < 767) {
                $('.product-detail-button').hide();
            }else{
                $('.product-detail-button').show();
            }
        });

        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }

        var baseUrl= '{{ request()->url() }}';
        $(document).on('change', '.select-option-redirect', function(){
            window.location = $(this).val();
        });

        function setListMode( value ){
            var d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }

        $("a.a-sub2 > span").click(function(){
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.ct-table-cell').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-model,.select-motor-mybike", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['mt' => '%s'])) }}", value );
            }
        });

        $(document).on('change', ".select-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['br' => '%s'])) }}", value );
            }
        });

        var last_preview = null;
        $('.product-item:not(.product-preview-box)').mouseover(function(){
            if(last_preview !== $(this).attr('data-sku')){
                last_preview = $(this).attr('data-sku');
                getPreviewBox(this);
            }else{
                $('.product-preview-box').show();
            }
        });

        $('.product-preview-box').mouseleave(function(){
            $('.product-preview-box').hide();
        });

        $(document).on('mouseover', ".product-preview-box .preview-left img", function(){
            $(this).closest('.product-preview-box').find('.preview-right img').prop('src', $(this).attr('data-src'));
        });

        function getPreviewBox(_this){
            var product = $(_this).clone();
            var preview = $('.product-preview-box');
            var css_data = {
                height: $(_this).height() - 5,
                width: $(_this).width() * 2,
                top: $(_this).position().top
            };
            preview.css(css_data);
            var product_image_area = product.find('.product-img');
            var product_image_gallery_area = product.find('.product-img-gallery');
            var product_description_area = product.find('.product-info-search-list').find('.product-description');
            $(product_description_area).find('.dotted-text1').removeClass('dotted-text1');
            $(product_description_area).find('.dotted-text').removeClass('dotted-text');
            var product_info = product.find('.product-info-search-list');
            $(product_info).find('.product-description').remove();

            var product_state_area = product.find('.product-state-search-list');
            var preview_right = preview.find('.preview-right');
            var preview_description = preview.find('.preview-description');
            var preview_images_area = preview.find('.preview-left .clearfix');
            $(preview_images_area).html('');
            $(product_image_gallery_area).find('.thumbnail').each(function(key, thumbnail){
                $(preview_images_area).append('<div class="img-box"><a class="thumbnail search-list-img zoom-image" href="javascript:void(0)" target="_blank">' + $(thumbnail).html() + '</a></div>');
            });
            if($(product_image_gallery_area).find('img').length > 8){
                $(preview_images_area).find('.img-box').addClass('thumbnail-col-3');
            }else{
                $(preview_images_area).find('.img-box').addClass('thumbnail-col-2');
            }
            preview_right.html('');
            preview_right.append('<div class="product-img">' + $(product_image_area).html() + '</div>');
            preview_right.append($(product_info).html());

            preview_description.find('.product-info-search-list .product-description').html('');
            preview_description.find('.product-info-search-list .product-description').append($(product_description_area).html());
            preview_description.find('.product-state-search-list').html('');
            preview_description.find('.product-state-search-list').append('<div class="product-state-search-list">' + $(product_state_area).html() + '</div>');
            preview.show();
            $(preview_images_area).find('img').unveil();
            preview.find('.preview-left').css('height', $(preview_right).height());
            preview.css('left', $(_this).position().left - preview.width() + $(_this).width());
        }

    </script>
    <script src="{!! assetRemote('js/basic/select2-size-reset.js') !!}"></script>
    <script>
        $(document.body).on('focus', '.input-search-keyword' ,function(){
            var current_suggest_connection = null;
            var _this = $(this)
            _this.autocomplete({
                minLength: 1,
                source: solr_source_search,
                focus: function( event, ui ) {
                    _this.val( ui.item.label );
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'parts');
//                     $( "#search" ).val( ui.item.label );
                    location.href = ui.item.href;
                    return false;
                },
                change: function(event, ui) {
                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }

                return bag
                    .append( '<a  href="'+ item.href +'">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
            function solr_source_search(request, response){

                var params = {q: request.term ,parts:true,url:window.location.pathname};

                current_suggest_connection = $.ajax({
                    url: "{{ route('api-suggest')}}",
                    method:'GET',
                    data : params,
                    dataType: "json",
                    beforeSend: function( xhr ) {
                        if(current_suggest_connection){
                            current_suggest_connection.abort();
                        }
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        });
    </script>
    <script>
        $(document.body).on('submit', '.input-search-keyword-form' ,function(){
            target_url = $(this).attr('action');
            if(target_url){
                target_url = decodeURIComponent(target_url);

                search_value = $(this).find('input[type=text]').val();

                if(search_value){
                    window.location.href = addParameter(target_url,'q',search_value);
                    return false;
                }

            }
        });
    </script>

@stop