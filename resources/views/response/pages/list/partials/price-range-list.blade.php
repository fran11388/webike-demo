<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title"><span>價格區間</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                @foreach ($price_range as $node)
                    @if ($node->count)
                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                            <div class="slide-down-btn">

                                <div class="a-sub2 {{request()->input('price') == $node->key ? 'active' : ''}}">
                                    <a href="{{getCurrentExploreUrl([ 'price' => $node->key ,'page' => '' ])}}">
                                    <span>
                                        {{-- <input class="checkbox-left-sidebar-li" type="checkbox"> --}}
                                        {{ $node->name }}
                                    </span>
                                        <span class="count">({{ $node->count }})</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
                @php
                    $price_params = explode('-', request()->get('price'));
                @endphp
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 price-search box">
                    <div class="slide-down-btn">
                        <span>$</span>
                        <input id="price_begin" class="price_begin" name="price_begin" type="text" value="{!! (count($price_params) and isset($price_params[0])) ? $price_params[0] : '' !!}">
                        <span> - $</span>
                        <input id="price_end" class="price_end" name="price_end" type="text" value="{!! (count($price_params) and isset($price_params[1])) ? $price_params[1] : '' !!}">
                    </div>
                    <div class="slide-down-btn">
                        <input class="btn btn-danger price-search-button" type="button" value="搜尋">
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</li>