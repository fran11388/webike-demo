<li class="li-left-title col-md-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title"><span>Color</span><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
    </div>
    <ul class="color-list-box container">
        <li class="color-box"><div><div class="red"></div></div></li>
        <li class="color-box"><div><div class="yellow"></div></div></li>
        <li class="color-box"><div><div class="blue"></div></div></li>
        <li class="color-box"><div><div class="pink"></div></div></li>
        <li class="color-box"><div><div class="green"></div></div></li>
        <li class="color-box"><div><div class="light-blue"></div></div></li>
    </ul>
</li>