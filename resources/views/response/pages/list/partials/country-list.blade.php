<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title"><span>進口或國產品</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                @foreach ($countries as $node)
                    @if ($node->count)
                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                            <a class="a-sub2 {{request()->input('country') == $node->key ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'country' => $node->key ,'page' => '' ])}}">
                                <span>
                                    {{-- <input class="checkbox-left-sidebar-li" type="checkbox"> --}}
                                    {{ $node->name }}
                                </span>
                                <span class="count">({{ $node->count }})</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>
    </ul>
</li>