<div class="row ct-right-below">
    <div class="container">
        <div class="row ct-right-below-header hidden-xs">
            <ul class="ul-header">
                <li class="li-header-right">
                    <ul>
                        <li class="col-md-4 col-sm-12 col-xs-12">
                            <div>
                                <span class="search-option-title size-10rem">
                                    全部共 {{ number_format($pager->total()) }}項商品。
                                </span>
                                <span class="search-option-title option-col-30 size-10rem">頁次 {{ $page }}/{{ $pager->lastPage() }}</span>
                                <ul class="page-change">
                                    <li class="prev-page {{ $page == 1 ? 'active':'' }}">
                                        <a class="btn btn-default" href="{{ $pager->onFirstPage() ? 'javascript:void(0)': $pager->previousPageUrl() }}">
                                            <i class="fa fa-caret-left" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="next-page {{ $page == $pager->lastPage() ? 'active':'' }}">
                                        <a class="btn btn-default" href="{{ $page == $pager->lastPage() ? 'javascript:void(0)':$pager->nextPageUrl() }}">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-4 col-xs-12 searchlist-select-dropdown">
                            <div>
                                <span class="search-option-title size-10rem">每頁顯示</span>
                                <select class="select2 select-option-redirect">
                                    <option value="{{ getCurrentExploreUrl(['limit'=>'']) }}" {{ $rows == 40 ? 'selected' : '' }}>
                                        40
                                    </option>
                                    <option value="{{ getCurrentExploreUrl(['limit'=>'100']) }}" {{ $rows == 100 ? 'selected' : '' }}>
                                        100
                                    </option>
                                </select>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-4 col-xs-12 searchlist-select-dropdown">
                            <div>
                                <span class="search-option-title size-10rem">排序方式</span>
                                <select class="sort select2 select-option-redirect">
                                    @if(\Route::currentRouteName() == 'outlet')
                                        <option value="{{ getCurrentExploreUrl(['sort'=>'', 'order'=>'']) }}" {{ $sort == '' ? 'selected' : '' }}>
                                            新上架排序
                                        </option>
                                        <option value="{{ getCurrentExploreUrl(['sort'=>'visits' , 'order'=>'']) }}" {{ $sort == 'visits' ? 'selected' : '' }}>
                                            人氣商品
                                        </option>

                                    @else
                                        <option value="{{ getCurrentExploreUrl(['sort'=>'' , 'order'=>'']) }}" {{ $sort == '' ? 'selected' : '' }}>
                                            人氣商品
                                        </option>
                                        <option value="{{ getCurrentExploreUrl(['sort'=>'new', 'order'=>'']) }}" {{ $sort == 'new' ? 'selected' : '' }}>
                                            新上架排序
                                        </option>
                                    @endif


                                    <option value="{{ getCurrentExploreUrl(['sort'=>'sales' , 'order' => 'asc']) }}" {{ ($sort == 'sales' && $direction == 'asc') ? 'selected' : '' }}>
                                        促銷排序
                                    </option>
                                    <option value="{{ getCurrentExploreUrl(['sort'=>'price' , 'order' => 'asc']) }}" {{ ($sort == 'price' && $direction == 'asc') ? 'selected' : '' }}>
                                        價格低排序
                                    </option>
                                    <option value="{{ getCurrentExploreUrl(['sort'=>'price', 'order'=>'']) }}" {{ $sort == 'price' && $direction == '' ? 'selected' : '' }}>
                                        價格高排序
                                    </option>
                                    <option value="{{ getCurrentExploreUrl(['sort'=>'manufacturer', 'order'=>'']) }}" {{ $sort == 'manufacturer' ? 'selected' : '' }}>
                                        品牌名稱
                                    </option>
                                    <option value="{{ getCurrentExploreUrl(['sort'=>'ranking', 'order'=>'']) }}" {{ $sort == 'ranking' ? 'selected' : '' }}>
                                        評價排序
                                    </option>
                                </select>
                            </div>
                        </li>
                        <li class="col-md-2 col-sm-4 col-xs-12 li-search-list-block-list-icon">
                            <div class="pull-right btn-label no-padding">
                                <a href="javascript:void(0)" onclick="setListMode('product-item-style-2')" class="list-mode">
                                    <i class="fa fa-th-list menu-icon size-19rem list {{ (isset($_COOKIE['listMode']) and $_COOKIE['listMode'] == 'product-item-style-2') ? 'menu-icon-active' : '' }}"></i>
                                </a>
                                &nbsp
                                <a href="javascript:void(0)" onclick="setListMode('')" class="common-mode">
                                    <i class="fa fa-th-large menu-icon size-19rem large {{ (!isset($_COOKIE['listMode']) or $_COOKIE['listMode'] != 'product-item-style-2') ? 'menu-icon-active' : '' }}"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row ct-right-below-body">
            @foreach ($products as $product)
                @include('response.common.product.e')
            @endforeach
            @php
                $product = $products->first();
            @endphp
            <div class="product-preview-mode product-item {{(isset($_COOKIE['listMode']) and $_COOKIE['listMode'] == 'product-item-style-2') ? '' : 'product-preview-box'}}" style="display: none;">
                <div class="row">
                    <div class="preview-left">
                        <div class="clearfix">

                        </div>
                    </div>
                    <div class="preview-right">

                    </div>
                </div>
                <div class="clearfix preview-description">
                    <div class="product-info-search-list">
                        <ul class="product-description"></ul>
                    </div>
                    <div class="product-state-search-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>