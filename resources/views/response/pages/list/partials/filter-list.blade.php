<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title"><span>商品篩選</span><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                    <a class="a-sub2 {{request()->get('fn') ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'fn' => '1' ,'page' => '' ])}}">
                        <span>
                            只顯示新商品
                        </span>
                    </a>
                </li>
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                    <a class="a-sub2 {{request()->get('in_stock') ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'in_stock' => '1' ,'page' => '' ])}}">
                        <span>
                            只顯示庫存商品
                        </span>
                    </a>
                </li>
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                    <a class="a-sub2 {{(!request()->get('fn') and !request()->get('in_stock')) ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'fn' => '' , 'in_stock' => '' ,'page' => '' ])}}">
                        <span>
                            全部商品
                        </span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>