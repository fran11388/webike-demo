<li class="li-left-title col-md-12 col-sm-12 col-xs-12">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title">
            <span>商品品牌</span>
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
            <ul class="ul-sub2 row">
                @php
                    $manufacturer_max_count = 5;
                @endphp
                @if($current_manufacturer)
                    @php
                        $selected_manufacturer = $manufacturer->first(function($br) use($current_manufacturer){
                            return $current_manufacturer->url_rewrite == $br->url_rewrite;
                        });
                    @endphp
                    @if($selected_manufacturer)
                        <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                            <a class="a-sub2 active" href="{{getCurrentExploreUrl(['page' => ''] ,['br'=> $selected_manufacturer->url_rewrite ])}}" title="{{$selected_manufacturer->name . $tail}}">
                                <span>
                                    {{ $selected_manufacturer->name }}
                                </span>
                                <span class="count">({{ $selected_manufacturer->count }})</span>
                            </a>
                        </li>
                    @endif
                @endif
                @foreach ($manufacturer as $key => $node)
                    @if($current_manufacturer and $current_manufacturer->url_rewrite == $node->url_rewrite)
                        @continue
                    @endif
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 {{$key >= $manufacturer_max_count ? 'hide' : ''}}">
                        <a class="a-sub2" href="{{getCurrentExploreUrl(['page' => ''] ,['br'=> $node->url_rewrite ])}}" title="{{$node->name . $tail}}">
                            <span>
                                {{-- <input class="checkbox-left-sidebar-li" type="checkbox"> --}}
                                {{ $node->name }}
                            </span>
                            <span class="count">({{ $node->count }})</span>
                        </a>
                    </li>
                @endforeach
                <li class="li-sub2 col-md-12 col-sm-12 col-xs-12">
                    <div class="a-sub2">
                        <select class="select2 select-manufacturer btn-label width-full">
                            <option value="">品牌搜尋</option>
                            @foreach ($manufacturer as $key => $node)
                                <option value="{!! $node->url_rewrite !!}">{{ $node->name }}({{ $node->count }})</option>
                            @endforeach
                        </select>
                    </div>
                </li>
                @if(count($manufacturer) > $manufacturer_max_count)
                    <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 toggle-partner-switch">
                        <div class="a-sub2 text-right" title="{{$node->name . $tail}}" onclick="togglePartner(this, 6, 10);">
                            <span class="font-color-blue">>> 查看更多</span>
                        </div>
                    </li>
                @endif
            </ul>
        </li>
    </ul>
</li>
