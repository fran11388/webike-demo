@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/search/product-preview-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('css/pages/search/double-search.css') }}">
    <style>
        .ct-right-below-body .product-item-style-2 .product-detail .product-info-search-list{
            padding-left: 20px !important;
        }
        @if(isset($_COOKIE['listMode']) and $_COOKIE['listMode'] == 'product-item-style-2')
            .ct-right-below .ct-right-below-body .product-item .product-img .search-list-img{
            height:auto;
        }
        @endif
        .product-detail .product-state-search-list .rating{
            height:19px !important;
        }
    </style>
@stop
@section('middle')
    <div class="ct-right-search-list">
        <div class="clearfix box">
                <h3 class="double-search-title">沒有找到與"<span>{{$keyword}}</span>"相關的結果</h3>
        </div>
        <div >
            <a href="{{URL::route('mitumori')}}" title="未登錄商品查詢購買系統{{$tail}}" class="btn-gap-right">
                <img src="{{ assetRemote('image/banner/banner-mitumori-small.png') }}" alt="未登錄商品查詢購買系統">
            </a>
            <a href="{{URL::route('genuineparts')}}" title="正廠零件查詢購買系統{{$tail}}">
                <img src="{{ assetRemote('image/oempart/banner01.jpg') }}" alt="正廠零件查詢購買系統">
            </a>
        </div>
        <div class="container">
            @foreach ($products_collection as $key_word => $collection)
                <div class="double-search-first row">
                    <h2>"<span>{{$key_word}}</span>"</h2>
                    <a href="{{$collection->url}}">查看所有<span>{{$collection->count}}</span>個結果</a>
                </div>
                @include('response.pages.list.partials.double-search-product-list')
            @endforeach


        </div>
    </div>
    {{--@include('response.pages.list.partials.customer-also-view')--}}

    @include('response.common.ad.banner-small')

    <!--Customers Who Viewed This Item Also Viewed -->
    <!-- Customer view -->
        <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('response.common.loading.md')
        </div>
    <!-- Customer view end-->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-advertisement">
        @include('response.common.dfp.ad-728x90')
    </div>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            if ($(window).width() < 767) {
                $('.product-detail-button').hide();
            }else{
                $('.product-detail-button').show();
            }
        });

        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }

        $('.list-mode').click(function(){
            $('.ct-right-below-body .product-preview-mode').removeClass('product-preview-box');
            $('.ct-right-below .ct-right-below-body .product-item .product-img .search-list-img').css('height','auto');
        });

        $('.common-mode').click(function(){
            $('.ct-right-below-body .product-preview-mode').addClass('product-preview-box');
            $('.ct-right-below .ct-right-below-body .product-item .product-img .search-list-img').css('height','190px');
        });

        var baseUrl= '{{ request()->url() }}';
        $(document).on('change', '.select-option-redirect', function(){
            window.location = $(this).val();
        });

        function setListMode( value ){
            var d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }

        $("a.a-sub2 > span").click(function(){
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.ct-table-cell').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-model,.select-motor-mybike", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['mt' => '%s'])) }}", value );
            }
        });

        $(document).on('change', ".select-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            if (value){
                window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['br' => '%s'])) }}", value );
            }
        });



    </script>
    <script src="{!! assetRemote('js/pages/searchList/product-preview-box.js') !!}"></script>
    <script src="{!! assetRemote('js/basic/select2-size-reset.js') !!}"></script>
    <script>
        $(document.body).on('focus', '.input-search-keyword' ,function(){
            var current_suggest_connection = null;
            var _this = $(this)
            _this.autocomplete({
                minLength: 1,
                source: solr_source_search,
                focus: function( event, ui ) {
                    _this.val( ui.item.label );
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'parts');
//                     $( "#search" ).val( ui.item.label );
                    location.href = ui.item.href;
                    return false;
                },
                change: function(event, ui) {
                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }

                return bag
                    .append( '<a  href="'+ item.href +'">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
            function solr_source_search(request, response){

                var params = {q: request.term ,parts:true,url:window.location.pathname};

                current_suggest_connection = $.ajax({
                    url: "{{ route('api-suggest')}}",
                    method:'GET',
                    data : params,
                    dataType: "json",
                    beforeSend: function( xhr ) {
                        if(current_suggest_connection){
                            current_suggest_connection.abort();
                        }
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        });
    </script>
    <script>
        $(document.body).on('submit', '.input-search-keyword-form' ,function(){
            target_url = $(this).attr('action');
            if(target_url){
                target_url = decodeURIComponent(target_url);

                search_value = $(this).find('input[type=text]').val();

                if(search_value){
                    window.location.href = addParameter(target_url,'q',search_value);
                    return false;
                }

            }
        });
    </script>
    <script>
        $(document).on('click', '.price-search-button',function(){
            var params_count = "{!! count(request()->except(['page', 'price', 'motor', 'category', 'manufacturer'])) !!}";
            /* if($('#mm-price_begin').val() && $('#mm-price_end').val()){
               price_begin = $('#mm-price_begin').val();
               price_end = $('#mm-price_end').val();
            }*/
            var price_begin = $("#price_begin").val();
            var price_end = $("#price_end").val();
            if(price_begin && price_end){
                var url = "{{getCurrentExploreUrl([ 'price' => null ,'page' => '' ])}}";
                if(parseInt(params_count) > 0){
                    url = url + '&price=' + price_begin + '-' + price_end;
                }else {
                    url = url + '?price=' + price_begin + '-' + price_end;
                }
                window.location.href = url;
            }else{
               alert('請輸入搜尋金額');
            }

        });
    </script>

@stop