@extends('response.layouts.2columns')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}">
@stop
@section('left')
    <form>
        <textarea name="skus" placeholder="一行一SKU" rows="15">{{request()->input('skus')}}</textarea>
        <input type="submit" value="查詢">
    </form>
@stop
@section('right')
    <div class="title-main-box">
        <h2>商品檢查區</h2>
    </div>


    <div class="history-table-container">
        <ul class="history-info-table history-table-adjust">
            <li class="history-table-title">
                <ul>
                    <li class="col-md-4 col-sm-4 col-xs-4"><span>商品</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span>定價</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span>一般價</span></li>
                    <li class="col-md-1 col-sm-1 col-xs-1"><span>一般折扣</span></li>
                    <li class="col-md-2 col-sm-2 col-xs-2"><span>經銷價</span></li>
                    <li class="col-md-1 col-sm-1 col-xs-1"><span>經銷折扣</span></li>
                </ul>
            </li>
            <li class="history-table-content">
                @if(isset($products))
                @foreach($products as $product)
                    <ul>
                        <li class="col-md-4 col-sm-4 col-xs-4 history-multi-content-estimate">
                            <div class="history-content-estimate-block">
                                <div class="history-content-img box-fix-with">
                                    <a class="zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}">
                                        <img src="{{ $product->getThumbnail() }}" alt="">
                                    </a>
                                </div>
                                <div class="history-content-estimate col-md-10">
                                    <h2>{{ $product->getManufacturerName() }} ： {{ $product->name }}</h2>
                                    <span>SKU：{{ $product->url_rewrite }}</span><br/>
                                    <span>商品編號：{{ $product->model_number }}</span><br/>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-2 col-sm-2 col-xs-2">
                            <span class="font-16px">NT${{ number_format($product->price) }}</span>
                        </li>
                        <li class="col-md-2 col-sm-2 col-xs-2">
                            <span class="font-16px">NT${{ number_format($product->getNormalPrice()) }}</span>
                        </li>
                        <li class="col-md-1 col-sm-1 col-xs-1">
                            <span class="font-16px">{{ number_format( $product->getNormalPrice() / $product->price ,2)}}</span>
                        </li>
                        <li class="col-md-2 col-sm-2 col-xs-2">
                            <?php
                                $dealer = \Everglory\Models\Customer::find(561);
                            ?>
                            <span class="font-16px">NT${{ number_format($product->getFinalPrice($dealer)) }}</span>
                        </li>
                        <li class="col-md-1 col-sm-1 col-xs-1">
                            <span class="font-16px">{{ number_format( $product->getFinalPrice($dealer) / $product->price ,2)}}</span>
                        </li>
                    </ul>
                @endforeach
                @endif
            </li>
        </ul>
    </div>



@stop
@section('script')
@stop