@extends('response.layouts.1column-simplify')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/login.css') !!}">
@stop
@section('page-title')
    {{--<h2>新會員申請</h2>--}}
@stop
@section('middle')
    <div class="ct-container-login-page box-fix-column">
        <div class="form-login-right box-fix-with">
            <h1>登入Webike</h1>
            <form method="POST" action="{{ route('login') }}">
                <input type="hidden" name="remember" value="1">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="list-style-type: none;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('pointMessage'))
                    <div class="alert alert-danger">
                        <ul style="list-style-type: none;">
                            <li>{{ session()->get('pointMessage') }}</li>
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <label>
                    <input required="" type="text" class="form-control" name="email" placeholder="電子信箱" value="{{ old('email') }}">
                </label>
                <label>
                    <input required="" type="password" class="form-control" name="password" placeholder="密碼">
                </label>
                <div class="form-group">
                    <button type="submit" class=" btn btn-default btn-warning border-radius-2 btn-signin">登入</button>
                </div>
                <div>
                    <a href="{{ route('login-facebook') }}" class="btn btn-default btn-primary border-radius-2 btn-login-with-face btn-full">Facebook 登入</a>
                </div>
                <div class="form-group form-unable-sign">
                    <a href="{{route('customer-fixer')}}">無法登入</a>
                    |
                    <a href="{{route('customer-forgot-password')}}">忘記密碼</a>
                </div>
                <hr>
                <h2>新會員募集</h2>
                <div class="form-group">
                    <button type="button" onclick="location.href='{{ route('customer-account-create') }}';" class="btn btn-danger border-radius-2 bnt-fast-registration">新會員募集</button>
                </div>
                <h2>加入會員，即贈100元禮券！</h2>
            </form>
        </div>
        <div class="form-image-left box-fix-auto visible-md visible-lg">
            <img src="{!! $background_image !!}" alt="會員登入{{$tail}}">
        </div>
    </div>
@stop

@section('script')

@stop
