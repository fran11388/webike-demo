@extends('response.layouts.1column-login')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/login.css') !!}">
@stop
@section('middle')
    <div class="ct-container-login-page box-fix-column">
        <div class="form-login-right box-fix-with">
            <div class="container-login-title">
                <img src="http://img.webike.tw/assets/images/user_upload/collection/Webikelogin_logo.png">
            </div>
            <form method="POST" action="{{ route('login') }}">
                <input type="hidden" name="remember" value="1">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul style="list-style-type: none;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('pointMessage'))
                    <div class="alert alert-danger">
                        <ul style="list-style-type: none;">
                            <li>{{ session()->get('pointMessage') }}</li>
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <label>
                    <input required="" type="text" class="form-control" name="email" placeholder="請輸入E-Mail" value="{{ old('email') }}">
                </label>
                <label>
                    <input required="" type="password" class="form-control" name="password" placeholder="請輸入密碼">
                </label>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger btn-signin" onclick="ga('send', 'event', 'account', 'login', 'normal');">登入</button>
                </div>
                <div>
                    <a href="{{ route('login-facebook') }}" class="btn btn-facebook btn-login-with-face btn-full" onclick="ga('send', 'event', 'account', 'login', 'facebook');">Facebook 登入</a>
                </div>
                <div class="form-group form-unable-sign">
                    <a href="{{route('customer-fixer')}}">無法登入</a>
                    |
                    <a href="{{route('customer-forgot-password')}}">忘記密碼</a>
                </div>
                <hr>
                <h2>新會員募集</h2>
                <div class="form-group">
                    <button type="button" class="btn btn-sub btn-fast-registration" onclick="location.href='{{ route('customer-account-create') }}';">立即註冊</button>
                </div>
                <h2>加入會員，即贈100元禮券！</h2>
            </form>
        </div>
    </div>
@stop

@section('script')

@stop