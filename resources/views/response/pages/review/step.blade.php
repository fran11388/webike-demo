@extends('response.layouts.1column')
@section('script')
	<link rel="stylesheet" type="text/css" href="{{assetRemote('/css/pages/intructions.css')}}">
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="container-intruction-detail">
	        <ul class="ul-membership text-center">
                <li>
                    <div><img src="https://img.webike.tw/assets/images/review/1.png" alt="簡單3步驟，現金點數輕鬆拿！"></div>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>STEP1 拍照</h2></div>
                	<span>
                		當您收到商品後請您拍下您覺得最能表達商品的相片，不論是開箱照、使用的時候或是安裝完成的照片，形式不拘，讓車友們也可以參考（請拍攝商品照片，若使用我們商品頁面提供的照片或網路撈取的圖片將不予採用）。
                	</span><br><br>
                    <div class="text-center"><img src="https://img.webike.tw/assets/images/review/2.jpg"></div>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>STEP2 選擇商品</h2></div>
                	<span>
                		當您拍完照片，準備撰寫評論的時候，您可以用以下方式進到評論撰寫頁面。<br>
                        1.「會員中心」→「撰寫評論記錄」中查看還有多少評論尚未撰寫
                	</span><br>
                    <div class="text-center"><img src="{{assetRemote('image/review/review_3.jpg')}}"></div><br>
                    <span>
                        進到連結頁面之後，可以看到您購買過的商品、時間、訂單編號等資訊，並且會提示您是否已經撰寫過，如需撰寫的話請點選「點我寫履歷」的連結即可進到撰寫頁面，並可以開始撰寫商品評論。<br><br>
                        2.由各商品頁的「撰寫評論連結」直接到您曾經買過的商品頁面，點擊頁面裡面的投稿按鈕，也可以連結到撰寫商品評論的頁面。
                    </span><br>
                    <div class="text-center"><img src="{{assetRemote('image/review/4.png')}}"></div><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title"><h2>STEP3 撰寫上傳</h2></div>
                	<span>
                		選擇完您要撰寫的商品後，就會進入到投稿頁面了，並請填妥商品相關資訊，如優缺點、安裝車型、主題、評價、照片、評論內容等等資訊、再送出投稿即可!<br>
                        在您送出評論後的7天內我們會進行審核，您的評論經採用之後會自動刊登於「商品評論」的頁面，提供給有興趣的車友作為參考。
                	</span><br>
                    <div class="text-center"><img src="{{assetRemote('image/review/review_5.png')}}"></div><br>
                    <div class="text-center"><img src="{{assetRemote('image/review/6.png')}}"></div><br>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	 <div class="title"><h2>※商品評論說明與規定</h2></div>
                	<span>
                		撰寫評論：當您收到商品使用過之後，歡迎在該商品評論區發表您的使用心得以及對該商品的評論，若經採用後我們會提供「回饋點數」給您。<br>
                        評論使用說明：<br>
                        評論審核作業流程：<br>
                        1.當您完成商品評論之後 ，請點選[送出]系統會自動將您的評論回傳到本公司服務中心。<br>
                        2.我們於七個工作天內審核您的評論，您的評論經採用之後會自動刊登於「商品評論」的頁面，提供給有興趣的車友作為參考。<br>
                        3.評論一經採用之後，系統自動將點數匯入您的個人帳戶中，您可以使用該點數作為購物折抵。詳情請參閱 ﹝現金點數說明﹞<br>
                        評論審核規則：<br>
                        1.我們誠摯歡迎您留下評論，但本站對於會員所撰寫的評論保有審核以及決定刊登的權利；若您的內容有包含針對性、攻擊性、不雅的字眼，本站不予刊登。<br>
                        2.撰寫字數限制為100個字元以上，有照片為佳（建議不要使用我們商品頁面提供的照片，否則會影響到您獲得的點數）。<br>
                        3.</span><span style="text-decoration:underline;">審核標準會依據您提供的資訊多寡與內容豐富性進行給點，例如：若是您的生動內容有超過100個字元，並附有照片，</span><span class="font-color-red" style="text-decoration:underline;">審核通過我們會給予50點點數</span><span style="text-decoration:underline;">，若沒有提供您所拍攝的照片將無法送出評論。假如有同時撰寫多篇評論，評論的文字內容也請勿過度重複，否則也將會影響到您獲得的點數多寡。</span>點數給予的審核權由本站決定，而我們不定期也會舉辦點數加倍活動，屆時以活動公告為準。<br>
                        4.您所撰寫的評論為您個人使用的心得，屬於個人言論自由，不代表本公司立場，審核人員無權也不會修改您的評論內容。<br>
                        5.您所撰寫的內容可能經由他人轉載而出現在其他網站平台、討論區，因著作權歸屬為您而非本站，本站無權限制，因此您留下的評論一經本站刊登之後即為公開資訊，在此特別聲明。<br>
                        6.您上傳的照片視同您同意本站進行有限度的使用，如：站內連結、宣傳素材、facebook分享…等相關活動。<br>
                        7.您提供的商品評論及照片，原則上我們不會進行移除，若您需要移除請隨時告知我們。<br>
                        評論加倍活動：除了撰寫一般評論之外，我們每個月會挑選不同的商品分類，只要您寫的評論是對應在該商品分類之下，我們會從原先提供的50點加倍到100點點數回饋給您，詳情可以至＂會員好康＂中查看。
                	</span><br>
	            </li>
	        </ul><br><br>
            <ul class="ul-membership">
                <li>
                    <div class="col-md-3 col-sm-12 col-xs-12"></div>
                    <div><a class="btn btn-default col-md-2 col-sm-12 col-xs-12" href="{{URL::route('review')}}">回評論首頁</a></div>
                    <div class="col-md-2 col-sm-12 col-xs-12"></div>
                    <div><a class="btn btn-info col-md-2 col-sm-12 col-xs-12" href="{{URL::route('customer-history-review')}}">開始撰寫評論</a></div>
                    <div class="col-md-3 col-sm-12 col-xs-12"></div>
                </li>
            </ul>
		</div>
	</div>
@stop
