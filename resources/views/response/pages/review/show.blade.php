@extends('response.layouts.1column')
@section('style')

@endsection
@section('middle')
    <div class="box-content-brand-top-page">

        <aside class="ct-left-search-list  col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div id="m-menu" class="hidden-xs">
                <div>
            @include('response.pages.review.partials.side-top-banner')

            @include('response.pages.review.partials.campaign')

            @include('response.pages.review.partials.not-writing')

            @include('response.pages.review.partials.popular')


            @include('response.common.dfp.ad-160x600')
                <div>
            <div>
        </aside>


        <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
            <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                <div class="row">
                    <div class="title-main-box">
                        <h1>【{{ $review->product_manufacturer }}】{{ $review->product_name }}</h1>
                    </div>

                    <div class="box-info-motor-article form-group">
                        <ul class="info-motor-article">
                            <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <ul>
                                    <li>
                                        <div class="clearfix">
                                            @if($motor)
                                                <span>
                                                    安裝車型：<a href="{!! getMtUrl(['motor_url_rewrite' => $motor->url_rewrite]) !!}" title="{{ $motor->manufacturer->name . ' ' . $motor->name . ($motor->synonym ? '(' . $motor->synonym . ')' : '' ) . $tail }}">{{ $motor->manufacturer->name . ' ' . $motor->name . ($motor->synonym ? '(' . $motor->synonym . ')' : '' ) }}</a>
                                                </span>
                                            @else
                                                <span>無</span>
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <div class="clearfix">
                                            <span>評價：</span>
                                            <span class="icon-star star-0{{ $review->ranking }}"></span>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <?php $ratings = unserialize($review->rating);  ?>
                            <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <ul>
                                    @if($ratings)
                                        @foreach ($ratings['pros'] as $rating)
                                            @if ($rating)
                                                <li>
                                                    <i class="icon-0">O</i>
                                                    {{ $rating }}
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </li>
                            <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <ul>
                                    @if($ratings)
                                        @foreach ($ratings['cons'] as $rating)
                                            @if ($rating)
                                                <li>
                                                    <i class="icon-x">X</i>
                                                    {{ $rating }}
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif

                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="box-fix-column row block box-info-motor-article2">
                        <div class="col-xs-12 col-sm-12 col-md-4 box">
                            <div class="box text-center">
                                <figure class="zoom-image thumb-img thumb-box-border">
                                    <?php
                                    $src = '';
                                    if(starts_with($review->photo_key,'http')){
                                        $src = $review->photo_key;
                                    }else{
                                        $src = $review->photo_key ? '//img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                        $src = fixHttpText($src);
                                    }
                                    ?>
                                    <img src="{{ cdnTransform($src) }}" alt="{{ $review->product_name . $tail }}">
                                </figure>
                            </div>
                            <ul class="row">
                                @if($review->product)
                                    <li class="col-xs-3 col-sm-2 col-md-4">
                                        <a href="{!! getBrUrl(['manufacturer_url_rewrite' => $review->product_manufacturer_code]) !!}">
                                            <img src="{{$review->product->manufacturer->image}}" alt="{{$review->product_manufacturer . $tail}}">
                                        </a>
                                    </li>
                                    <li class="col-xs-9 col-sm-10 col-md-8">
                                        <a class="size-10rem" href="{{ route('product-detail', $review->product_sku) }}" target="_blank">{{$review->product_manufacturer . '：' . $review->product_name}}</a>
                                    </li>
                                @else
                                    <li class="col-xs-12 col-sm-12 col-md-12">
                                        <span class="size-10rem">{{$review->product_manufacturer . '：' . $review->product_name}}</span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <h2 class="block"><strong>{{ $review->title }}</strong></h2>
                            <ul class="row box">
                                <li class="col-xs-4 col-sm-6 col-md-6">投稿者：{{ $review->nick_name }}</li>
                                <li class="col-xs-8 col-sm-6 col-md-6 text-right">投稿日期：{{ $review->created_at }}</li>
                            </ul>
                            <span class="text-content">
                                {!! nl2br(e(strip_tags($review->content))) !!}
                            </span>

                        </div>
                    </div>

                    @include('response.pages.review.partials.show-review-comments')

                </div>
            </div>


            @if(count($reviews))
                <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                    <div class="row">
                        <div class="title-main-box">
                            <h2>其他評論</h2>
                        </div>
                        @include('response.pages.review.partials.list')
                    </div>
                </div>
            @endif

            @include('response.common.ad.banner-small')

            <!-- Customer view -->
            <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('response.common.loading.md')
            </div>
            <!-- Customer view end-->

            <div class="box-motor-data row-advertisement">
                @include('response.common.dfp.ad-728x90')
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Product",
            "image": "{{$review->photo_key ? 'http://img.webike.tw/review/'.$review->photo_key : $review->product_thumbnail}}",
            "name": "{{$review->product_name}}",
            "review": {
                "@type": "Review",
                    "reviewRating": {
                        "@type": "Rating",
                        "ratingValue": "{{ $review->ranking }}"
                    },
                "name": "{{ $review->title }}",
                "author": {
                    "@type": "Person",
                    "name": "{{ $review->nick_name }}"
            },
            "datePublished": "{{ date('Y-m-d', strtotime($review->created_at)) }}",
                "reviewBody": "{!! nl2br(e($review->content)) !!}",
                "publisher": {
                    "@type": "Organization",
                    "name": "「Webike-摩托百貨」"
                }
            }
        }
    </script>
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script>
        $('form').submit(function(){

            if(typeof $(this).find('textarea') !== "undefined" && $(this).find('textarea').val().trim() == ''){
                swal(
                        '錯誤',
                        '請輸入回應內容!',
                        'error'
                )
                return false;
            };
        });
    </script>
@stop
