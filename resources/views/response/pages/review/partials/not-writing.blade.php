<div class="box-page-group row box-page-group-category">
    <div class="title-box-page-group">
        <h3>購買商品未撰寫評論({{count($notWriteReview)}})</h3>
    </div>
    <div class="ct-box-page-group">
        <ul class="ul-menu-ct-box-page-group ul-menu-review-top">
            @foreach($notWriteReview as $item)
                <li>
                    <a href="{{route('review-create',$item->mainProduct ? $item->mainProduct->sku : $item->sku)}}"><span>{{$item->manufacturer_name}}：{{$item->mainProduct ? $item->mainProduct->name : $item->product_name}}{{$item->id}}</span></a>
                </li>
            @endforeach
        </ul>
    </div>
</div>