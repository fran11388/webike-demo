<ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ct-review-comment">
    @foreach ($reviews as $review)
        <li class="box-fix-column">
            <div class="box-fix-with left">
                <a href="{{ route('review-show', $review->id) }}">
                    <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
                        <?php
                        $src = '';
                        if (starts_with($review->photo_key, 'http'))
                            $src = $review->photo_key;
                        else
                            $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                        ?>
                        <img src="{{ str_replace('http:','',$src) }}" alt="{{ $review->product_name }}">
                    </figure>
                </a>
            </div>
            <div class="box-fix-auto right">
                <div class="title-review-comment">
                    <a href="{{ route('review-show', $review->id) }}"
                       class="title dotted-text2">{{ $review->product_name }}</a></div>
                <ul class="ul-info-review-search">
                    <li class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <span>投稿日：</span>{{ date("Y/m/d", strtotime($review->created_at)) }}
                    </li>
                    <li class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <span>投稿人:</span> {{ $review->nick_name }}
                    </li>
                    {{--<li class="col-xs-12 col-sm-8 col-md-8 col-lg-8">--}}
                    {{--<span>安裝車型：</span>{{ ($review->motor_name)  ? ( $review->motor_manufacturer . ' ' . $review->motor_name) : '無' }}--}}
                    {{--</li>--}}
                    <li class="col-xs-12 col-sm-4 col-md-4 col-lg-4"><span
                                class="icon-star star-0{{  $review->ranking }}"></span></li>
                    <li class="col-md-12 col-sm-12 dotted-text3 hidden-xs">
                        {!! nl2br(e($review->content)) !!}
                    </li>
                </ul>
            </div>
        </li>
    @endforeach
</ul>