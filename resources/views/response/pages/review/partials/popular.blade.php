<div class="box-page-group row box-page-group-category">
    <div class="title-box-page-group">
        <h3>近期最多人撰寫的商品</h3>
    </div>
    <ul class="box-content-popularity">
        @foreach ($mostReviewProducts as $product)
            <li>
                <div class="container-box-content-popularity">
                    <p class="lank">{{ $loop->iteration }}</p>
                    <a class="zoom-image" href="{{ route('review-search' , [ null ,'sku'=>$product->url_rewrite ]) }}">
                        {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                    </a>
                </div>
                <a href="{{ route('review-search' , [ null ,'sku'=>$product->url_rewrite ]) }}" class="title-product-item title-review-top dotted-text2">【{{ $product->manufacturer->name }}】{{ $product->name }}</a>
                <span class="access-number">{{ $product->review_count }}則評論</span>
            </li>
        @endforeach
    </ul>
</div>