<div class="title-main-box">
    <h2>車友回應</h2>
</div>

@if(count($review->comments))
<div class="col-xs-12 col-sm-12 col-md-12 thumb-box-border box-review-comment box">
    <ul>
        @foreach ($review->comments as $comment)
            <li>
                <h3 class="form-group">【回應人】{{ $comment->nick_name }}</h3>
                <div class="box">{{ $comment->content }}</div>
                <div class="text-right">
                    於{{ $comment->created_at }}。
                </div>
            </li>
        @endforeach
    </ul>
</div>
@endif

<div class="col-xs-12 col-sm-12 col-md-12 box-review-comment">
    <div class="block row">
        @if ( $current_customer )
            @if ($current_customer->role_id == \Everglory\Constants\CustomerRole::REGISTERED)
                <div class="col-xs-12 col-sm-8 col-md-8">
                    <h3 class="btn-label">回應評論請先完成「完整註冊」。</h3>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <a class="btn btn-warning btn-full" href="{{route('customer-account-complete')}}" title="完整註冊 - 「Webike-摩托百貨」">完整註冊</a>
                </div>
            @else
                <form action="{{ URL::route('review-comment-post',$review->id)}}" method="POST">
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-8 col-md-10">
                            <textarea class="form-control" rows="3" required name="content"></textarea>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-2">
                            <button type="submit" class="btn btn-danger btn-full">
                                送出回應
                            </button>
                        </div>
                    </div>
                </form>

            @endif

        @else
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h3 class="btn-label">回應評論請先「登入」，並完成「完整註冊」。</h3>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <a class="btn btn-warning btn-full" href="{{route('login')}}" title="會員登入 - 「Webike-摩托百貨」">會員登入</a>
                    </div>
                    <div class="hidden-xs col-sm-4 col-md-6"></div>
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <a class="btn btn-danger btn-full" href="{{route('customer-account-create')}}" title="註冊會員 - 「Webike-摩托百貨」">註冊會員</a>
                    </div>
                </div>
            </div>

        @endif

    </div>

    <div class="ct-comment-facebook">
        <div class="fb-comments" data-href="{{ request()->url() }}"
             data-width="100%" data-numposts="2"></div>
    </div>
</div>