@extends('response.layouts.1column')
@section('middle')
    <div class="ct-oem-part-right box-fix-auto">
        <div class="title-main-box">
            <h1>評論撰寫完成</h1>
        </div>
        <div class="center-box">
            <h1 class="number-text-genuuepart-complate">感謝您的撰寫!!</h1>
        </div>
        <div class="box-info-oem box-info-compalate">
            感謝您提供寶貴的評論：<br>
            1.我們將於七個工作天內審核您的投稿，您的評論採用之後會自動刊登於「商品評論」的頁面，提供給有興趣的車友作為參考。<br>
            2.評論一經採用後，系統自動將"<a target="_blank" href="{{ route('customer') }}" title="查詢改裝零件、正廠零件、騎士用品回饋點數-in webike台灣">回饋點數</a>"匯入您的個人帳戶中，您可以使用該點數作為購物折抵，詳情請參閱"<a target="_blank" href="{{URL::route('customer')}}" title="改裝零件、正廠零件、騎士用品回饋點數使用規則-in webike台灣">點數回饋使用說明</a>"。<br>
            3.若您對評論審核有相關的問題，請參閱<a target="_blank" href="{{ route('customer') }}" title="改裝零件、正廠零件、騎士用品評論撰寫規則-in webike台灣">"商品評論說明與規定"</a>或是來信service@webike.tw，謝謝。<br>
        </div>
        <div class="center-box">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{!! $home_url !!}">回首頁</a>
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish btn-send-genuinepart-finish-complate" href="{!! $base_url !!}">回商品評論首頁</a>
        </div>
    </div>
@stop
@section('script')
@stop