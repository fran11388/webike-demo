@extends('response.layouts.1column')
@section('style')

@endsection
@section('middle')

    <aside class="ct-left-search-list ct-left-search-list-main-menu col-xs-12 col-sm-4 col-md-3 col-lg-3 visible-sm visible-md visible-lg">
        <ul class="ul-left-title row">
            <!--Category-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>MyBike評論快速搜尋</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 box-choice-mybike">

                    @foreach ($my_bikes as $motor)
                        <li class="li-sub1 col-md-12 box-fix-column">
                            <div class="left-choice-bike box-fix-with">
                                <a href="{{ route('review-search',['url_path'=>'','mt'=>$motor->url_rewrite]) }}">
                                    <figure class="thumb-box-border zoom-image thumb-img">
                                        <img src="{{ $motor->manufacturer->image }}" alt="honda brand">
                                    </figure>
                                </a>
                            </div>
                            <div class="right-choice-bike box-fix-auto">
                                <a href="{{ route('review-search',['url_path'=>'','mt'=>$motor->url_rewrite]) }}">
                                    {{$motor->name}}
                                </a>
                            </div>
                        </li>
                    @endforeach

                </ul>
            </li>
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>分類</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 col-md-12">
                        <ul class="ul-sub2 row">
                            @foreach($tree as $category)
                                <li class="li-sub2 col-md-12">
                                    <div class="slide-down-btn">
                                        <i class="fa fa-minus" aria-hidden="true"></i><a class="a-sub2" href="{{ modifyReviewSearchUrl(['ca'=>$category->url_path] + request()->except('page'))  }}">
                                            &nbsp<span>{{$category->caregory_name}}</span>&nbsp<span
                                                    class="count">({{$category->count}})</span></a>
                                    </div>
                                    @if(count($category->subcategory))
                                        <ul class="ul-sub3 row">
                                            @foreach($category->subcategory as $subcategory)
                                                <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                                         href="{{ modifyReviewSearchUrl(['ca'=>$subcategory->url_path] + request()->except('page') )}}"><span>{{$subcategory->caregory_name}}</span>&nbsp<span
                                                                class="count">({{$subcategory->count}})</span></a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </li>
            <!--/.category-->
            @if(!$current_manufacturer)
                <li class="li-left-title col-md-12">
                    <div class="slide-down-btn">
                        <a href="javascript:void(0)" class="a-left-title"><span>品牌</span><i
                                    class="fa fa-chevron-up" aria-hidden="true"></i></a>
                    </div>
                    <div>
                        <select class="select2 select-review-br">
                                <option>品牌選擇</option>
                                @foreach($manufacturers as $manufacturer)
                                <option value="{{modifyGetParameters(['page'=> '', 'br'=>$manufacturer->name])}}">{{$manufacturer->name}} ({{$manufacturer->count}})</option>
                                @endforeach
                            </select>
                    </div>
                    {{-- <ul class="ul-sub1 ul-menu-list-brand">
                        <li>
                            <select class="select2">
                                <option>請選擇廠牌</option>
                                @foreach($manufacturers as $manufacturer)
                                <option data-href="{{modifyGetParameters(['page'=> '', 'br'=>$manufacturer->name])}}">{{$manufacturer->name}}</option>
                                @endforeach
                            </select>
                        </li> --}}
                    {{--     @foreach($manufacturers as $manufacturer)
                            @if($loop->iteration <= 5 )
                                <li><a href="{{modifyGetParameters(['page'=> '', 'br'=>$manufacturer->name])}}"><i class="fa fa-circle" aria-hidden="true"></i> {{$manufacturer->name}}
                                    <span>({{$manufacturer->count}})</span></a></li>
                            @else
                                <li style="display:none;"><a href="{{modifyGetParameters(['page'=> '','br'=>$manufacturer->name])}}"><i class="fa fa-circle" aria-hidden="true"></i> {{$manufacturer->name}}
                                        <span>({{$manufacturer->count}})</span></a></li>
                            @endif
                        @endforeach
                        @if(count($manufacturers) >5)
                            <li class="li-btn-show-more"><a href="javascript:void(0)"><i class="fa fa-plus"
                                                                                     aria-hidden="true"></i> Show
                                more</a></li>
                        @endif --}}
                    {{-- </ul> --}}
                </li> <!--/.brands-->
            @endif
            <!--Brands-->
            <!--Satisfaction-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>Satisfaction</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>

                <ul class="ul-sub1 row">
                    @foreach($rankings as $_ranking)
                        <li class="li-sub1 col-md-12"><a class="a-sub1 a-size-option"
                                                         href="{{modifyGetParameters(['page'=> '', 'ranking'=>$_ranking->ranking])}}"><span
                                        class="icon-star star-0{{$_ranking->ranking}}"></span><span>({{$_ranking->count}})</span></a></li>
                    @endforeach
                </ul>
            </li> <!--/satisfaction-->

        </ul>
        <div class="ct-left-banner row">
            @include('response.common.dfp.ad-160x600')
        </div>
    </aside>
    <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
        <div class="container">
            @include('response.pages.review.partials.filter')
            <div class="row ct-right-below">
                <div class="title-main-box">
                    <h1>{{$meta_filters}}搜尋結果</h1>
                </div>
                @include('response.pages.review.partials.list')
            </div>
            <div class="row ct-pagenav">
                {!! $pager->render() !!}
            </div>
        </div>
    </div>
    {{-- <aside class="ct-left-search-list ct-left-search-list-main-menu col-xs-12 col-sm-4 col-md-3 col-lg-3 visible-xs">
        <ul class="ul-left-title row">
            <!--Category-->
            <li class="li-left-title col-xs-12 col-sm-12 col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>分類</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1col-xs-12 col-sm-12 col-md-12">
                        <ul class="ul-sub2 row">
                            <li class="li-sub2 col-xs-12 col-sm-12 col-md-12">
                                <div class="slide-down-btn">
                                    <a class="a-sub2" href="javascript:void(0)"><i class="fa fa-minus"
                                                                                   aria-hidden="true"></i>&nbsp<span>Helmet</span></a>
                                </div>
                                <ul class="ul-sub3 row">
                                    <li class="li-sub2 li-sub3 col-xs-12 col-sm-12 col-md-12"><a class="a-sub2"
                                                                                                 href="javascript:void(0)"><span>Fullface</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-xs-12 col-sm-12 col-md-12"><a class="a-sub2"
                                                                                                 href="javascript:void(0)"><span>Jet</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-xs-12 col-sm-12 col-md-12"><a class="a-sub2"
                                                                                                 href="javascript:void(0)"><span>System</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                </ul>
                            </li>
                            <li class="li-sub2 col-xs-12 col-sm-12 col-md-12">
                                <div class="slide-down-btn">
                                    <a class="a-sub2" href="javascript:void(0)"><i class="fa fa-minus"
                                                                                   aria-hidden="true"></i>&nbsp<span>Riding Clothes</span></a>
                                </div>
                                <ul class="ul-sub3 row">
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Jackets</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Rainwear</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Protect wear</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                </ul>
                            </li>
                            <li class="li-sub2 col-md-12">
                                <div class="slide-down-btn">
                                    <a class="a-sub2" href="javascript:void(0)"><i class="fa fa-minus"
                                                                                   aria-hidden="true"></i>&nbsp<span>Shoes</span></a>
                                </div>
                                <ul class="ul-sub3 row">
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Shoes</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Boots</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Motocross</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                    <li class="li-sub2 li-sub3 col-md-12"><a class="a-sub2"
                                                                             href="javascript:void(0)"><span>Boots</span>&nbsp<span
                                                    class="count">(500)</span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li><!--/.category-->
            <!--Brands-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>Brands</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">Honda &nbsp<span
                                        class="count">(500)</span></label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">Shoei &nbsp<span
                                        class="count">(752)</span></label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">Arai &nbsp<span
                                        class="count">(525)</span></label></a></li>
                </ul>
            </li> <!--/.brands-->
            Price Range
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>Price Range</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">$0 - $500 &nbsp<span
                                        class="count">(500)</span></label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">$500 - $1000 &nbsp<span
                                        class="count">(752)</span></label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">$1000 - $1500
                                &nbsp<span class="count">(525)</span></label></a></li>
                </ul>
            </li> <!--/.price range-->
            <!--Color-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>Color</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="color-list-box container">
                    <li class="color-box">
                        <div>
                            <div class="red"></div>
                        </div>
                    </li>
                    <li class="color-box">
                        <div>
                            <div class="yellow"></div>
                        </div>
                    </li>
                    <li class="color-box">
                        <div>
                            <div class="blue"></div>
                        </div>
                    </li>
                    <li class="color-box">
                        <div>
                            <div class="pink"></div>
                        </div>
                    </li>
                    <li class="color-box">
                        <div>
                            <div class="green"></div>
                        </div>
                    </li>
                    <li class="color-box">
                        <div>
                            <div class="light-blue"></div>
                        </div>
                    </li>
                </ul>
            </li> <!--./color-->
            <!--Size-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>Size</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">0</label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">1</label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">2</label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="checkbox">3</label></a></li>
                </ul>
            </li> <!--/.size-->
            <!--Satisfaction-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>Satisfaction</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 col-md-12"><a class="a-sub1 a-size-option"
                                                     href="javascript:void(0)"><span
                                    class="icon-star star-04"></span><span>& Up</span></a></li>
                    <li class="li-sub1 col-md-12"><a class="a-sub1 a-size-option"
                                                     href="javascript:void(0)"><span
                                    class="icon-star star-04"></span><span>& Up</span></a></li>
                    <li class="li-sub1 col-md-12"><a class="a-sub1 a-size-option"
                                                     href="javascript:void(0)"><span
                                    class="icon-star star-04"></span><span>& Up</span></a></li>
                </ul>
            </li> <!--/satisfaction-->
            <!--New Lineup-->
            <li class="li-left-title col-md-12">
                <div class="slide-down-btn">
                    <a href="javascript:void(0)" class="a-left-title"><span>New Lineup</span><i
                                class="fa fa-chevron-up" aria-hidden="true"></i></a>
                </div>
                <ul class="ul-sub1 row">
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="radio" name="rdb_day">Nearly 30
                                days</label></a></li>
                    <li class="li-sub1 li-sub1-style2 col-md-12"><a class="a-sub1"
                                                                    href="javascript:void(0)"><label><input
                                        class="checkbox-left-sidebar-li" type="radio" name="rdb_day">Nearly 60
                                days</label></a></li>
                </ul>
            </li> <!--/.new lineup-->
        </ul>
        <div class="ct-left-banner row">
            <img src="{{assetRemote('image/banner/banner-05.jpg')}}">
        </div>
    </aside> --}}

    @include('response.common.ad.banner-small')

    <!-- Customer view -->
    <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @include('response.common.loading.md')
    </div>
    <!-- Customer view end-->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row-advertisement">
        @include('response.common.dfp.ad-728x90')
    </div>
@endsection
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>

    <script type="text/javascript">
        var baseUrl = '{{ request()->url() }}';
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }
        $(".select-option-redirect").change(function () {
            window.location = $(this).val();
        });

        $("a.a-sub2 > span").click(function () {
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(".select-motor-manufacturer").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get(url, function (data) {
                    var $target = $(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]).text(data[i]))
                    }
                });
            }
        });

        $(".select-motor-displacement").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                var url = path + "/api/motor/model?manufacturer=" +
                    $(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get(url, function (data) {
                    var $target = $(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(".select-motor-model,.select-motor-mybike").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                redirect('', value);
            }
        });

        $(".select-review-br").change(function(){
            var value = $(this).find('option:selected').val();
            if(value){
                window.location = value;
            }
        });

        $(".")

        function redirect(q, mt) {
            var params = {};
            if (q)
                params.q = q;
            else if (getParameterByName('q'))
                params.q = getParameterByName('q');
            if (mt)
                params.mt = mt;
            else if (getParameterByName('mt'))
                params.mt = getParameterByName('mt');


            var esc = encodeURIComponent;
            var query = Object.keys(params).map(k => esc(k) + '=' + esc(params[k])).join('&');
            if (query)
                query = "?" + query;

            window.location = window.location.href.split('?')[0] + query;
        }

        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
@stop
