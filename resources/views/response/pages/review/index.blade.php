@extends('response.layouts.1column')
@section('style')
<style>
    .box-search-motor .right input{
        -webkit-user-select: text !important
    }
</style>
@endsection
@section('middle')

    <div class="box-content-brand-top-page">
        <div id="m-menu" class="hidden-xs">
            <div>
                <aside class="ct-left-search-list  col-xs-12 col-sm-4 col-md-3 col-lg-3">
                    @include('response.pages.review.partials.side-top-banner')

                    @include('response.pages.review.partials.campaign')

                    @include('response.pages.review.partials.not-writing')

                    {{--@include('response.pages.review.partials.popular')--}}


                    @include('response.common.dfp.ad-160x600')
                </aside>
            </div>
        </div>
        <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
            <div class="mitumori-right-part">
                <div class="title-main-box">
                  <h1>商品評論</h1>
                </div>
            </div>
            @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))
                @include('response.common.ad.banner-simple',['ad_id'=>15])
            @else
                @include('response.common.ad.banner-simple',['ad_id'=>5])
            @endif

            <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                <div class="row">
                    <div class="title-main-box title-detail-info">
                        <h2>評論搜尋</h2>
                    </div>
                    <form action="{{ route('review-search','') }}">
                        <div class="box-fix-column box-search-motor">
                            <div class="box-fix-with left">
                                @include('response.common.list.filter-mybike')
                            </div>
                            <div class="box-fix-auto right">
                                <div class="box-fix-with">
                                    <button type="submit" class="btn btn-danger border-radius-2">搜尋</button>

                                </div>
                                <div class="box-fix-auto">
                                    <input type="search" name="q" required class="btn btn-default col-md-12"
                                           placeholder="輸入商品關鍵字...">
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="box-motor-data box-content">
                <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>評論分類</h2>
                </div>
                <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 review-category-top">

                    <li class="li-review-category-top">
                        <ul class="box-fix-column content">
                            <li class="box-fix-with left">
                                <a href="javascript:void(0)">
                                    <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
                                        <img src="{{ getCategoryImage( $customReviewObject->category) }}"
                                             alt="{{ $customReviewObject->category->name }}">
                                    </figure>
                                </a>
                            </li>
                            <li class="box-fix-auto right">
                                <ul class="item-review-category-top">
                                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">{{ $customReviewObject->category->name }}
                                        ({{$customReviewObject->count}})
                                    </li>
                                    @foreach ($customReviewObject->reviews as $review)
                                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                            <a href="{{ route('review-search' , [ $review->url_rewrite ]) }}"> {{ $review->parent_name }}
                                                ({{ $review->count }})</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>


                    <li class="li-review-category-top">
                        <ul class="box-fix-column content">
                            <li class="box-fix-with left">
                                <a href="javascript:void(0)">
                                    <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
                                        <img src="{{ getCategoryImage( $riderReviewObject->category) }}"
                                             alt="{{ $riderReviewObject->category->name }}">
                                    </figure>
                                </a>
                            </li>
                            <li class="box-fix-auto right">
                                <ul class="item-review-category-top">
                                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">{{ $riderReviewObject->category->name }}
                                        ({{$riderReviewObject->count}})
                                    </li>
                                    @foreach ($riderReviewObject->reviews as $review)
                                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                            <a href="{{ route('review-search' , [ $review->url_rewrite ]) }}"> {{ $review->parent_name }}
                                                ({{ $review->count }})</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="li-review-category-top">
                        <ul class="box-fix-column content">
                            <li class="box-fix-with left">
                                <a href="javascript:void(0)">
                                    <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
                                        <img src="{{assetRemote('image/review/bm_9000.png')}}"
                                             alt="{{ $genuineReviewObject->category->name }}">
                                    </figure>
                                </a>
                            </li>
                            <li class="box-fix-auto right">
                                <ul class="item-review-category-top">
                                    <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">{{ $genuineReviewObject->category->name }}
                                        ({{$genuineReviewObject->count}})
                                    </li>
                                    @foreach ($genuineReviewObject->reviews as $review)
                                        <li class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                            <a href="{{ route('review-search' , [ $review->url_rewrite ]) }}?br={{$review->product_manufacturer}}"> {{ $review->product_manufacturer }}
                                                ({{ $review->count }})</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="box-motor-data box-content">
                <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12"><h2>最新評論</h2>
                </div>
                <div class="ct-new-custom-part-on-sale-top col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="ct-item-product-grid-category-customer-review col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @foreach ($newReviews as $review)
                            <?php $href = route('review-show', $review->id) ?>
                            <li class="box-fix-column item item-product-grid item-product-custom-part-on-sale">
                                <div class="box-fix-auto">
                                    <div class="box-fix-with item-product-grid-pars-on-sale-left">
                                        <a href="{{ $href }}">
                                            <figure class="zoom-image thumb-box-border">
                                                <?php
                                                $src = '';
                                                if (starts_with($review->photo_key, 'http'))
                                                    $src = $review->photo_key;
                                                else
                                                    $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                                ?>

                                                <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="box-fix-auto item-product-grid-pars-on-sale-right text-left">
                                        <span class="dotted-text1">{{ $review->product_manufacturer }}</span>
                                        <a href="{{ $href }}" class="title-product-item-top dotted-text2" title="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                            <span class="dotted-text2">{{ $review->product_name }}</span>
                                        </a>
                                        <span class="icon-star star-0{{ $review->ranking }}"></span>
                                    </div>
                                </div>
                                <div class="box-fix-auto">
                                    <a href="{{ $href }}" class="title-product-item dotted-text2">{{ $review->title }}</a>
                                    <label class="normal-text dotted-text3">{!! e($review->content) !!}</label>
                                    <a href="{{ $href }}" class="btn-customer-review-read-more">>> 顯示全部</a>
                                </div>
                            </li>
                            @if ($loop->index == 3)
                                <div class="clearfix"></div>
                            @endif
                        @endforeach

                    </ul>
                </div>
            </div>


            @include('response.pages.review.partials.ranking')


            <div class="box-motor-data box-content">
                <div class="title-main-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2>最新回應</h2>
                </div>
                <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ct-review-comment">
                    @foreach ($newComments as $comment)
                        <?php $review = $comment->review; ?>
                        <li class="box-fix-column">
                            <div class="box-fix-with left">
                                <a href="{{ route('review-show', $comment->review->id)}}">
                                    <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
                                        <?php
                                        $src = '';
                                        if (starts_with($review->photo_key, 'http'))
                                            $src = $review->photo_key;
                                        else
                                            $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                        ?>
                                        <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name }}">
                                    </figure>
                                </a>
                            </div>
                            <div class="box-fix-auto right">
                                <div class="title-review-comment">
                                    <a href="{{ route('review-show', $comment->review->id) }}" class="title dotted-text2">{{ $review->product_name }}</a>
                                    <span>{{ date("Y/m/d", strtotime($review->created_at)) }}</span></div>
                                <div class="star-name">
                                    <span class="icon-star star-0{{ $review->ranking }}"></span>
                                    <div class="time">
                                        <span>來自 </span>{{ $review->nick_name }}<span> 於 {{ date("Y/m/d", strtotime($comment->created_at)) }}</span>
                                    </div>
                                </div>
                                <label class="normal-text dotted-text3 pull-left">{{ $review->nick_name }}：{{ $comment->content }}</label>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>

            @include('response.common.ad.banner-small')

            <!-- Customer view -->
            <div id="recent_view" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include('response.common.loading.md')
            </div>
            <!-- Customer view end-->

            <div class="box-motor-data row-advertisement">
                @include('response.common.dfp.ad-728x90')
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script type="text/javascript">
        $('#mybike_select').change(function(){
            window.location = "{{route('review-search','')}}" + '?mt=' + $(this).val();
        });
    </script>

@stop