<div class="row box-content winter-collerction">
    <div class="title-winter-collerction col-xs-12 col-sm-2 col-md-2 col-lg-2">
        <h2>
            <span>2017 春夏 Collection</span>
            <a href="{!! URL::route('collection-type-detail',['type'=>'category','url_rewrite'=>'2017SS']) !!}" title="{!! '2017 春夏 Collection' . $tail !!}">>> 更多新品</a>
        </h2>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 ct-winter-collerction">
        <ul class="product-list owl-carousel-5">
            @foreach($riding_gears as $product)
            <li>
                @include('response.common.product.a')
            </li>
            @endforeach
        </ul>
    </div>
</div>
<script>
    // carousel for 5 items
    $('.owl-carousel-5').addClass('owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        margin:5,
        slideBy : 5,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
</script>