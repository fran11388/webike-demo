<div class="row box-content custom-parts">
    <div class="title-main-box-clear">
        <h2 class="with-tag">
            <span>最新雜誌&書籍　</span>
            <a href="{!! route('summary', 'ca/3000-1210') !!}" title="雜誌&書籍全部商品">>> 查看全部</a>
        </h2>
    </div>
    @foreach($amazons as $collection)
        <div class="ct-new-magazine col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="owl-carousel">
                @foreach($collection as $product)
                    <li class="item item-product-grid item-product-new-magazine">
                        <a href="{!! route('product-detail', ['url_rewrite' => $product->url_rewrite]) !!}" title="{{$product->full_name}}" target="_blank">
                            <figure class="zoom-image">
                                {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                            </figure>
                        </a>
                    </li>
                @endforeach
            </div>
        </div>
    @endforeach
</div>