<div class="row box-content winter-collerction">
    <div class="title-winter-collerction col-xs-12 col-sm-2 col-md-2 col-lg-2">
        <img class=" visible-md visible-lg" src="{!! assetRemote('image/shopping/img-outlet.jpg') !!}" width="120" height="73" alt=""/>
        <h2>
            <span>OUTLET STORE OPEN!!</span>
            <a href="{!! URL::route('outlet') !!}" title="{!! 'OUTLET' . $tail !!}">>> 每周更新</a>
        </h2>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 ct-outlet-store-open">
        <ul class="product-list owl-carousel-5">
            @foreach($outlet_collection as $product)
                <li>
                    @include('response.common.product.b')
                </li>
            @endforeach
        </ul>
    </div>
</div>
<script>
    // carousel for 5 items
    $('.owl-carousel-5').addClass('owl-carousel').owlCarousel({
        loop:false,
        nav:true,
        margin:5,
        slideBy : 5,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
</script>