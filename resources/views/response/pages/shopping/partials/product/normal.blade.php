<div class="row box-content">
    <div class="title-main-box-clear">
        <h2>
            <span>{!! $category->name !!}推薦商品　</span>
            <a href="{!! URL::route('summary', 'ca/' . $category->mptt->url_path) !!}" title="{!! $category->name . '全部商品' !!}">>> 查看全部</a>
        </h2>
    </div>
    <div class="ct-riding-gear col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ul class="product-list owl-carousel-ajax" data-owl-ajax="{{json_encode(${'category_'.$url_path}->params)}}" data-owl-ajax-load="false">
            @foreach(${'category_'.$url_path}->collection as $product)
                <li data-sku="{{$product->sku}}">
                    @include('response.common.product.b')
                </li>
            @endforeach
        </ul>
    </div>
</div>
