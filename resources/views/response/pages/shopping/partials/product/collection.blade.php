<div class="row box-content winter-collerction">
    <div class="title-winter-collerction col-xs-12 col-sm-2 col-md-2 col-lg-2">
        <h2>2017 秋冬 Collection</h2>
        <hr class=" visible-xs">
    </div>
    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 ct-winter-collerction">
        <ul class="ct-item-product-grid-car-page owl-carousel-5">
            @foreach($collection as $product)
            <li class="item item-product-grid">
                <a href="{!! route('product-detail', ['url_rewrite' => $product->url_rewrite]) !!}" title="{!! $product->full_name !!}" target="_blank">
                    <figure class="zoom-image thumb-box-border">
                        <img src="{!! $product->getThumbnail() !!}" alt="{!! $product->full_name !!}">
                    </figure>
                </a>
                <a href="{!! route('product-detail', ['url_rewrite' => $product->url_rewrite]) !!}" title="{!! $product->full_name !!}" target="_blank">
                    <span class="title-product-item text-center dotted-text2">{!! $product->name !!}</span>
                </a>
            </li>
            @endforeach
        </ul>
    </div>
</div>