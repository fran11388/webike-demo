<ul class="menu-left col-md-12 list-inline">
    <li class="col-xs-12 col-sm-12 col-md-12"><a href="{{route('shopping')}}">
        <!-- <img src="{!! assetRemote('image/icon-sale.png') !!}" alt="摩托百貨"/> -->
        <h3><span>摩托百貨</span><img src="{!! assetRemote('image/SS-sale-label.png') !!}"></h3>
    </a></li>
    <li class="col-xs-12 col-sm-12 col-md-12"><a href="{{route('outlet')}}">
        <!-- <img src="{!! assetRemote('image/icon-outlet.png') !!}" alt="Outlet出清商品"> -->
        <h3>Outlet出清</h3>
    </a></li>
    <li class="col-xs-12 col-sm-12 col-md-12"><a href="{{route('motor')}}">
        <!-- <img src="{!! assetRemote('image/icon-model.png') !!}" alt="車型規格索引"> -->
        <h3>車型索引</h3>
    </a></li>
    <li class="col-xs-12 col-sm-12 col-md-12"><a href="{{ route('benefit-event-motogp-2017')}}">
        <!-- <img src="{!! assetRemote('image/icon-motogp.png') !!}" alt="MotoGP"> -->
        <h3><span>MotoGP</span><img src="{!! assetRemote('image/motogp-lable.gif?station=3') !!}"></h3>
    </a></li>
    <li class="col-xs-12 col-sm-12 col-md-12"><a href="{{MOTOMARKET}}">
        <!-- <img src="{!! assetRemote('image/icon-review.png') !!}" alt="摩托車市"> -->
        <h3>摩托車市</h3>
    </a></li>
    <li class="col-xs-12 col-sm-12 col-md-12"><a href="{{BIKENEWS}}">
        <!-- <img src="{!! assetRemote('image/icon-collection.png') !!}" alt="摩托新聞"> -->
        <h3>摩托新聞</h3>
    </a></li>
</ul>