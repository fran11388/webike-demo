<style type="text/css">
    .popups-ui .popups-ui-window.popups-login{
        width: 750px;
        margin: 150px auto 0;
        padding-top: 0;
        z-index: 1060;
    }
    .popups-ui-cover {
        padding-top: 150px;
        margin-top: 0px;
    }
    .popups-ui .popups-ui-window.popups-login .title {
        background-color: #e61e25;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }
    .popups-ui .popups-ui-window.popups-login .content {
        background-color: white;
        padding: 10px;
    }
    @media(max-width: 1500px){
        .popups-login {
            margin: 60px auto 0 !important;
        }
    }
    @media (max-width: 992px){
        .popups-ui .popups-ui-window .popups-ui-window-close{
            right: 30px;
        }
        .popups-ui .popups-ui-window.popups-login .title {
            height: 105px;
        }
    }
    @media (max-width: 767px){
        .popups-ui-cover {
            padding-top: 100px !important;
        }
        .popups-ui {
            margin-top: 0px !important;
        }
        .popups-ui .popups-ui-window .popups-ui-window-close{
            right: 0px;
        }
        .popups-ui .popups-ui-window.popups-login {
            width: 95%;
            margin-top: 0px;
        }
        .popups-ui .popups-ui-window.popups-login .title {
            height: 85px;
        }
        .popups-ui .popups-ui-window.popups-login .title img{
            width: 90%;
        }
        .send_email_block {
            display: block;
            text-align: center;
            margin-top: 20px;
        }
    }
    @media (max-width: 351px){
        .popups-ui .popups-ui-window.popups-login .title {
            height: 75px;
        }
        .popups-ui .popups-ui-window.popups-login .content{
            padding: 10px;
        }
    }
</style>
<div class="popups-ui popups-ui-cover">
    <div class="popups-ui-window popups-login">
        <div class="popups-ui-window-close">
            <img src="{!! assetRemote('image/customer/account/pop-ups-close.png') !!}">
        </div>
        <div class="title text-center">
            <img src="{!! assetRemote('image/customer/account/pop-ups-title3.png') !!}">
            <span class="helper"></span>
        </div>
        <div class="content">
            <h2 class="font-bold">我們已發送一封認證信件至您的電子郵件帳號。</h2>
            <br>
            <p>因應本站導入電子發票系統，為避免電子發票發送至無效的電子信箱，請協助我們進行有效電子郵件認證。</p>
            <br>
            <div><p>收不到信件？建議您可以檢查您的垃圾信件夾，或是  重新發送驗證信 。    <span class="send_email_block"><button type="button" class="btn btn-warning verify-email">重新發送驗證信</button>    <span class="font-color-red showtime" style="display:none;"><span id="showtime"></span> 秒後可重新發送…</span></span></p></div>

        </div>


    </div>


    <script>
        var s = 60;
        $('.verify-email').click(function(){

            var email_verify_url = "{{ \URL::route('api-send-verify-email') }}";
            var customer_email = "{{ session()->get('customer_email') }}";
            $.ajax({
                url: email_verify_url,
                data: {customer_email:customer_email},
                success: function(result){
                }
            });

            s = 60;
            $('.verify-email').attr('disabled',true);
            $('.showtime').show();
            reciprocal();
        });

        function reciprocal(){
            document.getElementById("showtime").innerHTML=s;
            if(s == 0){
                $('.showtime').hide();
                $('.verify-email').attr('disabled',false);
                return false;
            }
            setTimeout(reciprocal,1000);
            s -= 1;
        }
    </script>
</div>

<script type="text/javascript">
    $(document).on('click', '.popups-ui-window.popups-login .popups-ui-window-close', function(){
        $(this).closest('.popups-ui-window').fadeOut();
        $(this).closest('.popups-ui').hide();
        $.ajax({
            url: "{!! route('customer-session', 'invite-join') !!}",
            method: "POST",
            data: {},
            dataType: "json",
            success: function(result){
                console.log(result);
            },
            error: function(error){

            },
        })
    });

</script>