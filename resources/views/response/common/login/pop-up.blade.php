<style type="text/css">
    .popups-ui {
        margin-top: 0px !important;
    }
    .popups-ui .popups-ui-window.popups-login{
        width: 800px;
        margin: 150px auto 0;
        padding-top: 0;
        background-image: url("{!! assetRemote('image/customer/account/pop-ups-background.png') !!}");
        background-size: 100% 100%;
        z-index: 1060;
    }
    .popups-ui .popups-ui-window.popups-login .title{
        height: 80px;
        font-size: 0;
    }
    .popups-ui .popups-ui-window.popups-login .title span{
        vertical-align: middle;
    }
    .popups-ui .popups-ui-window.popups-login .title img{
        vertical-align: middle;
    }
    .popups-ui .popups-ui-window.popups-login .content{
        padding: 15px;
    }
    .popups-ui .popups-ui-window.popups-login .content .advantages [class^="col-"]{
        padding-left: 10px;
        padding-right: 10px;
    }
    .popups-ui .popups-ui-window.popups-login .content .advantages dl dt{
        font-weight: bold;
        font-size: 1rem;
        text-align: center;
        height: 20px;
        line-height: 20px;
    }
    .popups-ui .popups-ui-window.popups-login .content .advantages dl dd{
        text-align: center;
    }
    .popups-ui .popups-ui-window.popups-login .content .advantages dl dd img{
        max-width: 100%;
        max-height: 120px;
    }
    .popups-ui .popups-ui-window.popups-login .content .advantages dl dd p{
        text-align: left;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions input[placeholder]{
        line-height: 34px;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions dt{
        font-weight: bold;
        font-size: 1.2rem;
        text-align: center;
        height: 30px;
        line-height: 30px;
        margin-bottom: 5px;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions .function-left{
        background-image: url("{!! assetRemote('image/customer/account/pop-ups-side-line.png') !!}");
        background-repeat: no-repeat;
        background-position-x: 100%;
        padding-right: 20px !important;
        margin-right: -15px;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions .float-link{
        text-decoration: underline;
        display: block;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions .float-link-posision{
        margin-top: 85px;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions [class^="col-"]{
        padding-left: 5px;
        padding-right: 5px;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions .note{
        font-weight: bold;
        text-align: center;
        display: none;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions .note.active{
        display: block;
    }
    .popups-ui .popups-ui-window.popups-login .content .functions hr{
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .btn-signin, .btn-fast-registration, .btn-facebook{
        position: relative;
    }
    .box-info-compalate {
        width: 610px;
        padding: 5px;
    }
    .cssload-container {
        display: none;
        height: 22px;
        text-align: center;
        position: absolute;
        right: 5px;
        top: 50%;
        margin-top: -11px;
    }
    .cssload-container.active {
        display: block;
    }
    .cssload-speeding-wheel {
        width: 21px;
        height: 21px;
        margin: 0 auto;
        border: 1px solid rgba(255,255,255,0.98);
        border-radius: 50%;
        border-left-color: transparent;
        border-right-color: transparent;
        animation: cssload-spin 800ms infinite linear;
        -o-animation: cssload-spin 800ms infinite linear;
        -ms-animation: cssload-spin 800ms infinite linear;
        -webkit-animation: cssload-spin 800ms infinite linear;
        -moz-animation: cssload-spin 800ms infinite linear;
    }
    .box-info-account-title-text{
        font-size: 1.2rem;
        font-weight: bold;
    }
    @media(max-width: 1500px){
        .popups-login {
            margin: 20px auto 0 !important;
        }
    }
    @media(min-width: 1000px){
        .advantages .custom div{
            width: 20%;
        }
    }
    @media (max-width: 767px){
        .box-info-compalate {
            width: 100%;
        }
        .box-info-account-title-text {
            font-size: .85rem;
        }
    }
    @keyframes cssload-spin {
        100%{ transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-o-keyframes cssload-spin {
        100%{ -o-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-ms-keyframes cssload-spin {
        100%{ -ms-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-webkit-keyframes cssload-spin {
        100%{ -webkit-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @-moz-keyframes cssload-spin {
        100%{ -moz-transform: rotate(360deg); transform: rotate(360deg); }
    }
    @media (max-width: 992px){
        .popups-ui .popups-ui-window .popups-ui-window-close{
            right: 30px;
        }
        .popups-ui .popups-ui-window.popups-login .title {
            height: 105px;
        }
    }
    @media (max-width: 767px){
        .popups-login {
            margin: 150px auto 0 !important;
        }
        .popups-ui {
            margin-top: -150px !important;
        }
        .popups-ui .popups-ui-window .popups-ui-window-close{
            right: 0px;
        }
        .popups-ui .popups-ui-window.popups-login {
            width: 95%;
            margin-top: 210px; 
        }
        .popups-ui .popups-ui-window.popups-login .title {
            height: 85px;
        }
        .popups-ui .popups-ui-window.popups-login .title img{
            width: 90%;
        }
        .popups-ui .popups-ui-window.popups-login .content .advantages dl dd p{
            text-align: center;
        }
        .popups-ui .popups-ui-window.popups-login .content .advantages dl dd img{
            max-height: 90px;
        }
        .popups-ui .popups-ui-window.popups-login .content .functions .function-left{
            background: none;
        }
        .box-info-compalate{
            width: 100%;
        }
    }
    @media (max-width: 351px){
        .popups-ui .popups-ui-window.popups-login .title {
            height: 75px;
        }
        .popups-ui .popups-ui-window.popups-login .content{
            padding: 10px;
        }
        .popups-ui .popups-ui-window.popups-login .content .functions .box{
            margin-bottom: 5px;
        }
        .popups-ui .popups-ui-window.popups-login .content .functions .content-last-box{
            margin-top: 5px;
        }
    }
</style>
<div class="popups-ui popups-ui-cover">
    <div class="popups-ui-window popups-login">
        <div class="popups-ui-window-close">
            <img src="{!! assetRemote('image/customer/account/pop-ups-close.png') !!}">
        </div>
        <div class="title text-center">
            <img src="{!! assetRemote('image/customer/account/pop-ups-title.png') !!}">
            <span class="helper"></span>
        </div>
        <div class="content">
            <div class="advantages box">
                <div class="clearfix custom">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <dl>
                            <dt>會員好康一</dt>
                            <dd>
                                <img src="{!! assetRemote('image/customer/account/advantages-1.png') !!}">
                                <p>
                                    購物及參加活動皆可獲得點數，1點即可折抵1元。
                                </p>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <dl>
                            <dt>會員好康二</dt>
                            <dd>
                                <img src="{!! assetRemote('image/customer/account/advantages-2.png') !!}">
                                <p>
                                    超過49萬項海內外摩托商品；正廠零件24小時免費報價服務!
                                </p>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <dl>
                            <dt>會員好康三</dt>
                            <dd>
                                <img src="{!! assetRemote('image/customer/account/advantages-3.png') !!}">
                                <p>
                                    全球車界新聞、最新商品、車輛規格、促銷訊息，每周更新。
                                </p>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <dl>
                            <dt>會員好康四</dt>
                            <dd>
                                <img src="{!! assetRemote('image/customer/account/advantages-4.png') !!}">
                                <p>
                                    定期舉辦會員專屬活動及遊戲，豐富獎品、點數等您帶回去！
                                </p>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <dl>
                            <dt>會員好康五</dt>
                            <dd>
                                <img src="{!! assetRemote('image/customer/account/advantages-5.png') !!}">
                                <p>
                                    Webike購物享有兩大保證，保證正品與保證退換貨，抹除你的網購疑慮！
                                </p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="functions">
                <div class="clearfix custom">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 function-left">
                        <div class="clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-8">
                                <dl>
                                    <dt >會員登入</dt>
                                    <dd class="clearfix">
                                        <form id="pop-up-login" method="POST" action="{{ route('login') }}">
                                            <input type="hidden" name="remember" value="1">
                                            <input class="form-control box" type="text" data-type="email" name="email" placeholder="請輸入E-Mail" value="{{ old('email') }}">
                                            <input class="form-control box" type="password" name="password" placeholder="請輸入密碼">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-full btn-signin font-bold box">
                                                登入
                                                <div class="cssload-container">
                                                    <div class="cssload-speeding-wheel"></div>
                                                </div>
                                            </button>
                                            <a href="{{ route('login-facebook') }}" class="btn btn-facebook btn-login-with-face btn-full font-bold">
                                                Facebook 登入
                                                <div class="cssload-container">
                                                    <div class="cssload-speeding-wheel"></div>
                                                </div>
                                            </a>
                                            <label class="note content-last-box"></label>
                                            <a id="redirect-create-tab" class="float-link text-center content-last-box hidden-lg hidden-md hidden-sm" href="javascript:void(0)">
                                                立即註冊成為會員
                                            </a>
                                            <!-- <a id="redirect-create-tab" class="btn btn-sub btn-full hidden-lg hidden-md hidden-sm" href="javascript:void(0)">會員註冊</a> -->
                                        </form>
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-2">
                                <a class="float-link float-link-posision" href="{{route('customer-forgot-password')}}" target="_blank">
                                    忘記<span class="hidden-lg hidden-md hidden-sm">?</span><span class="hidden-xs">密碼?</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-2"></div>
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-8">
                                <dl>
                                    <dt >新會員註冊</dt>
                                    <dd class="clearfix">
                                        <form id="pop-up-create" method="POST" action="{{ route('customer-account-create') }}">
                                            {{ csrf_field() }}
                                            <input type="text" data-type="email" class="form-control box" name="email" placeholder="請輸入E-Mail" >
                                            <input type="password" class="form-control box" name="password" placeholder="密碼不能低於6個字元">
                                            <input type="password" class="form-control box" name="password_confirm" placeholder="確認密碼">
                                            <button type="submit" class="btn btn-sub btn-full btn-fast-registration font-bold">
                                                E-Mail驗證
                                                <div class="cssload-container">
                                                    <div class="cssload-speeding-wheel"></div>
                                                </div>
                                            </button>
                                            <label class="note content-last-box"></label>
                                        </form>
                                        <a id="redirect-login-tab" class="float-link text-center content-last-box hidden-lg hidden-md hidden-sm" href="javascript:void(0)">
                                            返回會員登入頁面
                                        </a>
                                        <!-- <a id="redirect-login-tab" class="btn btn-sub btn-full hidden-lg hidden-md hidden-sm" href="javascript:void(0)">會員登入</a> -->
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$finish_content = '<img class="box" src="'.assetRemote('image/customer/account/pop-ups-benefit.jpg').'">'.
                    '<div class="box-info-account-title-text box">您已經完成「快速註冊」，繼續完成<a href="' . route('customer-account-complete') . '" title="完整註冊" target="_blank">「完整註冊」</a>。</div>'.
                    '<div class="box-info-oem box-info-compalate text-left">'.
                    '會員福利與服務：<br>'.
                    '1.<a href="' . route('shopping') . '" title="「Webike-摩托百貨」" target="_blank">「Webike-摩托百貨」</a>100元體驗折價券，您可於<a href="' . route('cart') .'" title="我的購物車 - 「Webike-摩托百貨」" target="_blank">購物車</a>結帳時選擇使用<br>'.
                    '2.「Webike」電子報(每週五發送)<br>'.
                    '3.首次登錄<a href="' . route('benefit-event-mybike') . '" title="MyBike" target="_blank">「MyBike」</a>即可獲得100元現金點數。<br>'.
                    '4.<a href="' . route('genuineparts') . '" title="正廠零件查詢系統 - 「Webike-摩托百貨」" target="_blank">正廠零件查詢系統</a><br>'.
                    '5.<a href="' . route('mitumori') .'" title="未登錄商品查詢系統 - 「Webike-摩托百貨」" target="_blank">未登錄商品查詢系統</a><br>'.
                    '6.<a href="' . route('groupbuy') .'" title="團購商品查詢系統 - 「Webike-摩托百貨」" target="_blank">團購商品查詢系統</a><br>'.
                    '7.<a href="http://www.webike.tw/motomarket/" title="「Webike-摩托車市」" target="_blank">「Webike-摩托車市」刊登服務</a><br>'.
                    '<br>'.
                    '注意事項：<br>'.
                    '1.<a href="' . route('shopping') . '" title="「Webike-摩托百貨」" target="_blank">「摩托百貨」</a>與<a href="http://www.webike.tw/motomarket/" title="「Webike-摩托車市」" target="_blank">「摩托車市」</a>的登入帳號與密碼通用，請您務必寄下您的註冊資料。<br>'.
                    '2.目前「電子報」預設為開通狀態，您可以至"<a href="' . route('customer') . '" title="會員中心 - 「Webike-摩托百貨」" target="_blank">會員中心</a>">"<a href="' . route('customer-account').'" title="基本資料設定 - 「Webike-摩托百貨」" target="_blank">基本資料設定</a>"進行修改；<br>　或是當您收到電子報後，於下方連結按下「取消訂閱」即可。<br>'.
                    '3.相關本站的規定與說明，請至<a href="' . route('customer') .'" title="會員中心 - 「Webike-摩托百貨」" target="_blank">「會員中心」</a>。<br>'.
                    '</div>';
?>
<script type="text/javascript">
    $(document).on('click', '.popups-ui-window.popups-login .popups-ui-window-close', function(){
        $(this).closest('.popups-ui-window').fadeOut();
        $(this).closest('.popups-ui').hide();
        $.ajax({
            url: "{!! route('customer-session', 'invite-join') !!}",
            method: "POST",
            data: {},
            dataType: "json",
            success: function(result){
                console.log(result);
            },
            error: function(error){

            },
        })
    });
    $('.btn-facebook').click(function(){
        $(this).find('.cssload-container').addClass('active');
        $(this).addClass('disabled').prop('disabled', true);
    });

    $('#pop-up-login').on('submit', function(){
        var form = $(this);
        var validate = formValidate(form);
        if(!validate){
            return false;
        }
        $('.btn-signin .cssload-container').addClass('active');
        $('.btn-signin').addClass('disabled').prop('disabled', true);
        $.ajax({
            url: "{!! route('login-api') !!}",
            method: "POST",
            data: form.serializeArray(),
            dataType: "JSON",
            success: function(result){
                if(result.success){
                    _fbq.push(['track', 'pop-up-login']);
                    ga('send', 'event', 'account', 'login', 'pop-up');

                    window.location.reload();
                }else{
                    if(result.redirect_check === true){
                        window.location.assign(result.redirect_url);
                    }
                    var note = form.find('.note').addClass('has-error');
                    note.text('').addClass('active');
                    $.each(result.messages, function(key, message){
                        note.text(note.text() + message);
                    });
                    $('.btn-signin .cssload-container').removeClass('active');
                    $('.btn-signin').removeClass('disabled').removeAttr('disabled');
                }
            },
            error: function(data){
                $('.btn-signin .cssload-container').removeClass('active');
                $('.btn-signin').removeClass('disabled').removeAttr('disabled');
            }
        });
        return false;
    });

    $('#pop-up-create').on('submit', function(){
        var form = $(this);
        var validate = formValidate(form);
        if(!validate){
            return false;
        }
        $('.btn-fast-registration .cssload-container').addClass('active');
        $('.btn-fast-registration').addClass('disabled').prop('disabled', true);

        $.ajax({
            url: "{!! route('customer-account-create') !!}",
            method: "POST",
            data: form.serializeArray(),
            dataType: "JSON",
            success: function(result){
                if(result.success){
                    _fbq.push(['track', 'pop-up-account-create']);
                    ga('send', 'event', 'account', 'create', 'pop-up');
                    PopUpOut();
                    swal({
                        title:'',
                        width: 650,
                        html:'{!! $finish_content !!}',
                        type: 'success'
                    }).then(function(result){
                        if (result) {
                            window.location.reload();
                        }
                    });
                }else{
                    if(result.redirect_check === true){
                        window.location.assign(result.redirect_url);
                    }
                    var note = form.find('.note').addClass('has-error');
                    note.text('').addClass('active');
                    $.each(result.messages, function(key, message){
                        note.text(note.text() + message);
                    });
                    $('.btn-fast-registration .cssload-container').removeClass('active');
                    $('.btn-fast-registration').removeClass('disabled').removeAttr('disabled');
                }
            },
            error: function(data){
                $('.btn-fast-registration .cssload-container').removeClass('active');
                $('.btn-fast-registration').removeClass('disabled').removeAttr('disabled');
            }
        });
        return false;
    });

    if($(window).width() <= 767){
        $('.popups-ui .popups-ui-window .advantages > .clearfix').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            navText: [
                '<i class="fa fa-chevron-left"></i>',
                '<i class="fa fa-chevron-right"></i>'
            ],
            margin:5,
            slideBy : 1,
            items:1
        });
        $('.popups-ui .popups-ui-window .functions > .clearfix').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            navClass: [
                "owl-prev hidden",
                "owl-next hidden"
            ],
            margin:5,
            slideBy : 1,
            items:1
        });
//        $('.popups-ui .popups-ui-window .functions > .clearfix > .owl-nav > .owl-next').html('立即註冊');
//        $('.popups-ui .popups-ui-window .functions > .clearfix > .owl-nav > .owl-prev').html('會員登入');
        $('#redirect-create-tab').click(function(){
            $(this).closest('.owl-carousel.custom').find('.owl-next').click();
        });
        $('#redirect-login-tab').click(function(){
            $(this).closest('.owl-carousel.custom').find('.owl-prev').click();
        });
//        $('.popups-ui .popups-ui-window .functions > .clearfix > .owl-nav > [class^="owl-"]').click(function(){
//            $(this).addClass('hidden');
//            if($(this).attr('class').indexOf('next') >= 0){
//                $(this).closest('.owl-nav').find('.owl-prev').removeClass('hidden');
//            }else{
//                $(this).closest('.owl-nav').find('.owl-next').removeClass('hidden');
//            }
//        });
    }

    function formValidate(form){
        var regexp = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        var validate = true;
        var emailValidate = false;
        $(form).find('.note').removeClass('has-error').removeClass('active').text('');
        $(this).find('input[data-type="email"]').each(function(key, element){
            emailValidate = ($(element).val().match(regexp) == null) ? false : true;
            if(!emailValidate){
                validate = false;
                $(form).find('.note').addClass('has-error').addClass('active').text('Email 必須符合Email格式。');
            }
        });
        var password = $(form).find('input[type="password"]').val();
        if(password.length < 6){
            validate = false;
            $(form).find('.note').addClass('has-error').addClass('active').text('密碼 不能低於6個字元。');
        }

        if($(form).find('input[name="password_confirm"]').length > 0){
            var password_confirm = $(form).find('input[name="password_confirm"]').val();
            if(password_confirm.length < 6){
                validate = false;
                $(form).find('.note').addClass('has-error').addClass('active').text('確認密碼 不能低於6個字元。');
            }
            if(password !== password_confirm){
                validate = false;
                $(form).find('.note').addClass('has-error').addClass('active').text('密碼與確認密碼不相同。');
            }
        }

        return validate;
    }
    function PopUpOut(){
        $('.popups-ui-window').fadeOut();
        $('.popups-ui').hide();
    }

</script>