<div class="col3-icon">
    <!-- When login succes we add class "login-success-active"  -->
    <ul class="nav top-nav{{ $current_customer ? ' login-success-active' : ' '}}">
        @if($current_customer)
            @if(count($current_customer->motors))
                <li class="my-bike animated fadeInRight">
                    <a href="javascript:void(0)" class="btn btn-mybike-add visible-md visible-lg">
                        <span>MyBike商品搜尋</span>
                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" class="btn-my-bike-tablet font-color-red visible-sm">
                        <i class="fa fa-motorcycle size-19rem hidden-xs"></i>
                        <i class="fa fa-motorcycle size-15rem hidden-lg hidden-md hidden-sm"></i>
                    </a>

                    <ul class="show-my-bike">
                        @foreach($current_customer->motors as $motor)
                            <li>
                                <a href="{{ route('parts',[ 'mt/'.$motor->url_rewrite ]) }}">
                                    {{ $motor->name }}
                                </a>
                            </li>
                        @endforeach
                        <li class="btn-add-more-bike">
                            <a class="text-left" href="{{ route('customer-mybike') }}">
                                <i class="fa fa-edit" aria-hidden="true"></i> 編輯MyBike
                            </a>
                        </li>
                    </ul>
                </li>
            @else
                <li class="my-bike animated">
                    <a href="{{ route('customer-mybike') }}" class="btn btn-mybike-add"><i class="fa fa-plus" aria-hidden="true"></i> 記住我的車</a>
                </li>
            @endif
        @endif
        <li class="login">
            <a href="{{ route('login') }}" class="btn btn-login font-color-red">會員登入</a>
        </li>
        <li class="menu-icon">
            <div class="icon-area login-success">
                <div class="login-info">
                    @if($current_customer)
                        @php
                            $palette = session()->get('palette');
                        @endphp
                        <div class="account-icon" title="{{$current_customer->nickname}}" style="background:{{$palette['background']}};border:{{$palette['border']}}">
                            <span class="helper"></span>
                            <span class="text" style="color:{{$palette['color']}}">{{$current_customer->role_id == 1 ? '您好' : mb_substr($current_customer->nickname, 0, 2)}}</span>
                        </div>
                    @endif
                    {{--<img class="img-circle img-responsive uh-avatar" src="{!! assetRemote('image/moto-gp/img-avarta.jpg') !!}" alt="avarta"/>--}}
                    {{--<span style="display:none" class="user-name dotted-text">{{ $current_customer ? $current_customer->nickname : 'Name'}}</span> <i style="display:none" class="fa fa-caret-down icon-down" aria-hidden="true"></i>--}}
                </div>
                <ul class="list">
                    <li><a href="{{ route('logout') }}" title="登出{{$tail}}">登出</a></li>
                </ul>
            </div>
        </li>

        @include('response.layouts.partials.header.cart-icon')
        @include('response.layouts.partials.header.info-icon')
        {{--<li class="helpdesk">--}}
            {{--<a href="{{ route('customer') }}" class="btn-helpdesk font-color-red">--}}
                {{--<i class="glyphicon glyphicon-info-sign size-19rem hidden-xs"></i>--}}
                {{--<i class="glyphicon glyphicon-info-sign size-15rem hidden-lg hidden-md hidden-sm"></i>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--@if($current_customer)--}}
            {{--<li class="helpdesk">--}}
                {{--<a href="{{ route('logout') }}" class="btn-helpdesk font-color-red">--}}
                    {{--<i class="glyphicon glyphicon-log-out size-19rem hidden-xs"></i>--}}
                    {{--<i class="glyphicon glyphicon-log-out size-15rem hidden-lg hidden-md hidden-sm"></i>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--@endif--}}
    </ul>
</div>