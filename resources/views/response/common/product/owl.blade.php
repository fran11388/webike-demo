@extends('response.layouts.1column')
@section('middle')

    <ul class="product-list owl-carousel-ajax" data-owl-ajax="{{json_encode($riding_gear->params)}}" data-owl-ajax-load="false">
        @foreach($riding_gear->collection as $product)
            <li data-sku="{{$product->sku}}">
                @include('response.common.product.a')
            </li>
        @endforeach
    </ul>
@stop
@section('script')
    <script src="{{ assetRemote('js/pages/owl.carousel-ajax.js') }}"></script>
@stop