{{--
CLASS B
Item information:
    image:140X140
    product name:2 line
    manufacturer:1 line
    Price:without discount box(margin bottom) , discount
    model:more then one will use ...
    option
--}}


<div class="product-grid">
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <figure class="zoom-image">
            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
        </figure>
    </a>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <span class="name product-name dotted-text2 force-limit">{!!  $product->name  !!}</span>
    </a>
    <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
    @php
        $final_price = $product->getFinalPrice($current_customer);
        $discount_text = '';
        $discount = round($final_price / $product->price ,2) * 100;
        if($discount < 90){
            $discount_text = '('. $discount . '%)';
        }
    @endphp
    <label class="final-price box">NT${{ number_format($final_price) }}{{$discount_text}}</label>
    @if($motors = $product->getMotors() and count($motors))
    <span class="name dotted-text1 force-limit">
        {{$motors->first()->name}}
    </span>
    @endif
    @if($options = $product->getOutletOptions() and count($options))
        <span class="name dotted-text1 force-limit">
            {{$options->first()->name}}
        </span>
    @endif
</div>