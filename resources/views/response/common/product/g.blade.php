{{--
CLASS G
Item information:
    image:90X90
    product name:2 line
    manufacturer:1 line
    Price:discount
    model:more then one will use ...
--}}

<div class="product-grid">
    <div class="product-grid-left">
        <a class="image-link" href="{{ route('product-detail' , [ $product->url_rewrite ]) }}">
            <figure class="zoom-image image-90-90">
                {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                <span class="helper"></span>
            </figure>
        </a>
    </div>
    <div class="product-grid-right">
        <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}">
            <span class="name product-name dotted-text2 force-limit">{{ $product->name }}</span>
        </a>
        <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
        @php
            $final_price = $product->getFinalPrice($current_customer);
            $discount_text = '';
            $discount = round($final_price / $product->price ,2) * 100;
            if($discount < 90){
                $discount_text = '('. $discount . '%)';
            }
        @endphp
        <label class="final-price dotted-text1">NT${{ number_format($final_price) }}{{$discount_text}}</label>
        <span class="name dotted-text1 force-limit">
        @if($motors = $product->getMotors() and count($motors))
                {{$motors->first()->name}}
        @endif
        </span>
    </div>
</div>