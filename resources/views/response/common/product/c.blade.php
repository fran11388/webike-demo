{{--
CLASS C
Item information:
    image:140X140
    product name:2 line
    manufacturer:1 line
    Price:without discount box(margin bottom),discount
    model:more then one will use ...
    review
--}}


<div class="product-grid">
    <a class="image-link" href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <figure class="zoom-image">
            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
            <span class="helper"></span>
        </figure>
    </a>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <span class="name product-name dotted-text2 force-limit">{!!  $product->name  !!}</span>
    </a>
    <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
    @php
        $final_price = $product->getFinalPrice($current_customer);
        $discount_text = '';
        $discount = round($final_price / $product->price ,2) * 100;
        if($discount < 90){
            $discount_text = '('. $discount . '%)';
        }
    @endphp
    <label class="final-price box">NT${{ number_format($final_price) }}{{$discount_text}}</label>
    <span class="name dotted-text1 force-limit">
        @if($motors = $product->getMotors() and count($motors))
            {{$motors->first()->name}}
        @endif
    </span>
    @php
        $review_info = $product->getReviewInfo();
    @endphp
    @if($review_info->count > 0)
        <div>
            <span class="icon-star star-{{ str_pad(round($review_info->average), 2,"0" , STR_PAD_LEFT)}}"></span>
            <a class="name icon-star-text" href="{{route('review-search',null)}}?sku={{$product->url_rewrite}}">{{$review_info->count}}</a>
        </div>
    @endif
</div>