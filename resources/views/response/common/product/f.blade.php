{{--
CLASS F
Item information:
    image:180X180(not finish)
    product name:2 line
    manufacturer:1 line
    Price:discount
    model:more then one will use ...
--}}


<div class="product-grid">
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <figure class="zoom-image image-180-180">
            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
        </figure>
    </a>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <span class="name product-name dotted-text2 force-limit">{!!  $product->name  !!}</span>
    </a>
    <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
    @php
        $final_price = $product->getFinalPrice($current_customer);
        $discount_text = '';
        $discount = round($final_price / $product->price ,2) * 100;
        if($discount < 90){
            $discount_text = '('. $discount . '%)';
        }
    @endphp
    <label class="final-price">NT${{ number_format($final_price) }}{{$discount_text}}</label>
    <span class="name dotted-text1 force-limit">
        @if($motors = $product->getMotors() and count($motors))
            {{$motors->first()->name}}
        @endif
    </span>

</div>