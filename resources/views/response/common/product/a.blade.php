{{--
CLASS A
Item information:
    image:140X140
    product name:2 line
    manufacturer:1 line
--}}


<div class="product-grid">
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <figure class="zoom-image">
            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
        </figure>
    </a>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <span class="name product-name dotted-text2 force-limit">{!!  $product->name  !!}</span>
    </a>
    <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
</div>