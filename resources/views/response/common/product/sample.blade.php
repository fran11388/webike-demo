{{--
CLASS D
Item information:
    image:140X140
    Title
    manufacturer
    Price(without discount)
    model(more then one will use ...)
--}}

@extends('response.layouts.1column')
@section('middle')
    <ul>
        <li class="item item-product-grid">
            <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
                <figure class="zoom-image">
                    <img src="{{$product->getThumbnail()}}" alt="{{$product->full_name}}">
                </figure>
            </a>
            <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
                <span class="title-product-item dotted-text2 force-limit">{!!  $product->name  !!}</span>
                <span class="title-product-item dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
            </a>
            <label class="price">NT${!!  number_format($product->getFinalPrice($current_customer))  !!}</label>
            <label class="price-old">NT${!!  number_format($product->price)  !!}</label>
            @php
                $review_info = $product->getReviewInfo();
            @endphp
            @if($review_info->count > 0)
                <div class="icon-star-review">
                    <span class="icon-star star-{{ str_pad(round($review_info->average), 2,"0" , STR_PAD_LEFT)}}"></span>
                    <a href="{{route('review-search',null)}}?sku={{$product->url_rewrite}}">{{$review_info->count}}</a>
                </div>
            @endif
        </li>
    </ul>
@stop
@section('script')

@stop