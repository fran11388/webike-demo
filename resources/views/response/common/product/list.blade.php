@extends('response.layouts.1column')
@section('middle')

    @foreach(range('a','i') as $word)
        <a href="{{route('product_block',['blade'=>$word,'owl'=>request('owl'),'category'=>request('category')])}}" class="btn bnt-ask-a-question2">{{$word}}</a>
    @endforeach
    <a href="{{route('product_block',['blade'=>$blade_type,'category'=>request('category')])}}" class="btn bnt-ask-a-question2">開啟輪播</a>
    <a href="{{route('product_block',['blade'=>$blade_type,'owl'=>'0','category'=>request('category')])}}" class="btn bnt-ask-a-question2">關閉輪播</a>
    <br/>
    分類:
    <form action="{{route('product_block',['blade'=>$blade_type,'owl'=>request('owl')])}}">
        <input type="text" value="{{request('category')}}" name="category">
        <input type="submit" value="更換">
    </form>
    <ul class="product-list {{$owl == 1 ? 'owl-carousel-history' : '' }}">
        @foreach($products as $product)
            <li class="">
                @include('response.common.product.'.$blade_type)
            </li>
        @endforeach
    </ul>
@stop
@section('script')
    <script>
        $('.owl-carousel-history').addClass('owl-carousel').owlCarousel({
            loop:false,
            nav:true,
            margin:5,
            slideBy : 6,
            URLhashListener:true,
            startPosition: 'URLHash',
            responsive:{
                0:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        });
        $('.owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
        $('.owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
        $('.owl-carousel2').find('.owl-prev').addClass('btn-owl-prev');
        $('.owl-carousel2').find('.owl-next').addClass('btn-owl-next');
    </script>
@stop