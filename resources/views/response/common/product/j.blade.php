{{--
CLASS j
Item information:
    image:140X140
    motor name:2 line
    manufacturer:1 line
    Price:discount
    model:more then one will use ...
--}}


<a class="image-box col-xs-block" href="{!! $new_motor->link !!}" title="{{$new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET}}" target="_blank">
    <figure class="zoom-image thumb-box-border">
        @if($motor_num == 0)
            <img src="{!! cdnTransform($new_motor->image) !!}" alt="{{$new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET}}">
        @else
            {!! lazyImage( cdnTransform($new_motor->image) , $new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET )  !!}
        @endif

    </figure>
</a>
<div class="content">
    <div class="manufacturer">
        {!! $new_motor->manufacurer!!}
    </div>
    <div class="name">
        <a class="dotted-text2 force-limit" href="{!! $new_motor->link !!}" title="{{$new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET}}" target="_blank">
            {!! $new_motor->motor_model_name!!}
        </a>
    </div>
    <div class="seller dotted-text1 force-limit">
        @if($new_motor->customer->group_id >= \Everglory\Constants\MotomarketCustomerGroup::CORPORATION)
            {{$new_motor->profiles->username}}
        @else
            個人自售
        @endif
    </div>
    <div class="price {!! $new_motor->price ? 'number font-color-red' : 'font-color-blue'!!}">
        {!! $new_motor->price ? '售價：' : '' !!}{!! $new_motor->price_text!!}
    </div>
</div>
