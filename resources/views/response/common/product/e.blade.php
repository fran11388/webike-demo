{{--
CLASS E
Item information:
    image:190X190
    product name:2 line
    manufacturer:1 line
    Price:without discount
    model:more then one will use ...
    review
--}}

<div class="product-item {{ isset($_COOKIE['listMode']) ? $_COOKIE['listMode'] : '' }}"
     data-sku="{{$product->url_rewrite}}">
    <div class="product-img">
        <a class="thumbnail thumbnail-pin search-list-img zoom-image"
           href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" target="_blank">
            {!! lazyImage( ThumborBuildWithCdn($product->getOrigImage(),190,190), $product->full_name, null,  'preview-img') !!}
            <span class="helper"></span>
            @if(count($product->getAllImages()->images) > 1)
                {!! lazyImage(assetRemote('image/icon_many_photo.png'), '', '', 'thumbnail-icon hidden-xs') !!}
            @endif
        </a>
    </div>
    <div class="product-img-gallery hide">
        @foreach($product->images as $img_key => $image)
            @if($img_key >= 10)
                @break
            @endif
            <a class="thumbnail search-list-img zoom-image" href="javascript:void(0)" target="_blank">
                {!! lazyImage( ThumborBuildWithCdn(($image->image ? $image->image : NO_IMAGE),190,190 ) , $product->full_name ) !!}
                <span class="helper"></span>
            </a>
        @endforeach
    </div>
    <div class="product-detail">
        <div class="product-info-search-list">
            <h3 class="product-name">
                <a class="dotted-text3" href="{{ route('product-detail' , [ $product->url_rewrite ]) }}"
                   target="_blank">
                    {{ $product->name }}
                </a>
            </h3>
            <div class="vendor-name dotted-text1 width-full">{{ $product->manufacturer->name }}</div>

            <?php
            $final_price = $product->getFinalPrice($current_customer);
            $final_point = $product->getFinalPoint($current_customer);
            $discount_text = '';
            $discount = round($final_price / $product->price, 2) * 100;
            if ($discount < 90) {
                $discount_text = '(' . $discount . '%)';
            }
            ?>
            <label class="price">NT$ {{ number_format($final_price) }}{{$discount_text}}</label>
            <div class="point-label dotted-text1 width-full">{{ $final_point }}點數回饋</div>

            <ul class="product-description hidden-xs">
                {!! $product->getFitCataloguesHtml() !!}
                <div class="model-name">商品編號：{{$product->model_number}}</div>
                <?php
                $summary_text = '';
                if ($product->type == 5) {
                    if ($options = $product->getOutletOptions() and count($options)) {
                        foreach ($options as $option) {
                            $summary_text .= '<li class="dotted-text dotted-text1 product-info-model"><span>' . $option->name . '</span></li>';
                        }
                    }
                } else {
                    if (strpos($product->summary, '<br>') !== false) {
                        $summaries = explode("<br>", $product->summary);
                    } else if (strpos($product->summary, '<br/>') !== false) {
                        $summaries = explode("<br/>", $product->summary);
                    } else {
                        $summaries = explode("\n", $product->summary);
                    }
                    foreach ($summaries as $summary) {
                        if ($summary) {
                            $summary_text .= '<li class="dotted-text dotted-text1 product-info-model"><span>' . str_replace('■', '', str_replace('/catalogue/', 'http://www.webike.net/catalogue/', strip_tags($summary, '<br>'))) . '</span></li>';
                        }
                    }
                }

                ?>

                {!! $summary_text !!}
            </ul>
        </div>
        <div class="product-state-search-list">
            <div class="tags clearfix">
                @php
                    if ($product->in_stock_flg != null){
                        $tags = $product->getTagsWithoutStock($current_customer);
                        $stock_tags = $product->getStockTagByInStockFlg($product);
                        if (array_key_exists('售完',$stock_tags)){
                            $tags =$stock_tags;
                        } else {
                        $tags = array_merge($stock_tags, $tags);
                        }

                    } elseif (isset($stock_informations)){

                        $tags = $product->getTagsWithoutStock($current_customer);
                        if(isset($stock_informations->{$product->sku}) and $stock_informations->{$product->sku}){
                            $stock_type = $stock_informations->{$product->sku};
                            $tags = array_merge($product->getStockTagByType($stock_type), $tags);
                        }
                    }else{
                        $tags = $product->getTags($current_customer);
                    }
                @endphp
                @if($tags)
                    @foreach($tags as $tagName => $tagClass)
                        <span class="{!! $tagClass !!}">{{$tagName}}</span>
                    @endforeach
                @endif
            </div>
            <div class="div-btn-check-detail container product-detail-button">
                <a href="{{ route('product-detail' , [ $product->url_rewrite ]) }}" title="{{$product->full_name}}"
                   target="_blank">
                    <span>商品詳細</span>
                </a>
            </div>
            @php
                $review_info = $product->getReviewInfo();
            @endphp

            <div class="rating col-md-12">
                <div class="container icon-star-container">
                    @if($review_info->count > 0)
                        <span class="icon-star star-{{ str_pad(round($review_info->average), 2,'0' , STR_PAD_LEFT) }} icon-star-element star-fix"></span>
                        <a class="name icon-star-text icon-star-element"
                           href="{{route('review-search',null)}}?sku={{$product->url_rewrite}}">({{$review_info->count}}
                            件)</a>
                    @else
                        <span class="icon-star star-hide icon-star-element star-fix"></span>
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>