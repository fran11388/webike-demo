{{--
CLASS D
Item information:
    image:140X140
    product name:2 line
    manufacturer:1 line
    Price:without discount
    model:more then one will use ...
    review
--}}


<div class="product-grid">
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <figure class="zoom-image">
            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
        </figure>
    </a>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <span class="name product-name dotted-text2 force-limit">{!!  $product->name  !!}</span>
    </a>
        <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>

    <label class="final-price box">NT${!!  number_format($product->getFinalPrice($current_customer))  !!}</label>
    <span class="name dotted-text1 force-limit">
        @if($motors = $product->getMotors() and count($motors))
            {{$motors->first()->name}}
        @endif
    </span>

</div>