{{--
CLASS H
Item information:
    image:90X90
    product name:2 line
    manufacturer:1 line
    Price:discount
    model:more then one will use ...
--}}

<div class="product-grid product-grid-ranking">
    <p class="lank lank-0{{$loop->iteration}}">{{$loop->iteration}}</p>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}" title="{{ $product->full_name }}">
        {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
        {{--<span class="btn btn-primary">在庫有り</span>--}}
    </a>
    <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>
    <span class="name dotted-text1 force-limit">
        @if($motors = $product->getMotors() and count($motors))
            {{$motors->first()->name}}
        @endif
    </span>
</div>