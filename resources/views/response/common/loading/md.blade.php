<style type="text/css">
    .md-floatingCirclesG{
        position:relative;
        width:272px;
        height:272px;
        margin:auto;
        transform:scale(0.6);
        -o-transform:scale(0.6);
        -ms-transform:scale(0.6);
        -webkit-transform:scale(0.6);
        -moz-transform:scale(0.6);
    }

    .md-f_circleG{
        position:absolute;
        background-color:rgb(255,255,255);
        height:49px;
        width:49px;
        border-radius:26px;
        -o-border-radius:26px;
        -ms-border-radius:26px;
        -webkit-border-radius:26px;
        -moz-border-radius:26px;
        animation-name:md-f_fadeG;
        -o-animation-name:md-f_fadeG;
        -ms-animation-name:md-f_fadeG;
        -webkit-animation-name:md-f_fadeG;
        -moz-animation-name:md-f_fadeG;
        animation-duration:1.2s;
        -o-animation-duration:1.2s;
        -ms-animation-duration:1.2s;
        -webkit-animation-duration:1.2s;
        -moz-animation-duration:1.2s;
        animation-iteration-count:infinite;
        -o-animation-iteration-count:infinite;
        -ms-animation-iteration-count:infinite;
        -webkit-animation-iteration-count:infinite;
        -moz-animation-iteration-count:infinite;
        animation-direction:normal;
        -o-animation-direction:normal;
        -ms-animation-direction:normal;
        -webkit-animation-direction:normal;
        -moz-animation-direction:normal;
    }

    .md-frotateG_01{
        left:0;
        top:111px;
        animation-delay:0.45s;
        -o-animation-delay:0.45s;
        -ms-animation-delay:0.45s;
        -webkit-animation-delay:0.45s;
        -moz-animation-delay:0.45s;
    }

    .md-frotateG_02{
        left:32px;
        top:32px;
        animation-delay:0.6s;
        -o-animation-delay:0.6s;
        -ms-animation-delay:0.6s;
        -webkit-animation-delay:0.6s;
        -moz-animation-delay:0.6s;
    }

    .md-frotateG_03{
        left:111px;
        top:0;
        animation-delay:0.75s;
        -o-animation-delay:0.75s;
        -ms-animation-delay:0.75s;
        -webkit-animation-delay:0.75s;
        -moz-animation-delay:0.75s;
    }

    .md-frotateG_04{
        right:32px;
        top:32px;
        animation-delay:0.9s;
        -o-animation-delay:0.9s;
        -ms-animation-delay:0.9s;
        -webkit-animation-delay:0.9s;
        -moz-animation-delay:0.9s;
    }

    .md-frotateG_05{
        right:0;
        top:111px;
        animation-delay:1.05s;
        -o-animation-delay:1.05s;
        -ms-animation-delay:1.05s;
        -webkit-animation-delay:1.05s;
        -moz-animation-delay:1.05s;
    }

    .md-frotateG_06{
        right:32px;
        bottom:32px;
        animation-delay:1.2s;
        -o-animation-delay:1.2s;
        -ms-animation-delay:1.2s;
        -webkit-animation-delay:1.2s;
        -moz-animation-delay:1.2s;
    }

    .md-frotateG_07{
        left:111px;
        bottom:0;
        animation-delay:1.35s;
        -o-animation-delay:1.35s;
        -ms-animation-delay:1.35s;
        -webkit-animation-delay:1.35s;
        -moz-animation-delay:1.35s;
    }

    .md-frotateG_08{
        left:32px;
        bottom:32px;
        animation-delay:1.5s;
        -o-animation-delay:1.5s;
        -ms-animation-delay:1.5s;
        -webkit-animation-delay:1.5s;
        -moz-animation-delay:1.5s;
    }



    @keyframes f_fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-o-keyframes md-f_fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-ms-keyframes md-f_fadeG{
    0%{
        background-color:rgb(0,0,0);
    }

    100%{
        background-color:rgb(255,255,255);
    }
    }

    @-webkit-keyframes md-f_fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-moz-keyframes md-f_fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }
</style>
<div class="loading-box">
    <div class="md-floatingCirclesG">
        <div class="md-f_circleG md-frotateG_01"></div>
        <div class="md-f_circleG md-frotateG_02"></div>
        <div class="md-f_circleG md-frotateG_03"></div>
        <div class="md-f_circleG md-frotateG_04"></div>
        <div class="md-f_circleG md-frotateG_05"></div>
        <div class="md-f_circleG md-frotateG_06"></div>
        <div class="md-f_circleG md-frotateG_07"></div>
        <div class="md-f_circleG md-frotateG_08"></div>
    </div>
</div>
