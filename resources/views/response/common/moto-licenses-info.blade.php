<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="content-language" content="zh-tw">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    @include('response.layouts.partials.head')
    <style type="text/css">
        @import url(http://fonts.googleapis.com/earlyaccess/notosanstc.css);
        .page{
            width: 96%;
            margin:0 auto;
            font-family: 'Noto Sans TC', sans-serif;
            font-size: 14px;
            font-weight: bold;
            line-height: 20px;
            letter-spacing: 1px;
            padding: 2%;
        }
        ul{
            margin: 0;
            padding: 0;
        }
        ul li{
            text-decoration: none;
            list-style: none;
        }
        a{
            color:#06c;
            text-decoration:none;
        }
        a:hover{
            color: #359aff;
        }
        a img:hover{
            opacity: 0.8;
        }
        .page .border-box{
            outline:1px #777 solid;
        }
        .page h1,.page h2, .page h3,.page h4, .page h5{
            margin:0 0 10px;
            color: #000;
        }
        .page h1{
            font-size: 20px;
        }
        .page h2, .page h3,.page h4, .page h5{
            font-size: 16px;
        }
        .page .box{
            margin: 0 0 10px 0;
        }
        .page .text{

        }
        .page .tips{
            color:red;
        }
        .page .examples{

        }
        .page .examples .item{
            float:left;
            width: 49%;
            margin: 0 2% 10px 0;
        }
        .page .examples .item img{
            width: 100%;
        }
        .page .examples .item:nth-child(2n){
            margin: 0 0 10px 0;
        }
        .page .clearfix{
            overflow: hidden;
            zoom:1;
        }
        .page .clearfix:before, .page .clearfix:after{
            content:"";
            display: table-cell;
        }
        .page .btn-ui{
            width: 100%;
            font-size: 0;
            text-align: center;
        }
        .page .btn-ui a, .page .btn-ui button{
            padding: 5px 7.5px;
            text-align: center;
        }
        @media only screen and (max-width:610px){
            .page .response{
                width: 100% !important;
            }
            .page .tips li{
                margin: 0 0 5px 0;
            }
        }
        @media only screen and (min-width:900px){
            .page{
                width: 800px;
                padding: 20px;
            }
        }
    </style>
    @include('response.layouts.partials.script')
    <!-- GA -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41366438-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- GA END -->
</head>
<body>
<div class="page">
    <h1>
        車輛機號
    </h1>
    <ul class="box">
        <li>※部分技術性改裝品需要確認車輛的形式，以確保可以安裝。</li>
        <li>※車輛機號在每輛車的<font color="red">”行照”</font>上都會註明，請看以下範例。</li>
    </ul>
    <hr>
    <div class="examples clearfix box">
        <div class="item response">
            <h3>
                YAMAHA 行照範例
            </h3>
            <div class="photo">
                <img src="https://img.webike.tw/assets/images/shared/domestic-license-4-1.png">
            </div>
        </div>
        <div class="item response">
            <h3>
                KYMCO 行照範例
            </h3>
            <div class="photo">
                <img src="https://img.webike.tw/assets/images/shared/domestic-license-4-2.png">
            </div>
        </div>
    </div>
    <div class="text-center">
        <button class="btn btn-danger" onclick="window.close();">關閉視窗</button>
    </div>
</div>
</body>
</html>