<!-- DFP -->
<script src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script type='text/javascript'>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>


<script type='text/javascript'>
    googletag.cmd.push(function() {
        // 300 x 250 ad
        googletag.defineSlot('/31090521/news-300x250', [300, 250], 'div-gpt-ad-1464001278280-0').addService(googletag.pubads());
        // header ad
        googletag.defineSlot('/31090521/Banner', [500, 75], 'div-gpt-ad-1407734900911-0').addService(googletag.pubads());
        // footer ad
        googletag.defineSlot('/31090521/WT_BTM_728X90', [728, 90], 'div-gpt-ad-1409220286385-0').addService(googletag.pubads());
        // side ad
        googletag.defineSlot('/31090521/WT_SIDE_160X600', [160, 600], 'div-gpt-ad-1488521677577-0').addService(googletag.pubads());

        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });
</script>
<!-- DFP END-->