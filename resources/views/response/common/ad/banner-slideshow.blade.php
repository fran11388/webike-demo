<!-- Ad banner slideshow -->
@if(isset($advertiser))
    @if(isset($lazy_load) && !$lazy_load)
        {!! $advertiser->call($ad_id,'',false) !!}
    @else
        {!! $advertiser->call($ad_id) !!}
    @endif
@endif
