@php

    $current_customer_role_id = Everglory\Constants\CustomerRole::GENERAL;
    
    if($current_customer){
        $current_customer_role_id = $current_customer->role_id;
    }
    
    $installment_setting = \Ecommerce\Shopping\Payment\CoreInstallment::getInstallHtmlSetting($current_customer_role_id);
@endphp
<div class="history-table-container block">
    <ul class="history-info-table history-table-adjust">
        <li class="history-table-title">
            <ul>
                <li class="col-lg-2 col-md-2 col-sm-{!! $installment_setting->col_sm !!} col-xs-12"><span class="size-08rem">分期期數</span></li>
                @if(in_array($current_customer_role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF]))
                    <li class="col-lg-4 col-md-4 col-sm-{!! $installment_setting->col_sm !!} hidden-xs"><span class="size-08rem">分期總價</span></li>
                @endif
                <li class="col-lg-{!! $installment_setting->col_lg[0] !!} col-md-{!! $installment_setting->col_md[0] !!} col-sm-{!! $installment_setting->col_sm !!} hidden-xs"><span class="size-08rem">首期金額</span></li>
                <li class="col-lg-{!! $installment_setting->col_lg[1] !!} col-md-{!! $installment_setting->col_md[1] !!} col-sm-{!! $installment_setting->col_sm !!} hidden-xs"><span class="size-08rem">其他期金額</span></li>
            </ul>
        </li>
        <li class="history-table-content">
        </li>
    </ul>
    @include('response.common.ad.banner-installment')
</div>