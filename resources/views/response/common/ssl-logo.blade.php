<!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->

@if(env('APP_DEBUG', false))
    <img src="{!! assetRemote('image/logo_secured.jpg') !!}" alt=""></a>
@else
    <script language="javascript" type="text/javascript" src="https://seal.geotrust.com/getgeotrustsslseal?host_name=www.webike.tw&amp;size=S&amp;lang=en"></script>
@endif

