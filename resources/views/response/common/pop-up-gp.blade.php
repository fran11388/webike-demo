<style type="text/css">
    .popups-ui.popups-ui-cover.popups-motogp-cover{
        background: rgba(0, 0, 0, 0.9);
    }
    .popups-ui-window.popups-motogp{
        z-index: 1100;
        height: 100%;
        width: 540px;
        margin: 150px auto 0;
        padding-top: 0px;
    }
    .popups-ui-window.popups-motogp .popups-ui-window-close{
        right: 15px;
        top: 15px;
        z-index: 1110;
    }
    .popups-ui-window.popups-motogp .content{
        height: 100%;
        text-align: center;
        font-size: 0;
        padding-top: 5%;
    }
    @media (max-width: 767px) {
        .popups-ui-window.popups-motogp{
            width: 100%;
            margin: 90px auto 0;
        }
        .popups-ui-window.popups-motogp .content {
            padding-top: 20%;
        }
    }
</style>
<div class="popups-ui popups-ui-cover popups-motogp-cover">
    <div class="popups-ui-window popups-motogp">
        <div class="popups-ui-window-close">
            <img src="{!! assetRemote('image/customer/account/pop-ups-close.png') !!}">
        </div>
        <div class="content">
            <a id="popups-full-screen" href="javascript:void(0)">
                <img class="hidden-lg hidden-md hidden-sm" src="{!! assetRemote('image/pop-up/MOTO-GP-300400.jpg') !!}" alt="2019-motogp">
                <img class="hidden-xs" src="{{$propaganda->pic_path}}" alt="{{$propaganda->text}}">
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    var pop_ups_full_redirect = false;
    $(document).on('click', '.popups-ui-window.popups-motogp .popups-ui-window-close', function(){
        $(this).closest('.popups-ui.popups-motogp-cover').fadeOut();
        PopUpFullScreenOut();
    });
    $(document).on('click', '.popups-ui-window.popups-motogp #popups-full-screen', function(){
        pop_ups_full_redirect = true;
        $(this).closest('.popups-ui.popups-motogp-cover').fadeOut();
        PopUpFullScreenOut();
    });

    function PopUpFullScreenOut(){
        $.ajax({
            url: "{!! route('customer-session', 'pop-ups-full-screen') !!}",
            method: "POST",
            data: {},
            dataType: "html",
            success: function(html){
                if(pop_ups_full_redirect){
                    var url = "{!!route('benefit-event-motogp-2019')!!}";
                    window.location.href = url;
                }
            },
            error: function(error){
                console.log(error);
            },
        });
        ga('send', 'event', 'link', 'motogp-2019', 'pop-up-full');
    }
</script>