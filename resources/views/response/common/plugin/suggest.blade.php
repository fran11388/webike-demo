<link rel="stylesheet" href="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.css') }}">
<style>
    .ui-menu{
        z-index: 9999;
    }
    .ui-menu .ui-menu-item a.ui-menu-item-wrapper{
        display: block;
    }
    .ui-menu .ui-menu-item a.ui-menu-item-wrapper:hover span{
        color: #fff;
        font-weight: bold;
    }
</style>
<script src="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.js') }}"></script>
<script>

    $(function() {
        $( "#search_bar" ).autocomplete({
            minLength: 1,
            source: solr_source,
            focus: function( event, ui ) {
                $('#search_bar').val();
                return false;
            },
            select: function( event, ui ) {
                ga('send', 'event', 'suggest', 'select', 'header');
                location.href = ui.item.href;
                return false;
            },
            change: function(event, ui) {

            }
        })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
            var bag = $( "<li>" );
            if( item.value == 'cut' ){
                return bag.addClass('cut').append('<hr>').appendTo( ul );
            } else if(item.value == '-separate-'){
                return bag.appendTo( ul );
            }



//            console.log(bag);
            return bag
            // .append( '<a href="http://localhost/test">' + item.icon + "<p>" + item.label + "</p></a>" )
                .append( '<a  href="'+ item.href +'">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                .appendTo( ul );
        };
    });
    var current_suggest_connection = null;
    function solr_source(request, response){
        var params = {q: request.term};

        current_suggest_connection = $.ajax({
            url: "{{ route('api-suggest')}}",
            method:'GET',
            data : params,
            dataType: "json",
            beforeSend: function( xhr ) {
                if(current_suggest_connection){
                    current_suggest_connection.abort();
                }
            },
            success: function(data) {
                response(data);
            }
        });
    }

</script>