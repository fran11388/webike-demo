@if($current_customer)
    <?php
    $customer_motor = $current_customer->motors;
    ?>
    @if(count($customer_motor))
        <li class="my-bike animated fadeInRight">
            <a href="javascript:void(0)" class="btn-my-bike visible-md visible-lg"> <span>MyBike商品搜尋</span>
                <p><i class="fa fa-chevron-down" aria-hidden="true"></i></p>
            </a>
            <a href="" class="btn-my-bike-tablet font-color-red visible-sm">
                <i class="fa fa-motorcycle size-15rem" aria-hidden="true"></i>
            </a>

            <ul class="show-my-bike">
                @foreach ($customer_motor as $my_motor)
                    <li>
                        <a class="select-motor-mybike" href="javascript:void(0)" name="{{ $my_motor->url_rewrite }}" title="{{ $my_motor->name . $tail }}">
                            {{ $my_motor->name }}
                        </a>
                    </li>
                @endforeach

                <li class="btn-add-more-bike">
                    <a href="{{ route('customer-mybike') }}" target="_blank">
                        <i class="fa fa-plus" aria-hidden="true"></i> 編輯MyBike
                    </a>
                </li>
            </ul>
        </li>
        <script type="text/javascript">
            @php
                $route_name = Route::currentRouteName();
            @endphp
            $('.select-motor-mybike').click(function(){
                var value = $(this).prop('name');
                console.log(value);
                if (value){
                    @if(strpos($route_name, 'review') === false)
                        window.location = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl([],['mt' => '%s'])) }}", value );
                    @else
                        @if(strpos($route_name, 'review') !== false)
                            var url = "{!! modifyGetParameters(['page' => '', 'mt'=> 'mtUrlRewrite'], route('review-search', '')) !!}";
                        @endif
                        window.location = url.replace('mtUrlRewrite', value);
                    @endif
                }
            });
        </script>
    @endif
@else
    <li class="my-bike animated"><a class="btn-danger btn" href="{{ route('customer-mybike') }}" class="btn-login font-color-red">登錄MyBike</a></li>
@endif