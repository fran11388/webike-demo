@if(!isset($current_motor) and !$current_motor and (isset($fit_motor_products) and count($fit_motor_products)))
    @if(count($my_bikes))
        <li class="my-bike animated fadeInRight content-my-bike box">
            <a href="javascript:void(0)" class="btn-my-bike"> <span>MyBike商品搜尋</span>
                <p><i class="fa fa-chevron-down" aria-hidden="true"></i></p>
            </a>
            <!--
            <a href="" class="btn-my-bike-tablet font-color-red visible-sm visible-xs">
                <i class="fa fa-motorcycle size-15rem" aria-hidden="true"></i>
            </a>
            -->

            <ul class="show-my-bike">
                @if(count($my_bikes))
                    @foreach ($my_bikes as $motor)
                        @if($motor->count)
                            <li>
                                @php
                                    $route_name = Route::currentRouteName();
                                @endphp
                                @if(strpos($route_name, 'summary') !== false)
                                    <a href="{{ modifySummaryUrl(['mt'=>$motor->url_rewrite],true) }}" title="{{$motor->name . $tail}}">
                                        {{ $motor->name }}
                                    </a>
                                @elseif($route_name == 'brand')
                                    <a href="{{ modifyGetParameters(['page' => '', 'motor'=> $motor->url_rewrite]) }}" title="{{$motor->name . $tail}}">
                                        {{ $motor->name }}
                                    </a>
                                @else
                                    <a href="{{ modifyGetParameters(['page' => '', 'mt'=> $motor->url_rewrite]) }}" title="{{$motor->name . $tail}}">
                                        {{ $motor->name }}
                                    </a>
                                @endif
                            </li>
                        @endif
                    @endforeach
                @endif

                <li class="btn-add-more-bike">
                    <a href="{{ route('customer-mybike') }}" target="_blank">
                        <i class="fa fa-plus" aria-hidden="true"></i> 編輯MyBike
                    </a>
                </li>
            </ul>
        </li>
    @else
        <li class="my-bike animated fadeInRight content-my-bike box"><a charset="btn-danger btn-my-bike-add" href="{{ route('customer-mybike') }}" class="btn-login font-color-red">登錄MyBike</a></li>
    @endif
@endif