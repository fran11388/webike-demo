<li class="li-left-title col-md-12 visible-lg visible-md visible-sm">
    <div class="slide-down-btn">
        <a href="javascript:void(0)" class="a-left-title">
            <span>銷售排行</span>
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </a>
    </div>
    <ul class="ul-sub1 row box-content-popularity">
        @foreach($top_sales_products as $product)
            <li class="li-sub1 li-sub1-style2 col-md-12">
                @include('response.common.product.h')
            </li>
        @endforeach
    </ul>
</li>
