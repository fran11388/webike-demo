@if($current_customer)
    <div class="box-ct-recommond-fix box-content custom-parts">
        <div class="title-main-box title-detail-info col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2>瀏覽紀錄</h2>
        </div>
        <div class="ct-inspired-by-your col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <ul class="ul-ct-inspired-by-your">
                @foreach ($recent_views as $product)
                    @if($loop->iteration % 7 == 1 and $loop->remaining > 7 )
                        <li class="item-product-grid" {{$loop->iteration == 1 ? '"active"' : ''}}>
                            <a class="btn-tab" href="#item-{{$loop->iteration}}">
                                <figure class="zoom-image thumb-box-border">
                                    <img src="{{$product->getThumbnail()}}" alt="{{$product->full_name}}">
                                </figure>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <div class="tab-content ct-tap-inspired-by-your">
                <div id="home" class="container-tap-inspired-by-your tab-pane fade in active">
                    <ul class="product-list owl-carousel-history">
                        @foreach ($recent_views as $product)
                            <div data-hash="item-{{$loop->iteration}}">
                                <li>
                                    @include('response.common.product.c')
                                </li>
                            </div>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        </div>
@endif