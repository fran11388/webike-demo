<style>
    .history-box-outside-content .title-box-page-group a.history-btn-close{
        padding: 2px 5px;
    }
</style>

@if(isset($disps_active) and $disps_active and isset($disps) and $disps->count())
    <div class="box-page-group history-box-outside-content">
        <div class="title-box-page-group">
            <a class="history-btn-chevron-down" href="javascript:void(0)"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            <h3>訊息通知</h3>
            <a class="history-btn-close" href="javascript:void(0)"><i class="fas fa-times"></i></a>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group" style="display: block;">
                @foreach($disps as $disp)
                    @if(!isset($ticket) or !$disp->group or !$ticket->increment_id == $disp->code)
                        <li>
                            <a class="disp" href="{{$disp->link}}" title="{{$disp->name}}" name="{{$disp->group . '@' . $disp->code}}" target="_blank"><i class="fa fa-chevron-right" aria-hidden="true"></i><span> {{$disp->name}}</span></a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
@endif