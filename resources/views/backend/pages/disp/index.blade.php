@extends('backend.layouts.default')
@section('style')
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" /> 
    <style>
    	.start{
			    float: left;
				margin-left: 17%;
    	}
    	.maturity{
    			float: left;
    	}

    </style>
@stop
@section('middle')
<div>
	<form  id="disp-form" action="{!! route('backend-postdisp-massage') !!}" method="post">
		<div class="panel-body clearfix">
			
			<div class="start block">
				<div class="col-md-4 col-sm-3 col-xs-3 ">
					<span>預約開始時間</span>
				</div>
				<div class="col-md-8 col-sm-9 col-xs-9">
                    <input  class="form-control" type='datetime-local' placeholder="From" name="start" id="start" >
                </div>
			</div>
			
			<div class="maturity block ">
				<div class="col-md-3 col-sm-3 col-xs-3 text-right">
					<span>到期時間</span>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input  class="form-control" type='datetime-local' placeholder="From" name="maturity" id="maturity" >
				</div>
			</div>
		</div>
        <div class="method-container clearfix block">
            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                <span class="size-10rem">發送方式</span>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">

                    <select class="form-control" name="select_method">
                        <option value="0" name="sand" selected>自訂客戶</option>
                        <option value="1" name="sand">發送所有客戶</option>
                    </select>

            </div>
        </div>
        <div class="method-container clearfix block ">
            <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                <span class="size-10rem">指定發送客戶</span>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <select class="width-full" name="customer_ids[]" id="customer_ids[]" multiple>
                   
                </select>
            </div>
        </div>
		<div class="method-container clearfix block">
			<div class="col-md-3 col-sm-3 col-xs-3 text-right">
				<span class="width-full">訊息內容</span>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input class="width-full" type="text" name="info" id="info" placeholder="超過14個中文字將跳行顯示"/>
			</div>
		</div>
		<div class="method-container clearfix block">
			<div class="col-md-3 col-sm-3 col-xs-3 text-right">
				<span class="width-full">文字連結</span>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input class="width-full" type="text" name="link" id="link" />
			</div>
		</div>
		<div class="text-right">
			<input class="btn btn-warning" type="button"  value="即時測試發送" name="submitbtn" id="testbutton" >
		   	<input  class="btn btn-danger " type="submit" value="確認送出" name="submitbtn" id="disp-submitbtn">
            <div class=" send-text hidden">
                    發送中...
            </div>
	   	</div>
	</form>
</div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script>
    	@if(session('error_log'))
    		alert("{{ session('error_log') }}");
    	@endif

        $('#disp-submitbtn').click(function(){
           send(); 
        });
        
        function send(){
    	 	$('.send-text').removeClass('hidden');
            $('#testbutton').addClass('disabled');
            $('#disp-submitbtn').addClass('disabled');
        }

        function sendEnd(){
            $('.send-text').addClass('hidden');
            $('#testbutton').removeClass('disabled');
            $('#disp-submitbtn').removeClass('disabled');
        }

         $('#testbutton').click(function(){
            // alert('123');
            send();
    	 	// console.log($('#disp-form').serialize());
        	$.ajax({
        		url: "{!! route('backend-testpostdisp-massage') !!}",
        		data:$('#disp-form').serialize(),
        		type:"POST",
        		dataType:'JSON',
        		success:function(result){
                    sendEnd();
        			swal({
						  position: 'top-right',
						  type: 'success',
						  title: '測試發送完成',
						  showConfirmButton: false,
						  timer: 1500
					})
        			
        		},
        		error:function(xhr,ajaxOptions,thrownError,textStatus){
        			alert(thrownError);
        		}
        	});
        });


        $(document).ready(function(){
            $('#datetimepicker4').datetimepicker({
                format:      'YYYY-MM-DD HH:mm:ss'
            });

            $('select[name^="customer_ids"]').select2({
                width : "100%", //若你的版面是 RWD 可以這樣，讓他重新調整寬度
                placeholder : "輸入關鍵字搜尋會員",

                //使用如$.ajax 參閱 ajax
                ajax :
                {
                    url: "{!! route('backend-api-customerNames') !!}",
                    //select2 預設使用json, 若要跨網域使用 jsonp，我個人偏愛html後再自己解碼
                    dataType: 'json',
                    delay: 500,
                    //我使用POST傳送
                    type: "POST",
                    //夾帶參數
                    data: function (param)
                    {
                        return { search_text: param.term };
                    },

                    //成功返回的項目
                    processResults: function (result)
                    {
                        //因為 dataType: 'html' 所以要自己解碼喔。但用html可以方便debug。
                        if(result.success){
                            return {results: result.datas};
                        }
                        var empty = [{id:0, text: null}];
                        return {results: empty};
                    }
                }
            });

            $('[name="select_method"]').change(function(){
                var parent = $('select[name^="customer_ids"]').parents('.method-container');
                if(parent.hasClass('hide')){
                    parent.removeClass('hide');
                }else{
                    parent.addClass('hide');
                }
            });
        });

        $("#create-form").submit(function(e){
            $('.basic-set input').each(function(){
                if(!$(this).val() && !$.isNumeric($(this).val())){
                    alert('欄位尚未輸入');
                    e.preventDefault();
                }
            })
        });

   




    </script>
@stop