@extends('backend.layouts.default')
@section('style')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
    <style>
        .file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
            float: left;
        }

    </style>
@stop
@section('middle')
    <div class="container clerafix">
        <div class="clerafix upload col-md-6">
            <form id="form" action="{!! route('backend-import-data') !!}" method="post" enctype="multipart/form-data">
                <input  class="file-upload" type="file" name="file" id="fileToUpload">
                <input class="btn btn-danger send-btn" type="button" value="確認完成" name="button">
            </form>
            <div class="btn-gap-top">
                <span class="font-color-red ">※匯入前請再次檢查資料是否正確</span>
            </div>
        </div>
        <div class="download col-md-6">
            <a href="\vendor\doc\weekly-promotion.xlsx"><button class="btn btn-warning">下載範本</button></a>
        </div>
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $('.upload input.send-btn').click(function(){
            swal({ 
                    title: '請確認匯入資料是否正確', 
                    type: 'warning',
                    showCancelButton: true, 
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '確定送出！', 
                    cancelButtonText: '取消', 
                }).then(function(){
                    $('#form').submit();
                })
        });
    </script>

@stop