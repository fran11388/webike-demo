@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            max-height:190px;
            min-width:120px;
            min-height:120px;
        }
        .active-select {
            float:right;
            width:150px;
            margin-right:15px;
        }
    </style>
@stop
@section('middle')
    <form method="post" action="{{\URL::route('backend-post-campaign')}}" class="row form clearfix">
        <div class="active-select block">
            <select class="select2" name="active_type">
                <option value="" selected>批次</option>
                <option value="open">開啟</option>
                <option value="close">關閉</option>
                <option value="delete">刪除</option>
            </select>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-book"></i>&nbsp週替列表</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th><input id="checkAll" type="checkbox"></th>
                                <th>網址名稱</th>
                                <th>名稱</th>
                                <th>週次</th>
                                <th>開始時間</th>
                                <th>狀態</th>
                                <th>預覽頁面</th>
                                <th>匯入時間</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($campaigns as $campaign)
                                <tr>
                                    <td><input class="btn btn-default" name="check[]" type="checkbox" value="{{$campaign->id}}"></td>
                                    <td>{{ $campaign->url_rewrite }}</td>
                                    <td>{{ $campaign->title }}</td>
                                    <td>
                                        {{ $campaign->year}}年第{{ $campaign->week }}週
                                    </td>
                                    <td>{{ $campaign->publish_at }}</td>
                                    <td>{{ $campaign->active? "開啟": "關閉"}}</td>
                                    <td>
                                      <a href="{{route('campaigns-promotion',$campaign->url_rewrite)}}" target="_blank">預覽</a>
                                    </td>
                                    <td>
                                      {{ $campaign->created_at }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="text-center">
        {!! $campaigns->render() !!}
    </div>
@stop
@section('script')
        <script>
            $("#checkAll").change(function () {
                $("input[name='check[]']").prop('checked', $(this).prop("checked"));
            });

            $(document.body).on("change",".select2",function(){
                var check = false;
                var choose = this.value;
                if(choose){
                    var title = "你確定要" + choose + "嗎";

                    $("input[name='check[]']").each(function (event) {
                        if ($(this).prop("checked")) {
                            check = true;
                        }
                    });

                    if (check){
                        var form = $('.form');
                        swal({
                            title: title,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "確定",
                            cancelButtonText: '取消',
                            closeOnConfirm: false
                        }).then(function () {
                            form.submit();
                        });
                    }else{
                        alert('未選擇項目');
                    }
                }
            });
        </script>
@stop