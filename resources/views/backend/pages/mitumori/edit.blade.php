@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .list-group-item{
            background-color: #fff;
            border: 1px solid #ddd;
            display: block;
            margin-bottom: -1px;
            padding: 10px 15px;
            position: relative;
        }

        .list-group-item.active {
            background-color: #00aff0;
            border-color: #00aff0;
            color: #fff;
            z-index: 2;
        }

        .list-group-item:first-child {
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        hr {
            border-style: solid none none;
            border-width: 1px 0 0;
            margin-bottom: 17px;
            margin-top: 17px;
            border-color: #eee;
        }

        .radius-div {
            background-color: #fff;
            border: 1px solid #e6e6e6;
            border-radius: 5px;
            box-shadow: 0 2px 0 rgba(0, 0, 0, 0.1), 0 0 0 3px #fff inset;
            margin: 0px 20px 20px;
            padding: 20px;
            position: relative;
        }

        .form-footer {
            background-color: #fcfdfe;
            height: 73px;
            margin: 15px -20px -20px;
            border-bottom-left-radius: 2px;
            border-bottom-right-radius: 2px;
            padding: 10px 15px;
            border-top: 1px solid #eee;
        }
        .brand-control span.checkbox{
            min-height: 0px;
            padding-top: 0;
        }
    </style>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false"
                 data-widget-togglebutton="false" data-widget-deletebutton="false">
                <div>
                    <div class="widget-body">

                        <div class="tabs-left">
                            <ul class="nav nav-tabs tabs-left" id="mitu-pill-nav">
                                <li class="active">
                                    <a href="#tab-r1" data-toggle="tab">
                                        @if($entity->status)
                                            <span class="badge bg-color-blue txt-color-white"><i
                                                        class="fa fa-check"></i></span>
                                        @else
                                            <span class="badge bg-color-blueDark txt-color-white"><i
                                                        class="fa fa-times"></i></span>
                                        @endif
                                        狀態
                                    </a>
                                </li>
                                @foreach ($entity->items  as  $item)
                                    <li class="">
                                        <a href="#tab-{{$item->id}}" data-toggle="tab">
                                            @if($item->product_check)
                                                <span class="badge bg-color-blue txt-color-white"><i
                                                            class="fa fa-check"></i></span>
                                            @else
                                                <span class="badge bg-color-blueDark txt-color-white"><i
                                                            class="fa fa-times"></i></span>
                                            @endif
                                            <br/>{{str_limit($item->syouhin_name,4)}}</a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-r1">
                                    <div class="radius-div">
                                        <fieldset>
                                            <div class="form-group  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label">客戶姓名</label>
                                                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                    {{$entity->customer->realname}}
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label">客戶屬性</label>
                                                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                    {{$entity->customer->role->name}}
                                                    <input type="hidden" value="{{$entity->customer->role->id}}"
                                                           id="role_id">
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label">狀態</label>
                                                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                    {{$entity->status ? '已回覆' : '尚未回覆' }}
                                                </div>
                                            </div>

                                        </fieldset>
                                        @if($entity->status == 0)
                                            <div class="form-footer">
                                                <form action="{{URL::route('backend-mitumori-edit-post',$entity->id)}}"
                                                      method="POST" id="form_estimate">
                                                    <button class="btn btn-default pull-right"
                                                            style="text-align: center;font-size:12px;padding: 4px 8px;margin-right:5px;"
                                                            type="submit">
                                                        <i class="fa fa-fw fa-lg  fa-save"
                                                           style="font-size: 28px;display: block;width:30px;height:28px;margin:0 auto;line-height:1;"></i>
                                                        提交
                                                    </button>
                                                </form>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @foreach ($entity->items  as  $item)
                                    <div class="tab-pane" id="tab-{{$item->id}}">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>商品品牌</th>
                                                <th>商品名稱</th>
                                                <th>商品編號</th>
                                                <th>TYPE(顏色/尺寸)</th>
                                                <th>Bike製造商</th>
                                                <th>出廠年份</th>
                                                <th>數量</th>
                                                <th>備註</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><a class="btn btn-info apply_data" href="javascript:void(0);"><i
                                                                class="fa fa-exclamation-sign"></i> 套用至表單</a></td>
                                                <td><span class="syouhin_maker">{{$item->syouhin_maker}}</span></td>
                                                <td><span class="syouhin_name">{{$item->syouhin_name}}</span></td>
                                                <td><span class="syouhin_code">{{$item->syouhin_code}}</span></td>
                                                <td>{{$item->syouhin_type}}</td>
                                                <td>{{$item->syouhin_taiou_maker}}</td>
                                                <td>{{$item->syouhin_taiou_syasyu}}</td>
                                                <td>{{$item->syouhin_kosuu}}</td>
                                                <td>{{$item->syouhin_bikou}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        @if($entity->status == 0)
                                            <form class="form-horizontal form_item"
                                                  action="{{URL::route('backend-mitumori-item-post',$item->id)}}"
                                                  method="POST">

                                                <div class="radius-div">
                                                    <fieldset>
                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label text-danger">已上架SKU</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                <input class="form-control" type="text" name="exist_sku"
                                                                       value="{{$item->exist_sku}}">
                                                                <p class="note text-danger"><strong>※註:</strong> 如有填入，交期請選不明.</p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label text-danger">交期*</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                {{--{{Form::select('product_nouki', $nouki , $item->product_nouki ,array('class'=>'form-control') ) }}--}}
                                                                {{--↓--}}
                                                                {{--↓--}}
                                                                {{--add by Frank--}}
                                                                <select class="form-control" name="product_nouki">
                                                                    <option value="" >請選擇交期</option>
                                                                    @foreach($nouki as $nokui_key=>$nokui_value)
                                                                        @if($item->product_nouki==$nokui_key)
                                                                            <option value="{{$nokui_key}}" selected="selected">{{$nokui_value}}</option>
                                                                        @else
                                                                            <option value="{{$nokui_key}}">{{$nokui_value}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!--品牌選擇-->
                                                        <div class="form-group  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-3 control-label text-danger">品牌名稱*</label>
                                                            <div class="col-lg-7 brand-control">
                                                                <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="checkbox">
                                                                        <label>
                                                                          <input type="checkbox"
                                                                                 class="checkbox style-0 custom_manufacturer"
                                                                                 name="custom_manufacturer"
                                                                                 {{ $item->custom_manufacturer ? 'checked="checked"'  : ''}} value="1">
                                                                          <span>自訂新品牌</span>
                                                                        </label>
                                                                    </span>
                                                                </span>
                                                                    @if($item->custom_manufacturer)
                                                                        <input class="form-control"
                                                                               placeholder="請輸入品牌名稱" type="text"
                                                                               name="product_manufacturer_name"
                                                                               value="{{$item->product_manufacturer_name}}">
                                                                        <select style="width:100%; display: none;"
                                                                                class="select2"
                                                                                name="product_manufacturer_id">
                                                                            <option value="">請選擇品牌</option>
                                                                            @foreach ($manufacturers as $select )
                                                                                <option value="{{$select->id}}" {{$item->custom_manufacturer == 0  && ($select->id == $item->product_manufacturer_id)  ? 'selected="selected"' : ''}}>{{ $select->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @else
                                                                        <input class="form-control"
                                                                               placeholder="請輸入品牌名稱" type="text"
                                                                               name="product_manufacturer_name" value=""
                                                                               style="display: none;">
                                                                        <select style="width:100%" class="select2"
                                                                                name="product_manufacturer_id"
                                                                                placeholder="請搜尋品牌">
                                                                            <option value="">請選擇品牌</option>
                                                                            @foreach ($manufacturers as $select )
                                                                                <option value="{{$select->id}}" {{$item->custom_manufacturer == 0  && ($select->id == $item->product_manufacturer_id)  ? 'selected="selected"' : ''}}>{{ $select->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label text-danger">商品名稱*</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                <input class="form-control" type="text"
                                                                       name="product_name"
                                                                       value="{{$item->product_name}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label">商品編號</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                <input class="form-control" type="text"
                                                                       name="product_code"
                                                                       value="{{$item->product_code}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label text-danger">商品成本*</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                <input class="form-control" type="text"
                                                                       name="product_cost"
                                                                       value="{{$item->product_cost}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-3 control-label">定價試算(單價)</label>
                                                            <div class="col-lg-7">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">JPY</span>
                                                                    <input type="hidden" name="rate" value="JPY">
                                                                    <input type="text" class="form-control" placeholder="試算價格(單價)">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" class="btn btn-primary btn_rate" tabindex="-1">試算</button>
                                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                                        <span class="caret"></span>
                                                                        </button>
                                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li><a href="javascript:void(0);" class="choice_rate JPY">日幣<i class="glyphicon glyphicon-ok"></i></a></li>
                                                                        <li><a href="javascript:void(0);" class="choice_rate TWD">台幣<i class="glyphicon glyphicon-ok" style="display: none;"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label text-danger">最終價格(單價)*</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                <input class="form-control" type="text"
                                                                       value="{{ ($item->product_price) }}"
                                                                       name="product_price">
                                                                <p class="note"><strong>一般會員價格：</strong>
                                                                    <span>{{$item->product_price_general}}</span></p>
                                                                <input type="hidden" name="product_price_general"
                                                                       value="{{$item->product_price_general}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-4 control-label">備註</label>
                                                            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                                                                <textarea class="form-control"
                                                                          name="product_bikou_x">{{$item->product_bikou_x}}</textarea>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <label class="col-lg-3 col-md-4 col-sm-4 col-xs-3 control-label">確認</label>
                                                            <div class="col-lg-7">
                                                                <label class="radio radio-inline">
                                                                    <input type="radio" class="radiobox"
                                                                           name="product_check"
                                                                           {{ $item->product_check ?  '' : 'checked="checked"'}} value="0">
                                                                    <span>未完成</span>
                                                                </label>
                                                                <label class="radio radio-inline">
                                                                    <input type="radio" class="radiobox"
                                                                           name="product_check" id="product_check_true"
                                                                           {{ $item->product_check ? 'checked="checked"'  : ''}} value="1">
                                                                    <span>已填完</span>
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </fieldset>

                                                    <div class="form-footer">
                                                        <a class="btn btn-default"
                                                           style="text-align: center;font-size:12px;padding: 4px 8px;">
                                                            <i class="fa fa-fw fa-lg  fa-times"
                                                               style="font-size: 28px;display: block;width:30px;height:28px;margin:0 auto;line-height:1;"></i>
                                                            重設
                                                        </a>
                                                        <button class="btn btn-default pull-right"
                                                                style="text-align: center;font-size:12px;padding: 4px 8px;margin-right:5px;"
                                                                type="submit">
                                                            <i class="fa fa-fw fa-lg  fa-save"
                                                               style="font-size: 28px;display: block;width:30px;height:28px;margin:0 auto;line-height:1;"></i>
                                                            儲存
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        @else
                                            @if($item->product)
                                                商品:<a href="/sd/{{$item->product->url_rewrite}}/"
                                                      target="_blank;">{{$item->product->name}}</a>
                                            @endif
                                        @endif

                                    </div>
                                @endforeach
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </article>

    </div>
@stop

@section('script')
    <script src="{{ asset('/plugin/select2/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $('.apply_data').click(function () {
            form = $(this).closest('table').next('form');
            //manufacturer
            select2 = form.find('.select2');
            manufacturer = $(this).closest('table').find('.syouhin_maker').text();
            val = select2.find('option').filter(function () {
                return $(this).html() == manufacturer
            }).val();
            select2.val(val).trigger("change");

            //name
            form.find('input[name="product_name"]').val($(this).closest('table').find('.syouhin_name').text());

            //code
            form.find('input[name="product_code"]').val($(this).closest('table').find('.syouhin_code').text());

        });

        $('input[name="custom_manufacturer"]').click(function () {
            if ($(this).prop("checked")) {
                $(this).closest('div.input-group').find('input[name="product_manufacturer_name"]').show();
                $(this).closest('div.input-group').find('.select2-container').hide();
            } else {
                $(this).closest('div.input-group').find('input[name="product_manufacturer_name"]').hide();
                $(this).closest('div.input-group').find('.select2-container').show();
            }

        });

        $('.choice_rate').click(function () {
            if ($(this).hasClass('JPY')) {
                $(this).closest('div.input-group').find('span:first').text('JPY');
                $(this).closest('div.input-group').find('input[type="hidden"]:first').val('JPY');
            } else {
                $(this).closest('div.input-group').find('span:first').text('TWD');
                $(this).closest('div.input-group').find('input[type="hidden"]:first').val('TWD');
            }
            $(this).closest('ul').find('.glyphicon').hide();
            $(this).find('.glyphicon').show();
        });

        $('.btn_rate').click(function () {
            btn = $(this);
            var rate = $(this).closest('div.input-group').find('input[type="hidden"]:first').val();
            var price = $(this).closest('div.input-group').find('input[type="text"]:first').val();
            role_id = $('#role_id').val();
            $.ajax(
                {
                    url : "{{URL::route('backend-mitumori-change-rate')}}",
                    type: "POST",
                    data: {price: price, role_id: role_id, rate: rate},
                    success: function (data, textStatus, jqXHR) {
                        obj = JSON.parse(data);
                        next = btn.closest('div.form-group').next('div.form-group');
                        next.find('p.note span').text(obj.price_general);
                        next.find('input[type="hidden"]').val(obj.price_general);
                        next.find('input[type="text"]:first').val(obj.price_final);
                        // syouhin_kosuu = parseInt($('#syouhin_kosuu').text());
                        // next.next('div.form-group').find('input').val(syouhin_kosuu * obj.price_final);


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('error!');
                    }
                });
            return false;
        });

        if (typeof String.prototype.startsWith != 'function') {
            // see below for better implementation!
            String.prototype.startsWith = function (str) {
                return this.indexOf(str) === 0;
            };
        }

        $('.form_item').submit(function (e) {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            if (postData[postData.length - 2].value == 1) {
                if ($(this).find('select[name="product_nouki"]').val().startsWith('A')) { //交期未定,不必產出商品

                } else {
                    for (var key in postData) {
                        if (postData[key].name == "product_nouki" && postData[key].value == "") {
                            alert('請選擇交期');
                            return false;
                        } else if (postData[key].name == "product_manufacturer_id" && postData[key].value == "" && !($(this).find('input[name="custom_manufacturer"]').prop('checked'))) {
                            alert('請選擇廠牌');
                            return false;
                        } else if (postData[key].name == "product_manufacturer_name" && postData[key].value == "" && $(this).find('input[name="custom_manufacturer"]').prop('checked')) {
                            alert('請輸入廠牌');
                            return false;
                        } else if (postData[key].name == "product_name" && postData[key].value == "") {
                            alert('請選擇名稱');
                            return false;
                        } else if (postData[key].name == "product_price" && (postData[key].value == "" || isNaN(postData[key].value) || postData[key].value == 0)) {
                            alert('請填寫正確的價格');
                            return false;
                        } else if (postData[key].name == "product_cost" && (postData[key].value == "" || isNaN(postData[key].value) || postData[key].value == 0)) {
                            alert('請填寫正確的成本');
                            return false;
                        }
                    }
                }
                if (!confirm('確定要儲存此項商品嗎？')) {
                    return false;
                }
            } else {
                p = $(this).find('input[name="product_price"]');
                if (p.val() == "" || isNaN(p.val()) || p.val() == 0) {
                    alert('請填寫正確的價格!');
                    return false;
                }
                c = $(this).find('input[name="product_cost"]');
                if (c.val() == "" || isNaN(c.val()) || c.val() == 0) {
                    alert('請填寫正確的價格!');
                    return false;
                }
                alert('註：未填完的商品不會驗證表單');
            }


            $.ajax(
                {
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (data, textStatus, jqXHR) {
                        if (data == 1) {
                            // $('#mitu-pill-nav > li.active').find('span').attr('class', 'badge bg-color-blue txt-color-white');
                            // // $('#mitu-pill-nav > li.active').find('span > i').switchClass('fa-times', 'fa-check');
                            // $('#mitu-pill-nav > li.active').addClass('fa-check').removeClass('fa-times');
                        } else {
                            // $('#mitu-pill-nav > li.active').find('span').attr('class', 'badge bg-color-blueDark txt-color-white');
                            // // $('#mitu-pill-nav > li.active').find('span > i').switchClass('fa-check', 'fa-times');
                            // $('#mitu-pill-nav > li.active').addClass('fa-times').removeClass('fa-check');
                        }
                        alert('已儲存！');
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('error!');
                    }
                });
            e.preventDefault(); //STOP default action
            return false;
        });

        $('#form_estimate').submit(function (e) {
            var s=document.querySelectorAll("span[class='badge bg-color-blueDark txt-color-white']");
            if(s.length>1){
                alert('有項目未完成');
                return false;
            }
            if (!confirm('提交後便會產出產品，要繼續？')) {
                return false;
            }
        });

    </script>

@stop