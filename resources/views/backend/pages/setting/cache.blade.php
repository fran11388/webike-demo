@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            max-height:190px;
            min-width:120px;
            min-height:120px;
        }
        .active-select {
            float:right;
            margin-right:15px;
        }
        .new-button {
            float:right;
            margin-right:20px;
        }
    </style>
@stop
@section('middle')
    <form method="post" action="{{\URL::route('backend-review')}}" class="row form clearfix">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspCache clear</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Active</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><span>Product detail page</span></td>
                                <td><span>Clear Product page cache</span></td>
                                <td><a href="{{ \URL::route('cache-product') }}" class="btn btn-danger" target="_blank" >Clear!</a></td>
                            </tr>
                            <tr>
                                <td><span>Summary page</span></td>
                                <td><span>Clear mt / ca / br page cache</span></td>
                                <td><a href="{{ \URL::route('cache-summary') }}" class="btn btn-danger" target="_blank" >Clear!</a></td>
                            </tr>
                            <tr>
                                <td><span>Static File page</span></td>
                                <td><span>Clear timestamp for reload js / css files</span></td>
                                <td><a href="{{ \URL::route('cache-staticFile') }}" class="btn btn-danger" target="_blank" >Clear!</a></td>
                            </tr>
                            <tr>
                                <td><span>Clear CDN cache</span></td>
                                <td><span>Clear js / css / image cache from cdn</span></td>
                                <td><a href="{{ \URL::route('cache-cdn') }}" class="btn btn-danger" target="_blank" >Clear!</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop
@section('script')
    <script>
        $('td a').click(function(e){
            var result = confirm('Are you sure execute it?');
            if(!result){
                e.preventDefault();
            }
        });
    </script>
@stop