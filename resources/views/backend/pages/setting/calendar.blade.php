@extends('backend.layouts.default')
@section('middle')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/jquery-ui/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/backend/fullcalendar/fullcalendar-custom.min.css')}}">
    <style>
        .bg-color-darken {
            background-color: #404040!important;
        }
        .bg-color-blue {
            background-color: #57889c!important;
        }
        .bg-color-orange {
            background-color: #c79121!important;
        }
        .bg-color-greenLight {
            background-color: #71843f!important;
        }
        .bg-color-blueLight {
            background-color: #92a2a8!important;
        }
        .bg-color-red {
            background-color: #a90329!important;
        }
        .txt-color-white {
            color: #fff!important;
        }
        .btn-select-tick i {
            display: none;
        }
        .btn-select-tick .active i {
            display: block;
            opacity: 1!important;
        }
        .white-screen {
            position: relative;
        }
        .loading-gif {
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -50px 0px 0px -50px;
            display: none;
            z-index: 1000;
        }
        #external-events>li {
            margin: 6px 4px 6px 0;
            display: inline-block;
        }
        #external-events>li>:first-child {
            padding: 5px 10px 10px;
            cursor: move;
            display: block;
        }
        #calendar-buttons {
            position: absolute;
            right: 14px;
            top: 5px;
        }
        .smart-form.widget-body-toolbar, .widget-body-toolbar {
            display: block;
            padding: 8px 10px;
            margin: -13px -13px 13px;
            min-height: 42px;
            border-bottom: 1px solid #ccc;
            background: #fafafa;
        }
        #external-events>li>:first-child:after {
            color: #fff;
            color: rgba(255,255,255,.7);
            content: attr(data-description);
            font-size: 11px;
            font-weight: 400;
            display: block;
            line-height: 0;
            margin: 7px 0;
            text-transform: lowercase;
        }
    </style>
    <img class="loading-gif" src="{{assetRemote('image/backend/ajax-loader.gif')}}" >
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">新增事件</h3>
                        </div>
                        <div class="panel-body">
                            <form id="add-event-form">
                                <fieldset>

                                    <div class="form-group">
                                        <label>新增事件ICON</label>
                                        <div class="btn-group btn-group-sm btn-group-justified" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="iconselect" id="icon-1" value="fa-info" checked="">
                                                <i class="fa fa-info text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-2" value="fa-warning">
                                                <i class="fa fa-warning text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-3" value="fa-check">
                                                <i class="fa fa-check text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-4" value="fa-user">
                                                <i class="fa fa-user text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-5" value="fa-lock">
                                                <i class="fa fa-lock text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-6" value="fa-clock-o">
                                                <i class="fa fa-clock-o text-muted"></i> </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>事件名稱</label>
                                        <input class="form-control" id="title" name="title" maxlength="40" type="text" placeholder="Event Title">
                                    </div>
                                    <div class="form-group">
                                        <label>事件短述</label>
                                        <textarea class="form-control" placeholder="Please be brief" rows="3" maxlength="40" id="description"></textarea>
                                        <p class="note">最多40字</p>
                                    </div>

                                    <div class="form-group">
                                        <label>選擇顏色</label>
                                        <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
                                            <label class="btn bg-color-darken active">
                                                <input type="radio" name="priority" id="option1" value="bg-color-darken txt-color-white" checked="">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn bg-color-blue">
                                                <input type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn bg-color-orange">
                                                <input type="radio" name="priority" id="option3" value="bg-color-orange txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn bg-color-greenLight">
                                                <input type="radio" name="priority" id="option4" value="bg-color-greenLight txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn bg-color-blueLight">
                                                <input type="radio" name="priority" id="option5" value="bg-color-blueLight txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn bg-color-red">
                                                <input type="radio" name="priority" id="option6" value="bg-color-red txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                        </div>
                                    </div>

                                </fieldset>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-default" type="button" id="add-event">
                                                新增
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="well well-sm" id="event-container">
                        <form>
                            <legend>
                                事件
                            </legend>
                            <ul id="external-events" class="list-unstyled">
                                @foreach($events as $event)
                                    <li class="ui-draggable" style="position: relative;">
                                        <span class="{{$event->priority}}" data-eventid="{{$event->id}}" data-description="{{$event->description}}" data-icon="{{$event->icon}}">{{$event->title}}</span>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="drop-remove" class="checkbox style-0">
                                    <span>加入行事曆後刪除</span> </label>

                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-calendar"></i> 行事曆</h3>
                </div>
                <div class="panel-body">
                    <div class="jarviswidget jarviswidget-color-blueDark">

                        <!-- widget div-->
                        <div>

                            <div class="widget-body no-padding">
                                <!-- content goes here -->
                                <div class="widget-body-toolbar">

                                    <div id="calendar-buttons">

                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="calendar"></div>

                                <!-- end content -->
                            </div>

                        </div>
                        <!-- end widget div -->
                    </div>
                </div>
            </div>
        </div>

    </div>



@stop
@section('script')
    <script src="{{ asset('js/backend/moment/moment.min.js')}}"></script>
    <script src="{{ asset('js/backend/fullcalendar/jquery.fullcalendar.min.js')}}"></script>
    <script src="{{ asset('js/backend/fullcalendar/zh-tw.js')}}"></script>
    <script src="{{ asset('plugin/jquery-ui/jquery-ui.min.js')}}"></script>

    <script type="text/javascript">
        $( document ).ajaxStart(function() {
            $( ".loading-gif" ).show();
        });
        $( document ).ajaxStop(function() {
            $( ".loading-gif" ).hide();
        });
        var fullviewcalendar;



        // full calendar

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var hdr = {
            left: 'title',
            center: 'month,agendaWeek,agendaDay',
            right: 'prev,today,next'
        };

        var initDrag = function (e) {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end

            var eventObject = {
                title: $.trim(e.children().text()), // use the element's text as the event title
                description: $.trim(e.children('span').attr('data-description')),
                icon: $.trim(e.children('span').attr('data-icon')),
                className: $.trim(e.children('span').attr('class')), // use the element's children as the event class
                eventid: $.trim(e.children('span').attr('data-eventid')),
            };
            // store the Event Object in the DOM element so we can get to it later
            e.data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            e.draggable({
                zIndex: 999,
                revert: true, // will cause the event to go back to its
                revertDuration: 0 //  original position after the drag
            });
        };

        var addEvent = function (title, priority, description, icon) {
            var data = {};
            data.title = title.length === 0 ? "Untitled Event" : title;
            data.description = description.length === 0 ? "No Description" : description;
            data.icon = icon.length === 0 ? " " : icon;
            data.priority = priority.length === 0 ? "label label-default" : priority;

            var html = $('<li><span class="' + data.priority + '" data-description="' + data.description + '" data-icon="' +
                data.icon + '">' + data.title + '</span></li>').prependTo('ul#external-events').hide().fadeIn();

            $("#event-container").effect("highlight", 800);

            $.ajax(
                {
                    url : "{{URL::route('backend-setting-calendar')}}",
                    type: "POST",
                    data : {action:'create_event',data:data},
                    success:function(data, textStatus, jqXHR)
                    {

                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('error!!');
                    }
                });

            initDrag(html);
        };

        /* initialize the external events
         -----------------------------------------------------------------*/

        $('#external-events > li').each(function () {
            initDrag($(this));
        });

        $('#add-event').click(function () {

            title = $('#title').val(),
                priority = $('input:radio[name=priority]:checked').val(),
                description = $('#description').val(),
                icon = $('input:radio[name=iconselect]:checked').val();

            addEvent(title, priority, description,icon);
        });

        /* initialize the calendar
         -----------------------------------------------------------------*/

        fullviewcalendar = $('#calendar').fullCalendar({

            header: hdr,
            buttonText: {
                prev: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>'
            },
            lang: 'zh-tw',
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar !!!

            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                var data = $.extend({}, originalEventObject);
                data.start = date.format();
                data.end = date.format();
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.end = date;
                data.eventid = copiedEventObject.eventid;
                data.eventdel = '';
                if ($('#drop-remove').is(':checked')) {
                    data.eventdel = true;
                }

                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)

                $.ajax(
                    {
                        url : "{{URL::route('backend-setting-calendar')}}",
                        type: "POST",
                        data : {action:'add_event',data:data},
                        async: false,
                        success:function(data, textStatus, jqXHR)
                        {
                            copiedEventObject.id = data;
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            alert('error!!');
                        }
                    });

                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            },
            select: function (start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        }, true // make the event "stick"
                    );
                }
                calendar.fullCalendar('unselect');
            },

            events: function(start, end, timezone, callback) {

                $.ajax({
                    url: "{{URL::route('backend-setting-calendar-events')}}",
                    dataType: 'JSON',
                    type:'POST',
                    data: {
                        start: start.format(),
                        end: end.format()
                    },
                    success: function(doc) {
                        callback(doc);
                    }
                });
            },

            eventRender: function (event, element, icon) {
                if (!event.description == "") {
                    element.find('.fc-event-title').append("<br/><span class='ultra-light'>" + event.description +
                        "</span>");
                }
                if (!event.icon == "") {
                    element.find('.fc-event-title').append("<i class='air air-top-right fa " + event.icon +
                        " '></i>");
                }
            },
            eventClick: function(event, jsEvent, view) {
                var switch_box = prompt("請選擇執行的動作：1:編輯 2:刪除", "1");
                switch (switch_box){
                    case "1":
                        var title = prompt('標題:', event.title, { buttons: { Ok: true, Cancel: false} });
                        var description = prompt('簡述:', event.description, { buttons: { Ok: true, Cancel: false} });
                        var update = false;
                        if(title){
                            event.title = title;
                            update = true;
                        }
                        if(description){
                            event.description = description;
                            update = true;
                        }
                        if(update){
                            $('#calendar').fullCalendar('updateEvent',event);
                            var data = {};
                            data.id = event.id;
                            data.title = event.title;
                            data.description = event.description;
                            data.start = event.start.format()
                            data.end = event.end.format();
                            $.ajax(
                                {
                                    url : "{{URL::route('backend-setting-calendar')}}",
                                    type: "POST",
                                    data : {action:'edit',data:data},
                                    success:function(data, textStatus, jqXHR)
                                    {

                                    },
                                    error: function(jqXHR, textStatus, errorThrown)
                                    {
                                        revertFunc();
                                        alert('error!!');
                                    }
                                });
                        }
                        break;
                    case "2":
                        if(confirm('確定要刪除?')){
                            var data = {};
                            data.id = event.id;
                            $.ajax(
                                {
                                    url : "{{URL::route('backend-setting-calendar')}}",
                                    type: "POST",
                                    data : {action:'delete',data:data},
                                    success:function(data, textStatus, jqXHR)
                                    {

                                    },
                                    error: function(jqXHR, textStatus, errorThrown)
                                    {
                                        revertFunc();
                                        alert('error!!');
                                    }
                                });
                            $('#calendar').fullCalendar( 'removeEvents' , event.id  );

                        }
                        break;
                    default:
                        alert('無此動作!');
                }


            },
            eventResize: function(event, delta, revertFunc) {
                // console.log(event)
                var data = {};
                data.id = event.id;
                data.title = event.title;
                data.description = event.description;
                data.start = event.start.format()
                data.end = event.end.format();
                $.ajax(
                    {
                        url : "{{URL::route('backend-setting-calendar')}}",
                        type: "POST",
                        data : {action:'edit',data:data},
                        success:function(data, textStatus, jqXHR)
                        {

                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            alert('error!!');
                            revertFunc();
                        }
                    });
            },
            eventDrop: function(event, delta, revertFunc) {
                var data = {};
                data.id = event.id;
                data.title = event.title;
                data.description = event.description;
                data.start = event.start.format()
                data.end = event.end.format();
                $.ajax(
                    {
                        url : "{{URL::route('backend-setting-calendar')}}",
                        type: "POST",
                        data : {action:'edit',data:data},
                        success:function(data, textStatus, jqXHR)
                        {

                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            alert('error!!');
                            revertFunc();
                        }
                    });

            },
            windowResize: function (event, ui) {
                $('#calendar').fullCalendar('render');
            }
        });

        /* hide default buttons */
        $('.fc-header-right, .fc-header-center').hide();



        $('#calendar-buttons #btn-prev').click(function () {
            $('.fc-button-prev').click();
            return false;
        });

        $('#calendar-buttons #btn-next').click(function () {
            $('.fc-button-next').click();
            return false;
        });

        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-button-today').click();
            return false;
        });

        $('#mt').click(function () {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        $('#ag').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        $('#td').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });


    </script>
@stop