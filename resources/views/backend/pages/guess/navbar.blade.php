<style>
    ul.dropdown-menu{
        width:auto;
    }
    nav ul li.active>a:before{
        text-align: right;
        top: 0;
        right: 50%;
        content: "\f0d7";
    }
    nav ul li.active{
        font-weight: bold;
    }

    .navbar {
        min-height: 30px;
    }
</style>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-7">
            <ul class="nav navbar-nav">
                {{--<li class="backend-guess"><a href="{{URL::route('backend-guess')}}">頁面編輯</a></li>--}}
                <li class="backend-guess-list"><a href="{{URL::route('backend-guess-list')}}">題目列表</a></li>
                <li class="backend-guess-insert"><a href="{{URL::route('backend-guess-insert')}}">新增題目</a></li>
                <li class="backend-guess-log"><a href="{{URL::route('backend-guess-log')}}">答題紀錄</a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
        </div>
    </div>
</nav>
