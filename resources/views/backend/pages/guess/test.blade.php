<!DOCTYPE html>
<html>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<h2>HTML Image</h2>
<img id="test" src="http://img.webike.tw/assets/images/user_upload/WebikeGuess/2018-04-10_before.jpg" alt="Trulli" width="1000" height="333">

</body>

<link rel="stylesheet" type="text/css" href="{{asset('css/pages/jquery.jqpuzzle.css')}}">
<script type="text/javascript" src="{{asset('js/pages/webikequiz/jquery-save-browser.js')}}"></script>
<script type="text/javascript" src="{{asset('js/pages/webikequiz/jquery.jqpuzzle.min.js')}}"></script>
<script type="text/javascript">
    // define your own texts
    var myTexts = {
        secondsLabel: '秒',
        movesLabel: '次移動',
        shuffleLabel: '亂數拼圖',
        toggleOriginalLabel: '顯示原圖',
        toggleNumbersLabel: '顯示編號',
        confirmShuffleMessage: '你確定要刷新Quiz?',
    };

    var mySettings = {
        rows: 3,
        cols: 3,
        language: 'en',
        shuffle: true,
        numbers: false,
        control: {
            shufflePieces: true,    // display 'Shuffle' button [true|false]
            confirmShuffle: true,   // ask before shuffling [true|false]
            toggleOriginal: true,   // display 'Original' button [true|false]
            toggleNumbers: true,    // display 'Numbers' button [true|false]
            counter: true,          // display moves counter [true|false]
            timer: true,            // display timer (seconds) [true|false]
            pauseTimer: true        // pause timer if 'Original' button is activated
                                    // [true|false]
        },
        animation: {
            shuffleRounds: 1,
            slidingSpeed: 100,
            shuffleSpeed: 200
        },
        success: {
            callback: function (results) {
                alert('移動' + results.moves + '次，共花費' + results.seconds + '秒。');
            }
        }
    };
    $('#test').jqPuzzle(mySettings, myTexts);
</script>
</html>
