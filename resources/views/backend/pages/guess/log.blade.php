@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row {
            margin-bottom: 15px;
            font-size: 17px;
        }

        .big-icon {
            width: 24px;
            height: 24px;
        }

        .selecter, input {
            height: 30px;
        }

        .selecter option {
            height: 30px;
        }

        .big_checkbox {
            width: 20px;
            height: 20px;
        }

        table .btn.btn-default {
            padding: 5px 10px;
        }

        header h2{
            display: inline;
        }
    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        @include('backend.pages.guess.navbar')
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2> 答題紀錄 </h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <?php $errors = \Session::get('errors'); ?>
                        @if( $errors )
                            <div class="row">
                                <div class="col col-1 col-lg-1"></div>
                                <div class="col col-10 col-lg-10">
                                    <ul>
                                        @foreach ($errors as $erorr)
                                            <li>{{$erorr}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col col-1 col-lg-1"></div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col col-1 col-lg-1"></div>
                            <div class="col col-10 col-lg-10">
                                <p>
                                    總答題次數:<B>{{ $attend_counts ? $attend_counts : 0 }}</B>人，目前條件下答題正確次數:<B>{{ $complete_counts ? $complete_counts : 0 }}</B>次。佔總參加次數的<B>{{ $complete_counts ? number_format( $complete_counts/$attend_counts*100, 2) : 0 }}
                                        %</B>。</p>
                            </div>
                            <div class="col col-1 col-lg-1"></div>
                        </div>
                        <div class="row">
                            <div class="col col-1 col-lg-1"></div>
                            <div class="col col-10 col-lg-10">
                                <table class="table table-bordered table-striped table-hover no-footer smart-form"
                                       style="width:100%">
                                    <thead>
                                    <tr>
                                        <form>
                                            <th style="width:5%;">
                                                <input name="id" value="{{$request->get('id')  ? $request->get('id') : ''}}"
                                                       placeholder="篩選客戶編號">
                                            </th>
                                            <th style="width:10%;">
                                                <input name="realname"
                                                       value="{{$request->get('realname')  ? $request->get('realname') : ''}}"
                                                       placeholder="篩選客戶姓名">
                                            </th>
                                            <th>
                                                <input name="email"
                                                       value="{{$request->get('email')  ? $request->get('email') : ''}}"
                                                       placeholder="篩選客戶信箱">
                                            </th>
                                            <th style="width:10%;">
                                                <input type="text" name="created_at[0]"
                                                       value="{{$request->get('created_at')  ? $request->get('created_at')[0] : ''}}"
                                                       placeholder="答題最舊時間"/>
                                                <input type="text" name="created_at[1]"
                                                       value="{{$request->get('created_at')  ? $request->get('created_at')[1] : ''}}"
                                                       placeholder="答題最新時間"/>
                                            </th>
                                            <th style="width:10%;">
                                                產出CSV
                                                <select name="csv_output" style="width: 60px">
                                                    <option value="" {{ $request->get('csv_output') ? '' : 'selected'}}>
                                                        否
                                                    </option>
                                                    <option value="true" {{$request->get('csv_output') == 'true' ? 'selected' : ''}}>
                                                        是
                                                    </option>
                                                </select>
                                            </th>
                                            <th style="width:10%">
                                                <select name="limit">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>
                                                        20
                                                    </option>
                                                    <option value="50" {{ $request->get('limit') == 50 ? 'selected' : ''}}>
                                                        50
                                                    </option>
                                                    <option value="100"{{ $request->get('limit') == 100 ? 'selected' : ''}}>
                                                        100
                                                    </option>
                                                </select>
                                                <a class="btn btn-default"
                                                   href="{{URL::route('backend-guess-log')}}">重置</a>
                                                <input class="btn btn-default" type="submit" value="篩選">
                                            </th>
                                        </form>
                                    </tr>
                                    <tr>
                                        <th>會員編號</th>
                                        <th>姓名</th>
                                        <th>E-mail</th>
                                        <th>答題時間</th>
                                        <th>會員答案</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if( isset($attends) )
                                        @foreach ($attends as $attend)
                                            <?php $customer = $attend->customer; ?>
                                            <tr>
                                                <td>{{$customer->id}}</td>
                                                <td>{{$customer->realname}}</td>
                                                <td>{{$customer->email}}</td>
                                                <td>{{$attend->created_at}}</td>
                                                <td>{{$attend->answer}}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="col col-1 col-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
            @if( isset($attends) )
                <div class="webike2_pagination webike2_box" style="text-align: center">
                    {{ $attends->appends($request->except('page'))->links() }}
                </div>
            @endif
            <form id="delete_form" action="{{URL::route('backend-guess-delete')}}" method="POST" style="display: none">
            </form>
        </article>
    </div>
@stop