@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row{
            margin-bottom:15px;
            font-size: 17px;
        }
        .big-icon{
            width:24px;
            height:24px;
        }
        .selecter,input{
            height:30px;
        }
        .selecter option{
            height:30px;
        }
        .big_checkbox{
            width:20px;
            height:20px;
        }
        table .btn.btn-default{
            padding:5px 10px;
        }

        header h2{
            display: inline;
        }
    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        @include('backend.pages.guess.navbar')
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2> 題目列表 </h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <div class="row">
                            <div class="col col-1 col-lg-1"></div>
                            <div class="col col-10 col-lg-10">
                                <table class="table table-bordered table-striped table-hover no-footer smart-form" style="width:100%">
                                    <thead>
                                    <tr>
                                        <form>
                                            <th style="width:42px;padding:10px;">
                                                <div class="btn btn-default" id="delete" style="padding:5px 10px">刪除</div>
                                            </th>
                                            <th style="width:10%;">
                                                <input type="text" name="date[0]" value="{{$request->get('date')  ? $request->get('date')[0] : ''}}" placeholder="題目最舊時間" />
                                                <input type="text" name="date[1]" value="{{$request->get('date')  ? $request->get('date')[1] : ''}}" placeholder="題目最新時間"/>
                                            </th>
                                            <th style="width:15%;text-align: center;">
                                                <select name="product_type">
                                                    <option disabled="true" {{ !$request->get('product_type') ? 'selected' : ''}}>請選擇商品種類</option>
                                                    <option value="1" {{$request->get('product_type') == 1 ? 'selected' : ''}}>一般商品</option>
                                                    <option value="2" {{$request->get('product_type') == 2 ? 'selected' : ''}}>正廠零件</option>
                                                </select>
                                            </th>
                                            <th style="width:5%;"></th>
                                            <th >
                                                <input type="text" name="product" placeholder="商品名稱" value="{{$request->get('product')  ? $request->get('product') : ''}}">
                                            </th>
                                            <th style="width:10%;">
                                                <input type="text" name="created_at[0]" value="{{$request->get('created_at')  ? $request->get('created_at')[0] : ''}}" placeholder="建立最舊時間" />
                                                <input type="text" name="created_at[1]" value="{{$request->get('created_at')  ? $request->get('created_at')[1] : ''}}" placeholder="建立最新時間"/>
                                            </th>
                                            <th style="width:10%">
                                                <select name="limit">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>20</option>
                                                    <option value="50" {{ $request->get('limit') == 50 ? 'selected' : ''}}>50</option>
                                                    <option value="100"{{ $request->get('limit') == 100 ? 'selected' : ''}}>100</option>
                                                </select>
                                                <a class="btn btn-default" href="{{URL::route('backend-guess-list')}}">重置</a>
                                                <input class="btn btn-default" type="submit" value="篩選" >
                                            </th>
                                        </form>
                                    </tr>
                                    <tr>
                                        <th>
                                            全選<br>
                                            <input type="checkbox" class="big_checkbox" id="checked_all">
                                        </th>
                                        <th >題目日期</th>
                                        <th >主圖</th>
                                        <th >答案</th>
                                        <th >商品</th>
                                        <th>建立時間</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($guesses as $guess)
                                        <tr>
                                            <td><input type="checkbox" class="big_checkbox checked_item" name="active_delete" value="{{$guess->id}}"></td>
                                            <td>{{$guess->date.' #'.$guess->id}}</td>
                                            <td><img src="{{$guess->before_img}}" width="150" height="100"></td>
                                            <td>{{$guess->answer}}</td>

                                            @if( $guess->product_id )
                                                <td>{{$guess->product}}</td>
                                            @else
                                                <td>{{$guess->genuine_name}}</td>
                                            @endif

                                            <td>{{$guess->created_at}}</td>
                                            <td><a href="{{URL::route( 'backend-guess-edit', array( 'id' => $guess->id ) )}}">編輯</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col col-1 col-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webike2_pagination webike2_box" style="text-align: center">
                {{ $guesses->appends($request->except('page'))->links() }}
            </div>
            <form id="delete_form" action="{{URL::route('backend-guess-delete')}}" method="POST" style="display: none">
            </form>
        </article>
    </div>

@stop

@section('script')
    <!-- 刪除 -->
    <script type="text/javascript">
        $('#delete').click(function(){
            $('input[name^="active_delete"]:checked').each(function(){
                $('#delete_form').append(
                    '<input name="delete_id[]" value="'+$(this).val()+'"" />'
                );
            });
            $('#delete_form').submit();
        });
    </script>
    <script type="text/javascript">
        $('#delete_form').submit(function(){
            if(!confirm('確定要刪除?')){
                return false;
            }
        });
    </script>
@stop