@extends('backend.layouts.default')
@section('middle')
    <div class="row order-info">
        <!-- LARGE -->
        @include('backend.pages.guess.navbar')

        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget collection">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2> WebikeGuess </h2>
                </header>
                <div class="row">
                    <form action="{{URL::route('backend-guess-post')}}" method="POST" enctype="multipart/form-data">
                        <div class="widget-body">
                            <div class="row">
                                <div class="col col-12 col-lg-12 col-centered">
                                    <img src="{{assetRemote('assets/images/user_upload/WebikeGuess/webikeguess750.jpg')}}"
                                         alt="WebikeGuess" width="800" height="330">
                                </div>
                                <div class="col col-12 col-lg-12 ">
                                    <label>修改主圖</label>
                                    <input type="file" class="btn btn-default required" name="mainbanner">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-lg-12">
                                    <img src="{{assetRemote('assets/images/user_upload/WebikeGuess/webikeguess_tit01.jpg')}}"
                                         alt="WebikeGuess" width="800">
                                </div>
                                <div class="col col-12 col-lg-12">
                                    <label>修改橫幅</label>
                                    <input type="file" class="btn btn-default required" name="tit1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-lg-12">
                                    <img src="{{assetRemote('assets/images/user_upload/WebikeGuess/webikeguess_tit02.jpg')}}"
                                         alt="WebikeGuess" width="800">
                                </div>
                                <div class="col col-12 col-lg-12">
                                    <label>修改月曆上方橫幅</label>
                                    <input type="file" class="btn btn-default required" name="tit2">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-6 col-lg-6">
                                    <a class="baseBtn_blue" href="{{URL::route('benefit-event-webikeguess-get')}}"
                                       target="_blank;">
                                        <div>查看活動頁面</div>
                                        <a/>
                                </div>
                                <div class="col col-6 col-lg-6">
                                    <input class="baseBtn_yellow pull_r" type="submit" value="修改"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </article>
    </div>
@stop
