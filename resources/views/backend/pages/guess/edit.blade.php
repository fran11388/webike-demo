@extends('backend.layouts.default')
@section('middle')
    <?php
    $photo_path = '/assets/images/user_upload/WebikeGuess/';
    $photo_path1 = '/assets/images/benefit/event/WebikeGuess/';
    ?>
    <style type="text/css">
        input.answer {
            width: 100%;
        }

        input.hidden {
            display: none;
        }
    </style>
    <style>
        /* プレイヤー */
        .player {
            background: #d90114;
            width: 100%;
        }

        .player a:hover {
            background: #a51320;
            text-decoration: none;
        }

        .player td {
            text-align: center;
        }

        .player .play,
        .player .playing {
            width: 60px;
            height: 60px;
        }

        .player .play a {
            background: #d90114 url(/assets/images/benefit/event/WebikeGuess/play_btn.png) no-repeat;
            display: block;
            width: 60px;
            height: 60px;
            text-indent: -9999pt;
        }

        .player .play a:hover {
            background: #a51320 url(/assets/images/benefit/event/WebikeGuess/play_btn.png) no-repeat;
        }

        .player .playing a {
            background: #a51320;
            text-indent: -9999pt;
            display: none;
        }

        .player .ans,
        .player .ansing {
            width: 60px;
            height: 60px;
            border-left: #fff 1px solid;
        }

        .player .ans a {
            background: #d90114 url(/assets/images/benefit/event/WebikeGuess/answer_btn.png) no-repeat;
            display: block;
            width: 60px;
            height: 60px;
            text-indent: -9999pt;
        }

        .player .ansing a {
            background: #a51320;
            text-indent: -9999pt;
            display: none;
        }

        .player .ans a:hover {
            background: #a51320 url(/assets/images/benefit/event/WebikeGuess/answer_btn.png) no-repeat;
        }

        .player .timer {
            border-left: #fff 1px solid;
            border-right: #fff 1px solid;
            padding: 10px;
        }

        .player .timer div {
            background: #fff;
        }

        .player .timer p {
            background: #ffba00;
            height: 15px;
            width: 0;
        }

        .player .switch {
            padding: 10px;
            width: 85px;
        }

        .player .switch a {
            color: #fff;
            padding: 10px;
            border-radius: 5px;
            display: inline-block;
        }

        .player .switch a.select {
            background: #a51320;
        }

        .photo {
            width: 450px;
            height: 340px;
            position: relative;
            background: #fff;
            float: none;
        }

        .photo.type_id2 {
            width: 600px !important;
        }

        .photo.type_id2 img {
            width: 600px !important;
        }

        .photo.type_id3 {
            width: auto;
            height: auto;
        }

        .uploader [class^=type_id] {
            display: none;
        }

        .photo img {
            width: 450px;
            position: absolute;
            z-index: 1;
            left: 0;
            top: 0;
        }

        .type_id3 img {
            position: static;

        }

        .photo div.choices {
            width: 100%;
            height: 100%;
            z-index: 9999;
        }

        .photo div.choices div {
            display: block;
            position: absolute;
            width: 33.33%;
            height: 33.33%;
            z-index: 999;
            background: none;
            border: none;
            font-size: 0px;
        }

        .photo div.choices div:hover {
            background: #fff;
            opacity: 0.4;

        }

        .photo div.choices div.onclick {
            background: #B0EDFF;
            opacity: 0.6;
        }

        .mask {
            position: absolute;
            background: #fff;
            height: 340px;
            width: 450px;
            top: 0;
        }

        .game_player {
            width: auto;
        }

        .player.player2{
            width: 450px;
        }


        .game_player .timer p {
            width: 0%;
            background: #ffba00;
        }

        header h2{
            display: inline;
        }
    </style>
    <div class="row">
        @include('backend.pages.guess.navbar')
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form action="{{URL::route( 'backend-guess-edit-post')}}" method="POST" enctype="multipart/form-data">
                <div class="jarviswidget version1">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-list-alt"></i> </span>
                        <h2> 題目 </h2>
                    </header>
                    <div class="row">
                        <div class="widget-body">
                            <?php $errors = \Session::get('errors'); ?>
                            @if( $errors )
                                <div class="row">
                                    <ul>
                                        @foreach ($errors as $key => $error)
                                            <li><span class="red_text">{{$error}}</span></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if( isset($guess) )
                                <div class="row">
                                    <div class="col col-3 col-lg-3"></div>
                                    <div class="col col-8 col-lg-8">
                                        <div class="game_player">
                                            @if($guess->type_id == 1)
                                                <div class="photo photo1 {{'type_id'.$guess->type_id}}">
                                                    <img id="after_img" src="{{$guess->after_img}}">
                                                    <img id="before_img" src="{{$guess->before_img}}">
                                                    <div class="mask">
                                                    </div>
                                                    <div class="border">
                                                        <img src="{{asset('image/event/9_cellnum.png')}}">
                                                    </div>
                                                </div>
                                                <table class="player player2" border="0" cellspacing="0"
                                                       cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td class="play"><a href="javascript:void(0);">Play</a></td>
                                                        <td class="ans"><a href="javascript:void(0);">查看答案</a></td>
                                                        <td class="timer">
                                                            <div><p></p></div>
                                                        </td>
                                                        <td class="switch"><a href="javascript:void(0);">OFF</a><a
                                                                    class="select" href="javascript:void(0);">ON</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            @elseif($guess->type_id == 2)
                                                <div class="photo photo1 {{'type_id'.$guess->type_id}}">
                                                    <img id="before_img" src="{{$guess->before_img}}">
                                                    <div class="border">
                                                        <img src="{{asset('image/event/36_cellnum.png')}}">
                                                    </div>
                                                </div>
                                            @else
                                                <div class="photo photo1 {{'type_id'.$guess->type_id}}" >
                                                    <img id="before_img" src="{{$guess->before_img}}">
                                                    <div class="floatingCirclesG">
                                                        <div class="f_circleG" id="frotateG_01"></div>
                                                        <div class="f_circleG" id="frotateG_02"></div>
                                                        <div class="f_circleG" id="frotateG_03"></div>
                                                        <div class="f_circleG" id="frotateG_04"></div>
                                                        <div class="f_circleG" id="frotateG_05"></div>
                                                        <div class="f_circleG" id="frotateG_06"></div>
                                                        <div class="f_circleG" id="frotateG_07"></div>
                                                        <div class="f_circleG" id="frotateG_08"></div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col col-3 col-lg-3"></div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col col-2 col-lg-2"></div>
                                <div class="col col-10 col-lg-10">
                                    @if( isset( $guess ) )
                                        <input type="hidden" name="id" value="{{$guess->id}}">
                                    @endif
                                    <label class="label_title">設定您的題目</label><br/>
                                    <section class="col col-6 col-lg-6">
                                        <label>
                                            <i class="fa fa-edit big-icon" style="float:left"></i><h4
                                                    style="float:left">輸入答案</h4>
                                        </label><br>
                                        <label class="input">
                                            <input type="text" class="required" id="answer" name="answer"
                                                   placeholder="輸入答案" value="{{ isset($guess) ? $guess->answer : ''}}"
                                                   style="width:100%">
                                        </label>
                                    </section>
                                    <section class="col col-6 col-lg-6 uploader">
                                        <div class="type_id1">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <i class="fa fa-camera-retro big-icon" style="float:left"></i>
                                                    <span>圖片-變化前</span>
                                                    <input type="file" class="btn btn-default required"
                                                           name="before_img">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <i class="fa fa-camera-retro big-icon" style="float:left"></i>
                                                    <span>圖片-變化後</span>
                                                    <input type="file" class="btn btn-default required"
                                                           name="after_img">
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="type_id2">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <i class="fa fa-camera-retro big-icon" style="float:left"></i>
                                                    <span>圖片-題目</span>
                                                    <input type="file" class="btn btn-default required"
                                                           name="before_img">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <i class="fa fa-camera-retro big-icon" style="float:left"></i>
                                                    <span>圖片-商品</span>
                                                    <input type="file" class="btn btn-default required"
                                                           name="after_img">
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <i class="fa fa-camera-retro big-icon"
                                                       style="float:left;"></i><span>圖片-解答</span>
                                                    <input type="file" class="btn btn-default required" name="ans_img">
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="type_id3">
                                            <div class="row">
                                                <div class="col col-lg-12">
                                                    <i class="fa fa-camera-retro big-icon" style="float:left"></i>
                                                    <span>圖片-題目</span>
                                                    <input type="file" class="btn btn-default required"
                                                           name="before_img">
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-2 col-lg-2"></div>
                                <div class="col col-10 col-lg-10">
                                    <div class="col col-lg-6">
                                        <div class="col col-lg-12">
                                            <select name="product_type">
                                                <option disabled="true" {{ isset($guess) ? '' : 'selected' }}>請選擇商品種類
                                                </option>
                                                <option value="1" {{ (isset($guess) and $guess->product_id) ? 'selected' : ''}}>
                                                    一般商品
                                                </option>
                                                <option value="2" {{ (isset($guess) and !$guess->product_id) ? 'selected' : ''}}>
                                                    正廠零件
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col col-lg-12">
                                            <div class="sku hidden">
                                                <label>商品SKU</label>
                                                <input name="sku" type="text" placeholder="請輸入商品sku"
                                                       value="{{ (isset($guess) and $guess->product_id) ? $guess->product->url_rewrite : '' }}">
                                            </div>
                                            <div class="genuineparts hidden">
                                                <label>廠牌</label>
                                                <input name="genuine_maker" type="text" placeholder="請輸入正廠零件品牌"
                                                       value="{{ (isset($guess) and !$guess->product_id) ? $guess->genuine_maker : '' }}"><br/>
                                                <label>名稱</label>
                                                <input name="genuine_name" type="text" placeholder="請輸入正廠零件名稱"
                                                       value="{{ (isset($guess) and !$guess->product_id) ? $guess->genuine_name : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="col col-lg-12">
                                            <h4>輸入題目日期</h4>
                                            <input class="date_choice" name="date" type="text"
                                                   placeholder="格式範例:2015-06-29"
                                                   value="{{ isset($guess) ? $guess->date : '' }}">
                                        </div>
                                        <div class="col col-lg-12">
                                            <h4>選擇遊戲類型</h4>
                                            <select name="type_id">
                                                <option value="1" {{ (isset($guess) and $guess->type_id == 1) ? 'selected' : ''}}>
                                                    前後圖形變化
                                                </option>
                                                <option value="2" {{ (isset($guess) and $guess->type_id == 2) ? 'selected' : ''}}>
                                                    尋找商品
                                                </option>
                                                <option value="3" {{ (isset($guess) and $guess->type_id == 3) ? 'selected' : ''}}>
                                                    拼圖
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-2 col-lg-2"></div>
                                <div class="col col-10 col-lg-10">
                                    <label><i class="fa fa-edit big-icon" style="float:left"></i>商品敘述</label><br/>
                                    <textarea id="description" name="description" class="col col-10 col-lg-10"
                                              placeholder="簡述，顯示於題目右方"
                                              rows="4">{{ isset($guess) ? $guess->description : "" }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="jarviswidget version">
                    <input class="btn btn-success offset pull-right" type="submit" style="width:180px;height:30px;"
                           value="{{isset($guess) ? '修改' : '新增'}}"/>
                </div>
            </form>
        </article>
    </div>
@stop

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if ($('select[name=product_type]').val() == 1) {
                $('div.sku').removeClass('hidden');
                $('input[name=sku]').removeAttr('disabled');
            }
            if ($('select[name=product_type]').val() == 2) {
                $('div.genuineparts').removeClass('hidden');
                $('input[name=genuineparts]').removeAttr('disabled');
            }


            $('.date_choice').datetimepicker({
                format:      'YYYY-MM-DD'
            });
        });
        $('select[name=product_type]').change(function () {
            var type = $(this).val();
            if (type == 1) {
                $('div.sku').removeClass('hidden');
                $('input[name=sku]').removeAttr('disabled');
                $('div.genuineparts').addClass('hidden');
                $('input[name=genuine_name]').prop('disabled', 'true');
                $('input[name=genuine_maker]').prop('disabled', 'true');
            } else if (type == 2) {
                $('div.genuineparts').removeClass('hidden');
                $('input[name=genuine_name]').removeAttr('disabled');
                $('input[name=genuine_maker]').removeAttr('disabled');
                $('div.sku').addClass('hidden');
                $('input[name=sku]').prop('disabled', 'true');
            }
        });

        $('select[name=type_id]').change(function () {
            $('.uploader').find('input').prop('disabled', 'disabled');
            $('.uploader').find('[class^=type_id]').hide();
            var target = $('.uploader').find('.type_id' + $(this).val());
            target.show();
            target.find('input').removeAttr('disabled');
            if ($(this).val() == 3) {
                $('input[name=answer]').prop('disabled', 'disabled');
            } else {
                $('input[name=answer]').removeAttr('disabled');
            }
        });

        $('select[name=type_id]').val({{!isset($guess) ? 1 : $guess->type_id}}).trigger('change');
    </script>
    <script type="text/javascript">
        $('td.play').find('a').click(function () {
            $('img#before_img').css({opacity: 1});
            $('.game_player .timer p').css({width: '0%'});
            $('img#before_img').animate({
                opacity: 0
            }, 15000);
            $('.game_player .timer p').animate({
                width: '100%'
            }, 15000);
        });
        $('td.ans').find('a').click(function () {
            $('img#before_img').css({opacity: 0});
            setInterval(function () {
                $('img#before_img').css({opacity: 1});
            }, 1000);
        });
        $('.switch').find('a').click(function () {
            $('.switch a').removeClass('select');
            $(this).addClass('select');
            if ($(this).html() == 'ON') {
                $('.game_player .border').css('display', 'block');
            } else {
                $('.game_player .border').css('display', 'none');
            }
        });
    </script>
    @if(isset($guess) and $guess->type_id == 3)
        <link rel="stylesheet" type="text/css" href="{{asset('css/pages/jquery.jqpuzzle.css')}}">
        <script type="text/javascript" src="{{asset('js/pages/webikequiz/jquery-save-browser.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/pages/webikequiz/jquery.jqpuzzle.min.js')}}"></script>
        <script type="text/javascript">
            // define your own texts
            var myTexts = {
                secondsLabel: '秒',
                movesLabel: '次移動',
                shuffleLabel: '亂數拼圖',
                toggleOriginalLabel: '顯示原圖',
                toggleNumbersLabel: '顯示編號',
                confirmShuffleMessage: '你確定要刷新Quiz?',
            };

            var mySettings = {
                rows: 3,
                cols: 3,
                language: 'en',
                shuffle: true,
                numbers: false,
                control: {
                    shufflePieces: true,    // display 'Shuffle' button [true|false]
                    confirmShuffle: true,   // ask before shuffling [true|false]
                    toggleOriginal: true,   // display 'Original' button [true|false]
                    toggleNumbers: true,    // display 'Numbers' button [true|false]
                    counter: true,          // display moves counter [true|false]
                    timer: true,            // display timer (seconds) [true|false]
                    pauseTimer: true        // pause timer if 'Original' button is activated
                                            // [true|false]
                },
                animation: {
                    shuffleRounds: 1,
                    slidingSpeed: 100,
                    shuffleSpeed: 200
                },
                success: {
                    callback: function (results) {
                        alert('移動' + results.moves + '次，共花費' + results.seconds + '秒。');
                    }
                }
            };
            $('.type_id3 img').jqPuzzle(mySettings, myTexts);
        </script>
    @endif
@stop