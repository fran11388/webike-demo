
<style>
.image-contaniner .image{
	width:22.5%;
	float: left;
	margin-bottom: 15px;
	margin-left: 2.5%;
	}
.image-contaniner .image figure{
	height:150px;
	width:240px;
}
</style>

{{-- {{dd(session()->all())}} --}}
<div class="image-contaniner">
	@if(session()->has('photos'))
	@php
		$names = session()->get('names');
	@endphp
		<h2>近期上傳圖片</h2>
		<ul class="no-list-style  clearfix">
			@foreach(session()->get('photos') as $key => $photo)
				<li class="box-comment box-fix-column image">  
					<figure class="ct-img-left thumb-box-border thumb-img">
			            <img src="//img.webike.tw/imageUpload/{{$photo}}">
			        </figure>
			        <div>圖片原始名稱:{{$names[$key]}}</div></br>
					<span class="dotted-text2">https://img.webike.tw/imageUpload/{{$photo}}</span>
				</li>
			@endforeach			
					
		</ul>	
	@endif
</div>