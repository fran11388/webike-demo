@extends('backend.layouts.default')
@section('style')
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
<style>
   .file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
    float: left;
    } 
    .upload{
        float: left;
    }
    .upload-contaniner{
        border-bottom: 10px solid;
        margin-bottom: 20px;
    }

</style> 
@stop
@section('middle')

<div class="clearfix upload-contaniner">
 <form style="left:-1000px;top:0px;" id="hidden-form" target="upload_target"
      action="{{route('backend-tool-upload')}}" method="post" enctype="multipart/form-data">
    <div class=" form-group">  
        <input id="choose-file" class="file-upload" type="file" size="40" name="file[]" accept="image/*" style="width:260px;margin:0" multiple="multiple" >
    </div>
</form>   
<form id="form-validate" method="post" action="{{route('backend-tool-store')}}" enctype="multipart/form-data">
    <div class="upload text-center form-group ">
        <button type="submit" id="writing_btn" disabled="disabled" class="btn btn-danger style-button-strong border-radius-2" href="">
            上傳
        </button>
    </div>  
    <input type="hidden" name="photo_key" value=""/>
    <input type="hidden" name="file_name" value="">
    <div class=" col-xs-12 col-sm-12 col-md-1 box-fix-with left">
            <input type="hidden" name="photo_preview" value="">      
    </div>   
</form>    
<form id="data-form" method="GET" action="{{ route('backend-tool-searchPhoto') }}">
    <div class="col-xs-12 col-sm-12 col-md-3 text-center form-group clearfix ">
        <P class="col-xs-12 col-sm-12 col-md-1">從</P>
        <input type="date" name="start" class="col-xs-12 col-sm-12 col-md-5" >
        <p class="col-xs-12 col-sm-12 col-md-1">到</p>
        <input type="date" name="end" class="col-xs-12 col-sm-12 col-md-5" >
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2" >
        名稱搜尋<input type="text" name="search_name">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-1 text-center form-group ">
        <button type="submit"   class="btn btn-asking style-button-strong border-radius-2" href="">
            搜尋
        </button>
    </div> 
</form>
<iframe id="upload_target" name="upload_target" src="#"
            style="display:none; width:0;height:0;border:0px solid #fff;"></iframe>
</div>
@include("backend.pages.tool.uploadImage")
@include("backend.pages.tool.image")

@endsection
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{{assetRemote('plugin/validate/jquery.validate.js') }}"></script>
    <script src="{{assetRemote('plugin/validate/jquery.validate-additional-methods.min.js') }}"></script>
    <script type="text/javascript">
        
    	var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }
		$('input[type="file"]').change(function () {
            $('input[name="photo_preview"]').val('1');
            $('#writing_btn').prop('disabled', true).css('cursor', 'no-drop');
            $('#hidden-form').submit();
            
        });
        function stopUpload(result) {
            $('#writing_btn').prop('disabled', false).css('cursor', 'pointer');
            $('input[name="photo_preview"]').val('');
            if (result.success) {
                $('input[name="photo_key"]').val(result.key);
                $('input[name="file_name"]').val(result.name);

            } else {
                $('input[name="photo_key"]').val('');
                alert(result.message);
                if (result.reload) {
                    window.location.reload();
                    return false;
                }
            }
            return true;
        }
        
    </script>
@stop