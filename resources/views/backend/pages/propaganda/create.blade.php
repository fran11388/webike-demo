@extends('backend.layouts.default')
@section('style')

@stop
@section('middle')
    <div class="newsletter-container ">
        <div class="clearfix">
            {{--@if (count($errors) > 0)--}}
            {{--<div class="alert alert-danger content-last-block">--}}
            {{--<ul style="list-style-type: none;">--}}
            {{--@foreach ($errors->all() as $error)--}}
            {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--@endif--}}

            <form id="create-form" method="post" action="">


                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspEdit propaganda</h3>
                    </div>
                    <div class="panel-body">
                        <div class="basic-set">
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">選擇類型*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <select class="form-control" name="type_id">

                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">開始時間*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">

                                    <input class="form-control" placeholder=""
                                           type="datetime-local" name="start_time" value="" required>
                                </div>
                            </div>

                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">結束時間*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="" type="datetime-local" name="end_time" value="" required>
                                </div>
                            </div>

                            <div class="name-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">圖片</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="背景圖片網址" type="text"
                                           name="pic_path" value="">
                                </div>
                            </div>

                            <div class="name-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">文字</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="" type="text" name="text" value="">
                                </div>
                            </div>

                            <div class="name-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">連結*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="" type="text" name="link" value="" >
                                </div>
                            </div>

                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">顯示位置*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <select class="form-control" name="position_id">

                                        @foreach($positions as $position)
                                            <option value="{{$position->id}}">{{$position->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">顯示客群*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <select class="form-control" name="audience_id">

                                        @foreach($audiences as $audience)
                                            <option value="{{$audience->id}}">{{$audience->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="name-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">不顯示頁面</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">

                                    <textarea class="form-control" name="banned_paths" placeholder="一網址一行" rows="5"
                                              cols="35"></textarea>
                                </div>
                            </div>


                        </div>

                        <div class="save-container text-right">
                            <input type="submit" class="btn btn-danger" name="create" value="Save">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('script')
    <script>
        // $('.datetimepicker').datetimepicker();
    </script>

@stop