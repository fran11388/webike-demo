@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            max-height: 190px;
            min-width: 120px;
            min-height: 120px;
        }

        .active-select {
            float: right;
            margin-right: 15px;
        }

        .new-button {
            float: right;
            margin-right: 20px;
        }
    </style>
@stop
@section('middle')
    <form method="post" action="{{\URL::route('backend-post-campaign')}}" class="row form clearfix">
        {{--<div class="active-select block">--}}
        {{--<select class="select2" name="active_type">--}}
        {{--<option value="" selected>批次</option>--}}
        {{--<option value="open">開啟</option>--}}
        {{--<option value="close">關閉</option>--}}
        {{--<option value="delete">刪除</option>--}}
        {{--</select>--}}
        {{--</div>--}}
        <div class="new-button">
            <a href="" class="btn btn-warning" onclick="deletePropagandas()">刪除</a>
        </div>
        <div class="new-button">
            <a href="{{ route('backend-propagandas-create') }}" class="btn btn-info">新增</a>
        </div>
        <div class="col-lg-12">

            <div class="panel panel-primary">

                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-book"></i>&nbsp行銷列表</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th><input id="checkAll" type="checkbox"></th>
                                <th>設定時間</th>
                                <th>類型</th>
                                <th>開始時間</th>
                                <th>結束時間</th>
                                <th>圖片</th>
                                <th>文字</th>
                                <th>連結</th>
                                <th>顯示位置</th>
                                <th>顯示客群</th>
                                <th>不顯示頁面</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($propagandas as $propaganda)
                                <tr
                                    <?php
                                    $mutable = \Carbon\Carbon::now();
                                    $start_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $propaganda->start_time);
                                    $end_time = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $propaganda->end_time);
                                    ?>
                                    @if($start_time->lte($mutable)&& $end_time->gte($mutable))
                                    style="background: #ffe4bb;"
                                    @endif
                                >
                                    <td><input class="btn btn-default" name="check[]" type="checkbox"
                                               value="{{$propaganda->id}}"></td>
                                    <td>{{$propaganda->updated_at->toDateString()}}</td>
                                    <td>{{$propaganda->type->name}}</td>
                                    <td>{{$propaganda->start_time}}</td>
                                    <td>{{$propaganda->end_time}}</td>
                                    <td><a href="{{$propaganda->pic_path}}"
                                           target="_blank">{{$propaganda->pic_path}}</a></td>
                                    <td>{{$propaganda->text}}</td>
                                    <td><a href="{{$propaganda->link}}" target="_blank">{{$propaganda->link}}</a></td>
                                    <td>{{$propaganda->position->name}}</td>
                                    <td>{{$propaganda->audience->name}}</td>
                                    <td>
                                        @foreach($propaganda->bannedPaths as $bannedPath )
                                            <a href="{{$bannedPath->original_url}}"
                                               target="_blank">{{$bannedPath->original_url}}</a> <br>
                                        @endforeach
                                    </td>
                                    <td>
                                        <a class="btn btn-danger btn-sm"
                                           href="{{route('backend-propagandas-edit',$propaganda->id)}}">Edit</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="text-center">
        {{--{!! $campaigns->render() !!}--}}
    </div>
@stop
@section('script')
    <script>
        $("#checkAll").change(function () {
            $("input[name='check[]']").prop('checked', $(this).prop("checked"));
        });

        $(document.body).on("change", ".select2", function () {
            var check = false;
            var choose = this.value;
            if (choose) {
                var title = "你確定要" + choose + "嗎";

                $("input[name='check[]']").each(function (event) {
                    if ($(this).prop("checked")) {
                        check = true;
                    }
                });

                if (check) {
                    var form = $('.form');
                    swal({
                        title: title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "確定",
                        cancelButtonText: '取消',
                        closeOnConfirm: false
                    }).then(function () {
                        form.submit();
                    });
                } else {
                    alert('未選擇項目');
                }
            }
        });


        function deletePropagandas() {
            var r = confirm("確定要刪除?");
            if (r == true) {

            } else {
                return;
            }


            var ids = [];
            $("input:checked[name='check[]']").each(function (index) {
                ids += $(this).val() + ',';

            });

            $.ajax({
                url: "{{route('backend-propagandas')}}",
                data: {
                    ids: ids
                },
                method: 'DELETE',
                success: function () {
                    window.location.reload();
                }
            });

        }

    </script>
@stop