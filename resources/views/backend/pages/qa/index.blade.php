@extends('backend.layouts.default')
@section('middle')
    <div class="function-cabinet">
        <div class="row">
            <!--
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="function-box large-box text-center" href="{!! route('backend-qa-search') !!}">
                    <i class="glyphicon glyphicon-search"></i> 搜尋Q&A
                </a>
            </div>
            -->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form action="{!! route('backend-qa-search') !!}" method="get">
                    <div class="form-group pre-hide-ui">
                        <div class="checkbox">
                            <label>
                                <input class="pre-hide-release" type="checkbox" value="1">搜尋商品SKU
                            </label>
                        </div>
                        <div class="pre-hide-box hide">
                            <label>請輸入需要指定的SKU</label>
                            <textarea class="form-control" name="skus" rows="4"></textarea>
                            <p class="help-block">您可以輸入多個商品sku，請已每行一個sku排列。</p>
                            <div class="box">
                                <div class="col-md-9 col-sm-9 col-xs-9"></div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input class="btn btn-danger btn-full" type="submit" value="搜尋" >
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="function-box large-box text-center" href="{!! route('backend-qa-weekly') !!}">
                    <i class="glyphicon glyphicon-list-alt"></i> Q&A本周上架
                </a>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="function-box large-box text-center" href="{!! route('backend-qa-edit') !!}">
                    <i class="glyphicon glyphicon-plus-sign"></i> 新增Q&A
                </a>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="function-box large-box text-center" href="{!! route('backend-qa-ticket-import') !!}">
                    <i class="glyphicon glyphicon-plus-sign"></i> 匯入外包Ticket
                </a>
            </div>
        </div>
    </div>
@stop