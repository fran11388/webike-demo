@extends('backend.layouts.default')
@section('style')
    <style type="text/css">

    </style>
@stop
@section('middle')
    @if (count($errors) > 0)
        <div class="alert alert-danger error-message">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="history-table-container">
                <ul class="history-info-table history-table-adjust">
                    <li class="history-table-title">
                        <ul class="vertical_fix_header">
                            <li class="col-md-3 col-sm-4 col-xs-4"><span class="hidden-xs">商品</span><span class="text-left hidden-lg hidden-md hidden-sm" style="padding:0 10px">商品列表</span></li>
                            <li class="col-md-9 col-sm-8 col-xs-8">
                                <div class="row splits">
                                    <div class="col-md-4 col-sm-4 col-xs-4 col-cell col-right-border"><span>問題</span></div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 col-cell col-right-border"><span>答案</span></div>
                                    <div class="col-md-2 col-sm-2 col-xs-2 col-cell col-right-border"><span>時間</span></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="history-table-content">
                        @foreach($collection as $item)
                            @php
                                $answer = $item->answer;
                                $products = $item->products;

                            @endphp
                        <ul class="vertical_fix_header">
                            <li class="col-md-3 col-sm-4 col-xs-4">
                                <div class="row">
                                    @if($products->first())
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <img src="{!! $products->first()->getThumbnail() !!}">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 overflow-auto">
                                            @if($answer->relation->product)
                                                {!! $products->first()->name !!}<br>
                                            @elseif($answer->relation->series)
                                                系列商品：{!! $products->first()->name !!}<br>
                                            @endif
                                            @foreach($products as $product)
                                                {{$product->model_number}}<a href="{!! route('product-detail', $product->url_rewrite) !!}" target="_blank"><i class="fa fa-fw fa-search"></i></a><br>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <img src="{!! NO_IMAGE !!}">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 overflow-auto">
                                            answer_id = {!! $item->answer->id !!}
                                        </div>
                                    @endif
                                </div>
                            </li>
                            <li class="col-md-9 col-sm-8 col-xs-8">
                                <div class="row splits">
                                    <div class="col-md-4 col-sm-4 col-xs-4 col-cell col-right-border">{{$answer->question->content}}</div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 col-cell col-right-border">{{$answer->content}}</div>
                                    <div class="col-md-2 col-sm-2 col-xs-2 col-cell "><span>{{$answer->created_at}}</span></div>
                                </div>
                            </li>
                        </ul>
                        @endforeach
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="row text-center">
        {{ $answers->links() }}
    </div>
@stop
@section('script')
@stop