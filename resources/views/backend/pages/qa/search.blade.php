@extends('backend.layouts.default')
@section('style')
    <style type="text/css">
        ul.vertical_fix{
            display: initial !important;
        }
        ul.vertical_fix li{
            border-right: none !important;
            margin: 0 !important;
        }
        ul.vertical_fix li .row{
            padding: 5px 0;
        }
        ul.vertical_fix li .col-right-border{
            border-right: 1px dotted #9fa7b5;
        }
        ul.vertical_fix li:last-child .splits{
            border-bottom: none;
        }
        .splits{
            display: flex;
            height: 100%;
            border-bottom: 1px dotted #9fa7b5;
        }
        .splits .col-right-border{
            border-right: 1px dotted #9fa7b5;
        }
        .splits .col-cell{
            display: flex;
            justify-content: center;
            flex-direction: column;
        }
        .splits .col-cell.to-top{
            justify-content: initial;
        }
    </style>
@stop
@section('middle')
    @if (count($errors) > 0)
        <div class="alert alert-danger error-message">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!--
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-zoom-in"></i> 設定搜尋客服訊息條件</h3>
        </div>
        <div class="panel-body">
            <form id="ticket-search" method="get" action="{!! route('backend-qa-search') !!}">
                <div class="form-group pre-hide-ui">
                    <div class="checkbox">
                        <label>
                            <input class="pre-hide-release" type="checkbox" value="1">指定車型
                        </label>
                    </div>
                    <div class="pre-hide-box hide">
                        <label>請輸入車型名稱</label>
                        <select class="form-control select2 select-motors" name="motor_ids" multiple="multiple">
                        </select>
                        <p class="help-block">您可以加入多個車型。</p>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>請輸入需要指定的SKU</label>
                        <textarea class="form-control" name="product_skus" rows="4" disabled>{!! request()->get('product_skus') !!}</textarea>
                        <p class="help-block">您可以輸入多個商品sku，請已每行一個sku排列。</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>請輸入品牌名稱</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <select class="form-control select2 select-manufacturers width-full" name="manufacturer_ids[]" multiple="multiple" disabled>
                        </select>
                    </div>
                </div>

                <div class="row content-last-block">
                    <div class="col-md-7 col-sm-12 col-xs-12"></div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <a class="btn btn-default btn-full" href="{!! route('backend-qa-search') !!}"> 清空</a>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <a class="btn btn-warning btn-full" href="javascript:void(0)" onclick="submitForm(this, $('#ticket-search').prop('action'))"><i class="glyphicon glyphicon-search"></i> 搜尋</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="history-table-container">
                <ul class="history-info-table history-table-adjust">
                    <li class="history-table-title">
                        <ul class="vertical_fix_header">
                            <li class="col-md-3 col-sm-4 col-xs-4"><span class="hidden-xs">問題</span><span class="text-left hidden-lg hidden-md hidden-sm" style="padding:0 10px">問題列表</span></li>
                            <li class="col-md-9 col-sm-8 col-xs-8">
                                <div class="row splits">
                                    <div class="col-md-6 col-sm-8 col-xs-8 col-cell col-right-border"><span>回答</span></div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 col-cell col-right-border"><span>QA顯示條件</span></div>
                                    <div class="col-md-2 col-sm-4 col-xs-4 col-cell"><span>管理</span></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="history-table-content">
                        @if(!count($questions))
                            <h1 class="text-center font-bold" style="padding: 100px 0">查無任何結果，請重新查詢。</h1>
                        @else
                            @foreach($questions as $question)
                                <ul class="col-sm-block col-xs-block clearfix">
                                    <li class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <div class="row text-left">
                                            {{ $question->content }}
                                        </div>
                                    </li>
                                    <li class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                                        @php
                                            $answers = $question->answers
                                        @endphp
                                        <ul class="col-sm-block col-xs-block clearfix vertical_fix">
                                            @foreach($answers as $answer)
                                                @php
                                                    $relation = $answer->relation;
                                                @endphp
                                                <li>
                                                    <div class="row splits">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-cell col-right-border">{!! nl2br($answer->content) !!}</div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-cell col-right-border to-top overflow-auto">
                                                            @if($relation->product and $product = new \Ecommerce\Accessor\ProductAccessor($relation->product))
                                                                <span>商品：{!! $product->name !!}<a href="{!! route('product-detail', $product->url_rewrite) !!}" target="_blank"><i class="fa fa-fw fa-search"></i></a></span>
                                                            @elseif($relation->series and $series = Everglory\Models\Product::where('relation_product_id', $relation->series_id)->get())
                                                                系列商品：{!! $series->first()->name !!}<br>
                                                                @foreach($series as $item)
                                                                    <span>{{$item->model_number}}<a href="{!! route('product-detail', $item->url_rewrite) !!}" target="_blank"><i class="fa fa-fw fa-search"></i></a></span>
                                                                @endforeach
                                                            @elseif($relation->manufacturer and $manufacturer = $relation->manufacturer)
                                                                品牌：<a href="{!! route('summary', '/br/' . $manufacturer->url_rewrite) !!}" target="_blank">{{$manufacturer->name}}</a><br/>
                                                            @elseif($relation->category and $category = $relation->category)
                                                                分類：{{$category->name}}<br/>
                                                            @elseif($relation->motor and $motor = $relation->motor)
                                                                車型：{{$motor->name}}<br/>
                                                            @endif
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-cell">
                                                            <a class="btn btn-danger btn-full" href="{!! route('backend-qa-edit', ['answer_id' => $answer->id]) !!}" target="_blank">編輯</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            @endforeach
                            {!! $questions->render() !!}
                        @endif
                    </li>
                </ul>

            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            runManufacturerAjaxSelect2();
        });
    </script>
@stop