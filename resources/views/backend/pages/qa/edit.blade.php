@extends('backend.layouts.default')
@section('middle')
    @if (count($errors) > 0)
        <div class="alert alert-danger error-message">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form id="main-form" action="{!! route('backend-qa-write') !!}" method="post">
        <div  class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-zoom-in"></i> 設定Q&A綁定的商品條件</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group pre-hide-ui">
                            <div class="checkbox">
                                <label>
                                    <input class="pre-hide-release" type="checkbox" value="1">指定商品SKU
                                </label>
                            </div>
                            <div class="pre-hide-box hide">
                                <label>請輸入需要指定的SKU</label>
                                <textarea class="form-control" name="skus" rows="4">{!! $skus ? $skus : request()->get('skus') !!}</textarea>
                                <p class="help-block">您可以輸入多個商品sku，請已每行一個sku排列。</p>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group pre-hide-ui">
                            <div class="checkbox">
                                <label>
                                    <input class="pre-hide-release" type="checkbox" value="1" disabled>指定商品系列
                                </label>
                            </div>
                            <div class="pre-hide-box hide">
                                <label>請輸入商品系列名稱</label>
                                <p class="help-block">暫無功能。</p>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group pre-hide-ui">
                            <div class="checkbox">
                                <label>
                                    <input class="pre-hide-release" type="checkbox" value="1" disabled>指定車型
                                </label>
                            </div>
                            <div class="pre-hide-box hide">
                                <label>請輸入車型名稱</label>
                                <select class="form-control select2 select-motors" name="motor_ids" multiple="multiple">
                                </select>
                                <p class="help-block">您可以加入多個車型。</p>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group pre-hide-ui">
                            <div class="checkbox">
                                <label>
                                    <input class="pre-hide-release" type="checkbox" value="1" disabled>指定商品分類
                                </label>
                            </div>
                            <div class="pre-hide-box hide">
                                <label>指定商品分類</label>
                                <p class="help-block">暫無功能。您可以一次設定多個商品分類。</p>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group pre-hide-ui">
                            <div class="checkbox">
                                <label>
                                    <input class="pre-hide-release" type="checkbox" value="1">指定品牌
                                </label>
                            </div>
                            <div class="pre-hide-box hide">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label>請輸入品牌名稱</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <select class="form-control select2 select-manufacturers width-full" multiple="multiple" name="manufacturer_ids[]">
                                        </select>
                                    </div>
                                </div>
                                <p class="help-block">您可以一次設定多個品牌。</p>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i> 設定Q&A內容</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Q&A分類</label>
                            <select class="select2" name="group_id">
                                @foreach(\Everglory\Models\Qa\Group::all() as $group)
                                    <option value="{!! $group->id !!}" {!! ($question and $question->group_id == $group->id) ? 'selected' : '' !!}>{{$group->name}}</option>
                                @endforeach
                            </select>
                            <p class="help-block">請選擇該Q&A所屬分類。</p>
                        </div>
                        <div class="form-group">
                            <label>Q.問題內容</label>
                            <input class="form-control" type="text" name="question_content" value="{!! $question ? $question->content : request()->get('question_content') !!}">
                            <p class="help-block">為未上述條件設定Q&A的問題，無法輸入換行編排。</p>
                        </div>
                        <div class="form-group">
                            <label>A.答案內容</label>
                            <textarea class="form-control" name="answer_content" rows="10">{!! $answer ? $answer->content : request()->get('answer_content')!!}</textarea>
                            <p class="help-block">為此問題填入適當的答案，可以進行換行編排。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6"></div>
            <div class="col-md-3 hidden-sm hidden-xs"></div>
            <div class="col-md-3 hidden-sm hidden-xs"></div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <a class="btn btn-danger btn-full" href="javascript:void(0)" onclick="submitForm(this, $('#main-form').prop('action'))"><i class="glyphicon glyphicon-ok-circle"></i> 確認送出</a>
            </div>
        </div>
    </form>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            runMotorAjaxSelect2();
            runManufacturerAjaxSelect2();
            @if(count(request()->all()))
                @foreach(request()->all() as $key => $input)
                    $('#main-form').find('[name="{!! $key !!}"]').closest('.pre-hide-ui').find('input[class="pre-hide-release"]').click();
                @endforeach
            @endif
            @if($skus)
                $('#main-form').find('[name="skus"]').closest('.pre-hide-ui').find('input[class="pre-hide-release"]').click();
            @endif
        });
    </script>
@stop