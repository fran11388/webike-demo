@extends('backend.layouts.default')
@section('style')
    <style type="text/css">

    </style>
@stop
@section('middle')
    @if (count($errors) > 0)
        <div class="alert alert-danger error-message">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-zoom-in"></i> 設定搜尋客服訊息條件</h3>
        </div>
        <div class="panel-body">
            <form id="ticket-search" method="get" action="{!! route('backend-qa-tickets') !!}">
                <!--
                <div class="form-group pre-hide-ui">
                    <div class="checkbox">
                        <label>
                            <input class="pre-hide-release" type="checkbox" value="1">指定車型
                        </label>
                    </div>
                    <div class="pre-hide-box hide">
                        <label>請輸入車型名稱</label>
                        <select class="form-control select2 select-motors" name="motor_ids" multiple="multiple">
                        </select>
                        <p class="help-block">您可以加入多個車型。</p>
                        <hr>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>請輸入需要指定的SKU</label>
                        <textarea class="form-control" name="product_skus" rows="4">{!! request()->get('product_skus') !!}</textarea>
                        <p class="help-block">您可以輸入多個商品sku，請已每行一個sku排列。</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>請輸入品牌名稱</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <select class="form-control select2 select-manufacturers width-full" name="manufacturer_ids[]" multiple="multiple">
                            @if($manufacturers)
                                @foreach($manufacturers as $manufacturer)
                                    <option value="{!! $manufacturer->id !!}">{!! $manufacturer->name !!}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row content-last-block">
                    <div class="col-md-7 col-sm-12 col-xs-12"></div>
                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <a class="btn btn-default btn-full" href="{!! route('backend-qa-tickets') !!}"> 清空</a>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <a class="btn btn-warning btn-full" href="javascript:void(0)" onclick="submitForm(this, $('#ticket-search').prop('action'))"><i class="glyphicon glyphicon-search"></i> 搜尋</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="history-table-container">
                <ul class="history-info-table history-table-adjust">
                    <li class="history-table-title">
                        <ul>
                            <li class="col-md-4 col-sm-12 col-xs-12"><span class="hidden-xs">商品</span><span class="text-left hidden-lg hidden-md hidden-sm" style="padding:0 10px">商品列表</span></li>
                            <li class="col-md-3 hidden-sm hidden-xs"><span>問題分類</span></li>
                            <li class="col-md-2 hidden-sm hidden-xs"><span>客服訊息</span></li>
                            <li class="col-md-3 hidden-sm hidden-xs"><span>管理</span></li>
                        </ul>
                    </li>
                    <li class="history-table-content">
                        @if(!count($pagination->collection))
                            <h1 class="text-center font-bold" style="padding: 100px 0">查無任何結果，請重新查詢。</h1>
                        @else
                            @foreach($pagination->collection as $ticket)
                                @php
                                    $product = $ticket->product;
                                @endphp
                                <ul class="col-sm-block col-xs-block clearfix">
                                    <li class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="row text-left">
                                            <div class="col-md-5 col-sm-4 col-xs-4">
                                                <a class="thumbnail zoom-image" href="{!! route('product-detail', ['url_rewrite' => $product->url_rewrite]) !!}" title="{!! $product->full_name !!}" target="_blank">
                                                    <img src="{!! $product->getThumbnail() !!}" alt="{!! $product->full_name !!}">
                                                </a>
                                            </div>
                                            <div class="col-md-7 col-sm-8 col-xs-8">
                                                <a href="{!! route('product-detail', ['url_rewrite' => $product->url_rewrite]) !!}" title="{!! $product->full_name !!}" target="_blank">
                                                    <h3>{!! $product->name !!}</h3>
                                                </a>
                                            </div>
                                            <div class="col-md-7 col-sm-12 col-xs-12">
                                                <span>品牌：{!! $product->manufacturer->name !!}</span>
                                                <div class="toggle-box">
                                                    <div class="toggle-button text-right">
                                                        <a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊...</a>
                                                    </div>
                                                    <div class="toggle-content hidden-lg hidden-md hidden-sm hidden-xs">
                                                        <span>商品編號：{{ $product->model_number }}</span>
                                                        @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                                                            <span>備註：{{$estimate_item->note}}</span>
                                                        @endif
                                                        @if($options = $product->getSelectsAndOptions() and $options->count())
                                                            <span>
                                                        規格：<br>
                                                                @foreach($options as $option)
                                                                    {{ $option->label }}：{{ $option->option }}<br>
                                                                @endforeach
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                    <li class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                                        【{{$ticket->type}}】<br>{{$ticket->category}}
                                    </li>
                                    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
                                        @if(Config::get('app.production'))
                                            <a href="{!! EG_ZERO . '/crm/service/detail/' . $ticket->url_rewrite !!}" target="_blank">{{$ticket->increment_id}}</a>
                                        @else
                                            <a href="{!! EAGLE_EG_ZERO . '/crm/service/detail/' . $ticket->url_rewrite !!}" target="_blank">{{$ticket->increment_id}}</a>
                                        @endif
                                    </li>
                                    <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <form id="qa-import" method="post" action="{!! route('backend-qa-edit') !!}" target="_blank">
                                                    <input type="hidden" name="skus" value="{!! $product->sku !!}">
                                                    @if(count($ticket->replies))
                                                        @php
                                                            $first_reply = $ticket->replies->shift();
                                                            $other_reply = $ticket->replies->map(function($reply){
                                                                if($reply->replyer->type === 'user'){
                                                                    $reply->replyer->name = '【' . $reply->replyer->name . '】';
                                                                }
                                                                return $reply->replyer->name . '：' . $reply->excerpt;
                                                            })->all();
                                                        @endphp
                                                        <input type="hidden" name="question_content" value="{!! strip_tags($first_reply->replyer->name . '：' . $first_reply->excerpt) !!}">
                                                        <input type="hidden" name="answer_content" value="{!! strip_tags(implode("\r\n\r\n", $other_reply)) !!}">
                                                    @endif
                                                    <a class="btn btn-danger btn-full" href="javascript:void(0)" onclick="submitForm(this, $('#qa-import').prop('action'))">匯入Q&A</a>
                                                </form>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <form id="qa-search" method="get" action="{!! route('backend-qa-search') !!}" target="_blank">
                                                    <input type="hidden" name="ticket_increment_ids[]" value="{!! $ticket->increment_id !!}">
                                                    <a class="btn btn-asking btn-full" href="javascript:void(0)" onclick="submitForm(this, $('#qa-search').prop('action'))"><i class="glyphicon glyphicon-search"></i> 相關Q&A</a>
                                                </form>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            @endforeach
                        @endif
                    </li>
                </ul>

            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            runManufacturerAjaxSelect2();
        });
    </script>
@stop