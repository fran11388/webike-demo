@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        table thead tr.filter{
            background-image:none;
        }
        table thead tr.filter th{
            padding:5px 4px;
            vertical-align: top;
        }
        tr.filter div.range-line:after{
            clear:both;
            content:".";
            display:block;
            font-size:0;
            height:0;
            line-height: 0;
            overflow: hidden;
        }
        tr.filter select{
            width:100%;
        }
        tr.filter input.input-text{
            float: right;
            margin-top: 0;
            width: 50px !important;
        }
        tr.filter .range-line {
            margin-bottom: 3px;
            width: 100px;
        }
        tr.filter .range-line span.label{
            color:#2f2f2f;
            line-height: 2.5;
        }
        tr.filter .field-100 input.input-text{
            float: left;
            width: 100% !important;
        }
        .widget-body table .thumbnail{
            max-width:45px;
            max-height:45px;
            margin: 0px;
            border: 1px solid #ddd;
            border-radius: 3px;
            padding:0px;
        }

        .widget-body table td.a_right{
            text-align: right;
        }
        .widget-body table td.a_center{
            text-align: center;
        }
    </style>
    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
                <header>
                    {{--<span class="widget-icon"> <i class="fa fa-comments"></i> </span>--}}
                    {{--<h2> 評論 </h2>--}}
                    <div class="widget-toolbar" role="menu">

                        <div class="btn-group" style="float:right" >
                            <button class="btn dropdown-toggle btn-xs btn-success" data-toggle="dropdown" aria-expanded="false">
                                批次... <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:void(0);"  onclick="page_action('delete')">強制刪除</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">


                        <table class="table table-bordered table-striped table-hover no-footer smart-form" style="width:100%">
                            <thead>
                            <tr>
                                <th style="width:42px;">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="checkbox style-2">
                                            <span></span> </label>
                                    </div>
                                </th>
                                <th style="width:60px;">ID</th>
                                <th style="width:60px">查詢編號</th>
                                <th style="width:100px;">客戶email</th>
                                <th style="width:100px;">查詢時間</th>
                                <th>回覆時間</th>
                                <th style="width:100px;">日本查詢單號</th>
                                <th style="width:61px;">狀態</th>
                                <th style="width:40px;">Action</th>
                            </tr>
                            <!--
                            <tr class="filter">
                                <th style="width:42px;"></th>
                                <th style="width:120px;">
                                    <div class="range">
                                        <div class="range-line">
                                            <span class="label">From:</span>
                                            <input type="text" class="input-text" />
                                        </div>
                                        <div class="range-line">
                                            <span class="label">To:</span>
                                            <input type="text" class="input-text" />
                                        </div>
                                    </div>
                                </th>
                                <th class="field-100">
                                    <input type="text" class="input-text" />
                                </th>
                                <th class="field-100">
                                    <input type="text" class="input-text" />
                                </th>
                                <th class="field-100">
                                    <input type="text" class="input-text" />
                                </th>
                                <th class="field-100">
                                    <input type="text" class="input-text" />
                                </th>
                                <th></th>
                                <th>
                                    <select>
                                        <option>TEST</option>
                                    </select>
                                </th>

                            </tr>
                            -->

                            </thead>
                            <tbody>
                            @foreach ($collection as &$entity)
                                <tr>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="checkbox style-2" value="{{ $entity->id }}">
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $entity->id }}
                                    </td>
                                    <td>
                                        {{ $entity->estimate_code }}
                                    </td>
                                    <td>
                                        @if($entity->customer)
                                            {{ $entity->customer->email }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $entity->request_date }}
                                    </td>
                                    <td>
                                        {{ $entity->response_date }}
                                    </td>
                                    <td>
                                        <span>{{ $entity->jp_order_code }}</span>
                                        <div>
                                            <input type="hidden" value="{{$entity->id}}" name="id" />
                                            <input type="text" value="{{ $entity->jp_order_code }}" name="jp_order_code" style="display: none;" />
                                            <a class="btn btn-info btn-xs btn_order" href="javascript:void(0);">輸入</a>
                                        </div>
                                    </td>
                                    <td>
                                        {!!  $entity->status ? '已回覆': '<span class="text-danger">尚未回覆</span>' !!}
                                    </td>
                                    <td>
                                        <a class="btn btn-danger btn-sm" href="{{URL::route('backend-groupbuy-edit',$entity->id)}}">建立商品</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="dt-toolbar-footer">
                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                <div aria-live="polite" role="status" id="dt_basic_info" class="dataTables_info">
                                    {{--Showing <span class="txt-color-darken">{{  $collection->getFrom() }}</span> to <span class="txt-color-darken"> {{  $collection->getTo() }}</span> of <span class="text-primary"> {{  $collection->getTotal()  }}</span> entries--}}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div id="dt_basic_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <?php echo $collection->links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

    <!-- row -->

    <div class="row">

        <!-- a blank row to get started -->
        <div class="col-sm-12">
            <!-- your contents here -->
        </div>

    </div>

    <!-- end row -->
@stop

@section('script')
    <script type="text/javascript">
        $(document).on('click', '.btn_order' , function(e){
            $(this).closest('div').prev('span').hide();
            $(this).closest('div').wrap('<form id="form_order" method="post" action="{{URL::route('backend-groupbuy-post')}}">');
            $(this).prev('input').show();
            $(this).text('送出');
            $(this).toggleClass('btn_submit');
            $(this).toggleClass('btn_order');
            return false;
        });

        $(document).on('click', '.btn_submit' , function(e){
            $(this).closest('div').closest('form').submit();
        });

        $(document).on('submit', '#form_order' , function(e){
            form = $(this);
            var postData = form.serializeArray();
            var formURL = form.attr("action");
            $.ajax(
                {
                    url : formURL,
                    type: "POST",
                    data : postData,
                    success:function(data, textStatus, jqXHR)
                    {
                        form.prev('span').text(data).show();
                        btn  = form.find('div a');
                        btn.text('輸入');
                        btn.toggleClass('btn_submit');
                        btn.toggleClass('btn_order');
                        btn.prev('input').hide();
                        form.find('div').unwrap();

                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('error!');
                    }
                });
            e.preventDefault(); //STOP default action
            return false;
        });

        $(document).on('click', 'th input:checkbox' , function(){
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                .each(function(){
                    this.checked = that.checked;
                    $(this).closest('tr').toggleClass('selected');
                });
        });
        //batch action

        function page_action(action){
            if(confirm('確定要'+ action +'?')){
                var value = $(".smart-form tbody input[type=checkbox]:checked").map(function() { return $(this).val(); }).get().join("_");
                if( action == 'delete' ){
                    $('<form method="post" action="{{ URL::route('backend-groupbuy-delete-post')}}"><input type="hidden" name="action_type" value="'+ action +'"><input name="ids" value="' + value + '"></form>').appendTo('body').submit();
                }
            }else{

                return false;
            }
        }
    </script>
@stop