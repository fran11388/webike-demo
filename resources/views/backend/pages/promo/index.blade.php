@extends('backend.layouts.default')
@section('style')
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" /> 
<style>
    .file-upload {
        border: 1px solid #ccc;
        display: inline-block;
        padding: 6px 12px;
        cursor: pointer;
        float: left;
    }

</style>
@stop
@section('middle')
<div class="container clerafix">
<div class="clerafix upload col-md-6">
		<form action="{!! route('backend-promo-promoData') !!}" method="post" enctype="multipart/form-data">
		    <input  class="file-upload" type="file" name="file" id="fileToUpload">
            <input class="btn btn-danger " type="submit" value="確認完成" name="submit">
		</form>
	</div>
	<div class="download col-md-6">
		<a href="\vendor\doc\monthpromo\promotion.xlsx"><button class="btn btn-warning">下載範本</button></a>
	</div>

</div>	
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

@stop