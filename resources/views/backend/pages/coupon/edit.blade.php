@extends('backend.layouts.default')
@section('style')

@stop
@section('middle')
    <div class="newsletter-container ">
        <div class="clearfix">
            @if (count($errors) > 0)
                <div class="alert alert-danger content-last-block">
                    <ul style="list-style-type: none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="create-form" method="post" action="">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspEdit COUPON</h3>
                    </div>
                    <div class="panel-body">
                        <div class="basic-set">
                            <div class="name-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">Name*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="Name" type="text" name="name" value="{{ $coupon->name }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">Code*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="Code" type="text" name="code" value="{{ $coupon->code }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">Discount type*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <select class="form-control" name="discount_type">
                                        <option value="product" {{ $coupon->discount_type ==='product' ? 'selected' : ''  }}>Discount product price</option>
                                        <option value="shippingfee" {{ $coupon->discount_type ==='shippingfee' ? 'selected' : ''  }}>Discount shipping fee</option>
                                    </select>
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">Discount*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="Discount" type="text" name="discount" value="{{ number_format( $coupon->discount ) }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">Period*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <select class="form-control" name="period">
                                        <option value=""></option>
                                        <option value="days" {{ $coupon->period ==='days' ? 'selected' : ''  }}>DAY</option>
                                        <option value="months" {{ $coupon->period ==='months' ? 'selected' : ''  }}>MONTH</option>
                                        <option value="years" {{ $coupon->period ==='years' ? 'selected' : ''  }}>YEAR</option>
                                    </select>
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">Deadline*</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <input class="form-control" placeholder="number" type="number" name="time_limit" value="{{  $coupon->time_limit  }}">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select class="form-control" name="time_limit_unit" required>
                                        <option value=""></option>
                                        <option value="days" {{ $coupon->time_limit_unit ==='days' ? 'selected' : ''  }}>DAY</option>
                                        <option value="months" {{ $coupon->time_limit_unit ==='months' ? 'selected' : ''  }}>MONTH</option>
                                        <option value="years" {{ $coupon->time_limit_unit ==='years' ? 'selected' : ''  }}>YEAR</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="save-container text-right">
                            <input type="submit" class="btn btn-danger" name="create" value="Save">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('script')
@stop