@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            max-height:190px;
            min-width:120px;
            min-height:120px;
        }
        .active-select {
            float:right;
            margin-right:15px;
        }
        .new-button {
            float:right;
            margin-right:20px;
        }
    </style>
@stop
@section('middle')
    <form method="get" class="row form clearfix">
        <div class="new-button">
            <a href="{{ route('backend-coupon-create') }}" class="btn btn-warning">Create</a>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspCOUPON List</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th><input id="checkAll" type="checkbox"></th>
                                <th>ID</th>
                                <th>name</th>
                                <th>Code</th>
                                <th>Discount type</th>
                                <th>Discount amount</th>
                                <th>Period</th>
                                <th>Deadline</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr>
                                    <td><input class="btn btn-default" name="check[]" type="checkbox" value="{{$coupon->id}}"></td>
                                    <td>
                                        {{ $coupon->id }}
                                    </td>
                                    <td>
                                        {{ $coupon->name }}
                                    </td>
                                    <td>
                                        {{ $coupon->code }}
                                    </td>
                                    <td>
                                        {{ $coupon->discount_type }}
                                    </td>
                                    <td>
                                        {{ number_format( $coupon->discount ) }}
                                    </td>
                                    <td>
                                        {{ $coupon->period }}
                                    </td>
                                    <td>
                                        {{ $coupon->time_limit . ' ' .  $coupon->time_limit_unit }}
                                    </td>
                                    <td>
                                        <a class="btn btn-danger btn-sm" href="{{route('backend-coupon-edit',$coupon->id)}}">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="text-center">
        {!! $coupons->render() !!}
    </div>
@stop
@section('script')

@stop