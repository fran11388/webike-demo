@extends('backend.layouts.default')
@section('style')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
    <style>
        .float-right {
            float:right;
            margin-right:10px;
        }
    </style>
@stop
@section('middle')
    <div class="newsletter-container ">
        <div class="clearfix">
            @if (count($errors) > 0)
                <div class="alert alert-danger content-last-block">
                    <ul style="list-style-type: none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="create-form" method="get" action="{{ \URL::route('backend-coupon-filter') }}">
                <div class="panel panel-primary search-container">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspEdit COUPON</h3>
                    </div>
                    <div class="panel-body">
                        <div class="basic-set col-md-6 col-sm-6 col-xs-6">
                            <div class="name-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">ID*</span>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <input class="form-control" placeholder="From" type="text" name="id_begin" value="{{ request()->get('id_begin') ? request()->get('id_begin') : '' }}">
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <input class="form-control" placeholder="To" type="text" name="id_end" value="{{ request()->get('id_end') ? request()->get('id_end') : '' }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">Name*</span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <input class="form-control" placeholder="Name" type="text" name="realname" value="{{ request()->get('realname') ? request()->get('realname') : '' }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">Birthday*</span>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <input class="datetimepicker4 form-control" placeholder="From" type="text" name="birthday-begin" value="{{ request()->get('birthday-begin') ? request()->get('birthday-begin') : '' }}">
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <input class="datetimepicker4 form-control" placeholder="To" type="text" name="birthday-end" value="{{ request()->get('birthday-end') ? request()->get('birthday-end') : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="basic-set col-md-6 col-sm-6 col-xs-6">
                            <div class="title-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">Email*</span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <input class="form-control" placeholder="email" type="text" name="email" value="{{ request()->get('email') ? request()->get('email') : '' }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">Role*</span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <select class="form-control" name="role_id">
                                        <option value="" selected>Choose role</option>
                                        @foreach(\Everglory\Models\Customer\Role::get() as $role)
                                            <option value="{{ $role->id }}" {{ request()->get('role_id')  == $role->id ? 'selected' : ''  }}> {{ $role->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">Received birthday coupon*</span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <label class="radio radio-inline">
                                        <input type="radio" class="radiobox" name="coupon_get" value="" checked>
                                        <span>not filter</span>
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" class="radiobox" name="coupon_get" value="yes" {{ request()->get('coupon_get') == 'yes' ? 'checked' : '' }}>
                                        <span>received already</span>
                                    </label>
                                    <label class="radio radio-inline">
                                        <input type="radio" class="radiobox" name="coupon_get" value="no"  {{ request()->get('coupon_get') == 'no' ? 'checked' : '' }}>
                                        <span>received yet</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="search-button text-right float-right">
                            <button type="submit" class="btn btn-danger" name="create" >Search</button>
                        </div>
                        <div class="clear-container text-right float-right">
                            <a class="btn btn-default" href="#" >Clear</a>
                        </div>
                    </div>
                </div>
            </form>
            <form id="create-form" method="post" action="{{ \URL::route('backend-coupon-history-post') }}">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspEdit COUPON</h3>
                    </div>
                    <div class="panel-body">
                        <div class="basic-set col-md-12 col-sm-12 col-xs-12">
                            <div class="name-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">ID*</span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <select class="form-control select2" name="couponbase_id">
                                        <option value="">Please choose coupon.</option>
                                        @foreach(\Everglory\Models\Coupon\Base::get() as $coupon)
                                            <option value="{{ $coupon->id }}" > {{ $coupon->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                                    <span class="size-10rem">Name*</span>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <textarea class="form-control" name="customer_ids" required="">{{ $customer_ids }}</textarea>
                                </div>
                            </div>
                            <div class="text-right float-right">
                                <button type="submit" class="btn btn-danger send-button" name="send" >Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form method="get" class="row form clearfix">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-comments"></i>&nbspCustomer List</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Realname</th>
                                        <th>Nickname</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>birthday</th>
                                        <th>Newsletter</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($customers as $customer)
                                        <tr>
                                            <td>
                                                {{ $customer->id }}
                                            </td>
                                            <td>
                                                {{ $customer->realname }}
                                            </td>
                                            <td>
                                                {{ $customer->nickname }}
                                            </td>
                                            <td>
                                                {{ $customer->email }}
                                            </td>
                                            <td>
                                                {{ $customer->role->name }}
                                            </td>
                                            <td>
                                                {{ $customer->birthday }}
                                            </td>
                                            <td>
                                                @if($customer->newsletter)
                                                    V
                                                @else
                                                    X
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {!! $customers->render() !!}
        </div>
    </div>
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.datetimepicker4').datetimepicker({
                format:      'MM-DD'
            });
        });

        $('.clear-container a').click(function (){
            $('.search-container input').val("")
        });

        $('.send-button').click(function(e){
            var result = false;
           $('.select2 option:selected').each(function(){
               if($(this).val()){
                   result = true;
               }
           });
           if(!result){
               alert("Coupon is not selected.");
               e.preventDefault();
           }
        });
    </script>
@stop