@extends('backend.layouts.default')
@section('style')
	<style>
		.btn-danger {
			margin-right:15px;
		}
	</style>
@stop
@section('middle')
	<form method="post" action="{{$assortment ? \URL::route('backend-assortment-insert-post',['assortment_id' => $assortment->id]) : \URL::route('backend-assortment-insert-post')}}" class="row newForm">
		<input type="hidden" name="photo_key" value="">
		<div class="col-lg-12 col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-comments"></i>版型選擇</h3>
                </div>
                <div class="panel-body">
                	<p><span class="size-10rem font-color-red">※</span><span class="size-10rem">請選擇版型</span></p>
                	<div>
                		<select class="select2" name="layout_id">
							@if($assortment)
                            	<option value="{{$assortment->layout->id}}">{{$assortment->layout->driver}}</option>
							@else
								<option value="">選擇版型</option>
							@endif
                            @foreach($layouts as $layout)
                                <option value="{{$layout->id}}">{{ $layout->driver }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="text-right">
                        <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-list-alt"></i>特輯</h3>
                </div>
                <div class="panel-body">
                	<p><span class="size-10rem font-color-red">※</span><span class="size-10rem">設定您的特輯資訊</span></p>
                	<div>
						<div class="col-log-6 col-md-6">
							<div class="form-group">
								<label><i class="fa fa-edit big-icon"></i><span class="font-color-red">※</span>輸入特輯網址代稱</label>
								<input class="form-control" name="url_rewrite" value="{{$assortment ? $assortment->url_rewrite : ''}}" placeholder="網址顯示，如CB-400。">
							</div>
							<div class="form-group">
								<label><i class="fa fa-edit big-icon"></i><span class="font-color-red">※</span>特輯名稱</label>
								<input class="form-control" name="name" value="{{$assortment ? $assortment->name : ''}}" placeholder="特輯名稱">
							</div>
							<div class="form-group">
								<label><i class="fa fa-edit big-icon"></i>特輯顯示日期<span class="font-color-red">※預設當日。請參照相同格式修改</span></label>
								<div class="form-group input-group width-full">
									<input type="text" name="publish_at" value="{{$assortment ? $assortment->publish_at : ''}}" class="form-control">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label><i class="fa fa-sitemap big-icon"></i><span class="font-color-red">※</span>特輯分類</label>
								<div>
									<select class="select2" name="type_id">
										@if($assortment)
											<option value="{{$assortment->type->id}}" selected="selected">{{$assortment->type->name}}</option>
										@else
											<option value="">請選擇特輯種類</option>
										@endif
										@foreach($types as $type)
											<option value="{{$type->id}}">{{ $type->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="col-log-6 col-md-6">
							<div class="form-group">
								<label><i class="fa fa-camera-retro big-icon"></i><span class="font-color-red">※</span>特輯圖片</label>
								<div class="uploadFile width-full">
									<figure class="ct-img-left thumb-box-border thumb-img">
										<img src="{{$assortment ? $assortment->banner : assetRemote('image/bg-btn-choose-file.jpg')}}" alt="" id="photo">
										<a class="btn btn-primary border-radius-2 btn-choose-file visible-sm visible-md visible-lg"
										   href="javascript:$('#choose-file').click();">上傳圖片</a>
									</figure>
									<a class="btn btn-primary border-radius-2 btn-choose-file visible-xs"
									   href="javascript:$('#choose-file').click();">上傳圖片</a>

									<input type="hidden" name="photo_preview" value="value">
									<input type="hidden" name="banner" value="{{$assortment ? $assortment->banner : ''}}">
									{{--<input name="banner" value="{{$assortment ? $assortment->banner : ''}}" type="file">--}}
								</div>
							</div>

							{{--<div class="form-group">--}}
								{{--<label><i class="fa fa-edit big-icon"></i>對應車型<span class="font-color-red">※請由車型頁面網址獲取代碼，<a href="{{\URL::route('summary',['mt' => 215])}}">範例</a>代碼為215</span></label>--}}
								{{--<input class="form-control" placeholder="Enter text">--}}
							{{--</div>--}}
						</div>
						<div class="col-log-12 col-md-12">
							<div class="form-group">
								<label><i class="fa fa-edit big-icon"></i>品牌簡述</label>
								<textarea name="meta_description" class="form-control" rows="5">{{$assortment ? $assortment->meta_description : ''}}</textarea>
							</div>
						</div>
                    </div>
                </div>
				<div class="text-right block">
					<input type="submit" name="editFinish" class="btn btn-danger" value="{{$assortment ? '編輯完成' : '新增'}}">
				</div>
            </div>
        </div>
	</form>
	@if($assortment)
		<form method="post" action="{{\URL::route('backend-assortment-edit-post',['assortment_id' => $assortment->id])}}" class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row clearfix">
							<h3 class="panel-title col-md-6"><i class="fa fa-list-alt"></i>現有區塊</h3>
							<div class="text-right col-md-6">
								<a href="{{\URL::route('backend-assortment-insert-block',['assortment_id' => $assortment->id])}}" class="btn btn-warning newBlock">新增區塊</a>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped">
							<thead>
							<tr>
								<th>全選<input type="submit" name="delete" value="刪除" class="btn btn-default"></th>
								{{--<th>區塊種類</th>--}}
								<th>名稱</th>
								<th>編輯</th>
								<th><input type="submit" name="sort" value="變更順序" class="btn btn-default"></th>
							</tr>
							</thead>
							<tbody>
							@foreach($assortment->items as $key => $item)
								@php
									$template_id = $item->template_id;
									$plugin_id = $item->plugin_id;
									$function = '';
									if($item->plugin->templates){
										foreach($item->plugin->templates as $template){
											if($template->id == $template_id){
												$function = $template->pivot->function;
											}
										}
									}
								@endphp
								<tr>
									<td><input type="checkbox" name="delete[{{$item->id}}]"></td>
									<td>{{$function}}</td>
									<td><a href="{{\URL::route('backend-assortment-insert-block',['assortment_id' => $assortment->id,'assortmentItem_id' => $item->id])}}" class="btn btn-warning">編輯</a></td>
									<td>
										<select class="select2" name="sort[{{$item->id}}]">
											<option value="{{$item->sort}}">{{$item->sort}}</option>
											@for($num = 1 ;$num <= 20 ; $num++)
												@if($item->sort != $num)
													<option value="{{$num}}">{{$num}}</option>
												@endif
											@endfor
										</select>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</form>
	@endif
	<form style="position: absolute;left:-1000px;top:0px;" id="hidden-form" target="upload_target"
		  action="{{route('backend-assortment-upload')}}" method="post" enctype="multipart/form-data">
		<input id="choose-file" type="file" size="40" name="photo" accept="image/*" style="width:260px;margin:0" value="value">
		<input type="hidden" size="40" name="path" value="/assets/images/user_upload/collection/">
	</form>

	<iframe id="upload_target" name="upload_target" src="#"
			style="display:none; width:0;height:0;border:0px solid #fff;"></iframe>
@stop
@section('script')
	<script>
		$('input[name="editFinish"]').click(function(e){
		    var toReturn = true;
			$('.newForm input').each(function(key,element){
			    if($(element).attr('type') != 'hidden') {
                    if (!$(element).val()) {
                        alert('欄位尚未輸入');
                        toReturn = false;
                        return false;
                    }
                }
			});
            $('.newForm .select2 option:selected').each(function(key,element){
                if(!$(element).val()){
                    alert('尚未選擇');
                    toReturn = false;
                    return false;
                }
            });
            if(!toReturn){
                e.preventDefault();
			}
		});
        $('input[type="file"]').change(function () {
            $('input[name="photo_preview"]').val('1');
            $('#hidden-form').submit();
        });
        function stopUpload(result) {
            if (result.success) {
                $('input[name="photo_key"]').val(result.key);
                $("#photo").attr('src', "{{assetPathTranslator('assets/images/user_upload/collection/',config('app.env'))}}"  +'/'+  result.key);
                $('input[name="banner"]').val('https://img.webike.tw/collection/' + result.key);
            } else {
                $('input[name="photo_key"]').val('');
                $("#photo").attr('src', "{{assetPathTranslator('image/bg-btn-choose-file.jpg',config('app.env'))}}");
                if (result.reload) {
                    window.location.reload();
                    return false;
                }
            }
            return true;
        }
	</script>
@stop