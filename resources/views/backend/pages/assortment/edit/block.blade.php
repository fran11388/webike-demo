@extends('backend.layouts.default')
@section('style')
    <style>
        .float-left {
            float:left;
            margin-right:10px;
        }
        .block-label{
            display:block;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/froala_editor.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/froala_style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/fullscreen.min.css') }}">

    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/char_counter.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/code_view.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/colors.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/fullscreen.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/image.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/image_manager.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/line_breaker.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/table.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/video.min.css') }}">
@stop
@section('middle')
<form method="post" action={{ $assortmentItem ? \URL::route('backend-assortment-insert-block-post',['assortment_id' => $assortment_id,'assortmentItem_id' => $assortmentItem->id]) : \URL::route('backend-assortment-insert-block-post',['assortment_id' => $assortment_id])}}>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-file"></i>現有區塊</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label><i class="fa fa-sitemap big-icon"></i><span class="font-color-red">※</span>請選擇區塊位置</label>
                        <div class="box">
                            <select class="select2 select-position" name="section[]">
                                @foreach($layoutSection as $section)
                                    <option value="{{$section}}" {{$assortmentItem ? $assortmentItem->section == $section ? 'selected' : '' : ''}}>{{$section}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label><i class="fa fa-sitemap big-icon"></i><span class="font-color-red">※</span>請選擇區塊類別</label>
                        <div class="box">
                            <select class="select2 select-block" name="plugin_template[]">
                                @if(!$assortmentItem)
                                    <option value="請選擇特輯分類">請選擇區塊類別</option>
                                @endif
                                @foreach($pluginTemplates as $pluginTemplate)
                                    @foreach($pluginTemplate->templates as $template_detail)
                                        <option value="{{$pluginTemplate->id.','.$template_detail->id}}" {{$assortmentItem ? $template->pivot->plugin_id.','.$template->pivot->template_id == $pluginTemplate->id.','.$template_detail->id ? 'selected' : 'disabled' : ''}}>{{ $template_detail->pivot->function }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        @if($assortmentItem)
                            <div class="old-column-container">
                                <div class="box old-column">
                                    @foreach($data as $column => $info)
                                        <div class="column {{$column}}">
                                            @if(is_array($info))
                                                @if($column == 'product_url_rewrites')
                                                    <label class="block-label">
                                                        <i class="fa fa-edit big-icon"></i>sku
                                                    </label>
                                                    <textarea name="product_url_rewrites[]" class="block-input box width-full sku" rows="5">{{implode("\r\n",$info)}}</textarea>
                                                @endif
                                                @foreach($info as $value)
                                                    @if($column != 'product_url_rewrites')
                                                        <label class="block-label">
                                                            <i class="fa fa-edit big-icon"></i>{{array_keys($column_info[$column])[0]}}
                                                        </label>
                                                    @endif
                                                    @if($column == 'description' || $column == 'single_html')
                                                        <textarea class="trans_content" name="{{$column.'[]'}}"  style="width: 100%;height: 200px">{{$value}}</textarea>
                                                    @elseif($column == 'image')
                                                        <figure class="ct-img-left thumb-box-border thumb-img">
                                                            <img src="{{$value ? $value : assetRemote('image/bg-btn-choose-file.jpg')}}" alt="" class="photo" name="photo[]">
                                                            <input type="hidden" name="image[]" class="image" value="{{$value ? $value : ''}}">
                                                            <input type="hidden" name="photo_key[]" class="photo_key" value="">
                                                            <a class="btn btn-primary border-radius-2 btn-choose-file visible-sm visible-md visible-lg"
                                                               href="javascript:$('#choose-file').click();">上傳圖片</a>
                                                        </figure>
                                                        <a class="btn btn-primary border-radius-2 btn-choose-file visible-xs"
                                                           href="javascript:$('#choose-file').click();">上傳圖片</a>

                                                        <input type="hidden" name="photo_preview" value="">
                                                    @elseif($column == 'product_url_rewrites')
                                                    @else
                                                        <input type="input" class="form-control block-input box " name="{{$column.'[]'}}" value="{{$value}}">
                                                    @endif
                                                @endforeach
                                            @else
                                                <label class="block-label">
                                                    <i class="fa fa-edit big-icon"></i>{{array_keys($column_info[$column])[0]}}
                                                </label>
                                                @if($column == 'description' || $column == 'single_html')
                                                    <textarea class="trans_content" name="{{$column.'[]'}}"  style="width: 100%;height: 200px">{{$info}}</textarea>
                                                @elseif($column == 'image')
                                                    <figure class="ct-img-left thumb-box-border thumb-img">
                                                        <img src="{{ $info ? $info : assetRemote('image/bg-btn-choose-file.jpg')}}" alt="" class="photo" name="photo[]">
                                                        <input type="hidden" name="image[]" class="image" value="{{$info ? $info : ''}}">
                                                        <input type="hidden" name="photo_key[]" class="photo_key" value="">
                                                        <a class="btn btn-primary border-radius-2 btn-choose-file visible-sm visible-md visible-lg"
                                                           href="javascript:$('#choose-file').click();">上傳圖片</a>
                                                    </figure>
                                                    <a class="btn btn-primary border-radius-2 btn-choose-file visible-xs"
                                                       href="javascript:$('#choose-file').click();">上傳圖片</a>

                                                    <input type="hidden" name="photo_preview" value="">
                                                @elseif($column == 'product_url_rewrites')
                                                    <textarea name="product_url_rewrites[]" class="block-input box width-full sku" rows="5"></textarea>
                                                @else
                                                    <input type="input" class="form-control block-input box " name="{{$column.'[]'}}" value="{{$info}}">
                                                @endif
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="new-column-container">
                            <div class="box new-column">
                            </div>
                        </div>
                        <div class="box new-button-container clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-right box">
        <input type="submit" class="btn btn-danger" name="newBlock" value="{{$assortmentItem ? '編輯完成' : '新增'}}">
    </div>
</form>
@if(!$assortmentItem)
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-file"></i>區塊範例</h3>
                </div>
                <div class="panel-body">
                    <div class="example">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<form style="position: absolute;left:-1000px;top:0px;" id="hidden-form" target="upload_target"
      action="{{route('backend-assortment-upload')}}" method="post" enctype="multipart/form-data">
    <input id="choose-file" type="file" size="40" name="photo" accept="image/*" style="width:260px;margin:0">
    <input type="hidden" size="40" name="path" value="/assets/images/user_upload/collection/">
</form>

<iframe id="upload_target" name="upload_target" src="#"
        style="display:none; width:0;height:0;border:0px solid #fff;"></iframe>

<div class="image-clone hidden-lg hidden-md hidden-sm">
    <figure class="ct-img-left thumb-box-border thumb-img">
        <img src="{{assetRemote('image/bg-btn-choose-file.jpg')}}" alt="" class="photo" name="photo[]">
        <input type="hidden" name="image[]" class="image" value="">
        <input type="hidden" name="photo_key[]" class="photo_key" value="">
        <a class="btn btn-primary border-radius-2 btn-choose-file visible-sm visible-md visible-lg"
           href="javascript:$('#choose-file').click();">上傳圖片</a>
    </figure>
    <a class="btn btn-primary border-radius-2 btn-choose-file visible-xs"
       href="javascript:$('#choose-file').click();">上傳圖片</a>

    <input type="hidden" name="photo_preview" value="">
</div>

<div class="textarea-clone hidden-lg hidden-md hidden-sm">
    <div class="description">
        <label class="block-label"><i class="fa fa-edit big-icon"></i>敘述<span class="font-color-red font-bold">必填</span></label>
        <textarea class="form-control trans_content description" name="description[]" style="width: 100%; height: 200px;"></textarea>
    </div>
</div>
@stop
@section('script')
    <script src="{{ assetRemote('js/backend/froala_editor_2.6.1/js/froala_editor.min.js')}}"></script>
    <script src="{{ assetRemote('js/backend/froala_editor_2.6.1/js/languages/zh_tw.js')}}"></script>
    <script src="{{ assetRemote('js/backend/froala_editor_2.6.1/js/plugins/fullscreen.min.js')}}"></script>

    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/align.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/char_counter.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/code_view.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/colors.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/entities.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/font_family.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/font_size.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/fullscreen.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/image.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/image_manager.min.js')}}"></script>
    {{-- 	<script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/line_breaker.min.js')}}"></script> --}}
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/link.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/lists.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/paragraph_format.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/paragraph_style.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/quote.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/save.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/table.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/url.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/video.min.js')}}"></script>
    <script>
        $(document).on("click",".btn-choose-file",function(){
            $('.photo').removeClass('photoActive');
            $('.image').removeClass('bannerActive');
            $('.photo_key').removeClass('nameActive');
            $(this).siblings('.photo').addClass('photoActive');
            $(this).siblings('.image').addClass('bannerActive');
            $(this).siblings('.photo_key').addClass('nameActive');
        });
        $('input[type="file"]').change(function () {
            $('input[name="photo_preview"]').val('1');
//            $('#writing_btn').prop('disabled', true).css('cursor', 'no-drop');
            $('#hidden-form').submit();
        });
        function stopUpload(result) {
            $('input[name="photo_preview"]').val('');
            if (result.success) {
                $('input[name="photo_key"]').val(result.key);
                $('.photoActive').attr('src', "{{assetPathTranslator('assets/images/user_upload/collection/',config('app.env'))}}"  +'/'+  result.key);
                $('.bannerActive').val('https://img.webike.tw/collection/' + result.key);
                $('.nameActive').val(result.key);
            } else {
                $('input[name="photo_key"]').val('');
                $('.photoActive').attr('src', "{{assetPathTranslator('image/bg-btn-choose-file.jpg',config('app.env'))}}");
                if (result.reload) {
                    window.location.reload();
                    return false;
                }
            }
            return true;
        }
        $('.select-block').change(function(){
            $('.new-column div').remove();
            $('.new-button-container div').remove();
            $('.example .render').remove();
            var value = $('.select-block option:selected').val();
            var pluginTemplate = value.split(',');
            var plugin = pluginTemplate[0];
            var template = pluginTemplate[1];
            var token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{URL::Route('backend-assortment-columns')}}",
                type:"POST",
                data: {'_token': token,'plugin':plugin,'template': template},
                dataType:'JSON',
                success:function(result){

                    $.each(result.columns,function(column,column_ch){
                        if(column_ch == '圖片'){
                            var file = $('.image-clone').html();
                            var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "<span class=\"font-color-red font-bold\">必填</span></label>"+ file +"</div>";
                        }else if(column_ch == 'sku'){
                            var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "<span class=\"font-color-red font-bold\">必填</span></label><textarea name=\"" + column + "\" class=\"block-input box width-full " + column +"\" name=\"" + column + "[]\" rows=\"5\"></textarea></div>";
                        }else if(column_ch == '敘述' || column == 'single_html'){
                            var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "<span class=\"font-color-red font-bold\">必填</span></label><textarea class=\"form-control trans_content " + column +"\" name=\"" + column + "[]\" style=\"width: 100%;height: 200px\"></textarea></div>";
                        }else{
                            var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "<span class=\"font-color-red font-bold\">必填</span></label><input class=\"form-control block-input box " + column + "\" name=\"" + column + "[]\"placeholder=\"" + column_ch + "\"></div>";
                        }

                        $('.new-column').append(input);
                    });
                    if(result.other_columns){
                        appendColumns(result.other_columns);
                    }
                    if(result.example){
                        var example = result.example;
                        var render = "<div class=\"render\">" + result.example + "</div>";
                        $('.example').append(render);
                    }
                    $.each(result.quantity,function(column,next){
                        $.each(next, function (name, quantity) {
                            if(quantity == 'mutiple'){
                                var name = "新增" + name;
                                var button = "<div class=\"float-left\"><div class=\""+column+"\"><input type=\"button\" name=\""+ column +"\"class=\"btn btn-primary new-columns mutiple\" value=\"" + name + "\"></div></div>";
                                $('.new-button-container').append(button);
                            }
                        });
                    });
                    froala();
                },error:function(){
                    alert("error!!!!");
                }
            });
        });

        $(document).on("click",".mutiple",function(){
            var className = $(this).parent().attr('class');
            if(className == 'description') {
                var htmlElement = "<div class=\"desciption\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>敘述<span class=\"font-color-red font-bold\">必填</span></label><textarea class=\"form-control trans_content description\" name=\"description[]\" style=\"width: 100%;height: 200px\"></textarea></div>";
                $('.new-column').append(htmlElement);
                froala();
            }else{
                var classStr = '.new-column .' + $(this).parent().attr('class');
                var htmlElement = $(classStr).first().clone();
                $('.new-column').append(htmlElement);
            }
        });

        function appendColumns(result)
        {
            $.each(result,function(column,column_ch){
                if(column_ch == '圖片'){
                    var file = $('.image-clone').html();
                    var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "</label>" + file + "</div>";
                }else if(column_ch == '敘述' || column == 'single_html'){
                    var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "</label><textarea class=\"form-control trans_content " + column +"\" name=\"" + column + "[]\" style=\"width: 100%;height: 200px\"></textarea></div>";
                }else{
                    var input = "<div class=\""+ column +"\"><label class=\"block-label\"><i class=\"fa fa-edit big-icon\"></i>" + column_ch + "</label><input class=\"form-control block-input box\" name=\"" + column + "[]\"placeholder=\"" + column_ch + "\"></div>";
                }

                $('.new-column').append(input);
                froala();
            });
        }

        $(document).ready(function(){
            froala();
        });

        function froala() {
            $('.trans_content').froalaEditor({
                language: 'zh_tw',
                height: 200,
                toolbarButtons: [
                    'fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'color', '|', 'insertTable', 'insertLink', 'insertImage', 'insertVideo',
                    'align', 'formatOL', 'formatUL', 'outdent', 'indent', '|', 'paragraphStyle', 'quote', 'insertHR', 'undo', 'redo', 'clearFormatting', '|', 'fullscreen', 'html'],
                // toolbarButtons: ['colors' , 'insertTable', '|', 'paragraphFormat', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'indent', 'outdent'],
            }).on('froalaEditor.contentChanged', function () {
                console.log('content changed');
                $('a:contains("Unlicensed Froala Editor")').hide();
            });
            $('a:contains("Unlicensed Froala Editor")').hide();
        };

    </script>
@stop
