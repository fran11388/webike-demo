@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            height:190px;
            width:450px;
        }
    </style>
@stop
@section('middle')
    <form method="post" action="{{\URL::route('backend-assortment-post')}}" class="row form">
        <div class="col-lg-12">
            <div class="text-right block">
                <a class="btn btn-warning" href="{{\URL::route('backend-assortment-insert')}}">新增特輯</a>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>流行特輯後台</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th><input class="btn btn-default" name="deleteButton" type="submit" value="刪除"></th>
                                    <th><input class="btn btn-default" name="activeStatus" type="submit" value="變更狀態"></th>
                                    <th>主圖</th>
                                    <th>名稱</th>
                                    <th>特輯類型</th>
                                    <th>版型</th>
                                    <th>特輯區塊數</th>
                                    <th>顯示時間</th>
                                    <th>修改時間</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assortments as $assortment)
                                    <tr>
                                        <td><input class="btn btn-default" name="delete[]" type="checkbox" value="{{$assortment->id}}"></td>
                                        <td>
                                            <select class="select2" name="active[{{$assortment->id}}]">
                                                @foreach($active as $key => $status)
                                                    <option value="{{$key}}" {{$assortment->active == $key ? 'selected' : ''}}>{{$status}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><img src="{{$assortment->banner}}"></td>
                                        <td>{{$assortment->name}}</td>
                                        <td>{{$assortment->type->name}}</td>
                                        <td>{{$assortment->layout->driver}}</td>
                                        <td>{{count($assortment->items)}}</td>
                                        <td>{{$assortment->publish_at}}</td>
                                        <td>{{$assortment->updated_at}}</td>
                                        <td><a href="{{\URL::route('backend-assortment-insert',['assortment_id' => $assortment->id])}}" class="btn btn-warning">編輯</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop
@section('script')
    <script>
        $('input[name="deleteButton"]').click(function(e) {
            e.preventDefault();
            var check = false;
            $("input[type='checkbox']").each(function (event) {
                if ($(this).prop("checked")) {
                    check = true;
                }
            });
            if (check){
                var form = $('.form');
                swal({
                    title: "你確定要刪除嗎?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "確定",
                    cancelButtonText: '取消',
                    closeOnConfirm: false
                }).then(function () {
                    form.submit();
                });
            }else{
                alert('未選擇刪除項目');
            }
        });
    </script>
@stop
