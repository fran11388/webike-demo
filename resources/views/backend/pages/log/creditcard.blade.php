@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row {
            margin-bottom: 15px;
            font-size: 17px;
        }

        .big-icon {
            width: 24px;
            height: 24px;
        }

        .selecter, input {
            height: 30px;
        }

        .selecter option {
            height: 30px;
        }

        .big_checkbox {
            width: 20px;
            height: 20px;
        }

        .reset {
            display: block;
            width: 60px;
            height: 20px;
            margin: 5px auto;
            background-color: #eee;
            border: 2px inset;
            border-spacing: 2px;
            color: #000;
            -webkit-appearance: button;
            cursor: pointer;
            box-sizing: content-box;
            font-weight: normal;
        }

        table {
            font-size: 12px;
            table-layout: fixed;
            word-break: break-all;
            width: 1450px;
        }

        table th {
            text-align: center;
        }

        table .num {
            text-align: right;
        }

        table input {
            width: 80%;
        }

        h2.title{
            display:inline;
        }
    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2 class="title"> 信用卡對帳Log </h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <div class="row">
                            <div class="col col-12 col-lg-12">
                                <table class="table table-bordered table-striped table-hover no-footer smart-form"
                                       style="width:100%">
                                    <thead>
                                    <form action="" method="GET">
                                        <tr>
                                            <th width="5%">
                                                每頁顯示<br/>
                                                <select name="limit" style="width: 60px">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>
                                                        20
                                                    </option>
                                                    <option value="50" {{$request->get('limit') == 50 ? 'selected' : ''}}>
                                                        50
                                                    </option>
                                                    <option value="100" {{$request->get('limit') == 100 ? 'selected' : ''}}>
                                                        100
                                                    </option>
                                                </select>
                                                項<br/>
                                            <!--
                                                    產出CSV
                                                    <select name="csv_output" style="width: 60px">
                                                        <option value="" {{ ($request->get('csv_output') == '' or !$request->get('limit')) ? 'selected' : ''}}>否</option>
                                                        <option value="true" {{$request->get('csv_output') == 'true' ? 'selected' : ''}}>是</option>
                                                    </select>
                                                    -->
                                            </th>
                                            <th width="15%">
                                                從 <input type="text" name="created_at[0]"
                                                         value="{{$request->get('created_at')  ? $request->get('created_at')[0] : ''}}"
                                                         placeholder="限制最舊時間"/>
                                                到 <input type="text" name="created_at[1]"
                                                         value="{{$request->get('created_at')  ? $request->get('created_at')[1] : ''}}"
                                                         placeholder="限制最新時間"/>
                                            </th>
                                            <th width="10%">
                                                <input type="text" name="increment_id"
                                                       value="{{$request->get('increment_id')  ? $request->get('increment_id') : ''}}"
                                                       placeholder="訂單編號">
                                            </th>
                                            <th width="20%">
                                                <input type="text" name="ONO"
                                                       value="{{$request->get('ONO')  ? $request->get('ONO') : ''}}"
                                                       placeholder="對帳號碼">
                                            </th>
                                            <th width="5%">
                                                <select name="company" style="width: 120px">
                                                    <option value="" {{ (!$request->get('company')) ? 'selected' : ''}}>
                                                        全部公司
                                                    </option>
                                                    <option value="玉山" {{$request->get('company') == '玉山' ? 'selected' : ''}}>
                                                        玉山
                                                    </option>
                                                    <option value="聯信" {{$request->get('company') == '聯信' ? 'selected' : ''}}>
                                                        聯信
                                                    </option>
                                                </select>
                                            </th>
                                            <th width="25%"></th>
                                            <th width="25%">
                                                <input type="text" name="email"
                                                       value="{{$request->get('email')  ? $request->get('email') : ''}}"
                                                       placeholder="用戶電子信箱">
                                            </th>
                                            <th width="5%">
                                                <input type="text" name="realname"
                                                       value="{{$request->get('realname')  ? $request->get('realname') : ''}}"
                                                       placeholder="用戶名稱">
                                            </th>
                                            <th width="10%">
                                                <input type="text" name="AN"
                                                       value="{{$request->get('AN')  ? $request->get('AN') : ''}}"
                                                       placeholder="信用卡卡號">
                                            </th>
                                            <th width="5%">
                                                <a href="{{URL::route('backend-log-creditcard')}}" class="reset">重置</a>
                                                <input type="submit" value="篩選"
                                                       style="width: 60px;height: 20px;margin: 10px 0;"/><br/>
                                                <br>
                                                <input type="text" name="TA"
                                                       value="{{$request->get('TA')  ? $request->get('TA') : ''}}"
                                                       placeholder="金額">
                                            </th>

                                        </tr>
                                    </form>
                                    <tr>
                                        <!--<th style="width:42px;padding:10px;">
                                            <input type="checkbox" class="big_checkbox" id="checked_all">全選
                                        </th>-->
                                        <th width="5%">No.</th>
                                        <th width="15%">查詢時間</th>
                                        <th width="10%">訂單編號</th>
                                        <th width="20%">信用卡對帳號碼</th>
                                        <th width="5%">信用卡公司</th>
                                        <th width="25%">分期資訊</th>
                                        <th width="25%">email</th>
                                        <th width="5%">客戶名稱</th>
                                        <th width="10%">信用卡卡號</th>
                                        <th width="5%">付款金額</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($logs as $num => $item)
                                        <?php
                                        $order = $item->order;

                                        if (strpos($item->ONO, 'NCCC') !== false) {
                                            $company = '聯信';
                                        } else {
                                            $company = '玉山';
                                        }
                                        ?>

                                        @if(isset($item->RRN) and !$order)
                                            <tr class="danger">
                                        @else
                                            <tr>
                                                @endif

                                                <td>{{ $num+1 }}</td>
                                                <td>{{ $item->created_at }}</td>

                                                <td>{{ $order ? $order->increment_id : '無單號' }}</td>
                                                <td>{{ $item->ONO }}</td>
                                                @if($order)
                                                    <?php
                                                    $payment_result = paymentFormat($order, $item);
                                                    ?>
                                                    <td>{{$payment_result['company']}}</td>
                                                    <td>
                                                        {{$payment_result['install_info']}}
                                                        @if($payment_result['risk'])
                                                            <span style="color:red;">**風險帳戶!**</span>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>{{$company}}</td>
                                                    <td></td>
                                                @endif
                                                <td>
                                                    @if(!$item->customer)
                                                        {{ ($order and $order->customer) ? $order->customer->email : '無單號' }}
                                                    @else
                                                        {{$item->customer->email}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!$item->customer)
                                                        {{ ($order and $order->customer) ? $order->customer->realname : '無單號' }}
                                                    @else
                                                        {{$item->customer->realname}}
                                                    @endif
                                                </td>
                                                <td>{{ $item->AN }}</td>
                                                <td>{{ $item->TA }}</td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webike2_pagination webike2_box" style="text-align: center">
                {{ $logs->appends($request->except('page'))->links() }}
            </div>
        </article>
    </div>
@stop