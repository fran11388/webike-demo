@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row{
            margin-bottom:15px;
            font-size: 17px;
        }
        .big-icon{
            width:24px;
            height:24px;
        }
        .selecter,input{
            height:30px;
        }
        .selecter option{
            height:30px;
        }
        .big_checkbox{
            width:20px;
            height:20px;
        }
        .reset{
            display: block;
            width: 60px;
            height: 20px;
            margin: 5px auto;
            background-color: #eee;
            border: 2px inset;
            border-spacing: 2px;
            color: #000;
            -webkit-appearance: button;
            cursor: pointer;
            box-sizing: content-box;
            font-weight: normal;
        }
        table{
            font-size: 12px;
            table-layout: fixed;
            word-break: break-all;
            width:1450px;
        }
        table th{
            text-align: center;
        }
        table .num{
            text-align: right;
        }
        table input{
            width: 80%;
        }
        #wid-id-0 > header > h2{
            display: inline;
        }
    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2> 虛擬帳戶Log </h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <div class="row">
                            <div class="col col-12 col-lg-12">
                                <table class="table table-bordered table-striped table-hover no-footer smart-form" style="width:100%">
                                    <thead>
                                    <form action="" method="GET">
                                        <tr>
                                            <th width="5%">
                                                每頁顯示<br/>
                                                <select name="limit" style="width: 60px">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>20</option>
                                                    <option value="50" {{$request->get('limit') == 50 ? 'selected' : ''}}>50</option>
                                                    <option value="100" {{$request->get('limit') == 100 ? 'selected' : ''}}>100</option>
                                                </select>
                                                項<br/>
                                                產出CSV
                                                <select name="csv_output" style="width: 60px">
                                                    <option value="" {{ ($request->get('csv_output') == '' or !$request->get('limit')) ? 'selected' : ''}}>否</option>
                                                    <option value="true" {{$request->get('csv_output') == 'true' ? 'selected' : ''}}>是</option>
                                                </select>

                                            </th>
                                            <th width="12%">

                                                從 <input type="text" name="bank_deal_date[0]" value="{{$request->get('bank_deal_date')  ? $request->get('bank_deal_date')[0] : ''}}" placeholder="限制最舊時間" />
                                                到 <input type="text" name="bank_deal_date[1]" value="{{$request->get('bank_deal_date')  ? $request->get('bank_deal_date')[1] : ''}}" placeholder="限制最新時間"/>
                                            </th>
                                            <th width="15%">
                                                <input type="text" name="increment_id" value="{{$request->get('increment_id')  ? $request->get('increment_id') : ''}}" placeholder="訂單編號">
                                            </th>
                                            <th width="15%">
                                                <input type="text" name="virtual_account" value="{{$request->get('virtual_account')  ? $request->get('virtual_account') : ''}}" placeholder="虛擬帳號">
                                            </th>
                                            <th width="10%">
                                                從 <input type="text" name="credit_amount[0]" value="{{$request->get('credit_amount')  ? $request->get('credit_amount')[0] : ''}}" placeholder="限制最低金額" />
                                                到 <input type="text" name="credit_amount[1]" value="{{$request->get('credit_amount')  ? $request->get('credit_amount')[1] : ''}}" placeholder="限制最高金額"/>
                                            </th>
                                            <th width="10%">
                                                <input type="text" name="realname" value="{{$request->get('realname')  ? $request->get('realname') : ''}}" placeholder="客戶姓名">
                                            </th>
                                            <th width="10%">
                                                <input type="text" name="email" value="{{$request->get('email')  ? $request->get('email') : ''}}" placeholder="Email">
                                            </th>
                                            <th width="10%">
                                                從 <input type="text" name="created_at[0]" value="{{$request->get('created_at')  ? $request->get('created_at')[0] : ''}}" placeholder="限制最舊時間" />
                                                到 <input type="text" name="created_at[1]" value="{{$request->get('created_at')  ? $request->get('created_at')[1] : ''}}" placeholder="限制最新時間"/>
                                            </th>

                                            <th>
                                                <a href="{{URL::route('backend-log-virtualbank')}}" class="reset">重置</a>
                                                <input type="submit" value="篩選" style="width: 60px;height: 20px;margin: 10px 0;" /><br/>
                                            </th>

                                        </tr>
                                    </form>
                                    <tr>
                                        <!--<th style="width:42px;padding:10px;">
                                            <input type="checkbox" class="big_checkbox" id="checked_all">全選
                                        </th>-->
                                        <th width="10%">No.</th>
                                        <th width="12%">匯款日期</th>
                                        <th width="15%">訂單編號</th>
                                        <th width="15%">虛擬帳號</th>
                                        <th width="10%">金額</th>
                                        <th width="10%">真實姓名</th>
                                        <th width="10%">Email</th>
                                        <th width="10%">建立時間</th>
                                        <th width=></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($collection as $num => $item)
                                        <?php
                                        $customer = null;
                                        $order = $item->order;
                                        if($order){
                                            $customer = $order->customer;
                                        }
                                        ?>
                                        <tr>
                                            <td>{{ $num+1 }}</td>
                                            <td>{{ $item->bank_deal_date }}</td>
                                            <td>{{ $order ? $order->increment_id : '-' }}</td>
                                            <td>{{ $item->virtual_account }}</td>
                                            <td>{{ $item->credit_amount }}</td>
                                            <td>{{ $customer ? $customer->realname : '-' }}</td>
                                            <td>{{ $customer ? $customer->email : '-' }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webike2_pagination webike2_box" style="text-align: center">
                {{ $collection->appends($request->except('page'))->links() }}
            </div>
        </article>
    </div>
@stop

@section('script')

@stop