@inject('logService', 'Ecommerce\Service\Backend\LogService')
@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row{
            margin-bottom:15px;
            font-size: 17px;
        }
        .big-icon{
            width:24px;
            height:24px;
        }
        .selecter,input{
            height:30px;
        }
        .selecter option{
            height:30px;
        }
        .big_checkbox{
            width:20px;
            height:20px;
        }
        table{
            font-size: 12px;
            table-layout: fixed;
            word-break: break-all;
            width:1450px;
        }
        table th{
            text-align: center;
        }
        table th select{
            width:60px;
        }
        table .num{
            text-align: right;
        }
        table input{
            width: 80%;
        }

        h2.title{
            display:inline;
        }

    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2 class="title"> 銷售品項LOG <B>(共{{$count}}項)</B></h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <div class="row">
                            <div class="col col-12 col-lg-12">
                                <table class="table table-bordered table-striped table-hover no-footer smart-form">
                                    <thead>
                                    <form action="" method="GET">
                                        <tr>
                                            <td>
                                                每頁顯示<br/>
                                                <select name="limit" style="width: 60px">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>20</option>
                                                    <option value="50" {{$request->get('limit') == 50 ? 'selected' : ''}}>50</option>
                                                    <option value="100" {{$request->get('limit') == 100 ? 'selected' : ''}}>100</option>
                                                </select>
                                                項<br/>
                                                產出CSV
                                                <select name="csv_output" style="width: 60px">
                                                    <option value="" {{ ($request->get('csv_output') == '' or !$request->get('limit')) ? 'selected' : ''}}>否</option>
                                                    <option value="true" {{$request->get('csv_output') == 'true' ? 'selected' : ''}}>是</option>
                                                </select>
                                            </td>
                                            <td >
                                                <input type="text" name="increment_id" value="{{$request->get('increment_id')  ? $request->get('increment_id') : ''}}" placeholder="訂單編號" />
                                            </td>
                                            <td >
                                                <input type="text" name="created_at[0]" value="{{$request->get('created_at')  ? $request->get('created_at')[0] : ''}}" placeholder="限制最舊時間" />
                                                <input type="text" name="created_at[1]" value="{{$request->get('created_at')  ? $request->get('created_at')[1] : ''}}" placeholder="限制最新時間"/>
                                            </td>
                                            <td >
                                                <input type="text" name="realname" value="{{$request->get('realname')  ? $request->get('realname') : ''}}" placeholder="姓名">
                                            </td>
                                            <td >
                                                <input type="text" name="email" value="{{$request->get('email')  ? $request->get('email') : ''}}" placeholder="用戶電子信箱">
                                            </td>
                                            <td>
                                                <select name="role_id">
                                                    <option value="" {{ ($request->get('role_id') == '' or !$request->get('role_id')) ? 'selected' : ''}}>All</option>
                                                    <option value="3" {{$request->get('role_id') == 3 ? 'selected' : ''}}>Wholesale</option>
                                                    <option value="2" {{$request->get('role_id') == 2 ? 'selected' : ''}}>General</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="manufacturer_name" value="{{$request->get('manufacturer_name')  ? $request->get('manufacturer_name') : ''}}" placeholder="品牌名稱">
                                            </td>
                                            <td>
                                                <input type="text" name="model_number" value="{{$request->get('model_number')  ? $request->get('model_number') : ''}}" placeholder="商品編號">
                                            </td>
                                            <td>
                                                <input type="text" name="product_name" value="{{$request->get('product_name')  ? $request->get('product_name') : ''}}" placeholder="商品名稱">
                                            </td>
                                            <td>
                                                <input type="text" name="sku" value="{{$request->get('sku')  ? $request->get('sku') : ''}}" placeholder="sku">
                                            </td>
                                            <td>
                                                <input type="text" name="price[0]" value="{{$request->get('price')  ? $request->get('price')[0] : ''}}" placeholder="限制最低金額" />
                                                <input type="text" name="price[1]" value="{{$request->get('price')  ? $request->get('price')[1] : ''}}" placeholder="限制最高金額"/>
                                            </td>
                                            <td>
                                                <input type="text" name="quantity" value="{{$request->get('quantity')  ? $request->get('quantity') : ''}}" placeholder="數量">
                                            </td>
                                            <td>
                                                <?php $suppliers = \Everglory\Models\Supplier::all(); ?>
                                                <select name="supplier_id" style="width:100%">
                                                    <option value="" {{$request->get('supplier_id') ? '' : 'selected'}}>不指定</option>
                                                    @foreach ($suppliers as $supplier)
                                                        <option value="{{$supplier->id}}" {{$request->get('supplier_id') === $supplier->id ? 'selected' : ''}}>{{$supplier->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="motors" value="{{$request->get('motors')  ? $request->get('motors') : ''}}" placeholder="車型名稱">
                                            </td>
                                            <td>
                                                <input type="text" name="models" value="{{$request->get('models')  ? $request->get('models') : ''}}" placeholder="分類">
                                            </td>
                                            <td>
                                                <input type="text" name="attribute_tag" value="{{$request->get('attribute_tag')  ? $request->get('attribute_tag') : ''}}" placeholder="標籤" style="width: 100%;">
                                            </td>
                                            <td>
                                                <input type="text" name="category_name_path[2]" value="{{$request->get('category_name_path')  ? $request->get('category_name_path')[2] : ''}}" placeholder="分類名稱">
                                            </td>
                                            <td>
                                                <input type="text" name="category_name_path[3]" value="{{$request->get('category_name_path')  ? $request->get('category_name_path')[3] : ''}}" placeholder="分類名稱">
                                            </td>
                                            <td>
                                                <select name="category_url_path">
                                                    <option value="" {{ ($request->get('category_url_path') == '' or !$request->get('category_url_path')) ? 'selected' : ''}}>不指定</option>
                                                    <option value="1000" {{$request->get('category_url_path') == 1000 ? 'selected' : ''}}>改裝零件</option>
                                                    <option value="3000" {{$request->get('category_url_path') == 3000 ? 'selected' : ''}}>騎士用品</option>
                                                    <option value="9000" {{$request->get('category_url_path') == 9000 ? 'selected' : ''}}>正廠零件</option>
                                                    <option value="t001" {{$request->get('category_url_path') == 't001' ? 'selected' : ''}}>未登錄商品</option>
                                                    <option value="t002" {{$request->get('category_url_path') == 't002' ? 'selected' : ''}}>團購商品</option>
                                                </select>
                                            </td>
                                            <td></td>
                                            <td >
                                                <?php $status_group = \Everglory\Models\Order\Status::all(); ?>
                                                <select name="status_id">
                                                    <option value="" {{$request->get('status_id') ? '' : 'selected'}}>不指定</option>
                                                    @foreach ($status_group as $status)
                                                        <option value="{{$status->id}}" {{$request->get('status_id') === $status->id ? 'selected' : ''}}>{{$status->name}}</option>
                                                    @endforeach
                                                </select>
                                                <br/>
                                                <input type="submit" value="篩選" style="width: 60px;height: 20px;margin: 10px 5px 5px;" /><br/>
                                                <a href="{{URL::route('backend-log-sales')}}" style="width: 60px;height: 20px;margin: 0px 5px;">重置</a>
                                            </td>
                                        </tr>
                                    </form>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <!--<th style="width:42px;padding:10px;">
                                            <input type="checkbox" class="big_checkbox" id="checked_all">全選
                                        </th>-->
                                        <th >No.</th>
                                        <th >訂單編號</th>
                                        <th >查詢時間</th>
                                        <th >姓名</th>
                                        <th >email</th>
                                        <th >帳號屬性</th>
                                        <th >品牌</th>
                                        <th >商品編號</th>
                                        <th >商品名稱</th>
                                        <th >sku</th>
                                        <th >商品金額</th>
                                        <th >數量</th>
                                        <th >商品來源</th>
                                        <th >對應車型</th>
                                        <th >型式/分類</th>
                                        <th >標籤</th>
                                        <th >分類(第 2 層)</th>
                                        <th >分類(第 3 層)</th>
                                        <th >產品屬性</th>
                                        <th >選擇肢</th>
                                        <th >訂單狀態</th>
                                    </tr>
                                    @foreach ($order_items as $num => $item)
                                        <?php
//                                        $details = $item->itemDetail;
                                        $details=$logService->itemDetailRelation($item);
                                        if(isset($details->category_name_path) and isset($details->category_url_path)){
                                            $categories = explode('|', ($details->category_name_path ? $details->category_name_path : ''));
                                            $categories_code = explode('-', ($details->category_url_path ? $details->category_url_path : ''));
                                        }

                                        $manufacturer = $item->manufacturer;
                                        $product = $item->product;
                                        ?>
                                        <tr>
                                            <td>{{ $num+1 }}</td>
                                            <td>{{ $item->order->increment_id }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->customer->realname }}</td>
                                            <td>{{ $item->customer->email }}</td>
                                            <td>{{ $item->customer->role->name }}</td>
                                            <td>
                                                @if(isset($details->manufacturer_name))
                                                    {{ $details->manufacturer_name }}
                                                @elseif($manufacturer)
                                                    {{ $manufacturer->name }}
                                                @endif
                                            </td>
                                            <td>{{ $item->model_number }}</td>
                                            <td>
                                                @if( isset($categories_code) and $categories_code[0] != 9000)
                                                    <a href="{{URL::route('product-detail', $item->sku)}}" target="_blank">{{ $details->product_name }}</a>
                                                @elseif(isset($details->product_name))
                                                    {{ $details->product_name }}
                                                @elseif($product)
                                                    <a href="{{URL::route('product-detail', $item->sku)}}" target="_blank">{{ $product->name }}</a>
                                                @endif
                                            </td>
                                            <td>

                                                @if( !isset($categories_code))
                                                    {{ $item->sku }}
                                                @elseif(isset($categories_code) and $categories_code[0] != 9000)
                                                    {{ $item->sku }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td class="num">{{ number_format( $item->price, 0 ) }}</td>
                                            <td class="num">{{ $item->quantity }}</td>
                                            <td >
                                                @foreach ( $item->purchases as $purchase)
                                                    @if( $purchase->supplier )
                                                        {{$purchase->supplier->name}}
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td>
                                                <?php
                                                $motor_group = '';
                                                if( isset($details->motor_names) ){
                                                    $motor_group = str_replace('|', '<br>', $details->motor_names);
                                                }
                                                ?>
                                                <div style="max-height: 100px;overflow: auto;">
                                                    {{ $motor_group }}
                                                </div>
                                            </td>
                                            <td>
                                                <?php
                                                $models = '';
                                                if( isset($details->models) ){
                                                    $models = str_replace('|', '<br>', $details->models);
                                                    $models = str_replace('$', '<br>>>', $models);
                                                }
                                                ?>
                                                <div style="max-height: 100px;overflow: auto;">
                                                    {{ $models }}
                                                </div>
                                            </td>
                                            <td>
                                                {{ $item->product ? $item->product->attribute_tag : '' }}
                                            </td>
                                            @for( $i=1 ; $i < 3 ; $i++ )
                                                @if( isset( $categories[$i] ) )
                                                    <td>{{ $categories[$i] }}</td>
                                                @else
                                                    <td></td>
                                                @endif
                                            @endfor
                                            <td>{{ isset($categories) ? $categories[0] : '' }}</td>
                                            <td>{{ $item->product_option }}</td>
                                            <td>{{ $item->order->status->name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webike2_pagination webike2_box" style="text-align: center">
                {{ $order_items->appends($request->except('page'))->links() }}
            </div>
        </article>
    </div>
@stop
@section('script')

@stop