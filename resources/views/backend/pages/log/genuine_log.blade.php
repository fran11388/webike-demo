@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row{
            margin-bottom:15px;
            font-size: 17px;
        }
        .big-icon{
            width:24px;
            height:24px;
        }
        .selecter,input{
            height:30px;
        }
        .selecter option{
            height:30px;
        }
        .big_checkbox{
            width:20px;
            height:20px;
        }
        table{
            font-size: 12px;
            table-layout: fixed;
            word-break: break-all;
            width:1450px;
        }
        table th{
            text-align: center;
        }
        table .num{
            text-align: right;
        }
        table input{
            width: 80%;
        }
        #wid-id-0 > header > h2{
            display: inline;
        }
    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2> 正廠零件查詢LOG <B>(共{{$count}}項)</B></h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <div class="row">
                            <div class="col col-12 col-lg-12">
                                @if( $request->get('errors') )
                                    <div>
                                        <ul>
                                            @foreach ( $request->get('errors') as $error )
                                                <li class="red_text">{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <table class="table table-bordered table-striped table-hover no-footer smart-form" style="width:100%">
                                    <thead>
                                    <form action="" method="GET">
                                        <tr>
                                            <td width="15px">
                                                每頁顯示<br/>
                                                <select name="limit" style="width: 60px">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>20</option>
                                                    <option value="50" {{$request->get('limit') == 50 ? 'selected' : ''}}>50</option>
                                                    <option value="100" {{$request->get('limit') == 100 ? 'selected' : ''}}>100</option>
                                                </select>
                                                項<br/>
                                                產出CSV
                                                <select name="csv_output" style="width: 60px">
                                                    <option value="" {{ ($request->get('csv_output') == '' or !$request->get('csv_output')) ? 'selected' : ''}}>否</option>
                                                    <option value="true" {{$request->get('csv_output') == 'true' ? 'selected' : ''}}>是</option>
                                                </select>
                                                <br/>
                                                進口/國產
                                                <select name="type" style="width: 60px">
                                                    <option value="" {{ ($request->get('type') == '' or !$request->get('type')) ? 'selected' : ''}}>全部</option>
                                                    <option value="1" {{$request->get('type') == '1' ? 'selected' : ''}}>進口</option>
                                                    <option value="2" {{$request->get('type') == '2' ? 'selected' : ''}}>國產</option>
                                                </select>

                                            </td>
                                            <td width="15%">
                                                <input type="text" name="created_at[0]" value="{{$request->get('created_at')  ? $request->get('created_at')[0] : ''}}" placeholder="限制最舊時間" />
                                                <input type="text" name="created_at[1]" value="{{$request->get('created_at')  ? $request->get('created_at')[1] : ''}}" placeholder="限制最新時間"/>

                                            </td>
                                            <td width="10%">
                                                <input type="text" name="realname" value="{{$request->get('realname')  ? $request->get('realname') : ''}}" placeholder="姓名">
                                            </td>
                                            <td width="25%">
                                                <input type="text" name="email" value="{{$request->get('email')  ? $request->get('email') : ''}}" placeholder="用戶電子信箱">
                                            </td>
                                            <td width="5%">
                                                <select name="role_id">
                                                    <option value="" {{ ($request->get('role_id') == '' or !$request->get('role_id')) ? 'selected' : ''}}>All</option>
                                                    <option value="3" {{$request->get('role_id') == 3 ? 'selected' : ''}}>Wholesale</option>
                                                    <option value="2" {{$request->get('role_id') == 2 ? 'selected' : ''}}>General</option>
                                                </select>
                                            </td>
                                            <td width="5%">
                                                <input type="text" name="manufacturer" value="{{$request->get('manufacturer')  ? $request->get('manufacturer') : ''}}" placeholder="品牌名稱">
                                            </td>
                                            <td width="5%">
                                                <input type="text" name="model_number" value="{{$request->get('model_number')  ? $request->get('model_number') : ''}}" placeholder="查詢料號">
                                            </td>
                                            <td width="5%">

                                            </td>
                                            <td width="5%">

                                            </td>
                                            <td width="5%">
                                                <select name="status">
                                                    <option value="" {{ (!is_numeric($request->get('status')) or !$request->has('status')) ? 'selected' : ''}}>不指定</option>
                                                    <option value="1" {{ (is_numeric($request->get('status')) and $request->get('status') == 1) ? 'selected' : ''}}>已回覆</option>
                                                    <option value="0" {{ (is_numeric($request->get('status')) and $request->get('status') == 0) ? 'selected' : ''}}>查詢中</option>
                                                </select>
                                            </td>
                                            <td width="5%">
                                                <input type="text" name="text" value="{{$request->get('text')  ? $request->get('text') : ''}}" placeholder="查詢狀態">
                                                <br/>
                                                <input type="submit" value="篩選" style="width: 60px;height: 20px;margin: 10px 5px 5px;" /><br/>
                                                <a href="{{URL::route('backend-log-genuineparts')}}" style="width: 60px;height: 20px;margin: 0px 5px;">重置</a>
                                            </td>
                                        </tr>
                                    </form>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <!--<th style="width:42px;padding:10px;">
                                            <input type="checkbox" class="big_checkbox" id="checked_all">全選
                                        </th>-->
                                        <th width="5%">No.</th>
                                        <th width="15%">查詢時間</th>
                                        <th width="10%">姓名</th>
                                        <th width="25%">email</th>
                                        <th width="5%">帳號屬性</th>
                                        <th width="10%">品牌</th>
                                        <th width="10%">料號</th>
                                        <th width="10%">廠商定價</th>
                                        <th width="10%">售價</th>
                                        <th width="10%">狀態</th>
                                        <th width="10%">回覆交期</th>
                                    </tr>
                                    @foreach ($genuineparts as $num => $item)
                                        <?php

                                        ?>
                                        <tr>
                                            <td>{{ $num+1 }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->realname }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->role }}</td>
                                            <td>{{ $item->manufacturer }}</td>
                                            <td>{{ $item->model_number }}</td>
                                            <td>{{ $item->product_price }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->status_id == 2 ? '已回覆' : '查詢中' }}</td>
                                            <td>{{ $item->text }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webike2_pagination webike2_box" style="text-align: center">
                {{ $genuineparts->appends($request->except('page'))->links() }}
            </div>
        </article>
    </div>
@stop
@section('script')

@stop