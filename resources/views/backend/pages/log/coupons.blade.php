@extends('backend.layouts.default')
@section('middle')
    <style type="text/css">
        .row{
            margin-bottom:15px;
            font-size: 17px;
        }
        .big-icon{
            width:24px;
            height:24px;
        }
        .selecter,input{
            height:30px;
        }
        .selecter option{
            height:30px;
        }
        .big_checkbox{
            width:20px;
            height:20px;
        }
        table{
            font-size: 12px;
            table-layout: fixed;
            word-break: break-all;
            width:1450px;
        }
        table th{
            text-align: center;
        }
        table .num{
            text-align: right;
        }
        table input{
            width: 80%;
        }
        #wid-id-0 > header > h2{
            display: inline;
        }
    </style>
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget" id="wid-id-0" >
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2> Coupons查詢LOG <B>(共{{$count}}項)</B></h2>
                </header>
                <div class="row">
                    <div class="widget-body">
                        <div class="row">
                            <div class="col col-12 col-lg-12">
                                @if( \Session::get('errors') )
                                    <div>
                                        <ul>
                                            @foreach ( \Session::get('errors') as $error )
                                                <li class="red_text">{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <table class="table table-bordered table-striped table-hover no-footer smart-form" style="width:100%">
                                    <thead>
                                    <form action="" method="GET">
                                        <tr>
                                            <td width="15px">
                                                每頁顯示<br/>
                                                <select name="limit" style="width: 60px">
                                                    <option value="20" {{ ($request->get('limit') == 20 or !$request->get('limit')) ? 'selected' : ''}}>20</option>
                                                    <option value="50" {{$request->get('limit') == 50 ? 'selected' : ''}}>50</option>
                                                    <option value="100" {{$request->get('limit') == 100 ? 'selected' : ''}}>100</option>
                                                </select>
                                                項<br/>
                                                產出CSV
                                                <select name="csv_output" style="width: 60px">
                                                    <option value="" {{ ($request->get('csv_output') == '' or !$request->get('limit')) ? 'selected' : ''}}>否</option>
                                                    <option value="true" {{$request->get('csv_output') == 'true' ? 'selected' : ''}}>是</option>
                                                </select>

                                            </td>
                                            <td>
                                                <input type="text" name="created_at[0]" value="{{$request->get('created_at') ? $request->get('created_at')[0] : ''}}" placeholder="限制最舊時間" />
                                                <input type="text" name="created_at[1]" value="{{$request->get('created_at') ? $request->get('created_at')[1] : ''}}" placeholder="限制最新時間"/>
                                            </td>
                                            <td>
                                                <input type="text" name="expired_at[0]" value="{{$request->get('expired_at') ? $request->get('expired_at')[0] : ''}}" placeholder="限制最舊時間" />
                                                <input type="text" name="expired_at[1]" value="{{$request->get('expired_at') ? $request->get('expired_at')[1] : ''}}" placeholder="限制最新時間"/>
                                            </td>
                                            <td>
                                                <input type="text" name="name" value="{{$request->get('name')  ? $request->get('name') : ''}}" placeholder="查詢名稱">
                                            </td>
                                            <td>
                                                <input type="text" name="discount[0]" value="{{$request->get('discount')  ? $request->get('discount')[0] : ''}}" placeholder="限制最低discount" />
                                                <input type="text" name="discount[1]" value="{{$request->get('discount')  ? $request->get('discount')[1] : ''}}" placeholder="限制最高discount"/>
                                            </td>
                                            <td>
                                                <select name="status">
                                                    <option value="" {{ ($request->get('status') == '' or !$request->get('status')) ? 'selected' : ''}}>All</option>
                                                    <option value="1" {{$request->get('status') == 1 ? 'selected' : ''}}>O</option>
                                                    <option value="2" {{$request->get('status') == 2 ? 'selected' : ''}}>X</option>
                                                </select>
                                            </td>
                                            <td></td>
                                            <td>
                                                <input type="text" name="realname" value="{{$request->get('realname')  ? $request->get('realname') : ''}}" placeholder="姓名">
                                            </td>
                                            <td>
                                                <select name="role_id">
                                                    <option value="" {{ ($request->get('role_id') == '' or !$request->get('role_id')) ? 'selected' : ''}}>All</option>
                                                    <option value="3" {{$request->get('role_id') == 3 ? 'selected' : ''}}>Wholesale</option>
                                                    <option value="2" {{$request->get('role_id') == 2 ? 'selected' : ''}}>General</option>
                                                    <option value="1" {{$request->get('role_id') == 1 ? 'selected' : ''}}>Registered</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="email" value="{{$request->get('email')  ? $request->get('email') : ''}}" placeholder="用戶電子信箱">
                                                <br/>
                                                <input type="submit" value="篩選" style="width: 60px;height: 20px;margin: 10px 5px 5px;" /><br/>
                                                <a href="{{URL::route('backend-log-coupons')}}" style="width: 60px;height: 20px;margin: 0px 5px;">重置</a>
                                            </td>
                                        </tr>
                                    </form>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <!--<th style="width:42px;padding:10px;">
                                            <input type="checkbox" class="big_checkbox" id="checked_all">全選
                                        </th>-->
                                        <th width="5%">No.</th>
                                        <th width="10%">發放日</th>
                                        <th width="10%">過期日</th>
                                        <th width="15%">名稱</th>
                                        <th width="10%">金額</th>
                                        <th width="10%">已使用</th>
                                        <th width="10%">代碼</th>
                                        <th width="10%">姓名</th>
                                        <th width="10%">帳號屬性</th>
                                        <th width="20%">email</th>
                                    </tr>
                                    @foreach ($collection as $num => $item)
                                        <tr>
                                            <td>{{ $num+1 }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->expired_at }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->discount }}</td>
                                            @if($item->increment_id)
                                                <td><span style="color:green"><B>O</B></span><br/><a href="{{EG_ZERO.'/order/edit/'.$item->order_id}}" target="_blank">訂單</a>({{$item->increment_id}})</td>
                                            @else
                                                <td><span style="color:red"><B>X</B></span></td>
                                            @endif
                                            <td>{{ $item->transcode }}</td>
                                            <td>{{ $item->realname }}</td>
                                            <td>{{ $item->role }}</td>
                                            <td>{{ $item->email }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="webike2_pagination webike2_box" style="text-align: center">
                {{ $collection->appends($request->except('page'))->links() }}
            </div>
        </article>
    </div>
@stop