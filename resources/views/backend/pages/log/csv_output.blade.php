<?php 
\Debugbar::disable();
date_default_timezone_set('Asia/Taipei');
$filename = date("Ymd").$type;
header("Content-type: text/x-csv;charset=utf-8");
header("Content-Disposition: attachment; filename={$filename}.csv");

echo $main_str;