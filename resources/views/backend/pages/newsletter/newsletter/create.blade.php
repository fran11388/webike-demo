@extends('backend.layouts.default')
@section('style')
    <style>

    </style>
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/froala_editor.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/froala_style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/fullscreen.min.css') }}">

    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/char_counter.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/code_view.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/colors.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/fullscreen.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/image.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/image_manager.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/line_breaker.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/table.min.css') }}">
    <link rel="stylesheet" href="{{ assetRemote('js/backend/froala_editor_2.6.1/css/plugins/video.min.css') }}">
@stop
@section('middle')
    <div class="newsletter-container ">
        <div class="clearfix">
            <form id="create-form" method="post" action="{{ $newsletter ? \URL::route('backend-newsletter-insert',['id' => $newsletter->id]) : \URL::route('backend-newsletter-insert') }}">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-comments"></i>&nbsp新增電子報</h3>
                    </div>
                    <div class="panel-body">
                        <div class="basic-set">
                            <div class="name-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">名稱*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="名稱" type="text" name="name" value="{{ $newsletter ? $newsletter->name : '' }}">
                                </div>
                            </div>
                            <div class="title-container clearfix block">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">標題(信件標題)*</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <input class="form-control" placeholder="標題" type="text" name="title" value="{{ $newsletter ? $newsletter->title : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="content-set block">
                            <textarea class="trans_content" name="content"  style="width: 100%;">{{ $newsletter ? $newsletter->content : ''}}</textarea>
                        </div>
                        <div class="save-container text-right">
                            <input type="submit" class="btn btn-danger" name="create" value="儲存">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('script')
    <script src="{{ assetRemote('js/backend/froala_editor_2.6.1/js/froala_editor.min.js')}}"></script>
    <script src="{{ assetRemote('js/backend/froala_editor_2.6.1/js/languages/zh_tw.js')}}"></script>
    <script src="{{ assetRemote('js/backend/froala_editor_2.6.1/js/plugins/fullscreen.min.js')}}"></script>

    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/align.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/char_counter.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/code_view.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/colors.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/entities.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/font_family.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/font_size.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/fullscreen.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/image.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/image_manager.min.js')}}"></script>
    {{-- 	<script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/line_breaker.min.js')}}"></script> --}}
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/link.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/lists.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/paragraph_format.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/paragraph_style.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/quote.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/save.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/table.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/url.min.js')}}"></script>
    <script src="{{assetRemote('js/backend/froala_editor_2.6.1/js/plugins/video.min.js')}}"></script>
    <script>

        $("#create-form").submit(function(e){
            $('.basic-set input').each(function(){
                if(!$(this).val()){
                    alert('欄位尚未輸入');
                    e.preventDefault();
                }
            })
        });

        $(document).ready(function(){
            froala();
        });

        function froala() {
            $('.trans_content').froalaEditor({
                language: 'zh_tw',
                height: 737,
                toolbarButtons: [
                    'fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'color', '|', 'insertTable', 'insertLink', 'insertImage', 'insertVideo',
                    'align', 'formatOL', 'formatUL', 'outdent', 'indent', '|', 'paragraphStyle', 'quote', 'insertHR', 'undo', 'redo', 'clearFormatting', '|', 'fullscreen', 'html'],
                // toolbarButtons: ['colors' , 'insertTable', '|', 'paragraphFormat', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'indent', 'outdent'],
            }).on('froalaEditor.contentChanged', function () {
                console.log('content changed');
                $('a:contains("Unlicensed Froala Editor")').hide();
            });
            $('a:contains("Unlicensed Froala Editor")').hide();
        };
    </script>
@stop