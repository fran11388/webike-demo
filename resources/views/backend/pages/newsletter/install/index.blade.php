@extends('backend.layouts.default')
@section('style')
    <style>
    .clerafix{
    	overflow: auto;
    	zoom:1;
    }
    .upload{
        float: left;
        width:50%;


    }
    /*.container input{
    	float: left;
    }*/
	/*input:nth-child(2){
		margin-top:20px; 
	}*/

    .file-upload {
        border: 1px solid #ccc;
        display: inline-block;
        padding: 6px 12px;
        cursor: pointer;
        float: left;
    }
    .download{
        float: left;
    }

    </style>
@stop
@section('middle')
<div class="container clerafix">
	<div class="clerafix upload">
		<form action="{!! route('backend-newsletter-install') !!}" method="post" enctype="multipart/form-data">
		    <input  class="file-upload" type="file" name="file" id="fileToUpload">
		    <input class="btn btn-danger " type="submit" value="預覽" name="submit">
		</form>
	</div>
	<div class="download">
		<a href="\vendor\doc\NewSletter.xlsx"><button class="btn btn-warning">下載範本</button></a>
	</div>
</div>	
@stop
@section('script')
@stop