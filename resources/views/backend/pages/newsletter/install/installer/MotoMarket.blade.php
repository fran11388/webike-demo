
<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:1em;" width="100%">
	<tbody>
		<tr>
			<td style=" text-align:center; word-break: break-all; width:100%; overflow: auto;
			zoom: 1;">
@foreach($urls as $key => $url)
				<div style="float:left; width: 25%;">
					<p style="text-align:center; font-size: 0.85rem;">
						<a href="{!! MOTOMARKET . '/detail/' . $url !!}" target="_blank" style="text-decoration:none;">【{!! $names[$key] !!}】</a>
					</p>
					<p style="text-align:center; font-size: 0.85rem;">
						<a href="{!! MOTOMARKET . '/detail/' . $url !!}" target="_blank" style="text-decoration:none;">
							<img src="{!! $imgs[$key] !!}" style="height:173px; width:230px" />
						</a>
					</p>
					<p style="text-align:center; font-size: 0.85rem;">
						<a href="{!! MOTOMARKET . '/detail/' . $url !!}" target="_blank" style="text-decoration:none; ">{!! $manufacturers[$key] !!} {!! $motonames[$key] !!}</a>
					</p>
				</div>
@endforeach
			</td>
		</tr>
	</tbody>
</table>