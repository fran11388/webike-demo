
<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:1em;" >
	<tr>
		<td style=" text-align:center;word-break: break-all;width:100%; zoom: 1;">
	@foreach($skus as $key => $sku)
			<div style="float:left; width: 46%; padding: 0% 2%;">
					<h2 style="font-family: Helvetica, Arial, Verdana, Simhei, Microsoft JhengHei, 微軟正黑體, PMingLiU, sans-serif;  font-size: 1.2rem;">
						<a href="{!!  route('product-detail',$sku) !!}"  target="_blank" style="text-decoration:none;">【{!! $product_manufacturers[$key] !!}】<br/>
									{!! $product_names[$key] !!}
						</a>
					</h2>
					<h3 style="font-size: 1rem;">
						<a href="{!! route('review-show',$ids[$key]) !!}"  target="_blank" style="text-decoration:none;">{!! $titles[$key] !!}
						</a><br/>投稿者：{!! $names[$key] !!}
					</h3>
					<ul style="list-style-type:none;padding: 0px;">
						<li><a href="{!! route('review-show',$ids[$key]) !!}"  target="_blank"><img alt="" src="{!! isset($product_imgs[$key]) ? 'http://img.webike.tw/review/' . $product_imgs[$key] : NO_IMAGE !!}" style="height:250px; width:240px;" /></a></li>
						
					</ul>
					<p style="width:100%; padding:10px 5px 10px 0px; font-size: 0.85rem;">{!! $contents[$key] !!}</p>
			</div>
	@endforeach
		</td>
	</tr>
</table>


