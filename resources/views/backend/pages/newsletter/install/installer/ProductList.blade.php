<table  align="center" border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td style=" text-align:center;word-break: break-all;width:100%; overflow: auto;
				zoom: 1;">
@foreach($productSkus as $key => $productSku)
			<div style="float:left; width: 25%; height:274px;">
				<div style="height:200px; width:100%;">
					<a href="{!! route('product-detail', $productSku) !!}" target="_blank" style="text-decoration:none;display: block;font-size: 0;height: 100%">
						<img alt="" src="{!! $imgs[$key] !!}" style=" max-height:100%; max-width: 100%;display: inline-block;vertical-align: middle;">
						<span style="display: inline-block; height: 100%; vertical-align: middle;" ></span>
					</a>
				</div>
				<p style="padding:5px; margin:5px 2.5% 5px 2.5%;font-size: 0.85rem;max-height:44px;text-overflow: ellipsis;overflow:hidden;white-space:nowrap;">
					<a href="{!! route('product-detail', $productSku) !!}" target="_blank" style="text-decoration:none;">
						【{!! $manufacturerNames[$key] !!}】
						<br/>
						{!! $names[$key] !!}
						<br/>
					</a>
				</p>
			</div>
@endforeach
		<td>
	</tr>
</table>
