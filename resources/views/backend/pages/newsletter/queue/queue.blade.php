@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            max-height:190px;
            min-width:120px;
            min-height:120px;
        }
        .active-select {
            float:right;
            margin-right:15px;
        }
        .new-button {
            float:right;
            margin-right:20px;
        }
    </style>
@stop
@section('middle')
    <div class="active-select block">
        <input type="button"  class="btn btn-danger delete-button" value="刪除">
    </div>
    <form method="post" action="{{\URL::route('backend-newsletter-queue')}}" class="row form clearfix">
        <input name="type" type="hidden" value="queue" >
        <div class="new-button">
            <a href="{{ \URL::route('backend-newsletter-queue-insert') }}" class="btn btn-warning">新增眝列</a>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-comments"></i>&nbsp電子報</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th><input id="checkAll" type="checkbox"></th>
                                <th>ID</th>
                                <th>標題</th>
                                <th>排定發送時間</th>
                                <th>目前發送狀態</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newsletter_queues as $newsletter_queue)
                                <tr>
                                    <td><input class="btn btn-default" name="check[]" type="checkbox" value="{{$newsletter_queue->id}}"></td>
                                    <td>{{ $newsletter_queue->id }}</td>
                                    <td><a href="{{ \URL::route('backend-newsletter-queue-insert',['id' => $newsletter_queue->id]) }}">{{ $newsletter_queue->title }}</a></td>
                                    <td><a href="{{ \URL::route('backend-newsletter-queue-insert',['id' => $newsletter_queue->id]) }}">{{ $newsletter_queue->send_start_time }}</a></td>
                                    <td>{{ $status[$newsletter_queue->status_id] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="text-center">
        {!! $newsletter_queues->render() !!}
    </div>
@stop
@section('script')
    <script>
        $("#checkAll").change(function () {
            $("input[name='check[]']").prop('checked', $(this).prop("checked"));
        });

        $('.delete-button').click(function(){
            var check = false;
            var choose = this.value;
            if(choose){
                var title = "你確定要" + choose + "嗎";

                $("input[name='check[]']").each(function (event) {
                    if ($(this).prop("checked")) {
                        check = true;
                    }
                });

                $('.form').attr('action',"{{ \URL::route('backend-newsletter-delete') }}");
                if (check){
                    var form = $('.form');
                    swal({
                        title: title,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "確定",
                        cancelButtonText: '取消',
                        closeOnConfirm: false
                    }).then(function () {
                        form.submit();
                    });
                }else{
                    alert('未選擇刪除項目');
                }
            }
        });
    </script>
@stop