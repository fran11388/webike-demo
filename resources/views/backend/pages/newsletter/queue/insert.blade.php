@extends('backend.layouts.default')
@section('style')
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
    <style>
        table {
            margin:auto !important;
        }
    </style>
@stop
@section('middle')
    @if($newsletter_queue == '' or $newsletter_queue->status_id < 2)
        <div class="newsletter-container ">
            <div class="clearfix">
                <form id="create-form" method="post" action="{{ $newsletter_queue ? \URL::route('backend-newsletter-queue-insert',['id' => $newsletter_queue->id]) : \URL::route('backend-newsletter-queue-insert') }}">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-comments"></i>&nbsp新增電子報</h3>
                        </div>
                        <div class="panel-body">
                            <div class="basic-set">
                                <div class="select-newsletter block clearfix">
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                        <span class="size-10rem">選擇電子報</span>
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <select class="select2" name="select_newsletter">
                                            @foreach($newsletters as $key => $newsletter)
                                                <option value="{{$newsletter->id}}" {{ $newsletter_queue ? $newsletter_queue->newsletter_id == $newsletter->id ? 'selected' : '' : '' }}>{{ $newsletter->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="begin-container clearfix block">
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                        <span class="size-10rem">開始發送日期*</span>
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        <input id='datetimepicker4' class="form-control" type='text' placeholder="From" name="begin_date" value="{{ $newsletter_queue ? $newsletter_queue->send_start_time : '' }}">
                                    </div>
                                </div>
                                @if($newsletter_queue)
                                    <div class="status-container clearfix block">
                                        <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                            <span class="size-10rem">狀態</span>
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                            <select class="form-control" name="select_status">
                                                @foreach($status as $key => $option)
                                                    <option value="{{ $key }}" {{ $newsletter_queue ? $newsletter_queue->status_id == $key ? 'selected' : '' : '' }}>{{ $option }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="method-container clearfix block">
                                    <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                        <span class="size-10rem">寄送方式</span>
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                        @if($newsletter_queue)
                                            <select class="form-control" name="select_method">
                                                <option value="0" {!! $newsletter_queue->send_method == 0 ? 'selected' : ''  !!}>自訂收件者</option>
                                                <option value="1" {!! $newsletter_queue->send_method == 1 ? 'selected' : ''  !!}>寄給電子報訂閱者</option>
                                            </select>
                                        @else
                                            <select class="form-control" name="select_method">
                                                <option value="0">自訂收件者</option>
                                                <option value="1" selected>寄給電子報訂閱者</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="method-container clearfix block {!! ($newsletter_queue and $newsletter_queue->send_method == 0) ? '' : 'hide' !!}">
                                <div class="col-md-3 col-sm-3 col-xs-3 text-right">
                                    <span class="size-10rem">指定收件人</span>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                    <select class="width-full" name="customer_ids[]" multiple>
                                        @if($newsletter_queue)
                                            @foreach($newsletter_queue->items as $queue_item)
                                                <option value="{!! $queue_item->customer_id !!}" selected>{!! $queue_item->customer->realname . '(' . $queue_item->customer->email . ')' !!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="save-container text-right">
                                <input type="submit" class="btn btn-danger" name="create" value="儲存">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
    @if($newsletter_queue)
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-comments"></i>&nbsp預覽</h3>
            </div>
            <div class="panel-body">
                <div class="content-set block" style="margin:0px auto;">
                    {!! $newsletter_queue->content !!}
                </div>
            </div>
        </div>
    @endif
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script>

        $(document).ready(function(){
            $('#datetimepicker4').datetimepicker({
                format:      'YYYY-MM-DD HH:mm:ss'
            });

            $('select[name^="customer_ids"]').select2({
                width : "100%", //若你的版面是 RWD 可以這樣，讓他重新調整寬度
                placeholder : "輸入關鍵字搜尋會員",

                //使用如$.ajax 參閱 ajax
                ajax :
                {
                    url: "{!! route('backend-api-customerNames') !!}",
                    //select2 預設使用json, 若要跨網域使用 jsonp，我個人偏愛html後再自己解碼
                    dataType: 'json',
                    delay: 500,
                    //我使用POST傳送
                    type: "POST",
                    //夾帶參數
                    data: function (param)
                    {
                        return { search_text: param.term };
                    },

                    //成功返回的項目
                    processResults: function (result)
                    {
                        //因為 dataType: 'html' 所以要自己解碼喔。但用html可以方便debug。
                        if(result.success){
                            return {results: result.datas};
                        }
                        var empty = [{id:0, text: null}];
                        return {results: empty};
                    }
                }
            });

            $('[name="select_method"]').change(function(){
                var parent = $('select[name^="customer_ids"]').parents('.method-container');
                if(parent.hasClass('hide')){
                    parent.removeClass('hide');
                }else{
                    parent.addClass('hide');
                }
            });
        });

        $("#create-form").submit(function(e){
            $('.basic-set input').each(function(){
                if(!$(this).val() && !$.isNumeric($(this).val())){
                    alert('欄位尚未輸入');
                    e.preventDefault();
                }
            })
        });
    </script>
@stop