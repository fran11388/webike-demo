<div style="font-size:1em;">
<div style="font-size:1em;">
<div style="font-size:1em;">
<div style="font-size:1em;">
<div style="font-size:1em;">
<div style="font-size:1em;">
<div style="font-size:1em;">
<h4>&nbsp;</h4>

<table align="center" border="1" cellpadding="1" cellspacing="1" style="width:810px">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:1em">
				<tbody>
					<tr>
						<td colspan="4">
						<table align="center" border="1" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
							<tbody>
								<tr>
									<td style="text-align:right"><span style="color:#FFFFFF">Vol.227 出版日期:2017/11/03</span></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<h1 style="text-align:center"><a href="https://www.webike.tw/shopping" style="font-size: 1em; text-align: center;"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/BANNER.jpg" style="height:315px; opacity:0.9; width:975px" /></a></h1>

						<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:13.3333px; text-align:center">
							<tbody>
								<tr>
									<td colspan="4">
									<table align="center" border="1" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
										<tbody>
											<tr>
												<td>
												<p><span style="font-size:1.5rem"><strong><span style="color:rgb(255, 255, 255)">週週開信拿點數，快來領取您的現金點數&nbsp;</span></strong></span></p>
												</td>
												<td><a href="{{$give_point_url}}" style="line-height: 21px;" target="_blank"><img src="http://img.webike.tw/assets/images/newsletter/201609/160929_2.png" style="height:50px; opacity:0.9; width:340px" /></a></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="4">
									<p style="text-align:center"><a href="https://www.webike.tw/benefit/sale/2017-11" style="font-size: 13.3333px; text-align: center;"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/November370.jpg" style="height:301px; width:975px" /></a></p>

									<p style="text-align:center"><a href="https://www.webike.tw/sd/t00079148" style="font-size: 13.3333px;"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/RF2370.jpg" style="height:301px; width:975px" /></a></p>

									<p style="text-align:center"><a href="https://www.webike.tw/parts/br/1358?q=買就送" style="font-size: 13.3333px; text-align: center;"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/MOTO.jpg" style="height:301px; opacity:0.9; width:975px" /></a></p>

									<p style="text-align:center"><a href="https://www.webike.tw/sd/t00082165"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/SUZUKI370.jpg" style="height:301px; width:975px" /></a></p>

									<p style="text-align:center"><a href="https://www.webike.tw/collection/category/2018AW" style="font-size: 13.3333px; text-align: center;"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/Autumn370.png" style="height:301px; opacity:0.9; width:975px" /></a></p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center">
						<table align="center" border="0" cellpadding="1" cellspacing="1">
							<tbody>
								<tr>
									<td colspan="4">
									<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
										<tbody>
											<tr>
												<td style="text-align:left">
												<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; opacity:0.9; width:15px" /><span style="font-size:1.2rem"><strong><span style="color:rgb(255, 255, 255)">精選焦點話題</span></strong></span></p>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="2" rowspan="1">
									<h4><a href="https://www.webike.tw/parts/br/t2419"><span style="font-size:1.5rem">【充滿時尚英倫風潮的設計感】</span></a><br />
									英國的頂級防護專家─英爵士 現正3折起</h4>
									</td>
									<td colspan="2" rowspan="1">
									<h4><span style="font-size:1.5rem"><a href="https://www.webike.tw/parts/ca/3000-3001-3002/br/527?sort=new">【橫空劃過，無聲無息】</a></span><br />
									OGK KABUTO 空氣刀V 輕又高雅，讓你感到完整的包覆及安心感</h4>
									</td>
								</tr>
								<tr>
									<td colspan="2"><a href="https://www.webike.tw/parts/br/t2419"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/EKSELSIOR628.jpg" style="height:249px; width:475px" /></a></td>
									<td colspan="2"><a href="https://www.webike.tw/parts/ca/3000-3001-3002/br/527?sort=new"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/OGK628.jpg" style="height:249px; width:475px" /></a></td>
								</tr>
								<tr>
									<td colspan="2">
									<div style="color: rgb(29, 33, 41);">
									<h4><span style="font-size:1.5rem"><a href="https://www.webike.tw/parts/ca/1000-1059-1117/br/597" style="font-size: 1em;">【預防荷包大失血】</a></span><br />
									R&amp;G 左傾、右倒都撐得住</h4>
									</div>
									</td>
									<td colspan="2">
									<h4><span style="font-size:1.5rem"><a href="https://www.webike.tw/parts/ca/3000-3260-1331-3135/br/260">【你，還有懷抱一顆冒險的心】</a></span><br />
									用 GIVI 全系列後箱 證明自己</h4>
									</td>
								</tr>
								<tr>
									<td colspan="2"><a href="https://www.webike.tw/parts/ca/1000-1059-1117/br/597"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/RG628.jpg" style="height:249px; opacity:0.9; width:475px" /></a></td>
									<td colspan="2"><a href="https://www.webike.tw/parts/ca/3000-3260-1331-3135/br/260"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/GIVI628.jpg" style="height:249px; width:475px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center">
						<table align="center" border="0" cellpadding="1" cellspacing="1">
							<tbody>
								<tr>
									<td colspan="4">
									<p>&nbsp;</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center">
						<p>&nbsp;</p>

						<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:1em">
							<tbody>
								<tr>
									<td colspan="4" style="text-align:center">
									<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:1em">
										<tbody>
											<tr>
												<td colspan="4">
												<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); line-height:20.8px; width:1200px">
													<tbody>
														<tr>
															<td>
															<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; width:15px" /><strong><span style="color:rgb(255, 255, 255)"><span style="font-size:1.2rem">2017 MotoGP &nbsp;冠軍預測活動</span></span></strong></p>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<tr>
												<td colspan="4"><a href="http://www.webike.tw/benefit/event/motogp/2017" target="_blank"><img src="http://img.webike.tw/shopping/image/benefit/event/motogp/banner1.png" style="height:266px; opacity:0.9; width:966px" /></a></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="2" rowspan="1" style="text-align:center">&nbsp;</td>
								</tr>
							</tbody>
						</table>

						<p><span style="font-size:1.2rem">更多最新消息，請關注Webike台灣粉絲專頁！</span><br />
						<br />
						&nbsp;<a href="https://www.facebook.com/WebikeTaiwan/"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/16pic_3986734_b.png" style="height:64px; width:500px" /></a></p>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); line-height:20.8px; width:1200px">
							<tbody>
								<tr>
									<td>
									<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; width:15px" /><strong><span style="font-size:1.2rem"><span style="color:#FFFFFF">2017推薦新品</span></span></strong></p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082233"><img alt="" src="https://img.webike.tw/product/t8/t00082233.jpg" style="height:264px; width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082585"><img alt="" src="https://img.webike.tw/product/2906/t00082585.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23562548"><img alt="" src="https://img.webike.net/catalogue/images/43127/calendar.jpg" style="height:230px; width:163px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23562549" target="_blank"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/10006-054-04_01.jpg" style="height:284px; width:230px" /></a></td>
					</tr>
					<tr>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082233">【SHARK】OPENLINE 可掀式安全帽(亮黃)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082585">【英爵士】倫敦特調防摔衣 (拿鐵棕)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23562548">【三榮書房】賽車日曆2018 Jet</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23562549">【100％】【手套】BRISKER COLDWEATHER</a></p>
						</td>
					</tr>
					<tr>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23563089"><img alt="logo brand" src="https://img.webike.net/catalogue/images/43146/96529.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23453834"><img alt="logo brand" src="https://img.webike.net/catalogue/images/41744/HS4306Gso1CB1100RS.jpg" style="height:173px; width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082653"><img alt="logo brand" src="https://img.webike.tw/product/t751/t00082653.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23454211"><img alt="logo brand" src="https://img.webike.net/catalogue/images/41927/16-374-11_1.jpg" style="width:230px" /></a></td>
					</tr>
					<tr>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23563089">【DAYTONA】握式棘輪工具組</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23453834">【HURRICANE】CB1100RS 分離式把手</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082653">【Go-works(goworks)】</a><br />
						<a href="https://www.webike.tw/sd/t00082653" style="font-size: 1em;">光陽 AK550專用後牌架</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23454211">【OVER】TT-Formula RS 2-1<br />
						鈦合金全段排氣管</a></p>
						</td>
					</tr>
					<tr>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23453766"><img alt="logo brand" src="https://img.webike.net/catalogue/images/41778/002-S020GD_01.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23453742"><img alt="logo brand" src="https://img.webike.net/catalogue/images/41789/q5kysk109t02_01.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23453742"><img alt="logo brand" src="https://img.webike.net/catalogue/images/15795/7595f.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/23379710"><img alt="logo brand" src="https://img.webike.net/catalogue/images/42024/23379710_1.jpg" style="width:230px" /></a></td>
					</tr>
					<tr>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23453766">【BABYFACE】腳踏後移套件 GSX-R1000</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23453742">【YAMAHA(日本山葉)】油箱保護貼</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23432361">【Puig】Touring 風鏡</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/23379710">【Webike MODE】<br />
						LED Sequential 方向燈組<br />
						(流動式方向燈)</a></p>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
							<tbody>
								<tr>
									<td>
									<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; opacity:0.9; width:15px" /><strong><span style="color:#FFFFFF"><span style="font-size:1.2rem">OUTLET商品推薦</span></span></strong></p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center"><a href="https://www.webike.tw/outlet"><img alt="" src="https://img.webike.tw/shopping/image/banner/11_outlet.jpg" style="height:301px; width:975px" /></a></td>
					</tr>
					<tr>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082162"><img alt="" src="https://img.webike.net/catalogue/images/29088/0SYES-W3M-W_P018.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082161"><img alt="logo brand" src="https://img.webike.net/catalogue/10317/1408773589_97eb7ff3791ae927529fe916f874141a.jpg" style="width:230px" /></a></td>
						<td style="text-align:center">
						<h1><a href="https://www.webike.tw/sd/t00082160" style="font-size: 15px;"><img alt="logo brand" src="https://img.webike.net/catalogue/images/10476/wl962w-blk.jpg" style="width:230px" /></a></h1>
						</td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082154"><img alt="logo brand" src="https://img.webike.tw/product/t2573/t00062063.jpg" style="width:230px" /></a></td>
					</tr>
					<tr>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082162">【HONDA RIDING GEAR】<br />
						A／W冬季騎士外套<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082161">【YOSHIMURA(吉村)】<br />
						CYCLONE CB400SF排氣管尾段<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082160">【DEGNER】冬季手套<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082154">【Biltwell】BONANZA<br />
						3/4 復古 安全帽-銀粉亮銀<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p>&nbsp;</p>
						</td>
						<td style="text-align:center">
						<p>&nbsp;</p>

						<p>&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00082155"><img alt="logo brand" src="https://img.webike.net/catalogue/11990/05-067.jpg" style="width:230px" /></a></td>
						<td style="text-align:center">
						<h1><a href="https://www.webike.tw/sd/t00081822" style="font-size: 15px;"><img alt="logo brand" src="https://img.webike.net/catalogue/images/33785/5_kagayama5.jpg" style="width:230px" /></a></h1>
						</td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00081816"><img alt="logo brand" src="https://img.webike.net/catalogue/images/28377/EBB201T.jpg" style="width:230px" /></a></td>
						<td style="text-align:center"><a href="https://www.webike.tw/sd/t00078399"><img alt="" src="https://img.webike.net/catalogue/images/23200/0SYTNW56_42FS_K.jpg" style="width:230px" /></a></td>
					</tr>
					<tr>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00082155">【KOMINE】BK-067 防護運動型短版騎士鞋<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00081822">【SHOEI】X-14 KAGAYAMA5<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00081816">【EFFEX】把手配重<br />
						(OUTLET出清商品)</a></p>
						</td>
						<td style="text-align:center">
						<p><a href="https://www.webike.tw/sd/t00078399">【HONDA RIDING GEAR】WingT恤<br />
						(OUTLET出清商品)</a></p>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
							<tbody>
								<tr>
									<td>
									<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; opacity:0.9; width:15px" /><strong><span style="color:#FFFFFF"><span style="font-size:1.2rem">BikeNews 海內外新聞</span></span></strong></p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<h2 style="text-align:center"><a href="https://www.webike.tw/bikenews/2017/10/31/%E9%87%8D%E6%96%B0%E5%AE%9A%E7%BE%A9%E5%A5%A2%E8%8F%AF%E6%97%97%E8%89%A6-%E5%A4%A7%E6%94%B9%E6%AC%BE%E3%80%8Cgoldwing-tour%E3%80%8D%E5%AE%8C%E6%95%B4%E8%A7%A3%E6%9E%90/">&rdquo;奢華新境界&rdquo; 大改款「GOLDWING 」完整解析</a></h2>
						</td>
						<td colspan="2" style="text-align:center">
						<h2><a href="https://www.webike.tw/bikenews/2017/11/02/%E3%80%90%E5%92%8C%E6%AD%8C%E5%B1%B1%E5%B0%88%E6%AC%84%E3%80%91%E5%BE%B7%E5%BC%8F%E5%86%92%E9%9A%AA%E5%B0%8F%E5%B0%87-bmw%E3%80%8Cg310gs%E3%80%8D/">【和歌山專欄】德式冒險小將 BMW「G310GS」</a></h2>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:center"><a href="https://www.webike.tw/bikenews/2017/10/31/重新定義奢華旗艦-大改款「goldwing-tour」完整解析/"><img alt="" src="https://www.webike.tw/bikenews/wp-content/uploads/2017/10/COVER-5.jpg" style="height:300px; width:550px" /></a></td>
						<td colspan="2" style="text-align:center"><a href="https://www.webike.tw/bikenews/2017/11/02/【和歌山專欄】德式冒險小將-bmw「g310gs」/"><img alt="" src="https://www.webike.tw/bikenews/wp-content/uploads/2017/11/20171025_wakayama_01.jpg" style="height:300px; width:550px" /></a></td>
					</tr>
					<tr>
						<td colspan="2">
						<p style="text-align:center">搭載1800cc水平對臥6汽缸引擎的摩托車界王者GOLDWING在推出43年後，<br />
						<span style="font-size:1em">進行了第6次的全面改款，這次的改款不止車身外型，<br />
						全新採用的改良版Hossack前叉和7速DCT變速箱<br />
						更是將GOLDWING進化到新次元領域。</span></p>
						</td>
						<td colspan="2">
						<p style="text-align:center">今年可說是輕型冒險車款蓬勃發展的一年，<br />
						KAWASAKI的VERSYS 300/250、SUZUKI的V Strom 250等輕型冒險車款陸續登場，<br />
						並且贏得高人氣的注目度。筆者總認為所謂的冒險車款，<br />
						就是把摩托車所具備的通用性達到高水準化的車款。能夠極致地進行彎道騎乘和越野騎乘的車款，<br />
						就可以說是極富實用性的冒險多功能車款。</p>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
							<tbody>
								<tr>
									<td>
									<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; opacity:0.9; width:15px" /><span style="font-size:1.2rem"><strong><span style="color:#FFFFFF">Review 會員評論</span></strong></span></p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:center">
						<h2><a href="https://www.webike.tw/sd/23121525">【HONDA RIDING GEAR】<br />
						STRIKER網格夾克</a></h2>

						<h3><a href="https://www.webike.tw/review/article/4562">HONDA防摔衣 最愛HRC配色~</a><br />
						投稿者：bebop7442</h3>

						<p>&nbsp;</p>

						<ul style="list-style-type:none">
							<li><a href="https://www.webike.tw/review/article/4701"><img alt="" src="https://img.webike.tw/review/1509389892227.JPG" style="height:225px; width:198px" /></a></li>
						</ul>

						<p>炎熱的夏天絕對是騎士最大的敵人,<br />
						不過騎乘的安全是不能打則扣的這次買的是STRIKER網格夾克,<br />
						收到後感到相當驚艷,帥氣程度不在話下<br />
						相當吸睛整體非常的柔軟活動起來相當自在 通風性能極為優良,<br />
						這下可以輕鬆應付炎熱夏天帶來的悶熱不適了</p>
						</td>
						<td colspan="2">
						<h2 style="text-align:center"><a href="https://www.webike.tw/sd/t00050064">【KITACO】<br />
						把手固定安全帽鎖</a></h2>

						<h3 style="text-align:center"><a href="https://www.webike.tw/review/article/4568">KITACO 安全帽鎖</a><br />
						投稿者：阿勇</h3>

						<ul style="list-style-type:none">
							<li style="text-align:center">&nbsp;</li>
							<li style="text-align:center"><a href="https://www.webike.tw/review/article/4694"><img alt="" src="https://img.webike.tw/review/1509356731165.jpg" style="height:225px; width:300px" /></a></li>
							<li style="text-align:center">&nbsp;</li>
						</ul>

						<p style="text-align:center">日本大廠KITACO安全帽鎖,金色配色讓人驚艷,<br />
						<span style="font-size:1em">外出時可以把安全帽放心地掛在這,重點價格又不高,<br />
						非常實用的改裝品.</span></p>

						<p style="text-align:center">&nbsp;</p>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
							<tbody>
								<tr>
									<td>
									<p><img src="http://img.webike.tw/shopping/image/V-tag.png" style="height:15px; opacity:0.9; width:15px" /><strong><span style="font-size:1.2rem"><span style="color:#FFFFFF">摩托車市-更多優質好車</span></span></strong></p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td style="text-align:center"><strong><a href="http://www.webike.tw/motomarket/detail/20171101C001576">【個人自售】</a></strong></td>
						<td style="text-align:center"><strong><a href="http://www.webike.tw/motomarket/detail/20170927C001508">【個人自售】</a></strong></td>
						<td style="text-align:center"><strong><a href="http://www.webike.tw/motomarket/detail/20170925D011080">【立昇車業】</a></strong></td>
						<td style="text-align:center"><strong><a href="http://www.webike.tw/motomarket/detail/20170922D011062">【兆豐車業】</a></strong></td>
					</tr>
					<tr>
						<td>
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:195px">
							<tbody>
								<tr>
									<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20171101C001576" target="_blank"><img src="http://img.webike.tw/moto_photo/b936ddd9-3b32-4f84-b0f9-fb6c9a550fe8.jpg" style="height:173px; width:230px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
						<td>
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:195px">
							<tbody>
								<tr>
									<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20170927C001508" target="_blank"><img src="http://img.webike.tw/moto_photo/784619a3-0dda-4254-9219-7e285e60fea6.jpg" style="height:173px; width:230px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
						<td>
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:195px">
							<tbody>
								<tr>
									<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20170925D011080" target="_blank"><img src="http://img.webike.tw/moto_photo/92327c89-879b-48bd-a065-231b8c469c68.jpg" style="height:173px; width:230px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
						<td>
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="width:195px">
							<tbody>
								<tr>
									<td><a href="http://www.webike.tw/motomarket/detail/20170922D011062" target="_blank"><img src="http://img.webike.tw/moto_photo/18d1fb65-733d-4edc-9f9f-11e6eda6f8cd.jpg" style="height:173px; width:230px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20171101C001576">HONDA CBR600RR</a></td>
						<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20170927C001508">KAWASAKI NINJA ZX-10R</a></td>
						<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20170925D011080">YAMAHA YZF-R15</a></td>
						<td style="text-align:center"><a href="http://www.webike.tw/motomarket/detail/20170922D011062">BMW S1000RR</a></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="font-size:1em">
							<tbody>
								<tr>
									<td colspan="4">
									<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); line-height:20.8px; width:1200px">
										<tbody>
											<tr>
												<td>
												<p>&nbsp;</p>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:center">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" rowspan="1" style="text-align:center"><a href="http://www.webike.tw/review/step" style="text-align: right;"><img alt="" src="http://img.webike.tw/assets/images/review/1.png" style="float:left; height:189px; margin-left:50px; margin-right:50px; opacity:0.9; width:472px" /></a></td>
						<td colspan="2" rowspan="1" style="text-align:center"><a href="http://www.webike.tw/cart" style="text-align: right;"><img alt="" src="http://img.webike.tw/assets/images/user_upload/collection/Creditcard2017_09B628.png" style="height:196px; opacity:0.9; width:472px" /></a></td>
						<td style="text-align:center">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:center">&nbsp;</td>
						<td colspan="2" style="text-align:center">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="1" cellpadding="1" cellspacing="1" style="background-color:rgb(36, 57, 85); width:1200px">
							<tbody>
								<tr>
									<td>
									<h2><strong><span style="color:#FFFFFF"><span style="font-size:1rem">週週開信拿點數，快來領取您的現金點數&nbsp;</span></span></strong></h2>
									</td>
									<td style="text-align:center"><a href="{{$give_point_url}}" style="line-height: 21.3333px;" target="_blank"><img src="http://img.webike.tw/assets/images/newsletter/201609/160929_2.png" style="height:50px; opacity:0.9; width:340px" /></a></td>
								</tr>
								<tr>
									<td>
									<h2><strong><a href="http://www.webike.tw/customer/service/proposal" style="font-size: 15px;" target="_blank"><span style="color:#FFFFFF">如您有任何問題歡迎隨時與我們聯絡</span></a></strong></h2>
									</td>
									<td style="text-align:center"><a href="http://www.webike.tw/customer/newsletter" target="_blank"><img alt="" src="http://img.webike.tw/assets/images/newsletter/201609/160929_3.gif" style="height:50px; width:340px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="4">
						<table align="center" border="0" cellpadding="1" cellspacing="1" style="background-color:rgb(127, 127, 127); width:1200px">
							<tbody>
								<tr>
									<td style="text-align:center"><a href="http://www.webike.tw/" target="_blank">滿足您渴望的摩托人生，請務必體驗</a><a href="http://www.webike.tw/" target="_blank">「Webike台灣」</a><a href="http://www.webike.tw/" target="_blank">!!<img alt="" src="{{$open_image}}" style="height:1px; line-height:18.5714px; width:1px" /></a></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<p>&nbsp;</p>
