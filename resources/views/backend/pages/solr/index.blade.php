@extends('backend.layouts.default')
@section('middle')
    @include('backend.pages.solr.partials.status-alert')
    <div class="function-cabinet">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="function-box large-box text-center" href="{!! route('backend-solr-product') !!}">
                    <i class="fa-lg fa-fw glyphicon glyphicon-retweet"></i> 即時商品資訊更新
                </a>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="function-box large-box text-center" href="{!! route('backend-solr-product-stock-reset') !!}">
                    <i class="fa-lg fa-fw fa fa-archive"></i> 即時庫存資訊重置
                </a>
            </div>
        </div>
    </div>
@stop