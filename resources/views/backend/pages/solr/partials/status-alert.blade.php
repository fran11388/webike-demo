<div class="solr-status">
    <div class="solr-loading">
        @include('response.common.loading.md')
    </div>
    <div class="alert hide">
        <div class="row">
            <div class="col-md-8 text-left font-bold size-10rem">Solr即時狀態查詢：</div>
            <div class="col-md-4 text-right">
                <a class="btn btn-default btn-small solr-refresh" href="javascript:void(0)">
                    <i class="fa fa-lg fa-refresh"></i>
                </a>
            </div>
        </div>
        <span class="solr-alert"></span>
        <p class="text-right solr-timestamp"></p>
    </div>
</div>
<script type="text/javascript">
    function getSolrCommandStatus(){
        var loading = $('.solr-status .solr-loading');
        var ban_replace = $('.solr-indexing-ban-replace');
        var ban_area = $('.solr-indexing-ban');
        loading.removeClass('hide');
        ban_replace.hide();
        $('.solr-status .solr-alert').html('');
        $('.solr-status .alert').removeClass('alert-danger').removeClass('alert-info').removeClass('alert-warning').addClass('hide');
        $.ajax({
            url: "{!! route('backend-solr-status-command') !!}",
            method: "get",
            data: {},
            dataType: "json",
            success: function(result){
                if(result.success){
                    var alertArea = $('.solr-status .alert');
                    if(result.datas.status == 'busy'){
                        alertArea.addClass('alert-danger').removeClass('hide');
                        alertArea.find('.solr-alert').html('<i class="fa fa-lg fa-exclamation-circle"></i> 目前Solr正在匯入資料當中，請勿更新Solr商品資訊。');
                        ban_area.html('');
                        ban_replace.text('無法使用！！目前Solr正在匯入資料...').addClass('size-10rem').addClass('font-bold').show();
                    }else if(result.datas.status == 'idle'){
                        alertArea.addClass('alert-info').removeClass('hide');
                        alertArea.find('.solr-alert').html('<i class="fa fa-lg fa-check-circle"></i> Solr處於閒置中。');
                    }else{
                        alertArea.addClass('alert-warning').removeClass('hide');
                        alertArea.find('.solr-alert').html('<i class="fa fa-lg fa-question-circle"></i> 目前無法獲取Solr運行狀態。');
                    }
                    $('.solr-status .solr-timestamp').text('於' + result.datas.timestamp + '。');
                }
                loading.addClass('hide');
            },
            error: function(error){
                loading.addClass('hide');
            }
        });
    }
    $('.solr-status .solr-refresh').click(function(){
        getSolrCommandStatus();
    });
    $(document).ready(function(){
        getSolrCommandStatus();
    });
</script>