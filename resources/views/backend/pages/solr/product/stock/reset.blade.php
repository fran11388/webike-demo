@extends('backend.layouts.default')
@section('middle')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel {{($result and !$result->errors) ? 'panel-primary' : 'panel-danger'}}">
                <div class="panel-heading">
                    @if(($result and !$result->errors))
                        <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i> 已重置即時庫存相關資訊。</h3>
                    @else
                        <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i> 重置失敗！</h3>
                    @endif
                </div>
                <div class="panel-body">
                    @if($result->messages)
                        <ul class="list-group">
                            @foreach($result->messages as $message)
                                <li class="list-group-item">{{$message}}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <a class="btn btn-default" href="{!! $base_url !!}">回到上一頁</a>
        </div>
    </div>
@stop