@extends('backend.layouts.default')
@section('middle')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel {{count($products) ? 'panel-primary' : 'panel-danger'}}">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i> 同步商品資訊</h3>
                </div>
                <div class="panel-body">
                    @if($alert)
                        <div class="alert alert-danger">
                            <strong>Danger!</strong> {!! $alert !!}
                        </div>
                    @endif
                    @if(request()->get('version') === 'engineer')
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>product_id</th>
                                <th>品名</th>
                                <th>manufacturer_id</th>
                                <th>sku</th>
                                <th>is_main</th>
                                <th>type</th>
                                <th>category_id</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->manufacturer_id}}</td>
                                        <td>{{$product->sku}}</td>
                                        <td>{{$product->is_main}}</td>
                                        <td>{{$product->type}}</td>
                                        <td>{{implode(',', $product->categories->sortBy('depth')->pluck('id')->toArray())}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>product_id</th>
                                    <th>sku</th>
                                    <th>manufacturer_id</th>
                                    <th>品名</th>
                                    <th>定價</th>
                                    <th>一般價格</th>
                                    <th>經銷價格</th>
                                    <th>網址</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    @php
                                        $product = new \Ecommerce\Accessor\ProductAccessor($product);
                                    @endphp
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td>{{$product->sku}}</td>
                                        <td>{{$product->manufacturer_id}}</td>
                                        <td>{{$product->full_name}}</td>
                                        <td>{!! formatPrice( 'base-price' , $product->getPrice()) !!}</td>
                                        <td>{!! formatPrice( 'normal-price' , $product->getNormalPrice()) !!}</td>
                                        <td>{!! formatPrice( 'final-price' , $product->getFinalPrice($dealer_account)) !!}</td>
                                        <td>
                                            <a href="{!! route('parts') . '?' . http_build_query(['q' => $product->url_rewrite]) !!}" target="_blank">
                                                <i class="fa fa-lg fa-share-square"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <a class="btn btn-default" href="{!! $base_url !!}">回到上一頁</a>
        </div>
    </div>
@stop