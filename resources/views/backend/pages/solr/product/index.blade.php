@extends('backend.layouts.default')
@section('middle')
    @include('backend.pages.solr.partials.status-alert')
    <div class="function-cabinet">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i> 同步商品資訊</h3>
                    </div>
                    <div class="panel-body">
                        <div class="solr-indexing-ban">
                            <form action="{!! route('backend-solr-product-update') !!}" method="get">
                                <div class="form-group">
                                    <div class="">
                                        <label>請選擇更新異常檢查</label>
                                        <select class="form-control box" name="illegal_pass">
                                            <option value="0" selected>如有異常不動作</option>
                                            <option value="1">忽略異常sku(僅更新正確sku)</option>
                                        </select>
                                        <label>請選擇更新後檢查版本</label>
                                        <select class="form-control box" name="version">
                                            <option value="normal" selected>內容資訊版本(適用較少量更新)</option>
                                            <option value="engineer">工程師版本(適用大量更新)</option>
                                        </select>
                                        <label>請輸入需要指定的SKU</label>
                                        <textarea class="form-control" name="skus" rows="4"></textarea>
                                        <p class="help-block">您可以輸入多個商品sku，請已每行一個sku排列。</p>
                                        <div class="box">
                                            <div class="col-md-9 col-sm-9 col-xs-9"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-3">

                                                <button class="btn btn-danger btn-full" type="submit">
                                                    <i class="glyphicon glyphicon-check"></i> 確認
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="solr-indexing-ban-replace">

                        </div>
                    </div>
                </div>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-edit"></i> 移除商品資訊</h3>
                    </div>
                    <div class="panel-body">
                        <div class="solr-indexing-ban">
                            <form action="{!! route('backend-solr-product-remove') !!}" method="get">
                                <div class="form-group">
                                    <div class="">
                                        <label>請輸入需要指定的SKU</label>
                                        <textarea class="form-control" name="skus" rows="4"></textarea>
                                        <p class="help-block">您可以輸入多個商品sku，請已每行一個sku排列。</p>
                                        <div class="box">
                                            <div class="col-md-9 col-sm-9 col-xs-9"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-3">

                                                <button class="btn btn-danger btn-full" type="submit">
                                                    <i class="glyphicon glyphicon-check"></i> 確認
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="solr-indexing-ban-replace">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop