@extends('backend.layouts.default')
@section('middle')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-alert"></i> 無法使用！！</h3>
                </div>
                <div class="panel-body">
                    <div class="size-15rem font-bold" style="color:#ff0000;">
                        目前Solr正在匯入資料...請稍後...
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <a class="btn btn-default" href="{!! $base_url !!}">回到上一頁</a>
        </div>
    </div>
@stop