<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <form action="{{URL::route('backend-product')}}">
        <div class="form-group">
            <label for="usr">sku:</label>
            <input type="text" class="form-control" id="usr" name="sku" value="{{$sku?$sku:''}}">
        </div>
        <div class="form-group">
            <label for="pwd">brand:</label>
            <input type="" class="form-control" id="pwd" name="" value="{{$result?$result->brand_name:''}}">
        </div>
        <div class="form-group">
            <label for="pwd">product_name:</label>
            <input type="" class="form-control" id="pwd" name="" value="{{$result?$result->product_name:''}}">
        </div>

        <img src="{{$result?$result->image:''}}" class="rounded" alt="No image" width="304" height="236">
        <br>
        <br>
        <button type="submit" class="btn btn-primary">query</button>
    </form>
    <br><br>
    <form action="{{URL::route('backend-insert-barcode')}}" method="post">
        <div class="form-group">
            <label for="usr">sku:</label>
            <input type="text" class="form-control" id="usr" name="sku" value="{{$sku?$sku:''}}">
        </div>
        <div class="form-group">
            <label for="pwd">barcode:</label>
            <input type="" class="form-control" id="pwd" name="barcode" value="">
        </div>

        <br>
        <br>
        <button type="submit" class="btn btn-primary">setting</button>
    </form>
</div>

</body>
</html>
