@extends('backend.layouts.default')
@section('style')
    <style>
        table img {
            max-height:190px;
            min-width:120px;
            min-height:120px;
        }
        .active-select {
            float:right;
            width:150px;
            margin-right:15px;
        }
    </style>
@stop
@section('middle')
    <form method="post" action="{{\URL::route('backend-review')}}" class="row form clearfix">
        <div class="active-select block">
            <select class="select2" name="active_type">
                <option value="" selected>批次</option>
                <option value="validate">審核通過</option>
                <option value="disable">審核不通過</option>
                <option value="delete">強制刪除</option>
            </select>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-book"></i>&nbsp評論-{{ $title }}</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th><input id="checkAll" type="checkbox"></th>
                                <th>ID</th>
                                <th>刊登者</th>
                                <th>email</th>
                                <th>category</th>
                                <th>標題</th>
                                <th>內容</th>
                                <th>圖片</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reviews as $review)
                                <tr>
                                    <td><input class="btn btn-default" name="check[]" type="checkbox" value="{{$review->id}}"></td>
                                    <td>{{ $review->id }}</td>
                                    <td>{{ $review->nick_name }}</td>
                                    <td>{{ $review->customer ? $review->customer->email : '' }}</td>
                                    <td>
                                        <a href="{{ URL::route('summary',['section' => '/ca/'.$review->product_category_search]) }}" target="_blank">{{ $review->category->name }}</a>
                                    </td>
                                    <td>{{ $review->title }}</td>
                                    <td>{{ $review->content }}</td>
                                    <td>
                                        <img src="{{ $review->photo_key ? 'http://img.webike.tw/review/'.$review->photo_key : $review->product_thumbnail }}" alt="{{ $review->product_name.'-'.$review->product_manufacturer }}">
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="text-center">
        {!! $reviews->render() !!}
    </div>
@stop
@section('script')
        <script>
            $("#checkAll").change(function () {
                $("input[name='check[]']").prop('checked', $(this).prop("checked"));
            });

            $(document.body).on("change",".select2",function(){
                var check = false;
                var choose = this.value;
                if(choose){
                    var title = "你確定要" + choose + "嗎";

                    $("input[name='check[]']").each(function (event) {
                        if ($(this).prop("checked")) {
                            check = true;
                        }
                    });

                    if (check){
                        var form = $('.form');
                        swal({
                            title: title,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "確定",
                            cancelButtonText: '取消',
                            closeOnConfirm: false
                        }).then(function () {
                            form.submit();
                        });
                    }else{
                        alert('未選擇刪除項目');
                    }
                }
            });
        </script>
@stop