@inject('mitumoriModel', 'Everglory\Models\Mitumori')
@inject('groupBuyModel', 'Everglory\Models\GroupBuy')
@inject('reviewModel', 'Everglory\Models\Review')

<ul class="nav navbar-nav side-nav">
    <li class="">
        <a name="backend-assortment" href="{{URL::route('backend-assortment')}}"><i class="glyphicon glyphicon-duplicate"></i> 流行特輯 </a>
    </li>
    <li class="">
        <a name="backend-qa" href="{{URL::route('backend-qa')}}"><i class="glyphicon glyphicon-question-sign"></i> Q&A</a>
    </li>
    <li>
        <a href="javascript:void(0);" data-toggle="collapse" data-target="#review"><i class="fa fa-lg fa-fw fa-newspaper-o"></i> 評論 <span class="badge inbox-badge bg-color-greenLight" style="background: #ff1715;">{{ $reviewModel::where('view_status', 0)->count() }}</span><i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="review" class="collapse">
            <li>
                <a href="{{ \URL::route('backend-review') }}">已審核</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-review-type',['type' => 'validate']) }}">待審核 {!! $reviewModel::where('view_status', 0)->count() > 0 ? '<i class="fa fa-exclamation-circle" style="color: #ff1715;"></i>' : '' !!}</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-review-type',['type' => 'violation']) }}">不合格評論</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#coupon"><i class="fa fa-lg fa-fw fa-ticket"></i> COUPON <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="coupon" class="collapse">
            <li>
                <a href="{{ \URL::route('backend-coupon') }}">COUPON List</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-coupon-filter') }}">COUPON Send</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#newsletter"><i class="fa fa-lg fa-fw fa-envelope"></i> 電子報 <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="newsletter" class="collapse">
            <li>
                <a href="{{ \URL::route('backend-newsletter-install') }}">電子報匯入功能</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-newsletter') }}">電子報一覽</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-newsletter-queue') }}">眝列</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-review-type',['type' => 'violation']) }}">發送進度一覽</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#disp"><i class="glyphicon glyphicon-modal-window"></i> 通知小視窗 <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="disp" class="collapse">
            <li>
                <a href="{{ \URL::route('backend-disp-massage') }}">通知小視窗訊息發送</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#promo"><i class="fa fa-lg fa-fw fa-bullhorn"></i> 促銷活動 <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="promo" class="collapse">
            <li>
                <a href="{{ \URL::route('backend-promo') }}">促銷活動設定</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#weekly-promotion"><i class="fa fa-lg fa-fw fa-bullhorn"></i> 週替頁面 <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="weekly-promotion" class="collapse">
            <li>
                <a href="{{ \URL::route('backend-campaigns') }}">週替頁面設定</a>
            </li>
            <li>
                <a href="{{ \URL::route('backend-campaigns-list') }}">週替頁面列表</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#solr"><i class="fa-lg fa-fw glyphicon glyphicon-transfer"></i> Solr更新作業 <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="solr" class="collapse">
            <li>
                <a href="{{route('backend-solr')}}"><i class="fa-lg fa-fw glyphicon glyphicon-home"></i> 控制台</a>
            </li>
            <li>
                <a href="{{route('backend-solr-product')}}"><i class="fa-lg fa-fw glyphicon glyphicon-retweet"></i> 即時商品資訊更新</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#tool"><i class="fa fa-lg fa-fw fa-wrench"></i> 小工具 <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="tool" class="collapse">
            <li>
                <a href="{{route('backend-tool-imageUpload')}}"><i class="fa fa-photo"></i> 圖片上傳</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-lg fa-fw fa-gear"></i> Setting <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="demo" class="collapse">
            <li>
                <a href="{{route('backend-setting-calendar')}}"><i class="fa fa-calendar-check-o"></i> Calendar</a>
            </li>
            <li>
                <a href="{{route('backend-setting-cache')}}"><i class="fa fa-archive"></i> Cache manage</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#log"><i class="fa fa-lg fa-fw fa-qrcode"></i> LOG <i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="log" class="collapse">
            <li>
                <a href="{{route('backend-log-sales')}}">銷售</a>
            </li>
            <li>
                <a href="{{route('backend-log-creditcard')}}">信用卡</a>
            </li>
            <li>
                <a href="{{route('backend-log-virtualbank')}}">虛擬帳戶</a>
            </li>
            <li>
                <a href="{{route('backend-log-genuineparts')}}">正廠零件</a>
        </li>
            <li>
                <a href="{{route('backend-log-coupons')}}">Coupons</a>
            </li>
            <li>
                <a href="{{route('backend-log-points')}}">Points</a>
            </li>

        </ul>
    </li>

    <li class="">
        <a name="" href="{{URL::route('backend-guess')}}"><i class="fa fa-lg fa-fw fa-send"></i>WebikeGuess</a>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#mitumori"><i class="fa fa-lg fa-fw fa-search-plus"></i>未登錄系統 <span class="badge inbox-badge bg-color-greenLight" style="background: #ff1715;">{{ $mitumoriModel::where('status',0)->count() }}</span><i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="mitumori" class="collapse">
            <li>
                <a href="{{route('backend-mitumori')}}">見積一覽</a>
            </li>

        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#groupbuy"><i class="fa fa-lg fa-fw fa-search-plus"></i>團購系統 <span class="badge inbox-badge bg-color-greenLight" style="background: #ff1715;">{{ $groupBuyModel::where('status',0)->count() }}</span><i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="groupbuy" class="collapse">
            <li>
                <a href="{{route('backend-groupbuy')}}">見積一覽</a>
            </li>

        </ul>
    </li>

    <li>
        <a href="javascript:;" data-toggle="collapse" data-target="#propagandas"><i class="fa fa-lg fa-fw fa-bullhorn"></i>行銷小工具 <span class="badge inbox-badge bg-color-greenLight" style="background: #ff1715;"></span><i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="propagandas" class="collapse">
            <li>
                <a href="{{route('backend-propagandas')}}">設定一覽</a>
            </li>

        </ul>
    </li>

</ul>