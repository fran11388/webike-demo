<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
    <head>
        @include ( 'mobile.layouts.partials.head' )
        <!-- Custom CSS -->
        @yield('style')
    </head>
    <body>
        @include ( 'mobile.layouts.partials.header.index-new' )
        @yield('middle')
    </body>
</html>
@yield('script')