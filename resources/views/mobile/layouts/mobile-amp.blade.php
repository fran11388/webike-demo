<!doctype html>
<html amp>
    <head>
        @include ( 'mobile.layouts.partials.head-amp' )
        <!-- Custom CSS -->
        
        <style amp-boilerplate>
            body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
        </style>
        @yield('head')
       <style amp-custom>
            @yield('style')
           <?php 
                include "css/basic/amp-bootstrap.css";
                include "mobile/css/basic/amp/amp-main.css";
           ?>
           
            .breadcrumbs{
                line-height: 1.3;
                margin: 0 0 10px;
            }
            li{
                list-style: none;
            }
            footer .menu p{
                margin-bottom: 7px;
            }
            footer .menu p a{
                color: #88b0ff;
            }
            footer {
                background: #2a313c;
                color:#d7dbe2;
            }
            footer .menu{
                padding: 15px 30px;
            }
            footer .copyright{
                line-height: 24px;
                padding: 10px 30px 0px 30px;
                color: #d7dbe2;
            }
            footer amp-img{
                width: 150px;
            }
            .container-intruction-detail .ul-membership{
                padding: 0 15px;
            }
            .breadcrumbs ul{
                overflow: auto;
            }
            footer .eg-block{
                padding-left: 15px;
            }
            footer .eg-block amp-img{
                margin: 0 auto;
            }
            header amp-img{
                width: 125px;
            }
            #sidebar_menu{
                width: 80%;
                background-color: #fff;
            }
            .menu_items{
                display: block;
                width: 100%;
            }

            #search.search-block{
                display:block;
            }
        </style>
        @yield('script')
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
        <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
        <script async custom-element="amp-live-list" src="https://cdn.ampproject.org/v0/amp-live-list-0.1.js"></script>
        <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
        <script async custom-element="amp-animation" src="https://cdn.ampproject.org/v0/amp-animation-0.1.js"></script>
        <script async custom-element="amp-position-observer" src="https://cdn.ampproject.org/v0/amp-position-observer-0.1.js"></script>
        <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

    </head>
    <body id="main-top">
        <amp-analytics type="gtag" data-credentials="include">
            <script type="application/json">
            {
              "vars" : {
                "gtag_id": "UA-41366438-1",
                "config" : {
                    "UA-41366438-1": { "groups": "default" }
                },
                "triggers": {
                    "trackPageview": { 
                    "on": "visible",
                    "request": "pageview"
                }
              }
              }
            }
            </script>
        </amp-analytics>
        <header>
            <div on="tap:sidebar_menu.open" class="menu_btn" role="button" tabindex="0"><i class="fa fa-bars size-15rem" aria-hidden="true"></i></div>
            <div class="logo">
                <a href="{{ route('home') }}">
                    <amp-img src="{{ assetRemote('image/logo.png') }}" width="20" height="5" layout="responsive" alt="「Webike-摩托百貨」"></amp-img>
                </a>
            </div>
            @include('mobile.layouts.partials.header.block.amp.amp-login-icon')
            <div class="helpdesk">
                <div class="icon-search" on="tap:search.toggleVisibility" role="button" tabindex="0">
                    <i class="glyphicon glyphicon-search size-15rem hidden-lg hidden-md hidden-sm"></i>
                </div>
            </div>
        </header>
        <amp-sidebar id="sidebar_menu" layout="nodisplay" side="left" class="menu">
                @include('mobile.layouts.partials.header.block.amp.amp-menu')
        </amp-sidebar>
        <nav>
            <ul>
                <li><a href="{{ route('customer-wishlist') }}"><i class="fa fa-star" aria-hidden="true" title="待購清單{{$tail}}"></i>待購清單</a></li>
                <li><a href="{{ route('customer-history-order') }}"><i class="fa fa-file-text" aria-hidden="true" title="訂單歷史{{$tail}}"></i>訂單歷史</a></li>
                <li><a href="{{ route('cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true" title="購物車{{$tail}}"></i>購物車</a></li>
            </ul>
        </nav>
        <div id="search" class="search-block" hidden>
            <div class="left">
                <form action="https://www.webike.tw/getdata" method="get" target="_blank">
                    <div class="select-search-container">
                        <select  class="select2 chosen-select" name="ca" on="change:AMP.setState({ca: event.value})">
                            @php
                                $depth = [2];
                            @endphp
                            @if(isset($current_category) and !$current_manufacturer)
                                @php
                                    $depth = [$current_category->depth];
                                @endphp
                            @else
                                <option value="">分類搜尋</option>
                            @endif
                            @php
                                $categories = \Ecommerce\Repository\CategoryRepository::getCategoriesByDepths($depth);
                            @endphp
                            @foreach($categories as $category)
                                <option value="{{ $category->url_path }}" title="{{$category->name . $tail}}" {{ (isset($current_category) && $current_category->id == $category->id) ? 'selected' : '' }}>
                                    {{$category->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <ul id="form_search">
                        <li>
                            <input type="search" class="txt-search col-md-12 search_bar" required name="q" placeholder="請輸入關鍵字" value="" id="cmTableInput" autocomplete="off">
                        </li>
                        <li class="submit">
                            <input type="submit" id="cmTableSubmit" value="搜尋" >
                        </li>
                    </ul>
                    <input type="hidden" name="mt" value="{{(isset($current_motor) and count($current_motor))? $current_motor->url_rewrite : '' }}">
                </form>
            </div>
        </div>
        <div id="w_spng_sp" class="sx_slide sx_page0" >
        <!--Content -->
            <div id="main">
                <div class="breadcrumbs">
                    <ul class="clearfix">
                        {!! $breadcrumbs_html !!}
                    </ul>
                </div>
                    @yield('middle')
            </div>
            @include ( 'mobile.layouts.partials.header.block.amp.amp-scroll-top' )
            <!--End Content -->
             @include ( 'mobile.layouts.partials.footer.amp-index' )
        </div>

    </body>
</html>