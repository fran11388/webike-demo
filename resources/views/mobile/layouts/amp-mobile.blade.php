<!DOCTYPE html>
<html amp>
<head>
@include ( 'mobile.layouts.partials.amp-head' )
<!-- Custom CSS -->
    @yield('style')
    <style amp-boilerplate>
        body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
    </style>

</head>
<body itemscope itemtype="https://schema.org/WebPage">
<div id="w_spng_sp" class="sx_slide sx_page0" style="position: absolute;">
 @include ( 'mobile.layouts.partials.header.index' )
<!--Content -->
    <div id="main">
        @include ( 'mobile.layouts.partials.header.bannerbar' )
        <div class="breadcrumbs">
            <ul class="clearfix">
                {!! $breadcrumbs_html !!}
            </ul>
        </div>
            @yield('middle')
    </div>
    <!--End Content -->
     @include ( 'mobile.layouts.partials.footer.index' )
     @include('mobile.common.disps')
</div>
@include ( 'mobile.layouts.partials.script' )
@yield('script')
<script type="text/javascript">
    jQuery(document).ready(function() {
        // パンくずが表示しきれない場合にスクロールバーを右端に寄せる
        var breadcrumbsWidth = 0;
        jQuery.each(jQuery('div.breadcrumbs > ul > li'), function (i, val) {
            breadcrumbsWidth += jQuery(val).width();
        });
        jQuery('div.breadcrumbs > ul').scrollLeft(breadcrumbsWidth);
    });
</script>
</body>
</html>