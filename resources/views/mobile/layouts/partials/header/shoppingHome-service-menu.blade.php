<div class="shoppingHome-service-menu">
    <a href="{{route('mitumori')}}" class="shoppingHome-service-bt">
        <div class="shoppingHome-service-i">
            <span></span>
        </div>
        <div class="shoppingHome-service-c">
            <p>未登錄商品</p>
            <small class="dotted-text1">報價查詢&訂購系統</small>
        </div> 
    </a>
    <a href="{!! route('outlet') !!}"  class="shoppingHome-service-bt">
        <div class="shoppingHome-service-i">
            <span></span>
        </div>
        <div class="shoppingHome-service-c">
            <p>OUTLET</p>
            <small class="dotted-text1">庫存出清挖寶去</small>
        </div>
    </a>
    <a href="{{route('service-dealer-join')}}" class="shoppingHome-service-bt">
        <div class="shoppingHome-service-i">
            <span></span>
        </div>
        <div class="shoppingHome-service-c">
            <p>經銷募集</p>
            <small class="dotted-text1">歡迎相關產業加入</small>
        </div>
    </a>
    <a href="{{route('groupbuy')}}" title="團購系統-所有商品皆可開團！{{$tail}}" class="shoppingHome-service-bt">
        <div class="shoppingHome-service-i">
            <span></span>
        </div>
        <div class="shoppingHome-service-c">
            <p>Webike團購</p>
            <small class="dotted-text1">團購服務正式啟動</small>
        </div>
    </a>
    <a href="{!! route('genuineparts') !!}" class="shoppingHome-service-bt">
        <div class="shoppingHome-service-i">
            <span></span>
        </div>
        <div class="shoppingHome-service-c">
            <p>正廠零件</p>
            <small class="dotted-text1">經銷商分期付款服務</small>
        </div>
    </a>
    <a href="{{route('service-supplier-join')}}" class="shoppingHome-service-bt">
        <div class="shoppingHome-service-i">
            <span></span>
        </div>
        <div class="shoppingHome-service-c">
            <p>我有商品要販賣</p>
            <small class="dotted-text1">合作/刊登需求</small>
        </div>
    </a>
    <div class="fixclear"></div>
</div>