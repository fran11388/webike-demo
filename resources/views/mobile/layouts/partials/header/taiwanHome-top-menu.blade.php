<div class="top-menu">
    <ul class="top-menu-class taiwanHome">
        <li>
            <a href="{{route('shopping')}}">
                <div>
                    <span></span>
                    {{-- <img src="{{ assetRemote('image/list-icon/shop-02.svg')}}" alt="摩托百貨"> --}}
                </div>
                <p>摩托百貨</p>
            </a>
        </li>
        <li>
            <a href="https://www.webike.tw/bikenews">
                <div>
                    <span></span>
                    {{-- <img src="{{ assetRemote('image/list-icon/news-01.svg')}}" alt="摩托新聞"> --}}
                </div>
                <p>摩托新聞</p>
            </a>
        </li>
        <li>
            <a href="http://www.webike.tw/motomarket">
                <div>
                    <span></span>
                    {{-- <img src="{{ assetRemote('image/list-icon/carshop-01.svg')}}" alt="摩托車市"> --}}
                </div>
                <p>摩托車市</p>
            </a>
        </li>
        <li>
            <a href="{{route('collection-video')}}">
                <div>
                    <span></span>
                    {{-- <img src="{{ assetRemote('image/list-icon/channel-01.svg')}}" alt="摩托頻道"> --}}
                </div>
                <p>摩托頻道</p>
            </a>
        </li>
        <li>
            <a href="{{route('benefit-event-motogp-2019')}}">
                <div>
                    <span></span>
                    {{-- <img src="{{ assetRemote('image/list-icon/motoGP-01.svg')}}" alt="MotoGP"> --}}
                </div>
                <p>MotoGP</p>
            </a>
        </li>
    </ul>
</div>