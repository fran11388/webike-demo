<div class="top-menu">
    <ul class="top-menu-ul">
        <li class="top-menu-li left">
            <a href="分類索引"><p>分類索引</p></a>
        </li>
        <li class="top-menu-li center">
            <a href="車型索引"><p>車型索引</p></a>
        </li>
        <li class="top-menu-li right">
            <a href="品牌索引"><p>品牌索引</p></a>
        </li>
    </ul>
    <div class="fixclear"></div>
    <ul class="top-menu-class class-left">
        <li>
            <a href="{{route('summary','/ca/1000')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/ShockAbsorbers-01.svg')}}" alt="改裝零件">
                </div>
                <p class="dotted-text1">改裝零件</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/1000-1110')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/moto-01.svg')}}" alt="外觀零件">
                </div>
                <p class="dotted-text1">外觀零件</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/1000-1001')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/ExhaustPipe-02.svg')}}" alt="排氣系統">
                </div>
                <p class="dotted-text1">排氣系統</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/1000-1150')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/Chain-03.svg')}}" alt="傳動系統">
                </div>
                <p class="dotted-text1">傳動系統</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/3000-3001')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/helmet-01.svg')}}" alt="安全帽">
                </div>
                <p class="dotted-text1">安全帽</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/3000')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/coat-01.svg')}}" alt="騎士用品">
                </div>
                <p class="dotted-text1">騎士用品</p>
            </a>
        </li>
        <li>
            <a href="車用包、袋">
                <div>
                    <img src="{{ assetRemote('image/list-icon/backBag-01.svg')}}" alt="車用包、袋">
                </div>
                <p class="dotted-text1">車用包、袋</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/4000')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/oil-02-01.svg')}}" alt="保養耗材">
                </div>
                <p class="dotted-text1" >保養耗材</p>
            </a>
        </li>
        <li>
            <a href="{{route('summary','/ca/8000')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/mototool-01.svg')}}" alt="機車工具">
                </div>
                <p class="dotted-text1">機車工具</p>
            </a>
        </li>
        <li>
            <a href="{{route('genuineparts')}}">
                <div>
                    <img src="{{ assetRemote('image/list-icon/R-01.svg')}}" alt="正廠零件">
                </div>
                <p class="dotted-text1">正廠零件</p>
            </a>
        </li>
    </ul>
    <ul class="top-menu-class class-center">
        <li>
            <a href="Honda">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/honda-01.svg')}}" alt="Honda">
                </div>
                <p class="dotted-text1">Honda</p>
            </a>
        </li>
        <li>
            <a href="Yamaha">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/yamaha-01.svg')}}" alt="Yamaha">
                </div>
                <p class="dotted-text1">Yamaha</p>
            </a>
        </li>
        <li>
            <a href="Suzuki">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/suzuki-01.svg')}}" alt="Suzuki">
                </div>
                <p class="dotted-text1">Suzuki</p>
            </a>
        </li>
        <li>
            <a href="Kawasaki">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/kawasaki-01.svg')}}" alt="Kawasaki">
                </div>
                <p class="dotted-text1">Kawasaki</p>
            </a>
        </li>
        <li>
            <a href="Ducati">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/ducati-01.svg')}}" alt="Ducati">
                </div>
                <p class="dotted-text1">Ducati</p>
            </a>
        </li>
        <li>
            <a href="BMW">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/bmw-01.svg')}}" alt="BMW">
                </div>
                <p class="dotted-text1">BMW</p>
            </a>
        </li>
        <li>
            <a href="KTM">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/ktm-01.svg')}}" alt="KTM">
                </div>
                <p class="dotted-text1">KTM</p>
            </a>
        </li>
        <li>
            <a href="Aprilia">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/aprilia-01.svg')}}" alt="Aprilia">
                </div>
                <p class="dotted-text1">Aprilia</p>
            </a>
        </li>
        <li>
            <a href="Kymco">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/kymco-01.svg')}}" alt="Kymco">
                </div>
                <p class="dotted-text1">Kymco</p>
            </a>
        </li>
        <li>
            <a href="SYM">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/sym-01.svg')}}" alt="SYM">
                </div>
                <p class="dotted-text1">SYM</p>
            </a>
        </li>
        <a class="top-menu-more" href="查看更多">查看更多
            <i class="fas fa-plus"></i>
        </a>
    </ul>
    <ul class="top-menu-class class-right">
        <li>
            <a href="Shoie">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/shoei-01.svg')}}" alt="Shoie">
                </div>
                <p class="dotted-text1">Shoie</p>
            </a>
        </li>
        <li>
            <a href="Arai">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/arai-01.svg')}}" alt="Arai">
                </div>
                <p class="dotted-text1">Arai</p>
            </a>
        </li>
        <li>
            <a href="RS Taichi">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/rs-taichi2-01.svg')}}" alt="RS Taichi">
                </div>
                <p class="dotted-text1">RS Taichi</p>
            </a>
        </li>
        <li>
            <a href="Yoshimura">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/yashimura-01.svg')}}" alt="Yoshimura">
                </div>
                <p class="dotted-text1">Yoshimura</p>
            </a>
        </li>
        <li>
            <a href="Komine">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/ducati-01.svg')}}" alt="Komine">
                </div>
                <p class="dotted-text1">Komine</p>
            </a>
        </li>
        <li>
            <a href="Akrapovic">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/akrapovic-01.svg')}}" alt="Akrapovic">
                </div>
                <p class="dotted-text1">Akrapovic</p>
            </a>
        </li>
        <li>
            <a href="Ohlins">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/ohlins-01.svg')}}" alt="Ohlins">
                </div>
                <p class="dotted-text1">Ohlins</p>
            </a>
        </li>
        <li>
            <a href="Dainese">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/dainese-01.svg')}}" alt="Dainese">
                </div>
                <p class="dotted-text1">Dainese</p>
            </a>
        </li>
        <li>
            <a href="Sunster">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/sunster-01.svg')}}" alt="Sunster">
                </div>
                <p class="dotted-text1">Sunster</p>
            </a>
        </li>
        <li>
            <a href="Puig">
                <div>
                    <img src="{{ assetRemote('image/logo/bike-logo/puig-01.svg')}}" alt="Puig">
                </div>
                <p class="dotted-text1">Puig</p>
            </a>
        </li>
        <a class="top-menu-more" href="查看更多">查看更多
            <i class="fas fa-plus"></i>
        </a>
    </ul>
    <div class="fixclear"></div>
</div>
<script type="text/javascript">

    jQuery(document).ready(function($) {

        // $('.top-menu-li').click(function(event) {

        //     event.preventDefault();

        //     // $(this).stop().toggleClass('open');
        //     $(this).find('p').stop().toggleClass('open');
        //     $(this).parent().siblings().find('p').removeClass('open');

        //     $(this).
        // });
        $(document).ready(function(event){
            $('.class-left').stop().addClass('open');
            $('.left').stop().addClass('open');
        });


        $('.left').click(function(event) {

            event.preventDefault();
            $(this).stop().addClass('open');
            $('.center').stop().removeClass('open');
            $('.right').stop().removeClass('open');


            $('.class-left').stop().addClass('open');
            $('.class-center').stop().removeClass('open');
            $('.class-right').stop().removeClass('open');
        });


        $('.center').click(function(event) {

            event.preventDefault();
            $(this).stop().addClass('open');
            $('.left').stop().removeClass('open');
            $('.right').stop().removeClass('open');

            $('.class-center').stop().addClass('open');
            $('.class-right').stop().removeClass('open');
            $('.class-left').stop().removeClass('open');
        });


        $('.right').click(function(event) {

            event.preventDefault();
            $(this).stop().addClass('open');
            $('.center').stop().removeClass('open');
            $('.left').stop().removeClass('open');


            $('.class-right').stop().addClass('open');
            $('.class-center').stop().removeClass('open');
            $('.class-left').stop().removeClass('open');
        });

        // $('.left').click(function(event) {

        //     event.preventDefault();

        //     $(this).stop().toggleClass('open');
        //     $(this).find('p').stop().toggleClass('open');
        // });
        // $('.center').click(function(event) {

        //     event.preventDefault();

        //     $(this).stop().toggleClass('open');
        //     $(this).find('p').stop().toggleClass('open');
        // });
        // $('.right').click(function(event) {

        //     event.preventDefault();

        //     $(this).stop().toggleClass('open');
        //     $(this).find('p').stop().toggleClass('open');
        // });


});
</script>