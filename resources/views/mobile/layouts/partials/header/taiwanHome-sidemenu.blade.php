<div class="down-menu">
    <div class="fixclear"></div>
    <div class="down-menu-body">

        <div class="menu-list">
            <ul class="menu-list-ul">
                <li class="menu-list-li">
                    <a href="{{route('shopping')}}">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/shop-02.svg')}}" alt="摩托百貨"> --}}
                        </div>
                        <p>摩托百貨</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="https://www.webike.tw/bikenews">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/news-01.svg')}}" alt="摩托新聞"> --}}
                        </div>
                        <p>摩托新聞</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="http://www.webike.tw/motomarket">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/carshop-01.svg')}}" alt="摩托車市"> --}}
                        </div>
                        <p>摩托車市</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{{route('collection-video')}}">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/channel-01.svg')}}" alt="摩托頻道"> --}}
                        </div>
                        <p>摩托頻道</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{{route('benefit-event-motogp-2019')}}">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/motoGP-01.svg')}}" alt="MotoGP"> --}}
                        </div>
                        <p>MotoGP</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{{route('benefit')}}">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/gift-01.svg')}}" alt="會員好康">
 --}}                   </div>
                        <p>會員好康</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{{route('customer')}}">
                        <div>
                            <span></span>
                          {{--   <img src="{{ assetRemote('image/list-icon/member-01.svg')}}" alt="會員中心"> --}}
                        </div>
                        <p>會員中心</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{{route('dealer')}}">
                        <div>
                            <span></span>
                            {{-- <img src="{{ assetRemote('image/list-icon/tag-01.svg')}}" alt="經銷據點"> --}}
                        </div>
                        <p>經銷據點</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.menu-list-li').click(function(event) {
        $(this).stop().toggleClass('open');
        $(this).find('i').stop().toggleClass('open');
    });
});


</script>