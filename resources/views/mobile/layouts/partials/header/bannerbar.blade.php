<style>
    .breadcrumb-mobile {
        margin-top:0px !important;
    }
    .bannerbar-container {
        width:100%;
        height:40px;
        @if(activeValidate('2018-07-01 00:00:00','2018-08-01'))
          background: url({{assetRemote('image/banner/bar/2018July_line.jpg')}});
        {{--background: url({{assetRemote('image/banner/bar/summersale_line5.png')}}) ;--}}
        @elseif(activeValidate('2018-02-20 23:59:58','2018-04-01'))
            background: url({{assetRemote('image/banner/bar/motogp.jpg')}});
        {{--background: url({{assetRemote('image/banner/bar/summersale_line4.png')}}) ; --}}
        {{-- @elseif(activeValidate('2017-12-25 00:00:00','2017-12-25')) --}}
        {{--background: url({{assetRemote('image/banner/bar/2017christmas_last_one.jpg')}}) ;--}}
        {{--background: url({{assetRemote('image/banner/bar/summersale_line3.png')}}) ;--}}
        {{--@elseif(date('Y-m-d') == '2017-07-30')--}}
        {{--background: url({{assetRemote('image/banner/bar/summersale_line2.png')}}) ;--}}
        {{--@elseif(date('Y-m-d') == '2017-07-31')--}}
        {{--@if(date('Y-m-d') >= '2017-10-01' and date('Y-m-d') < '2017-11-01')--}}

        @endif
        {{--@endif--}}
        background-position: center;
        background-repeat: no-repeat;
        position:relative;
        margin-bottom:10px;
    }
    .bannerbar-phone{
        position:fixed;
        bottom:0px;
        width:100%;
        z-index:9999;
    }
    .bannerbar-phone .fa-times-circle-o {
        position:absolute;
        z-index:2;
        right:0px;
        top:0px;
    }
    .bannerbar-phone .fa-times-circle-o:hover {
        cursor:pointer;
    }
    .bannerbar-phone .closebutton {
        position:absolute;
        width:45px;
        height:45px;
        right:0px;
        top:0px;
        cursor:pointer;
    }
</style>

<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::STRIPE, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
?>
@if($propaganda && !session('bannerbar-close') && (strpos(Route::currentRouteName(), 'product-detail') === false) and (strpos(Route::currentRouteName(), 'cart') === false))
    <div class="visible-xs">
        <div class="bannerbar-phone">
            <div class="closebutton">
                <i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i>
            </div>
            {{-- @if(Route::currentRouteName() == 'cart')
                <a href="javascript:void(0)" onclick="slipTo('#cart')">
            @else --}}

            {{-- @endif --}}


                <a href="{{ $propaganda->link }}" target="_blank">
                    <img src="{{ $propaganda->pic_path }}">
                </a>

        </div>
    </div>
    <script>
        $('.bannerbar-phone .fa-times-circle-o').click(function(){
            $.post("{{ route('customer-session','bannerbar-close') }}");
            $('.bannerbar-phone').fadeOut(500);
        });
    </script>
@endif



