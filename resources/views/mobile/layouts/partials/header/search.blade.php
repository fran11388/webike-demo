<div class="down-search">
    <div class="sale">
        
        @if(strpos(Route::currentRouteName(), 'parts') === false)
            @include ( 'mobile.layouts.partials.header.header-search-motor' )
        @endif

        @if(strpos(Route::currentRouteName(), 'home') !== false)
            @include ( 'mobile.layouts.partials.header.search-typeBar' )
        @endif

        <div class="search-all">
            <input id="search-bar" class="search-bar" type="search" placeholder="輸入關鍵字">
            <button class="search-key-word">
                <img src="{{ assetRemote('image/list-icon/search-01.svg')}}" alt="search">
            </button>
        </div>        

    </div>
    
</div>
<script src="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.modal_btn_right').click(function(event) {
        $('.inner').stop().toggleClass('open')
        $('#search .right a').stop().toggleClass('open')
        $('.fa-plus').stop().toggleClass('open')
    });

    $(".search-bar").keydown(function (event) {
        if (event.which == 13) {
            $('.search-all .search-key-word').get(0).click();
        }
    });


    $('.search-all .search-key-word').click(function(){
        soflow = $('#soflow').find('option:selected').val();
        key_word = $('.search-all .search-bar').val();

        if(key_word){
            switch (soflow)
            {
                case "parts":
                    url = "{{route('parts')}}" + "?q=" + key_word;
                    break;
                case "review":
                    url = "{{route('review-search')}}" + "?q=" + key_word;
                    break;
                case "motor":
                    url = "http://www.webike.tw/motomarket/search/fulltext?q=" + key_word;
                    break;
                case "news":
                    uel = "https://www.webike.tw/bikenews/?s=" + key_word;
                    break;
                case "search":
                    url = "{{route('google')}}" + "?q=" + key_word;
                    break;
                default:
                    url = "{{route('parts')}}" + "?q=" + key_word;
                    break;

            }
            window.location.href = url;
        }else{
            swal('請輸入關鍵字');
        }
    });


});

</script>
    <script>

        $(function() {
            $( ".down-search #search-bar" ).autocomplete({
                minLength: 1,
                source: solr_search_source,
                focus: function( event, ui ) {
                    $('.down-search #search-bar').val();
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'header');
                    return false;
                },
                change: function(event, ui) {

                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }

                return bag
                
                .append( '<a  href="'+ item.href +'" class="search-keyword">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                .appendTo( ul )
                .closest('ul').addClass('down-search-ul');
                

            };

        });

        var current_suggest_connection = null;
        function solr_search_source(request, response){
            var params = {q: request.term};

            current_suggest_connection = $.ajax({
                url: "{{ route('api-suggest')}}",
                method:'GET',
                data : params,
                dataType: "json",
                beforeSend: function( xhr ) {
                    if(current_suggest_connection){
                        current_suggest_connection.abort();
                    }
                },
                success: function(data) {
                    response(data);
                    $("ul.ui-autocomplete.ui-menu").addClass("active");
                }
            });
        }


        $('.down-search #search-bar').keyup(function(){
            text = $(this).val();
            if(!text){
                $("ul.ui-autocomplete.ui-menu").removeClass("active");
            }
        });
    </script>