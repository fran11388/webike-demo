<div class="down-cart">
    <div class="down-cart-list">
        <ul>
            <li>
                <a href="{{route('cart')}}">
                    <p>購物車<span class="icon-count cart">(0)</span></p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
            <li>
                <a href="{{route('customer-wishlist')}}">
                    <p>待購清單<span class="icon-count wishlist">(0)</span></p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>