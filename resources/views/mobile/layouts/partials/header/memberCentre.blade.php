<div class="down-member">
    <div class="down-member-list">

        @if(\Auth::check())
            <div>
                <div class="signIn-on-hi">
                    <div class="signIn-on-member">
                        <div class="signIn-on-icon">
                            <i class="fas fa-user"></i>
                        </div>
                        <a href="{{route('customer')}}">
                            <p class="signIn-on-hi-p">{{Auth::user()->nickname}}</p>
                            <span>你好!</span>
                        </a>
                    </div>

                    <ul class="signIn-on">
                        <li>
                            <a href="{{route('cart')}}">
                                <p>折價券：<span><?php
                                        echo Everglory\Models\Coupon::whereNull('order_id')
                                            ->where('customer_id', Auth::user()->id)
                                            ->where('expired_at', '>', date('Y-m-d'))
                                            ->count();
                                        ?></span> 張</p>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('cart')}}">
                                <p>現金點數：<span>{{ $current_customer->getCurrentPoints() }}</span> 點</p>
                            </a>
                        </li>
                    </ul>
                </div>
                
            </div>
            <div class="fixclear"></div>
        @else
            <div class="down-member-login">
                <div class="signIn-off-registered-btn">
                    <a href="{{route('customer-account-create')}}" title="註冊">
                        <i class="fas fa-user-check"></i>
                        <p>註冊</p>
                    </a>
                </div>
                <div class="signIn-off-signIn-btn">
                    <a href="{{route('login')}}" title="登入">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>登入</p>
                    </a>
                </div>
            </div>
        
        @endif

            <div class="fixclear"></div>

        <ul class="down-member-ul">
            <li>
                <a href="{{ route('customer-service-message') }}">
                    <p>新訊息<span class="icon-count new_message">(0)</span></p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
            <li>
                <a href="{{route('customer-history-order')}}">
                    <p>訂單歷史</p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('customer-service-proposal') }}">
                    <p>線上諮詢</p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('customer') }}">
                    <p>會員中心</p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
            <li>
                <a href="{{ route('customer-history-purchased') }}">
                    <p>購入商品履歷</p>
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
            @if(\Auth::check())
                <li>
                    <a href="{{ route('logout') }}">
                        <p>登出</p>
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>
            @endif
        </ul>
    </div>       
        
</div>