@include('mobile.layouts.partials.header.ad-bar')
<header>
    <div class="burger-switch-background">
        <a href="javascript:void(0)" class="burger-switch hidden-lg"><span></span></a>
    </div>


    {{-- 不同頁面採用不同LOGO 註解為各LOGO的連接 --}}
    <div class="logoHome">
        @if($current_route == 'home')
            <a href="{{ route('home') }}"> <img src="{{ assetRemote('image/webike-logo/webike_taiwan_logo_b.svg') }}" width="102" alt="「Webike-摩托台灣」"></a>
        @elseif($current_route == 'collection-video')
            <a href="{{ route('collection-video') }}"> <img src="{{ assetRemote('image/webike-logo/webike_channel_logo_b.svg') }}" width="102" alt="「Webike-摩托頻道」"></a>
        @else
            <a href="{{ route('shopping') }}"> <img src="{{ assetRemote('image/webike-logo/webike_shop_logo_b.svg') }}" width="102" alt="「Webike-摩托百貨」"></a>
        @endif

{{--         <a href="{{ route('home') }}"> <img src="{{ assetRemote('image/webike-logo/webike_news_logo_b.svg') }}" width="102" alt="「Webike-摩托新聞」"></a>
        <a href="{{ route('home') }}"> <img src="{{ assetRemote('image/webike-logo/webike_carshop_logo_b.svg') }}" width="102" alt="「Webike-摩托車市」"></a> --}}
    </div>
    
    <ul>
        <li class="search-switch-background"><a class="search-switch" href=""><i class="fas fa-search"></i></a></li>

        <li class="cart-switch-background ">
            <a class="cart-switch" href="javascript:void(0)">
                <i class="fas fa-shopping-cart {{(!session()->has('cart_count') or session()->get('cart_count') == 0) ? '' : 'font-color-red'}}"></i>
            </a>
        </li>
        
        <li class="member-switch-background">
            <a class="member-switch" href="javascript:void(0)">
                <i class="fas fa-user-circle {{$current_customer ? 'font-color-red' : ''}}"></i>
            </a>
        </li>
    </ul>
    
    <div class="fixclear"></div>
</header>

@if($current_route == 'home')
    @include('mobile.layouts.partials.header.taiwanHome-sidemenu')
@else
    @include('mobile.layouts.partials.header.sidemenu')
@endif

@include('mobile.layouts.partials.header.search')


@include('mobile.layouts.partials.header.memberCentre')
@include('mobile.layouts.partials.header.cart')


<!-- / #w_spng_sp -->
<div id="pagetop"> <i class="fa fa-angle-top" aria-hidden="true"></i> </div>
<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::MOBILE_TAG, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
?>
@if($propaganda && (strpos(Route::currentRouteName(), 'product-detail') === false) and (strpos(Route::currentRouteName(), 'cart') === false))

    <div id="coupon">
        <a href="{{$propaganda->link}}">
            <img src="{{ $propaganda->pic_path }}" style="width: 140px;">
        </a>
    </div>
@endif



@if(!Auth::check()
    and (
        (strpos(Route::currentRouteName(), 'customer-fixer')  === false)
        and (Route::currentRouteName() !== 'customer-forgot-password')
        and (Route::currentRouteName() !== 'customer-reset-password')
    )
)
    @if(session()->has('invite-join') and session()->get('invite-join'))
        @include('response.common.login.pop-up')
    @endif
@endif

<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::POPUP, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
?>
@if($propaganda && (!session()->has('pop-ups-full-screen') and !session()->get('pop-ups-full-screen')))

    @include('mobile.common.pop-up-gp')
@endif



<script type="text/javascript">

    jQuery(document).ready(function($) {


        $(function(){
          var h = $(window).height();

          var th = -h
          $(".down-menu ,.down-cart ,.down-search ,.down-member").css("height", h);
          $(".down-menu ,.down-cart ,.down-search ,.down-member").css("top", th);

        });

        $('.burger-switch').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('.cart-switch-background').stop().removeClass('open');
            $('.member-switch-background').stop().removeClass('open');
            $('.search-switch-background').stop().removeClass('open');
            $('.down-cart').stop().removeClass('open');
            $('.down-member').stop().removeClass('open');
            $('.down-search').stop().removeClass('open');

            $('.down-menu').stop().toggleClass('open');
            $('.burger-switch-background').stop().toggleClass('open');

            $("ul.ui-autocomplete.ui-menu").removeClass("active");

        });
        $('.search-switch').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('.cart-switch-background').stop().removeClass('open');
            $('.member-switch-background').stop().removeClass('open');
            $('.burger-switch-background').stop().removeClass('open');

            $('.burger-switch').stop().removeClass("on");

            $('.down-menu').stop().removeClass('open');
            $('.down-cart').stop().removeClass('open');
            $('.down-member').stop().removeClass('open');
            $('.down-search').stop().toggleClass('open');
            $('.search-switch-background').stop().toggleClass('open');
            
            $("ul.ui-autocomplete.ui-menu").removeClass("active");

        });

        $('.member-switch').click(function(event) {
            /* Act on the event */
            event.preventDefault();

            $('.cart-switch-background').stop().removeClass('open');
            $('.search-switch-background').stop().removeClass('open');
            $('.burger-switch-background').stop().removeClass('open');

            $('.burger-switch').stop().removeClass("on");

            $('.down-menu').stop().removeClass('open');
            $('.down-cart').stop().removeClass('open');
            $('.down-search').stop().removeClass('open');

            $('.member-switch-background').stop().toggleClass('open');
            $('.down-member').stop().toggleClass('open');

            $("ul.ui-autocomplete.ui-menu").removeClass("active");
        });

        $('.cart-switch').click(function(event) {
            /* Act on the event */
            event.preventDefault();


            $('.member-switch-background').stop().removeClass('open');
            $('.search-switch-background').stop().removeClass('open');
            $('.burger-switch-background').stop().removeClass('open');


            $('.burger-switch').stop().removeClass("on");

            $('.down-menu').stop().removeClass('open');
            $('.down-member').stop().removeClass('open');
            $('.down-search').stop().removeClass('open');


//            $('.cart-switch-background').stop().toggleClass('open');
           $('.cart-switch-background').stop().toggleClass('open');
            $('.down-cart').stop().toggleClass('open');

            $("ul.ui-autocomplete.ui-menu").removeClass("active");
        });




        /*
        $('.cart-switch').click(function(event) {
//             Act on the event
            event.preventDefault();
            $('.cart-switch-background').stop().toggleClass('open');
            
            // $('.down-menu').stop().removeClass('open');
            // $('.burger-switch').stop().removeClass("on");
            // $('.down-search').stop().toggleClass('open');
            // $('.search-switch-background').stop().toggleClass('open');

        });
        */


        $(".burger-switch").click(function() {
            $(this).toggleClass("on");
            $(".main-mnu").slideToggle();
            return false;
        });


    });


    /* PGETOP */
    /*
    jQuery(function() {
        var topBtn = jQuery('#pagetop');
        var coupon = $("#coupon");
        topBtn.hide();
        coupon.hide();
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > 100) {
                topBtn.fadeIn();
                coupon.fadeIn();
            } else {
                topBtn.fadeOut();
                coupon.fadeOut();
            }
        });
        //スクロールしてトップ
        topBtn.click(function() {
            jQuery('body,html').animate({
                scrollTop : 0
            }, 500);
            return false;
        });

        var agent = navigator.userAgent;
        if (agent.indexOf('iPhone') > 0
            || (agent.indexOf('Android') > 0 && agent
                .indexOf('Mobile') > 0)) {
            jQuery("a:not(header a, footer a)").attr("target", "_top");
        }

    });
    */
</script>

