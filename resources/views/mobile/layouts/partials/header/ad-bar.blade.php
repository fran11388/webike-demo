<?php
if (!isset($propagandas)) $propagandas = collect([]);
$propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::HEAD_STRIPE, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
?>
@if($propaganda)
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/basic/ad-bar.css') }}">
    <div class="w-ad-bar">
        <a href="{{$propaganda->link}}">
            <p>{{$propaganda->text}}</p>
        </a>
    </div>
@endif