<div class="car-search">
    <button class="modal_btn_right">
    車型搜索
    <i class="fas fa-plus"></i>
    </button>
    
    <div class="modal model_search head-motor-search">
        <div class="inner">
            <div class="form_ui box_ui">
                <form class="motor-select-container ">
                    <div class="arrow">
                        <select id="hd_mybikelist" class="select2 hd_mybikelist">
                            <option value="">MyBike快速搜尋</option>
                            @if($current_customer and count($current_customer->motors))
                                @foreach($current_customer->motors as $motor)
                                <option value="{{ route('parts',[ 'mt/'.$motor->url_rewrite ]) }}?sort=new"> {{ $motor->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <select id="hd_mmlist" name="hd_mm" class="select-motor-manufacturer select2">
                        <option value="">請選擇廠牌</option>
                        @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                        <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                        @endforeach
                    </select>
                    <select id="hd_mdlist" name="hd_md" class="select-motor-displacement select2">
                        <option value="">cc數</option>
                    </select>
                    <select id="hd_mclist" name="hd_mc" class="select-motor-model select2">
                        <option value="">車型</option>
                    </select>
                    <div class="button_ui ">
                        <input type="button" value="搜尋" class="bc01 hd_mdl_search"  id="hd_mdl_search">
                    </div>
                    <div id="motor-search-load-container" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 motor-search-load-container">
                        @include('response.common.loading.md')
                    </div>
                </form>
            </div>
        </div>
        <div class="overlay"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('.motor-select-container').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.motor-select-container').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('.motor-select-container').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('click','.motor-select-container .hd_mdl_search',function(){
            var top = $(this).closest('.motor-select-container');
            var mybike = top.find('.hd_mybikelist').find('option:selected').val();
            var manufacturer = top.find('.select-motor-manufacturer').find('option:selected').val();
            var displacement = top.find('.select-motor-displacement').find('option:selected').val();
            var motor_model = '/mt/' + top.find('.select-motor-model').find('option:selected').val();

            var category_search_path = '';
            var current_motor_path = motor_model;
            if(!motor_model){
                current_motor_path =  "{{ (isset($current_motor) and count($current_motor)) ? '/mt/'.$current_motor->url_rewrite : ''}}";
            }
            var current_manufacturer_path = "{{ (isset($current_manufacturer) and ($current_manufacturer)) ? '/br/'.$current_manufacturer->url_rewrite : ''}}";
            var search_url = "{{ \URL::route('parts') }}";
            var category_search_value = $('#search .select-search-container select').find('option:selected').val();
            if(category_search_value){
                category_search_path = '/ca/' + category_search_value;
            }
            var search_value = $('input[name=q]').val();
            var search_words = '';
            if(search_value){
                search_words = '?q=' + search_value + '&sort=new';
            }else{
                search_words = '?sort=new';
            }
            if(mybike){
                top.find('.motor-search-load-container').show();
                window.location = mybike;
            }else if(manufacturer && displacement && motor_model){
                top.find('.motor-search-load-container').show();
                search_url = search_url + current_motor_path + category_search_path + current_manufacturer_path + search_words;
                window.location = search_url;
            }else{
                alert('請選擇車型');
            }
        });
</script>