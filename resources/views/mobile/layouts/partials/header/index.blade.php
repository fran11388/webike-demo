<header>
    <div class="logo">
        <a href="{{ route('home') }}"> <img src="{{ assetRemote('image/logo.png') }}" width="125" alt="「Webike-摩托百貨」"></a>
    </div>
    <div class="menu_btn"><i class="fa fa-bars" aria-hidden="true"></i></div>
    <div class="close_btn">×</div>
    <div class="menu_items">
        @if($current_customer)
            <div class="account-content">
                <div class="account">
                    <div class="name-title">{{ $current_customer->nickname.' 您好！' }}</div>
                    <div class="content">
                        <div class="point-now">
                            現金點數：<span class="count">{{ $current_customer->getCurrentPoints()}}</span>點 <a class="count" href="{{ \URL::route('customer-history-points') }}" title="{{ '點數獲得及使用履歷'.$tail }}">
                                <i class="glyphicon glyphicon-search size-08rem"></i>
                            </a></div>
                        <div class="coupon-now">
                            折價券：<span class="count">{{ count($current_customer->getCurrentCoupons()) }} </span>張<a class="count" href="{{ \URL::route('cart') }}" title="{{ '購物車'.$tail }}">
                                <i class="glyphicon glyphicon-search size-08rem"></i>
                            </a></div>
                    </div>
                </div>
            </div>
            @if( $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE and (strpos(Route::currentRouteName(), 'shopping') === 0) )
                <div class="level-card {!! $current_customer->getCurrentLevelCssClass() !!}">
                    <label>{{ $current_customer->getCurrentLevelName() }} 本月回饋{{$current_customer->getCurrentLevelRate()}}倍</label>
                    @if(!is_null($current_customer->getNextLevelAmount()))
                        <p>距離下個級距 NT${{$current_customer->getNextLevelAmount()}}</p>
                    @endif
                </div>
                <div class="wholesale-content">
                    <ul class="no-rcj-a">
                        <li>
                            <a href="{{ route('service-dealer-grade-info') }}" title="經銷分級詳細說明{{$tail}}" target="_blank">詳細說明</a>
                        </li>
                        <li>
                            <a href="{{ route('cart') }}" title="我的購物車{{$tail}}">我的購物車<span class="icon-count cart"></span></a>
                        </li>
                        <li>
                            <a href="{{ route('customer-wishlist') }}" title="待購清單{{$tail}}">待購清單<span class="icon-count wishlist"></span></a>
                        </li>
                        <li>
                            <a href="{{ route('customer-history-order') }}" title="訂單動態及歷史履歷{{$tail}}">訂單動態及歷史履歷<span class="tips order"></span></a>
                        </li>
                        <li>
                            <a href="{{ route('customer-history-genuineparts') }}" title="正廠零件查詢履歷{{$tail}}">正廠零件查詢履歷<span class="tips estimate-genuine"></span></a>
                        </li>
                        <li>
                            <a href="{{ route('customer-history-estimate') }}" title="商品交期查詢履歷{{$tail}}">商品交期查詢履歷<span class="tips estimate-general"></span></a>
                        </li>
                        <li>
                            <a href="{{ route('customer-history-mitumori') }}" title="未登錄商品查詢履歷{{$tail}}">未登錄商品查詢履歷<span class="tips mitumori"></span></a>
                        </li>
                        <li>
                            <a href="{{ route('customer-history-groupbuy') }}" title="團購商品查詢履歷{{$tail}}">團購商品查詢履歷<span class="tips groupbuy"></span></a>
                        </li>
                    </ul>
                </div>
            @endif
        @endif
        <div class="inner">
            <dl>
                <dt>{{ $current_route == 'home' ? '首頁' : '摩托百貨' }}</dt>
                @if($current_route == 'home')
                    @include('mobile.layouts.partials.header.block.home-link')
                @else
                    @include('mobile.layouts.partials.header.block.common-link')
                @endif
            </dl>
        </div>
    </div>
    @include('mobile.layouts.partials.header.block.login-icon')
    <div class="helpdesk">
        <a href="javascript:void(0)" class="icon-search" target="_top">
            <i class="glyphicon glyphicon-search size-15rem hidden-lg hidden-md hidden-sm"></i>
        </a>
    </div>
    <div class="overlay"></div>
</header>
<nav>
    <ul>
        <li><a href="{{ route('customer-wishlist') }}"><i class="fa fa-star" aria-hidden="true" title="待購清單{{$tail}}"></i>待購清單</a></li>
        <li><a href="{{ route('customer-history-order') }}"><i class="fa fa-file-text" aria-hidden="true" title="訂單歷史{{$tail}}"></i>訂單歷史</a></li>
        <li><a href="{{ route('cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true" title="購物車{{$tail}}"></i>購物車</a></li>
    </ul>
</nav>
@if($current_route != 'parts')
    <div id="search">
        <div class="left">
            <div class="select-search-container">
                <select  class="select2 chosen-select" name="Sel" >
                    @php
                        $depth = [2];
                    @endphp
                    @if(isset($current_category) and !$current_manufacturer)
                        @php
                            $depth = [$current_category->depth];
                        @endphp
                    @else
                        <option value="">分類搜尋</option>
                    @endif
                    @php
                        $categories = \Ecommerce\Repository\CategoryRepository::getCategoriesByDepths($depth);
                    @endphp
                    @foreach($categories as $category)
                        <option value="{{ $category->url_path }}" title="{{$category->name . $tail}}" {{ (isset($current_category) && $current_category->id == $category->id) ? 'selected' : '' }}>
                            {{$category->name}}
                        </option>
                    @endforeach
                    <script type="text/javascript">
                        // selectBunrui();
                    </script>
                </select>
            </div>
            @include('mobile.layouts.partials.header.block.searchbar')
        </div>
        <div class="right"> <a href="javascript:void(0);" class="modal_btn model_search">車型<br>
                搜尋</a> </div>
        @include('mobile.layouts.partials.header.block.motor_search')
    </div>
@endif
<!-- / #w_spng_sp -->
@php
    $mk_ticket = null;
    if(activeValidate('2018-08-01 00:00:00','2018-08-08')){
        $coupon_code = "2018Happyfathersday";
        $mk_ticket = shippingFreeCoupon($coupon_code);
    }elseif(activeValidate('2018-12-24 00:00:00','2018-12-25')){
        $mk_ticket = true;
    }
@endphp
<div id="pagetop"> <i class="fa fa-angle-top" aria-hidden="true"></i> </div>
@if($mk_ticket)
    <div id="coupon"> 
        <a href="{{route('benefit-sale-2018Christmas')}}"> 
            <img src="{{ assetRemote('/image/promotion/2018Christmas/banner/Xmastag1_1.png') }}" style="width: 140px;">
        </a> 
    </div>
@endif

@if(!Auth::check()
    and (
        (strpos(Route::currentRouteName(), 'customer-fixer')  === false)
        and (Route::currentRouteName() !== 'customer-forgot-password')
        and (Route::currentRouteName() !== 'customer-reset-password')
    )
)
    @if(session()->has('invite-join') and session()->get('invite-join'))
        @include('response.common.login.pop-up')
    @endif
@endif

@if((!session()->has('pop-ups-full-screen') and !session()->get('pop-ups-full-screen')) and ((strpos(Route::currentRouteName(), 'motogp') === false) and (strpos(Route::currentRouteName(), '2019') === false)) and activeValidate('2019-04-11 00:00:00', '2019-06-15 00:00:00'))
    @include('mobile.common.pop-up-gp')
@elseif((!session()->has('pop-ups-full-screen') and !session()->get('pop-ups-full-screen')) and activeValidate('2018-08-27 00:00:00','2018-08-31') and (strpos(Route::currentRouteName(), 'benefit-event-fatherday') === false))
    @include('mobile.common.pop-up-gp')
@endif

<script type="text/javascript">
    <!--
    /* PAGETOP */
    jQuery(function() {
        var topBtn = jQuery('#pagetop');
        var coupon = $("#coupon");
        topBtn.hide();
        coupon.hide();
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > 100) {
                topBtn.fadeIn();
                coupon.fadeIn();
            } else {
                topBtn.fadeOut();
                coupon.fadeOut();
            }
        });
        //スクロールしてトップ
        topBtn.click(function() {
            jQuery('body,html').animate({
                scrollTop : 0
            }, 500);
            return false;
        });

        var agent = navigator.userAgent;
        if (agent.indexOf('iPhone') > 0
            || (agent.indexOf('Android') > 0 && agent
                .indexOf('Mobile') > 0)) {
            jQuery("a:not(header a, footer a)").attr("target", "_top");
        }

    });
    //-->
</script>
<script type="text/javascript">
    if('{{$current_route}}' != 'parts'){

        $(document).on('change', ".select-motor-manufacturer", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get( url , function( data ) {
                    var $target = _this.closest('.motor-select-container').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".select-motor-displacement", function(){
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value){
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.motor-select-container').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get( url , function( data ) {
                    var $target = _this.closest('.motor-select-container').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0 ; i < data.length ; i++){
                        $target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('click','.motor-select-container .hd_mdl_search',function(){
            var top = $(this).closest('.motor-select-container');
            var mybike = top.find('.hd_mybikelist').find('option:selected').val();
            var manufacturer = top.find('.select-motor-manufacturer').find('option:selected').val();
            var displacement = top.find('.select-motor-displacement').find('option:selected').val();
            var motor_model = '/mt/' + top.find('.select-motor-model').find('option:selected').val();

            var category_search_path = '';
            var current_motor_path = motor_model;
            if(!motor_model){
                current_motor_path =  "{{ (isset($current_motor) and count($current_motor)) ? '/mt/'.$current_motor->url_rewrite : ''}}";
            }
            var current_manufacturer_path = "{{ (isset($current_manufacturer) and ($current_manufacturer)) ? '/br/'.$current_manufacturer->url_rewrite : ''}}";
            var search_url = "{{ \URL::route('parts') }}";
            var category_search_value = $('#search .select-search-container select').find('option:selected').val();
            if(category_search_value){
                category_search_path = '/ca/' + category_search_value;
            }
            var search_value = $('input[name=q]').val();
            var search_words = '';
            if(search_value){
                search_words = '?q=' + search_value + '&sort=new';
            }else{
                search_words = '?sort=new';
            }
            if(mybike){
                top.find('.motor-search-load-container').show();
                window.location = mybike;
            }else if(manufacturer && displacement && motor_model){
                top.find('.motor-search-load-container').show();
                search_url = search_url + current_motor_path + category_search_path + current_manufacturer_path + search_words;
                window.location = search_url;
            }else{
                alert('請選擇車型');
            }
        });
    }
    $('.helpdesk').click(function(){
        if($('#search').hasClass('active')){
            $('#search').removeClass('active');
        }else{
            $('#search').addClass('active');
        }
    });

    $(window).scroll(function(){
        if($(window).width() <= 320 ){
            if($(window).scrollTop() >= $('header').height() + $('nav').height() + $('#search').height()){
                $("nav").addClass('fixed');
            }else{
                $("nav").removeClass('fixed');
            }
        }else{
            if($(window).scrollTop() >= $('#w_spng_sp header').height() + 4 ){
                $("nav").addClass('fixed');
            }else{
                $("nav").removeClass('fixed');
            }
        }

        last = $("body").height()-$(window).height();
    });

    $('#cmTableSubmit').click(function(){
        var category_search_path = '';
        var current_motor_path =  "{{ (isset($current_motor) and count($current_motor)) ? '/mt/'.$current_motor->url_rewrite : ''}}";
        var current_manufacturer_path = "{{ (isset($current_manufacturer) and ($current_manufacturer)) ? '/br/'.$current_manufacturer->url_rewrite : ''}}";
        var search_url = "{{ \URL::route('parts') }}";
        var category_search_value = $('#search .select-search-container select').find('option:selected').val();
        if(category_search_value){
            category_search_path = '/ca/' + category_search_value;
        }
        var search_value = $('input[name=q]').val();
        var search_words = '';
        if(search_value){
            search_words = '?q=' + search_value;
        }

        if(!category_search_value && !search_value){
            alert('未輸入搜尋條件');
        }else{
            search_url = search_url + current_motor_path + category_search_path + current_manufacturer_path + search_words;
            window.location = search_url;
        }
    });

    //-->
</script>
