<div class="menu-icon login">
    @if($current_customer)
        @php
            $palette = session()->get('palette');
        @endphp
        <div class="pull-left icon-area" title="{{$current_customer->nickname}}" style="background:{{$palette['background']}};border:{{$palette['border']}};margin-top: 12px; border-radius: 50%;">
            <a class="account-icon" href="javascript: void(0)" title="會員登入{{$tail}}">
                <span class="helper"></span>
                <span class="text" style="color:{{$palette['color']}}">{{$current_customer->role_id == 1 ? '您好' : mb_substr($current_customer->nickname, 0, 2)}}</span>
            </a>
            <ul class="list">
                <li><a href="{{ route('customer-service-message') }}" title="新訊息{{$tail}}">新訊息<span class="icon-count new_message">(0)</span></a></li>
                <li><a href="{{ route('customer-service-proposal') }}" title="線上諮詢{{$tail}}">線上諮詢</a></li>
                <li><a href="{{ route('customer') }}" title="會員中心{{$tail}}">會員中心</a></li>
                <li><a href="{{ route('logout') }}" title="登出{{$tail}}">登出</a></li>
            </ul>
        </div>

    @else
        <a href="{{route('login')}}" class="">
            <i class="glyphicon glyphicon-user size-19rem hidden-xs"></i>
            <i class="glyphicon glyphicon-user size-15rem hidden-lg hidden-md hidden-sm"></i>
        </a>
    @endif
</div>