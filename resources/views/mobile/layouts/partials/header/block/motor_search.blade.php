<div class="modal model_search head-motor-search">
    <div class="inner">
        <div class="form_ui box_ui">
            <form class="motor-select-container ">
                <div class="arrow">
                    <select id="hd_mybikelist" class="select2 hd_mybikelist">
                        <option value="">MyBike快速搜尋</option>
                        @if($current_customer and count($current_customer->motors))
                            @foreach($current_customer->motors as $motor)
                                <option value="{{ route('parts',[ 'mt/'.$motor->url_rewrite ]) }}?sort=new"> {{ $motor->name }}</option>
                            @endforeach
                            <option value="{{ route('customer-mybike') }}"><i class="fa fa-edit" aria-hidden="true"></i> 編輯MyBike</option>
                        @endif
                    </select>
                </div>
                <select id="hd_mmlist" name="hd_mm" class="select-motor-manufacturer select2">
                    <option value="">請選擇廠牌</option>
                    @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                        <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                    @endforeach
                </select>
                <select id="hd_mdlist" name="hd_md" class="select-motor-displacement select2">
                    <option value="">cc數</option>
                </select>
                <select id="hd_mclist" name="hd_mc" class="select-motor-model select2">
                    <option value="">車型</option>
                </select>
                <div class="button_ui ">
                    <input type="button" value="搜尋" class="bc01 hd_mdl_search"  id="hd_mdl_search">
                </div>
                <div id="motor-search-load-container" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 motor-search-load-container">
                    @include('response.common.loading.md')
                </div>
            </form>
        </div>
    </div>
    <div class="overlay"></div>
</div>