<dd><a href="{!! route('shopping') !!}"> 摩托百貨 </a></dd>
<dd><a href="{!! MOTOMARKET !!}"> 摩托車市 </a></dd>
<dd><a href="{!! BIKENEWS !!}"> 摩托新聞 </a></dd>
<dd><a href="{{ MOTOCHANNEL }}" target="_blank"> 摩托頻道 </a></dd>
<dd><a href="{{ route('benefit-event-motogp-2019') }}" target="_blank"> 2019 MotoGP</a></dd>
<dd><a href="{{ route('collection-suzuka2018') }}" target="_blank"> 2018 鈴鹿8耐</a></dd>
<dd><a href="{{ route('benefit') }}" target="_blank"> 會員好康 </a></dd>
<dd><a href="{{ route('dealer') }}" target="_blank"> 經銷據點 </a></dd>
@if($current_customer)
    <dd><a href="{{route('customer')}}"><i class="glyphicon glyphicon-info-sign size-85rem"></i> 會員中心</a><dd>
    <dd><a href="{{route('logout')}}"><i class="glyphicon glyphicon-log-out size-85rem"></i> 登出</a></dd>
@else
    <dd><a href="{{route('login')}}"><i class="glyphicon glyphicon-user size-85rem"></i> 會員登入</a></dd>
    <dd><a href="{{route('customer-account-create')}}"><i class="glyphicon glyphicon-plus-sign size-85rem"></i> 加入會員</a></dd>
@endif