<div class="menu_items">
    @if($current_customer)
        <div class="account-content">
            <div class="account">
                <div class="name-title">{{ $current_customer->nickname.' 您好！' }}</div>
                <div class="content">
                    <div class="point-now">
                        現金點數：<span class="count">{{ $current_customer->getCurrentPoints()}}</span>點 <a class="count" href="{{ \URL::route('customer-history-points') }}" title="{{ '點數獲得及使用履歷'.$tail }}">
                            <i class="glyphicon glyphicon-search size-08rem"></i>
                        </a></div>
                    <div class="coupon-now">
                        折價券：<span class="count">{{ count($current_customer->getCurrentCoupons()) }} </span>張<a class="count" href="{{ \URL::route('cart') }}" title="{{ '購物車'.$tail }}">
                            <i class="glyphicon glyphicon-search size-08rem"></i>
                        </a></div>
                </div>
            </div>
        </div>
    @endif
	<div>
		<dl>        
			<dt>摩托百貨</dt>
			<dd><a href="{{ route('motor') }}" >車型索引</a></dd>
			@foreach(\Everglory\Constants\Category::TOP as $top_category_url_rewrite)
			    <?php
			    $menu_category = \Ecommerce\Repository\CategoryRepository::find($top_category_url_rewrite);
			    ?>
    			<amp-accordion disable-session-states>
				    <section>
				    	<h2 class="menu_link_ui amp-toogle">{{$menu_category->name}}</h2>
					    <div>
						    <dd>
						        @php
						            $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByUrlPath($menu_category->mptt->url_path, $menu_category->depth +1);
						        @endphp
						        <ul>
						            @foreach($menu_subcategories as $menu_subcategory)
						                <li>
						                    <a href="{{route('summary', ['ca/' . $menu_subcategory->url_path])}}" title="{{$menu_subcategory->name . $tail}}">
						                        {{$menu_subcategory->name}}
						                    </a>
						                </li>
						            @endforeach
						        </ul>
						    </dd>
						</div>
					</section>
				</amp-accordion>
			@endforeach
			<dd><a href="{!! URL::route('genuineparts') !!}" >正廠零件</a></dd>
			<dd><a href="{!! URL::route('outlet') !!}">Outlet</a></dd>
			<dd><a href="{{ route('benefit') }}">會員好康</a></dd>
			<dd><a href="{{ route('ranking') }}">銷售排行</a></dd>
			<dd><a href="{{URL::route('collection')}}">流行特輯</a></dd>
			<dd><a href="{{URL::route('dealer')}}">經銷據點</a></dd>
			<amp-accordion disable-session-states>
				<section>
					<h2 class="menu_link_ui amp-toogle">其他服務</h2>
					<div>
						<dd>
							<ul>
							    <li><a href="{{ route('benefit-event-motogp-2019') }}">2019 MotoGP</a></li>
							    <li><a href="{{ route('collection-suzuka2018') }}"> 2018 鈴鹿8耐</a></li>
							    <li><a href="{{route('review')}}">商品評論</a></li>
							    <li><a href="{!! MOTOMARKET !!}">摩托車市</a></li>
							    <li><a href="{!! BIKENEWS !!}">摩托新聞</a></li>
							    <li><a href="{!! MOTOCHANNEL !!}">摩托頻道</a></li>
							    <li><a href="{!! route('home') !!}">「Webike 台灣」首頁</a></li>
							</ul>
						</dd>
					</div>
				</section>
			</amp-accordion>
		</dl>
	</div>
</div>