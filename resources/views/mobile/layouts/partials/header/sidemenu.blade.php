<div class="down-menu">
    <div class="down-menu-header">
        <ul>
            <li class="down-menu-service">
                <a href="{{route('customer-service-proposal')}}" title="客服中心">
                    <i class="fas fa-question-circle"></i>
                    <p>客服中心</p>
                </a>
            </li>
            <li>
                <a href="{{route('home')}}"title="Webike台灣" >
                    <i class="fas fa-home"></i>
                    <p>Webike台灣</p>
                </a>
            </li>
        </ul>
        <div class="fixclear"></div>
    </div>
    <div class="fixclear"></div>
    <div class="down-menu-body">

        <div class="menu-list">
            <ul class="menu-list-ul">
                <li class="menu-list-li">
                    <a href="{{route('motor')}}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>車型索引</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li menu-list-li-plus1">
                    <a href="javascript:void(0)">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>改裝零件</p>
                        <i class="fas fa-plus"></i>
                    </a>
                    <ul>
                        <?php
                        $menu_category = \Ecommerce\Repository\CategoryRepository::find(1000);
                        $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByCategory($menu_category, $menu_category->depth +1);
                        ?>
                        @foreach($menu_subcategories as $menu_subcategory)
                                <a href="{{route('parts', ['ca/' . $menu_subcategory->url_path])}}" title="{{$menu_subcategory->name }}"><li class="dotted-text1">{{$menu_subcategory->name}}</li></a>
                        @endforeach
                    </ul>
                </li>
                <li class="menu-list-li menu-list-li-plus2">
                    <a href="javascript:void(0)">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>騎士用品</p>
                        <i class="fas fa-plus"></i>
                    </a>
                    <ul>
                        <?php
                        $menu_category = \Ecommerce\Repository\CategoryRepository::find(3000);
                        $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByCategory($menu_category, $menu_category->depth +1);
                        ?>
                        @foreach($menu_subcategories as $menu_subcategory)
                            <a href="{{route('parts', ['ca/' . $menu_subcategory->url_path])}}" title="{{$menu_subcategory->name }}"><li class="dotted-text1" >{{$menu_subcategory->name}}</li></a>
                        @endforeach
                    </ul>
                </li>
                <li class="menu-list-li">
                    <a href="javascript:void(0)">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>保養耗材</p>
                        <i class="fas fa-plus"></i>
                    </a>
                    <ul>
                        <?php
                        $menu_category = \Ecommerce\Repository\CategoryRepository::find(4000);
                        $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByCategory($menu_category, $menu_category->depth +1);
                        ?>
                        @foreach($menu_subcategories as $menu_subcategory)
                            <a href="{{route('parts', ['ca/' . $menu_subcategory->url_path])}}" title="{{$menu_subcategory->name }}"><li class="dotted-text1">{{$menu_subcategory->name}}</li></a>
                        @endforeach
                    </ul>
                </li>

                <li class="menu-list-li">
                    <a href="javascript:void(0)">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>機車工具</p>
                        <i class="fas fa-plus"></i>
                    </a>

                    <ul>
                        <?php
                        $menu_category = \Ecommerce\Repository\CategoryRepository::find(8000);
                        $menu_subcategories = \Ecommerce\Repository\CategoryRepository::getMpttByCategory($menu_category, $menu_category->depth +1);
                        ?>
                        @foreach($menu_subcategories as $menu_subcategory)
                            <a href="{{route('parts', ['ca/' . $menu_subcategory->url_path])}}" title="{{$menu_subcategory->name }}"><li class="dotted-text1">{{$menu_subcategory->name}}</li></a>
                        @endforeach
                    </ul>
                </li>
                <li class="menu-list-li">
                    <a href="{!! route('genuineparts') !!}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>正廠零件</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{!! route('outlet') !!}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>Outlet</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{!! route('benefit') !!}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>會員好康</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{!! route('ranking') !!}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>銷售排行</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li">
                    <a href="{!! route('collection') !!}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>流行特輯</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
                <li class="menu-list-li end">
                    <a href="{!! route('dealer') !!}">
                        <div>
                            <span class="asidemenu-span"></span>
                        </div>
                        <p>經銷據點</p>
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="fixclear"></div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.menu-list-li').click(function(event) {
        $(this).stop().toggleClass('open');
        $(this).find('i').stop().toggleClass('open');
    });

   /* $(".menu-list-li-plus1").click(function(event) {
        $(".down-menu").animate({
            scrollTop: $(".menu-list-li-plus1").offset().top - 72
        }, 1000)
    });    
    $(".menu-list-li-plus2").click(function(event) {
        $(".down-menu").animate({
            scrollTop: $(".menu-list-li-plus2").offset().top - 72
        }, 1000)
    });    
    $(".menu-list-li-plus3").click(function(event) {
        $(".down-menu").animate({
            scrollTop: $(".menu-list-li-plus3").offset().top - 72
        }, 1000)
    });

    $(".menu-list-li-plus4").click(function(event) {
        $(".down-menu").animate({
            scrollTop: $(".menu-list-li-plus4").offset().top - 72
        }, 1000)
    });*/


});


</script>