<footer>
    <div class="menu">
        <a href="{{route('home')}}" id="footer-logo">
            <amp-img src="{!!  assetRemote('image/logo.png') !!}" alt="{{\Everglory\Constants\SEO::WEBIKE_TW_LOGO_TEXT}}"  width="60" height="15" layout="responsive"></amp-img>
        </a>
        <p><i>Good ride, Good life! 台灣最大級摩托車線上百貨</i></p>

        <p><i>營運公司：<a href="http://www.everglory.asia/" title="榮芳興業有限公司" target="_blank" rel="nofollow">榮芳興業有限公司</a></i></p>
         <p><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'order'])}}">訂單及配送問題 </a></i></p>
        <p><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'service'])}}">網站操作及商品諮詢</a></i></p>
        <p><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'support'])}}">售後服務相關問題</a></i></p>
        <p><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'biz'])}}">經銷夥伴招募</a></i></p>
        <p><i><a target="_blank" style="color:#d6dbe1;" href="{{route('customer-service-proposal-create',['ticket_type' => 'supply'])}}">供應商合作洽談</a></i></p>
    </div>
    <small>
        <div class="eg-block">
            <a href="http://www.everglory.asia/" target="_blank">
                <amp-img src="https://img-webike-tw-370429.c.cdn77.org/shopping/image/eg_logo-clear.png" width="60" height="15" layout="responsive"alt="EverGlory-榮芳興業有限公司">                </amp-img>
            </a>
        </div>
        <div class="copyright">
            <h3>EverGlory © 2018 EverGlory Motors.All Rights Reserved.</h3>
        </div>
    </small>

</footer>