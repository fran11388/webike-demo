<div class="fans">
    <div class="fans-title">
        <p>關注我們</p>
        <ul>
            <li class="line"></li>
        </ul>
    </div>

    <div class="fansbox">
        <div class="fansicon">
            <a href="https://www.facebook.com/WebikeTaiwan/" rel="nofollow">
                {!! lazyImage(assetRemote('image/list-icon/facebook-c-01.svg'), '', '', 'testimg') !!}
            </a>
        </div>
    </div>
    <div class="fansbox">
        <div class="fansicon">
            <a href="https://line.me/R/ti/p/%40uqv6347i" rel="nofollow">
                {!! lazyImage(assetRemote('image/list-icon/line-c-01.svg')) !!}
            </a>
        </div>
    </div>
    <div class="fansbox">
        <div class="fansicon">
            <a href="https://www.instagram.com/webike_taiwan/?utm_source=ig_profile_share&igshid=wwl8jdoybu8i" rel="nofollow">
                {!! lazyImage(assetRemote('image/list-icon/instagram-c-01.svg')) !!}
            </a>
        </div>
    </div>
</div>

    
{{-- <div class="close_btn">×</div> --}}


<div class="fixclear"></div>

<footer>
    <div class="footer-menu">
        <div class="footer-logo">
            <a href="{{route('home')}}" id="footer-logo">
                <img src="{!! assetRemote('image/webike-logo/webike_taiwan_logo_b.svg') !!}" alt="{{\Everglory\Constants\SEO::WEBIKE_TW_LOGO_TEXT}}"/>
            </a>
        </div>
        <div class="footer-content">
            <p><i>營運公司：<a href="http://www.everglory.asia/" title="榮芳興業有限公司" target="_blank" rel="nofollow">榮芳興業有限公司</a></i></p>
            <p class="footer-content-small"><i>Good ride, Good life!<br> 台灣最大級摩托車線上百貨</i></p>
        </div>
        <div class="fixclear"></div>
        <ul>
            <li class="helper">
                <a href="{{route('customer')}}">
                    <p>客服中心 <i class="fas fa-chevron-right"></i></p>
                </a>
            </li>
            <li class="helper">
                <a href="{{route('customer-rule','member_rule_2')}}">
                    <p>常見問題 <i class="fas fa-chevron-right"></i></p>
                </a>
            </li>
            <li class="helper">
                <a href="{{route('service-dealer-join-form')}}">
                    <p>經銷招募 <i class="fas fa-chevron-right"></i></p>
                </a>
            </li>
            <li class="helper">
                <a href="{{route('service-supplier-join')}}">
                    <p>供應商合作 <i class="fas fa-chevron-right"></i></p>
                </a>
            </li>
        </ul>


         <div class="fixclear"></div>
    </div>
    <small>
        <div>
            <a href="http://www.everglory.asia/" target="_blank"><img src="https://img-webike-tw-370429.c.cdn77.org/shopping/image/eg_logo-clear.png" width="115" alt="EverGlory-榮芳興業有限公司"></a>
        </div>
        <div class="copyright">
            <h3>EverGlory © 2019 EverGlory Motors.All Rights Reserved.</h3>
        </div>
    </small>
</footer>