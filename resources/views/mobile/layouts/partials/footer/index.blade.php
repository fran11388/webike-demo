<footer>
    <div class="menu">
        <div class="clearfix">
            <div class="helper">
                <a href="{{route('home')}}" id="footer-logo">
                    <img src="{!! assetRemote('image/logo.png') !!}" alt="{{\Everglory\Constants\SEO::WEBIKE_TW_LOGO_TEXT}}"/>
                </a>
            </div>
            <div class="helper icon">
                <span class="channelicon">
                <span><a href="https://www.facebook.com/WebikeTaiwan/" rel="nofollow"> <img src="{{assetRemote('image/channelicon/facebook.png')}}" style="width: 23%;"></a></span>
                <span><a href="https://line.me/R/ti/p/%40uqv6347i" rel="nofollow"> <img src="{{assetRemote('image/channelicon/line.png')}}" style="width: 23%;"></a></span>
                <span><a href="https://www.instagram.com/webike_taiwan/?utm_source=ig_profile_share&igshid=wwl8jdoybu8i" rel="nofollow"> <img src="{{assetRemote('image/channelicon/instagram.png')}}" style="width: 23%;"></a></span>
                </span>
            </div>
        </div>
        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i>Good ride, Good life! 台灣最大級摩托車線上百貨</i></p>

        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i>營運公司：<a href="http://www.everglory.asia/" title="榮芳興業有限公司" target="_blank" rel="nofollow">榮芳興業有限公司</a></i></p>
        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1!important;" href="{{route('customer-service-proposal-create',['ticket_type' => 'order'])}}">訂單及配送問題 </a></i></p>
        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1!important;" href="{{route('customer-service-proposal-create',['ticket_type' => 'service'])}}">網站操作及商品諮詢</a></i></p>
        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1!important;" href="{{route('customer-service-proposal-create',['ticket_type' => 'support'])}}">售後服務相關問題</a></i></p>
        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1!important;" href="{{route('customer-service-proposal-create',['ticket_type' => 'biz'])}}">經銷夥伴招募</a></i></p>
        <p style="margin-bottom: 7px;/* margin-top: 0; */"><i><a target="_blank" style="color:#d6dbe1!important;" href="{{route('customer-service-proposal-create',['ticket_type' => 'supply'])}}">供應商合作洽談</a></i></p>
    </div>
    <small><div><a href="http://www.everglory.asia/" target="_blank"><img src="https://img-webike-tw-370429.c.cdn77.org/shopping/image/eg_logo-clear.png" width="115" alt="EverGlory-榮芳興業有限公司"></a></div>
        <div class="copyright">
            <h3>EverGlory © 2018 EverGlory Motors.All Rights Reserved.</h3>
        </div>
    </small>

</footer>