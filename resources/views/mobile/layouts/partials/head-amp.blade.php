<meta charset="utf-8">
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-language" content="zh-tw">
<meta name="csrf-token" content="{!! csrf_token() !!}">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.gstatic.com" rel="preconnect">
<link href="https://fonts.gstatic.com" rel="dns-prefetch">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<noscript>
  <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
</noscript>

<link rel="shortcut icon" href="{{ assetPathTranslator('favicon.ico', config('app.env')) }}" type="image/x-icon">
<link rel="icon" href="{{ assetPathTranslator('favicon.ico', config('app.env')) }}" type="image/x-icon">

@php
    $page_arg = \Request::get('page');
@endphp
@if( $page_arg )
    @php
        $url = \URL::current();
    @endphp
@endif

<title>{{ $seo_title }}</title>
<meta name="title" content="{{ $seo_title }}"/>
<meta name="description" content="{{ isset($seo_description) ? $seo_description : '' }}"/>
<meta itemprop="name" content="Webike-摩托百貨">
<meta itemprop="description" content="{{ isset($seo_description) ? $seo_description : '' }}">
<meta itemprop="image" content="{{ isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE }}">
<meta property="og:title" content="{{ $seo_title }}"/>
<meta property="og:description" content="{{ isset($seo_description) ? $seo_description : '' }}"/>
<meta property="og:image" content="{{ isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE }}"/>
<meta property="og:url" content="{!! Request::url() !!}"/>
<meta property="og:site_name" content="Webike-摩托百貨"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="Webike-摩托百貨"/>
<meta property="og:locale" content="zh_TW"/>
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="{{ $seo_title }}"/>
<meta name="twitter:description" content="{{ isset($seo_description) ? $seo_description : '' }}"/>
<meta name="twitter:image" content="{{ isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE }}"/>
<meta name="twitter:url" content="{!! Request::url() !!}"/>

