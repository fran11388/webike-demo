<meta charset="utf-8">
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="content-language" content="zh-tw">
<meta name="csrf-token" content="{!! csrf_token() !!}">

<noscript>
  <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
</noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>

<link rel="shortcut icon" href="{{ assetPathTranslator('favicon.ico', config('app.APP_URL')) }}" type="image/x-icon">
<link rel="icon" href="{{ assetPathTranslator('favicon.ico', config('app.APP_URL')) }}" type="image/x-icon">

@if(\Request::route()->getName() == 'summary' || \Request::route()->getName() == 'product-detail')
    <link rel="canonical" href="{{\URL::current()}}">
@endif
@php
    $page_arg = \Request::get('page');
@endphp
@if( $page_arg )
    @php
        $url = \URL::current();
    @endphp
    <link rel="next" href="{!!$url.'?&page='.($page_arg+1)!!}" />
    <link rel="prev" href="{!!$url.'?&page='.($page_arg-1)!!}" />
@endif

<title>{!! $seo_title !!}</title>
<meta name="title" content="{!! $seo_title !!}"/>
<meta name="description" content="{!! isset($seo_description) ? $seo_description : '' !!}"/>
<meta name="keywords" content="{!! isset($seo_keywords) ? $seo_keywords : '' !!}"/>
<meta itemprop="name" content="Webike-摩托百貨">
<meta itemprop="description" content="{!! isset($seo_description) ? $seo_description : '' !!}">
<meta itemprop="image" content="{!! isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE !!}">
<meta property="og:title" content="{!! $seo_title !!}"/>
<meta property="og:description" content="{!! isset($seo_description) ? $seo_description : '' !!}"/>
<meta property="og:image" content="{!! isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE !!}"/>
<meta property="og:url" content="{!! Request::url() !!}"/>
<meta property="og:site_name" content="Webike-摩托百貨"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="Webike-摩托百貨"/>
<meta property="og:locale" content="zh_TW"/>
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="{!! $seo_title !!}"/>
<meta name="twitter:description" content="{!! isset($seo_description) ? $seo_description : '' !!}"/>
<meta name="twitter:image" content="{!! isset($seo_image) ? $seo_image : \Everglory\Constants\SEO::IMAGE !!}"/>
<meta name="twitter:url" content="{!! Request::url() !!}"/>
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/basic/bootstrap.min.css') }}">

<!-- Basic CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('mobile/css/basic/jquery.sidr.light.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('mobile/css/basic/popup.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('mobile/css/basic/w_spng_sp.css') }}">

<!-- Custom Fonts -->

<link href="{{ asset('plugin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/owl-carousel/assets/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/slick/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('plugin/slick/slick-theme.css') }}">
<link rel="stylesheet" href="{{ assetRemote('plugin/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ assetRemote('plugin/sweetalert2/sweetalert2.min.css') }}">
{{--<link rel="stylesheet" href="{{ assetRemote('plugin/jQuery.mmenu-master/dist/jquery.mmenu.all.css') }}">--}}

<!-- Custom CSS -->
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/basic/main.css') }}">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('css/basic/common-script.css') }}">

<!-- js preload -->
<!-- jQuery -->
<script src="{{ assetRemote('js/basic/jquery.js') }}"></script>
<!-- owl carousel-->
<script src="{{ assetRemote('plugin/owl-carousel/owl.carousel.min.js') }}"></script>

@include('response.common.dfp.ad-script')

<!-- Piwik -->
<script type="text/javascript">
    var _paq = _paq || [];
    @if(isset($current_customer) and $current_customer)
        _paq.push(['setUserId', 'UID-{{ ($current_customer->id + 9487 ) }}']);
    @endif
</script>
<!-- End Piwik Code -->