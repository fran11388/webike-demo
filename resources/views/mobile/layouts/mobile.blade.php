<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>

@include ( 'mobile.layouts.partials.head' )

<!-- Custom CSS -->
    @yield('style')
    

</head>
<body itemscope itemtype="https://schema.org/WebPage">
<div id="w_spng_sp" class="sx_slide sx_page0" style="position: absolute;">
        {{-- @include ( 'mobile.layouts.partials.header.index' ) --}}
        @include ( 'mobile.layouts.partials.header.index-new' )

<!--Content -->
    <div id="main">
        @if((strpos(Route::currentRouteName(), 'shopping') !== false))
            @include ( 'mobile.layouts.partials.header.shoppingHome-top-menu' )
        @elseif((strpos(Route::currentRouteName(), 'home') !== false))
            @include ( 'mobile.layouts.partials.header.taiwanHome-top-menu' )
        @endif

        @include ( 'mobile.layouts.partials.header.bannerbar' )
        <div class="breadcrumbs">
            <ul class="clearfix">
                {!! $breadcrumbs_html !!}
            </ul>
        </div>
            @yield('middle')
    </div>
    <!--End Content -->

{{--     @include ( 'mobile.layouts.partials.footer.index' ) --}} 

    @include ( 'mobile.layouts.partials.footer.index-new' )

    @include('mobile.common.disps')
</div>
@include ( 'mobile.layouts.partials.script' )
@yield('script')
@include ( 'response.layouts.partials.track.google-sending' )
<script type="text/javascript">
    jQuery(document).ready(function() {
        // パンくずが表示しきれない場合にスクロールバーを右端に寄せる
        var breadcrumbsWidth = 0;
        jQuery.each(jQuery('div.breadcrumbs > ul > li'), function (i, val) {
            breadcrumbsWidth += jQuery(val).width();
        });
        jQuery('div.breadcrumbs > ul').scrollLeft(breadcrumbsWidth);
    });


</script>
</body>
</html>