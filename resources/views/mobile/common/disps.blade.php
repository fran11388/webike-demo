<style>
    .disps-container.history-box-outside-content{
        margin: 2%;
        top: 10%;
        right: 0;
        width: 250px !important;
        position: fixed;
        z-index: 9999;
        background-color: #fff;
    }
    .disps-container.box-page-group {
        float: left;
        border: 1px solid #e0e6f0;
        list-style: none;
        -webkit-box-shadow: 0 1px 2px #e0e6
    }
    .disps-container.history-box-outside-content .title-box-page-group {
        background-color: #e61e25 !important;
        color: #fff;
        display: flex;
        flex-wrap: nowrap;
        justify-content: center;
        position: relative;
        padding: 0px 5px;
        width: 100%;
        float: left;
        border-bottom: 1px solid #e61e25;
    }
    .disps-container.history-box-outside-content .title-box-page-group a.history-btn-chevron-down {
        left: 10px;
        width: 35px;
        height: 35px;
    }
    .disps-container.history-box-outside-content .title-box-page-group a {
        color: #fff;
        position: absolute;
        font-size: 1.5em;
        display: flex;
        text-decoration: none;
        transition: all 0.3s ease;
        border: none;
    }
    .disps-container.history-box-outside-content .title-box-page-group a.history-btn-close {
        right: -5px;
        top: -10px;
        background: #000;
        color: #fff;
        border-radius: 50%;
        font-weight: normal;
    }
    .disps-container.box-page-group .ct-box-page-group{
        width: 100%;
        float: left;
    }
    .disps-container.box-page-group .ct-box-page-group .ul-menu-ct-box-page-group{
        padding: 0 8px;
        width: 100%;
        float: left;
    }
    .disps-container.box-page-group .ct-box-page-group .ul-menu-ct-box-page-group li a {
        width: 100%;
        color: #333333;
        float: left;
        font-size: 0.85rem;
        line-height: 30px;
    }
    .disps-container.history-box-outside-content .ul-menu-ct-box-page-group li a i{
        color: #e61e25;
    }
    .disps-container.history-box-outside-content .ul-menu-ct-box-page-group li a span{
        color: #005599;
        font-weight: bold;
    }
    .disps-container .title-box-page-group h3, .title-box-page-group span{
        float: left;
        font-weight: bold;
        line-height: 34px;
    }
    .disps-container.history-box-outside-content .title-box-page-group a.history-btn-chevron-down:hover{
        color: #000;
    }


</style>
@if(isset($disps_active) and $disps_active and isset($disps) and $disps->count())
    <div class="box-page-group history-box-outside-content disps-container">
        <div class="title-box-page-group">
            <a class="history-btn-chevron-down" href="javascript:void(0)"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
            <h3>訊息通知</h3>
            <a class="history-btn-close" href="javascript:void(0)"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a>
        </div>
        <div class="ct-box-page-group">
            <ul class="ul-menu-ct-box-page-group" style="display: block;">
                @foreach($disps as $disp)
                    @if(!isset($ticket) or !$disp->group or !$ticket->increment_id == $disp->code)
                        <li>
                            <a class="disp" href="{{$disp->link}}" title="{{$disp->name}}" name="{{$disp->group . '@' . $disp->code}}" target="_blank"><i class="fa fa-chevron-right" aria-hidden="true"></i><span> {{$disp->name}}</span></a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
@endif