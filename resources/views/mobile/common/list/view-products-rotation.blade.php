<div class="row box-content">
    @if(count($pd) != 0)
        <div class="ct-riding-gear col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="product-list owl-carousel-history">
                @foreach ($pd as $product)
                <li>
                    @include('response.common.product.d')
                </li>
                @endforeach
            </ul>
        </div>
    @else
        <h3 class="text-center font-bold" style="padding:20px">此品牌暫無新商品</h3>
    @endif
</div>

