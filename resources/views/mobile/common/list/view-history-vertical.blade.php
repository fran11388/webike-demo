@if($current_customer)
    <div class="box-page-group">
        <div class="title-box-page-group title-box-cart-page-detail">
            <h3>最近瀏覽商品</h3>
        </div>
        <div class="ct-box-page-group">
            <div>
                <ul class="ct-item-product-grid-car-page">
                    @foreach ($recent_views as $product)
                        <li class="item-product-grid item-product-grid-car-page">
                            <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
                                <figure class="zoom-image">
                                    <img src="{!! $product->getThumbnail() !!}" alt="{!! $product->full_name !!}"/>
                                </figure>
                            </a>
                            <a class="title-product-item dotted-text2 text-left" href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}" title="{!! $product->full_name !!}">{!!  $product->full_name  !!}</a>
                            <label class="price">NT${!!  number_format($product->getFinalPrice($current_customer))  !!}</label>
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
@endif