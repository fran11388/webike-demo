<div class="slide-down-btn">
    <a href="javascript:void(0)" class="a-left-title">
        <span>車輛搜尋</span>
        <i class="fa fa-chevron-up" aria-hidden="true"></i>
    </a>
</div>
<ul class="ul-sub1 row">
    <li class="li-sub1 col-md-12 col-sm-12 col-xs-12">
        <ul class="ul-sub2 row">
            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box btn-gap-top">
                <div class="slide-down-btn">
                    <div class="a-sub2">
                        <select class="select2 select-motor-manufacturer">
                            <option>請選擇廠牌</option>
                            @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </li>
            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                <div class="slide-down-btn">
                    <div class="a-sub2">
                        <select class="select2 select-motor-displacement">
                            <option>cc數</option>
                        </select>
                    </div>
                </div>
            </li>
            <li class="li-sub2 col-md-12 col-sm-12 col-xs-12 box">
                <div class="slide-down-btn">
                    <div class="a-sub2">
                        <select class="select2 select-motor-model">
                            <option>車型</option>
                        </select>
                    </div>
                </div>
            </li>
        </ul>
    </li>
</ul>