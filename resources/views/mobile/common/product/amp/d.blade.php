<div class="product-grid text-center">
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <figure class="zoom-image">
            <amp-img src="{!! $product->getThumbnail()!!}" alt="{{ $product->full_name }}" width="200" height="200" layout="responsive"></amp-img>            
        </figure>
    </a>
    <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}">
        <span class="name product-name dotted-text2 force-limit">{!!  $product->name  !!}</span>
    </a>
        <span class="name dotted-text1 force-limit">{!!  $product->manufacturer->name  !!}</span>

    <label class="final-price box show">NT${!!  number_format($product->getFinalPrice($current_customer))  !!}</label>
    <span class="name dotted-text1 force-limit">
        @if($motors = $product->getMotors() and count($motors))
            {{$motors->first()->name}}
        @endif
    </span>

</div>