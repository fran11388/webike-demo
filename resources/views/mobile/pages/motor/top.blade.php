@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/motor-top.css') }}">
    <link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">
@stop
@section('middle')
<div>
    <h1 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h1>
    @include('mobile.pages.summary.partials.content-tabs')
    <div class="section block">
        <h3 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h3>
        <div class="mybike_log">
            @include('mobile.pages.motor.partials.info')
            @include('mobile.pages.motor.partials.analysis')
        </div>
    </div>
    <div class="container-block">
        @if(count($news))
            <div class="section block">
                <h3 class="common">{{isset($current_name) ? $current_name : $summary_title}} 最新消息</h3>
                <div class="relation_info mb10">
                    <ul>
                        @foreach ($news as $key => $post)
                            @php
                                $photo = \Everglory\Models\News\Meta::join('wp_posts', 'meta_value', '=', 'wp_posts.ID')
                                ->where( 'wp_postmeta.post_id', $post->ID )
                                ->where( 'wp_postmeta.meta_key', '_thumbnail_id' )
                                ->first();
                                $photo_path = 'http://www.webike.tw/bikenews/wp-content/themes/opinion_tcd018/img/common/no_image2.jpg';
                                if( $photo and $photo->guid ){
                                    $photo_path = $photo->guid;
                                }

                                $description_data = \Everglory\Models\News\Meta::where( 'post_id', $post->ID )
                                ->where( 'meta_key', 'short_description' )
                                ->first();

                                if( $description_data ){
                                    $short_description = $description_data->meta_value;
                                }
                            @endphp
                            <li class="news">
                                <div class="news-img">
                                    <a class="zoom-image" href="{{ BIKENEWS.'/'.$post->post_name}} "> 
                                        <img src="{{$photo_path}}" alt="{{$post->post_title}} - 「Webike-摩托新聞」">
                                    </a>
                                </div>
                                <div class="news-detail">
                                    <div class="news-title">
                                        <a href="{{ BIKENEWS.'/'.$post->post_name}} "> 
                                            {{$post->post_title}}
                                        </a>
                                    </div> 
                                </div>
                                <div class="news-time">
                                        <span>{{$post->post_date}}</span>                                        
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
       <div class="section block">
            <h3 class="common"> {{$current_name}} 商品搜尋 </h3>
            <div class="price_info">
                <div><input id="motor_search_text" type="text" name="q" value="" class="btn btn-default btn-full" placeholder="請輸入商品關鍵字..."></div>
                <div><a id="motor_search" class="btn btn-danger btn-full btn-gap-top" href="javascript:void(0)">搜尋</a></div>
            </div>
        </div>
        @if(count($new_products))
            <div class="section block">
                <div class="scroll_ui">
                    <h3 class="common"> {{ $summary_title }} 新商品</h3>
                    <div>
                        <ul class="product-list owl-carousel-6">
                            @foreach ($new_products->chunk(50) as $chunks)
                                    @foreach ($chunks as $product)
                                        <li>
                                            @include('mobile.pages.motor.partials.newproducts')
                                        </li>
                                    @endforeach
                            @endforeach
                        </ul>
                        <div class="text-center more-bolck">
                            <a href="{{ modifySummaryUrl(null,true) }}?sort=new" class="btn btn-warning" title="{{ $summary_title . ' 全部新商品' . $tail }}">查看全部</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count($reviews))
            <div class="section block">
                <h3 class="common">{{ $summary_title }} 商品評論　</h3>
                <div class="new-review">
                    <ul class="owl-carousel-4">
                        @foreach ($reviews as $review)
                            <?php $href = route('review-show', $review->id) ?>
                            <li class=" item item-product-grid item-product-custom-part-on-sale">
                                <div>
                                    <div class="text-center">
                                        <a href="{{ $href }}">
                                            <figure class="zoom-image thumb-box-border">
                                                <?php
                                                $src = '';
                                                if (starts_with($review->photo_key, 'http'))
                                                    $src = $review->photo_key;
                                                else
                                                    $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                                ?>

                                                <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="item-product-grid-pars-on-sale-right text-left">
                                        <span class="dotted-text1 ">{{ $review->product_manufacturer }}</span>
                                        <a href="{{ $href }}" class="title-product-item-top dotted-text2" title="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                            <span class="dotted-text2">{{ $review->product_name }}</span>
                                        </a>
                                        <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
                                    </div>
                                </div>
                                <div class="new-review-content">
                                    <a href="{{ $href }}" class="title-product-item dotted-text2">{{ $review->title }}</a>
                                    <label class="normal-text dotted-text2">{!! mb_substr(e($review->content),0,35,"utf-8") !!}
                                        <a href="{{ $href }}" class="btn-customer-review-read-more">...More</a>
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="text-center more-bolck">
                        @php
                            $replace_segments = null;
                            if(isset($current_manufacturer)){
                                $replace_segments = ['br' => $current_manufacturer->name];
                            }
                        @endphp
                        <a href="{{modifyReviewSearchUrlSummary($replace_segments)}}" class="btn btn-warning" title="{{ $summary_title . ' 全部商品評論' . $tail }}">更多評論</a>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($newmotors) and count($newmotors))
            <div class="section block">
                <h3 class="common"> {{isset($current_name) ? $current_name : $summary_title}} 新車・中古車情報 </h3>
                <div class="newmotors">
                    <div class="ct-new-magazine">
                        <ul class=" owl-carousel-6">
                            @foreach($newmotors as $motor_num => $new_motor)
                                <li class="product-grid">
                                    @include('mobile.pages.motor.partials.newmotors',['motor' => $new_motor, 'motor_num' => $motor_num])
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="text-center more-bolck">
                        <a href="http://www.webike.tw/motomarket/brand/{{$newmotors->first()->manufacurer}}/motor/{{$newmotors->first()->motor_model_id}}" class="btn btn-warning" target="_blank">查看全部</a>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@stop
@section('script')
    <script src="{{assetRemote('js/pages/searchList/searchList.js')}}"></script>
    <script>
        $(document).on("click", "#motor_search", function () {
            q = $('#motor_search_text').val();
            window.location.href = "{{URL::route('parts', 'mt/' . $current_motor->url_rewrite)}}" + "?q=" + q;
        });
        @if($current_motor)
            function addMyBike()
            {
                $('.add-my-bikes-btn').addClass('hide');
                $('.add-my-bikes-disabled').removeClass('hide');
                $.ajax({
                    type: 'GET',
                    url: "{!! route('customer-mybike-add', $current_motor->url_rewrite) !!}",
                    data: {},
                    dataType: 'json',
                    success: function(result){
                        if(result.success){
                            var ajax_html = '<span class="size-10rem">{!! $current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') !!} 已經加入MyBike清單。若要移除或新增車型，請至<a href="{!! route('customer-mybike') !!}" target="_blank">會員中心修正</a>。</span><br><br>';
                            if(result.messages.length){
                                ajax_html += result.messages.join('<br>');
                            }
                            swal({
                                title: "成功加入Mybike",
                                html:  '<div class="text-left">' + ajax_html + '</div>',
                                type: "success",
                                confirmButtonText: "確定"
                            });
                        }

                    }
                });
            }
        @endif
    </script>
@stop