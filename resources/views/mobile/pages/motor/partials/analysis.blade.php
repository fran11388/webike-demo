@if(isset($modelInfo))
    <div class="rating_all side_p">
        <div class="title">車友綜合評價</div>
        <?php
        $names = [
            'mybike_self_rating_fuelcost' => '油耗',
            'mybike_self_rating_driving' => '馬力',
            'mybike_self_rating_design' => '外觀',
            'mybike_self_rating_mente' => '保養',
            'mybike_self_rating_carry' => '乘載',
            'mybike_self_rating_handring' => '操控',
        ];
        $num = 1;
        ?>
        <table border="0" cellspacing="0" cellpadding="0" class="ratings">
            <tbody>
            @foreach($names as $key => $name)
                @if($num % 2 !=0)
                    <tr>
                @endif
                    <?php
                    $avg = ( (is_numeric($modelInfo->$key) ? $modelInfo->$key : 0) / 5 ) * 100;
                    ?>
                    <th>{{ $name }}</th>
                    <td>{{ number_format($avg / 20,1)  }}</td>
                @if($num % 2 ==0)
                    </tr>
                @endif
                @php
                    $num++;
                @endphp
            @endforeach
                <tr>
                    <td colspan="4">
                    <p class="analysis-block">
                        <span>綜合評價</span>
                        <span class="icon-star-bigger {{is_numeric($modelInfo->mybike_self_rating_total) ? 'star-0' . round($modelInfo->mybike_self_rating_total / 20) : ''}}"></span>
                        <span id="ave">{{ number_format(is_numeric($modelInfo->mybike_self_rating_total) ? $modelInfo->mybike_self_rating_total : 0 / 20,1) }}</span>
                    </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endif