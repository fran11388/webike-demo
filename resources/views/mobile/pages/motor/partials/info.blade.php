<div class="main_image">
    {{--<img src="https://img.webike.net/sys_images/bg.png" data-src="{{$current_motor->image ? $current_motor->image_path . 'L_' . $current_motor->image : NO_IMAGE }}">--}}
    <img src="{{$current_motor->image ? cdnTransform($current_motor->image_path . 'L_' . $current_motor->image) : cdnTransform(NO_IMAGE) }}" alt="{{$current_motor->name}}">
</div>
<div class="mybike_reg side_p box">
    <div>
        @if($current_customer)
            　<a class="btn btn-default add-my-bikes-btn {!! (!$addMyBikesDisabled ? 'add-my-bikes-disabled hide' : '') !!}" href="javascript:void(0);" disabled><i class="glyphicon glyphicon-ok-sign"></i> 已加入MyBike清單</a>
            　<a class="btn btn-danger add-my-bikes-btn {!! ($addMyBikesDisabled ? 'add-my-bikes-not-disabled hide' : '') !!}" href="javascript:void(0);" onclick="addMyBike();"><i class="glyphicon glyphicon-plus-sign"></i> 加入MyBike清單</a>
        @endif
        {{--<a href="https://ssl.webike.net/private/flow/mybikeEdit-flow"><i class="fa fa-plus-circle" aria-hidden="true"></i>Myバイクを登録する</a>--}}
    </div>
</div>
