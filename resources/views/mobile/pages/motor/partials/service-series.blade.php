<dd>
    <ul class="box-content-child grid-item-motor-video">
        @foreach($series as $year => $series_motors)
            <li class="box-fix-column item-motor-video {{$catalogue->model_release_year == $year ? 'active' : '' }}">
                @foreach($series_motors as $series_motor)
                    @if($loop->iteration == 1 )
                        <div class="box-fix-auto right series-motor">
                            <a href="{{route('motor-service',[$current_motor->url_rewrite,$series_motor->model_catalogue_id])}}">
                                {{$current_motor->name . $series_motor->model_grade}} {{$series_motor->model_release_year}}年
                            </a>
                        </div>
                    @endif
                @endforeach
            </li>
        @endforeach
    </ul>
</dd>