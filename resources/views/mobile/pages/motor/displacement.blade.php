@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/motor-mf.css') }}"> 
@stop
@section('middle')

@foreach($motors as $motor)
    <?php 
        $title_name = str_replace(array('【','】'),'',$title);
        $manufacturer = $motor->manufacturer;
        $url_rewrite = $motor->url_rewrite;
        $faurl_rewrite = $motor->faurl_rewrite;
        $import_manufacturers = array('HONDA','YAMAHA','SUZUKI','KAWASAKI');
        $manufacturer_value = false;
    ?>
@endforeach
    <div class="page-main-part box-fix-auto">
        <div class="page-part displacement-search">
            <div class="title-main-box">
                @if(in_array($manufacturer,$import_manufacturers))
                    <h1>{{$title_name .' 車型索引'}}</h1>
                    @php
                        $manufacturer_value = true;
                    @endphp
                @else
                    <h1>{{$manufacturer .' 車型索引'}}</h1>
                @endif
            </div>
            <div class="search-motor-bar">
                <label class="search-text">
                    <input class="search-motor-text" type="text" name="search" placeholder="輸入關鍵字搜尋">
                </label>
                <div>
                    <label class="search-btn">
                        <button class="btn bg-color-blue serach-clear">
                            清除
                        </button>
                    </label>
                    <label class="search-btn">
                        <button class="btn bg-color-red search-motos">
                            搜尋
                        </button>
                    </label>
                </div>
            </div>
        </div>
        <div class="page-part">
             @if($manufacturer_value)   
                <div class="ct-box-page-group">
                    <div class="displacement-title">
                        <h2>排氣量搜尋</h2>
                    </div>
                    <div class="displacement-search-bar">
                        <select class="select2" onChange="location.href=this.value">
                            <option value="" >請選擇排氣量</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,50]) }}">0 - 50cc</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,125]) }}">51 - 125cc</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,250]) }}">126 - 250cc</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,400]) }}">251 - 400cc</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,750]) }}">401 - 750cc</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,1000]) }}">751 - 1000cc</option>
                            <option value="{{URL::route('motor-manufacturer',[$faurl_rewrite,1001]) }}">1001cc 以上</option>
                        </select>
                    </div>
                </div>
            @endif
        </div>
        <div id="motors-displacement-list" class="page-part page-border">
            <div class="displacement-title displacement-list">
                <h2>{{$manufacturer .' 車型列表'}}</h2>
            </div>
        </div>  
        <div class="brand-moto-table page-part">
            <div class="brand-moto-table-title">
                <div class="col-md-2 col-sm-2 col-xs-2 table-title-left"><span class="size-10rem">排氣量</span></div>
                <div class="col-md-10 col-sm-10 col-xs-10"><span class="size-10rem">車型名稱</span></div>
            </div>
            <div class="brand-moto-table-content">
                <ul class="clearfix">
                    <?php $num = 1;?>
                    @foreach($motors as $motor)
                        @php
                            if($motor->displacement == 0){
                                $other_motor = $motor;
                            }
                        @endphp
                        @if($motor->displacement != 0)
                            <li>
                                @if($num != $motor->displacement)
                                    <div class="brand-moto-table-content-left col-md-2 col-sm-2 col-xs-2"><span class="size-10rem">{{$motor->displacement.'cc'}}</span>
                                    </div>
                                @else
                                    <div class="brand-moto-table-content-left col-md-2 col-sm-2 col-xs-2"><span class="size-10rem"></span>
                                    </div>
                                @endif
                                <div class="brand-moto-table-content-right col-md-10 col-sm-10 col-xs-10">
                                    <h3 class="float-left"><a href="{{URL::route('summary',['mt/' . $motor->motorurl_rewrite]) }}">{{$motor->name}}</a></h3>
                                    <h3 class="float-right"><a href="{{URL::route('summary',['mt/' . $motor->motorurl_rewrite]) }}">詳細資訊</a></h3>
                                    <h3 class="float-right join-myBike hidden-xs hidden-sm"><a href="{{ route('customer-mybike-add',[$motor->motorurl_rewrite]) }}" target="_blank">加入MyBike</a></h3>
                                </div>
                            </li>
                            <?php $num = $motor->displacement; ?>
                        @endif
                    @endforeach
                    @if(isset($other_motor))
                        <li>
                            <div class="brand-moto-table-content-left col-md-2 col-sm-2 col-xs-2"><span class="size-10rem"></span></div>
                            <div class="brand-moto-table-content-right col-md-10 col-sm-10 col-xs-10">
                                <h3 class="float-left"><a href="{{URL::route('summary',['mt/' . $other_motor->motorurl_rewrite]) }}">{{$other_motor->name}}</a></h3>
                                <h3 class="float-right"><a href="{{URL::route('summary',['mt/' . $other_motor->motorurl_rewrite]) }}">詳細資訊</a></h3>
                                <h3 class="float-right join-myBike hidden-xs hidden-sm"><a href="{{ route('customer-mybike-add',[$other_motor->motorurl_rewrite]) }}" target="_blank">加入MyBike</a></h3>
                            </div>
                        </li>
                    @endif
                    <li class="no-value" style="display: none;">
                        <div class="text-center"> 
                            <h3>關鍵字錯誤</h3>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('script')
<script>
    $("button.search-motos").click(function(){
        var searchtext = $(".search-motor-text").val().toUpperCase();
        if(!searchtext){
            alert("請輸入關鍵字");
        }else{
            var searchvalue = "";
            $(".moto-list").hide();
            $(".no-value").css("display","none");
            $(".moto-list").each(function(){
                var motoname = $(this).find(".motos a").text();
                if(motoname.indexOf(searchtext) != -1){
                    $(this).find(".brand-moto-table-content-left span.moto-displacement").removeClass("hidden");
                    $(this).find(".brand-moto-table-content-right").addClass("value");
                    searchvalue = motoname;
                    $(this).show();
                }
            });
            if(!searchvalue){
                $(".no-value").css("display","block");
            }
            var y = $('#motors-displacement-list').offset().top - $('nav').height() -20;
            $('html,body').animate({scrollTop: y},800);
        }

    });
    $('button.serach-clear').click(function(){
        $(".moto-list").show();
        $(".brand-moto-table-content-right").removeClass("value");
        $(".brand-moto-table-content-left span.moto-displacement").addClass("hidden");
    });

</script>
@stop