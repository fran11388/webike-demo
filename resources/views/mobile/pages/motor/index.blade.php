@extends('mobile.layouts.mobile')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/motor-index.css') }}">
@stop
@section('middle')
<div class="motors-container">
	<div class="title-box">
        <h1>車型索引</h1>
    </div>
    <div class="ct-brand-selection">
        <!-- ul.ul-ct-brand-selection>li*4>(div.item-product-grid>a>figure>img)+(ul.list-info-brand-selection>li*7>a) -->
        <ul class="ul-ct-brand-selection">
            @foreach($manufacturers as $key => $info)
            	@php
            		switch ($info->country_code)
            		{
						case 'TW':
									$num = '01';
									break;
						case 'JP' :
									$num = '02';
									break;
						case 'DE' :
									$num = '04';
									break;
						case 'IT' :
									$num = '05';
									break;
						default: 
									$num = '01';
									break;				
            		}
            	@endphp
				@if($key < 4)
					<li>
						<div class="item-product-grid link_ui">
							<a class=" link_ui_a toggle clearfix">
								<img src="{{assetRemote('/image/oempart/logo/'.$key.'.jpg')}}">
								<h2>{{$info->name}}</h2>
							</a>
							<div class="brand-menu-list">
								<ul class="list-info-brand-selection">
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,50]) }}">- 50cc</a></li>
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,125]) }}">51 -125cc</a></li>
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,250]) }}">126 - 250cc</a></li>
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,400]) }}">251 - 400cc</a></li>
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,750]) }}">401 - 750cc</a></li>
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,1000]) }}">751 -1000cc</a></li>
									<li><a href="{{URL::route('motor-manufacturer',[$info->name,1001]) }}">1001cc -</a></li>
								</ul>
							</div>
						</div>
					</li>
				@else
					<li>
						<div class="link_ui no-after">
							<a class=" link_ui_a toggle clearfix" href="{{URL::route('motor-manufacturer',[$info->name,'00'])}}">
								<img src="{{assetRemote('/image/oempart/logo/'.$key.'.jpg')}}">
								<h2>{{$info->name}}</h2>
							</a>
						</div>
					<li>
				@endif
            @endforeach
        </ul>
    </div>
    <div id="myNavbar" class="box-part-number">
        <div class="title-main-box">
            <h2>品牌國籍</h2>
        </div>
        <?php $num = 1;?>
        <div class="ct-brand-nationality">
            @foreach($motors as $country => $information)
                <?php
                    if($num < 10){
                        $idnum = '0'.$num;    
                    }else{
                        $idnum ="$num";
                    }
                ?>
                <div id="menu-brand-{{$idnum}}" class="item-brand-nationality link_ui">
                    <a class="title-item-brand-nationality  link_ui_a toggle clearfix">
                        <img src="{{assetRemote('/image/oempart/icon-flag'.$idnum.'.jpg')}}" alt="">
                        <h2>{{$country}}</h2>
                    </a>
					<div class="brand-menu-list">
	                    <ul class="ct-item-brand-nationality">
	                        @foreach($information as $manufacturer)
	                            <?php $url_rewrite = $manufacturer->url_rewrite; ?>
	                            <li>
	                            	<a href="{{URL::route('motor-manufacturer',[$url_rewrite,'00']) }}">
	                            		{{$manufacturer->name}}
	                            	</a>
	                            </li>
	                        @endforeach
	                    </ul>
                	</div>
                </div>
                <?php $num++; ?>
            @endforeach  
        </div>
    </div>
</div>
@stop
@section('script')
@stop