@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/motor-review.css') }}">
<link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">
@stop
@section('middle')
	<h1 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h1>
	@include('mobile.pages.summary.partials.content-tabs')
	<div class="section">
		<h3 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h3>
		<div class="mybike_log">
		    @include('mobile.pages.motor.partials.info')
		    @include('mobile.pages.motor.partials.analysis')
		</div>
	</div>
	<div class="section">
		<div>
			<h3 class="common">商品評論</h3>
		</div>
		<div class="new-review">
			<ul class="owl-carousel-4"> 
				 @foreach ($reviews as $review)
	                <?php $href = route('review-show', $review->id) ?>
	                <li class=" item item-product-grid item-product-custom-part-on-sale">
	                    <div>
	                        <div>
	                            <a href="{{ $href }}">
	                                <figure class="zoom-image thumb-box-border">
	                                    <?php
	                                    $src = '';
	                                    if (starts_with($review->photo_key, 'http'))
	                                        $src = $review->photo_key;
	                                    else
	                                        $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
	                                    ?>

	                                    <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
	                                </figure>
	                            </a>
	                        </div>
	                        <div class="item-product-grid-pars-on-sale-right text-left">
	                            <span class="dotted-text1">{{ $review->product_manufacturer }}</span>
	                            <a href="{{ $href }}" class="title-product-item-top dotted-text2" title="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
	                                <span class="dotted-text2">{{ $review->product_name }}</span>
	                            </a>
	                            <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
	                        </div>
	                    </div>
	                    <div class="new-review-content text-left">
	                        <a href="{{ $href }}" class="title-product-item dotted-text2">{{ $review->title }}</a>
	                        <label class="normal-text dotted-text2">{!! mb_substr(e($review->content),0,35,"utf-8") !!}
	                        	<a href="{{ $href }}" class="btn-customer-review-read-more">...More</a>
	                        </label>
	                    </div>
	                </li>
	            @endforeach
			</ul>
		</div>	
	</div>
@stop
@section('script')
    <script>
        @if($current_motor)
	        function addMyBike()
	        {
	            $('.add-my-bikes-btn').addClass('hide');
	            $('.add-my-bikes-disabled').removeClass('hide');
	            $.ajax({
	                type: 'GET',
	                url: "{!! route('customer-mybike-add', $current_motor->url_rewrite) !!}",
	                data: {},
	                dataType: 'json',
	                success: function(result){
	                    if(result.success){
	                        var ajax_html = '<span class="size-10rem">{!! $current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') !!} 已經加入MyBike清單。若要移除或新增車型，請至<a href="{!! route('customer-mybike') !!}" target="_blank">會員中心修正</a>。</span><br><br>';
	                        if(result.messages.length){
	                            ajax_html += result.messages.join('<br>');
	                        }
	                        swal({
	                            title: "成功加入Mybike",
	                            html:  '<div class="text-left">' + ajax_html + '</div>',
	                            type: "success",
	                            confirmButtonText: "確定"
	                        });
	                    }

	                }
	            });
	        }
        @endif
    </script>
@stop