@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/product-detail.css') }}">
    <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/product-relieved-shopping.css') !!}">
    <!-- <link type="text/css" rel="stylesheet" href="{!! asset('plugin/lightslider/src/css/lightslider.css') !!}" /> -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css"/>
    <!-- <link type="text/css" rel="stylesheet" href="{!! asset('plugin/lightGallery/src/css/lightgallery.css') !!}" /> -->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css"/>
@stop

<style>

    body.swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown){
        overflow-y: visible !important;
    }
    .estimate_time {
        color: #999999;
        font-weight: normal;
    }

</style>

@section('middle')
<div class="product-detail-cart-block clearfix col-xs-12">
    <div class="product-detail-add-to-wishlist col-xs-2 submit-disabled loading-stock" id="add_to_wishlist">
        @if($WishlistItem)
            <div class="product-detail-add-to-wishlist-icon-y delete_wishlist">
                <i class="fa fa-star size-15rem"></i>
                <span class="hidden protect_code">{{$WishlistItem->protect_code}}</span>
            </div>
        @else
            <div class="product-detail-add-to-wishlist-icon-n add_wishlist">
                <i class="fa fa-star-o size-15rem"></i>
            </div>
        @endif
    </div>
    <div class="product-detail-service col-xs-2">
        <a href="{{URL::route('customer-service-proposal-create', 'service') . '?skus=' . $product->url_rewrite}}">
            <i class="far fa-comments size-15rem"></i>
        </a>
    </div>
    <div class="product-detail-add-to-cart col-xs-8 size-12rem buy_btn submit-disabled loading-stock">
        <i class="fas fa-cart-plus"></i>
        <span>加入購物車</span>
    </div>
</div>
<form method="post" action="{{ route('cart-update') }}" id="acb">
    <div class="product-add-cart clearfix">
        <div class="product-add-cart-container clearfix">
            <div class="close-cart-btn-block size-15rem clearfix">
                <i class="fa fa-remove close-cart-btn"></i>
            </div>

            <div class="product-add-cart-block clearfix">
                <div class="product-add-cart-select-block clearfix">
                </div>
                <div class="col-xs-12 determine-number flex-box">
                    <input type="hidden" name="sku" id="sku" class="sku" value="{{ $product->group_code ? '' : $product->sku }}"/>
                    <label class="btn-label label-large text-nowrap">數量</label>
                    <ul class="btn btn-large">
                        <li><a href="javascript:void(0)" class="count-button minusQty size-19rem font-color-red" {{ in_array($product->type,[\Everglory\Constants\ProductType::OUTLET,\Everglory\Constants\ProductType::GROUPBUY]) ? '' : 'onclick=minusQty()' }}>-</a></li>
                        <li><input type="text" name="qty" id="qty" value="1" readonly></li>
                        <li><a href="javascript:void(0)" class="count-button addQty size-19rem font-color-red" {{ in_array($product->type,[\Everglory\Constants\ProductType::OUTLET,\Everglory\Constants\ProductType::GROUPBUY]) ? '' : 'onclick=addQty()' }} >+</a></li>
                    </ul>
                </div>
                @if(in_array($product->type,[\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::NORMAL]))
                    <div class="groupbuy-product-block col-xs-12">
                        <a id="groupbuy-product" class="size-08rem" href="javascript:void(0)" >購買10件以上優惠 >></a>
                    </div>
                @endif
                <div class="col-xs-12 btn-addcart">
                    <a id="add_to_cart" class="btn bg-color-red size-10rem buy_btn submit-disabled " href="javascript:void(0)">加入購物車</a>
                </div>
            </div>
        </div>
        <div class="product-add-cart-container-background hidden"></div>
    </div>
</form>
<div class="clearfix">
    <div class="product-detail">

        @include('mobile.pages.product-detail.partials.info')
        @include('mobile.pages.product-detail.partials.detail')
        @include('mobile.pages.product-detail.partials.info-detail')
        @include('mobile.pages.product-detail.partials.product-detail-ranking')
        @include('mobile.pages.product-detail.partials.recommend')
    </div>
</div>

@stop

@section('script')
    <!-- <script async src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/PhotoSwipe-4.1.2/photoswipe-ui-default.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe-ui-default.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/elevatezoom-master/jquery.elevateZoom-3.0.8.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/elevatezoom/3.0.8/jquery.elevatezoom.min.js"></script>

    <!-- <script src="{!! assetRemote('plugin/lightslider/src/js/lightslider.js') !!}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <!-- <script src="{!! assetRemote('plugin/lightGallery/src/js/lightgallery.min.js') !!}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/js/lightgallery.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/lightGallery/src/js/lg-zoom.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/lg-zoom/1.1.0/lg-zoom.min.js"></script>
    <!-- <script async src="{!! assetRemote('plugin/lightGallery/src/js/lg-thumbnail.min.js') !!}"></script> -->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/lg-thumbnail/1.1.0/lg-thumbnail.min.js"></script>



    <script src="//translate.google.com/translate_a/element.js?cb=googleSectionalElementInit&ug=section&hl=zh-TW"></script>
    <script src="{!! assetRemote('plugin/google/translator.js') !!}"></script>

    @include('mobile.pages.product-detail.partials.page-script')

    <script type="text/javascript">
        _paq.push(['setEcommerceView', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$category_names) !!}"],{!! round($product->getFinalPrice($current_customer)) !!}]);

        $('.lSSlideWrapper #imageGallery li.active').click(function(){
            $('nav.fixed').css('display','none');
        });
        $('.lg-toolbar .lg-close').click(function(){
            $('nav.fixed').css('display','block');
        });

    </script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Product",
        "name": "{{$product->name}}",
        "image": "{{$product->getThumbnail()}}",
        "url": "{{URL::route('product-detail', $product->url_rewrite)}}",
        "description": "
            @if( $product->type == Everglory\Constants\ProductType::OUTLET and $outlet = $product->outlet )
                @if($outlet->description)
                    商品狀況：{{ $outlet->description }}。
                @else
                    商品狀況：庫存新品。
                @endif
                @if(count($product->options))
                    @foreach ($product->options as &$option)
                        {{ $option->label->name.'：'.$option->name }}
                    @endforeach
                @endif
            @endif
            @if($product->productDescription)
                {{ str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $product->productDescription->summary) }}
                {!! str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $product->productDescription->sentence)  !!}
            @endif
        ",
        "mpn": "{{$product->model_number}}",
        "brand": {
            "@type": "Thing",
            "name": "{{$product->manufacturer->name}}"
        },
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT) }}",
            "reviewCount": "{{ count($reviews_info) }}"
        },
        "offers": {
            "@type": "Offer",
            "priceCurrency": "TWD",
            "price": "{!! $product->getNormalPrice() !!}",
            "priceValidUntil": "{!! date('Y-m-d') !!}",
            "itemCondition": "http://schema.org/UsedCondition",
            "availability": "http://schema.org/InStock",
            "seller": {
                "@type": "Organization",
                "name": "「Webike-摩托百貨」"
            }
        }
    }
    </script>
@stop
