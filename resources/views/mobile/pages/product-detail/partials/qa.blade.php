@if(count($answers))
<div id="customer-qa" class="customer-qa">
    <div class="title-main-box title-detail-info link_ui">
        <a id="customer-q-a" class="title-content-detail-info link_ui_a toggle">客戶問與答</a>
        <div>      
            <ul class="ul-ct-customer-qa">
                @php
                    $qa_num = 0;
                @endphp
                @foreach($answers as $key => $answer)
                    @php
                        $question = $answer->question;
                        $qa_num++;
                    @endphp
                    <li class="li-ct-customer-qa">
                        <!-- Customer Q & A Show -->
                        <div class="customer-qa-show {!! $qa_num > 3 ? 'hide' : '' !!} box">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="ct-customer-qa-show">
                                        <h3 class="title"><i class="fa fa-comment-o size-12rem font-color-blue"></i> {!! $qa_num . '.' . $question->content !!}</h3>
                                        <div class="ct-customer-qa-show-detail"><i class="fa fa-check size-12rem font-color-green"></i> {!! echoWithLinkTag($answer->content) !!}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
                @if(count($answers) > 3)
                    <li class="text-right">
                        <a id="customer-qa-show-all" class="btn-detail-info no-rcj-a" href="javascript:void(0)">
                            查看全部
                            <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </li>
                @endif
            </ul>
        </div>
    </div> 
</div>
@endif