@foreach ($reviews_info->forPage($reviews_page,$review_per_page) as $review)
    <li class="content-comment-detail row">
        <div class="newcomments-photo">
            <a href="{{route('review-show',$review->id)}}">
                <figure class="zoom-image thumb-box-border">
                    <img src="{!! $review->photo_key ? '//img.webike.tw/review/'.$review->photo_key : $review->product_thumbnail !!}"
                     alt="{!! $review->product_name !!} {!!  $review->product_manufacturer !!}"/>
                </figure>
            </a>
        </div>
        <div class="ct-comment-detail">
            <div class="newcomments-text">
                <div class="title-review-comment">
                    <a href="{{route('review-show',$review->id)}}" class="title dotted-text2 no-rcj-a review-bolck">{{ $review->title }}</a>
                    <span class="review-bolck">{{ date("Y/m/d", strtotime($review->created_at)) }}</span>
                </div>
                <div class="star-name">
                    <span class="icon-star-bigger star-0{{ $review->ranking }} review-bolck"></span>
                    <div class="time">
                        <p class="review-bolck">來自 {{ $review->nick_name }}</p> 
                        <p class="review-bolck"> 於 {{ date("Y/m/d", strtotime($review->created_at)) }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="newcomments-content">
            <span class="dotted-text2">
                {{ $review->nick_name }}：{!! mb_substr($review->content,0,30,"utf-8") !!}
                <a href="{{route('review-show',$review->id)}}">
                    ...More
                </a>    
            </span>
        </div>
    </li>
@endforeach
