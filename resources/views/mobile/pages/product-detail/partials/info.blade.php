<div class="module-title">
    <div class="product-info-block clearfix">       
{{--         <div class="brand-logo">
            <a class="img-thumbnail thumbnail-logo-brand" href="{{ URL::route('summary', 'br/' . $product->manufacturer->url_rewrite) }}" title="{{$product->manufacturer->name . $tail}}">
                <img src="{{ $product->manufacturer->image }}" alt="{{$product->manufacturer->name . $tail}}"/>
            </a>
        </div> --}}
        <div class="product-info">
            <ul class="info-brand">
                <div class="name-brand-block">    
                    <li>
                        <h2>
                            <span class="name-brand">
                                <a href="{{ URL::route('summary', 'br/' . $product->manufacturer->url_rewrite) }}" title="{{$product->manufacturer->name . $tail}}">
                                    {{ $product->manufacturer->name }}
                                </a>
                            </span>
                        </h2>
                    </li>
                </div> 

{{--                 <div class="star-review-block">   
                    <li>
                        <span class="icon-star star-{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT)}}"></span>
                    </li>
                    <li>
                        <a class="review" href="{{ URL::route('review-search', '') }}?sku={{$product->url_rewrite}}">共{{ count($reviews_info) }}則評論</a>
                    </li>
                </div>  --}}   

            </ul>
        </div>
    </div>

    <h1 class="product-detail-MainTitle">{{ $product->name }}</h1>

        <div class="top-info-detail">
            <h3 class="common-product-number">商品編號：</h3>
        <div id="model-number" >{{ $product->model_number }}</div>
</div>
</div>