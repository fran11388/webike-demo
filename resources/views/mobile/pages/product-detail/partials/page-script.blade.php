<script type="text/javascript">

     var galleryLightSlider;
     var $lg;
     var galleryLightSliderSetting = {
         gallery:false,
         item:1,
         loop:false,
         thumbItem:7,
         slideMargin:0,
         adaptiveHeight:true,
         enableDrag: false,

         onSliderLoad: function(el) {
             $lg = el.lightGallery({
                 selector: '#imageGallery .lslide',
                 thumbnail:true
             });

             $('.img-current').text(el.getCurrentSlideCount());
             $('.img-total').text(el.getTotalSlideCount());
         },
         onAfterSlide:function(el){
             $('.img-current').text(el.getCurrentSlideCount());
         },

         onBeforeSlide:function(el){
             $('.img-current').text(el.getCurrentSlideCount());
         },
         responsive : [
             {
                 breakpoint:921,
                 settings: {
                     thumbItem:5,
//                        item:2,
//                        slideMove:1
                 }
             },
             {
                 breakpoint:767,
                 settings: {
                     thumbItem:3,
//                        item:2,
//                        slideMove:1
                 }
             }
         ]

     };

     galleryLightSlider = $('#imageGallery').lightSlider(galleryLightSliderSetting);

    $(document).ready(function(){
        var parent_width = $(window).width() * 0.6;
        if($('.container-box').length > 0){
            parent_width = 0;
            $('.container-box').each(function(key, item){
                if($(item).width() > parent_width){
                    parent_width = $(item).width();
                }
            });
        }
        var iframe = $('.box-detail-info iframe');
        iframe.width(parent_width * 0.9);
        iframe.height(iframe.width() * 0.5625);
        iframe.css({'display':'block', 'margin-left':'auto','margin-right':'auto'});

        recoverOptionBorder();
        $('[data-toggle="tooltip"]').tooltip();
//        galleryLightSlider = $('#imageGallery').lightSlider(galleryLightSliderSetting);

        $(document).on('click', '.question-title', function(){
            var tag = $(this).parent().find('i.fa-question-circle');
            var content = $(this).parents('li').find('.customer-qa-show');
            if($(tag).length > 0){
                $(tag).removeClass('fa-question-circle').addClass('fa-toggle-down');
                $(content).addClass('show');
            }else{
                tag = $(this).parent().find('i.fa-toggle-down');
                $(tag).removeClass('fa-toggle-down').addClass('fa-question-circle');
                $(content).removeClass('show');
            }
        });

        // getAlsoBuy();
    });

    function recoverOptionBorder(){
        var target = $('#product-options .dropdown-toggle');
        var option_index = target.find('.product-select-text').attr('data-selected-option-index');
        if(typeof option_index != 'undefined'){
            if(option_index.length <= 0){
                target.addClass('has-error');
            }else{
                target.removeClass('has-error');
            }
        }
    }

    setQty(1);
    function setQty(qty) {
        var $el = $("#qty");
        $el.val(qty);
    }
    function addQty() {
        var $el = $("#qty");
        $el.val(parseInt($el.val()) + 1);
    }
    function minusQty() {
        var $el = $("#qty");
        $el.val(parseInt($el.val()) - 1);
        selectedQty();
    }
    function selectedQty() {
        var $el = $("#qty");
        var value = $el.val();
        if (value != parseInt(value) || value <= 0) {
            setQty(1);
        } else {
            return value;
        }
    }

    //product information and group
    var initial_loading = true;
    var group_flg = false;
    @if($product->group_code)
        <?php $selects = $product->getGroupSelectsAndOptions(); ?>
        group_flg = true;
        $(".product-select-option").click(function () {
            $(this).closest('.dropdown').find('.product-select-text').text($(this).text())
                    .attr("data-selected-option-index", $(this).attr('data-option-index'))
                    .attr("data-selected-option-value", $(this).attr('data-option-value'));
            $('.estimate_request').addClass('disabled');
            $('.estimate_static').addClass('disabled');
            $('.buy_btn').addClass('submit-disabled').addClass('disabled');

            getProductInfo();
        });
    @endif

    function importInstallHtml(installments){
        $("#installment-table").find('.history-table-content').html('');
        $.each(installments, function(key, object){
            $("#installment-table").find('.history-table-content').append(object.html);
        });
        $("#installment-table").removeClass('hide');
    }

    function initialInstallment(){

        $.get("./info?sku=" + '{{ $product->url_rewrite }}', function (result) {
            if(result.installments.length){
                importInstallHtml(result.installments);
            }
        });
    }
     initialInstallment();

    function getProductInfo() {
        var $options = $(".product-select-text").filter((function () {
            recoverOptionBorder();
            return $(this).attr("data-selected-option-index") != "";
        }));
        if( initial_loading || (group_flg && $options.length == $(".select-info .dropdown-toggle").length) ){
            if(!initial_loading){
                $('.select-info .loading-box').show();
            }
            var marge_name = $options.map(function () {
                return $(this).attr('data-selected-option-value');
            }).get().join('|');
            $.get("./info?sku=" + '{{ $product->url_rewrite }}' + "&na=" + encodeURIComponent(marge_name) + "&gc=" + {{ $product->group_code }} + "&mi=" + {{ $product->manufacturer_id }}, function (result) {
                if (result.sku) {

                    if(result.tags){
                        $('#tag_area').empty();
                        $.each(result.tags, function(index, value) {
                            $('#tag_area').append('<span class="'+value+'">'+index+'</span>\n');
                        });
                    }
                    @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]))
                        $('.tag.label.label-purple').hide();
                    @endif

                    if((initial_loading && !group_flg) || !initial_loading){
                        $(".delivery-message").html(result.stock.stock_info);


                        if(result.stock.stock){
                                $('.select-address').removeClass('hide');
                                $('.select-address').attr('data',result.stock.ship_day);
                                shipping_day = $('.select-info .select-area').find('option:selected').val();
                                
                                getShippingInfo(shipping_day,result.stock.ship_day);

                        }else{
                            $('.select-address').addClass('hide');
                            $('.select-address').attr('data','');
                        }

                        if(result.information.estimate_flg){
                            $('.estimate_request').show('fast');
                            $('.estimate_request').removeClass('disabled');
                            $('.estimate_request.estimate').removeClass('none-stock');
                            $('.estimate_request.estimate').removeClass('submit-disabled');
                            $('label.estimate_request.tip').removeClass('hide').addClass('show');
                        }else{
                            $('.estimate_request').show('fast');
                            $('.estimate_request').addClass('disabled');
                            $('.estimate_request.estimate').addClass('none-stock');
                            $('.estimate_request.estimate').addClass('submit-disabled');
                            $('label.estimate_request.tip').removeClass('show').addClass('hide');
                        }

                        if(result.information.buy_flg){
                            $('.buy_btn').removeClass('submit-disabled').removeClass('disabled');
                            $('#add_to_wishlist').removeClass('submit-disabled');
                            $('.product-detail-cart-block').removeClass('no-click');
                            $('#add_to_wishlist').removeClass('none-stock');
                            $('.add_wishlist').removeClass('none-stock');
                        }else{
                            $('#add_to_cart').addClass('none-stock');
                            $('#add_to_wishlist').addClass('submit-disabled');
                            $('.add_wishlist').addClass('none-stock');
                            $('.product-detail-add-to-cart').removeClass('loading-stock');
                            $('.product-detail-cart-block').addClass('no-click');
                            $('.product-detail-add-to-cart').addClass('none-stock');
                            $('#add_to_wishlist').addClass('none-stock');
                            $('#groupbuy-product').addClass('none-stock');
                        }

                        if(result.stock.stock){
                            $('.estimate_request.estimate').addClass('in-stock');
                            $('.estimate_request.estimate').addClass('submit-disabled');
                            $('.estimate_request.estimate').removeClass('none-stock');
                        }

                        if(result.thumbnails.length){
                            galleryLightSlider.destroy();
//                            $lg.data('lightGallery').destroy(true);
                            $('#imageGallery').html('');
                            $.each(result.thumbnails, function(key, thumbnail_url){
                                var li = '<li class="img-thumbnail thumbnail-img-product img-zoom-in" data-thumb="' + thumbnail_url + '" data-src="' + result.images[key] + '">' +
                                        '<img src="' + result.images[key] + '" alt="' + result.full_name + '" title="' + result.full_name + '" />' +
                                        '</li>';
                                $('#imageGallery').append(li);
                            });
                            galleryLightSlider = $('#imageGallery').lightSlider(galleryLightSliderSetting);
                        }
                    }

                    if(!initial_loading){
                        $("#model-number").html(result.model_number);
                        $("#final-price").html(result.final_price);
                        $("#base-price").html(result.base_price);
                        $(".sku").val(result.sku);
                        @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]))
                            $("#normal-price").html(result.normal_price);
                        @endif
                        $("#final-point").html(result.final_point);

                        if(result.installments.length){
                            importInstallHtml(result.installments);
                        }

                        window.history.pushState("", "", result.url);
                        CheckWishlistItem(result.product_id);
                        $('.select-info .loading-box').hide();
                        if(result.information.buy_flg) {
                            $('#add_to_cart').removeClass('disabled').removeAttr('disabled');
                        }
                    }
                    $('#add_to_cart').removeClass('loading-stock');
                    $('.product-detail-add-to-cart').removeClass('loading-stock');
                    $('#add_to_wishlist').removeClass('loading-stock');
                    $('#groupbuy-product').removeClass('loading-stock');
                    initial_loading = false;
                }else{
                    swal(
                        '發生不明錯誤',
                        '請重新再試',
                        'error'
                    );
                    $('.select-info .loading-box').hide();
                    $('#add_to_cart').removeClass('disabled').removeAttr('disabled');
                    $('#add_to_cart').removeClass('loading-stock');
                    $('.product-detail-add-to-cart').removeClass('loading-stock');
                    $('#add_to_wishlist').removeClass('loading-stock');
                    $('#groupbuy-product').removeClass('loading-stock');
                }


            });

        }


    }
    getProductInfo();

    <?php
        $selectOptions = $product->getGroupSelectsAndOptions();
    ?>
    @if ($product->group_code and ( count($selectOptions) == 1 ))
        query_skus = $.map($('.product-select-option') ,function(option) {
            return $(option).data('product-sku');
        }).join(',');
        if(query_skus){
            $.ajax({
                type: 'GET',
//                url: 'http://www.webike.net/wbs/stock-cooperation-sd-json.html',
                url: "{{route('api-stock')}}",
                dataType: 'json',
                data: "sku=" + query_skus,
                callback: 'stock',
                success: function(json){
//                    console.log('success');
                    $.each($('.product-select-option'), function(key, option) {
                        if($(option).data('product-sku')){
                            stock_result = false;
                            $.each(json, function(i, element) {
                                if (element.sku == $(option).data('product-sku')) {
//                                    console.log(element);
                                    if(element.stock > 0 && element.is_maker_stock){
//                                        $(option).text($(option).text()  + "【廠商在庫:" + element.stock + "個】" );
                                        $(option).text($(option).text()  + "【廠商在庫】" );
                                    }else if(element.stock > 0 && !element.is_maker_stock){
                                        $(option).text($(option).text()  + "【在庫:" + element.stock + "個】" );
                                    }else if(element.sold_out){
                                        $(option).text($(option).text()  + "※售完" );
                                    }else if(element.discontinued){
                                        $(option).text($(option).text()  + "※停產" );
                                    }
                                    stock_result = true;
                                }
                            });
                            if(!stock_result){
                                $(option).text($(option).text()  + "※售完" );
                            }
                        }
                    });
                }
            });
        }
    @endif

    function CheckWishlistItem(product_id){
        var token = $('meta[name=csrf-token]').attr('content');
        $.ajax({
            url: "{!! route('product-CheckWishlistItem') !!}",
            data:{'_token':token ,'product_id':product_id},
            type:"POST",
            dataType:'JSON',
            success:function(result){
                if(result){
                    $('#add_to_wishlist div.add_wishlist').removeClass('add_wishlist');
                    $('#add_to_wishlist div.product-detail-add-to-wishlist-icon-n').removeClass('product-detail-add-to-wishlist-icon-n');
                    $('#add_to_wishlist div i.fa').removeClass('fa-star-o');
                    $('#add_to_wishlist div').addClass('delete_wishlist');
                    $('#add_to_wishlist div').addClass('product-detail-add-to-wishlist-icon-y');
                    $('#add_to_wishlist i.fa').addClass('fa-star');
                    $('#add_to_wishlist div').append('<span class="hidden protect_code">' + result['protect_code'] + '</span>');
                }else{
                    $('#add_to_wishlist div.delete_wishlist').removeClass('delete_wishlist');
                    $('#add_to_wishlist div.product-detail-add-to-wishlist-icon-y').removeClass('product-detail-add-to-wishlist-icon-y');
                    $('#add_to_wishlist div i.fa').removeClass('fa-star');
                    $('#add_to_wishlist div').addClass('add_wishlist');
                    $('#add_to_wishlist div').addClass('product-detail-add-to-wishlist-icon-n');
                    $('#add_to_wishlist i.fa').addClass('fa-star-o');
                    $('#add_to_wishlist div span.protect_code').remove('span');
                }
            },
            error:function(xhr,ajaxOptions,thrownError,textStatus){

            }
        });
 
    }

    function openLicenses(){
        window.open("{{URL::route('motor-licenses-info')}}", "什麼是機號? - 「Webike-摩托百貨」", "width=800, height=600");
    }

    function submitGroupBuy(){
        var href = "{{URL::route('groupbuy')}}";
        var sku = $('#sku').val() ? $('#sku').val() : '{{$product->url_rewrite}}';
        window.open(href + '?skus=' + sku);
    }

    function testSelectOption(element, url, description) {
        url = (url === undefined) ? null : url;
        if(!description || description === undefined){
            description =  "您尚未選擇商品選項";
        }
        if(element.hasClass('loading-stock')){
            description = "讀取中請稍後...";
        }else if(element.hasClass('none-stock')){
            description = "商品已售完";
        }else if(element.hasClass('in-stock')){
            description = "商品在庫，可直接下定";
        }

        var productOptions = $('#product-options');
        var disabled = element.hasClass('submit-disabled');
        productOptions .removeClass('has-error');
        if(disabled){
            productOptions .addClass('has-error');
            swal('錯誤', description, 'warning');
            $('html,body').animate({scrollTop:$('.price-product-deatil').offset().top}, "slow");
            return false;
        }else{
            if(url){
                submitForm(element, url);
            }
            return true;
        }
    }

    $('.estimate_request.estimate').click(function(){
        testSelectOption($(this), "{{ route('customer-estimate-update') }}");
    });


    $('#add_to_cart').click(function(){
        testSelectOption($(this), "{{ route('cart-update') }}");
        fbq('track', 'AddToCart', {
            content_type: 'product', //either 'product' or 'product_group'
            content_ids: ["{{$product->id}}"], //array of one or more product ids in the page
            value: {{intval($product->getFinalPrice($current_customer))}}, //OPTIONAL, but highly recommended
            currency: 'TWD' //REQUIRED if you a pass value
        });
        ga('send', {
            hitType: 'event',
            eventCategory: 'cart',
            eventAction: 'buttom_click',
            eventLabel: 'Add To Cart'
        });

    });



    /*$('#add_to_wishlist').click(function() {
        fbq('track', 'AddToWishlist', {
            content_type: 'product', //either 'product' or 'product_group'
            content_ids: ['{{$product->id}}'], //array of one or more product ids in the page
            value: {{intval($product->getFinalPrice($current_customer))}},  //OPTIONAL, but highly recommended
            currency: 'TWD' //REQUIRED if you a pass value
        });
        testSelectOption($(this), "{{ route('customer-wishlist-update') }}"
                {!! in_array($product->type, [\Everglory\Constants\ProductType::NORMAL, \Everglory\Constants\ProductType::OUTLET]) ? ', "您尚未選擇商品選項"' : ', "正廠零件、團購、未登錄商品無法加入待購清單。"' !!});
    });*/

    $('#add_to_wishlist').click(function() {
        @if(!$current_customer)
              swal('錯誤', '請登入會員', 'warning');
              return false;     
        @endif
       var SelectOption = testSelectOption($(this),null , {!! in_array($product->type, [\Everglory\Constants\ProductType::NORMAL, \Everglory\Constants\ProductType::OUTLET]) ? ' "您尚未選擇商品選項"' : '"正廠零件、團購、未登錄商品無法加入待購清單。"' !!});
       if(SelectOption){
            var token = $('meta[name=csrf-token]').attr('content');
            if($('#add_to_wishlist div').hasClass('delete_wishlist')){
                var protect_code = $('#add_to_wishlist div.delete_wishlist span.protect_code').text();
                $.ajax({
                    url: "{!! route('product-delete-wishlist') !!}",
                    data:{'_token':token ,'protect_code':protect_code},
                    type:"POST",
                    dataType:'JSON',
                    success:function(result){
                        $('#add_to_wishlist div.delete_wishlist').removeClass('delete_wishlist');
                        $('#add_to_wishlist div.product-detail-add-to-wishlist-icon-y').removeClass('product-detail-add-to-wishlist-icon-y');
                        $('#add_to_wishlist div i.fa').removeClass('fa-star');
                        $('#add_to_wishlist div').addClass('add_wishlist');
                        $('#add_to_wishlist div').addClass('product-detail-add-to-wishlist-icon-n');
                        $('#add_to_wishlist i.fa').addClass('fa-star-o');
                        $('#add_to_wishlist div span.protect_code').remove('span');
                    swal({
                          title: '完成',
                          html: '已從<a href ={{route("customer-wishlist")}} target="_blank">待購清單</a>移除',
                          type: 'success',
                          confirmButtonClass: 'confirm-ok',
                          allowOutsideClick: false
                        });
                    },
                    error:function(xhr,ajaxOptions,thrownError,textStatus){

                    }
                });
            }else{
                var product_sku = $(".product-add-cart .sku").val();;
                $.ajax({
                        url: "{!! route('product-insert-wishlist') !!}",
                        data:{'_token':token ,'sku':product_sku},
                        type:"POST",
                        dataType:'JSON',
                        success:function(result){
                            $('#add_to_wishlist div.add_wishlist').removeClass('add_wishlist');
                            $('#add_to_wishlist div.product-detail-add-to-wishlist-icon-n').removeClass('product-detail-add-to-wishlist-icon-n');
                            $('#add_to_wishlist div i.fa').removeClass('fa-star-o');
                            $('#add_to_wishlist div').addClass('delete_wishlist');
                            $('#add_to_wishlist div').addClass('product-detail-add-to-wishlist-icon-y');
                            $('#add_to_wishlist i.fa').addClass('fa-star');
                            $('#add_to_wishlist div').append('<span class="hidden protect_code">' + result['item']['protect_code'] + '</span>');
                            swal({
                                  title: '完成',
                                  html: '已加入<a href ={{route("customer-wishlist")}} target="_blank">待購清單</a>',
                                  type: 'success',
                                  confirmButtonClass: 'confirm-ok',
                                  allowOutsideClick: false
                                });
                        },
                        error:function(xhr,ajaxOptions,thrownError,textStatus){

                        }
                });
            }
        }
    });

    $('#groupbuy-product').click(function() {
        if(testSelectOption($(this))){
            submitGroupBuy();
        }
    });

    $('.licenses-info').click(function(){
        openLicenses();
    });

    $('.translate-pin').click(function() {

        if($(this).hasClass('orig_text')){

            $('.description_trans').show();
            $('.description_orig').hide();
            $(this).text('顯示原文');
        }else{
            $('.description_trans').hide();
            $('.description_orig').show();
            $(this).text('翻譯');
        }

        $(this).toggleClass('orig_text');

        setTimeout(function(){
            var DetailHeight = $('.box-detail-info').height();


            if ($('.animate-btn-down').hasClass('hidden')) {

                $('.left-box-detail').css("height", DetailHeight);

            }else {

                $('.left-box-detail').css("height", 300);
            }
            ChangHeight();
        },100);

    });

//     $('.translate-pin').click(function(){
//         for (var i = 0; i < $('a.goog-te-gadget-link').length; i++) {
//             $('a.goog-te-gadget-link')[i].click();
//         };
//     });

    $(document).on("click",".review-sort-option",function(e) {
        $('#review-select-text').text($(this).text())
            .data("selected-option-value", $(this).data('option-value'));
        page = parseInt($('#review-pagination > li > a.active').text());
        reload_review(page,$(this).data('option-value'));
    });

    $(document).on("click","#review-pagination > li",function(e) {
        e.preventDefault();
        currnet_page = $('#review-pagination > li > a.active');
        var page;
        if($(this).find('i').length > 0){
            icon = $(this).find('i');
            if(icon.hasClass('fa-caret-left')){
                target_li = currnet_page.closest('li').prev('li');
            }else if(icon.hasClass('fa-caret-right')) {
                target_li = currnet_page.closest('li').next('li');
            }

        }else{
            target_li = $(this);
        }
        if(target_li.text() != ''){
            page = parseInt(target_li.text());
        }


        if(page){
            currnet_page.removeClass('active');
            target_li.find('a').addClass('active');
            sort = $('#review-select-text').data('selected-option-value');
            reload_review(page,sort);

        }

        return false;

    });
    function reload_review(page,sort) {
        slipTo('#anchor_customer_review');
        $('#review-list').fadeOut("slow").html('');
        $.ajax({
            url: "{{route('product-detail-review',$product->url_rewrite)}}",
            data: {page:page,sort:sort},
            type:"POST",
            success: function(data){
                data = JSON.parse(data);
                $('#review-list').fadeIn("slow").html(data.html);
            },

            error:function(xhr, ajaxOptions, thrownError){
                button.removeClass('disabled');
                swal(
                        'Oops...',
                        '發生例外錯誤!',
                        'error'
                )
            }
        });
    }
    function qaPraise(btn){
        $(btn).addClass('disabled').prop('disabled', true);
        $.ajax({
            url: "{{route('product-detail-qa-praise',$product->url_rewrite)}}",
            data: {answer_id:$(btn).attr('data-value')},
            type:"POST",
            success: function(data){
                data = JSON.parse(data);
                if(data.success){
                    $(btn).find('span.count').text('(' + data.count + ')');
                    $(btn).closest('.like-comment').find('.btn-label').remove();
                }
            },

            error:function(xhr, ajaxOptions, thrownError){
                $(btn).removeClass('disabled').removeAttrs('disabled');
                swal(
                        'Oops...',
                        '發生例外錯誤!',
                        'error'
                )
            }
        });
    }
    $('.answer-praise').click(function(){
        qaPraise($(this));
    });

    var answer_ids = $('.answer-praise').map(function() {
        return $(this).attr('data-value');
    }).get().join('_');
    $.ajax({
        url: "{{route('product-detail-qa-praise-count',$product->url_rewrite)}}",
        data: {answer_ids:answer_ids},
        type:"POST",
        success: function(data){
            data = JSON.parse(data);
            if(data.success){
                $.each(data.counts, function(key, count){
                    var btn = $('.answer-praise[data-value="' + count.answer_id + '"]');
                    btn.find('span.count').text('(' + count.count + ')');
                });
                $.each(data.locks, function(key, lock){
                    var btn = $('.answer-praise[data-value="' + lock.answer_id + '"]');
                    btn.addClass('disabled').prop('disabled', true);
                    btn.closest('.like-comment').find('.btn-label').remove();
                });
            }
        },

        error:function(xhr, ajaxOptions, thrownError){
            swal(
                    'Oops...',
                    '發生例外錯誤!',
                    'error'
            )
        }
    });

     $('#customer-qa-show-all').click(function(){
         $('.customer-qa-show.hide').removeClass('hide');
         $(this).addClass('hide');
     });
/*
    function getAlsoBuy(){
        $.ajax({
            url: "{{route('product-detail-alsoBuy')}}",
            data: {product_url_rewrite:"{!! $product->url_rewrite !!}"},
            type:"POST",
            dataType: "json",
            success: function(result){
                if(result.success && result.html.length){
                    $('.also-buy').html(result.html).show();
                    $('.also-buy').find('img').unveil();
                    loadOwlCarousel3();
                    reloadOwlControl();
                }
            },

            error:function(xhr, ajaxOptions, thrownError){
                swal(
                    'Oops...',
                    '發生例外錯誤!',
                    'error'
                )
            }
        });
    }
*/

    function ChangHeight(){

        var DetailHeight = $('.box-detail-info').height();

        if (DetailHeight < 319) {

            $('.animate-btn-down').addClass('hidden');
            $('.animate-btn-up').addClass('hidden');
            $('.top-info-detail').removeClass('re-product-info-padding');
            $('.left-box-detail').css("height", DetailHeight);
        }

    };


    $('.animate-btn-down').click(function(){

        var DetailHeight = $('.box-detail-info').height();
        
        $('.left-box-detail').animate({height: DetailHeight});
        $('.animate-btn-down').addClass('hidden');
        $('.animate-btn-up').removeClass('hidden');
    });

        
    $('.animate-btn-up').click(function(){ 
        $('.left-box-detail').animate({height:'300px'});
        $('.animate-btn-down').removeClass('hidden');
        $('.animate-btn-up').addClass('hidden');
        $('html,body').animate({scrollTop:$('#ct-product-info-detail-top').offset().top}, "slow");
    });

    $('.product-detail-add-to-cart').click(function(event) {
        var SelectOption = testSelectOption($(this),null ,{!! in_array($product->type, [\Everglory\Constants\ProductType::NORMAL, \Everglory\Constants\ProductType::OUTLET]) ? ' "您尚未選擇商品選項"' : '"正廠零件、團購、未登錄商品無法加入待購清單。"' !!});
        if(SelectOption){
            $('.product-add-cart-container-background').removeClass('hidden');
            $('.product-add-cart-container-background').animate({opacity:'1'},"slow");
            $('.product-add-cart-container').animate({bottom:'0px'},"slow");

            $('.product-table-content ul.product-table-row').each(function(index, el) {
                var i = index;
                var SelectValue = $(this).find('.product-select-text').text();
                var class_name = "item" + i;
                var Selectwidth = $('#main').width() - 30;
                $('.product-add-cart-select-block').append("<div class="+ class_name +"></div>");
                $('.item' + i).text(SelectValue);
                $('.product-add-cart-select-block div').addClass('product-select-item');
                $('.product-add-cart-container').find('.product-select-item').css('max-width', Selectwidth);
            });
        }
    });

    $('#main').on('click', '.product-select-item',function(event){


        $('.product-add-cart-container').animate({bottom:'-100%'},"slow");
        $('.product-add-cart-container-background').animate({opacity:'0'},"slow");
        $('.product-add-cart-container-background').addClass('hidden');
        $('html,body').animate({scrollTop:$('#select-info-anchor').offset().top}, "slow");
        $('.product-add-cart-select-block').empty();

    });


    $('.close-cart-btn').click(function(event) {

        $('.product-add-cart-container').animate({bottom:'-100%'},"slow");
        $('.product-add-cart-container-background').animate({opacity:'0'},"slow");
        $('.product-add-cart-container-background').addClass('hidden');
        $('.product-add-cart-select-block').empty();

    });
    
        
    $('.product-point-block-ri').click(function(event) {
       swal({
          title: 'POINT 回饋點數',
          text: '此商品配送完成後可獲得的點數<br>(每1點可折抵商品金額 NT$ 1)',
          imageUrl: '{{ asset('image/mobile-icon/point.svg') }}',
          imageWidth: 80,
          imageHeight: 80,
          animation: true
        })
    });

    $(document).on('change','.select-info .select-address .select-country',function(){
        var country_id = $(this).find('option:selected').val();
        $('.select-info .select-address .address-masage span.masage-text').text('您可以在此選擇居住地區，以確認收到商品包裹之日期。');
        area = $('.select-info .select-address .select-area');
        $(area).find("option:not(:first)").remove().end();

        $.ajax({
            url: "{{route('getAreaByCountry')}}",
            data: {country_id:country_id},
            type:"POST",
            dataType: "json",
            success: function(result){
                $.each(result,function(key,val){
                    $(area).append($("<option></option>").attr("value",val.shipping_day).text(val.area));
                });
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal(
                    'Oops...',
                    '發生例外錯誤!',
                    'error'
                )
            }
        });
    });


    $(document).on('change','.select-info .select-address .select-area',function(){
       shipping_day = $(this).find('option:selected').val();
       stock_ship_day = $('.select-address').attr("data");
       $('.select-info .select-address').addClass('active');
       getShippingInfo(shipping_day,stock_ship_day);

    });

    function getShippingInfo(shipping_day,stock_ship_day){
        if($('.select-info .select-address').hasClass('active') && (stock_ship_day && shipping_day)){
            $.ajax({
                url: "{{route('getShippingInfo')}}",
                data: {shipping_day:shipping_day,ship_day:stock_ship_day},
                type:"POST",
                dataType: "json",
                success: function(result){
                    $('.select-info .select-address .address-masage span.masage-text').text(result);
                },
                error:function(xhr, ajaxOptions, thrownError){

                }
            });
        }else{
            $('.select-info .select-address .address-masage span.masage-text').text('您可以在此選擇居住地區，以確認收到商品包裹之日期。');
        }
    }

</script>

    