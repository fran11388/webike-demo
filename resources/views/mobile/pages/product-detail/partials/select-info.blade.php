<div class="select-info" id="select-info-anchor">
    
    <form method="post" action="{{ route('customer-estimate-update') }}" id="acb">
        <ul class="product-data-table">
            <li class="product-table-content">
                @if( $product->group_code )
                    <?php
                    $selects = $product->getGroupSelectsAndOptions();
                    ?>
                    @foreach ($selects as $select )
                        <ul class="product-table-row col-xs-block row">
{{--                             <li class="col-md-3 col-sm-4 col-xs-12">
                                <cite class="font-color-red">選擇商品形式：</cite>
                            </li> --}}
                            <li class="col-md-9 col-sm-8 col-xs-12 loading-nail">
                                @include('response.common.loading.xs')
                                <div id="product-options" class="dropdown">
                                    <button class="btn btn-default dropdown-toggle form-control" type="button" data-toggle="dropdown">
                                        <span class="product-select-text" data-selected-option-index="">{{ $select->name }}</span>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        @foreach ($select->options as $option)
                                            <li>
                                                <a href="javascript:void(0)" class="product-select-option"
                                                   data-product-sku="{{ isset($option->product_sku) ? $option->product_sku : '' }}"
                                                   data-option-value="{{ $option->name }}"
                                                   data-option-index="{{ $loop->index }}">
                                                    {{ $option->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    @endforeach
                @endif

            </li>
        </ul>
        @if(in_array($product->type,[Everglory\Constants\ProductType::NORMAL,Everglory\Constants\ProductType::OUTLET]))
            <div class="clearfix block" id="estimate_tr">
                <input type="hidden" name="sku" class="sku" value="{{ $product->group_code ? '' : $product->sku }}"/>
                <input type="hidden" name="qty"  value="1" readonly>
                <div class="estimate_request">
                   @if($product->p_country == 'J')
                        <div class="goldenweek-position-mb-sd">
                            @include('response.pages.product-detail.partials.goldenweek-tag')
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            @if($product->group_code)
                                <p class="delivery-box delivery-message">請先選擇商品形式。</p>
                            @else
                                <p class="delivery-box delivery-message">查詢中...</p>
                            @endif
                        </div>
                    </div>
                    @if($product->type == \Everglory\Constants\ProductType::NORMAL or $product->type == \Everglory\Constants\ProductType::OUTLET)
                        <div class="text-right">
                            <a class="estimate_request estimate submit-disabled" href="javascript:void(0)" title="您可以在購買前查詢庫存交期。"><i class="fa fa-envelope hidden-xs hidden-sm"></i>預先確認交期</a>
                        </div>
                    @endif
                </div>
            </div>
        @endif
        
        <div class="clearfix select-address {{$customer_address ? 'active' : ''}} hide btn-gap-top btn-gap-bottom">
            <div class="select-city">
                <div>
                    <select class="select2 select-country  btn-label" >
                        <option value="">請選擇縣市</option>
                        @foreach ($shippingCountry as $country)
                            <option value="{{$country->id}}" {{($customer_address and ($customer_address->county == $country->name)) ? 'selected="selected"': ' '}}>{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <select class="select2 select-area btn-label " >
                        <option value="">請選擇地區</option>
                        @if(isset($country_area))
                            @foreach ($country_area as $area)
                                <option value="{{$area->shipping_day}}" {{($customer_address and ($customer_address->district == $area->area)) ? 'selected="selected"': ' '}}>{{$area->area}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="address-masage btn-gap-top">
                <span class="masage-text">您可在此選擇居住地區，以確認收到商品包裹之日期</span>
            </div>
        </div>
        @include('response.pages.product-detail.partials.product-relieved-shopping')
        {{-- @if( $product->checkPointDiscount($current_customer) )
            <div class="point_discount">
                <div class="title clearfix">
                    <span class="font-bold size-12rem">POINT點數現折活動!</span>
                </div>
                <div class="info clearfix">
                    <p class="font-bold font-color-red col-sm-block col-xs-block size-10rem">購買立即折抵{!! number_format($product->getFinalPoint($current_customer),0) !!}元</p>
                    <a class="btn btn-info col-sm-block col-xs-block" href="{{URL::route('benefit-sale-pointinfo')}}" title="點數現折商品活動說明 - 「Webike-摩托百貨」" target="_blank">活動說明
                    </a>
                </div>
            </div>
        @endif --}}
        <div class="tags clearfix box" id="tag_area"></div>
    </form>
</div>
