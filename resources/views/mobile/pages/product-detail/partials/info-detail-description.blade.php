@if($productDescription = $product->getproductDescription())
    @if($productDescription->status_id == 4)
        {{--WITHOUT API DATA , USING ORIGINAL METHOD--}}
        <?php $descriptionOrig = $product->hasNotTransDescription(); ?>
        <div class="container-box">
            @if(isset($product->noTranslateButton) and $product->noTranslateButton)
            @else
                @if((((!$productDescription->summary|| $productDescription->summary == '') && ($descriptionOrig && !$descriptionOrig->summary)) || $productDescription->summary_trans_flg >= 5 ) and (((!$productDescription->sentence || $productDescription->sentence == '') && ($descriptionOrig && !$descriptionOrig->sentence)) || $productDescription->sentence_trans_flg >= 5) and (((!$productDescription->caution || $productDescription->caution == '') && ($descriptionOrig && !$descriptionOrig->caution)) || $productDescription->caution_trans_flg >= 5))
                    <div class="container-box">
                        <span>以下翻譯由Webike提供，若您想取消請點選</span>
                        <a id="main-trans-btn" class="translate-pin orig_text btn-gap-left" href="javascript:void(0)">顯示原文</a>
                    </div>
                @else
                    <div class="container-box text-center google-translation">
                        <span>以下翻譯由google提供，若想取消請點選</span>
                        <a id="main-trans-btn" class="translate-pin orig_text btn-gap-left" href="javascript:void(0)">顯示原文</a>
                    </div>
                @endif
            @endif
            @if ($productDescription->summary or ($descriptionOrig and $descriptionOrig->summary))
                <h3 class="font-bold">[要點]</h3>
                @if($productDescription->summary_trans_flg >= 8 or $productDescription->summary_trans_flg == 5)
                    <div class="description_trans" style="display: none;">
                        {!! $productDescription->summary !!}
                    </div>
                    @if($descriptionOrig)
                        <div class="description_orig">
                            {!! $descriptionOrig->summary !!}
                        </div>
                    @endif
                @else
                    <div class="goog-trans-section">
                        <a href="javascript:;" class="goog-trans-control hidden"></a>
                            {!! isset($descriptionOrig->summary) ? $descriptionOrig->summary : '' !!}
                        <br/>
                    </div>
                @endif
            @endif
            @if ($productDescription->sentence or ($descriptionOrig and $descriptionOrig->sentence))
                <h3 class="font-bold content-last-block">[商品描述]</h3>
                @if($productDescription->sentence_trans_flg >= 8 or $productDescription->sentence_trans_flg == 5)
                    <div class="description_trans" style="display: none;">
                        {!! $productDescription->sentence !!}
                    </div>
                    @if($descriptionOrig)
                        <div class="description_orig">
                            {!! $descriptionOrig->sentence !!}
                        </div>
                    @endif
                @else
                    <div class="goog-trans-section">
                        <a href="javascript:;" class="goog-trans-control hidden"></a>
                        {!! isset($descriptionOrig->sentence) ? $descriptionOrig->sentence : '' !!}
                        <br/>
                    </div>
                @endif
            @endif
            @if ($productDescription->caution or ($descriptionOrig and $descriptionOrig->caution))
                <h3 class="font-bold">[注意]</h3>
                @if($productDescription->caution_trans_flg >= 8 or $productDescription->caution_trans_flg == 5)
                    <div class="description_trans" style="display: none;">
                        {!! $productDescription->caution !!}
                    </div>
                    @if($descriptionOrig)
                        <div class="description_orig">
                            {!! $descriptionOrig->caution !!}
                        </div>
                    @endif
                @else
                    <div class="goog-trans-section">
                        <a href="javascript:void(0);" class="goog-trans-control hidden"></a>
                            {!! isset($descriptionOrig->caution) ? $descriptionOrig->caution : '' !!}
                        <br/>
                    </div>
                @endif
            @endif
        </div>


    @else
        {{--WITH API DATA , USING NEW METHOD--}}

        <?php $descriptionOrig = $product->hasNotTransDescription(); ?>
        <div class="container-box">
            @if($descriptionOrig)
                @if(isset($product->noTranslateButton) and $product->noTranslateButton)
                @else
                    <div class="container-box text-center google-translation">
                        <span>以下翻譯由google提供，若想查看原文請點選</span>
                        <a id="main-convert-btn" class="btn-gap-left translate-pin orig_text" href="javascript:void(0)">顯示原文</a>
                    </div>
                @endif
            @else
                <div class="container-box hide text-center google-translation">
                    <span>以下翻譯由google提供，若想查看原文請點選</span>
                    <a id="main-convert-btn" class="btn-gap-left translate-pin orig_text" href="javascript:void(0)">顯示原文</a>
                </div>
            @endif
            @if ($productDescription->summary)
                <h3 class="font-bold">【要點】</h3>
                <div class="description_trans" style="display: none;">
                    {!! $productDescription->summary !!}
                </div>
                @if($descriptionOrig)
                <div class="description_orig">
                    {!! $descriptionOrig->summary !!}
                </div>
                @endif
            @endif
        </div>
        <div class="container-box">
            @if ($productDescription->sentence)
                <h3 class="font-bold">【商品描述】</h3>
                <div class="description_trans" style="display: none;">
                    {!! $productDescription->sentence !!}
                </div>
                @if($descriptionOrig)
                <div class="description_orig">
                    {!! $descriptionOrig->sentence !!}
                </div>
                @endif
            @endif
        </div>
        <div class="container-box">
            @if ($productDescription->caution)
                <h3 class="font-bold">【注意】</h3>
                <div class="description_trans" style="display: none;">
                    {!! $productDescription->caution !!}
                </div>
                @if($descriptionOrig)
                <div class="description_orig">
                    {!! $descriptionOrig->caution !!}
                </div>
                @endif
            @endif
        </div>
    @endif
@endif