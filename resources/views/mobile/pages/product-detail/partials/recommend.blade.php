@if(count($recommends))
    <div id="product_ranking" class="box-ct-recommond-fix box-content custom-parts link_ui claerfix">
        <a class="link_ui_a toggle">{{isset($last_category->name) ? $last_category->name : ''}} 推薦商品</a>
        <div class="ct-inspired-by-your col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="content-branking col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="title-content-branking base-btn-gray">
                    推薦商品排名
                    <i class="fa fa-question-circle" aria-hidden="true" title="Webike摩托百貨銷售數據的排名。"></i>
                </div>
                <ul class="ct-item-content-branking">
                    @foreach($recommends as $product)
                        
                        <li class="box-fix-column item item-product-grid item-product-content-branking">
                            <div class="box-fix-with item-product-grid-pars-content-branking-left">

                                <a class="thumbnail thumbnail-pin search-list-img zoom-image"
                                       href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}" target="_blank">
                                    <figure class="zoom-image thumb-box-border">
                                        {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                                    </figure>
                                </a>
                                <span class="lank-small lank-small-0{{$loop->iteration}}">{{ $loop->iteration }}</span>
                            </div>
                            <div class="box-fix-auto item-product-grid-pars-content-branking-right">
                                <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}"
                                   class="title-product-item dotted-text2">{!!  $product->manufacturer->name  !!}</a>
                                <a href="{!!  route('product-detail', ['url_rewrite' => $product->url_rewrite])  !!}" class="title-product-item dotted-text2">{!!  $product->name  !!}</a>
                            </div>
                        </li>
                        @if($loop->iteration == 3)
                            @break
                        @endif
                    @endforeach
                    @if($last_category)
                        <div class="hidden-lg hidden-md hidden-sm">
                            <a class="btn btn-warning btn-full" href="{!! forceGetSummeryUrl(['category' => $last_category]) !!}">更多 {{$last_category->name}}</a>
                        </div>
                    @endif

                </ul>
            </div>
        </div>
    </div>
@endif