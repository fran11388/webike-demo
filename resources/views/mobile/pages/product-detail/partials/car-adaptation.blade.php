@if (count($product->fitModels))
                <div class=" link_ui car-adaptation" >
                    <a class="link_ui_a toggle">適用車型</a>
                    <div class="adaptation" >
                        <div class="box-detail-info">
                            <div class="container-box">
                                <ul class="ul-fix-model">
                                    @foreach ($product->fitModels->groupBy('maker') as $models)
                                        <?php
                                            $first_model = $models->first();
                                        ?>
                                        <li class="li-fix-model">
                                            <div class="ct-fix-model">
                                                <div class="ct-fix-model-right">
                                                    <div class="market-name">
                                                        <h2 class="size-08rem">{{$first_model->maker}}</h2>
                                                    </div>
                                                    <ul>
                                                        @foreach ($models as $key => $model)
                                                            <li>
                                                                <div class="model-name">
                                                                    {{ $model->model . ' ' . $model->style }}
                                                                </div>
                                                            </li>
                                                            @if($loop->iteration == 5 )
                                                                </ul>
                                                                <ul class="show-more-fix-model" style="display: none;">
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                    @if(count($models) > 5)
                                                        <div class="btn-see-all-pagination btn-see-all-pagination-fix-model">
                                                            <a href="javascript:void(0)">顯示全部 <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        @if (count($product->motors))
                            <div class="box-detail-info model-detail">
                                <div class="title-main-box">
                                    <h2 class="title-content-detail-info" id="anchor_fit_model">適用車型改裝及用品</h2>
                                    <!-- <div class="ct-title-main2-right">
                                        <a class="btn btn-asking licenses-info" href="javascript:void(0)">什麼是機號?</a>
                                    </div> -->
                                </div>
                                <div class="container-box">
                                    <ul class="ul-fix-model">
                                        @foreach ($product->motors->groupBy('manufacturer_id') as $motors)
                                            <?php
                                            $first_motor = $motors->first();
                                            if($first_motor->manufacturer){
                                                $image = $first_motor->manufacturer->image;
                                            }
                                            ?>
                                            <li class="li-fix-model">
                                                <div class="motor-link-block">
                                                    <div class="ct-fix-model-right">
                                                        <ul>
                                                            <li class="motor-block-title">
                                                                <span class="font-bold">{{ $first_motor->manufacturer->name }}</span>
                                                            </li>
                                                            @foreach ($motors as $key => $motor)
                                                                <li class="motor-block">
                                                                    <a href="{{ \URL::route('summary',['section' => '/mt/'.$motor->url_rewrite]) }}" alt="{{ $motor->name }}" target="_blank">{{ $motor->name }}</a>
                                                                </li>
                                                                @if($loop->iteration == 5 )
                                                        </ul>
                                                        <ul class="show-more-fix-model" style="display: none;">
                                                            @endif
                                                            @endforeach
                                                        </ul>
                                                        @if(count($motors) > 5)
                                                            <div class="btn-see-all-pagination btn-see-all-pagination-fix-model">
                                                                <a href="javascript:void(0)">顯示全部 <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif