<div class="ct-product-info-detail col-xs-12 col-sm-12 col-md-12" id="ct-product-info-detail-top">
    <div class="top-info-detail re-product-info-padding">
        <div class="left-box-detail">
            <!-- box detail info -->
            <div class="box-detail-info">
                <div class="title-main-box title-detail-info title-info-border">
                    <h3 class="title-content-detail-info common" id="anchor_detail_information">商品詳細敘述</h3>
                </div>
                @if( $product->type == Everglory\Constants\ProductType::OUTLET and $outlet = $product->outlet )
                    <div class="container-box">
                        <h3 class="font-bold">【Outlet商品敘述】</h3>
                        <div class="font-color-red font-bold size-10rem">
                            @if($outlet->description)
                                商品狀況：{{ $outlet->description }}<br>
                            @else
                                商品狀況：庫存新品<br>
                            @endif

                            @if(count($product->options))
                                @foreach ($product->options as &$option)
                                    {{ $option->label->name.'：'.$option->name }}<br/>
                                @endforeach
                            @endif
                        </div>
{{--                         <hr> --}}
                    </div>
                @endif
                @if($extraDescriptions = $product->getExtraDescription())
                    @foreach($extraDescriptions as $extraDescription)
                        {!! $extraDescription !!}
                    @endforeach
                    <br>
                @endif

                @if( $product->type == Everglory\Constants\ProductType::OUTLET )
                    <div class="container-box">
{{--                         <hr> --}}
                        <h3 class="font-bold">【outlet補充說明】</h3>
                        『OUTLET出清商品』為現貨出清商品不提供信用卡分期付款作為結帳。如果您訂單中有選購『OUTLET出清商品』此筆訂單將無法使用信用卡分期付款方式作為結帳，請改用信用卡一次付清或是貨到付款、轉帳匯款等方式結帳。<br>
                        <br>
                        部分『OUTLET出清商品』有商品瑕疵、或是包裝破損、缺件等情況都會以照片和文字敘述說明，可以接受商品狀況再進行訂購。如果對於商品狀況有不清楚的部分，請您在商品諮詢中提出詢問，我們將為您回答。<br>
                        <br>
                        部分『OUTLET出清商品』廠商已經不再生產，也無法提供後續零配件、耗材補充訂購、以及保固等服務。<br>
                        <br>
                        訂購『OUTLET出清商品』如商品本身有瑕疵或是包裝破損缺件無法為您辦理[不良品更換良品]服務。<br>
                        <br>
                        會員在「Webike-摩托百貨」訂購『OUTLET出清商品』 依規定按照結帳金額開立統一發票。<br>
                        <br>
                        『OUTLET出清商品』數量有限，售完為止，如果結帳之後因該項商品已經售完沒有庫存，客服人員將通知您並且取消該項商品。<br>
                    </div>
                @endif

                @include('mobile.pages.product-detail.partials.info-detail-description')

                <div>
                    <div class="container-box">
                        <a class="btn btn-asking btn-full" href="{{URL::route('customer-service-proposal-create', 'service') . '?skus=' . $product->url_rewrite}}" target="_blank"><i class="fa fa-comments-o size-12rem"></i> 商品諮詢</a>
                    </div>
                    <div>
                        <a class="btn btn-asking btn-full" href="{{URL::route('mitumori') . '?skus=' . $product->url_rewrite}}" target="_blank"><i class="fa fa-comments-o size-12rem"></i> 未登錄商品諮詢</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="animate-btn-down">
            <i class="fa fa-angle-down"></i>
        </div>
        <div class="animate-btn-up hidden">
            <i class="fa fa-angle-up"></i>
        </div>

    </div>

    <!-- 商品評論 -->
    <div class="customer-review ">
        <h3 class="common">商品相關資訊</h3>
        
        @include('mobile.pages.product-detail.partials.car-adaptation')

        <div class="title-main-box title-detail-info link_ui">
            <a class="title-content-detail-info with-tag link_ui_a toggle " id="anchor_customer_review">商品評論</a>
            <div class="customer-review-total adaptation">
                <?php $reviews_group = $reviews_info->groupBy('ranking'); ?>
                <ul class="col-xs-block clearfix">
                    <li class="col-xs-12 text-center">
                        <div class=" title-detail-info ">
                            <h2>商品綜合評價</h2>
                        </div>
                        <div class="ct-review-detail clearfix">
                            <div class="col-xs-6">
                                <span class="icon-star-bigger star-{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT)  }}"></span>
                            </div>
                            <div class="col-xs-6">
                                <p>{{ number_format($reviews_info->avg('ranking'),1)  }}</p>
                            </div>
                        </div>
                    </li>
                    <li class="col-xs-12">
                        <div class="ct-totaldetail ">
                            @if(count($reviews_info))
                                <table class="rating-star-table">
                                    <tr>
                                        <?php
                                        $_count = (isset($reviews_group[5]) ? count($reviews_group[5]) : 0);
                                        $_percentage = 0;
                                        if (count($reviews_info)) {
                                            $_percentage = round($_count * 100 / count($reviews_info));
                                        }
                                        ?>
                                        <td><p class="rating-star-name">5星</p></td>
                                        <td>
                                            <p class="rating-bar-bg">
                                                <span class="rating-bar-with" style="width:{{ $_percentage }}%;"></span>
                                            </p>
                                        </td>
                                        <td>
                                            <span class="rating-star-number">{{ $_count }}件</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <?php
                                        $_count = (isset($reviews_group[4]) ? count($reviews_group[4]) : 0);
                                        $_percentage = 0;
                                        if (count($reviews_info)) {
                                            $_percentage = round($_count * 100 / count($reviews_info));
                                        }
                                        ?>
                                        <td><p class="rating-star-name">4星</p></td>
                                        <td>
                                            <p class="rating-bar-bg">
                                                <span class="rating-bar-with"style="width:{{ $_percentage }}%;"></span>
                                            </p>
                                        </td>
                                        <td><span class="rating-star-number">{{ $_count }}件</span></td>
                                    </tr>
                                    <tr>
                                        <?php
                                        $_count = (isset($reviews_group[3]) ? count($reviews_group[3]) : 0);
                                        $_percentage = 0;
                                        if (count($reviews_info)) {
                                            $_percentage = round($_count * 100 / count($reviews_info));
                                        }
                                        ?>
                                        <td><p class="rating-star-name">3星</p></td>
                                        <td>
                                            <p class="rating-bar-bg">
                                                <span class="rating-bar-with"style="width:{{ $_percentage }}%;"></span>
                                           </p>
                                        </td>
                                        <td><span class="rating-star-number">{{ $_count }}件</span></td>
                                    </tr>
                                    <tr>
                                        <?php
                                        $_count = (isset($reviews_group[2]) ? count($reviews_group[2]) : 0);
                                        $_percentage = 0;
                                        if (count($reviews_info)) {
                                            $_percentage = round($_count * 100 / count($reviews_info));
                                        }
                                        ?>
                                        <td><p class="rating-star-name">2星</p></td>
                                        <td>
                                            <p class="rating-bar-bg">
                                                <span class="rating-bar-with" style="width:{{ $_percentage }}%;"></span>
                                            </p>
                                        </td>
                                        <td><span class="rating-star-number">{{ $_count }}件</span></td>
                                    </tr>
                                    <tr>
                                        <?php
                                        $_count = (isset($reviews_group[1]) ? count($reviews_group[1]) : 0);
                                        $_percentage = 0;
                                        if (count($reviews_info)) {
                                            $_percentage = round($_count * 100 / count($reviews_info));
                                        }
                                        ?>
                                        <td><p class="rating-star-name">1星</p></td>
                                        
                                        <td>
                                            <p class="rating-bar-bg">
                                                <span class="rating-bar-with" style="width:{{ $_percentage }}%;"></span>
                                            </p>
                                       </td>
                                        <td><span class="rating-star-number">{{ $_count }}件</span></td>
                                    </tr>
                                </table>
                            @else
                                <span>此商品沒有商品評論。</span>
                            @endif
                        </div>
                    </li>
                    <li class="col-xs-12 col-sm-4 col-md-3 col-lg-3 no-after">
                        <a class="btn btn-warning btn-full no-margin-horizontal" href="{{route('review-create',$product->sku )}}" target="_blank">撰寫評論 <i class="fa fa-edit size-12rem"></i></a>
                    </li>
                </ul>
                @if(count($reviews_info))
                    <ul class="ul-content-comment-detail no-after clearfix" id="review-list">
                        @include('mobile.pages.product-detail.partials.review')
                    </ul>
                    <div class="clearfix all-review">
                        <ul class="pagination pagination-sm no-after" id="review-pagination">
                            <li><a href="#" class="pagination-fa-caret"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
                            <li><a class="active" href="#">1</a></li>
                            @for($i = 2; $i <= ceil(count($reviews_info)/$review_per_page) ; $i++)
                                <li><a href="#">{{$i}}</a></li>
                            @endfor
                            <li><a href="#" class="pagination-fa-caret"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                        </ul>
                        <div class="btn-see-all-pagination no-after">
                            <span>{{count($reviews_info)}} 則 評論</span>
                            <a href="{{  route('review-search',['ca'=> null , 'sku'=>$product->url_rewrite]) }}">
                                查看全部
                            </a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <!-- end 商品評論 -->
        <!-- customer-q&a -->
        @include('mobile.pages.product-detail.partials.qa')
        <!--end customer-q&a -->
    </div>
</div>