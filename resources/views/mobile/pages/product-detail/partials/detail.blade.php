
<div class="ct-product-deatil">
    <div class="ct-product-deatil-left">
        <ul id="imageGallery">
            @if($images = $product->images and count($images))
                @foreach ($images as $image)
                    <li class="img-thumbnail thumbnail-img-product img-zoom-in" data-thumb="{{ cdnTransform($image->thumbnail) }}" data-src="{{ cdnTransform($image->image) }}">
                        <img src="{{ cdnTransform($image->image) }}" alt="{{$product->full_name . $tail}}" title="{{$product->full_name . $tail}}" />
                    </li>
                @endforeach
            @else
                <li class="img-thumbnail thumbnail-img-product">
                    <img src="{{cdnTransform(NO_IMAGE)}}" alt="{{$product->full_name . $tail}}" title="{{$product->full_name . $tail}}"/>
                </li>
            @endif
        </ul>
        <div class="img-counter">
            <span class="img-current"></span>
            <span>/</span>
            <span class="img-total"></span>
        </div>
    </div>
    <div class="ct-product-deatil-right">
        <ul class="info-brand">
            <div class="star-review-block">   
                <li>
                    <span class="icon-star star-{{ str_pad(round($reviews_info->avg('ranking')), 2,"0" , STR_PAD_LEFT)}}"></span>
                </li>
                <li>
                    <a class="review" href="{{ URL::route('review-search', '') }}?sku={{$product->url_rewrite}}">共{{ count($reviews_info) }}則評論</a>
                </li>
            </div> 
        </ul>
        <div class="fixclear"></div>
        <h1 class="price-product-deatil">
            @php
                $point_type = false;    
            @endphp
            @if($product->type == Everglory\Constants\ProductType::OUTLET)
                出清價：
            @elseif($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]))
                經銷價：
            @else
                @if( $product->checkPointDiscount($current_customer) )
                    @php
                        $point_type = true;    
                    @endphp
                    點數現折價：
                @else
                    會員價：
                @endif
            @endif
            <strong>
                @if($point_type)
                    {!! formatPrice( 'final-price' , $product->getFinalPrice($current_customer) - round($product->getFinalPoint($current_customer))) !!}
                @else
                    {!! formatPrice( 'final-price' , $product->getFinalPrice($current_customer)) !!}
                @endif
            </strong>
            @if($product->getDifferencePrice($current_customer))
                <span class="note">(上月會員價{!! formatPrice( 'final-price' , $product->getDifferencePrice($current_customer)) !!}，現省{!! formatPrice( 'final-price' , $product->getDifferencePrice($current_customer) - $product->getFinalPrice($current_customer) ) !!})</span>
            @endif
        </h1>
        <div class="ct-price-old row">
            <div class="clearfix">
                    @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]) and !($product->type == Everglory\Constants\ProductType::OUTLET))
                    <div class="product-price-block">
                        <span>
                            會員價：
                            <s>{!! formatPrice( 'normal-price' , $product->getNormalPrice()) !!}</s>
                        </span>
                    </div>
                    @endif
<!--                 <div class="product-price-block">
                    <span>
                        定價：
                        <s>{!! formatPrice( 'base-price' , $product->getPrice()) !!}</s>
                    </span>
                </div> -->
                <div class="product-point-block-ri">
                    <img class="point-icon" src="{{ assetRemote('image/mobile-icon/point.svg') }}">
                    <span>點數回饋：</span>
                    <span class="font-color-blue product-point-block">
                        <span id="final-point">{!! number_format($product->getFinalPoint($current_customer),0) !!}</span>
                        <i class="fa fa-angle-right point-swal"></i>
                    </span>
                </div>
            </div>
        </div>

        @include('mobile.pages.product-detail.partials.select-info')

    </div>
</div>
{{-- <div class="ct-product-info-detail">
    <div class="top-info-detail">
        <h3 class="common">商品編號：</h3>
        <div id="model-number" >{{ $product->model_number }}</div>
    </div>
</div> --}}
<?php

$summary = '';
if($product->productDescription and
    $product->productDescription->status_id <> 4 and //4 >> no API data
    $product->productDescription->summary_trans_flg >= 8 // > 8 >> translated
){
    $summary = $product->productDescription->summary;
}
$summary = str_replace('/catalogue/', 'http://www.webike.net/catalogue/', $summary);

//警語
$summary = $warning_text . $summary;

?>
@if ($summary)
    <div class="ct-product-info-detail">
        <div class="top-info-detail">
            <h3 class="common">商品特點:</h3>
            <span>
                @if($extraDescriptions = $product->getExtraDescription())
                    @foreach($extraDescriptions as $extraDescription)
                        {!! $extraDescription !!}
                    @endforeach
                    <br>
                @endif
                {!!  nl2br($summary) !!}
            </span>
        </div>
    </div>
@endif