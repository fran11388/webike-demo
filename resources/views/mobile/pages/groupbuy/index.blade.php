@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/groupbuy/index.css') }}">
@stop
@section('middle')
<div class="groupbuy-container">
	<div>
		<h1 class="common">團購商品查詢購買系統</h1>
	</div>
	<div class="groupbuy-container-block">
		<a class="banner-advertisement" href="javascript:void(0)"><img src="{{ assetRemote('image/banner/banner-groupbuy.png') }}" alt="banner"></a>
	</div>
	<div class="groupbuy-container-box">
		<div>
			<h3 class="common">團購商品查詢說明</h3>
		</div>
		<div class="groupbuy-container-block">
			<div class="link_ui">
	           <a class="link_ui_a toggle clearfix"> 一、團購規則</a>
	           <div class="groupbuy-description">
	           		<p>
			            1.種類：凡是「Webike台灣」網站上所有商品(<a href="{{route('summary', ['ca/1000'])}}" title="改裝零件{{$tail}}" target="_blank">改裝零件</a>、<a href="{{route('summary', ['ca/3000'])}}" title="騎士用品{{$tail}}" target="_blank">騎士用品</a>)、<a href="{{route('mitumori')}}" title="未登錄商品{{$tail}}" target="_blank">未登錄商品</a>、<a href="{{route('genuineparts')}}" title="正廠零件{{$tail}}" target="_blank">正廠零件</a>均可團購。
	            	</p>
	            	<p>
			            2.開團：凡是「Webike台灣」會員都可以開團，申請人即為團長，我們統一向您報價及收款，發票僅開立一張(含明細)，無法分別開立開發票。
	            	</p>
	            	<p>
			            3.數量：以相同規格的商品，10件以上即可開團，低於10件以下商品請利用一般購物方式。
	            	</p>
	            	<p>
			            4.規格：產品的規格必須相同，包含：功能、尺寸、顏色，若不同規格請另外開團。
	            	</p>
	            	<p>
			            5.申請：統一由此頁面進行團購申請，送出申請後，我們會於3個工作天內回覆您報價及交期。
		            </p>
	           </div>
			</div>
			<div  class="link_ui">
	            <a class="link_ui_a toggle clearfix">二、使用說明</a>
	            <div class="groupbuy-description">
	            	<p>
		            	1.查詢：請團長於下方【團購申請】填寫商品品牌、名稱、商品編號及數量，再按下”送出申請”； 約1~3個工作天我們會回覆您報價及交期，團購回覆資訊請您查閱您的email或到【<a href="{{route('customer-history-mitumori')}}" title="團購商品查詢履歷{{$tail}}" target="_blank">團購商品查詢履歷</a>】查詢。
	            	</p>
	            	<p>
		            	2.商品確認：販售的商品以您填寫的”商品編號”作為依據，若您對商品編號不確定，請點我提問。
	            	</p>
	            	<p>
		            	3.報價：均為新台幣含稅、含運報價，報價有效期限7天(到期後商品會自動消失，若要購買請再次查詢)。
	            	</p>
	            	<p>r
		            	4.待料期：為供應商備料時間(不含進口時間，進口一般為3~5個工作天)，此待料期提供您等候商品的參考；真正的交期請依據”訂購確認通知書”中的交期為準。
	            	</p>
	            	<p>
		            	5.購買：若您的團購確定開團後，請您到【<a href="{{route('customer-history-mitumori')}}" title="團購商品查詢履歷{{$tail}}" target="_blank">團購商品查詢履歷</a>】將欲購商品加入<a href="{{route('cart')}}" title="購物車{{$tail}}">購物車</a>，並進行結帳。
	            	</p>
	            	<p>
		            	6.付款：付款方式與一般購物相同，可以選擇匯款、信用卡與貨到付款(兩萬元以內)。
	            	</p>
	            	<p>
	            		7.取消或退貨：團購商品為批發販售，無法取消訂單或退貨，因此下訂前請審慎考慮清楚!!
	            	</p>
	            	<p>
			            8.若商品有以下情形可以進行換貨：商品缺件、瑕疵、運送受損、規格不符…等，我們會協助您換取新品。            		
	            	</p>
	        	</div>
	            
	        </div>
	        <div class="link_ui">
	            <a class="link_ui_a toggle clearfix">三、其他說明</a>
	            <div class="groupbuy-description">
	            	<p>
		            	1.未登錄商品：未登錄商品也可以團購，若您知道商品編號請務必填寫，若不知道商品編號，請您在備註欄填上更多訊息(如：商品網址…)。
	            	</p>
	            	<p>
	            		2.正廠零件：品牌請填寫正廠零件品牌，商品名稱請寫"正廠零件"，商品編號請填"零件料號"。
	            	</p>
	            	<p>
	            		3.關於團購相關問題，請至<a href="{{route('customer-service-proposal-create', 'service')}}" title="團購相關問題提問{{$tail}}" target="_blank">團購相關問題提問</a>，或詢問<a href="{{DEALER_URL}}" title="Webike-實體經銷商{{$tail}}" target="_blank">Webike-實體經銷商</a>。
	            	</p>
	            	<p>
	            		4.團購商品均為優惠批發價，不會發送回饋點數。
	            	</p>
	            </div>
			</div>
		</div>
	</div>
	<div class="groupbuy-container-box pages-action active" id="page-1">
		<div>
			<h3 class="common">輸入團購商品資訊</h3>
		</div>
		<div class="groupbuy-container-block">
	        <form class="mitumori-form simple-testing" method="POST" action="{!! URL::route('groupbuy-estimate') !!}">
	            <div class="form-block">
	                @php
	                    $preload_count = 1;
	                @endphp
	                @if(isset($products[0]))
		                @foreach($products as $product)
		                    @php
		                        $preload_count++;
		                        $options = $product->getSelectsAndOptions();
		                        $option_texts = [];
		                        foreach ($options as $option){
		                            $option_texts[] = $option->label . ':' . $option->option;
		                        }
		                        $option_text = implode(', ', $option_texts);
		                        $data_source =  '資料來源網址:摩托百貨(http://www.webike.tw/sd/' . $product->url_rewrite . ')';
		                    @endphp
		                    <div class=" mitumori-form-block-item st-row">
		                        <div class="mitumori-number">
		                        	<span>商品 </span>
		                            <h1>{!! $preload_count !!}</h1>
		                        </div>
		                        <ul class="ul-mitumori-form-item-row">
		                            <li>
		                                <div>
		                                    <label class=" font-color-red">商品品牌</label>
		                                    <div><input class="st-require-text width-full" type="text" name="syouhin_maker[]" value="{{$product->manufacturer->name}}"></div>
		                                </div>
		                            </li>
		                            <li>
		                                <div>
		                                    <label class=" font-color-red">商品名稱</label>
		                                    <div><input class="st-require-text width-full" type="text" name="syouhin_name[]" value="{{$product->name}}"></div>
		                                </div>
		                            </li>
		                            <li>
		                                <div>
		                                    <label class=" font-color-red">商品編號</label>
		                                    <div><input class="st-require-text width-full" type="text" name="syouhin_code[]" value="{{$product->model_number}}"></div>
		                                </div>
		                            </li>
		                            <li>
		                                <div>
		                                    <label>形式(顏色/尺寸)</label>
		                                    <div><input class="width-full" type="text" name="syouhin_type[]" value="{{$option_text}}"></div>
		                                </div>
		                            </li>
		                            <li>
		                                <div>
		                                    <label class=" font-color-red">數量(10件以上)</label>
		                                    <div><input class="st-require-number width-full" type="text" name="syouhin_kosuu[]" value="10"></div>
		                                </div>
		                            </li>
		                            <li>
		                                <div>
		                                    <label>備註說明(請貼商品網址或其他資訊)</label>
		                                    <div>
		                                        <textarea class="width-full" type="text" rows="3" name="syouhin_bikou[]">{{$data_source}}</textarea>
		                                    </div>
		                                </div>
		                            </li>
		                        </ul>
		                    </div>
		                @endforeach
	              	@else
	                    <div class=" mitumori-form-block-item st-row">
	                        <div class="mitumori-number">
	                        	<span>商品 </span>
	                            <h1>{!! $preload_count !!}</h1>
	                        </div>
	                        <ul class="ul-mitumori-form-item-row">
	                            <li>
	                                <div>
	                                    <label class=" font-color-red">商品品牌</label>
	                                    <div><input class="st-require-text width-full" type="text" name="syouhin_maker[]" value=""></div>
	                                </div>
	                            </li>
	                            <li>
	                                <div>
	                                    <label class=" font-color-red">商品名稱</label>
	                                    <div><input class="st-require-text width-full" type="text" name="syouhin_name[]" value=""></div>
	                                </div>
	                            </li>
	                            <li>
	                                <div>
	                                    <label class=" font-color-red">商品編號</label>
	                                    <div><input class="st-require-text width-full" type="text" name="syouhin_code[]" value=""></div>
	                                </div>
	                            </li>
	                            <li>
	                                <div>
	                                    <label>形式(顏色/尺寸)</label>
	                                    <div><input class="width-full" type="text" name="syouhin_type[]" value=""></div>
	                                </div>
	                            </li>
	                            <li>
	                                <div>
	                                    <label class=" font-color-red">數量(10件以上)</label>
	                                    <div><input class="st-require-number width-full" type="text" name="syouhin_kosuu[]" value=""></div>
	                                </div>
	                            </li>
	                            <li>
	                                <div>
	                                    <label>備註說明(請貼商品網址或其他資訊)</label>
	                                    <div>
	                                        <textarea class="width-full" type="text" rows="3" name="syouhin_bikou[]"></textarea>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </div>
	               	@endif
	            </div>
	            <div class=" submit-form">
	                <div>
	                    <button type="button" class="btn btn-default border-radius-2 base-btn-gray btn-submit-form-mitumori" onclick="addRow();">追加輸入欄</button>
	                    <button type="button" class="btn btn-danger border-radius-2 btn-submit-form-mitumori" onclick="goNextStep();">送出查詢</button>
	                </div>
	            </div>
	        </form>
        </div>
    </div>
    <div class="pages-action groupbuy-container-box " id="page-2">
    	<div>
    		<h3 class="common">商品確認</h3>
    	</div>
        <ul class="table-main-info groupbuy-container-block">
            <li class="table-main-title visible-md visible-lg">
                <ul>
                    <li><span class="size-10rem">&nbsp;</span></li>
                    <li><span class="size-10rem">商品品牌</span></li>
                    <li><span class="size-10rem">商品名稱</span></li>
                    <li><span class="size-10rem">商品編號</span></li>
                    <li><span class="size-10rem">形式</span></li>
                    <li><span class="size-10rem">數量</span></li>
                </ul>
            </li>
            <li class="table-main-content" id="mitumori-preview-content">
            </li>
        </ul>
        <div class="center-box groupbuy-container-block">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish" onclick="goPrevPage();">返回修正</a>
            <a class="btn btn-danger border-radius-2 btn-send-genuinepart-finish" onclick="doSubmit();">確認送出</a>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script type="text/javascript" src="{!! assetRemote('js/pages/mitumori/mitumori.js') !!}"></script>
    <script type="text/javascript">
        st_config = {number_require:10};
        function goNextStep(){
            var result = simpleTesting(st_config);
            if(result.success){
                var confirm_table = '';
                var row_count = 0;
                $(mitumori_preview_content).html('');
                $(mitumori_form + ' .mitumori-form-block-item').each(function(ul_key, ul){
                    var tds = '';
                    var null_count = 0;
                    var has_count = false;
                    $(ul).find('input').each(function(input_key, input){
                        if(!has_count){
                            tds += '<li><h3>商品' + (row_count + 1) + '</h3></li>';
                            has_count = true;
                        }
                        if(!$(input).val()){
                            null_count++;
                        }
                        if($(input).attr('name').indexOf('syouhin_kosuu') >= 0){
                            width_num = 1;
                        }else if($(input).attr('name').indexOf('syouhin_type') >= 0){
                            width_num = 1;
                        }else if($(input).attr('name').indexOf('syouhin_bikou') >= 0){
                            return true;
                        }
                        tds += '<li><span>' + $(input).val() + '</span></li>';
                    });
                    if(tds.length > 0 && null_count != $(ul).find('input').length ){
                        row_count++;
                        confirm_table += '<ul class="table-main-item col-sm-block col-xs-block clearfix">' + tds + '</ul>';
                    }
                });
                $(mitumori_preview_content).append(confirm_table);
                reSizeTableResponsive();
                goNextPage();
            }else {
                swal({
                    title: '錯誤',
                    html: "<h2>紅色項目為必填，且團購數量需為" + st_config.number_require + "件以上。<br>請再次確認</h2>",
                    type: 'error',
                }).then(function () {
                    slipTo(result.error_row);
                });
            }
        }

        function doSubmit(){
            var result = simpleTesting(st_config);
            if(result.success){
                swal({
                    title: '您確定要送出嗎?',
                    text: "點選「確認送出」後將送出查詢",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '確認送出',
                    cancelButtonText: '取消',
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#777',
                }).then(function () {
                    $(mitumori_form).submit();
                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        return false;
                    }
                });
            }else{
                swal({
                    title: '錯誤',
                    html: "<h2>紅色項目為必填，且團購數量需為" + st_config.number_require + "件以上。<br>請再次確認</h2>",
                    type: 'error',
                }).then(function () {
                    goPrevPage();
                });
            }
        }
    </script>
@stop