@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/home.css') }}">
@stop
@section('middle')
<div class="container container-top-page">
    <div class="box-content-brand-top-page">
        <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
            <div class="clearfix">
                <!-- news -->
                <div id="index_featured_post">
                    <div id="main_slider" class="flexslider clearfix">
                        <ul class="slides clearfix">
                            @php
                                $post_images = [];
                            @endphp
                            @foreach($posts as $key => $post)
                                @php
                                    $post_thumb_img = ThumborBuildWithCdn($post->thumbnail,650,380);
                                    $post_images[$key] = $post_thumb_img;
                                @endphp
                                <li>
                                    <a href="{{$post->guid}}" title="{{$post->post_title . $tail}}">
                                        <!-- <img src="{{ str_replace('http:','',str_replace('.jpg','-650x380.jpg',$post->thumbnail)) }}" alt="{{$post->post_title . $tail}}"> -->
                                        <img src="{{ $post_thumb_img }}" alt="{{$post->post_title . $tail}}">
{{--                                        <img src="{{ \Thumbor\Url\Builder::construct(config('phumbor.server'), config('phumbor.secret'), unicodeConvertForSpace(str_replace('http:','',$post->thumbnail)))->fitIn(600, 312)->build() }}" alt="{{$post->post_title . $tail}}">--}}
                                        <span class="helper"></span>
                                    </a>
                                    <a class="flex-caption" href="{{$post->guid}}" title="{{$post->post_title . $tail}}">{{$post->post_title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="flexslider-controls">
                        <ol class="flex-control-nav">
                            @foreach($posts as $key => $post)
                            <li>
                                <a href="{{$post->guid}}" title="{{$post->post_title . $tail}}">
                                    <div class="image">
                                        <img src="{{ isset($post_images[$key]) ? $post_images[$key] : '' }}" alt="{{$post->post_title . $tail}}">
                                    </div>
                                    <div class="info">
                                        <p class="date">{{date('Y/m/d', strtotime($post->post_date))}}</p>
                                    <!-- category name
                                        <p class="category">
                                        </p>
                                    -->
                                        <h4 class="title dotted-text2">{{$post->post_title}}</h4>
                                    </div>
                                </a>
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
            <div class="clearfix block">
                <a class="btn-customer-review-read-more" href="{!! BIKENEWS !!}" target="_blank">>> 更多新聞</a>
            </div>
            <!--
            <div class="clearfix block blue-box">
                <a href="http://www.webike.tw/bikenews/2017/04/24/%E3%80%90%E7%B3%BB%E7%B5%B1%E5%85%AC%E5%91%8A%E3%80%91%E5%9B%A0hinet-apcn2%E6%B5%B7%E7%BA%9C%E9%9A%9C%E7%A4%99%EF%BC%8C%E9%83%A8%E5%88%86%E9%80%A3%E7%B7%9A%E5%93%81%E8%B3%AA%E6%9C%83%E5%8F%97%E5%88%B0/" title="【系統公告】因HiNet APCN2海纜障礙，部分連線品質會受到影響" target="_blank">
                    <h2>
                        【系統公告】因HiNet APCN2海纜障礙，部分連線品質會受到影響。
                    </h2>
                </a>
            </div>
            -->
            <div class="clearfix box-content">
                <div class="clearfix">
                    <div class="title-main-box-clear">
                        <h2>
                            <span>會員好康　</span>
                            <a href="{!! URL::route('benefit') !!}" title="{!! '會員好康' . $tail !!}">>> 更多好康</a>
                        </h2>
                    </div>
                </div>
                @include('response.common.ad.banner-small')
            </div>

{{--                    @include('response.pages.home.category')--}}

            <div class="box-content">
                <div class="clearfix">
                    <div class="title-main-box-clear">
                        <h2>
                            <span>摩托百貨商品分類</span>
                        </h2>
                    </div>
                </div>
                <div class="categories-box">
                    <!-- <div class="owl-carousel-responsive"> -->
                    <div class="clearfix">
                        @php
                            $last_category = $main_categories->pull(count($main_categories) - 1);
                        @endphp
                        @foreach($main_categories as $main_category)
                            <div class="col-xs-6 col-sm-2-4 col-md-2-4 col-lg-2-4 box">
                                @php
                                    $link = route('summary', ['ca/' . $main_category->mptt->url_path]);
                                    if($main_category->url_rewrite == 9000){
                                        $link = route('genuineparts');
                                    }
                                @endphp
                                <a class="content" href="{!! $link !!}" title="{{$main_category->name . $tail}}">
                                    <img src="{{assetRemote('image/category/index/' . $main_category->url_rewrite . '.png')}}" alt="{{$main_category->name . $tail}}">
                                    <span class="name">
                                        <span class="text">{{$main_category->name}}</span>
                                        <span class="helper"></span>
                                    </span>
                                </a>
                            </div>
                        @endforeach
                        @if($last_category)
                            <div class="hidden-xs">
                                <div class="col-xs-12 col-sm-2-4 col-md-2-4 col-lg-2-4 box">
                                @php
                                    $link = route('summary', ['ca/' . $last_category->mptt->url_path]);
                                    if($last_category->url_rewrite == 9000){
                                        $link = route('genuineparts');
                                    }
                                @endphp
                                    <a class="content" href="{!! $link !!}" title="{{$last_category->name . $tail}}">
                                        <img src="{{assetRemote('image/category/index/' . $last_category->url_rewrite . '.png')}}" alt="{{$last_category->name . $tail}}">
                                        <span class="name">
                                            <span class="text">{{$last_category->name}}</span>
                                            <span class="helper"></span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="box-content hidden-sm hidden-md hidden-lg">
                <div class="clearfix">
                    <div class="title-main-box-clear">
                        <h2>
                            <span>正廠零件查詢與購買　</span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box">
                        <a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::IMPORT['url_code']]) !!}" title="進口正廠零件 - 「Webike-摩托百貨」">
                            {!! lazyImage("https://img.webike.tw/shopping/image/oempart/banner-". \Everglory\Constants\CountryGroup::IMPORT['url_code'] .".png") !!}
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <a href="{!! URL::route('genuineparts-divide', [\Everglory\Constants\CountryGroup::DOMESTIC['url_code']]) !!}" title="國產正廠零件 - 「Webike-摩托百貨」">
                            {!! lazyImage("https://img.webike.tw/shopping/image/oempart/banner-". \Everglory\Constants\CountryGroup::DOMESTIC['url_code'] .".png") !!}
                        </a>
                    </div>
                </div>
            </div>

            <div class="box-content">
                <div class="clearfix">
                    <div class="title-main-box-clear">
                        <h2>
                            <span>全球摩托車資料庫　</span>
                            <a href="{!! URL::route('motor') !!}" title="{!! '車型索引' . $tail !!}">>> 更多廠牌車型</a>
                        </h2>
                    </div>
                </div>
                <!-- motors -->
                <div class="motor-select-box">
                    <form action="" method="GET" id="motor_search_form">
                        <input type="hidden" name="rel" value="homepage">
                        <ul class="box clearfix no-list-style" id="bike-first-row">
                            <li class="col-xs-6 col-sm-3 col-md-4 box">
                                <select class="select2 select-motor-manufacturer-home">
                                    <option value="">請選擇廠牌</option>
                                    @foreach ($motor_manufacturers as $_manufacturer)
                                        <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-4 box">
                                <select class="select2 select-motor-model-home" disabled>
                                    <option value="">車型</option>
                                </select>
                            </li>
                            <li class="col-xs-12 col-sm-3 col-md-2 box">
                                <input class=" btn btn-primary btn-full" type="submit" name="" value="搜尋">
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="motor-manufacturer-box">
                            <div class="row">
                                @foreach($main_manufacturers as $key => $main_manufacturer)
                                    @if($key == 4)
                                        @break;
                                    @endif
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                        <div class="box-content manufacturer-logo">
                                            <a class="logo-link" href="javascript:void(0)" title="{{$main_manufacturer->name . $tail}}">
                                                {!! lazyImage(assetRemote('image/motor-manufacturer/logo-index/' . $main_manufacturer->url_rewrite . '.png'), $main_manufacturer->name . $tail) !!}
                                                <span class="helper"></span>
                                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </a>
                                            <ul class="manufacturer-logo-list">
                                                @foreach(\Everglory\Constants\MotorDisplacementLevel::DISPLACEMENT_LEVELS as $displacement_level)
                                                    @php
                                                        $variable_name = 'DISPLACEMENT_' . $displacement_level;
                                                    @endphp
                                                    <li>
                                                        <a href="{{route('motor-manufacturer', [$main_manufacturer->url_rewrite, $displacement_level])}}" title="{{$main_manufacturer->name . \Everglory\Constants\MotorDisplacementLevel::getConst($variable_name) . $tail}}">
                                                            {{\Everglory\Constants\MotorDisplacementLevel::getConst($variable_name)}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-content">
                <div class="motor-product-box">
                    <div class="clearfix">
                        <div class="title-main-box-clear">
                            <h2>
                                <span>新車．中古車情報　</span>
                                <a href="{!! MOTOMARKET !!}" title="{!! \Everglory\Constants\Website::MOTOMARKET . $tail !!}" target="_blank">>> 更多車輛情報</a>
                            </h2>
                        </div>
                    </div>
                    <ul class="row owl-carousel-responsive">
                        @foreach($new_motors as $motor_num => $new_motor)
                            <li class="col-xs-12 col-sm-12 col-md-2-4 col-lg-2-4 item-product-grid">
                                @include('response.common.product.j',['motor' => $new_motor, 'motor_num' => $motor_num])
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ assetRemote('plugin/FlexSlider-master/jquery.flexslider-min.js') }}"></script>
<script type="text/javascript">
    $('.motor-manufacturer-box .manufacturer-logo').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $('.motor-manufacturer-box .manufacturer-logo').removeClass('active');
            $(this).addClass('active');
        }

    });

    $(document).on("change", ".select-motor-manufacturer-home", function() {
        manufacturer = $(this).val();
        motor = $(this).closest('form').find('.select-motor-model-home');
        motor.attr('disabled',false).select2('destroy');
        motor.select2({
            width:"100%",
            minimumInputLength: 1,
            language: {
                inputTooShort: function(args) {
                    // args.minimum is the minimum required length
                    // args.input is the user-typed text
                    return "請輸入至少"+ args.minimum + "個字元";
                },
//                errorLoading: function() {
//                    return "發生錯誤";
//                },
                noResults: function() {
                    return "找不到結果";
                },
                searching: function() {
                    return "搜尋中...";
                }
            },
            ajax: {
                url: "{{route('api-motor-manufacturer-model')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        motor_name: params.term, // search term
                        manufacturer: manufacturer
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj.key, text: obj.name };
                        })
                    };
                }
            }
        });
    });

    $('#motor_search_form').submit(function () {
       if(!$(this).attr('action')){
           swal(
               '未選擇車型!',
               '請先選擇車型再進行搜尋!',
               'error'
           );
           return false;
       }
    });
    $(document).on("change", ".select-motor-model-home", function() {
        motor = $(this).closest('form').find('.select-motor-model-home');
        $(this).closest('form').attr('action',"{{route('summary','mt')}}/" + motor.val());
    });



    jQuery('.flexslider').flexslider({
        slideshowSpeed: 4000,
        directionNav: false,
        manualControls: ".flex-control-nav li",
        // animation: "slide",
        pasneOnHover: true,
        start: function(){
            jQuery("#main_slider ul").css("display","block");
        }

    });

</script>
@endsection