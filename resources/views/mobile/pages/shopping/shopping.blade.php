@extends('mobile.layouts.mobile')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/shopping.css') }}">
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/transform_keyframes.css') }}">
@section('style')
@stop
@section('middle')


{{-- 右邊的menu隱藏 --}}
{{-- <div class="shopping-menu-icon menu-icon-fixed">
	<a href="javascript:void(0)">
		<i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i>
	</a>
</div> --}}

<div class="shopping-title">
	<div class="shopping-close_btn">×</div>
	<div class="shopping-menu_items">
		<div class="shopping-inner">
			<dl class="menu-list">
				@include('mobile.pages.shopping.partials.shopping-menu')
			</dl>
		</div>
	</div>
	<div class="shopping-overlay"></div>
</div>
<div class="container">
    @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))
        @include('response.common.ad.banner-slideshow',['ad_id'=>11])
    @else
        @include('response.common.ad.banner-slideshow',['ad_id'=>1])
    @endif
    @php
	    $ca_name = ["3000" => "騎士用品", "1000" => "改裝用品","4000" => "保養耗材","8000" => "機車工具"];
	@endphp
    @foreach($categories as $url_path => $category)
    	@if(($url_path == 4000) or ($url_path == 8000))
	        <div class="row box-content">
		        <div class="title-main-box-clear">
		            <h2>
		                <span>推薦品牌</span>
		            </h2>
		        </div>
		        <div class="box-content">
		            @if(isset($advertiser))
		                {!! $advertiser->call(28) !!}
		            @endif
		        </div>
		    </div>
    	@endif
        @include('mobile.pages.shopping.partials.normal')
    @endforeach
</div>
@stop
@section('script')
    <script src="{{ assetRemote('js/pages/owl.carousel-ajax.js') }}"></script>
    <script src="{!! assetRemote('plugin/slick/slick.js') !!}"></script>
    <script type="text/javascript">
    	$(".shopping-menu-icon, .shopping-overlay, .shopping-close_btn").on('click', function(){
			if($(".shopping-menu_items").css("display") != "block"){
				$("html").css("overflow","hidden");
				$("html").css("position","relative");
				$("#w_spng_sp").css("position","relative");
				var winHeight = $(window).height();
				$(".shopping-menu_items").height(winHeight).scrollTop(0);
				$(".shopping-menu_items .shopping-inner").css("min-height",winHeight);
				$(".shopping-menu_items").removeClass("hide").addClass("show");
				$(".shopping-overlay,  .shopping-close_btn").show();
				$(".shopping-menu_items dl dd ul.no-rcj-a").show();
			}else{		
				$(".shopping-overlay, .shopping-close_btn").hide();
				$(".shopping-menu_items dl dd ul").hide();
				$(".shopping-menu_items").removeClass("show").addClass("hide");
				setTimeout(function(){$(".shopping-menu_items").removeClass("hide");},500);
				$(".shopping-menu_items a").removeClass("open");
				$("#w_spng_sp").css("position","absolute");
				$("html").css("overflow","");
				$("html").css("position","");
			}
		});
	</script>
@stop