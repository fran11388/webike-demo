<dt>摩托百貨</dt>
<dd>
    <a class="toggle" href="javascript:void(0)">車型索引</a>
    <ul>
        @php
            $motor_manufacturers = \Ecommerce\Repository\MotorRepository::selectManufacturersByUrlRewrite(\Everglory\Constants\ViewDefine::MAIN_MOTOR_MANUFACTURERS);
        @endphp
        @foreach($motor_manufacturers as $motor_manufacturer)
            <li>
                <a href="{{route('motor-manufacturer', [$motor_manufacturer->url_rewrite, '00'])}}" title="{{$motor_manufacturer->name . $tail}}">
                    <span>{{$motor_manufacturer->name}}</span>
                </a>
            </li>
        @endforeach
    </ul>
</dd>
<dd>
    <a class="toggle" href="javascript:void(0)">其他連結</a>
    <ul>
        <li>
            <a href="{{route('service-yfcshipping')}}" target="_blank" title="YFC橫濱物流中心介紹{{$tail}}">
                YFC橫濱物流中心介紹
            </a>
        </li>
        <li>
            <a href="{{route('customer-rule', ['member_rule_2'])}}#G" target="_blank" title="DHL國際快遞進口作業{{$tail}}">
                DHL國際快遞進口作業
            </a>
        </li> 
        <li>
            <a href="{{route('customer-rule', ['member_rule_2'])}}#E" target="_blank" title="付款方式說明(信用卡/分期0利率/匯款/貨到付款){{$tail}}">
                付款方式說明
            </a>
        </li>  
    </ul>
</dd>
<dd>
    <a href="{{route('mitumori')}}" target="_blank" title="未登錄商品報價及交期查詢！{{$tail}}" class="text-center">
        <img src="{{ assetRemote('image/banner/banner-mitumori-small.png') }}" alt="未登錄商品報價及交期查詢！{{$tail}}">
    </a>
</dd>
<dd>
    <a href="{{route('groupbuy')}}" target="_blank" title="團購系統-所有商品皆可開團！{{$tail}}" class="text-center">
        <img src="{{ assetRemote('image/banner/banner-groupbuy-small.png') }}" alt="團購系統-所有商品皆可開團！{{$tail}}">
    </a>
</dd>
<dd>
    <a href="{{route('service-dealer-join')}}" target="_blank" title="經銷商募集{{$tail}}" class="text-center">
        <img src="{{ assetRemote('image/banner/dealerAddBanner.png') }}" alt="經銷商募集{{$tail}}">
    </a>
</dd>
<dd>
    <a href="{{route('service-supplier-join')}}" target="_blank" title="品牌大募集{{$tail}}" class="text-center">
        <img src="{{ assetRemote('image/banner/manufacturerAdd.png') }}" alt="品牌大募集{{$tail}}">
    </a>
</dd>
