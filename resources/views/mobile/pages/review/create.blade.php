@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/review/create.css') }}">
@stop
@section('middle')
<div class="review-writing">
	 <div class="pages-title">
		<h1 class="common">撰寫商品評論</h1>
	</div>
    <form id="form-validate" method="post" action="{{ URL::route('review-store')}}">
        <input type="hidden" name="product_id" value="{{$product->id}}"/>
        <input type="hidden" name="photo_key" value=""/>
        <div class="box-content-brand-top-page border-box">
            <ul class="ct-review-writing">
                <li class="box-top section">
                    <div class="review-writing-product">
                        <div class="box-fix-with">
                            <a href="{{route('product-detail', $product->sku )}}">
	                        	<div class="review-product-photo">
		                                <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
		                                    {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
		                                </figure>
	                        	</div>
	                            <div class="review-product-font">
	                            	
	                                <h2>{{ $product->name }}</h2>
	                                <span>商品編號：{{ $product->model_number }}</span>
	                            </div>
                            </a>
                        </div>
                        <div class="box-fix-motor">
                            <div class="box-top-right">
                                <ul class="select-motor">
                                    <li class="review-writing-title"><h3>安裝車輛：<span>(選填)</span></h3></li>
                                    <li>
                                        <select class="sort select2 select-motor-manufacturer">
                                            <option>請選擇廠牌</option>
                                            @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                                <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                            @endforeach
                                        </select>
                                    </li>
                                    <li>
                                        <select class="sort select2 select-motor-displacement">
                                            <option>CC數</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select class="sort select2 select-motor-model" name="motor_id">
                                            <option>車型</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            
              
                <li class="box-theme section">
                    <ul>
                        <li class="review-writing-title">
                           <h3 class="common"> 主題：<span>(必填)</span></h3>
                        </li>
                        <li>
                        	<div class="review-writing-box">
                            	<input type="search" name="title" value="" class="textbox-style" placeholder="填入評論主題" >
                        	</div>
                        </li>
                    </ul>
                </li>
            
      			<li class="box-items section">
      				<div class="review-writing-title">
      					<h3 class="common"> 優點&缺點 : <span>(必填)</span></h3>
      				</div>
                    <div class="box-items-good ">
                    	<div class="review-writing-title">
                        		<h3 class="common">O 優點</h3>
                		</div>
                		<div class="review-writing-box">
	                        <ul>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[pros][]"
		                                           value="CP質高"> 
		                                     <span>CP質高</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[pros][]"
		                                           value="品質質感佳">
		                                    <span>品質質感佳</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[pros][]"
		                                           value="精美包裝設計">
		                                    <span>精美包裝設計</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[pros][]"
		                                           value="可以信賴的品牌">
		                                    <span>可以信賴的品牌</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[pros][]"
		                                           value="性能令人激賞">
		                                    <span>性能令人激賞</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[pros][]"
		                                           value="安裝容易">
		                                    <span>安裝容易</span>
	                                	</div>
	                                </label>
	                            </li>
	                        </ul>
	                        <label class="textbox-group">
	                            <span>其他優點</span>
	                            <input class="textbox-style" type="text"  name="rating[pros][usr]" placeholder="其他優點">
	                        </label>
                    	</div>
                    </div>
                    <div class="box-items-bad ">
                    	<div class="review-writing-title">
                        		<h3 class="common">X 缺點</h3>
                        </div>
                        <div class="review-writing-box">
	                        <ul>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[cons][]"
		                                           value="品質質感不好">
		                                    <span>品質質感不好</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[cons][]"
		                                           value="包裝設計不佳">
		                                    <span>包裝設計不佳</span>
	                                	</div>
	                                </label>
	                            </li>
	                            <li>
	                                <label>
	                                	<div>
		                                    <input type="checkbox"
		                                           name="rating[cons][]"
		                                           value="性能不如預期">
		                                    <span>性能不如預期</span>
	                                	</div>
	                                </label>
	                            </li>
	                        </ul>
	                        <label class=" textbox-group">
	                            <span>其他缺點</span>
	                            <input class="textbox-style" type="text" name="rating[cons][usr]" placeholder="其他缺點">
	                        </label>
                    	</div>
                    </div>
                </li>
                <li class="box-review section">
                    <div class="review-writing-title">
                       <h3 class="common">商品綜合評價：<span>(必填)</span></h3>
                    </div>
					<div class="review-writing-box review-star-bigger">
                        <div>
                            <label class="radio-inline">
                                <input type="radio" name="ranking" value="5"><span class="icon-star-bigger star-05"></span>
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                                <input type="radio" name="ranking" value="4"><span class="icon-star-bigger star-04"></span>
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                                <input type="radio" name="ranking" value="3"><span class="icon-star-bigger star-03"></span>
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                                <input type="radio" name="ranking" value="2"><span class="icon-star-bigger star-02"></span>
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                                <input type="radio" name="ranking" value="1"><span class="icon-star-bigger star-01"></span>
                            </label>
                        </div>
                    </div>
                </li> 
               <li class="box-comment box-fix-column section">
	               	<div class="review-writing-title">
	               		<h3 class="common">上傳圖片</h3>
	               	</div>
	               	<div class="clearfix review-writing-box">
	                     <div class="review-writing-photo">
	                        <figure class="ct-img-left thumb-box-border thumb-img">
	                            <img src="{{assetRemote('image/bg-btn-choose-file.jpg')}}" alt="" id="photo">
	                            <a class="btn btn-primary border-radius-2 btn-choose-file visible-sm visible-md visible-lg"
	                               href="javascript:$('#choose-file').click();">上傳圖片</a>
	                        </figure>
	                        <a class="btn btn-primary border-radius-2 btn-choose-file visible-xs"
	                           href="javascript:$('#choose-file').click();">上傳圖片</a>

	                        <input type="hidden" name="photo_preview" value="">
	                    </div>
	                    <div class="review-writing-font">
	                        <textarea class="textbox-style" type="text" rows="3" name="content"
	                                  placeholder="請輸入50字以上，1000字以下商品心得"></textarea>
	                        <span class="font-color-red">(必填)</span>
	                    </div>
                	</div>
                </li>
               <li class="box-info">
               		<div class="review-writing-title">
                        <h3 class="common">投稿說明</h3>
                    </div>
                    <div class="box-info-font">
		                <span>
	                        1.紅色框框為必填欄位。<br>
	                        2.上傳的照片必須為商品實拍照片，否則<span class="font-color-red">不予採用</span><br>
	                        3.請勿使用注音文及不雅的文字與敘述，本公司有進行審核的權力。<br>
	                        4.評論內容均屬於個人言論，僅供刊登分享使用，不代表本公司立場。<br>
	                        5.投稿內容需三個工作天進行審核，採用後即刊登於商品頁面。<br>
	                        6.您上傳照片視同於同意「Webike台灣」網站使用，未經您的允許不另做其他用途。<br>
	                        7.評論採用後，我們將會以點數的方式贈送給您<br>
		                </span>
                	</div>
                </li>
            </ul>
        </div>
        <div class="text-center form-group review-writing-box">
            <button type="submit" id="writing_btn" class="btn btn-danger style-button-strong border-radius-2" href="">
                投稿
            </button>
        </div>
    </form>
    <form style="position: absolute;left:-1000px;top:0px;" id="hidden-form" target="upload_target"
          action="{{route('review-upload')}}" method="post" enctype="multipart/form-data">
        <input id="choose-file" type="file" size="40" name="photo" accept="image/*" style="width:260px;margin:0">
    </form>

    <iframe id="upload_target" name="upload_target" src="#" style="display:none; width:0;height:0;border:0px solid #fff;"></iframe> 
</div>
@endsection
@section('script')
<script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{{assetRemote('plugin/validate/jquery.validate.js') }}"></script>
    <script src="{{assetRemote('plugin/validate/jquery.validate-additional-methods.min.js') }}"></script>
    <script type="text/javascript">

        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }

        $(".select-motor-manufacturer").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get(url, function (data) {
                    var $target = $(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]).text(data[i]))
                    }
                });
            }
        });
        $(".select-motor-displacement").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                var url = path + "/api/motor/model?manufacturer=" +
                    $(this).closest('ul').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get(url, function (data) {
                    var $target = $(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });


        $('input[type="file"]').change(function () {
            $('input[name="photo_preview"]').val('1');
            $('#writing_btn').prop('disabled', true).css('cursor', 'no-drop');
            $('#hidden-form').submit();            
        });
        function stopUpload(result) {
            $('#writing_btn').prop('disabled', false).css('cursor', 'pointer');
            $('input[name="photo_preview"]').val('');            
            if (result.success) {            	
                $('input[name="photo_key"]').val(result.key);
                $("#photo").attr('src', "{{assetPathTranslator('assets/photos/review/',config('app.env'))}}"  +'/'+  result.key);
            } else {
                $('input[name="photo_key"]').val('');
                $("#photo").attr('src', "{{assetPathTranslator('image/bg-btn-choose-file.jpg',config('app.env'))}}");
                alert(result.message);
                if (result.reload) {
                    window.location.reload();
                    return false;
                }
            }
            return true;
        }
    </script>
    <script type="text/javascript">

        var zhMsg = {
            required: "此為必填欄位",
            extension: "僅允許上傳jpg格式",
            remote: "請修正該字段",
            email: "請輸入正確格式的電子郵件",
            url: "請輸入合法的網址",
            date: "請輸入合法的日期",
            dateISO: "請輸入合法的日期 (ISO).",
            number: "請輸入合法的數字",
            digits: "只能輸入整數",
            creditcard: "請輸入合法的信用卡號",
            equalTo: "請再次輸入相同的值",
            accept: "請輸入擁有合法後綴名的字符串",
            maxlength: $.validator.format("請輸入一個長度最多是{0} 的字符串"),
            minlength: $.validator.format("請輸入一個長度最少是{0} 的字符串"),
            rangelength: $.validator.format("請輸入一個長度介於{0} 和{1} 之間的字符串"),
            range: $.validator.format("請輸入一個介於{0} 和{1} 之間的值"),
            max: $.validator.format("請輸入一個最大為{0} 的值"),
            min: $.validator.format("請輸入一個最小為{0} 的值")
        };
        $(document).ready(function () {
            /* 設置默認屬性 */
            $.validator.setDefaults({
                submitHandler: function(form) {
                    if( $(form).attr('id') == 'tmp_form' ){
                        form.submit();
                    }
                },
                errorPlacement: function (error, element) {
                    var submit_btn = $('#writing_btn');
                    if (element.is(':radio') || element.is(':checkbox')) {
                        var eid = element.attr('name');
                        // console.log(eid);
                        $('.review-star-bigger div:last-child').after(error);
                    }else {
                        error.insertAfter(element);
                    }

                    submit_btn.prop('disabled', false).css('cursor', 'pointer');
                }
            });
            jQuery.validator.addMethod("trimRangeLength", function(value, element, param) {
                var length = $.trim(value.length);
                return this.optional(element) || ( length >= param[0] && length <= param[1] );
            }, "請確保輸入的值在{0}-{1}個字節之間");

            $.extend($.validator.messages, zhMsg);
            validator = $("#form-validate").validate({
                rules: {
                    ranking : {
                        required:true,
                    },
                    title : {
                        required:true,
                    },
                    photo : {
                        required:true,
                        extension: "jpeg|jpg"
                    },
                    content : {
                        required: true,
                        trimRangeLength : [50,1000]
                    }
                },

                submitHandler: function(form) {
                    photo = $('input[name=photo]').val();
                    var submit_btn = $('#writing_btn');
                    submit_btn.prop('disabled', true).css('cursor', 'no-drop');
                    if(photo == ""){
                        swal({
                              text: "請上傳您拍攝的商品照片",
                            })
                        return false;
                    }
                    if( !confirm('您確定要送出?') ){
                        submit_btn.prop('disabled', false).css('cursor', 'pointer');
                        return false;
                    }
                    form.submit();
                },
            });

        });

    </script>
@endsection