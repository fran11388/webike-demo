@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/review/list.css') }}">
@stop
@section('middle')
<div class="filter-bolck section">
    <h3 class="common">搜尋條件</h3>
	@include('mobile.pages.review.partials.filter')
</div>
<div class="review-container section">
	<div class="title-main-box">
		<h3 class="common">{{$meta_filters}}搜尋結果</h3>
	</div>
	<div class="review-box">
		@include('mobile.pages.review.partials.comments-list')
	</div>
	<div class="ct-pagenav">
		{!! $pager->render() !!}
	</div>
</div>

@endsection
@section('script')
  <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>

    <script type="text/javascript">
        var baseUrl = '{{ request()->url() }}';
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }
        $(".select-option-redirect").change(function () {
            window.location = $(this).val();
        });

        $("a.a-sub2 > span").click(function () {
            window.location = $(this).parent().attr('data-href');
            return 0;
        });

        $(".selsect-motor .select-motor-manufacturer").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get(url, function (data) {
                    var $target = $(".selsect-motor .select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]).text(data[i]))
                    }
                });
            }
        });

        $(".selsect-motor .select-motor-displacement").change(function () {
            var value = $(this).find('option:selected').val();
            var test = $(".selsect-motor .select-motor-manufacturer").find('option:selected').val();
            console.log(test);
            if (value) {
                var url = path + "/api/motor/model?manufacturer=" +
                    $(".selsect-motor .select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get(url, function (data) {
                    var $target = $(".selsect-motor .select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(".selsect-motor .select-motor-model").change(function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                redirect('', value);
            }
        });


        function redirect(q, mt) {
            var params = {};
            if (q)
                params.q = q;
            else if (getParameterByName('q'))
                params.q = getParameterByName('q');
            if (mt)
                params.mt = mt;
            else if (getParameterByName('mt'))
                params.mt = getParameterByName('mt');


            var esc = encodeURIComponent;
            var query = Object.keys(params).map(k => esc(k) + '=' + esc(params[k])).join('&');
            if (query)
                query = "?" + query;

            window.location = window.location.href.split('?')[0] + query;
        }

        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
@stop
