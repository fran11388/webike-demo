@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/review/article.css') }}">
@stop
@section('middle')
<div class="box-content-brand-top-page">
    <div class="ct-right-search-list">
        <div class="section">
            <div class="title-main-box">
                <h1 class="common">【{{ $review->product_manufacturer }}】{{ $review->product_name }}</h1>
            </div>
            <div class="product-review-block">
                <div class="box-info-motor-article form-group">
                    <ul class="info-motor-article">
                        <li>
                            <ul>
                                <li>
                                    <div class="clearfix">
                                        @if($motor)
                                            <span>
                                                安裝車型：<a href="{!! getMtUrl(['motor_url_rewrite' => $motor->url_rewrite]) !!}" title="{{ $motor->manufacturer->name . ' ' . $motor->name . ($motor->synonym ? '(' . $motor->synonym . ')' : '' ) . $tail }}">{{ $motor->manufacturer->name . ' ' . $motor->name . ($motor->synonym ? '(' . $motor->synonym . ')' : '' ) }}</a>
                                            </span>
                                        @else
                                            <span>無</span>
                                        @endif
                                    </div>
                                </li>
                                <li class="review-start">
                                    <div class="clearfix">
                                        <span>評價：</span>
                                        <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="clearfix">
                        <?php $ratings = unserialize($review->rating);  ?>
                            <ul>
                                @if($ratings)
                                    @foreach ($ratings['pros'] as $rating)
                                        @if ($rating)
                                            <li>
                                                <i class="icon-0">O</i>
                                                {{ $rating }}
                                            </li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                        <li class="clearfix">
                            <ul>
                                @if($ratings)
                                    @foreach ($ratings['cons'] as $rating)
                                        @if ($rating)
                                            <li>
                                                <i class="icon-x">X</i>
                                                {{ $rating }}
                                            </li>
                                        @endif
                                    @endforeach
                                @endif

                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="box-fix-column  block box-info-motor-article2">
                    <div class=" box review-photo ">
                            <figure class="zoom-image thumb-box-border">
                                <img src="{!! \Ecommerce\Service\ReviewService::getImageUrl($review) !!}" alt="{{ $review->product_name . $tail }}">
                            </figure>
                    </div>
                    <div class="box review-font clearfix">
                        <h2 class="box"><strong>主題: {{ $review->title }}</strong></h2>
                        <ul class="box">
                            <li>投稿者：{{ $review->nick_name }}</li>
                            <li>投稿日期：{{ $review->created_at }}</li>
                        </ul>
                        <span class="text-content">
                            {!! nl2br(e(strip_tags($review->content))) !!}
                        </span>
                    </div>
                    <div class="review-product">
                        <a class="btn btn-warning btn-full" href="{{ route('product-detail', $review->product_sku) }}">
                            查看商品
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
            @include('mobile.pages.review.partials.show-review-comments')
        </div>
        @if(count($reviews))
            <div class=" form-group other-reivew">
                <div class="title-main-box">
                    <h3 class="common">其他評論</h3>
                </div>
                @include('mobile.pages.review.partials.list')
            </div>
        @endif 
            
    </div> 
</div> 


@endsection
@section('script')
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.8&appId=791319474279962";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Product",
            "image": "{{$review->photo_key ? 'http://img.webike.tw/review/'.$review->photo_key : $review->product_thumbnail}}",
            "name": "{{$review->product_name}}",
            "review": {
                "@type": "Review",
                    "reviewRating": {
                        "@type": "Rating",
                        "ratingValue": "{{ $review->ranking }}"
                    },
                "name": "{{ $review->title }}",
                "author": {
                    "@type": "Person",
                    "name": "{{ $review->nick_name }}"
            },
            "datePublished": "{{ date('Y-m-d', strtotime($review->created_at)) }}",
                "reviewBody": "{!! nl2br(e($review->content)) !!}",
                "publisher": {
                    "@type": "Organization",
                    "name": "「Webike-摩托百貨」"
                }
            }
        }
    </script>
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script>
        $('form').submit(function(){

            if(typeof $(this).find('textarea') !== "undefined" && $(this).find('textarea').val().trim() == ''){
                swal(
                        '錯誤',
                        '請輸入回應內容!',
                        'error'
                )
                return false;
            };
        });
    </script>
@stop
