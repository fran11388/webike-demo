<div class="ct-right-top">
    <div class="ct-table">
        @if(request()->input())
            <div class="ct-table-row condition">
                <div class="ct-table-cell">
                    <ul class="ul-cell-dropdown-list container">
                        @foreach($filters as $key => $filter)
                            @if(request()->has($key))
                                <li class="li-cell-dropdown-item">
                                    <div class="div-tag-item">
                                        <span>{{$filter['label']}}:{{ $filter['name'] }}</span>
                                        @php
                                            $arg = [];
                                            foreach($filters as $_key => $_filter){
                                                if($_key == $key ){
                                                    $arg[$_key] = '';
                                                }else{
                                                    $arg[$_key] = request()->input($_key);
                                                }
                                            }
                                        @endphp
                                        <a href="{{ modifyReviewSearchUrl($arg) }}">
                                            <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="ct-table-row">
            <div class="ct-table-cell selsect-motor">
                <ul class="ul-cell-dropdown-list container">
                    <li class="li-cell-dropdown-item col-xs-5">
                        <div class="container select-list-box">
                            <select class="select2 select-motor-manufacturer">
                                <option>請選擇廠牌</option>
                                @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                    <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </li>
                    <li class="li-cell-dropdown-item col-xs-3">
                        <div class="container select-list-box">
                            <select class="select2 select-motor-displacement">
                                <option>cc數</option>
                            </select>
                        </div>
                    </li>
                    <li class="li-cell-dropdown-item col-xs-4">
                        <div class="container select-list-box">
                            <select class="select2 select-motor-model">
                                <option>車型</option>
                            </select>
                        </div>
                    </li>
                </ul>
            </div>
        </div>  
        <div class="ct-table-row">
            <div class="ct-table-cell">
                <div class="cell-content clearfix">
                    <input type="text" class="input-search-keyword search-text col-xs-9" value="">
                    <a class="btn btn-danger btn-add-key-word" href="javascript:void(0)" onclick="submitKeyword(this);">搜尋</a>
                </div>
            </div>
        </div>
    </div>
</div>