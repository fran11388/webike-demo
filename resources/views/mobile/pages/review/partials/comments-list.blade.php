<ul class="newcomments">
    @foreach ($reviews as $review)
        <li>
            <div class="newcomments-photo">
                <a href="{{ route('review-show', $review->id)}}">
                    <figure class="zoom-image thumb-box-border">
                        <?php
                        $src = '';
                        if (starts_with($review->photo_key, 'http'))
                            $src = $review->photo_key;
                        else
                            $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                        ?>
                        <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name }}">
                    </figure>
                </a>
            </div>
            <div class="newcomments-text">
                <div class="title-review-comment">
                    <a href="{{route('review-show', $review->id) }}" class="title dotted-text2">{{ $review->product_name }}</a>
                    <span>{{ date("Y/m/d", strtotime($review->created_at)) }}</span>
                </div>
                <div class="star-name">
                    <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
                    <div class="time">
                        <p>來自 {{ $review->nick_name }}</p> 
                        <p> 於 {{ date("Y/m/d", strtotime($review->created_at)) }}</p>
                    </div>
                </div>
            </div>
            <div class="newcomments-content">
                <span class="dotted-text2">
                    {{ $review->nick_name }}：{!! mb_substr($review->content,0,35,"utf-8") !!}
                    <a href="{{route('review-show', $review->id) }}" class="font-color-blue">
                        ...More
                    </a>    
                </span>
            </div>
        </li>
    @endforeach
</ul>