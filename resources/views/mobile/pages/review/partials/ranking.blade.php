<div id="brand" class=" box-content custom-parts clearfix block-box">
    <div class="title-main-box title-detail-info ">
        <h3 class="common">評論排行</h3>
        {{--<div class="ct-title-main2-right">--}}
            {{--<a href="javascript:void(0)" class="btn btn-asking">顯示全部</a>--}}
        {{--</div>--}}
    </div>
    <div class="ct-inspired-by-your">
        <div class="content-branking">
            <div class="title-content-branking base-btn-gray">
                最多人撰寫商品
                <i class="fa fa-question-circle" aria-hidden="true"></i>
            </div>
            <ul class="ct-item-content-branking">
                @foreach($mostReviewProducts as $product)
                    <?php
                    $review = $product->reviews->first();
                    ?>
                <li class="box-fix-column item item-product-grid item-product-content-branking">
                    <div class="box-fix-with item-product-grid-pars-content-branking-left">
                        <a href="{{route('review-show',$review->id)}}">
                            <figure class="zoom-image thumb-box-border">
                                {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                            </figure>
                        </a>
                        <span class="lank-small lank-small-0{{$loop->iteration}}">{{ $loop->iteration }}</span>
                    </div>
                    <div class="box-fix-auto item-product-grid-pars-content-branking-right">
                        <a href="{{route('review-show',$review->id)}}"
                           class="title-product-item dotted-text2">{{ $product->manufacturer->name }}</a>
                        <span class="icon-star star-0{{$review->ranking}}"></span>
                        <a href="{{route('review-show',$review->id)}}" class="title-product-item dotted-text2">{{ $product->name }}</a>
                        <span>{{ str_limit($review->content , 40 ) }}</span>
                    </div>
                </li>
                @endforeach
                {{--<li class="box-fix-column item item-product-grid item-product-content-branking">--}}
                    {{--<a href="javascript:void(0)"--}}
                       {{--class="btn btn-warning border-radius-2 bnt-see-all-branking">更多銷售排行</a>--}}
                {{--</li>--}}
            </ul>
        </div>
        <div class="content-branking">
            <div class="title-content-branking base-btn-gray">
                最多人回應評論
                <i class="fa fa-question-circle" aria-hidden="true"></i>
            </div>
            <ul class="ct-item-content-branking">
                @foreach($mostCommentReview as $review)
                    <?php
                    $src = '';
                    if (starts_with($review->photo_key, 'http'))
                        $src = $review->photo_key;
                    else
                        $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                    ?>
                    <li class="box-fix-column item item-product-grid item-product-content-branking">
                        <div class="box-fix-with item-product-grid-pars-content-branking-left">
                            <a href="{{route('review-show',$review->id)}}">
                                <figure class="zoom-image thumb-box-border">
                                    <img src="{{ str_replace('http:','',$src) }}" alt="{{$review->title}}">

                                </figure>
                            </a>
                            <span class="lank-small lank-small-0{{$loop->iteration}}">{{ $loop->iteration }}</span>
                        </div>
                        <div class="box-fix-auto item-product-grid-pars-content-branking-right">
                            <a href="{{route('review-show',$review->id)}}"
                               class="title-product-item dotted-text2">{{$review->product_manufacturer}}</a>
                            <span class="icon-star star-0{{$review->ranking}}"></span>
                            <a href="{{route('review-show',$review->id)}}" class="title-product-item dotted-text2">{{$review->product_name}}</a>
                            <span>{{str_limit( $review->content , 40 ) }}</span>
                        </div>
                    </li>
                @endforeach
                {{--<li class="box-fix-column item item-product-grid item-product-content-branking">--}}
                    {{--<a href="javascript:void(0)" class="btn btn-warning border-radius-2 bnt-see-all-branking">更多熱門瀏覽</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</div>