<ul class=" ct-review-comment">
    @foreach ($reviews as $review)
        <li class="clearfix other-review-block">
            <div class="left">
                <a href="{{ route('review-show', $review->id) }}">
                    <figure class="zoom-image thumbnail-img-140 thumb-img thumb-box-border">
                        <?php
                        $src = '';
                        if (starts_with($review->photo_key, 'http'))
                            $src = $review->photo_key;
                        else
                            $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                        ?>
                        <img src="{{ str_replace('http:','',$src) }}" alt="{{ $review->product_name }}">
                    </figure>
                </a>
            </div>
            <div class=" right">
                <div class="title-review-comment">
                    <a href="{{ route('review-show', $review->id) }}"
                       class="title dotted-text2">{{ $review->product_name }}</a></div>
                <ul class="ul-info-review-search">
                    <li class="">
                        <span>投稿日：</span>{{ date("Y/m/d", strtotime($review->created_at)) }}
                    </li>
                    <li class="">
                        <span>投稿人:</span> {{ $review->nick_name }}
                    </li>
                    <li class=""><span
                                class="icon-star-bigger star-0{{  $review->ranking }}"></span></li>
                </ul>
            </div>
            <div class="other-review-content">
                <span class="dotted-text2">
                    {!! mb_substr(Strip_tags(nl2br(e($review->content))),0,35,"utf-8") !!}
                    <a href="{{ route('review-show', $review->id) }}">
                        ...More
                    </a>
                </span>
            </div>
        </li>
    @endforeach
</ul>