<div class="title-main-box">
    <h3 class="common">車友回應</h3>
</div>
<div class="review-block-box">
    @if(count($review->comments))
    <div class=" thumb-box-border box-review-comment box">
        <ul>
            @foreach ($review->comments as $comment)
                <li>
                    <h2 class="form-group">【回應人】{{ $comment->nick_name }}</h2>
                    <div class="box">{{ $comment->content }}</div>
                    <div class="text-right">
                        於{{ $comment->created_at }}。
                    </div>
                </li>
            @endforeach
        </ul>       
    </div>
    @endif

    <div class="box-review-comment">
        <div>
            @php 
                $facebook = false; 
            @endphp
            @if ( $current_customer )
                @if ($current_customer->role_id == \Everglory\Constants\CustomerRole::REGISTERED)
                    <div>
                        <h2 class="btn-label">回應評論請先完成「完整註冊」。</h2>
                    </div>
                    <div class="btn-gap-top">
                        <a class="btn btn-warning btn-full" href="{{route('customer-account-complete')}}" title="完整註冊 - 「Webike-摩托百貨」">完整註冊</a>
                    </div>
                    @php
                         $facebook = true; 
                    @endphp
                @else
                    <form action="{{ URL::route('review-comment-post',$review->id)}}" method="POST">
                        <div class="review-login">
                            <div>
                                <textarea class="form-control" rows="3" required name="content"></textarea>
                            </div>
                            <div class="form-btn">
                                <button type="submit" class="btn btn-danger btn-full">
                                    送出回應
                                </button>
                            </div>
                        </div>
                    </form>
                @endif

            @else
                <div>
                    <h2 class="btn-label">回應評論請先「登入」，並完成「完整註冊」。</h2>
                </div>
                <div class="review-no-login">
                    <div>
                        <a class="btn btn-warning btn-full" href="{{route('login')}}" title="會員登入 - 「Webike-摩托百貨」">會員登入</a>
                    </div>
                    <div>
                        <a class="btn btn-danger btn-full" href="{{route('customer-account-create')}}" title="註冊會員 - 「Webike-摩托百貨」">註冊會員</a>
                    </div>
                </div>
                @php
                    $facebook = true;
                @endphp
            @endif

        </div>
        @if($facebook)
            <div  class="ct-comment-facebook">
                <div  class="fb-comments" data-href="{{ request()->url() }}"
                     data-width="100%" data-numposts="2"></div>
            </div>
        @endif
    </div>
</div>