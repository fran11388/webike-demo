@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/review/index.css') }}">
@stop
@section('middle')
<div class="review-container clearfix">
	<div>
		<h1 class="common">商品評論</h1>
	</div>
	<div class="block">
        @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))
            @include('response.common.ad.banner-simple',['ad_id'=>15])
        @else
            @include('response.common.ad.banner-simple',['ad_id'=>5])
        @endif
	</div>
	<div class="review-block"></div>
	<div class="block-box">
		<div>
			<h3 class="common">評論搜尋</h3>
		</div>
		<div class="review-box">
			<form action="{{ route('review-search','') }}">
				<div class="review-search">
			        <div class="review-search-text">
			            <input type="search" name="q" required class="btn btn-default review-search-view"
			                   placeholder="輸入商品關鍵字...">
			        </div>
			        <div>
			            <button type="submit" class="btn btn-danger border-radius-2 review-search-view">搜尋</button>
			        </div>
				</div>
			</form>
		</div>
	</div>
	<div class="block-box">
		<div>
			<h3 class="common">評論分類</h3>
		</div>
		<div class="link_ui">
			<ul>
				<li>
					<a class="link_ui_a toggle clearfix">
						{{ $customReviewObject->category->name }}({{$customReviewObject->count}})
					</a>
					<ul>
	                    @foreach ($customReviewObject->reviews as $review)
	                        <li class="">
	                            <a href="{{ route('review-search' , [ $review->url_rewrite ]) }}"> {{ $review->parent_name }}
	                                ({{ $review->count }})</a>
	                        </li>
	                    @endforeach
					</ul>
				</li>
				<li>
					<a class="link_ui_a toggle clearfix">
						{{ $riderReviewObject->category->name }}({{$riderReviewObject->count}})
					</a>
					<ul>
	                    @foreach ($riderReviewObject->reviews as $review)
	                        <li class="">
	                            <a href="{{ route('review-search' , [ $review->url_rewrite ]) }}"> {{ $review->parent_name }}
                                                ({{ $review->count }})</a>
	                        </li>
	                    @endforeach
					</ul>
				</li>
				<li>
					<a class="link_ui_a toggle clearfix">
						{{ $genuineReviewObject->category->name }}({{$genuineReviewObject->count}})
					</a>
					<ul>
	                    @foreach ($genuineReviewObject->reviews as $review)
	                        <li class="">
                                <a href="{{ route('review-search' , [ $review->url_rewrite ]) }}?br={{$review->product_manufacturer}}"> 
                                	{{ $review->product_manufacturer }}({{ $review->count }})
                                </a>
	                        </li>
	                    @endforeach
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="block-box">
		<div>
			<h3 class="common">最新評論</h3>
		</div>
		<div class="new-review">
			<ul class="owl-carousel-4"> 
				 @foreach ($newReviews as $review)
                    <?php $href = route('review-show', $review->id) ?>
                    <li class=" item item-product-grid item-product-custom-part-on-sale">
                        <div>
                            <div>
                                <a href="{{ $href }}">
                                    <figure class="zoom-image thumb-box-border">
                                        <?php
                                        $src = '';
                                        if (starts_with($review->photo_key, 'http'))
                                            $src = $review->photo_key;
                                        else
                                            $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                        ?>

                                        <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                    </figure>
                                </a>
                            </div>
                            <div class="item-product-grid-pars-on-sale-right text-left">
                                <span class="dotted-text1">{{ $review->product_manufacturer }}</span>
                                <a href="{{ $href }}" class="title-product-item-top dotted-text2" title="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                    <span class="dotted-text2">{{ $review->product_name }}</span>
                                </a>
                                <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
                            </div>
                        </div>
                        <div class="new-review-content">
                            <a href="{{ $href }}" class="title-product-item dotted-text2">{{ $review->title }}</a>
                            <label class="normal-text dotted-text2">{!! mb_substr(e($review->content),0,35,"utf-8") !!}
                            	<a href="{{ $href }}" class="btn-customer-review-read-more">...More</a>
                            </label>
                        </div>
                    </li>
                @endforeach
			</ul>
		</div>
	</div>
	@include('mobile.pages.review.partials.ranking')

	
	 <div class="block-box">
		<div>
			<h3 class="common">最新回應</h3>
		</div>
		<div class="review-box">
			<ul class="newcomments">
				@foreach ($newComments as $comment)
					<?php $review = $comment->review; ?>
					<li>
						<div class="newcomments-photo">
                            <a href="{{ route('review-show', $comment->review->id)}}">
                                <figure class="zoom-image thumb-box-border">
                                    <?php
                                    $src = '';
                                    if (starts_with($review->photo_key, 'http'))
                                        $src = $review->photo_key;
                                    else
                                        $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                    ?>
                                    <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name }}">
                                </figure>
                            </a>
						</div>
						<div class="newcomments-text">
                            <div class="title-review-comment">
	                            <a href="{{ route('review-show', $comment->review->id) }}" class="title dotted-text2">{{ $review->product_name }}</a>
	                            <span>{{ date("Y/m/d", strtotime($review->created_at)) }}</span>
	                        </div>
                            <div class="star-name">
                                <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
                                <div class="time">
                                    <p>來自 {{ $review->nick_name }}</p> 
                                    <p> 於 {{ date("Y/m/d", strtotime($comment->created_at)) }}</p>
                                </div>
                            </div>
						</div>
                        <div class="newcomments-content">
                        	<span class="dotted-text2">
                        		{{ $review->nick_name }}：{!! mb_substr($comment->content,0,35,"utf-8") !!}
	    	                    <a href="{{ route('review-show', $comment->review->id) }}">
			                        ...More
			                    </a>	
                        	</span>
                        </div>
					</li>
				@endforeach
			</ul>
		</div>
	</div> 
</div>
@endsection
@section('script')
@stop