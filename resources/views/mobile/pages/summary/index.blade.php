@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/summary.css') }}">

    <link rel="amphtml" href="{{config('app.url')}}/amp/{{$url_path}}">

    <style>
        .owl-carouse-advertisement img {
            width: auto !important;
            float: initial !important;
            margin: auto !important;
        }
        .owl-carouse-advertisement a {
            margin: auto !important;
            background: white !important;
            border: 0px !important;
            font-weight: initial !important;
        }
    </style>
@stop
@section('middle')
    @if(isset($current_motor) and $current_motor)
        <h1 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h1>
    @elseif(isset($current_manufacturer) and $current_manufacturer)
        <h1 class="common">{{ $current_manufacturer->name }}</h1>
    @endif
    <?php \Debugbar::startMeasure('render','content-tabs'); ?>
    @include('mobile.pages.summary.partials.content-tabs')
    <?php \Debugbar::stopMeasure('render'); ?>
    @include('mobile.pages.summary.partials.top-info')
    @if($current_manufacturer)
        @if(($current_manufacturer->url_rewrite == 26) or ($current_manufacturer->url_rewrite == 411))
            @include('mobile.pages.summary.partials.brand-movie')
        @endif
    @endif
    <?php \Debugbar::startMeasure('render','subcategory_view'); ?>
    {!! $subcategory_view !!}
    <?php \Debugbar::stopMeasure('render'); ?>
    <?php \Debugbar::startMeasure('render','new-lineup'); ?>
    @include('mobile.pages.summary.partials.new-lineup-ajax')
    <?php \Debugbar::stopMeasure('render'); ?>
    <?php \Debugbar::startMeasure('render','ATS-2'); ?>
    @if(isset($current_category) and !$current_manufacturer)
        @if(isset($advertiser))
            @if(false !== strpos($current_category->mptt->url_path , '1000') )
                <div class="section">
                    <div class="link_ui">
                        <h3 class="common">{{ $summary_title }} 新品牌　</h3>
                        <div>
                            {!! $advertiser->call(26) !!}
                        </div>
                    </div>
                </div>
            @elseif(false !== strpos($current_category->mptt->url_path , '3000') )
                <div class="section">
                    <div class="link_ui">
                        <h3 class="common">{{ $summary_title }} 新品牌</h3>
                        <div>
                            {!! $advertiser->call(27) !!}
                        </div>
                    </div>
                </div>
            @endif
        @endif
    @endif
    {{--@if($current_category and !$current_motor and !$current_manufacturer)--}}
        {{--<!-- slide -->--}}
        {{--<div class="section">--}}
            {{--<div class="link_ui">--}}
                {{--<h3 class="common">促銷活動</h3>--}}
                {{--<div>--}}
                    {{--@if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))--}}
                        {{--@if(false !== strpos($current_category->mptt->url_path , '1000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>16])--}}
                        {{--@elseif(false !== strpos($current_category->mptt->url_path , '3000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>17])--}}
                        {{--@elseif(false !== strpos($current_category->mptt->url_path , '8000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>30])--}}
                        {{--@elseif(false !== strpos($current_category->mptt->url_path , '4000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>29])--}}
                        {{--@endif--}}
                    {{--@else--}}
                        {{--@if(false !== strpos($current_category->mptt->url_path , '1000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>6])--}}
                        {{--@elseif(false !== strpos($current_category->mptt->url_path , '3000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>7])--}}
                        {{--@elseif(false !== strpos($current_category->mptt->url_path , '8000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>30])--}}
                        {{--@elseif(false !== strpos($current_category->mptt->url_path , '4000') )--}}
                            {{--@include('response.common.ad.banner-slideshow',['ad_id'=>29])--}}
                        {{--@endif--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--@endif--}}
    <?php \Debugbar::stopMeasure('render'); ?>
    @if(isset($current_category) or isset($current_manufacturer))
        <?php \Debugbar::startMeasure('render','motor_search'); ?>
        @include('mobile.pages.common.funtions.motor-search')
        <?php \Debugbar::stopMeasure('render'); ?>
    @endif
    <?php \Debugbar::startMeasure('render','hot-brand'); ?>
    @include('mobile.pages.summary.partials.hot-brand')
    <?php \Debugbar::stopMeasure('render'); ?>
    <?php \Debugbar::startMeasure('render','ranking'); ?>
    @include('mobile.pages.summary.partials.ranking')
    <?php \Debugbar::stopMeasure('render'); ?>
    <?php \Debugbar::startMeasure('render','brand-list'); ?>
    @include('mobile.pages.summary.partials.brand-list')
    <?php \Debugbar::stopMeasure('render'); ?>
    <?php \Debugbar::startMeasure('render','banner-small'); ?>
    @include('mobile.pages.common.ad.banner-small')
    <?php \Debugbar::stopMeasure('render'); ?>
@stop
@section('script')
    <script>
        @if($current_motor)
        function addMyBike()
        {
            $('.add-my-bikes-btn').addClass('hide');
            $('.add-my-bikes-disabled').removeClass('hide');
            $.ajax({
                type: 'GET',
                url: "{!! route('customer-mybike-add', $current_motor->url_rewrite) !!}",
                data: {},
                dataType: 'json',
                success: function(result){
                    if(result.success){
                        var ajax_html = '<span class="size-10rem">{!! $current_motor->name . ($current_motor->synonym ? '(' . $current_motor->synonym . ')' : '') !!} 已經加入MyBike清單。若要移除或新增車型，請至<a href="{!! route('customer-mybike') !!}" target="_blank">會員中心修正</a>。</span><br><br>';
                        if(result.messages.length){
                            ajax_html += result.messages.join('<br>');
                        }
                        swal({
                            title: "成功加入Mybike",
                            html:  '<div class="text-left">' + ajax_html + '</div>',
                            type: "success",
                            confirmButtonText: "確定"
                        });
                    }

                }
            });
        }
        @endif
    </script>
@endsection