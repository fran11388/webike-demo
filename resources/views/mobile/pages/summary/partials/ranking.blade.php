@if(count($ranking_sales_products) or count($ranking_popularity_products)  )
    <div class="section clearfix">
        <h3 class="common">{{ $summary_title }} 商品排行</h3>
        <div id="product_ranking" class="box-ct-recommond-fix box-content custom-parts">
            <div class="ct-inspired-by-your col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="content-branking col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="title-content-branking base-btn-gray">
                        銷售商品排名
                        <i class="fa fa-question-circle" aria-hidden="true" title="Webike摩托百貨銷售數據的排名。"></i>
                    </div>
                    <ul class="ct-item-content-branking">
                        @foreach($ranking_sales_products as $product)
                            <?php
                            if($product->_numFound > 1){
                                $link = route('parts') .'?relation='.$product->relation_product_id;
                            }else{
                                $link = route('product-detail' , [ $product->url_rewrite ]);
                            }
                            ?>
                            <li class="box-fix-column item item-product-grid item-product-content-branking">
                                <div class="box-fix-with item-product-grid-pars-content-branking-left">

                                    <a class="thumbnail thumbnail-pin search-list-img zoom-image"
                                       href="{!! $link !!}" target="_blank">
                                        <figure class="zoom-image thumb-box-border">
                                            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                                        </figure>
                                    </a>
                                    <span class="lank-small lank-small-0{{$loop->iteration}}">{{ $loop->iteration }}</span>
                                </div>
                                <div class="box-fix-auto item-product-grid-pars-content-branking-right">
                                    <a href="{!! $link !!}"
                                       class="title-product-item dotted-text2">{{ $product->manufacturer->name }}</a>
                                    <a href="{!! $link !!}" class="title-product-item dotted-text2">{{ $product->name }}</a>
                                </div>
                            </li>
                            @if($loop->iteration == 3)
                                @break
                            @endif
                        @endforeach
                    </ul>
                    <a href="/ranking{{ str_replace('/parts','',getCurrentExploreUrl([])) }}" class="btn btn-warning btn-full bnt-see-all-branking">更多銷售排行</a>
                </div>
                <div class="content-branking col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="title-content-branking base-btn-gray">
                        商品注目<span class="hidden-xs">度</span>排名
                        <i class="fa fa-question-circle" aria-hidden="true" title="Webike摩托百貨商品瀏覽量的排名。"></i>
                    </div>
                    <ul class="ct-item-content-branking">
                        @foreach($ranking_popularity_products as $product)
                            <?php
                            if($product->_numFound > 1){
                                $link = route('parts') .'?relation='.$product->relation_product_id;
                            }else{
                                $link = route('product-detail' , [ $product->url_rewrite ]);
                            }
                            ?>
                            <li class="box-fix-column item item-product-grid item-product-content-branking">
                                <div class="box-fix-with item-product-grid-pars-content-branking-left">

                                    <a class="thumbnail thumbnail-pin search-list-img zoom-image"
                                       href="{!! $link !!}" target="_blank">
                                        <figure class="zoom-image thumb-box-border">
                                            {!! lazyImage( $product->getThumbnail() , $product->full_name )  !!}
                                        </figure>
                                    </a>
                                    <span class="lank-small lank-small-0{{$loop->iteration}}">{{ $loop->iteration }}</span>
                                </div>
                                <div class="box-fix-auto item-product-grid-pars-content-branking-right">
                                    <a href="{!! $link !!}"
                                       class="title-product-item dotted-text2">{{ $product->manufacturer->name }}</a>
                                    <a href="{!! $link !!}" class="title-product-item dotted-text2">{{ $product->name }}</a>
                                </div>
                            </li>
                            @if($loop->iteration == 3)
                                @break
                            @endif
                        @endforeach
                    </ul>
                    <a href="/ranking{{ str_replace('/parts','',getCurrentExploreUrl([])) }}?type=popularity" class="btn btn-warning btn-full bnt-see-all-branking">更多商品注目度排名</a>
                </div>
            </div>
        </div>
    </div>
@endif