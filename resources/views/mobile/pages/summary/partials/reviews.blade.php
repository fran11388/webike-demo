@if (count($reviews))
    <div class="section">
        <div class="">
            <h3 class="common">{{ $summary_title }}商品評論
            @php
                $replace_segments = null;
                if(isset($current_manufacturer)){
                    $replace_segments = ['br' => $current_manufacturer->name];
                }
            @endphp
            <a href="{{modifyReviewSearchUrlSummary($replace_segments)}}" title="{{ $summary_title . ' 全部商品評論' . $tail }}">>> 更多評論</a>
            </h3>
            <div id="product-review" class="box-ct-new-lineup box-content custom-parts clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row">
                        <ul class="col-xs-12 col-sm-12 col-md-12 col-lg-12 owl-carousel-4">
                            @foreach ($reviews as $review)
                                <?php $href = route('review-show', $review->id) ?>
                                <li class="item item-product-grid">
                                    <a href="{{ $href }}" class="title-product-item" title="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                        <div class="box-fix-auto box">
                                            <div class="box-fix-with item-product-grid-pars-on-sale-left">
                                                <figure class="zoom-image thumb-box-border">
                                                    <?php
                                                    $src = '';
                                                    if (starts_with($review->photo_key, 'http'))
                                                        $src = $review->photo_key;
                                                    else
                                                        $src = $review->photo_key ? '//img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                                                    ?>

                                                    <img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                                                </figure>
                                            </div>
                                            <div class="box-fix-auto item-product-grid-pars-on-sale-right text-left">
                                                <div class="title-product-item-top">
                                                    <span class="dotted-text2 force-limit">{{ $review->product_name }}</span>
                                                </div>
                                                <span class="dotted-text1 force-limit font-color-normal">{{ $review->product_manufacturer }}</span>
                                                <span class="icon-star star-0{{ $review->ranking }} no-margin-horizontal"></span>
                                            </div>
                                        </div>

                                        <div class="box-fix-auto cursor-pointer">
                                            <span class="dotted-text2 force-limit font-color-normal">{{ $review->title }}</span>
                                            <label class="normal-text dotted-text4 force-limit font-color-normal cursor-pointer">{!! e($review->content) !!}</label>
                                            <?php /*
                                            <a href="{{ $href }}" class="btn-customer-review-read-more">>> 顯示全部</a>
                                        */ ?>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif