@if(isset($current_motor) and $current_motor)
    <div class="tab_ui mb15">
        <ul class="btn link_btn">
            <li><a href="{{URL::route('motor-top',['url_rewrite' => $current_motor->url_rewrite])}}" title="車型首頁{{$tail}}"><span>車型首頁</span></a></li>
            <li><a href="{{URL::route('motor-service', $current_motor->url_rewrite)}}" title="{{$current_motor->name}}規格總覽{{$tail}}"><span>規格總覽</span></a></li>
            <li><a href="{{URL::route('summary', 'mt/' . $current_motor->url_rewrite)}}" title="改裝及用品{{$tail}}"><span>改裝及用品</span></a></li>
            <li><a href="{{URL::route('motor-review',  $current_motor->url_rewrite)}}" title="商品評論{{$tail}}"><span>商品評論</span></a></li>
        </ul>
        <ul class="btn link_btn">
            @if($genuine_link)
                <li><a href="{{$genuine_link}}" title="正廠零件{{$tail}}" target="_blank"><span>正廠零件</span></a></li>
            @endif
            <li><a href="{{URL::route('motor-video',  $current_motor->url_rewrite)}}" title="車友影片{{$tail}}"><span>車友影片</span></a></li>
            <li><a href="http://www.webike.tw/motomarket/search/fulltext?q={{$current_motor->name}}" title="新車、中古車{{$tail}}" target="_blank">新車、<span>中古車</span></a></li>
        </ul>
    </div>
@elseif(count($current_manufacturer))
    <div class="tab_ui mb15">
        <ul class="btn link_btn">
            <li><a href="{{route('summary', 'br/' . $current_manufacturer->url_rewrite)}}"><span>品牌首頁</span></a></li>
            <li><a href="{{getPartsUrl(['manufacturer' => $current_manufacturer])}}"><span>所有商品</span></a></li>
            <li><a href="{!! route('review-search') !!}?br={{$current_manufacturer->name}}"><span>商品評論</span></a></li>
            @if($manufacturer_info and $manufacturer_info->active)
                <li><a href="{!! route('brand-info', $current_manufacturer->url_rewrite) !!}"><span>品牌介紹</span></a></li>
            @else
                <li><a href="{{getPartsUrl(['manufacturer' => $current_manufacturer])}}?sort=new"><span>最新商品</span></a></li>
            @endif
        </ul>

    </div>
@endif