<div class="section">
    <div>
        <h3 class="common">{{ $summary_title }} 新商品</h3>
        <div class="">
            <?php $page = $new_products->params->page; ?>
            @for($i=1;$i<=$page;$i++)
                <?php $new_products->params->page = $i; ?>
                <ul class="product-list owl-carousel-history" data-owl-ajax="{{json_encode($new_products->params)}}" data-owl-ajax-load="false">
                    @foreach ($new_products->collection[$i] as $product)
                        <li data-sku="{{$product->sku}}">
                            @include('response.common.product.d')
                        </li>
                    @endforeach
                </ul>
            @endfor
        </div>
    </div>
    <div class="text-center block">
        <a class="btn btn-warning" href="{{ modifySummaryUrl(null,true) }}?sort=new" title="{{ $summary_title . ' 全部新商品' . $tail }}">查看全部</a>
    </div>
</div>