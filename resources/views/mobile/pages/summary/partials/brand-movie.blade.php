<div class="section">
    <h3 class="common">{{$current_manufacturer->name}} MOVIE</h3>
    <div class="text-center block">
        @if($current_manufacturer->url_rewrite == 26)
            <iframe width="80%" height="315" src="https://www.youtube.com/embed/Y2RI7MSPiOQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @elseif($current_manufacturer->url_rewrite == 411)
            <iframe width="80%" height="315" src="https://www.youtube.com/embed/wwYK-oxwvF4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        @endif
    </div>
</div>