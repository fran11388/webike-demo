<div class="exp clearfix">
    <?php
    $names = [
        '5' => '非常好',
        '4' => '很好',
        '3' => '普通',
        '2' => '不佳',
        '1' => '糟糕',
    ];
    ?>
    @for ($i = 5 ; $i > 0 ; $i--)
        {{--<tr>--}}
            <?php
            $field = 'ranking_' . $i;
            $avg = 0;
            if ($ranking_avg->counts)
                $avg = floor($ranking_avg->$field * 100 / $ranking_avg->counts);
            ?>
            {{--<td><p class="rating-star-name">{{ $i }}</p></td>--}}
            {{--<td><i class="glyphicon glyphicon-star rating-star-icon" aria-hidden="true"></i></td>--}}
            {{--<td><p class="rating-star-name"></p>{{ $names[$i] }}</td>--}}
            {{--<td><p class="rating-bar-bg"><span class="rating-bar-with" style="width:{{ $avg }}%;"></span></p>--}}
            {{--</td>--}}
            {{--<td><span class="rating-star-number">{{ $ranking_avg->$field }}</span></td>--}}
        {{--</tr>--}}
    @endfor
    <div class="clearfix">
        <div class="brand-image"><img alt="{{ $current_manufacturer->name }}" src="{{ cdnTransform(str_replace('http:','',$current_manufacturer->image)) }}"></div>
        <div class="brand-office">
            @if($current_manufacturer->website)
                <div class="ct-title-main2-right box">
                    <a class="btn btn-warning" href="{{ $current_manufacturer->website }}" title="{{ $current_manufacturer->name . '官方網站' . $tail }}" target="_blank">官方網站</a>
                </div>
            @endif
            <p>{!! $current_manufacturer->description !!}</p>
        </div>
    </div>
    <div class="rating analysis-block"><span>評價總和:</span><span class="icon-star-bigger star-0{{round($ranking_avg->avg)}}"></span><h2>{{ number_format($ranking_avg->avg, 1) }}</h2><span class="refer">({{ $ranking_avg->counts }} 商品評論)</span></div>

</div>