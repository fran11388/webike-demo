@if (!isset($current_manufacturer) or !$current_manufacturer)
    <div class="section">
        <div>
            <h3 class="common">{{ $summary_title }} 熱門品牌</h3>
            <div class="ct-new-custom-part-on-sale-top clearfix">
                <ul class="ul-ct-famous-brand owl-carousel-responsive" data-owl-items="3">
                    @foreach ($hot_manufacturers as $key => $node)

                        <li>
                            <a href="{{modifySummaryUrl(['br'=> $node->url_rewrite ])}}">{!! lazyImage( $node->image , $node->name . '('  . $node->count . ')' )  !!}</a>
                        </li>
                        @if($loop->iteration == 40)
                            @break
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="text-center block">
            <a class="btn btn-warning" href="{{route('brand',request()->only(['category','motor']) )}}" title="{{ $summary_title . ' 全部熱門品牌' . $tail }}">查看全部</a>
        </div>
    </div>
@endif