@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/basic/no-result.css') }}">
@stop
@section('middle')
<div class="clearfix">
    <div class="ct-right-search-list col-xs-12 col-sm-8 col-md-9 col-lg-9">
        <div class="container container-search-nore">
            <div>
                @if ( $keyword )
                <h2>您輸入的關鍵字 "<b class="font-color-red container-search-nore-bold">{{ $keyword }}</b>" 沒有結果。</h2>
                @else
                    <h2>您輸入的關鍵字沒有結果。</h2>
                @endif
                <span class="ct-text-search-nore clearfix">
                    <h2>請查看關鍵字並再次搜尋。</h2>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <i> &#8226; </i>請確認是否有輸入錯字或錯誤<br>
                            <i>	&#8226; </i>或是您用替代的關鍵字再搜尋一次<br>
                            <i>	&#8226; </i>如您查詢的商品為原廠零件，或是我們未上架的商品，請使用以下的方式訂購<br>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <a href="{{URL::route('mitumori')}}" title="未登錄商品查詢購買系統{{$tail}}">
                                <img src="{{ assetRemote('image/banner/banner-mitumori.png') }}" alt="未登錄商品查詢購買系統">
                            </a>
                            <a href="{{URL::route('genuineparts')}}" title="正廠零件查詢購買系統{{$tail}}">
                                <img src="{!! assetRemote('image/oempart/banner02.jpg') !!}" alt="正廠零件查詢購買系統">
                            </a>
                        </div>
                    </div>
                </span>
            </div>
            @include('mobile.pages.list.partials.right-filter')
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>

    <script type="text/javascript">
        var baseUrl= '{{ request()->url() }}';
        var path = '';
        var string = window.location.href;
        if(string.includes("dev/")){
            path = '/dev/index.php';
        }
        $(".select-option-redirect").change(function(){
            window.location = $(this).val();
        });

        function setListMode( value ){
            var d = new Date();
            d.setTime(d.getTime() + (30*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }
        $("a.a-sub2 > span").click(function(){
            window.location =  $(this).parent().attr('data-href');
            return 0;
        });

        {{--$(".select-motor-manufacturer").change(function(){--}}
            {{--var value = $(this).find('option:selected').val();--}}
            {{--if (value){--}}
                {{--var url = path + "/api/motor/displacements?manufacturer=" + value;--}}
                {{--$.get( url , function( data ) {--}}
                    {{--var $target = $(".select-motor-displacement");--}}
                    {{--$target.find("option:not(:first)").remove().end();--}}
                    {{--for (var i = 0 ; i < data.length ; i++){--}}
                        {{--$target.append($("<option></option>").attr("value",data[i]).text(data[i]))--}}
                    {{--}--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}

        {{--$(".select-motor-displacement").change(function(){--}}
            {{--var value = $(this).find('option:selected').val();--}}
            {{--if (value){--}}
                {{--var url = path + "/api/motor/model?manufacturer=" + --}}
                {{--$(this).closest('ul').find('.select-motor-manufacturer option:selected').val() +--}}
                    {{--"&displacement=" +--}}
                    {{--value;--}}

                {{--$.get( url , function( data ) {--}}
                    {{--var $target = $(".select-motor-model");--}}
                    {{--$target.find("option:not(:first)").remove().end();--}}
                    {{--for (var i = 0 ; i < data.length ; i++){--}}
                        {{--$target.append($("<option></option>").attr("value",data[i]['key']).text(data[i]['name']))--}}
                    {{--}--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}

        {{--$(".select-motor-model,.select-motor-mybike").change(function(){--}}
            {{--var value = $(this).find('option:selected').val();--}}
            {{--if (value.length > 0){--}}
                {{--var url = sprintf("{{ modifyPartsUrl([],['mt' => '%s']) }}", value);--}}
                {{--if(url.indexOf('%s') < 0){--}}
                    {{--window.location = url;--}}
                {{--}--}}
            {{--}--}}
        {{--});--}}
    </script>
    <script>
        $(document.body).on('focus', '.input-search-keyword' ,function(){
            var current_suggest_connection = null;
            var _this = $(this)
            _this.autocomplete({
                minLength: 1,
                source: solr_source_search,
                focus: function( event, ui ) {
                    _this.val( ui.item.label );
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'parts');
//                     $( "#search" ).val( ui.item.label );
                    location.href = ui.item.href;
                    return false;
                },
                change: function(event, ui) {
                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }

                return bag
                    .append( '<a  href="'+ item.href +'">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
            function solr_source_search(request, response){

                var params = {q: request.term ,parts:true,url:window.location.pathname};

                current_suggest_connection = $.ajax({
                    url: "{{ route('api-suggest')}}",
                    method:'GET',
                    data : params,
                    dataType: "json",
                    beforeSend: function( xhr ) {
                        if(current_suggest_connection){
                            current_suggest_connection.abort();
                        }
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        });
    </script>
    <script>
        $(document.body).on('submit', '.input-search-keyword-form' ,function(){
            target_url = $(this).attr('action');
            if(target_url){
                target_url = decodeURIComponent(target_url);

                search_value = $(this).find('input[type=text]').val();

                if(search_value){
                    window.location.href = addParameter(target_url,'q',search_value);
                    return false;
                }

            }
        });
    </script>
@stop