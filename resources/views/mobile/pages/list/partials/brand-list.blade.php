<li class="li-left-title select-manufacturer-list {{$current_manufacturer ? 'search-active': ''}}" data="{{$current_manufacturer ? $current_manufacturer->url_rewrite : ''}}">
    @php
        $br_url = null;
        if($current_manufacturer){
            $br_url = $current_manufacturer->url_rewrite;
        }
    @endphp
    <div class="a-sub2 ">
        <ul class="clearfix">
            <li class="ct-table-cell-title title-style">
                <span>
                    商品品牌
                </span>
                @if ( $current_manufacturer)
                    <span class="active">
                        <span>{{ $current_manufacturer->name }}</span>
                        <a href="{{ getCurrentExploreUrl([] , ['br'=>'']) }}">
                            <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                        </a>
                    </span>
                @endif
            </li>
            <li class="ct-table-cell">
                <select class="select2 select-manufacturer btn-label ">
                    <option value="">品牌搜尋</option>
                    @foreach ($manufacturer as $key => $node)
                        <option value="{!! $node->url_rewrite !!}" {{ $br_url == $node->url_rewrite ? 'selected' : '' }}>{{ $node->name }}({{ $node->count }})</option>
                    @endforeach
                </select>
            </li>
        </ul>
    </div>
</li>
