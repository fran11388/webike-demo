@php
    $price_params = explode('-', request()->get('price'));
@endphp
<li class="price-range-block li-left-title clearfix {{((request()->input('sort') == "price") or (request()->input('order') == "asc") or isset($price_params[1])) ? 'search-active' : ''}}">
    <div class="price-range-title title-style">
        價格區間
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1">
            <ul class="ul-sub2 clearfix">
{{--                 <li class="li-sub2 {{ (request()->input('sort') == "price") && !(request()->input('order') == "asc") ? 'active' : ''}}">
                    <a class="{{(request()->input('sort') == "price") && !(request()->input('order') == "asc") == "price" ? 'active' : ''}}">
                        <span id="sort=price">
                            高至低
                        </span>
                    </a>
                </li>
                <li class="li-sub2 {{request()->input('order') == "asc" ? 'active' : ''}}" style="margin-left: 5px">
                    <a class="{{request()->input('order') == "asc" ? 'active' : ''}}">
                        <span id="sort=price&order=asc">
                            低至高
                        </span>
                    </a>
                </li> --}}
                <div class="price-search {{ isset($price_params[1]) ? "search-active" : ""}}">
                    <input id="price_begin" class="price_begin {{ $price_params[0] ? "active" : ""}}" name="price_begin" type="tel" placeholder="最小值" value="{!! (count($price_params) and isset($price_params[0])) ? $price_params[0] : '' !!}" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
                    <span>-</span>
                    <input id="price_end" class="price_end {{ isset($price_params[1]) ? "active" : ""}}" name="price_end" type="tel" placeholder="最大值" value="{!! (count($price_params) and isset($price_params[1])) ? $price_params[1] : '' !!}" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
                </div>
            </ul>
        </li>
    </ul>
</li>