<li class="li-left-title select-motor {{$current_motor ? 'search-active': ''}}" data="{{$current_motor ? $current_motor->url_rewrite : ''}}">
    <div class="ct-table-row row">
        <div class="ct-table-cell-title title-style">
            <span>
                對應車型
            </span>
            @if ( $current_motor)
                <span class="active">
                    <span>{{ $current_motor->name }}</span>
                    <a href="{{ getCurrentExploreUrl([] , ['mt'=>'']) }}">
                        <i class="fa fa-times search-tag-remove" aria-hidden="true"></i>
                    </a>
                </span>
            @endif
        </div>
        <div class="ct-table-cell">
            <ul class="ul-cell-dropdown-list container">
                <li class="li-cell-dropdown-item">
                    <div class="container select-list-box">
                        <select class="select2 select-motor-manufacturer">
                            <option value="">請選擇廠牌</option>
                            @foreach (Ecommerce\Repository\MotorRepository::selectAllManufacturer() as $_manufacturer)
                                <option value="{{ $_manufacturer->url_rewrite }}">{{ $_manufacturer->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </li>
                <li class="li-cell-dropdown-item">
                    <div class="container select-list-box">
                        <select class="select2 select-motor-displacement">
                            <option value="">cc數</option>
                        </select>
                    </div>
                </li>
                <li class="li-cell-dropdown-item">
                    <div class="container select-list-box">
                        <select class="select2 select-motor-model">
                            <option value="">車型</option>
                        </select>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</li>