<div class="clearfix">
	<div class="col-xs-6 btn-gap-bottom btn-gap-top">
	    <a href="{{URL::route('mitumori')}}" title="未登錄商品查詢購買系統{{$tail}}">
	        <img src="{{ assetRemote('image/banner/banner-mitumori-small.png') }}" alt="未登錄商品查詢購買系統">
	    </a>
	</div>
	<div class="col-xs-6 btn-gap-bottom btn-gap-top">
	    <a href="{{URL::route('genuineparts')}}" title="正廠零件查詢購買系統{{$tail}}">
	        <img src="{{ assetRemote('image/oempart/banner01.jpg') }}" alt="正廠零件查詢購買系統">
	    </a>
	</div>
</div>
@foreach ($products_collection as $key_word => $collection)
	<div class="double-search-first">
	    <h2>"<span>{{$key_word}}</span>"</h2>
	    <a href="{{$collection->url}}">查看所有<span>{{$collection->count}}</span>個結果</a>
	</div>
    @php
        if($collection){
            $products = $collection->products->take(4);
        }
    @endphp
	@include('mobile.pages.list.partials.right-product-list')
	
@endforeach

