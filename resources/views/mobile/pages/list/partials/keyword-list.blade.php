<li class="li-keyword-list li-left-title {{request()->input('q') ? 'search-active' : ''}}">
    <div class="keyword-list-title title-style ul-sub2">
        <input type="text" id="search_bar" name="q"  class="search {{request()->input('q') ? 'active' : ''}}" placeholder="請輸入關鍵字" autocomplete="off" value="{{request()->input('q') ? request()->input('q') : ''}}">
    </div>
</li>