<li class="li-category-list li-left-title {{$current_category ? 'search-active' : ''}}" data="{{$current_category ? $current_category->mptt->url_path : ''}}">
    <div class="category-list-title title-style clearfix">
        <a href="javascript:void(0)" class="a-left-title ">
            <span class="ca-text">商品分類</span>
            <div class="category-tag-block parent {{$current_category ? 'active' : ''}}">
               <span class="category-tag-name">{{$current_category ? $current_category->name : ''}}</span>
            </div>
        </a>
    </div>
</li>