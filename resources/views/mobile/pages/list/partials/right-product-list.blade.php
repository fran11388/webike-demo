<div class="row ct-right-below">
    <div class="container">
        <div class="row ct-right-below-body">
            @foreach ($products as $product)
                @include('mobile.common.product.e')
            @endforeach
            @php
                $product = $products->first();
            @endphp
            <div class="product-preview-mode product-item {{(isset($_COOKIE['listMode']) and $_COOKIE['listMode'] == 'product-item-style-2') ? '' : 'product-preview-box'}}" style="display: none;">
                <div class="row">
                    <div class="preview-left">
                        <div class="clearfix">

                        </div>
                    </div>
                    <div class="preview-right">

                    </div>
                </div>
                <div class="clearfix preview-description">
                    <div class="product-info-search-list">
                        <ul class="product-description"></ul>
                    </div>
                    <div class="product-state-search-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>