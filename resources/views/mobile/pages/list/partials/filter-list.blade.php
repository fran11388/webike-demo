<li class="filter-list-block li-left-title clearfix {{(((request()->get('sort') == 'new') or (request()->get('in_stock') == 1))) ? 'search-active' : ''}}">
    <div class="filter-list-title title-style">
        商品排序
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1">
            <ul class="ul-sub2 clearfix">
                <li class="li-sub2 {{request()->get('sort') == 'new' ? 'active' : ''}}">
                    <a class="a-sub2 {{request()->get('sort') == 'new' ? 'active' : ''}}">
                        <span id="sort=new">
                            新上架
                        </span>
                    </a>
                </li>
                <li class="li-sub2 {{ (!(request()->get('sort')) and !(request()->get('in_stock'))) ? 'active' : ''}}">
                    <a class="a-sub2 {{ (!(request()->get('sort')) and !(request()->get('in_stock'))) ? 'active' : ''}}">
                        <span id="">
                            人氣商品
                        </span>
                    </a>
                </li>
                <li class="li-sub2 {{request()->get('in_stock') == 1 ? 'active' : ''}}">
                    <a class="a-sub2 {{request()->get('in_stock') == 1 ? 'active' : ''}}">
                        <span id="in_stock=1">
                            有庫存
                        </span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>