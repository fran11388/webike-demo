<li class="country-list-block li-left-title clearfix {{request()->has('country') ? 'search-active' : ''}}">
    <div class="country-list-title title-style">
        商品性質
    </div>
    <ul class="ul-sub1 row">
        <li class="li-sub1">
            <ul class="ul-sub2 clearfix">
                @foreach ($countries as $node)
                    @if ($node->count)
                        <li class="li-sub2 {{request()->input('country') == $node->key ? 'active' : ''}}">
                            <a class="a-sub2 {{request()->input('country') == $node->key ? 'active' : ''}}">
                                <span id="country={{$node->key}}">
                                    {{-- <input class="checkbox-left-sidebar-li" type="checkbox"> --}}
                                    {{ $node->name }}
                                </span>
                                <span class="count">({{ $node->count }})</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>
    </ul>
    <!--以下為原function by SIAN-->
    {{--<ul class="ul-sub1 row">--}}
        {{--<li class="li-sub1 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<ul class="ul-sub2 row">--}}
                {{--@foreach ($countries as $node)--}}
                    {{--@if ($node->count)--}}
                        {{--<li class="li-sub2 col-md-12 col-sm-12 col-xs-12">--}}
                            {{--<a class="a-sub2 {{request()->input('country') == $node->key ? 'active' : ''}}" href="{{getCurrentExploreUrl([ 'country' => $node->key ,'page' => '' ])}}">--}}
                                {{--<span>--}}
                                    {{-- <input class="checkbox-left-sidebar-li" type="checkbox"> --}}
                                    {{--{{ $node->name }}--}}
                                {{--</span>--}}
                                {{--<span class="count">({{ $node->count }})</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--@endif--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--</li>--}}
    {{--</ul>--}}
</li>