@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/search-new.css') }}">
    <style>
        .ct-right-below-body .product-item-style-2 .product-detail .product-info-search-list {
            padding-left: 20px !important;
        }
        .select-manufacturer-list .select2.select2-container{
            width: 100%!important;
        }
        .ui-menu-item{
          padding: 5px;
        }
            
        @if(isset($_COOKIE['listMode']) and $_COOKIE['listMode'] == 'product-item-style-2')
            .ct-right-below .ct-right-below-body .product-item .product-img .search-list-img {
            height: auto;
        }

        @endif
        .product-detail .product-state-search-list .rating {
            height: 19px !important;
        }
    </style>
@stop
@section('middle')
    <div class="ct-right-search-list">
        <div class="condition-search">
            <h3 class="{{ $keyword ? '' : 'hide' }}">「<span>{{ $keyword }}</span>」的搜索結果</h3>
            @if($current_manufacturer || $current_motor || $current_category)
                <ul>
                    <h4>搜索條件：</h4>
                    @if ( $current_manufacturer)
                        <li>
                            <div class="search-tag">
                                <span>品牌</span>
                                <a href="{{ getCurrentExploreUrl([] , ['br'=>'']) }}">
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>
                            <p class="dotted-text1">{{ $current_manufacturer->name }}</p>

                        </li>
                    @endif
                    @if ( $current_motor)
                        <li>
                            <div class="search-tag">
                                <span>車型</span>
                                <a href="{{ getCurrentExploreUrl([] , ['mt'=>'']) }}">
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>
                            <p class="dotted-text1">{{ $current_motor->name }}</p>

                        </li>
                    @endif
                    @if ( $current_category)
                        <li>
                            <div class="search-tag">
                                <span>分類</span>
                                <a href="{{ getCurrentExploreUrl([] , ['ca'=>'']) }}">
                                <i class="fas fa-times"></i>
                            </a>
                            </div>
                            <p class="dotted-text1">{{ $current_category->name }}</p>
                        </li>
                    @endif
    {{--                 <li>
                        <p></p>
                        <a href="">
                            <i class="fas fa-times"></i>
                        </a>
                    </li> --}}
                    <div class="fixclear"></div>
                </ul>
            @endif
        </div>
        
        <div class="sort-search">
            <div class="sort-button">
                <select name="" id="soflow" >
                    <option value="" disabled selected hidden>商品排序</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'' , 'order'=>'']) }}" {{ $sort == '' ? 'selected' : '' }}>人氣商品</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'new', 'order'=>'']) }}" {{ $sort == 'new' ? 'selected' : '' }}>新上架商品</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'sales' , 'order' => 'asc']) }}" {{ ($sort == 'sales' && $direction == 'asc') ? 'selected' : '' }}>促銷商品</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'price' , 'order' => 'asc']) }}" {{ ($sort == 'price' && $direction == 'asc') ? 'selected' : '' }}>價格低至高</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'price', 'order'=>'']) }}" {{ $sort == 'price' && $direction == '' ? 'selected' : '' }}>價格高至低</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'manufacturer', 'order'=>'']) }}" {{ $sort == 'manufacturer' ? 'selected' : '' }}>品牌名稱</option>
                    <option value="{{ getCurrentExploreUrl(['sort'=>'ranking', 'order'=>'']) }}" {{ $sort == 'ranking' ? 'selected' : '' }}>商品評價</option>
                </select>
            </div>



            <div class="filter-box">
                <div class="filter-box-div">
                    <a class="btn" href="javascript:void(0)">
                    篩選條件
                    </a>
                </div>
                <i class="fa fa-filter"></i>
            </div> 
        </div>




        <div class="title-box clearfix">
            @if($no_result)
                <div class="title-main-box">
                    <h3 class="double-search-title">沒有找到與"<span>{{$keyword}}</span>"相關的結果</h3>
                </div>
            @else
                <div class="title-main-box">
                    <h1>{!! (Route::currentRouteName() == 'outlet') ? 'Outlet' : '' !!}商品一覽</h1>
                </div>
            @endif

            @include('mobile.pages.list.partials.pager')
        </div>
        <div class="container">
            @if(Route::currentRouteName() == 'outlet')
                <div class="box">

                    <?php
                    if (!isset($propagandas)) $propagandas = collect([]);
                    $propaganda = \Ecommerce\Service\Backend\PropagandaService::filterPropagandas($propagandas, \Everglory\Constants\Propaganda\Type::OUTLET_BANNER, \Everglory\Constants\Propaganda\Position::MOBILE_BLADE);
                    ?>
                    @if($propaganda)
                        <img src="{{ $propaganda->pic_path }}">
                    @endif

                </div>
            @endif
            
            @if($no_result)
                @include('mobile.pages.list.partials.double-search')
            @else
                @include('mobile.pages.list.partials.right-product-list')
                @include('mobile.pages.list.partials.pager')
            @endif

        </div>
    </div>
    <div class="ct-search-list-main-menu">
        <aside class="ct-search-list">
            <div class="ct-search-title-block">
                <i class="fa fa-chevron-left back-btn hidden"></i>
                <span class="ct-search-title">篩選</span>
                <i class="fa fa-close delete-btn"></i>
            </div>
            <ul class="ul-category-list">
                <li class="ct-search-title-block">
                    <a href="{{getCurrentExploreUrl([],['ca' => ''])}}">清除分類搜尋</a>
                </li>
            @if ($category and $category->depth > 1 )
                <?php
                $url_path = explode('-', $category->mptt->url_path);
                $target = $tree->where('url_rewrite', $url_path[0])->first();
                if($target){
                    for ($i = 1; $i < count($url_path) - 1; $i++) {
                        $target = $target->nodes->where('url_rewrite', $url_path[$i])->first();
                    }
                    $parent = $target;
                    if($target){
                        $target = $target->nodes->reject(function ($node) {
                            return $node->count == 0;
                        });
                    }else{
                        $target = collect();
                    }
                }else{
                    $target = collect();
                }

                ?>
            @else
                <?php  $target = $tree ?>
            @endif
            @foreach ($target as $key => $node)
                @php
                    $nodes[$node->mptt->url_path] =  $node->nodes;
                @endphp
                <li class="li-left-title" id="{{$node->mptt->url_path}}">
                    <div class="category-second-title title-style slide-down-btn list_child">
                        <i class="fa fa-circle-thin uncheck-btn"></i>
                        <i class="fa fa-check-circle check-btn hidden"></i>
                        <a href="javascript:void(0)" class="a-left-title">
                            <span>
                                {{ $node->name }}({{ $node->count }})
                            </span>
                        </a>
                    </div>
                </li>
            @endforeach
            </ul>
            @if(isset($nodes))
                @foreach($nodes as $key => $nodes)
                    <ul class=" nodes_child_list {{$key}}" id="{{$key}}">
                        @foreach($nodes as $nodes_child)
                            <li class="li-left-title" id="{{$nodes_child->mptt->url_path}}">
                                <div class="nodes_child_list_title title-style slide-down-btn">
                                    <a href="javascript:void(0)" class="a-left-title">
                                        <i class="fa fa-circle-thin uncheck-btn"></i>
                                        <i class="fa fa-check-circle check-btn hidden"></i>
                                        {{ $nodes_child->name }}({{ $nodes_child->count }})
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endforeach
            @endif
            <ul class="ul-left-title">
                @include('mobile.pages.list.partials.keyword-list')
                <!--Event-->
                {{--@include('mobile.pages.list.partials.event-list')--}} <!--特別折扣-->
                <!--Category-->
                @include('mobile.pages.list.partials.category-list')
                <!--Brands-->
                @include('mobile.pages.list.partials.brand-list')
                <!--Motor-->
                @include('mobile.pages.list.partials.motor-select')
                <!--Filter-->
                {{-- @include('mobile.pages.list.partials.filter-list') --}}
                <!--Country-->
                {{-- @include('mobile.pages.list.partials.country-list') --}}
                <!--Price Range-->

                <li class="filter-list-block-background">
                </li>

                @include('mobile.pages.list.partials.price-range-list')

                <li class="clear-search-btn clearfix">
                    <a href="{{route('parts')}}" class="btn btn-warning col-xs-12">清除搜尋條件</a>
                </li>
            </ul>
        </aside>
        <div class="search-button-block">
            <input class="btn btn-danger search-button uncheck-search-button disabled" type="button" value="搜尋">
        </div>
        <div class="ct-search-background"></div>
    </div>
@stop
@section('script')
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>
    <script src="{!! assetRemote('js/pages/searchList/searchList.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if ($(window).width() < 767) {
                $('.product-detail-button').hide();
            } else {
                $('.product-detail-button').show();
            }
        });

        var path = '';
        var string = window.location.href;
        if (string.includes("dev/")) {
            path = '/dev/index.php';
        }

        $('.list-mode').click(function () {
            $('.ct-right-below-body .product-preview-mode').removeClass('product-preview-box');
            $('.ct-right-below .ct-right-below-body .product-item .product-img .search-list-img').css('height', 'auto');
        });

        $('.common-mode').click(function () {
            $('.ct-right-below-body .product-preview-mode').addClass('product-preview-box');
            $('.ct-right-below .ct-right-below-body .product-item .product-img .search-list-img').css('height', '190px');
        });

        var baseUrl = '{{ request()->url() }}';
        $(document).on('change', '.select-option-redirect', function () {
            window.location = $(this).val();
        });

        function setListMode(value) {
            var d = new Date();
            d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = "listMode=" + value + ";" + expires + ";path=/";
        }

        $(document).on('change', ".ct-search-list-main-menu .select-motor-manufacturer", function () {
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value) {
                var url = path + "/api/motor/displacements?manufacturer=" + value;
                $.get(url, function (data) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-displacement");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]).text(data[i]))
                    }
                });
            }
        });

        $(document).on('change', ".ct-search-list-main-menu .select-motor-displacement", function () {
            var value = $(this).find('option:selected').val();
            var _this = $(this);
            if (value) {
                var url = path + "/api/motor/model?manufacturer=" +
                    _this.closest('.ct-table-cell').find(".select-motor-manufacturer").find('option:selected').val() +
                    "&displacement=" +
                    value;

                $.get(url, function (data) {
                    var $target = _this.closest('.ct-table-cell').find(".select-motor-model");
                    $target.find("option:not(:first)").remove().end();
                    for (var i = 0; i < data.length; i++) {
                        $target.append($("<option></option>").attr("value", data[i]['key']).text(data[i]['name']))
                    }
                });
            }
        });

        $(document).on('change', ".ct-search-list-main-menu .select-motor-model", function () {
            var value = $(this).find('option:selected').val();
            if (value) {
                $('.li-left-title.select-motor').addClass('search-active');
                $('.li-left-title.select-motor').attr('data',value);
            }else{
                $('.li-left-title.select-motor').removeClass('search-active');
                $('.li-left-title.select-motor').attr('data',' ');
            }
            searchButtonLock();
        });

    </script>
    <script src="{!! assetRemote('js/pages/searchList/product-preview-box.js') !!}"></script>
    <script src="{!! assetRemote('js/basic/select2-size-reset.js') !!}"></script>
    <script>
        $(document.body).on('focus', '.input-search-keyword', function () {
            var current_suggest_connection = null;
            var _this = $(this)
            _this.autocomplete({
                minLength: 1,
                source: solr_source_search,
                focus: function (event, ui) {
                    _this.val(ui.item.label);
                    return false;
                },
                select: function (event, ui) {
                    ga('send', 'event', 'suggest', 'select', 'parts');
//                     $( "#search" ).val( ui.item.label );
                    location.href = ui.item.href;
                    return false;
                },
                change: function (event, ui) {
                }
            })
                .autocomplete("instance")._renderItem = function (ul, item) {

                var bag = $("<li>");
                if (item.value == 'cut') {
                    return bag.addClass('cut').append('<hr>').appendTo(ul);
                }

                return bag
                    .append('<a  href="' + item.href + '">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>")
                    .appendTo(ul);
            };

            function solr_source_search(request, response) {

                var params = {q: request.term, parts: true, url: window.location.pathname};

                current_suggest_connection = $.ajax({
                    url: "{{ route('api-suggest')}}",
                    method: 'GET',
                    data: params,
                    dataType: "json",
                    beforeSend: function (xhr) {
                        if (current_suggest_connection) {
                            current_suggest_connection.abort();
                        }
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            }
        });
    </script>
    <script>
        $(document.body).on('submit', '.input-search-keyword-form', function () {
            target_url = $(this).attr('action');
            if (target_url) {
                target_url = decodeURIComponent(target_url);

                search_value = $(this).find('input[type=text]').val();

                if (search_value) {
                    window.location.href = addParameter(target_url, 'q', search_value);
                    return false;
                }

            }
        });
    </script>
    <script>
        $(document).on('click', '.price-search-button', function () {
            var params_count = "{!! count(request()->except(['page', 'price', 'motor', 'category', 'brand'])) !!}";
            var price_begin = $("#price_begin").val();
            var price_end = $("#price_end").val();
            if ($('#mm-price_begin').val() && $('#mm-price_end').val()) {
                price_begin = $('#mm-price_begin').val();
                price_end = $('#mm-price_end').val();
            }
            if (price_begin && price_end) {
                var url = "{{getCurrentExploreUrl([ 'price' => null ,'page' => '' ])}}";
                if (parseInt(params_count) > 0) {
                    url = url + '&price=' + price_begin + '-' + price_end;
                } else {
                    url = url + '?price=' + price_begin + '-' + price_end;
                }
                window.location.href = url;
            } else {
                alert('請輸入搜尋金額');
            }

        });
    </script>

    <!--***********以下script by SIAN***************-->

    <script>

        $(document).ready(function () {

           $('.ct-search-list').addClass('fixedtop');

        });

        $('.price-search input[type="tel"]').on('click', function () {

            var client_h = document.documentElement.clientHeight;

            $(window).on("resize",function(){

                var body_h =  document.body.scrollHeight;

                if(body_h < client_h){

                    $('.ct-search-list').removeClass('fixedtop');
                    $('.ct-search-list').addClass('fixedbottom');

                }else{

                    $('.ct-search-list').addClass('fixedtop');
                    $('.ct-search-list').removeClass('fixedbottom');

                }

            });

        });

        $('.ul-cell-dropdown-list .li-cell-dropdown-item').click(function () {

            var client_h = document.documentElement.clientHeight;

            $(window).on("resize",function(){

                var body_h =  document.body.scrollHeight;

                if(body_h < client_h){

                    $('.ct-search-list').addClass('fixedtop');
                    $('.ct-search-list').removeClass('fixedbottom');

                }else{

                    $('.ct-search-list').addClass('fixedtop');
                    $('.ct-search-list').removeClass('fixedbottom');

                }

            });

        })



        $('.filter-box,.helpdesk').click(function () {

            $('.ct-search-list-main-menu').animate({left: "0"});
            $('header').addClass('filterblur');
            $('nav').addClass('filterblur');
            $('footer').addClass('filterblur');
            $('.breadcrumbs').addClass('filterblur');
            $('.ct-right-search-list').addClass('filterblur');
            $('.visible-xs .bannerbar-phone').addClass('hidden');
            var winHeight = $(window).height();

            setTimeout(function () {

                if ($('.ct-search-list-main-menu').css('left') == '0px') {

                    $('body, html').css('overflow', 'hidden');

                }

            }, 500)

        });

        $('.delete-btn').click(function () {

            if ($('.ul-category-list').css('left') == '0px') {
                $('.ul-category-list').animate({left: "100%"});
            }

            if($('.nodes_child_list').css('left') == '0px'){
                $('.nodes_child_list').animate({left: "100%"});
            }

            $('.ul-left-title').animate({right: "0"});

            $('header').removeClass('filterblur');
            $('nav').removeClass('filterblur');
            $('footer').removeClass('filterblur');
            $('.breadcrumbs').removeClass('filterblur');
            $('.ct-right-search-list').removeClass('filterblur');
            $('.ct-search-list-main-menu').animate({left: "100%"});
            $('.visible-xs .bannerbar-phone').removeClass('hidden');

            $('body, html').css('overflow', "");
        });

        $('.filter-list-block .ul-sub1 .li-sub2').click(function () {

            if ($(this).hasClass('active')){

                $(this).removeClass('active');
                $(this).find('a').removeClass('active');
                $('.filter-list-block').removeClass('search-active');
            }else {

                $('.filter-list-block .ul-sub1 .li-sub2').removeClass('active');
                $('.filter-list-block .ul-sub1 .li-sub2 a').removeClass('active');
                $(this).addClass('active');
                $(this).find('a').addClass('active');
                $('.filter-list-block').addClass('search-active');

            }

            searchButtonLock();
        });

        $('.country-list-block .ul-sub1 .li-sub2').click(function () {

            if ($(this).hasClass('active')) {

                $(this).removeClass('active');
                $(this).find('a').removeClass('active');
                $('.country-list-block').removeClass('search-active');

            } else {

                $('.country-list-block .ul-sub1 .li-sub2').removeClass('active');
                $('.country-list-block .ul-sub1 .li-sub2 a').removeClass('active');
                $(this).addClass('active');
                $(this).find('a').addClass('active');
                $('.country-list-block').addClass('search-active');

            }
            searchButtonLock();
        });

        $('.price-range-block .ul-sub1 .li-sub2').click(function () {

            if ($(this).hasClass('active')) {

                $(this).removeClass('active');
                $(this).find('a').removeClass('active');
                if(!$('.price-range-block .price-search').hasClass('search-active')){
                    $('.price-range-block').removeClass('search-active');
                }

            } else {

                $('.price-range-block .ul-sub1 .li-sub2').removeClass('active');
                $('.price-range-block .ul-sub1 .li-sub2 a').removeClass('active');
                $('.price-search span').css('color', '#333333');                
                $(this).addClass('active');
                $(this).find('a').addClass('active');
                $('.price-range-block').addClass('search-active');

            }
            searchButtonLock();
        });


        $('.li-keyword-list .keyword-list-title input#search_bar').keyup(function () {

            if($(this).val().length <= 0){

                $(this).removeClass('active');
                $('.li-keyword-list').removeClass('search-active');
            }else {
                $(this).addClass('active');
                $('.li-keyword-list').addClass('search-active');
            }

            searchButtonLock();
        });

        $('.price-range-block .ul-sub1 .price-search input').click(function () {

            $('.price-range-block .ul-sub1 .price-search input').addClass('active');
            $('.price-search span').css('color', '#e61e25');

        });

        $('.price-search input').blur(function () {

            if($(this).val().length <= 0){

                $('.price-range-block .ul-sub1 .price-search input').removeClass('active');
                $('.price-search span').css('color', '#333333');
                if(!$('.price-range-block .ul-sub2 .li-sub2').hasClass('active')){
                    $('.price-range-block').removeClass('search-active');
                }
                $('.price-range-block .price-search').removeClass('search-active');
            }else {

                $('.price-range-block .ul-sub1 .price-search input').addClass('active');
                $('.price-search span').css('color', '#e61e25');
                $('.price-range-block').addClass('search-active');
                $('.price-range-block .price-search').addClass('search-active');

            }

            searchButtonLock();
        });

        $('.search-button').click(function () {

            if($('.ul-left-title').css('right') == '0px') {

                var SelectManufacturer = $('.ct-search-list-main-menu .select-motor-manufacturer').parent().find('.select2-selection').hasClass('active');
                var SelectCC = $('.ct-search-list-main-menu .select-motor-displacement').parent().find('.select2-selection').hasClass('active');
                var SelectMotor = $('.ct-search-list-main-menu .select-motor-model').parent().find('.select2-selection').hasClass('active');

                if (SelectManufacturer && SelectCC && SelectMotor) {

                    var ValueManufacturer = $('.ct-search-list-main-menu .select-motor-manufacturer').select2("val");
                    var ValueCC = $('.ct-search-list-main-menu .select-motor-displacement').select2("val");
                    var ValueMotor = $('.ct-search-list-main-menu .select-motor-model').select2("val");

                }else if ((SelectManufacturer && !SelectCC && !SelectMotor)) {

                    swal(
                        '錯誤',
                        '請輸入正確車型',
                        'error'
                    );
                    return false;
                }else if (SelectManufacturer && SelectCC && !SelectMotor) {

                    swal(
                        '錯誤',
                        '請輸入正確車型',
                        'error'
                    );
                    return false;

                }

                sort = $('.filter-list-block.search-active .li-sub1 .li-sub2.active span').attr('id');

                country = $('.country-list-block.search-active .ul-sub2 .li-sub2.active span').attr('id');

                priceSort = $('.price-range-block.search-active .li-sub1 .li-sub2.active span').attr('id');

                q = null;
                keyword = $('.li-keyword-list .keyword-list-title input').val();
                if(keyword){
                    q = 'q=' + keyword;
                }

                var PriceMinimum = $('.price-search .price_begin').val();
                var PriceMaximum = $('.price-search .price_end').val();
                priceUrl = null;
                if( parseInt(PriceMinimum) > parseInt(PriceMaximum)){
                    swal(
                        '錯誤',
                        '最小值不可大於最大值',
                        'error'
                    );
                    return false;
                }else if((PriceMinimum.length > 0 && PriceMaximum.length > 0)){
                    priceUrl = "price=" + PriceMinimum + "-" + PriceMaximum;
                }else if((PriceMinimum ^ PriceMaximum)){
                    swal(
                            '錯誤',
                            '請輸入搜尋金額',
                            'error'
                        );
                    return false;
                }

                filter = {'q' : q,'sort' : sort, 'country' : country, 'priceSort' : priceSort, 'priceUrl' : priceUrl};

                var pathUrl = get_urlPath(filter);

                serchLocation(pathUrl);

            }else{

                if ($('.ul-category-list').css('left') == '0px') {
                    $(this).addClass('disabled');

                    if ($('.ul-category-list .uncheck-btn').hasClass('hidden')) {
                       var url = $('.ul-category-list .uncheck-btn.hidden').closest('.li-left-title').attr('id');
                        $('.back-btn').addClass('hidden');
                        $('.ul-left-title').animate({right: "0"});
                        $('.ul-category-list').animate({left: "100%"});
                        $('.search-button').removeClass('uncheck-search-button');
                        $('.search-button').val('搜尋');
                        $('.ct-search-title-block .ct-search-title').text('篩選');
                        $('.li-category-list.li-left-title').addClass('search-active');
                        $('.li-category-list.li-left-title').attr('data',url);
                        $(this).removeClass('disabled');
                    }
                }else{
                    if ($('.nodes_child_list .uncheck-btn').hasClass('hidden')) {
                        var parentUrl = $('.nodes_child_list .uncheck-btn.hidden').closest('.nodes_child_list').attr('id');
                        var url = $('.nodes_child_list .uncheck-btn.hidden').closest('.li-left-title').attr('id');
                        $('.ul-category-list .li-left-title#' + parentUrl + ' .uncheck-btn').addClass('hidden');
                        $('.ul-category-list .li-left-title#' + parentUrl + ' .check-btn').removeClass('hidden');
                        $('.li-category-list.li-left-title').attr('data',url);
                        $('.back-btn').addClass('hidden');
                        $('.ul-left-title').animate({right: "0"});
                        $('.nodes_child_list').animate({left: "100%"});
                        $('.search-button').removeClass('uncheck-search-button');
                        $('.search-button').val('搜尋');
                        $('.ct-search-title-block .ct-search-title').text('篩選/排序');
                        $('.li-category-list.li-left-title').addClass('search-active');
                        $(this).removeClass('disabled');
                    }
                }

                searchButtonLock();
            }
        });

        function get_urlPath(path){

            var i = 0;
            var get_path = [];
            var pathUrl = null;

            $.each(path,function(key,value){
               if(value){
                    if(i == 0){
                        get_path[i] = "?" + value;
                    }else{
                        get_path[i] = get_path[i-1] + "&" + value;
                    }

                    pathUrl = get_path[i];
                    i++;    
               }
            });

            return pathUrl;
        }

        function serchLocation(get_pathUrl){

            url= "{{ route('parts') }}";
            ca_url_path = null;
            mt_url_path = null;
            br_url_path = null;

            if($('.li-category-list.li-left-title').hasClass("search-active")){
                ca_url_path = $('.li-category-list.li-left-title.search-active').attr('data');
            }

            if($('.li-left-title.select-motor').hasClass("search-active")){
                mt_url_path = $('.li-left-title.select-motor.search-active').attr('data');
            }
            
            if($('.li-left-title.select-manufacturer-list').hasClass("search-active")){
                br_url_path = $('.li-left-title.select-manufacturer-list.search-active').attr('data');
            }

            if((mt_url_path  && ca_url_path &&  br_url_path)){ 
                
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['mt' => '%s','ca' => '%s','br' => '%s'])) }}",mt_url_path, ca_url_path, br_url_path);
            }else if(mt_url_path && ca_url_path){
                
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['mt' => '%s','ca' => '%s','br' => ''])) }}",mt_url_path, ca_url_path);
            }else if(ca_url_path && br_url_path){
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['ca' => '%s','br' => '%s','mt' => ''])) }}",ca_url_path, br_url_path);
            }else if(mt_url_path && br_url_path){
                
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['mt' => '%s','br' => '%s','ca' => ''])) }}",mt_url_path, br_url_path);
            }else if(ca_url_path){
                
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['ca' => '%s','mt' => '','br' => ''])) }}", ca_url_path);
            }else if(mt_url_path){
                
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['mt' => '%s', 'ca' => '', 'br' => ''])) }}", mt_url_path);
            }else if(br_url_path){
                
                url = sprintf("{{ preg_replace( '/%(?=[A-Z0-9])/' ,'%%', getCurrentExploreUrl(['limit' => '','q' => '','sort' => '','order' => '', 'price' => '','in_stock' => '','country'=> ''],['br' => '%s', 'ca' => '', 'mt' => ''])) }}", br_url_path);
            }
            if(get_pathUrl){
                window.location = url + get_pathUrl;
            }else{
                window.location = url;
            }

        }

        $('.ul-left-title .li-category-list').click(function () {

            var SelectCategoryTitle = $('.li-category-list .category-list-title a span.ca-text').text()

            $('.back-btn').removeClass('hidden');
            $('.ul-left-title').animate({right: "100%"});
            $('.ul-category-list').animate({left: "0"});    
            $('.ct-search-title-block .ct-search-title').text(SelectCategoryTitle);

            if ($('.ul-category-list .li-left-title .category-second-title').find('.uncheck-btn').hasClass('hidden')) {

                $('.search-button').val('確認選取');

            } else {

                $('.search-button').addClass('uncheck-search-button');
                $('.search-button').val('請勾選');

            }

        });

        $('.back-btn').click(function () {
            if(!$('.li-category-list.li-left-title').hasClass('search-active')){
                $('.check-btn').addClass('hidden');
                $('.uncheck-btn').removeClass('hidden');
                $('.category-tag-name').text("");
                $('.category-tag-block').removeClass('active');
            }

            if( $('.ul-category-list').css('left') == '0px'){
                $('.back-btn').addClass('hidden');
                $('.ul-left-title').animate({right: "0"});
                $('.ul-category-list').animate({left: "100%"});
                $('.search-button').val('搜尋');
                $('.ct-search-title-block .ct-search-title').text('篩選/排序');
                searchButtonLock();
            }else{
                $('.ul-category-list').animate({left: "0"});
                $('.nodes_child_list').animate({left: "100%"});
                $('.ul-category-list').removeClass('hidden');
                $('.ct-search-title-block .ct-search-title').text('商品分類');
                $('.search-button').val('請勾選');
                $('.category-list-title div.child').remove();
            }

        });

        $('.uncheck-btn').click(function () {

            var parentCategoryName = null;

            if( !($('.ul-category-list').css('left') == '0px')){
                var parentCategoryName = $('span.ct-search-title').text();
            }

            var SelectCategoryName = $(this).parent().text();

            $('.check-btn').addClass('hidden');
            $('.uncheck-btn').removeClass('hidden');
            $('.category-list-title div.child').remove();

            if ($(this).parent().find('.check-btn').hasClass('hidden')) {

                $(this).parent().find('.check-btn').removeClass('hidden');
                $(this).addClass('hidden');
                $('.category-tag-block').addClass('active');

                if(parentCategoryName){
                    $('.parent.category-tag-block.active .category-tag-name').text(parentCategoryName);
                    $('.category-tag-block.active').after("<div class='category-tag-block child active'> <span class=' category-tag-name'>" + SelectCategoryName + "</span> </div>");
                }else{
                    $('.parent .category-tag-name').text(SelectCategoryName);
                }

            }

            $('.search-button').removeClass('uncheck-search-button');
            $('.search-button').val('確認選取');
            $('.search-button').removeClass('disabled');

        });

        $('.check-btn').click(function () {

            $(this).addClass('hidden');
            $('.uncheck-btn').removeClass('hidden');
            $('.check-btn').addClass('hidden');
            $(this).parent().find('.uncheck-btn').removeClass('hidden');
            $('.search-button').addClass('uncheck-search-button');
            $('.search-button').val('請勾選');
            $('.li-category-list.li-left-title').removeClass('search-active');
            $('.li-category-list.li-left-title').attr('data',' ');
            $('.category-tag-block').removeClass('active');
            $('.category-list-title div.child').remove();
            $('.category-tag-name').text("");
                
            searchButtonLock();
            
        });

        function searchButtonLock(){
            
            if($('.ul-left-title li').hasClass('search-active')){
                $('.search-button').removeClass('disabled');
                $('.search-button').removeClass('uncheck-search-button');
            }else{
                $('.search-button').addClass('disabled');
                $('.search-button').addClass('uncheck-search-button');
            }
        }

        $(document).on('change', ".ct-search-list-main-menu .select-motor-manufacturer", function () {

            var value = $(this).find('option:selected').val();

            if(value == ""){

                $(this).parent().find('.select2-selection').removeClass('active');
                $(this).parent().find('.select2-selection span').css('color','#333333');

            }else {

                $(this).parent().find('.select2-selection').addClass('active');
                $(this).parent().find('.select2-selection span').css('color','#e61e25');

            }

        });


        $(document).on('change', ".ct-search-list-main-menu .select-motor-displacement", function () {

            var value = $(this).find('option:selected').val();

            if(value == ""){

                $(this).parent().find('.select2-selection').removeClass('active');
                $(this).parent().find('.select2-selection span').css('color','#333333');

            }else {

                $(this).parent().find('.select2-selection').addClass('active');
                $(this).parent().find('.select2-selection span').css('color','#e61e25');

            }

        });

        $(document).on('change', ".ct-search-list-main-menu .select-motor-model", function () {

            var value = $(this).find('option:selected').val();

            if(value == ""){

                $(this).parent().find('.select2-selection').removeClass('active');
                $(this).parent().find('.select2-selection span').css('color','#333333');

            }else {

                $(this).parent().find('.select2-selection').addClass('active');
                $(this).parent().find('.select2-selection span').css('color','#e61e25');
                
            }

        });
            
        $(document).on('change', ".ct-search-list-main-menu .select-manufacturer-list .select-manufacturer", function(){
            
            var value = $(this).find('option:selected').val();
            
            if (value){
                 
                $('.select-manufacturer-list .select2-selection__clear').css('font-size','1.2rem'); 

                $(this).parent().find('.select2-selection').addClass('active');
                $(this).parent().find('.select2-selection span').css('color','#e61e25');
                $('.li-left-title.select-manufacturer-list').addClass('search-active');
                $('.li-left-title.select-manufacturer-list').attr('data',value);

            }else{
                
                $(this).parent().find('.select2-selection').removeClass('active');
                $(this).parent().find('.select2-selection span').css('color','#333333');
                $('.li-left-title.select-manufacturer-list').removeClass('search-active');
                $('.li-left-title.select-manufacturer-list').attr('data',' ');

            }

            searchButtonLock();
        });


        $(document).ready(function(){

            $('.ct-search-list-main-menu .select-manufacturer-list .select-manufacturer').select2({
                placeholder: '品牌搜尋',
                allowClear: true
            });

            var height = $(window).height();
            if(height <= 568){
                $('.ct-search-list-main-menu .ul-left-title').css('height',height - 104);
            }

            $('.ul-category-list li').each(function(){
                ca_list_li_id = $(this).attr('id');
                ca_list_child = $('.nodes_child_list.' + ca_list_li_id + ' li.li-left-title').attr('id');
                if(!ca_list_child){
                    $(this).find('.category-second-title a').removeClass('a-left-title');
                    $(this).find('.category-second-title').removeClass('list_child');
                }
            });

            $('.ul-category-list .category-second-title .a-left-title').click(function(){
                var category = $(this).closest('li.li-left-title').attr('id');
                var CategoryTitle = $(this).find('span').text()
                $('.ul-category-list').animate({left: "100%"});
                $('.nodes_child_list.' + category).animate({left: "0"});
                $('.ct-search-title-block .ct-search-title').text(CategoryTitle);
                
            });
            var ca_url = $('.ct-search-list-main-menu .li-category-list').attr('data');
            
            if(ca_url){
                $('.ul-category-list .li-left-title#' + ca_url + ' .uncheck-btn').addClass('hidden');
                $('.ul-category-list .li-left-title#' + ca_url + ' .check-btn').removeClass('hidden');
            }


            var br_url = "{{$current_manufacturer}}";

            if(br_url){

                $('.select-manufacturer-list .select2-selection__clear').css('font-size','1.2rem'); 

                $('.select-manufacturer').parent().find('.select2-selection').addClass('active');
                $('.select-manufacturer').parent().find('.select2-selection span').css('color','#e61e25');
                $('.li-left-title.select-manufacturer-list').addClass('search-active');
            }


            $('#search .select-search-container .select2.select2-container.select2-container--default').css('width','100%');
            $('.inner .select2.select2-container.select2-container--default').css('width','100%');
        });


    </script>
    <script>

        $(function() {
            $( "#search_bar" ).autocomplete({
                minLength: 1,
                source: solr_source,
                focus: function( event, ui ) {
                    $('#search_bar').val();
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'header');
                    return false;
                },
                change: function(event, ui) {

                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }
                
                return bag
                // .append( '<a href="http://localhost/test">' + item.icon + "<p>" + item.label + "</p></a>" )
                    .append( '<a  href="javascript:void(0)" class="search-keyword">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
        });

        
        $(document).on('click', ".ui-menu li.ui-menu-item  a.search-keyword", function(){
            var keyword = $(this).find('span.font-normal').text();
            $('.li-keyword-list #search_bar').val(keyword);
        });

        var current_suggest_connection = null;
        function solr_source(request, response){
            var params = {q: request.term};

            current_suggest_connection = $.ajax({
                url: "{{ route('api-suggest')}}",
                method:'GET',
                data : params,
                dataType: "json",
                beforeSend: function( xhr ) {
                    if(current_suggest_connection){
                        current_suggest_connection.abort();
                    }
                },
                success: function(data) {
                    response(data);
                }
            });
        }

        $(document).on('change', '#soflow', function(){
            window.location = $(this).val();
        });

    </script>
@stop