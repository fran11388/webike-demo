<div class="page-block " id="BrandDouble">
	<h2 class="page-block-title">{!! $title !!}</h2>
	<div class="page-block-product">
		<div class="block-product">
			<ul class="clearfix ul-list">
				@foreach($brandDouble_datas as $double_brand_url_rewrite => $double_brand)
					<li class="product-list li-list" data="{{$double_brand_url_rewrite}}">
						<div class="loop-btn">+</div>
						<a href="{{route('parts', 'br/'.$double_brand_url_rewrite).'?'.$rel_parameter}}">
							<div class="product-img">
								<img src="{{$double_brand['brand_image']}}">
							</div>
						</a>
						<div class="product-text text-center">
							<span>點數{{$week_active ? $double_brand['offer'] .'倍':'加倍'}}回饋</span>
						</div>
						<div class="product-tag">
							@foreach($double_brand['ca_name'] as $double_ca_key => $double_categorie)
								@php
									$path = explode('-',$double_categorie->url_path);
									$tag_class = 'label-primary';
									switch ($path['0']) {
										case '3000':
											$tag_class = 'label-primary';
										break;
										case '1000':
											$tag_class = 'label-danger';
										break;
										case '8000':
											$tag_class = 'label-warning';
										break;
										case '4000':
											$tag_class = 'label-warning';
										break;
										default:
											$tag_class = 'label-primary';
										break;
									}
								@endphp
								<div class="dotted-text">
									<span class="tag label {{$tag_class}} {{$double_ca_key == 2 ? 'hidden': ' ' }}">{{$double_categorie->name}}</span>
								</div>
							@endforeach
						</div>
					</li>
				@endforeach 
			</ul>
		</div>
	</div>
</div>
<div class="brand_product_image">
	<ul class="clearfix">
		@foreach($brandDouble_datas as $double_brand_url_rewrite => $double_brand)
			<li id="{{$double_brand_url_rewrite}}" class="loop-photo hidden">
				<h2>{{$double_brand['brand_name']}}</h2>
				<span class="cancel">X</span>
				<ul class="owl-carouse-product-loop">
					@if($double_brand['product_image'])
						@foreach($double_brand['product_image'] as $double_image_key => $double_product_image)
							<li class="{{$double_image_key == 0 ? ' ': 'hidden'}}">
								<img src="{{ $double_product_image ? $double_product_image : NO_IMAGE}}">
							</li>
						@endforeach
					@else
						<li>
							<img src="{{NO_IMAGE}}">
						</li>
					@endif
				</ul>
			</li>
		@endforeach
	</ul>
</div>