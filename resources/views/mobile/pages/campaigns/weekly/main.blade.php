<style type="text/css">
	.banner-container .banner .banner-btn .show-theme:before{
	    font-size: 0.85rem;
	    position: absolute;
	    right: 40%;
	    top: 40%;
	    color: {!! $main_color_text !!};/*+號顏色，同主題文字色*/
	}
	.banner-container .banner .banner-btn .show-theme{
		width: 55px;
		height: 55px;
		position: relative;
		border-radius: 999em;
		background: rgb( {!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!});/*+號背景色，同主題色*/
	}
	.banner-container .banner{
		position: relative;
	}
	.banner-container .banner .banner-btn{
		position: absolute;
		bottom: 10px;
		right: 10px;
	}
	.banner-container .banner .close-banner-btn{
		position: absolute;
		top: 10px;
		right: 10px;
	}
	.banner-container .banner .theme-block{
		position: absolute;
    	top: 250;
    	background: rgba({!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!}, 0.6);/*主題文字背景，同主題色*/
    	color: {!! $main_color_text !!};
    	min-height: 100%;
    	padding: 10px 20px;
	    z-index: 1;
	}
	.banner-container .banner .theme-block .theme-text{
		width: 80%;
	}
	.banner-container .banner .theme-block .close-banner-btn .close-theme{
		background: #000000a6;
	    border: 1px solid #000000a6;
	    color: #ffffff;
	    border-radius: 99em;
	    width: 55px;
	    height: 55px;
	    font-size: 0.85rem;
	}
	.banner-container .banner .theme-block .close-banner-btn .close-theme:before{
		position: absolute;
		top: 40%;
		right: 36%;
	}
</style>
<div class="banner-container">
    <div class="banner block-container">
        <a href="javascript:void(0)">
            <img src="{!! $mobile_image[0] !!}">
        </a>
        <div class="banner-btn hidden">
    		<i class="fa fa-plus show-theme" aria-hidden="true"></i>
        	<!-- <input type="button" name="theme" value="+"> -->
        </div>
        <div class="theme-block">
        	<div class="theme-text">
	        	<h2 class="size-15rem box">{!! $promotion_title !!}</h2>
	        	<span>{!! $promotion_introduce !!}</span>
        	</div>
        	<div class="close-banner-btn">
        		<i class="glyphicon glyphicon-remove close-theme"></i>
        		<!-- <input type="button" name="theme" value="x">	 -->
        	</div>
        </div>
    </div>
</div>
