<style type="text/css">
		.search{
		background: rgb( {!! $main_color_r !!}, {!! $main_color_g !!}, {!! $main_color_b !!});/*搜尋背景色，同主題色*/
		padding: 10px;
		position: relative;
		z-index: 1;
		width: 100%;
	}
	.search .search-text{
		border-radius: 25px 0 0 25px;
		background: #fff;
		border: none;
		width: auto;
		padding: 10px;
		overflow: hidden;
	}
	.search .search-text input{
		border: 0;
		width: 100%;
	}
	.search .search-btn{
		float: right;
	}
	.search .search-btn input{
		font-family: FontAwesome, 'メイリオ';
		background: #910000;
		font-weight: bold;
		color: #fff;
		border: 0;
		padding: 11px;
		cursor: pointer;
		border-radius: 0 25px 25px 0;
	}
</style>
<div class="search">
	<div class="container">
		<div class="search-btn">
			<input type="submit" value="">			
		</div>
		<div class="search-text">
			<input id="search_bar" type="text" name="search" autocomplete="off" placeholder="請輸入關鍵字(54萬項商品、1840家廠牌)">
		</div>
	</div>
</div>