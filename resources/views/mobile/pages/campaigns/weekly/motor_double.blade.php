<style type="text/css">
#MotorDouble.motor .model_search .button_ui input.finish{
	color: {!! $anchor_color !!}!important;
	background: {!! $anchor_background_color !!};/*車型搜尋按鈕，同錨點顏色*/
}
@foreach($tag_colors as $tag_sort => $tag_color)
	.motor .product_area ul li:nth-child({!! $tag_sort+1 !!})  .text-tag{
		line-height: 30px;
		width: 100%;
		background: {!! $tag_color !!};
	}
@endforeach
</style>
<div class="page-block motor" id="MotorDouble">
	<h2 class="page-block-title">{!! $title !!}</h2>
	<div class="page-block-product">
		<div class="block-product">
			<div class="product_area">
				<div>
					<ul class="clearfix owl-carouse-advertisement-loop owl-carousel-promo">
						@foreach($url_rewrites as $motor_sort => $url_rewrite)
							<li class="">
								<a href="{{ route('parts','mt/'.$url_rewrite).'?'.$rel_parameter}}" target="_blank" class="zoom-image"><img src="{{$images[$motor_sort]}}" class="motor-img"></a>	
								<div class="text-center">
									<span class="size-10rem">{{$names[$motor_sort]}}</span>
								</div>
								@if($week_active)
									<div class="text-center">
										<span class=" size-10rem label text-tag">{{$tag_text[$motor_sort]}}</span>
									</div>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
				@include('mobile.pages.benefit.sale.weekly.otherblock.motor-function')
			</div>
		</div>
	</div>
</div>