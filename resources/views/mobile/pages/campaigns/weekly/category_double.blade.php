<style type="text/css">
@foreach($tag_colors as $tag_sort => $tag_color)
	.category .product_area ul li:nth-child({!! $tag_sort+1 !!}) .text-tag{
		line-height: 30px;
		width: 100%;
		background: {!! $tag_color !!};
	}
@endforeach
</style>
<div class="page-block category" id="CategoryDouble">
	<h2 class="page-block-title">{!! $title !!}</h2>
	<div class="page-block-product">
		<div class="block-product">
			<div class="product_area">
				<ul class="clearfix owl-carouse-advertisement-loop owl-carousel-promo">
					@foreach($url_rewrites as $category_sort => $url_rewrite)
						<li class="text-center">
							<a href="{{ route('parts','ca/'.$url_rewrite).'?'.$rel_parameter}}" target="_blank" class="zoom-image">
								<div class="vertical-align">
									<img src="{!! $images[$category_sort] !!}">
								</div>
								<span class="vertical-align"></span>
							</a>	
							<div class="text-center">
								<span class="size-10rem">{{$names[$category_sort]}}</span>
							</div>
							@if($week_active)
								<div class="text-center">
									<span class=" size-10rem label text-tag">{{$tag_text[$category_sort]}}</span>
								</div>
							@endif
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>