@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/benefit/sale/2019-weekly.css') }}">
<link rel="stylesheet" href="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.css') }}">
<style type="text/css">
	.promotion-container .owl-prev,.promotion-container .owl-next{
		background-color: rgb( 5, 48, 125)!important;/*owl 背景顏色，同主題色*/
		color: #ffffff;/*owl箭頭顏色，同主題文字色*/
	}
	.page-block .page-block-title{
		text-align: center;
		font-size: 18px;
		font-weight: bold;
		background: rgb( 5, 48, 125);/*title 背景顏色，同主題色*/
		padding: 10px 4px;
		line-height: 1.2;
		color: #ffffff;/*title 文字顏色，同主題文字色*/
		margin-top: 10px;
	}
	.page-block .text-tag{
		background: #ff0000;
		line-height: 30px;
		width: 100%;
	}
</style>
@section('middle')
@include('mobile.pages.benefit.sale.weekly.otherblock.main-banner')
@include('mobile.pages.benefit.sale.weekly.otherblock.search-bar')
<div class="promotion-container">
	@include('mobile.pages.benefit.sale.weekly.otherblock.link-bar')
	@include('mobile.pages.benefit.sale.weekly.otherblock.weekly-sale')
	@include('mobile.pages.benefit.sale.weekly.otherblock.double-point')
	@include('mobile.pages.benefit.sale.weekly.otherblock.product-image')
	@include('mobile.pages.benefit.sale.weekly.otherblock.category')
	@include('mobile.pages.benefit.sale.weekly.otherblock.motor')
	@include('mobile.pages.benefit.sale.weekly.otherblock.collection')
</div>
@stop
@section('script')
<script src="{!! assetRemote('js/pages/searchList/sprintf.js') !!}"></script>
<script src="{{ assetRemote('plugin/jquery-ui/jquery-ui.min.js') }}"></script>
	<script type="text/javascript">
		function slipTo1(element){
            var y = parseInt($(element).offset().top) - 104;
            console.log(y);
            $('html,body').animate({scrollTop: y}, 400);
        }
	</script>
    <script>

        $(function() {
            $( "#search_bar" ).autocomplete({
                minLength: 1,
                source: solr_source,
                focus: function( event, ui ) {
                    $('#search_bar').val();
                    return false;
                },
                select: function( event, ui ) {
                    ga('send', 'event', 'suggest', 'select', 'header');
                    return false;
                },
                change: function(event, ui) {

                }
            })
                .autocomplete( "instance" )._renderItem = function( ul, item ) {

                var bag = $( "<li>" );
                if( item.value == 'cut' ){
                    return bag.addClass('cut').append('<hr>').appendTo( ul );
                }
                
                return bag
                // .append( '<a href="http://localhost/test">' + item.icon + "<p>" + item.label + "</p></a>" )
                    .append( '<a  href="javascript:void(0)" class="search-keyword">' + item.icon + '<span class="label size-10rem font-normal font-color-normal">' + item.label + "</span></a>" )
                    .appendTo( ul );
            };
        });

        
        var current_suggest_connection = null;
        function solr_source(request, response){
            var params = {q: request.term};

            current_suggest_connection = $.ajax({
                url: "{{ route('api-suggest')}}",
                method:'GET',
                data : params,
                dataType: "json",
                beforeSend: function( xhr ) {
                    if(current_suggest_connection){
                        current_suggest_connection.abort();
                    }
                },
                success: function(data) {
                    response(data);
                }
            });
        }
       
        $(document).on('click', ".ui-menu li.ui-menu-item", function(){
            var keyword = $(this).find('span.font-normal').text();
            $('.search-text #search_bar').val(keyword);
        });

        $('.search .search-btn input').click(function(){
        	keyword = $('.search-text #search_bar').val();
        	if(keyword){
        		window.location = '{{route('parts')}}' + '?q=' + keyword;
        	}
        });

    </script>
    <script type="text/javascript">
    	
    	$('.owl-carouse-product-loop').addClass('owl-carousel').owlCarousel({
			autoplayTimeout:2000,
			autoplayHoverPause:true,
			loop:true,
			margin:10,
			nav:false,
		    slideBy : 1,
		    center: true,
		    autoplay: false,
			autoPlaySpeed: 500,
			dots: true,
			autoplayHoverPause: false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});

    	$('.loop-btn').click(function(){
			brand_url_rewrite =  $(this).closest('li.product-list').attr('data');
			$('.brand_product_image li.loop-photo#' + brand_url_rewrite).removeClass('hidden');
			$('.brand_product_image li.loop-photo#' + brand_url_rewrite).closest('div.brand_product_image').addClass('active');
			$('body, html').css('overflow', 'hidden');
			$('nav.fixed').addClass('hidden');
		  	$('.brand_product_image li.loop-photo#' + brand_url_rewrite).addClass('active');
		  	$('header').addClass('visibility-hidden');
			setTimeout(function(){ 
			  		$('.brand_product_image li.loop-photo.active .owl-carouse-product-loop li').removeClass('hidden');
			}, 500);
		});

		$('.loop-photo  .cancel').click(function(){
			$(this).closest('li.loop-photo').addClass('hidden').removeClass('active');
			$('.brand_product_image').removeClass('active');
			$('nav.fixed').removeClass('hidden');
			$('header').removeClass('visibility-hidden');
			$('body, html').css('overflow', "");
		});
    </script>
    <script>
		$('.owl-carouse-advertisement-loop').addClass('owl-carousel').owlCarousel({
		autoplay:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		loop:true,
		margin:10,
		nav:true,
	    slideBy : 3,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:3
			}
		}
	});

	</script>
	<script>
		$(document).ready(function(){
		$('.owl-carousel-promo').addClass('owl-carousel').owlCarousel({
			loop:true,
			nav:true,
			margin:5,
			slideBy : 6,
			URLhashListener:true,
			startPosition: 'URLHash',
			responsive:{
				0:{
					items:2
				},
				600:{
					items:3
				},
				1000:{
					items:5
				}
			}
		});

		$('.owl-carousel-promo').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>')
		$('.owl-carousel-promo').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>')
	});
	</script>
	<script type="text/javascript">

		$(document).ready(function(){
			$('.banner-container .block-container .theme-block').animate({top: "0"},1000);
		});

		$('.banner-container .block-container .banner-btn').click(function(){
			$(this).addClass('hidden');
			$(this).closest('.block-container').find('.theme-block').animate({top: "0"},600);
		});


		$('.banner-container .block-container .theme-block .close-banner-btn').click(function(){
			$(this).closest('.theme-block').animate({top: "200"},600);
			$('.banner-container .block-container .banner-btn').removeClass('hidden');
		});
	</script>
	<script type="text/javascript">
		 $(document).on('change', ".select-motor-model,.select-motor-mybike", function(){
            var value = $(this).find('option:selected').val();
            if (value){
		 		$('#motor .model_search .button_ui input').addClass('finish');
            }else{
            	$('#motor .model_search .button_ui input').removeClass('finish');
            }
        });
		 $(document).on('change', ".select-motor-manufacturer,.select-motor-displacement", function(){
        	$('#motor .model_search .button_ui input').removeClass('finish');            
        });
	</script>
@stop