<style type="text/css">
	.promotion-container .links-bar-container {
        width:100%;
        top: 0;
        z-index: 10;
    }
    .promotion-container .links-bar-container .links-bar {
        width:100%;
        overflow: scroll;
    }
    .promotion-container .links-bar-container .links-bar ul {
        width: 1100px;
	}
	.promotion-container .links-bar-container .links-bar ul li:last-child{
		border-right: none;
	}
	 .promotion-container .links-bar-container .links-bar ul li{
	    float: left;
	    width: 25%;
	    background-color: #cfe2f3;/*錨點背景色*/
	    border-right: 2px solid rgb( 5, 48, 125);/*錨點邊線色，同主題色*/
	    box-sizing: border-box;
	    text-align: center;
	    display: -webkit-box;
	    display: -ms-flexbox;
	    display: -webkit-flex;
	    display: flex;
	    -webkit-box-align: center;
	    -ms-flex-align: center;
	    -webkit-align-items: center;
	    align-items: center;
	    padding: 7px;
	    font-size: 1rem;
	}
	 .promotion-container .links-bar-container .links-bar ul li a {
	    font-weight: bold;
	    color: #000000;/*錨點文字顏色*/
	    text-decoration: none;
	    margin: 0 auto;
	    line-height: 1.3;
	    padding: 5px 0 8px;
	}
</style>
<div class="links-bar-container">
	<div class="links-bar">
		<ul class="clearfix">
			<li onclick="slipTo1('#week_sale')">
				<a href="javascript:void(0)">
					熱血風格品牌	
				</a>
			</li>
			<li onclick="slipTo1('#double-point')">
				<a href="javascript:void(0)">
					當週加倍品牌
				</a>
			</li>
			<li onclick="slipTo1('#category')">
				<a href="javascript:void(0)">
					仿賽熱門分類
				</a>
			</li>
			<li onclick="slipTo1('#motor')">
				<a href="javascript:void(0)">
					當週加倍車型
				</a>
			</li>
		</ul>
	</div>
</div>