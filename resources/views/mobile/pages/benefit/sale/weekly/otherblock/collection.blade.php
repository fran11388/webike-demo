	<div class="page-block collection">
		<h2 class="page-block-title">推薦特輯</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix owl-carouse-advertisement-loop owl-carousel-promo">
					{{-- @foreach($collections as $key => $collection)
						<li class="collection-list">
							<div class="collection-list-product">
								<a href="{{$collection->link.'?'.$rel_parameter}}" title="{{$collection->name.$tail}}" target="_blank" class="clearfix">
									<div class="collection-img box">
										<img src="{!! $collection->banner !!}" alt="{{$collection->name.$tail}}">
									</div>
								</a>
								<div class="collection-text">
									<span>{{$collection->meta_description}}</span>
								</div>
							</div>
						</li>
					@endforeach --}}
					<li class="collection-list">
						<div class="collection-list-product">
							<a href="https://www.webike.tw/collection/category/specialprice" title="price" target="_blank" class="clearfix">
								<div class="collection-img box">
									<img src="https://img.webike.tw/assets/images/collection/specialprice_980.jpg" alt="price">
								</div>
							</a>
							<div class="collection-text">
								<span>精選騎士用品、熱門改裝零件，價格再定！Webike要你收穫滿滿，備齊所有騎士配備！</span>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>