<style type="text/css">
	#motor .page-block-product .product_area ul.owl-carouse-advertisement-loop li{
		max-width: 600px;
		max-height: 600px;
	}
	#motor .model_search{
		margin: 15px 10px;
	}
	#motor .model_search .button_ui input.finish{
		background: #cfe2f3;/*車型搜尋按鈕，同錨點顏色*/
	}
</style>
<div class="page-block " id="motor">
	<h2 class="page-block-title">當週加倍車型</h2>
	<div class="page-block-product">
		<div class="block-product">
			<div class="product_area">
				<div>
					@php
						$motors = [
							'CBR1000RR' => ['image' => 'https://img.webike.tw/imageUpload/15472017151547201715761.jpg','link' => '303'],
							'YZF-R1' => ['image' => 'https://img.webike.tw/imageUpload/15472016621547201662395.jpg','link' => '926'],
							'GSX-R1000' => ['image' => 'https://img.webike.tw/imageUpload/15472016461547201646981.jpg','link' => '672'],
							'ZX-10R' => ['image' => 'https://img.webike.tw/imageUpload/15472016331547201633174.jpg','link' => '465'],
							'YZF-R6' => ['image' => 'https://img.webike.tw/imageUpload/15472016751547201675863.jpg','link' => '896'],
							'CBR600RR' => ['image' => 'https://img.webike.tw/imageUpload/15472017021547201702466.jpg','link' => '256']
						];
					@endphp
					<ul class="clearfix owl-carouse-advertisement-loop owl-carousel-promo">
						@foreach($motors as $motor_name => $motor)
							<li class="">
								<a href="{{ route('summary','mt/'.$motor['link'].'?'.$rel_parameter)}}" target="_blank" class="zoom-image"><img src="{{$motor['image']}}" class="motor-img"></a>	
								<div class="text-center">
									<span class="size-10rem">{{$motor_name}}</span>
								</div>
								<div class="text-center">
									<span class=" size-10rem label text-tag">點數2倍</span>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
				@include('mobile.pages.benefit.sale.weekly.otherblock.motor-function')
			</div>
		</div>
	</div>
</div>