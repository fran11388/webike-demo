<style type="text/css">
	.banner-container .banner .banner-btn input{
		background: rgb( 5, 48, 125);/*+號背景色，同主題色*/
	    border: 1px solid rgb( 5, 48, 125);/*+號背景色，同主題色*/
	    color: #ffffff;/*+號顏色，同主題文字色*/
	    border-radius: 99em;
	    font-size: 1.5rem;
	    width: 55px;
	    height: 55px;
	}
	.banner-container .banner{
		position: relative;
	}
	.banner-container .banner .banner-btn{
		position: absolute;
		bottom: 10px;
		right: 10px;
	}
	.banner-container .banner .close-banner-btn{
		position: absolute;
		top: 10px;
		right: 10px;
	}
	.banner-container .banner .theme-block{
		position: absolute;
    	top: 200;
    	background: rgba(5, 48, 125, 0.6);/*主題文字背景，同主題色*/
    	color: #fff;
    	min-height: 100%;
    	padding: 10px 20px;
	    z-index: 1;
	}
	.banner-container .banner .theme-block .theme-text{
		width: 80%;
	}
	.banner-container .banner .theme-block .close-banner-btn input{
		background: #000000a6;
	    border: 1px solid #000000a6;
	    color: #ffffff;/*x顏色*/
	    border-radius: 99em;
	    font-size: 1.5rem;
	    width: 55px;
	    height: 55px;
	}
</style>
<div class="banner-container">
    <div class="banner block-container">
        <a href="javascript:void(0)">
            <img src="{!! assetRemote('image/benefit/big-promotion/2019/suzuki-gsxr1000.jpg') !!}">
        </a>
        <div class="banner-btn hidden">
        	<input type="button" name="theme" value="+">
        </div>
        <div class="theme-block">
        	<div class="theme-text">
	        	<h2 class="size-15rem box">仿賽跑車專屬週</h2>
	        	<span>如賽車般的外觀，低趴的姿勢，跨上車就進入戰鬥狀態。仿賽車顧名思義，為在封閉場地比賽的賽車的市售版本，通常也是各家工藝技術的集大成。製造商取自在賽事中獲得的經驗.，並將配備下放至一般市售車種，成為戰鬥氣息滿點的仿賽車。本週我們為您準備了仿賽車的相關品牌以及分類，一起享受這個熱血沸騰的氣息吧！</span>
        	</div>
        	<div class="close-banner-btn">
        		<input type="button" name="theme" value="x">	
        	</div>
        </div>
    </div>
</div>
