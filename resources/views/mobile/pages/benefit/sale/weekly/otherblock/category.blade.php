<div class="page-block" id="category">
	<h2 class="page-block-title">仿賽熱門分類</h2>
	<div class="page-block-product">
		<div class="block-product">
			<div class="product_area">
				@php
					$categories=[
					'賽車手套' => ['image' => 'https://img.webike.tw/imageUpload/15471988331547198833730.jpg','link' => '3000-3020-3060-3061'],
					'全罩式安全帽' => ['image' => 'https://img.webike.tw/imageUpload/15477967421547796742173.jpg','link' => '3000-3001-3002'],
					'排氣管尾段' => ['image' => 'https://img.webike.tw/imageUpload/15471989541547198954569.jpg','link' => '1000-1001-1003'],
					'頭罩、擋風鏡' => ['image' => 'https://img.webike.tw/imageUpload/15471988641547198864559.JPG','link' => '1000-1110-1109']
					];
				@endphp
				<ul class="clearfix owl-carouse-advertisement-loop owl-carousel-promo">
					@foreach($categories as $category_name => $category)
						<li class="text-center">
							<a href="{{ route('summary','ca/'.$category['link'].'?'.$rel_parameter )}}" target="_blank" class="zoom-image">
								<div class="vertical-align">
									<img src="{{$category['image']}}">
								</div>
								<span class="vertical-align"></span>
							</a>	
							<div class="text-center">
								<span class="size-10rem">{{$category_name}}</span>
								
							</div>
							<div class="text-center">
								<span class=" size-10rem label text-tag">點數2倍</span>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>