<div class="page-block " id="double-point">
		<h2 class="page-block-title">當週加倍品牌</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix ul-list">
					{{-- @foreach($discount_brands as $discount_brands_url_rewrite => $discount_brand)
						<li class="product-list li-list" data="{{$discount_brands_url_rewrite}}">
							<div class="loop-btn">+</div>
							<a href="{{route('summary', 'br/'.$discount_brands_url_rewrite).'?'.$rel_parameter}}">
								<div class="product-img">
									<img src="{{$discount_brand['brand_image']}}">
								</div>
							</a>
							<div class="product-text text-center">
								<span>點數2倍現折</span>
							</div>
							<div class="product-tag">
								@foreach($discount_brand['ca_name'] as $discount_ca_key => $categorie)
									@php
										$path = explode('-',$categorie->url_path);
										$tag_class = 'label-primary';
										switch ($path['0']) {
											case '3000':
												$tag_class = 'label-primary';
											break;
											case '1000':
												$tag_class = 'label-danger';
											break;
											case '8000':
												$tag_class = 'label-warning';
											break;
											case '4000':
												$tag_class = 'label-warning';
											break;
											default:
												$tag_class = 'label-primary';
											break;
										}
									@endphp
									<div class="dotted-text">
										<span class=" tag label {{$tag_class}} {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$categorie->name}}</span>
									</div>
								@endforeach
							</div>
						</li>
					@endforeach --}}
					@for($i=1;$i<=5;$i++)
						<li class="product-list li-list" data="635">
							<div class="loop-btn">+</div>
							<a href="https://www.webike.tw/br/635">
								<div class="product-img">
									<img src="https://img.webike.net/sys_images/brand/brand_635.gif">
								</div>
							</a>
							<div class="product-text text-center">
								<span>點數2倍現折</span>
							</div>
							<div class="product-tag">
								<div class="dotted-text">
									<span class="tag label label-primary">安全帽</span>
									<span class="tag label label-primary">騎士服裝</span>
								</div>
							</div>
						</li>
					@endfor
				</ul>
			</div>
		</div>
	</div>