@extends('mobile.layouts.mobile')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/benefit/sale/2019-promotion.css') }}">
@stop
@section('middle')
	@include('mobile.pages.benefit.sale.2019promotion.otherblock.main-banner')
	<div class="promotion-container">
		@include('response.pages.benefit.sale.project.2019.otherblock.top-100')
	@include('mobile.pages.benefit.sale.2019promotion.otherblock.collection')

@stop
@section('script')
@stop