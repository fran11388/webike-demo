@php
	$categories = [
		'改裝零件' => ['排氣系統' => 'https://www.webike.tw/parts/ca/1000-1001',
		'底盤' => 'https://www.webike.tw/parts/ca/1000-1311',
		'外觀零件' => 'https://www.webike.tw/parts/ca/1000-1110',
		'轉向系統' => 'https://www.webike.tw/parts/ca/1000-1030',
		'傳動系統' => 'https://www.webike.tw/parts/ca/1000-1150',
		'煞車系統' => 'https://www.webike.tw/parts/ca/1000-1010',
		'引擎' => 'https://www.webike.tw/parts/ca/1000-1180',
		'電系零配件' => 'https://www.webike.tw/parts/ca/1000-1149',
		'車台骨架' => 'https://www.webike.tw/parts/ca/1000-1059'],
		'騎士用品' => ['安全帽' => 'https://www.webike.tw/parts/ca/3000-3001',
		'騎士服裝' => 'https://www.webike.tw/parts/ca/3000-3020',
		'騎士鞋、車靴' => 'https://www.webike.tw/parts/ca/3000-1325',
		'護具' => 'https://www.webike.tw/parts/ca/3000-3027',
		'摩托車相關精品' => 'https://www.webike.tw/ca/3000-1327',
		'保管防盜用品' => 'https://www.webike.tw/parts/ca/3000-3100',
		'電子機器類' => 'https://www.webike.tw/parts/ca/3000-1326',
		'旅行用品' => 'https://www.webike.tw/parts/ca/3000-3260'],
		'耗材、工具' => ['保養耗材' => 'https://www.webike.tw/parts/ca/4000',
		'機車工具' => 'https://www.webike.tw/parts/ca/8000']
	];
@endphp
<div class="page-block " id="category">
		<h2 class="page-block-title">分類搜尋</h2>
		<div class="page-block-product">
			<p class="text-center box">騎士、改裝、工具、耗材一應俱全</p>
			<div class="block-product">
				<div class="link_ui">
					<ul>
						@foreach($categories as $category_name => $category)
							<li>
								<a class="link_ui_a toggle clearfix">
									{{$category_name}}
								</a>
								<ul>
									@foreach($category as $category_child_name =>  $category_child_link)
										<li>
											<a href="{{$category_child_link.'?'.$rel_parameter}}">{{$category_child_name}}</a>
										</li>
									@endforeach
								</ul>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>