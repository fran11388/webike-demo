	<div class="page-block collection">
		<h2 class="page-block-title">推薦特輯</h2>
		<div class="page-block-product">
			<div class="block-product">
				<ul class="clearfix owl-carouse-advertisement-loop owl-carousel-promo">
					@foreach($collections as $key => $collection)
						<li class="collection-list">
							<div class="collection-list-product">
								<a href="{{$collection->link.'?'.$rel_parameter}}" title="{{$collection->name.$tail}}" target="_blank" class="clearfix">
									<div class="collection-img box">
										<img src="{!! $collection->banner !!}" alt="{{$collection->name.$tail}}">
									</div>
								</a>
								<div class="collection-text">
									<span>{{$collection->meta_description}}</span>
								</div>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<script>
	$('.owl-carouse-advertisement-loop').addClass('owl-carousel').owlCarousel({
	autoplay:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	loop:true,
	margin:10,
	nav:true,
    slideBy : 3,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});

</script>
<script>
	$(document).ready(function(){
	$('.owl-carousel-promo').addClass('owl-carousel').owlCarousel({
		loop:true,
		nav:true,
		margin:5,
		slideBy : 6,
		URLhashListener:true,
		startPosition: 'URLHash',
		responsive:{
			0:{
				items:2
			},
			600:{
				items:3
			},
			1000:{
				items:5
			}
		}
	});

	$('.owl-carousel-promo').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>')
	$('.owl-carousel-promo').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>')
});
</script>