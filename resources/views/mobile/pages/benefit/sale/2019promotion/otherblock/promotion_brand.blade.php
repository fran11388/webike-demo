<div class="page-block " id="week_sale">
		<h2 class="page-block-title">本週嚴選 SALE品牌</h2>
		<div class="page-block-product">
			<p class="text-center box ">{{$start_day}}-{{$end_day}} 專屬折扣品牌</p>
			<div class="block-product">
				<ul class="clearfix ul-list">
					@foreach($sale_brands as $sale_brands_url_rewrite => $sale_brand)
						<li class="product-list li-list" data="{{$sale_brands_url_rewrite}}">
							<div class="loop-btn">+</div>
							<a href="{{route('summary', 'br/'.$sale_brands_url_rewrite).'?'.$rel_parameter}}">
								<div class="product-img">
									<img src="{{$sale_brand['brand_image']}}">
								</div>
							</a>
							<div class="product-text text-center">
								<span>最大88折優惠</span>
							</div>
							<div class="product-tag">
								@foreach($sale_brand['ca_name'] as $sale_ca_key => $categorie)
									@php
										$path = explode('-',$categorie->url_path);
										$tag_class = 'label-primary';
										switch ($path['0']) {
											case '3000':
												$tag_class = 'label-primary';
											break;
											case '1000':
												$tag_class = 'label-danger';
											break;
											case '8000':
												$tag_class = 'label-warning';
											break;
											case '4000':
												$tag_class = 'label-warning';
											break;
											default:
												$tag_class = 'label-primary';
											break;
										}
									@endphp
									<div class="dotted-text">
										<span class="tag label {{$tag_class}} {{$sale_ca_key == 2 ? 'hidden': ' ' }}">{{$categorie->name}}</span>
									</div>
								@endforeach
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="page-block " id="country_discount">
		<h2 class="page-block-title">New Year Sale 點數現折</h2>
		<div class="page-block-product">
			<p class="text-center box">{{$start_day}}-{{$end_day}} 點數加倍現折品牌</p>
			<div class="block-product">
				<ul class="clearfix ul-list">
					@foreach($discount_brands as $discount_brands_url_rewrite => $discount_brand)
						<li class="product-list li-list" data="{{$discount_brands_url_rewrite}}">
							<div class="loop-btn">+</div>
							<a href="{{route('summary', 'br/'.$discount_brands_url_rewrite).'?'.$rel_parameter}}">
								<div class="product-img">
									<img src="{{$discount_brand['brand_image']}}">
								</div>
							</a>
							<div class="product-text text-center">
								<span>點數2倍現折</span>
							</div>
							<div class="product-tag">
								@foreach($discount_brand['ca_name'] as $discount_ca_key => $categorie)
									@php
										$path = explode('-',$categorie->url_path);
										$tag_class = 'label-primary';
										switch ($path['0']) {
											case '3000':
												$tag_class = 'label-primary';
											break;
											case '1000':
												$tag_class = 'label-danger';
											break;
											case '8000':
												$tag_class = 'label-warning';
											break;
											case '4000':
												$tag_class = 'label-warning';
											break;
											default:
												$tag_class = 'label-primary';
											break;
										}
									@endphp
									<div class="dotted-text">
										<span class=" tag label {{$tag_class}} {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$categorie->name}}</span>
									</div>
								@endforeach
							</div>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="brand_product_image">
		<ul class="clearfix">
			@foreach($sale_brands as $sale_brands_rewrite => $sale_brand)
				<li id="{{$sale_brands_rewrite}}" class="loop-photo hidden">
					<h2>{{$sale_brand['brand_name']}}</h2>
					<span class="cancel">X</span>
					<ul class="owl-carouse-product-loop">
						@foreach($sale_brand['product_image'] as $sale_image_key => $sale_product_image)
							<li class="{{$sale_image_key == 0 ? ' ': 'hidden'}}">
								<img src="{{ $sale_product_image ? $sale_product_image : NO_IMAGE}}">
							</li>
						@endforeach
					</ul>
				</li>
			@endforeach
			@foreach($discount_brands as $discount_brands_url_rewrite => $discount_brand)
				<li id="{{$discount_brands_url_rewrite}}" class="loop-photo hidden">
					<h2>{{$discount_brand['brand_name']}}</h2>
					<span class="cancel">X</span>
					<ul class="owl-carouse-product-loop">
						@foreach($discount_brand['product_image'] as $discount_image_key => $discount_product_image)
							<li class="{{$discount_image_key == 0 ? ' ': 'hidden'}}">
								<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
							</li>
						@endforeach
					</ul>
				</li>
			@endforeach
		</ul>
	</div>