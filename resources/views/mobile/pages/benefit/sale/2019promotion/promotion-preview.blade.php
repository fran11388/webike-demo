@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/benefit/sale/2019-promotion.css') }}">
<style type="text/css">

	.page-block .schedule-list ul li{
		float: left;
		border: #ddc764 1px solid;
		font-size: 12px;
		color: #fff;
		width: 20%;
		border-radius: 0 0 10px 10px;
		background: #910000;
	}
	.page-block .schedule-list ul li a{
	    display: block;
	    padding: 4px 0;
	    text-align: center;
	    font-size: 12px;
	    color: #fff;
	    font-weight: bold;
	}
	.page-block .schedule-list ul li.active{
		background: #ddc764;
	}
	.schedule-list .schedule-list-product span{
		display: block;
	}
	.page-block .block-product .product-title{
		color: #ddc764;
		background: #910000;
		font-size: 1.5rem;
		padding: 5px 0px;
		margin: 15px 0px;
		text-align: center;
		font-weight: bold;
	}
</style>
@stop
@section('middle')
@include('mobile.pages.benefit.sale.2019promotion.otherblock.main-banner')
<div class="promotion-container">
	<div class="page-block">
		<h2 class="page-block-title">本週嚴選 SALE品牌</h2>
		<div class="page-block-product">
		<div class="schedule-list">
			<ul class="clearfix">
				@php
					$weeks = ['12/28-1/3','1/4-1/10','1/11-1/17','1/18-1/24','1/25-1/31'];
					$weeks_day = ['第一週','第二週','第三週','第四週','第五週'];
				@endphp
				@foreach($weeks as $key => $week)
					<li class="schedule-list-product {{$key == $week_key ? 'active': '' }}" data="week_{{$key}}">
						<a href="javascript:void(0)">
							<span>{{$week}}</span>
							<span>{{$weeks_day[$key]}}</span>
						</a>
					</li>
				@endforeach
			</ul>
		</div>
		@foreach($week_sale_brands as $page_key => $week_sale_brand)
				<div class="week-page {{$page_key == $week_key ? 'active': 'hidden'}} week_{{$page_key}}">
					<div class="block-product">
						<h3 class="product-title">優惠品牌</h3>
						<ul class="clearfix ul-list">
							@foreach($week_sale_brand as $week_sale_brand_url_rewrite => $sale_brand)
								<li class="product-list li-list" data="{{$week_sale_brand_url_rewrite}}">
									<div class="loop-btn">+</div>
									<a href="{{route('summary','br/'.$week_sale_brand_url_rewrite).'?'.$rel_parameter}}">
										<div class="product-img">
											<img src="{{$sale_brand['brand_image']}}">
										</div>
									</a>
									<div class="product-text text-center">
										<span>最大88折優惠</span>
									</div>
									<div class="product-tag">
										@foreach($sale_brand['ca_name'] as $sale_ca_key => $sale_categorie)
											@php
												$path = explode('-',$sale_categorie->url_path);
												$tag_class = 'label-primary';
												switch ($path['0']) {
													case '3000':
														$tag_class = 'label-primary';
													break;
													case '1000':
														$tag_class = 'label-danger';
													break;
													case '8000':
														$tag_class = 'label-warning';
													break;
													case '4000':
														$tag_class = 'label-warning';
													break;
													default:
														$tag_class = 'label-primary';
													break;
												}
											@endphp
											<div class="dotted-text">
												<span class="tag label {{$tag_class}}  {{$sale_ca_key == 2 ? 'hidden': ' ' }}">{{$sale_categorie->name}}</span>
											</div>
										@endforeach
									</div>
								</li>
							@endforeach
						</ul>
					</div>
					<div class="block-product">
						<h3 class="product-title">2倍點數現折品牌</h3>
						<ul class="clearfix ul-list">
							@foreach($week_discount_brands[$page_key] as $week_discount_brands_url_rewite => $discount_brand)
								<li class="product-list li-list" data="{{$week_discount_brands_url_rewite}}">
									<div class="loop-btn">+</div>
									<a href="{{route('summary','br/'.$week_discount_brands_url_rewite).'?'.$rel_parameter}}">
										<div class="product-img">
											<img src="{{$discount_brand['brand_image']}}">
										</div>
									</a>
									<div class="product-text text-center">
										<span>點數2倍現折</span>
									</div>
									<div class="product-tag">
										@foreach($discount_brand['ca_name'] as $discount_ca_key => $discount_categorie)
										@php
											$path = explode('-',$discount_categorie->url_path);
											$tag_class = 'label-primary';
											switch ($path['0']) {
												case '3000':
													$tag_class = 'label-primary';
												break;
												case '1000':
													$tag_class = 'label-danger';
												break;
												case '8000':
													$tag_class = 'label-warning';
												break;
												case '4000':
													$tag_class = 'label-warning';
												break;
												default:
													$tag_class = 'label-warning';
												break;
											}
										@endphp
										<div class="dotted-text">
											<span class=" tag label {{$tag_class}}  {{$discount_ca_key == 2 ? 'hidden': ' ' }}">{{$discount_categorie->name}}</span>
										</div>
										@endforeach
									</div>
								</li>
							@endforeach
						</ul>
					</div>
					<div class="brand_product_image">
						<ul class="clearfix">
							@foreach($week_sale_brand as $week_sale_brand_url_rewrite => $sale_brand)
								<li id="{{$week_sale_brand_url_rewrite}}" class="loop-photo hidden">
									<h2>{{$sale_brand['brand_name']}}</h2>
									<span class="cancel">X</span>
									<ul class="owl-carouse-product-loop">
										@foreach($sale_brand['product_image'] as $sale_image_key => $sale_product_image)
											<li class="{{$sale_image_key == 0 ? ' ': 'hidden'}}">
												<img src="{{ $sale_product_image ? $sale_product_image : NO_IMAGE}}">
											</li>
										@endforeach
									</ul>
								</li>
							@endforeach
							@foreach($week_discount_brands[$page_key] as $week_discount_brands_url_rewite => $discount_brand)
								<li id="{{$week_discount_brands_url_rewite}}" class="loop-photo hidden">
									<h2>{{$discount_brand['brand_name']}}</h2>
									<span class="cancel">X</span>
									<ul class="owl-carouse-product-loop">
										@foreach($discount_brand['product_image'] as $discount_image_key => $discount_product_image)
											<li class="{{$discount_image_key == 0 ? ' ': 'hidden'}}">
												<img src="{{ $discount_product_image ? $discount_product_image : NO_IMAGE}}">
											</li>
										@endforeach
									</ul>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endforeach
			<div class="text-center clearfix">
				<a class="promotion-btn" href="{{route('benefit-sale-2019-promotion')}}">回前頁</a>
			</div>
		</div>
	</div>
	@include('mobile.pages.benefit.sale.2019promotion.otherblock.collection')
</div>
@stop
@section('script')
<script type="text/javascript">
		$('.owl-carouse-product-loop').addClass('owl-carousel').owlCarousel({
			autoplayTimeout:2000,
			autoplayHoverPause:true,
			loop:true,
			margin:10,
			nav:false,
		    slideBy : 1,
		    center: true,
		    autoplay: false,
			autoPlaySpeed: 500,
			dots: true,
			autoplayHoverPause: false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});

		$('.loop-btn').click(function(){
			brand_url_rewrite =  $(this).closest('li.product-list').attr('data');
			$('.brand_product_image li.loop-photo').addClass('hidden')
			$('.brand_product_image li.loop-photo#' + brand_url_rewrite).removeClass('hidden');
			$('.brand_product_image li.loop-photo#' + brand_url_rewrite).closest('div.brand_product_image').addClass('active');			
			$('body, html').css('overflow', 'hidden');
			$('nav.fixed').addClass('hidden');
		  	$('.brand_product_image li.loop-photo#' + brand_url_rewrite).addClass('active');
			setTimeout(function(){ 
			  		$('.brand_product_image li.loop-photo.active .owl-carouse-product-loop li').removeClass('hidden');
			}, 500);
		});

		$('.loop-photo  .cancel').click(function(){
			$(this).closest('li.loop-photo').addClass('hidden').removeClass('active');
			$('.brand_product_image').removeClass('active');
			$('nav.fixed').removeClass('hidden');
			$('body, html').css('overflow', "");
		});

		$('.page-block-product .schedule-list .schedule-list-product').click(function(){
			$('.page-block-product .schedule-list .schedule-list-product').removeClass('active');
			$(this).addClass('active');
			data = $(this).attr('data');
			$('.page-block-product .week-page.active').addClass('hidden').removeClass('active');
			$('.page-block-product .week-page.' + data).addClass('active').removeClass('hidden');
			$('.brand_product_image li.loop-photo').addClass('hidden').removeClass('active');
			$('body, html').css('overflow', "");
		});
</script>
@stop