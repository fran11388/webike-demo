@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('mobile/css/pages/cartpagedetail.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('mobile/css/pages/cartpagedetail-new.css') !!}">
    <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/product-relieved-shopping.css') !!}">
@stop
@section('middle')
<div class="all-page">
    <div class="title"><p>購物車</p></div>
    <div class="content">
        <span class="max-points hidden">{{ $service->getMaxAllowPoints() >= $current_customer->getCurrentPoints() ? $current_customer->getCurrentPoints() : $service->getMaxAllowPoints() }}</span>
        @if (count($errors) > 0)
            <div class="alert alert-danger content-last-block">
                <ul style="list-style-type: none;">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="commodity-title"><p>選購的商品</p></div>
        <div class="commodity-list">
            @if(count($carts))
                <ul>
                    @foreach ($carts as $cartKey => $cart)
                        <?php $product = $cart->product; ?>
                        @include('mobile.pages.cart.partials.item-v2')
                    @endforeach

                    {{-- @include('mobile.pages.cart.partials.gift-v2') --}}
                    @foreach ($gifts as $giftKey => $gift)
                        <?php $product = $gift; ?>
                        @include('mobile.pages.cart.partials.gift-v2')
                    @endforeach

                </ul>
            @else
                <h2 class="empty-text">購物車沒有商品</h2>
            @endif
        </div>

        @include('response.pages.product-detail.partials.product-relieved-shopping')

        <div class="font-color-red join-addition-text text-center">加價購商品新增中...</div>
        <div class="additional-block">
            @include('mobile.pages.cart.partials.additional')
        </div>
        @include('mobile.pages.cart.partials.calculator')

    </div>
</div>




@stop
@section('script')
<script>
    $('.coupon-info').click(function(event) {
        swal(
            '折價卷?',
            '每次購物僅限用一張<br>折扣僅折抵商品金額',
            'question'
        )
    });
    $('.point-info').click(function(event) {
        var max_points = $('.max-points').text();
        swal({
            title: '點數?',
            text: '現有{{ $current_customer->getCurrentPoints() }}點<br>最多可使用' +max_points + '點',
            type: 'question',
            animation: true
        })
    });
    $('.cart-checkout').click(function(){
        $(this).attr("disabled", true);
        if($('.commodity-list .item').hasClass('sold_out')){
            swal({
                title: '提醒',
                text: "請先刪除\"售完\"商品，即可進行結帳。",
                type: 'warning',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33'
            }).then(function(result) {
                slipTo('#sold_out');
                allowCheckout();
            });

        }else{
            location.href="{!! route('checkout') !!}";
            allowCheckout();
        }
    });
    $(document).on('click', '.gift-add', function(){
        $(this).closest('.gift-form').submit();
    });
    $(document).on('click', '.number .plus', function(){
        if(!$(this).hasClass('disabled')){
            var quantity = $(this).closest('.number').find('input[name=qty]');
            var number = parseInt($(quantity).val());
            if(number <= 999){
                $(quantity).val(number + 1);
                $(quantity).trigger('change');
            }
        }
    });
    $(document).on('click', '.number .minus', function(){
        if(!$(this).hasClass('disabled')){
            var quantity = $(this).closest('.number').find('input[name=qty]');
            var number = parseInt($(quantity).val());
            if(number >= 1){
                $(quantity).val(number - 1);
                $(quantity).trigger('change');
            }
        }
    });
    function reloadCartOwlControl(){
        $('.owl-carousel').owlCarousel('destroy');
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:2,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:false
                }
            }
        });

        $('.additional-block .owl-carousel').find('.owl-prev').html('<div class=""><i class="fa fa-chevron-left" aria-hidden="true"></i></div>');
        $('.additional-block .owl-carousel').find('.owl-next').html('<div class=""><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
    }
    $('.determine-quantity input[name=qty]').on('change', function () {

        if ($(this).val() % 1 > 0) {
            $(this).val(parseInt($(this).val()));
        }

        var DetermineNumber = parseInt($(this).val());
        var maxQuantity = parseInt($(this).prop('max'));
        var area = $(this).closest('.commodity-list .item');
        if(DetermineNumber > maxQuantity){
            $(this).val(maxQuantity);
            DetermineNumber = maxQuantity;
        }

        var UnitPrice = $(area).find('.unit_product_price').text();
        var FinalItemPrice = parseInt(UnitPrice) * DetermineNumber;
        $('input[name=points]').prop('disabled',true);
        $('input[name=qty]').prop('disabled',true);
        $('select[name=code]').prop('disabled',true);
        $('.number .minus').addClass('disabled');
        $('.number .plus').addClass('disabled');

        if (DetermineNumber <= 0) {
            $(this).val(1);
            $(area).find('.item-final-price').text(number_format(parseInt(UnitPrice)));
        } else if (DetermineNumber >= 1) {
            $(area).find('.item-final-price').text(number_format(FinalItemPrice));
        }

        var sku = $(this).closest('.determine-quantity').find('input[name=sku]').val();
        changeActivity();

        $.ajax({
            url: "{!! URL::route('cart-update') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content') ,'qty' : DetermineNumber ,'sku' : sku},
            type:"POST",
            dataType:'html',

            success: function(result){
                setCartTotalPrice();
                reloadAddition();
                checkProduct();
                $('.number .minus').removeClass('disabled');
                $('.number .plus').removeClass('disabled');
                allowCheckout();
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal(
                        '發生錯誤...請重新整理'
                )
            }
        });
    });

    $(document).on('click','.cart-remove',function(){
        var item = $(this).closest('.commodity-list .item');
        var protected_code = item.find('input[name=code]').val();
        item.removeClass('sold_out').removeAttr('id');
        item.fadeOut(1000);
        $('input[name=points]').prop('disabled',true);
        $('input[name=qty]').prop('disabled',true);
        $('select[name=code]').prop('disabled',true);

        changeActivity();

        $.ajax({
            url: "{!! URL::route('cart-remove') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content') ,'code' : protected_code},
            type:"POST",
            dataType:'html',

            success: function(result){
                setCartTotalPrice();
                reloadAddition();
                checkProduct();
                allowCheckout();
            },

            error:function(xhr, ajaxOptions, thrownError){
                swal(
                    '發生錯誤...請重新整理'
                )
            }
        });
    });

    $(document).on('click','.join-addition',function(){
        $('input[name=points]').prop('disabled',true);
        $('input[name=qty]').prop('disabled',true);
        $('select[name=code]').prop('disabled',true);

        var qty = $(this).closest('.join-addition-block').find("input[name=qty]").val();
        var sku = $(this).closest('.join-addition-block').find("input[name=sku]").val();
        var joinAddition = true;
        $('.join-addition-text').show();

        changeActivity();

        $.ajax({
            url: "{!! URL::route('cart-update') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content') ,'qty' : qty ,'sku' : sku},
            type:"POST",
            dataType:'html',

            success: function(result){
                joinAdditionProduct(sku,qty);
                setCartTotalPrice();
                reloadAddition();
                checkProduct();
                allowCheckout();
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal(
                        '發生錯誤...請重新整理'
                )
            }
        });
    });

    $(document).on("change",".coupon-select",function(){
        var code = $('select[name=code]').val();
        changeActivity();
        $('input[name=points]').prop('disabled',true);
        $('input[name=qty]').prop('disabled',true);

        $.ajax({
            url: "{!! URL::route('cart-coupon') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content') ,'code' : code},
            type:"POST",
            dataType:'html',

            success: function(result){
                setCartTotalPrice();
                reloadAddition();
                checkProduct();
                allowCheckout();
            },

            error:function(xhr, ajaxOptions, thrownError){
                swal(
                        '折價券輸入有誤...請聯絡客服人員'
                )
            }
        });
    });

    $(document).on("change","input[name='points']",function(){
        var points = parseInt($('input[name=points]').val());
        changeActivity();
        $('select[name=code]').prop('disabled',true);
        $('input[name=qty]').prop('disabled',true);

        $.ajax({
            url: "{!! URL::route('cart-points') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content') ,'points' : points},
            type:"POST",
            dataType:'html',

            success: function(result){
                var max_points = parseInt($('.max-points').text());

                if(points > max_points){
                    swal(
                            {
                                type: 'info',
                                title: '已達上限',
                                html: '最多可使用' + max_points + '點'
                            }
                    );
                    $('input[name=points]').val(max_points);
                }

                setCartTotalPrice();
                checkProduct();
                reloadAddition();
                allowCheckout();
            },

            error:function(xhr, ajaxOptions, thrownError){
                swal(
                        '點數輸入有誤...請聯絡客服人員'
                )
            }
        });
    });

    function allowCheckout()
    {
        $('.cart-checkout').removeAttr('disabled');
        $('.cart-checkout').removeClass('disabled');
    }

    function changeActivity()
    {
        $('.calculating').show();
        $('.final-total-price').hide();
        $('.cart-checkout').attr('disabled','disabled');
        $('.addition-standard').hide();
        $('.addition-loading').show();
    }

    function reloadAddition()
    {
        $.ajax({
            url: "{!! URL::route('cart-reload-addition') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content')},
            type:"POST",
            dataType:'html',

            success: function(result){
                $('.additional-block').html(result);
                $('.addition-loading').hide();
                reloadCartOwlControl();
            },

            error:function(xhr, ajaxOptions, thrownError){
            }
        });
    }

    function setCartTotalPrice() {
        $.ajax({
            url: "{!! URL::route('cart-get-total-price') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content')},
            type:"POST",
            dataType:'json',

            success: function(result){
                var check_href = "{{route('checkout')}}";
                $('.final-total-price').text(result.final_price);
                $('.calculating').hide();
                $('.final-total-price').show();
                $('input[name=points]').prop('disabled',false);
                $('select[name=code]').prop('disabled',false);
                $('input[name=qty]').prop('disabled',false);
                $('.cart-checkout').attr('href',check_href);
                $('.receive-points').text(result.receive_points);
                $('.max-points').attr('placeholder', '/' + result.max_points);
                $('input[name=points]').val(result.now_use_points);
                $('.shipping_fee').text(result.shipping_fee);
            },

            error:function(xhr, ajaxOptions, thrownError){
            }
        });
    }

    function checkProduct()
    {
        $.ajax({
            url: "{!! URL::route('cart-check-product') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content')},
            type:"POST",
            dataType:'json',

            success: function(result){
                if(result){
                    var num;
                    var check = false;
                    $('.commodity-list .item').each(function(){
                        check = false;
                        var product_id = $(this).data('title');
                        $.each(result,function(index,id) {
                            if(product_id == id){
                                check = true;
                            }
                        });
                        if(check === false){
                            $(this).fadeOut(1000);
                        }
                    });
                }
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal(
                        '發生錯誤...請重新整理'
                )
            }
        });
    }

    function number_format(num) {
        num = num + "";
        var re = /(-?\d+)(\d{3})/
        while (re.test(num)) {
            num = num.replace(re, "$1,$2")
        }
        return num;
    }
    jQuery(document).ready(function($) {
        $('.about').click(function(event) {
        /* Act on the event */
//        event.preventDefault();
            $(this).stop().toggleClass('dotted-text4');
        
        });

    });

    function joinAdditionProduct(sku,qty)
    {
        $.ajax({
            url: "{!! URL::route('cart-join-addition-product') !!}",
            data: {'_token': $('meta[name=csrf-token]').prop('content') ,'qty' : qty ,'sku' : sku},
            type:"POST",
            dataType:'html',

            success: function(result){
                if(result){
                    $('.join-addition-text').hide();
                    $('.all-page .content .commodity-list ul').append(result).fadeIn(1000);
//                    adjuctProductIBlock();
                }
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal(
                    '發生錯誤...請重新整理'
                )
            }
        });
    }

</script>


@stop

