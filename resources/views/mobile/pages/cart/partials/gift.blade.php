<ul class="gifts-cart-ui clearfix" {!! (count($gifts) > 1 and $giftKey == 0) ?  : '' !!}>
    <form method="post" action="{{route('customer-history-gifts')}}">
        <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
        <input name="action" type="hidden" value="remove_cart">
        <input name="redirect" type="hidden" value="cart">
        <div class="btn-default-block clearfix">
            <button type="submit" class="btn-default">
                <i class="fa fa-close"></i>
            </button>
        </div>
    </form>
    @if(!count($carts) or $product->customer_gift_status == \Ecommerce\Service\GiftService::REWARD)
        <div class="cover-box">

        </div>
        @if(!$hasGiftInCart)
            <div class="center-btn">
                @if(!count($carts))
                    <a class="btn btn-default btn-large size-08rem font-color-red" href="javascript:void(0)"><span class="hidden-lg hidden-md">購買商品可免費兌換</span><span class="hidden-sm hidden-xs">購物車無商品-當您購買商品時可免費兌換此贈品</span></a>
                @else
                    <form method="post" action="{{route('customer-history-gifts')}}">
                        <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
                        <input name="action" type="hidden" value="add_cart">
                        <input name="redirect" type="hidden" value="cart">
                        <button class="btn btn-default btn-large size-08rem font-color-red" type="submit"><span class="hidden-lg hidden-md">點擊後結帳可免費兌換</span><span class="hidden-sm hidden-xs">點擊兌換-當您購買商品時可免費兌換此贈品</span></button>
                    </form>
                @endif
            </div>
        @else
            <div class="center-btn" >
                <a class="btn btn-default btn-large size-08rem font-color-red" href="javascript:void(0)">每次結帳僅能兌換一項</a>
            </div>
        @endif
    @endif
    <li class="gifts-item-content">
        <div class="row clearfix">
            <div class="gifts-item-img-block">
                <div class="gifts-item-img">
                    <a class="zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                        <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
                    </a>
                </div>
            </div>
            <div class="info-brand">
                <span class="name-brand-block" title="{{ $product->manufacturer->name }}">{{ $product->manufacturer->name }}</span>
                <a href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                    <h3>{{ $product->name }}</h3>
                </a>
            </div>
        </div>
        {{-- ATTATENION !! Follow is Mobile Block , Modify Both Block if needed --}}
        <div class="gifts-content-estimate-block visible-sm visible-xs row">
            <div class="gifts-content-number">
                {{--<form class="form-horizontal" method="post" action="{{route('cart-update')}}">--}}
                    <div class="determine-number flex-box">
                        {{--<label class="btn-label label-small start-label text-nowrap">數量：</label>--}}
                        <button class="btn btn-default btn-full" type="submit" disabled>無法修改數量</button>
                    </div>
                {{--</form>--}}
                {{--<form method="post" action="{{route('customer-history-gifts')}}">--}}
                    {{--<div class="col-sm-5 col-xs-5">--}}
                        {{--<input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">--}}
                        {{--<input name="action" type="hidden" value="remove_cart">--}}
                        {{--<input name="redirect" type="hidden" value="cart">--}}
                        {{--<button type="submit" class="btn btn-small btn-full btn-default">下次再兌換</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            </div>
            <div class="gifts-content-price-block">
                <span class="font-color-red hidden-lg hidden-md hidden-sm">
                贈品
                </span>
                <span class="font-color-red hidden-lg hidden-md hidden-sm gifts-content-price">
                NT$0
                </span>
            </div>
            <div class="toggle-box">
                <div class="toggle-button hidden-lg hidden-md hidden-sm text-right">
                    <a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊</a>
                </div>
                <div class="toggle-content hidden-xs">
                    <div>商品編號：{{ $product->model_number }}</div>
                    @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                        <div>備註：{{$estimate_item->note}}</div>
                    @endif
                    @if($options = $product->getSelectsAndOptions() and $options->count())
                        <div>
                            規格：<br>
                            @foreach($options as $option)
                                {{--{{ $option->label }}：--}}
                                {{ $option->option }}<br>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
         {{--Mobile Block END--}}
        <?php
        $categories = $product->categories->sortBy('depth')->pluck('name');
        $categories->shift();
        ?>
        <script type="text/javascript">
            _paq.push(['addEcommerceItem', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$categories->toArray()) !!}"],{{round($product->getCartPrice($current_customer))}},1]);
        </script>
    </li>
     {{--ATTATENION !! Follow is PC Block , Modify Both Block if needed--}}
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <span>贈品 NT$0</span>
        <span><s>原價 NT${{ number_format($product->price) }}</s></span>
    </li>
    <li class="col-lg-2 col-md-2 history-content-cart-option hidden-sm hidden-xs">
        無法修改數量
    </li>
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <span>NT$0</span>
    </li>
     {{--PC Block END --}}
</ul>