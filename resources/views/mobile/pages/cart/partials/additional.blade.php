@foreach($additional_collection as $area)
    <div class="addPurchase additional-area">
        <div class="addPurchase-title"><p>{{ $area->title }}</p></div>
        <div class="owl-carousel owl-theme">
            @foreach($area->items as $item)
            <div class="item additional-item {!! $item->disabled_flg ? 'disabled' : '' !!}">
                <a class="image-box" href="{{ $item->url }}" title="{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}" target="_blank">
                    <figure class="zoom-image">
                        @if($item->icon_flg)
                            <!-- <div class="sale-off-icon"><span>${{ number_format($item->price) }}元</span></div> -->
                                <div class="sale-off-icon"><span>特惠</span></div>
                        @endif
                        <img src="{{ $item->thumbnail }}" alt="{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}" >
                    </figure>
                </a>
                <p class="dotted-text3">{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}</p>
                <label for="">NT${{ number_format($item->price) }}</label>
                @if($item->disabled_flg)
                    <div class="addition-loading">
                        @include('mobile.common.loading.xs')
                    </div>
                    <a class="additional-to-cart btn-default addition-standard" href="javascript:void(0)">
                        @if($area->sale_flg)
                            <span>{{ $item->selected_flg ? '已加入購物車' : '售完' }}</span>
                        @else
                            <span>未達加購標準</span>
                        @endif
                    </a>
                @else
                    <div class="join-addition-block">
                        <input name="qty" type="hidden" value="1">
                        <input name="sku" type="hidden" value="{{ $item->url_rewrite }}">
                        <div class="addition-loading">
                            @include('mobile.common.loading.xs')
                        </div>
                        <a class="additional-to-cart join-addition" href="javascript:void(0)">加入購物車</a>
                    </div>
                @endif
            </div>
            @endforeach
        </div>

    </div>
@endforeach