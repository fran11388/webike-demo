<div class="promotions">
    <ul>
        <li>
            <span class="coupon-info">折價券</span>
            <div class="sale">
                <select class="coupon-select" name="code" id="soflow">
                    <option class="soflow-hidden" disabled selected hidden value="">{{ $coupons ? count($coupons) : 0 }} 張折價券</option>

                    @php
                        $coupon_type = [];
                    @endphp
                    @if ($coupons)
                        @foreach ($coupons as $coupon)
                            @php
                                $coupon_type[] = $coupon->discount_type;
                            @endphp
                            <option value="{{ $coupon->uuid }}" {{ $coupon->uuid == request()->session()->get('coupon-code')  ? 'selected' : ''}}>
                                {{ $coupon->name }}[NT{{ number_format($coupon->discount) }}]
                                @if($coupon->code == "1111celebrate")
                                    {{date("m/d",strtotime("$coupon->expired_at -1 day"))." 23:59截止"}}
                                @endif
                            </option>
                        @endforeach
                        <option value="">不使用折價券</option>
                    @endif
                </select>
            </div>
        </li>

        <li class="fixclear"></li>

        <li>
            <span class="point-info">現金點數</span>
            @if(request()->session()->get('usage-points'))
                <input class="max-points" placeholder="/{{ $current_customer->getCurrentPoints() }}" type="number" name="points" value="{{ request()->session()->get('usage-points') }}">
            @else
                <div class="max-points-position">
                    <input class="max-points" type="number" pattern="\d*" name="points" value="">
                    <span class="max-points-view">/{{ $current_customer->getCurrentPoints() }}</span>
                </div>     
            @endif
        </li>

        <li class="fixclear"></li>
        @if(activeShippingFree())
            <li id="add-shop-tag">
                <p class="label-list clearfix">
                    <span class="label label-danger size-08rem">
                        5/1-5/31，結帳金額滿1000享免運費
                    </span>
                </p>
                    {{-- <img style="width:30px;" src="{{assetRemote('image/label/cart.gif')}}"> --}}
            </li>
        @elseif(activeValidate('2019-05-01 00:00:00','2019-06-01') && !(activeShippingFree()))
            <li id="add-shop-tag">
                <p class="label-list clearfix">
                    <span class="label label-danger size-08rem">
                        5/1-5/31，結帳滿1000退運費點數
                    </span>
                </p>
            </li>
        @endif
        @if(activeValidate('2019-04-01 00:00:00','2019-05-01'))
            <li id="add-shop-tag">
                <label class="note width-full" style="color:#e61e25; ">4/1-4/30，6期分期0利率</label>
            </li>
        @endif
        
        <li class="promotions-line">
            <div class="total-f">

                <p>
                    運費：<i>NT$</i><i class="shipping_fee">{{ $service->getFee() }}</i>
                </p>
                <p>合計：<i class="final-total-price">NT$ {{ number_format($service->getSubtotal() + $service->getFee()) }}</i><i class="calculating">計算中...</i></p>
                @if(active2019MayEvent($current_customer->role_id, $service->getSubtotal() + $service->getFee()))
                    <p>此訂單可獲得：<i class="receive-points">{{ number_format($service->getReceivedPoints()) . ' + ' . $service->getFee() }}</i>點</p>
                @else
                    <p>此訂單可獲得：<i class="receive-points">{{ number_format($service->getReceivedPoints()) }}</i>點</p>
                @endif
            </div>
            <div class="total-l">
                <button class="calculator-btn btn-checkout cart-checkout" href="javascript:void(0)">結帳</button>
                <button class="calculator-btn btn-returnShop" onclick="javascript:location.href='{{route('shopping')}}'">繼續購物</button>
            </div>
        </li>
    </ul>
</div>