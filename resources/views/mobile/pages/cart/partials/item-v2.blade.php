@php
    $sold_out = (isset($carts_stock_info[$product->sku]) and $carts_stock_info[$product->sku]['sold_out']) ? true : false;
    $delivery = $carts_stock_info ? $carts_stock_info :null;
    $image_cover_types = [\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY];
    $estimate_time = date('m/d H:i',strtotime(date('Y-m-d H:i:s')));
    $max = 999;
    if(isset($delivery[$product->sku]) and isset($delivery[$product->sku]['stock_type']) and $delivery[$product->sku]['stock_type'] == 'tw_stock'){
        $max = $delivery[$product->sku]['stock'];
    }
@endphp
<li data-title="{{ $product->id }}" id="{{ $sold_out ? 'sold_out' : ''}}" class="item {{ $sold_out ? 'sold_out' : ''}}">
    <input name="code" type="hidden" value="{{ $cart->protect_code }}">
    <div class="left item-image-block">
        @if( in_array($product->type, [\Everglory\Constants\ProductType::UNREGISTERED, \Everglory\Constants\ProductType::GROUPBUY]))
            <a class="image-box" href="javascript:void(0)" title="{{ $product->full_name }}">
                <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
                <span class="helper"></span>
            </a>
        @else
            <a class="image-box" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}" target="_blank">
                <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
                <span class="helper"></span>
            </a>
        @endif
    </div>
    <div class="right">
        <a href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}" target="_blank">
            <p class="item-name">{{ $product->manufacturer->name }} {{ $product->name }}</p>
        </a>
        <?php /*
        @if($delivery and isset($delivery[$product->sku]))
            <div class="delivery-block">
                <p class="delivery-text">
                    @if($sold_out)
                        {{ '商品售完，請選購其他商品。' }}
                        <span class="estimate_time"> {!! $estimate_time !!}</span>
                    @else
                        {!!  $delivery[$product->sku]['delivery_info']  !!}
                    @endif
                </p>
            </div> --}}
        @endif
        */ ?>
        {{-- 如果描述過長 點擊後向下展開 拿掉dotted-text4標籤 --}}
        <div class="about dotted-text4">
            <div>商品編號：{{ $product->model_number }}</div>
            @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                <div>備註：{{$estimate_item->note}}</div>
            @endif
            @if($options = $product->getSelectsAndOptions() and $options->count())
                <div class="btn-gap-top">
                    規格：<br>
                    @foreach($options as $option)
                        @php
                            $explode_sign = ['：',':'];
                            foreach($explode_sign as $sign){
                                $option_content = explode($sign,$option->option);
                                if(isset($option_content[1])){
                                    break;
                                }
                            }
                            $option_title = str_replace('請選擇','',$option->label);
                        @endphp
                        @if(isset($option_content[1]))
                            {{ $option->option }}<br>
                        @else
                            {{ $option_title }}：{{ $option->option }}<br>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="fixclear"></div>
    <div class="bottom row">
        <div class="col-md-4 col-sm-4 col-xs-4">
            @if($product->checkPointDiscount($current_customer))
                <span><s>NT${{ number_format($product->getFinalPrice($current_customer) * $cart->quantity) }}</s></span>
                <p><span >NT$</span><span class="item-final-price">{{ number_format($product->getCartPrice($current_customer) * $cart->quantity) }}</span></p>
            @else
                <p><span >NT$</span><span class="item-final-price">{{ number_format($product->getCartPrice($current_customer) * $cart->quantity) }}</span></p>
            @endif
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4" style="padding: 0px;">
            @if($sold_out)
                <div class="tip tip-danger">
                    售完
                </div>
            @elseif(in_array($product->type,[Everglory\Constants\ProductType::GROUPBUY,Everglory\Constants\ProductType::OUTLET]))
                <div class="tip tip-warning">
                    無法修改數量
                </div>
            @elseif(in_array($product->type,[Everglory\Constants\ProductType::ADDITIONAL]))
                <div class="tip tip-warning">
                    限購一件
                </div>
            @else
                <div class="determine-quantity">
                    <span class="hidden unit_product_price">{{ (int)$product->getCartPrice($current_customer) }}</span>
                    <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                    <div class="number">
                        <div class="minus"><i class="fas fa-minus"></i></div>
                        <input name="qty" type="number" value="{{ $cart->quantity }}">
                        <div class="plus"><i class="fas fa-plus"></i></div>
                    </div>
                </div>
            @endif
            
            {{-- 商品已售完 --}}
{{--             <div class="number">
                <div class="number-style">商品已售完</div>
            </div> --}}
            




        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-right">
            <input name="code" type="hidden" value="{{ $cart->protect_code }}">
            <button class="delete-btn cart-remove">刪除</button>
        </div>
    </div>
</li>
<?php
$categories = $product->categories->sortBy('depth')->pluck('name');
$categories->shift();
?>
<script type="text/javascript">
    /*
    _paq.push(['addEcommerceItem', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$categories->toArray()) !!}"],{{round($product->getCartPrice($current_customer))}},{{$cart->quantity}}]);
    */
</script>
