@if($service->getCount())
    <div class="history-table-container">
        <ul class="history-info-table history-table-adjust">
            <!--*******************待刪除 by Jason(僅顯示於PC)************************-->
            {{--<li class="history-table-title">--}}
                {{--<ul>--}}
                    {{--<li class="col-md-4 col-sm-12 col-xs-12"><span class="hidden-xs">商品</span><span class="text-left hidden-lg hidden-md hidden-sm" style="padding:0 10px">商品列表</span></li>--}}
                    {{--<li class="col-md-2 hidden-sm hidden-xs"><span>販賣價格</span></li>--}}
                    {{--<li class="col-md-2 hidden-sm hidden-xs"><span>數量</span></li>--}}
                    {{--<li class="col-md-2 hidden-sm hidden-xs"><span>小計</span></li>--}}
                    {{--<li class="col-md-2 hidden-sm hidden-xs"><span>管理</span></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li class="history-table-content">
                @foreach ($carts as $cartKey => $cart)
                    <?php $product = $cart->product; ?>
                    @include('mobile.pages.cart.partials.item')

                @endforeach
                @foreach ($gifts as $giftKey => $gift)
                    <?php $product = $gift; ?>
                    @include('mobile.pages.cart.partials.gift')
                @endforeach
            </li>
        </ul>
    </div>
    <div class="checkout-container">
        <div class="ct-cart-page-detail-sum clearfix">
            <ul class="ul-cart-page-detail-sum">
                <li class="ct-cart-page-detail-sum-left">
                    <table class="table-page-detail-sum-left">
                        <tr>
                            <td class="col-md-3 hidden-sm hidden-xs">使用折價券：</td>
                            <td class="coupon-block clearfix">
                                <span class="coupon-title visible-sm visible-xs">折價券
                                    <i class="fa fa-question-circle"></i>
                                </span>
                                <form name="CouponForm" method="post" action="{{route('cart-coupon')}}">
                                    <select class="coupon-select form-control select2" name="code">
                                        <option value="">您目前有 {{ $coupons ? count($coupons) : 0 }} 張折價券</option>
                                        @php
                                            $coupon_type = [];
                                        @endphp
                                        @if ($coupons)
                                            @foreach ($coupons as $coupon)
                                                @php
                                                    $coupon_type[] = $coupon->discount_type;
                                                @endphp

                                                <option value="{{ $coupon->uuid }}" {{ $coupon->uuid == request()->session()->get('coupon-code')  ? 'selected' : ''}}>
                                                    {{ $coupon->name }}[NT{{ number_format($coupon->discount) }}]
                                                    @if($coupon->code == "1111celebrate")
                                                        {{date("m/d",strtotime("$coupon->expired_at -1 day"))." 23:59截止"}}
                                                    @endif
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </form>
                                <!--*******************待刪除 by Jason(要拔掉)************************-->
                                {{--<div class="row">--}}
                                    {{--<div class="col-sm-12 col-xs-12 visible-sm visible-xs">--}}
                                        {{--<button class="btn btn-default base-btn-gray btn-full" onclick="document.CouponForm.submit()">輸入</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </td>
                            <!--*******************待刪除 by Jason(要拔掉)************************-->
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                                {{--<button class="btn btn-default base-btn-gray history-btn-default" onclick="document.CouponForm.submit()">輸入</button>--}}
                            {{--</td>--}}
                        </tr>
                        {{--<tr>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                            {{--</td>--}}
                            {{--<td class="col-md-6 col-sm-block col-xs-block col-sm-full col-xs-full font-color-red">--}}
                                {{--@if(in_array('shippingfee',$coupon_type) and activeValidate('2018-07-31 00:00:00','2018-08-09'))--}}
                                    {{--<img style="width:70px;" src="{{assetRemote('image/label/labelshippingfree.gif')}}">--}}
                                {{--@endif--}}
                                {{--每次購物僅限用一張，折扣僅折抵商品金額。--}}
                            {{--</td>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}

                            {{--</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td class="col-md-3 hidden-sm hidden-xs">使用點數：</td>
                            <td class="point-block">
                                <span class="point-title">
                                    <span>點數
                                        <i class="fa fa-question-circle"></i>
                                    </span>
                                </span>
                                {{--<form name="PointsForm" method="post" action="{{route('cart-points')}}">--}}
                                    <input class="form-control" type="number" name="points" value="{{ request()->session()->get('usage-points' , '0') }}"/>
                                    {{--<input class="form-control" type="text" name="points" value="{{ request()->session()->get('usage-points' , '0') }}" />--}}
                                {{--</form>--}}
                                <!--*******************待刪除 by Jason(要拔掉)************************-->
                                {{--<div class="row">--}}
                                    {{--<div class="col-sm-12 col-xs-12 visible-sm visible-xs">--}}
                                        {{--<button class="btn btn-default base-btn-gray btn-full" onclick="document.PointsForm.submit()">--}}
                                            {{--輸入--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="point-total">
                                    <span> / {{ $current_customer->getCurrentPoints() }}</span>
                                </div>
                            </td>
                            <!--*******************待刪除 by Jason(要拔掉)************************-->
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                                {{--<button class="btn btn-default base-btn-gray history-btn-default" onclick="document.PointsForm.submit()">--}}
                                    {{--輸入--}}
                                {{--</button>--}}
                            {{--</td>--}}
                        </tr>
                        {{--<tr>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs"></td>--}}
                            {{--<td class="col-md-6 col-sm-block col-xs-block col-sm-full col-xs-full font-color-red">--}}
                                {{--現有{{ $current_customer->getCurrentPoints() }}點 / 最多可使用{{ $service->getMaxAllowPoints() >= $current_customer->getCurrentPoints() ? $current_customer->getCurrentPoints() : $service->getMaxAllowPoints() }}點--}}
                            {{--</td>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs"></td>--}}

                            {{--</td>--}}
                        {{--</tr>--}}
                    </table>
                </li>
                <li class="ct-cart-page-detail-sum-right clearfix">
                    <ul>
                        <li>
                            <label>運費：</label>
                            <span class="shipping_fee">{{ $service->getFee() }}</span>
                            {{--@if(hasChangeDealerShippingFee())--}}
                            {{--@if($current_customer and $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE)--}}
                            {{--@php--}}
                            {{--if($customerLevel = $current_customer->getCurrentLevelData()){--}}
                            {{--if(isset($customerLevel['cart_text'])){--}}
                            {{--$shipping_fee_text = $customerLevel['cart_text'];--}}
                            {{--}else{--}}
                            {{--$shipping_fee_text = '※10/1運費調降至'.\Ecommerce\Shopping\Shipping\HctShipping::SHIPPING_FEE_D.'元，結束滿額免運活動。';--}}
                            {{--}--}}
                            {{--}else{--}}
                            {{--$shipping_fee_text = '※10/1運費調降至'.\Ecommerce\Shopping\Shipping\HctShipping::SHIPPING_FEE_D.'元，結束滿額免運活動。';--}}
                            {{--}--}}
                            {{--@endphp--}}
                            {{--@else--}}
                            {{--@php--}}
                            {{--$shipping_fee_text = '※10/1運費調降至'.\Ecommerce\Shopping\Shipping\HctShipping::SHIPPING_FEE_C.'元，結束滿額免運活動。';--}}
                            {{--@endphp--}}
                            {{--@endif--}}
                            {{--<a href="{{ URL::route('customer-rule',['rule_code'=>'member_rule_2#H']) }}" target="_blank"><label class="note width-full" style="cursor:pointer;text-decoration: underline;">{{ $shipping_fee_text }}</label></a>--}}
                            {{--@else--}}
                            {{--<a href="{{ URL::route('customer-rule',['rule_code'=>'member_rule_2#H']) }}" target="_blank"><label class="note width-full" style="cursor:pointer;text-decoration: underline;">{{ ($current_customer and $current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE) ? '※10/1運費調降至'.\Ecommerce\Shopping\Shipping\HctShipping::SHIPPING_FEE_D.'元，結束滿額免運活動。' : '※10/1運費調降至'.\Ecommerce\Shopping\Shipping\HctShipping::SHIPPING_FEE_C.'元，結束滿額免運活動。' }}</label></a>--}}
                            {{--@endif--}}
                            @if(activeShippingFree())
                                <div class="width-full" style="float: right;">
                                    <img style="width:70px;" src="{{assetRemote('image/label/cart.gif')}}">
                                </div>
                            @endif
                        </li>
                        {{--<li>--}}
                            {{--<label>優惠折抵:</label>--}}
                            {{--<span>-{{ $service->getDiscountTotal() }}</span>--}}
                        {{--</li>--}}
                        <li>
                            <label>合計：</label>
                            <span class="font-color-red calculating">計算中...</span>
                            <span class="font-color-red final-total-price">NT$ {{ number_format($service->getSubtotal() + $service->getFee()) }}</span>
                            {{--<span class="font-color-red">NT${{ number_format($service->getSubtotal() + $service->getFee()) }}</span>--}}
                            {{--@if ($celebration->getDiscount())--}}
                                {{--<label class="note width-full" >※{{ $celebration->getDescriptionInCart() }}</label>--}}
                            {{--@endif--}}
                            <!-- <script type="text/javascript">_paq.push(['trackEcommerceCartUpdate', {{$service->getSubtotal() + $service->getFee()}}]);</script> -->
                        </li>
                    </ul>
                    <div class="flex-box-child cart-checkout">
                        <a class="btn btn-danger btn-full font-bold" href="{{route('checkout')}}">結帳</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="ct-btn-cart-page-detail-sum">
            <div class="row flex-box col-xs-block">
                <div class="font-color-red flex-box-child christmas-promo-text get-received-points {{ $celebration->getLevelUpPrice() ? 'text-center' : '' }}" style="{{ $celebration->getLevelUpPrice() ? 'display:block;' : '' }}">
                    <!--*******************待刪除 by Jason(要拔掉)************************-->
                    {{--<span class="hidden-sm hidden-xs btn-label size-08rem">結帳 《此次購物商品配送完成後，您可再獲得 {{ number_format($service->getReceivedPoints()) }} 點》{{$service->hasPointDiscount() ? '，點數現折商品則不會累積點數。' : ''}}</span>--}}
                    <span class="max-points hidden">{{ $service->getMaxAllowPoints() >= $current_customer->getCurrentPoints() ? $current_customer->getCurrentPoints() : $service->getMaxAllowPoints() }}</span>
                    <span class="btn-label size-08rem get-received-points-text">商品配送完成後，獲得 <span class="receive-points">{{ number_format($service->getReceivedPoints()) }}</span> 點</span>
                    @if ($level_up_price = $celebration->getLevelUpPrice() and $next_level_discount_price = $celebration->getNextLevelDiscountPrice() and $next_level_price = $celebration->getNextLevelPrice())
                        <span class="hidden-sm hidden-xs btn-label size-08rem">耶誕活動 《距離滿{{ number_format($next_level_price) }}折扣{{ number_format($next_level_discount_price) }}元。還差{{ number_format($level_up_price) }}元》</span>
                        <span class="hidden-md hidden-lg btn-label size-08rem">還差{{ number_format($level_up_price) }}元即可折抵{{ number_format($next_level_discount_price) }}元</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@else
    <div class="history-table-container">
        @if(count($gifts))
            <ul class="history-info-table history-table-adjust">
                {{--<li class="history-table-title">--}}
                    {{--<ul>--}}
                        {{--<li class="col-md-4 col-sm-12 col-xs-12"><span class="hidden-xs">商品</span><span class="text-left hidden-lg hidden-md hidden-sm" style="padding:0 10px">商品列表</span></li>--}}
                        {{--<li class="col-md-2 hidden-sm hidden-xs"><span>販賣價格</span></li>--}}
                        {{--<li class="col-md-2 hidden-sm hidden-xs"><span>數量</span></li>--}}
                        {{--<li class="col-md-2 hidden-sm hidden-xs"><span>小計</span></li>--}}
                        {{--<li class="col-md-2 hidden-sm hidden-xs"><span>管理</span></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <li class="history-table-content">
                    @foreach ($gifts as $giftKey => $gift)
                        <?php $product = $gift; ?>
                        @include('mobile.pages.cart.partials.gift')
                    @endforeach
                </li>
            </ul>
        @else
            <div class="container">
                <h2 class="center-box text-second-menu">目前購物車內沒有商品</h2>
            </div>
        @endif
    </div>
    <div class="checkout-container">
        <div class="ct-cart-page-detail-sum">
            <ul class="ul-cart-page-detail-sum">
                <li class="ct-cart-page-detail-sum-left">
                    <table class="table-page-detail-sum-left">
                        <tr>
                            <td class="col-md-3 hidden-sm hidden-xs">使用折價券：</td>
                            <td class="coupon-block">
                                <span class="coupon-title visible-sm visible-xs">折價券
                                    <i class="fa fa-question-circle"></i>
                                </span>
                                <form name="CouponForm" method="post" action="{{route('cart-coupon')}}">
                                    <select class="coupon-select form-control select2" name="code">
                                        <option value="">您目前有 {{ $coupons ? count($coupons) : 0 }} 張折價券</option>
                                        @php
                                            $coupon_type = [];
                                        @endphp
                                        @if ($coupons)
                                            @foreach ($coupons as $coupon)
                                                @php
                                                    $coupon_type[] = $coupon->discount_type;
                                                @endphp
                                                <option value="{{ $coupon->uuid }}" {{ $coupon->uuid == request()->session()->get('coupon-code')  ? 'selected' : ''}}>
                                                    {{ $coupon->name }}[NT{{ number_format($coupon->discount)}}]
                                                    @if($coupon->code == "1111celebrate")
                                                        {{date("m/d",strtotime("$coupon->expired_at -1 day"))." 23:59截止"}}
                                                    @endif
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </form>
                                <!--*******************待刪除 by Jason(要拔掉)************************-->
                                {{--<div class="row">--}}
                                    {{--<div class="col-sm-12 col-xs-12 visible-sm visible-xs">--}}
                                        {{--<button class="btn btn-default base-btn-gray btn-full" onclick="document.CouponForm.submit()">輸入</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </td>
                            <!--*******************待刪除 by Jason(要拔掉)************************-->
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                                {{--<button class="btn btn-default base-btn-gray history-btn-default" onclick="document.CouponForm.submit()">輸入</button>--}}
                            {{--</td>--}}
                        </tr>
                        {{--<tr>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                            {{--</td>--}}
                            {{--<td class="col-md-6 col-sm-block col-xs-block col-sm-full col-xs-full font-color-red">--}}
                                {{--@if(in_array('shippingfee',$coupon_type) and activeValidate('2018-07-31 00:00:00','2018-08-09'))--}}
                                    {{--<img style="width:70px;" src="{{assetRemote('image/label/labelshippingfree.gif')}}">--}}
                                {{--@endif--}}
                                {{--每次購物僅限用一張，折扣僅折抵商品金額。--}}
                            {{--</td>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td class="col-md-3 hidden-sm hidden-xs">使用點數：</td>
                            <td class="point-block">
                                <span class="point-title">
                                    <span>點數
                                        <i class="fa fa-question-circle"></i>
                                    </span>
                                </span>
                                <form name="PointsForm" method="post" action="{{route('cart-points')}}">
                                    <input class="form-control" type="text" name="points" value="{{ request()->session()->get('usage-points' , '0') }}" />
                                </form>
                                {{--<!--*******************待刪除 by Jason(要拔掉)************************-->--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-sm-12 col-xs-12 visible-sm visible-xs">--}}
                                {{--<button class="btn btn-default base-btn-gray btn-full" onclick="document.PointsForm.submit()">--}}
                                {{--輸入--}}
                                {{--</button>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            {{--</td>--}}
                            {{--<!--*******************待刪除 by Jason(要拔掉)************************-->--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs">--}}
                            {{--<button class="btn btn-default base-btn-gray history-btn-default" onclick="document.PointsForm.submit()">--}}
                            {{--輸入--}}
                            {{--</button>--}}
                            {{--</td>--}}
                            <div class="point-total">
                                <span> / {{ $current_customer->getCurrentPoints() }}</span>
                            </div>
                        </tr>
                        {{--<tr>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs"></td>--}}
                            {{--<td class="col-md-6 col-sm-block col-xs-block col-sm-full col-xs-full font-color-red">--}}
                                {{--現有{{ $current_customer->getCurrentPoints() }}點 / 最多可使用{{ $service->getMaxAllowPoints() >= $current_customer->getCurrentPoints() ? $current_customer->getCurrentPoints() : $service->getMaxAllowPoints() }}點--}}
                            {{--</td>--}}
                            {{--<td class="col-md-3 hidden-sm hidden-xs"></td>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    </table>
                </li>
                <li class="ct-cart-page-detail-sum-right clearfix">
                    <ul>
                        <li>
                            <label>運費：</label>
                            <span> - </span>
                            @if(activeShippingFree())
                                <div class="width-full" style="float: right;">
                                    <img style="width:70px;" src="{{assetRemote('image/label/cart.gif')}}">
                                </div>
                            @endif
                        </li>
                        {{--<li>--}}
                            {{--<label>優惠折抵:</label>--}}
                            {{--<span> - </span>--}}
                        {{--</li>--}}
                        <li>
                            <label>合計：</label>
                            <span class="font-color-red"> - </span>
                            <!-- <script type="text/javascript">_paq.push(['trackEcommerceCartUpdate',0]);</script> -->
                        </li>
                    </ul>
                    <div class="flex-box-child continue-shopping">
                        <a class="btn btn-default btn-full" href="{{route('shopping')}}" onclick="history.back();">繼續購物</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="ct-btn-cart-page-detail-sum get-received-points">
            <span class="font-color-red size-08rem">目前購物車內沒有商品</span>
        </div>
    </div>
@endif