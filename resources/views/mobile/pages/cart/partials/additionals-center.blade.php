@foreach($additional_collection as $area)
    <div class="addPurchase additional-area">
        <div class="addPurchase-title"><p>{{ $area->title }}</p></div>
        <div class="owl-carousel owl-theme center">
            @foreach($area->items as $item)
            <div class="item additional-item">
                <figure class="zoom-image">
                    @if($item->icon_flg)
                        <!-- <div class="sale-off-icon"><span>${{ number_format($item->price) }}元</span></div> -->
                            <div class="sale-off-icon"><span>特惠</span></div>
                    @endif
                    <img src="{{ $item->thumbnail }}" alt="{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}" >
                </figure>
                <a class="additional-item-title" href="{{ $item->url }}" title="{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}" target="_blank">
                    <p class='dotted-text3'>{{ formatProductFullName($item->product_name, $item->manufacturer_name) }}</p>
                </a>
                <label for="">NT${{ number_format($item->price) }}</label>
                <a class="additional-to-cart" href="">加入購物車</a>

                {{-- <a class="additional-to-cart" href="">已售完或已加入</a> --}}

            </div>
            
            @endforeach
        </div>

    </div>
@endforeach

<script>
    
</script>