@php
    $sold_out = (isset($carts_stock_info[$product->sku]) and $carts_stock_info[$product->sku]['sold_out']) ? true : false;
    $delivery = $carts_stock_info ? $carts_stock_info :null;
    $image_cover_types = [\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY];
    $estimate_time = date('m/d H:i',strtotime(date('Y-m-d H:i:s')));
    $max = 999;
    if(isset($delivery[$product->sku]) and isset($delivery[$product->sku]['stock_type']) and $delivery[$product->sku]['stock_type'] == 'tw_stock'){
        $max = $delivery[$product->sku]['stock'];
    }
@endphp
<ul class="history-table-item clearfix product-block {{ $sold_out ? 'sold_out sold_out_color' : ''}}" id="{{ $sold_out ? 'sold_out' : ''}}" data-title = "{{ $product->id }}">
    {{--<form method="post" action="{{route('cart-remove')}}">--}}
        <div class="btn-default-block clearfix">
            <input name="code" type="hidden" value="{{ $cart->protect_code }}">
            <button type="button" class="btn-default cart-remove">
                <i class="fa fa-close"></i>
            </button>
        </div>
    {{--</form>--}}
    <li class="history-table-item-content">
        <div class="row clearfix">
            <div class="history-table-item-img-block">
                <div class="history-table-item-img">
                    <a class="zoom-image" href="{{ route('product-detail', ['url_rewrite' => $product->url_rewrite]) }}" title="{{ $product->full_name }}">
                        <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
                    </a>
                </div>
            </div>
            <div class="info-brand box">
                <span class="name-brand-block" title="{{ $product->manufacturer->name }}">{{ $product->manufacturer->name }}</span>
                <h3>{{ $product->name }}</h3>
            </div>
            <div class="delivery-block size-08rem font-bold">
                <div class="delivery-text">
                    @if($delivery)
                        @if($sold_out)
                            {{ '商品售完，請選購其他商品。' }}
                            <span class="estimate_time"> {!! $estimate_time !!}</span>
                        @else
                            {!!  $delivery[$product->sku]['delivery_info']  !!}
                        @endif
                    @endif
                </div>
                <div class="helper-bottom"></div>
            </div>
        </div>

        {{-- ATTATENION !! Follow is Mobile Block , Modify Both Block if needed --}}
        <div class="history-content-estimate-block visible-sm visible-xs row">
            <div class="history-content-number">
{{--            <form class="form-horizontal" method="post" action="{{route('cart-update')}}">--}}
                @if($product->type == Everglory\Constants\ProductType::GROUPBUY)
                    <div class="determine-number flex-box">
                        {{--<label class="btn-label label-small start-label text-nowrap">數量：</label>--}}
                        <button class="btn btn-default btn-full" type="submit" disabled>無法修改</button>
                    </div>
                {{--</form>--}}
                @elseif($product->type == Everglory\Constants\ProductType::ADDITIONAL)
                    <div class="determine-number flex-box">
                        {{--<label class="btn-label label-small start-label text-nowrap">數量：</label>--}}
                        <button class="btn btn-default btn-full" type="submit" disabled>限一件</button>
                    </div>
                    {{--</form>--}}
                @else
                    <div class="determine-number flex-box">
                        <!--*******************待刪除 by Jason(僅顯示於PC)************************-->
                        {{--<label class="btn-label label-small start-label text-nowrap">數量：</label>--}}
                        {{--<input class="btn-label label-small width-full text-center" name="qty" type="text" value="{{ $cart->quantity }}">--}}
                        <input class="btn-label label-small width-full text-center" name="qty" type="number" min="0" max="{{$max}}" value="{{ $cart->quantity }}">
                        <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                    </div>
                    <!--*******************待刪除 by Jason(僅顯示於PC)************************-->
                    {{--<div class="col-sm-3 col-xs-3">--}}
                        {{--<button class="btn btn-default btn-small btn-full" type="submit">修改修改</button>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                @endif
            </div>
            <div class="history-content-price-block">

                <!--*********************by SIAN**************************-->
                <span class="font-color-red hidden-lg hidden-md hidden-sm">
                    NT$
                </span>
                <span class="font-color-red hidden-lg hidden-md hidden-sm history-content-price">
                    {{ number_format($product->getCartPrice($current_customer) * $cart->quantity ) }}
                </span>
                <span class="hidden unit_product_price">{{ (int)$product->getCartPrice($current_customer) }}</span>

                {{--<span class="font-color-red hidden-lg hidden-md hidden-sm">--}}
                    {{--NT${{ number_format($product->getCartPrice($current_customer) * $cart->quantity ) }}--}}
                {{--</span>--}}
            </div>
            <div class="toggle-box">
                <div class="toggle-button hidden-lg hidden-md hidden-sm text-right">
                    <a href="javascript:void(0)" onclick="toggleCartInfo(this);">詳細資訊</a>
                </div>
                <div class="toggle-content hidden-xs">
                    <div>商品編號：{{ $product->model_number }}</div>
                    @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                        <div>備註：{{$estimate_item->note}}</div>
                    @endif
                    @if($options = $product->getSelectsAndOptions() and $options->count())
                        <div class="btn-gap-top">
                            規格：<br>
                            @foreach($options as $option)
                                @php
                                    $explode_sign = ['：',':'];
                                    foreach($explode_sign as $sign){
                                        $option_content = explode($sign,$option->option);
                                        if(isset($option_content[1])){
                                            break;
                                        }
                                    }
                                    $option_title = str_replace('請選擇','',$option->label);
                                @endphp
                                @if(isset($option_content[1]))
                                    {{ $option->option }}<br>
                                @else
                                    {{ $option_title }}：{{ $option->option }}<br>
                                @endif
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
        {{-- Mobile Block END --}}
        <?php
        $categories = $product->categories->sortBy('depth')->pluck('name');
        $categories->shift();
        ?>
        <script type="text/javascript">
            /*
            _paq.push(['addEcommerceItem', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$categories->toArray()) !!}"],{{round($product->getCartPrice($current_customer))}},{{$cart->quantity}}]);
            */
        </script>
    </li>
    {{-- ATTATENION !! Follow is PC Block , Modify Both Block if needed --}}
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        @if($product->checkPointDiscount($current_customer))
            <span><s>NT${{ number_format($product->getFinalPrice($current_customer)) }}</s></span>
            <span class="font-color-red font-bold">點數現折價</span>
            <span class="font-color-red font-bold">NT${{ number_format($product->getCartPrice($current_customer)) }}</span>
        @else
            <span>NT${{ number_format($product->getCartPrice($current_customer)) }}</span>
        @endif
    </li>
    <li class="col-lg-2 col-md-2 history-content-cart-option hidden-sm hidden-xs">
        @if(in_array($product->type,[Everglory\Constants\ProductType::GROUPBUY,Everglory\Constants\ProductType::OUTLET]))
            無法修改數量
        @elseif(in_array($product->type,[Everglory\Constants\ProductType::ADDITIONAL]))
            限購一件
        @else
            <form class="row" method="post" action="{{route('cart-update')}}">
                <div class="col-lg-6 col-md-6">
                    <input class="btn-label btn-full text-center" name="qty" type="number" value="{{ $cart->quantity }}">
                    <input name="sku" type="hidden" value="{{ $product->url_rewrite }}">
                </div>
                <div class="col-lg-6 col-md-6">
                    <button class="btn btn-default btn-full" type="submit">修改</button>
                </div>
            </form>
        @endif
    </li>
    <li class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <span>NT${{ number_format($product->getCartPrice($current_customer) * $cart->quantity ) }}</span>
    </li>
    <li class="col-lg-2 col-md-2 col-sm-12 hidden-xs">
        <form method="post" action="{{route('cart-remove')}}">
            <input name="code" type="hidden" value="{{ $cart->protect_code }}">
            <span class="visible-md visible-lg">
                <div class="col-lg-3 col-md-3"></div>
                <div class="col-lg-6 col-md-6">
                    <button type="submit" class="btn btn-default">刪除</button>
                </div>
                <div class="col-lg-3 col-md-3"></div>
            </span>
        </form>
    </li>
    <!-- PC Block END -->
</ul>