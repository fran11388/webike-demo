@php
    $sold_out = (isset($carts_stock_info[$product->sku]) and $carts_stock_info[$product->sku]['sold_out']) ? true : false;
    $delivery = $carts_stock_info ? $carts_stock_info :null;
    $image_cover_types = [\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY];
    $estimate_time = date('m/d H:i',strtotime(date('Y-m-d H:i:s')));
    $max = 999;
    if(isset($delivery[$product->sku]) and isset($delivery[$product->sku]['stock_type']) and $delivery[$product->sku]['stock_type'] == 'tw_stock'){
        $max = $delivery[$product->sku]['stock'];
    }
@endphp
    <li class="gift {{((!count($carts) or $product->customer_gift_status == \Ecommerce\Service\GiftService::REWARD) and !$hasGiftInCart) ? "problem" : "" }}" data-title="{{ $product->id }}">
    <form class="" method="post" action="{{route('customer-history-gifts')}}">
        <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
        <input name="action" type="hidden" value="remove_cart">
        <input name="redirect" type="hidden" value="cart">
{{--         <div class="btn-default-block clearfix">
            <button type="submit" class="btn-default">
                <i class="fa fa-close"></i>
            </button>
        </div> --}}
    </form>
    <div class="left item-image-block">
        <a class="zoom-image" href="javascript:void(0)">
            <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
        </a>
    </div>
    <div class="right">
        <p class="item-name">{{ $product->manufacturer->name }} {{ $product->name }}</p>
        <?php /*
        @if($delivery and isset($delivery[$product->sku])))
            @if($sold_out)
                {{ '商品售完，請選購其他商品。' }}
                <span class="estimate_time"> {!! $estimate_time !!}</span>
            @else
                {!!  $delivery[$product->sku]['delivery_info']  !!}
            @endif
        @endif
        */ ?>
        {{-- 如果描述過長 點擊後向下展開 拿掉dotted-text4標籤 --}}
        <div class="about dotted-text4">
            <div>商品編號：{{ $product->model_number }}</div>
            @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                <div>備註：{{$estimate_item->note}}</div>
            @endif
            @if($options = $product->getSelectsAndOptions() and $options->count())
                <div class="btn-gap-top">
                    規格：<br>
                    @foreach($options as $option)
                        @php
                            $explode_sign = ['：',':'];
                            foreach($explode_sign as $sign){
                                $option_content = explode($sign,$option->option);
                                if(isset($option_content[1])){
                                    break;
                                }
                            }
                            $option_title = str_replace('請選擇','',$option->label);
                        @endphp
                        @if(isset($option_content[1]))
                            {{ $option->option }}<br>
                        @else
                            {{ $option_title }}：{{ $option->option }}<br>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="fixclear"></div>
    @if(!count($carts) or in_array($product->customer_gift_status, [\Ecommerce\Service\GiftService::REWARD, \Ecommerce\Service\GiftService::IN_CART]) )
        @if($hasGiftInCart)
            <div class="bottom row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <span><s>原價 NT${{ number_format($product->price) }}</s></span>
                    <p><span>NT$</span>0</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 number">
                    無法修改數量
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <form class="gift-form" method="post" action="{{route('customer-history-gifts')}}">
                        <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
                        <input name="action" type="hidden" value="remove_cart">
                        <input name="redirect" type="hidden" value="cart">
                        <button type="submit">下次再兌換</button>
                    </form>
                </div>
            </div>
        @else
            <div class="bottom row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <span><s>NT${{ number_format($product->price) }}</s></span>
                    <p><span>NT$</span>0</p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 number">
                    無法修改數量
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4"></div>
            </div>
        @endif
    @endif
</li>
@if(!$hasGiftInCart)
    <li class="tail">
        <form class="gift-form" method="post" action="{{route('customer-history-gifts')}}">
            <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
            <input name="action" type="hidden" value="add_cart">
            <input name="redirect" type="hidden" value="cart">
            <div class="gift-banner">
                <div class="gift-tip gift-add"><p>點擊兌換-當您購買商品時可免費兌換此商品</p></div>
            </div>
        </form>
    </li>
@else
    <li class="tail">
        <div class="gift-banner">
            <div class="gift-tip"><p>每次結帳僅能兌換一項</p></div>
        </div>
    </li>
@endif


<?php
$categories = $product->categories->sortBy('depth')->pluck('name');
$categories->shift();
?>
<script type="text/javascript">
    _paq.push(['addEcommerceItem', "{{$product->url_rewrite}}", "{{$product->name}}", ["{!! implode('","',$categories->toArray()) !!}"],{{round($product->getCartPrice($current_customer))}},1]);

</script>