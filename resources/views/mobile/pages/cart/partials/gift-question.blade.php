<li class="gift question" data-title="{{ $product->id }}">
    <form class="" method="post" action="{{route('customer-history-gifts')}}">
        <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
        <input name="action" type="hidden" value="remove_cart">
        <input name="redirect" type="hidden" value="cart">
{{--         <div class="btn-default-block clearfix">
            <button type="submit" class="btn-default">
                <i class="fa fa-close"></i>
            </button>
        </div> --}}
    </form>
    @if(!count($carts) or $product->customer_gift_status == \Ecommerce\Service\GiftService::REWARD)
        @if(!$hasGiftInCart)
            @if(!count($carts))
                <a class="btn btn-default btn-large size-08rem font-color-red" href="javascript:void(0)"><span class="hidden-lg hidden-md">購買商品可免費兌換</span><span class="hidden-sm hidden-xs">購物車無商品-當您購買商品時可免費兌換此贈品</span></a>
            @else
                <form method="post" action="{{route('customer-history-gifts')}}">
                    <input name="gift" type="hidden" value="{{ $product->customer_gift_id }}">
                    <input name="action" type="hidden" value="add_cart">
                    <input name="redirect" type="hidden" value="cart">
                    <button class="btn btn-default btn-large size-08rem font-color-red" type="submit"><span class="hidden-lg hidden-md">點擊後結帳可免費兌換</span><span class="hidden-sm hidden-xs">點擊兌換-當您購買商品時可免費兌換此贈品</span></button>
                </form>
            @endif
        @else
            <div class="center-btn">
                <a class="btn btn-default btn-large size-08rem font-color-red" href="javascript:void(0)"></a>
            </div>
        @endif
    @endif
    <div class="left item-image-block">
        <a class="zoom-image" href="javascript:void(0)">
            <img src="{{ $product->getThumbnail() }}" alt="{{ $product->full_name }}">
        </a>
    </div>
    <div class="right">
        <p class="item-name">{{ $product->manufacturer->name }} {{ $product->name }}</p>
{{--         @if($delivery)
            @if($sold_out)
                {{ '商品售完，請選購其他商品。' }}
                <span class="estimate_time"> {!! $estimate_time !!}</span>
            @else
                {!!  $delivery[$product->sku]['delivery_info']  !!}
            @endif
        @endif --}}
        {{-- 如果描述過長 點擊後向下展開 拿掉dotted-text4標籤 --}}
        <div class="about dotted-text4">
            <div>商品編號：{{ $product->model_number }}</div>
            @if($product->type == \Everglory\Constants\ProductType::GENUINEPARTS and $estimate_item = \Ecommerce\Service\EstimateService::getEstimateNote($product->id))
                <div>備註：{{$estimate_item->note}}</div>
            @endif
            @if($options = $product->getSelectsAndOptions() and $options->count())
                <div class="btn-gap-top">
                    規格：<br>
                    @foreach($options as $option)
                        @php
                            $explode_sign = ['：',':'];
                            foreach($explode_sign as $sign){
                                $option_content = explode($sign,$option->option);
                                if(isset($option_content[1])){
                                    break;
                                }
                            }
                            $option_title = str_replace('請選擇','',$option->label);
                        @endphp
                        @if(isset($option_content[1]))
                            {{ $option->option }}<br>
                        @else
                            {{ $option_title }}：{{ $option->option }}<br>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="fixclear"></div>
{{--     <div class="bottom">
        <div class="question"></div>
    </div> --}}
</li>
<div class="question-banner">
    <div class="question"><p>點擊兌換-當您購買商品時可免費兌換此商品</p></div>
</div>