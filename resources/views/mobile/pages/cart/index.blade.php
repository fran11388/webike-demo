@extends('mobile.layouts.mobile')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('mobile/css/pages/cartpagedetail.css') !!}">
    <style>
        @if ($celebration->getLevelUpPrice())
            .christmas-promo-text span.btn-label{
            display:block;
            padding:0px !important;
            height:25px !important;
        }
        @endif
        @media(max-width:500px){
            .ct-btn-cart-page-detail-sum .cart-checkout {
                margin-top:20px; !important
            }
        }

        body .swal2-shown:not(.swal2-no-backdrop):not(.swal2-toast-shown) {
            overflow-y: visible !important;
        }
        .estimate_time {
            display: block;
            width: 100%;
        }
        input.form-control {
            box-shadow: none;
        }
    </style>
@stop
@section('middle')
    @include('mobile.common.message.error')
    <div class="cart-title">
        <h1 class="common">我的購物車</h1>
    </div>
    <div class="section" id="cart">
        <h3 class="common">商品列表</h3>

        <!--*******************待刪除 by Jason(僅顯示於PC)************************-->
        {{--<div class="cart-page-detail-left box-fix-with visible-lg visible-md">--}}
            {{--@if(in_array($current_customer->role_id, [\Everglory\Constants\CustomerRole::STAFF, \Everglory\Constants\CustomerRole::WHOLESALE]))--}}
                {{--<a class="visible-lg visible-md btn btn-primary box" href={{URL::route('cart-quotes')}}>下載報價單</a>--}}
            {{--@endif--}}
            {{--<div class="box-page-group">--}}
                {{--<div class="title-box-page-group title-box-cart-page-detail">--}}
                    {{--<h3>加密安全防護</h3>--}}
                {{--</div>--}}
                {{--<div class="ct-box-page-group ct-btn-secured">--}}
                    {{--@include('mobile.common.ssl-logo')--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--@include('mobile.common.list.view-history-vertical')--}}
        {{--</div>--}}

        <!--*******************待刪除 by Jason************************-->
        {{--@if($service->getCount())--}}
            {{--<div class="row block hidden-lg hidden-md hidden-sm">--}}
                {{--<div class="col-xs-12">--}}
                    {{--<a class="btn btn-large btn-danger btn-full font-bold" href="{{route('checkout')}}">結帳 <i class="fa fa-chevron-right" aria-hidden="true"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row box hidden-xs">--}}
                {{--<div class="col-xs-6 col-sm-3 col-md-2">--}}
                    {{--<a class="btn btn-default btn-full" href="{{route('shopping')}}"><span class=""><i class="fa fa-chevron-left" aria-hidden="true"></i> 繼續購物</span></a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-sm-6 col-md-7 hidden-xs"></div>--}}
                {{--<div class="col-xs-6 col-sm-3 col-md-3">--}}
                    {{--<a class="pull-right btn btn-default btn-full" href="javascript:void(0)" onclick="slipTo('#additional')">精選加價購</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}

        @include('mobile.pages.cart.partials.carts')
        <div class="font-color-red join-addition-text text-center">加價購商品新增中...</div>
    </div>
    <div class="section" id="cart">
        <h3 class="additional-purchase common">加價購</h3>

        <!--*******************待刪除 by Jason(僅顯示於PC)************************-->
        {{--@include('mobile.pages.cart.partials.genuineparts')--}}
        {{--@if($service->getCount())--}}
            {{--<hr style="margin: 20px 0">--}}
            {{--<div class="hide" id="installment-table">--}}
                {{--<h3>--}}
                    {{--@if($current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE)--}}
                        {{--以上結帳金額提供經銷商分期服務，若有需要請在結帳頁面選擇付款方式(詳細說明請參閱"<a href="{!! route( 'service-dealer-installment-info' ) !!}" title="經銷商信用卡分期服務{!! $tail !!}" target="_blank">經銷商信用卡分期服務</a>")。--}}
                    {{--@else--}}
                        {{--信用卡分期服務，若有需要請在結帳頁面選擇付款方式(詳細說明請參閱"<a href="{!! route('customer-rule', 'member_rule_2') !!}#E_b" title="信用卡分期說明{!! $tail !!}" target="_blank">信用卡分期說明</a>")。--}}
                    {{--@endif--}}
                {{--</h3>--}}
                {{--@include('mobile.common.installments-table')--}}
            {{--</div>--}}
        {{--@endif--}}
        <div class="additional-block">
            @include('mobile.pages.cart.partials.promotions')
        </div>
        <div class="row box hidden-sm hidden-md hidden-lg">
            <div class="col-xs-12">
                <a class="btn btn-default btn-full" href="{{route('shopping')}}" onclick="history.back();"><span class=""><i class="fa fa-chevron-left" aria-hidden="true"></i> 繼續購物</span></a>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script type="text/javascript">
        //        *******************待刪除 by Jason(僅顯示於PC)************************
        //        function importInstallHtml(installments){
        //            $("#installment-table").find('.history-table-content').html('');
        //            $.each(installments, function(key, object){
        //                $("#installment-table").find('.history-table-content').append(object.html);
        //            });
        //            $("#installment-table").removeClass('hide');
        //        }
        //
        //        function initialInstallment(){
        //            $.get("./cart/installment", function (result) {
        //                if(result.length){
        //                    importInstallHtml(result);
        //                }
        //            });
        //        }
        //        initialInstallment();

        <!--*******************by SIAN************************-->

        $(window).load(function () {
            setTimeout( adjuctProductIBlock(), 100 );

        });

        function adjuctProductIBlock() {
            var HistoryImgwidth = $('.history-table-item-img-block').width();
            var GiftsImgwidth = $('.gifts-item-img-block').width();

            $('.history-table-item-img a').css('width', HistoryImgwidth);
            $('.history-table-item-img a').css('height', HistoryImgwidth);
            $('.gifts-item-img a').css('width', GiftsImgwidth);
            $('.gifts-item-img a').css('height', GiftsImgwidth);

            $('.history-table-adjust .history-table-content ul').each(function(){
                var infoBrandHeight = $(this).find('.info-brand').height() + 10;
                var historyHeight = $(this).find('.delivery-block').height();
                if((infoBrandHeight + historyHeight) > HistoryImgwidth || infoBrandHeight >= HistoryImgwidth){
                    $(this).find('.delivery-block').css('float','right');
                }else{
                    var deliveryHeight = HistoryImgwidth - $(this).find('.info-brand').height() - 10 + 2;
                    $(this).find('.delivery-block').height(deliveryHeight);
                }

            });
        }

        $('.coupon-title').click(function(event) {

            swal(
                '折價卷?',
                '每次購物僅限用一張<br>折扣僅折抵商品金額',
                'question'
            )

        });

        $('.point-title').click(function(event) {
            var max_points = $('.max-points').text();
            swal({
                title: '點數?',
                text: '現有{{ $current_customer->getCurrentPoints() }}點<br>最多可使用' +max_points + '點',
                type: 'question',
                animation: true
            })

        });

        $('.cart-checkout').click(function(e){
            if($('.product-block').hasClass('sold_out')){
                e.preventDefault();
                swal({
                    title: '提醒',
                    text: "請先刪除\"售完\"商品，即可進行結帳。",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33'
                }).then(function(result) {
                    slipTo1('#sold_out');
                });
            }
        });

        function slipTo1(element){
            if($(window).width() < 600) {
                var y = parseInt($(element).offset().top) - $('#sold_out').height() + 120;
                $('html,body').animate({scrollTop: y}, 400);
            }else{
                var y = parseInt($(element).offset().top) - $('#sold_out').height();
                $('html,body').animate({scrollTop: y}, 400);
            }
        }

        $('.determine-number input[name=qty]').on('change', function () {

            var DetermineNumber = parseInt($(this).val());
            var maxQuantity = parseInt($(this).prop('max'));
            if(DetermineNumber > maxQuantity){
                $(this).val(maxQuantity);
                DetermineNumber = maxQuantity;
            }

            var UnitPrice = $(this).parent().parent().parent().find('.unit_product_price').text();
            var FinalItemPrice = parseInt(UnitPrice) * DetermineNumber;
            $('input[name=points]').prop('disabled',true);
            $('input[name=qty]').prop('disabled',true);
            $('select[name=code]').prop('disabled',true);

            if (DetermineNumber <= 0) {
                $(this).val(1);
                $(this).parent().parent().parent().find('.history-content-price').text(number_format(parseInt(UnitPrice)));

            } else if (DetermineNumber >= 1) {

                $(this).parent().parent().parent().find('.history-content-price').text(number_format(FinalItemPrice));
            }

            var sku = $(this).next().val();
            $('input[name=points]').prop('disabled',true);
            $('select[name=code]').prop('disabled',true);
            changeActivity();

            $.ajax({
                url: "{!! URL::route('cart-update') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content') ,'qty' : DetermineNumber ,'sku' : sku},
                type:"POST",
                dataType:'html',

                success: function(result){
                    setCartTotalPrice();
                    reloadAddition();
                    checkProduct();
                },
                error:function(xhr, ajaxOptions, thrownError){
                    swal(
                        '發生錯誤...請重新整理'
                    )
                }
            });

        });

        function number_format(num) {
            num = num + "";
            var re = /(-?\d+)(\d{3})/
            while (re.test(num)) {
                num = num.replace(re, "$1,$2")
            }

            return num;
        }

        function setCartTotalPrice() {
            $.ajax({
                url: "{!! URL::route('cart-get-total-price') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content')},
                type:"POST",
                dataType:'json',

                success: function(result){
                    var check_href = "{{route('checkout')}}";
                    $('.final-total-price').text(result.final_price);
                    $('.calculating').hide();
                    $('.final-total-price').show();
                    $('input[name=points]').prop('disabled',false);
                    $('select[name=code]').prop('disabled',false);
                    $('input[name=qty]').prop('disabled',false);
                    $('.cart-checkout a').attr('href',check_href);
                    $('.receive-points').text(result.receive_points);
                    $('.max-points').text(result.max_points);
                    $('input[name=points]').val(result.now_use_points);
                    $('.shipping_fee').text(result.shipping_fee);
                },

                error:function(xhr, ajaxOptions, thrownError){
                }
            });
        }

        function changeActivity()
        {
            $('.calculating').show();
            $('.final-total-price').hide();
            $('.cart-checkout a').attr('href','javascript:void(0)');
            $('.addition-standard').hide();
            $('.addition-loading').show();
        }

        function reloadAddition()
        {
            $.ajax({
                url: "{!! URL::route('cart-reload-addition') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content')},
                type:"POST",
                dataType:'html',

                success: function(result){
                    $('.additional-block').html(result);
                },

                error:function(xhr, ajaxOptions, thrownError){
                }
            });
        }

        $(document).on('click','.join-addition',function(){
            $('input[name=points]').prop('disabled',true);
            $('input[name=qty]').prop('disabled',true);
            $('select[name=code]').prop('disabled',true);

            var qty = $(this).closest('.join-addition-block').find("input[name=qty]").val();
            var sku = $(this).closest('.join-addition-block').find("input[name=sku]").val();
            var joinAddition = true;
            $('.join-addition-text').show();

            changeActivity();

            $.ajax({
                url: "{!! URL::route('cart-update') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content') ,'qty' : qty ,'sku' : sku},
                type:"POST",
                dataType:'html',

                success: function(result){
                    joinAdditionProduct(sku,qty);
                    setCartTotalPrice();
                    reloadAddition();
                    checkProduct();
                },
                error:function(xhr, ajaxOptions, thrownError){
                    swal(
                        '發生錯誤...請重新整理'
                    )
                }
            });
        });

        function joinAdditionProduct(sku,qty)
        {
            $.ajax({
                url: "{!! URL::route('cart-join-addition-product') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content') ,'qty' : qty ,'sku' : sku},
                type:"POST",
                dataType:'html',

                success: function(result){
                    if(result){
                        $('.join-addition-text').hide();
                        $('#cart .history-table-container .history-table-adjust .history-table-content').append(result).fadeIn(1000);
                        adjuctProductIBlock();
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){
                    swal(
                        '發生錯誤...請重新整理'
                    )
                }
            });
        }

        function checkProduct()
        {
            $.ajax({
                url: "{!! URL::route('cart-check-product') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content')},
                type:"POST",
                dataType:'json',

                success: function(result){
                    if(result){
                        var num;
                        var check = false;
                        $('ul.product-block').each(function(){
                            check = false;
                            var product_id = $(this).data('title');
                            $.each(result,function(index,id) {
                                if(product_id == id){
                                    check = true;
                                }
                            });
                            if(check === false){
                                $(this).fadeOut(1000);
                            }
                        });
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){
                    swal(
                        '發生錯誤...請重新整理'
                    )
                }
            });
        }

        $(document).on('click','.cart-remove',function(){
            var protected_code = $(this).closest('div').find('input[name=code]').val();
            $(this).closest('.product-block').removeClass('sold_out').removeAttr('id');
            $(this).closest('.history-table-item').fadeOut(1000);
            $('input[name=points]').prop('disabled',true);
            $('input[name=qty]').prop('disabled',true);
            $('select[name=code]').prop('disabled',true);

            changeActivity();

            $.ajax({
                url: "{!! URL::route('cart-remove') !!}",
                data: {'_token': $('meta[name=csrf-token]').prop('content') ,'code' : protected_code},
                type:"POST",
                dataType:'html',

                success: function(result){
                    setCartTotalPrice();
                    reloadAddition();
                    checkProduct();
                },

                error:function(xhr, ajaxOptions, thrownError){
                    swal(
                        '發生錯誤...請重新整理'
                    )
                }
            });
        });





        {{--$('.coupon-select').on('change', function () {--}}

        {{--calculateFinalTotal();--}}

        {{--});--}}

        {{--$('.form-control').on('change', function () {--}}

        {{--calculateFinalTotal();--}}

        {{--});--}}


        // function calculateFinalTotal(){
        //
        //     var ProductPrice = calculateItemPrice();
        //     // var Coupon = $('.coupon-select').find('option:selected').val();
        //     // var Point = $('.point-block input').val();
        //     // var DiscountTotal = parseInt(Coupon) + parseInt(Point);
        //     // var FinalTotalPrice = parseInt(ProductPrice) - parseInt(DiscountTotal);
        //     var FinalTotalPrice = parseInt(ProductPrice)
        //
        //     $('.final-total-price').text(FinalTotalPrice);
        //
        //     return FinalTotalPrice;
        //
        // }

    </script>
@stop