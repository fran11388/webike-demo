@if(isset($advertiser))
    <div class="section clearfix">
        @if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE]))
            <!-- Ad banner owl -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                {!! $advertiser->call(12) !!}
            </div>
        @else
            <!-- Ad banner owl -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-content">
                {!! $advertiser->call(2) !!}
            </div>
        @endif
    </div>
@endif