@extends('mobile.layouts.mobile')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/genuineparts-index.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/transform_keyframes.css') }}">
	<link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/genuineparts-relieved-shopping.css') !!}">
	<style type="text/css">
		.relieved-shopping {
		    margin: 15px 0 0 10px;
		}
	</style>
@stop
@section('middle')
<div class="genuineparts-menu-icon menu-icon-fixed">
	<a href="javascript:void(0)">
		<i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i>
	</a>
</div>
<div class="genuineparts-container">
	<div class="section top">
		<div class="genuineparts-title">
			<h1 class="common col-xs-10">正廠零件查詢購買系統</h1>
			<div class="genuineparts-menu-icon col-xs-2"><i class="fa fa-info-circle font-color-red" aria-hidden="true"></i></div>
			<div class="genuineparts-close_btn">×</div>
			<div class="genuineparts-menu_items">
				<div class="genuineparts-inner">
					<dl>
						<dt>正廠零件查詢購買系統</dt>
						@include('mobile.pages.genuineparts.partials.index-manual')

					</dl>
				</div>
			</div>
			<div class="genuineparts-overlay"></div>
		</div>
		@include('mobile.pages.genuineparts.partials.top')
	</div>
	@foreach($sect_manufacturers as $name => $collection)
        @if(isset($collection['group']))
    		<div class="section">
	            <div class="box-items-group">
	                <div class="title-main-box clearfix">
	                    <h3 style="{{ $name == '日本' ? 'float:left;margin-right:10px;' : '' }}" class="common">
	                    	@foreach($collection['icons'] as $icon)
	                        	<img src="{!! assetRemote($icon) !!}" alt="icon flag"/>
	                    	@endforeach
	                    	<span>{!! $name !!}廠牌正廠零件</span>
	                    </h3>
                        @if($name == '日本')
                            <span class="realtime-tag"><img src="{{ assetRemote('image/label/real_time.gif') }}" alt="正廠零件即時回覆"></span>
                        @endif
	                </div>
	                <div class="ct-box-items-group">
	                    <ul class="ul-ct-box-items-group clearfix">
	                        @foreach($collection['group'] as $genuine_manufacturer)
	                            <li><a style="display: table-cell;" class="logo-brand logo-harley" href="{!! URL::route('genuineparts-divide', [$collection['url_code'], $genuine_manufacturer->api_usage_name]) !!}"><img src="{!! assetRemote('image/oempart/'.$genuine_manufacturer->api_usage_name.'.png') !!}" alt="logo brand"/></a></li>
	                        @endforeach
	                    </ul>
	                </div>
	            </div>
    		</div>
        @endif
    @endforeach
</div>
@stop
@section('script')
	<script type="text/javascript">
    	$(".genuineparts-menu-icon, .genuineparts-overlay, .genuineparts-close_btn").on('click', function(){
			if($(".genuineparts-menu_items").css("display") != "block"){
				$("html").css("overflow","hidden");
				$("html").css("position","relative");
				$("#w_spng_sp").css("position","relative");
				var winHeight = $(window).height() + 20;
				$(".genuineparts-menu_items").height(winHeight).scrollTop(0);
				$(".genuineparts-menu_items .genuineparts-inner").css("min-height",winHeight);
				$(".genuineparts-menu_items").removeClass("hide").addClass("show");
				$(".genuineparts-overlay,  .genuineparts-close_btn").show();
			}else{		
				$(".genuineparts-overlay, .genuineparts-close_btn").hide();
				$(".genuineparts-menu_items dl dd ul").hide();
				$(".genuineparts-menu_items").removeClass("show").addClass("hide");
				setTimeout(function(){$(".genuineparts-menu_items").removeClass("hide");},500);
				$(".genuineparts-menu_items a").removeClass("open");
				$("#w_spng_sp").css("position","absolute");
				$("body, html").css("overflow","");
				$("html").css("position","");
			}
		});
	</script>
@stop