@extends('mobile.layouts.mobile')
@section('style')
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/genuineparts-realtime_divide.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/transform_keyframes.css') }}">
    <link rel="stylesheet" type="text/css"  href="{!! assetRemote('css/pages/genuineparts-relieved-shopping.css') !!}">
    <style type="text/css">
        .relieved-shopping {
            margin: 15px 0 0 10px;
        }
    </style>
@stop
@section('middle')
<div class="genuineparts-menu-icon menu-icon-fixed">
	<a href="javascript:void(0)">
		<i class="glyphicon glyphicon-triangle-left" aria-hidden="true"></i>
	</a>
</div>
<div class="pages-action active" id="page-1">
	<div class="section top">
		<div class="genuineparts-title">
            @include('response.pages.product-detail.partials.goldenweek-tag')
	    	<h1 class="common col-xs-10">{!! $divide_name !!}正廠零件查詢購買系統</h1>	
		    <div class="genuineparts-menu-icon col-xs-2"><i class="fa fa-info-circle font-color-red" aria-hidden="true"></i></div>
		    <div class="genuineparts-close_btn">×</div>
		    <div class="genuineparts-menu_items">
    	        <div class="genuineparts-inner">
		            <dl>
		                <dt>正廠零件查詢購買系統</dt>
						@include('mobile.pages.genuineparts.partials.manual-'. $divide)
		  
		            </dl>
		        </div>
		    </div>
		    <div class="genuineparts-overlay"></div>
		</div>
		@include('mobile.pages.genuineparts.partials.top')
	</div>
	<div class="section manufacturer">
		<h3 class="common">{!! $divide_name !!}摩托車正廠零件廠牌選擇</h3>
		<div>
            <ul class="title-box-tap clearfix">
                @foreach($target_sents as $name => $collection)
                    <li class="tap-box-country tap-box-{!! $collection['code'] !!}">
                        <a href="javascript:void(0)">
                            @foreach($collection['icons'] as $icon)
                                <img src="{!! assetRemote($icon) !!}" alt="icon flag"/>
                            @endforeach
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="ct-box-part-number clearfix">
                @foreach($target_sents as $name => $collection)
                    <ul class="ul-ct-box-items-group ct-tap-country" id="{!! $collection['code'] !!}">
                        @foreach($collection['group'] as $genuine_manufacturer)
                            <li class="logo-brand">
                                <a style="display: table-cell;" href="javascript:void(0)" name="{!! $genuine_manufacturer->api_usage_name !!}">
                                    <img class="img-brand-black" src="{!! assetRemote('image/oempart/'.$genuine_manufacturer->api_usage_name.'-black.png') !!}" alt="logo brand">
                                    <img class="img-brand-active" src="{!! assetRemote('image/oempart/'.$genuine_manufacturer->api_usage_name.'.png') !!}" alt="logo brand">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endforeach
                <div class="ct-show-info-brand">
                    @foreach($target_sents as $name => $collection)
                        @foreach($collection['group'] as $genuine_manufacturer)
                            <div class="box-items-group-show box-fix-column container-show-info-brand show-info-brand-{!! $genuine_manufacturer->api_usage_name !!}">
                                <div class="box-items-group-show-right box-fix-auto">
                                    {!! $genuine_manufacturer->caution !!}
                                </div>
                            </div>
                        @endforeach
                    @endforeach

                </div>
            </div>
		</div>
	</div>
	<div class="section">
		<h3 class="common">請輸入<cite class="manufacturer-name"></cite>正廠零件料號及數量</h3>
	    <div class="content-box-part-number">
			<span>請輸入半形英文及數字(英文一律大寫)，請不要留空格。單一料件查詢個數上限99個。</span>
	        <div id="genuineparts-table" class="rows">
	            <form id="genuineparts-form" method="post" action="{{URL::route('realTime-genuineparts-estimate')}}">
	                <ul class="title clearfix genuineparts-block">
	                    <li >No.</li>
	                    <li >零件料號</li>
	                    <li >數量</li>
	                    <li >備註</li>
	                </ul>
	                <div class="daterow">
	                    @for($i=1;$i<=10;$i++)
	                        <ul class="item clearfix">
	                            <li><span class="number">{!! $i !!}</span>.</li>
	                            <li><input class="steps step-3" type="text" name="syouhin_code[]"></li>
	                            <li><input class="steps step-3" type="text" name="qty[]"></li>
	                            <li><input class="steps step-3" type="text" name="note[]"></li>
	                        </ul>
	                    @endfor
	                </div>
	                <div class="bottom-genuineparts-table">
	                    <div class="text-center genuineparts-block"> <a class="btn btn-default" onclick="addRows(this);">追加更多欄位(10項)</a></div>
	                    <div class="box-items-group-show">
	                        <div class="ct-more-box-items-group-show">
	                            1. 請務必輸入正確的零件料號，中、英文名稱等其他編號不能用來查詢。<br>
	                            2. 外觀零組件請務必確認好零件料號，以免造成顏色錯誤的問題。<br>
	                            3. 部分料號於手冊中備註標示為""整組""或是""整套""販售時，即無法拆售。<br>
	                            4. 螺絲、墊片等細小零件如數量未達原廠包裝標準數量時，可能會有單獨包裝或是無法販售的狀況。<br>
	                            5. 2001年以前出廠的車輛，部分零件已經停止供應，可能會有斷料的狀況。<br>
	                            6. 若您有任何疑問請”正廠零件系統提問”，零件查詢問題請洽「Webike-實體經銷商」<br>
	                        </div>
	                    </div>
	                    <span class="font-color-red">注意事項：請務必閱讀，按下”送出查詢”視同您已同意相關規定</span>
	                    <div class="text-center btn-gap-top"> <a class="btn btn-danger disabled go-next" style="display:none;">送出查詢</a></div>
	                </div>
	                <input name="divide" type="hidden" value="{!! $divide !!}">
	                @if(isset($target_manufacturer))
	                    <input id="select_manufacturer_code" type="hidden" name="api_usage_name" value="{!! $target_manufacturer->api_usage_name !!}">
	                    <input id="target_manufacturer" type="hidden" value="{!! $target_manufacturer->api_usage_name !!}">
	                    <input id="target_sent_code" type="hidden" value="{!! $target_sent_code !!}">
	                @else
	                    <input id="select_manufacturer_code" name="api_usage_name" type="hidden" value="">
	                @endif
	                @if(isset($name_compares))
	                    <textarea id="name_compares" class="hide" >{!! $name_compares !!}</textarea>
	                @endif
	            </form>
	        </div>
	    </div>
	</div>
</div>
<div class="pages-action" id="page-2">

	<h1 class="common">正廠零件查詢資料確認</h1>
	<div class="page-block">
	    <div class="box">
	        <span class="size-08rem">※請確認廠牌、料號及商品數量，確定後請按下"送出查詢"。</span>
	    </div>
	    <ul class="table-main-info">
	        <li class="table-main-title clearfix">
	            <ul>
	                <li class="col-xs-5"><h2>零件料號</h2></li>
	                <li class="col-xs-2"><h2>數量</h2></li>
	                <li class="col-xs-5"><h2>備註</h2></li>
	            </ul>
	        </li>
	        <li class="table-main-content clearfix" id="genuineparts-confirm-table">

	        </li>
	    </ul>
	    <div class="btn-box clearfix">
	        <a href="javascript:void(0)" class="btn btn-default pull-left" onclick="goPrevPage();">回上頁修改</a>
	        <a href="javascript:void(0)" class="btn btn-danger pull-right" onclick="doEstimate(this);">送出查詢</a>
	    </div>
	</div>	
</div>
@stop
@section('script')
    <script>
        var select_manufacturer = '#select_manufacturer';
        var select_manufacturer_code = '#select_manufacturer_code';
        var manufacturer_name = '.manufacturer-name';
        var tab_class = '.tap-box-country';
        var target_manufacturer = '#target_manufacturer';
        var target_sent_code = '#target_sent_code';
        var name_compares = '#name_compares';
        var go_next = '.go-next';


        $('#menu-icon').click(function(){

        });

        function doEstimate(_this){
            if($(_this).attr('disabled') != 'disabled'){
                var has_item = false;
                $('input[name^="syouhin_code"]').each(function(key, element){
                    var qty = $(element).closest('ul.item').find('input[name^=qty]');
                    if(qty.val() > 0 && $(element).val()){
                        has_item = true;
                        return false;
                    }
                });
                	
                if(has_item){
                    swal({
                        title: '您確定要送出嗎?',
                        text: "點選「確認送出」後將送出查詢。",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '送出查詢',
                        cancelButtonText: '取消',
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#777',
                    }).then(function () {

                        var step = false;
                        var postData = $('#genuineparts-form').serializeArray();
                        var divide = $('input[name="divide"]').val();
                        var api_usage_name = $('input[name="api_usage_name"]').val();
                        var syouhin_code = [];
                        var qty = [];
                        var note = [];
                        $('input[name="syouhin_code[]"]').each(function(key,val){
                            syouhin_code[key] = $(this).val();
                        });
                        $('input[name="qty[]"]').each(function(key,val){
                            qty[key] = $(this).val();
                        });
                        $('input[name="note[]"]').each(function(key,val){
                            note[key] = $(this).val();
                        });
                       
                        $.ajax({
                            url: "{!! route('realTime-genuineparts-estimate') !!}",
                            data: {_token: $('meta[name=csrf-token]').prop('content'),api_usage_name: api_usage_name,step: step,divide: divide,syouhin_code: syouhin_code,qty: qty,note: note},
                            type: 'POST',
                            dataType: 'json',
                            timeout: 200000,
                            success: function(result){
                                if(result.success){
                                    link = "{{ \URL::route('genuineparts-estimate-redirect-estimate-done') }}" + "?code=" + result.code;
                                    $('.realtime-genuineparts').attr('href',link).show(1000);
                                    $('.realtime-genuineparts-text').show();
                                    setTimeout(function() {
                                        window.location.href = link;
                                    }, 100000);

                                    if(result.code){
                                        stepOther(result);
                                    }
                                }else{
                                    if(result.type == 'not_login'){
                                        window.location.href = "{{ \URL::route('login') }}";
                                    }else{
                                        alert(result.message);
                                    }
                                }

                            },
                            error: function(xhr, ajaxOption, thrownError){
                                window.location.href = "{{ \URL::route('customer-history-genuineparts') }}";
                            }
                        });

                        swal({
                            title: '正廠零件查詢中',
                            html: "<span>請稍後約10秒鐘<span><span class='realtime-genuineparts-text' style='display:none;'>，若不想等待請按 <a href='' class='realtime-genuineparts' style='text-decoration: underline'>\"查詢完畢再通知\"</a></span>",
                            allowOutsideClick: false,
                            onOpen: function () {
                                swal.showLoading()
                            }

                        }).then(
                            function () {},
                            // handling the promise rejection
                            function (dismiss) {
                                if (dismiss === 'timer') {
                                    console.log('I was closed by the timer')
                                }
                            }
                        );

                        $(_this).attr('disabled',true);
//                        $('#genuineparts-form').submit();
                    }, function (dismiss) {
                        if (dismiss === 'cancel') {
                            return false;
                        }
                    })
                }
            }
        }

        function stepOther(result){
            var estimate_code = result.code;
            var divide = result.divide;
            var curl_para = result.curl_para;
            var step = true;
            $.ajax({
                url: "{!! route('realTime-genuineparts-estimate') !!}",
                data: {_token: $('meta[name=csrf-token]').prop('content'),estimate_code: estimate_code,step: step,divide: divide,curl_para: curl_para},
                type: 'POST',
                dataType: 'json',
                success: function(result){
                    if(result.real_time){
                        window.location.href = "{{request()->getSchemeAndHttpHost()}}" + '/customer/history/genuineparts/detail/' + result.code;
                    }else{
                        window.location.href = "{{ \URL::route('genuineparts-estimate-redirect-estimate-done') }}" + "?code=" + result.code;
                    }
                },
                error: function(xhr, ajaxOption, thrownError){

                }
            });
        }
        /*
         * manufacturer tab click action
         */
        $(tab_class).click(function(){
            $('[class*=show-info-brand-]').hide();
            $(this).addClass("active").siblings('li').removeClass('active');
            $(".container-show-info-brand").hide();
            $('.logo-brand').removeClass('active');
            var classes = $(this).prop('class').split(' ');
            var targetId = null;
            var indexStr = 'tap-box-';
            $.each(classes, function(key, value){
                if(value.indexOf(indexStr) >= 0 && value != 'tap-box-country'){
                    targetId = value.substr(indexStr.length, value.length);
                }
            });
            $('#' + targetId).addClass("active-ct-tap-country").siblings('ul').removeClass('active-ct-tap-country');
        });

        /*
         * manufacturer image click action
         */
        $(".logo-brand a").click(function(){
            var url_code = $(this).prop('name');
            $('[class*=show-info-brand-]').hide();
            $('.show-info-brand-' + url_code).fadeIn();
            var compares = $.parseJSON($(name_compares).val());
            var name = compares[url_code];
            $(manufacturer_name).text(name);
            $(select_manufacturer_code).val(url_code);
        });

        /*
         * add cols button click action
         */
        function addRows(element) {
            var area = '#genuineparts-table .daterow';
            var num = parseInt($('#genuineparts-table .daterow ul:last-child li span').text());
	            for (var i=1;i<=10;i++){
	                var newRow = $(area + ' ul.item').last().clone();
	                var rownum = num+i;
	                newRow.find('li span').text("");
	                newRow.find('li input').val("");
	                newRow.find('li div').remove();
	                newRow.find('li span').text(rownum);
	                $(area).append(newRow);
	            }
            	if(rownum >= 50){
            		 $(element).hide();
            	}
        }

        /*
        * recheck .error tag exist
        */
        function test(element) {
            if($('div.error').length == 0){
                $(go_next).removeClass("disabled");
            }
        }

        /*
         * confirm input content use regexp
         */
        $(document).on('blur','input[name^="syouhin_code"]',function(){
            $(this).parent().find(".error").remove();
            var txt = $(this).val();
            var partner = $(this).closest('ul').find('input[name^="qty"]');
            if (txt == '' && partner.val()){
                $(this).after('<div class="error"><span style="color:red;">請至少輸入四個字元</span></div>');
                $(go_next).addClass("disabled");
                return;
            }else if(txt == ''){
                $(this).closest('ul.item').find("li div.error").remove();
                $(go_next).removeClass("disabled");
                return;
            }

            var txtlength = txt.length;
            //var regExp = /^[\d|a-zA-Z|/-]+$/;
            var regExp = /^[\d|.|A-Z|/-]+$/;
            if (txtlength < 4){
                $(this).after('<div class="error"><span style="color:red;">請至少輸入四個字元</span></div>');
                $(go_next).addClass("disabled");
                return;
            }else if (!regExp.test(txt) || txt.indexOf("/") > -1){
                $(this).after('<div class="error"><span style="color:red;">請輸入正確的英數大寫半形或「-」符號</span></div>');
                $(go_next).addClass("disabled");
            }else if(!partner.val()){
                partner.after('<div class="error"><span style="color:red;">請輸入正確數字</span></div>');
                $(go_next).addClass("disabled");
            }
            var same_txt_test = false;
            $('input[name="syouhin_code[]"]').not(this).each(function(){
                if($(this).val() === txt){
                    same_txt_test = true;
                    return false;
                }
            });
            if(same_txt_test){
                $(this).after('<div class="error"><span style="color:red;">已輸入相同料號</span></div>');
                $(go_next).addClass("disabled");
            }

            if($('div.error').length == 0){
                $(go_next).removeClass("disabled");
            }
        });

        $(document).on('blur','input[name^="qty"]',function(){
            $(this).parent().find(".error").remove();
            var txt = $(this).val();
            var partner = $(this).closest('ul').find('input[name^="syouhin_code"]');
            if (txt == ''){
                partner.trigger('blur');
//                partner.closest('ul').find('.error').remove();
                return;
            }
            var regExp = /^[\d]+$/;
            if (!regExp.test(txt)){
                $(this).after('<div class="error"><span style="color:red;">請輸入正確數字</span></div>');
                $(go_next).addClass("disabled");
            }else if(txt <= 0){
                $(this).after('<div class="error"><span style="color:red;">查詢數量需大於0</span></div>');
                $(go_next).addClass("disabled");
            }else if(txt > 99){
                $(this).after('<div class="error"><span style="color:red;">查詢數量需小於99</span></div>');
                $(go_next).addClass("disabled");
            }else if(!partner.val()){
                partner.after('<div class="error"><span style="color:red;">請至少輸入四個字元</span></div>');
                $(go_next).addClass("disabled");
            }

            if($('div.error').length == 0){
                $(go_next).removeClass("disabled");
            }
        });

        /*
         * go to next page action
         */
        $('.go-next').click(function(){
            if(!$(select_manufacturer_code).val()){
                swal(
                        '錯誤!',
                        '您尚未選擇廠牌',
                        'error'
                );
                slipTo(select_manufacturer);
                return false;
            }

            var confirm_table = '';
            var url_code = $(select_manufacturer_code).val();
            var compares = $.parseJSON($(name_compares).val());
            var name = compares[url_code];

            $('#genuineparts-confirm-table').html('');
            $('#genuineparts-table ul.item').each(function(ul_key, ul){
                var tds = '';
                var tds_object = {};
                $(ul).find('input').each(function(input_key, input){
                    if(input_key == 0 && $(input).val()){
                        tds_object[input_key] = '【' + name + '】' + $(input).val() ;
                    }else{
                        tds_object[input_key] = $(input).val() ;
                    }
                });
                if(tds_object[0] && tds_object[1]){
                    tds += '<li class="col-md-5 col-sm-5 col-xs-5"><h3>' + tds_object[0] + '</h3></li>';
                    tds += '<li class="col-md-2 col-sm-2 col-xs-2"><h3>' + tds_object[1] + '</h3></li>';
                    tds += '<li class="col-md-5 col-sm-5 col-xs-5"><h3>' + tds_object[2] + '</h3></li>';
                }

                if(tds.length > 0){
                    confirm_table += '<ul class="table-main-item">' + tds + '</ul>';
                }
            });
            $('#genuineparts-confirm-table').append(confirm_table);
            goNextPage();
        });

        /*
         * first times setting
         */
        if($(target_manufacturer).length > 0 && $(target_sent_code).length > 0){
            code = $(target_sent_code).val();
            id = $(target_manufacturer).val();
            $(".tap-box-" + code).addClass('active');
            $(".ct-tap-country#" + code + "").addClass('active-ct-tap-country');
            $(".ct-tap-country a[name='" + id + "']").closest('li').addClass('active');
            $(".ct-tap-country a[name='" + id + "']").click();
        }else{
            $(tab_class + ":first-child").addClass('active');
            $(".ct-tap-country:first-child").addClass('active-ct-tap-country');
        }
    </script>

    <script>
        $(document).ready(function(){
            $('.go-next').show();
        }); 

    	$(".genuineparts-menu-icon, .genuineparts-overlay, .genuineparts-close_btn").on('click', function(){
			if($(".genuineparts-menu_items").css("display") != "block"){
				$("html").css("overflow","hidden");
				$("html").css("position","relative");
				$("#w_spng_sp").css("position","relative");
				var winHeight = $(window).height() + 20;
				$(".genuineparts-menu_items").height(winHeight).scrollTop(0);
				$(".genuineparts-menu_items .genuineparts-inner").css("min-height",winHeight);
				$(".genuineparts-menu_items").removeClass("hide").addClass("show");
				$(".genuineparts-overlay,  .genuineparts-close_btn").show();
			}else{		
				$(".genuineparts-overlay, .genuineparts-close_btn").hide();
				$(".genuineparts-menu_items dl dd ul").hide();
				$(".genuineparts-menu_items").removeClass("show").addClass("hide");
				setTimeout(function(){$(".genuineparts-menu_items").removeClass("hide");},500);
				$(".genuineparts-menu_items a").removeClass("open");
				$("#w_spng_sp").css("position","absolute");
				$("body, html").css("overflow","");
				$("html").css("position","");
			}
		});
    </script>
@stop