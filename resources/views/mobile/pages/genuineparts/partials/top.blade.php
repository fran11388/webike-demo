<div class="banner-block">
	<a class="banner-advertisement" href="{{route('genuineparts-step')}}"><img src="{!! assetRemote('image/oempart/banner02.jpg') !!}" alt="banner"></a>
    @if(activeValidate('2019-03-29','2019-05-03') and (Route::currentRouteName() === "genuineparts") and $bar_obvious)
        <div class="btn-gap-top"> 
            <img src="{!! assetRemote('image/benefit/big-promotion/2019/newyear/1901_genuine_370.jpg') !!}" alt="banner">
        </div>
    @endif
    <div class="btn-gap-top btn-gap-bottom">
        <iframe width="100%" height="300" src="https://www.youtube.com/embed/2IXSPsEESzI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="goldenweek-position">
        @include('response.pages.product-detail.partials.goldenweek-tag')
    </div>
	<div>
		<span>【Webike正廠零件查詢購買系統】購買進口，國產原廠機車零件不求人，24小時線上免費報價，簡單又快速!</span>
	</div>
</div>
@if($current_customer and in_array($current_customer->role_id,[Everglory\Constants\CustomerRole::WHOLESALE,Everglory\Constants\CustomerRole::STAFF]))
    <div class=" link_ui box-page manual">
        <a href="{{route('genuineparts-manual')}}">經銷商零件手冊閱覽</a>
    </div>
@endif
<div class="link_ui">
    <a class="link_ui_a toggle clearfix">正廠零件功能與說明</a>
    <div class="no-rcj-a" >
        <ul class="box-page-group">
            <li class="box-page">
                <a href="{{route('customer-history-genuineparts')}}"><span>正廠零件報價履歷</span></a>
            </li>
            <li class="box-page">
                <a href="{{route('genuineparts-step')}}"><span>正廠零件系統使用說明</span></a>
            </li>
            <li class="box-page">
                <a href="{{route('customer-service-proposal-create','service') . '?category_id=83' }}" target="_blank"><span>正廠零件系統提問</span></a>
            </li>
            <li class="box-page">
                <a href="{{route('genuineparts-trans')}}"><span>中英日摩托車專有名詞對照表</span></a>
            </li>
        </ul>
    </div>
</div>
<div class="link_ui">
	<a class="link_ui_a toggle">注意事項</a>
	<div class="no-rcj-a box-text box-page-group">
		1.本系統查詢報價完全免費，使用前請先"<a href="{{route('login')}}">登入會員</a>"；若您還沒加入會員，請點選"<a href="{{route('customer-account-create')}}">快速註冊</a>"。<br>
		2.此系統僅限定使用「正廠零件料號」進行查詢，零件料號須由會員自行透過原廠零件手冊進行確認。<br>
		3.若有技術性問題或訂購安裝服務，請洽全台各地「<a href="{{route('dealer')}}" target="_blank">Webike經銷商</a>」。<br>
		4.其他關於系統相關問題，請至 <a href="{{route('customer-service-proposal-create','service')}}" target="_blank">網站操作及商品諮詢</a> 提問。<br>
	</div>	
</div>

@include('response.pages.genuineparts.partials.genuineparts-relieved-shopping-img')