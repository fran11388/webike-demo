@extends('mobile.layouts.mobile-amp')
@section('head')
    <link rel="canonical" href="{{URL::route('customer-rule', 'pointinfo')}}">
    <script async custom-element="amp-geo" src="https://cdn.ampproject.org/v0/amp-geo-0.1.js"></script>      
    <script async custom-element="amp-user-notification" src="https://cdn.ampproject.org/v0/amp-user-notification-0.1.js"></script>
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
          "@type" : "WebSite",
          "mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
          "name" : "「Webike-摩托百貨」",
          "headline": "現金點數說明",
          "datePublished": "2018-06-14T11:30:41Z",
            "dateModified": "2018-06-15T14:30:41Z",
            "description": "Webike會員專屬現金集點活動，一點可扣抵一元，集越多省越多",
        "author": {
          "@type": "Person",
          "name": "Jordan M Adler"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Google",
          "logo": {
            "@type": "ImageObject",
            "url": "http://cdn.ampproject.org/logo.jpg",
            "width": 600,
            "height": 60
          }
        },
        "image": {
          "@type": "ImageObject",
          "url": "http://cdn.ampproject.org/leader.jpg",
          "height": 2000,
          "width": 800
        }
      }
    </script>
@stop
@section('style')
@stop
@section('middle')
	<div class="box-container-intruction">
        <div class="title-main-box">
            <h1 class="common">現金點數說明</h1>
        </div>
        <div class="container-intruction-detail">
            <ul class="ul-membership">
                <li>
                	<div class="title text-center">
                        <amp-img src="https://img.webike.tw/assets/images/service/Snap1.png" alt="零件手冊「Webike-摩托百貨」" title="零件手冊「Webike-摩托百貨」" width="258" height="80" layout="responsive"></amp-img>
                    </div>
	            </li>
	        </ul>
	        <ul class="ul-membership">
                <li>
                	<div class="title text-center">
                        <h3 class="common">「Webike台灣」點數積點使用規則</h3>
                    </div>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">一.點數取得方式：</h3>
                    </div>
                    <span>
                    	1.購物回饋：凡「Webike-摩托百貨」正式會員（必須完成完整註冊），在「Webike-摩托百貨」購物網站內購買商品，可以獲得回饋點數，此點數可以累積。<br>
						2.撰寫商品評論：凡「Webike-摩托百貨」正式會員（必須完成完整註冊），撰寫商品評論，投稿之評論獲得採用我們會發送點數獎勵，一般商品評論20點（無照片），優秀商品評論（有附照片）50點，此點數可以累積，特定主題評論撰寫時也會有加倍獎勵。<br>
						3.參與活動：凡「Webike-摩托百貨」正式會員（必須完成完整註冊），參與網站活動（猜謎、抽獎…）將有機會獲得點數，點數以活動公告為準，此點數也可以累積。<br>
						4.會員電子報：每周寄送的會員電子報，周周開信拿點數的活動，每次開信點擊「現金點數領取」，即可獲得開信點數10點。
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">二.點數折抵金額：</h3>
                    </div>
                    <span>
                    	每1點可在「Webike-摩托百貨」購物網站內折抵購物金額新台幣1元。
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">三.點數比例：</h3>
                    </div>
                    <span>
                    	1.點數回饋比例：凡是在「Webike-摩托百貨」購物網站內購物結帳無論是否使用折價券、點數進行折抵，以最後結帳總金額作為計算標準，比例為每次消費結帳總金額1%。<br>
						（範例：結帳總金額1000元可以獲得10點）<br>
						2.點數加倍活動：「Webike-摩托百貨」不定期舉辦點數加倍活動，只要在活動期間消費即可享有點數加倍回饋（2%~8%），實際加倍比例以活動公告為準。
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">四.點數發放時間與查詢方式：</h3>
                    </div>
                    <span>
                    	1.購物回饋：每次消費所得到的回饋點數，將於商品配送完成後7個工作天，匯入會員點數帳戶，同時會寄發email通知會員。<br>
						2.撰寫評論：一般為投稿後7個工作天，凡經採用我們會配發點數作為獎勵，同時會寄發email通知會員。<br>
						3.參與活動：管理員會將點數配發至得獎會員帳戶，發放時間以活動公告時間為準，同時會寄發email通知會員。
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">五.點數查詢方式：</h3>
                    </div>
                    <span>
                    	請您進入【會員中心】->【點數獲得及使用履歷】，會員可逐筆查詢點數獲得及使用的狀態。
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">六.點數使用上限：</h3>
                    </div>
                    <span>
                    	每次消費可使用的點數上限：購物商品總金額70%，若輸入超過使用上限，系統會自動計算，直接套用點數的使用上限。<br>
						（範例：結帳總金額$2000元，若輸入使用點數2000點，系統會自動計算使用點數扣抵為1400點）<br>
                    </span>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">七.點數使用期限：</h3>
                    </div>
                    <span>
                    	每一筆獲得的點數都有使用期限，不論是經由購物或是參與活動所得到點數，若該筆點數一年內沒有動用，系統將自動歸零該筆點數，若其他筆點數時間未到期則可以繼續使用，請會員把握使用期限多加利用。<br>
						點數使用的部分，我們系統將會採取先進先出的方式使用。<br>
						（範例：A會員擁有兩筆點數，2013/11/1想使用點數，但是第1筆點數已經到期，所以該帳戶只有150點可以使用）<br>
						（範例：A會員擁有兩筆點數，2013/11/1想使用點數，但是第1筆點數已經到期，所以該帳戶只有150點可以使用）
						第1筆200點 使用期限2013/10/30<br>
						第2筆150點 使用期限2013/11/31
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">八:商品退貨：</h3>
                    </div>
                    <span>
                    	會員訂購商品後如有退貨的情形，該筆訂單所得到的累積點數將會被追回，如果該筆訂單有使用點數進行折抵時，點數將在辦理退貨完成後退回會員點數帳戶。
                    </span><br>
                </li>
            </ul>
            <ul class="ul-membership">
                <li>
                	<div class="title">
                        <h3 class="common">九.注意事項：</h3>
                    </div>
                    <span>
                    	1.累積點數僅限於「Webike-摩托百貨」購物網站內使用，無法與其他網路、購物賣場流通。<br>
						2.每位會員所累積點數無法轉讓給其他會員使用。<br>
						3.無法使用現金購買點數，點數也無法兌換成現金。<br>
						4.會員如果違反會員條約或是自行退出會員，帳號刪除後累積點數也一併歸零，會員視同放棄此權益，不得要求點數轉讓他人或是折換現金。<br>
						5.若會員有利用不當方式獲取點數（利用系統漏洞洗點數）的情形，本公司有權利沒收不當取得之點數，並依據會員條約取消該會員資格。
                    </span><br>
                </li>
            </ul>
		</div>
	</div>
    <div>
        <amp-geo layout=nodisplay>
          <script type="application/json">
            {
              "ISOCountryGroups": {
              "nafta": [ "ca", "mx", "us" ],
              "waldo": [ "unknown" ],
              "anz": [ "au", "nz" ]
              }
            }
          </script>
        </amp-geo>
        <amp-user-notification
            layout=nodisplay
            id="amp-user-notification1"
            data-show-if-geo="nafta, anz"
            data-dismiss-href="https://example.com/api/echo/post">
            This notice is only shown in Canada, Mexico and USA.
            <a class="btn" on="tap:amp-user-notification1.dismiss">I accept</a>
        </amp-user-notification>
    </div>
@stop
