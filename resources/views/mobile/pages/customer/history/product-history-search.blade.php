    <div class="product-history-search">
        <div class="product-history-search-t">
            <a href="javascript:void(0)"><i class="fas fa-times"></i></a>
            <p>篩選</p>
        </div>
        <div class="product-history-search-c">

            <div class="search-bar">
                <input type="search" placeholder="搜索" class="wm-search">
                <button class="wm-botton-search"></button>
            </div>

            <select name="" id="" class="wm-select-down">
                <option class="soflow-hidden" disabled="" selected="" hidden="" value="">品牌</option>
                {{-- 以下為購買過商品的品牌才顯示 --}}
                <option value="Honda">Honda</option>
                <option value="Yamasaki">Yamasaki</option>
                <option value="Suzuki">Suzuki</option>
                <option value="Kawasaki">Kawasaki</option>
            </select>

            <select name="" id="" class="wm-select-down">
                <option class="soflow-hidden" disabled="" selected="" hidden="" value="">商品分類</option>
                <option value="改裝零件">改裝零件</option>
                <option value="騎士用品">騎士用品</option>
                <option value="保養耗材">保養耗材</option>
                <option value="機車工具">機車工具</option>
                <option value="正廠零件">正廠零件</option>
            </select>


        </div>
    </div>