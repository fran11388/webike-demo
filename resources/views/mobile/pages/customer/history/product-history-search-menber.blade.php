<div class="product-history-search-number-background">
    <div class="product-history-search-number">
        <div class="product-history-search-number-title">
            <a href="javascript:void(0)"><i class="fas fa-times"></i></a>
            <span>數量</span>
            <button class="w-botton">查詢</button>
        </div>
        <div class="product-history-search-number-content">
            <div class="select-type" hidden></div>
            <div class="number-check">
                <input type="number" value="1" min="0" id="amount" placeholder="1" class="productNumber" onkeydown="this.value=this.value.substr(0,3)" >
                <button class="w-botton-b plus">+</button>
                <button class="w-botton-b reduce">-</button>
            </div>
            <div class="genuine-section">
                <div style="border-top: 1px #ccc solid;padding: 4px 8px;">正廠零件備註：</div>
                <input name="note" placeholder="備註內容" value="" style="width: 100%;padding: 8px;border: 0;">
            </div>
        </div>
    </div>
</div>