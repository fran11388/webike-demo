@extends('mobile.layouts.mobile')
@section('style')
    {{--     <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/customer.css') !!}">
        <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/history.css') !!}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/product-history.css') }}">


@section('middle')
    <?php
    $role_id = Auth::user()->role->id;
    $price_string = '';
    if (in_array($role_id, [\Everglory\Constants\CustomerRole::GENERAL])) $price_string = '會員價';
    if (in_array($role_id, [\Everglory\Constants\CustomerRole::WHOLESALE, \Everglory\Constants\CustomerRole::STAFF])) $price_string = '經銷價';
    ?>

    <form method="get" action="{{route('customer-history-purchased')}}">
        <div class="product-history-title">
            <h2>購入商品履歷</h2>
        </div>

        <div class="product-history-search">
            <div class="product-history-search-t">
                <a href="javascript:void(0)"><i class="fas fa-times"></i></a>
                <p>篩選</p>
            </div>
            <div class="product-history-search-c">

                <div class="search-bar">
                    <input type="search" placeholder="搜索" class="wm-search" name="keyword" value="{{request()->get('keyword')}}">
                    <button type="submit" class="wm-botton-search"></button>
                </div>

                <select name="m_id" id="" class="wm-select-down">
                    <option class="soflow-hidden" disabled="" selected="" hidden="" value="">品牌</option>
                    {{-- 以下為購買過商品的品牌才顯示 --}}
                    @foreach($manufacturers as $manufacturer)
                        <option value="{{$manufacturer->id}}"
                                @php
                                    $m_id = request()->get('m_id');
                                @endphp
                                @if($manufacturer->id==$m_id)
                                selected
                                @endif
                        >{{$manufacturer->name}}</option>
                    @endforeach
                </select>

                <select name="c_identifier" id="" class="wm-select-down">
                    <option class="soflow-hidden" disabled="" selected="" hidden="" value="">商品分類</option>
                    @foreach($first_layer_categories as $url_rewrite=>$name)
                        <option value="{{$url_rewrite}}"
                                @php
                                    $c_identifier=request()->get('c_identifier');
                                @endphp
                                @if($url_rewrite == $c_identifier)
                                selected
                                @endif
                        >{{$name}}</option>
                    @endforeach
                </select>


            </div>
        </div>


        <div class="product-history-bar">
            <div class="product-history-status">
                {{--<p>關鍵字：<span>{{request()->get('keyword')}}</span></p>--}}
                <p>共:<span>{{$collection->count()}}</span>項商品</p>
                <div class="fixclear"></div>

                <ul>
                    {{--品牌BOX--}}
                    @foreach($manufacturers as $manufacturer)
                        @php
                            $m_id = request()->get('m_id');
                        @endphp
                        @if($manufacturer->id==$m_id)
                            <li>
                                <div class="product-history-tag">
                                    <span>品牌</span>
                                    <a href="{{route('customer-history-purchased',request()->except('m_id'))}}"><i
                                                class="fas fa-times"></i></a>
                                </div>
                                <p class="dotted-text1">{{$manufacturer->name}}</p>
                            </li>
                        @endif
                    @endforeach

                    {{--分類BOX--}}
                    @foreach($first_layer_categories as $url_rewrite=>$name)
                        @php
                            $c_identifier=request()->get('c_identifier');
                        @endphp
                        @if($url_rewrite == $c_identifier)
                            <li>
                                <div class="product-history-tag">
                                    <span>分類</span>
                                    <a href="{{route('customer-history-purchased',request()->except('c_identifier'))}}"><i
                                                class="fas fa-times"></i></a>
                                </div>
                                <p class="dotted-text1">{{$name}}</p>
                            </li>
                        @endif
                    @endforeach

                    {{--關鍵字BOX--}}
                    @if(request()->get('keyword')!='')
                        <li>
                            <div class="product-history-tag">
                                <span>關鍵字</span>
                                <a href="{{route('customer-history-purchased',request()->except('keyword'))}}"><i
                                            class="fas fa-times"></i></a>
                            </div>
                            <p class="dotted-text1">{{request()->get('keyword')}}</p>
                        </li>
                    @endif

                </ul>
                <div class="fixclear"></div>
            </div>

            <div class="product-history-select">

                <select name="sort" id="" class="wm-sort-down" onchange="submit()">
                    <option class="soflow-hidden" disabled="" selected="" hidden="" value="">商品排序</option>
                    @foreach($sortingWays as $key=>$value)
                        <option value="{{$key}}"
                                @php
                                    $sort = request()->get('sort');
                                @endphp
                                @if($key==$sort)
                                selected
                                @endif
                        >{{$value}}</option>
                    @endforeach
                </select>

                <button type="button" class="wm-select-button">
                    篩選條件
                </button>

                {{-- <div class="fixclear"></div> --}}
            </div>
        </div>
    </form>

    <div class="product-history-content">
        <ul class="product-history-list">
            @foreach($collection as $item)
                @foreach($sku_grouped_order_items->get($item->sku) as $order_item)
                    <li class="product-history-list-project">
                        <ul class="product-history-list-project-t">
                            <li>
                                <span>{{$order_item->order->created_at->toDateString()}}</span>
                                <span>NT$ {{number_format($order_item->price)}}</span>
                            </li>
                            <li>
                                <p>{{$order_item->details->where('attribute','product_name')->first()->value}}</p>
                                <p>{{$order_item->details->where('attribute','manufacturer_name')->first()->value}}</p>
                            </li>
                        </ul>
                        <ul class="product-history-list-project-c">
                            <li>
                                <div class="project-img">
                                    <a href="{{ route('product-detail',$order_item->sku) }}">
                                        <img src="{{ $order_item->productAccessor ? $order_item->productAccessor->getImage() : NO_IMAGE }}"
                                             alt="">
                                    </a>
                                </div>

                                <div class="project-data">
                                    <p class="dotted-text1">商品編號：<span>{{$order_item->model_number}}</span></p>
                                    <p class="dotted-text1">目前{{$price_string}}：<span>
                                            @if($order_item->productAccessor)
                                                @if(in_array($order_item->product_type,[\Everglory\Constants\ProductType::GENUINEPARTS,\Everglory\Constants\ProductType::UNREGISTERED,\Everglory\Constants\ProductType::GROUPBUY]))
                                                    NT$ --
                                                @else
                                                    NT$ {{number_format($order_item->productAccessor->getFinalPrice($order_item->customer))}}
                                                @endif

                                            @else
                                                商品已下架
                                            @endif
                                        </span></p>

                                    <div class="project-bt">
                                        <i class="w-botton-b stock_tag_{{$order_item->sku}}" id=""
                                           style="display: none">有庫存</i>
                                        <a href="{{route('review-create',$order_item->sku)}}" target="_blank"
                                           class="w-botton-y">撰寫評論</a>
                                        <span>評論獲得點數回饋</span>
                                    </div>
                                </div>

                                <div class="fixclear"></div>

                            </li>

                            <li>
                                <div class="project-data-l">
                                    <p class="dotted-text1">訂單日期：<span>{{$order_item->order->created_at->toDateString()}}</span></p>
                                    <p class="dotted-text1">單價：<span>NT$ {{number_format($order_item->price)}} </span></p>
                                    <p class="dotted-text1">
                                        訂單編號：<a href="{{route('customer-history-order-detail',$order_item->order->increment_id)}}"><span>{{$order_item->order->increment_id}}</span></a>
                                    </p>
                                    <p class="dotted-text1">購買數量：<span>{{$order_item->quantity}}</span></p>
                                </div>
                                <div class="project-data-r">
                                    <p>購買次數：<span>{{$item->buy_times}}</span></p>
                                    <p>購買總數：<span>{{$item->buy_count}}</span></p>
                                    <p>購買頻率：<span>
                                            @if($item->buy_fq==0)
                                                --
                                            @else
                                                {{number_format($item->buy_fq)}}
                                            @endif
                                            天</span></p>
                                    {{--<p>上次購買日：<span>2018/12/30</span></p>--}}
                                </div>
                                <div class="fixclear"></div>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="fas fa-pen dotted-text1" data-item-id="{{$order_item->id}}"></i>
                                </a>
                                @if($order_item->product_type == 0 and $order_item->productAccessor)
                                    <button class="w-botton-r execute-btn"
                                            data-product_type="{{$order_item->product_type}}"
                                            data-sku="{{$order_item->sku}}">加入購物車
                                    </button>
                                @elseif($order_item->product_type == 1 and isset($manufacture_suppliers[$order_item->manufacturer_id]))
                                    <button class="w-botton-b execute-btn"
                                            data-product_type="{{$order_item->product_type}}"
                                            data-model_number="{{$order_item->model_number}}"
                                            data-manufacturer_name="{{$order_item->manufacturer->name}}"
                                            data-supplier_id="{{$manufacture_suppliers[$order_item->manufacturer_id]}}"
                                            data-note="{{$order_item->note}}">一鍵查詢
                                    </button>
                                @endif
                                <div class="fixclear"></div>
                            </li>
                        </ul>
                    </li>
                @endforeach
            @endforeach


        </ul>
    </div>

    
    @include ( 'mobile.pages.customer.history.product-history-number' )
    @include ( 'mobile.pages.customer.history.product-history-remarks' )

    {{-- @include ( 'mobile.pages.customer.history.product-history-search-menber' ) --}}
    <div class="text-center">
        {!! $collection->appends(request()->except('page'))->links() !!}
    </div>

@stop

<script>
    jQuery(document).ready(function ($) {

        //畫面動作

        //側邊搜索攔
        $('.wm-select-button').click(function (event) {
            event.stopPropagation();
            $(".product-history-search").addClass('open');
        });
        $(function () {
            var h = $(window).height();
            $(".product-history-search").css("height", h);
        });
        $('.product-history-search-t a').click(function (event) {
            event.stopPropagation();
            $(".product-history-search").removeClass('open');
        });
        //商品內容展開
        $('.product-history-list-project-t').click(function (event) {
            event.stopPropagation();
            $(this).toggleClass('open');
            $(this).parent().find(".product-history-list-project-c").toggleClass('open2');
            $(this).parent().find('.product-history-list-project-c').stop().slideToggle(250);
        });
        //搜索顯示方塊關閉
        $('.product-history-tag a').click(function (event) {
            event.stopPropagation();
            $(this).parent().parent().addClass('close')
        });
        //加入購物車視窗 關閉
        $(".product-history-number-title i").click(function (event) {
            event.stopPropagation();
            $(".product-history-number-background").toggle();
        });


        //正廠零件查詢
        $(".product-history-search-number-title i").click(function (event) {
            event.stopPropagation();
            $(".product-history-search-background").toggle();
        });

        $(".add-search-bt").click(function (event) {
            event.stopPropagation();
            $(".product-history-search-number-background").toggle();
        });

        // $(function(){
        //   // var w = $('.project-img').width();
        //   $(".project-img").css("height", $('.project-img').width());
        // });


        //打開加入購物車視窗 並加入需選擇枝
        $(".execute-btn").click(function (event) {
            var windowElement = $('.product-history-number');
            var product_type = $(this).attr('data-product_type');
            if (product_type == 0) {
                $(windowElement).find('input').val('1');
                $(windowElement).find('.genuine-section').hide();
                $(windowElement).find('input[name="sku"]').val($(this).attr('data-sku'));
                $(windowElement).find('input[name="product_type"]').val(product_type);

                //選擇枝1.拿到原始商品sku
                sku_original = $(this).attr('data-sku');

                $(".select-type").removeAttr("hidden");

                var _token = $('meta[name="csrf-token"]').attr("content");
                $.ajax({
                    url: '{{route('api-product-options')}}',
                    method: 'GET',
                    data: {
                        sku: sku_original,
                        _token: _token
                    },
                    success: function (result) {
                        $("#execute").attr('disabled', false);
                        var selectType = $('.product-history-number .select-type'); 
                        var selectAdd = "";

                        for(i=0 ; i<result.length ; i++){
                            // console.log(result[i].options[i].name);
                            // console.log(result[i].options.length);
                            // console.log(Object.keys(result[i].options).length)
                            var optionsAdd = "",
                                gc_str = "",
                                mi_str = "";

                            for(j=0 ; j < Object.keys(result[i].options).length ; j++){

                                var str2 = '<option value="selected" mi_str="'+ result[i].options[j].manufacturer_id +'"' + 'gc_str="'+ result[i].options[j].group_code + '">' + result[i].options[j].name + '</option>';

                                optionsAdd = optionsAdd + str2;
                            }

                            // console.log(optionsAdd);
                            var str = '<select class="w-select-down"><option value="original" disabled selected hidden>' + result[i].name + '</option>' + optionsAdd + '</select>'

                            selectAdd = selectAdd + str;
                        }

                        selectType.html(selectAdd);
                    },
                    error: function () {
                    }
                });

            } else if (product_type == 1) {
                $(windowElement).find('input').val('1');
                $(windowElement).find('.genuine-section').show();
                $(windowElement).find('input[name="model_number"]').val($(this).attr('data-model_number'));
                $(windowElement).find('input[name="product_type"]').val(product_type);
                $(windowElement).find('input[name="note"]').val($(this).attr('data-note'));
                $(windowElement).find('input[name="manufacturer"]').val($(this).attr('data-manufacturer_name'));
                if ($(this).attr('data-supplier_id') == 1) {
                    $(windowElement).find('input[name="divide"]').val("{{\Everglory\Constants\CountryGroup::IMPORT['url_code']}}");
                } else {
                    $(windowElement).find('input[name="divide"]').val("{{\Everglory\Constants\CountryGroup::DOMESTIC['url_code']}}");
                }

                //正廠零件沒有選擇枝
                $(".select-type").attr("hidden",true);
            }
            event.stopPropagation();//停止事件冒泡
            $(".product-history-number-background").toggle();


        });

        //避免重複點擊加入購物車
        function unlock() {
            $('#execute').removeClass('disabled').removeAttr('disabled', 'disabled');
        }

        var sku_original = "";

        //加入購物車
        $('#execute').click(function () {
            $(this).addClass('disabled').attr('disabled', 'disabled');
            var windowElement = $(this).closest('.product-history-number');
            var product_type = $(windowElement).find('input[name="product_type"]').val();
            if (product_type == 0) {
                var qty = $(windowElement).find('#amount').val();
                var sku = $(windowElement).find('input[name="sku"]').val();
                var select_judje = $(".select-type select").find("option:selected");
                var select_judje_key = true;

                // 判斷選擇枝 有沒有被選過
                select_judje.each(function(){
                    
                    if($(this).val() == "original"){
                        $("#execute").attr('disabled', false);
                        select_judje_key = false;

                        alert("您還有規格沒選取");
                        return false;
                    }
                    
                })

                if(select_judje_key == true){

                    var option_name = "";
                    var option_name_first = $(".select-type select").find("option:selected").html();
                    var option_mi = $(".select-type select").find("option:selected").attr("mi_str");
                    var option_gc = $(".select-type select").find("option:selected").attr("gc_str");

                    if($(".select-type select").find("option:selected").size() > 1){
                        $(".select-type select").find("option:selected").eq(1).each(function(){
                            option_name = option_name_first + "|" + $(this).html();
                            // console.log($(this).html())
                        })
                    }else{
                        option_name = option_name_first;
                        // console.log(option_name_first);
                    }
                    $.ajax({
                            url: '{{route('product-detail-info')}}',
                            data:{
                                sku: sku_original,
                                na: option_name,
                                gc: option_gc,
                                mi: option_mi,
                            },
                            method: 'GET',
                            success: function (result) {
                                // console.log(result.sku);
                                // return result.sku;

                                addCart(result.sku, qty);
                            },
                            error: function () {
                            }
                        });
                }


            } else if (product_type == 1) {
                var manufacturer = $(windowElement).find('input[name="manufacturer"]').val();
                var divide = $(windowElement).find('input[name="divide"]').val();
                var model_numbers = [];
                var qty = [];
                var note = [];
                model_numbers[0] = $(windowElement).find('input[name="model_number"]').val();
                note[0] = $(windowElement).find('input[name="note"]').val();
                qty[0] = $(windowElement).find('#amount').val();
                doEstimate(model_numbers, qty, note, manufacturer, divide);
            } else {
                unlock();
                swal('error!', 'selected action not exist.', 'error');
            }

        });

        function addCart(sku, qty) {
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('cart-update')}}',
                data: {
                    sku: sku,
                    qty: qty,
                    _token: _token
                },
                success: function (result) {
                    unlock();
                    $(".product-history-number-background").toggle();
                    swal('已加入購物車。', '成功', 'success')
                },
                error: function () {
                    unlock();
                }
            });
        }



        //填寫備註
        var AllI = $(".product-history-list-project-c li:last-of-type a i");
        var totalID1 = $(".product-history-list-project-c li:last-of-type a i").eq(0).attr('data-item-id');
        var totalID = totalID1;

        for( i=1 ; i<AllI.length ; i++ ){

            var ID = AllI.eq(i).attr('data-item-id');
            totalID =  totalID + "," + ID;
        }


        //1.畫面進入時 載入所有備註並顯示

        var _token = $('meta[name="csrf-token"]').attr("content");

        $.ajax({
            url: '{{route('rest-order-items')}}',
            method: 'GET',
            data: {
                ids: totalID,
                _token: _token
            },
            success: function (result) {

                for( i=0 ; i<AllI.length ; i++ ){
                    var AllI_ID = AllI.eq(i).attr('data-item-id');

                        if(result[AllI_ID].remark == null || result[AllI_ID].remark == ""){
                            
                        }else{
                            AllI.eq(i).html(result[AllI_ID].remark );
                            AllI.eq(i).removeClass('fa-pen');
                        }
                    }


                },
            error: function () {
                }
        });


        //2.點擊備註按鈕 

        AllI.click(function(event){
            
            var id = $(this).attr('data-item-id');
            var remark_Window_view = $(".product-history-remarks").find('textarea');

            //挑出視窗動作
             event.stopPropagation();
            $(".product-history-remarks-background").toggle();

            //標記共同視窗 暫存屬性
            $(".product-history-remarks").attr("variableId",id);

            //共同視窗 寫入內容
            
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('rest-order-items')}}',
                method: 'GET',
                data: {
                    ids: id,
                    _token: _token
                },
                success: function (result) {
                    remark_Window_view.val(result[id].remark);
                },
                error: function () {
                }
            });
        });


        //儲存 備註內容並更新畫面顯示

        $('.product-history-remarks-title button').click(function (event) {

            var id2 = $(this).parent().parent().attr("variableId");
            var text = $(this).parent().parent().find('textarea').val();

            var remark_view = $(".product-history-list-project-c li:last-of-type a i[data-item-id = '" + id2 + "']");

            //視窗內有無內容
            if(text != "" && removeAllSpace(text) != ""){

                //存入資料庫
                putOrderItems(id2,text);

                //關閉視窗
                $(".product-history-remarks-background").hide();
                remark_view.removeClass('fa-pen');

                remark_view.html(text);

                
            }else if(text=="" || removeAllSpace(text) == ""){

                //存入資料庫
                putOrderItems(id2,"");

                console.log(3)

                $(".product-history-remarks-background").hide();
                remark_view.addClass('fa-pen');

                remark_view.html(text);
            }
        });

        $(".product-history-remarks-title .fa-times").click(function(event) {
            $(".product-history-remarks-background").hide();
        });
        


        // 去除所有空格
        function removeAllSpace(str) {
            return str.replace(/\s+/g, "");
        }

        //  putOrderItems(172918,'備註內容...')
        function putOrderItems(order_item_id, remark) {
            var _token = $('meta[name="csrf-token"]').attr("content");
            $.ajax({
                url: '{{route('rest-order-items')}}',
                method: 'PUT',
                data: {
                    id: order_item_id,
                    remark: remark,
                    _token: _token
                },
                success: function (result) {
                    // console.log(result)
                },
                error: function () {

                }
            });
        }

        function doEstimate(model_numbers, quantity, note, manufacturer, divide) {
            if (model_numbers.length > 0 && quantity.length > 0) {
                swal({
                    title: '您確定要送出嗎?',
                    text: "點選「確認送出」後將送出查詢。",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '送出查詢',
                    cancelButtonText: '取消',
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#777',
                }).then(function () {
                    var step = false;
                    $.ajax({
                        url: "{!! route('realTime-genuineparts-estimate') !!}",
                        data: {
                            _token: $('meta[name=csrf-token]').prop('content'),
                            api_usage_name: manufacturer,
                            step: step,
                            divide: divide,
                            syouhin_code: model_numbers,
                            qty: quantity,
                            note: note
                        },
                        type: 'POST',
                        dataType: 'json',
                        timeout: 200000,
                        success: function (result) {
                            unlock();
                            if (result.success) {
                                link = "{{ \URL::route('genuineparts-estimate-redirect-estimate-done') }}" + "?code=" + result.code;
                                $('.realtime-genuineparts').attr('href', link).show(1000);
                                $('.realtime-genuineparts-text').show();
                                setTimeout(function () {
                                    window.open(link);
                                }, 100000);

                                if (result.code) {
                                    stepOther(result);
                                }
                            } else {
                                if (result.type == 'not_login') {
                                    window.location.href = "{{ \URL::route('login') }}";
                                } else {
                                    alert(result.message);
                                }
                            }

                        },
                        error: function (xhr, ajaxOption, thrownError) {
                            unlock();
                            window.open("{{ \URL::route('customer-history-genuineparts') }}");
                        }
                    });

                    swal({
                        title: '正廠零件查詢中',
                        html: "<span>請稍後約10秒鐘<span><span class='realtime-genuineparts-text' style='display:none;'>，若不想等待請按 <a href='' class='realtime-genuineparts' style='text-decoration: underline'>\"查詢完畢再通知\"</a></span>",
                        allowOutsideClick: false,
                        onOpen: function () {
                            swal.showLoading()
                        }

                    }).then(
                        function () {
                        },
                        // handling the promise rejection
                        function (dismiss) {
                            if (dismiss === 'timer') {
                                console.log('I was closed by the timer')
                            }
                            unlock();
                        }
                    );

                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        unlock();
                        return false;
                    }
                })
            }
        }


    });

</script>


@stop
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: '{{route('api-stock')}}',
                method: 'GET',
                data: {
                    sku: "{{implode(',',$collection->pluck('sku')->all())}}",
                },
                success: function (result) {
                    result = JSON.parse(result);
                    for (let [key, value] of Object.entries(result)) {
                        if (value.stock > 0) {
                            $('.stock_tag_' + key).css('display', 'block');
                        }
                        // console.log(key, value);
                    }
                }
            });
        });

    </script>
@stop

