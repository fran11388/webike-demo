<li class="pay-address" id="pay-address">
    <p>配送資料</p>
</li>
<div class="pay-address-content">
    <p><i>※</i> 收件人資料</p>
        <input type="text" name="name" placeholder="收件人姓名" value="{{ $current_customer->address->firstname }}" required>
    <p><i>※</i> 配送地址</p>
        <div class="pay-address-place">
            <div id="twzipcode" class="tip_brfore form-inline row" >
                <div class="form-group col-sm-4 col-xs-4">
                    <div data-name="county" data-role="county"></div>
                </div>
                <div class="form-group col-sm-4 col-xs-4">
                    <div data-name="district" data-role="district"></div>
                </div>
                <div class="form-group col-sm-4 col-xs-4">
                    <div data-name="zipcode" data-role="zipcode" data-value="{{ $current_customer->address->zipcode }}"></div>
                </div>
            </div>
        </div>
        <input type="text" name="street" placeholder="地址" value="{{ $current_customer->address->address }}" required>
    <p><i>※</i> 行動電話</p>
        <input type="text" name="mobile" value="{{ $current_customer->address->mobile }}" placeholder="行動電話" required>
    <p><i>※</i> 市內電話</p>
        <input type="text" name="phone" value="{{ $current_customer->address->phone }}" placeholder="市內電話" required>
    <p><i>※</i> 配送時間</p>
        <button type="button" class="pay-address-time" name="shipping-time" value="morning">上午</button>
        <button type="button" class="pay-address-time" name="shipping-time" value="noon">下午</button>
        <button type="button" class="pay-address-time open" name="shipping-time" value="anytime">不指定</button>
    <p>配送備註</p>
        <textarea name="shipping_comment" placeholder="您對本次配送有任何想法，都可以告訴我們" id=""></textarea>

</div>

