<div class="pay-step">
    <ul>
        @include ( 'mobile.pages.checkout.partial.pay-method' )

        @include ( 'mobile.pages.checkout.partial.pay-receipt' )

        @include ( 'mobile.pages.checkout.partial.pay-address' )

        <li class="checkAgree" id="title_agreement">
            <p>購買同意書（同意請打勾）</p>
        </li>
        <div class="checkAgree-content">
            <form action="">
                <div class="checkAgree-checkbox">
                    <label>
                        <input type="checkbox" name="agreement-3">
                        <p>發票開立</p>
                    </label>
                    <div class="contact-detail" id="invoice-detail">詳細閱讀
                    </div>
                </div>
                <div class="checkAgree-checkbox">
                    <label>
                        <input type="checkbox" name="agreement-1">
                        <p>交貨日期</p>
                    </label>
                    <div class="contact-detail" id="shipping-detail">詳細閱讀
                    </div>
                </div>
                <div class="checkAgree-checkbox">
                    <label>
                        <input type="checkbox" name="agreement-2">
                        <p>購買條約</p>
                    </label>
                    <div class="contact-detail" id="buy-detail">詳細閱讀
                    </div>
                </div>
            </form>
        </div>
        <li class="remark">
            <p>備註事項</p>
        </li>
        <div class="remark-content">
            <textarea name="comment" placeholder="您對本次交易有任何想法，都可以告訴我們" id=""></textarea>
        </div>
    </ul>
</div>