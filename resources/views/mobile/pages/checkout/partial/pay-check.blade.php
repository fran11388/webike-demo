<div class="pay-check">
    <div class="pay-check-total">
        <h2>結帳流程</h2>
    </div>
    <div class="pay-check-fake"></div>
    <ul class="pay-check-content">

        {{-- 當資料填妥後 icon 會變成彩色 --}}
        <li>
            <a href="" class="pay-method-check">
                <div>
                    <img class="yet" src="{{ assetRemote('image/list-icon/credit card_bw-01.svg')}}" alt="creditCard">
                    <img class="active" src="{{ assetRemote('image/list-icon/credit card-01.svg')}}" alt="creditCard">
                </div>
            </a>
        </li>
        <li>
            <a href="" class="pay-receipt-check">
                <div>
                    <img class="yet" src="{{ assetRemote('image/list-icon/invoice_bw-01.svg')}}" alt="invoice">
                    <img class="active" src="{{ assetRemote('image/list-icon/invoice-01.svg')}}" alt="invoice">
                </div>
            </a>
        </li>
        <li>
            <a href="" class="pay-address-check">
                <div>
                    <img class="yet" src="{{ assetRemote('image/list-icon/box_bw-01.svg')}}" alt="expressDelivery">
                    <img class="active" src="{{ assetRemote('image/list-icon/box-01.svg')}}" alt="expressDelivery">
                </div>
            </a>
        </li>
        <li>
            <a href="" class="checkAgree-check">
                <div>
                    <img class="yet" src="{{ assetRemote('image/list-icon/checkAgree_bw-01.svg')}}" alt="checkAgree">
                    <img class="active" src="{{ assetRemote('image/list-icon/checkAgree-01.svg')}}" alt="checkAgree">
                </div>
            </a>
        </li>
        {{--<li>--}}
            {{--<a href="" class="remark-check">--}}
                {{--<div>--}}
                    {{--<img class="yet" src="{{ assetRemote('image/list-icon/remark_bw-01.svg')}}" alt="remark">--}}
                    {{--<img class="active" src="{{ assetRemote('image/list-icon/remark-01.svg')}}" alt="remark">--}}
                {{--</div>--}}
            {{--</a>--}}
        {{--</li>--}}

        <div class="fixclear"></div>
    </ul>
</div>