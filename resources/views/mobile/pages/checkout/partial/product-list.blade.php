<li>
    <div class="left">
        <img src="{{$cart->product->getThumbnail()}}" alt="{{$cart->product->full_name}}" >
    </div>
    <div class="right">
        <h2>{{ formatProductFullName($cart->product->name, $cart->product->manufacturer->name) }}</h2>
    </div>

    <div class="about">
        <p>數量：<span>{{ $cart->quantity }}</span></p>
        <p class="price">NT$<span>{{ number_format($cart->product->getCartPrice($current_customer) * $cart->quantity) }}</span></p>
    </div>
    <div class="fixclear"></div>
</li>



