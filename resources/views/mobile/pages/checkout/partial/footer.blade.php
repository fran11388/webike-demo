<div id="footer-login-page">
    <div class="f-content">
        <p><label class="layaway-origin">實際結帳金額：</label><label class="layaway-override" style="display: none;">原始結帳總金額：</label>
            <span class="hide-in-cd">NT$<i>{{ number_format($service->getFee() + $service->getSubtotal() - $celebration->getDiscount() ) }}</i></span>
            <span class="show-in-cd" style="display:none;">NT$<i>{{ number_format($service->getFee() + $service->getSubtotal() + $method->getFee() - $celebration->getDiscount()) }}</i></span>
        </p>
        <p class="font-color-red font-bold layaway-total-price-area hide"><label class="layaway-aliquot size-10rem"></label>期分期金額：<span>NT$<label class="layaway-total-price"></label></span></p>
        {{--<p class="font-color-red font-bold"><label class="layaway-aliquot size-10rem"></label>期分期金額：<span>NT$<label class="layaway-total-price">100</label></span></p>--}}
        {{--<p>商品總金額：--}}
            {{--<span>NT$ <i>1000</i></span>--}}
        {{--</p>--}}
        {{-- 選擇貨到付款才出現 --}}
        {{--<p>貨到付款手續費：--}}
            {{--<span>NT$ <i>60</i></span>--}}
        {{--</p>--}}
        {{--<p>結帳金額：--}}
            {{--<span>NT$ <i class="total">1060</i></span>--}}
        {{--</p>--}}
        <div class="button">
            <a class="f-return" href="{{ \URL::route('cart') }}">
            <i class="fas fa-chevron-left"></i>回購物車
            </a>
            <button class="f-send" id="#checkout_form">送出訂單</button>
        </div>
    </div>
</div>