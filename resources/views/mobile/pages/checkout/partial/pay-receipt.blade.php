<li class="pay-receipt" id="invoice">
    <p>發票資料</p>
</li>

<li class="pay-receipt-content">
    <div class="pay-receipt-bt-box">
        <button type="button" name="vat-require" class="personal-receipt open" id="personal-invoice-content" value="0">個人電子發票(二聯式)</button>
        <button type="button" name="vat-require" class="company-receipt" id="company-invoice-content" value="1">公司報帳用電子發票(三聯式)</button>
        <button type="button" name="vat-require" class="donate-receipt" id="donat-invoice-content" value="2">捐贈發票</button>
    </div>

    <div class="pay-receipt-mail-green personal-invoice-content">
        <h3>請輸入電子信箱</h3>
        <input type="email" name="email" id="email" placeholder="電子信箱" value="{{ $current_customer->email }}">
        <span>綠界科技載具(電子發票開立通知將透過Email發送)</span>
        <p>
            在訂單包裹出貨後，我們會透過
            <a href="https://www.ecpay.com.tw/">綠界科技</a>
            將您訂單發票的電子檔透過電子郵件或是手機簡訊，發送至您在會員帳號所登錄的電子信箱或是手機號碼。<br>依統一發票使用辦法規定：個人(二聯式)發票一經開立，無法更改或改開公司戶(三聯式)發票。
            <a href="https://www.einvoice.nat.gov.tw/index">(財政部電子發票流程說明)</a>
        </p>
    </div>

    <div class="pay-receipt-input company-invoice-content">
        <h3>請輸入公司名稱</h3>
        <input type="text" id="company" name="company" placeholder="公司名稱" value="{{$current_customer->address->company}}">
        <h3>請輸入統一編號</h3>
        <input type="text" id="vat_number" name="vat_number" placeholder="統一編號" value="{{$current_customer->address->vat_number}}">
        <div id='vat_number_massage' class='check'>
            <span class="font-color-red hidden vat_number_count_error ">※統一編號字數錯誤</span>
            <span class="font-color-red hidden vat_number_error">※統一編號錯誤，請輸入正確的統一編號</span>
            <i class='fa fa-check-circle hidden'></i>
        </div>
        <div class="pay-receipt-mail">
            <h3>請輸入電子信箱</h3>
            <input type="email" name="email" id="email" placeholder="電子信箱" value="{{ $current_customer->email }}">
            <span>綠界科技載具(電子發票開立通知將透過Email發送)</span>
            <p>
                在訂單包裹出貨後，我們會透過
                <a href="https://www.ecpay.com.tw/">綠界科技</a>
                將您訂單發票的電子檔透過電子郵件或是手機簡訊，發送至您在會員帳號所登錄的電子信箱或是手機號碼。<br>依統一發票使用辦法規定：個人(二聯式)發票一經開立，無法更改或改開公司戶(三聯式)發票。
                <a href="https://www.einvoice.nat.gov.tw/index">(財政部電子發票流程說明)</a>
            </p>
        </div>
    </div>

    <div class="pay-receipt-donate donat-invoice-content">
        <div class="pay-receipt-radio">
            <label>
                <input type="radio" name="donate_code" value="25885"><span>伊甸基金會</span>
            </label>
        </div>
        <div class="pay-receipt-radio">
            <label>
                <input type="radio" name="donate_code" value="919"><span>創世基金會</span>
            </label>
        </div>
        <div class="pay-receipt-radio">
            <label>
                <input type="radio" name="donate_code" value="531"><span>董事基金會</span>
            </label>
        </div>
        <div class="pay-receipt-radio">
            <label>
                <input type="radio" name="donate_code" value="621"><span>漸凍人協會</span>
            </label>
        </div>
        <div class="pay-receipt-radio">
            <label>
                <input type="radio" name="donate_code" value="8585"><span>家扶基金會</span>
            </label>
        </div>

        <div class="pay-receipt-mail">
            <h3>請輸入電子信箱</h3>
            <input type="email" name="email" id="email" placeholder="電子信箱" value="{{ $current_customer->email }}">
            <span>綠界科技載具(電子發票開立通知將透過Email發送)</span>
            <p>
                在訂單包裹出貨後，我們會透過
                <a href="https://www.ecpay.com.tw/">綠界科技</a>
                將您訂單發票的電子檔透過電子郵件或是手機簡訊，發送至您在會員帳號所登錄的電子信箱或是手機號碼。<br>依統一發票使用辦法規定：個人(二聯式)發票一經開立，無法更改或改開公司戶(三聯式)發票。
                <a href="https://www.einvoice.nat.gov.tw/index">(財政部電子發票流程說明)</a>
            </p>
        </div>
    </div>

</li>
