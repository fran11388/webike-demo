<li class="pay-method" id="pay-method">
    <p>付款方式</p>
</li>

<li class="pay-method-content">
    <div class="pay-method-bt-box">
        <?php $method = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('cash-on-delivery'); ?>
        @if ($method->isAvailable())
            <div class="pay-method-bt pay-method-installment-off">
                <button type="button" name="payment-method" value="cash-on-delivery">貨到付款</button>
                <a href="{{ route('customer-rule', 'member_rule_2'.'#E_a') }}">
                    <span>(手續費及說明)</span>
                </a>
            </div>
        @endif
        <div class="pay-method-bt pay-method-installment-off">
            <button type="button" name="payment-method" value="bank-transfer">匯款轉帳</button>
            <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_c' )}}">
                <span>說明</span>
            </a>
        </div>
        @php
            $multiples = \Ecommerce\Shopping\Payment\CoreInstallment::decideMultiples($current_customer->role_id);
            $esan_disabled = ['text' => '', 'flg' => false, 'layaway_flg' => false];
            if(activeValidate('2019-04-16 04:00:00', '2019-04-16 23:00:00')){
                $esan_disabled['flg'] = true;
                $esan_disabled['layaway_flg'] = true;
                $esan_disabled['text'] = '※系統商安全性維護中，請選擇其他付款方式。維護時間為2019-04-16 04:00~06:00。';
            }else if($service->includeGroupbuyItem()){
                $esan_disabled['layaway_flg'] = true;
                $esan_disabled['text'] = '※選購"團購商品"無法使用信用卡分期付款。';
            }else if($service->includeOutletItem()){
                $esan_disabled['layaway_flg'] = true;
                $esan_disabled['text'] = '※選購"OUTLET出清商品"無法使用信用卡分期付款。';
            }
            $nccc_disabled = ['text' => '', 'flg' => false, 'layaway_flg' => false];
            if(activeValidate('2019-04-16 04:00:00', '2019-04-16 23:00:00')){
                $nccc_disabled['flg'] = true;
                $nccc_disabled['layaway_flg'] = true;
                $nccc_disabled['text'] = '※系統商安全性維護中，請選擇其他付款方式。<br>維護時間為2019-10-25 02:00~04:30。';
            }else if($service->includeGroupbuyItem()){
                $nccc_disabled['layaway_flg'] = true;
                $nccc_disabled['text'] = '※選購"團購商品"無法使用信用卡分期付款。';
            }else if($service->includeOutletItem()){
                $nccc_disabled['layaway_flg'] = true;
                $nccc_disabled['text'] = '※選購"OUTLET出清商品"無法使用信用卡分期付款。';
            }
        @endphp
        <div class="pay-method-bt pay-method-installment-off">
            <button type="button" name="payment-method" value="credit-card" {{ $esan_disabled['flg'] ? 'disabled': '' }}>信用卡一次付清</button>
            <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b' )}}">
                <span>說明</span>
            </a>
        </div>
        @php
            $has_layaway_payment = \Ecommerce\Shopping\Payment\PaymentManager::hasLayawayPayment();
            $layaway_multiple_text = '0利率';
            $layaway_info_text = '分期服務說明';
            $layaway_info_url = route( 'customer-rule', 'member_rule_2'.'#E_b_2' );
            $layaway_front = '';
            $layaway_tag = '';
            if($current_customer->role_id == \Everglory\Constants\CustomerRole::WHOLESALE){
                $layaway_multiple_text = '';
                $layaway_info_text = '分期服務說明';
                $layaway_info_url = route( 'service-dealer-installment-info' );
                $layaway_front = '';
                $layaway_tag = '';
                /*$layaway_tag = '<img src="' . assetRemote('image/new_right.gif') . '">';*/
            }
        @endphp
        @if($esan_disabled['layaway_flg'])
            <div class="pay-method-bt pay-method-installment">
                <button type="button" name="payment-method" value="credit-card-layaway">{!! $layaway_front !!}信用卡<span class="installment_title_text" style="color:#333333"></span>期分期付款</button>
                <a href="{!! $layaway_info_url !!}" target="_blank">
                    <span>({!! $layaway_info_text !!})</span>
                </a>
                {!! $layaway_tag !!}
            </div>
            <div class="pay-method-bt pay-method-installment">
                <span class="font-color-red">{{$esan_disabled['text']}}</span>
            </div>
        @elseif($has_layaway_payment and !$esan_disabled['layaway_flg'])
            <div class="pay-method-bt pay-method-installment">
                <button type="button" name="payment-method" value="credit-card-layaway">{!! $layaway_front !!}信用卡<span class="installment_title_text hidden-xs hide" style="color:#333333"></span>分期付款</button>
                <a href="{!! $layaway_info_url !!}" target="_blank">

                    <span>({!! $layaway_info_text !!})</span>
                </a>
                {!! $layaway_tag !!}
            </div>
        @endif
    </div>
    @if($has_layaway_payment)
        <div class="pay-method-credit-card clearfix layaway-payment">
            <div class="pay-method-credit-card-L">
                <span>聯合信用卡中心分期付款</span>
                <?php
                $ncccMethod = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('credit-card-layaway-nccc');
                $nccc_disabled['layaway_flg'] ? $disabled = 'disabled' : $disabled = '';
                ?>
                @if ($ncccMethod->isAvailable())
                    <button type="button" value="credit-card-layaway-nccc" class="installment_text" name="payment-method" data-install="3" {{ $disabled }}>3期{!! (isset($multiples[3]) and $multiples[3] > 1) ? '' : '0利率' !!}信用卡分期</button>
                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                    @endif
                @endif
                <?php $ncccMethod->setInstall(6) ?>
                @if ($ncccMethod->isAvailable())
                    <button type="button" value="credit-card-layaway-nccc" class="installment_text" name="payment-method" data-install="6" {{ $disabled }}>6期{!! (isset($multiples[6]) and $multiples[6] > 1) ? '' : '0利率' !!}信用卡分期</button>
                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                    @endif
                @endif
                <?php $ncccMethod->setInstall(12) ?>
                @if ($ncccMethod->isAvailable())
                    <button type="button" value="credit-card-layaway-nccc" class="installment_text" name="payment-method" data-install="12" {{ $disabled }}>12期{!! (isset($multiples[12]) and $multiples[12] > 1) ? '' : '0利率' !!}信用卡分期</button>
                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                    @endif
                @endif
                <p class="layaway-aliquot-text-3">支援：<br>中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、遠東銀行、新光銀行、澳盛銀行、大眾銀行、台北富邦、第一銀行；共十二家發卡銀行。</p>
                <p class="layaway-aliquot-text-6">支援：<br>中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、遠東銀行、新光銀行、澳盛銀行、大眾銀行、台北富邦、第一銀行；共十二家發卡銀行。</p>
                <p class="layaway-aliquot-text-12">支援：<br>中信銀行、國泰世華、台新銀行、花旗銀行、永豐銀行、聯邦銀行、新光銀行、澳盛銀行、台北富邦、第一銀行；共十家發卡銀行。</p>
            </div>
            <div class="pay-method-credit-card-R">
                <span>玉山銀行分期付款</span>
                <?php
                $esunMethod = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('credit-card-layaway-esun');
                $esan_disabled['layaway_flg'] ? $disabled = 'disabled' : $disabled = '';
                ?>
                @if ($esunMethod->isAvailable())
                    <button type="button" value="credit-card-layaway-esun" name="payment-method" data-install="3" {{ $disabled }}>3期{!! (isset($multiples[3]) and $multiples[3] > 1) ? '' : '0利率' !!}信用卡分期</button>
                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                    @endif
                @endif
                <?php $esunMethod->setInstall(6) ?>
                @if ($esunMethod->isAvailable())
                    <button type="button" value="credit-card-layaway-esun" name="payment-method" data-install="6" {{ $disabled }}>6期{!! (isset($multiples[6]) and $multiples[6] > 1) ? '' : '0利率' !!}信用卡分期</button>
                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                    @endif
                @endif
                <?php $esunMethod->setInstall(12) ?>
                @if ($esunMethod->isAvailable())
                    <button type="button" value="credit-card-layaway-esun" name="payment-method" data-install="12" {{ $disabled }}>12期{!! (isset($multiples[12]) and $multiples[12] > 1) ? '' : '0利率' !!}信用卡分期</button>
                    @if($current_customer->role_id != \Everglory\Constants\CustomerRole::WHOLESALE)
                        <a href="{{route( 'customer-rule', 'member_rule_2'.'#E_b_2' )}}" style="display: inline-block;vertical-align: middle;" target="_blank">(說明)</a>
                    @endif
                @endif
                <p>僅支援玉山銀行信用卡</p>
            </div>
            <div class="layaway-payment-pre-area fixclear">
                {{--<li class="option-style1-title layaway-payment-pre" style="display: none;">--}}
                <div class="option-style1-title layaway-payment-pre">
                    <hr>
                    <h3 class="layaway-total-price-area hide">信用卡<span class="layaway-aliquot"></span>期分期結帳金額：<span class="layaway-total-price"></span> 元。<span class="layaway-multiple"></span></h3>
                    <h3>分期費用：首期 <span class="first"></span> 元，其他期 <span class="other"></span> 元。</h3>
                </div>
            </div>
            {{--<div class="fixclear"></div>--}}
        </div>
    @endif
</li>