@extends('mobile.layouts.1column-simplify')
@section('style')
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/checkout.css') !!}">
    {{-- 2019.3.21 William Tai --}}
    <link rel="stylesheet" type="text/css" href="{!! assetRemote('css/pages/checkout-new.css') !!}">
    <style>
        body.swal2-iosfix {
            position: relative !important;
        }
        /*body.swal2-show:not(.swal2-no-backdrop):not(.swal2-toast-show) { overflow-y: visible !important; }*/
        /*html.swal2-iosfix,body.swal2-iosfix { overflow-y: hidden !important; height: auto!important;}*/
    </style>
@endsection
@section('middle')
    <form method="post" action="{{route('checkout-save')}}" id="checkout_form">
        <?php
        $celebration = new \Ecommerce\Core\Celebration();
        ?>
        <input type="hidden" name="protect-code" value="{{ $protect_code }}"/>
        <div class="all-page">

            <div class="title"><p>結帳</p></div>
            <div class="content">
                <div class="commodity-title"><p>購物明細</p></div>
                <div class="commodity-list">
                    <ul>
                        @foreach ($carts as $cart)
                            @include ( 'mobile.pages.checkout.partial.product-list' )
                        @endforeach
                        @foreach ($gifts as $product)
                            @include ( 'mobile.pages.checkout.partial.product-list-gifts' )
                        @endforeach
                        <div class="total">
                            <div class="point">
                                <p>折扣備駐：</p>
                                @if($coupon = $service->getUsageCoupon())
                                    <p>已使用{{$coupon->name}}。</p>
                                @endif
                                @if($points = $service->getUsagePoints())
                                    <p>已使用{{$points}}點點數。</p>
                                @endif
                                @if ($celebration->getDiscount())
                                    {{ $celebration->getTitleInCheckout() }}
                                @endif
                            </div>
                            <div class="total-price">
                                <p>運費： <span>NT$<i>{{ number_format($service->getFee()) }}</i></span></p>
                                <?php $method = \Ecommerce\Shopping\Payment\PaymentManager::getPaymentMethod('cash-on-delivery'); ?>
                                @if ($method->isAvailable())
                                    <p class="show-in-cd" style="display:none;">貨到付款手續費：<span>NT${{ number_format($method->getFee()) }}</span></p>
                                @endif
                                @if ($celebration->getDiscount())
                                    <p>滿額折抵：<span>NT$<i>{{ number_format($celebration->getDiscount()) }}</i></span></p>
                                @endif
                                <p>折抵金額合計： <span>NT$<i>{{ number_format($service->getDiscountTotal() + $celebration->getDiscount() ) }}</i></span></p>
                                <p>商品金額合計： <span>NT$<i>{{ number_format($service->getBaseTotal()) }}</i></span> </p>
                                <p><label class="layaway-origin">實際結帳金額：</label><label class="layaway-override" style="display: none;">原始結帳總金額：</label>
                                    <span class="hide-in-cd">NT$<i>{{ number_format($service->getFee() + $service->getSubtotal() - $celebration->getDiscount() ) }}</i></span>
                                    <span class="show-in-cd" style="display:none;">NT$<i>{{ number_format($service->getFee() + $service->getSubtotal() + $method->getFee() - $celebration->getDiscount()) }}</i></span>
                                </p>
                                <p class="font-color-red font-bold layaway-total-price-area hide"><label class="layaway-aliquot size-10rem"></label>期分期金額：<span>NT$<label class="layaway-total-price"></label></span></p>
                            </div>
                        </div>
                    </ul>
                </div>


                @include ( 'mobile.pages.checkout.partial.pay-check' )

                @include ( 'mobile.pages.checkout.partial.pay-step' )

            </div>

            {{-- @include ( 'response.layouts.partials.script' )
            @yield('script')
            @include ( 'response.layouts.partials.track.google-sending' ) --}}
            @include ( 'mobile.pages.checkout.partial.footer' )
            <input type="hidden" name="payment-method">
            <input type="hidden" name="vat-require">
            <input type="hidden" name="shipping-time">
            <input type="hidden" name="install">
            <input type="hidden" name="final_email">
        </div>
    </form>
@endsection
@section('script')
    <script src="{!! assetRemote('plugin/jquery.twzipcode.min.js') !!}"></script>
    <script>
        function disablePayment( value ){
            var outside = ["連江縣", "金門縣", "南海諸島", "澎湖縣"];
            var payway = $('input[value=cash-on-delivery]');
            if( $.inArray(value, outside) !== -1 ){
                payway.prop('disabled', 'disabled');
                payway.removeAttr('checked');
            }else{
                payway.removeAttr('disabled');
            }
        }

        jQuery(document).ready(function($) {

            var value = $('select[name=county]').find('option:selected').val();
            disablePayment(value);
            // $(function(){
            //     $('.pay-method-content').removeToggle(250);
            // }
            var installment_text = '';
            var text = '';
            $('.pay-method-credit-card .pay-method-credit-card-L button').each(function(index,value){
                if(index != 0){
                    text = '、';
                }
                installment_text = installment_text + text + $(this).text().trim().match(/\d+/);
            });
            $('.installment_title_text').text(installment_text);

            // $('.pay-check ul li div').css('height',$('.pay-check ul li div').css('width'));
            validateAddress();
            ValidateInvoice();
        });

        $(".pay-method-bt-box button[name=payment-method]").click(function(event) {
            $(this).closest('.pay-method-bt-box').find('button').removeClass('open');
            $(this).stop().addClass('open');

            if($(this).parent().hasClass('pay-method-installment')){
                $('.pay-method-credit-card').stop().slideDown(250);
            }else{
                $('.pay-method-credit-card').stop().slideUp(250);
            }
        });

        $("button[name=payment-method]").click(function(event) {

            if ($(this).val() == 'cash-on-delivery'){
                $(".show-in-cd").show();
                $(".hide-in-cd").hide();
            } else {
                $(".show-in-cd").hide();
                $(".hide-in-cd").show();
            }

            var parent = $('.layaway-payment');
            $('.layaway-total-price-area').addClass('hide');
            $('.layaway-override').hide();
            $('.layaway-origin').show();
            parent.find('.layaway-payment-pre').hide();

            if($(this).val().indexOf('layaway') >= 0){
                $('[class^=layaway-aliquot-text-]').hide();
                $('[class=layaway-aliquot-text-3]').show();
                var total = {{$service->getFee() + $service->getSubtotal()}};
//                var aliquot = $(this).closest('label').text().trim().substr(0, 1);
                var aliquot = $(this).text().trim().match(/\d+/);
                var sub_total = "{{ $service->getSubtotal() }}";
                var multiples = JSON.parse('{!! json_encode(\Ecommerce\Shopping\Payment\CoreInstallment::decideMultiples($current_customer->role_id)) !!}');
                        @php
                            $celebration = new \Ecommerce\Core\Celebration();
                            $celebration_discount = $celebration->getDiscount();
                        @endphp
                var promo_discount = 0;

                total = Math.round({!! ($service->getSubtotal() - $celebration_discount) !!} * multiples[aliquot]) + {!! $service->getFee() !!};

                var pre_layaway = Math.floor(total / aliquot);

                parent.find('span.first').html((total - (pre_layaway * (aliquot - 1))).toLocaleString(
                    undefined, // use a string like 'en-US' to override browser locale
                    {}
                ));
                parent.find('span.other').html(pre_layaway.toLocaleString(
                    undefined, // use a string like 'en-US' to override browser locale
                    {}
                ));
                if($(this).val().indexOf('layaway-') >= 0){
                    parent.find('.layaway-payment-pre').show();
                    $('.layaway-aliquot').text(aliquot);
                    if($(this).val() == 'credit-card-layaway-nccc' || $(this).val() == 'credit-card-layaway-esun'){
                        $('[class^=layaway-aliquot-text-]').hide();
                        $('.layaway-aliquot-text-' + aliquot).show();
                    }
                    if((multiples[aliquot] - 1) > 0){
                        $('.layaway-override').show();
                        $('.layaway-origin').hide();
                        var origin_total = "{{number_format($service->getBaseTotal())}}";
                        var total_discount = "{{ number_format($service->getDiscountTotal() + $celebration->getDiscount() ) }}";
                        $('.layaway-multiple').text('(' + origin_total.toLocaleString(
                                undefined, // use a string like 'en-US' to override browser locale
                                {}

                            ) + ' - '  + total_discount + ')' + ' x ' + '(1 + ' + (((multiples[aliquot] - 1) * 100).toFixed(1)) + '%)' + {!! $service->getFee() ? '" + " + ' . $service->getFee() : "''" !!});

                        $('.layaway-total-price').text(total.toLocaleString(
                            undefined, // use a string like 'en-US' to override browser locale
                            {}
                        ));
                        $('.layaway-total-price-area').removeClass('hide');
                    }
                    $('.pay-method-check .active').show();
                    $('.pay-method-check .yet').hide();
                }else{
                    $('.pay-method-check .active').hide();
                    $('.pay-method-check .yet').show();
                }
                $(this).closest('li').find('.fake-point').show();
            }else{
                $('.pay-method-check .active').show();
                $('.pay-method-check .yet').hide();
                parent.find('.layaway-payment-pre').hide();
                $(this).closest('div').find('.fake-point').hide();
                parent.find('button').removeClass('open');
            }
        });

        top_height = $('.pay-check-content').height() + $('header').height() + $('.w-ad-bar').height() + 10;

        $('#checkout_form').submit(function(e){
            payment_method = $('button[name="payment-method"].open').last().val();
            if(!payment_method || payment_method =='credit-card-layaway'){
                swal(
                    '付款方式錯誤',
                    '請選擇一種付款方式!',
                    'error'
                ).then(function (result) {
                    $('html,body').animate({
                        scrollTop: $('.pay-method').position().top - top_height
                    }, 200);
                });
                return false;
            }

            if($('input[name^="agreement"]:checked').length != 3){
                //                alert('請選擇付款方式!');
                swal(
                    '購買同意書未確認',
                    '請確認所有購買同意書!',
                    'error'
                ).then(function (result) {
                    $('html,body').animate({
                        scrollTop: $('#title_agreement').position().top - top_height
                    }, 200);
                });

                return false;
            }

            var tab_class = $('button[name=vat-require].open').attr('id');
            var email = $('.pay-receipt-content .' + tab_class).find('#email').val();

            if(!email){
                swal(
                    '電子信箱尚未輸入',
                    '請輸入寄送電子信箱',
                    'error'
                ).then(function (result) {
                    $('html,body').animate({
                        scrollTop: $('#invoice').position().top - top_height
                    }, 200);
                });
                return false;
            }else{
                $('input[name=final_email]').val(email);
            }


            if($('button[name=vat-require].open').val() == 2){
                var donat_code = $('input[name=donate_code]:checked').val();
                if(!donat_code){
                    swal(
                        '捐贈代碼尚未選擇',
                        '請選擇捐贈代碼',
                        'error'
                    ).then(function (result) {
                        $('html,body').animate({
                            scrollTop: $('#invoice').position().top - top_height
                        }, 200);
                    });
                    return false;
                }
            }

            if($('button[name=vat-require].open').val() == 1){
                var vat_number = $('#vat_number').val();
                var vat_number_result =  checkVat_number(vat_number);
                if(!vat_number){
                    swal(
                        '統一編號未輸入',
                        '請輸入統一編號',
                        'error'
                    ).then(function (result) {
                        $('html,body').animate({
                            scrollTop: $('#invoice').position().top - top_height
                        }, 200);
                        $("#vat_number_massage span.vat_number_count_error").removeClass('hidden');
                    });
                    return false;
                }else if(!vat_number_result){
                    swal(
                        '輸入錯誤',
                        '請輸入正確的統一編號',
                        'error'
                    ).then(function (result) {
                        $('html,body').animate({
                            scrollTop: $('#invoice').position().top - top_height
                        }, 200);
                    });

                    return false;
                }
            }

            var vat_require = $('button[name=vat-require].open').val();
            if(vat_require == 0){
                $('.company-invoice-block input').val('');
                $('.donat-invoice-block input').val('');
            }else if(vat_require == 1){
                $('.personal-invoice-block input').val('');
                $('.donat-invoice-block input').val('');
            }else if(vat_require == 2){
                $('.personal-invoice-block input').val('');
                $('.company-invoice-block input').val('');
            }

            var choose_time = $("button[name=shipping-time].open").val();
            if(!choose_time){
                swal(
                    '請選擇配送時段'
                ).then(function (result) {
                    $('html,body').animate({
                        scrollTop: $('#pay-address').position().top - top_height
                    }, 200);
                });
            }

            $("input[name=vat-require]").val($("button[name=vat-require].open").val());
            $("input[name=shipping-time]").val($("button[name=shipping-time].open").val());

            $('.checkout_submit').attr('disabled',true);

            $("input[name=install]").val( $("button[name=payment-method].open").last().attr('data-install'));
        });

        $('.pay-method-credit-card-L button').click(function(event) {
            $(this).stop().toggleClass('open');
            $(this).siblings().removeClass('open');
            $('.pay-method-credit-card-R button').removeClass('open');
        });

        $('.pay-method-credit-card-R button').click(function(event) {
            $(this).stop().toggleClass('open');
            $(this).siblings().removeClass('open');
            $('.pay-method-credit-card-L button').removeClass('open');
        });

        $('.pay-receipt-bt-box button').click(function(event) {
            /* Act on the event */
            $(this).stop().addClass('open');
            $(this).siblings().removeClass('open');
        });

        $('.personal-receipt').click(function(event) {
            /* Act on the event */
            $('.pay-receipt-mail-green').stop().slideDown(250);
            $('.pay-receipt-input').stop().slideUp(250);
            $('.pay-receipt-donate').stop().slideUp(250);
        });

        $('.company-receipt').click(function(event) {
            /* Act on the event */
            $('.pay-receipt-input').stop().slideDown(250);
            $('.pay-receipt-mail-green').stop().slideUp(250);
            $('.pay-receipt-donate').stop().slideUp(250);
        });
        $('.donate-receipt').click(function(event) {
            /* Act on the event */
            $('.pay-receipt-donate').stop().slideDown(250);
            $('.pay-receipt-mail-green').stop().slideUp(250);
            $('.pay-receipt-input').stop().slideUp(250);
        });

        $('.pay-address-time').click(function(event) {
            /* Act on the event */
            $(this).stop().addClass('open');
            $(this).siblings().removeClass('open');
        });

        $(function() {
            $(window).scroll(function() {
                if ($(this).scrollTop() > $('.pay-check-fake').offset().top - $('header').height() - $('.w-ad-bar').height() )  {          /* 要滑動到選單的距離 */
                    $('.pay-check-content').addClass('navFixed');   /* 幫選單加上固定效果 */
                    $('.navFixed').css('top',50 + $('.w-ad-bar').height());
                    $('.pay-check-fake').css('height',$('.pay-check-content').css('height'))
                } else {
                    $('.pay-check-content').removeClass('navFixed'); /* 移除選單固定效果 */
                    $('.pay-check-fake').css('height','0')
                }
            });
        });

        $('.pay-method-check').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $('.pay-method').offset().top - top_height
            }, 100);
        });
        $('.pay-receipt-check').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $('.pay-receipt').offset().top - top_height
            }, 100);
        });
        $('.pay-address-check').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $('.pay-address').offset().top - top_height
            }, 100);
        });
        $('.checkAgree-check').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $('.checkAgree').offset().top - top_height
            }, 100);
        });
        $('.remark-check').click(function(event) {
            /* Act on the event */
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $('.remark').offset().top - top_height
            }, 100);
        })

        $(document).on('change', $('select[name=county]'), function(){
            var value = $(this).find('option:selected').val();
            disablePayment( value );
        });

        $('#twzipcode').twzipcode({
            readonly:true,
            'css': ['form-control select2', 'form-control select2', 'form-control'],
        });


        $("#vat_number").change(function(){
            var vat_number = $(this).val();
            checkVat_number(vat_number);

        });

        function checkVat_number(vat_number ){
            $("#vat_number_massage span").addClass('hidden');
            $("#vat_number_massage i").addClass('hidden');
            var check_number = ['1','2','1','2','1','2','4','1'];
            if(vat_number.length != 8 ){
                $("#vat_number_massage span.vat_number_count_error").removeClass('hidden');
                return false;
            }
            var number = vat_number.split("");
            var sum_multiple_number = 0;

            for(var i=0;i<=7;i++){
                var multiple_number = number[i] * check_number[i];
                if(multiple_number > 9){
                    var string_multiple_number = multiple_number.toString() ;
                    var multiple_number_ten = parseInt(string_multiple_number.substr(0,1));
                    var multiple_number_one = parseInt(string_multiple_number.substr(1,1));
                    multiple_number = multiple_number_ten + multiple_number_one;
                }
                sum_multiple_number = multiple_number + sum_multiple_number;
            }

            if(sum_multiple_number % 10 == 0 || (number[6] == 7 && ((sum_multiple_number - (10 - 1)) % 10 == 0 || (sum_multiple_number - 10) % 10 == 0))){
                $("#vat_number_massage i").removeClass('hidden');
                $("input[name=vat_number]").addClass('correct');
                ValidateInvoice();
                return true;
            }else{
                $("#vat_number_massage span.vat_number_error").removeClass('hidden');
                $("input[name=vat_number]").addClass('incorrect');
                ValidateInvoice();
                return false;
            }
        }

        $('.checkAgree-content input').click(function(){
            if ($('.checkAgree-content input:checked').length == $('.checkAgree-content input').length) {
                activeShow('.checkAgree-check');
            }else{
                activeHide('.checkAgree-check');
            }
        });

        $(document).on('change',".pay-address-content input[name=name],.pay-address-content input[name=street],.pay-address-content input[name=mobile],.pay-address-content input[name=phone],.pay-address-content select[name=county],.pay-address-content select[name=district],.pay-address-content input[name=zipcode]",function(){
            validateAddress();
        });

        function validateAddress()
        {
            var isValid = true;
            $(".pay-address-content input").each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                }
            });
            $(".pay-address-content select").each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                }
            });

            if(isValid == true){
                activeShow('.pay-address-check');
            }else{
                activeHide('.pay-address-check');
            }
        }


        $('.pay-receipt-content button').click(function(){
            ValidateInvoice();
        });

        $(document).on('change','.pay-receipt-content input',function(){
            ValidateInvoice();
        });

        function ValidateInvoice(){
            var tab_val = $('button[name=vat-require].open').val();

            var isValid = true;
            $('.pay-receipt-content input[name=email]').each(function(){
                var element = $(this);
                if(element.val() == ""){
                    isValid = false;
                }
            });

            if(isValid == true){
                switch (tab_val) {
                    case "1":
                        if($("input[name=vat_number]").hasClass('incorrect') || $("input[name=vat_number]").val() == ""){
                            isValid = false;
                        }
                        break;
                    case "2":
                        if(!$(".donat-invoice-content input[name=donate_code]:checked").length == true){
                            isValid = false;
                        }
                        break;
                }
            }

            if(isValid == true){
                activeShow('.pay-receipt-check');
            }else{
                activeHide('.pay-receipt-check');
            }
        }

        function activeShow(class_name){
            $(class_name + ' .yet').hide();
            $(class_name + ' .active').show();
        }

        function activeHide(class_name){
            $(class_name + ' .yet').show();
            $(class_name + ' .active').hide();
        }

        $("button").click(function(){
            var button_val = $(this).val();
            var button_name = $(this).attr('name');
            if(false === (button_val == "credit-card-layaway" && button_name == "payment-method")) {
                $("input[name=" + button_name + "]").val(button_val);
            }
        });


        // 解決鍵盤推移下方結帳fixed問題

        // 鍵盤未跳出時 視窗高度
        var windheight = $(window).height();

        // 鍵盤跳出時 視窗高度
        $(window).resize(function(){
           var docheight = $(window).height();
           if(docheight < windheight){
            //當鍵盤高度小於未跳出鍵盤高度時執行 
              $(".f-content").css("display","none");
              $(".navFixed").css("display","none");
           }else{
              $(".f-content").css("display","block");
              $(".navFixed").css("display","block");
           }
        });

        $('.contact-detail').click(function(){
            switch ($(this).attr('id')) {
                case "invoice-detail":
                    text = '<div style="text-align:left;max-height: 400px;overflow:auto;">同意<a href="https://www.ecpay.com.tw/" target="_blank">綠界科技</a>在電子發票相關業務上使用會員個人資料。 (<a href="{{ \URL::route('customer-rule','member_rule_8') }}" target="_blank">會員條約-隱私權保護政策</a>)<br><br>若有退換貨需求時，我同意由榮芳興業有限公司代為處理電子發票以及銷貨退回證明單，以加速退貨退款作業。</div>';
                    swal({
                        html: text
                    });
                    break;
                case "shipping-detail":
                    text = '<div style="text-align:left;max-height: 400px;overflow:auto;">確認訂單與交期<br>交期確認中：在我們接到您的訂單後，需有1~2個工作天進行「交期與商品庫存的確認」（如遇到例假日則順延）。<br>關於交期：<br>a.單一商品交期：本公司由日本進口商品為每周定期進口，一般交期約5~7個工作天，（以交期通知的交期為準），由台灣廠商進貨商品一般交期為2~3個工作天 。<br>b.複數商品交期：若您一次購買複數商品，我們以等待最長時間的商品為統一交期；若部分商品交期過長，會員可以選擇繼續等待或要求修改「訂單內容」取消該項商品。<br>交期確認之後我們會回覆「交期」與「匯款資訊」供您確認，確認無誤後請您進行匯款。信用卡付款、貨到付款在商品交期正常的狀況下，我們會直接為您向廠商訂購商品，並且回覆「交期通知信」向您告知訂單交期以及完成對供貨廠商訂購作業。<br>備註：<br>如遇交貨期過長的情形，我們提供給您修改訂單的服務。您可以選擇取消交期過長的商品，我們會修改訂單並重新計算金額，請您依據修改後的「訂單內容」進行匯款。使用信用卡結帳付款會為您調整信用卡刷卡金額（信用卡分期付款無法修改金額，必須取消整筆訂單請您重新訂購交期正常之商品重新結帳）。<br>交期與付款通知信」、「交期通知信」中標示的交期為預估交期，是依照下訂商品「有庫存」的情況下，從對供貨廠商訂購完成到您收到商品的大約時間。<br></div>';
                    swal({
                        html: text
                    });
                    break;
                case "buy-detail":
                    text = '<div style="text-align:left;max-height: 400px;overflow:auto;">第1條　與顧客的聯絡方法<br> <br>1.關於購物方面的事項本公司認為有必要與您聯絡時，會使用您在本公司登錄的電子郵件信箱與您連絡，或是透過線上訊息的方式向您告知。此外，本公司要對不特定多數的會員做聯絡時會直接於網站上發表公告公布。您必須同意接受由本公司發到您電子郵件信箱的通知信或發表在網站上的通知文章。<br>2.發至您信箱的郵件，不論是登錄時輸入錯誤、郵件軟體或防毒軟體的環境設定問題、網路提供者的SPAM對策，使用免費信箱等等理由，而造成信件無法送達，本公司不付一切責任。<br>3.請確認您所登錄的信箱是可以正確收信的。另外，本公司發給您的重要通知信、或網站上發布的公告一經發出或公布，即視為您已有收到通知。<br><br>第2條　成立買賣契約<br> <br>1.訂單成立（買賣契約成立）<br> 您已在本公司網站下訂單的情況、商品的買賣契約成立時間為本公司向您發出[訂單確認]的通知信後。<br><br>2.取消訂單（解除買賣契約） <br>如為以下事由，不論買賣契約成立前後，都可能取消訂單。（契約成立後的契約解除）這種情況不論本公司有無通知，都視為您已同意。<br>■信用卡公司審核不通過的情況。<br>■我們確認您的個人資料有偽造的情況。<br>■下訂的商品在經過供貨廠商庫存貨確認後發現商品到貨日期未定或已停產無法交貨的情況。<br>■貨到付款送達不進行簽收付款、地址不明、收件地址長期無人收件、無法聯絡......等等無法完成交貨程序的情況，本公司會電話通知請盡速完成簽收動作，若惡意不完成交易者，隨即取消該會員資格，並委請律師進行後續處理。<br>■其他、特別是本公司認為必須取消訂單（解除買賣契約）的情況。<br>■選擇匯款轉帳方式付款未於「交期與付款通知信」所規定匯款期限完成匯款的情況。<br> <br>第3條　關於退換貨<br> <br>1.個人因素退貨<br>（1）.因個人因素要退換貨的情況，以收到商品後7日內提出申請為限。但是，有標示無法退換貨的商品、收到商品後超過7日的情況，不接受退換貨。不良品、商品錯誤的情況以商品到達後7日內提出申請為限。在收到商品後請盡速確認商品的狀況。<br>（2）.原則上商品一經使用就無法退換貨。<br>（3）.與商品一同包裝的物品，請保持收到商品時的原樣（外箱、標籤、保證書或說明書、配件、等等備品）。商品的備品不全的情況不接受退換貨。<br>（4）.不論良品/不良品/ ，已使用/未使用，若無事先寄送申請資料或是聯絡就將商品寄回，我們會再將商品寄送退還給您，不做退換貨的處理，運費也將由您自行負擔。<br>（5）.如有刻上您的姓名或特別訂單等等含有特別訂製的商品無法退換貨。<br>（6）.網站上的圖案與實物顏色有所出入，是因為各螢幕不同所顯現的顏色也不同，此狀況無法退貨。<br>（7）.手工訂製品、進口品上即使有塗裝不平均或擦傷、縫製缺陷也不能退換貨。<br>（8）.退貨的商品訂單當中如有加價購商品也需要一併退回，否則加價購商品與原價的價差我們將在給您的退款中扣除。<br>（9）.因個人因素要退換貨時，如為下列情況則不予以退換貨（不良品、商品錯誤例外）。<br>■有標示不可退換貨的商品。<br>■廠商特價商品。<br>■現貨特價的OUTLET出清商品。<br>■各車廠正廠零件。<br>■未登錄商品或各廠商官方網頁沒有刊登的商品。<br>■團購商品。<br>■已使用過的商品。<br>■包裝破損的商品。<br>■商品配件（包裝）遺失。<br>■衛生用品。<br>■書籍・ＤＶＤ等。<br>■只有套裝商品的一部分。<br>■福袋等特價品。<br>■抽獎商品。<br>■本公司已告知物品不可退貨、交換、取消等，並且您已同意購買的商品。<br>■其他本公司判斷已不能再販賣的物品。<br><br>2.不良品更換<br>（1）.我們只受理不良品更換良品。商品只能更換同一種商品尺寸或顏色等規格，不能更換不同商品。<br>（2）.關於商品有部分零件不足、損壞的情況，必須全部都做交換。<br>（3）.關於商品的更換，通常需要向廠商訂貨。如因到貨時間有延後、欲更換的商品缺貨而無法準備的情況下發生的損失，本公司不負擔任何責任。<br>（4）.您希望更換的商品我們會在收到您的申請後向廠商進行交期的確認。因可能有售完、停產的情況發生，如遇此情況，很抱歉我們只能為您辦理退貨並將您已經支付的款項退還給您（更換商品的交期確認完成後本公司將發送訊息通知您）。<br>（5）.商品是良品或不良品由廠商或本公司做判斷。<br> <br>3.退貨退款<br>（1）.退款手續在確認商品已到達本公司物流中心之後進行。因配合金融機關的營業日可能會有需要一星期左右的情況。<br>（2）.退款時，除了信用卡付款（訂單結帳使用信用卡付款以使用的信用卡做退款、如已經超過信用卡退款期限則使用銀行匯款方式退款）之外，皆使用銀行匯款方式退款，但是如有發生轉帳手續費的需求，退款時將從退款金額裡扣除。<br>（3）.不論是商品新品不良、破損退貨或是個人因素的退貨，運費皆由本公司負擔。<br>（4）.因商品的瑕疵及顧客使用商品時直接或間接造成顧客或第三者產生損失的損害賠償，我們僅負擔因產品更換或商品購買金額的退款，不負擔其餘的損害責任。<br>（5）.退貨、退款的商品為使用本公司點數下訂的商品時，原則上是退還使用的點數，不使用現金做退還。<br> <br>第4條　商品訂價<br> <br>1.本公司商品頁面所的標示價格即為商品銷售價格，價格會因為廠商調價、匯率變動、進口稅金改變而變動調整。訂購時以訂單成立時的價格為銷售價格，不因事後價格變動而有補償，折價等動作。<br>2.我們對刊登商品或價格都非常小心謹慎，萬一因人為的失誤等等，造成刊登價格與實際價格有誤差時，雖然對您非常抱歉，但是我們必須依照民法95條「錯誤」認定該刊登商品無效。這種情況，我們將用我們認為適當的方法與您做聯絡，告知正確價格，並確認您的購買意願，或取消後再重新訂購的方式進行處理。<br> <br>第5條　運費、手續費<br> <br>1.運費依照「訂單內容確認畫面（最後確認）」所表示的費用做收取。運費若無特別標示，或本公司無特別聯絡，均依照系統內所記載之運費（運費已包含營業稅）為準。但因商品狀況或寄送特殊地區等因素，在下訂後本公司有做運費變更的聯絡時，則以變更後的運費為準。<br>2.使用貨到付款服務時需另外支付貨到付款手續費（宅配公司收取）。另外銀行匯款手續費，須由顧客自行負擔。<br> <br>第6條　關於商品的交期<br> <br>1.本公司所刊登的交期計算是依照下訂商品的廠商有庫存的情況下，到本公司出貨為止的大約時間。預估交期會記載在交期通知信、交期查詢履歷或是在訂單動態及歷史履歷的訂單明細裡。<br>2.依據供貨廠商的庫存狀況與生產情形，商品的交期可能會有所變動。此外，如果因商品缺貨、停產或是廠商發生無法出貨.....等等無法交貨等情況，很抱歉對於無法供應的商品本公司將修改訂單取消訂單裡該項商品。<br> <br>第7條 電子發票開立<br> <br>1. 什麼是「電子發票」？<br>根據財政部訂定之「電子發票實施作業要點」，於「Webike-摩托百貨」消費開立之「二聯式電子發票」將不主動寄送，「Webike-摩托百貨」會將發票上傳至『財政部電子發票整合服務平台』與『綠界科技 ECPay電子發票平台』供會員查閱。相關資料請參考財政部電子發票整合服務平台 。<br><br>2. 選擇「電子發票」後，若中獎怎麼辦？可以把電子檔發票印出來拿去兌獎嗎？<br>依統一發票使用辦法規定，發票兌獎必須使用發票正本。帳戶內的電子發票樣張僅供參考，不具兌獎功能。「Webike-摩托百貨」每逢單月26日將根據財政部公佈之統一發票中獎號碼進行電腦對獎(已索取、已作廢發票除外)，若有中獎將以簡訊、e-mail或由專人電話通知，並以掛號郵遞方式把中獎發票郵寄給您。若您的電子發票使用通用載具(手機憑證載具、自然人憑證載具)，則將由財政部統一通知中獎人，或您可由超商之多媒體服務機(如:ibon)查詢、列印並兌獎。<br><br>3. 選擇「電子發票」後，若想要索取紙本發票怎麼辦？<br>根據財政部訂定之 「電子發票實施作業要點」，於「Webike-摩托百貨」消費開立之統一發票將不主動寄送，但需於期限內上傳至財政部電子發票整合服務平台留存，您可利用該網站查詢消費紀錄。或請至會員中心\線上客服諮詢 訂單及配送問題 留言提問，將有專人為您處理。<br><br>4. 如何選擇開立三聯式發票？(我的發票需抬頭統編)<br>請在結帳流程中的「發票資料」處，選擇三聯式發票並填寫統一編號及發票抬頭即可。電子發票及二聯式發票一經開立，恕無法換開為三聯式發票。<br><br>5. 發票可以指定日期、開立其它品名或金額嗎？<br>「Webike-摩托百貨」為正派經營且發票全面採用自動化系統開立，因此發票上商品名稱將依訂購當時品名和金額開立，請恕無法指定開立其它品名、金額，或指定開立日期。<br><br>6. 收到發票後，可否改開立為多張發票？<br>為配合電子請款結帳，故一次出貨對應一張發票，恕無法分別開立多張。<br><br>7. 因為報帳需要，能不能先開立發票給我？<br>您的訂單於付款完成、商品出貨當日即產生發票號碼，並以e-mail、客服訊息通知您。若商品尚未出貨，則無法先行開立發票。<br><br>8. 電子發票沒有顯示抬頭，是否可以加開抬頭？<br>配合財政部新版電子發票格式，電子發票證明聯無抬頭欄位，恕無法加開抬頭。<br> <br>第8條　免責事項<br> <br>1.您已下訂的商品如廠商無庫存而不能提供給您時，本公司除取消訂單以及退款之外不負擔一切責任。<br>2.本公司不能保證刊登在本網站上的價格標示、商品說明以及其他服務內容相關資料內容的正確性，完全性與有用性。<br>3.由於本公司產品多數為國外進口，商品資料是依據各供應廠商所提供之資料進行翻譯，但由於國情不同表達用語可能會有誤解的現象。本公司僅提供參考翻譯，商品實際規格依照原廠資料為準。<br>4.商品頁面關於商品名稱、圖片以及資料有可能作修改、變更或調整，會員在訂購商品時所依據的標準為廠商所提供的商品的編號或是取代編號，我們能保證會員所訂購的商品編號是正確的，其餘與商品頁面所示的內容如有差異我們無法保證。若會員有任合關於商品方面的疑問請於商品頁面選擇「商品諮詢」功能進行詢問或洽詢「Webike-摩托百貨」各地區經銷商，或是使用電子郵件service@webike.tw與我們聯繫。<br>5.未成年者的商品購買申請，視為在監護人的監護下進行。未滿二十歲之未成年人，應由家長（或監護人）陪同閱讀、瞭解並同意本服務條款之所有內容（含未來之修改變更）後，方得繼續登陸註冊及使用本服務。當會員使用或繼續使用本服務時，即推定其家長（或監護人）已閱讀、瞭解並同意接受本規約之當時所有內容及其後之修訂變更。<br>6.本公司不負責因天災等等不歸屬本公司責任的事由造成的損害。<br>7.本條約為規範中華民國台灣國內寄送的訂單所訂立的條約。寄送到國外的情況則不適用。<br></div>';
                    swal({
                        html: text
                    });
                    break;
            }
        });

    </script>
@endsection