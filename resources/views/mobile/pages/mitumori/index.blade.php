@extends('mobile.layouts.mobile')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ assetRemote('mobile/css/pages/mitumori-index.css') }}">
@stop
@section('middle')
<div class="mitumori-container">
	<div>
		<h1 class="common">未登入商品查詢購買系統</h1>
	</div>
	<div class="mitumori-container-block">
		<a class="banner-advertisement" href="javascript:void(0)"><img src="{{ assetRemote('image/banner/banner-mitumori.png') }}" alt="banner"></a>
	</div>
	<div class="mitumori-container-box">
		<div>
			<h3 class="common">未登入商品查詢注意事項</h3>
		</div>
		<div class="mitumori-container-block">
			<span class="description-text">此查詢功能提供「Webike摩托百貨」網站上未登錄之商品，查詢報價及交期。</span>
	         <div class="link_ui">
	          	<a class="link_ui_a toggle clearfix"> 
	            	查詢注意事項：
	          	</a>
	          	<div class="mitumori-description">
		            <p>
		            	1.我們大約需要一至三個工作天，請耐心等候回復。
		            </p>
		            <p>
		            	2.報價均為新台幣含稅、含運報價，報價有效期限7天(到期後查詢會自動取消，請再次查詢)。
		            </p> 
		            <p>
		            	3.如您能提供該正確的商品編號、商品網址…等資訊，將加快我們查詢的時間。
		            </p>
		            <p>
		            	4.報價回覆後請到：會員中心>>未登錄商品查詢履歷裡面查看商品資訊
		            </p>
		            <p>
		            	5.未登錄之商品為「Webike摩托百貨」代為購買，一但下訂後無法取消訂單，因此下訂購買前請審慎考慮!!
		            </p>          
	          	</div>
	         </div>
          	<div class="link_ui">
	           <a class="link_ui_a toggle clearfix">
	           		其他注意事項
	           </a> 
	           <div class="mitumori-description">
		           <p>
		           		1.正廠零件請利用"<a href="{{route('genuineparts')}}" title="正廠零件查詢系統{{$tail}}" target="_blank">正廠零件查詢系統</a>"查詢（HRC、競技零件、選配配件、工具除外）。
		           </p>
		           <p>
		           		2.當商品有年份、車身顏色…等區分時，我們會再請您提供車身編號、引擎號碼或是車輛的色碼編號，國產車型可查看您的"<a href="{{route('motor-licenses-info')}}" target="_blank">行照中顯示的機號</a>"。
		           </p>
		           <p>
		           		3.關於未登錄商品查詢相關問題，請至<a href="{{route('customer-service-proposal-create', 'service')}}" title="未登錄商品問題提問{{$tail}}" target="_blank">未登錄商品問題提問</a>，或詢問<a href="{{DEALER_URL}}" title="Webike-實體經銷商{{$tail}}" target="_blank">Webike-實體經銷商</a>。
		           </p>
	           </div>
          	</div>
		</div>
	</div>
	<div class="mitumori-container-box pages-action active" id="page-1">
		<div>
			<h3 class="common">輸入未登入商品資訊</h3>
		</div>
		<form class="mitumori-form simple-testing" method="POST" action="{!! URL::route('mitumori-estimate') !!}">
            <div class="mitumori-container-block form-block">
                <div class=" mitumori-form-block-item st-row">
                    <div class="mitumori-number">
                    	<span>商品</span>
                        <h1>1</h1>
                    </div>
                    <ul class="ul-mitumori-form-item-row">
                        <li>
                            <div>
                                <label class=" font-color-red">商品品牌</label>
                                <div><input class="st-require-text width-full" type="text" name="syouhin_maker[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label class=" font-color-red">商品名稱</label>
                                <div><input class="st-require-text width-full" type="text" name="syouhin_name[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label>商品編號（建議）</label>
                                <div><input class="width-full" type="text" name="syouhin_code[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label>形式(顏色/尺寸)</label>
                                <div><input class="width-full" type="text" name="syouhin_type[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label>車輛製造商</label>
                                <div><input class="width-full" type="text" name="syouhin_taiou_maker[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label>車型/出廠年份</label>
                                <div><input class="width-full" type="text" name="syouhin_taiou_syasyu[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label class=" font-color-red">數量(個)</label>
                                <div><input class="st-require-number width-full" type="text" name="syouhin_kosuu[]"></div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <label>備註說明(請貼商品網址或其他資訊)</label>
                                <div>
                                    <textarea class="width-full" type="text" rows="3" name="syouhin_bikou[]"></textarea>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="submit-form mitumori-container-block">
                <div>
                    <button type="button" class="btn btn-default border-radius-2 base-btn-gray btn-submit-form-mitumori" onclick="addRow();">追加輸入欄</button>
                    <button type="button" class="btn btn-danger border-radius-2 btn-submit-form-mitumori" onclick="goNextStep();">送出查詢</button>
                </div>
            </div>
        </form>
	</div>
	<div class="pages-action mitumori-container-box" id="page-2">
		<div>
			<h3 class="common">商品確認</h3>
		</div>
        <ul class="table-main-info mitumori-container-block">
            <li class="table-main-title visible-md visible-lg">
                <ul>
                    <li><span class="size-10rem">&nbsp;</span></li>
                    <li><span class="size-10rem">商品品牌</span></li>
                    <li><span class="size-10rem">商品名稱</span></li>
                    <li><span class="size-10rem">商品編號</span></li>
                    <li><span class="size-10rem">車輛製造商</span></li>
                    <li><span class="size-10rem">形式</span></li>
                    <li><span class="size-10rem">年分</span></li>
                    <li><span class="size-10rem">數量</span></li>
                </ul>
            </li>
            <li class="table-main-content" id="mitumori-preview-content">
            </li>
        </ul>
        <div class="center-box mitumori-container-block">
            <a class="btn btn-default border-radius-2 base-btn-gray btn-send-genuinepart-finish" onclick="goPrevPage();">返回修正</a>
            <a class="btn btn-danger border-radius-2 btn-send-genuinepart-finish" onclick="doSubmit();">確認送出</a>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript" src="{{assetRemote('js/pages/history/table-response.js')}}"></script>
    <script type="text/javascript" src="{!! assetRemote('js/pages/mitumori/mitumori.js') !!}"></script>
    <script type="text/javascript">
        function goNextStep(){
            var result = simpleTesting();
            if(result.success){
                var confirm_table = '';
                var row_count = 0;
                $(mitumori_preview_content).html('');
                $(mitumori_form + ' .mitumori-form-block-item').each(function(ul_key, ul){
                    var tds = '';
                    var null_count = 0;
                    var has_count = false;
                    $(ul).find('input').each(function(input_key, input){
                        if(!has_count){
                            tds += '<li><h3>商品' + (row_count + 1) + '</h3></li>';
                            has_count = true;
                        }
                        if(!$(input).val()){
                            null_count++;
                        }
                        if($(input).attr('name').indexOf('syouhin_taiou_syasyu') >= 0){
                            width_num = 1;
                            width_sm_num = 12;
                        }else if($(input).attr('name').indexOf('syouhin_kosuu') >= 0){
                            width_num = 1;
                            width_sm_num = 12;
                        }else if($(input).attr('name').indexOf('syouhin_taiou_maker') >= 0){
                            width_num = 2;
                            width_sm_num = 12;
                        }else if($(input).attr('name').indexOf('syouhin_bikou') >= 0){
                            return true;
                        }
                        tds += '<li><span>' + $(input).val() + '</span></li>';
                    });
                    if(tds.length > 0 && null_count != $(ul).find('input').length ){
                        row_count++;
                        confirm_table += '<ul class="table-main-item col-sm-block col-xs-block clearfix">' + tds + '</ul>';
                    }
                });
                $(mitumori_preview_content).append(confirm_table);
                reSizeTableResponsive();
                goNextPage();
            }else {
                swal({
                    title: '錯誤',
                    html: "<h2>紅色項目為必填，且商品數量不可為0。<br>請再次確認</h2>",
                    type: 'error',
                }).then(function () {
                    slipTo(result.error_row);
                });
            }
        }

        function doSubmit(){
            var result = simpleTesting();
            if(result.success){
                swal({
                    title: '您確定要送出嗎?',
                    text: "點選「確認送出」後將送出查詢",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '確認送出',
                    cancelButtonText: '取消',
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#777',
                }).then(function () {
                    $(mitumori_form).submit();
                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        return false;
                    }
                });
            }else{
                swal({
                    title: '錯誤',
                    html: "<h2>紅色項目為必填，且數量不可為0。<br>請再次確認</h2>",
                    type: 'error',
                }).then(function () {
                    goPrevPage();
                });
            }
        }
    </script>
@stop