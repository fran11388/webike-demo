@extends('mobile.layouts.mobile-amp')
@section('head')
<script type="application/ld+json">
	{
        "@context": "http://schema.org",
          "@type" : "WebSite",
          "mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
          "name" : "「Webike-摩托百貨」",
          "headline": "{{$seo_title}}",
          "datePublished": "2019-05-03T16:15:41Z",
            "dateModified": "2019-05-03T16:15:41Z",
            "description": "{{$seo_description}}",
        "author": {
          "@type": "Person",
          "name": "Jordan M Adler"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Google",
          "logo": {
            "@type": "ImageObject",
            "url": "http://cdn.ampproject.org/logo.jpg",
            "width": 600,
            "height": 60
          }
        },
        "image": {
          "@type": "ImageObject",
          "url": "http://cdn.ampproject.org/leader.jpg",
          "height": 2000,
          "width": 800
        }
      }
</script>
@stop
@section('script')
	<link rel="canonical" href="{{config('app.url')}}/{{$url_path}}">
  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
@stop
@section('style')
      <?php include "mobile/css/pages/amp/partials/content-tabs.css";?>
      <?php include "mobile/css/pages/amp/partials/analysis.css";?>
@stop
@section('middle')
<?php
  $info = preg_replace("#<style(.*?)>(.*?)</style>#is", '', $manufacturer_info->content);
  $content =  strip_tags($info);
  $content = nl2br($content);
?>
    @include('mobile.pages.amp.motor.partials.content-tabs')
    @include('mobile.pages.amp.summary.partials.top-info')
    <div class="section block">
      <div class="box-content-brand-top-page exp">
          <div class="ct-right-search-list col-xs-12 col-sm-12 col-md-12 col-lg-12">
              {!! $content !!}
          </div>
      </div>
    </div>
@stop
