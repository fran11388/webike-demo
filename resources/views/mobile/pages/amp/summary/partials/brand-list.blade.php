@if (!$current_manufacturer)
    <div class="section">
        <div class="model_search common-motor-search">
            <h3 class="common">{{ $summary_title }} 商品品牌</h3>
            <div class="form_ui box_ui">
                @php
                    $manufacturer_max_count = 5;
                @endphp
                <select on="change:AMP.navigateTo(url=event.value)">
                    <option value="" selected>請選擇品牌</option>
                    @foreach ($manufacturer as $key => $node)
                        <option value="{{modifySummaryUrl(['br'=> $node->url_rewrite ])}}">{{ $node->name }}<span class="count">({{ $node->count }})</span></option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
@endif