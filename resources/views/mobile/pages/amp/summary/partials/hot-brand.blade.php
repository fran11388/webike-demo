@if (!isset($current_manufacturer) or !$current_manufacturer)
    <div class="section">
        <div>
            <h3 class="common">{{ $summary_title }} 熱門品牌</h3>
            <div class="ct-new-custom-part-on-sale-top clearfix">
                <ul class="ul-ct-famous-brand product-grid" >
                    <amp-carousel width="200" height="80" layout="responsive" type="slides" controls loop>
                        @foreach ($hot_manufacturers as $key => $node)
                            <li>
                                <a href="{{modifySummaryUrl(['br'=> $node->url_rewrite ])}}">
                                    <figure class="zoom-image">
                                            <amp-img src="{!! $node->image!!}" alt="{!! $node->name . '('  . $node->count . ')' !!}" width="200" height="150" layout="responsive"></amp-img>
                                    </figure>
                                </a>
                            </li>
                            @if($loop->iteration == 40)
                                @break
                            @endif
                        @endforeach
                    </amp-carousel>
                </ul>
            </div>
        </div>
        <div class="text-center block">
            <a class="btn btn-warning" href="{{route('brand',request()->only(['category','motor']) )}}" title="{{ $summary_title . ' 全部熱門品牌' . $tail }}">查看全部</a>
        </div>
    </div>
@endif