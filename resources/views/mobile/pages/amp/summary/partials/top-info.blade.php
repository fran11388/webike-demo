@if($current_category and !$current_motor and !$current_manufacturer)

    <div class="category-content">
        <h1 class="common">{{ $summary_title }}</h1>
    </div>
@else
    @if($current_motor)
        <div class="section">
            <h3 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h3>
            <div class="mybike_log">
                @include('mobile.pages.amp.motor.partials.info')
            </div>
        </div>
    @else
        <div class="section brand-introduce">
            @include('mobile.pages.amp.summary.partials.manufacturer-info')
        </div>
    @endif
@endif
