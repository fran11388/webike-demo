<div class="exp clearfix">
    <div class="clearfix">
        <div class="brand-image">
            <amp-img src="{{ cdnTransform(str_replace('http:','',$current_manufacturer->image)) }}" alt="{{ $current_manufacturer->name }}" width="150" height="150"></amp-img>
        </div>
        <div class="brand-office">
            @if($current_manufacturer->website)
                <div class="ct-title-main2-right box">
                    <a class="btn btn-warning" href="{{ $current_manufacturer->website }}" title="{{ $current_manufacturer->name . '官方網站' . $tail }}" target="_blank">官方網站</a>
                </div>
            @endif
            <p>{!! $current_manufacturer->description !!}</p>
        </div>
    </div>
</div>