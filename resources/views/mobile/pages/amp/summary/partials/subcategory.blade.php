<?php
$category_page = false;
?>
@if($current_category and !$current_motor and !$current_manufacturer)
    <?php
            $category_page = true;
    ?>
@endif
<div class="section">
    <div class="link_ui">
        <h3 class="common">
            {{ isset($current_name) ? $current_name : $summary_title }}  商品分類
        </h3>
        @php
            $child_flg = false;
        @endphp
        @if (isset($current_category) and $current_category )
            <?php
            $url_path = explode('-', $current_category->mptt->url_path);
            $target = $tree->where('url_rewrite', $url_path[0])->first();
            if($target){
                for ($i = 1; $i < count($url_path); $i++) {
                    $target = $target->nodes->where('url_rewrite', $url_path[$i])->first();
                }
                $parent = $target;
                if($target){
                    $target = $target->nodes->reject(function ($node) {
                        return $node->count == 0;
                    });
                }else{
                    $target = collect();
                }
            }else{
                $target = collect();
            }
            ?>
        @else
            <?php  $target = $tree; ?>
        @endif
        <ul>
            @php
                $all = $target;
                $target = $target->filter(function($category,$key){
                    return count($category->nodes) != 0;
                });
                foreach($all as $key => $node){
                    if(count($node->nodes) == 0){
                        $target = $target->push($node);
                    }
                }
            @endphp
            @foreach ($target as $key => $node)
                @if ($node->count)
                    <?php
                    $expand = false;
                    if (isset($current_category) and $current_category)
                        $expand = ($node->id == $current_category->id) ? true : false;
                    elseif ($loop->first)
                        $expand = true;
                    ?>
                    <li>
                        <?php
                        $args = [];
                        $args['ca'] = $node->mptt->url_path;
                        if (isset($current_manufacturer)){
                            $args['br'] = $current_manufacturer->url_rewrite;
                        }
                        ?>

                        @if(count($node->nodes))
                            <a href="javascript:void(0);" class="{{ count($node->nodes) ? 'toggle' : ''}}">{{ $node->name }} <!-- ({{ $node->count }}) -->{!! lazyImage( getCategoryImage($node) , $node->name ) !!}</a>
                            @php
                                $child_flg = true;
                            @endphp
                            <ul>
                                @foreach ($node->nodes as $key=> $node)
                                    @if ($node->count)
                                        <li>
                                            <?php
                                            $args = [];
                                            $args['ca'] = $node->mptt->url_path;
                                            if (isset($current_manufacturer) and $current_manufacturer){
                                                $args['br'] = $current_manufacturer->url_rewrite;
                                            }
                                            ?>
                                            <a href="{{modifySummaryUrl($args)}}" title="{!! $node->name !!}">
                                                <span class="size-08rem">{{ $node->name }} ({{ $node->count }})</span>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @else
                            <a href="{{modifySummaryUrl($args)}}" class="{{ count($node->nodes) ? 'toggle' : ''}}">{{ $node->name }} <!-- ({{ $node->count }}) -->{!! lazyImage( getCategoryImage($node) , $node->name ) !!}</a>
                        @endif
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
    <div class="text-center block">
        <a class="btn btn-warning" href="{{ modifySummaryUrl(null,true) }}" title="{{ (isset($current_name) ? $current_name : $summary_title) . ' 全部分類商品' . $tail }}">查看全部</a>
    </div>
</div>