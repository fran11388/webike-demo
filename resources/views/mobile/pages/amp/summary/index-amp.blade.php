@extends('mobile.layouts.mobile-amp')
@section('head')
<script type="application/ld+json">
	{
        "@context": "http://schema.org",
          "@type" : "WebSite",
          "mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
          "name" : "「Webike-摩托百貨」",
          "headline": "{{$seo_title}}",
          "datePublished": "2019-05-03T16:15:41Z",
            "dateModified": "2019-05-03T16:15:41Z",
            "description": "{{$seo_description}}",
        "author": {
          "@type": "Person",
          "name": "Jordan M Adler"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Google",
          "logo": {
            "@type": "ImageObject",
            "url": "http://cdn.ampproject.org/logo.jpg",
            "width": 600,
            "height": 60
          }
        },
        "image": {
          "@type": "ImageObject",
          "url": "http://cdn.ampproject.org/leader.jpg",
          "height": 2000,
          "width": 800
        }
      }
</script>
@stop
@section('script')
	<link rel="canonical" href="{{config('app.url')}}/{{$url_path}}">
  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
@stop
@section('style')
    	<?php include "mobile/css/pages/amp/summary.css";?>
      <?php include "mobile/css/pages/amp/partials/content-tabs.css";?>
@stop
@section('middle')
    @if(isset($current_motor) and $current_motor)
        <h1 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h1>
    @elseif(isset($current_manufacturer) and $current_manufacturer)
        <h1 class="common">{{ $current_manufacturer->name }}</h1>
    @endif
    @include('mobile.pages.amp.motor.partials.content-tabs')
    @include('mobile.pages.amp.summary.partials.top-info')    
    @include('mobile.pages.amp.motor.partials.subcategory')
    @include('mobile.pages.amp.summary.partials.new-lineup-ajax')

    @include('mobile.pages.amp.summary.partials.hot-brand')

    @include('mobile.pages.amp.summary.partials.ranking')

@stop
