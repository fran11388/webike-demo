<?php $href = route('review-show', $review->id) ?>
<li class=" item item-product-grid item-product-custom-part-on-sale">
    <div>
        <div>
            <a href="{{ $href }}" class="font-color-blue">
                <figure class="zoom-image thumb-box-border">
                    <?php
                    $src = '';
                    if (starts_with($review->photo_key, 'http'))
                        $src = $review->photo_key;
                    else
                        $src = $review->photo_key ? 'http://img.webike.tw/review/' . $review->photo_key : $review->product_thumbnail;
                    ?>
                    <amp-img src="{{ str_replace('http:','',$src) }}" alt="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}" width="200" height="150"></amp-img>
                </figure>
            </a>
        </div>
        <div class="item-product-grid-pars-on-sale-right text-center">
            <span class="dotted-text1">{{ $review->product_manufacturer }}</span>
            <a href="{{ $href }}" class="title-product-item-top dotted-text2 font-color-blue" title="{{ '【' . $review->product_manufacturer . '】' . $review->product_name . '商品評論' }}">
                <span class="dotted-text2">{{ $review->product_name }}</span>
            </a>
            <span class="icon-star-bigger star-0{{ $review->ranking }}"></span>
        </div>
    </div>
    <div class="new-review-content text-left">
        <a href="{{ $href }}" class="title-product-item dotted-text2 font-color-blue">{{ $review->title }}</a>
        <label class="normal-text dotted-text2">{!! mb_substr(e($review->content),0,35,"utf-8") !!}
          <a href="{{ $href }}" class="btn-customer-review-read-more font-color-blue">...More</a>
        </label>
    </div>
</li>