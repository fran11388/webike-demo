@extends('mobile.layouts.mobile-amp')
@section('head')
<script type="application/ld+json">
	{
        "@context": "http://schema.org",
          "@type" : "WebSite",
          "mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
          "name" : "「Webike-摩托百貨」",
          "headline": "{{$seo_title}}",
          "datePublished": "2019-05-03T16:15:41Z",
            "dateModified": "2019-05-03T16:15:41Z",
            "description": "{{$seo_description}}",
        "author": {
          "@type": "Person",
          "name": "Jordan M Adler"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Google",
          "logo": {
            "@type": "ImageObject",
            "url": "http://cdn.ampproject.org/logo.jpg",
            "width": 600,
            "height": 60
          }
        },
        "image": {
          "@type": "ImageObject",
          "url": "http://cdn.ampproject.org/leader.jpg",
          "height": 2000,
          "width": 800
        }
      }
</script>
@stop
@section('script')
	<link rel="canonical" href="{{config('app.url')}}/{{$url_path}}">
  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
@stop
@section('style')
    	<?php include "mobile/css/pages/amp/motor-review.css";?>
      <?php include "mobile/css/pages/amp/partials/content-tabs.css";?>
      <?php include "mobile/css/pages/amp/partials/analysis.css";?>
@stop
@section('middle')

<h1 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h1>
  @include('mobile.pages.amp.motor.partials.content-tabs')
  <div class="section">
    <h3 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h3>
    <div class="mybike_log">
        @include('mobile.pages.amp.motor.partials.info')
        @include('mobile.pages.motor.partials.analysis')
    </div>
  </div>
  <div class="section">
    <div>
      <h3 class="common">商品評論</h3>
    </div>
    <div class="new-review exp">
      <ul class="product-list"> 
        <amp-carousel width="300" height="350" layout="responsive" type="slides" controls loop>
          @foreach ($reviews as $review)
            @include('mobile.pages.amp.review.review-product')
          @endforeach
        </amp-carousel>
       </ul>
      </div>
    </div>

@stop
