@extends('mobile.layouts.mobile-amp')
@section('head')
<script type="application/ld+json">
	{
        "@context": "http://schema.org",
          "@type" : "WebSite",
          "mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
          "name" : "「Webike-摩托百貨」",
          "headline": "{{$seo_title}}",
          "datePublished": "2019-05-03T16:15:41Z",
            "dateModified": "2019-05-03T16:15:41Z",
            "description": "{{$seo_description}}",
        "author": {
          "@type": "Person",
          "name": "Jordan M Adler"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Google",
          "logo": {
            "@type": "ImageObject",
            "url": "http://cdn.ampproject.org/logo.jpg",
            "width": 600,
            "height": 60
          }
        },
        "image": {
          "@type": "ImageObject",
          "url": "http://cdn.ampproject.org/leader.jpg",
          "height": 2000,
          "width": 800
        }
      }
</script>
@stop
@section('script')
	<link rel="canonical" href="{{config('app.url')}}/{{$url_path}}">
  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
@stop
@section('style')
    	<?php include "mobile/css/pages/amp/motor-top.css";?>
      <?php include "mobile/css/pages/amp/motor-review.css";?>
      <?php include "mobile/css/pages/amp/partials/content-tabs.css";?>
      <?php include "mobile/css/pages/amp/partials/analysis.css";?>
@stop
@section('middle')
<div>
    <h1 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h1>
    @include('mobile.pages.amp.motor.partials.content-tabs')
    <div class="section block">
        <h3 class="common">{{ isset($current_name) ? $current_name : $summary_title }}</h3>
        <div class="mybike_log">
            @include('mobile.pages.amp.motor.partials.info')
            @include('mobile.pages.motor.partials.analysis')
        </div>
    </div>
    <div class="container-block">
        @if(count($news))
            <div class="section block">
                <h3 class="common">{{isset($current_name) ? $current_name : $summary_title}} 最新消息</h3>
                @include('mobile.pages.amp.motor.partials.news')
            </div>
        @endif
       <div class="section block">
            <h3 class="common"> {{$current_name}} 商品搜尋 </h3>
            <div class="price_info exp">
              <form action="{{ \URL::route('parts',['section' => "mt/$current_motor->url_rewrite"]) }}" method="get" target="_blank">
                <div><input id="motor_search_text" type="text" name="q" value="" class="btn btn-default btn-full" placeholder="請輸入商品關鍵字..."></div>
                <div><button id="motor_search" class="btn btn-danger btn-full box-top" type="submit">搜尋</button></div>
                <input type="hidden" name="mt" value="{{(isset($current_motor) and count($current_motor))? $current_motor->url_rewrite : '' }}">
              </form>
            </div>
        </div>
        @if(count($new_products))
            <div class="section block">
                <div class="scroll_ui">
                    <h3 class="common"> {{ $summary_title }} 新商品</h3>
                    <div>
                        <ul class="product-list exp">
                          <amp-carousel width="300" height="200" layout="responsive" type="slides" controls loop>
                            @foreach ($new_products->chunk(50) as $chunks)
                                @foreach ($chunks as $product)
                                    <li>
                                        @include('mobile.pages.amp.motor.partials.newproducts')
                                    </li>
                                @endforeach
                            @endforeach
                          </amp-carousel>
                        </ul>
                        <div class="text-center more-bolck">
                            <a href="{{ modifySummaryUrl(null,true) }}?sort=new" class="btn btn-warning" title="{{ $summary_title . ' 全部新商品' . $tail }}">查看全部</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count($reviews))
          <div class="section block">
              <h3 class="common">{{ $summary_title }} 商品評論　</h3>
              <div class="new-review exp">
                <ul class="product-list"> 
                  <amp-carousel width="300" height="350" layout="responsive" type="slides" controls loop>
                    @foreach ($reviews as $review)
                      @include('mobile.pages.amp.review.review-product')
                    @endforeach
                  </amp-carousel>
                 </ul>
                <div class="text-center more-bolck">
                    @php
                        $replace_segments = null;
                        if(isset($current_manufacturer)){
                            $replace_segments = ['br' => $current_manufacturer->name];
                        }
                    @endphp
                    <a href="{{modifyReviewSearchUrlSummary($replace_segments)}}" class="btn btn-warning" title="{{ $summary_title . ' 全部商品評論' . $tail }}">更多評論</a>
                </div>
              </div>
          </div>
        @endif
        @if(isset($newmotors) and count($newmotors))
            <div class="section block">
                <h3 class="common"> {{isset($current_name) ? $current_name : $summary_title}} 新車・中古車情報 </h3>
                <div class="newmotors exp text-center">
                    <div class="ct-new-magazine">
                        <ul>
                            <amp-carousel width="300" height="250" layout="responsive" type="slides" controls loop>
                              @foreach($newmotors as $motor_num => $new_motor)
                                  <li class="product-grid">
                                      @include('mobile.pages.amp.motor.partials.newmotors',['motor' => $new_motor, 'motor_num' => $motor_num])
                                  </li>
                              @endforeach
                            </amp-carousel>
                        </ul>
                    </div>
                    <div class="text-center more-bolck">
                        <a href="http://www.webike.tw/motomarket/brand/{{$newmotors->first()->manufacurer}}/motor/{{$newmotors->first()->motor_model_id}}" class="btn btn-warning" target="_blank">查看全部</a>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

@stop
