@extends('mobile.layouts.mobile-amp')
@section('head')
<script type="application/ld+json">
      {
        "@context": "http://schema.org",
          "@type" : "WebSite",
          "mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
          "name" : "「Webike-摩托百貨」",
          "headline": "【{{$current_name}}規格總覽】 - 各年分詳細車輛規格、配備及圖鑑",
          "datePublished": "2018-09-10T13:15:41Z",
            "dateModified": "2018-09-10T13:15:41Z",
            "description": "{{$current_name}}車輛圖鑑、基本規格、維修資訊、車體、懸吊、配備、引擎、其他詳細車輛規格、商品分類、對應零件快速搜尋。",
        "author": {
          "@type": "Person",
          "name": "Jordan M Adler"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Google",
          "logo": {
            "@type": "ImageObject",
            "url": "http://cdn.ampproject.org/logo.jpg",
            "width": 600,
            "height": 60
          }
        },
        "image": {
          "@type": "ImageObject",
          "url": "http://cdn.ampproject.org/leader.jpg",
          "height": 2000,
          "width": 800
        }
      }
</script>
@stop
@section('script')
<link rel="canonical" href="{{config('app.url')}}/{{$url_path}}">
  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>

@stop
@section('style')
<?php include "mobile/css/pages/amp/partials/content-tabs.css";?>

.ct-img-slider-nav2{
    width: 100%;
    background-color: #e4e8eb;
}
.section-block{
	margin-bottom:30px;
	padding: 0 10px;
}
.carousel-preview .amp-carousel-slide{
    margin: 2%;
	padding: 0;
}
.summary-subcategory .block{
	margin-bottom: 0px;
}
.table{
	margin: 10px 0px;
}
.carousel-preview button{
	width: 21%;
	-webkit-appearance: none;
	border: none;
}
#motor_search_text{
	-webkit-user-select: text ;
}
#main{
	border-bottom: 30px solid #eee;
}
.tab-pane{
	padding: 0px 10px;
}

@media(max-width: 350px){
  	.carousel-preview{
  		height: 55px;
  	}
}
@media(min-width:400px){
	.carousel-preview{
		height: 72px;
	}
}
@stop
@section('middle')
<div>
	@if($catalogue)
		<h1 class="common">{{$current_name}}規格總覽</h1>
		@include('mobile.pages.amp.motor.partials.content-tabs')
		<div class="section">
			<h3 class="common">{{$current_name}}車輛圖鑑</h3>
			@if(count($current_images))
				<div class="clearfix section-block">
	                <div class="thumb-box-border">
	                	<amp-carousel id="carousel-with-preview" width="450" height="300" layout="responsive" type="slides" on="slideChange:AMP.setState({selected: {slide: event.index}})">
		                    @foreach($current_images as $current_image)
		                        
	                        	<amp-img src="{!! $current_image !!}" alt="{{$current_name}}" width="450" height="300" layout="responsive"></amp-img>
		                        
		                    @endforeach
		                </amp-carousel>

	                </div>
	                <div class="ct-img-slider-nav2 thumb-box-border carousel-preview">
	                    <amp-carousel width="auto" height="65" layout="fixed-height" type="carousel" >
	                        @foreach($current_images as $key => $current_image)
	                            <button class="img-thumbnail img-thumbnail-detail"  [class]="selected.slide == {{$key}} ? 'active' : ''"  on="tap:carousel-with-preview.goToSlide(index={{$key}})">
	                            	<amp-img src="{!! $current_image !!}" alt="{{$current_name}}" width="70" height="55" layout="responsive" heights="(min-width:500px) 100px, 70%">
	                            	</amp-img>
	                            </button>
	                        @endforeach
	                    </amp-carousel>
	                </div>
	             </div>
	        @else
	            <div class="img-slider-for2 section-block">
	                <amp-img src="{{ NO_IMAGE }}" alt="{{$current_name}}"  width="450" height="300" layout="responsive"></amp-img>
	            </div>
	        @endif
		</div>
		<div class="section">
			<h3 class="common">{{$current_name}}基本規格</h3>
			<div class="section-block">
				<?php
					$groups = $catalogue->groups->pluck('attribute_group_display','attribute_code');
					$strings = $catalogue->strings->pluck('attribute_string_values','attribute_code');
					$flags = $catalogue->flags->pluck('attribute_flag_values','attribute_code');
					$floats = $catalogue->floats->pluck('attribute_float_display','attribute_code');
					$integers = $catalogue->integers->pluck('attribute_integer_display','attribute_code');
				?>
				<table class="table table-bordered">
					<tbody>
	                    <tr>
	                        <th class="active">廠牌</th>
	                        <td>{{ $current_motor->manufacturer->name }}</td>
	                        <th class="active">引擎形式</th>
	                        <td>{{ $catalogue->model_engin_data }}</td>
	                    </tr>
	                    <tr>
	                        <th class="active">車型名稱</th>
	                        <td>{{$current_motor->name}}</td>
	                        <th class="active">引擎啟動方式</th>
	                        <td>{{ $groups->get(13, ' - ') }}</td>
	                    </tr>
	                    <tr>
	                        <th class="active">款式・種類</th>
	                        <td>{{$catalogue->model_grade}}</td>
	                        <th class="active">最高馬力</th>
	                        <td>{{ $floats->get(16, ' - ') }}</td>
	                    </tr>
	                    <tr>
	                        <th class="active">動力方式</th>
	                        <td>-</td>
	                        <th class="active">最大扭力</th>
	                        <td>{{ $floats->get(17, ' - ') }}</td>
	                    </tr>
	                    <tr>
	                        <th class="active">型式</th>
	                        <td>{{ $catalogue->model_katashiki }}</td>
	                        <th class="active">車體重量(乾燥重量)</th>
	                        <td>{{ $floats->get(24, ' - ') }}(概算値)kg</td>
	                    </tr>
	                    <tr>
	                        <th class="active">排氣量</th>
	                        <td>{{ $catalogue->model_displacement }}cc</td>
	                        <th class="active">車體重量(裝備重量)</th>
	                        <td>{{ $floats->get(25, ' - ') }}kg</td>
	                    </tr>
	                    <tr>
	                        <th class="active">開始銷售年分</th>
	                        <td>{{ $catalogue->model_release_year }}年</td>
	                        <th class="active">馬力重量比</th>
	                        <td>[{{ $floats->get(24, ' - ') }} / {{ $floats->get(16, ' - ') }}] kg/PS</td>
	                    </tr>
	                    <tr>
	                        <th class="active">能源消耗值</th>
	                        <td>{{ $floats->get(14, ' - ') }}</td>
	                        <th class="active">全長・全高・全寬</th>
	                        <td>{{ $integers->get(28, ' - ') }}mm
	                            × {{ $integers->get(26, ' - ') }}mm
	                            × {{ $integers->get(27, ' - ') }}mm</td>
	                    </tr>
	                    <tr>
	                        <th class="active">油箱容量</th>
	                        <td>{{ $floats->get(20, ' - ') }}公升</td>
	                        <th class="active">座高</th>
	                        <td>{{ $integers->get(22, ' - ') }}mm</td>
	                    </tr>
	                    <tr>
	                        <th class="active">行駛距離</th>
	                        <td>[{{ $floats->get(14, ' - ') }} * {{ $floats->get(20, ' - ') }}]km(概算値)</td>
	                        <th class="active">前輪尺寸</th>
	                        <td>
	                            {{ $strings->get(33, ' - ') }}
	                        </td>
	                    </tr>
	                    <tr>
	                        <th class="active">燃料給油方式</th>
	                        <td>{{ $groups->get(9) ,' - '}}</td>
	                        <th class="active">後輪尺寸</th>
	                        <td>
	                            {{ $strings->get(42, ' - ') }}
	                        </td>
	                    </tr>
	                </tbody>
	        	</table>
			</div>
		</div>
		<div class="section">
			<div>
	                <h3 class="common">{{$current_name}}維修資訊</h3>
	        </div>
	        <div class="section-block">
		        <table class="table table-bordered">
		            <tbody>
			            <tr>
			                <th class="active">標準裝備火星塞</th>
			                <td>{{ $strings->get(69, ' - ') }}</td>
			                <th class="active">齒盤規格</th>
			                <td>傳動(前) > {{ $integers->get(62, ' - ') }}T<br/>傳動(後) > {{ $integers->get(63, ' - ') }}T</td>
			            </tr>
			            <tr>
			                <th class="active">火星塞使用數</th>
			                <td>{{ $integers->get(70, ' - ') }}個</td>
			                <th class="active">鍊條規格</th>
			                <td>{{ $groups->get(64, ' - ') }} / {{ $groups->get(65, ' - ') }}鍊目數</td>
			            </tr>
			            <tr>
			                <th class="active">火星塞間隙</th>
			                <td>{{ $strings->get(71, ' - ') }}</td>
			                <th class="active">電瓶型號</th>
			                <td>{{ $strings->get(83, ' - ') }}</td>
			            </tr>
			            <tr>
			                <th class="active">引擎機油總量</th>
			                <td>{{ $floats->get(10, ' - ') }}公升</td>
			                <th class="active">大燈</th>
			                <td>{{ $strings->get(84, ' - ') }}<br/>燈型:{{ $strings->get(85, ' - ') }}</td>
			            </tr>
			            <tr>
			                <th class="active">機油交換時</th>
			                <td>{{ $floats->get(11, ' - ') }}</td>
			                <th class="active">大燈備註</th>
			                <td>{{ $strings->get(84, ' - ') }}</td>
			            </tr>
			            <tr>
			                <th class="active">機油濾心交換時</th>
			                <td>{{ $floats->get(12, ' - ') }}</td>
			                <th class="active">尾燈</th>
			                <td>{{ $strings->get(87, ' - ') }} / {{ $strings->get(88, ' - ') }}</td>
			            </tr>
			            <tr>
			                <th class="active">前方向燈規格</th>
			                <td>{{ $strings->get(92, ' - ') }}</td>
			                <th class="active">後方向燈</th>
			                <td>{{ $strings->get(95, ' - ') }}</td>
			            </tr>
		            </tbody>
		        </table>
	        </div>
		</div>
		@include('mobile.pages.amp.motor.partials.other-service')
		<div class="section">
	    	<div>
		        <div>
		            <h3 class="common">{{$current_name}}零件用品快速搜尋</h3>
		        </div>
		        <div class="section-block">
		            <div class="btn-gap-bottom">
		            	<form action="{{ \URL::route('parts',['section' => "mt/$url_rewrite"]) }}" method="get" target="_blank">
		            		<div>
		            			
		            			<input id="motor_search_text" type="text" name="q" value="" class="btn btn-default btn-full box" placeholder="請輸入商品關鍵字...">
		            		</div>
		            		<div class="size-10rem">
		            			<input id="motor_search" type="submit" class="btn btn-danger btn-full" value="搜尋" >
		            		</div>
		            	</form>
		            </div>
		        </div>
		    </div>
		</div>
		@include('mobile.pages.amp.motor.partials.subcategory')
	@else
	    <div class="center-box">
	        <h1 class="common">暫無維修資訊</h1>
	    </div>
	@endif
</div>
@stop
