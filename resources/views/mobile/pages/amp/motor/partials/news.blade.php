<div class="relation_info mb10">
    <ul>
        @foreach ($news as $key => $post)
            @php
                $photo = \Everglory\Models\News\Meta::join('wp_posts', 'meta_value', '=', 'wp_posts.ID')
                ->where( 'wp_postmeta.post_id', $post->ID )
                ->where( 'wp_postmeta.meta_key', '_thumbnail_id' )
                ->first();
                $photo_path = 'http://www.webike.tw/bikenews/wp-content/themes/opinion_tcd018/img/common/no_image2.jpg';
                if( $photo and $photo->guid ){
                    $photo_path = $photo->guid;
                }

                $description_data = \Everglory\Models\News\Meta::where( 'post_id', $post->ID )
                ->where( 'meta_key', 'short_description' )
                ->first();

                if( $description_data ){
                    $short_description = $description_data->meta_value;
                }
            @endphp
            <li class="news clearfix">
                <div class="news-img">
                    <a class="zoom-image" href="{{ BIKENEWS.'/'.$post->post_name}} "> 
                        <amp-img src="{{$photo_path}}" alt="{{$post->post_title}} - 「Webike-摩托新聞」" width="150" height="100"></amp-img>
                    </a>
                </div>
                <div class="news-detail">
                    <div class="news-title">
                        <a class="font-color-blue" href="{{ BIKENEWS.'/'.$post->post_name}} "> 
                            {{$post->post_title}}
                        </a>
                    </div> 
                </div>
                <div class="news-time">
                        <span>{{$post->post_date}}</span>                                        
                </div>
            </li>
        @endforeach
    </ul>
</div>