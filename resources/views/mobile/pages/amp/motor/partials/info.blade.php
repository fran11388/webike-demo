<div class="main_image">
    {{--<img src="https://img.webike.net/sys_images/bg.png" data-src="{{$current_motor->image ? $current_motor->image_path . 'L_' . $current_motor->image : NO_IMAGE }}">--}}
    <amp-img src="{{$current_motor->image ? cdnTransform($current_motor->image_path . 'L_' . $current_motor->image) : cdnTransform(NO_IMAGE) }}" alt="{{$current_motor->name}}" width="450" height="300" layout="responsive"></amp-img>
</div>
