<div class="section">
    <div class="area">
        <div>
            <h3 class="common">{{$current_name}}其他詳細維修資訊</h3>
        </div>
        <amp-accordion disable-session-states>
        	<section>
        		<h2 class="amp_link_ui amp-toogle">車體</h2>
        		<div class="tab-pane fade in ">
                    <table class="table table-bordered ">
                        <tbody>
                        <tr>
                            <th class="active">引擎形式</th>
                            <td>{{ $strings->get(51, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">缸徑</th>
                            <td>{{ $floats->get(98, ' - ') }}mm</td>
                        </tr>
                        <tr>
                            <th class="active">行程</th>
                            <td>{{ $floats->get(99, ' - ') }}mm</td>
                        </tr>
                        <tr>
                            <th class="active">壓縮比</th>
                            <td>{{ $floats->get(15, ' - ') }}:1</td>
                        </tr>
                        <tr>
                            <th class="active">點火方式</th>
                            <td>{{ $groups->get(68, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">引擎潤滑方式</th>
                            <td>{{ $strings->get(115,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">車架樣式</th>
                            <td>{{ $groups->get(100,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">最小迴轉半徑</th>
                            <td>2.{{ $floats->get(30, ' - ') }}m</td>
                        </tr>
                        <tr>
                            <th class="active">傾角</th>
                            <td>{{ $floats->get(113,' - ') }}m</td>
                        </tr>
                        <tr>
                            <th class="active">前叉角度</th>
                            <td>{{ $floats->get(114,' - ') }}mm</td>
                        </tr>
                        <tr>
                            <th class="active">最低高度</th>
                            <td>{{ $integers->get(29, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">軸距</th>
                            <td>{{ $floats->get(116,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">轉向角度（右）</th>
                            <td>2.{{ $floats->get(117,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">轉向角度（左）</th>
                            <td>{{ $floats->get(118,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">乘員</th>
                            <td>{{ $integers->get(119,' - ') }}人</td>
                        </tr>
                        <tr>
                            <th class="active">油箱容量</th>
                            <td>{{ $floats->get(21, ' - ') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        	</section>
        	<section>
        		<h2 class="amp_link_ui amp-toogle">懸吊</h2>
        		<div class="tab-ct-customer-center-page-detail tab-ct-customer-center-question tab-pane fade">
                    <table class="table table-bordered ">
                        <tbody>
                        <tr>
                            <th class="active">前煞車系統</th>
                            <td>{{ $groups->get(72, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前煞車備註</th>
                            <td>{{ $strings->get(73, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後煞車系統</th>
                            <td>{{ $groups->get(74, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後煞車備註</th>
                            <td>{{ $strings->get(75, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前懸吊系統</th>
                            <td>{{ $strings->get(101,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前懸吊行程</th>
                            <td>{{ $integers->get(104,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前叉類型</th>
                            <td>{{ $groups->get(103,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前叉直徑</th>
                            <td>{{ $integers->get(105,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後懸吊系統</th>
                            <td>{{ $strings->get(102,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後避震器個數</th>
                            <td>{{ $integers->get(110,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後懸吊行程</th>
                            <td>{{ $integers->get(111,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後輪行程</th>
                            <td>{{ $integers->get(112,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">ABS</th>
                            <td>{{ $flags->get(31, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">傳動方式</th>
                            <td>{{ $groups->get(32, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">機油交換量（公升）</th>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th class="active">鍊條備註</th>
                            <td>{{ $strings->get(66, ' - ') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        	</section>
        	<section>
        		<h2 class="amp_link_ui amp-toogle">配備</h2>
        		<div class="tap-ct-create-question tab-pane fade">
                    <table class="table table-bordered ">
                        <tbody>
                        <tr>
                            <th class="active">坐墊下容量</th>
                            <td>{{ $floats->get(23, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">安全帽收納空間</th>
                            <td>-</td>
                        </tr>
                        <tr>
                            <th class="active">後視鏡安裝螺絲孔直徑</th>
                            <td>{{ $integers->get(120,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後視鏡備註</th>
                            <td>{{ $strings->get(121,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">電子設備</th>
                            <td>{{ $strings->get(122,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">油表</th>
                            <td>{{ $flags->get(123,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">碼表</th>
                            <td>{{ $flags->get(124,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">雙黃警示燈</th>
                            <td>{{ $flags->get(125,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">防盜系統</th>
                            <td>{{ $flags->get(126,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">尾燈燈座形狀</th>
                            <td>{{ $strings->get(89, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">尾燈燈座形狀2</th>
                            <td>{{ $strings->get(90, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">尾燈備註</th>
                            <td>{{ $strings->get(91, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前方向燈燈泡形式</th>
                            <td>{{ $strings->get(93, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前方向燈燈座形狀</th>
                            <td>-</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        	</section>
        	<section>
        		<h2 class="amp_link_ui amp-toogle">引擎</h2>
        		<div class="tab-ct-customer-center-page-detail tab-pane fade ">
                    <table class="table table-bordered ">
                        <tbody>
                        <tr>
                            <th class="active">離合器形式</th>
                            <td>{{ $groups->get(82, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">變速系統</th>
                            <td>{{ $groups->get(81, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">齒輪數</th>
                            <td>{{ $integers->get(76, ' - ') }}速</td>
                        </tr>
                        <tr>
                            <th class="active">齒輪比</th>
                            <td>{{ $strings->get(77, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">一次減速比</th>
                            <td>{{ $strings->get(78, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">二次減速比</th>
                            <td>{{ $strings->get(79, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">無段變速車變速比</th>
                            <td>{{ $strings->get(80, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">進氣頂桿間隙</th>
                            <td> - </td>
                        </tr>
                        <tr>
                            <th class="active">排氣頂桿間隙</th>
                            <td> - </td>
                        </tr>
                        <tr>
                            <th class="active">怠數轉速</th>
                            <td>{{ $strings->get(56, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">油針段數(固定位置)</th>
                            <td>{{ $strings->get(57, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">浮球液位</th>
                            <td>{{ $integers->get(58, ' - ') }}mm</td>
                        </tr>
                        <tr>
                            <th class="active">怠速螺絲</th>
                            <td>{{ $strings->get(59, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">噴油嘴形式</th>
                            <td>{{ $strings->get(60, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">噴油嘴編號</th>
                            <td>{{ $integers->get(61, ' - ') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        	</section>
        	<section>
        		<h2 class="amp_link_ui amp-toogle">其他</h2>
        		<div class="tab-ct-customer-center-page-detail tab-pane fade ">
                    <table class="table table-bordered ">
                        <tbody>
                        <tr>
                            <th class="active">離合器鋼索/油管長度</th>
                            <td>{{ $strings->get(129,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">阻風門鋼索長度</th>
                            <td>{{ $strings->get(130,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">碼錶鋼索長度</th>
                            <td>{{ $strings->get(131,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">油門線長度</th>
                            <td>{{ $strings->get(132,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">前制動鋼索/油管長度</th>
                            <td>{{ $strings->get(133,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">後制動鋼索/油管長度</th>
                            <td>{{ $strings->get(134,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">其他鋼索/油管</th>
                            <td>{{ $strings->get(135,' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">車架 號碼開始</th>
                            <td> - </td>
                        </tr>
                        <tr>
                            <th class="active">車架 號碼結束</th>
                            <td> - </td>
                        </tr>
                        <tr>
                            <th class="active">引擎編號（生產開始）</th>
                            <td>{{ $strings->get(52, ' - ') }}</td>
                        </tr>
                        <tr>
                            <th class="active">引擎編號（生產結束）</th>
                            <td>{{ $strings->get(53, ' - ') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        	</section>
        </amp-accordion>
    </div>
</div>