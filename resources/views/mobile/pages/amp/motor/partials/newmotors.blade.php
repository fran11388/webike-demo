<div>
    <a class="image-box col-xs-block" href="{!! $new_motor->link !!}" title="{{$new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET}}" target="_blank">
        <figure class="zoom-image">
            <amp-img src="{!! $new_motor->image !!}" alt="{{$new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET}}" width="220" height="150"></amp-img>
        </figure>
    </a>
</div>
<div class="content">
    <div class="manufacturer block-margin">
        {!! $new_motor->manufacurer!!}
    </div>
    <div class="name">
        <a class="dotted-text2" href="{!! $new_motor->link !!}" title="{{$new_motor->getFullName() . \Everglory\Constants\Website::MOTOMARKET}}" target="_blank">
            {!! $new_motor->motor_model_name!!}
        </a>
    </div>
    <div class="seller dotted-text1 force-limit block-margin">
        @if($new_motor->customer->group_id >= \Everglory\Constants\MotomarketCustomerGroup::CORPORATION)
            {{$new_motor->profiles->username}}
        @else
            個人自售
        @endif
    </div>
    <div class="price block-margin {!! $new_motor->price ? 'number font-color-red' : 'font-color-blue'!!}">
        {!! $new_motor->price ? '售價：' : '' !!}{!! $new_motor->price_text!!}
    </div>
</div>
