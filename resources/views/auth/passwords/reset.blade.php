@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>忘記密碼</h2>
    </div>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="box-items-group-customer-account">
            <div class="box-customer-account">
                <h2>重設密碼</h2>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul style="list-style-type: none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">電子信箱 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="steps step-3 txt-customer-account" type="text" name="email" value="" required autofocus>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">密碼 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="steps step-3 txt-customer-account" type="password" name="password" value="" required>
                    </td>
                </tr>

                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">確認密碼 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="steps step-3 txt-customer-account" type="password" name="password_confirmation" value="" required>
                    </td>
                </tr>
            </table>
        </div>
        <div class=" text-center col-xs-12 col-sm-12 col-md-12"> <input class=" btn btn-primary border-radius-2 btn-danger btn-confirm-the-changes" type="submit" name="" value="重 設 密 碼"></div>
    </form>

@stop
@section('script')

@stop
