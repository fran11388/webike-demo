@extends('response.layouts.2columns')
@section('left')
    @include('response.pages.customer.partials.menu')
@stop
@section('right')
    <div class="title-main-box">
        <h2>忘記密碼</h2>
    </div>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
        <div class="box-items-group-customer-account">
            <div class="box-customer-account">
                <h2>忘記密碼</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <table class="table-page-customer-account col-xs-12 col-sm-12 col-md-12">
                <tr>
                    <td class="col-xs-3 col-sm-3 col-md-3">註冊時的電子信箱 </td>
                    <td class="col-xs-9 col-sm-9 col-md-9">
                        <input class="steps step-3 txt-customer-account" type="text" name="email" value="" required autofocus>
                    </td>
                </tr>
            </table>
        </div>
        <div class=" text-center col-xs-12 col-sm-12 col-md-12"> <input class=" btn btn-primary border-radius-2 btn-danger btn-confirm-the-changes" type="submit" name="" value="送 出"></div>
    </form>

@stop
@section('script')

@stop
