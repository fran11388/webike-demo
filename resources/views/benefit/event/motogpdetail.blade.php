@extends('response.layouts.1column')
@section('style')
    <meta property="og:url" content="{{URL::route('benefit-event-motogp-2017detail', $thisSpeedway->station_name)}}"/>
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/motogpevent.css">
    <style>
    @media screen and (max-width: 767px) {
        .title-moto-gp-event .ct-title-moto-gp-event-left span {
            min-width: 154px !important;
        }
        .box-about-the-track-left {
            padding:0px;
        }
        .box-about-the-track-right {
            padding:0px;
            margin-top:15px;
        }
        .item-ct-television-broadcast-left {
            padding:0px;
        }
        .item-ct-television-broadcast-right {
            padding:0px;
            margin-top:15px;
        }
        .ct-oem-part-left .box-page-group .zoom-image{
            display:block;
        }
        .ct-oem-part-left .box-page-group .zoom-image img {
            width:100%;
        }
        #chart {
            overflow-x: auto
        }
        #makerBarChart {
            overflow-x: auto
        }
        #teamBarChart {
            overflow-x: auto
        }
    }
    .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left{
            width:100%;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left span{
            width:90%;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:after{
            content:none;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:before{
            width:10%;
        }
        .returnbtn .returnbtnhref{
            display:block;
        }
        .returnbtn .returnbtnhref:hover {
            opacity: 0.7;
            display:block;
            color:white;
        }
    </style>
@stop
@section('middle')
    <?php 
        $photo_home = 'http://img.webike.tw/assets/images/benefit/event/motoGp/2016/';
    ?>
    <div class="oem-part-page box-fix-column col-xs-12 col-sm-12 col-md-12">
        <div class="ct-oem-part-left box-fix-with">
            <div class="box-page-group text-center">
                <div class="title-box-page-group">
                    <h3>2017 MotoGP</h3>
                </div>
                <div class="ct-box-page-group">
                    <ul class="ul-menu-ct-box-page-group">
                        @foreach ($speedwaies as $key => $speedway)
                            @if($speedway->id == $thisSpeedway->id)
                                <li class="active">
                                    <a><span>{{'第'.($key + 1).'站 '.$speedway->name}}</span></a>
                                </li>
                            @else
                                <li>
                                    <a href="{{URL::route('benefit-event-motogp-2017detail', $speedway->station_name)}}" title="{{'2017 MotoGP 第'.($key + 1).'站 '.$speedway->name}}"><span>{{'第'.($key + 1).'站 '.$speedway->name}}</span></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="box-page-group">
                <a href="{{URL::route('benefit-event-motogp-2017')}}" title="2017 MotoGP 冠軍大預測{{$tail}}">
                    <figure class="zoom-image"><img src="{{$photo_home}}mainbanner.png" alt="2017 MotoGP 冠軍大預測{{$tail}}"></figure>
                </a>

            </div>
        </div>
        <div class="ct-oem-part-right box-fix-auto">
            <div class="row container box-motogpevent-top">
                <div id="fb-root"></div>
                <script>
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.5";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                document.write('<div class="fb-like" style="float:right" data-href="http://'+document.location.hostname+document.location.pathname+'" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>');
                </script>

                <div class="box-about-the-track-left col-xs-12 col-sm-6 col-md-6">
                    <img src="{{'http://img.webike.tw'.$thisSpeedway->img_url}}" alt="【2017{{$thisSpeedway->name}}】台灣正賽時間{{$thisSpeedway->raced_at_local.$tail}}">
                </div>
                <div class="box-about-the-track-right col-xs-12 col-sm-6 col-md-6">
                    <div class="title-main-box title-main-box-moto-gp">
                        <h2>賽道簡介</h2>
                    </div>
                    <table class="table table-bordered table-motogp-detail">
                        <tbody>
                        <tr>
                            <td>賽道名稱：{{$thisSpeedway->speedway_name.'('.$thisSpeedway->speedway_name_en.')'}}</td>
                        </tr>
                        <tr>
                            <td>國家：{{$thisSpeedway->country}}</td>
                        </tr>
                        <tr>
                            <td>賽道長度：{{$thisSpeedway->speedway_length}}</td>
                        </tr>
                        <tr>
                            <td>彎道數：{{$thisSpeedway->speedway_curve}}</td>
                        </tr>
                        <tr>
                            <td>比賽圈數：{{$thisSpeedway->turns_num}}</td>
                        </tr>
                        <tr>
                            <td>比賽總里程：{{$thisSpeedway->distance}}</td>
                        </tr>
                        <tr>
                            <td>台灣正賽時間：{{$thisSpeedway->raced_at_local}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="post ct-below-post">
                 <div class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 {{$thisSpeedway->country}}GP</span>&nbsp 賽後報導</span></div>
                    </div>
                </div>
                <div class="entry-header thumbnail-below-post col-xs-4 col-sm-4 col-md-4 ">
                    <a class="bg-color-red text-center btn-category" href="http://www.webike.tw/bikenews/2017/02/23/%e3%80%90aprilia-taiwan%e3%80%91%e5%b1%85%e9%ab%98%e8%87%a8%e4%b8%8b%ef%bc%8c%e5%b0%8a%e6%a6%ae%e4%b8%8d%e5%87%a1%ef%bc%81-dorsoduro-750-%e9%99%90%e9%87%8f%e7%89%b9%e6%83%a0%e4%b8%ad%ef%bc%81" target="_blank">News</a>
                    <div class="entry-thumbnail"><a class="zoom-image rounded-img" href="http://www.webike.tw/bikenews/2017/02/23/%e3%80%90aprilia-taiwan%e3%80%91%e5%b1%85%e9%ab%98%e8%87%a8%e4%b8%8b%ef%bc%8c%e5%b0%8a%e6%a6%ae%e4%b8%8d%e5%87%a1%ef%bc%81-dorsoduro-750-%e9%99%90%e9%87%8f%e7%89%b9%e6%83%a0%e4%b8%ad%ef%bc%81" target="_blank">
                                                    <img class="img-responsive rounded-img" src="https://www.webike.tw/bikenews/wp-content/uploads/2017/02/0213.jpg" alt="">
                                            </a></div>
                </div>
                <!--
                <div class="entry-meta-2">
                    <ul class="list-inline meta-ct">
                        <li class="comments"><a href="#"><i class="fa fa-comment-o size-12rem"></i> <span class="comments-show">Comment</span> </a></li>
                        <li class="share"><i class="fa fa-share-alt icon-share size-12rem"></i>
                            <ul class="share-show">
                                <li><a href="#"><span>Facebook</span><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><span>Twitter</span><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                -->
                <div class="col-xs-8 col-sm-8 col-md-8 post-content content-below-post">
                     <a class="title-content-below-post" href="http://www.webike.tw/bikenews/2017/02/23/%e3%80%90aprilia-taiwan%e3%80%91%e5%b1%85%e9%ab%98%e8%87%a8%e4%b8%8b%ef%bc%8c%e5%b0%8a%e6%a6%ae%e4%b8%8d%e5%87%a1%ef%bc%81-dorsoduro-750-%e9%99%90%e9%87%8f%e7%89%b9%e6%83%a0%e4%b8%ad%ef%bc%81" target="_blank">
                        <h2 class="entry-title a-link-blue-color"> 【Aprilia Taiwan】居高臨下，尊榮不凡！ Dorsoduro 750 限量特惠中！ </h2>
                    </a> <span>義大利精緻造車工藝結晶Dorsoduro 750限量特惠中</span>
                </div>
            </div>
            <div class="row container box-motogpevent-top">
                <div class="title-moto-gp-event">
                    <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp 電視轉播資訊</span></div>
                </div>
                <div class=" ct-television-broadcast ct-your-scoreboard">
                    <h1>FOX體育台 - {{$thisSpeedway->country}}站轉播時間</h1>
                    <ul class="ul-ct-television-broadcast">
                        <li class="item-ct-television-broadcast-left col-xs-12 col-sm-6 col-md-6">
                            <div class="item-ct-television-broadcast-info">
                                <h2>MotoGP排位賽</h2>
                                @if($thisSpeedway->ranking_view_datetime)
                                    <a href="{{$thisSpeedway->ranking_view}}" title="2017 MotoGP排位賽 FOX 直播" nofollow="true" target="_blank">{{$thisSpeedway->ranking_view_datetime}} (直撥)MotoGP排位賽: {{$thisSpeedway->station_name}}</a>
                                @else
                                    尚未公布
                                @endif
                            </div>
                        </li>
                        <li class="item-ct-television-broadcast-right col-xs-12 col-sm-6 col-md-6">
                            <div class="item-ct-television-broadcast-info">
                                <h2>MotoGP正賽</h2>
                                @if($thisSpeedway->formal_view_datetime)
                                    <a href="{{$thisSpeedway->formal_view}}" title="2017 MotoGP正賽 FOX 3 HD 直播" nofollow="true" target="_blank">{{$thisSpeedway->formal_view_datetime}} (直播)MotoGP正賽: {{$thisSpeedway->station_name}}</a>
                                @else
                                    尚未公布
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <?php 
                function recordExchange($string)
                {
                    $values = $string;
                    if(strpos($values, ',') !== false){
                        $values = explode(',', $values);
                    }else{
                        $values = array('-', '-');
                    }
                    return $values;
                }
            ?>
            @if($thisSpeedway->pole_rider_2017)
            <div class="row container box-motogpevent-top">
                <div class="title-main-box title-main-box-moto-gp">
                    <h2>2017 {{$thisSpeedway->station_name}}成績</h2>
                </div>
                <table class="table table-bordered table-motogp-detail">
                    <thead class="table-moto-boder-none">
                    <tr>
                        <th class="text-center"></th>
                        <th class="text-center">車手姓名</th>
                        <th class="text-center">成績</th>
                        <th class="text-center">時速</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">桿位</td>
                        <td class="text-center">{{$thisSpeedway->pole_rider_2017}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->pole_record_2017);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">冠軍</td>
                        <td class="text-center">{{$thisSpeedway->first_2017}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->first_record_2017);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr>
                        <td class="text-center">亞軍</td>
                        <td class="text-center">{{$thisSpeedway->second_2017}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->second_diff_2017);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">季軍</td>
                        <td class="text-center">{{$thisSpeedway->third_2017}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->third_diff_2017);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @endif
            <div class="row container box-motogpevent-top">
                <div class="title-main-box title-main-box-moto-gp">
                    <h2>賽道紀錄</h2>
                </div>
                <table class="table table-bordered table-motogp-detail">
                    <thead class="table-moto-boder-none">
                    <tr>
                        <th class="text-center"></th>
                        <th class="text-center">紀錄保持人</th>
                        <th class="text-center">紀錄成績</th>
                        <th class="text-center">時速</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">最快單圈</td>
                        <td class="text-center">{{$thisSpeedway->max_singleturn_racer}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->max_singleturn);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">正賽紀錄</td>
                        <td class="text-center">{{$thisSpeedway->max_record_racer}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->max_record);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr>
                        <td class="text-center">最快排位</td>
                        <td class="text-center">{{$thisSpeedway->max_qualifying_racer}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->max_qualifying);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">最快時速</td>
                        <td class="text-center">{{$thisSpeedway->max_speed_racer}}</td>
                        <td class="text-center">-</td>
                        <td class="text-center">{{$thisSpeedway->max_speed}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row container box-motogpevent-top">
                <div class="title-main-box title-main-box-moto-gp">
                    <h2>2016 {{$thisSpeedway->station_name}}成績</h2>
                </div>
                <table class="table table-bordered table-motogp-detail">
                    <thead class="table-moto-boder-none">
                    <tr>
                        <th class="text-center"></th>
                        <th class="text-center">車手姓名</th>
                        <th class="text-center">成績</th>
                        <th class="text-center">時速</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">桿位</td>
                        <td class="text-center">{{$thisSpeedway->pole_rider_2016}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->pole_record_2016);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">冠軍</td>
                        <td class="text-center">{{$thisSpeedway->first_2016}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->first_record_2016);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr>
                        <td class="text-center">亞軍</td>
                        <td class="text-center">{{$thisSpeedway->second_2016}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->second_diff_2016);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    <tr class="active">
                        <td class="text-center">季軍</td>
                        <td class="text-center">{{$thisSpeedway->third_2016}}</td>
                        <?php 
                            $values = recordExchange($thisSpeedway->third_diff_2016);
                        ?>
                        <td class="text-center">{{$values[0]}}</td>
                        <td class="text-center">{{$values[1]}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="driver-information" class="row container box-motogpevent-top">
                <div class="title-moto-gp-event">
                    <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp 車手資訊及成績</span></div>
                </div>
                <div class="container-moto-gp-event ct-your-scoreboard">
                    <div style="overflow-x: auto;width:100%">
                        <table class="table table-bordered table-motogp-driver-information">
                            <thead>
                            <tr>
                                <th class="text-center">車號</th>
                                <th class="text-center">國籍</th>
                                <th class="text-center">車手姓名</th>
                                <th class="text-center">車隊名稱</th>
                                <th class="text-center">車輛廠牌</th>
                                <th class="text-center">2017累計積分</th>
                                <th class="text-center">2017目前排名</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach( $racers as $key => $racer )
                                    <tr>
                                        <td class="text-center">{{$racer->number}}</td>
                                        <td class="text-center"><img src="{{$photo_home}}country/{{$racer->country}}.png" alt="2017MotoGP【{{$racer->number}}】{{$racer->name}}國籍"></td>
                                        <td><a href="{{$racer->site}}" title="2017MotoGP【{{$racer->number}}】{{$racer->name}}" target="_blank" nofollow>{{$racer->name}}</a></td>
                                        <td><a href="{{$racer->team_site}}" title="2017MotoGP{{$racer->team_name}}" target="_blank" nofollow>{{$racer->team_name}}</a></td>
                                        <td class="text-center">{{$racer->motor_manufacturer_name}}</td>
                                        <td class="text-center">{{$racer->total_scores ? $racer->total_scores : 0}}</td>
                                        <td class="text-center">{{$racer->ranking ? $racer->ranking : '-'}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="note">
                        註1:生涯頒獎台包含MotoGP&GP500最高級距離賽事累積成績，由左至右依序為冠軍、亞軍、季軍次數。<br>
                        註2:總冠軍為MotoGP&GP500最高級距離賽事總和。<br>
                        資料來源：MotoGP <br>
                    </div>
                </div>
            </div>
            <div id="points-ranking" class="row container box-motogpevent-top">
                @if(count($top10_score) > 0)
                    <div id="points-ranking" class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp積分排行</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-your-scoreboard">
                        <div class="lineChart col-xs-12 col-sm-12 col-md-12">
                            <h2 class="title-chart">車手積分排行榜</h2>
                            <div id="chart" class="ct-lineChart text-center"></div>
                        </div>
                        <div class="half-groph half-groph-left col-xs-12 col-sm-6 col-md-6">
                            <h2 class="title-chart">車隊積分排行榜</h2>
                            <div id="teamBarChart" class="ct-half-groph"></div>
                        </div>
                        <div class="half-groph half-groph-left col-xs-12 col-sm-6 col-md-6">
                            <h2 class="title-chart">車廠積分排行榜</h2>
                            <div id="makerBarChart" class="ct-half-groph"></div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="returnbtn text-center col-xs-12 col-sm-12 col-md-12">
                <div class="col-xs-12 col-sm-3 col-md-3"></div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="title-moto-gp-event title-moto-gp-event-border">
                        <a class="returnbtnhref" href= "{{URL::route('benefit-event-motogp-2017')}}" target="_blank">   
                            <div class="ct-title-moto-gp-event-left"><span>MotoGP預測總冠軍首頁</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3"></div>
            </div>
        </div>
    </div>
    @if($other_champigns and count($other_champigns))
        <?php $total_champion = 0 ?>
        @foreach($other_champigns as $other_chamign)
            <?php 
                $fk_champion = $other_chamign->champion * 11;
                $total_champion += $fk_champion;
             ?>
        @endforeach
    @endif
@stop
@section('script')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    @if(isset($total_champion))
        <script type="text/javascript">
            var total_champion = "{{$total_champion}}";
        </script>
    @endif
<script type="text/javascript">
    var first_view = true;
    $(document).scroll(function(){
        if( first_view && $(this).scrollTop() + $(window).height() >= $(".ct-percent:last").offset().top ){
            first_view = false;
            peoples = 0;
            $('.ct-percent').each(function(key,element){
                peoples += parseInt($(element).text());
                percentage = Math.round(parseInt($(element).text()) / parseInt(total_champion) * 100 ) + '%';
                // $(element).text('').animate({display: block});
                $(element).text('').animate({width: percentage}, 1500);
                $(element).css('display','block');
                $(element).closest('li').find('.num-percent').html(percentage);
            });
            var h2 = $('.ct-percent').closest('.item-ct-your-scoreboard-right').find('h2');
            // alert();
            h2.text(h2.text() + '(共 '+ peoples +' 人)');
        }
    });

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var options = {
            titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
            lineWidth: 1,
            // colors: ['#2864D1', '#D10501'],
            chartArea: {
                left: 80,
                right: 25,
                top: 50
            },
            width: 900,
            height: 300,
            pointSize: 6,
            hAxis: {
                baseline: 0,
                slantedText: true,
                slantedTextAngle:30,
                title: '賽次'
            },
            vAxis: {
                viewWindow: {
                    min: 0
                },
                baseline: 0,
                title: '累積積分'
            },
            legend: {
                position: 'top'
            },
            tooltip: {
                isHtml: true,
                textStyle: {
                    fontSize: 13
                }
            },
            focusTarget: 'category',
            'areaOpacity': 0.1,
            annotations: {
                textStyle: {
                    fontName: 'Times-Roman',
                    fontSize: 13,
                    bold: true,
                    italic: true,
                }
            },
        };

        var data = new google.visualization.DataTable();
        data.addColumn('string', '站別');
        
        @foreach($top10 as $racer)
            data.addColumn('number', '【{{$racer->number}}】{{$racer->nickname}}');
        @endforeach

        data.addRows([
            <?php 
                $scores = array();
                $last_top10_str = '';
                for($i = 1 ; $i <= 18 ; $i++){
                    foreach($top10 as $key => $racer){
                        if( !isset($scores[$key]) ){
                            $scores[$key] = 0;
                        }
                        $top10_score->filter(function($racer_score) use($racer, $i, $key, &$scores){
                            if($racer_score->speedway_id == $i and $racer_score->racer_id == $racer->id){
                                $scores[$key] += $racer_score->score;
                            }
                        });
                    }
                    $top10_str = implode(',', $scores);
                    if($top10_str !== $last_top10_str){
                        $last_top10_str = $top10_str;
                        echo '["第'.($i).'站", '.$top10_str.'],';
                    }
                }
            ?>
        ]);

        var formatter = new google.visualization.PatternFormat('{1}');
        formatter.format(data, [0, 1], 1);
        formatter.format(data, [0, 2], 2);

        var chart = new google.visualization.LineChart(document.getElementById('chart'));
        chart.draw(data, options);

        var data = new google.visualization.DataTable();
        data.addColumn('string', '車隊');
        data.addColumn('number', '積分');
        data.addRows([
                @foreach( $teamData as $team )
                    ['{{$team->team_name}}', {{$team->sum}}],
                @endforeach
            ]);
        var view = new google.visualization.DataView(data);
        var options = {
                titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
                chartArea: {
                    left: 120,
                    right: 25,
                    top: 25,
                    height: 220
                },
                width: 470,
                height: 300,
                bar: {groupWidth: "60%"},
                legend: { position: "none" },
                vAxis: {
                    viewWindow: {
                        min: 0
                    },
                    baseline: 0,
                    title: ''
                },
                hAxis: {
                    title: '',
                    slantedText: true,
                    slantedTextAngle:30
                }
        };
        var teamBarChart = new google.visualization.BarChart(document.getElementById("teamBarChart"));
        teamBarChart.draw(view, options);

        var data = new google.visualization.DataTable();
        data.addColumn('string', '車廠');
        data.addColumn('number', '積分');
        data.addRows([
                @foreach( $makerData as $maker )
                    ['{{$maker->motor_manufacturer_name}}', {{$maker->sum}}],
                @endforeach
            ]);
        var view = new google.visualization.DataView(data);
        var options = {
                titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
                chartArea: {
                    left: 100,
                    right: 25,
                    top: 25,
                    height: 220
                },
                width: 470,
                height: 300,
                bar: {groupWidth: "60%"},
                legend: {
                    position: 'none'
                },
                vAxis: {
                    viewWindow: {
                        min: 0
                    },
                    baseline: 0,
                    title: ''
                },
                hAxis: {
                    title: '',
                    slantedText: true,
                    slantedTextAngle:30
                }
            };
        var makerBarChart = new google.visualization.BarChart(document.getElementById("makerBarChart"));
        makerBarChart.draw(view, options);
    }
</script>
@stop
