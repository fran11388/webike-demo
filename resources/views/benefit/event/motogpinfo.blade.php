@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/motogpevent.css">
    <style>
    	.motogpinfo {
    		width:100%;
			border:1px solid black;
			margin-bottom:30px;
    	}
    	.ct-activitiesa-award img {
    		width:100%;
    	}
    	.ct-activitiesa-award li span {
	    	min-height: 150px !important;
	    }
    	@media screen and (max-width: 767px) {
	        .ct-activitiesa-award li {
	        	float:none;
	        	width:100%;
	        	clear:both;
	        	margin-left:0px !important;
	        }
	        .ct-activitiesa-award span {
		        word-break: break-all;
				min-height:0;
		    }
		    .container-moto-gp-event .ul-ct-guessing-method li{
		    	width:100%;
		    	display:block;
		    	margin-top:10px;
		    }
	    }
	    .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left{
	    	width:100%;
	    }
	    .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left span{
	    	width:90%;
	    }
	    .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:after{
	    	content:none;
	    }
	    .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:before{
			width:10%;
	    }
	    .returnbtn .returnbtnhref{
	    	display:block;
	    }
	    .returnbtn .returnbtnhref:hover {
			opacity: 0.7;
			display:block;
			color:white;
	    }
	    
	    .right-side-box{
            padding-right: 5%;
        }
        .left-side-box{
            padding-left: 5%;
        }
        .gp-box{
            margin-top: 20px;
            border: 2px #777 solid;
        }
        .gp-col{
            display: flex;
            justify-content: center;
            flex-direction: column;
            padding: 3% 2%;
        }
        .gp-box-content{
            display: flex;
            background: #e7e7e7;
        }
        .gp-box-content h3{
            font-weight: bold;
            font-size: 1.2rem;
        }
        .gp-box-content ul li{
            text-decoration: none;
            list-style: none;
        }
        .gp-box-title h2{
            position: relative;
        }
        .gp-box-title h2 span{
            color: #fff;
            position: absolute;
            left: 2%;
            line-height: 30px;
            font-size: 1.2rem;
            font-weight: bold;
        }
        .gp-box-title h2 img{
            width:25%;
        }
        .gp-box-border{
            border:1px #ccc solid;
        }
        .box-green{
            background: rgba(0, 189, 3, 0.6);
        }
        .box-green1{
            background: rgba(0, 189, 3, 1);
        }
        .box-white{
            background: #fff;
        }
        .btn-winner {
            border: 2px solid #e61e25;
            color: #e61e25;
            width: 100%;
        }
        .btn-winner:hover {
            background-color: #e61e25;
            color: #fff;
        }
        .gp-col .box-white .result{
            float:none;
        }
        .gp-col .right-side-box .box-white li {
            display:block;
            clear:both;
            margin:2%;
        }
        .gp-col .right-side-box .box-white .border-radius-2{
            text-align: center;
            padding: 5px 10px;
            width: 40%;
            border: 2px solid #333333;
            font-weight: bold;
            text-transform: uppercase;
            min-height: 35px;
            float:left;
        }
        .gp-col .right-side-box .box-white .num-percent {
            width: 20%;
            line-height: 35px;
            float: left;
            text-align: center;
        }
        .gp-col .right-side-box .box-white .percent {
            margin: 12px 0;
            width: 40%;
            height: 10px;
            background-color: #efefef;
            float: left;
        }
        .gp-col .right-side-box .box-white .ct-percent {
            height: 10px;
            background-color: #ff6600;
            float: left;
        }
        .box-light-blue{
            background: rgba(3, 79, 137, 0.6);
        }
        .box-light-blue1{
            background: rgba(3, 79, 137, 1);
        }
        .box-light-red{
            background:rgba(227, 17, 16 ,0.6);
        }
        .box-light-red1{
            background:rgba(227, 17, 16 ,1);
        }
        .box-white{
            background: #fff;
        }
        .nail-box{
            position: relative;
        }
        .fly-box{
            position: absolute;
            width:450px;
        }
        .box-tall{
            height:100%;
        }
        .nail-left{
            left:10%;
        }
        .nail-right {
            right:10%;
        }
        .nail-top {
            top: 8.5%;
        }
        /*.nail-bottom {
            bottom: 8.5%;
        }*/
        .gp-box .line {
            height:5px;
            float:none;
        }
        .box-white .title-site-activity-forecast-right{
            font-size: 1.2rem;
            padding: 5px;
            width: 100%;
            background-color: #0066c0;
            color: #fff;
            text-align: center;
        }
        .box-white .note{
            width: 100%;
            padding: 5px;
            float: left;
            text-align:center;
            background-color: #efefef;
            bottom: 0;
        }
        .nail-box .info {
            float:right;
        }
        .gp-box-content .info-table {
            position: absolute;
            width: 100%;
            z-index: 2;
        }
    </style>
@stop
@section('middle')
	<div id="event-description" class="row container box-motogpevent-top">
		<div class="motogpinfo">
			<h2>活動訊息banner</h2>
		</div>
	    <div class="title-moto-gp-event title-moto-gp-event-border">
	        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2016 MotoGP</span>&nbsp 冠軍大預測</span></div>
	        <div class="ct-title-moto-gp-event-right">
	            <iframe name="f15dd4e91fd3d5c" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.5/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F_IDqWBiKXtV.js%3Fversion%3D42%23cb%3Df48194bbf97c3%26domain%3Dwww.webike.tw%26origin%3Dhttp%253A%252F%252Fwww.webike.tw%252Ff2cc3e9386b3aa4%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2Fwww.webike.tw%2Fbenefit%2Fevent%2Fmotogp%2F2016&amp;layout=button&amp;locale=zh_TW&amp;sdk=joey&amp;share=true&amp;show_faces=false" style="border: none; visibility: visible; width: 82px; height: 20px;" class=""></iframe>
	        </div>
	    </div>
	    <div class="container-moto-gp-event2">
	        【活動說明】預測2016MotoGP各分站前三名，猜對即可贏得免費點數，並有機會贏得SHOEI X-14次世代賽車安全帽及300元禮券。<br>
	        【活動時間】自2016/03/11日起開放答題，每站都可參加，詳見各 <a href="javascript:void(0)">站活動時間表</a>。<br>
	        【活動資格】凡是「Webike-摩托百貨」完整會員皆可參加，若還沒註冊的會員，請按此 <a href="javascript:void(0)"> 免費加入</a>。<br>
	        【活動解答】MotoGP各站結束後 隔日，我們會公布每站正確答案，結果以MotoGP官方結果為準。<br>
	        【活動規範】每個會員每站僅限答題一次，提交答案後無法修改，請小心回答。得獎者我們會以電話進行會員身分資料確認，若查證冒名者將會取消資格，並依據<a href="javascript:void(0)">會員規約</a>刪除會員帳號<br>
	    </div>
	</div>
	<div class="row container box-motogpevent-top">
	    <div class="title-moto-gp-event title-moto-gp-event-border">
	        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2016 MotoGP</span>&nbsp活動獎勵</span></div>
	    </div>
	    <div class="row container-moto-gp-event2">
	        <ul class="ct-activitiesa-award">
	            <li>
	                <figure class="row"><img src="/image/moto-gp/img-activities-step1.png" alt="Name image"></figure>

	                <h2>猜對分站前三名現金點數免費拿</h2>
	                <span>1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。 <br>
						2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。 <br>
						3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱現金點數說明 <br>
						4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在點數獲得及使用履歷中查詢。
						                    1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。 <br>
						2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。 <br>
						3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱<a href="http://www.webike.tw/customer/rule/pointinfo" title="現金點數說明 - 「Webike-摩托百貨」" target="_blank">現金點數說明</a> <br>
						4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在<a href="http://www.webike.tw/customer/history/points" title="點數獲得及使用履歷 - 「Webike-摩托百貨」" target="_blank">點數獲得及使用履歷</a>。
	                </span>
	            </li>
	            <li>
	                <figure class="row"><img src="https://www.webike.tw/assets/images/benefit/event/motoGp/2016/benefit2.png" alt="Name image"></figure>

	                <h2>猜對分站前三名現金點數免費拿</h2>
	                <span>1.只要您答對2016MotoGP各分站冠軍，SHOEI X-14賽車安全帽直接帶回家，名額不限。<br>
	2.贈品規格<a href="http://www.webike.tw/sd/22605076" title="【SHOEI】X-14賽車安全帽 - 「Webike-摩托百貨」" target="_blank">SHOEI X-14賽車安全帽 白色/SIZE:L</a><br>
	3.答對名單會在第18站瓦倫西亞比賽結束後次日公布在此活動頁面。<br>
	</span>
	            </li>
	            <li>
	                <figure class="row"><img src="https://www.webike.tw/assets/images/benefit/event/motoGp/2016/benefit3.png" alt="Name image"></figure>

	                <h2>猜對分站前三名現金點數免費拿</h2>
	                <span>1.連續18站參加活動，系統會自動累計總冠軍分數，若您預測的年度總冠軍與2016MotoGP總冠軍相同，即可獲得「Webike-摩托百貨」300元現金禮券，並可參加X-14安全帽抽獎。<br>
	2.答對者抽出<a href="http://www.webike.tw/sd/22605076" title="【SHOEI】X-14賽車安全帽 - 「Webike-摩托百貨」" target="_blank">SHOEI X-14賽車安全帽 白色/SIZE:L，一名。</a><br>
	3.獲獎名單預定會在第18站瓦倫西亞比賽結束後次日公布在此活動頁面。若有提前封王的情況，我們將以封王該站為標準提前宣布獎名單。
	※為了公平起見，此好康僅限每站皆有參加的會員為主。</span>
	            </li>
	        </ul>
	    </div>
	</div>
	<div id="guess-the-way" class="row container box-motogpevent-top">
	    <div class="title-moto-gp-event">
	        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2016 MotoGP</span>&nbsp猜獎方式說明</span></div>
	    </div>
	    <div class="container-moto-gp-event ct-guessing-method">
	        <ul class="ul-ct-guessing-method">
	            <li><img src="/image/moto-gp/img-step-01.jpg" alt="image step 1"></li>
	            <li><i class="fa fa-chevron-right visible-md visible-lg" aria-hidden="true"></i></li>
	            <li><img src="/image/moto-gp/img-step-02.jpg" alt="image step 2"></li>
	            <li><i class="fa fa-chevron-right visible-md visible-lg" aria-hidden="true"></i></li>
	            <li><img src="/image/moto-gp/img-step-03.jpg" alt="image step 3"></li>
	        </ul>
	    </div>
	</div>
	<div class="returnbtn text-center col-xs-12 col-sm-12 col-md-12">
		<div class="col-xs-12 col-sm-3 col-md-3"></div>
		<div class="col-xs-12 col-sm-6 col-md-6">
			<div class="title-moto-gp-event title-moto-gp-event-border">
				<a class="returnbtnhref" href= "{{URL::route('benefit-event-motogp-2017')}}" target="_blank">	
		        	<div class="ct-title-moto-gp-event-left"><span>本站活動預測</span></div>
		        </a>
		    </div>
	    </div>
		<div class="col-xs-12 col-sm-3 col-md-3"></div>
	</div>
@stop