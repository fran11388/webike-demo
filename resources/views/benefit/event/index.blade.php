@extends('response.layouts.1column')
@section('style')
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/motogpevent.css">
	<style>
		.short-description{
			border: 4px solid #dcdee3;
    		margin: 20px 0px;
		}
		#selfranking ul {
			margin-bottom:10px;
		}
		#selfranking table{
			margin-top: 0px;
		    float: left;
		    width: 10%;
		    background: white;
		}
		#mobilehistory{
			display:none;
		}
		#pchistory table{
	        width:100%;
	    }
	    form .drop-site-activity-forecast .box-fix-column {
			padding:37px 10px !important;
		}
		@media screen and (max-width: 767px) {
		    #pchistory{
		        display:none;
		    }
		    #mobilehistory{
				display:block;
				width:100%;
				white-space:nowrap;
				overflow-x: auto;
		    }
		    .container-moto-gp-event .ul-ct-site-activity-forecast {
				display:block;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-left {
				padding-right: 0px;
				margin-bottom: 14px;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .drop-site-activity-forecast .box-fix-column{
				float:none;
				width:100%;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .note {
				position:static;
				text-align:center;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .note span{
				float:none;
				display:block;
			}
			.container-moto-gp-event .ul-ct-site-activity-forecast .ct-site-activity-forecast-right .note a{
				float:none;
			}
			.ct-item-ct-your-scoreboard-left .right{
				padding:0px;
			}
			.ul-ct-your-scoreboard .item-ct-your-scoreboard-right .ul-table-info-moto-gp .border-radius-2 {
				margin-top:10px !important;
				float:none !important;
			}
			.ul-ct-your-scoreboard .item-ct-your-scoreboard-right .table-info-moto-gp{
				text-align:center;
			}
			.ul-ct-your-scoreboard .item-ct-your-scoreboard-right li{
				margin-top: 10px;
			}
			#chart {
				overflow-x: auto
			}
			#makerBarChart {
				overflow-x: auto
			}
			#teamBarChart {
				overflow-x: auto
			}
		}
		.raceafter {
		    border: 1px solid black;
		    padding: 11px;
		}
		.topthree span{
			white-space: initial;
			padding: 4px 12px;
		    width: 10px;
		    line-height: 16px;
		    color: #000;
		    font-weight: bold;
		    display: block;
		    font-size: 12px;
		    white-space: initial;
		    padding: 4px 24px 4px 13px;
		}
		.topthree label{
			margin-top: 10px;
		}
		.topthree {
			margin-top:20px;
		}
		.topthree .num1{
			background: #FFAA00;
		}
		.topthree .num2{
			background: #E6E6E6;
		}
		.topthree .num3{
			background: #F26946;
		}
		.select2-results .select2-results__option {
			font-size:5px;
		}
		iframe html body ._li{
			display:none !important;
		}
		.uiScaledImageContainer {
			width:100% !important;
		}
	</style>
@stop
@section('middle')
	<?php
		$photo_home = 'http://img.webike.tw/assets/images/motogp/2017/';
	?>
	<div class="motogpevent-main-page product-detail col-xs-12 col-sm-12 col-md-12">
		<div class="banner-motogp-top">
    		<img src="/image/moto-gp/banner-top.jpg" alt="banner moto GP">
		</div>
        <div class="menu-motogpevent-top">
        	<ul class="ul-menu-motogpevent-top short-description">
        		<li class="col-xs-6 col-sm-12 col-md-12 text-left">
  					<span><a href="{{URL::route('benefit-event-motogp-2017info')}}"><h2>活動說明banner</h2></a></span>
        		</li>
        		<li class="col-xs-6 col-sm-12 col-md-12">
        			<li class="col-xs-6 col-sm-10 col-md-12">
        				<div id="fb-root"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.8";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="fb-post" data-href="https://www.facebook.com/WebikeTaiwan/posts/480050898785203" data-width="auto" data-show-text="true"><blockquote cite="https://www.facebook.com/WebikeTaiwan/posts/480050898785203" class="fb-xfbml-parse-ignore"><p>&#x3010;&#x7de8;&#x8f2f;&#x9577;&#x5c08;&#x6b04;&#x3011;&#x76f4;&#x7dda;&#x738b;&#x8005;&#x7684;&#x6b9e;&#x843d;! YAMAHA &#x300c;VMAX&#x300d;&#x6b77;&#x53f2;&#x56de;&#x9867;

&#x7814;&#x767c;&#x51fa;VMAX&#x7684;&#x7814;&#x767c;&#x5718;&#x968a;&#x8981;&#x8868;&#x660e;&#x7684;&#x5c31;&#x662f;&#x300c;&#x76e1;&#x60c5;&#x5730;&#x904b;&#x7528;VMAX&#x4f86;&#x8c50;&#x5bcc;&#x4eba;&#x751f;&#x300d;&#xff0c; &#x6b63;&#x662f;&#x5411;&#x4e16;&#x9593;&#x5c55;&#x73fe;&#x6469;&#x6258;&#x8eca;&#x6b3e;&#x5b58;&#x5728;&#x610f;&#x7fa9;&#x7684;&#x540d;&#x8a00;&#x3002;

&#x8a73;&#x7d30;&#x65b0;&#x805e;&#xff1a;http://www.webike.tw/bikenews/?p=14807</p>由 <a href="https://www.facebook.com/WebikeTaiwan/">Webike台灣</a>貼上了&nbsp;<a href="https://www.facebook.com/WebikeTaiwan/posts/480050898785203">2017年3月5日</a></blockquote></div>
        			</li>
		        </li>
        	</ul>
        </div>
		<div id="myNavbar" class=" row menu-motogpevent-top">
		    <ul class="ul-menu-motogpevent-top">
		    	<li class="col-xs-6 col-sm-1 col-md-1 visible-md visible-lg"></li>
		    	<li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#site-prediction">本站活動預測</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#event-discuss">活動討論區</a>
		        </li>
		         <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#driver-information">車手資訊及成績</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#points-ranking">目前積分排行</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#schedule">賽程表</a>
		        </li>
		        <li class="col-xs-6 col-sm-1 col-md-1 visible-md visible-lg"></li>
		    </ul>
		</div>
		<div id="site-prediction" class="row container box-motogpevent-top">
		    <div class="title-moto-gp-event">
		        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp本站活動預測</span></div>
		    </div>
		    <div class="container-moto-gp-event ct-site-activity-forecast">
		        <ul class="ul-ct-site-activity-forecast">
			        <li class="ct-site-activity-forecast-left col-xs-12 col-sm-5 col-md-5">
			        	@if(count($now_speedway) != 0)
			                <a href="{{URL::route('benefit-event-motogp-2017detail', $now_speedway->station_name)}}" title="{{$now_speedway->station_name.$tail}}" target="_blank">
			                    <img src="https://img.webike.tw{{$now_speedway->img_medium}}" alt="【2017{{$now_speedway->name}}】台灣正賽時間{{$now_speedway->raced_at_local.$tail}}">
			                    <span>{{$now_speedway->station_name}}賽事概要</span>
			                </a>
		                @endif
		            </li>
		            {{\Session::get('danger')}}
		            <?php
						$now = date('Y-m-d H:i:s');
						// $now = '2017-04-03 00:00:01';
						$nums = array('冠軍', '亞軍', '季軍');
					 ?>
		            <li class="ct-site-activity-forecast-right col-xs-12 col-sm-7 col-md-7" style="padding:10px;padding: 22px 0px;border: 1px solid black;margin: 0px 10px;">
			            @if(count($choices) != 0)
			            	<div class="title-site-activity-forecast-right border-radius-2">
								<h2>您所選的前三名</h2>
							</div>
							@foreach($choices as $key => $choice)
								<?php 
									$racer = $choice->racer;
								?>
								<div class="topthree col-xs-12 col-sm-12 col-md-12">
									<div class="col-xs-12 col-sm-3 col-md-3"></div>
									<div class="col-xs-12 col-sm-2 col-md-2 text-right">
										<span class="num{{$key + 1}}">{{$nums[$key]}}</span>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-4">
										<label title="【{{$racer->number}}】{{ $racer->name }}">【{{$racer->number}}】{{ $racer->name }}</label>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-3"></div>
								</div>
							@endforeach
						@else
							@if(count($now_speedway) != 0 && strtotime($now) > strtotime($now_speedway->closested_at))
								<div class="title-site-activity-forecast-right border-radius-2">
									<h2>已過本次活動時間</h2>
								</div>
								<div class="topthree col-xs-12 col-sm-12 col-md-12">
									<div class="links" style="font-size:18px">
										您尚未參加預測<br>下次活動時間，詳見各站<a href="#calendar" title="活動時間表{{$tail}}">活動時間表</a>
									</div>
								</div>
							@elseif( !isset($customer) )
								<div class="title-site-activity-forecast-right border-radius-2">
									<h4>請先登入，並完成"完整註冊"</h4>
								</div>
								<div class="link-area col-xs-12 col-sm-12 col-md-12">
									<div class="links small" style="margin-top:25px">
										<a class="btn btn-warning" href="" title="登入會員{{$tail}}">登入會員</a>
										<a class="btn btn-danger" href="{{URL::route('customer-account-create')}}" title="免費加入會員{{$tail}}">免費加入會員</a>
									</div>
								</div>
							@elseif( isset($customer) and $customer->role_id == 1 )
								<div class="title-site-activity-forecast-right border-radius-2">
									<h4>參加本活動需完成"完整註冊"</h4>
								</div>
								<div class="link-area col-xs-12 col-sm-12 col-md-12">
									<div class="links" style="margin-top:25px">
										<a class="btn btn-danger" href="{{URL::route('customer-account-complete')}}" title="完整註冊{{$tail}}">完整註冊</a>
									</div>
								</div>
							@elseif(count($now_speedway) != 0)
				                <div class="title-site-activity-forecast-right border-radius-2">
				                    第{{$now_speedway->id}}站 {{$now_speedway->station_name}} 比賽預測活動
				                </div>
				                <form method="post">
					                <ul class="drop-site-activity-forecast">
					                    <li class="box-fix-column ">
					                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="champion">冠軍</span></div>
					                        <div class="box-fix-auto drop-site-activity-forecast-right">
					                            <select class="select2" name="my_choice[]">
					                                <option value="">預測分站第1名</option>
					                                @foreach($racers as $racer)
					                                	<option value="{{$racer->id}}">【{{$racer->number}}】{{ $racer->name }}</option>
					                                @endforeach
					                            </select>
					                        </div>
					                    </li>
					                    <li class="box-fix-column ">
					                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="runner-up">亞軍</span></div>
					                        <div class="box-fix-auto drop-site-activity-forecast-right">
					                            <select class="select2" name="my_choice[]">
					                                <option value="">預測分站第2名</option>
					                                @foreach($racers as $racer)
					                                	<option value="{{$racer->id}}">【{{$racer->number}}】{{ $racer->name }}</option>
					                                @endforeach
					                            </select>
					                        </div>
					                    </li>
					                    <li class="box-fix-column ">
					                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="third">季軍</span></div>
					                        <div class="box-fix-auto drop-site-activity-forecast-right">
					                            <select class="select2" name="my_choice[]">
					                                <option value="">預測分站第3名</option>
					                                @foreach($racers as $racer)
					                                	<option value="{{$racer->id}}">【{{$racer->number}}】{{ $racer->name }}</option>
					                                @endforeach
					                            </select>
					                        </div>
					                    </li>
					                </ul>
				                	<input class="btn btn-default btn-warning border-radius-2 btn-moto-gp-member" type="submit" value="送出答案" id="answer">
				                </form>
				            @endif
			        	@endif
			        	@if(count($now_speedway) != 0)
			        		<div class="note" style="<?php if(count($choices) != 0){ echo 'position:static;'.'margin-top:55px;';}?>"><span>本站活動截止時間：{{date('m/d Ah:i',strtotime($now_speedway->closested_at . '- 1 second'))}}止</span><a href="https://sports.bwin.com/en/sports/40/betting/motorbikes#sportId=40" title="歐洲bwin網站冠軍預測參考" target="_blank" nofollow>歐洲bwin網站冠軍預測參考</a></div>
			        	@endif
			        </li>
		        </ul>
		    </div>
		</div>
		<div id="selfranking" class="row container box-motogpevent-top">
		    <div class="title-moto-gp-event">
		        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp您的積分排行榜</span></div>
		    </div>
		    <div  class="container-moto-gp-event ct-your-scoreboard">
		        <ul class="ul-ct-your-scoreboard clearfix">
		            <li class="item-ct-your-scoreboard-left col-xs-12 col-sm-6 col-md-6 block">
		                <h2 class="title">您目前的成績統計</h2>
		                @if(isset($customer))
			                <div class="ct-item-ct-your-scoreboard-left">
			                    <div class="left col-xs-12 col-sm-5 col-md-5">
			                        您的預測結果 <br>
			                        參加次數：{{isset($stations) ? $stations : 0}}站 <br>
			                        猜對第一名：{{isset($choice_result[25]) ? $choice_result[25] : 0}}次<br>
			                        猜對第二名：{{isset($choice_result[20]) ? $choice_result[20] : 0}}次<br>
			                        猜對第三名：{{isset($choice_result[16]) ? $choice_result[16] : 0}}次<br>
			                        目前獲得點數：{{$point ? $point->points_current : 0}}點<br>
			                        <a href="javascript:void(0)">點數獲得履歷</a>
			                    </div>
			                    <div class="right col-xs-12 col-sm-7 col-md-7">
			                        <div><img src="/image/moto-gp/icon-champion.jpg" alt="name image"><h2>您心目中的總冠軍</h2></div>
			                        <?php 
										$first_score = 0;
									?>
									@foreach($my_champigns as $my_champign)
										@if($my_champign->scores >= $first_score or $first_score == 0)
											<a class="btn btn-winner" href="">{{$my_champign->racer_number}} {{$my_champign->racer_name}}</a>
										@else
											<?php continue; ?>
										@endif
										<?php $first_score = $my_champign->scores; ?>
									@endforeach
			                    </div>
		                	</div>
		                @else
		                	<div class="ct-item-ct-your-scoreboard-left">
			                    <div class="left col-xs-12 col-sm-5 col-md-5">
			                    </div>
			                    <div class="right col-xs-12 col-sm-7 col-md-7">
			                        <div><img src="/image/moto-gp/icon-champion.jpg" alt="name image"><h2>您心目中的總冠軍</h2></div>
			                    </div>
		                	</div>
		                @endif
		            </li>
		            <li class="item-ct-your-scoreboard-right col-xs-12 col-sm-6 col-md-6 block">
		                <h2>其他會員預測的冠軍支持度</h2>
		                <div class="table-info-moto-gp">
		                	@if($other_champigns and count($other_champigns))
			                    <ul class="ul-table-info-moto-gp">
			                    	<?php $total_champion = 0 ?>
									@foreach($other_champigns as $other_chamign)
										<?php 
										 	$fk_champion = $other_chamign->champion;
										 	$total_champion += $fk_champion;
										 ?>
				                        <li>
				                            <span class="border-radius-2">{{$other_chamign->racer_number}} {{$other_chamign->racer_name}}</span>
				                            <div class="num-percent"></div>
				                            <div class="percent">
				                                <div class="ct-percent" style="width:0%;display:none">{{$fk_champion}}</div>
				                            </div>
				                        </li>
				                    @endforeach
			                    </ul>
			                @endif
		                </div>
		            </li>
		            @if(isset($customer))
		            	@if( isset($speedway_choices) && isset($speedway_real_choices))
				            <li class="item-ct-your-scoreboard-right col-xs-12 col-sm-12 col-md-12">
						        <div class="record">
									<h2>您的預測歷史紀錄</h2>
									<div class="clearfix" id="pchistory">
										<table class="table table-bordered text-center" >
											<?php 
												$rank = array('title','第1名','第2名','第3名');
											?>
											@foreach($rank as $a)
												<tr>
													@if($a == 'title')
														@for($i = 0; $i <= 9 ; $i++)
															@if($i==0)
																<th> </th>
															@else
																<th colspan="2" class="text-center">第{{$i}}站</th>
															@endif
														@endfor
													@else
														@for($i = 0; $i <= 9 ; $i++)
															@if($i==0)
																<td>{{$a}}</td>
															@elseif($a == '第1名' && array_key_exists($i, $speedway_choices->one))
																<td>{{$speedway_choices->one[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
																		<?php $times[25] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第2名' && array_key_exists($i, $speedway_choices->two))
																<td>{{$speedway_choices->two[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
																		<?php $times[20] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第3名' && array_key_exists($i, $speedway_choices->three))
																<td>{{$speedway_choices->three[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
																		<?php $times[16] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@else
																<td>-</td>
																<td>-</td>
															@endif
														@endfor
													@endif
												</tr>
											@endforeach
										</table>
										<table class="table table-bordered text-center" >
											@foreach($rank as $a)
												<tr>
													@if($a == 'title')
														@for($i = 10; $i <= 18 ; $i++)
															@if($i==0)
																<th> </th>
															@else
																<th colspan="2" class="text-center">第{{$i}}站</th>
															@endif
														@endfor
														<th class="text-center">答對</th>
													@else
														@for($i = 10; $i <= 18 ; $i++)
															@if($i==0)
																<td>{{$a}}</td>
															@elseif($a == '第1名' && array_key_exists($i, $speedway_choices->one))
																<td>{{$speedway_choices->one[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
																		<?php $times[25] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第2名' && array_key_exists($i, $speedway_choices->two))
																<td>{{$speedway_choices->two[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
																		<?php $times[20] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第3名' && array_key_exists($i, $speedway_choices->three))
																<td>{{$speedway_choices->three[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
																		<?php $times[16] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@else
																<td>-</td>
																<td>-</td>
															@endif
														@endfor
														@if($a == '第1名')
															<td>{{$times[25]}}</td>
														@elseif($a == '第2名')
															<td>{{$times[20]}}</td>
														@else
															<td>{{$times[16]}}</td>
														@endif
													@endif
												</tr>
											@endforeach
										</table>
									</div>
									<div class="clearfix" id="mobilehistory">
										<table class="table table-bordered text-center" >
											<?php 
												$rank = array('title','第1名','第2名','第3名');
												$times = array(25 => 0, 20 => 0, 16 => 0);
											?>
											@foreach($rank as $a)
												<tr>
													@if($a == 'title')
														@for($i = 0; $i <= 18 ; $i++)
															@if($i==0)
																<th> </th>
															@else
																<th colspan="2" class="text-center">第{{$i}}站</th>
															@endif
														@endfor
														<th class="text-center">答對</th>
													@else
														@for($i = 0; $i <= 18 ; $i++)
															@if($i==0)
																<td>{{$a}}</td>
															@elseif($a == '第1名' && array_key_exists($i, $speedway_choices->one))
																<td>{{$speedway_choices->one[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->one[$i][0] == $speedway_choices->one[$i][0])
																		<?php $times[25] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第2名' && array_key_exists($i, $speedway_choices->two))
																<td>{{$speedway_choices->two[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->two[$i][0] == $speedway_choices->two[$i][0])
																		<?php $times[20] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@elseif($a == '第3名' && array_key_exists($i, $speedway_choices->three))
																<td>{{$speedway_choices->three[$i][0]}}</td>
																@if(isset($speedway_real_choices->one[$i][0]) )
																	@if($speedway_real_choices->three[$i][0] == $speedway_choices->three[$i][0])
																		<?php $times[16] += 1; ?>
																		<td>O</td>
																	@else
																		<td>X</td>
																	@endif
																@endif
															@else
																<td>-</td>
																<td>-</td>
															@endif
														@endfor
														@if($a == '第1名')
															<td>{{$times[25]}}</td>
														@elseif($a == '第2名')
															<td>{{$times[20]}}</td>
														@else
															<td>{{$times[16]}}</td>
														@endif
													@endif
												</tr>
											@endforeach
										</table>
									</div>
								</div>
							</li>
						@endif
					@endif
				</ul>
			</div>
		</div>
		<hr>
		<div class="title-moto-gp-event">
	        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp;活動討論區</span></div>
	    </div>
		<div id="event-discuss" class="row container box-motogpevent-top">
		    <div class="title-moto-gp-event">
	        	<div class="fb_area clearfix">
					<div id="fb-root"></div>
					<script>
					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.5";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					document.write('<div class="fb-like" data-href="http://'+document.location.hostname+document.location.pathname+'" data-layout="button" data-action="like" data-show-faces="false" data-share="true" style="float:right;"></div>');
					</script>
					

					<div id="fb-root"></div>
					<script>
					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));

					document.write('<div class="fb-comments" data-href="http://'+document.location.hostname+document.location.pathname+'" data-num-posts="3" data-width="100%" data-order-by="reverse_time"></div>');
					</script>
				</div>
		    </div>
		</div>
		<div id="driver-information" class="row container box-motogpevent-top">
		    <div class="title-moto-gp-event">
		        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp車手資訊及成績</span></div>
		    </div>
			<div style="overflow-x: auto;width:100%">
		        <table class="table table-bordered table-motogp-driver-information">
		            <thead>
		            <tr>
		                <th class="text-center">車號</th>
		                <th class="text-center">國籍</th>
		                <th class="text-center">車手姓名</th>
		                <th class="text-center">車隊名稱</th>
		                <th class="text-center">車輛廠牌</th>
		                <th class="text-center">生涯頒獎台</th>
		                <th class="text-center">生涯總冠軍</th>
		                <th class="text-center">2016年度排名</th>
		                <th class="text-center">2017累計積分</th>
		                <th class="text-center">2017目前排名</th>
		            </tr>
		            </thead>
		            <tbody>
		            @foreach( $racers as $key => $racer )
			            <tr>
			                <td class="text-center">{{$racer->number}}</td>
			                <td class="text-center"><img src="{{$photo_home}}country/{{$racer->country}}.png" alt="2017MotoGP【{{$racer->number}}】{{$racer->name}}國籍"></td>
			                <td><a href="{{$racer->site}}" title="2017MotoGP【{{$racer->number}}】{{$racer->name}}" target="_blank" nofollow>{{$racer->name}}</a></td>
			                <td><a href="{{$racer->team_site}}" title="2017MotoGP{{$racer->team_name}}" target="_blank" nofollow>{{$racer->team_name}}</a></td>
			                <td class="text-center">{{$racer->motor_manufacturer_name}}</td>
			                <td class="text-center">{{$racer->podium_quantity}}</td>
			                <td class="text-center">{{$racer->champion_quantity}}</td>
			                <td class="text-center">{{$racer->champion_lastyear}}</td>
			                <td class="text-center">{{$racer->total_scores ? $racer->total_scores : 0}}</td>
			                <td class="text-center">{{$racer->ranking ? $racer->ranking : '-'}}</td>
			            </tr>
		            @endforeach
		            {{-- <tr>
		                <td class="text-center">4</td>
		                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
		                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
		                <td><a href="javascript:void(0)">Ducati Team</a></td>
		                <td class="text-center">Ducati</td>
		                <td class="text-center">2,14,18</td>
		                <td class="text-center">0</td>
		                <td class="text-center">7</td>
		                <td class="text-center">171</td>
		                <td class="text-center">5</td>
		            </tr> --}}
		            {{-- <tr>
		                <td class="text-center">4</td>
		                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
		                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
		                <td><a href="javascript:void(0)">Ducati Team</a></td>
		                <td class="text-center">Ducati</td>
		                <td class="text-center">2,14,18</td>
		                <td class="text-center">0</td>
		                <td class="text-center">7</td>
		                <td class="text-center">171</td>
		                <td class="text-center">5</td>
		            </tr>
		            <tr>
		                <td class="text-center">4</td>
		                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
		                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
		                <td><a href="javascript:void(0)">Ducati Team</a></td>
		                <td class="text-center">Ducati</td>
		                <td class="text-center">2,14,18</td>
		                <td class="text-center">0</td>
		                <td class="text-center">7</td>
		                <td class="text-center">171</td>
		                <td class="text-center">5</td>
		            </tr>
		            <tr>
		                <td class="text-center">4</td>
		                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
		                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
		                <td><a href="javascript:void(0)">Ducati Team</a></td>
		                <td class="text-center">Ducati</td>
		                <td class="text-center">2,14,18</td>
		                <td class="text-center">0</td>
		                <td class="text-center">7</td>
		                <td class="text-center">171</td>
		                <td class="text-center">5</td>
		            </tr>
		            <tr>
		                <td class="text-center">4</td>
		                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
		                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
		                <td><a href="javascript:void(0)">Ducati Team</a></td>
		                <td class="text-center">Ducati</td>
		                <td class="text-center">2,14,18</td>
		                <td class="text-center">0</td>
		                <td class="text-center">7</td>
		                <td class="text-center">171</td>
		                <td class="text-center">5</td>
		            </tr> --}}
		            </tbody>
		        </table>
			</div>
		        <div class="note">
		            註1:生涯頒獎台包含MotoGP&GP500最高級距離賽事累積成績，由左至右依序為冠軍、亞軍、季軍次數。<br>
		            註2:總冠軍為MotoGP&GP500最高級距離賽事總和。<br>
		            資料來源：MotoGP <br>
		        </div>
		    </div>
		</div>
		<div class="row container box-motogpevent-top">
			@if(count($top10_score) > 0)
			    <div id="points-ranking" class="title-moto-gp-event">
			        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp積分排行</span></div>
			    </div>
			    <div class="container-moto-gp-event ct-your-scoreboard">
			        <div class="lineChart col-xs-12 col-sm-12 col-md-12">
			            <h2 class="title-chart">車手積分排行榜</h2>
			            <div id="chart" class="ct-lineChart text-center"></div>
			        </div>
			        <div class="half-groph half-groph-left col-xs-12 col-sm-6 col-md-6">
			            <h2 class="title-chart">車隊積分排行榜</h2>
			            <div id="teamBarChart" class="ct-half-groph"></div>
			        </div>
			        <div class="half-groph half-groph-left col-xs-12 col-sm-6 col-md-6">
			            <h2 class="title-chart">車廠積分排行榜</h2>
			            <div id="makerBarChart" class="ct-half-groph"></div>
			        </div>
			    </div>
			@endif
		</div>
		<div id="schedule" class="row container box-motogpevent-top">
		    <div class="title-moto-gp-event">
		        <div class="ct-title-moto-gp-event-left"><span><span class="visible-md visible-lg">2017 MotoGP</span>&nbsp 賽程表</span></div>
		    </div>
		    <div class="container-moto-gp-event ct-your-scoreboard">
		        <h1 class="font-color-red">※此時間為官方提供時間僅供參考，若臨時有異動將會再行更新。</h1>
		        <ul class="promo-calendar-page">
		        	@foreach ($speedwaies as $key => $speedway)
		        		<?php $winerGroup = $speedway->winerGroup; ?>
			            <li class="col-xs-6 col-sm-4 col-md-2">
			                <a href="{{URL::route('benefit-event-motogp-2017detail', $speedway->station_name)}}" title="2017 MotoGP {{$speedway->station_name.$tail}}" target="_blank">
			                    <figure><img src="https://img.webike.tw{{$speedway->thumbnail}}" alt="【2017{{$speedway->name}}】台灣正賽時間{{$speedway->raced_at_local.$tail}}"></figure>
			                    @if($speedway->active == 2)
				                    <label for="">比賽結果:</label>
				                    <span class="name-driver">
				                    	@foreach( $winerGroup as $key => $winer )
					                        第{{$key}}名：<br>{{$winer ? $winer->name : ''}} <br>
					                    @endforeach
				                    </span>
				                @else
				                	<span class="name-driver">
										2017 {{$speedway->station_name}}<br>
										<br>
										正賽當地時間：<br>
										{{date('m/d Ah:i',strtotime($speedway->raced_at))}}<br>
										<br>
										正賽台灣時間：<br>
										{{date('m/d Ah:i',strtotime($speedway->raced_at_local))}}<br><br>
				                	</span>
				                @endif
			                </a>
			            </li>
			        @endforeach
		        </ul>
		    </div>
		</div>
		@if(!isset($customer))
			<div class="center-box">
				<a class="btn btn-warning btn-moto-gp-member border-radius-2" href="javascript:void(0)">登入會員</a>
				<a class="btn btn-danger btn-moto-gp-member border-radius-2" href="javascript:void(0)">免費加入會員</a>
			</div>
		@endif
		<div id="myNavbar" class=" row menu-motogpevent-top">
		    <ul class="ul-menu-motogpevent-top">
		    	<li class="col-xs-6 col-sm-1 col-md-1 visible-md visible-lg"></li>
		    	<li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#site-prediction">本站活動預測</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#event-discuss">活動討論區</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#driver-information">車手資訊及成績</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#points-ranking">目前積分排行</a>
		        </li>
		        <li class="col-xs-6 col-sm-2 col-md-2">
		            <a href="#schedule">賽程表</a>
		        </li>
		        <li class="col-xs-6 col-sm-1 col-md-1 visible-md visible-lg"></li>
		    </ul>
		</div>
	</div>
@stop
@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

@if(isset($total_champion))
	<script type="text/javascript">
		var total_champion = "{{$total_champion}}";
	</script>
@endif
<script>
	var first_view = true;
	$(document).scroll(function(){
        if( first_view && $(this).scrollTop() + $(window).height() >= $(".ct-percent:last").offset().top ){
        	first_view = false;
        	peoples = 0;
			$('.ct-percent').each(function(key,element){
				peoples += parseInt($(element).text());
				percentage = Math.round(parseInt($(element).text()) / parseInt(total_champion) * 100 ) + '%';
				// $(element).text('').animate({display: block});
				$(element).text('').animate({width: percentage}, 1500);
				$(element).css('display','block');
				$(element).closest('li').find('.num-percent').html(percentage);
			});
			var h2 = $('.ct-percent').closest('.item-ct-your-scoreboard-right').find('h2');
			// alert();
			h2.text(h2.text() + '(共 '+ peoples +' 人)');
		}
	});

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var options = {
        	titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
	        lineWidth: 1,
	        // colors: ['#2864D1', '#D10501'],
	        chartArea: {
	            left: 80,
	            right: 25,
	            top: 50
	        },
	        width: 1161,
            height: 300,
	        pointSize: 6,
	        hAxis: {
	            baseline: 0,
	            slantedText: true,
		        slantedTextAngle:30,
	        	title: '賽次'
	        },
	        vAxis: {
	            viewWindow: {
	                min: 0
	            },
	            baseline: 0,
	            title: '累積積分'
	        },
	        legend: {
	            position: 'top'
	        },
	        tooltip: {
	        	isHtml: true,
	            textStyle: {
	                fontSize: 13
	            }
	        },
	        focusTarget: 'category',
	        'areaOpacity': 0.1,
	        annotations: {
				textStyle: {
					fontName: 'Times-Roman',
					fontSize: 13,
					bold: true,
					italic: true,
				}
			},
	    };

        var data = new google.visualization.DataTable();
	    data.addColumn('string', '站別');
	    
        @foreach($top10 as $racer)
        	data.addColumn('number', '【{{$racer->number}}】{{$racer->nickname}}');
        @endforeach

	    data.addRows([
	    	<?php 
				$scores = array();
				$last_top10_str = '';
		    	for($i = 1 ; $i <= 18 ; $i++){
	    			foreach($top10 as $key => $racer){
	    				if( !isset($scores[$key]) ){
	    					$scores[$key] = 0;
	    				}
	    				$top10_score->filter(function($racer_score) use($racer, $i, $key, &$scores){
	    					if($racer_score->speedway_id == $i and $racer_score->racer_id == $racer->id){
    							$scores[$key] += $racer_score->score;
	    					}
	    				});
	    			}
	    			$top10_str = implode(',', $scores);
	    			if($top10_str !== $last_top10_str){
	    				$last_top10_str = $top10_str;
	    				echo '["第'.($i).'站", '.$top10_str.'],';
	    			}
		    	}
	    	?>
        ]);

	    var formatter = new google.visualization.PatternFormat('{1}');
	    formatter.format(data, [0, 1], 1);
	    formatter.format(data, [0, 2], 2);

	    var chart = new google.visualization.LineChart(document.getElementById('chart'));
	    chart.draw(data, options);

	    var data = new google.visualization.DataTable();
	    data.addColumn('string', '車隊');
	    data.addColumn('number', '積分');
	    data.addRows([
    			@foreach( $teamData as $team )
    				['{{$team->team_name}}', {{$team->sum}}],
    			@endforeach
    		]);
    	var view = new google.visualization.DataView(data);
    	var options = {
				titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
				chartArea: {
		            left: 120,
		            right: 25,
		            top: 25,
		            height: 220
		        },
				width: 470,
				height: 300,
				bar: {groupWidth: "60%"},
				legend: { position: "none" },
				vAxis: {
		            viewWindow: {
		                min: 0
		            },
		            baseline: 0,
		            title: ''
		        },
		        hAxis: {
		        	title: '',
		        	slantedText: true,
		            slantedTextAngle:30
		        }
	    };
	    var teamBarChart = new google.visualization.BarChart(document.getElementById("teamBarChart"));
	    teamBarChart.draw(view, options);

	    var data = new google.visualization.DataTable();
	    data.addColumn('string', '車廠');
	    data.addColumn('number', '積分');
	    data.addRows([
    			@foreach( $makerData as $maker )
    				['{{$maker->motor_manufacturer_name}}', {{$maker->sum}}],
    			@endforeach
    		]);
    	var view = new google.visualization.DataView(data);
    	var options = {
				titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
				chartArea: {
		            left: 100,
		            right: 25,
		            top: 25,
		            height: 220
		        },
				width: 470,
				height: 300,
				bar: {groupWidth: "60%"},
				legend: {
		            position: 'none'
		        },
		        vAxis: {
		            viewWindow: {
		                min: 0
		            },
		            baseline: 0,
		            title: ''
		        },
		        hAxis: {
		        	title: '',
		        	slantedText: true,
		            slantedTextAngle:30
		        }
	    	};
	    var makerBarChart = new google.visualization.BarChart(document.getElementById("makerBarChart"));
	    makerBarChart.draw(view, options);
    }

	function checkingRacer(){
	var select_racer = [];
		$('.select2 option:selected').each(function(key,element){
			if($(element).val()){
				select_racer.push($(element).val());
				console.log(select_racer);
			}
		});
		$('.select2 option').attr('disabled',false);
		$('.select2 option').each(function(key,element){
			if($.inArray( $(element).val(), select_racer ) >= 0 && !$(element).is(':selected')){
				$(element).attr('disabled',true);
				$(".select2").select2();
			}
		});
	}
	checkingRacer();
	$('.select2').change(function(){
		checkingRacer();
	});

	$('#answer').click(function(){
		var validate_fail = false;
		$('.error').remove();
		$('.select2 option:selected').each(function(key,element){
			if(!$(element).val()){
				$(element).closest('.box-fix-column .drop-site-activity-forecast-right').after('<div><span class="error" style="color:red;">請選擇車手!</span></div>');
				validate_fail = true;
			}
		});
		if(validate_fail){
			return false;
		}

		// $(this).prop('disabled', true).css('cursor', 'no-drop');
		$(this).submit();
		// _fbq.push(['track', 'MotoGP2017']);
		// _gaq.push(['_trackEvent','link', 'click', 'MotoGP2017']);
	});
	
</script>
@stop
