@extends('response.layouts.2columns')
@section('style')
    <meta property="og:url" content="{{URL::route('benefit-event-motogp-2017detail', $thisSpeedway->station_name)}}"/>
    <link rel="stylesheet" type="text/css" href="{{assetRemote('css/benefit/event/motogpevent.css')}}">
    <style>
    @media screen and (max-width: 767px) {
        .title-moto-gp-event .ct-title-moto-gp-event-left span {
            min-width: 154px !important;
        }
        .box-about-the-track-left {
            padding:0px;
        }
        .box-about-the-track-right {
            padding:0px;
            margin-top:15px;
        }
        .item-ct-television-broadcast-left {
            padding:0px;
        }
        .item-ct-television-broadcast-right {
            padding:0px;
            margin-top:15px;
        }
        .ct-oem-part-left .box-page-group .zoom-image{
            display:block;
        }
        .ct-oem-part-left .box-page-group .zoom-image img {
            width:100%;
        }
        #chart {
            overflow-x: auto
        }
        #makerBarChart {
            overflow-x: auto
        }
        #teamBarChart {
            overflow-x: auto
        }
    }
    .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left{
            width:100%;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left span{
            width:90%;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:after{
            content:none;
        }
        .returnbtn .title-moto-gp-event .ct-title-moto-gp-event-left:before{
            width:10%;
        }
        .returnbtn .returnbtnhref{
            display:block;
        }
        .returnbtn .returnbtnhref:hover {
            opacity: 0.7;
            display:block;
            color:white;
        }
        .right-side-box{
            padding-right: 5%;
        }
        .left-side-box{
            padding-left: 5%;
        }
        .gp-box{
            margin-top: 40px;
            border: 2px #777 solid;
        }
        .gp-col{
            display: flex;
            justify-content: center;
            flex-direction: column;
            padding: 3% 2%;
        }
        .gp-box-content{
            display: flex;
            background: #e7e7e7;
        }
        .gp-box-content h3{
            font-weight: bold;
            font-size: 1.2rem;
        }
        .gp-box-content ul li{
            text-decoration: none;
            list-style: none;
        }
        .gp-box-title {
            height:40px;
        }
        .gp-box-title h2,.gp-box-title h1{
            position: relative;
        }
        .gp-box-title h2 span,.gp-box-title h1 span{
            color: #fff;
            position: absolute;
            left: 2%;
            line-height: 30px;
            font-size: 1.2rem;
            font-weight: bold;
            height:40px !important;
            padding-top:5px !important;
        }
        .gp-box-title h2 img,.gp-box-title h1 img{
            width:30%;
            height: 40px !important;
        }
        .gp-box-border{
            border:1px #ccc solid;
        }
        .box-green{
            background: rgba(0, 189, 3, 0.6);
        }
        .box-green1{
            background: rgba(0, 189, 3, 1);
        }
        .box-white{
            background: #fff;
        }
        .btn-winner {
            border: 2px solid #e61e25;
            color: #e61e25;
            width: 100%;
        }
        .btn-winner:hover {
            background-color: #e61e25;
            color: #fff;
        }
        .gp-col .box-white .result{
            float:none;
        }
        .gp-col .right-side-box .box-white li {
            display:block;
            clear:both;
            margin:2%;
        }
        .gp-col .right-side-box .box-white .border-radius-2{
            text-align: center;
            padding: 5px 10px;
            width: 40%;
            border: 2px solid #333333;
            font-weight: bold;
            text-transform: uppercase;
            min-height: 35px;
            float:left;
        }
        .gp-col .right-side-box .box-white .num-percent {
            width: 20%;
            line-height: 35px;
            float: left;
            text-align: center;
        }
        .gp-col .right-side-box .box-white .percent {
            margin: 12px 0;
            width: 40%;
            height: 10px;
            background-color: #efefef;
            float: left;
        }
        .gp-col .right-side-box .box-white .ct-percent {
            height: 10px;
            background-color: #ff6600;
            float: left;
        }
        .box-light-blue{
            background: rgba(3, 79, 137, 0.6);
        }
        .box-light-blue1{
            background: rgba(3, 79, 137, 1);
        }
        .box-light-red{
            background:rgba(227, 17, 16 ,0.6);
        }
        .box-light-red1{
            background:rgba(227, 17, 16 ,1);
        }
        .box-white{
            background: #fff;
        }
        .nail-box{
            position: relative;
        }
        .fly-box{
            position: absolute;
            width:450px;
        }
        .box-tall{
            height:100%;
        }
        .nail-left{
            left:5%;
        }
        .nail-right {
            right:10%;
        }
        .nail-top {
            top: 8.5%;
        }
        .col-md-6 .nail-left{
            left:10%;
        }
        /*.nail-bottom {
            bottom: 8.5%;
        }*/
        .gp-box .line {
            height:5px;
            float:none;
        }
        .box-white .title-site-activity-forecast-right{
            font-size: 1.2rem;
            padding: 5px;
            width: 100%;
            background-color: #0066c0;
            color: #fff;
            text-align: center;
        }
        .box-white .note{
            width: 100%;
            padding: 5px;
            float: left;
            text-align:center;
            background-color: #efefef;
            bottom: 0;
        }
        .nail-box .info {
            float:right;
        }
        .gp-box-content .info-table {
            position: absolute;
            width: 100%;
            z-index: 2;
        }
        .speedwayinfo li {
            margin: 2% 0;
        }
        .row span {
            font-size: 1rem;
        }
        .allteam li {
            margin: 5px 0px 0px;
            font-size: 1rem;
            color:#fff;
        }
        .box-light-blue{
            background: rgba(0, 155, 238, 0.8);
        }
        .box-light-red2{
            background:rgba(255, 120, 0, 0.87);
        }
        .box-green{
            background: rgba(0, 176, 0, 0.8);
        }
        .note1 {
            border: 1px solid #ddd;
            width:100%;
            background: white;
            padding:2%;
        }
        .info-table table{
            margin-bottom:0px;
        }
        .box-motogpevent-top {
            overflow-x: auto;
        }
        .row {
            margin-bottom:40px;
        }
        .btn-bottom {
            margin-bottom:40px;
        }
        .btn-bottom .btn-danger{
            width:200px !important;
            height:40px !important;
            padding-top:9px;
        }
        #event-discuss .gp-col .gp-box-border{
            padding:20px;
        }
    </style>
@stop
@section('left')
    <?php 
        $photo_home = 'https://img.webike.tw/assets/images/benefit/event/motoGp/2016/';
    ?>
    <div class="row oem-part-page box-fix-column col-xs-12 col-sm-12 col-md-12">
        <div class="ct-oem-part-left box-fix-with">
            <div class="box-page-group text-center">
                <div class="title-box-page-group">
                    <h3>2017 MotoGP</h3>
                </div>
                <div class="ct-box-page-group">
                    <ul class="ul-menu-ct-box-page-group">
                        @foreach ($speedwaies as $key => $speedway)
                            @if($speedway->id == $thisSpeedway->id)
                                <li class="active">
                                    <a><span>{{'第'.($key + 1).'站 '.$speedway->name}}</span></a>
                                </li>
                            @else
                                <li>
                                    <a href="{{URL::route('benefit-event-motogp-2017detail', $speedway->station_name)}}" title="{{'2017 MotoGP 第'.($key + 1).'站 '.$speedway->name}}"><span>{{'第'.($key + 1).'站 '.$speedway->name}}</span></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="box-page-group">
                <a href="{{URL::route('benefit-event-motogp-2017')}}" title="2017 MotoGP 冠軍大預測{{$tail}}">
                    <figure class="zoom-image"><img src="{{assetRemote('image/benefit/event/motogp/banner1.png')}}" alt="2017 MotoGP 冠軍預測活動{{$tail}}"></figure>
                </a>

            </div>
        </div>
    </div>
@stop
@section('right')
<div class="row">
    <?php
        $color = array('box-light-blue','box-green','box-light-red2','box-light-red2','box-light-blue','box-green','box-green','box-light-red2','box-light-blue','box-light-blue','box-green','box-light-red2','box-light-red2','box-light-blue','box-green','box-green','box-light-red2','box-light-blue');
        $key = $thisSpeedway->id - 1;
    ?>
    <div class="col-md-6 col-sm-12 col-xs-12" style="padding-right:0px">
        <div class="gp-box thisspeedwayinfo" style="margin-top:0px !important;min-height: 369px;">
            <div class="gp-box-title">
                <h1 class="clearfix">
                    <span style="float:left;padding:0 23px;position:static;background:black">{{$thisSpeedway->station_name.'賽事概要'}}</span>
                    <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP {{$thisSpeedway->station_name.'賽事概要'}}" style="width:99px;height:30px;float:left">
                </h1>
            </div>
            <div class="gp-box-content clearfix col-xs-block col-sm-block {{$color[$key]}}" style="height:89%">
                <div class="col-md-12 col-sm-6 col-xs-12 gp-col">
                    <div class="box-white gp-box-border" style="height:100%;padding:2%">
                        <div class="box-about-the-track-right col-xs-12 col-sm-12 col-md-12 speedwayinfo">
                            <ul>
                                <li>
                                    <h3>賽道名稱：{{$thisSpeedway->speedway_name.'('.$thisSpeedway->speedway_name_en.')'}}</h3>
                                </li>
                                <li>
                                    <h3>國家：{{$thisSpeedway->country}}</h3>
                                </li>
                                <li>
                                    <h3>賽道長度：{{$thisSpeedway->speedway_length}}</h3>
                                </li>
                                <li>
                                    <h3>彎道數：{{$thisSpeedway->speedway_curve}}</h3>
                                </li>
                                <li>
                                    <h3>比賽圈數：{{$thisSpeedway->turns_num}}</h3>
                                </li>
                                <li>
                                    <h3>比賽總里程：{{$thisSpeedway->distance}}</h3>
                                </li>
                                <li>
                                    <h3>台灣正賽時間：{{$thisSpeedway->raced_at_local}}</h3>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12" style="padding-left:0px">
        <div class="gp-box thisspeedway" style="margin-top:0px !important;border-left:0px">
            <div class="gp-box-content clearfix col-xs-block col-sm-block">
                @if(count($now_speedway) != 0)
                    <div style="position:absolute;width:40%;height:100%;padding:5%;top:0px;overflow:auto" class="{{$color[$key]}}">
                        <img src="{{assetRemote('image/benefit/event/motogp/speedway/'.$thisSpeedway->name.'.jpg')}}" alt="【2017 MotoGP {{$thisSpeedway->name}}】台灣正賽時間{{$thisSpeedway->raced_at_local.$tail}}"><br>
                        @if($thisSpeedway->active == 2)
                            <ul style="color:white" class="allteam allteamresult">
                                <li style="padding:4px 0px">
                                    <h3>{{$thisSpeedway->name}}</h3>
                                </li>
                                <li>
                                    <span class="font-bold">
                                        比賽結果:
                                    </span>
                                </li>
                                @if(is_null($thisSpeedway->numberOne) && is_null($thisSpeedway->numberTwo) && is_null($thisSpeedway->numberThree))
                                    <li>
                                        <span>
                                            第1名：
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            第2名：
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            第3名：
                                        </span>
                                    </li>
                                @else
                                    <li>
                                        <span>
                                            第1名：{{$thisSpeedway->numberOne['name']}}
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            第2名：{{$thisSpeedway->numberTwo['name']}}
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            第3名：{{$thisSpeedway->numberThree['name']}}
                                        </span>
                                    </li>
                                @endif
                            </ul>
                        @else
                            <ul style="color:white" class="allteam">
                                <li>
                                    <h3>{{$thisSpeedway->name}}</h3>
                                </li>
                                <li>
                                    <span style="color:white;font-weight:bold;">
                                        正賽當地時間：
                                    </span>
                                </li>
                                <li>
                                    <span style="color:white;">
                                        {{date('m/d h:i A',strtotime($thisSpeedway->raced_at))}}
                                    </span>
                                </li>
                                <li>
                                    <span style="color:white;font-weight:bold;">
                                        正賽台灣時間：
                                    </span>
                                </li>
                                <li>
                                    <span style="color:white;">
                                        {{date('m/d h:i A',strtotime($thisSpeedway->raced_at_local))}}
                                    </span>
                                </li>
                            </span>
                        @endif
                    </div>
                    <a title="{{$thisSpeedway->station_name.$tail}}">
                        <img src="{{assetRemote('image/benefit/event/motogp/speedway/banner/'.$thisSpeedway->name.'.jpg')}}" alt="【2017 MotoGP {{$thisSpeedway->name}}】台灣正賽時間{{$thisSpeedway->raced_at_local.$tail}}">
                        {{-- <span>{{$thisSpeedway->station_name}}賽事概要</span> --}}
                    </a>
                @endif
               {{--  <img src="{{assetRemote('image/benefit/event/motogp/speedway/banner/'.$thisSpeedway->name.'.jpg')}}" alt="【2017{{$thisSpeedway->name}}】台灣正賽時間{{$thisSpeedway->raced_at_local.$tail}}"> --}}
            </div>
        </div>
    </div>
    @if($thisSpeedway->active == 2 and $stationNew)
        <div class="col-md-12 col-sm-12 col-xs-12" id="event-discuss">
            <div class="gp-box">
                <div class="gp-box-title">
                    <h2>
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 {{$thisSpeedway->name}} 賽後報導</span>
                        <img src="{{assetRemote('image/moto-gp/2017/title-bg.png')}}" alt="2017 MotoGP {{$thisSpeedway->station_name.'賽後報導'}}">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    <div class="col-md-12 col-sm-12 col-xs-12 gp-col">
                        <div class="box-white gp-box-border">
                            <div class="entry-header thumbnail-below-post col-xs-4 col-sm-4 col-md-4 ">
                                <div class="entry-thumbnail">
                                    @foreach($stationNewsimg as $key => $newsImage)
                                        @if($key == 0)
                                            <a class="zoom-image rounded-img" href="{{$stationNew->guid}}" target="_blank">
                                                <img class="img-responsive rounded-img box" src="{{$newsImage}}" alt="2017 MotoGP {{$thisSpeedway->name}} 賽後報導">
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-xs-8 col-sm-8 col-md-8 post-content content-below-post">
                                <a class="title-content-below-post" href="{{$stationNew->guid}}" target="_blank">
                                    <h2 class="entry-title a-link-blue-color"> {{$stationNew->post_title}} </h2>
                                </a>
                                <span>
                                    {!!$stationNewsContent!!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif($thisSpeedway->active == 1)
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="gp-box">
                <div class="gp-box-title" class="clearfix">
                    <h2 class="clearfix">
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 電視轉播資訊</span>
                        <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP {{$thisSpeedway->name}} 電視轉播資訊" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    {{-- <div class="col-md-12 col-sm-12 col-xs-12 gp-col" style="float:none">
                        <h3>MotoGP正賽</h3>
                    </div> --}}
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col" >
                        <div class="box-white gp-box-border" style="padding: 22px;<?php /*if($thisSpeedway->formal_view_datetime){echo 'height: 185.97px';}*/?>">
                            <h3>MotoGP排位賽</h3><br>
                            @if($thisSpeedway->ranking_view_datetime)
                                <a href="{{$thisSpeedway->ranking_view}}" title="2017 MotoGP排位賽 FOX2 直播" nofollow="true" target="_blank">{{$thisSpeedway->ranking_view_datetime}} <br> <span>FOX2體育台 (直撥)MotoGP排位賽: {{$thisSpeedway->station_name}}</span></a>
                            @else
                                尚未公布
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col" >
                        <div class="box-white gp-box-border" style="padding: 22px;">
                            <h3>MotoGP正賽</h3><br>
                            @if($thisSpeedway->formal_view_datetime)
                                <a href="{{$thisSpeedway->formal_view}}" title="2017 MotoGP正賽 FOX 2 HD 直播" nofollow="true" target="_blank">{{$thisSpeedway->formal_view_datetime}} <br> <span>FOX2體育台 (直播)MotoGP正賽: {{$thisSpeedway->station_name}}</a></span>
                                {{-- <br><br> --}}
                                {{-- <a href="{{$thisSpeedway->formal_view}}" title="2017 MotoGP正賽 FOX 3 HD 直播" nofollow="true" target="_blank">{{$thisSpeedway->formal_view_datetime}} <br> <span>FOX3體育台 (直播)MotoGP正賽: {{$thisSpeedway->station_name}}</span></a> --}}
                            @else
                                尚未公布
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="col-md-12 col-sm-12 col-xs-12" id="event-discuss">
        <div class="gp-box">
            <div class="gp-box-title" class="clearfix">
                <h2 class="clearfix">
                    <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 賽道資訊</span>
                    <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP {{$thisSpeedway->name}} 賽道資訊" style="width:99px;height:30px;float:left">
                </h2>
            </div>
            <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
            <div class="gp-box-content clearfix col-xs-block col-sm-block">
                <div class="col-md-12 col-sm-12 col-xs-12 gp-col">
                    <div class="box-white gp-box-border">
                        <?php 
                            function recordExchange($string)
                            {
                                $values = $string;
                                if(strpos($values, ',') !== false){
                                    $values = explode(',', $values);
                                }else{
                                    $values = array('-', '-');
                                }
                                return $values;
                            }
                        ?>
                        @if($thisSpeedway->pole_rider_2017)
                            <div class="row container box-motogpevent-top">
                                <div class="title-main-box title-main-box-moto-gp">
                                    <h2>2017 {{$thisSpeedway->station_name}}成績</h2>
                                </div>
                                <table class="table table-bordered table-motogp-detail">
                                    <thead class="table-moto-boder-none">
                                    <tr>
                                        <th class="text-center"></th>
                                        <th class="text-center"><span>車手姓名</span></th>
                                        <th class="text-center"><span>成績</span></th>
                                        <th class="text-center"><span>時速</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center"><span>桿位</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->pole_rider_2017}}</span></td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->pole_record_2017);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr class="active">
                                        <td class="text-center"><span>冠軍</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->first_2017}}</span></td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->first_record_2017);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>亞軍</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->second_2017}}</span></td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->second_diff_2017);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    <tr class="active">
                                        <td class="text-center"><span>季軍</span></td>
                                        <td class="text-center"><span>{{$thisSpeedway->third_2017}}</span></td>
                                        <?php 
                                            $values = recordExchange($thisSpeedway->third_diff_2017);
                                        ?>
                                        <td class="text-center"><span>{{$values[0]}}</span></td>
                                        <td class="text-center"><span>{{$values[1]}}</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                         @endif
                        <div class="row container box-motogpevent-top">
                            <div class="title-main-box title-main-box-moto-gp">
                                <h2>賽道紀錄</h2>
                            </div>
                            <table class="table table-bordered table-motogp-detail">
                                <thead class="table-moto-boder-none">
                                <tr>
                                    <th class="text-center"></th>
                                    <th class="text-center"><span>紀錄保持人</span></th>
                                    <th class="text-center"><span>紀錄成績</span></th>
                                    <th class="text-center"><span>時速</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center"><span>最快單圈</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->max_singleturn_racer}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->max_singleturn);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                <tr class="active">
                                    <td class="text-center"><span>正賽紀錄</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->max_record_racer}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->max_record);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><span>最快排位</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->max_qualifying_racer}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->max_qualifying);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                <tr class="active">
                                    <td class="text-center"><span>最快時速</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->max_speed_racer}}</span></td>
                                    <td class="text-center"><span>-</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->max_speed}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row container box-motogpevent-top">
                            <div class="title-main-box title-main-box-moto-gp">
                                <h2>2016 {{$thisSpeedway->station_name}}成績</h2>
                            </div>
                            <table class="table table-bordered table-motogp-detail">
                                <thead class="table-moto-boder-none">
                                <tr>
                                    <th class="text-center"></th>
                                    <th class="text-center"><span>車手姓名</span></th>
                                    <th class="text-center"><span>成績</span></th>
                                    <th class="text-center"><span>時速</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center"><span>桿位</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->pole_rider_2016}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->pole_record_2016);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                <tr class="active">
                                    <td class="text-center"><span>冠軍</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->first_2016}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->first_record_2016);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><span>亞軍</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->second_2016}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->second_diff_2016);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                <tr class="active">
                                    <td class="text-center"><span>季軍</span></td>
                                    <td class="text-center"><span>{{$thisSpeedway->third_2016}}</span></td>
                                    <?php 
                                        $values = recordExchange($thisSpeedway->third_diff_2016);
                                    ?>
                                    <td class="text-center"><span>{{$values[0]}}</span></td>
                                    <td class="text-center"><span>{{$values[1]}}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($top10_score) > 0)
        <div class="col-md-12 col-sm-12 col-xs-12" id="points-ranking">
            <div class="gp-box totalrank" style="position: relative;">
                <div class="gp-box-title" class="clearfix">
                    <h2>
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 目前積分排行</span>
                        <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP {{$thisSpeedway->name}} 目前積分排行" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    <div class="rankcontainer" style="height:422px">
                        <div class="info-table" style="width:100%;top: 10%;width:100%">
                            <div class="container-moto-gp-event ct-your-scoreboard">
                                <div class="lineChart col-xs-12 col-sm-12 col-md-12">
                                    <h3>車隊積分排行榜</h3>
                                    <div id="chart" class="ct-lineChart text-center"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12 nail-box no-padding">
                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col box-tall box-light-blue"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
                    </div>
                </div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col">
                        <h3>車隊積分排行榜</h3>
                        <div class="box-white gp-box-border">
                            <div id="teamBarChart" class="ct-half-groph"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 nail-box no-padding">
                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col box-tall box-light-blue">
                            <div class="fly-box nail-left nail-bottom">
                                <h3>車廠積分排行榜</h3>
                                <div class="box-white gp-box-border">
                                    <div id="makerBarChart" class="ct-half-groph"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 gp-col"></div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-12 col-sm-12 col-xs-12" id="driver-information">
        <div class="gp-box">
            <div class="gp-box-title" class="clearfix">
                <h2 class="clearfix">
                    <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP 車手資訊及成績</span>
                    <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP 車手資訊及成績" style="width:99px;height:30px;float:left">
                </h2>
            </div>
            <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
            <div class="gp-box-content clearfix col-xs-block col-sm-block">
                <div class="col-md-12 col-sm-12 col-xs-12 nail-box no-padding">
                    <div class="fly-box nail-left info-table-container info-table">
                        <table class="table table-bordered table-motogp-driver-information">
                            <thead>
                            <tr>
                                <th class="text-center"><span>車號</span></th>
                                <th class="text-center"><span>國籍</span></th>
                                <th class="text-center"><span>車手姓名</span></th>
                                <th class="text-center"><span>車隊名稱</span></th>
                                <th class="text-center"><span>車輛廠牌</span></th>
                                <th class="text-center"><span>生涯頒獎台</span></th>
                                <th class="text-center"><span>生涯總冠軍</span></th>
                                <th class="text-center"><span>2016年度排名</span></th>
                                <th class="text-center"><span>2017累計積分</span></th>
                                <th class="text-center"><span>2017目前排名</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $racers as $key => $racer )
                                <tr>
                                    <td class="text-center"><span>{{$racer->number}}</span></td>
                                    <td class="text-center"><span><img src="{{$photo_home}}country/{{$racer->country}}.png" alt="2017MotoGP【{{$racer->number}}】{{$racer->name}}國籍"></span></td>
                                    <td><a href="{{$racer->site}}" title="2017MotoGP【{{$racer->number}}】{{$racer->name}}" target="_blank" nofollow>{{$racer->name}}</a></td></td>
                                    <td><a href="{{$racer->team_site}}" title="2017MotoGP{{$racer->team_name}}" target="_blank" nofollow>{{$racer->team_name}}</a></td></td>
                                    <td class="text-center"><span>{{$racer->motor_manufacturer_name}}</span></td>
                                    <td class="text-center"><span>{{$racer->podium_quantity}}</span></td>
                                    <td class="text-center"><span>{{$racer->champion_quantity}}</span></td>
                                    <td class="text-center"><span>{{$racer->champion_lastyear}}</span></td>
                                    <td class="text-center"><span>{{$racer->total_scores ? $racer->total_scores : 0}}</span></td>
                                    <td class="text-center"><span>{{$racer->ranking ? $racer->ranking : '-'}}</span></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                         <div class="note1" style="overflow-y: auto">
                            <span>
                                註1:生涯頒獎台包含MotoGP&GP500最高級距離賽事累積成績，由左至右依序為冠軍、亞軍、季軍次數。<br>
                                註2:總冠軍為MotoGP&GP500最高級距離賽事總和。<br>
                                資料來源：MotoGP <br>
                            </span>
                        </div>
                    </div>      
                    <div class="col-md-3 col-sm-3 col-xs-12 gp-col box-tall box-light-blue info">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 gp-col contestant-info">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($thisSpeedway->active == 2)
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="gp-box">
                <div class="gp-box-title" class="clearfix">
                    <h2 class="clearfix">
                        <span style="float:left;padding:0 23px;position:static;background:black">2017 MotoGP {{$thisSpeedway->name}} 電視轉播資訊</span>
                        <img src="{{assetRemote('image/benefit/event/motogp/title-bg.png')}}" class="visible-md visible-lg" alt="2017 MotoGP {{$thisSpeedway->name}} 電視轉播資訊" style="width:99px;height:30px;float:left">
                    </h2>
                </div>
                <div class=" col-md-12 col-sm-12 col-xs-12 line box-light-blue1"></div>
                <div class="gp-box-content clearfix col-xs-block col-sm-block">
                    {{-- <div class="col-md-12 col-sm-12 col-xs-12 gp-col" style="float:none">
                        <h3>MotoGP正賽</h3>
                    </div> --}}
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col" >
                        <div class="box-white gp-box-border" style="padding: 22px;<?php if($thisSpeedway->formal_view_datetime){echo 'height: 185.97px';}?>">
                            <h3>MotoGP排位賽</h3><br>
                            @if($thisSpeedway->ranking_view_datetime)
                                <a href="{{$thisSpeedway->ranking_view}}" title="2017 MotoGP排位賽 FOX3 直播" nofollow="true" target="_blank">{{$thisSpeedway->ranking_view_datetime}} <br> <span>FOX3體育台 (直撥)MotoGP排位賽: {{$thisSpeedway->station_name}}</span></a>
                            @else
                                尚未公布
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gp-col" >
                        <div class="box-white gp-box-border" style="padding: 22px;">
                            <h3>MotoGP正賽</h3><br>
                            @if($thisSpeedway->formal_view_datetime)
                                <a href="{{$thisSpeedway->formal_view}}" title="2017 MotoGP正賽 FOX 2 HD 直播" nofollow="true" target="_blank">{{$thisSpeedway->formal_view_datetime}} <br> <span>FOX2體育台 (直播)MotoGP正賽: {{$thisSpeedway->station_name}}</a></span><br><br>
                                {{-- <a href="{{$thisSpeedway->formal_view}}" title="2017 MotoGP正賽 FOX 3 HD 直播" nofollow="true" target="_blank">{{$thisSpeedway->formal_view_datetime}} <br> <span>FOX3體育台 (直播)MotoGP正賽: {{$thisSpeedway->station_name}}</span></a> --}}
                            @else
                                尚未公布
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
<div class="text-center btn-bottom">
    <a class="btn btn-danger" href="{{route('benefit-event-motogp-2017')}}" title="2017 MotoGP 預測總冠軍首頁">MotoGP預測總冠軍首頁</a>
</div>
@stop
@section('script')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    @if(isset($total_champion))
        <script type="text/javascript">
            var total_champion = "{{$total_champion}}";
        </script>
    @endif
<script type="text/javascript">
    $('.link-shopping').removeClass('navbar-active');
    $(window).resize(function(){
        fiyBoxResize();
        resetTableContainer();
        // var height = $('.discussfb').height() + 50;
        // $('.discuss').height(height);
    });
    $(document).ready(function(){
        fiyBoxResize();
        resetTableContainer();
    });

    var first_view = true;
    $(document).scroll(function(){
        if( first_view && $(this).scrollTop() + $(window).height() >= $(".ct-percent:last").offset().top ){
            first_view = false;
            peoples = 0;
            $('.ct-percent').each(function(key,element){
                peoples += parseInt($(element).text());
                percentage = Math.round(parseInt($(element).text()) / parseInt(total_champion) * 100 ) + '%';
                // $(element).text('').animate({display: block});
                $(element).text('').animate({width: percentage}, 1500);
                $(element).css('display','block');
                $(element).closest('li').find('.num-percent').html(percentage);
            });
            var h2 = $('.ct-percent').closest('.item-ct-your-scoreboard-right').find('h2');
            // alert();
            h2.text(h2.text() + '(共 '+ peoples +' 人)');
        }
    });

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var options = {
            titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
            lineWidth: 1,
            // colors: ['#2864D1', '#D10501'],
            chartArea: {
                left: 80,
                right: 25,
                top: 50
            },
            width: 900,
            height: 300,
            pointSize: 6,
            hAxis: {
                baseline: 0,
                slantedText: true,
                slantedTextAngle:30,
                title: '賽次'
            },
            vAxis: {
                viewWindow: {
                    min: 0
                },
                baseline: 0,
                title: '累積積分'
            },
            legend: {
                position: 'top'
            },
            tooltip: {
                isHtml: true,
                textStyle: {
                    fontSize: 13
                }
            },
            focusTarget: 'category',
            'areaOpacity': 0.1,
            annotations: {
                textStyle: {
                    fontName: 'Times-Roman',
                    fontSize: 13,
                    bold: true,
                    italic: true,
                }
            },
        };

        var data = new google.visualization.DataTable();
        data.addColumn('string', '站別');
        
        @foreach($top10 as $racer)
            data.addColumn('number', '【{{$racer->number}}】{{$racer->name}}');
        @endforeach

        data.addRows([
            <?php 
                $scores = array();
                $last_top10_str = '';
                for($i = 1 ; $i <= 18 ; $i++){
                    foreach($top10 as $key => $racer){
                        if( !isset($scores[$key]) ){
                            $scores[$key] = 0;
                        }
                        $top10_score->filter(function($racer_score) use($racer, $i, $key, &$scores){
                            if($racer_score->speedway_id == $i and $racer_score->racer_id == $racer->id){
                                $scores[$key] += $racer_score->score;
                            }
                        });
                    }
                    $top10_str = implode(',', $scores);
                    if($top10_str !== $last_top10_str){
                        $last_top10_str = $top10_str;
                        echo '["第'.($i).'站", '.$top10_str.'],';
                    }
                }
            ?>
        ]);

        var formatter = new google.visualization.PatternFormat('{1}');
        formatter.format(data, [0, 1], 1);
        formatter.format(data, [0, 2], 2);

        var chart = new google.visualization.LineChart(document.getElementById('chart'));
        chart.draw(data, options);

        var data = new google.visualization.DataTable();
        data.addColumn('string', '車隊');
        data.addColumn('number', '積分');
        data.addRows([
                @foreach( $teamData as $team )
                    ['{{$team->team_name}}', {{$team->sum}}],
                @endforeach
            ]);
        var view = new google.visualization.DataView(data);
        var options = {
                titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
                chartArea: {
                    left: 120,
                    right: 25,
                    top: 25,
                    height: 220
                },
                width: 430,
                height: 300,
                bar: {groupWidth: "60%"},
                legend: { position: "none" },
                vAxis: {
                    viewWindow: {
                        min: 0
                    },
                    baseline: 0,
                    title: ''
                },
                hAxis: {
                    title: '',
                    slantedText: true,
                    slantedTextAngle:30
                }
        };
        var teamBarChart = new google.visualization.BarChart(document.getElementById("teamBarChart"));
        teamBarChart.draw(view, options);

        var data = new google.visualization.DataTable();
        data.addColumn('string', '車廠');
        data.addColumn('number', '積分');
        data.addRows([
                @foreach( $makerData as $maker )
                    ['{{$maker->motor_manufacturer_name}}', {{$maker->sum}}],
                @endforeach
            ]);
        var view = new google.visualization.DataView(data);
        var options = {
                titleTextStyle: {color: 'black', fontName: 'Arial', fontSize: '18', fontWidth: 'normal'},
                chartArea: {
                    left: 100,
                    right: 25,
                    top: 25,
                    height: 220
                },
                width: 430,
                height: 300,
                bar: {groupWidth: "60%"},
                legend: {
                    position: 'none'
                },
                vAxis: {
                    viewWindow: {
                        min: 0
                    },
                    baseline: 0,
                    title: ''
                },
                hAxis: {
                    title: '',
                    slantedText: true,
                    slantedTextAngle:30
                }
            };
        var makerBarChart = new google.visualization.BarChart(document.getElementById("makerBarChart"));
        makerBarChart.draw(view, options);
    }
    var height = $('.thisspeedway').height();
    $('.thisspeedwayinfo').height(height);

    function resetTableContainer(){
        var height = $('.info-table-container').height() + 85;
        $('.contestant-info').closest('.nail-box').height(height);
    }

    function fiyBoxResize(){
        $('.fly-box').each(function(){
            $(this).width($(this).closest('.nail-box').width() * 0.90);
        });
    }
</script>
@stop
