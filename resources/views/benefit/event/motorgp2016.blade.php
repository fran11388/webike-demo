@extends('response.layouts.1column')
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Webike Taiwan</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/bootstrap.min.css">
    <link href="/css/benefit/event/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/slick.css">
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/slick-theme.css">
    <!-- Custom Fonts -->
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/font.css" media="none" onload="media='screen'">
    <!-- Sass covert-->
    <link rel="stylesheet" href="/css/benefit/event/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/main.css">
    <link rel="stylesheet" type="text/css" href="/css/benefit/event/motogpevent.css">
    <!-- Custom CSS -->
</head>
<body>
<div class="all-page">
	<section id="contents top-shopping contents-all-page" class="top-shopping">
        <div class="container">
            <div class="breadcrumb-mobile"><ol class="breadcrumb breadcrumb-product-detail">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">摩托百貨</a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">會員中心</a></li>
                <li class="breadcrumb-item active"><span>商品交期待查清單</span></li>
            </ol></div>
            <div class="motogpevent-main-page product-detail col-xs-12 col-sm-12 col-md-12">
                <div class="banner-motogp-top">
                    <img src="/image/moto-gp/banner-top.jpg" alt="banner moto GP">
                </div>
                <div id="myNavbar" class=" row menu-motogpevent-top">
                    <ul class="ul-menu-motogpevent-top">
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#event-description">活動說明</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#guess-the-way">猜獎方式</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#site-prediction">本站預測</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#driver-information">車手資訊</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#points-ranking">積分排行</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#schedule">賽程表</a>
                        </li>
                    </ul>
                </div>
                <div id="event-description" class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event title-moto-gp-event-border">
                        <div class="ct-title-moto-gp-event-left"><span>2016 MotoGP 冠軍大預測</span></div>
                        <div class="ct-title-moto-gp-event-right">
                            <iframe name="f15dd4e91fd3d5c" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.5/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F_IDqWBiKXtV.js%3Fversion%3D42%23cb%3Df48194bbf97c3%26domain%3Dwww.webike.tw%26origin%3Dhttp%253A%252F%252Fwww.webike.tw%252Ff2cc3e9386b3aa4%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2Fwww.webike.tw%2Fbenefit%2Fevent%2Fmotogp%2F2016&amp;layout=button&amp;locale=zh_TW&amp;sdk=joey&amp;share=true&amp;show_faces=false" style="border: none; visibility: visible; width: 82px; height: 20px;" class=""></iframe>
                        </div>
                    </div>
                    <div class="container-moto-gp-event2">
                        【活動說明】預測2016MotoGP各分站前三名，猜對即可贏得免費點數，並有機會贏得SHOEI X-14次世代賽車安全帽及300元禮券。<br>
                        【活動時間】自2016/03/11日起開放答題，每站都可參加，詳見各 <a href="javascript:void(0)">站活動時間表</a>。<br>
                        【活動資格】凡是「Webike-摩托百貨」完整會員皆可參加，若還沒註冊的會員，請按此 <a href="javascript:void(0)"> 免費加入</a>。<br>
                        【活動解答】MotoGP各站結束後 隔日，我們會公布每站正確答案，結果以MotoGP官方結果為準。<br>
                        【活動規範】每個會員每站僅限答題一次，提交答案後無法修改，請小心回答。得獎者我們會以電話進行會員身分資料確認，若查證冒名者將會取消資格，並依據<a href="javascript:void(0)">會員規約</a>刪除會員帳號<br>
                    </div>
                </div>
                <div class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event title-moto-gp-event-border">
                        <div><h1>【活動獎勵】</h1></div>
                    </div>
                    <div class="row container-moto-gp-event2">
                        <ul class="ct-activitiesa-award">
                            <li>
                                <figure class="row"><img src="/image/moto-gp/img-activities-step1.png" alt="Name image"></figure>

                                <h2>猜對分站前三名現金點數免費拿</h2>
                                <span>1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。 <br>
2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。 <br>
3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱現金點數說明 <br>
4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在點數獲得及使用履歷中查詢。
                                    1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。 <br>
2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。 <br>
3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱現金點數說明 <br>
4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在點數獲得及使用履歷中查詢。
                                </span>
                            </li>
                            <li>
                                <figure class="row"><img src="/image/moto-gp/img-activities-step1.png" alt="Name image"></figure>

                                <h2>猜對分站前三名現金點數免費拿</h2>
                                <span>1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。
2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。
3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱現金點數說明
4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在點數獲得及使用履歷中查詢。</span>
                            </li>
                            <li>
                                <figure class="row"><img src="/image/moto-gp/img-activities-step1.png" alt="Name image"></figure>

                                <h2>猜對分站前三名現金點數免費拿</h2>
                                <span>1.各分站活動期間選擇您心目中的前三名，若您的預測與比賽結果相同，則可贏得現金點數。
2.各站答對第一名25點、第二名20點、第三名16點，全部18站答對最多可獲得1098點。
3.現金點數1點=1元，可折抵「Webike摩托百貨」中所有商品金額，並且可以累計，詳細說明參閱現金點數說明
4.答案公布後次個工作天，我們會將答對的總點數匯入會員個人帳戶，會員可在點數獲得及使用履歷中查詢。</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="guess-the-way" class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span>猜獎方式說明</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-guessing-method">
                        <ul class="ul-ct-guessing-method">
                            <li><img src="/image/moto-gp/img-step-01.jpg" alt="image step 1"></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
                            <li><img src="/image/moto-gp/img-step-02.jpg" alt="image step 2"></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
                            <li><img src="/image/moto-gp/img-step-03.jpg" alt="image step 3"></li>
                        </ul>
                    </div>
                </div>
                <div id="site-prediction" class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span>本站活動預測</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-site-activity-forecast">
                        <ul class="ul-ct-site-activity-forecast">
                            <li class="ct-site-activity-forecast-left col-xs-12 col-sm-5 col-md-5">
                                <a href="javascript:void(0)">
                                    <img src="/image/moto-gp/img-time-moto-gp.jpg" alt="Name images">
                                    <span>瓦倫西亞站賽事概要</span>
                                </a>
                            </li>
                            <li class="ct-site-activity-forecast-right col-xs-12 col-sm-7 col-md-7">
                                <div class="title-site-activity-forecast-right border-radius-2">
                                    第18站 瓦倫西亞站 比賽預測活動
                                </div>
                                <ul class="drop-site-activity-forecast">
                                    <li class="box-fix-column col-xs-12 col-sm-4 col-md-4">
                                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="champion">冠軍</span></div>
                                        <div class="box-fix-auto drop-site-activity-forecast-right">
                                            <select class="select2">
                                                <option>Dropdown Example</option>
                                                <option>Dropdown Example 2</option>
                                                <option>Dropdown Example 3</option>
                                                <option>Dropdown Example 4</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="box-fix-column col-xs-12 col-sm-4 col-md-4">
                                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="runner-up">亞軍</span></div>
                                        <div class="box-fix-auto drop-site-activity-forecast-right">
                                            <select class="select2">
                                                <option>Dropdown Example</option>
                                                <option>Dropdown Example 2</option>
                                                <option>Dropdown Example 3</option>
                                                <option>Dropdown Example 4</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="box-fix-column col-xs-12 col-sm-4 col-md-4">
                                        <div class="box-fix-with drop-site-activity-forecast-left"><span class="third">季軍</span></div>
                                        <div class="box-fix-auto drop-site-activity-forecast-right">
                                            <select class="select2">
                                                <option>Dropdown Example</option>
                                                <option>Dropdown Example 2</option>
                                                <option>Dropdown Example 3</option>
                                                <option>Dropdown Example 4</option>
                                            </select>
                                        </div>
                                    </li>

                                </ul>
                                <a class="btn btn-default btn-warning border-radius-2 btn-moto-gp-member" href="">送出答案</a>
                                <div class="note"><span>本站活動截止時間：11/11 PM11:59止</span><a href="">歐洲bwin網站冠軍預測參考</a></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span>您的積分排行榜</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-your-scoreboard">
                        <ul class="ul-ct-your-scoreboard">
                            <li class="item-ct-your-scoreboard-left col-xs-12 col-sm-6 col-md-6">
                                <h2 class="title">您目前的成績統計</h2>
                                <div class="ct-item-ct-your-scoreboard-left">
                                    <div class="left col-xs-5 col-sm-5 col-md-5">
                                        您的預測結果 <br>
                                        參加次數：0站 <br>
                                        猜對第一名：0次<br>
                                        猜對第二名：0次<br>
                                        猜對第三名：0次<br>
                                        目前獲得點數：0點<br>
                                        <a href="javascript:void(0)">點數獲得履歷</a>
                                    </div>
                                    <div class="right col-xs-7 col-sm-7 col-md-7">
                                        <div><img src="/image/moto-gp/icon-champion.jpg" alt="name image"><h2>您心目中的總冠軍</h2></div>
                                        <a class="btn btn-winner" href="">46 Valentino Rossi</a>
                                    </div>
                                </div>
                            </li>
                            <li class="item-ct-your-scoreboard-right col-xs-12 col-sm-6 col-md-6">
                                <h2>其他會員預測的冠軍支持度(共 9922 人)</h2>
                                <div class="table-info-moto-gp">
                                    <ul class="ul-table-info-moto-gp">
                                        <li>
                                            <span class="border-radius-2">46 Valentino Rossi</span>
                                            <div class="num-percent">10%</div>
                                            <div class="percent">
                                                <div class="ct-percent" style="width: 10%"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="border-radius-2">46 Valentino Rossi</span>
                                            <div class="num-percent">30%</div>
                                            <div class="percent">
                                                <div class="ct-percent" style="width: 30%"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="border-radius-2">46 Valentino Rossi</span>
                                            <div class="num-percent">70%</div>
                                            <div class="percent">
                                                <div class="ct-percent" style="width: 70%"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="border-radius-2">46 Valentino Rossi</span>
                                            <div class="num-percent">50%</div>
                                            <div class="percent">
                                                <div class="ct-percent" style="width: 50%"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="border-radius-2">46 Valentino Rossi</span>
                                            <div class="num-percent">40%</div>
                                            <div class="percent">
                                                <div class="ct-percent" style="width: 40%"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-right">
                            <iframe name="f15dd4e91fd3d5c" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.5/plugins/like.php?action=like&amp;app_id=&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F_IDqWBiKXtV.js%3Fversion%3D42%23cb%3Df48194bbf97c3%26domain%3Dwww.webike.tw%26origin%3Dhttp%253A%252F%252Fwww.webike.tw%252Ff2cc3e9386b3aa4%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2Fwww.webike.tw%2Fbenefit%2Fevent%2Fmotogp%2F2016&amp;layout=button&amp;locale=zh_TW&amp;sdk=joey&amp;share=true&amp;show_faces=false" style="border: none; visibility: visible; width: 82px; height: 20px;" class=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="row container box-motogpevent-top">
                    <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="2"></div>
                </div>
                <div id="driver-information" class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span>2016 MotoGP 車手資訊及成績</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-your-scoreboard">
                        <table class="table table-bordered table-motogp-driver-information">
                            <thead>
                            <tr>
                                <th class="text-center">車號</th>
                                <th class="text-center">國籍</th>
                                <th class="text-center">車手姓名</th>
                                <th class="text-center">車隊名稱</th>
                                <th class="text-center">車輛廠牌</th>
                                <th class="text-center">生涯頒獎台</th>
                                <th class="text-center">生涯總冠軍</th>
                                <th class="text-center">2015年度排名</th>
                                <th class="text-center">2016累計積分</th>
                                <th class="text-center">2016目前排名</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
                                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
                                <td><a href="javascript:void(0)">Ducati Team</a></td>
                                <td class="text-center">Ducati</td>
                                <td class="text-center">2,14,18</td>
                                <td class="text-center">0</td>
                                <td class="text-center">7</td>
                                <td class="text-center">171</td>
                                <td class="text-center">5</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
                                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
                                <td><a href="javascript:void(0)">Ducati Team</a></td>
                                <td class="text-center">Ducati</td>
                                <td class="text-center">2,14,18</td>
                                <td class="text-center">0</td>
                                <td class="text-center">7</td>
                                <td class="text-center">171</td>
                                <td class="text-center">5</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
                                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
                                <td><a href="javascript:void(0)">Ducati Team</a></td>
                                <td class="text-center">Ducati</td>
                                <td class="text-center">2,14,18</td>
                                <td class="text-center">0</td>
                                <td class="text-center">7</td>
                                <td class="text-center">171</td>
                                <td class="text-center">5</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
                                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
                                <td><a href="javascript:void(0)">Ducati Team</a></td>
                                <td class="text-center">Ducati</td>
                                <td class="text-center">2,14,18</td>
                                <td class="text-center">0</td>
                                <td class="text-center">7</td>
                                <td class="text-center">171</td>
                                <td class="text-center">5</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
                                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
                                <td><a href="javascript:void(0)">Ducati Team</a></td>
                                <td class="text-center">Ducati</td>
                                <td class="text-center">2,14,18</td>
                                <td class="text-center">0</td>
                                <td class="text-center">7</td>
                                <td class="text-center">171</td>
                                <td class="text-center">5</td>
                            </tr>
                            <tr>
                                <td class="text-center">4</td>
                                <td class="text-center"><img src="/image/oempart/icon-flag01.jpg" alt=""></td>
                                <td><a href="javascript:void(0)">Andrea Dovizioso</a></td>
                                <td><a href="javascript:void(0)">Ducati Team</a></td>
                                <td class="text-center">Ducati</td>
                                <td class="text-center">2,14,18</td>
                                <td class="text-center">0</td>
                                <td class="text-center">7</td>
                                <td class="text-center">171</td>
                                <td class="text-center">5</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="note">
                            註1:生涯頒獎台包含MotoGP&GP500最高級距離賽事累積成績，由左至右依序為冠軍、亞軍、季軍次數。<br>
                            註2:總冠軍為MotoGP&GP500最高級距離賽事總和。<br>
                            資料來源：MotoGP <br>
                        </div>
                    </div>
                </div>
                <div id="points-ranking" class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span>2016 MotoGP 賽程表</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-your-scoreboard">
                        <div class="lineChart col-xs-12 col-sm-12 col-md-12">
                            <h2 class="title-chart">車手積分排行榜</h2>
                            <div class="ct-lineChart">
                                <img src="/image/moto-gp/img-linechart.jpg" alt="">
                            </div>
                        </div>
                        <div class="half-groph half-groph-left col-xs-6 col-sm-6 col-md-6">
                            <h2 class="title-chart">車手積分排行榜</h2>
                            <div class="ct-half-groph">
                                <img src="/image/moto-gp/img-linechart02.jpg" alt="">
                            </div>
                        </div>
                        <div class="half-groph half-groph-left col-xs-6 col-sm-6 col-md-6">
                            <h2 class="title-chart">車手積分排行榜</h2>
                            <div class="ct-half-groph">
                                <img src="/image/moto-gp/img-linechart03.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="schedule" class="row container box-motogpevent-top">
                    <div class="title-moto-gp-event">
                        <div class="ct-title-moto-gp-event-left"><span>2016 MotoGP 賽程表</span></div>
                    </div>
                    <div class="container-moto-gp-event ct-your-scoreboard">
                        <h1 class="font-color-red">※此時間為官方提供時間僅供參考，若臨時有異動將會再行更新。</h1>
                        <ul class="promo-calendar-page">
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage1.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage2.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage3.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage4.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage5.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage6.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage1.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage2.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage3.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage4.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage5.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage6.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage1.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage2.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage3.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage4.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage5.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                            <li class="col-xs-6 col-sm-4 col-md-2">
                                <a href="">
                                    <figure><img src="/image/moto-gp/2016motogp_small_stage6.jpg" alt="Name images"></figure>
                                    <label for="">比賽結果:</label>
                                    <span class="name-driver">
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                        第1名：Jorge Lorenzo <br>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="center-box"><a class="btn btn-warning btn-moto-gp-member border-radius-2" href="javascript:void(0)">登入會員</a><a class="btn btn-danger btn-moto-gp-member border-radius-2" href="javascript:void(0)">免費加入會員</a></div>
                <div id="myNavbar" class=" row menu-motogpevent-top">
                    <ul class="ul-menu-motogpevent-top">
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#event-description">活動說明</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#guess-the-way">猜獎方式</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#site-prediction">本站預測</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#driver-information">車手資訊</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#points-ranking">積分排行</a>
                        </li>
                        <li class="col-xs-6 col-sm-2 col-md-2">
                            <a href="#schedule">賽程表</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

</div>