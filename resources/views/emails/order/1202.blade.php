<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div id="mail" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->customer->realname }}先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳 筱玲，感謝您本次的訂購。</p>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 90px; float: none; display: block; position: relative; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <div style="text-align: center; font-size: 19px; margin-top: 15px;"> <b>	交期確認通知
                </b> 
            </div>
            <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="clearfix">
                <div style="position: absolute;top:10px;left:20px;"> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"  style="background: url(/image/tmp_webike_tw_logos.png);background-position: -200px 0px;background-repeat: no-repeat;height: 80px;width: 180px;display: block;">  </a> </div>
            </div>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 26px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">訂單編號:#{{ $order->increment_id }}</p>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 113px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 760px; height: 113px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="customer">
                <tbody>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 62px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 105px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">會員名稱 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 125px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->customer->realname }}</td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 103px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">連絡電話 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                            T: {{ $order->address->phone }}<br>
                            M: {{ $order->address->mobile }}<br>
                        </td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 102px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">訂購時間 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 117px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                            {{ date("Y年m月d日<br>H時i分s秒", strtotime($order->created_at)) }}
                        </td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 112px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">付款方式 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 142px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{!! '3/6期信用卡分期(首期215元/其他期214元)' !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="items">
                <thead>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th width="5%">No.</th>
                        <th width="15%">品牌</th>
                        <th width="25%">商品名稱</th>
                        <th width="20%">商品編號</th>
                        <th width="10%">數量</th>
                        <th width="10%">單價</th>
                        <th width="15%">小計</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->items as $item_number =>$item )
                        @if($product = $item->product)
                            <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                                <td align="center">{{ $item_number + 1 }}</td>
                                <td align="center">{{ $product->manufacturer->name }}</td>
                                <td>{{ $product->name }}
                                    @if ($product->group_code)
                                        @foreach ($product->options as &$option)
                                            {{ $option->name }}
                                        @endforeach
                                    @endif
                                </td>
                                <td align="center">{{ $product->model_number }}</td>
                                <td align="center">{{ $item->quantity }}</td>
                                <td align="center">NT${{ number_format($item->final_price) }}</td>
                                <td align="center">NT${{ number_format($item->final_price * $item->quantity) }}</td>
                            </tr>
                        @else
                            <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                                <td align="center">{{ $item_number + 1 }}</td>
                                <td align="center"></td>
                                <td>{{ $item->name }}</td>
                                <td align="center"></td>
                                <td align="center">{{ $item->quantity }}</td>
                                <td align="center">NT${{ number_format($item->final_price) }}</td>
                                <td align="center">NT${{ number_format($item->final_price * $item->quantity) }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 122px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox clearfix">
            <div style="float:left;width:470px;">
                <label>備註:</label> 
                <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 470px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->comment }}</p>
            </div>
            <div style="float:right">
                <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 280px; height: 122px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="total">
                    <tbody>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">商品費用合計</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->grand_total) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">運費</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->shipping_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">手續費</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->payment_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">折扣</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">-NT${{ number_format($order->coupon_amount + $order->rewardpoints_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">總計</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->subtotal) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 760px; height: 31px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <p style="font-size: 17px; font-family: sans-serif; font-weight: 700; color: rgb(255, 0, 0); letter-spacing: 1px; text-align: center; width: 760px; height: 21px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="point">您所訂購的商品預計的交貨期為 {{$order->jp_delivery}} 個工作天。</p>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 90px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <ul>
                <li>請注意:</li>
                <li>1.以上交期為所有商品備齊一同寄出的交貨時間，出貨前我們將會以e-mail通知您。</li>
                <li>2.若是有突發狀況導致商品延誤、缺貨或是停售時，我們將主動在第一時間告知您。</li>
                <li>3.工作天數計算不包含台灣或日本的例假日。</li>
            </ul>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox"> 您也可以至" <a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a> "查看目前的訂單狀態，若您有其他問題與建議請" <a href="{{route('customer-service-information')}}">聯絡我們</a>"。 </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        </div>
    </div>
</body>