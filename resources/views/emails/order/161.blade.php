<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 383px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" id="mail">
        <div> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"> <img src="{{ assetRemote('assets/images/shared/webike_shop_logo.png') }}" alt="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"> </a> </div>
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 15px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="gap">非常感謝您使用<a href="{{route('mitumori')}}">「Webike-摩托百貨」未登錄商品查詢系統。</a></p>
            <br> 
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">您的查詢已正常受理，本郵件為確認信箱地址而自動發送的郵件。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">我們已經向廠商查詢商品資訊，約1~2個工作天我們會回覆至您的信箱。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">如超過3個工作天仍未收到回覆，請您來信service@webike.tw</p>
        </div>
        <div class="description">
            ****************************************************************************************************  
            <ul class="indent">
                <li style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 750px; height: 16px; float: none; display: list-item; position: static; margin-left: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">查詢編號： 
                {{$mitumori->estimate_code}}</li>
                @foreach($mitumori->items as $key => $item)
                <li style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 750px; height: 16px; float: none; display: list-item; position: static; margin-left: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">商品明細({{$key + 1 }}){{$item->syouhin_maker}} {{$item->syouhin_name}} x {{$item->syouhin_kosuu}}</li>
                @endforeach
            </ul>
            **************************************************************************************************** 
        </div>
        <div class="tips">  </div>
        <hr>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        <hr>
    </div>
</body>