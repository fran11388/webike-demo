<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <?php 
    $date = '2017-02-02';
    $h1 = 941;
    $org_h1 = 901;
    $h2 = 114;
    $org_h2 = 64;
    $org = '目前我們正在確認商品的交期(約1~2個工作天)。<br> 若需要更改或是調整訂單內容，或者超過3個工作天尚未收到”交期確認通知信”，請您來信 order@webike.tw，或是於上班時間來電' .OFFICE_EMAIL_PHONE.'我們隨即幫您處理。';
    $rewrite = '我們正在確認商品的交期。<font color="red" style="font-size: 15px">由於2017年1月27日(星期五) ~ 2017年2月1日(星期三)，適逢農曆春節連續假期，台灣各廠商將有6~10天的連續假期，商品交期回覆與出貨進度將有所延誤。進口商品因配合海關休假，停止進口報關作業。出貨進度也將有延誤的情況，請您耐心等候，不便之處敬請見諒。</font><br> 交期確認後我們會發送”交期確認通知信”，請注意您的信箱。若需要更改或是調整訂單內容，請您至客服聯絡頁面<a href="http://www.webike.tw/customer/service/proposal" target="_blank">http://www.webike.tw/customer/service/proposal</a>留言將由值班人員為您處理您的問題。';
    ?>
    <div id="mail" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; min-height: {{ strtotime(date('Y-m-d')) < strtotime($date) ? $h1 : $org_h1 }}px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: {{ strtotime(date('Y-m-d')) < strtotime($date) ? $h2 : $org_h2 }}px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->customer->realname }}先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。<br> 您選擇的付款方式"{{ $order->payment->name }}"，{!! strtotime(date('Y-m-d')) < strtotime($date) ? $rewrite : $org !!}<br> </p>
            <br> 
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">以下是您的訂單明細：</p>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; min-height: 125px; float: none; display: block; position: relative; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <div style="text-align: center; font-size: 19px; margin-top: 15px;"> <b> 新訂單({{ $order->payment->name }}) </b> </div>
            <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="clearfix">
                <div style="position: absolute;top:10px;left:20px;"> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"  style="background: url({{assetRemote('/image/tmp_webike_tw_logos.png')}});background-position: -200px 0px;background-repeat: no-repeat;height: 80px;width: 180px;display: block;">  </a> </div>
            </div>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; min-height: 26px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">訂單編號:#{{ $order->increment_id }}</p>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; min-height: 207px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 760px; height: 207px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="customer">
                <tbody>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 62px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 96px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">會員名稱 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 125px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->customer->realname }}</td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 102px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">連絡電話 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> T: {{ $order->address->phone }}<br> M: {{ $order->address->mobile }}<br> </td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 101px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">訂購時間 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 117px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> {!! date("Y年m月d日<br>H時i分s秒", strtotime($order->created_at)) !!} </td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 97px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">統一編號 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 142px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->address->company }} {{ $order->address->vat_number }}</td>
                        <?php 
                            $payment_result = paymentFormat($order);
                            if($payment_result['install']){
                                $payment_name = $payment_result['install_info'];
                            }else{
                                $payment_name = $payment_result['name'];
                            }
                        ?>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 107px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">付款方式 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 133px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $payment_name }}</td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 98px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">收件人 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 154px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->address->firstname }}</td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 98px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">配送地址 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 649px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" colspan="5">{{ $order->address->zipcode }} , {{ $order->address->county }} {{ $order->address->district }} {{$order->address->address}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; min-height: 146px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="items">
                <thead>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th width="5%">No.</th>
                        <th width="15%">品牌</th>
                        <th width="25%">商品名稱</th>
                        <th width="20%">商品編號</th>
                        <th width="10%">數量</th>
                        <th width="10%">單價</th>
                        <th width="15%">小計</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->items as $item_number =>$item )
                        <?php
                            $service = new \Ecommerce\Service\OrderService;
                            $detail = $service->getItemDetail($item);
                        ?>
		                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
		                        <td align="center">{{ $item_number + 1 }}</td>
		                        <td align="center">{{ $detail->manufacturer_name }}</td>
		                        <td>{{ $detail->product_name }}
		                         	@if ($detail->product_options)
                                        <?php $options = explode('|', $detail->product_options); ?>
						                @foreach ($options as $option)
                                        <?php  $option_value = explode('$',$option) ?>
                                            <br/>{!! $option_value[1] !!}
                                      @endforeach
						          	@endif
					          	</td>
		                        <td align="center">{{ $item->model_number }}</td>
		                        <td align="center">{{ $item->quantity }}</td>
		                        <td align="center">NT${{ number_format($item->price) }}</td>
		                        <td align="center">NT${{ number_format($item->price * $item->quantity) }}</td>
	                    </tr>
	                   
                    @endforeach
                </tbody>
            </table>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; min-height: 122px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox clearfix">
            <div style="float:left;width:470px;">
                <label>備註:</label> 
                <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 470px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->comment }}</p>
            </div>
            <div style="float:right">
                <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 280px; height: 122px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="total">
                    <tbody>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">商品費用合計</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->grand_total) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">運費</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->shipping_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">手續費</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->payment_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">折扣</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">-NT${{ number_format($order->coupon_amount + $order->rewardpoints_amount + $order->celebration_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">總計</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->subtotal) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox"> 您也可以至" <a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a> "查看目前的訂單狀態，若您有其他問題與建議請" <a href="{{route('customer-service-information')}}">聯絡我們</a>"。 </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        </div>
    </div>
</body>