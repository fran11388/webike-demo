<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div id="mail" style="height: auto;">
        <div class="title"> <label>  「Webike-摩托百貨」：商品售完通知# 訂單編號  </label> </div>
        <div class="content">
            <p>[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。</p>
            <br>  
        </div>
        <div class="borderbox" style="position: relative; height:90px;border:1px black solid;width:760px;margin:-1px auto; padding: 10px;">
            <div style="text-align: center; font-size: 19px; margin-top: 15px;"> <b> 商品售完通知(信用卡) </b> </div>
            <div class="clearfix">
                <div style="position: absolute;top:10px;left:20px;"> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"  style="background: url(/image/tmp_webike_tw_logos.png);background-position: -200px 0px;background-repeat: no-repeat;height: 80px;width: 180px;display: block;">  </a> </div>
            </div>
        </div>
        <style type="text/css"> #mail .point{ text-align: left; } </style>
        <div class="borderbox" style="padding:10px;">
            <p>訂單編號:#</p>
        </div>
        <div class="borderbox">
            <table class="customer">
                <tbody>
                    <tr>
                        <th>會員名稱 :</th>
                        <td>江 百裕</td>
                        <th>連絡電話 :</th>
                        <td> T: 06-2313577<br> M: 0982509753<br> </td>
                        <th>訂購時間 :</th>
                        <td> 2014年5月12日<br> 下午1時32分07秒 </td>
                    </tr>
                    <tr>
                        <th>付款方式 :</th>
                        <td>信用卡</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="borderbox">
            <div class="point">非常抱歉，您本次訂單其中有：</div>
            <br> 
            <table class="items">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="15%">品牌</th>
                        <th width="25%">商品名稱</th>
                        <th width="20%">商品編號</th>
                        <th width="10%">數量</th>
                        <th width="10%">單價</th>
                        <th width="15%">小計</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">1</td>
                        <td align="center">HONDA</td>
                        <td>皮革吊環式鑰匙圈</td>
                        <td align="center">0SYEPN98KF</td>
                        <td align="center">1</td>
                        <td align="center">NT$ 222</td>
                        <td align="center">NT$ 222</td>
                    </tr>
                    <tr>
                        <td align="center">1</td>
                        <td align="center">HONDA</td>
                        <td>皮革吊環式鑰匙圈</td>
                        <td align="center">0SYEPN98KF</td>
                        <td align="center">1</td>
                        <td align="center">NT$ 222</td>
                        <td align="center">NT$ 222</td>
                    </tr>
                    <tr>
                        <td align="center">1</td>
                        <td align="center">HONDA</td>
                        <td>皮革吊環式鑰匙圈</td>
                        <td align="center">0SYEPN98KF</td>
                        <td align="center">1</td>
                        <td align="center">NT$ 222</td>
                        <td align="center">NT$ 222</td>
                    </tr>
                    <tr>
                        <td align="center">1</td>
                        <td align="center">HONDA</td>
                        <td>皮革吊環式鑰匙圈</td>
                        <td align="center">0SYEPN98KF</td>
                        <td align="center">1</td>
                        <td align="center">NT$ 222</td>
                        <td align="center">NT$ 222</td>
                    </tr>
                    <tr>
                        <td align="center">1</td>
                        <td align="center">HONDA</td>
                        <td>皮革吊環式鑰匙圈</td>
                        <td align="center">0SYEPN98KF</td>
                        <td align="center">1</td>
                        <td align="center">NT$ 222</td>
                        <td align="center">NT$ 222</td>
                    </tr>
                </tbody>
            </table>
            <br> 
            <div class="point">上述商品廠商回復已"售完"，無法訂購。</div>
        </div>
        <div class="borderbox clearfix"> 由於廠商庫存變動快速，以至於商品還來不及下架，我們將取消這筆訂單<br> 造成您不便再次致上最誠摯的歉意。<br> 如果您想要購買其他商品，請您到<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>再次選購。<br> 若是您本次消費有使用折價券進行折抵，以至於折價券失效無法使用，請您下次結帳時於備註欄註明，我們會為您修改訂單折扣。<br> </div>
        <div class="borderbox"> 您也可以至" <a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a> "查看目前的訂單狀態，若您有其他問題與建議請" <a href="{{route('customer-service-information')}}">聯絡我們</a>"。 </div>
        <div class="borderbox">
            <div class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        </div>
    </div>
</body>