<p><img src="{{ assetRemote('assets/images/shared/webike_shop_logo.png') }}" alt="「Webike-摩托百貨」" /></p>
<div>親愛的「Webike-摩托百貨」會員您好 &nbsp;</div>
<div>&nbsp;</div>
<div>感謝您對「Webike-摩托百貨」的惠顧與支持。</div>
<div>&nbsp;</div>
<div>不知您對我們的服務滿意嗎?購入的商品喜歡嗎?</div>
<div>&nbsp;</div>
<div>現在回到「Webike-摩托百貨」寫下您的商品使用心得，每則評論我們送出最多50點購物點數，歡迎您與其他會員分享。</div>
<div>=====================================================================================</div>
<div><span style="font-size: xx-large;">&nbsp; &nbsp;<a href="{{URL::route('review-step')}}" target="_blank">「Webike-摩托百貨」會員商品評論說明</a></span></div>
<div>=====================================================================================</div>
<div><a href="{{URL::route('review-step')}}" target="_blank"><img src="https://www.webike.tw/assets/images/banner/review_step.png" style="border: 1px solid black;" alt="簡單3步驟，現金點數輕鬆拿！" /></a></div>
<div>&nbsp;</div>
<div>※您所投稿的使用感想在經過工作人員確認後將會刊登在該商品頁面以供其他顧客參考。</div>
<div>&nbsp;</div>
<div>如經採用我們將會贈送您最多50點作為回饋。</div>
<div><span><strong><span style="font-size: large;">※一般商品評論20點(無照片)，優秀商品評論(有附照片)50點，此點數可以累積。<br/>※正廠零件請務必附上照片，無照片將會審核不通過，請注意。</span></strong></span></div>
<div>&nbsp;</div>
<div>如您所購入的商品因其他因素而無刊登在「Webike-摩托百貨」上、或已下架之情況</div>
<div>&nbsp;</div>
<div>則無法投稿，敬請見諒。</div>
<div>=====================================================================================</div>
<div><span style="font-size: xx-large;">「Webike-摩托百貨」售後服務調查問卷</span></div>
<div>=====================================================================================</div>
<div>&nbsp;</div>
<div>「Webike-摩托百貨」非常重視顧客的意見，請您撥空填寫以下問卷，您的寶貴意見是我們成長動力!!</div>
<div>&nbsp;</div>
<div>&nbsp; 再次感謝您的寶貴意見，期待您再次光臨「Webike-摩托百貨」!!</div>
<div>=====================================================================================</div>
<div>&nbsp;</div>
<div>
<div><form action="https://docs.google.com/forms/d/12GSYbpFDIkzwWYy1HwY6nmqfFsVsK2zuXJtWBstiXIU/formResponse" method="POST" target="_blank">
<div>
<div dir="ltr">
<div>
<div style="font-weight: bold;">1.收到商品的包裝狀況(可複選)</div>
<ul class="ss-choices" role="group" aria-label="1.收到商品的包裝狀況(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1918875294" value="&#27794;&#26377;&#21839;&#38988;" id="group_1918875294_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1918875294" value="&#21253;&#35037;&#32025;&#31665;&#23610;&#23544;&#19981;&#21512;&#36969;" id="group_1918875294_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">包裝紙箱尺寸不合適</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1918875294" value="&#22635;&#20805;&#29289;&#20445;&#35703;&#24615;&#19981;&#36275;" id="group_1918875294_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">填充物保護性不足</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1918875294" value="&#21253;&#35037;&#32025;&#31665;&#30772;&#25613;" id="group_1918875294_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">包裝紙箱破損</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1918875294" value="&#21830;&#21697;&#25613;&#22750;" id="group_1918875294_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">商品損壞</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1918875294" value="&#20986;&#21697;&#24288;&#21830;&#21253;&#35037;&#35373;&#35336;&#19981;&#33391;" id="group_1918875294_6" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">出品廠商包裝設計不良</span>
</label></li></ul>
<div class="error-message" id="1484309890_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1128525183"><div style="font-weight: bold;">2.宅配業者服務品質(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="2.宅配業者服務品質(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.990954084" value="&#27794;&#26377;&#21839;&#38988;" id="group_990954084_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.990954084" value="&#37197;&#36865;&#26085;&#26399;&#12289;&#26178;&#38291;&#24310;&#35492;" id="group_990954084_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">配送日期、時間延誤</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.990954084" value="&#37197;&#36865;&#21069;&#27794;&#26377;&#20808;&#32879;&#32097;&#65288;&#36008;&#21040;&#20184;&#27454;&#65289;" id="group_990954084_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">配送前沒有先聯絡（貨到付款）</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.990954084" value="&#37197;&#36865;&#20154;&#21729;&#26381;&#21209;&#24907;&#24230;&#19981;&#22909;" id="group_990954084_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">配送人員服務態度不好</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.990954084" value="&#37197;&#36865;&#21830;&#21697;&#26377;&#21839;&#38988;" id="group_990954084_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">配送商品有問題</span>
</label></li></ul>
<div class="error-message" id="1128525183_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1102379069"><div style="font-weight: bold;">3.關於商品訂購交期方面
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="3.關於商品訂購交期方面  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.2000101183" value="&#20132;&#26399;&#28310;&#26178;" id="group_2000101183_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">交期準時</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.2000101183" value="&#20132;&#26399;&#31245;&#38263;&#65292;&#20294;&#26159;&#21487;&#20197;&#25509;&#21463;" id="group_2000101183_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">交期稍長，但是可以接受</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.2000101183" value="&#20132;&#26399;&#36942;&#38263;" id="group_2000101183_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">交期過長</span>
</label></li></ul>
<div class="error-message" id="1102379069_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_194827684"><div style="font-weight: bold;">4.關於訂單處理服務與速度(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="4.關於訂單處理服務與速度(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#27794;&#26377;&#21839;&#38988;" id="group_1542583549_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#12300;&#21830;&#21697;&#35330;&#36092;&#23436;&#25104;&#36890;&#30693;&#20449;&#12301;&#22826;&#24930;" id="group_1542583549_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">「商品訂購完成通知信」太慢</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#12300;&#21830;&#21697;&#37197;&#36865;&#36890;&#30693;&#20449;&#12301;&#22826;&#24930;" id="group_1542583549_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">「商品配送通知信」太慢</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#27604;&#38928;&#23450;&#37197;&#36865;&#26085;&#26089;&#23492;&#36948;" id="group_1542583549_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">比預定配送日早寄達</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#27604;&#38928;&#23450;&#37197;&#36865;&#26085;&#26202;&#23492;&#36948;" id="group_1542583549_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">比預定配送日晚寄達</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#36890;&#30693;&#20449;&#20839;&#23481;&#38627;&#25026;" id="group_1542583549_6" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">通知信內容難懂</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1542583549" value="&#25552;&#20986;&#30340;&#35426;&#21839;&#20839;&#23481;&#27794;&#26377;&#24471;&#21040;&#22238;&#35206;" id="group_1542583549_7" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">提出的詢問內容沒有得到回覆</span>
</label></li></ul>
<div class="error-message" id="194827684_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1695219994"><div style="font-weight: bold;">5.關於客服人員的服務態度(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="5.關於客服人員的服務態度(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1472602802" value="&#27794;&#26377;&#21839;&#38988;" id="group_1472602802_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1472602802" value="&#22826;&#26202;&#22238;&#35206;" id="group_1472602802_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">太晚回覆</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1472602802" value="&#27794;&#36774;&#27861;&#19968;&#27425;&#23601;&#35299;&#27770;&#21839;&#38988;" id="group_1472602802_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒辦法一次就解決問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1472602802" value="&#27794;&#26377;&#35299;&#27770;&#21839;&#38988;" id="group_1472602802_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有解決問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1472602802" value="&#26377;&#35299;&#27770;&#21839;&#38988;&#65292;&#20294;&#26159;&#25514;&#35422;&#19981;&#30070;" id="group_1472602802_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">有解決問題，但是措詞不當</span>
</label></li></ul>
<div class="error-message" id="1695219994_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_980917919"><div style="font-weight: bold;">6.網站頁面問題(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="6.網站頁面問題(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1391186686" value="&#27794;&#26377;&#21839;&#38988;" id="group_1391186686_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1391186686" value="&#32178;&#38913;&#38283;&#21855;&#36895;&#24230;&#22826;&#24930;" id="group_1391186686_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">網頁開啟速度太慢</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1391186686" value="&#32178;&#38913;&#38283;&#21855;&#30332;&#29983;&#25925;&#38556;" id="group_1391186686_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">網頁開啟發生故障</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1391186686" value="&#31995;&#32113;&#24120;&#24120;&#30332;&#29983;&#37679;&#35492;" id="group_1391186686_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">系統常常發生錯誤</span>
</label></li></ul>
<div class="error-message" id="980917919_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-radio"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2024991637"><div style="font-weight: bold;">7.關於商品的庫存資訊
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="radiogroup" aria-label="7.關於商品的庫存資訊  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1537127055" value="&#33391;&#22909;" id="group_1537127055_1" role="radio" class="ss-q-radio" aria-label="&#33391;&#22909;"></span>
<span class="ss-choice-label">良好</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1537127055" value="&#24819;&#35201;&#36092;&#36023;&#30340;&#21830;&#21697;&#27794;&#26377;&#24235;&#23384;" id="group_1537127055_2" role="radio" class="ss-q-radio" aria-label="&#24819;&#35201;&#36092;&#36023;&#30340;&#21830;&#21697;&#27794;&#26377;&#24235;&#23384;"></span>
<span class="ss-choice-label">想要購買的商品沒有庫存</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1537127055" value="&#21830;&#21697;&#38913;&#38754;&#39023;&#31034;&#26377;&#24235;&#23384;&#65292;&#19979;&#35330;&#20043;&#24460;&#23601;&#27794;&#26377;&#24235;&#23384;" id="group_1537127055_3" role="radio" class="ss-q-radio" aria-label="&#21830;&#21697;&#38913;&#38754;&#39023;&#31034;&#26377;&#24235;&#23384;&#65292;&#19979;&#35330;&#20043;&#24460;&#23601;&#27794;&#26377;&#24235;&#23384;"></span>
<span class="ss-choice-label">商品頁面顯示有庫存，下訂之後就沒有庫存</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1537127055" value="&#19981;&#30693;&#36947;&#26377;&#24235;&#23384;&#36039;&#35338;" id="group_1537127055_4" role="radio" class="ss-q-radio" aria-label="&#19981;&#30693;&#36947;&#26377;&#24235;&#23384;&#36039;&#35338;"></span>
<span class="ss-choice-label">不知道有庫存資訊</span>
</label></li></ul>
<div class="error-message" id="2024991637_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1605139071"><div style="font-weight: bold;">8.關於商品查詢方面(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="8.關於商品查詢方面(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#21830;&#21697;&#21517;&#31281;&#25628;&#23563;&#28961;&#27861;&#36805;&#36895;&#25214;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用商品名稱搜尋無法迅速找到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#21830;&#21697;&#21517;&#31281;&#25628;&#23563;&#25214;&#19981;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用商品名稱搜尋找不到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#36554;&#22411;&#20998;&#39006;&#25628;&#23563;&#25214;&#19981;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用車型分類搜尋找不到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#21697;&#29260;&#21517;&#31281;&#25628;&#23563;&#25214;&#19981;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用品牌名稱搜尋找不到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#21830;&#21697;&#20998;&#39006;&#25214;&#19981;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用商品分類找不到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#36554;&#36635;&#21517;&#31281;&#25214;&#19981;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_6" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用車輛名稱找不到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;Google&#25628;&#23563;&#24471;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_7" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用Google搜尋得到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;Yahoo&#25628;&#23563;&#24471;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_8" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用Yahoo搜尋得到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#20854;&#20182;&#25628;&#23563;&#24341;&#25806;&#24471;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_9" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用其他搜尋引擎得到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;facebook&#25110;Twitter&#31561;&#31038;&#32676;&#32178;&#32097;&#25214;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_10" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用facebook或Twitter等社群網絡找到想要找的商品</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.12272392" value="&#21033;&#29992;&#20854;&#20182;&#32178;&#31449;&#36899;&#32080;&#25214;&#21040;&#24819;&#35201;&#25214;&#30340;&#21830;&#21697;" id="group_12272392_11" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">利用其他網站連結找到想要找的商品</span>
</label></li></ul>
<div class="error-message" id="1605139071_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_380078768"><div style="font-weight: bold;">9.關於商品資訊方面(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="9.關於商品資訊方面(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.365762498" value="&#21830;&#21697;&#35498;&#26126;&#21313;&#20998;&#35443;&#32048;" id="group_365762498_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">商品說明十分詳細</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.365762498" value="&#21830;&#21697;&#36039;&#35338;&#19981;&#36275;" id="group_365762498_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">商品資訊不足</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.365762498" value="&#21830;&#21697;&#35498;&#26126;&#20839;&#23481;&#38627;&#25026;" id="group_365762498_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">商品說明內容難懂</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.365762498" value="&#32178;&#31449;&#20839;&#23481;&#38627;&#25026;" id="group_365762498_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">網站內容難懂</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.365762498" value="&#21830;&#21697;&#22294;&#29255;&#19981;&#22816;" id="group_365762498_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">商品圖片不夠</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.365762498" value="&#20729;&#26684;&#19981;&#21512;&#29702;" id="group_365762498_6" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">價格不合理</span>
</label></li></ul>
<div class="error-message" id="380078768_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1257305752"><div style="font-weight: bold;">10.關於商品評論方面(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="10.關於商品評論方面(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.417340243" value="&#20540;&#24471;&#21443;&#32771;" id="group_417340243_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">值得參考</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.417340243" value="&#27794;&#26377;&#24847;&#35211;" id="group_417340243_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有意見</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.417340243" value="&#30456;&#38364;&#21830;&#21697;&#35413;&#35542;&#20540;&#24471;&#21443;&#32771;" id="group_417340243_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">相關商品評論值得參考</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.417340243" value="&#30456;&#38364;&#21830;&#21697;&#30340;&#35413;&#35542;&#28961;&#27861;&#25552;&#20379;&#21443;&#32771;" id="group_417340243_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">相關商品的評論無法提供參考</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.417340243" value="&#27794;&#26377;&#30475;&#21040;&#30456;&#38364;&#35413;&#35542;" id="group_417340243_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">沒有看到相關評論</span>
</label></li></ul>
<div class="error-message" id="1257305752_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-radio"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_141486927"><div style="font-weight: bold;">11.對於本站商品特輯的感想
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="radiogroup" aria-label="11.對於本站商品特輯的感想  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1652378141" value="&#20540;&#24471;&#21443;&#32771;" id="group_1652378141_1" role="radio" class="ss-q-radio" aria-label="&#20540;&#24471;&#21443;&#32771;"></span>
<span class="ss-choice-label">值得參考</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1652378141" value="&#22240;&#28858;&#30475;&#21040;&#21830;&#21697;&#29305;&#36655;&#32780;&#36092;&#36023;" id="group_1652378141_2" role="radio" class="ss-q-radio" aria-label="&#22240;&#28858;&#30475;&#21040;&#21830;&#21697;&#29305;&#36655;&#32780;&#36092;&#36023;"></span>
<span class="ss-choice-label">因為看到商品特輯而購買</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1652378141" value="&#27794;&#26377;&#24847;&#35211;" id="group_1652378141_3" role="radio" class="ss-q-radio" aria-label="&#27794;&#26377;&#24847;&#35211;"></span>
<span class="ss-choice-label">沒有意見</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1652378141" value="&#27794;&#26377;&#30475;&#36942;&#21830;&#21697;&#29305;&#36655;" id="group_1652378141_4" role="radio" class="ss-q-radio" aria-label="&#27794;&#26377;&#30475;&#36942;&#21830;&#21697;&#29305;&#36655;"></span>
<span class="ss-choice-label">沒有看過商品特輯</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.1652378141" value="&#27794;&#26377;&#30475;&#21040;&#30456;&#38364;&#35413;&#35542;" id="group_1652378141_5" role="radio" class="ss-q-radio" aria-label="&#27794;&#26377;&#30475;&#21040;&#30456;&#38364;&#35413;&#35542;"></span>
<span class="ss-choice-label">沒有看到相關評論</span>
</label></li></ul>
<div class="error-message" id="141486927_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-checkbox"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_1283680597"><div style="font-weight: bold;">12.利用本網站後的感想(可複選)
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

<ul class="ss-choices" role="group" aria-label="12.利用本網站後的感想(可複選)  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#20351;&#29992;&#38918;&#21033;&#27794;&#26377;&#20219;&#20309;&#21839;&#38988;" id="group_1893116714_1" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">使用順利沒有任何問題</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#25805;&#20316;&#26178;&#24863;&#35258;&#21040;&#19981;&#23433;&#20840;" id="group_1893116714_2" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">操作時感覺到不安全</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#23565;&#26044;Email&#36890;&#30693;&#19981;&#28415;&#24847;" id="group_1893116714_3" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">對於Email通知不滿意</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#23565;&#23458;&#26381;&#30340;&#26381;&#21209;&#19981;&#28415;&#24847;" id="group_1893116714_4" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">對客服的服務不滿意</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#21830;&#21697;&#31278;&#39006;&#19981;&#40778;&#20840;" id="group_1893116714_5" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">商品種類不齊全</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#32178;&#31449;&#19981;&#22826;&#22909;&#25805;&#20316;" id="group_1893116714_6" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">網站不太好操作</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#32178;&#31449;&#36895;&#24230;&#24456;&#24930;" id="group_1893116714_7" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">網站速度很慢</span>
</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="checkbox" name="entry.1893116714" value="&#20170;&#24460;&#19981;&#24819;&#20877;&#20351;&#29992;" id="group_1893116714_8" role="checkbox" class="ss-q-checkbox"></span>
<span class="ss-choice-label">今後不想再使用</span>
</label></li></ul>
<div class="error-message" id="1283680597_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-paragraph-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2090303953"><div style="font-weight: bold;">13.希望「Webike-摩托百貨」新增哪些商品?
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr">請輸入您寶貴的意見</div></label>
<textarea name="entry.2090303953" rows="8" cols="0" class="ss-q-long" id="entry_2090303953" dir="auto" aria-label="13.希望「Webike-摩托百貨」新增哪些商品? 請輸入您寶貴的意見 " style="width: 300px;"></textarea>
<div class="error-message" id="48179961_errorMessage"></div>
</div></div></div> <div class="ss-form-question errorbox-good" role="listitem">
<div dir="ltr" class="ss-item  ss-paragraph-text"><div class="ss-form-entry">
<label class="ss-q-item-label" for="entry_2095632572"><div style="font-weight: bold;">14.您是否有其它問題或是建議?
</div>
<div class="ss-q-help ss-secondary-text" dir="ltr">請輸入您寶貴的意見</div></label>
<textarea name="entry.2095632572" rows="8" cols="0" class="ss-q-long" id="entry_2095632572" dir="auto" aria-label="14.您是否有其它問題或是建議? 請輸入您寶貴的意見 " style="width: 300px;"></textarea>

<input type="hidden" name="entry.1671619481" value="{{$customer->email}}" class="ss-q-short" id="entry_1671619481" dir="auto" aria-label="email  " title="">
<input type="hidden" name="entry.1738629723" value="{{$customer->realname}}" class="ss-q-short valid" id="entry_1738629723" dir="auto" aria-label="customer_name  " title="">
<input type="hidden" name="entry.2131632674" value="{{$order->created_at}}" class="ss-q-short valid" id="entry_2131632674" dir="auto" aria-label="order_created_at  " title="">
<input type="hidden" name="entry.174718599" value="{{$order->id}}" class="ss-q-short valid" id="entry_174718599" dir="auto" aria-label="order_id  " title="">
<input type="hidden" name="entry.1314670077" value="{{$order->increment_id}}" class="ss-q-short valid" id="entry_1314670077" dir="auto" aria-label="increment_id  " title="">
<table>
<tbody>
<tr>
<td dir="ltr"><input type="submit" name="submit" value="提交" />
<div>請勿透過 Google 表單送出密碼。</div>
</td>
</tr>
</tbody>
</table>
</div>
</form></div>
<div>
<div>&nbsp;</div>
<div>
<div>&nbsp;</div>
<div dir="ltr">
<div>技術提供：&nbsp;<a href="http://docs.google.com/" target="_blank"><img alt="Google 雲端硬碟" /></a></div>
<div>這個表單是在 榮芳興業 內建立。&nbsp;<br /><a href="https://docs.google.com/forms/d/1udgqtPm_uVW06Za_jvoubCz3NynOvIxExZXMwmQ-kDY/reportabuse?source=https://docs.google.com/forms/d/1udgqtPm_uVW06Za_jvoubCz3NynOvIxExZXMwmQ-kDY/viewform?sid%3D3bd8af5c62ff8a66%26token%3DePH8D0ABAAA.Zs-DP9RYBkUscQgvwAGUbg.VLnFyDPNooDyquwzGe1pDQ" target="_blank">檢舉濫用情形</a>&nbsp;-&nbsp;<a href="http://www.google.com/accounts/TOS" target="_blank">服務條款</a>&nbsp;-&nbsp;<a href="http://www.google.com/google-d-s/terms.html" target="_blank">其他條款</a></div>
</div>
</div>
</div>
</div>
<p><img src="{{$open_image}}" alt="" width="1" height="1" border="0" /></p>