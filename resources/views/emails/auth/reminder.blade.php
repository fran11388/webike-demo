<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>重設密碼</h2>

		<div>
			請點選下面連結重新設定密碼:<a href="{{ URL::route('customer-reset', array($token)) }}">重設密碼</a><br/>
			本連結將於{{ Config::get('auth.reminder.expire', 60) }}分鐘後失效.
		</div>
	</body>
</html>
