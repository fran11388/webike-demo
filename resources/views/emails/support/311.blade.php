<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div id="mail" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 859px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 48px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->customer->realname }}先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 楊千慧，感謝您本次的訂購。<br> 您的訂單 #{{ $order->increment_id }} 於 {{ date("Y/m/d") }}已經出貨，預定明後天會配送到府。<br> 您的配送明細如下：<br> </p>
            <br>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 90px; float: none; display: block; position: relative; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <div style="text-align: center; font-size: 19px; margin-top: 15px;"> <b> 商品配送通知 </b> </div>
            <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="clearfix">
                <div style="position: absolute;top:10px;left:20px;"> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"  style="background: url(/image/tmp_webike_tw_logos.png);background-position: -200px 0px;background-repeat: no-repeat;height: 80px;width: 180px;display: block;">  </a> </div>
            </div>
        </div>
        <div class="borderbox" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 26px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">訂單編號:#{{ $order->increment_id }}</p>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 207px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 760px; height: 207px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="customer">
                <tbody>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 62px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 96px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">會員名稱 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 125px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->customer->realname }}</td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 102px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">連絡電話 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> T: {{ $order->address->phone }}<br> M: {{ $order->address->mobile }}<br> </td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 101px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">訂購時間 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 117px; height: 60px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> {{ date("Y年m月d日<br>H時i分s秒", strtotime($order->created_at)) }} </td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 97px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">統一編號 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 142px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->address->company }} {{ $order->address->vat_number }}</td>
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 107px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">付款方式 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 133px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->payment->name }}</td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 98px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">收件人 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 154px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->address->firstname }}</td>
                    </tr>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 756px; height: 45px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 98px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">配送地址 :</th>
                        <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 649px; height: 43px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" colspan="5">{{ $order->address->zipcode }} , {{ $order->address->county }} {{ $order->address->district }} {{$order->address->address}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="items">
                <thead>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th width="5%">No.</th>
                        <th width="15%">品牌</th>
                        <th width="25%">商品名稱</th>
                        <th width="20%">商品編號</th>
                        <th width="10%">數量</th>
                        <th width="10%">單價</th>
                        <th width="15%">小計</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->items as $item_number =>$item )
                        @if($product = $item->product)
                            <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                                <td align="center">{{ $item_number + 1 }}</td>
                                <td align="center">{{ $product->manufacturer->name }}</td>
                                <td>{{ $product->name }}
                                    @if ($product->group_code)
                                        @foreach ($product->options as &$option)
                                            {{ $option->name }}
                                        @endforeach
                                    @endif
                                </td>
                                <td align="center">{{ $product->model_number }}</td>
                                <td align="center">{{ $item->quantity }}</td>
                                <td align="center">NT${{ number_format($item->final_price) }}</td>
                                <td align="center">NT${{ number_format($item->final_price * $item->quantity) }}</td>
                        </tr>
                        @else
                            <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                                <td align="center">{{ $item_number + 1 }}</td>
                                <td align="center"></td>
                                <td>{{ $item->name }}</td>
                                <td align="center"></td>
                                <td align="center">{{ $item->quantity }}</td>
                                <td align="center">NT${{ number_format($item->final_price) }}</td>
                                <td align="center">NT${{ number_format($item->final_price * $item->quantity) }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 122px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox clearfix">
            <div style="float:left;width:470px;">
                <label>備註:</label>
                <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 470px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">{{ $order->comment }}</p>
            </div>
            <div style="float:right">
                <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 280px; height: 122px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="total">
                    <tbody>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">商品費用合計</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->grand_total) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">運費</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->shipping_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">手續費</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->payment_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">折扣</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">-NT${{ number_format($order->coupon_amount + $order->rewardpoints_amount) }}</td>
                        </tr>
                        <tr>
                            <th style="font-size: 16px; font-family: sans-serif; font-weight: 700; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: left; width: 120px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">總計</th>
                            <td style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: right; width: 150px; height: 20px; float: none; display: table-cell; position: static; padding-top: 1px; padding-right: 1px; padding-bottom: 1px; padding-left: 1px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">NT${{ number_format($order->subtotal) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: block; position: static; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px; margin-top: -1px; margin-bottom: -1px; border-top: 1px solid rgb(0, 0, 0); border-right: 1px solid rgb(0, 0, 0); border-bottom: 1px solid rgb(0, 0, 0); border-left: 1px solid rgb(0, 0, 0); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="borderbox">
            <table style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 760px; height: 146px; float: none; display: table; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="items">
                <thead>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                        <th width="5%">配送</th>
                        <th width="15%">追蹤/宅配單號</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="font-size: 16px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 756px; height: 22px; float: none; display: table-row; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
                            <td align="center">{{ hasChangedLogistic() ? '新竹物流' : '台灣宅配通'}}</td>
                            <td align="center">{{ $order->tw_shipping }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="content">
            貨況追蹤請點選"<a href="{{ hasChangedLogistic() ? 'https://www.hct.com.tw/Search/SearchGoods_n.aspx' : 'http://query2.e-can.com.tw/%E5%A4%9A%E7%AD%86%E6%9F%A5%E4%BB%B6A.htm' }}">{{ hasChangedLogistic() ? '新竹物流' : '台灣宅配通'}}查詢網址</a>" <br/>
            *************************************************************************************************************************************
            <ul>
                <li>請注意：</li>
                <li>請您在<a href="{{ hasChangedLogistic() ? 'https://www.hct.com.tw/Search/SearchGoods_n.aspx' : 'http://query2.e-can.com.tw/%E5%A4%9A%E7%AD%86%E6%9F%A5%E4%BB%B6A.htm' }}">{{ hasChangedLogistic() ? '新竹物流' : '台灣宅配通'}}查詢網址</a>輸入您的單號即可查詢配送狀態。</li>
                <li>如果收件地址沒有人收件，宅配人員會留一張送達通知單在您收件地址的信箱中，上面載有投遞日期、時間及營業所聯絡電話，您可依此電話與營業所人員聯絡，約定下次配送時間；若未接獲您的來電，宅配人員將在隔天再次配送，因此請您在收到「商品發送通知信」後密切注意收件地址的收件狀況以免發生無人收件的情形，而耽誤收件的時間({{ hasChangedLogistic() ? '新竹物流' : '台灣宅配通'}}據點查詢"<a href="{{ hasChangedLogistic() ? 'https://www.hct.com.tw/Search/SearchGoods_n.aspx' : 'http://query2.e-can.com.tw/%E5%A4%9A%E7%AD%86%E6%9F%A5%E4%BB%B6A.htm' }}">{{ hasChangedLogistic() ? 'https://www.hct.com.tw/Search/SearchGoods_n.aspx' : '' }}</a>")。</li>
                <li>※若您付款方式為貨到付款，宅配人員將在送達商品時向您收取商品費用。</li>
                {{-- <br/>
                <li>您收到商品後，可以至我們的商品購買頁面填寫會員評論，凡經過審核完成的評論除了會將您的評論內容分享給會員之外，也會另外回饋最多50點購物點數給您 ※一般商品評論20點(無照片)，優秀商品評論(有附照片)50點，此點數均可以累積。</li>
                <li>商品評論說明：http://www.webike.tw/imp/step</li> --}}
            </ul>
            *************************************************************************************************************************************
            您也可以至" <a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a> "查看目前的訂單狀態，若您有其他問題與建議請" <a href="{{route('customer-service-information')}}">聯絡我們</a>"。<br>
            <hr style="border:1px black dotted">
                     <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a></label>
            <hr style="border:1px black dotted">
        </div>
    </div>
</body>