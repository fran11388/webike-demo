<?php \Debugbar::disable(); ?>
<html xml:lang="zh" lang="zh">
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="/assets/js/inline.plugin.js"></script>
	    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css" />
	</head>
	<body>
		<div id="mail">
			<div class="title">
				<label>
					@yield('title')
				</label>
			</div>
			<div>
				<a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">
					<img src="{{ assetRemote('assets/images/shared/webike_shop_logo.png') }}" alt="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"/>
				</a>
			</div>
			<div class="content">
				@yield('content')
			</div>
			<div class="description">
			****************************************************************************************************
				@yield('description')
			****************************************************************************************************
			</div>
			<div class="tips">
				@yield('tips')
			</div>
			<hr>
			<div class="mail_footer">
				@include('emails.template.footer')
			</div>
			<hr>
		</div>
	</body>
</html>