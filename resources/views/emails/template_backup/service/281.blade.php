@extends('emails.template.article')
@section('title')
	「Webike-摩托車市」：【個人自售】BENELLI TNT 899 義大利 BENELLI 倍利尼 TNT 899 可全貸 - 刊登成功通知
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的刊登。</p>
	<p>您本次刊登的商品刊登日期為2015/3/17，刊登天數為 60 天</p>
@stop
@section('description')
<br>
<ul>
	<li>物件連結為：<a href="http://www.webike.tw/motomarket/detail/20150306C000209">http://{{route('shopping')}}/motomarket/detail/20150306C000209</a></li>
	<li>您可以點擊物件頁面中的Facebook的分享按鈕，將您的物件分享給更多人看到。</li>
	<li>【物件資訊】</li>
	<li>【售價】</li>
	<li>【點閱數】</li>
	<li>【被追蹤數】</li>
	<li>【刊登日期】</li>
</ul>
<br>
@stop
@section('tips')
<p>歡迎您再次重新撰寫投稿評論，我們將會重新進行審核</p>
<p><a href="">點我撰寫商品評論</a></p>
<br>
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
@stop