@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您撰寫商品評論。</p>
	<p>您撰寫的評論已正常受理，本郵件為確認信箱地址而自動發送的郵件。</p>
@stop
@section('description')
<br>
<ul>
	<li>我們的人員已經在審核您的評論，約3~4個工作天我們會將結果回覆至您的信箱。</li>
	<li>投稿商品：</li>	
	<li>評論標題：</li>
	<li>評論內文：</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">如超過4個工作天仍未收回覆，請您來信service@webike.tw</p>
@stop