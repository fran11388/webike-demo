@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您對我們的支持。</p>
	<p>提醒您，您的帳號目前還有現金點數尚未使用，因點數有一年的使用期限</p>
@stop
@section('description')
<br>
<ul>
	<li>您可以至<a href="{{URL::route('history-points')}}">會員中心-點數獲得及使用履歷</a>查看點數數量及期限</li>
	<li>本月有眾多品牌、車型特輯介紹及促銷活動，以下也提供給您參考</li>
	<li>本月促銷活動</li>	
	<li>品牌、車型特輯介紹</li>
	<li>免費點數專區</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
@stop