@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您撰寫商品評論。</p>
	<p>您撰寫的評論經審核後，目前已經刊登在我們的平台上了</p>
@stop
@section('description')
<br>
<ul>
	<li>此次您撰寫的商品評論共獲得 50 點點數，點數已經匯入您的會員帳號內</li>
	<li>您可以至 <a href="{{URL::route('history-points')}}">會員中心-點數獲得及使用履歷</a> 裡面確認</li>
	<li>您撰寫的商品評論連結：<a href="http://www.webike.tw/imp/article/1411">http://{{route('shopping')}}/imp/article/1411</a></li>
	<li>您可以點擊「Facebook分享按鈕」分享給更多車友參考</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
@stop