@extends('emails.template.article')
@section('title')
	「Webike-摩托車市」CB400SF、ZRX1200 新物件通知!
@stop
@section('content')
<p>───────────────────────────────────────────────────────────────────</p>
<p>「Webike-摩托車市」您追蹤的車輛物件更新({{date('Y/m/d')}}更新)</p>
<p>───────────────────────────────────────────────────────────────────</p>
<br>
@stop
@section('description')
<br>
<ul>
	<li>────────────────────────────────────</li>
	<li>(1) HONDA CB400SF本周最新物件</li>
	<li>────────────────────────────────────</li>
	<li><a href="" target="_blank">1.[個人自售] 20.5萬  好發好騎，可換車…            新北市</a></li>
	<li><a href="" target="_blank">2.[隆澤車業] 38.3萬  2015年出廠現車可領牌         新竹市</a></li>
	<br>
	<li><a href="" target="_blank">全部CB400SF物件</a></li>
	<br>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p>發行：「Webike-摩托車市」</p>
<p>●取消或修改自動追蹤：<a href="http://www.webike.tw/motomarket/customer/follow?default_tab=3" target="_blank">http://www.webike.tw/motomarket/customer/follow?default_tab=3</a></p>
<p>●刊登新物件：<a href="http://www.webike.tw/motomarket/customer/product/create" target="_blank">http://www.webike.tw/motomarket/customer/product/create</a></p>
<p>●進口車物件：<a href="http://www.webike.tw/motomarket/search/imp" target="_blank">http://www.webike.tw/motomarket/search/imp</a></p>
<p>●國產車物件：<a href="http://www.webike.tw/motomarket/search/dom" target="_blank">http://www.webike.tw/motomarket/search/dom</a></p>
<p class="gap">●台灣No.1「Webike-摩托百貨」：<a href="{{route('shopping')}}" target="_blank">{{route('shopping')}}</a></p>
@stop