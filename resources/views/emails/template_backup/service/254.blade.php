@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您撰寫商品評論。</p>
	<p>此次您撰寫的商品評論經審核之後，因評論內容不足而未審核通過</p>
@stop
@section('description')
<br>
<ul>
	<li>未通過的原因為：</li>
	<li>1.正廠零件未附照片。</li>
	<li>2.撰寫多篇內容重複之評論。</li>
	<li>3.評論內文未達50字。</li>
	<li>4.內容有包含針對性、攻擊性、不雅的字眼。</li>
</ul>
<br>
@stop
@section('tips')
<p>歡迎您再次重新撰寫投稿評論，我們將會重新進行審核</p>
<p><a href="">點我撰寫商品評論</a></p>
<br>
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
@stop