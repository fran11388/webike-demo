@extends('emails.template.article')
@section('title')
	「Webike-摩托車市」：【個人自售】BENELLI TNT 899 義大利 BENELLI 倍利尼 TNT 899 可全貸 - 刊登成功通知
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的刊登。</p>
	<p>您本次刊登的商品刊登日期為2015/3/17，刊登天數為 60 天</p>
@stop
@section('description')
<br>
<ul>
	<li>【物件資訊】</li>
	<li>【物件名稱】：【Webike車業(刊登Sample)】KAWASAKI ZEPHYR750 ◎RS版 西風，經典美車(這是刊登Sample)◎</li>
	<li>【物件網址】：http：//www.webike.tw/motomarket/detail/20140731D000440</li>
	<li>【買家的問題】</li>
	<li>dinxxxxxxx的問題：</li>
	<li>測試測試~ (2014-09-10 09:10:38)</li>
	<br>
	<li>賣家的回覆</li>
	<li>請來電詳談 (2014-09-11 11:20:45)</li>
	<br>
	<li><a href="">點我回覆賣家問題</a></li>
	<br>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
@stop