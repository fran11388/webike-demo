@extends('emails.template.article')
@section('title')
	「Webike-摩托車市」：【個人自售】BENELLI TNT 899 義大利 BENELLI 倍利尼 TNT 899 可全貸 - 刊登時間結束通知
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的刊登。</p>
	<p>您刊登的商品刊登時間已經結束</p>
@stop
@section('description')
<br>
<ul>
	<li>並且已經下架，如您需要繼續刊登，請至</li>
	<li><a href="http://www.webike.tw/motomarket/customer/product">會員中心->物件管理</a> 將您要繼續刊登的商品重新上架即可。</li>
	<br>
	<li>【物件資訊】</li>
	<li>【售價】</li>
	<li>【點閱數】</li>
	<li>【被追蹤數】</li>
	<li>【刊登日期】</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
@stop