@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您撰寫商品評論。</p>
	<p>會員[小昱]在你所撰寫的評論留下了新回應!</p>
@stop
@section('description')
<br>
<ul>
	<li>標題：</li>
<li>回應者：</li>
<li>回應內容：</li>
<li>回應時間：</li>
<li>快去評論頁面回覆留言吧!</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
@stop