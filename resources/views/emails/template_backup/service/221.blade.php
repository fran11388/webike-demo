@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您註冊成為<a href="{{URL::route('customer')}}">【Webike-摩托百貨】會員。</a></p>
	<p>歡迎您加入「Webike-摩托百貨」，您已經完成【快速註冊】</p>
@stop
@section('description')
<ul>
	@if(strtotime(date('Y-m-d')) >= strtotime('2016-04-01'))
		<li>恭喜您獲得$100元體驗折價券，請您在購物結帳時使用!!</li>
	@else
		<li>恭喜您獲得$200元體驗折價券，請您在購物結帳時使用!!</li>
	@endif
	<li>折價券號碼：wt49b5vd</li>
</ul>
<br>
<ul>
	<li>折價券注意事項：</li>
	<li>1.請您先完成<a href="{{URL::route('customer-complete')}}">「完整註冊」</a>，開通購物功能後，方可使用。</li>
	<li>2.本體驗券有效期限為60天，並且每個帳號僅能使用一次。</li>
	<li>3.<a href="{{URL::route('customer-rule','member_rule_2')}}#A">購物流程說明</a></li>
	<li>4.<a href="{{URL::route('customer-rule','member_rule_2')}}#L">折價券使用說明</a></li>
</ul>
<br>
<ul>
	<li>您完成【快速註冊】之後，享有的功能有：</li>
	<li>1.「Webike-摩托百貨」電子報(每週五發送)。</li>
	<li>2.<a href="{{URL::route('genuineparts-step')}}">正廠零件查詢系統</a></li>
	<li>3.<a href="{{URL::route('mitumori')}}">未登錄商品查詢系統</a></li>
	<li>4.<a href="{{URL::route('groupbuy')}}">團購商品查詢系統</a></li>
	<li>5.<a href="{{route('shopping')}}/motomarket/">「Webike-摩托車市」刊登服務</a></li>
</ul>
<br>
<ul>
	<li>【完整註冊】可享有更多好康及服務：</li>
	<li>1.改裝零件、騎士用品、正廠零件…等購物功能。</li>
	<li>2.購物滿額免運費。</li>
	<li>3.商品評論及討論功能。</li>
	<li>4.投稿評論拿50點點數。</li>
	<li>5.$200元生日禮券。</li>
	<li>6.愛車特惠活動通知。</li>
	<li><a href="{{URL::route('customer-complete')}}">點我馬上完成【完整註冊】</a></li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
@stop