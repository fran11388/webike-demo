@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您完成會員【完整註冊】。</p>
@stop
@section('description')
<br>
<ul>
	<li>感謝您完成【完整註冊】，您享有的好康及服務：</li>
	<li>1.「Webike-摩托百貨」電子報(每週五發送)-開信可拿現金點數。</li>
	<li>2.<a href="{{URL::route('genuineparts-step')}}">正廠零件查詢系統</a></li>
	<li>3.<a href="{{URL::route('mitumori')}}">未登錄商品查詢系統</a></li>
	<li>4.改裝零件、騎士用品、正廠零件…等購物功能。</li>
	<li>5.購物滿額免運費。</li>
	<li>6.商品評論及討論功能。</li>
	<li>7.投稿評論拿50點點數。</li>
	<li>8.$200元生日禮券。</li>
	<li>9.愛車特惠活動通知。</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
@stop