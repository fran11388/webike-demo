@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您對我們的支持。</p>
	<p>您此次在「Webike-摩托百貨」購物，訂單編號為：</p>
@stop
@section('description')
<br>
<ul>
	<li>在這次的購物中您共獲得 26 點點數，從 2015-02-06 開始可以使用您可以在<a href="{{URL::route('history-points')}}">【點數獲得及使用履歷】</a>中查詢點數獲得及使用的狀態。</li>
	<li>點數的使用方式和規則請參考「Webike-摩托百貨」<a href="{{URL::route('customer-rule','pointinfo')}}">【回饋點數使用說明】</a></li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
@stop