@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，恭喜您為本月壽星。</p>
@stop
@section('description')
<br>
<ul>
	<li>感謝您對我們的支持與鼓勵，本月份為您的生日月份</li>
	<li>藉此發放$200元生日禮券給您，並祝您生日快樂</li>	
	<li>您的生日禮劵代碼為：</li>
	<li>此禮劵使用期限為40天，在期間內消費可以使用以折抵費用</li>
	<li style="color:red">※請勿轉讓此折價券，如轉讓造成無法使用本站一概不負責</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
@stop