@extends('emails.template.article')
@section('title')
	「Webike-摩托百貨」：XXXX禮劵發送，現金折抵XXX元!
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您對我們的支持。</p>
@stop
@section('description')
<br>
<ul>
	<li>您的折價劵代碼為：</li>
</ul>
<br>
@stop
@section('tips')
<p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
<p class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
@stop