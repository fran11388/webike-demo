@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">您好，非常感謝您訂閱<a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a>電子報。</p>
@stop
@section('description')
<ul>
	<li>目前已經將您的電子報訂閱服務取消</li>
	<li>希望往後也有機會能為您服務。</li>
	<li>如您日後還需要訂閱<a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a>電子報</li>
	<li>請點擊：<a href="">訂閱電子報</a></li>
</ul>
@stop
@section('tips')
<ul>
	<li>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</li>
	<li>◎若您有任何問題，請您來信：service@webike.tw</li>
</ul>
@stop