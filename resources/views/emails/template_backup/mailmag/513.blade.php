@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">您好，非常感謝您訂閱<a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a>電子報。</p>
	<p>我們每週為您介紹摩托車相關資訊</p>
@stop
@section('description')
<ul>
	<li>1.每月優惠及促銷特價，好康活動。</li>
	<li>2.開信免費拿現金點數。</li>
	<li>3.新車發表、賽事報導、海外摩托車專題介紹、Webike獨家新聞。</li>
	<li>4.每週最新流行特輯、品牌介紹、改裝特輯。</li>
	<li>5.每週新商品及新品牌上架資訊。</li>
	<li>6.會員商品評論分享。</li>
	<li>7.每週<a href="{{route('shopping')}}/motomarket">「Webike-摩托車市」</a>最新販售資訊。</li>
</ul>
@stop
@section('tips')
<ul>
<li>還有更多的相關資訊，每週更新發送</li>
<li>請您密切注意<a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a>電子報訊息。</li>

<li>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</li>
<li>◎若您有任何問題，請您來信：service@webike.tw</li>
</ul>
@stop