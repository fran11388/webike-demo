<?php \Debugbar::disable(); ?>
<html xml:lang="zh" lang="zh">
	<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="/assets/js/inline.plugin.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css" />
	</head>
	<body>
		<div id="mail" style="height: auto;" >
			<div class="title">
				<label>
					@yield('title')
				</label>
			</div>
			
			<div class="content">
				@yield('content')
			</div>
			<div class="borderbox" style="position: relative; height:90px;border:1px black solid;width:760px;margin:-1px auto;
		padding: 10px;">
				<div style="text-align: center; font-size: 19px; margin-top: 15px;">
					<B>@yield('table_title')</B>
				</div>
				<div class="clearfix">
					<div style="position: absolute;top:10px;left:20px;">
						<a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">
							<img src="{{ assetRemote('assets/images/shared/webike_shop_logo.png') }}" alt="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"  width="200px"/>
						</a>
					</div>
				</div>
			</div>
			@yield('table')
			<div class="borderbox">
				您也可以至"
				<a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a>
				"查看目前的訂單狀態，若您有其他問題與建議請"
				<a href="{{route('customer-service-information')}}">聯絡我們</a>"。
			</div>
			<div class="borderbox">
				<div class="mail_footer">
					@include('emails.template.footer')
				</div>
			</div>
		</div>
	</body>
</html>