@extends('emails.template.order')
@section('title')
	{{$mail_category->title}}訂單編號
@stop
@section('content')
	<p>[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。<br/>
		您選擇的付款方式"{{"銀行匯款"}}"，目前我們正在確認商品的交期(約1~2個工作天)。<br/>
		交期確認後我們會發送”交期確認通知信”，請注意您的信箱。<br/>
		若超過3個工作天尚未收到”交期確認通知信”，請您來信 order@webike.tw，我們隨即幫您處理。<br/>
	</p>
	<br/>
	<p>以下是您的訂單明細：</p>
@stop
@section('table_title')
	{{$mail_category->name}}
@stop
@section('table')

<div class="borderbox" style="padding:10px;">
	<p>訂單編號:#</p>
</div>
<div class="borderbox">
	<table class="customer">
		<tr>
			<th>會員名稱 :</th>
			<td>江 百裕</td>
			<th>連絡電話 :</th>
			<td>
				T: 06-2313577<br/>
				M: 0982509753<br/>
			</td>
			<th>訂購時間 :</th>
			<td>
				2014年5月12日<br/>
				下午1時32分07秒
			</td>
		</tr>
		<tr>
			<th>統一編號 :</th>
			<td></td>
			<th>付款方式 :</th>
			<td></td>
		</tr>
		<tr>
			<th>收件人 :</th>
			<td>江 百裕</td>
		</tr>
		<tr>
			<th>配送地址 :</th>
			<td colspan="5" >710 , 台南市 永康區大橋一街247巷21號</td>
		</tr>
	</table>
</div>
<div class="borderbox">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="15%">品牌</th>
				<th width="25%">商品名稱</th>
				<th width="20%">商品編號</th>
				<th width="10%">數量</th>
				<th width="10%">單價</th>
				<th width="15%">小計</th>
			</tr>
		</thead>
		<tbody>
			@for ($i=1; $i <= 5 ; $i++)
				<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
			@endfor
		</tbody>
	</table>
</div>
<div class="borderbox clearfix">
	<div style="float:left;width:470px;">
		<label>備註:</label>
		<p>希望4/1前到貨</p>
	</div>
	<div style="float:right">
		<table class="total">
			<tr>
				<th>商品費用合計</th>
				<td>NT$ 322</td>
			</tr>
			<tr>
				<th>運費</th>
				<td>NT$ 100</td>
			</tr>
			<tr>
				<th>手續費</th>
				<td>NT$0</td>
			</tr>
			<tr>
				<th>折扣</th>
				<td>-NT$0</td>
			</tr>
			<tr>
				<th>總計</th>
				<td>NT$ 322</td>
			</tr>
		</table>
	</div>
</div>
@stop
