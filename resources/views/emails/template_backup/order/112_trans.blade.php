<html xml:lang="zh" lang="zh"><head> <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css"> <style id="style-1-cropbar-clipper">/* Copyright 2014 Evernote Corporation. All rights reserved. */
.en-markup-crop-options {
    top: 18px !important;
    left: 50% !important;
    margin-left: -100px !important;
    width: 200px !important;
    border: 2px rgba(255,255,255,.38) solid !important;
    border-radius: 4px !important;
}

.en-markup-crop-options div div:first-of-type {
    margin-left: 0px !important;
}
</style></head> <body style="padding-bottom: 26px;"> <div id="mail" style="font-size: 13px; font-family: 微軟正黑體; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: auto; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-right: 529px; margin-bottom: 10px; margin-left: 529px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: rgba(0, 0, 0, 0); background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> <div class="title"> <label style="font-size: 13px; font-family: 微軟正黑體; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 321px; height: auto; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: rgba(0, 0, 0, 0); background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> 	「Webike-摩托百貨」：交期確認通知信#訂單編號
 </label> </div> <div class="content"> 	<p>[江 百裕]先生/小姐您好，我是<a href="http://localhost/test">「Webike-摩托百貨」</a>的服務人員 吳 筱玲，感謝您本次的訂購。</p>
 </div> <div class="borderbox" style="position: relative; height:90px;border:1px black solid;width:760px;margin:-1px auto; padding: 10px;"> <div style="text-align: center; font-size: 19px; margin-top: 15px;"> <b>	交期確認通知
</b> </div> <div class="clearfix"> <div style="position: absolute;top:10px;left:20px;"> <a href="http://localhost/test" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"> <img src="{{ assetRemote('assets/images/shared/webike_shop_logo.png') }}" width="200px" alt="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"> </a> </div> </div> </div>
<div class="borderbox" style="padding:10px;">
	<p>訂單編號:#</p>
</div>
<div class="borderbox">
	<table class="customer">
		<tbody><tr>
			<th>會員名稱 :</th>
			<td>江 百裕</td>
			<th>連絡電話 :</th>
			<td>
				T: 06-2313577<br>
				M: 0982509753<br>
			</td>
			<th>訂購時間 :</th>
			<td>
				2014年5月12日<br>
				下午1時32分07秒
			</td>
		</tr>
		<tr>
			<th>付款方式 :</th>
			<td></td>
		</tr>
	</tbody></table>
</div>
<div class="borderbox">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="15%">品牌</th>
				<th width="25%">商品名稱</th>
				<th width="20%">商品編號</th>
				<th width="10%">數量</th>
				<th width="10%">單價</th>
				<th width="15%">小計</th>
			</tr>
		</thead>
		<tbody>
							<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
							<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
							<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
							<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
							<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
					</tbody>
	</table>
</div>
<div class="borderbox" style="text-align:center">
	<p class="point">您所訂購的商品預計的交貨期為 XX 個工作天。</p>
</div>
<div class="borderbox">
	<ul>
		<li>請注意:</li>
		<li>1.以上交期為所有商品備齊一同寄出的交貨時間，出貨前我們將會以e-mail通知您。</li>
		<li>2.若是有突發狀況導致商品延誤、缺貨或是停售時，我們將主動在第一時間告知您。</li>
		<li>3.工作天數計算不包含台灣或日本的例假日。</li>
	</ul>
</div>
 <div class="borderbox"> 您也可以至" <a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a> "查看目前的訂單狀態，若您有其他問題與建議請" <a href="{{route('customer-service-information')}}">聯絡我們</a>"。 </div> <div class="borderbox"> <div class="mail_footer"> <label><a href="http://localhost/test" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="http://localhost/test" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">http://localhost/test</a> </label> </div> </div> </div> <link rel="stylesheet" type="text/css" href="http://localhost/_debugbar/assets/stylesheets?1423800674">
<script type="text/javascript" src="https://localhost/_debugbar/assets/javascript?1423800674"></script>
<script type="text/javascript">jQuery.noConflict(true);</script>
<script type="text/javascript">
var phpdebugbar = new PhpDebugBar.DebugBar();
phpdebugbar.addTab("messages", new PhpDebugBar.DebugBar.Tab({"icon":"list-alt","title":"Messages", "widget": new PhpDebugBar.Widgets.MessagesWidget()}));
phpdebugbar.addIndicator("time", new PhpDebugBar.DebugBar.Indicator({"icon":"clock-o","tooltip":"Request Duration"}), "right");
phpdebugbar.addTab("timeline", new PhpDebugBar.DebugBar.Tab({"icon":"tasks","title":"Timeline", "widget": new PhpDebugBar.Widgets.TimelineWidget()}));
phpdebugbar.addIndicator("memory", new PhpDebugBar.DebugBar.Indicator({"icon":"cogs","tooltip":"Memory Usage"}), "right");
phpdebugbar.addTab("exceptions", new PhpDebugBar.DebugBar.Tab({"icon":"bug","title":"Exceptions", "widget": new PhpDebugBar.Widgets.ExceptionsWidget()}));
phpdebugbar.addTab("views", new PhpDebugBar.DebugBar.Tab({"icon":"leaf","title":"Views", "widget": new PhpDebugBar.Widgets.TemplatesWidget()}));
phpdebugbar.addTab("route", new PhpDebugBar.DebugBar.Tab({"icon":"share","title":"Route", "widget": new PhpDebugBar.Widgets.VariableListWidget()}));
phpdebugbar.addIndicator("currentroute", new PhpDebugBar.DebugBar.Indicator({"icon":"share","tooltip":"Route"}), "right");
phpdebugbar.addTab("queries", new PhpDebugBar.DebugBar.Tab({"icon":"inbox","title":"Queries", "widget": new PhpDebugBar.Widgets.SQLQueriesWidget()}));
phpdebugbar.addTab("emails", new PhpDebugBar.DebugBar.Tab({"icon":"inbox","title":"Mails", "widget": new PhpDebugBar.Widgets.MailsWidget()}));
phpdebugbar.addTab("request", new PhpDebugBar.DebugBar.Tab({"icon":"tags","title":"Request", "widget": new PhpDebugBar.Widgets.VariableListWidget()}));
phpdebugbar.setDataMap({
"messages": ["messages.messages", []],
"messages:badge": ["messages.count", null],
"time": ["time.duration_str", '0ms'],
"timeline": ["time", {}],
"memory": ["memory.peak_usage_str", '0B'],
"exceptions": ["exceptions.exceptions", []],
"exceptions:badge": ["exceptions.count", null],
"views": ["views", []],
"views:badge": ["views.nb_templates", 0],
"route": ["route", {}],
"currentroute": ["route.uri", ],
"queries": ["queries", []],
"queries:badge": ["queries.nb_statements", 0],
"emails": ["swiftmailer_mails.mails", []],
"emails:badge": ["swiftmailer_mails.count", null],
"request": ["request", {}]
});
phpdebugbar.restoreState();
phpdebugbar.ajaxHandler = new PhpDebugBar.AjaxHandler(phpdebugbar);
phpdebugbar.ajaxHandler.bindToXHR();
phpdebugbar.setOpenHandler(new PhpDebugBar.OpenHandler({"url":"http:\/\/localhost\/_debugbar\/open"}));
phpdebugbar.addDataSet({"__meta":{"id":"44d92131d1d46f91d5c3243c649b000a","datetime":"2015-03-24 09:23:22","utime":1427160202.0443,"method":"GET","uri":"\/admin\/mail\/112","ip":"::1"},"php":{"version":"5.5.11"},"messages":{"count":0,"messages":[]},"time":{"start":1427160200.8682,"end":1427160202.0443,"duration":1.1760668754578,"duration_str":"1.18s","measures":[{"label":"Booting","start":1427160200.8682,"relative_start":0,"end":1427160201.4213,"relative_end":1427160201.4213,"duration":0.5530309677124,"duration_str":"553.03ms","params":[],"collector":null},{"label":"Application","start":1427160201.4283,"relative_start":0.56003189086914,"end":1427160202.0303,"relative_end":1427160202.0303,"duration":0.60203409194946,"duration_str":"602.03ms","params":[],"collector":null},{"label":"After application","start":1427160202.0303,"relative_start":1.1620659828186,"end":1427160202.0443,"relative_end":0,"duration":0.01400089263916,"duration_str":"14ms","params":[],"collector":null}]},"memory":{"peak_usage":10747904,"peak_usage_str":"10.25MB"},"exceptions":{"count":0,"exceptions":[]},"views":{"nb_templates":3,"templates":[{"name":"emails.order.112 (\\app\\views\\emails\\order\\112.blade.php)","param_count":1,"params":["mail_category"],"type":"blade"},{"name":"emails.template.order (\\app\\views\\emails\\template\\order.blade.php)","param_count":6,"params":["obLevel","__env","app","errors","mail_category","i"],"type":"blade"},{"name":"emails.template.footer (\\app\\views\\emails\\template\\footer.blade.php)","param_count":6,"params":["obLevel","__env","app","errors","mail_category","i"],"type":"blade"}]},"route":{"uri":"GET admin\/mail\/{template_code}","as":"mail-template","controller":"Backend\\MailController@getTemplate","namespace":null,"prefix":"admin\/mail","where":[],"file":"\\app\\controllers\\Backend\\MailController.php:5-10"},"queries":{"nb_statements":4,"nb_failed_statements":0,"accumulated_duration":0.47004,"accumulated_duration_str":"470.04ms","statements":[{"sql":"select * from `tracker_sessions` where `session_id` = '06930008a751636f56f830c347cd84451a5adf7c' limit 1","params":{"0":"06930008a751636f56f830c347cd84451a5adf7c","hints":"Use <code>SELECT *<\/code> only if you need all columns from table<br \/><code>LIMIT<\/code> without <code>ORDER BY<\/code> causes non-deterministic results, depending on the query execution plan"},"duration":0.11901,"duration_str":"119.01ms","stmt_id":null},{"sql":"select * from `customers` where `id` = '5415' and `remember_token` = 's7LYAhtxupgEbcTEWh78jNWSD4DOkp7aNuY99CRPrItL3UyrIENxR4SncGlt' limit 1","params":{"0":"5415","1":"s7LYAhtxupgEbcTEWh78jNWSD4DOkp7aNuY99CRPrItL3UyrIENxR4SncGlt","hints":"Use <code>SELECT *<\/code> only if you need all columns from table<br \/><code>LIMIT<\/code> without <code>ORDER BY<\/code> causes non-deterministic results, depending on the query execution plan"},"duration":0.11601,"duration_str":"116.01ms","stmt_id":null},{"sql":"update `tracker_sessions` set `path` = 'http:\/\/localhost\/admin\/mail\/112', `is_robot` = '0', `updated_at` = '2015-03-24 09:23:21' where `id` = '16734'","params":{"0":"http:\/\/localhost\/admin\/mail\/112","1":"0","2":"2015-03-24 09:23:21","3":"16734"},"duration":0.11801,"duration_str":"118.01ms","stmt_id":null},{"sql":"select * from `mail_category` where `code` = '112' limit 1","params":{"0":"112","hints":"Use <code>SELECT *<\/code> only if you need all columns from table<br \/><code>LIMIT<\/code> without <code>ORDER BY<\/code> causes non-deterministic results, depending on the query execution plan"},"duration":0.11701,"duration_str":"117.01ms","stmt_id":null}]},"swiftmailer_mails":{"count":0,"mails":[]},"request":{"format":"html","content_type":"text\/html; charset=UTF-8","status_text":"OK","status_code":"200","request_query":"[]","request_request":"[]","request_headers":"array:7 [\n  \"host\" => array:1 [\n    0 => \"localhost\"\n  ]\n  \"connection\" => array:1 [\n    0 => \"keep-alive\"\n  ]\n  \"accept\" => array:1 [\n    0 => \"text\/html,application\/xhtml+xml,application\/xml;q=0.9,image\/webp,*\/*;q=0.8\"\n  ]\n  \"user-agent\" => array:1 [\n    0 => \"Mozilla\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/41.0.2272.89 Safari\/537.36\"\n  ]\n  \"accept-encoding\" => array:1 [\n    0 => \"gzip, deflate, sdch\"\n  ]\n  \"accept-language\" => array:1 [\n    0 => \"zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4\"\n  ]\n  \"cookie\" => array:1 [\n    0 => \"remember_82e5d2c56bdd0811318f0cf078b78bfc=eyJpdiI6IlhGNUZKT2lJTlwvb1hsbDhKVmFFdlhRPT0iLCJ2YWx1ZSI6IkpaOGdoeUJlRDdpVmFnKzBRVmVhTmxGWjBmZ21Md2t4N0YydFQ4eGpON2oxQ25JYjlyc28yTVIyOXNwRWpmUHp1T3BUZHE2azhBZmZFRWRYYTNCd21OQXJEdW00XC9BVWRoRXkwM1paUlI2ND0iLCJtYWMiOiIzZDJhNmY2YjBiZjNmZjgyZDlkOWYyMmM0NDEzZTNhM2I5MjI1MjNmZmE1ODhlNzYxYThhNmFmZWE1ZTJiOTM4In0%3D; laravel_session=eyJpdiI6IkYrZ1dycndBS2FYV1NJcjVSZ29CMHc9PSIsInZhbHVlIjoiWFN2bDhjOHFsTjVHNEw2R2xScTRlZEE4STI1b3dCM1FaN2RWMEl6N0Zld29Vc0xcL3VLckhzQ3djTjNEYTZ4WlZ3WnNvVVp4elM0MlF2ZWV3MTBBaVF3PT0iLCJtYWMiOiI5ZjJjNjAxMWI2YmM1YzRlNTU0MGYzM2M5ZWFjMzRjZmY0NDZlZGE4YTMyODgwYTIwZGRkODBjMmRmNTJkNWJiIn0%3D\"\n  ]\n]","request_server":"array:48 [\n  \"REDIRECT_MIBDIRS\" => \"C:\/xampp\/php\/extras\/mibs\"\n  \"REDIRECT_MYSQL_HOME\" => \"\\xampp\\mysql\\bin\"\n  \"REDIRECT_OPENSSL_CONF\" => \"C:\/xampp\/apache\/bin\/openssl.cnf\"\n  \"REDIRECT_PHP_PEAR_SYSCONF_DIR\" => \"\\xampp\\php\"\n  \"REDIRECT_PHPRC\" => \"\\xampp\\php\"\n  \"REDIRECT_TMP\" => \"\\xampp\\tmp\"\n  \"REDIRECT_STATUS\" => \"200\"\n  \"MIBDIRS\" => \"C:\/xampp\/php\/extras\/mibs\"\n  \"MYSQL_HOME\" => \"\\xampp\\mysql\\bin\"\n  \"OPENSSL_CONF\" => \"C:\/xampp\/apache\/bin\/openssl.cnf\"\n  \"PHP_PEAR_SYSCONF_DIR\" => \"\\xampp\\php\"\n  \"PHPRC\" => \"\\xampp\\php\"\n  \"TMP\" => \"\\xampp\\tmp\"\n  \"HTTP_HOST\" => \"localhost\"\n  \"HTTP_CONNECTION\" => \"keep-alive\"\n  \"HTTP_ACCEPT\" => \"text\/html,application\/xhtml+xml,application\/xml;q=0.9,image\/webp,*\/*;q=0.8\"\n  \"HTTP_USER_AGENT\" => \"Mozilla\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/41.0.2272.89 Safari\/537.36\"\n  \"HTTP_ACCEPT_ENCODING\" => \"gzip, deflate, sdch\"\n  \"HTTP_ACCEPT_LANGUAGE\" => \"zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4\"\n  \"HTTP_COOKIE\" => \"remember_82e5d2c56bdd0811318f0cf078b78bfc=eyJpdiI6IlhGNUZKT2lJTlwvb1hsbDhKVmFFdlhRPT0iLCJ2YWx1ZSI6IkpaOGdoeUJlRDdpVmFnKzBRVmVhTmxGWjBmZ21Md2t4N0YydFQ4eGpON2oxQ25JYjlyc28yTVIyOXNwRWpmUHp1T3BUZHE2azhBZmZFRWRYYTNCd21OQXJEdW00XC9BVWRoRXkwM1paUlI2ND0iLCJtYWMiOiIzZDJhNmY2YjBiZjNmZjgyZDlkOWYyMmM0NDEzZTNhM2I5MjI1MjNmZmE1ODhlNzYxYThhNmFmZWE1ZTJiOTM4In0%3D; laravel_session=eyJpdiI6IkYrZ1dycndBS2FYV1NJcjVSZ29CMHc9PSIsInZhbHVlIjoiWFN2bDhjOHFsTjVHNEw2R2xScTRlZEE4STI1b3dCM1FaN2RWMEl6N0Zld29Vc0xcL3VLckhzQ3djTjNEYTZ4WlZ3WnNvVVp4elM0MlF2ZWV3MTBBaVF3PT0iLCJtYWMiOiI5ZjJjNjAxMWI2YmM1YzRlNTU0MGYzM2M5ZWFjMzRjZmY0NDZlZGE4YTMyODgwYTIwZGRkODBjMmRmNTJkNWJiIn0%3D\"\n  \"PATH\" => \"C:\\Ruby21-x64\\bin;C:\\Ruby200-x64\\bin;C:\\ProgramData\\Oracle\\Java\\javapath;C:\\Windows\\system32;C:\\Windows;C:\\Windows\\System32\\Wbem;C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\;%JAVA_HOME%\\bin;%MYSQL_HOME%\\bin;C:\\HashiCorp\\Vagrant\\bin;C:\\Program Files (x86)\\Git\\cmd;C:\\xampp\\php;C:\\ProgramData\\ComposerSetup\\bin;C:\\Program Files (x86)\\Skype\\Phone\\;C:\\Program Files (x86)\\IDM Computer Solutions\\UltraEdit\\\"\n  \"SystemRoot\" => \"C:\\Windows\"\n  \"COMSPEC\" => \"C:\\Windows\\system32\\cmd.exe\"\n  \"PATHEXT\" => \".COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH;.MSC;.RB;.RBW\"\n  \"WINDIR\" => \"C:\\Windows\"\n  \"SERVER_SIGNATURE\" => \"\"\"\n    <address>Apache\/2.4.9 (Win32) OpenSSL\/1.0.1g PHP\/5.5.11 Server at localhost Port 80<\/address>\n    \"\"\"\n  \"SERVER_SOFTWARE\" => \"Apache\/2.4.9 (Win32) OpenSSL\/1.0.1g PHP\/5.5.11\"\n  \"SERVER_NAME\" => \"localhost\"\n  \"SERVER_ADDR\" => \"::1\"\n  \"SERVER_PORT\" => \"80\"\n  \"REMOTE_ADDR\" => \"::1\"\n  \"DOCUMENT_ROOT\" => \"C:\/xampp\/htdocs\/webike2.0\/public\"\n  \"REQUEST_SCHEME\" => \"http\"\n  \"CONTEXT_PREFIX\" => \"\"\n  \"CONTEXT_DOCUMENT_ROOT\" => \"C:\/xampp\/htdocs\/webike2.0\/public\"\n  \"SERVER_ADMIN\" => \"postmaster@localhost\"\n  \"SCRIPT_FILENAME\" => \"C:\/xampp\/htdocs\/webike2.0\/public\/index.php\"\n  \"REMOTE_PORT\" => \"51100\"\n  \"REDIRECT_URL\" => \"\/admin\/mail\/112\"\n  \"GATEWAY_INTERFACE\" => \"CGI\/1.1\"\n  \"SERVER_PROTOCOL\" => \"HTTP\/1.1\"\n  \"REQUEST_METHOD\" => \"GET\"\n  \"QUERY_STRING\" => \"\"\n  \"REQUEST_URI\" => \"\/admin\/mail\/112\"\n  \"SCRIPT_NAME\" => \"\/index.php\"\n  \"PHP_SELF\" => \"\/index.php\"\n  \"REQUEST_TIME_FLOAT\" => 1427160200.865\n  \"REQUEST_TIME\" => 1427160200\n]","request_cookies":"array:2 [\n  \"remember_82e5d2c56bdd0811318f0cf078b78bfc\" => \"5415|s7LYAhtxupgEbcTEWh78jNWSD4DOkp7aNuY99CRPrItL3UyrIENxR4SncGlt\"\n  \"laravel_session\" => \"06930008a751636f56f830c347cd84451a5adf7c\"\n]","response_headers":"array:3 [\n  \"cache-control\" => array:1 [\n    0 => \"no-cache\"\n  ]\n  \"date\" => array:1 [\n    0 => \"Tue, 24 Mar 2015 01:23:22 GMT\"\n  ]\n  \"content-type\" => array:1 [\n    0 => \"text\/html; charset=UTF-8\"\n  ]\n]","path_info":"\/admin\/mail\/112","session_attributes":"array:3 [\n  \"_token\" => \"n4LTQB5YZDzky5ur25VxNrvAAjBzQcbw0VoY3Okk\"\n  \"PHPDEBUGBAR_STACK_DATA\" => []\n  \"flash\" => array:2 [\n    \"old\" => []\n    \"new\" => []\n  ]\n]"}}, "44d92131d1d46f91d5c3243c649b000a");

</script><div class="phpdebugbar phpdebugbar-minimized phpdebugbar-closed"><div class="phpdebugbar-drag-capture"></div><div class="phpdebugbar-resize-handle" style="display: none;"></div><div class="phpdebugbar-header" style="display: none;"><div class="phpdebugbar-header-left"><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-list-alt"></i><span class="phpdebugbar-text">Messages</span><span class="phpdebugbar-badge" style="display: none;"></span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-tasks"></i><span class="phpdebugbar-text">Timeline</span><span class="phpdebugbar-badge"></span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-bug"></i><span class="phpdebugbar-text">Exceptions</span><span class="phpdebugbar-badge" style="display: none;"></span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-leaf"></i><span class="phpdebugbar-text">Views</span><span class="phpdebugbar-badge" style="display: inline;">3</span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-share"></i><span class="phpdebugbar-text">Route</span><span class="phpdebugbar-badge"></span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-inbox"></i><span class="phpdebugbar-text">Queries</span><span class="phpdebugbar-badge" style="display: inline;">4</span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-inbox"></i><span class="phpdebugbar-text">Mails</span><span class="phpdebugbar-badge" style="display: none;"></span></a><a href="javascript:" class="phpdebugbar-tab"><i class="fa fa-tags"></i><span class="phpdebugbar-text">Request</span><span class="phpdebugbar-badge"></span></a></div><div class="phpdebugbar-header-right"><a href="javascript:" class="phpdebugbar-close-btn"></a><a href="javascript:" class="phpdebugbar-open-btn" style="display: block;"></a><select class="phpdebugbar-datasets-switcher"><option value="44d92131d1d46f91d5c3243c649b000a">#1 112 (09:23:22)</option></select><span class="phpdebugbar-indicator"><i class="fa fa-clock-o"></i><span class="phpdebugbar-text">1.18s</span><span class="phpdebugbar-tooltip">Request Duration</span></span><span class="phpdebugbar-indicator"><i class="fa fa-cogs"></i><span class="phpdebugbar-text">10.25MB</span><span class="phpdebugbar-tooltip">Memory Usage</span></span><span class="phpdebugbar-indicator"><i class="fa fa-share"></i><span class="phpdebugbar-text">GET admin/mail/{template_code}</span><span class="phpdebugbar-tooltip">Route</span></span></div></div><div class="phpdebugbar-body" style="height: 40px; display: none;"><div class="phpdebugbar-panel"><div class="phpdebugbar-widgets-messages"><ul class="phpdebugbar-widgets-list"></ul><div class="phpdebugbar-widgets-toolbar"><i class="fa fa-search"></i><input type="text"></div></div></div><div class="phpdebugbar-panel"><ul class="phpdebugbar-widgets-timeline"><li><div class="phpdebugbar-widgets-measure"><span class="phpdebugbar-widgets-value" style="left: 0%; width: 47%;"></span><span class="phpdebugbar-widgets-label">Booting (553.03ms)</span></div></li><li><div class="phpdebugbar-widgets-measure"><span class="phpdebugbar-widgets-value" style="left: 48%; width: 51%;"></span><span class="phpdebugbar-widgets-label">Application (602.03ms)</span></div></li><li><div class="phpdebugbar-widgets-measure"><span class="phpdebugbar-widgets-value" style="left: 99%; width: 1%;"></span><span class="phpdebugbar-widgets-label">After application (14ms)</span></div></li></ul></div><div class="phpdebugbar-panel"><div class="phpdebugbar-widgets-exceptions"><ul class="phpdebugbar-widgets-list"></ul></div></div><div class="phpdebugbar-panel"><div class="phpdebugbar-widgets-templates"><div class="phpdebugbar-widgets-status"><span>3 templates were rendered</span></div><ul class="phpdebugbar-widgets-list"><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><span class="phpdebugbar-widgets-name">emails.order.112 (\app\views\emails\order\112.blade.php)</span><span title="Parameter count" class="phpdebugbar-widgets-param-count">1</span><span title="Type" class="phpdebugbar-widgets-type">blade</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value"><pre><code>mail_category</code></pre></td></tr></tbody></table></li><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><span class="phpdebugbar-widgets-name">emails.template.order (\app\views\emails\template\order.blade.php)</span><span title="Parameter count" class="phpdebugbar-widgets-param-count">6</span><span title="Type" class="phpdebugbar-widgets-type">blade</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value"><pre><code>obLevel</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">1</td><td class="phpdebugbar-widgets-value"><pre><code>__env</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">2</td><td class="phpdebugbar-widgets-value"><pre><code>app</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">3</td><td class="phpdebugbar-widgets-value"><pre><code>errors</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">4</td><td class="phpdebugbar-widgets-value"><pre><code>mail_category</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">5</td><td class="phpdebugbar-widgets-value"><pre><code>i</code></pre></td></tr></tbody></table></li><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><span class="phpdebugbar-widgets-name">emails.template.footer (\app\views\emails\template\footer.blade.php)</span><span title="Parameter count" class="phpdebugbar-widgets-param-count">6</span><span title="Type" class="phpdebugbar-widgets-type">blade</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value"><pre><code>obLevel</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">1</td><td class="phpdebugbar-widgets-value"><pre><code>__env</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">2</td><td class="phpdebugbar-widgets-value"><pre><code>app</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">3</td><td class="phpdebugbar-widgets-value"><pre><code>errors</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">4</td><td class="phpdebugbar-widgets-value"><pre><code>mail_category</code></pre></td></tr><tr><td class="phpdebugbar-widgets-name">5</td><td class="phpdebugbar-widgets-value"><pre><code>i</code></pre></td></tr></tbody></table></li></ul></div></div><div class="phpdebugbar-panel"><dl class="phpdebugbar-widgets-kvlist phpdebugbar-widgets-varlist"><dt class="phpdebugbar-widgets-key"><span title="uri">uri</span></dt><dd class="phpdebugbar-widgets-value">GET admin/mail/{template_code}</dd><dt class="phpdebugbar-widgets-key"><span title="as">as</span></dt><dd class="phpdebugbar-widgets-value">mail-template</dd><dt class="phpdebugbar-widgets-key"><span title="controller">controller</span></dt><dd class="phpdebugbar-widgets-value">Backend\MailController@getTemplate</dd><dt class="phpdebugbar-widgets-key"><span title="namespace">namespace</span></dt><dd class="phpdebugbar-widgets-value">null</dd><dt class="phpdebugbar-widgets-key"><span title="prefix">prefix</span></dt><dd class="phpdebugbar-widgets-value">admin/mail</dd><dt class="phpdebugbar-widgets-key"><span title="where">where</span></dt><dd class="phpdebugbar-widgets-value"></dd><dt class="phpdebugbar-widgets-key"><span title="file">file</span></dt><dd class="phpdebugbar-widgets-value">\app\controllers\Backend\MailController.php:5-10</dd></dl></div><div class="phpdebugbar-panel"><div class="phpdebugbar-widgets-sqlqueries"><div class="phpdebugbar-widgets-status"><span>4 statements were executed</span><span title="Accumulated duration" class="phpdebugbar-widgets-duration">470.04ms</span></div><ul class="phpdebugbar-widgets-list"><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><code class="phpdebugbar-widgets-sql"><span class="hljs-operator"><span class="hljs-keyword">select</span> * <span class="hljs-keyword">from</span> <span class="hljs-string">`tracker_sessions`</span> <span class="hljs-keyword">where</span> <span class="hljs-string">`session_id`</span> = <span class="hljs-string">'06930008a751636f56f830c347cd84451a5adf7c'</span> limit <span class="hljs-number">1</span></span></code><span title="Duration" class="phpdebugbar-widgets-duration">119.01ms</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value">06930008a751636f56f830c347cd84451a5adf7c</td></tr><tr><td class="phpdebugbar-widgets-name">hints</td><td class="phpdebugbar-widgets-value">Use <code>SELECT *</code> only if you need all columns from table<br><code>LIMIT</code> without <code>ORDER BY</code> causes non-deterministic results, depending on the query execution plan</td></tr></tbody></table></li><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><code class="phpdebugbar-widgets-sql"><span class="hljs-operator"><span class="hljs-keyword">select</span> * <span class="hljs-keyword">from</span> <span class="hljs-string">`customers`</span> <span class="hljs-keyword">where</span> <span class="hljs-string">`id`</span> = <span class="hljs-string">'5415'</span> <span class="hljs-keyword">and</span> <span class="hljs-string">`remember_token`</span> = <span class="hljs-string">'s7LYAhtxupgEbcTEWh78jNWSD4DOkp7aNuY99CRPrItL3UyrIENxR4SncGlt'</span> limit <span class="hljs-number">1</span></span></code><span title="Duration" class="phpdebugbar-widgets-duration">116.01ms</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value">5415</td></tr><tr><td class="phpdebugbar-widgets-name">1</td><td class="phpdebugbar-widgets-value">s7LYAhtxupgEbcTEWh78jNWSD4DOkp7aNuY99CRPrItL3UyrIENxR4SncGlt</td></tr><tr><td class="phpdebugbar-widgets-name">hints</td><td class="phpdebugbar-widgets-value">Use <code>SELECT *</code> only if you need all columns from table<br><code>LIMIT</code> without <code>ORDER BY</code> causes non-deterministic results, depending on the query execution plan</td></tr></tbody></table></li><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><code class="phpdebugbar-widgets-sql"><span class="hljs-operator"><span class="hljs-keyword">update</span> <span class="hljs-string">`tracker_sessions`</span> <span class="hljs-keyword">set</span> <span class="hljs-string">`path`</span> = <span class="hljs-string">'http://localhost/admin/mail/112'</span>, <span class="hljs-string">`is_robot`</span> = <span class="hljs-string">'0'</span>, <span class="hljs-string">`updated_at`</span> = <span class="hljs-string">'2015-03-24 09:23:21'</span> <span class="hljs-keyword">where</span> <span class="hljs-string">`id`</span> = <span class="hljs-string">'16734'</span></span></code><span title="Duration" class="phpdebugbar-widgets-duration">118.01ms</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value">http://localhost/admin/mail/112</td></tr><tr><td class="phpdebugbar-widgets-name">1</td><td class="phpdebugbar-widgets-value">0</td></tr><tr><td class="phpdebugbar-widgets-name">2</td><td class="phpdebugbar-widgets-value">2015-03-24 09:23:21</td></tr><tr><td class="phpdebugbar-widgets-name">3</td><td class="phpdebugbar-widgets-value">16734</td></tr></tbody></table></li><li class="phpdebugbar-widgets-list-item" style="cursor: pointer;"><code class="phpdebugbar-widgets-sql"><span class="hljs-operator"><span class="hljs-keyword">select</span> * <span class="hljs-keyword">from</span> <span class="hljs-string">`mail_category`</span> <span class="hljs-keyword">where</span> <span class="hljs-string">`code`</span> = <span class="hljs-string">'112'</span> limit <span class="hljs-number">1</span></span></code><span title="Duration" class="phpdebugbar-widgets-duration">117.01ms</span><table class="phpdebugbar-widgets-params"><tbody><tr><th colspan="2">Params</th></tr><tr><td class="phpdebugbar-widgets-name">0</td><td class="phpdebugbar-widgets-value">112</td></tr><tr><td class="phpdebugbar-widgets-name">hints</td><td class="phpdebugbar-widgets-value">Use <code>SELECT *</code> only if you need all columns from table<br><code>LIMIT</code> without <code>ORDER BY</code> causes non-deterministic results, depending on the query execution plan</td></tr></tbody></table></li></ul></div></div><div class="phpdebugbar-panel"><div class="phpdebugbar-widgets-mails"><ul class="phpdebugbar-widgets-list"></ul></div></div><div class="phpdebugbar-panel"><dl class="phpdebugbar-widgets-kvlist phpdebugbar-widgets-varlist"><dt class="phpdebugbar-widgets-key"><span title="format">format</span></dt><dd class="phpdebugbar-widgets-value">html</dd><dt class="phpdebugbar-widgets-key"><span title="content_type">content_type</span></dt><dd class="phpdebugbar-widgets-value">text/html; charset=UTF-8</dd><dt class="phpdebugbar-widgets-key"><span title="status_text">status_text</span></dt><dd class="phpdebugbar-widgets-value">OK</dd><dt class="phpdebugbar-widgets-key"><span title="status_code">status_code</span></dt><dd class="phpdebugbar-widgets-value">200</dd><dt class="phpdebugbar-widgets-key"><span title="request_query">request_query</span></dt><dd class="phpdebugbar-widgets-value">[]</dd><dt class="phpdebugbar-widgets-key"><span title="request_request">request_request</span></dt><dd class="phpdebugbar-widgets-value">[]</dd><dt class="phpdebugbar-widgets-key"><span title="request_headers">request_headers</span></dt><dd class="phpdebugbar-widgets-value">array:7 [
  "host" =&gt; array:1 [
    0 =&gt; "localhost"
  ]
  "connection" =&gt; array:1 [
    0 =&gt; "keep-...</dd><dt class="phpdebugbar-widgets-key"><span title="request_server">request_server</span></dt><dd class="phpdebugbar-widgets-value">array:48 [
  "REDIRECT_MIBDIRS" =&gt; "C:/xampp/php/extras/mibs"
  "REDIRECT_MYSQL_HOME" =&gt; "\xampp\mys...</dd><dt class="phpdebugbar-widgets-key"><span title="request_cookies">request_cookies</span></dt><dd class="phpdebugbar-widgets-value">array:2 [
  "remember_82e5d2c56bdd0811318f0cf078b78bfc" =&gt; "5415|s7LYAhtxupgEbcTEWh78jNWSD4DOkp7aNuY...</dd><dt class="phpdebugbar-widgets-key"><span title="response_headers">response_headers</span></dt><dd class="phpdebugbar-widgets-value">array:3 [
  "cache-control" =&gt; array:1 [
    0 =&gt; "no-cache"
  ]
  "date" =&gt; array:1 [
    0 =&gt; "Tue...</dd><dt class="phpdebugbar-widgets-key"><span title="path_info">path_info</span></dt><dd class="phpdebugbar-widgets-value">/admin/mail/112</dd><dt class="phpdebugbar-widgets-key"><span title="session_attributes">session_attributes</span></dt><dd class="phpdebugbar-widgets-value">array:3 [
  "_token" =&gt; "n4LTQB5YZDzky5ur25VxNrvAAjBzQcbw0VoY3Okk"
  "PHPDEBUGBAR_STACK_DATA" =&gt; []
...</dd></dl></div></div><a href="javascript:" class="phpdebugbar-restore-btn" style=""></a></div><div class="phpdebugbar-openhandler" style="display: none;"><div class="phpdebugbar-openhandler-header">PHP DebugBar | Open<a href="javascript:"><i class="fa fa-times"></i></a></div><table><thead><tr><th width="150">Date</th><th width="55">Method</th><th>URL</th><th width="125">IP</th><th width="100">Filter data</th></tr></thead><tbody></tbody></table><div class="phpdebugbar-openhandler-actions"><a href="javascript:">Load more</a><a href="javascript:">Show only current URL</a><a href="javascript:">Show all</a><a href="javascript:">Delete all</a><form><br><b>Filter results</b><br>Method: <select name="method"><option></option><option>GET</option><option>POST</option><option>PUT</option><option>DELETE</option></select><br>Uri: <input type="text" name="uri"><br>IP: <input type="text" name="ip"><br><button type="submit">Search</button></form></div></div><div class="phpdebugbar-openhandler-overlay" style="display: none;"></div>
 </body></html>