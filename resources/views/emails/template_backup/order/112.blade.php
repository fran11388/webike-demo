@extends('emails.template.order')
@section('title')
	{{$mail_category->title}}訂單編號
<script type="text/javascript">
    $('#mail').inlineCSS();
    $('.borderbox').inlineCSS();
    $('.mail_footer').inlineCSS();
    $('.title label').inlineCSS();
    $('p.gap').inlineCSS();
    $('p').inlineCSS();
    $('ul.indent li').inlineCSS();
    $('.point').inlineCSS();
    $('table').inlineCSS();
    $('table.total').inlineCSS();
    $('table.total th').inlineCSS();
    $('table.total td').inlineCSS();
    $('table.customer').inlineCSS();
    $('table.customer tr').inlineCSS();
    $('table.customer td').inlineCSS();
    $('table.customer th').inlineCSS();
    $('table.items tr').inlineCSS();
</script>
@stop
@section('content')
	<p>[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳 筱玲，感謝您本次的訂購。</p>
@stop
@section('table_title')
	{{$mail_category->name}}
@stop
@section('table')

<div class="borderbox" style="padding:10px;">
	<p>訂單編號:#</p>
</div>
<div class="borderbox">
	<table class="customer">
		<tr>
			<th>會員名稱 :</th>
			<td>江 百裕</td>
			<th>連絡電話 :</th>
			<td>
				T: 06-2313577<br/>
				M: 0982509753<br/>
			</td>
			<th>訂購時間 :</th>
			<td>
				2014年5月12日<br/>
				下午1時32分07秒
			</td>
		</tr>
		<tr>
			<th>付款方式 :</th>
			<td></td>
		</tr>
	</table>
</div>
<div class="borderbox">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">No.</th>
				<th width="15%">品牌</th>
				<th width="25%">商品名稱</th>
				<th width="20%">商品編號</th>
				<th width="10%">數量</th>
				<th width="10%">單價</th>
				<th width="15%">小計</th>
			</tr>
		</thead>
		<tbody>
			@for ($i=1; $i <= 5 ; $i++)
				<tr>
					<td align="center">1</td>
					<td align="center">HONDA</td>
					<td>皮革吊環式鑰匙圈</td>
					<td align="center">0SYEPN98KF</td>
					<td align="center">1</td>
					<td align="center">NT$ 222</td>
					<td align="center">NT$ 222</td>
				</tr>
			@endfor
		</tbody>
	</table>
</div>
<div class="borderbox" style="text-align:center">
	<p class="point">您所訂購的商品預計的交貨期為 XX 個工作天。</p>
</div>
<div class="borderbox">
	<ul>
		<li>請注意:</li>
		<li>1.以上交期為所有商品備齊一同寄出的交貨時間，出貨前我們將會以e-mail通知您。</li>
		<li>2.若是有突發狀況導致商品延誤、缺貨或是停售時，我們將主動在第一時間告知您。</li>
		<li>3.工作天數計算不包含台灣或日本的例假日。</li>
	</ul>
</div>
@stop
