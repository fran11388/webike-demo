@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您的回覆。</p>
	<p class="gap">您的訂單編號 #</p>
	<p class="gap">我們已經將您的訂單取消。</p>
	<p class="gap red_text">信用卡消費金額也同時為您向銀行取消</p>
	<p class="gap">歡迎您下次再度光臨「Webike台灣」。</p>
@stop
@section('description')
@stop
@section('tips')
<p class="red_text">※若您有不清楚的地方尚請來電{{ hsaNewOffice() ? "(02)22982020" : "(02)29041080 分機11" }}</p>
@stop