@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。</p>
	<p>訂單編號 #</p>
	<p>非常抱歉，您本次所訂購的商品：</p>
	<ul class="indent">
	@for ($i=1; $i <= 5; $i++)
		<li>({{$i}})</li>
	@endfor
	</ul>
@stop
@section('description')
<p class="gap">由於廠商出貨延誤，交貨必須延長至 XX 個工作天(工作天數計算不包含台灣或日本的例假日)，造成您的不便，我們感到非常抱歉。</p>
<p class="gap">目前您的訂單暫時保留，在這裡想請教您處理的方式：</p>
<ul class="indent">
	<li>(1)願意等待，等商品備齊後再寄出。</li>
	<li>(2)修改訂單取消交期過長的商品，其他商品繼續訂購。</li>
	<li>(到貨時間將縮短，取消後如訂購總金額未滿3000需加收100元運費)</li>
</ul>
@stop
@section('tips')
<p class="red_text">※請您於一週內直接回覆本信件，若您有不清楚的地方請來電{{ hsaNewOffice() ? "(02)22982020" : "(02)29041080 分機11" }}</p>
@stop