@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。</p>
	<p>訂單編號 #</p>
	<p>非常抱歉，您本次訂單其中有：</p>
	<ul class="indent">
	@for ($i=1; $i <= 5; $i++)
		<li>({{$i}})</li>
	@endfor
	</ul>
	<p class="gap">上述商品已"售完"，無法訂購。</p>
	<p class="gap">其餘商品統一交貨期為 XX 個工作天(工作天數計算不包含台灣或日本的例假日)。</p>
	<p class="gap">由於缺貨的關係，我們將修改您的訂單，在這裡想請教您處理的方式：</p>
@stop
@section('description')
<ul class="indent">
	<li>(1)取消"售完的商品"，其他商品照正常訂購程序進行。</li>
	<li>　(取消後如訂單總金額未滿3000需加收100元運費)</li>
	<li>(2)售完缺貨商品改訂其他配色或尺寸規格(需要重新查詢庫存確認)。</li>
	<li>(3)取消整筆訂單，請您到「Webike-摩托百貨」再次選購。</li>
</ul>
@stop
@section('tips')
<p class="red_text">※請您於一週內直接回覆本信件，若您有不清楚的地方尚請來電{{ hsaNewOffice() ? "(02)22982020" : "(02)29041080 分機11" }}</p>
@stop