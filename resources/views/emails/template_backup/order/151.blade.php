@extends('emails.template.article')
@section('mail_name')
	{{$mail_category->code}}{{$mail_category->name}}
@stop
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p>非常感謝您使用<a href="{{route('shopping')}}">「Webike-摩托百貨」</a><a href="{{URL::route('genuineparts')}}">正廠零件查詢系統</a>。</p>
@stop
@section('description')
<ul class="indent">
	<li>查詢編號：W1214412</li>
	<li>商品明細(1)HONDA 53175-KAE-730 x 10</li>
</ul>
@stop
@section('tips')
<p>
您的查詢已正常受理，本郵件為確認信箱地址而自動發送的郵件。<br/>
我們已經向廠商查詢商品資訊，約1~2個工作天我們會回復至您得信箱。<br/>
如超過3個工作天仍未收回復，請您來信service@webike.tw<br/>
</p>
@stop