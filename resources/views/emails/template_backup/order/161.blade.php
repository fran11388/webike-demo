@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">非常感謝您使用<a href="{{URL::route('mitumori')}}">「Webike-摩托百貨」未登錄商品查詢系統。</a></p><br/>
	<p>您的查詢已正常受理，本郵件為確認信箱地址而自動發送的郵件。</p>
	<p>我們已經向廠商查詢商品資訊，約1~2個工作天我們會回覆至您的信箱。</p>
	<p>如超過3個工作天仍未收到回覆，請您來信service@webike.tw</p>
@stop
@section('description')
<ul class="indent">
	<li>查詢編號： W1214412</li>
	<li>商品明細(1)HONDA 53175-KAE-730 x 1</li>
</ul>
@stop
