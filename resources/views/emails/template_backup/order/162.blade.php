@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">非常感謝您使用<a href="{{URL::route('mitumori')}}">「Webike-摩托百貨」未登錄商品查詢系統。</a></p><br/>
	<p>您的報價查詢已完成。</p>
	<p>請由下方連結進入：</p>
@stop
@section('description')
<ul class="indent">
	<li>報價編號：</li>
	<li>{{URL::route('history-mitumori-detail')}}</li>
	<li>報價日： 2015/03/22</li>
	<li>有效期限：報價日起7天(超過有效期限報價內容將不能瀏覽與購買)</li>
</ul>
@stop
@section('tips')
<p>您購買的商品可經由<a href="{{URL::route('history-mitumori')}}">「未登錄商品查詢履歷」</a>頁面將商品放入購物車後進行結帳。 </p>
<p>注意事項：<br/>
■可以與一般商品同時訂購<br/>
您可以把一般商品與未登錄商品分別加入購物車後再一起結帳。<br/>
若您要修改購買數量，請您在購物車中修改。<br/>
</p>

<p>
	■發票開立<br/>
	本公司合法進口、販售摩托車相關商品，所有價格均包含進口貨物稅以及營業稅， 不論購買的金額依法開立統一發票。<br/>
</p>

<p>
	■關於退換貨<br/>
	購買商品前請您詳細閱讀<a href="{{URL::route('customer-rule', 'member_rule_2')}}#M">「退換貨說明」</a>。<br/>
</p>
@stop
