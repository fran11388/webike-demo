@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。</p>
	<p>訂單編號 #</p>
	<p>我們已經收到您的匯款金額新台幣： 元。</p>
@stop
@section('description')
<p>
同一時間我們也著手商品的訂購作業，當所有商品備齊準備出貨時，我們將以e-mail通知您。<br/>
若是有突發狀況導致商品延誤、缺貨或是停售時，我們將主動在第一時間通知您。<br/>
您也可以至"<a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a>"查看目前的訂單狀態，若您有其他問題與建議請"<a href="{{route('customer-service-information')}}">聯絡我們</a>"。<br/>
</p>
@stop
