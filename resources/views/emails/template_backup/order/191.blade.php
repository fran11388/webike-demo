@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。</p>
	<p>我們已經為您查詢您的訂單，訂單編號#W12220114</p>
@stop
@section('description')
<ul class="indent">
	<li>目前狀態為：</li>
	<li>預計於 年 月 日廠商出貨</li>
	<li>年 月 日抵達台灣完成進口報關手續</li>
	<li>年 月 日進行配送</li>
	<li>年 月 日配送到您訂單指定收件地址</li>
	<li>收單交易時間：</li>
	<li>簽帳單序號：</li>
	<li>授權碼：</li>
	<li>卡號：</li>
</ul>
@stop
@section('tips')
<p>
當商品配送後我們會寄發通知信至您的E-MAIL信箱。<br/>
您也可以至"<a href="{{route('customer-history-order')}}">訂單動態及歷史履歷</a>"查看目前的訂單狀態，若您有其他問題與建議請"<a href="{{route('customer-service-information')}}">聯絡我們</a>"。<br/>
</p>
@stop