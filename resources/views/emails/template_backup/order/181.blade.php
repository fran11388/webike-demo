@extends('emails.template.article')
@section('title')
	{{$mail_category->title}}
@stop
@section('content')
	<p class="gap">[江 百裕]先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您本次的訂購。</p>
	<p>訂單編號 #</p>
	<p>非常抱歉，您本次訂單其中有：</p>
	<p>您所購買的 XXXXX 商品</p>
	<p>由於廠商作業關係，需要您提供車輛詳細資料進行核對。</p>
	<p>請您提供商品對應的車輛資訊：</p>
@stop
@section('description')
<ul class="indent">
	<li>1.車型名稱：</li>
	<li>2.出廠年分：</li>
	<li>(請填寫車輛出廠年分，非掛牌年份)</li>
	<li>3.車身號碼：</li>
	<li>(檔車位於車身龍頭靠近三角台部位，速克達一般位於踏板下內部方左右兩側骨架)</li>
	<li>4.車身顏色色碼：</li>
	<li>(如零件有顏色區別才需要填寫)</li>
</ul>
@stop
@section('tips')
<p class="red_text">※以上車輛補充資訊請您盡速回覆以利出貨作業。</p>
@stop