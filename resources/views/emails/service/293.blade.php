<p><span style="color:rgb(34, 34, 34); font-family:arial">親愛的Webike會員您好</span></p>

<p>&nbsp;</p>

<p><span style="color:rgb(34, 34, 34); font-family:arial">因應本站導入電子發票系統，為避免電子發票發送至無效的電子信箱，請協助我們進行有效電子郵件驗證。</span></p>

<p><span style="color:rgb(34, 34, 34); font-family:arial">-----------------------------------------------</span></p>

<p><span style="color:rgb(34, 34, 34); font-family:arial">帳號驗證說明</span></p>

<p><span style="color:rgb(34, 34, 34); font-family:arial">-----------------------------------------------</span></p>

<p><span style="color:rgb(34, 34, 34); font-family:arial">請您點選以下驗證連結以進行帳號驗證：</span></p>

<p>&nbsp;</p>

<p><a href="{{ request()->getHttpHost().'/customer/account/verify-email?code='.$uuid.'&number='.$id }}" target="_blank">{{ request()->getHttpHost().'/customer/account/verify-email?code='.$uuid.'&number='.$id }}</a></p>

<p>&nbsp;</p>

<p><span style="color:rgb(34, 34, 34); font-family:arial">謝謝您的配合，祝您購物愉快！</span></p>