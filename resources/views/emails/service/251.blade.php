<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 410px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" id="mail">
        <div> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城" style="background: url({{assetRemote('/image/tmp_webike_tw_logos.png')}});background-position: -200px 0px;background-repeat: no-repeat;height: 80px;width: 180px;;display: block;"> </a> </div>
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 15px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="gap">{{$review->customer->realname}}先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您撰寫商品評論。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">您撰寫的評論已正常受理，本郵件為確認信箱地址而自動發送的郵件。</p>
        </div>
        <div class="description">
            ****************************************************************************************************  <br> 
            <ul>
                <li>我們的人員已經在審核您的評論，約3~4個工作天我們會將結果回覆至您的信箱。</li>
                <li>投稿商品：{{$review->product->name}}</li>
                <li>評論標題：{{$review->title}}</li>
                <li>評論內文：{{$review->content}}</li>
            </ul>
            <br>  **************************************************************************************************** 
        </div>
        <div class="tips">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 700; color: rgb(255, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="red_text">如超過4個工作天仍未收回覆，請您來信service@webike.tw</p>
        </div>
        <hr>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        <hr>
    </div>
</body>