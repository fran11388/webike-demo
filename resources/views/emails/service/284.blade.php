<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 458px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" id="mail">
        <div class="title"> <label style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 478px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">  「Webike-摩托車市」：您的刊登的物件{{$bike->product_name}}賣家已回覆問題!  </label> </div>
        <div> <a href="http://www.webike.tw/motomarket" title="「Webike-摩托車市」"> <img src="https://www.webike.tw/motomarket/assets/img/header/logobikeshop.png" alt="「Webike-摩托車市」"> </a> </div>
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 15px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="gap">{{$bike->asker}}先生/小姐您好，我是<a href="http://www.webike.tw/motomarket">「Webike-摩托車市」</a>的服務人員 吳筱玲，感謝您本次的詢問。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">您詢問的問題賣家已經有了回應</p>
        </div>
        <div class="description">
            ****************************************************************************************************  <br>
            <ul>
                <li>【物件資訊】</li>
                <li>【物件名稱】【{{$bike->product_name}}</li>
                <li>【物件網址】<a href="http://www.webike.tw/motomarket/detail/{{$bike->serial}}">http://www.webike.tw/motomarket/detail/{{$bike->serial}}</a></li>
                <li>【買家的問題】</li>
                <li>{{$bike->asker}}的問題：</li>
                <li>{{$bike->content}}</li>
                <li></li>
                <li>賣家的回覆</li>
                <li>{{$bike->content_reply}}</li>
                <li></li>
                <li><a href="{{'http://www.webike.tw/motomarket/detail/'.$bike->serial.'?tab=qa'}}" target="_blank;">點我回覆賣家問題</a></li>
            </ul>
            <br>  ****************************************************************************************************
        </div>
        <div class="tips">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 700; color: rgb(255, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="red_text">◎若您有任何問題，請您來信：service@webike.tw</p>
        </div>
        <hr>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="http://www.webike.tw/motomarket" title="「Webike-摩托車市」">「Webike-摩托車市」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="http://www.webike.tw/motomarket" title="「Webike-摩托車市」">www.webike.tw/motomarket</a> </label> </div>
        <hr>
    </div>
</body>