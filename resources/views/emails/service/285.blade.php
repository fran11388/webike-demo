<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<div id="mail" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px;  float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
   <div class="title" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px;  float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> <label>  「Webike-摩托車市」{{$bike->product_name}} 新物件通知!  </label> </div>
   <div> <a href="{!! route('shopping') !!}" title="「Webike-摩托車市」"> <img src="https://www.webike.tw/motomarket/assets/img/header/logobikeshop.png" alt="「Webike-摩托車市」"> </a> </div>
   <div class="content" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px;  float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
      <hr>
      <p>「Webike-摩托車市」您追蹤的車輛物件更新({{date('Y/m/d')}}更新)</p>
      <hr>
      <br>  
   </div>
   <div class="description" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px;  float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
      ****************************************************************************************************  <br> 
      <ul style="list-style:none;">
         <?php 
         $motor_key = ''; 
         $cate_num = 0;
         ?>
         @foreach ($bike->products as $motor_id => $motor_products)
            <?php $cate_num++; ?>
            @if( $motor_key != $motor_id )
               <li>────────────────────────────────────</li>
               <li>({{$cate_num}}) {{$bike->motors[$motor_id]}}本周最新物件</li>
               <li>────────────────────────────────────</li>
            @endif
            
            @foreach ($motor_products as $key => $product)
               <li>{{$key + 1}}.{{$product}}</li>
            @endforeach
         @endforeach
      </ul>
      <br>  **************************************************************************************************** 
   </div>
   <div class="tips" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px;  float: none; display: block; position: static; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">
      <p>p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
      <p>發行：「Webike-摩托車市」</p>
      <p>●取消或修改自動追蹤：<a style="text-decoration:none;" target="_blank" href="http://www.webike.tw/motomarket/customer/follow?default_tab=3">http://www.webike.tw/motomarket/customer/follow?default_tab=3</a></p>
      <p>●刊登新物件：<a style="text-decoration:none;" target="_blank" href="http://www.webike.tw/motomarket/customer/product/create">http://www.webike.tw/motomarket/customer/product/create</a></p>
      <p>●進口車物件：<a style="text-decoration:none;" target="_blank" href="http://www.webike.tw/motomarket/search/imp">http://www.webike.tw/motomarket/search/imp</a></p>
      <p>●國產車物件：<a style="text-decoration:none;" target="_blank" href="http://www.webike.tw/motomarket/search/dom">http://www.webike.tw/motomarket/search/dom</a></p>
      <p class="gap">●台灣No.1「Webike-摩托百貨」：<a style="text-decoration:none;" target="_blank" href="{{route('shopping')}}">{{route('shopping')}}</a></p>
   </div>
   <hr>
   <div class="mail_footer" style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 425px;  float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;"> <label><a style="text-decoration:none;" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城" href="{{route('shopping')}}">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a style="text-decoration:none;" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城" href="{{route('shopping')}}">{{route('shopping')}}</a> </label> </div>
   <hr>
</div>