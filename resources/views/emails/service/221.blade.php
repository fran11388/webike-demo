<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 808px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" id="mail">
        <div> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城" style="background: url({{assetRemote('/image/tmp_webike_tw_logos.png')}});background-position: -200px 0px;background-repeat: no-repeat;height: 80px;width: 180px;display: block;"> </a> </div>
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 15px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="gap">先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您註冊成為<a href="{{URL::route('customer')}}">【Webike-摩托百貨】會員。</a></p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">歡迎您加入「Webike-摩托百貨」，您已經完成【快速註冊】</p>
        </div>
        <div class="description">
            ****************************************************************************************************  
            @if($password)
                <ul>
                    您的密碼為:{{ $password }}，請妥善保存。<br>
                    <br>
                    除了使用Facebook登入之外<br>
                    您也可以使用申請Facebook時使用的「Email」以及「此密碼」登入會員。<br>
                    <br>
                    若您想修改密碼，也請至會員中心->基本資料管理中修改。<br>
                </ul>
            @endif
            <ul>
                <li>恭喜您獲得$100元體驗折價券，您可於購物車結帳時選擇使用!!</li>
            </ul>
            <br> 
            <ul>
                <li>折價券注意事項：</li>
            <li>1.請您先完成<a href="{{URL::route('customer-account-complete')}}">「完整註冊」</a>，開通購物功能後，方可使用。</li>
                <li>2.本體驗券有效期限為60天，並且每個帳號僅能使用一次。</li>
                <li>3.<a href="{{URL::route('customer-rule','member_rule_2').'#A'}}">購物流程說明</a></li>
                <li>4.<a href="{{URL::route('customer-rule','member_rule_2').'#L'}}">折價券使用說明</a></li>
            </ul>
            <br> 
            <ul>
                <li>您完成【快速註冊】之後，享有的功能有：</li>
                <li>1.「Webike-摩托百貨」電子報(每週五發送)。</li>
                <li>2.<a href="{{URL::route('genuineparts-step')}}">正廠零件查詢系統</a></li>
                <li>3.<a href="{{URL::route('mitumori')}}">未登錄商品查詢系統</a></li>
                <li>4.<a href="{{URL::route('groupbuy')}}">團購商品查詢系統</a></li>
                <li>5.<a href="http://www.webike.tw/motomarket/">「Webike-摩托車市」刊登服務</a></li>
            </ul>
            <br> 
            <ul>
                <li>【完整註冊】可享有更多好康及服務：</li>
                <li>1.改裝零件、騎士用品、正廠零件…等購物功能。</li>
                <li>2.購物滿額免運費。</li>
                <li>3.商品評論及討論功能。</li>
                <li>4.投稿評論拿50點點數。</li>
                <li>5.$200元生日禮券。</li>
                <li>6.愛車特惠活動通知。</li>
                <li><a href="{{URL::route('customer-account-complete')}}">點我馬上完成【完整註冊】</a></li>
            </ul>
            <br>  **************************************************************************************************** 
        </div>
        <div class="tips">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 700; color: rgb(255, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
        </div>
        <hr>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        <hr>
    </div>
</body>