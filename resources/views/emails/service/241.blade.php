<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <script src="/assets/js/inline.plugin.js"></script> 
    <link rel="stylesheet" type="text/css" href="/assets/css/editor_new.css">
</head>
<body>
    <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 394px; float: none; display: block; position: static; padding-top: 20px; padding-right: 20px; padding-bottom: 20px; padding-left: 20px; margin-top: 10px; margin-bottom: 10px; border-top: 3px solid rgb(204, 204, 204); border-right: 3px solid rgb(204, 204, 204); border-bottom: 3px solid rgb(204, 204, 204); border-left: 3px solid rgb(204, 204, 204); background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" id="mail">
        <div class="title"> <label style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 252px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">  「Webike-摩托百貨」：點數獲得通知信
            </label> 
        </div>
        <div> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"> <img src="{{ assetRemote('assets/images/shared/webike_shop_logo.png') }}" alt="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城"> </a> </div>
        <div class="content">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 15px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="gap">{{ $order->customer->realname }}先生/小姐您好，我是<a href="{{route('shopping')}}">「Webike-摩托百貨」</a>的服務人員 吳筱玲，感謝您對我們的支持。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">您此次在「Webike-摩托百貨」購物，訂單編號為：{{ $order->increment_id }}</p>
        </div>
        <div class="description">
            ****************************************************************************************************  <br> 
            <ul>
                <li>在這次的購物中您共獲得 {{$point->points_current}} 點點數，從 {{$point->start_date}} 開始可以使用您可以在<a href="{{URL::route('history-points')}}">【點數獲得及使用履歷】</a>中查詢點數獲得及使用的狀態。</li>
                <li>點數的使用方式和規則請參考「Webike-摩托百貨」<a href="{{ url('/customer/rule/pointinfo') }}">【回饋點數使用說明】</a></li>
            </ul>
            <br>  **************************************************************************************************** 
        </div>
        <div class="tips">
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;">p.s.本信件為系統自動發送，請勿直接回覆，謝謝。</p>
            <p style="font-size: 13px; font-family: sans-serif; font-weight: 700; color: rgb(255, 0, 0); letter-spacing: 1px; text-align: start; width: 800px; height: 16px; float: none; display: block; position: static; margin-top: 5px; margin-bottom: 5px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="red_text">◎若您有任何問題，請您來信：service@webike.tw </p>
        </div>
        <hr>
        <div style="font-size: 13px; font-family: sans-serif; font-weight: 400; color: rgb(0, 0, 0); letter-spacing: 1px; text-align: center; width: 446px; height: 15px; float: none; display: inline; position: static; margin-top: 10px; margin-bottom: 10px; background-color: transparent; background-image: none; background-repeat: repeat; background-position: 0% 0%; background-attachment: scroll;" class="mail_footer"> <label><a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">「Webike-摩托百貨」</a> 感謝您，歡迎您再次光臨!</label> <label> <a href="{{route('shopping')}}" title="「Webike-摩托百貨」-進口重機, 機車, 改裝零件, 騎士用品, 網路購物商城">www.webike.tw</a> </label> </div>
        <hr>
    </div>
</body>
