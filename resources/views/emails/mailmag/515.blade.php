<?php
$carts_item = $customer->carts()->orderBy('product_id')->get();
$gen_check = true;
$product_check = true;
$first_product = true;
$campaign = date("Ymd").'_promomail';
?>

<div style="background-color:#000;background-image:url(http://www.webike.tw/assets/images/benefit/sale/2016/fall/background1.gif);background-repeat:repeat;line-height:1.3;font-size:14px;color:#fff;text-align:left;padding:0 15px;background-position:center;"><!-- header -->
<div style="max-width:630px;margin:0 auto;"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-1.png" style="height:24px; line-height:20.8px; opacity:0.9; text-align:center; width:630px" /></div>


<div style="max-width:630px;margin:0 auto;background:#fff;padding:0 0 25px;">
<div style="padding:30px 15px 15px;"><a href="http://www.webike.tw/benefit/sale/2016/fall/ca/3000?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="2016SUMMER SALE" src="https://img-webike-tw-370429.c.cdn77.org/assets/images/user_upload/billboard/2016-09-30Random5GKic.png" style="height:240px; width:600px" /></a>
<span style="color:#000000"><span style="font-family:helvetica,microsoft jhenghei,新細明體,verdana,lihei pro,sans-serif; font-size:18px">
「Webike」品牌在2000年10月於日本東京成立，這16年來我們以騎士為出發點，發展各式符合騎士需求的功能以及跨越國境的相關服務。</span>
</span><span style="color:rgb(0, 0, 0)"><span style="font-family:helvetica,microsoft jhenghei,新細明體,verdana,lihei pro,sans-serif; font-size:18px">為期一個月的感謝季活動已經邁向最後倒數，這次特別推出&rdquo;滑滑9宮格</span></span><span style="color:rgb(0, 0, 0)"><span style="font-family:helvetica,microsoft jhenghei,新細明體,verdana,lihei pro,sans-serif; font-size:18px">&rdquo;贏取點數小遊戲、正廠零件95折、35萬項商品特價優惠等活動，歡迎各位會員共襄盛舉。</span></span><img src="https://www.webike.tw/assets/images/shared/pricedownmail-2.png" style="height:47px; line-height:20.8px; opacity:0.9; text-align:center; width:600px" /></div>

<div style="padding:30px 15px 15px;">


@foreach($carts_item as $item)
    <?php
        $product = $item->product;
        $product_check = false;
    ?>
    @if($product)
        @if($product->type == 0 and $product->p_country == 'J')
            <?php
                $images = $product->images;
                    $thumbnail = 'http://img.webike.net/catalogue/noimage.gif';
                    if (count($images)){
                        $thumbnail =  $images[0]->thumbnail;
                    }
                $old_price = $product->cost  / 0.98 ;
                $new_price = $product->getFinalpriceAttribute($customer);
                $new_point = $product->getFinalpointAttribute($customer);
                if($old_price > 0 ){
                    $discount = round( ($old_price - $new_price) / $old_price * 100  );
                }else{
                    $discount = 0;
                }
            ?>
            @if($discount > 5)
                    <table style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12px; line-height:20.8px; padding:20px; width:600px">
                        <tbody>
                            <tr>
                                <td rowspan="5" style="text-align:center; width:160px"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(17, 85, 204);" target="_blank"><img alt="購物車商品圖片" class="CToWUd" src="{{$thumbnail}}" style="border:1px solid rgb(204, 204, 204); height:140px; opacity:0.9; width:140px" /></a></td>
                                <td colspan="3"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank">{{$product->manufacturer->name . '：' . $product->name}}</a></td>
                            </tr>
                            <tr>
                                <td colspan="3">商品編號：{{$product->model_number}}</td>
                            </tr>
                            <tr>
                                <td><strong>原本定價:NT$</strong><s>{{number_format( $old_price )}}</s></td>
                                <td><strong>目前售價:<span style="color:rgb(255, 0, 0)"><span style="font-size:20px">NT$</span></span></strong><span style="color:rgb(255, 0, 0)"><span style="font-size:20px"><strong>{{ number_format($new_price) }}</strong></span></span></td>
                                <td>{{ $discount }}%OFF/{{number_format( $new_point )}}點點數回饋</td>
                            </tr>
                            <tr>
                              <?php
                                $models = $product->fitModels()->select(\DB::Raw("CONCAT(maker,' : ',model) as model_name"))->lists('model_name');
                                $model_name = implode('／', $models);
                            ?>
                                <td colspan="3">適合車型/對應型式:{{str_limit($model_name,45)}}</td>
                            </tr>
                            <tr>
                                <td colspan="3"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-3.png" /></a></td>
                            </tr>
                        </tbody>
                    </table>
                <p>&nbsp;</p>
            @endif
            @elseif($product->type == 1 and $product->active == 1 and $gen_check)
            <?php
                $gen_check = false;
                $thumbnail = 'http://img.webike.net/catalogue/noimage.gif';
                // $estimate_item = \GenuinePart\Item::where('product_id',$product->id)->first();
                $estimate_item = \Zero\Entities\Estimate\Item::where('product_id',$product->id)->first();

                $service = new \Ecommerce\Service\OrderService;
                $itemDetail = $service->getItemDetail($estimate_item);

                if(isset($itemDetail->original_price)){
                    $old_price = round($itemDetail->original_price);
                }else{
                    $old_price = $product->price;
                }

                // if($genuinepart->type == 2){
                //     $old_price = \Weight::setCustomer($customer)->setRate('TWD')->getGenuinePart($estimate_item->syouhin_teika,$product->manufacturer_id,true,2)->price_final;
                // }else{
                //     $old_price = \Weight::setCustomer($customer)->setRate('JPY')->getGenuinePart($estimate_item->syouhin_teika,$product->manufacturer_id,true,1)->price_final;    
                // }                                    
                $new_price = $product->price;
                $new_point = $product->point;
            ?>
            <table style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12px; line-height:20.8px; padding:20px; width:600px">
                <tbody>
                    <tr>
                        <td rowspan="5" style="text-align:center; width:160px"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(17, 85, 204);" target="_blank"><img alt="購物車商品圖片" class="CToWUd" src="{{$thumbnail}}" style="border:1px solid rgb(204, 204, 204); height:140px; opacity:0.9; width:140px" /></a></td>
                        <td colspan="3"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank">{{$product->manufacturer->name . '：' . $product->name}}(正廠零件)</a></td>
                    </tr>
                    <tr>
                        <td colspan="3">商品編號：{{$product->model_number}}</td>
                    </tr>
                    <tr>
                        <td><strong>原本定價:NT$</strong><s>{{number_format( $old_price )}}</s></td>
                        <td><strong>目前售價:<span style="color:rgb(255, 0, 0)"><span style="font-size:20px">NT$</span></span></strong><span style="color:rgb(255, 0, 0)"><span style="font-size:20px"><strong>{{ number_format($new_price) }}</strong></span></span></td>
                        <td>{{ $old_price ?  round( ($old_price - $new_price) / $old_price * 100  ) : 0 }}%OFF/{{number_format( $new_point )}}點點數回饋</td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="3"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-3.png" /></td>
                    </tr>
                </tbody>
            </table>

            <p>&nbsp;</p>
    @endif
    @endif
@endforeach

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><a href="{{URL::route('customer-wishlist').'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=wishlist&utm_campaign='.$campaign}}"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-4.png" style="line-height:20.8px; opacity:0.9; text-align:center" /></a></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-5.png" style="height:48px; line-height:20.8px; opacity:0.9; text-align:center; width:600px" /></p>

<p style="text-align:center"><a href="http://www.webike.tw/benefit/event/webikequiz?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="滑滑九宮格" src="https://img-webike-tw-370429.c.cdn77.org/assets/images/user_upload/billboard/2016-09-30Randomn0Deh.png" style="height:120px; width:300px" /></a><a href="http://www.webike.tw/genuineparts?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="正廠零件95折優惠" src="https://img-webike-tw-370429.c.cdn77.org/assets/images/user_upload/billboard/2016-09-30RandomRyhyG.png" style="height:120px; width:300px" /></a><a href="http://www.webike.tw/benefit/outlet?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank">        <img alt="WEBIKE OUTLET" src="https://img-webike-tw-370429.c.cdn77.org/assets/images/user_upload/billboard/2016-09-30Randommp7DD.png" style="height:120px; width:300px" /></a><a href="http://www.webike.tw/collection/brand/Pointdiscount?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="愛用國貨POINT點數現折" src="https://img-webike-tw-370429.c.cdn77.org/assets/images/user_upload/billboard/2016-10-03RandomXGyUP.png" style="height:120px; width:300px" /></a><a href="http://www.webike.tw/review?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="評論大募集" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-09-30RandomGUXRQ.png" style="height:120px; width:300px" /></a><a href="http://www.webike.tw/cart?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}#additional" target="_blank"><img alt="本月加價購" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-09-30RandomlXQH8.png" style="height:120px; width:300px" /></a></p>

<p>&nbsp;</p>
</div>

<div style="max-width:580px;margin:0 auto;padding:0 10px;">
<div style="margin: 0 0 15px;">
<p style="text-align:center"><a href="http://www.webike.tw/genuineparts?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-11.png" style="height:34px; opacity:0.9; width:140px" /></a>&nbsp;<a href="http://www.webike.tw/motor?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-12.png" style="height:34px; line-height:20.8px; opacity:0.9; width:140px" /></a>&nbsp;<a href="http://www.webike.tw/ca/1000?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-13.png" style="height:34px; line-height:20.8px; opacity:0.9; width:140px" /></a>&nbsp;<a href="http://www.webike.tw/ca/3000" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-14.png" style="height:34px; width:140px" /></a></p>

<p style="text-align:center"><a href="http://www.webike.tw/collection?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-7.png" style="height:34px; line-height:20.8px; width:140px" /></a>&nbsp;<a href="http://www.webike.tw/review?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-8.png" style="height:34px; line-height:20.8px; opacity:0.9; width:140px" /></a>&nbsp;<a href="http://www.webike.tw/bikenews?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-9.png" style="height:34px; width:140px" /></a>&nbsp;<a href="http://www.webike.tw/benefit?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-10.png" style="height:34px; line-height:20.8px; opacity:0.9; width:140px" /></a></p>
</div>
</div>
</div>
<!-- // body --><!-- footer -->

<div style="background-color:#EEE;max-width:630px;margin:0 auto;">
<div style="padding:10px;">
<table border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td rowspan="3"><a href="http://www.webike.tw?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="「Webike台灣」摩托百貨" src="https://www.webike.tw/assets/images/shared/webike_shop_logo.png" style="height:101px; width:215px" /></a></td>
            <td style="text-align:center"><a href="https://www.facebook.com/WebikeTaiwan/" target="_blank"><img alt="Find us on FACEBOOK" src="https://img.webike.net/sys_images/mail1/global_diapaus_12.gif" style="max-width:130px; width:100%" /></a><a href="http://www.webike.tw/bikenews?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/bikenews/wp-content/uploads/tcd-w/logo.png?1468313524" style="height:51px; width:109px" /></a><a href="http://www.webike.tw/motomarket?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="「Webike台灣」" src="https://www.webike.tw/motomarket/assets/img/header/logobikeshop.png" style="height:51px; width:109px" /></a></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><a href="http://www.webike.tw/customer/service/proposal?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://img.webike.net/gcm/tw/twimage/Welcomemail/global_diapaus_15.gif" style="height:35px; max-width:397px; width:397px" /></a></td>
        </tr>
        <tr>
            <td colspan="3"><span style="color:#000000">若您不想收到任何相關資訊，請點選取消<a href="http://www.webike.tw/customer/account/preview?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}">訂閱電子報</a></span></td>
        </tr>
    </tbody>
</table>
</div>
</div>

<div style="text-align:center;font-size:11px;padding:10px 0;background-color:#222;margin:0 auto;max-width:630px;">Published and edited by Webike 摩托百貨&copy; {{ date('Y') }} EverGlory Motors.All Rights Reserved.</div>
<!-- // footer --><img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/2324611/tracking30&amp;sz=1x1&amp;c=crm_w1" /></div>
