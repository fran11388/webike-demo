<?php
$carts_item = $customer->carts()->orderBy('product_id')->get();
$gen_check = true;
$product_check = true;
$first_product = true;
$campaign = date("Ymd").'_promomail';
?>
<table border="0" cellpadding="1" cellspacing="1" style="width:750px">
    <tbody>
        <tr>
            <td colspan="2"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-1.png" /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center"><a href="http://www.webike.tw/benefit/event/creation3rd?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" style="line-height: 18.5714px; text-align: center;" target="_blank"><img alt="Webike台灣3rd開站紀念活動" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-06-08Random15TfE.png" /></a></td>
        </tr>
        <tr>
            <td colspan="2">
            <p><span style="font-size:16px"><span style="font-family:helvetica,microsoft jhenghei,新細明體,verdana,lihei pro,sans-serif">「Webike台灣」開站3rd紀念，感謝各位「Webike台灣」會員這三年來的支持與愛護，未來我們也會不斷精進並且提供更多方便的服務，還請各位會員們多多指教。 活動內容： 在開站紀念活動期間購買，皆可參加抽獎活動，最大獎價值$5860的【HONDA RIDING GEAR】Monkey 安全帽，還有更多好康獎品等你來拿。</span></span></p>

            <p><span style="font-size:16px"><span style="font-family:helvetica,microsoft jhenghei,新細明體,verdana,lihei pro,sans-serif">活動期間： 2016/06/01(三)~06/30(四)</span></span></p>
            </td>
        </tr>
                    @foreach($carts_item as $item)
                        <?php
                            $product = $item->product;
                            $product_check = false;
                        ?>
                        @if($product)
                            @if($product->type == 0 and $product->p_country == 'J')
                                <?php
                                    $images = $product->images;
                                        $thumbnail = 'http://img.webike.net/catalogue/noimage.gif';
                                        if (count($images)){
                                            $thumbnail =  $images[0]->thumbnail;
                                        }
                                    $old_price = $product->cost  / 0.98 ;
                                    $new_price = $product->getFinalpriceAttribute($customer);
                                    $new_point = $product->getFinalpointAttribute($customer);
                                    if($old_price > 0 ){
                                        $discount = round( ($old_price - $new_price) / $old_price * 100  );
                                    }else{
                                        $discount = 0;
                                    }
                                ?>
                                @if($discount > 5)
                                    @if($first_product)
                                        <?php $first_product = false; ?>
                                            <tr>
                                                <td colspan="2" style="text-align:center"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-2.png" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                <table style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12px; padding:20px; width:770px">
                                                    <tbody>
                                    @endif
                                    <tr>
                                        <td rowspan="5" style="text-align:center; width:160px">
                                            <a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(17, 85, 204);" target="_blank">
                                            <img alt="購物車商品圖片" class="CToWUd" src="{{$thumbnail}}" style="border:1px solid rgb(204, 204, 204); height:140px; width:140px" />
                                            </a>
                                        </td>
                                        <td colspan="3"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank">{{$product->manufacturer->name . '：' . $product->name}}</a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">商品編號：{{$product->model_number}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>原本定價:NT$</strong><s>{{number_format( $old_price )}}</s></td>
                                        <td><strong>目前售價:<span style="color:#FF0000"><span style="font-size:20px">NT$</span></span></strong><span style="color:#FF0000"><span style="font-size:20px"><strong>{{ number_format($new_price) }}</strong></span></span></td>
                                        <td>{{ $discount }}%OFF/{{number_format( $new_point )}}點點數回饋</td>
                                    </tr>
                                    <tr>
                                        <?php
                                            $models = $product->fitModels()->select(\DB::Raw("CONCAT(maker,' : ',model) as model_name"))->lists('model_name');
                                            $model_name = implode('／', $models);
                                        ?>
                                        <td colspan="3">適合車型/對應型式:{{str_limit($model_name,45)}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-3.png" /></a></td>
                                    </tr>
                                @endif
                            @elseif($product->type == 1 and $gen_check)
                                <?php
                                    $gen_check = false;
                                    $thumbnail = 'http://img.webike.net/catalogue/noimage.gif';
                                    $estimate_item = \GenuinePart\Item::where('product_id',$product->id)->first();
                                    $genuinepart = $estimate_item->genuinepart;
                                    if($genuinepart->type == 2){
                                        $old_price = \Weight::setCustomer($customer)->setRate('TWD')->getGenuinePart($estimate_item->syouhin_teika,$product->manufacturer_id,true,2)->price_final;
                                    }else{
                                        $old_price = \Weight::setCustomer($customer)->setRate('JPY')->getGenuinePart($estimate_item->syouhin_teika,$product->manufacturer_id,true,1)->price_final;    
                                    }

                                    // $estimateItem = \Zero\Entities\Estimate\Item::where('customer_id', $customer->id)->where('product_id', $product->id)->orderby('created_at', 'DESC')->first();
                                    // if($estimateItem){
                                    //     $estimateItemDetail = $item->cacheEstimateItem->itemDetail;
                                    //     $old_price = $estimateItemDetail->normal_price;
                                    //     $old_price = $estimateItem->price;=
                                    // }
                                    
                                    $contents = array(
                                        $row_key+1,
                                        '【'.$itemDetail->manufacturer_name.'】'.$itemDetail->product_name,
                                        $item->quantity,
                                        ($generalPrice ? $generalPrice : $item->price),
                                        ($generalPrice ? $generalPrice : $item->price ) * $item->quantity
                                    );
                                    $new_price = $product->price;
                                    $new_point = $product->point;
                                ?>
                                @if($first_product)
                                    <?php $first_product = false; ?>
                                    <tr>
                                        <td colspan="2" style="text-align:center"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-2.png" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <table style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12px; padding:20px; width:770px">
                                            <tbody>

                                @endif
                                <tr>
                                    <td rowspan="5" style="text-align:center; width:160px">
                                        <a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(17, 85, 204);" target="_blank">
                                        <img alt="購物車商品圖片" class="CToWUd" src="{{$thumbnail}}" style="border:1px solid rgb(204, 204, 204); height:140px; width:140px" />
                                        </a>
                                    </td>
                                    <td colspan="3"><a href="{{URL::route('product',$product->sku).'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank">{{$product->manufacturer->name . '：' . $product->name}}(正廠零件)</a></td>
                                </tr>
                                <tr>
                                    <td colspan="3">商品編號：{{$product->model_number}}</td>
                                </tr>
                                <tr>
                                    <td><strong>原本定價:NT$</strong><s>{{number_format( $old_price )}}</s></td>
                                    <td><strong>目前售價:<span style="color:#FF0000"><span style="font-size:20px">NT$</span></span></strong><span style="color:#FF0000"><span style="font-size:20px"><strong>{{ number_format($new_price) }}</strong></span></span></td>
                                    <td>{{ $old_price ?  round( ($old_price - $new_price) / $old_price * 100  ) : 0 }}%OFF/{{number_format( $new_point )}}點點數回饋</td>
                                </tr>
                                <tr>

                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><a href="{{URL::route('cart').'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=product&utm_campaign='.$campaign}}" style="color: rgb(65, 96, 144); text-decoration: none;" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-31.png" /></a></td>
                                </tr>
                            @endif
                        @endif
                    @endforeach
        @if(!$first_product)
                    </tbody>
                </table>
                </td>
            </tr>
        @endif

        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </tbody>
</table>

<table border="0" cellpadding="1" cellspacing="1" style="width:820px">
    <tbody>
        <tr>
            <td style="text-align:center"><a href="{{URL::route('customer-wishlist').'?utm_source=webikeMailmagazine&utm_medium=email&utm_content=wishlist&utm_campaign='.$campaign}}"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-4.png" /></a></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="1" cellspacing="1" style="width:820px">

    <tbody>
        <tr>
            <td colspan="3" style="text-align:center"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-5.png" style="line-height:20.7999992370605px; text-align:center" /></td>
        </tr>

        <tr>
            <td style="text-align:right"><a href="http://www.webike.tw/benefit/sale/2016-06?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="2016 6月精選商品SALE" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-05-31Random0lYSP.png" style="height:152px; width:380px" /></a></td>
            <td colspan="2"><a href="http://www.webike.tw/br/1963?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="IMPACT全品85折" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-05-31RandomDPNT2.png" style="height:152px; width:380px" /></a></td>
        </tr>
        <tr>
            <td style="text-align:right"><a href="http://www.webike.tw/parts/ca/3000-3020-3040?sort=new&utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img alt="「雨衣」商品本月現金點數5倍" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-05-31Randomfy7OK.png" style="height:152px; width:380px" /></a></td>
            <td colspan="2"><a href="http://www.webike.tw/cart?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}#additional" target="_blank"><img alt="6月精選加價購" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-06-01RandomEccfS.png" style="height:152px; width:380px" /></a></td>
        </tr>
        <tr>
            <td style="text-align:right"><a href="http://www.webike.tw/benefit/event/motogp/2016?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}#finish" target="_blank"><img alt="【2016TT阿森】台灣正賽時間2016-06-26 20:00:00 - 「Webike-摩托百貨」" src="https://www.webike.tw/assets/images/benefit/event/motoGp/2016/speedway/2016motogp_stage8.jpg" style="height:253px; width:380px" /></a></td>
            <td colspan="2">
            <p><a href="http://www.webike.tw/benefit/event/motogp/2016?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" style="line-height: 20.8px; text-align: right;" target="_blank"><img alt="2016MotoGP2016MotoGP冠軍大預測" src="https://img.webike.tw/assets/images/user_upload/billboard/2016-03-10RandomJt45b.png" style="height:152px; width:380px" /></a></p>
            <p style="text-align:center"><a href="http://www.webike.tw/benefit/event/motogp/2016?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}#finish" style="line-height: 20.8px; text-align: center;" target="_blank"><img alt="" src="https://img.webike.tw/assets/images/user_upload/newsletter/160315-motogpjoin.png" style="height:66px; width:293px" /></a></p>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center">
            <p style="text-align:center"><a href="http://www.webike.tw/genuineparts?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-11.png" style="height:43px; width:175px" /></a>&nbsp; &nbsp;&nbsp;<a href="http://www.webike.tw/motor?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-12.png" style="height:43px; line-height:20.7999992370605px; opacity:0.9; width:175px" /></a>&nbsp; &nbsp;&nbsp;<a href="http://www.webike.tw/ca/1000?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-13.png" style="height:43px; line-height:20.7999992370605px; opacity:0.9; width:175px" /></a>&nbsp; &nbsp;&nbsp;<a href="http://www.webike.tw/ca/3000?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-14.png" style="height:43px; width:175px" /></a></p>

            <p style="text-align:center"><a href="http://www.webike.tw/collection?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-7.png" style="height:43px; line-height:20.7999992370605px; width:175px" /></a>&nbsp; &nbsp;&nbsp;<a href="http://www.webike.tw/review?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-8.png" style="height:43px; line-height:20.7999992370605px; opacity:0.9; width:175px" /></a>&nbsp; &nbsp;&nbsp;<a href="http://www.webike.tw/information/news/" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-9.png" style="height:43px; width:175px" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.webike.tw/benefit?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" target="_blank"><img src="https://www.webike.tw/assets/images/shared/pricedownmail-10.png" style="height:43px; line-height:20.7999992370605px; opacity:0.9; width:175px" /></a></p>
            </td>
        </tr>
        <tr>
            <td style="text-align:center"><span style="color:rgb(0, 0, 0); font-family:arial,sans-serif; font-size:14px">如您有任何問題歡迎隨時與我們聯絡</span><br />
            <a href="mailto:Service@webike.tw" style="color: rgb(17, 85, 204); text-align: center; font-family: arial, sans-serif; font-size: 14px; line-height: 18.5714282989502px;" target="_blank"><span style="color:rgb(0, 0, 0)">Service@webike.tw</span></a></td>
            <td style="text-align:center"><img alt="EverGlory" src="https://www.webike.tw/assets/images/shared/eg.png" style="line-height:20.7999992370605px; text-align:center" /></td>
            <td style="text-align:center"><a href="http://www.webike.tw/customer/newsletter" target="_blank"><img src="https://webike.tw/assets/images/mailmag/mailcancel.png" style="line-height:20.7999992370605px; opacity:0.9; text-align:center" /></a></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:center"><strong>滿足您渴望的摩托人生，請務必體驗<a href="http://www.webike.tw?utm_source=webikeMailmagazine&utm_medium=email&utm_content=information&utm_campaign={{$campaign}}" style="color: rgb(17, 85, 204); line-height: 18.5714282989502px;" target="_blank">「Webike台灣」</a>!!</strong><img alt="" src="{{$open_image}}" style="height:1px; width:1px" /></td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>