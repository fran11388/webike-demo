<?php

return [
    'hostname' =>  env('SOLR_HOSTNAME', '153.120.83.216'),
    'port' =>  env('SOLR_PORT', '9000'),
    'path' =>  env('SOLR_PATH', 'solr/webike'),
    'timeout' =>  env('SOLR_TIMEOUT', '60'),

];
