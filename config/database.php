<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_OBJ,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'eg_zero'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'eg_zero' => [
            'driver'    => 'mysql',
            'read' => [
                'host' => env('ZERO_DB_HOST_READ',''),
            ],
            'write' => [
                'host' => env('ZERO_DB_HOST_WRITE',''),
            ],
            'database'  => env('ZERO_DB_DATABASE',''),
            'username'  => env('ZERO_DB_USERNAME',''),
            'password'  => env('ZERO_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'wordpress' => [
            'driver'    => 'mysql',
            'host'      => env('WORDPRESS_DB_HOST',''),
            'database'  => env('WORDPRESS_DB_DATABASE',''),
            'username'  => env('WORDPRESS_DB_USERNAME',''),
            'password'  => env('WORDPRESS_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        
        'white' => [
            'driver'    => 'mysql',
            'host'      => env('WHITE_DB_HOST',''),
            'database'  => env('WHITE_DB_DATABASE',''),
            'username'  => env('WHITE_DB_USERNAME',''),
            'password'  => env('WHITE_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'smax_warehouse' => [
            'driver'    => 'mysql',
            'host'      => env('SMAXWAREHOUSE_DB_HOST',''),
            'database'  => env('SMAXWAREHOUSE_DB_DATABASE',''),
            'username'  => env('SMAXWAREHOUSE_DB_USERNAME',''),
            'password'  => env('SMAXWAREHOUSE_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
            'port'      => env('SMAXWAREHOSE_DB_PORT','5140'),
        ],

        'smax_productstation' => [
            'driver'    => 'mysql',
            'host'      => env('SMAXPRODUCTSTATION_DB_HOST',''),
            'database'  => env('SMAXPRODUCTSTATION_DB_DATABASE',''),
            'username'  => env('SMAXPRODUCTSTATION_DB_USERNAME',''),
            'password'  => env('SMAXPRODUCTSTATION_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
            'port'      => env('SMAXPRODUCTSTATION_DB_PORT','5140'),
        ],

        'smax_pricemanager' => [
            'driver'    => 'mysql',
            'host'      => '153.120.25.92',
            'database'  => 'eg_pricemanager',
            'username'  => 'webike_new',
            'password'  => 'J@y/TYkt',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
            'port'      => env('SMAXPRODUCTSTATION_DB_PORT','5140'),
        ],

        'eg_product' => [
            'driver'    => 'mysql',
            'host'      => env('PRODUCT_DB_HOST',''),
            'database'  => env('PRODUCT_DB_DATABASE',''),
            'username'  => env('PRODUCT_DB_USERNAME',''),
            'password'  => env('PRODUCT_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'eg_news' => [
            'driver'    => 'mysql',
            'host'      => env('NEWS_DB_HOST',''),
            'database'  => env('NEWS_DB_DATABASE',''),
            'username'  => env('NEWS_DB_USERNAME',''),
            'password'  => env('NEWS_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'motomarket' => [
            'driver'    => 'mysql',
            'host'      => env('MOTOMARKET_DB_HOST',''),
            'database'  => env('MOTOMARKET_DB_DATABASE',''),
            'username'  => env('MOTOMARKET_DB_USERNAME',''),
            'password'  => env('MOTOMARKET_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'dobar' => [
            'driver'    => 'mysql',
            'host'      => env('DOBAR_DB_HOST',''),
            'database'  => env('DOBAR_DB_DATABASE',''),
            'username'  => env('DOBAR_DB_USERNAME',''),
            'password'  => env('DOBAR_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'ec_dbs' => [
            'driver'    => 'mysql',
            'read' => [
                'host' => env('DBS_DB_HOST_READ',''),
            ],
            'write' => [
                'host' => env('DBS_DB_HOST_WRITE',''),
            ],
            'database'  => env('DBS_DB_DATABASE',''),
            'username'  => env('DBS_DB_USERNAME',''),
            'password'  => env('DBS_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'tracker' => [
            'driver'    => 'mysql',
            'host' => env('TRACKER_DB_HOST',''),
            'database'  => env('TRACKER_DB_DATABASE',''),
            'username'  => env('TRACKER_DB_USERNAME',''),
            'password'  => env('TRACKER_DB_PASSWORD',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'ats' => [
            'driver'    => 'mysql',
            'host' => env('ATS_DB_HOST'),
            'database'  => env('ATS_DB_DATABASE'),
            'username'  => env('ATS_DB_USERNAME'),
            'password'  => env('ATS_DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'eg_accounting' => [
            'driver'    => 'mysql',
            'host'      => env('ACCOUNTING_HOST', ''),
            'database'  => 'eg_accounting',
            'username'  => env('ACCOUNTING_USERNAME', ''),
            'password'  => env('ACCOUNTING_PASSWORD', ''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
//             'unix_socket'   => env('DB_UNIX_SOCKET', ''),
            'strict'    => false,
        ],

        
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', ''),
            'password' => env('REDIS_PASSWORD', ''),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
