<?php
define('STATIC_FILE_CACHE_KEY', 'static-file-timestamp');
define('ATS_MAX_PATH', '/data/ats');

define('APP_UPDATING', 'UPDATING'); // for weekly update
define('APP_NORMAL', 'NORMAL'); // for weekly update

if(hsaNewOffice()){
	define('OFFICE_GLOBAL_PHONE', '+886-2298-2020');
	define('OFFICE_PHONE', '02-22982020');
	define('OFFICE_FAX', '02-22982868');
	define('OFFICE_ADDRESS', '台灣新北市五股區五工三路101號2樓');
	define('OFFICE_EMAIL_PHONE', '(02)22982020 分機11');
}else{
	define('OFFICE_GLOBAL_PHONE', '+886-2904-1080');
	define('OFFICE_PHONE', '02-29041080');
	define('OFFICE_FAX', '02-29041083');
	define('OFFICE_ADDRESS', '台灣新北市泰山區南泰路1-1號108室');
	define('OFFICE_EMAIL_PHONE', '(02)29041080 分機11');
}