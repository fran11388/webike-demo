<?php

//define('DOMAIN_ROOT' , 'webike.argus.tw');

define('DOMAIN_ROOT' , 'www.webike.tw');
define('MOTOMARKET' , 'http://www.webike.tw/motomarket');
define('BIKENEWS' , 'https://www.webike.tw/bikenews');
define('MOTOCHANNEL' , 'http://www.webike.tw/collection/video');
define('DEALER_URL' , 'http://www.webike.tw/information/dealer/');

define('CDN_TAIWAN' , 'https://img-webike-tw-370429.c.cdn77.org');
define('CDN_JAPAN' , 'http://img-webike-370429.c.cdn77.org');
define('STOCK_API_JAPAN', 'http://api.webike.net/api/stock/stockCooperationSr.json?s=');
define('NO_IMAGE' , 'https://img.webike.net/garage_img/no_photo.jpg');
define('RCJ_IMAGE_URL' , 'https://img.webike.net');
define('IMAGE_URL' , 'https://img.webike.tw');
define('THUMBOR' , 'http://img.webike.tw:8888');

define('EG_ZERO' , 'http://153.120.83.199');
define('EG_ZERO_STOCK_INFO' , 'http://153.120.83.199/api/wms/sim/stock-information');
define('EG_ZERO_STOCK_FLG_FOR_LIST' , 'http://153.120.83.199/api/wms/sim/stock-flg-for-list');
define('EAGLE_EG_ZERO' , 'http://zero-dev.everglory.asia/');
define('EAGLE_EG_ZERO_STOCK_INFO' , 'http://zero-dev.everglory.asia/api/wms/sim/stock-information');
define('EAGLE_EG_ZERO_STOCK_FLG_FOR_LIST' , 'http://zero-dev.everglory.asia/api/wms/sim/stock-flg-for-list');





