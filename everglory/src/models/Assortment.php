<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment\Item;
use Everglory\Models\Assortment\Type;
use Everglory\Models\Assortment\Tag;
use Everglory\Models\Assortment\Layout;

Class Assortment extends BaseDbs{

    protected $table = 'assortments';

    public function items(){
        return $this->hasMany(Item::class)->orderBy('sort');
    }

    public function type(){
        return $this->belongsTo(Type::class);
    }

    public function layout(){
        return $this->belongsTo(Layout::class);
    }

    public function tags(){
        return $this->hasMany(Tag::class);
    }
}
