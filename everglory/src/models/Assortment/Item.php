<?php

namespace Everglory\Models\Assortment;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment;
use Everglory\Models\Assortment\Plugin;
use Everglory\Models\Assortment\Template;

Class Item extends BaseDbs{

    protected $table = 'assortment_items';

    public function assortment(){
        return $this->belongsTo(Assortment::class);
    }

    public function plugin(){
        return $this->belongsTo(Plugin::class);
    }

    public function template(){
        return $this->belongsTo(Template::class);
    }


}
