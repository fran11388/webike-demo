<?php

namespace Everglory\Models\Assortment;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment\Item;
use Everglory\Models\Assortment\Template;

Class Plugin extends BaseDbs{

    protected $table = 'assortment_plugins';

    public function item(){
        return $this->hasOne(Item::class);
    }

    public function templates(){
        return $this->belongsToMany(Template::class,'assortment_plugin_templates')->withPivot('function');
    }
}
