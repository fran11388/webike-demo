<?php

namespace Everglory\Models\Assortment;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment;

Class Type extends BaseDbs{

    protected $table = 'assortment_types';

    public function assortments(){
        return $this->hasMany(Assortment::class);
    }

    public function plugins(){
        return $this->belongsToMany(Plugin::class);
    }
}
