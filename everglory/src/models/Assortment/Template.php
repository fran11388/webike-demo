<?php

namespace Everglory\Models\Assortment;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment;
use Everglory\Models\Assortment\Plugin;

Class Template extends BaseDbs{

    protected $table = 'assortment_templates';

    public function item(){
        return $this->hasOne(Item::class);
    }

    public function plugins(){
        return $this->belongsToMany(Plugin::class);
    }
}
