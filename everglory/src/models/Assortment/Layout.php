<?php

namespace Everglory\Models\Assortment;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment;

Class Layout extends BaseDbs{

    protected $table = 'assortment_layouts';

    public function assortments(){
        return $this->hasMany(Assortment::class);
    }
}
