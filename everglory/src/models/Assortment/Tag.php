<?php

namespace Everglory\Models\Assortment;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Assortment;

Class Tag extends BaseDbs{

    protected $table = 'assortment_tags';

    public function assortment(){
        return $this->belongsTo(Assortment::class);
    }
}
