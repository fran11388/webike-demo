<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;

Class Wishlist extends BaseDbs{

	protected $table = 'wishlists';

	public function product(){
		return $this->belongsTo(Product::class);
	}
}
