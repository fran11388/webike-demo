<?php
namespace Everglory\Models\Season;
use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Product;

/**
 * Created by PhpStorm.
 * User: EG_EC
 * Date: 2018/10/23
 * Time: 下午 02:29
 */
class Item extends BaseDbs
{
    protected $table = 'season_items';
    protected $guarded  = array('id');

    public function product(){
        return $this->belongsTo( Product::class);
    }

}