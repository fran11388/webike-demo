<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseWarehouse;

class WarehouseStock extends BaseWarehouse
{
    protected $table = 'ref_product_stock';
    public $primaryKey = 'product_id';
  }
