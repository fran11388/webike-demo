<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Promotion\Item;

/**
 * Everglory\Models\Promotion
 *
 * @property int $id
 * @property string $url_rewrite
 * @property string $banner
 * @property string $main_color
 * @property string $font_color
 * @property string $name
 * @property string $start_at
 * @property string $end_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Promotion\Item[] $items
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereBanner($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereEndAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereFontColor($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereMainColor($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereStartAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion whereUrlRewrite($value)
 * @mixin \Eloquent
 */
class Promotion extends BaseDbs
{
    protected $table = 'promotions';
    public function items()
    {
        return $this->hasMany(Item::class);
    }
}