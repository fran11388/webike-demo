<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;


/**
 * Everglory\Models\Unusual
 *
 * @property integer $id
 * @property integer $unusual_id
 * @property string $unusual_type
 * @property string $info
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Unusual whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Unusual whereUnusualId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Unusual whereUnusualType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Unusual whereInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Unusual whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Unusual whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Unusual extends BaseDbs
{

    protected $table = 'unusual';

}
