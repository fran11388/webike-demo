<?php
namespace Everglory\Models\Email\Newsletter;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Newsletter;

class Queue extends BaseDbs{
    
    protected $table = 'email_newsletter_queue';
    
    public function items(){
        return $this->hasMany( Item::class ,'queue_id');
    }

    public function newsletter()
    {
        return $this->belongsTo( Newsletter::class ,'id','newsletter_id');
    }
}
