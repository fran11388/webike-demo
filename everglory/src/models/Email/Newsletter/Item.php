<?php
namespace Everglory\Models\Email\Newsletter;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Customer;

class Item extends BaseDbs{

    protected $table = 'email_newsletter_queue_items';

    public function customer(){
        return $this->hasOne( Customer::class ,'id','customer_id');
    }
}
