<?php
namespace Everglory\Models\Email\Log;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Order;	

class Service extends BaseDbs{

	/*
	 * This table is after order service mail 
	 * 
	 * */
	protected $table = 'email_log_service';

	public function order()
	{
		return $this->belongsTo(Order::class);
	}
}
