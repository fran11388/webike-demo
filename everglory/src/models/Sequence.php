<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Sequence
 *
 * @property integer $id
 * @property string $name
 * @property integer $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $char
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sequence whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sequence whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sequence whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sequence whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sequence whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sequence whereChar($value)
 * @mixin \Eloquent
 */
class Sequence extends BaseZero{

	protected $table = 'core_sequence';

}