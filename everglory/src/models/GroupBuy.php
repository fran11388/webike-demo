<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\GroupBuy\Item as GroupBuyItem;

Class GroupBuy extends BaseDbs
{
    const CREATED_AT = 'request_date';
    const UPDATED_AT = 'response_date';
    protected $table = 'groupbuy';
    protected $guarded = ['id'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }

    public function items()
    {
        return $this->hasMany(GroupBuyItem::class, 'groupbuy_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    
    
}

