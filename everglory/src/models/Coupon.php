<?php
namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Coupon
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $history_id
 * @property string $uuid
 * @property string $name
 * @property string $code
 * @property float $discount
 * @property string $expired_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $discount_type
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereHistoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereDiscount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereExpiredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon whereDiscountType($value)
 * @mixin \Eloquent
 */
class Coupon extends BaseZero
{
    protected $table = 'coupons';

}
