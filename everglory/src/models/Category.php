<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Category
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property integer $manufacturer_id
 * @property string $url_rewrite
 * @property string $meta_description
 * @property string $meta_title
 * @property string $meta_keyword
 * @property boolean $active
 * @property string $sku
 * @property string $model_number
 * @property float $cost
 * @property float $price
 * @property integer $point
 * @property boolean $stock_management 0:USE API
 * 1:USE TABLE
 * @property integer $stock_quantity
 * @property integer $sale_quantity
 * @property boolean $has_branch
 * @property integer $parent_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $visits
 * @property integer $group_code
 * @property string $ean_code
 * @property boolean $type 0:一般商品
 * 1:正廠零件
 * 2:未登錄商品
 * 3:團購商品
 * 4:加價購商品
 * @property boolean $is_main
 * @property string $p_country
 * @property boolean $is_new
 * @property boolean $point_type
 * @property string $synonym
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Manufacturer[] $manufacturers
 * @property-read \Everglory\Models\Mptt $mptt
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereMetaKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereModelNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category wherePoint($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereStockManagement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereStockQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereSaleQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereHasBranch($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereVisits($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereEanCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereIsMain($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category wherePCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereIsNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category wherePointType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereSynonym($value)
 * @mixin \Eloquent
 * @property boolean $depth
 * @property string $sort
 * @property integer $n_left
 * @property integer $n_right
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereNLeft($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Category whereNRight($value)
 */
class Category extends BaseProduct
{

    protected $table = 'categories';

    public function products()
    {
        return $this->belongsToMany(Product::class,'category_product');
    }

    public function manufacturers(){
        return $this->hasManyThrough( Manufacturer::class , Product::class, 'id', 'id' );
    }

    public function mptt(){
        return  $this->belongsTo(Mptt::class,'id');
    }

}
