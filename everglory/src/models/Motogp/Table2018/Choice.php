<?php 

namespace Everglory\Models\Motogp\Table2018;

use Everglory\BaseModel\BaseDbs;

class Choice extends BaseDbs{
	protected $table = 'campaign_motogp2018_choice';
	protected $guarded  = array('id');

	public function speedway()
	{
		return $this->belongsTo(Speedway::class);
	}

	public function racer()
	{
		return $this->belongsTo(Racer::class);
	}

	public function customer()
	{
		return $this->belongsTo('\Customer');
	}
}
