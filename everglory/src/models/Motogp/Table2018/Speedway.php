<?php 

namespace Everglory\Models\Motogp\Table2018;

use Everglory\BaseModel\BaseDbs;

class Speedway extends BaseDbs{
	protected $table = 'campaign_motogp2018_speedway';
	protected $guarded  = array('id');

	public function choices()
	{
		return $this->hasMany('\Motogp\Choice');
	}

	public function numberOne()
	{
		return $this->belongsTo(Racer::class,'number_one');
	}

	public function numberTwo()
	{
		return $this->belongsTo(Racer::class, 'number_two');
	}

	public function numberThree()
	{
		return $this->belongsTo(Racer::class, 'number_three');
	}

	public function getWinerGroupAttribute()
	{
		$this->with(['numberOne', 'numberTwo', 'numberThree']);
		$winers = array();
		$winers[1] = $this->numberOne;
		$winers[2] = $this->numberTwo;
		$winers[3] = $this->numberThree;

		return $winers;
	}
	// public function items()
	// {
	// 	return $this->hasMany('\Campaign\Additional\Item','additional_id');
	// }
}
