<?php 

namespace Everglory\Models\Motogp\Table2018;

use Everglory\BaseModel\BaseDbs;
use \Everglory\Models\Motogp\Table2018\Racer\Detail;

class Racer extends BaseDbs{
	protected $table = 'campaign_motogp2018_racer';
	protected $guarded  = array('id');

	
	 public function details()
	 {
	 	return $this->hasMany(Detail::class);
	 }
}
