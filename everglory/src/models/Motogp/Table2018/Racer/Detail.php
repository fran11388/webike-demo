<?php 

namespace Everglory\Models\Motogp\Table2018\Racer;

use Everglory\BaseModel\BaseDbs;

class Detail extends BaseDbs{
	protected $table = 'campaign_motogp2018_racer_details';
	protected $guarded  = array('id');

	public function racer()
	{
		return $this->belongsTo(Racer::class);
	}
}
