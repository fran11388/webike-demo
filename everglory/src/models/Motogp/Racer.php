<?php 

namespace Everglory\Models\Motogp;

use Everglory\BaseModel\BaseDbs;

class Racer extends BaseDbs{
	protected $table = 'campaign_motogp2017_racer';
	protected $guarded  = array('id');

	
	// public function items()
	// {
	// 	return $this->hasMany('\Campaign\Additional\Item','additional_id');
	// }
}
