<?php 

namespace Everglory\Models\Motogp\Table2019\Racer;

use Everglory\BaseModel\BaseDbs;

class Detail extends BaseDbs{
	protected $table = 'campaign_motogp2019_racer_details';
	protected $guarded  = array('id');

	public function racer()
	{
		return $this->belongsTo(Racer::class);
	}
}
