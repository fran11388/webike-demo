<?php 

namespace Everglory\Models\Motogp\Table2019;

use Everglory\BaseModel\BaseDbs;
use \Everglory\Models\Motogp\Table2019\Racer\Detail;

class Racer extends BaseDbs{
	protected $table = 'campaign_motogp2019_racer';
	protected $guarded  = array('id');

	
	 public function details()
	 {
	 	return $this->hasMany(Detail::class);
	 }
}
