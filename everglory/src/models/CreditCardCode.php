<?php

namespace  Everglory\Models;

use Everglory\BaseModel\BaseZero;
use Illuminate\Database\Eloquent\SoftDeletes;

Class CreditCardCode extends BaseZero
{
    protected $table = 'creditcard_codes';

}

