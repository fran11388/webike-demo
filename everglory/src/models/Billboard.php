<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Billboard\Category;
use Everglory\Models\Billboard\Page;
use Everglory\Models\Billboard\Slider;

Class Billboard extends BaseDbs{

	protected $table = 'billboards';

	public function category(){
		return $this->belongsTo( Category::class );
	}

	public function slider(){
		return $this->belongsToMany( Slider::class );
	}

	public function page(){
		return $this->belongsTo( Page::class, 'page_type', 'id' );
	}


}
