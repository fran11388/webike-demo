<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;

class Gift extends BaseZero
{
    protected $table = 'gifts';

    public function origProduct()
    {
        return $this->belongsTo(Product::class, 'orig_product_id','id');
    }

}