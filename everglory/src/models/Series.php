<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Product\Series AS ProductSeries;

/**
 * Everglory\Models\Review
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $url_rewrite
 * @property integer $data_source_code
 * @property integer $brand_id magento_id
 * @property string $name
 * @property string $source_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Series[] $products
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereDataSourceCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereSourceName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Series whereUpdatedAt($value)
 */
class Series extends BaseProduct
{

    protected $table = 'series';

    public function products()
    {
        return $this->hasMany(ProductSeries::class);
    }
}
