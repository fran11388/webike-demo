<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\ManufacturerInfo
 *
 * @property integer $id
 * @property integer $manufacturer_id
 * @property integer $category_id
 * @property string $banner
 * @property string $content
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereBanner($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ManufacturerInfo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ManufacturerInfo extends BaseProduct
{
    protected $table='manufacturer_infos';

}
