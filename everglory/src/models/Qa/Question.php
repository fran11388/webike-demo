<?php

namespace Everglory\Models\Qa;

use Everglory\BaseModel\BaseDbs;


class Question extends BaseDbs
{

    protected $table = 'qa_questions';
    public function answers()
    {
        return $this->hasMany(Answer::class );
    }

}
