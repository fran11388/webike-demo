<?php

namespace Everglory\Models\Qa;

use Everglory\BaseModel\BaseDbs;

class Relation extends BaseDbs
{

    protected $table = 'qa_relations';

    public function answers()
    {
        return $this->hasMany(Answer::class,'relation_id','id');
    }

    public function product()
    {
        return $this->belongsTo(\Everglory\Models\Product::class);
    }

    public function series()
    {
        return $this->hasMany(\Everglory\Models\Product::class, 'relation_product_id', 'series_id');
    }

    public function manufacturer()
    {
        return $this->belongsTo(\Everglory\Models\Manufacturer::class);
    }

    public function category()
    {
        return $this->belongsTo(\Everglory\Models\Category::class);
    }

    public function motor()
    {
        return $this->belongsTo(\Everglory\Models\Motor::class);
    }

}
