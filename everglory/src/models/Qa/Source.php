<?php

namespace Everglory\Models\Qa;

use Everglory\BaseModel\BaseDbs;


class Source extends BaseDbs
{

    protected $table = 'qa_sources';

    public function Question()
    {
        return $this->hasMany(Question::class );
    }
}
