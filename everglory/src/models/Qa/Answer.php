<?php

namespace Everglory\Models\Qa;

use Everglory\BaseModel\BaseDbs;

class Answer extends BaseDbs
{

    protected $table = 'qa_answers';

    public function question()
    {
        return $this->belongsTo(Question::class );
    }

    public function relation()
    {
        return $this->belongsTo(Relation::class );
    }
}
