<?php

namespace Everglory\Models\Qa;

use Everglory\BaseModel\BaseDbs;


class Group extends BaseDbs
{

    protected $table = 'qa_groups';

    public function Question()
    {
        return $this->hasMany(Question::class );
    }
}
