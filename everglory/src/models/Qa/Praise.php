<?php

namespace Everglory\Models\Qa;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Customer;


class Praise extends BaseDbs
{
    public $timestamps = false;
    protected $table = 'qa_praises';

    public function Answer()
    {
        return $this->belongsTo(Answer::class );
    }

    public function Customer()
    {
        return $this->belongsTo(Customer::class );
    }
}
