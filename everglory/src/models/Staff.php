<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;


Class Staff extends BaseZero{
    protected $connection = 'eg_zero';
    protected $morphClass = 'MorphStaff';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'staffs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

}
