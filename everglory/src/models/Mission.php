<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Mission
 *
 * @property int $id
 * @property string $name
 * @property string $mode
 * @property string $category
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereMode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Mission extends BaseZero
{
    protected $table = 'mission';

}
