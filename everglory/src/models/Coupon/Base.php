<?php
namespace Everglory\Models\Coupon;

use Everglory\BaseModel\BaseZero;


/**
 * Everglory\Models\Coupon\Base
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property float $discount
 * @property string $period
 * @property int $time_limit
 * @property string $time_limit_unit
 * @property int $mail_template_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $discount_type
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereDiscount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereDiscountType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereMailTemplateCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base wherePeriod($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereTimeLimit($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereTimeLimitUnit($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\Base whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Base extends BaseZero
{
    protected $table = 'coupon_base';

}
