<?php
namespace Everglory\Models\Coupon;

use Everglory\BaseModel\BaseZero;


/**
 * Everglory\Models\Coupon\History
 *
 * @property int $id
 * @property string $name
 * @property string $customer_ids
 * @property float $discount
 * @property int $time_limit
 * @property string $time_limit_unit
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $send_by
 * @property int $base_id
 * @property string $tag
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereBaseId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereCustomerIds($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereDiscount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereSendBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereTag($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereTimeLimit($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereTimeLimitUnit($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Coupon\History whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class History extends BaseZero
{
    protected $table = 'coupon_histories';

}
