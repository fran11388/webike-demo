<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/3
 * Time: 下午 02:20
 */

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
class Propaganda extends BaseDbs
{
    protected $table = 'propagandas';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function type(){
        return $this->belongsTo('Everglory\Models\Propaganda\Type');
    }

    public function position(){
        return $this->belongsTo('Everglory\Models\Propaganda\Position');
    }

    public function audience(){
        return $this->belongsTo('Everglory\Models\Propaganda\Audience');
    }

    public function bannedPaths(){
        return $this->hasMany('Everglory\Models\Propaganda\Banned\Path');
    }
}