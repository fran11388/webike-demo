<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;

class ShippingCountry extends BaseZero
{
    protected $table = 'shipping_country';

    public function shippingArea()
    {
        return $this->hasMany(ShippingArea::class,'country_id','id');
    }

}