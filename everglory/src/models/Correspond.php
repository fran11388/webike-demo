<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;

/**
 * Everglory\Models\Mptt
 *
 * @property integer $id
 * @property string $name
 * @property boolean $depth
 * @property string $url_rewrite
 * @property integer $parent_id
 * @property integer $n_left
 * @property integer $n_right
 * @property string $url_path
 * @property string $name_path
 * @property string $sort
 * @property boolean $_active
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereNLeft($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereNRight($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereUrlPath($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereNamePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereActive($value)
 * @mixin \Eloquent
 */
class Correspond extends BaseDbs
{

    protected $table = 'campaign_corresponds';

    public function mptt()
	{
		return $this->hasOne(Mptt::class, 'url_rewrite','ca_rewrite');
	}

	public function manufacturer()
	{
		return $this->hasOne(Manufacturer::class, 'url_rewrite','br_rewrite');
	}

}