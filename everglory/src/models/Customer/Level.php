<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Product;

/**
 * Everglory\Models\Customer\Level
 *
 * @property int $id
 * @property int $customer_id
 * @property int $year_month yyyyMM
 * @property int $performance 績效....該期間內發票金額
 * @property int $level 等級
 * @property float $rate 點數倍率
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereRate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level wherePerformance($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Level whereYearMonth($value)
 * @mixin \Eloquent
 */
class Level extends BaseZero{

	protected $table = 'customer_levels';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
