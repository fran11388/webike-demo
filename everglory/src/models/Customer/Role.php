<?php //Zero model
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Customer\Role
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends BaseZero
{
    protected $table = 'customer_roles';
    protected $guarded  = array('id');

}