<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;

/**
 * Everglory\Models\Customer\Reward
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $points_collected
 * @property integer $points_used
 * @property integer $points_waiting
 * @property integer $points_current
 * @property integer $points_lost
 * @property string $checked_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward wherePointsCollected($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward wherePointsUsed($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward wherePointsWaiting($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward wherePointsCurrent($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward wherePointsLost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward whereCheckedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Reward whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Reward extends BaseZero
{
    protected $table = 'customer_rewards';
    protected $guarded  = array('id');

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}