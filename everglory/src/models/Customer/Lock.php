<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;

class Lock extends BaseZero{

	/*
	 * This table is lock for all site (black list)
	 * 
	 * */
	protected $table = 'customer_lock';
	protected $guarded  = array('id');

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}
}
