<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Product;

/**
 * Everglory\Models\Customer\Visit
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $product_id
 * @property integer $count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Everglory\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Visit whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Visit whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Visit whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Visit whereCount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Visit whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Visit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Visit extends BaseZero{

	protected $table = 'customer_visits';

	public function customer(){
		return $this->belongsTo( Customer::class );
	}

	public function product(){
		return $this->hasOne( Product::class , 'id' , 'product_id');
	}
}
