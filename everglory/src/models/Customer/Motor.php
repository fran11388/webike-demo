<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Customer\Motor
 *
 * @property int $customer_id
 * @property int $motor_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Motor whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Motor whereMotorId($value)
 * @mixin \Eloquent
 */
class Motor extends BaseZero{

	protected $table = 'customer_motor';
	protected $fillable = array('customer_id','motor_id');
	public $timestamps = false;
	protected $primaryKey = 'motor_id';
}
