<?php

namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;

Class Estimate extends BaseZero{

    protected $table = 'customer_estimates';

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
