<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;


/**
 * Everglory\Models\Customer\Facebook
 *
 * @property int $customer_id
 * @property string $access_token
 * @property string $name
 * @property string $expired_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Facebook whereAccessToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Facebook whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Facebook whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Facebook whereExpiredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Facebook whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Facebook whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Facebook extends BaseZero{

	protected $table = 'customer_facebooks';
	protected $guarded  = array('id');
	protected $primaryKey = 'customer_id';
}
