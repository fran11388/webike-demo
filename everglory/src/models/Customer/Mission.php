<?php

namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;

/**
 * Everglory\Models\Customer\Mission
 *
 * @property int $id
 * @property int $customer_id
 * @property int $mission_id
 * @property int $reward_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Everglory\Models\Mission $mission
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Mission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Mission whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Mission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Mission whereMissionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Mission whereRewardStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Mission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Mission extends BaseZero
{
    protected $table = 'customer_mission';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function mission()
    {
        return $this->belongsTo(\Everglory\Models\Mission::class);
    }

}
