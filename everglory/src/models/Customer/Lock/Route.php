<?php
namespace Everglory\Models\Customer\Lock;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Customer\Address;

class Route extends BaseZero{

	/*
	 * This table is lock for special route , like genuineparts
	 * 
	 * */
	protected $table = 'customer_lock_routes';

	public function customer()
	{
		return $this->belongsTo(Customer::class);
	}
	
	public function address(){
		return $this->hasMany(Address::class,'customer_id','customer_id');
	}
}
