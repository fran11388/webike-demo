<?php

namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Gift as GiftList;
use Everglory\Models\Product;

class Gift extends BaseZero
{
    protected $table = 'customer_gifts';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function gift()
    {
        return $this->belongsTo(GiftList::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}