<?php
namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Product;

/**
 * Everglory\Models\Customer\Address
 *
 * @property integer $id
 * @property integer $customer_id
 * @property boolean $is_default
 * @property string $zipcode
 * @property string $county
 * @property string $district
 * @property string $address
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $vat_number
 * @property string $phone
 * @property string $mobile
 * @property string $shipping_time
 * @property string $shipping_comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereZipcode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereCounty($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereDistrict($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereVatNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereMobile($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereShippingTime($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereShippingComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Address whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Address extends BaseZero{

	protected $table = 'customer_addresses';

}
