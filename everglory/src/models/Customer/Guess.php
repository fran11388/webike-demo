<?php

namespace Everglory\Models\Customer;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;

/**
 * Everglory\Models\Customer\Guess
 *
 * @property int $customer_id
 * @property int $guess_id
 * @property string $answer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Everglory\Models\Guess $mission
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Guess whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Guess whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Guess whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Guess whereGuessId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer\Guess whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Guess extends BaseZero
{
    protected $table = 'customer_guess';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function mission()
    {
        return $this->belongsTo(\Everglory\Models\Guess::class);
    }

}
