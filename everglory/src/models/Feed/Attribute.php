<?php

namespace Everglory\Models\Feed;

use Everglory\BaseModel\BaseDbs;


Class Attribute extends BaseDbs{

	protected $guarded = ['id'];
	protected $table = 'feed_attributes';

}
