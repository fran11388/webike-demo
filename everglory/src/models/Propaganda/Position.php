<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/3
 * Time: 下午 02:18
 */

namespace Everglory\Models\Propaganda;

use Everglory\BaseModel\BaseDbs;
class Position extends BaseDbs
{
    protected $table = 'propaganda_positions';
    protected $guarded = [];
}