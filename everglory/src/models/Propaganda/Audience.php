<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/3
 * Time: 下午 02:19
 */

namespace Everglory\Models\Propaganda;

use Everglory\BaseModel\BaseDbs;
class Audience extends BaseDbs
{
    protected $table = 'propaganda_audiences';
    protected $guarded = [];
}