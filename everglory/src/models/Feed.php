<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Feed\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Everglory\Models\Review
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $data_id
 * @property string $data_category
 * @property string $data_type
 * @property string $display_date
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $image_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $deadline
 * @property string $sticky_sort
 * @property string $module
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Feed\Attribute[] $attributes
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDataId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDataCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDataType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDisplayDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereImageUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereStickySort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Feed whereModule($value)
 */
class Feed extends BaseDbs
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $guarded = ['id'];
    protected $table = 'feeds';

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

}
