<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;

class WarehouseQueue extends BaseDbs
{
    protected $table = 'warehouse_queue';
    public $primaryKey = 'id';
    protected $fillable = [];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

  }
