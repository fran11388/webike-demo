<?php

namespace Everglory\Models\Mitumori;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Mitumori;
use Everglory\Models\Manufacturer;
use Everglory\Models\Product;
use Everglory\Models\Mitumori\Nouki;

Class Item extends BaseDbs
{
    protected $table = 'mitumori_items';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function mitumori()
    {
        return $this->belongsTo(Mitumori::class);
    }

    public function manufacturer(){
        return $this->belongsTo(Manufacturer::class, 'product_manufacturer_id', 'id' );
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function nouki()
    {
        return $this->hasOne(Nouki::class, 'nouki_code', 'product_nouki');
    }
}