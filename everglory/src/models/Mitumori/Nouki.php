<?php

namespace Everglory\Models\Mitumori;

use Everglory\BaseModel\BaseDbs;

Class Nouki extends BaseDbs
{
    protected $table = 'mitumori_nouki_code';
    protected $guarded = ['nouki_code'];

    public function item()
    {
        return $this->belongsTo('\Everglory\Models\Mitumori\Item', 'product_nouki', 'nouki_code');
    }

}