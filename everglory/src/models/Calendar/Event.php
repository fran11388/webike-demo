<?php

namespace Everglory\Models\Calendar;

use Everglory\BaseModel\BaseDbs;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends BaseDbs
{
    use SoftDeletes;
    protected $table = 'calendar_event';
    protected $guarded  = ['id'];
}
