<?php //Zero model
namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;


/**
 * Everglory\Models\Stock
 *
 * @property integer $id
 * @property integer $inbound_id
 * @property integer $purchase_id
 * @property string $bar_code
 * @property string $purchase_code
 * @property integer $product_id
 * @property float $orig_cost
 * @property float $rate
 * @property float $shipping_cost
 * @property float $import_tax_cost
 * @property float $service_fee_cost
 * @property float $other_cost
 * @property float $cost
 * @property string $name
 * @property string $sku
 * @property string $model_number
 * @property string $location_name
 * @property integer $status_id
 * @property integer $situation_id
 * @property integer $type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $note
 * @property integer $stock_out_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereInboundId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock wherePurchaseId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereBarCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock wherePurchaseCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereOrigCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereRate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereShippingCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereImportTaxCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereServiceFeeCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereOtherCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereModelNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereLocationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereLocationName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereSituationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereShelfId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock whereStockOutId($value)
 * @mixin \Eloquent
 */
class Stock  extends BaseZero {


    protected $table = 'stocks';
    protected $guarded  = array('id');

}