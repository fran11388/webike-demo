<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex\Item;
use Everglory\Models\Rex\Status;
use Everglory\Models\Rex\History;
use Everglory\Models\Customer\Address;
use Everglory\Models\Rex\Type;
use Illuminate\Database\Eloquent\SoftDeletes;

Class Rex extends BaseZero
{
    protected $table = 'rexes';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function histories()
    {
        return $this->hasMany(History::class);
    }
}

