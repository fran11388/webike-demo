<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Sales\Shipping
 *
 * @property integer $id
 * @property string $name
 * @property string $classname
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Shipping whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Shipping whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Shipping whereClassname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Shipping whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Shipping whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Shipping whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Shipping extends BaseZero{

	protected $table = 'order_shipment_methods';
	protected $guarded  = array('id');

}
