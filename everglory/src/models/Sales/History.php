<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

/**
 * Everglory\Models\Sales\History
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $stuff_id
 * @property integer $staff_id
 * @property integer $status_id
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $notify
 * @property-read \Everglory\Models\Sales\Status $status
 * @property-read \Everglory\Models\Order $order
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereStuffId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereStaffId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\History whereNotify($value)
 * @mixin \Eloquent
 */
class History extends BaseZero{

	protected $table = 'order_histories';
    protected $guarded  = array('id');

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
