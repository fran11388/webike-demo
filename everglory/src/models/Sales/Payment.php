<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

/**
 * Everglory\Models\Sales\Payment
 *
 * @property integer $id
 * @property string $name
 * @property string $backendname
 * @property string $code
 * @property string $classname
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Sales\Status[] $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order[] $order
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereBackendname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereClassname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payment extends BaseZero
{

    protected $table = 'order_payment_methods';
    protected $guarded = array('id');

    public function status()
    {
        return $this->belongsToMany(Status::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
