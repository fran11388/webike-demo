<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Product;

/**
 * Everglory\Models\Sales\Cart
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $product_id
 * @property integer $quantity
 * @property float $price
 * @property integer $rewards
 * @property string $protect_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $cache_id
 * @property-read \Everglory\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereRewards($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereProtectCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Cart whereCacheId($value)
 * @mixin \Eloquent
 */
class Cart extends BaseZero{

	protected $table = 'carts';

	public function product(){
		return $this->belongsTo(Product::class);
	}
}
