<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Coupon;

/**
 * Everglory\Models\Sales\Quote
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $code
 * @property string $request
 * @property string $cart_items
 * @property integer $used_coupon_id
 * @property integer $used_points
 * @property integer $received_points
 * @property integer $cart_subtotal
 * @property integer $subtotal
 * @property integer $discount_total
 * @property integer $coupon_amount
 * @property integer $shipping_method
 * @property integer $payment_method
 * @property integer $shipping_amount
 * @property integer $payment_amount
 * @property boolean $install
 * @property integer $order_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Everglory\Models\Coupon $coupon
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereRequest($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereCartItems($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereUsedCouponId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereUsedPoints($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereReceivedPoints($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereCartSubtotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereSubtotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereGrandTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereCouponAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote wherePaymentMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereShippingAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote wherePaymentAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereInstall($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Quote whereDiscountTotal($value)
 */
class Quote extends BaseZero{

	protected $table = 'quotes';

	public function customer(){
		return $this->belongsTo(Customer::class);
	}

	public function coupon(){
        return $this->belongsTo(Coupon::class , 'used_coupon_id' , 'id');
    }
}
