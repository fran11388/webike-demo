<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

/**
 * Everglory\Models\Sales\Status
 *
 * @property integer $id
 * @property string $name
 * @property string $backend_name
 * @property boolean $active
 * @property boolean $show_date
 * @property string $sort
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order[] $orders
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereBackendName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereShowDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $group_name
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Status whereGroupName($value)
 */
class Status extends BaseZero
{

    protected $table = 'order_statuses';
    protected $guarded = array('id');

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
