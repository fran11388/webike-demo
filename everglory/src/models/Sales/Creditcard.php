<?php namespace Everglory\Models\Sales;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Sales\Creditcard
 *
 * @property integer $id
 * @property string $RC https://docs.google.com/spreadsheets/d/1ZA24dytfHaBtnlvZs0Z7kLeh8rCDoGuViID6DGezL_0/edit#gid=1181434803
 * @property string $MID
 * @property string $ONO
 * @property string $LTD
 * @property string $LTT
 * @property string $RRN
 * @property string $AIR
 * @property string $AN
 * @property integer $order_id
 * @property integer $quote_id
 * @property string $remote_addr
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $customer_id
 * @property string $TA
 * @property integer $check
 * @property string $remark
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereRC($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereMID($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereONO($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereLTD($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereLTT($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereRRN($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereAIR($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereAN($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereQuoteId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereRemoteAddr($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereTA($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereCheck($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Sales\Creditcard whereRemark($value)
 * @mixin \Eloquent
 */
class Creditcard extends BaseZero{

    protected $table = 'order_creditcards';

    public function order(){
        return $this->belongsTo('\Everglory\Models\Order');
    }

    public function customer(){
        return $this->belongsTo('\Everglory\Models\Customer');
    }
}
