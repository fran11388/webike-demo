<?php

namespace  Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Illuminate\Database\Eloquent\SoftDeletes;

Class cms_file extends BaseDbs
{
    protected $table = 'cms_files';
    
    public function object(){
        return $this->morphTo();
    }
}

