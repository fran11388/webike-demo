<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;

/**
 * Everglory\Models\ReviewMotor
 *
 * @property int $id
 * @property int $review_id
 * @property int $motor_id
 * @property bool $customer_selected_flg
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Review $review
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewMotor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewMotor whereCustomerSelectedFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewMotor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewMotor whereMotorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewMotor whereReviewId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewMotor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReviewMotor extends BaseDbs
{
    protected $table = 'review_motors';
    protected $guarded = ['id'];

    public function review(){
        return $this->belongsTo( Review::class );
    }

    public function motorModel(){
        return $this->belongsTo( Motor::class, 'motor_id', 'id' );
    }
}
