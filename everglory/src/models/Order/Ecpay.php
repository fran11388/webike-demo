<?php
namespace Everglory\Models\Order;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

class Ecpay extends BaseZero{

    protected $table = 'order_ecpay';

    public function order(){
        return $this->belongsTo( Order::class );
    }

}
