<?php
namespace Everglory\Models\Order;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

/**
 * Everglory\Models\Order\Status
 *
 * @property int $id
 * @property string $name
 * @property string $backend_name
 * @property string $group_name
 * @property bool $active
 * @property bool $show_date
 * @property string $sort
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order[] $order
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereBackendName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereGroupName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereShowDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Status whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Status extends BaseZero{

    protected $table = 'order_statuses';

    public function order(){
        return $this->hasMany( Order::class );
    }

}
