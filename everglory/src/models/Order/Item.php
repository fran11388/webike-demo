<?php

namespace Everglory\Models\Order;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Gift;
use Everglory\Models\Order;
use Everglory\Models\Product;
use Everglory\Models\Order\Item\Detail;
use Everglory\Models\Review;
use Illuminate\Support\Facades\Request;
use Everglory\Models\Purchase;


/**
 * Everglory\Models\Order\Item
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $customer_id
 * @property integer $role_id
 * @property integer $product_id
 * @property integer $main_product_id
 * @property integer $manufacturer_id
 * @property integer $quantity
 * @property float $price
 * @property integer $point
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $sku
 * @property integer $status_id
 * @property boolean $product_type
 * @property string $model_number
 * @property boolean $is_owe
 * @property integer $cache_id
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Everglory\Models\Order $order
 * @property-read \Everglory\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereMainProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item wherePoint($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereProductType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereModelNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereIsOwe($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item whereCacheId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order\Item\Detail[] $details
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Purchase[] $purchases
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Review[] $reviews
 */
class Item extends BaseZero
{

    protected $table = 'order_items';

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function details()
    {
        return $this->hasMany(Detail::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'product_id', 'main_product_id')->where('customer_id', $this->customer_id);
    }

    public function purchases()
    {
        return $this->morphMany(Purchase::class, 'item');
    }

    public function gift()
    {
        return $this->hasOne(Gift::class, 'product_id', 'product_id');
    }

    public function manufacturer()
    {
        return $this->belongsTo('\Everglory\Models\Manufacturer');
    }

    public function mainProduct()
    {
        return $this->belongsTo(Product::class, 'main_product_id', 'id');
    }

//    public function getItemDetailAttribute()
//    {
//        $itemDetails = $this->details->keyBy('attribute');
//        $collection = new \stdClass();
//        foreach ($itemDetails as $key => $entity) {
//            $collection->$key = $entity->value;
//        }
//        return $collection;
//    }


}
