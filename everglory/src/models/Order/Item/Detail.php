<?php
namespace Everglory\Models\Order\Item;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer;
use Everglory\Models\Order;
use Everglory\Models\Order\Item;

/**
 * Everglory\Models\Order\Item\Detail
 *
 * @property integer $id
 * @property integer $item_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $attribute
 * @property string $value
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item\Detail whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item\Detail whereItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item\Detail whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item\Detail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item\Detail whereAttribute($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Item\Detail whereValue($value)
 * @mixin \Eloquent
 */
class Detail extends BaseZero{

	protected $table = 'order_item_details';
    protected $guarded  = array('id');

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
