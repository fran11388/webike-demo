<?php
namespace Everglory\Models\Order;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

/**
 * Everglory\Models\Order\History
 *
 * @property int $id
 * @property int $order_id
 * @property int $stuff_id
 * @property int $staff_id
 * @property int $status_id
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property bool $notify
 * @property-read \Everglory\Models\Order\Status $status
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereNotify($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereStaffId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereStuffId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\History whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class History extends BaseZero{

    protected $table = 'order_histories';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

}
