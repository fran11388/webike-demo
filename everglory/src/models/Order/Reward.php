<?php
namespace Everglory\Models\Order;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order;

/**
 * Everglory\Models\Order\Reward
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $points_current
 * @property integer $points_spend
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $start_date
 * @property string $end_date
 * @property boolean $status
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward wherePointsCurrent($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward wherePointsSpend($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereStartDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereEndDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order\Reward whereStatus($value)
 * @mixin \Eloquent
 */
class Reward extends BaseZero
{

    protected $table = 'order_rewards';

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
