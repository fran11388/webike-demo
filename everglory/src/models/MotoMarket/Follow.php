<?php

namespace Everglory\Models\MotoMarket;

use Everglory\BaseModel\BaseMotoMarket;

class Follow extends BaseMotoMarket
{
    protected $table = 'follows';

    public function target(){
        return $this->morphTo();
    }
    
}
