<?php

namespace Everglory\Models\MotoMarket;

use Everglory\BaseModel\BaseMotoMarket;


/**
 * Everglory\Models\MotoMarket\MotorModel
 *
 * @property integer $id
 * @property string $import
 * @property string $manufacturer
 * @property integer $manufacturer_id
 * @property integer $displacement
 * @property string $title
 * @property string $user_defined
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $url_rewrite
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereImport($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereManufacturer($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereDisplacement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereUserDefined($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\MotorModel whereUrlRewrite($value)
 * @mixin \Eloquent
 */
class MotorModel extends BaseMotoMarket
{
    protected $table = 'motor_model';

}
