<?php

namespace Everglory\Models\MotoMarket;

use Everglory\BaseModel\BaseMotoMarket;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Everglory\Models\MotoMarket\Product
 *
 * @property integer $id
 * @property string $serial
 * @property integer $customer_id
 * @property string $is_import
 * @property string $is_new
 * @property integer $motor_model_id
 * @property integer $new_motor_model_id
 * @property string $product_description
 * @property string $zipcode
 * @property string $county
 * @property string $district
 * @property string $display_price
 * @property float $price
 * @property integer $publish_days
 * @property string $deadline
 * @property float $current_distance
 * @property integer $motor_year
 * @property integer $motor_status_id
 * @property integer $motor_color_id
 * @property integer $motor_type_id
 * @property string $motor_photos
 * @property string $other_description
 * @property string $status
 * @property integer $views
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \Everglory\Models\MotoMarket\MotorModel $motorModel
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereSerial($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereIsImport($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereIsNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereMotorModelId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereNewMotorModelId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereProductDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereZipcode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereCounty($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereDistrict($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereDisplayPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product wherePublishDays($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereCurrentDistance($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereMotorYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereMotorStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereMotorColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereMotorTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereMotorPhotos($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereOtherDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereViews($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotoMarket\Product whereDeletedAt($value)
 * @mixin \Eloquent
 */
class Product extends BaseMotoMarket
{
    use SoftDeletes;
    protected $table = 'products';
    protected $dates = ['deleted_at'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function motorModel(){
        return $this->belongsTo(MotorModel::class, 'motor_model_id', 'id');
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
