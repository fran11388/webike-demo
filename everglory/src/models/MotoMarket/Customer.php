<?php

namespace Everglory\Models\MotoMarket;

use Everglory\BaseModel\BaseMotoMarket;

class Customer extends BaseMotoMarket
{
    protected $table = 'customers';

    public function follows(){
        return $this->morphMany(Follow::class,'target');
    }

    public function profiles(){
        return $this->hasMany(Profile::class,'customer_id');
    }
    
    public function group(){
        return $this->belongsto(Group::class,'group_id','id');
    }
    
}
