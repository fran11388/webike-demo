<?php

namespace Everglory\Models\Estimate;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Estimate;

Class Status extends BaseZero
{
    protected $table = 'estimate_statuses';
    protected $guarded = ['id'];

    public function estimate()
    {
        return $this->belongsTo(Estimate::class);
    }
}

