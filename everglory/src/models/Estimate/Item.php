<?php

namespace Everglory\Models\Estimate;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Estimate;
use Everglory\Models\Estimate\Item\Detail;
use Everglory\Models\Purchase;
use Everglory\Models\DeliveryDay;
use Everglory\Models\Manufacturer;


Class Item extends BaseZero
{
    protected $table = 'estimate_items';
    protected $guarded = ['id'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function estimate()
    {
        return $this->belongsTo(Estimate::class);
    }

    public function details()
    {
        return $this->hasMany(Detail::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }
}

