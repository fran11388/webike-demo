<?php

namespace Everglory\Models\Estimate;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Estimate\Item;

Class Status extends BaseZero
{
    protected $table = 'estimate_statuses';
    protected $guarded = ['id'];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}

