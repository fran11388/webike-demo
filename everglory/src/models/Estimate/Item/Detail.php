<?php

namespace Everglory\Models\Estimate\Item;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Estimate\Item;

Class Detail extends BaseZero
{
    protected $table = 'estimate_item_details';
    protected $guarded = ['id'];

    public function item()
    {
        return $this->hasOne(Item::class);
    }
}

