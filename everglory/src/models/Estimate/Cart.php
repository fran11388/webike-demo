<?php

namespace Everglory\Models\Estimate;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Product;

Class Cart extends BaseZero{

	protected $table = 'estimate_carts';

	public function product(){
		return $this->belongsTo(Product::class);
	}
}
