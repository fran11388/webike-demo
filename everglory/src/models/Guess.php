<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Product;

/**
 * Everglory\Models\Guess
 *
 * @property int $id
 * @property int $mission_id
 * @property int $product_id
 * @property int $type_id
 * @property string $genuine_maker
 * @property string $genuine_name
 * @property string $description
 * @property string $date
 * @property string $answer
 * @property string $before_img
 * @property string $after_img
 * @property string $ans_img
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereAfterImg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereAnsImg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereBeforeImg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereGenuineMaker($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereGenuineName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereMissionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Guess whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Guess extends BaseDbs
{
    protected $table = 'guess';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
