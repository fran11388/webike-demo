<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;

/**
 * Everglory\Models\Review
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $customer_id
 * @property string $nick_name
 * @property boolean $ranking
 * @property string $rating
 * @property string $title
 * @property string $content
 * @property string $photo_key
 * @property string $customer_ip
 * @property boolean $view_status 0:尚未審核
 * 1:已審核
 * @property integer $product_id
 * @property string $product_sku
 * @property string $product_manufacturer
 * @property integer $product_manufacturer_id
 * @property string $product_manufacturer_code
 * @property string $product_name
 * @property string $product_thumbnail
 * @property string $product_category
 * @property string $product_category_search
 * @property integer $motor_id
 * @property string $motor_name
 * @property string $motor_manufacturer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereNickName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereRanking($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereRating($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review wherePhotoKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereCustomerIp($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereViewStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductManufacturer($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductManufacturerCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductThumbnail($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereProductCategorySearch($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereMotorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereMotorName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereMotorManufacturer($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Review whereDeletedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\ReviewComment[] $comments
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\ReviewMotor[] $motors
 * @property-read \Everglory\Models\Product $product
 */
class Review extends BaseDbs
{

    protected $table = 'ec_dbs.reviews';
    protected $guarded  = array('id');

    public function customer(){
        return $this->belongsTo( Customer::class );
    }

    public function product(){
        return $this->belongsTo( Product::class );
    }

//    public function motor(){
//        return $this->belongsTo( Motor::class, 'motor_id', 'url_rewrite' );
//    }
    public function comments(){
        return $this->hasMany( ReviewComment::class )
            ->orderBy('created_at','desc');
    }

    public function publishedComments(){
        return $this->comments()->where('verify' , '1');
    }

    public function motors(){
        return $this->hasMany( ReviewMotor::class );
    }

    public function category(){
        return $this->belongsTo( Category::class, 'product_category');
    }
}
