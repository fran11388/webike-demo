<?php

namespace Everglory\Models\Promotion;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Promotion\Item;

/**
 * Everglory\Models\Promotion\Type
 *
 * @property int $id
 * @property string $group
 * @property string $name
 * @property string $code
 * @property int $sort
 * @property string $template
 * @property bool $is_anchor
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Promotion\Item[] $items
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereIsAnchor($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Type whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Type extends BaseDbs
{
    protected $table = 'promotion_types';
    public function items()
    {
        return $this->hasMany(Item::class);
    }
}