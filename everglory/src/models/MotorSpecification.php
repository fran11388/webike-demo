<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\MotorSpecification
 *
 * @property integer $id
 * @property string $identifier
 * @property string $motor_id
 * @property integer $divide_code
 * @property string $manufacturer_name
 * @property string $motor_model
 * @property string $motor_name
 * @property string $spec_group_code
 * @property string $image
 * @property string $summary
 * @property float $displacement
 * @property integer $release_year
 * @property integer $release_month
 * @property integer $fuel_consumption
 * @property integer $fuel_consumption_rate_speed
 * @property float $fuel_capacity
 * @property boolean $fuel_system
 * @property string $engine_type
 * @property string $maximum_power
 * @property string $maximum_torque
 * @property boolean $weight_type
 * @property string $dry_weight
 * @property string $dimensions
 * @property string $seat_height
 * @property float $turning_radius
 * @property integer $riding_capacity
 * @property boolean $motortype_code
 * @property string $manufacture_country
 * @property string $bikou
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $status
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereIdentifier($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereMotorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereDivideCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereManufacturerName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereMotorModel($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereMotorName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereSpecGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereSummary($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereDisplacement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereReleaseYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereReleaseMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereFuelConsumption($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereFuelConsumptionRateSpeed($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereFuelCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereFuelSystem($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereEngineType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereMaximumPower($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereMaximumTorque($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereWeightType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereDryWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereDimensions($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereSeatHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereTurningRadius($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereRidingCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereMotortypeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereManufactureCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereBikou($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorSpecification whereStatus($value)
 * @mixin \Eloquent
 */
class MotorSpecification extends BaseProduct
{

    protected $table = 'motor_specifications';


    public function motor(){
        return $this->belongsTo(Motor::class);
    }
}
