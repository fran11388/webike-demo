<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Mptt
 *
 * @property integer $id
 * @property string $name
 * @property boolean $depth
 * @property string $url_rewrite
 * @property integer $parent_id
 * @property integer $n_left
 * @property integer $n_right
 * @property string $url_path
 * @property string $name_path
 * @property string $sort
 * @property boolean $_active
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereNLeft($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereNRight($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereUrlPath($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereNamePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Mptt whereActive($value)
 * @mixin \Eloquent
 */
class Mptt extends BaseProduct
{

    protected $table = 'mptt_categories_flat';

}
