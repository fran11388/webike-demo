<?php

namespace Everglory\Models;

use Ecommerce\Enums\CustomerRoleEnum;
use Ecommerce\Enums\PointType;
use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Campaign\Preorder\Item;
use Everglory\Models\Product\Attribute\StringValue;
use Everglory\Models\Product\Description;
use Everglory\Models\Product\FitModel;
use Everglory\Models\Product\Image;
use Everglory\Models\Product\Option;
use Everglory\Models\Product\OptionFlat;
use Everglory\Models\Product\Point;
use Everglory\Models\Product\Price;
use Everglory\Models\Product\PriceDifference;
use Everglory\Models\Product\Relation;
use Everglory\Models\Stock;
use Everglory\Models\Stock\Outlet;
use Everglory\Models\Series;
use Everglory\Models\Stock\Additional;

/**
 * Everglory\Models\Product
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property integer $manufacturer_id
 * @property string $url_rewrite
 * @property string $meta_description
 * @property string $meta_title
 * @property string $meta_keyword
 * @property boolean $active
 * @property string $sku
 * @property string $model_number
 * @property float $cost
 * @property float $price
 * @property integer $point
 * @property boolean $stock_management 0:USE API
 * 1:USE TABLE
 * @property integer $stock_quantity
 * @property integer $sale_quantity
 * @property boolean $has_branch
 * @property integer $parent_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $visits
 * @property integer $group_code
 * @property string $ean_code
 * @property boolean $type 0:一般商品
 * 1:正廠零件
 * 2:未登錄商品
 * 3:團購商品
 * 4:加價購商品
 * @property boolean $is_main
 * @property string $p_country
 * @property boolean $is_new
 * @property boolean $point_type
 * @property string $synonym
 * @property-read \Everglory\Models\Stock\Outlet $outlet
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Category[] $categories
 * @property-read \Everglory\Models\Product\Description $productDescription
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Option[] $options
 * @property-read \Everglory\Models\Manufacturer $manufacturer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\FitModel[] $fitModels
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor[] $motors
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Price[] $prices
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Point[] $points
 * @property-read \Everglory\Models\Campaign\Preorder\Item $preorderItem
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Review[] $reviews
 * @property-read \Everglory\Models\Product\Stock $stock
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Attribute\StringValue[] $attrString
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product[] $groups
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereMetaKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereModelNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product wherePoint($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereStockManagement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereStockQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereSaleQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereHasBranch($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereVisits($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereEanCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereIsMain($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product wherePCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereIsNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product wherePointType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product whereSynonym($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Series[] $series
 * @property-read \Everglory\Models\Product\Relation $relationKey
 * @property-read \Everglory\Models\Product\OptionFlat $optionFlat
 * @property-read \Everglory\Models\Stock\Additional $reflect
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Stock[] $stocks
 */
class Product extends BaseProduct
{

    protected $table = 'eg_product.products';


    public function outlet(){
        return $this->hasOne( Outlet::class, 'product_id', 'id' );
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function productDescription()
    {
        return $this->hasOne(Description::class);
    }

    public function descriptionOrig()
    {
        return $this->hasOne(Description\Orig::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function optionFlat()
    {
        return $this->hasOne(OptionFlat::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function fitModels()
    {
        return $this->hasMany(FitModel::class);
    }

    public function motors()
    {
        return $this->belongsToMany(Motor::class, 'eg_product.product_motor', 'product_id', 'motor_id');
    }

    public function prices()
    {
        return $this->hasMany(Price::class)->where('rule_date','=',date('Y-m-d'));
    }

    public function priceDifference()
    {
        return $this->hasOne(PriceDifference::class);
    }

    public function points()
    {
        return $this->hasMany(Point::class)->where('rule_date','=',date('Y-m-d'));
    }

    public function preorderItem()
    {
        return $this
            ->hasOne(Item::class, 'product_id', 'id')
            ->where('ended_at', '>=', strtotime(date('Y-m-d')));
    }

    public function reviews()
    {
        return $this
            ->hasMany(Review::class)
            ->orderBy('created_at', 'desc');
    }

    public function stocks()
    {
        return $this->hasMany( Stock::class ,'bar_code','sku')->whereIn('stocks.type_id',[1,8])->where('stocks.status_id','1')->where('stocks.location_id',1);
    }

    public function attrString()
    {
        return $this->hasMany(StringValue::class);
    }

    public function groups()
    {
        return $this
            ->hasMany(Product::class, 'group_code', 'group_code')
            ->where('manufacturer_id', $this->manufacturer_id);
    }

    public function series()
    {
        return $this->belongsToMany(Series::class,'product_series','product_id','series_id');
    }

    public function relationKey()
    {
        return $this->hasOne(Relation::class);
    }

    public function reflect(){
        return $this->hasOne(Additional::class, 'product_id', 'id');
    }

    public function queue(){
        return $this->hasOne( WarehouseQueue::class );
    }
    
    public function supplier()
    {
        return $this->hasOne(\Everglory\Models\Supplier\Product::class);
    }

    public function suppliers()
    {
        return $this->hasMany(\Everglory\Models\Supplier\Product::class);
    }
    
}
