<?php
namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Email\Newsletter\Queue;

class Newsletter extends BaseDbs{

    protected $table = 'email_newsletter';

    public function queues(){
        return $this->hasMany( Queue::class ,'queue_id');
    }
}
