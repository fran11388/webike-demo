<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;

/**
 * Everglory\Models\ReviewComment
 *
 * @property int $id
 * @property int $review_id
 * @property int $customer_id
 * @property string $nick_name
 * @property string $content
 * @property string $verify
 * @property string $customer_ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Everglory\Models\Review $review
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereCustomerIp($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereNickName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereReviewId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\ReviewComment whereVerify($value)
 * @mixin \Eloquent
 */
class ReviewComment extends BaseDbs
{

    protected $table = 'review_comments';
    public function customer(){
        return $this->belongsTo( Customer::class );
    }

    public function review(){
        return $this->belongsTo( Review::class );
    }
}
