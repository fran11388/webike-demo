<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Mitumori\Item as MitumoriItem;

Class Mitumori extends BaseDbs
{
    const CREATED_AT = 'request_date';
    const UPDATED_AT = 'response_date';
    protected $table = 'mitumori';
    protected $guarded = ['id'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }

    public function items()
    {
        return $this->hasMany(MitumoriItem::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}

