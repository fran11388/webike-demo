<?php

namespace Everglory\Models\Rex;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex;
use Everglory\Models\Rex\Item\Detail;
use Everglory\Models\DeliveryDay;


Class Item extends BaseZero
{
    protected $table = 'rex_items';
    protected $guarded = ['id'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function rex()
    {
        return $this->belongsTo(Rex::class);
    }

    public function details()
    {
        return $this->hasMany(Detail::class);
    }
}

