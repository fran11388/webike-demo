<?php

namespace Everglory\Models\Rex;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex;

Class Status extends BaseZero
{
    protected $table = 'rex_statuses';
    protected $guarded = ['id'];

    public function rex()
    {
        return $this->belongsTo(Rex::class);
    }
}

