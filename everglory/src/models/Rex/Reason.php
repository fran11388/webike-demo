<?php

namespace Everglory\Models\Rex;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex;

Class Reason extends BaseZero
{
    protected $table = 'rex_reasons';
    protected $guarded = ['id'];
}

