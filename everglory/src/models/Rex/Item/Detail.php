<?php

namespace Everglory\Models\Rex\Item;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex\Item;

Class Detail extends BaseZero
{
    protected $table = 'rex_item_details';
    protected $guarded = ['id'];

    public function item()
    {
        return $this->hasOne(Item::class);
    }
}

