<?php

namespace Everglory\Models\Rex;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex;

Class Type extends BaseZero
{
    protected $table = 'rex_types';
    protected $guarded = ['id'];
}

