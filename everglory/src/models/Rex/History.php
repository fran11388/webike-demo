<?php

namespace Everglory\Models\Rex;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Rex;

Class History extends BaseZero
{
    protected $table = 'rex_histories';
    protected $guarded = ['id'];

    public function rex()
    {
        return $this->belongsTo(Rex::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

}

