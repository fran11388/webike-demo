<?php

namespace Everglory\Models\Campaign\Preorder;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Campaign\Preorder;
use Everglory\Models\Product;

/**
 * Everglory\Models\Campaign\Preorder\Item
 *
 * @property-read \Everglory\Models\Campaign\Preorder $preorder
 * @property-read \Everglory\Models\Product $product
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $preorder_id
 * @property integer $product_id
 * @property string $quantity_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $ended_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item wherePreorderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item whereQuantityStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Campaign\Preorder\Item whereEndedAt($value)
 */
class Item extends BaseDbs
{
    protected $table = 'campaign_preorder_items';

    public function preorder(){
        return $this->belongsTo( Preorder::class , 'preorder_id', 'id' );
    }

    public function product(){
        return $this->belongsTo( Product::class );
    }
}