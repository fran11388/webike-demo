<?php

namespace Everglory\Models\Campaign;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Product;

class Recommend extends BaseDbs
{
    protected $table = 'campaign_recommends';

    public function product()
    {
        return $this->belongsTo( Product::class, 'sku', 'url_rewrite');
    }
}