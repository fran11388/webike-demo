<?php

namespace Everglory\Models\Campaign;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Campaign\Preorder\Item;

/**
 * Everglory\Models\Campaign\Preorder
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Campaign\Preorder\Item[] $items
 * @mixin \Eloquent
 */
class Preorder extends BaseProduct
{
    protected $table = 'campaign_preorder';

    public function items()
    {
        
        return $this->hasMany( Item::class,'preorder_id');
    }    
}