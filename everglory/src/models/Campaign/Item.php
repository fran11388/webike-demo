<?php

namespace Everglory\Models\Campaign;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Campaign;

/**
 * Everglory\Models\Promotion\Item
 *
 * @property int $id
 * @property int $promotion_id
 * @property int $type_id
 * @property string $name
 * @property string $note
 * @property string $url_rewrite
 * @property string $link
 * @property string $image
 * @property string $sub_image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Promotion $promotion
 * @property-read \Everglory\Models\Promotion\Type $type
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item wherePromotionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereSubImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Promotion\Item whereUrlRewrite($value)
 * @mixin \Eloquent
 */
class Item extends BaseDbs
{
    protected $table = 'campaign_items';
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function template()
    {
        return $this->hasOne(Template::class,'id','template_id');
    }

}