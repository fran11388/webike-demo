<?php //Zero model
namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\DeliveryDay;

/**
 * Everglory\Models\Purchase
 *
 * @property int $id
 * @property int $item_id
 * @property string $item_type
 * @property int $quantity
 * @property int $supplier_id
 * @property int $status_id
 * @property string $estimate_code
 * @property int $estimate_method_id
 * @property string $api_code
 * @property string $buy_code
 * @property int $buy_method_id
 * @property string $delivery_code
 * @property bool $delivery_days
 * @property bool $execute_flg
 * @property bool $is_main
 * @property string $arrive_date
 * @property string $note
 * @property float $cost
 * @property float $orig_cost
 * @property string $require_info
 * @property bool $confirm_flg 1:交期異動
 * 2:等待取消
 * 3:同訂單採購無庫存再次見積，等待中。
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $group_id
 * @property-read \Everglory\Models\DeliveryDay $delivery
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $item
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereApiCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereArriveDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereBuyCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereBuyMethodId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereConfirmFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereDeliveryCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereDeliveryDays($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereEstimateCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereEstimateMethodId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereExecuteFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereIsMain($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereItemType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereOrigCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereRequireInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereSupplierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Purchase whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Purchase  extends BaseZero {

    protected $table = 'purchases';
    protected $guarded  = array('id');

    public function item()
    {
        return $this->morphTo();
    }

    public function delivery()
    {
        return $this->belongsTo(DeliveryDay::class, 'delivery_code', 'code');
    }

    public function supplier()
    {
        return $this->belongsTo('\Everglory\Models\Supplier');
    }
}