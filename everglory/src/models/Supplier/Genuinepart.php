<?php

namespace Everglory\Models\Supplier;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Manufacturer;
use Everglory\Models\MotorManufacturer;


Class Genuinepart extends BaseProduct{

	protected $table = 'supplier_genuineparts';

	public function manufacturer(){
		return $this->belongsTo( Manufacturer::class );
	}

    public function motor_manufacturer(){
        return $this->belongsTo( MotorManufacturer::class );
    }


}
