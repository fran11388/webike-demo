<?php

namespace Everglory\Models\Supplier;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Supplier;


class Product extends BaseProduct
{

    protected $table = 'product_suppliers';

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}