<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Order\Item;
use Everglory\Models\Order\Status;
use Everglory\Models\Order\History;
use Everglory\Models\Customer\Address;
use Everglory\Models\Sales\Payment;
use Everglory\Models\Order\VirtualBank;

/**
 * Everglory\Models\Order
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $address_id
 * @property string $comment
 * @property integer $status_id
 * @property string $coupon_code
 * @property string $coupon_name
 * @property integer $shipping_method
 * @property integer $payment_method
 * @property integer $rewardpoints_amount
 * @property float $coupon_amount
 * @property float $shipping_amount
 * @property float $payment_amount
 * @property float $grand_total
 * @property float $subtotal
 * @property string $increment_id
 * @property string $remote_addr
 * @property string $delivered_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $total_qty
 * @property string $tw_shipping
 * @property string $jp_shipping
 * @property integer $relation_id
 * @property integer $edit_increment
 * @property integer $points_current
 * @property boolean $jp_delivery
 * @property boolean $read
 * @property string $virtual_account
 * @property string $coupon_uuid
 * @property integer $shop_id
 * @property string $note stuff note
 * @property string $shipping_group_code
 * @property integer $segment_id
 * @property string $jp_order
 * @property string $jp_query
 * @property boolean $is_print
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order\Item[] $items
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereCouponCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereCouponName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereRewardpointsAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereCouponAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereShippingAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order wherePaymentAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereGrandTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereSubtotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereIncrementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereRemoteAddr($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereDeliveredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereTotalQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereTwShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereJpShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereRelationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereEditIncrement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order wherePointsCurrent($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereJpDelivery($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereRead($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereVirtualAccount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereCouponUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereShippingGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereSegmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereJpOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereJpQuery($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Order whereIsPrint($value)
 * @mixin \Eloquent
 * @property-read \Everglory\Models\Customer\Address $address
 * @property-read \Everglory\Models\Customer $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order\History[] $histories
 * @property-read \Everglory\Models\Sales\Payment $payment
 * @property-read \Everglory\Models\Rex $rex
 * @property-read \Everglory\Models\Order\Status $status
 */
class Order extends BaseZero
{

    protected $table = 'orders';

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_method');
    }

    public function histories()
    {
        return $this->hasMany(History::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function rex()
    {
        return $this->hasOne(Rex::class,'parent_id');
    }

    public function virtualbank()
    {
        return $this->hasOne(VirtualBank::class)->where('credit',0)->orderby('deadline', 'desc');
    }

}
