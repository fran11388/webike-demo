<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;


class Supplier extends BaseProduct
{
    protected $table = 'suppliers';
}
