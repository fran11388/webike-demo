<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Manufacturer\Info;

/**
 * Everglory\Models\Manufacturer
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $image
 * @property string $website
 * @property string $url_rewrite
 * @property boolean $active
 * @property boolean $is_new
 * @property string $synonym
 * @property integer $old_id magento_id
 * @property string $country
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereMetaKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereIsNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereSynonym($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereOldId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Manufacturer whereCountry($value)
 * @mixin \Eloquent
 * @property-read \Everglory\Models\Product $products
 */
class Manufacturer extends BaseProduct
{
    protected $table='manufacturers';

    public function products()
    {
        return $this->hasOne(Product::class);
    }

    public function info()
    {
        return $this->hasOne(Info::class);
    }
}
