<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Customer\Address;
use Everglory\Models\Customer\Visit;
use Everglory\Models\Customer\Reward;
use Everglory\Models\Customer\Role;
use Everglory\Models\Motor;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use App\Notifications\WebikeResetPassword as ResetPasswordNotification;
use Everglory\Models\Customer\Lock;

/**
 * Everglory\Models\Customer
 *
 * @property integer $id
 * @property string $email
 * @property string $nickname
 * @property string $password
 * @property string $birthday
 * @property boolean $gender
 * @property boolean $newsletter
 * @property integer $role_id
 * @property string $reset_password_code
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $realname
 * @property string $last_login
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereNickname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereNewsletter($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereResetPasswordCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereRealname($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Customer whereLastLogin($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Customer\Visit[] $visits
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Customer\Address[] $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Order[] $orders
 * @property-read \Everglory\Models\Customer\Reward $points
 * @property-read \Everglory\Models\Customer\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor[] $motors
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
class Customer extends BaseZero implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    protected $table = 'customers';

    protected $hidden = ['password', 'remember_token'];

    public function visits()
    {
        return $this->hasMany( Visit::class );
    }
    public function address(){
        return $this->hasMany( Address::class );
    }

    public function orders(){
        return $this->hasMany( Order::class );
    }

    public function points(){
        return $this->hasOne(Reward::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function blacklist(){
        return $this->hasOne(Lock::class);
    }

    public function motors(){
        return $this->belongsToMany( Motor::class,'eg_zero.customer_motor');
    }

    //Replace default and custom notify
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
