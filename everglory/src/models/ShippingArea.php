<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;

class ShippingArea extends BaseZero
{
    protected $table = 'shipping_area';

    public function shippingCountry()
    {
        return $this->hasOne(ShippingCountry::class, 'id' ,'country_id');
    }

}