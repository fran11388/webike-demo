<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProductStation;

class PsQaFinalization extends BaseProductStation
{
    protected $table = 'content_qa_finalizations';
    public $primaryKey = 'id';
}
