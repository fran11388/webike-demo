<?php

namespace Everglory\Models\News;

use Everglory\BaseModel\BaseNews;


Class Meta extends BaseNews{

	protected $table = 'wp_postmeta';

//	public function image(){
//		return $this->belongsTo(MotorManufacturer::class);
//	}

	public function post(){
		return $this->belongsTo(Post::class, 'post_id', 'ID');
	}

	public function thumbnail(){
		return $this->belongsTo(Post::class, 'meta_value', 'ID');
	}
}
