<?php

namespace Everglory\Models\News;

use Everglory\BaseModel\BaseNews;


Class Post extends BaseNews{

	protected $table = 'wp_posts';

	public function metas()
	{
		return $this->hasMany(Meta::class, 'post_id', 'ID');
	}


}
