<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\MotorManufacturer
 *
 * @property integer $id
 * @property string $url_rewrite
 * @property string $name
 * @property string $description
 * @property string $country
 * @property string $country_code
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_key
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $image
 * @property string $website
 * @property string $short_description
 * @property string $sort
 * @property-read \Everglory\Models\Motor $motors
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereCountryCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereMetaKeyword($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereMetaKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\MotorManufacturer whereSort($value)
 * @mixin \Eloquent
 */
class MotorManufacturer extends BaseProduct
{

    protected $table = 'motor_manufacturers';

    public function motors(){
        return $this->belongsTo(Motor::class);
    }
}
