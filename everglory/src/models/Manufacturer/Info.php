<?php

namespace Everglory\Models\Manufacturer;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Manufacturer;

Class Info extends BaseProduct
{
    protected $table = 'manufacturer_infos';
    protected $guarded = ['id'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }

}