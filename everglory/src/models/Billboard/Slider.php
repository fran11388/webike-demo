<?php

namespace Everglory\Models\Billboard;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Billboard;


/**
 * Everglory\Models\Billboard\Slider
 *
 * @property-read \Everglory\Models\Billboard $billboard
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $sort
 * @property integer $billboard_id
 * @property string $type
 * @property string $customer_role
 * @property integer $status
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereBillboardId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereCustomerRole($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Slider whereCreatedAt($value)
 */
class Slider extends BaseDbs{

	protected $table = 'billboard_sliders';

	public function billboard(){
		return $this->hasOne( Billboard::class, 'id', 'billboard_id' );
	}

}
