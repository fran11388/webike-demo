<?php

namespace Everglory\Models\Billboard;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Billboard;

/**
 * Everglory\Models\Billboard\Page
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Billboard[] $billboards
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property string $code
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Page whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Page whereCode($value)
 */
class Page extends BaseDbs {

	protected $table = 'billboard_page';

	public function billboards(){
		return $this->hasMany( Billboard::class, 'page_type', 'id' );
	}

}
