<?php

namespace Everglory\Models\Billboard;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\Billboard;

/**
 * Everglory\Models\Billboard\Category
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Billboard[] $billboards
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $page_id
 * @property string $tag
 * @property string $name
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $limit
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category wherePageId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereTag($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Billboard\Category whereLimit($value)
 */
class Category extends BaseDbs{

	protected $table = 'billboard_categories';

	public function billboards(){
		return $this->hasMany( Billboard::class , 'category_id', 'id' );
	}

}
