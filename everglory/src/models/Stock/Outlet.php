<?php

namespace Everglory\Models\Stock;

use Everglory\BaseModel\BaseZero;

/**
 * Everglory\Models\Stock\Outlet
 *
 * @property integer $id
 * @property integer $status_id
 * @property integer $stock_id
 * @property integer $creater_id
 * @property integer $checker_id
 * @property string $description
 * @property string $image_1
 * @property string $image_2
 * @property string $image_3
 * @property float $initial_price
 * @property boolean $auto_discount_flg
 * @property boolean $min_sales_flg
 * @property integer $product_id
 * @property integer $source_product_id
 * @property integer $situation_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Product\StockSituation $situation
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereStockId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereCreaterId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereCheckerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereImage1($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereImage2($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereImage3($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereInitialPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereAutoDiscountFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereMinSalesFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereSourceProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereSituationId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Outlet whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Outlet extends BaseZero
{
	protected $table = 'stock_outlets';
    public function situation()
    {
        return $this->belongsTo( StockSituation::class );
    }

}
