<?php //Zero model
namespace Everglory\Models\Stock;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Product;
use Everglory\Models\Stock\Additional\Area;

/**
 * Everglory\Models\Stock\Additional
 *
 * @property int $id
 * @property int $additional_id
 * @property int $product_id
 * @property int $reflect_id
 * @property float $discount
 * @property string $name
 * @property int $quantity
 * @property string $note
 * @property bool $icon_flg
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Everglory\Models\Stock\Additional\Area $area
 * @property-read \Everglory\Models\Product $product
 * @property-read \Everglory\Models\Product $reflect
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereAdditionalId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereDiscount($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereIconFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereReflectId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Additional  extends BaseZero {


    protected $table = 'stock_additionals';
    protected $guarded  = array('id');
    
    public function reflect(){
        return $this->belongsTo( Product::class , 'reflect_id', 'id' );
    }

    public function area(){
        return $this->belongsTo( Area::class , 'additional_id', 'id' );
    }

    public function product(){
        return $this->belongsTo( Product::class );
    }
}