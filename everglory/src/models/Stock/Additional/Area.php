<?php //Zero model
namespace Everglory\Models\Stock\Additional;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Stock\Additional;

/**
 * Everglory\Models\Stock\Additional\Area
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $trigger_price
 * @property string $class
 * @property bool $status
 * @property string $short_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Stock\Additional[] $items
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereClass($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereShortName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereTriggerPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Stock\Additional\Area whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Area  extends BaseZero {


    protected $table = 'stock_additional_area';
    protected $guarded  = array('id');

    public function items()
    {
        return $this->hasMany( Additional::class ,'additional_id');
    }
}