<?php

namespace Everglory\Models\Stock;

use Everglory\BaseModel\BaseZero;


/**
 * Everglory\Models\Product\StockSituation
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $sort
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\StockSituation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\StockSituation whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\StockSituation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\StockSituation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\StockSituation whereSort($value)
 * @mixin \Eloquent
 */
class Situation extends BaseZero
{
	protected $table = 'stock_situations';

}
