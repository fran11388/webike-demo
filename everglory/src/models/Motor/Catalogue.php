<?php

namespace Everglory\Models\Motor;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Motor\Catalogue\Color;


/**
 * Everglory\Models\Motor\Catalogue
 *
 * @property int $model_catalogue_id
 * @property string $model_search_key
 * @property int $model_code
 * @property int $model_maker_code
 * @property int $model_displacement
 * @property string $model_hyouji
 * @property string $model_name
 * @property string $model_name_en
 * @property string $model_name_kana1
 * @property string $model_name_kana2
 * @property string $model_name_kanji
 * @property string $model_name_alphabet
 * @property string $model_number_frame
 * @property string $model_katashiki
 * @property string $model_grade
 * @property int $model_release_year
 * @property int $model_release_month
 * @property int $model_release_date
 * @property int $model_eoa_year
 * @property int $model_eoa_month
 * @property int $model_eoa_date
 * @property string $model_engin_data エンジン型式
 * @property bool $model_current_flag
 * @property int $model_group_code グループコード
 * @property bool $model_group_top グループ親フラグ
 * @property int $model_price
 * @property int $model_price_without_tax 税抜価格
 * @property string $model_country
 * @property string $model_destination 仕向地
 * @property string $model_image_file 画像ファイル
 * @property string $model_image_url 画像パス
 * @property string $model_text
 * @property string $created_on
 * @property string $created_user_id
 * @property string $updated_on
 * @property string $updated_user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor\AttributeFlag[] $flags
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor\AttributeFloat[] $floats
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor\AttributeGroup[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor\AttributeInteger[] $integers
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Motor\AttributeString[] $strings
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereCreatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelCatalogueId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelCurrentFlag($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelDestination($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelDisplacement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelEnginData($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelEoaDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelEoaMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelEoaYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelGrade($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelGroupTop($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelHyouji($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelImageFile($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelImageUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelKatashiki($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelMakerCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelNameAlphabet($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelNameKana1($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelNameKana2($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelNameKanji($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelNumberFrame($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelPriceWithoutTax($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelReleaseDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelReleaseMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelReleaseYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelSearchKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereModelText($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereUpdatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\Catalogue whereUpdatedUserId($value)
 * @mixin \Eloquent
 */
class Catalogue extends BaseProduct
{

//    protected $table = 'motor_catalogues';
    protected $table = 'mst_model_catalogue';
    protected $primaryKey = 'model_catalogue_id';
    
    public function strings(){
        return $this->hasMany( AttributeString::class ,'model_catalogue_id','model_catalogue_id');
    }
    
    public function flags(){
        return $this->hasMany( AttributeFlag::class ,'model_catalogue_id','model_catalogue_id');
    }
    
    public function floats(){
        return $this->hasMany( AttributeFloat::class ,'model_catalogue_id','model_catalogue_id');
    }
    
    public function groups(){
        return $this->hasMany( AttributeGroup::class ,'model_catalogue_id','model_catalogue_id');
    }
    
    public function integers(){
        return $this->hasMany( AttributeInteger::class ,'model_catalogue_id','model_catalogue_id');
    }

    public function colors(){
        return $this->hasMany( Color::class ,'model_catalogue_id','model_catalogue_id');
    }
}
