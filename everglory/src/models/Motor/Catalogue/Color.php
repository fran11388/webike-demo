<?php

namespace Everglory\Models\Motor\Catalogue;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Motor\Catalogue;

class Color extends BaseProduct
{

    protected $table = 'motor_catalogue_colors';
    protected $primaryKey = 'id';
    
    public function Catalogue()
    {
        return $this->belongsTo(Catalogue::class);
    }
    
}
