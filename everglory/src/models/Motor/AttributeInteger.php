<?php

namespace Everglory\Models\Motor;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Motor\AttributeInteger
 *
 * @property int $model_catalogue_id
 * @property int $attribute_code
 * @property string $attribute_integer_display
 * @property int $attribute_integer_values
 * @property int $attribute_integer_sort
 * @property string $created_on
 * @property string $created_user_id
 * @property string $updated_on
 * @property string $updated_user_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereAttributeIntegerDisplay($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereAttributeIntegerSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereAttributeIntegerValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereCreatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereModelCatalogueId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereUpdatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeInteger whereUpdatedUserId($value)
 * @mixin \Eloquent
 */
class AttributeInteger extends BaseProduct
{

    protected $table = 'tbl_catalogue_attribute_integer';
    
}
