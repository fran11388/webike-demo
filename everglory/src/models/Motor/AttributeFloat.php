<?php

namespace Everglory\Models\Motor;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Motor\AttributeFloat
 *
 * @property int $model_catalogue_id
 * @property int $attribute_code
 * @property string $attribute_float_display
 * @property float $attribute_float_values
 * @property int $attribute_float_sort
 * @property string $created_on
 * @property string $created_user_id
 * @property string $updated_on
 * @property string $updated_user_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereAttributeFloatDisplay($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereAttributeFloatSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereAttributeFloatValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereCreatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereModelCatalogueId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereUpdatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFloat whereUpdatedUserId($value)
 * @mixin \Eloquent
 */
class AttributeFloat extends BaseProduct
{

    protected $table = 'tbl_catalogue_attribute_float';
    
}
