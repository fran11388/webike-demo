<?php

namespace Everglory\Models\Motor;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Motor\AttributeFlag
 *
 * @property int $model_catalogue_id カタログID
 * @property int $attribute_code 属性コード
 * @property bool $attribute_flag_values 値(0 or 1)
 * @property int $attribute_flag_sort ソート番号
 * @property string $created_on 作成日
 * @property string $created_user_id 作成ユーザーID
 * @property string $updated_on 更新日
 * @property string $updated_user_id 更新ユーザーID
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereAttributeFlagSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereAttributeFlagValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereCreatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereModelCatalogueId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereUpdatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeFlag whereUpdatedUserId($value)
 * @mixin \Eloquent
 */
class AttributeFlag extends BaseProduct
{

    protected $table = 'tbl_catalogue_attribute_flag';
    
}
