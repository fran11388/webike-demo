<?php

namespace Everglory\Models\Motor;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Motor\AttributeGroup
 *
 * @property int $model_catalogue_id
 * @property int $attribute_code
 * @property string $attribute_group_display
 * @property int $attribute_group_code
 * @property int $attribute_group_sort
 * @property string $created_on
 * @property string $created_user_id
 * @property string $updated_on
 * @property string $updated_user_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereAttributeGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereAttributeGroupDisplay($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereAttributeGroupSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereCreatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereModelCatalogueId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereUpdatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeGroup whereUpdatedUserId($value)
 * @mixin \Eloquent
 */
class AttributeGroup extends BaseProduct
{

    protected $table = 'tbl_catalogue_attribute_group';
    
}
