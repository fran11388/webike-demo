<?php

namespace Everglory\Models\Motor;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Motor\AttributeString
 *
 * @property int $model_catalogue_id カタログID
 * @property int $attribute_code 属性コード
 * @property string $attribute_string_values 管理値(文字列)
 * @property int $attribute_string_sort ソート番号
 * @property string $created_on 作成日
 * @property string $created_user_id 作成ユーザーID
 * @property string $updated_on 更新日
 * @property string $updated_user_id 更新ユーザーID
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereAttributeStringSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereAttributeStringValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereCreatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereModelCatalogueId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereUpdatedOn($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor\AttributeString whereUpdatedUserId($value)
 * @mixin \Eloquent
 */
class AttributeString extends BaseProduct
{

    protected $table = 'tbl_catalogue_attribute_string';
    
}
