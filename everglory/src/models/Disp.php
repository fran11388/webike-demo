<?php

namespace  Everglory\Models;

use Everglory\BaseModel\BaseDbs;
use Illuminate\Database\Eloquent\SoftDeletes;

Class Disp extends BaseDbs
{
    use SoftDeletes;
    protected $table = 'disps';
    protected $dates = ['deleted_at'];

    public function object(){
        return $this->morphTo();
    }
}

