<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Product\Series
 *
 * @property integer $series_id
 * @property integer $product_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Series whereSeriesId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Series whereProductId($value)
 * @mixin \Eloquent
 */
class Series extends BaseProduct
{
	protected $table = 'product_series';
	public $primaryKey = 'product_id';

}
