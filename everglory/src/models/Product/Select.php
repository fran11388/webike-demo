<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Select
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort
 * @property integer $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Select whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Select whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Select whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Select whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Select whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Select whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Select extends BaseProduct
{
    protected $table = 'selects';
}