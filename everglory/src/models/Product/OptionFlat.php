<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\Product;

/**
 * Everglory\Models\Product\OptionFlat
 *
 * @property integer $product_id
 * @property string $name
 * @property integer $group_code
 * @property integer $manufacturer_id
 * @property-read \Everglory\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\OptionFlat whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\OptionFlat whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\OptionFlat whereGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\OptionFlat whereManufacturerId($value)
 * @mixin \Eloquent
 */
class OptionFlat extends BaseProduct
{
    protected $table = 'product_option_flat';
    public function product(){
        return $this->belongsTo(Product::class);
    }
}