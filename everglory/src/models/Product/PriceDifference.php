<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Price
 *
 * @property integer $id
 * @property string $rule_date
 * @property integer $product_id
 * @property string $name
 * @property integer $role_id
 * @property float $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereRuleDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Price whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class PriceDifference extends BaseProduct
{
    protected $table='product_price_difference';
}