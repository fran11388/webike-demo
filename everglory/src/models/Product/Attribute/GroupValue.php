<?php

namespace Everglory\Models\Product\Attribute;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Product\Attribute\GroupValue
 *
 * @property integer $product_id
 * @property integer $attribute_code
 * @property string $attribute_group_display
 * @property integer $attribute_group_code
 * @property integer $attribute_group_sort
 * @property integer $created_user_id
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_user_id
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereAttributeGroupDisplay($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereAttributeGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereAttributeGroupSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereUpdatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\GroupValue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class GroupValue extends BaseProduct{

	protected $table = 'product_attribute_group';

}
