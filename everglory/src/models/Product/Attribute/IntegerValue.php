<?php

namespace Everglory\Models\Product\Attribute;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Product\Attribute\IntegerValue
 *
 * @property integer $product_id
 * @property integer $attribute_code
 * @property string $attribute_integer_display
 * @property integer $attribute_integer_values
 * @property integer $attribute_integer_sort
 * @property integer $created_user_id
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_user_id
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereAttributeIntegerDisplay($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereAttributeIntegerValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereAttributeIntegerSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereUpdatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\IntegerValue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IntegerValue extends BaseProduct{

	protected $table = 'product_attribute_integer';

}
