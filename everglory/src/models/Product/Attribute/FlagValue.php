<?php

namespace Everglory\Models\Product\Attribute;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Attribute\FlagValue
 *
 * @property integer $product_id
 * @property integer $attribute_code
 * @property boolean $attribute_flag_values
 * @property integer $attribute_flag_sort
 * @property integer $created_user_id
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_user_id
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereAttributeFlagValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereAttributeFlagSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereUpdatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FlagValue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FlagValue extends BaseProduct{

	protected $table = 'product_attribute_flag';

}
