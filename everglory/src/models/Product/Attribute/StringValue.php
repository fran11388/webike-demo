<?php

namespace Everglory\Models\Product\Attribute;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Attribute\StringValue
 *
 * @property integer $product_id
 * @property integer $attribute_code
 * @property string $attribute_string_values
 * @property integer $attribute_string_sort
 * @property integer $created_user_id
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_user_id
 * @property \Carbon\Carbon $updated_at
 * @property boolean $status_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereAttributeStringValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereAttributeStringSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereUpdatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\StringValue whereStatusId($value)
 * @mixin \Eloquent
 */
class StringValue extends BaseProduct{

	protected $table = 'product_attribute_string';

}
