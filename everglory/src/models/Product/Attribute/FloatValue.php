<?php

namespace Everglory\Models\Product\Attribute;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Attribute\FloatValue
 *
 * @property integer $product_id
 * @property integer $attribute_code
 * @property string $attribute_float_display
 * @property float $attribute_float_values
 * @property integer $attribute_float_sort
 * @property integer $created_user_id
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_user_id
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereAttributeCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereAttributeFloatDisplay($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereAttributeFloatValues($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereAttributeFloatSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereCreatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereUpdatedUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Attribute\FloatValue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FloatValue extends BaseProduct{

	protected $table = 'product_attribute_float';

}
