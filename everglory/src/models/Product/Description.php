<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Description
 *
 * @property integer $product_id
 * @property string $remarks
 * @property string $summary
 * @property string $sentence
 * @property string $caution
 * @property boolean $remarks_trans_flg
 * @property boolean $summary_trans_flg
 * @property boolean $sentence_trans_flg
 * @property boolean $caution_trans_flg
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereRemarks($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereSummary($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereSentence($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereCaution($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereRemarksTransFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereSummaryTransFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereSentenceTransFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereCautionTransFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Description whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Description extends BaseProduct
{
    protected $table='product_description';
}