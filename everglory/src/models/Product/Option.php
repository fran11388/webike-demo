<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Option
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $select_id
 * @property string $name
 * @property integer $sort
 * @property boolean $sold_out
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereSelectId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereSoldOut($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Option whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Everglory\Models\Product\Select $label
 */
class Option extends BaseProduct
{
    protected $table = 'product_option';

    public function label(){
        return $this->belongsTo(Select::class , 'select_id', 'id' );
    }
}