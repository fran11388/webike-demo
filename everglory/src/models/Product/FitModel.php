<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;
use Everglory\Models\MotorManufacturer;

/**
 * Everglory\Models\Product\FitModel
 *
 * @property integer $id
 * @property integer $product_id
 * @property boolean $custom_flg
 * @property string $maker
 * @property string $model
 * @property string $style
 * @property integer $motor_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereCustomFlg($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereMaker($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereModel($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereStyle($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereMotorId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\FitModel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Everglory\Models\MotorManufacturer $motorManufacturerByName
 */
class FitModel extends BaseProduct
{
    protected $table='product_fit_models';

    public function motorManufacturerByName(){
        return $this->belongsTo(MotorManufacturer::class ,'maker','name');
    }
}