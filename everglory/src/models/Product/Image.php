<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;;

/**
 * Everglory\Models\Product\Image
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $thumbnail
 * @property string $image
 * @property integer $sort
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereThumbnail($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereSort($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Image whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Image extends BaseProduct
{
    protected $table='product_images';
    
}