<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Product\Point
 *
 * @property integer $id
 * @property string $rule_date
 * @property integer $product_id
 * @property string $name
 * @property integer $role_id
 * @property float $point
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereRuleDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point wherePoint($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Point whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Point extends BaseProduct
{
    protected $table='product_points';
}