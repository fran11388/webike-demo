<?php

namespace Everglory\Models\Product;

use Everglory\BaseModel\BaseProduct;


/**
 * Everglory\Models\Product\Relation
 *
 * @property integer $product_id
 * @property string $groups
 * @property integer $relation_product_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\Product\Relation[] $relations
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Relation whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Relation whereGroups($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Product\Relation whereRelationProductId($value)
 * @mixin \Eloquent
 */
class Relation extends BaseProduct
{
	protected $table = 'product_relations';
	public $primaryKey = 'product_id';

	public function relations()
	{
		return $this->hasMany(Relation::class,'relation_product_id','relation_product_id');
	}

}
