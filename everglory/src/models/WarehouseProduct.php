<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseWarehouse;

class WarehouseProduct extends BaseWarehouse
{
    protected $table = 'core_product';
    public $primaryKey = 'id';
  }
