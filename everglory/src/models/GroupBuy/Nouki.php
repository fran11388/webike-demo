<?php

namespace Everglory\Models\GroupBuy;

use Everglory\BaseModel\BaseDbs;

Class Nouki extends BaseDbs
{
    protected $table = 'groupbuy_nouki_code';
    protected $guarded = ['nouki_code'];

}