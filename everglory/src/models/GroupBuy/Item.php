<?php

namespace Everglory\Models\GroupBuy;

use Everglory\BaseModel\BaseDbs;
use Everglory\Models\GroupBuy;
use Everglory\Models\Manufacturer;
use Everglory\Models\Product;
use Everglory\Models\GroupBuy\Nouki;

Class Item extends BaseDbs
{
    protected $table = 'groupbuy_items';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function groupbuy()
    {
        return $this->belongsTo(GroupBuy::class, 'groupbuy_id', 'id');
    }

    public function manufacturer(){
        return $this->belongsTo(Manufacturer::class, 'product_manufacturer_id', 'id' );
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function nouki()
    {
        return $this->hasOne(Nouki::class, 'nouki_code', 'product_nouki');
    }
}