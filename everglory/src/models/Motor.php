<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseProduct;

/**
 * Everglory\Models\Motor
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $manufacturer_id
 * @property string $url_rewrite
 * @property integer $displacement
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $short_description
 * @property string $short_name
 * @property string $synonym
 * @property string $motor_type
 * @property integer $old_id magento_id
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereManufacturerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereUrlRewrite($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereDisplacement($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereShortDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereShortName($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereSynonym($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereMotorType($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereOldId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Everglory\Models\MotorSpecification[] $specifications
 * @property-read \Everglory\Models\MotorManufacturer $manufacturer
 * @property string $image_path
 * @property string $image
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Everglory\Models\Motor whereImagePath($value)
 */
class Motor extends BaseProduct
{

    protected $table = 'eg_product.motors';

    public function specifications(){
        return $this->hasMany(MotorSpecification::class );
    }
    public function manufacturer(){
        return $this->belongsTo(MotorManufacturer::class);
    }

}
