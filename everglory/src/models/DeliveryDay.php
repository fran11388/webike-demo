<?php

namespace  Everglory\Models;

use Everglory\BaseModel\BaseZero;

Class DeliveryDay extends BaseZero
{
    protected $table = 'delivery_days';
    protected $guarded  = ['code'];

}

