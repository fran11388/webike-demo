<?php

namespace Everglory\Models;

use Everglory\BaseModel\BaseZero;
use Everglory\Models\Estimate\Item;
use Everglory\Models\Estimate\Status;

Class Estimate extends BaseZero
{
    protected $table = 'estimates';
    protected $guarded = ['id'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
    
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}

