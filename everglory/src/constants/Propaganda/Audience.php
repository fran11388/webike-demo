<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/6
 * Time: 下午 04:52
 */

namespace Everglory\Constants\Propaganda;


class Audience
{
    const ALL = 1; //全顯示
    const NORMAL = 2; //一般會員(含Staff)
    const DEALER = 3; //經銷商
    const ALL_EXCEPT_DEALER = 4; //全顯示(排除經銷商)
}