<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/6
 * Time: 下午 04:51
 */

namespace Everglory\Constants\Propaganda;


class Position
{
    const ALL = 1; //全顯示
    const PC = 2; //PC版顯示
    const MOBILE = 3; //手機板顯示

    const PC_BLADE = [Position::ALL, Position::PC];
    const MOBILE_BLADE = [Position::ALL, Position::MOBILE];
}