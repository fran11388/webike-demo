<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/6
 * Time: 下午 04:49
 */

namespace Everglory\Constants\Propaganda;


class Type
{
    const STRIPE = 1;  //橫條
    const HEAD_STRIPE = 2;  //HEAD 橫條
    const POPUP = 3;  //POPUP
    const DESK_TAG = 4;  //桌機版tag
    const MOBILE_TAG = 5;  //手機版tag
    const OUTLET_BANNER = 6;  //Outlet Banner
}