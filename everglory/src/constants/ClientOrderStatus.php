<?php

namespace Everglory\Constants;

use ReflectionClass;

class ClientOrderStatus extends BaseConstant
{
    const ESTIMATE = [
        'status_ids' => [1, 2, 11],
        'name' => '交期確認中',
        'state' => '',
    ];
    const PENDING = [
        'status_ids' => [3, 4, 5, 12, 13],
        'name' => '等待付款中',
        'state' => '',
    ];
    const SHIPPING = [
        'status_ids' => [8, 9, 15],
        'name' => '準備出貨',
        'state' => '',
    ];
    const BUYING = [
        'status_ids' => [6, 7, 14],
        'name' => '商品備貨',
        'state' => [true=>'預定出貨日：<br>{{$date}}<br>宅配單號<br><a href="https://www.hct.com.tw/Search/SearchGoods_n.aspx" title="宅配貨件追蹤" target="_blank">{{$shipping_code}}</a>',false=>'預定出貨日：<br>{{$date}}<br>宅配單號<br><a href="http://query2.e-can.com.tw/self_link/id_link_c.asp?txtMainid={{$shipping_code}}" title="宅配貨件追蹤" target="_blank">{{$shipping_code}}</a>'],
    ];
    const FINISH = [
        'status_ids' => [10],
        'name' => '配送完成',
        'state' => '完成({{$date}})',
    ];

    public static function getSelfConsts()
    {
        $reflect = new ReflectionClass(__CLASS__);
        return $reflect->getConstants();
    }
}