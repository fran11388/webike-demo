<?php

namespace Everglory\Constants;

class SEO extends BaseConstant
{
    const TITLE = '「Webike-摩托百貨」進口‧國產重機、機車改裝部品、正廠零件、騎士用品-台灣最大級摩托商品購物商城!';

    const DESCRIPTION = '【超過1300個全球知名品牌、40萬項商品】進口國產最新改裝部品、24車廠正廠零件、當季新款人身部品應有盡有。免費加入會員享有更多好康，Webike滿足您的摩托人生!';

    const KEYWORDS = '進口重機, 機車, 摩托車, 重型機車, 電單車, 改裝, 正廠, 零件, 精品, 騎士用品, 人身部品, 日本, 進口, 高品質, honda, yamaha, suzuki, kawasaki, 三陽, 光陽, 台鈴, 山葉, ducati, aprilia, ktm, bmw, 哈雷, HARLEY-DAVIDSON, TRIUMPH, 凱旋, HUSABERG, HUSQVARNA, MV AGUSTA, BIMOTA, 網路商城, 線上購物賣場';

    const IMAGE = 'https://img.webike.tw/shopping/image/webike_shopping_logo.png';

    const SHIPPING_FREE = [true=>'【購物滿千免運】',false=>'【支援3、6及12分期付款】'];

    const WEBIKE_SHOPPING_LOGO_TEXT = '「Webike-摩托百貨」';

    const WEBIKE_TW_LOGO_TEXT = '「Webike-台灣」';

}