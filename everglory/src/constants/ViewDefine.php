<?php

namespace Everglory\Constants;

class ViewDefine
{
    const MAIN_MOTOR_MANUFACTURERS = [
        'HONDA',
        'YAMAHA',
        'SUZUKI',
        'Kawasaki',
        'HARLEY-DAVIDSON',
        'BMW',
        'DUCATI',
//        'KTM',
        'APRILIA',
        'TRIUMPH',
        'KYMCO',
        'TW_YAMAHA',
        'SYM',
        'TW_SUZUKI',
        'PGO',
    ];
    const CUSTOM_CATEGORY_PATH = 1000;
    const RIDER_CATEGORY_PATH = 3000;
    const MAINTENANCE_CATEGORY_PATH = '1000-4000';
}