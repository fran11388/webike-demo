<?php

namespace Everglory\Constants;

class Palette
{
    const CUSTOMER = [
        ['background' => '#78909c', 'color' => '#fff', 'border' => '1px #566f7b solid'],
        ['background' => '#428bca', 'color' => '#fff', 'border' => '1px #1e69ab solid'],
        ['background' => '#5bc0de', 'color' => '#fff', 'border' => '1px #46b8da solid'],
        ['background' => '#d9534f', 'color' => '#fff', 'border' => '1px #d43f3a solid'],
        ['background' => '#dff0d8', 'color' => '#333', 'border' => '1px #00C851 solid'],
        ['background' => '#5cb85c', 'color' => '#fff', 'border' => '1px #4cae4c solid'],
        ['background' => '#fff', 'color' => '#333', 'border' => '1px #cccccc solid'],
        ['background' => '#337ab7', 'color' => '#fff', 'border' => '1px #2e6da4 solid'],
        ['background' => '#f0ad4e', 'color' => '#fff', 'border' => '1px #eea236 solid'],
        ['background' => '#4CAF50', 'color' => '#fff', 'border' => '1px #4CAF50 solid'],
    ];
}