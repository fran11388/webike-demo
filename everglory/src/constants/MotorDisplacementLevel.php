<?php

namespace Everglory\Constants;

class MotorDisplacementLevel extends BaseConstant
{
    const DISPLACEMENT_50 = ' - 50cc';
    const DISPLACEMENT_125 = ' 51cc-125cc';
    const DISPLACEMENT_250 = ' 126cc-250cc';
    const DISPLACEMENT_400 = ' 251cc-400cc';
    const DISPLACEMENT_750 = ' 401cc-750cc';
    const DISPLACEMENT_1000 = ' 751cc-1000cc';
    const DISPLACEMENT_1001 = ' 1001cc 以上';
    const DISPLACEMENT_LEVELS = ['50','125','250','400','750','1000','1001'];

}