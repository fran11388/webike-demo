<?php

namespace Everglory\Constants;

use ReflectionClass;

class BaseConstant
{
    public static function getConst($attribure)
    {
        if($attribure){
            return constant('static::' . $attribure);
        }
        return null;
    }

    public static function getConsts($attribures = NULL)
    {
        if($attribures){
            $arr = [];
            foreach ($attribures as $attribure){
                $arr[$attribure] = constant('static::' . $attribure);
            }
            return $arr;
        }
    }

    public static function getConstName($value)
    {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());

        return $constants[$value];
    }
}