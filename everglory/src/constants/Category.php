<?php

namespace Everglory\Constants;

class Category
{
    const TOP = [1000,3000,4000,8000];

    const REDIRECTS = [
        '1000-5000' => '4000-5000',
        '1000-4000' => '4000',
        '1000-8000' => '8000',

    ];
}