<?php

namespace Everglory\Constants;

class PointType
{
    const NORMAL = '0';

    const DISCOUNT = '1';
}