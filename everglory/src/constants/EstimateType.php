<?php

namespace Everglory\Constants;

use Everglory\Constants\BaseConstant;

class EstimateType extends BaseConstant
{
    const IMPORT = 1;
    const DOMESTIC = 2;
    const GENERAL = 3;
}