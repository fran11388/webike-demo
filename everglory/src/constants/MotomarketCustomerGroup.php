<?php

namespace Everglory\Constants;

class MotomarketCustomerGroup
{
    const REGISTERED = 1;
    const GENERAL = 2;
    const CORPORATION = 3;
    const SUPER_ADMIN = 4;
}