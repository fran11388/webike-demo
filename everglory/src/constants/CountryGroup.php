<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2016/10/4
 * Time: 上午 10:31
 */

namespace Everglory\Constants;

use ReflectionClass;

class CountryGroup extends BaseConstant
{

    const CACHE_LIFETIME = 24 * 60;

    const EU = [
        'code' => 'EU',
        'name' => '歐系',
        'group' => ['英國', '義大利', '德國', '奧地利'],
        'icons' => ['image/oempart/icon-flag044.jpg'],
    ];
    const JP = [
        'code' => 'JP',
        'name' => '日本',
        'group' => ['日本', '韓國'],
        'icons' => ['image/oempart/icon-flag02.jpg'],
    ];
    const US = [
        'code' => 'US',
        'name' => '美系',
        'group' => ['美國'],
        'icons' => ['image/oempart/icon-flag03.jpg'],
    ];
    const EU_US = [
        'code' => 'EU_US',
        'name' => '歐美',
        'group' => ['美國', '英國', '義大利', '德國', '奧地利'],
        'icons' => ['image/oempart/icon-flag044.jpg', 'image/oempart/icon-flag03.jpg'],
    ];
    const TH = [
        'code' => 'TH',
        'name' => '泰國',
        'group' => ['泰國'],
        'icons' => ['image/oempart/icon-flag055.jpg'],
    ];
    const TW = [
        'code' => 'TW',
        'name' => '台灣',
        'group' => ['台灣'],
        'icons' => ['image/oempart/icon-flag01.jpg'],
    ];
    const DOMESTIC = [
        'code' => 'DOMESTIC',
        'name' => '國產',
        'url_code' => 'domestic',
        'group' => ['台灣']
    ];
    const IMPORT = [
        'code' => 'IMPORT',
        'name' => '進口',
        'url_code' => 'import',
        'group' => ['台灣']
    ];

}