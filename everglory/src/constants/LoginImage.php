<?php

namespace Everglory\Constants;

use Everglory\Constants\BaseConstant;

class LoginImage extends BaseConstant
{
    const GALLERY = [
        '//img.webike.tw/assets/images/login/170305_01.jpg',
        '//img.webike.tw/assets/images/login/170305_02.jpg',
        '//img.webike.tw/assets/images/login/170305_03.jpg',
        '//img.webike.tw/assets/images/login/170305_04.jpg',
        '//img.webike.tw/assets/images/login/170305_05.jpg',
        '//img.webike.tw/assets/images/login/170305_05.jpg',
    ];
}