<?php

namespace Everglory\Constants;

class ProductType
{
    const NORMAL = 0;
    const GENUINEPARTS = 1;
    const UNREGISTERED = 2;
    const GROUPBUY = 3;
    const ADDITIONAL = 4;
    const OUTLET = 5;
}