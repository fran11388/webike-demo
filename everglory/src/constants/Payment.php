<?php

namespace Everglory\Constants;

class Payment
{
    const CREDIT_CARD = 1;
    const CASH_ON_DELIVERY = 2;
    const Bank_Transfer = 3;
    const CREDIT_CARD_LAYAWAY_ESUN = 4;
    const CREDIT_CARD_LAYAWAY_NCCC = 5;
    const PAID = 6;

}