<?php

namespace Everglory\Constants;

class OrderStatus
{
    const ORDERCANCLE = 0;
    const NEW_ORDER = 1;
    const ESTIMATE = 2;
    const WAIT_PAY = 3;
    const WAIT_REPLIER = 4;
    const WAIT_PURCHASE = 5;
    const PURCHASE  = 6;
    const INTO_STOCK  = 7;
    const WAIE_SHIPPING  = 8;
    const SHIPPING  = 9;
    const FINISH  = 10;
    const NEW_ORDER_ACCEPT = 11;
    const ESTIMATE_CHECK  = 12;
    const PAY_ABERRANT  = 13;
    const ORDER_ABERRANT  = 14;
    const ORDER_RETAIN  = 15;
    const CUSTOMER_CANCLE  = 16;

}