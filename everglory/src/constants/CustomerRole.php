<?php

namespace Everglory\Constants;

class CustomerRole
{
    const REGISTERED = 1;
    const GENERAL = 2;
    const WHOLESALE = 3;
    const STAFF = 4;
    const SPY = 5;
    const BUYER = 6;
    const NOT_LOGGED_IN  = 7;

}