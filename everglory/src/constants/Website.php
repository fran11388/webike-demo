<?php

namespace Everglory\Constants;

class Website
{
    const SHOPPING = 'Webike-摩托百貨';
    const MOTOMARKET = 'Webike-摩托車市';
    const BIKENEWS = 'Webike-摩托新聞';
    const MOTOCHANNEL = 'Webike-摩托頻道';
}