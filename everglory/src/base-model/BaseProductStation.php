<?php

namespace Everglory\BaseModel;

use Eloquent;

class BaseProductStation extends Eloquent
{
    protected $connection = 'smax_productstation';
}