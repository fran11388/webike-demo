<?php

namespace Everglory\BaseModel;

use Eloquent;

class BaseWarehouse extends Eloquent
{
    protected $connection = 'smax_warehouse';
}